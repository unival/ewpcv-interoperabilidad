# Interoperabilidad Erasmus Without Papers (EWP) 

## Gestión de contribuciones externas

### Envío de propuestas de nueva funcionalidad

Aunque la decisión final de aceptar o evolucionar nuevas funcionalidades propuestas será tomada siempre por las universidades que comparten la gobernanza de este proyecto, cualquier entidad externa o usuario podrá realizar propuestas de nueva funcionalidad siempre que estén convenientemente descritas y argumentadas.

El procedimiento para remitir las propuesta se gestionará a través de la página pública del proyecto en Bitbucket. Accediendo al apartado issues, se insertará una nueva incidencia marcada con la etiqueta “proposal” y con todos los detalles necesarios documentados en el campo descripción.

Una vez recibida la propuesta, será discutida en Bitbucket con la participación de los representantes de las universidades miembro antes de que su alcance e implicaciones queden totalmente definidas. Finalmente, esta propuesta se aceptará o denegará.

### Implementación de nuevas funcionalidades

Si se acepta, esta nueva funcionalidad deberá analizarse y ser dividida en el conjunto total de historias de usuario necesarias para su implementación. 

La creación, mantenimiento y evolución de estas historias de usuario se realizará en el backlog que las universidades miembro comparten en JIRA.

Cada historia de usuario deberá pasar, igual que todas las actuales, por una fase de testeo funcional antes de ser aprobada. Esto quiere decir que el código necesario para que esta historia de usuario se pueda implementar deberá ser proporcionado por la persona que la ha propuesto o, en su defecto, por la empresa contratada para su implementación. 

La implementación de la nueva funcionalidad deberá realizarse por la persona o empresa que contribuye y el código deberá subirse al repositorio para ser validado.

El mecanismo de entrega de las modificaciones de código asociadas a cada historia de usuario será el de pull request, siendo este el mecanismo habitual en los proyectos abiertos. El pull request se enviará a una rama nueva que tendrá el ID de la issue interna en JIRA si se conoce o una breve descripción de la funcionalidad si es algo no recogido en la gestión actual del proyecto. El nombre de la rama deberá seguir la estructura "feature/<id de jira>" o "feature/<resumen funcionalidad>".
