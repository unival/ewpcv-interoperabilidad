
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
	CREATE OR REPLACE TYPE EWP.arrayResultado is table OF VARCHAR2(255char);
/
	DECLARE
		id_Actualizar EWP.arrayResultado;
		
		CURSOR DUPLICADOS IS SELECT VERSION, LANG, TEXT, COUNT(1) FROM EWP.EWPCV_LANGUAGE_ITEM item 
		INNER JOIN EWP.EWPCV_INSTITUTION_NAME ein ON item.ID = ein.NAME_ID GROUP BY VERSION, LANG, TEXT, ein.INSTITUTION_ID HAVING COUNT(1) > 1;
		
	BEGIN
		FOR REGISTRO IN DUPLICADOS LOOP
			SELECT ID BULK COLLECT INTO id_Actualizar
			FROM EWP.EWPCV_LANGUAGE_ITEM
			WHERE UPPER("TEXT") = UPPER(REGISTRO."TEXT")
			AND (("LANG" IS NULL AND REGISTRO.LANG IS NULL) OR (UPPER("LANG") = UPPER(REGISTRO.LANG)));
			
			DELETE FROM EWP.EWPCV_INSTITUTION_NAME WHERE NAME_ID IN (select * from table(id_Actualizar)) AND NAME_ID NOT IN (id_Actualizar(1));
			DELETE FROM EWP.EWPCV_LANGUAGE_ITEM WHERE ID IN (select * from table(id_Actualizar)) AND ID NOT IN (id_Actualizar(1));
		   
		END LOOP;
	END;
	
/
	DROP TYPE EWP.ARRAYRESULTADO;
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT.ID) running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_CONTACT C,
			EWP.EWPCV_CONTACT_NAME CN,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE C.ID = CN.CONTACT_ID
			AND CN.NAME_ID = LAIT.ID
			AND C.ID = P_CONTACTO_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT.ID) running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LOS_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE N.NAME_ID = LAIT.ID
			AND N.LOS_ID = P_LOS_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT.ID) running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_ORGANIZATION_UNIT O,
			EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE O.ID = N.ORGANIZATION_UNIT_ID
			AND N.NAME_ID = LAIT.ID
			AND O.ID = P_OUNIT_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_LANGUAGE_ITEM(P_LANG_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT.ID) running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LANGUAGE_ITEM LAIT
        WHERE ID = P_LANG_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_ADDIT_INFO(P_SECTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ADDITIONAL_INFO, '|;|') within group (order by ADDITIONAL_INFO)||
			'...'
	else
		listagg(ADDITIONAL_INFO, '|;|') within group (order by ADDITIONAL_INFO)
	end aux into v_lista
	from
	(
		select
			add_inf.ADDITIONAL_INFO AS ADDITIONAL_INFO ,
			sum(length(add_inf.ADDITIONAL_INFO) + 3) over (order by add_inf.ID) running_length,
			sum(length(add_inf.ADDITIONAL_INFO) + 3) over (order by add_inf.ADDITIONAL_INFO) total_length
		FROM EWP.EWPCV_ADDITIONAL_INFO add_inf
        WHERE add_inf.SECTION_ID=P_SECTION_ID
		order by add_inf.ADDITIONAL_INFO
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION(P_INSTITUTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		 FROM EWP.EWPCV_INSTITUTION I,
			EWP.EWPCV_INSTITUTION_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE I.ID = N.INSTITUTION_ID
			AND N.NAME_ID = LAIT.ID
			AND UPPER(I.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID)
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_COMMON AS 

	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER;

	FUNCTION VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER;

	FUNCTION VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;
	
	FUNCTION VALIDA_DATOS_C_PERSON_LIST(P_CONTACT_PERSON_LIST IN EWP.CONTACT_PERSON_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;
	
	/* 
		Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
	*/
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2);

	/* 
		Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
	*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2);

    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2);

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2);

    /*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta datos de persona independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2;

	/*
		Inserta un telefono de contacto independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2;

    /*
		Inserta una direccion independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2;

    /*
		Inserta datos de contacto
	*/
    FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2;

    /*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2;    

    /*
		Inserta un nivel de idioma independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2;

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) ;

    /*
        Utilidad para pintar clobs por consola
    */
    procedure print_clob( p_clob in clob );


END PKG_COMMON;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_COMMON AS 
	
	-------------------------------------------------------------
	-----------------------VALIDACIONES--------------------------
	-------------------------------------------------------------

	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL AND EWP.VALIDA_CEFR_LEVEL(P_LANGUAGE_SKILL.CEFR_LEVEL, v_error_message) <> 0 THEN 
		v_cod_retorno := -1;
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
								-CEFR_LEVEL: ');
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(2000);
		v_error_message_country VARCHAR2(2000);
		v_error_message_phone VARCHAR2(2000);
	BEGIN
		IF P_CONTACT IS NOT NULL THEN
			IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN 
				FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST LOOP 
					IF EWP.VALIDA_URL(P_CONTACT.CONTACT_URL(i).TEXT, 0,v_error_message_url) <> 0 THEN
						v_cod_retorno := -1;
						
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-URL('|| i|| '): ');
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
						v_error_message_url := '';
					END IF;
				END LOOP;
			END IF;

			IF P_CONTACT.STREET_ADDRESS IS NOT NULL AND P_CONTACT.STREET_ADDRESS.COUNTRY IS NOT NULL THEN 
				IF EWP.VALIDA_COUNTRY_CODE(P_CONTACT.STREET_ADDRESS.COUNTRY, v_error_message_country) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-STREET_ADDRESS:' || ' 
								-COUNTRY: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_country);
					v_error_message_country := '';
				END IF;
			END IF;

			IF P_CONTACT.MAILING_ADDRESS IS NOT NULL AND P_CONTACT.MAILING_ADDRESS.COUNTRY IS NOT NULL THEN 
				IF EWP.VALIDA_COUNTRY_CODE(P_CONTACT.MAILING_ADDRESS.COUNTRY, v_error_message_country) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-MAILING_ADDRESS:' || '
								-COUNTRY: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_country);
					v_error_message_country := '';
				END IF;
			END IF;

			IF P_CONTACT.PHONE IS NOT NULL AND P_CONTACT.PHONE.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_CONTACT.PHONE.E164, v_error_message_phone) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-PHONE: ' || '
								-E164: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
				v_error_message_phone := '';
			END IF;

			IF P_CONTACT.FAX IS NOT NULL AND P_CONTACT.FAX.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_CONTACT.FAX.E164, v_error_message_phone) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-FAX:' || '
								-E164: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
				v_error_message_phone := '';
			END IF;
	
			IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN 
				FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST LOOP 
					IF EWP.VALIDA_EMAIL(P_CONTACT.EMAIL(i), v_error_message_phone) <> 0 THEN
						v_cod_retorno := -1;
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-EMAIL('|| i|| '): ');
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
						v_error_message_phone := '';
					END IF;
				END LOOP;
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_CONTACT_PERSON IS NOT NULL THEN
			IF P_CONTACT_PERSON.CONTACT IS NOT NULL AND VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-CONTACT: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;

			IF P_CONTACT_PERSON.GENDER IS NOT NULL AND EWP.VALIDA_GENDER(P_CONTACT_PERSON.GENDER, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-GENDER: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;

			IF P_CONTACT_PERSON.CITIZENSHIP IS NOT NULL AND EWP.VALIDA_COUNTRY_CODE(P_CONTACT_PERSON.CITIZENSHIP, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-CITIZENSHIP: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_C_PERSON_LIST(P_CONTACT_PERSON_LIST IN EWP.CONTACT_PERSON_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_CONTACT_PERSON_LIST IS NOT NULL AND P_CONTACT_PERSON_LIST.COUNT > 0 THEN 
			FOR i IN P_CONTACT_PERSON_LIST.FIRST .. P_CONTACT_PERSON_LIST.LAST LOOP 
				IF VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON_LIST(i), v_error_message) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
					-CONTACT_PERSON_LIST('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
				END IF;
			END LOOP;
		END IF;
		RETURN v_cod_retorno;
	END;
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	
	/* Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) */
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2) AS
		v_phone_id VARCHAR2(255 CHAR);
        v_fax_id VARCHAR2(255 CHAR);
		v_mailing_id VARCHAR2(255 CHAR);
		v_street_id VARCHAR2(255 CHAR);
		CURSOR c_contact_detail(p_contact_id IN VARCHAR2) IS 
			SELECT PHONE_NUMBER, MAILING_ADDRESS, STREET_ADDRESS, FAX_NUMBER
			FROM EWPCV_CONTACT_DETAILS
			WHERE ID = p_contact_id;
		CURSOR c_urls(p_contact_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_CONTACT_URL
			WHERE CONTACT_DETAILS_ID = p_contact_id;
	BEGIN
		OPEN c_contact_detail(P_ID);
		FETCH c_contact_detail INTO v_phone_id, v_mailing_id, v_street_id, v_fax_id;
		CLOSE c_contact_detail;

		FOR url_rec IN c_urls(P_ID)
		LOOP 
			DELETE FROM EWPCV_CONTACT_URL WHERE URL_ID = url_rec.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = url_rec.URL_ID;
		END LOOP;
		DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = P_ID;

		DELETE FROM EWPCV_CONTACT_DETAILS WHERE ID = P_ID;

		DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_phone_id;

        DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_fax_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_mailing_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_street_id;

		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_street_id;
	END;

	/* Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2) AS
		v_person_id VARCHAR2(255 CHAR);
		v_details_id VARCHAR2(255 CHAR);
		CURSOR c_contact(p_contact_id IN VARCHAR2) IS 
			SELECT PERSON_ID, CONTACT_DETAILS_ID
			FROM EWPCV_CONTACT
			WHERE ID = p_contact_id;
		CURSOR c_names(p_contact_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = p_contact_id;
		CURSOR c_descs(p_contact_id IN VARCHAR2) IS 
			SELECT DESCRIPTION_ID
			FROM EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = p_contact_id;
	BEGIN
		IF P_ID IS NOT NULL THEN 
			OPEN c_contact(P_ID);
			FETCH c_contact INTO v_person_id, v_details_id;
			CLOSE c_contact;
			FOR name_rec IN c_names(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_NAME WHERE NAME_ID = name_rec.NAME_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = name_rec.NAME_ID;
			END LOOP;
			FOR desc_rec IN c_descs(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = desc_rec.DESCRIPTION_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = desc_rec.DESCRIPTION_ID;
			END LOOP;
			DELETE FROM EWPCV_CONTACT WHERE ID = P_ID;
			BORRA_CONTACT_DETAILS(v_details_id);
			DELETE FROM EWPCV_PERSON WHERE ID = v_person_id;
		END IF;
	END;

    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2) AS
        CURSOR c_fs_url(p_fs_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_FACT_SHEET_URL
			WHERE FACT_SHEET_ID = p_fs_id;
    BEGIN
        FOR fact_sheet_url IN c_fs_url(P_FACTSHEET_ID)
		LOOP 
			DELETE FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = fact_sheet_url.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = fact_sheet_url.URL_ID;
		END LOOP; 
    END;

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2) AS
		v_ay_id VARCHAR2(255 CHAR);
		v_ay_count NUMBER;
		CURSOR c_ay(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_YEAR_ID
			FROM EWPCV_ACADEMIC_TERM
			WHERE ID = p_id;

		CURSOR c_n(p_id IN VARCHAR2) IS 
			SELECT DISP_NAME_ID
			FROM EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = p_id;
	BEGIN

		FOR rec IN c_n(P_ID) 
		LOOP
			DELETE FROM EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = rec.DISP_NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.DISP_NAME_ID;
		END LOOP;

		OPEN c_ay(P_ID);
		FETCH c_ay INTO v_ay_id;
		CLOSE c_ay;
		DELETE FROM EWPCV_ACADEMIC_TERM WHERE ID = P_ID;
		SELECT COUNT(1) INTO v_ay_count FROM EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = v_ay_id;
		IF v_ay_count = 0 THEN 
			DELETE FROM EWPCV_ACADEMIC_YEAR WHERE ID = v_ay_id;
		END IF;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_LANGUAGE_ITEM.LANG IS NOT NULL OR  P_LANGUAGE_ITEM.TEXT IS NOT NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (v_id, P_LANGUAGE_ITEM.LANG, P_LANGUAGE_ITEM.TEXT); 
		END IF;

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
	CURSOR exist_lang_it_cursor(p_inst_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_INSTITUTION_NAME INA ON LAIT.ID = INA.NAME_ID 
			WHERE INA.INSTITUTION_ID = p_inst_id AND
			((p_lang IS NULL AND LANG IS NULL) OR (UPPER(LANG) = UPPER(p_lang)))	
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_INSTITUTION_NAMES IS NOT NULL AND P_INSTITUTION_NAMES.COUNT > 0 THEN
			FOR i IN P_INSTITUTION_NAMES.FIRST .. P_INSTITUTION_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_INSTITUTION_ID, P_INSTITUTION_NAMES(i).LANG, P_INSTITUTION_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION_NAMES(i));
                    INSERT INTO EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES (v_lang_item_id, P_INSTITUTION_ID);
				END IF; 
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_INSTITUTION 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_id);
	BEGIN

		OPEN exist_cursor(P_INSTITUTION_ID) ;
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID) VALUES (v_id, LOWER(P_INSTITUTION_ID), P_ABREVIATION, P_LOGO_URL, P_FACTSHEET_ID, P_PRIM_CONT_DETAIL_ID); 
		ELSIF P_ABREVIATION IS NOT NULL OR P_LOGO_URL IS NOT NULL OR P_FACTSHEET_ID IS NOT NULL THEN
			UPDATE  EWPCV_INSTITUTION SET  ABBREVIATION = P_ABREVIATION, LOGO_URL = P_LOGO_URL, FACT_SHEET = P_FACTSHEET_ID WHERE ID = v_id; 
		END IF;

		INSERTA_INSTITUTION_NAMES(v_id, P_INSTITUTION_NAMES);

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_ounit_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_ORGANIZATION_UNIT_NAME ONA ON LAIT.ID = ONA.NAME_ID 
			WHERE ONA.ORGANIZATION_UNIT_ID = p_ounit_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_ORGANIZATION_UNIT_NAMES IS NOT NULL AND P_ORGANIZATION_UNIT_NAMES.COUNT > 0 THEN
			FOR i IN P_ORGANIZATION_UNIT_NAMES.FIRST .. P_ORGANIZATION_UNIT_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_OUNIT_ID, P_ORGANIZATION_UNIT_NAMES(i).LANG,P_ORGANIZATION_UNIT_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := INSERTA_LANGUAGE_ITEM(P_ORGANIZATION_UNIT_NAMES(i));
					INSERT INTO EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES (v_lang_item_id, P_OUNIT_ID);
				END IF;       
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_hei_id IN VARCHAR, p_code IN VARCHAR) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_code)
			AND UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id);

	BEGIN
	    IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			OPEN exist_cursor(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ORGANIZATION_UNIT_CODE);
			FETCH exist_cursor INTO v_id;
			CLOSE exist_cursor;

			IF  v_id IS NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (v_id, P_INSTITUTION.ORGANIZATION_UNIT_CODE); 
			END IF;

			INSERTA_OUNIT_NAMES(v_id, P_INSTITUTION.ORGANIZATION_UNIT_NAME);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
        v_inst_id VARCHAR2(255);
        v_ou_id VARCHAR2(255);
        v_count VARCHAR2(255);
    BEGIN
        v_inst_id := INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, null, null, null, P_INSTITUTION.INSTITUTION_NAME, null);
        IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            v_ou_id := INSERTA_OUNIT(P_INSTITUTION);
            SELECT COUNT(1) INTO v_count FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = v_ou_id;
            IF v_count = 0 THEN 
                INSERT INTO EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (v_inst_id, v_ou_id);
            END IF;
        END IF;
        RETURN v_ou_id;



    END;

	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PERSON.GIVEN_NAME IS NOT NULL 
			OR P_PERSON.FAMILY_NAME IS NOT NULL 
			OR P_PERSON.BIRTH_DATE IS NOT NULL 
			OR P_PERSON.CITIZENSHIP IS NOT NULL 
			OR P_PERSON.GENDER IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
					VALUES (v_id, P_PERSON.GIVEN_NAME, P_PERSON.FAMILY_NAME, UPPER(P_PERSON.CITIZENSHIP), P_PERSON.BIRTH_DATE, P_PERSON.GENDER);
		END IF;
		return v_id;
	END;

	/*
		Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PHONE.E164 IS NOT NULL 
			OR P_PHONE.EXTENSION_NUMBER IS NOT NULL 
			OR P_PHONE.OTHER_FORMAT IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
					VALUES (v_id, P_PHONE.E164, P_PHONE.EXTENSION_NUMBER, P_PHONE.OTHER_FORMAT);
		END IF;
		return v_id;
	END;

	/*
		Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_ADDRESS.POSTAL_CODE IS NOT NULL 
			OR P_ADDRESS.LOCALITY IS NOT NULL 
			OR P_ADDRESS.REGION IS NOT NULL
			OR P_ADDRESS.COUNTRY IS NOT NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (v_id, P_ADDRESS.BUILDING_NUMBER, P_ADDRESS.BUILDING_NAME, UPPER(P_ADDRESS.COUNTRY),
					P_ADDRESS.BUILDING_FLOOR, P_ADDRESS.LOCALITY, P_ADDRESS.POST_OFFICE_BOX, P_ADDRESS.POSTAL_CODE,
					P_ADDRESS.REGION, P_ADDRESS.STREET_NAME,P_ADDRESS.UNIT );

				IF P_ADDRESS.RECIPIENT_NAMES IS NOT NULL AND P_ADDRESS.RECIPIENT_NAMES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.RECIPIENT_NAMES.FIRST .. P_ADDRESS.RECIPIENT_NAMES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) VALUES (v_id, P_ADDRESS.RECIPIENT_NAMES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.ADDRESS_LINES IS NOT NULL AND P_ADDRESS.ADDRESS_LINES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.ADDRESS_LINES.FIRST .. P_ADDRESS.ADDRESS_LINES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE) VALUES (v_id, P_ADDRESS.ADDRESS_LINES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.DELIVERY_POINT_CODES IS NOT NULL AND P_ADDRESS.DELIVERY_POINT_CODES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.DELIVERY_POINT_CODES.FIRST .. P_ADDRESS.DELIVERY_POINT_CODES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE) VALUES (v_id, P_ADDRESS.DELIVERY_POINT_CODES(i));
					END LOOP;
				END IF;
		END IF;
		return v_id;
	END;

	/*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
		v_f_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
        v_ma_id VARCHAR2(255);
	BEGIN
		v_p_id:= INSERTA_TELEFONO(P_CONTACT.PHONE);
		v_f_id:= INSERTA_TELEFONO(P_CONTACT.FAX);
		v_a_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.STREET_ADDRESS);
        v_ma_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.MAILING_ADDRESS);

		IF v_p_id IS NOT NULL 
			OR v_a_id IS NOT NULL 
			OR (P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0) 
			OR (P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0) THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT_DETAILS (ID, FAX_NUMBER, PHONE_NUMBER, STREET_ADDRESS, MAILING_ADDRESS) VALUES (v_id, v_f_id, v_p_id, v_a_id, v_ma_id);

				IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN
					FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST 
					LOOP
						INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (v_id, P_CONTACT.EMAIL(i));
					END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN
					FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST 
					LOOP
						v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_URL(i));
						INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (v_id, v_li_id);
					END LOOP;
				END IF;
		END IF;

		return v_id;
	END;

    /*
		Inserta datos de contacto y retorna id de contact detail id
	*/
	FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
		v_li_id VARCHAR2(255);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN

		v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT);

		IF v_c_id IS NOT NULL 
			OR (P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0) 
			OR (P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;

               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;

				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT.CONTACT_ROLE, v_c_id, null);

				IF P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_NAME.FIRST .. P_CONTACT.CONTACT_NAME.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_NAME(i));
							INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT.CONTACT_DESCRIPTION.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_DESCRIPTION(i));
							INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;
		END IF;	
		RETURN v_c_id;
	END;

	/*
        Inserta datos de contacto y de persona 
    */
    FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
        v_id VARCHAR2(255);
        v_p_id VARCHAR2(255);
        v_c_id VARCHAR2(255);
        v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
        v_li_id VARCHAR2(255);
        CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
            FROM EWPCV_ORGANIZATION_UNIT O 
            INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
            INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
            WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
            AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
    BEGIN

        v_p_id := INSERTA_PERSONA(P_CONTACT_PERSON);
        v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT);

        IF v_p_id IS NOT NULL 
            OR v_c_id IS NOT NULL 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0) 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;

               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;


                v_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
                    VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT_PERSON.CONTACT.CONTACT_ROLE, v_c_id, v_p_id);



                IF P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_NAME.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_NAME.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_NAME(i));
                            INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;



                IF P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION(i));
                            INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;
        END IF;    
        RETURN v_id;
    END;

		/*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN

		IF P_SUBJECT_AREA.ISCED_CODE IS NOT NULL OR P_SUBJECT_AREA.ISCED_CLARIFICATION IS NOT NULL THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (v_id, P_SUBJECT_AREA.ISCED_CODE, P_SUBJECT_AREA.ISCED_CLARIFICATION);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2 AS
        v_id VARCHAR2(255);
        CURSOR exist_cursor(p_lang IN VARCHAR, p_cefr IN VARCHAR) IS SELECT ID
            FROM EWPCV_LANGUAGE_SKILL
            WHERE UPPER("LANGUAGE") = UPPER(p_lang)
            AND  UPPER(CEFR_LEVEL) = UPPER(p_cefr);
        CURSOR exist_cursor_null(p_lang IN VARCHAR, p_cefr IN VARCHAR) IS SELECT ID
            FROM EWPCV_LANGUAGE_SKILL
            WHERE UPPER("LANGUAGE") = UPPER(p_lang)
            AND  CEFR_LEVEL is null;
    BEGIN
        
        IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN
            OPEN exist_cursor_null(P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL );
            FETCH exist_cursor_null INTO v_id;
            CLOSE exist_cursor_null;
        ELSE
            OPEN exist_cursor(P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL );
            FETCH exist_cursor INTO v_id;
            CLOSE exist_cursor;
        END IF;



       IF v_id IS NULL AND (P_LANGUAGE_SKILL.LANG IS NOT NULL OR  P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL) THEN
            v_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (v_id, P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL);
        END IF;
        RETURN v_id;
    END;
	
	procedure print_clob( p_clob in clob ) is
          v_offset number default 1;
          v_chunk_size number := 10000;
      begin
          loop
              exit when v_offset > dbms_lob.getlength(p_clob);
              dbms_output.put_line( dbms_lob.substr( p_clob, v_chunk_size, v_offset ) );
              v_offset := v_offset +  v_chunk_size;
          end loop;
      end print_clob;


	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) AS
	BEGIN
		INSERT INTO EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (EWP.GENERATE_UUID(),P_ELEMENT_ID, LOWER(P_NOTIFY_HEI), SYSDATE, P_TYPE, 1, 0, LOWER(P_NOTIFIER_HEI) );
	END;

END PKG_COMMON;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '31_UPGRADE_FEATURE_EHU_ERROR_nombres_intitucion_repetidos');
COMMIT;
    
    
    
    
    
    
    
    
    
