/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

    CREATE TABLE EWP.EWPCV_FILES (
                ID varchar2(255 char) not null,
				FILE_ID varchar2(255 char) not null,
                FILE_CONTENT BLOB,
                SCHAC varchar2(255 char),
				MIME_TYPE varchar2(255 char),
                primary key (ID)
            );

    INSERT INTO  EWP.EWPCV_AUDIT_CONFIG (NAME,VALUE) VALUES ('ewp.fileApi.audit','true');

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/



/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FILES               TO APLEWP;
    GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FILES               TO EWP_APROVISIONAMIENTO;


/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

	CREATE OR REPLACE SYNONYM APLEWP.EWPCV_FILES                    FOR EWP.EWPCV_FILES;
/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v03.00.00', SYSDATE, '48_FILE_API_v03.00.00');
COMMIT;