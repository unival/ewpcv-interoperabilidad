
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/

/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/
CREATE OR REPLACE PACKAGE EWP.PKG_INSTITUTION AS 

	-- TIPOS DE DATOS
	/* Objeto que modela una INSTITUTION. Campos obligatorios marcados con *.
			INSTITUTION_ID *				Código SCHAC de la institución
			INSTITUTION_NAME_LIST *			Lista con los nombres de la institución en varios idiomas
			ABBREVIATION					Abreviación de la universidad
			UNIVERSITY_CONTACT_DETAILS		Datos de contacto de la universidad (FK de Institution a EWPCV_CONTACT_DETAILS a travÃ©s del campo PRIMARY_CONTACT_DETAIL_ID)
			CONTACT_DETAILS_LIST			Lista de los datos de contacto
			FACTSHEET_URL_LIST				Lista de urls de los factsheet de la institución
			LOGO_URL						Url del logo de la institución
	*/
	TYPE INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		INSTITUTION_NAME_LIST			EWP.LANGUAGE_ITEM_LIST,
		ABBREVIATION					VARCHAR2(255 CHAR),
		UNIVERSITY_CONTACT_DETAILS		EWP.CONTACT,
		CONTACT_DETAILS_LIST			EWP.CONTACT_PERSON_LIST,
		FACTSHEET_URL_LIST				EWP.LANGUAGE_ITEM_LIST,
		LOGO_URL						VARCHAR2(255 CHAR)
	  );

	-- FUNCIONES 

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros: 
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_INSTITUTION;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_INSTITUTION AS 

	/*
		Valida los campos obligatorios de una institución 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID IS NULL THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_ID');
		END IF;
		IF  P_INSTITUTION.INSTITUTION_NAME_LIST IS NULL OR P_INSTITUTION.INSTITUTION_NAME_LIST.COUNT = 0 THEN 
            IF P_ERROR_MESSAGE IS NULL THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
            END IF;
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_NAME_LIST');
		END IF;

		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_DATOS_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
		v_error_message VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST LOOP 
				IF EWP.VALIDA_URL(P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, 0, v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-FACTSHEET_URL_LIST('|| i|| '): ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;
		END IF;

		IF P_INSTITUTION.LOGO_URL IS NOT NULL AND EWP.VALIDA_URL(P_INSTITUTION.LOGO_URL, 0,v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-LOGO_URL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;

		IF P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_DETAILS(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-UNIVERSITY_CONTACT_DETAILS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_INSTITUTION.CONTACT_DETAILS_LIST, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-CONTACT_DETAILS_LIST: '); 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

    FUNCTION VALIDA_FS_OUNIT(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE  IN OUT VARCHAR2) RETURN VARCHAR2 IS
        v_tiene_ounit NUMBER := 0;
        v_tiene_fs NUMBER := 0;
        v_cod_retorno NUMBER := 0;
    BEGIN
        SELECT COUNT(1) INTO v_tiene_ounit FROM EWPCV_INST_ORG_UNIT iou 
            INNER JOIN EWPCV_INSTITUTION i ON iou.INSTITUTION_ID = i.ID 
            WHERE UPPER(i.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);

        SELECT COUNT(1) INTO v_tiene_fs FROM EWPCV_INSTITUTION ins
            INNER JOIN EWPCV_FACT_SHEET fs ON ins.FACT_SHEET = FS.ID
            WHERE UPPER(ins.INSTITUTION_ID) = P_INSTITUTION_ID
            AND fs.CONTACT_DETAILS_ID IS NOT NULL;

        IF v_tiene_ounit > 0 THEN 
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,  
                    'La institución tiene OUNITS asociadas y no se puede borrar.');
        ELSIF v_tiene_fs > 0 THEN
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                    'La institución tiene FACT_SHEETS asociadas y no se puede borrar.');
        END IF;

        RETURN v_cod_retorno;
    END;

    /*
		Actualiza la lista de fact_sheet_url
	*/
	PROCEDURE ACTUALIZA_FACT_SHEET_URL(P_INSTITUTION_ID IN VARCHAR2, FACTSHEET_URL_LIST IN EWP.LANGUAGE_ITEM_LIST) AS
		v_fact_sheet_id VARCHAR2(255);
		v_count NUMBER;
        v_lang_item_id VARCHAR2(255);
		CURSOR c_fact_sheet(p_inst_id IN VARCHAR2) IS SELECT fs.ID
			FROM EWPCV_FACT_SHEET fs
			INNER JOIN EWPCV_INSTITUTION i ON i.FACT_SHEET = fs.ID 
			WHERE UPPER(i.INSTITUTION_ID) = UPPER(p_inst_id);
		CURSOR c_fact_sheet_urls(p_f_s_id IN VARCHAR2) IS SELECT URL_ID
			FROM EWPCV_FACT_SHEET_URL
			WHERE FACT_SHEET_ID = p_f_s_id;
	BEGIN
		OPEN c_fact_sheet(P_INSTITUTION_ID);
			FETCH c_fact_sheet INTO v_fact_sheet_id;
			CLOSE c_fact_sheet;


		FOR fact_sheet_url IN c_fact_sheet_urls(v_fact_sheet_id)
		LOOP 
			DELETE FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = fact_sheet_url.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = fact_sheet_url.URL_ID;
		END LOOP;


		IF FACTSHEET_URL_LIST IS NOT NULL AND FACTSHEET_URL_LIST.COUNT > 0 THEN
			FOR i IN FACTSHEET_URL_LIST.FIRST .. FACTSHEET_URL_LIST.LAST 
			LOOP
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(FACTSHEET_URL_LIST(i));
				END IF; 
				v_count := 1;
				SELECT COUNT(1) INTO v_count FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = v_lang_item_id AND FACT_SHEET_ID = v_fact_sheet_id;
				IF v_lang_item_id IS NOT NULL AND v_count = 0 THEN 
					INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (v_lang_item_id, v_fact_sheet_id);
				END IF;
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Inserta las urls de factsheet de la institución 
	*/
	FUNCTION INSERTA_FACTSHEET_URLS(P_INSTITUTION IN INSTITUTION) RETURN VARCHAR2 AS 
	    v_lang_item_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		CURSOR exist_lang_it_cursor(p_lang IN VARCHAR2, p_text IN VARCHAR2, p_fs_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM lait
            INNER JOIN EWPCV_FACT_SHEET_URL fsu ON fsu.URL_ID = lait.ID
			WHERE UPPER(lait.LANG) = UPPER(p_lang)
			AND UPPER(lait.TEXT) = UPPER(p_text)
            AND fsu.FACT_SHEET_ID = p_fs_id;
        CURSOR exist_fact_sheet_cursor(p_ins_id IN VARCHAR2) IS SELECT FACT_SHEET
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
        OPEN exist_fact_sheet_cursor(P_INSTITUTION.INSTITUTION_ID);
        FETCH exist_fact_sheet_cursor INTO v_factsheet_id;
        CLOSE exist_fact_sheet_cursor;
        IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN
            IF v_factsheet_id IS NULL THEN
                v_factsheet_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_FACT_SHEET (ID) VALUES (v_factsheet_id);
            END IF;

            FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST 
            LOOP
                OPEN exist_lang_it_cursor(P_INSTITUTION.FACTSHEET_URL_LIST(i).LANG, P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, v_factsheet_id) ;
                FETCH exist_lang_it_cursor INTO v_lang_item_id;
                CLOSE exist_lang_it_cursor;
                IF v_lang_item_id IS NULL THEN 
                    v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION.FACTSHEET_URL_LIST(i));
                    INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (v_lang_item_id, v_factsheet_id);
                    v_lang_item_id := null;
                END IF; 		
            END LOOP;
		END IF;
		RETURN v_factsheet_id;
	END;

    /*
        Borra los nombres de la institution
    */
    PROCEDURE BORRA_INSTITUTION_NAMES (P_INST_ID_INTEROP IN VARCHAR2) AS
        CURSOR c_names(p_inst_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_INSTITUTION_NAME
			WHERE INSTITUTION_ID = p_inst_id;
    BEGIN
        FOR names_rec IN c_names(P_INST_ID_INTEROP)
		LOOP 
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE NAME_ID = names_rec.NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = names_rec.NAME_ID;
		END LOOP;
    END;   

     /*
		Borra una institución
	*/
	PROCEDURE BORRA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2) AS
        v_fact_sheet_id VARCHAR2(255);
		v_prim_cont_id VARCHAR2(255);
        v_prim_cont_det_id VARCHAR2(255);
        v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS 
			SELECT id, fact_sheet
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);           
        CURSOR c_contacts_inst(p_institution_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_institution_id);
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop, v_fact_sheet_id;
		CLOSE c_inst_p_id;			       
        BORRA_INSTITUTION_NAMES(v_inst_id_interop);    

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE INSTITUTION_ID = P_INSTITUTION_ID;  

        --Borramos Factsheet urls
        PKG_COMMON.BORRA_FACT_SHEET_URL(v_fact_sheet_id);
        DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_fact_sheet_id;

        --Borramos contacts
        FOR contact IN c_contacts_inst(P_INSTITUTION_ID)       
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;

        DELETE FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);
	END;

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_prim_contact_id VARCHAR2(255);
		v_contact_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS 
			SELECT id
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);   
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION.INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop;
		CLOSE c_inst_p_id;
		
		IF v_inst_id_interop IS NOT NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución ya existe en el sistema');
			RETURN -1;
		END IF;
	
		v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

		IF v_cod_retorno = 0 THEN

			v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

			IF v_cod_retorno = 0 THEN
	            -- Insertamos el contacto principal
				v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);

				-- Insertamos la lista de contactos
        IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN     
            FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST 
            LOOP
                v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
            END LOOP;
        END IF;

        -- Insertamos la lista de fact sheet urls
        v_factsheet_id := INSERTA_FACTSHEET_URLS(P_INSTITUTION);

				-- Insertamos la INSTITUTION en EWPCV_INSTITUTION
				P_INSTITUTION_ID := PKG_COMMON.INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ABBREVIATION, P_INSTITUTION.LOGO_URL, v_factsheet_id, P_INSTITUTION.INSTITUTION_NAME_LIST, v_prim_contact_id);
			END IF;

		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_INSTITUTION; 	

    PROCEDURE ACTUALIZA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION) IS
      v_prim_contact_id VARCHAR2(255);
      v_factsheet_id VARCHAR2(255);
      v_contact_id VARCHAR2(255);
      v_inst_id_interop VARCHAR2(255);
     CURSOR c_fsheet_id_inst_id (p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);     
    CURSOR c_contacts(p_inst_hei IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE INSTITUTION_ID = p_inst_hei;
    BEGIN      
        OPEN c_fsheet_id_inst_id(P_INSTITUTION_ID);
		FETCH c_fsheet_id_inst_id INTO v_inst_id_interop, v_factsheet_id;
		CLOSE c_fsheet_id_inst_id;

        BORRA_INSTITUTION_NAMES(v_inst_id_interop);
        PKG_COMMON.INSERTA_INSTITUTION_NAMES(v_inst_id_interop, P_INSTITUTION.INSTITUTION_NAME_LIST);

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE ID = v_inst_id_interop;

        FOR contact IN c_contacts(P_INSTITUTION.INSTITUTION_ID) 
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;
        -- Insertamos de nuevo el contacto principal
        v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);
        -- Insertamos de nuevo la lista de contactos
        IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN     
            FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST 
            LOOP
                v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
            END LOOP;  
		END IF; 

		PKG_COMMON.BORRA_FACT_SHEET_URL(v_factsheet_id);
        DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_factsheet_id AND CONTACT_DETAILS_ID IS NULL;
        v_factsheet_id := INSERTA_FACTSHEET_URLS(P_INSTITUTION);

        UPDATE EWPCV_INSTITUTION SET 
            PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id, FACT_SHEET = v_factsheet_id, ABBREVIATION = P_INSTITUTION.ABBREVIATION, LOGO_URL = P_INSTITUTION.LOGO_URL 
            WHERE ID = v_inst_id_interop;
    END;

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros: 
        El identificador (Código SCHAC) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_prim_contact_id VARCHAR2(255);
        v_count NUMBER := 0;
	BEGIN
		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
	        v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	        IF v_cod_retorno = 0 THEN
	          v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	          IF v_cod_retorno = 0 THEN
	              ACTUALIZA_INSTITUTION(P_INSTITUTION_ID, P_INSTITUTION);
	              COMMIT;
	          END IF;
	        END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_INSTITUTION;

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros: 
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_count NUMBER := 0;
        v_tiene_fs_o_ounit NUMBER := 0;
	BEGIN

		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
            v_tiene_fs_o_ounit := VALIDA_FS_OUNIT(P_INSTITUTION_ID, P_ERROR_MESSAGE);
            IF v_tiene_fs_o_ounit = 0 THEN
                BORRA_INSTITUTION(P_INSTITUTION_ID);
            END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
        COMMIT;
        return v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_INSTITUTION; 	 
END PKG_INSTITUTION;
/

CREATE OR REPLACE PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoño
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoño
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  

	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );

	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;



	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;

	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );

	-- FUNCIONES 

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_FACTSHEET;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-DECISION_WEEK_LIMIT');
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-TOR_WEEK_LIMIT');
		END IF;

		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-NOMINATIONS_AUTUM_TERM');
		END IF;

		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-NOMINATIONS_SPRING_TERM');
		END IF;

		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-APPLICATION_AUTUM_TERM');
		END IF;

		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-APPLICATION_SPRING_TERM');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-EMAIL');
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-PHONE');
		END IF;

		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-URL');
		END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(2000);
	BEGIN
		IF EWP.VALIDA_EMAIL(P_INFORMATION_ITEM.EMAIL, v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '-EMAIL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;
	
		IF P_INFORMATION_ITEM.PHONE.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_INFORMATION_ITEM.PHONE.E164, v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-PHONE.E164: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;
	
		IF P_INFORMATION_ITEM.URL IS NOT NULL AND P_INFORMATION_ITEM.URL.COUNT > 0 THEN 
			FOR i IN P_INFORMATION_ITEM.URL.FIRST .. P_INFORMATION_ITEM.URL.LAST LOOP 
				IF EWP.VALIDA_URL(P_INFORMATION_ITEM.URL(i).TEXT, 0,v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-URL('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;			
		END IF;
		RETURN v_cod_retorno;
	END ;

	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP

			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it = 0 AND VALIDA_DATOS_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: 
						'|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				ADITIONAL_INFO['|| i || ']:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it = 0 AND VALIDA_DATOS_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: 
						'|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				ACCESSIBILITY_REQUIREMENTS['|| i || ']:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				REQUIREMENTS_INFO'|| i || ':');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
		v_max_mensaje VARCHAR2(8168 CHAR);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS 
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);       
	BEGIN
        OPEN exist_cursor_inst(P_FACTSHEET_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;

		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-INSTITUTION_ID');
        ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-INSTITUTION_ID: La institución no existe en el sistema.');
		END IF;

		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			FACTSHEET:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_factsheet);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			APPLICATION_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_app_info);
		END IF;
	
		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-APPLICATION_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_app_info);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			HOUSING_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_hou_info);
		END IF;
	
		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-HOUSING_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_hou_info);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			VISA_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_vis_info);
		END IF;

		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-VISA_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_vis_info);
		END IF;	
	
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			INSURANCE_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_ins_info);
		END IF;

		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-INSURANCE_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_ins_info);
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			ADDITIONAL_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_add_info);
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			ACCESSIBILITY_REQUIREMENTS:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_acc_req);
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			REQUIREMENTS_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_add_req);
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			P_ERROR_MESSAGE := '';
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
		END IF;

		RETURN v_cod_retorno;
	END;


	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;

	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
		v_cd_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
		OPEN c(P_FACTSHEET_ID);
		FETCH c into v_cd_id;
		CLOSE c;

        UPDATE EWPCV_FACT_SHEET
        SET DECISION_WEEKS_LIMIT = null,
                TOR_WEEKS_LIMIT = null,
                NOMINATIONS_AUTUM_TERM = null,
                NOMINATIONS_SPRING_TERM = null,
                APPLICATION_AUTUM_TERM = null,
                APPLICATION_SPRING_TERM = null,
                CONTACT_DETAILS_ID = null
        WHERE ID = P_FACTSHEET_ID;

		PKG_COMMON.BORRA_CONTACT_DETAILS(v_cd_id);
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;

		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);

		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;

		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,P_INF_TYPE,v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_FS_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

        IF P_FS_ID IS NULL THEN 
            v_factsheet_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
                VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
                    P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
        ELSE
            v_factsheet_id := P_FS_ID;
            UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT, 
                TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT,
                NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
                NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
                APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
                APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
                CONTACT_DETAILS_ID = v_contact_id
                WHERE ID = v_factsheet_id;
        END IF;

		RETURN v_factsheet_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);

		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, LOWER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;

	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_FS_ID IN VARCHAR2) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO, P_FS_ID);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME, null);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);

		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;

	END;


	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;

	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN

		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);

		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;


	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);

	BEGIN

		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;

		IF P_FACTSHEET.INSTITUTION_NAME IS NOT NULL AND P_FACTSHEET.INSTITUTION_NAME.COUNT > 0 THEN
			BORRA_NOMBRES(P_INSTITUTION_ID);
			PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		END IF;

		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);

		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);

		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
        v_fs_id VARCHAR2(255);
        v_fs_cd_id VARCHAR2(255);
        CURSOR c_fid(p_id IN VARCHAR2) IS 
		SELECT fs.id, fs.CONTACT_DETAILS_ID
		FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(p_id);
	BEGIN
        OPEN c_fid(P_FACTSHEET.INSTITUTION_ID);
        FETCH c_fid INTO v_fs_id, v_fs_cd_id;
        CLOSE c_fid;

		IF v_fs_cd_id IS NOT NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET, v_fs_id);
		END IF;
		COMMIT;

		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no existe');
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no tiene fact sheet insertelo antes de actualizarlo');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no existe');
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no tiene fact sheet insertelo antes de actualizarlo');
			RETURN -1;
		END IF;

		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;
/


/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '31_UPGRADE_FEATURE_MEJORA_fix_update_institution');
COMMIT;
