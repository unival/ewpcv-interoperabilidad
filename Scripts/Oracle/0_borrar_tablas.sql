DROP TABLE EWP.EWPCV_ACADEMIC_TERM_NAME                      ;
DROP TABLE EWP.EWPCV_BLENDED_LA_COMPONENT                    ;
DROP TABLE EWP.EWPCV_CONTACT_DESCRIPTION                     ;
DROP TABLE EWP.EWPCV_CONTACT_DETAILS_EMAIL                   ;
DROP TABLE EWP.EWPCV_CONTACT_NAME                            ;
DROP TABLE EWP.EWPCV_CONTACT_URL                             ;
DROP TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL	             	 ;
DROP TABLE EWP.EWPCV_COOPCOND_EQFLVL	             		 ;
DROP TABLE EWP.EWPCV_COOPCOND_SUBAR                          ;
DROP TABLE EWP.EWPCV_COOPERATION_CONDITION                   ;
DROP TABLE EWP.EWPCV_IIA_APPROVALS							 ;
DROP TABLE EWP.EWPCV_DISTR_DESCRIPTION                       ;
DROP TABLE EWP.EWPCV_DOCTORAL_LA_COMPONENT                   ;
DROP TABLE EWP.EWPCV_DURATION                                ;
DROP TABLE EWP.EWPCV_FACT_SHEET_URL                          ;
DROP TABLE EWP.EWPCV_FLEXIBLE_ADDRESS_LINE            		 ;
DROP TABLE EWP.EWPCV_FLEXAD_DELIV_POINT_COD				     ;
DROP TABLE EWP.EWPCV_FLEXAD_RECIPIENT_NAME			         ;
DROP TABLE EWP.EWPCV_GRADING_SCHEME_DESC                     ;
DROP TABLE EWP.EWPCV_GRADING_SCHEME_LABEL                    ;
DROP TABLE EWP.EWPCV_IIA                                     ;
DROP TABLE EWP.EWPCV_IIA_PARTNER_CONTACTS                    ;
DROP TABLE EWP.EWPCV_INS_REQUIREMENTS                        ;
DROP TABLE EWP.EWPCV_INST_INF_ITEM                           ;
DROP TABLE EWP.EWPCV_INST_ORG_UNIT                           ;
DROP TABLE EWP.EWPCV_INSTITUTION_NAME                        ;
DROP TABLE EWP.EWPCV_LOI_CREDITS                             ;
DROP TABLE EWP.EWPCV_LOS_DESCRIPTION                         ;
DROP TABLE EWP.EWPCV_LOS_LOI                                 ;
DROP TABLE EWP.EWPCV_LOS_LOS                                 ;
DROP TABLE EWP.EWPCV_LOS_NAME                                ;
DROP TABLE EWP.EWPCV_LOS_URLS                                ;
DROP TABLE EWP.EWPCV_MOBILITY_LA                             ;
DROP TABLE EWP.EWPCV_MOBILITY_LANG_SKILL					 ;
DROP TABLE EWP.EWPCV_MOBILITY_NUMBER                         ;
DROP TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST                 ;
DROP TABLE EWP.EWPCV_NOTIFICATION                            ;
DROP TABLE EWP.EWPCV_ORGANIZATION_UNIT_NAME                  ;
DROP TABLE EWP.EWPCV_OU_INF_ITEM                             ;
DROP TABLE EWP.EWPCV_OU_REQUIREMENT_INFO                     ;
DROP TABLE EWP.EWPCV_RECOGNIZED_LA_COMPONENT                 ;
DROP TABLE EWP.EWPCV_REQUIREMENTS_INFO                       ;
DROP TABLE EWP.EWPCV_RESULT_DIST_CATEGORY		             ;
DROP TABLE EWP.EWPCV_STUDIED_LA_COMPONENT                    ;
DROP TABLE EWP.EWPCV_VIRTUAL_LA_COMPONENT                    ;
DROP TABLE EWP.EWPCV_COMPONENT_CREDITS                       ;
DROP TABLE EWP.EWPCV_CREDIT                                  ;
DROP TABLE EWP.EWPCV_IIA_PARTNER                             ;
DROP TABLE EWP.EWPCV_INFORMATION_ITEM                        ;
DROP TABLE EWP.EWPCV_INSTITUTION                             ;
DROP TABLE EWP.EWPCV_LA_COMPONENT                            ;
DROP TABLE EWP.EWPCV_LEARNING_AGREEMENT                      ;
DROP TABLE EWP.EWPCV_LEVEL_DESCRIPTION						 ;
DROP TABLE EWP.EWPCV_LEVELS									 ;
DROP TABLE EWP.EWPCV_LEVEL									 ;
DROP TABLE EWP.EWPCV_LOI_GROUPING							 ;
DROP TABLE EWP.EWPCV_GROUP									 ;
DROP TABLE EWP.EWPCV_GROUP_TYPE								 ;
DROP TABLE EWP.EWPCV_ATTACHMENT_CONTENT						 ;
DROP TABLE EWP.EWPCV_ATTACHMENT_TITLE						 ;
DROP TABLE EWP.EWPCV_ATTACHMENT_DESCRIPTION					 ;
DROP TABLE EWP.EWPCV_ADDITIONAL_INFO						 ;
DROP TABLE EWP.EWPCV_DIPLOMA_SECTION_SECTION				 ;
DROP TABLE EWP.EWPCV_DIPLOMA_SEC_ATTACH						 ;
DROP TABLE EWP.EWPCV_DIPLOMA_SECTION						 ;
DROP TABLE EWP.EWPCV_LOI_ATT								 ;
DROP TABLE EWP.EWPCV_REPORT_ATT								 ;
DROP TABLE EWP.EWPCV_TOR_ATTACHMENT							 ;
DROP TABLE EWP.EWPCV_LOI                                     ;
DROP TABLE EWP.EWPCV_DIPLOMA								 ;
DROP TABLE EWP.EWPCV_GRADE_FREQUENCY						 ;
DROP TABLE EWP.EWPCV_ISCED_TABLE							 ;
DROP TABLE EWP.EWPCV_ATTACHMENT								 ;
DROP TABLE EWP.EWPCV_REPORT									 ;
DROP TABLE EWP.EWPCV_TOR									 ;
DROP TABLE EWP.EWPCV_LOS                                     ;
DROP TABLE EWP.EWPCV_MOBILITY                                ;
DROP TABLE EWP.EWPCV_MOBILITY_PARTICIPANT                    ;
DROP TABLE EWP.EWPCV_MOBILITY_TYPE                           ;
DROP TABLE EWP.EWPCV_ORGANIZATION_UNIT                       ;
DROP TABLE EWP.EWPCV_RESULT_DISTRIBUTION                     ;
DROP TABLE EWP.EWPCV_SIGNATURE                               ;
DROP TABLE EWP.EWPCV_SUBJECT_AREA                            ;
DROP TABLE EWP.EWPCV_CONTACT                                 ;
DROP TABLE EWP.EWPCV_FACT_SHEET                              ;
DROP TABLE EWP.EWPCV_GRADING_SCHEME                          ;
DROP TABLE EWP.EWPCV_LANGUAGE_SKILL                          ;
DROP TABLE EWP.EWPCV_ACADEMIC_TERM                           ;
DROP TABLE EWP.EWPCV_ACADEMIC_YEAR                           ;
DROP TABLE EWP.EWPCV_PHOTO_URL                               ;
DROP TABLE EWP.EWPCV_CONTACT_DETAILS                         ;
DROP TABLE EWP.EWPCV_FLEXIBLE_ADDRESS                        ;
DROP TABLE EWP.EWPCV_PHONE_NUMBER                            ;
DROP TABLE EWP.EWPCV_PERSON                                  ;
DROP TABLE EWP.EWPCV_LANGUAGE_ITEM                           ;
DROP TABLE EWP.EWPCV_DICTIONARY                              ;
DROP TABLE EWP.EWPCV_EVENT									 ;
DROP TABLE EWP.EWPCV_INSTITUTION_IDENTIFIERS				 ;
DROP TABLE EWP.EWPCV_SCRIPT_VERSIONS                         ;
DROP TABLE EWP.EWPCV_QRTZ_FIRED_TRIGGERS					 ;
DROP TABLE EWP.EWPCV_QRTZ_PAUSED_TRIGGER_GRPS				 ;
DROP TABLE EWP.EWPCV_QRTZ_SCHEDULER_STATE					 ;
DROP TABLE EWP.EWPCV_QRTZ_LOCKS								 ;
DROP TABLE EWP.EWPCV_QRTZ_SIMPLE_TRIGGERS					 ;
DROP TABLE EWP.EWPCV_QRTZ_SIMPROP_TRIGGERS					 ;
DROP TABLE EWP.EWPCV_QRTZ_CRON_TRIGGERS						 ;
DROP TABLE EWP.EWPCV_QRTZ_BLOB_TRIGGERS						 ;
DROP TABLE EWP.EWPCV_QRTZ_TRIGGERS							 ;
DROP TABLE EWP.EWPCV_QRTZ_JOB_DETAILS						 ;
DROP TABLE EWP.EWPCV_QRTZ_CALENDARS							 ;
DROP TABLE EWP.EWPCV_AUDIT_CONFIG							 ;
DROP TABLE EWP.EWPCV_STATS_OLA                           	 ;
DROP TABLE EWP.EWPCV_STATS_ILA                           	 ;
DROP TABLE EWP.EWPCV_STATS_IIA                           	 ;
DROP TABLE EWP.EWPCV_FILES;

DROP SEQUENCE EWP.SEC_EWPCV_NOTIFICACTION					 ;
DROP SEQUENCE EWP.SEC_EWPCV_SCRIPT_VERSION					 ;
DROP SEQUENCE EWP.SEC_EWPCV_ORG_UNIT						 ;
DROP SEQUENCE EWP.SEC_EWPCV_COOPERATION_CONDITION			 ;
DROP SEQUENCE EWP.SEC_EWPCV_IIA_APPROVALS;

DROP PACKAGE EWP.PKG_COMMON									 ;
DROP PACKAGE EWP.PKG_IIAS									 ;
DROP PACKAGE EWP.PKG_MOBILITY_LA							 ;
DROP PACKAGE EWP.PKG_FACTSHEET								 ;
DROP PACKAGE EWP.PKG_INSTITUTION							 ;
DROP PACKAGE EWP.PKG_OUNITS									 ;
DROP PACKAGE EWP.PKG_LOS									 ;
DROP PACKAGE EWP.PKG_TORS									 ;
DROP PACKAGE EWP.PKG_DICTIONARY									 ;

DROP TYPE EWP.INSTITUTION									 ;
DROP TYPE EWP.CONTACT_PERSON_LIST							 ;
DROP TYPE EWP.CONTACT_PERSON								 ;
DROP TYPE EWP.CONTACT										 ;
DROP TYPE EWP.EMAIL_LIST									 ;
DROP TYPE EWP.FLEXIBLE_ADDRESS								 ;
DROP TYPE EWP.DELIVERY_POINT_CODE_LIST						 ;
DROP TYPE EWP.ADDRESS_LINE_LIST								 ;
DROP TYPE EWP.RECIPIENT_NAME_LIST							 ;
DROP TYPE EWP.PHONE_NUMBER_LIST								 ;
DROP TYPE EWP.PHONE_NUMBER									 ;
DROP TYPE EWP.ACADEMIC_TERM									 ;
DROP TYPE EWP.LANGUAGE_SKILL_LIST							 ;
DROP TYPE EWP.LANGUAGE_SKILL								 ;
DROP TYPE EWP.SUBJECT_AREA_LIST								 ;
DROP TYPE EWP.SUBJECT_AREA									 ;
DROP TYPE EWP.LANGUAGE_ITEM_LIST							 ;
DROP TYPE EWP.LANGUAGE_ITEM									 ;
DROP TYPE EWP.MULTILANGUAGE_BLOB_LIST						 ;
DROP TYPE EWP.MULTILANGUAGE_BLOB							 ;

DROP VIEW EWP.IIAS_VIEW										 ;
DROP VIEW EWP.IIA_COOP_CONDITIONS_VIEW						 ;
DROP VIEW EWP.IIA_COOP_COND_CONTACTS_VIEW					 ;
DROP VIEW EWP.MOBILITY_VIEW									 ;
DROP VIEW EWP.LEARNING_AGREEMENT_VIEW						 ;
DROP VIEW EWP.LA_COMPONENTS_VIEW							 ;
DROP VIEW EWP.DICTIONARIES_VIEW								 ;
DROP VIEW EWP.FACTSHEET_VIEW								 ;
DROP VIEW EWP.FACTSHEET_ADD_INFO_VIEW						 ;
DROP VIEW EWP.FACTSHEET_REQ_INFO_VIEW						 ;
DROP VIEW EWP.EVENT_VIEW									 ;
DROP VIEW EWP.NOTIFICATIONS_VIEW							 ;
DROP VIEW EWP.UPDATE_REQUESTS_VIEW							 ;
DROP VIEW EWP.INSTITUTION_IDENTIFIERS_VIEW					 ;
DROP VIEW EWP.INSTITUTION_VIEW								 ;
DROP VIEW EWP.INSTITUTION_CONTACTS_VIEW						 ;
DROP VIEW EWP.OUNIT_VIEW									 ;
DROP VIEW EWP.OUNIT_CONTACTS_VIEW							 ;
DROP VIEW EWP.LOS_VIEW										 ;
DROP VIEW EWP.DIPLOMA_VIEW									 ;
DROP VIEW EWP.GRADING_SCHEME_VIEW							 ;
DROP VIEW EWP.GROUPING_VIEW									 ;
DROP VIEW EWP.IIA_CONTACTS_VIEW								 ;
DROP VIEW EWP.IIAS_PDF_VIEW									 ;
DROP VIEW EWP.TOR_ATTACHMENT_VIEW							 ;
DROP VIEW EWP.IIAS_STATS;
DROP VIEW EWP.INCOMING_LA_STATS;
DROP VIEW EWP.LA_STATS;
DROP VIEW EWP.LA_STATS_MODIFIED;
DROP VIEW EWP.LA_STATS_UNMODIFIED;
DROP VIEW EWP.LA_STATS_V_STATUS;
DROP VIEW EWP.OUTGOING_LA_STATS;

DROP FUNCTION EWP.EXTRAERANIO								 ;
DROP FUNCTION EWP.GENERATE_UUID								 ;
DROP FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION				 ;
DROP FUNCTION EWP.EXTRAE_NOMBRES_OUNIT						 ;
DROP FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO					 ;
DROP FUNCTION EWP.EXTRAE_DESCRIPCIONES_CONTACTO				 ;
DROP FUNCTION EWP.EXTRAE_URLS_CONTACTO						 ;
DROP FUNCTION EWP.EXTRAE_EMAILS								 ;
DROP FUNCTION EWP.EXTRAE_TELEFONO							 ;
DROP FUNCTION EWP.EXTRAE_ADDRESS_LINES						 ;
DROP FUNCTION EWP.EXTRAE_ADDRESS							 ;
DROP FUNCTION EWP.EXTRAE_NIVELES_IDIOMAS					 ;
DROP FUNCTION EWP.EXTRAE_FIRMAS								 ;
DROP FUNCTION EWP.EXTRAE_CREDITS							 ;
DROP FUNCTION EWP.EXTRAE_ACADEMIC_TERM						 ;
DROP FUNCTION EWP.EXTRAE_NOMBRES_ACADEMIC_TERM				 ;
DROP FUNCTION EWP.EXTRAE_NOMBRES_LOS						 ;
DROP FUNCTION EWP.EXTRAE_DESCRIPTIONS_LOS					 ;
DROP FUNCTION EWP.EXTRAE_URLS_LOS							 ;
DROP FUNCTION EWP.EXTRAE_S_AREA_L_SKILL						 ;
DROP FUNCTION EWP.EXTRAE_EQF_LEVEL							 ;
DROP FUNCTION EWP.EXTRAE_FAX								 ;
DROP FUNCTION EWP.EXTRAE_URLS_FACTSHEET						 ;
DROP FUNCTION EWP.EXTRAE_PHOTO_URL_LIST						 ;
DROP FUNCTION EWP.EXTRAE_S_AREA								 ;
DROP FUNCTION EWP.RESET_M_UPDATE_REQUEST					 ;
DROP FUNCTION EWP.RESET_NOTIFICATION						 ;
DROP FUNCTION EWP.VALIDA_ACADEMIC_YEAR						 ;
DROP FUNCTION EWP.VALIDA_CADENA								 ;
DROP FUNCTION EWP.VALIDA_CEFR_LEVEL							 ;
DROP FUNCTION EWP.VALIDA_COUNTRY_CODE						 ;
DROP FUNCTION EWP.VALIDA_CREDIT_LEVEL						 ;
DROP FUNCTION EWP.VALIDA_EMAIL								 ;
DROP FUNCTION EWP.VALIDA_EQFLEVEL							 ;
DROP FUNCTION EWP.VALIDA_GENDER								 ;
DROP FUNCTION EWP.VALIDA_PHONE_NUMBER						 ;
DROP FUNCTION EWP.VALIDA_URL								 ;
DROP FUNCTION EWP.EXTRAE_CODIGO_OUNIT;


DROP FUNCTION EWP.CLOB_TO_BLOB								 ;
DROP FUNCTION EWP.EXTRAE_ACADEMIC_YEAR						 ;
DROP FUNCTION EWP.EXTRAE_ADDIT_INFO							 ;
DROP FUNCTION EWP.EXTRAE_ATTACH_DESC						 ;
DROP FUNCTION EWP.EXTRAE_ATTACH_TITLE						 ;
DROP FUNCTION EWP.EXTRAE_DELIVERY_POINT						 ;
DROP FUNCTION EWP.EXTRAE_GRAD_SCHEME_DESC					 ;
DROP FUNCTION EWP.EXTRAE_GRAD_SCHEME_LABEL					 ;
DROP FUNCTION EWP.EXTRAE_GRADE_FREC							 ;
DROP FUNCTION EWP.EXTRAE_GROUP								 ;
DROP FUNCTION EWP.EXTRAE_GROUP_TYPE							 ;
DROP FUNCTION EWP.EXTRAE_ISCED_GRADE						 ;
DROP FUNCTION EWP.EXTRAE_LANGUAGE_ITEM						 ;
DROP FUNCTION EWP.EXTRAE_LEVEL_DESC							 ;
DROP FUNCTION EWP.EXTRAE_LEVELS								 ;
DROP FUNCTION EWP.EXTRAE_RES_DIST							 ;
DROP FUNCTION EWP.NOTIFICA_ELEMENTO							 ;
DROP FUNCTION EWP.RECURSIVE_NON_OVERFLOW_CONCAT				 ;
DROP PROCEDURE EWP.CONCAT_WITHOUT_OVERFLOW					 ;