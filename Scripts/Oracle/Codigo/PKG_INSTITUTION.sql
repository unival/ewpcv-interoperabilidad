/
CREATE OR REPLACE PACKAGE EWP.PKG_INSTITUTION AS 

	-- TIPOS DE DATOS
	/* Objeto que modela una INSTITUTION. Campos obligatorios marcados con *.
			INSTITUTION_ID *				Código SCHAC de la institución
			INSTITUTION_NAME_LIST *			Lista con los nombres de la institución en varios idiomas
			ABBREVIATION					Abreviación de la universidad
			UNIVERSITY_CONTACT_DETAILS		Datos de contacto de la universidad (FK de Institution a EWPCV_CONTACT_DETAILS a travÃ©s del campo PRIMARY_CONTACT_DETAIL_ID)
			CONTACT_DETAILS_LIST			Lista de los datos de contacto
			FACTSHEET_URL_LIST				Lista de urls de los factsheet de la institución
			LOGO_URL						Url del logo de la institución
	*/
	TYPE INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		INSTITUTION_NAME_LIST			EWP.LANGUAGE_ITEM_LIST,
		ABBREVIATION					VARCHAR2(255 CHAR),
		UNIVERSITY_CONTACT_DETAILS		EWP.CONTACT,
		CONTACT_DETAILS_LIST			EWP.CONTACT_PERSON_LIST,
		FACTSHEET_URL_LIST				EWP.LANGUAGE_ITEM_LIST,
		LOGO_URL						VARCHAR2(255 CHAR)
	  );

	-- FUNCIONES 

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros: 
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_INSTITUTION;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_INSTITUTION AS 

	/*
		Valida los campos obligatorios de una institución 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID IS NULL THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_ID');
		END IF;
		IF  P_INSTITUTION.INSTITUTION_NAME_LIST IS NULL OR P_INSTITUTION.INSTITUTION_NAME_LIST.COUNT = 0 THEN
            IF P_ERROR_MESSAGE IS NULL THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
            END IF;
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_NAME_LIST');
		END IF;

		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_DATOS_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
		v_error_message VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN
			FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST LOOP
				IF EWP.VALIDA_URL(P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, 0, v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-FACTSHEET_URL_LIST('|| i|| '): ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;
		END IF;

		IF P_INSTITUTION.LOGO_URL IS NOT NULL AND EWP.VALIDA_URL(P_INSTITUTION.LOGO_URL, 0,v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-LOGO_URL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;

		IF P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_DETAILS(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-UNIVERSITY_CONTACT_DETAILS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_INSTITUTION.CONTACT_DETAILS_LIST, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-CONTACT_DETAILS_LIST: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

    FUNCTION VALIDA_FS_OUNIT(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE  IN OUT VARCHAR2) RETURN VARCHAR2 IS
        v_tiene_ounit NUMBER := 0;
        v_tiene_fs NUMBER := 0;
        v_cod_retorno NUMBER := 0;
    BEGIN
        SELECT COUNT(1) INTO v_tiene_ounit FROM EWPCV_INST_ORG_UNIT iou
            INNER JOIN EWPCV_INSTITUTION i ON iou.INSTITUTION_ID = i.ID
            WHERE UPPER(i.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);

        SELECT COUNT(1) INTO v_tiene_fs FROM EWPCV_INSTITUTION ins
            INNER JOIN EWPCV_FACT_SHEET fs ON ins.FACT_SHEET = FS.ID
            WHERE UPPER(ins.INSTITUTION_ID) = P_INSTITUTION_ID
            AND fs.CONTACT_DETAILS_ID IS NOT NULL;

        IF v_tiene_ounit > 0 THEN
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,
                    'La institución tiene OUNITS asociadas y no se puede borrar.');
        ELSIF v_tiene_fs > 0 THEN
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,
                    'La institución tiene FACT_SHEETS asociadas y no se puede borrar.');
        END IF;

        RETURN v_cod_retorno;
    END;

	/*
		Inserta las urls de factsheet de la instituciÃ³n
	*/
	FUNCTION INSERTA_INSTITUTION_URLS(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID IN VARCHAR2) RETURN NUMBER AS
	    v_cod_retorno NUMBER := 0;
        v_lang_item_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		CURSOR exist_lang_it_cursor(p_lang IN VARCHAR2, p_text IN VARCHAR2, p_ins_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM lait
            INNER JOIN EWPCV_INST_FACT_SHEET_URL ifsu ON ifsu.URL_ID = lait.ID
			WHERE UPPER(lait.LANG) = UPPER(p_lang)
			AND UPPER(lait.TEXT) = UPPER(p_text)
            AND ifsu.INSTITUTION_ID = p_ins_id;
	BEGIN
        IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN
            FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST
            LOOP
                OPEN exist_lang_it_cursor(P_INSTITUTION.FACTSHEET_URL_LIST(i).LANG, P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, P_INSTITUTION.INSTITUTION_ID) ;
                FETCH exist_lang_it_cursor INTO v_lang_item_id;
                CLOSE exist_lang_it_cursor;
                IF v_lang_item_id IS NULL THEN
                    v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION.FACTSHEET_URL_LIST(i));
                    INSERT INTO EWPCV_INST_FACT_SHEET_URL (URL_ID, INSTITUTION_ID) VALUES (v_lang_item_id, P_INSTITUTION_ID);
                    v_lang_item_id := null;
                END IF;
            END LOOP;
		END IF;

        RETURN v_cod_retorno;
	END;

    /*
        Borra los nombres de la institution
    */
    PROCEDURE BORRA_INSTITUTION_NAMES (P_INST_ID_INTEROP IN VARCHAR2) AS
        CURSOR c_names(p_inst_id IN VARCHAR2) IS
			SELECT NAME_ID
			FROM EWPCV_INSTITUTION_NAME
			WHERE INSTITUTION_ID = p_inst_id;
    BEGIN
        FOR names_rec IN c_names(P_INST_ID_INTEROP)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE NAME_ID = names_rec.NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = names_rec.NAME_ID;
		END LOOP;
    END;

     /*
		Borra una institución
	*/
	PROCEDURE BORRA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2) AS
        v_fact_sheet_id VARCHAR2(255);
		v_prim_cont_id VARCHAR2(255);
        v_prim_cont_det_id VARCHAR2(255);
        v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS
			SELECT id, fact_sheet
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);
        CURSOR c_contacts_inst(p_institution_id IN VARCHAR2) IS
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_institution_id);
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop, v_fact_sheet_id;
		CLOSE c_inst_p_id;
        BORRA_INSTITUTION_NAMES(v_inst_id_interop);

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE INSTITUTION_ID = P_INSTITUTION_ID;

        --Borramos Inst Factsheet urls
        PKG_COMMON.BORRA_INST_FACT_SHEET_URL(v_inst_id_interop);
        DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_fact_sheet_id;

        --Borramos contacts
        FOR contact IN c_contacts_inst(P_INSTITUTION_ID)
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;

        DELETE FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);
	END;

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_prim_contact_id VARCHAR2(255);
		v_contact_id VARCHAR2(255);
		v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS
			SELECT id
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION.INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop;
		CLOSE c_inst_p_id;
		
		IF v_inst_id_interop IS NOT NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución ya existe en el sistema');
			RETURN -1;
		END IF;
	
		v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

		IF v_cod_retorno = 0 THEN

			v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

			IF v_cod_retorno = 0 THEN
	            -- Insertamos el contacto principal
				v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);

				-- Insertamos la lista de contactos
                IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN
                    FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST
                    LOOP
                        v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
                    END LOOP;
                END IF;
				-- Insertamos la INSTITUTION en EWPCV_INSTITUTION
				P_INSTITUTION_ID := PKG_COMMON.INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ABBREVIATION, P_INSTITUTION.LOGO_URL, P_INSTITUTION.INSTITUTION_NAME_LIST, v_prim_contact_id);
                -- Insertamos la lista de URLs
                v_cod_retorno := INSERTA_INSTITUTION_URLS(P_INSTITUTION, P_INSTITUTION_ID);

                INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), P_INSTITUTION.INSTITUTION_ID, 6, 0);
			END IF;

		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_INSTITUTION;

    PROCEDURE ACTUALIZA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION) IS
      v_prim_contact_id VARCHAR2(255);
      v_factsheet_id VARCHAR2(255);
      v_contact_id VARCHAR2(255);
      v_inst_id_interop VARCHAR2(255);
      v_cod_retorno number;
     CURSOR c_fsheet_id_inst_id (p_ins_id IN VARCHAR2) IS
			SELECT ID, FACT_SHEET
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
    CURSOR c_contacts(p_inst_hei IN VARCHAR2) IS
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE INSTITUTION_ID = p_inst_hei;
    BEGIN
        OPEN c_fsheet_id_inst_id(P_INSTITUTION_ID);
		FETCH c_fsheet_id_inst_id INTO v_inst_id_interop, v_factsheet_id;
		CLOSE c_fsheet_id_inst_id;

        BORRA_INSTITUTION_NAMES(v_inst_id_interop);
        PKG_COMMON.INSERTA_INSTITUTION_NAMES(v_inst_id_interop, P_INSTITUTION.INSTITUTION_NAME_LIST);

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE ID = v_inst_id_interop;

        FOR contact IN c_contacts(P_INSTITUTION.INSTITUTION_ID)
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;
        -- Insertamos de nuevo el contacto principal
        v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);
        -- Insertamos de nuevo la lista de contactos
        IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN
            FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST
            LOOP
                v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
            END LOOP;
		END IF;

        v_cod_retorno := INSERTA_INSTITUTION_URLS(P_INSTITUTION, v_inst_id_interop);--c_fsheet_id_inst_id.ID

        UPDATE EWPCV_INSTITUTION SET
            PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id, FACT_SHEET = v_factsheet_id, ABBREVIATION = P_INSTITUTION.ABBREVIATION, LOGO_URL = P_INSTITUTION.LOGO_URL
            WHERE ID = v_inst_id_interop;
    END;

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros:
        El identificador (Código SCHAC) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_prim_contact_id VARCHAR2(255);
        v_count NUMBER := 0;
	BEGIN
		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
	        v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	        IF v_cod_retorno = 0 THEN
	          v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	          IF v_cod_retorno = 0 THEN
	              IF P_INSTITUTION.INSTITUTION_ID = P_INSTITUTION_ID THEN
                      ACTUALIZA_INSTITUTION(P_INSTITUTION_ID, P_INSTITUTION);
                      INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), P_INSTITUTION_ID, 6, 1);
                      COMMIT;
                  ELSE
                      v_cod_retorno := -1;
                      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El identificador de la institución no puede ser modificado');
                  END IF;
	          END IF;
	        END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_INSTITUTION;

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros:
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_count NUMBER := 0;
        v_tiene_fs_o_ounit NUMBER := 0;
	BEGIN

		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
            v_tiene_fs_o_ounit := VALIDA_FS_OUNIT(P_INSTITUTION_ID, P_ERROR_MESSAGE);
            IF v_tiene_fs_o_ounit = 0 THEN
                BORRA_INSTITUTION(P_INSTITUTION_ID);
                INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), P_INSTITUTION_ID, 6, 2);
            END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
        COMMIT;
        return v_cod_retorno;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_INSTITUTION;
END PKG_INSTITUTION;
