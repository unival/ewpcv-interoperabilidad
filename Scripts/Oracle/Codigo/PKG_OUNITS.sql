/
CREATE OR REPLACE PACKAGE EWP.PKG_OUNITS AS 

	-- TIPOS DE DATOS

	/* Objeto que modela una OUNIT. Campos obligatorios marcados con *.
        HEI_ID *                        Codigo SCHAC de la institucion
        OUNIT_CODE *					Codigo del Ounit
        OUNIT_NAME_LIST                 Lista con los nombres de la institucion en varios idiomas
        ABBREVIATION					Abreviacion de la universidad
        OUNIT_CONTACT_DETAILS			Datos de contacto de la universidad (FK de Institution a EWPCV_CONTACT_DETAILS a traves del campo PRIMARY_CONTACT_DETAIL_ID)
        CONTACT_DETAILS_LIST			Lista de los datos de contacto
        FACTSHEET_URL_LIST				Lista de urls de los factsheet de la institucion
        LOGO_URL						Url del logo de la institucion
        IS_TREE_STRUCTURE  *            Flag que define si se desea introducir la estructura de ounits como un arbol de jerarquias
        PARENT_OUNIT_ID                 En caso de disponer las ounits en estructura de arbol, este campo representara los ids del Ounit padre de la 
										OUNIT que estamos introduciendo.
                                        Si el OUNIT que estamos insertando es el root, este campo será null. Si por el contrario estamos insertando un "hijo", 
                                        deberemos informarlo con el id del padre.
                                        Si no se desea implementar la estructura de arbol bastara con no informar este campo e informar el campo IS_TREE_STRUCTURE a 0
	*/
	TYPE OUNIT IS RECORD
	  (
        HEI_ID                          VARCHAR2(255 CHAR),
		OUNIT_CODE						VARCHAR2(255 CHAR),
		OUNIT_NAME_LIST			        EWP.LANGUAGE_ITEM_LIST,
		ABBREVIATION					VARCHAR2(255 CHAR),
		OUNIT_CONTACT_DETAILS		    EWP.CONTACT,
		CONTACT_DETAILS_LIST			EWP.CONTACT_PERSON_LIST,
		FACTSHEET_URL_LIST				EWP.LANGUAGE_ITEM_LIST,
		LOGO_URL						VARCHAR2(255 CHAR),
        IS_TREE_STRUCTURE               NUMBER(1,0),
        PARENT_OUNIT_ID                 VARCHAR2(255 CHAR)
	  );

	-- FUNCIONES 

	/* Inserta una OUNIT en el sistema
		Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_OUNIT(P_OUNIT IN OUNIT, P_OUNIT_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza una OUNIT del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la OUNIT
		Admite un objeto de tipo OUNIT con toda la informacion actualizada de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina una OUNIT del sistema.
        En el caso de estar en estructura de Ã¡rbol y ser una OUNIT padre, se borrarÃ¡n tambiÃ©n todas las hijas.
		Recibe como parametros: 
        El identificador de la OUNIT
		El codigo schac de la institucion a la que pertenece la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

    /* Oculta una ounit de manera que esta deja de estar expuesta. 
        Recibe como parametros: 
        El identificador de la OUNIT
		El codigo schac de la institucion a la que pertenece la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION OCULTAR_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

    /* Oculta una ounit de manera que esta deja de estar expuesta. 
       En el caso de estar en estructura de Ã¡rbol y ser una OUNIT padre, se expondrÃ¡n tambiÃ©n todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		El codigo schac de la institucion a la que pertenece la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION EXPONER_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;


END PKG_OUNITS;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_OUNITS AS 

    /*
        Valida los datos introducidos en el objeto
        La institucion indicada debe exisitir
        El ounit code no se puede repetir en la misma institucion
        No se pueden mezclar estructuras de representacion de unidades organizativas, o todas van sueltas o todas van en estructura de arbol
        La estructura de representacion de unidades organizativas solo debe tener un root si se representa como arbol
        El ounit code indicado como padre debe existir asociado a la institucion
    */
    FUNCTION VALIDA_OUNIT_DATOS(P_OUNIT IN OUNIT, P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
        v_cod_retorno 	       NUMBER := 0;
        v_count       	       NUMBER;
        v_ou_id       	       VARCHAR2(255);
        v_inst_id              VARCHAR2(255);
        v_ounit_code_anterior  VARCHAR2(255) := 'v_ounit_code_anterior';
        v_parent_ounit_id_antes VARCHAR2(255);
        v_check_db_tree_struct NUMBER;
        v_tot_db_tree_struct   NUMBER;
        v_error_message_url VARCHAR2(2000);
        v_error_message VARCHAR2(2000);
        CURSOR c_is_tree_structure(p_id_ins IN VARCHAR) IS SELECT DISTINCT(IS_TREE_STRUCTURE)
            FROM EWPCV_ORGANIZATION_UNIT ou
            INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
            INNER JOIN EWPCV_INSTITUTION i
                ON i.ID = iou.INSTITUTION_ID
            WHERE UPPER(i.INSTITUTION_ID) = UPPER(p_id_ins) 
            ORDER BY ou.IS_TREE_STRUCTURE ASC;
        CURSOR exist_cursor(p_code IN VARCHAR, p_hei_id IN VARCHAR) IS SELECT ou.ID
            FROM EWPCV_ORGANIZATION_UNIT ou
            INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
            INNER JOIN EWPCV_INSTITUTION i
                ON i.ID = iou.INSTITUTION_ID
            WHERE UPPER(ORGANIZATION_UNIT_CODE) = UPPER(p_code)
            AND UPPER(i.INSTITUTION_ID) = UPPER(p_hei_id);
        CURSOR exist_cursor_i(p_hei_id IN VARCHAR) IS SELECT ID
            FROM EWPCV_INSTITUTION
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_hei_id);
        CURSOR c_ounit_code(p_ounit_id IN VARCHAR2) IS SELECT ORGANIZATION_UNIT_CODE, PARENT_OUNIT_ID
            FROM EWPCV_ORGANIZATION_UNIT ou
			INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID 
            INNER JOIN EWPCV_INSTITUTION i
                ON i.ID = iou.INSTITUTION_ID
            WHERE ou.ID = p_ounit_id;
    BEGIN    

         OPEN exist_cursor(P_OUNIT.OUNIT_CODE, P_OUNIT.HEI_ID);
         FETCH exist_cursor INTO v_ou_id;
         CLOSE exist_cursor;

         OPEN exist_cursor_i(P_OUNIT.HEI_ID);
         FETCH exist_cursor_i INTO v_inst_id;
         CLOSE exist_cursor_i;

         IF v_inst_id IS NULL THEN 
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - HEI_ID. No existe ninguna institucion con el Codigo SCHAC ' || P_OUNIT.HEI_ID);
             RETURN -1;
         END IF;    

         IF (P_OUNIT_ID IS NOT NULL) THEN
			IF P_OUNIT_ID = P_OUNIT.PARENT_OUNIT_ID THEN 
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. No se puede establecer como padre de una unidad organizativa a sÃ¿Â­ misma.');
                RETURN -1;
            END IF;

            OPEN c_ounit_code(P_OUNIT_ID);
            FETCH c_ounit_code INTO v_ounit_code_anterior, v_parent_ounit_id_antes;
            CLOSE c_ounit_code;
            IF v_parent_ounit_id_antes IS NULL AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL AND P_OUNIT.IS_TREE_STRUCTURE = 1 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. EstÃ¡ actualizando una unidad organizativa declarada como root asignÃ¡ndole una unidad organizativa padre.');
                RETURN -1;
             END IF;
             IF v_parent_ounit_id_antes IS NOT NULL AND P_OUNIT.PARENT_OUNIT_ID IS NULL AND P_OUNIT.IS_TREE_STRUCTURE = 1 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. EstÃ¡ tratando de convertir en ROOT una unidad organizativa que no lo era. Actulice el ROOT en su lugar.');
                RETURN -1;
             END IF;
         END IF;

         -- Si es actualizaciÃ³n hay que validar que el OUNITCODE que viene no estÃ© duplicado solo si no es el que ya tenÃ¿Â­a
         IF v_ou_id IS NOT NULL AND P_OUNIT.OUNIT_CODE <> v_ounit_code_anterior THEN 
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_CODE. Ya existe una unidad organizativa con el Codigo ' || P_OUNIT.OUNIT_CODE || ' para la institucion ' || P_OUNIT.HEI_ID);
             RETURN -1;  
         END IF;

         OPEN c_is_tree_structure(P_OUNIT.HEI_ID);
         LOOP
            FETCH c_is_tree_structure INTO v_check_db_tree_struct;
            EXIT WHEN c_is_tree_structure%notfound;
         END LOOP;
         v_tot_db_tree_struct := c_is_tree_structure%rowcount;
         CLOSE c_is_tree_structure;

         IF v_tot_db_tree_struct > 1 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                            'IS_TREE_STRUCTURE: Existen unidades organizativas con estructura de Ã¡rbol y sin estructura de Ã¡rbol para esta la institucion idicada, la BBDD necesita revisiÃ³n.');
            RETURN v_cod_retorno; 
         END IF;

         IF v_check_db_tree_struct <> P_OUNIT.IS_TREE_STRUCTURE THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                            'IS_TREE_STRUCTURE: Ya hay unidades organizativas introducidas para esta universidad con diferente estructura de representaciÃ³n a la indicada: ' || P_OUNIT.IS_TREE_STRUCTURE);
            RETURN v_cod_retorno;
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 0 AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - La estructura de representacion de la unidad organizativa no estÃ¡ como formato Ã¡rbol. El atributo PARENT_OUNIT_ID debe ser nulo.');
              RETURN v_cod_retorno;
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 1 AND P_OUNIT.PARENT_OUNIT_ID IS NULL THEN
            SELECT COUNT(1)INTO v_count FROM EWPCV_ORGANIZATION_UNIT ou             
                INNER JOIN EWPCV_INST_ORG_UNIT iou ON ou.INTERNAL_ID = iou.ORGANIZATION_UNITS_ID
                INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID) AND ou.PARENT_OUNIT_ID IS NULL AND ou.ORGANIZATION_UNIT_CODE <> P_OUNIT.OUNIT_CODE;
                IF v_count > 0 THEN
                    v_cod_retorno := -1;
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            PARENT_OUNIT_ID: ');
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - Ya existe una unidad organizativa informada como "root".');
                    RETURN v_cod_retorno;
                END IF;            
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 1 AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL THEN
                SELECT COUNT(1)INTO v_count FROM EWPCV_ORGANIZATION_UNIT ou             
                    INNER JOIN EWPCV_INST_ORG_UNIT iou ON ou.INTERNAL_ID = iou.ORGANIZATION_UNITS_ID
                    INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
                    WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID) AND ou.PARENT_OUNIT_ID IS NULL;
                    IF v_count = 0 THEN
                        v_cod_retorno := -1;
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - No existe ninguna unidad organizativa informada como root, inserte primero el "root".');
                    END IF;          
                    SELECT COUNT(1) INTO v_count FROM EWPCV_INST_ORG_UNIT iou
                    INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
					INNER JOIN EWPCV_ORGANIZATION_UNIT ou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID 
                    WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID)
                    AND UPPER(iou.ORGANIZATION_UNITS_ID) = 
                   (SELECT INTERNAL_ID FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT.PARENT_OUNIT_ID);
                    IF v_count = 0 THEN
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
                        v_cod_retorno := -1;
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - La unidad organizativa informada como padre no existe.');
                    END IF;		
         END IF;

         IF P_OUNIT.FACTSHEET_URL_LIST IS NOT NULL AND P_OUNIT.FACTSHEET_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_OUNIT.FACTSHEET_URL_LIST.FIRST .. P_OUNIT.FACTSHEET_URL_LIST.LAST LOOP 
				IF EWP.VALIDA_URL(P_OUNIT.FACTSHEET_URL_LIST(i).TEXT, 0, v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-FACTSHEET_URL_LIST('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;
		END IF;

		IF P_OUNIT.LOGO_URL IS NOT NULL AND EWP.VALIDA_URL(P_OUNIT.LOGO_URL, 0,v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-LOGO_URL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;

		IF P_OUNIT.OUNIT_CONTACT_DETAILS IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_DETAILS(P_OUNIT.OUNIT_CONTACT_DETAILS, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-OUNIT_CONTACT_DETAILS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_OUNIT.CONTACT_DETAILS_LIST, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-CONTACT_DETAILS_LIST: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

        RETURN v_cod_retorno;
    END;

	/*
		Valida los campos obligatorios de una OUNIT 
	*/
    FUNCTION VALIDA_OUNIT_STRUCT(P_OUNIT IN OUNIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
        v_cod_retorno 	    NUMBER := 0;
    BEGIN       
          IF P_OUNIT.HEI_ID IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - HEI_ID');
         END IF;

         IF P_OUNIT.OUNIT_CODE IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_CODE');
         END IF;

         IF  P_OUNIT.OUNIT_NAME_LIST IS NULL OR P_OUNIT.OUNIT_NAME_LIST.COUNT = 0 THEN 
              IF P_ERROR_MESSAGE IS NULL THEN
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              END IF;
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_NAME_LIST');
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - IS_TREE_STRUCTURE');
         END IF;

         RETURN v_cod_retorno;
    END;

     /*
		Inserta las urls de factsheet de la organizaciÃ³n 
	*/
	PROCEDURE INSERTA_FACTSHEET_URLS(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT) AS
	    v_lang_item_id VARCHAR2(255);
	BEGIN
        IF P_OUNIT.FACTSHEET_URL_LIST IS NOT NULL AND P_OUNIT.FACTSHEET_URL_LIST.COUNT > 0 THEN
            FOR i IN P_OUNIT.FACTSHEET_URL_LIST.FIRST .. P_OUNIT.FACTSHEET_URL_LIST.LAST 
            LOOP
                v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_OUNIT.FACTSHEET_URL_LIST(i));
                INSERT INTO EWPCV_OUNIT_FACT_SHEET_URL (URL_ID, OUNIT_ID) VALUES (v_lang_item_id, P_OUNIT_ID);
            END LOOP;
		END IF;
	END;

    /*
        Borra los nombres de la institution
    */
    PROCEDURE BORRA_OUNIT_NAMES (P_OUNIT_ID IN VARCHAR2) AS
        CURSOR c_names(p_orgun_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_ORGANIZATION_UNIT_NAME
			WHERE ORGANIZATION_UNIT_ID = p_orgun_id;
    BEGIN
        FOR names_rec IN c_names(P_OUNIT_ID)
		LOOP 
			DELETE FROM EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID = names_rec.NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = names_rec.NAME_ID;
		END LOOP;
    END;

    /*
		 Borra una ounit
	 */
    PROCEDURE BORRA_OUNIT(P_OUNIT_ID IN VARCHAR2) AS
        v_prim_cont_id   VARCHAR2(255);
        v_prim_cont_det_id VARCHAR2(255);
        CURSOR c_contacts_inst(v_ounit_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT 
			WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(v_ounit_id);
		BEGIN

			BORRA_OUNIT_NAMES(P_OUNIT_ID);    

            UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE INTERNAL_ID = P_OUNIT_ID;

            --Borramos Factsheet urls
            PKG_COMMON.BORRA_OUNIT_FACT_SHEET_URL(P_OUNIT_ID);

            --Borramos contacts
            FOR contact IN c_contacts_inst(P_OUNIT_ID)       
            LOOP
                PKG_COMMON.BORRA_CONTACT(contact.id);
            END LOOP;

            DELETE FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = P_OUNIT_ID;
            DELETE FROM EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = P_OUNIT_ID;

    END;

    /*
		Persiste una ounit en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_OUNIT_ID IN VARCHAR2,P_HEI_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		P_FACTSHEET_ID IN VARCHAR2, P_ORG_UNIT_CODE IN VARCHAR2, P_OUNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST, 
        P_PRIM_CONT_DETAIL_ID IN VARCHAR2, P_IS_TREE_STRUCTURE IN NUMBER,
        P_PARENT_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id        VARCHAR2(255);
        v_inst_id   VARCHAR2(255);
        CURSOR exist_cursor_i(p_hei_id IN VARCHAR) IS SELECT ID
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_hei_id);
	BEGIN 
        OPEN exist_cursor_i(P_HEI_ID) ;
        FETCH exist_cursor_i INTO v_inst_id;
        CLOSE exist_cursor_i;

        v_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE, ABBREVIATION, LOGO_URL, PRIMARY_CONTACT_DETAIL_ID, IS_TREE_STRUCTURE, PARENT_OUNIT_ID, INTERNAL_ID)
                                     VALUES (v_id, P_ORG_UNIT_CODE, P_ABREVIATION, P_LOGO_URL, P_PRIM_CONT_DETAIL_ID, P_IS_TREE_STRUCTURE, P_PARENT_OUNIT_ID,P_OUNIT_ID);
        INSERT INTO EWPCV_INST_ORG_UNIT (ORGANIZATION_UNITS_ID, INSTITUTION_ID) VALUES (P_OUNIT_ID, v_inst_id);                                     
		PKG_COMMON.INSERTA_OUNIT_NAMES(P_OUNIT_ID, P_OUNIT_NAMES);

		RETURN v_id;
	END;

    /* Inserta una OUNIT en el sistema
		Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_OUNIT(P_OUNIT IN OUNIT, P_OUNIT_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_prim_contact_id VARCHAR2(255);
		v_contact_id VARCHAR2(255);
		v_parent_internal_id VARCHAR2(255);
		v_id        VARCHAR2(255);
		v_id_parent VARCHAR2(255);
	BEGIN

		v_cod_retorno := VALIDA_OUNIT_STRUCT(P_OUNIT, P_ERROR_MESSAGE);

		IF v_cod_retorno = 0 THEN  

            v_cod_retorno := VALIDA_OUNIT_DATOS(P_OUNIT, null, P_ERROR_MESSAGE);

            IF v_cod_retorno = 0 THEN

                --Recogemos el INTERNAL_ID de el padre si viene informado
                IF P_OUNIT.PARENT_OUNIT_ID IS NOT NULL AND P_OUNIT.IS_TREE_STRUCTURE = 1 THEN
                    SELECT OU.INTERNAL_ID INTO v_parent_internal_id FROM EWPCV_ORGANIZATION_UNIT OU
                        INNER JOIN EWPCV_INST_ORG_UNIT IOU ON IOU.ORGANIZATION_UNITS_ID = OU.INTERNAL_ID
                        INNER JOIN EWPCV_INSTITUTION I ON I.ID = IOU.INSTITUTION_ID
                        WHERE OU.ID = P_OUNIT.PARENT_OUNIT_ID AND I.INSTITUTION_ID = P_OUNIT.HEI_ID;
                END IF;

            	v_id := EWP.GENERATE_UUID();

                -- Buscamos le ID del parent
                SELECT ou.INTERNAL_ID INTO v_id_parent 
                FROM EWPCV_ORGANIZATION_UNIT ou
                INNER JOIN EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
                INNER JOIN EWPCV_INSTITUTION ins ON ins.INSTITUTION_ID = iou.INSTITUTION_ID
                WHERE ou.ID = P_OUNIT.PARENT_OUNIT_ID AND ins.INSTITUTION_ID = P_OUNIT.HEI_ID;

                -- Insertamos la OUNIT en EWPCV_ORGANIZATION_UNIT. Lo hacemos ya para tener el ounit_code disponible a la hora de hacer el insert del contacto principal
                P_OUNIT_ID := INSERTA_OUNIT(v_id, P_OUNIT.HEI_ID, P_OUNIT.ABBREVIATION, P_OUNIT.LOGO_URL, null, P_OUNIT.OUNIT_CODE, 
                                                       P_OUNIT.OUNIT_NAME_LIST, null, P_OUNIT.IS_TREE_STRUCTURE, v_id_parent);

                -- Insertamos el contacto principal
                v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_OUNIT.OUNIT_CONTACT_DETAILS, P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);

                -- Insertamos la lista de contactos
                IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL AND P_OUNIT.CONTACT_DETAILS_LIST.COUNT > 0 THEN                
                    FOR i IN P_OUNIT.CONTACT_DETAILS_LIST.FIRST .. P_OUNIT.CONTACT_DETAILS_LIST.LAST 
                    LOOP
                        v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_OUNIT.CONTACT_DETAILS_LIST(i), P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);
                    END LOOP;
                END IF;

                -- Insertamos la lista de fact sheet urls
                INSERTA_FACTSHEET_URLS(v_id, P_OUNIT);

                -- Hacemos update de la OUNIT en EWPCV_ORGANIZATION_UNIT para setear el factsheet y el primary_contact_detail_id
                UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id
                    WHERE INTERNAL_ID = v_id;

                INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), v_id, 7, 0);
            END IF;
		END IF;

		COMMIT;

		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_OUNIT;	

    PROCEDURE ACTUALIZA_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT) IS
        v_prim_contact_id   VARCHAR2(255);
        v_factsheet_id      VARCHAR2(255);
        v_contact_id        VARCHAR2(255);
       CURSOR c_contacts(p_ounit_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(p_ounit_id);
    BEGIN

            BORRA_OUNIT_NAMES(P_OUNIT_ID);
            PKG_COMMON.INSERTA_OUNIT_NAMES(P_OUNIT_ID, P_OUNIT.OUNIT_NAME_LIST);

            UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE INTERNAL_ID = P_OUNIT_ID;
            FOR contact IN c_contacts(P_OUNIT_ID) 
            LOOP
                PKG_COMMON.BORRA_CONTACT(contact.id);
            END LOOP;
            -- Insertamos de nuevo el contacto principal
            v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_OUNIT.OUNIT_CONTACT_DETAILS, P_OUNIT.HEI_ID, P_OUNIT_ID);
            -- Insertamos de nuevo la lista de contactos
            IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL AND P_OUNIT.CONTACT_DETAILS_LIST.COUNT > 0 THEN                
                FOR i IN P_OUNIT.CONTACT_DETAILS_LIST.FIRST .. P_OUNIT.CONTACT_DETAILS_LIST.LAST 
                LOOP
                    v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_OUNIT.CONTACT_DETAILS_LIST(i), P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);
                END LOOP;
            END IF;

            PKG_COMMON.BORRA_OUNIT_FACT_SHEET_URL(P_OUNIT_ID);
            INSERTA_FACTSHEET_URLS(P_OUNIT_ID, P_OUNIT);

            UPDATE EWPCV_ORGANIZATION_UNIT SET 
               PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id, ABBREVIATION = P_OUNIT.ABBREVIATION, LOGO_URL = P_OUNIT.LOGO_URL, ORGANIZATION_UNIT_CODE = P_OUNIT.OUNIT_CODE
                WHERE INTERNAL_ID = P_OUNIT_ID;
    END;

	/* Actualiza una OUNIT del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la OUNIT
		Admite un objeto de tipo OUNIT con toda la informacion actualizada de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_prim_contact_id VARCHAR2(255);
        v_count NUMBER := 0;
       	v_internal_ounit_id VARCHAR2(255);
	BEGIN
		--obtenemos el id interno de la ounit a partir del id de interoperabilidad y el id de la institucion
		SELECT INTERNAL_ID INTO v_internal_ounit_id FROM EWPCV_ORGANIZATION_UNIT ou 
		INNER JOIN EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
		INNER JOIN EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
		WHERE ou.ID = P_OUNIT_ID AND ei.INSTITUTION_ID = P_OUNIT.HEI_ID;
		
        SELECT COUNT(1) INTO v_count FROM EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = v_internal_ounit_id;
		IF v_count > 0 THEN
            v_cod_retorno := VALIDA_OUNIT_STRUCT(P_OUNIT, P_ERROR_MESSAGE);
            IF v_cod_retorno = 0 THEN  
                v_cod_retorno := VALIDA_OUNIT_DATOS(P_OUNIT, P_OUNIT_ID, P_ERROR_MESSAGE);
                IF v_cod_retorno = 0 THEN      
                    ACTUALIZA_OUNIT(v_internal_ounit_id, P_OUNIT);
                    INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), v_internal_ounit_id, 7, 1);
                    COMMIT;
                END IF;
            END IF;
        ELSE
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			
			IF v_internal_ounit_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe o no está asociada a la institucion '||P_OUNIT.HEI_ID||' ');
			END IF;
			
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_OUNIT;

	/* Elimina una OUNIT del sistema.
		Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
        v_count_ounit_root NUMBER := 0;
       	v_internal_ounit_id VARCHAR2(255);
	BEGIN
		--obtenemos el id interno de la ounit a partir del id de interoperabilidad y el id de la institucion
		SELECT INTERNAL_ID INTO v_internal_ounit_id FROM EWPCV_ORGANIZATION_UNIT ou 
		INNER JOIN EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
		INNER JOIN EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
		WHERE ou.ID = P_OUNIT_ID AND ei.INSTITUTION_ID = P_INSTITUTION_SCHAC;
		
		SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = v_internal_ounit_id;
        SELECT COUNT(1) INTO v_count_ounit_root FROM EWPCV_ORGANIZATION_UNIT WHERE PARENT_OUNIT_ID = v_internal_ounit_id;
		IF v_count_ounit > 0 AND v_count_ounit_root = 0 THEN
			BORRA_OUNIT(v_internal_ounit_id);
			INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), v_internal_ounit_id, 7, 2);
		ELSE
			v_cod_retorno := -1;
            IF v_count_ounit = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
            ELSE
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se puede borrar una OUNIT padre sin antes borrar los hijos');
            END IF;
		END IF;
        COMMIT;
        return v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			
			IF v_internal_ounit_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe o no está asociada a la institucion '||P_INSTITUTION_SCHAC||' ');
			END IF;
			
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_OUNIT; 	 

    /*
        Actualiza el campo IS_EXPOSED de forma recursiva
    */
   PROCEDURE OCULTAR_O_MOSTRAR(P_OUNIT_ID IN VARCHAR2, P_IS_EXPOSED IN NUMBER) AS
        CURSOR c_org_ou(p_id_ou IN VARCHAR2) IS SELECT INTERNAL_ID
			FROM EWPCV_ORGANIZATION_UNIT
			WHERE PARENT_OUNIT_ID = p_id_ou;
   BEGIN
        UPDATE EWPCV_ORGANIZATION_UNIT SET IS_EXPOSED = P_IS_EXPOSED WHERE INTERNAL_ID = P_OUNIT_ID;
        INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (EWP.GENERATE_UUID(), P_OUNIT_ID, 7, 1);
        FOR org_ou IN c_org_ou(P_OUNIT_ID)
        LOOP
            OCULTAR_O_MOSTRAR(org_ou.INTERNAL_ID, P_IS_EXPOSED);
        END LOOP;
   END;

     /* Oculta una ounit de manera que esta deja de estar expuesta. 
        En el caso de estar en estructura de Ã¡rbol y ser una OUNIT padre, se ocultarÃ¡n tambiÃ©n todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION OCULTAR_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
       	v_internal_ounit_id VARCHAR2(255);
    BEGIN
	    --obtenemos el id interno de la ounit a partir del id de interoperabilidad y el id de la institucion
		SELECT INTERNAL_ID INTO v_internal_ounit_id FROM EWPCV_ORGANIZATION_UNIT ou 
		INNER JOIN EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
		INNER JOIN EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
		WHERE ou.ID = P_OUNIT_ID AND ei.INSTITUTION_ID = P_INSTITUTION_SCHAC;
	    
        SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = v_internal_ounit_id;
        IF v_count_ounit = 0 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        ELSE 
            OCULTAR_O_MOSTRAR(v_internal_ounit_id, 0);
        END IF;
        COMMIT;
        RETURN v_cod_retorno;

        EXCEPTION 
			WHEN OTHERS THEN
			
			IF v_internal_ounit_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe o no está asociada a la institucion '||P_INSTITUTION_SCHAC||' ');
			END IF;
			
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END OCULTAR_OUNIT;

    /* 	Muestra una ounit de manera que esta deja de estar expuesta. 
        En el caso de estar en estructura de Ã¡rbol y ser una OUNIT padre, se expondrÃ¡n tambiÃ©n todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION EXPONER_OUNIT(P_OUNIT_ID IN VARCHAR2, P_INSTITUTION_SCHAC IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
       	v_internal_ounit_id VARCHAR2(255);
    BEGIN
	    --obtenemos el id interno de la ounit a partir del id de interoperabilidad y el id de la institucion
		SELECT INTERNAL_ID INTO v_internal_ounit_id FROM EWPCV_ORGANIZATION_UNIT ou 
		INNER JOIN EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
		INNER JOIN EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
		WHERE ou.ID = P_OUNIT_ID AND ei.INSTITUTION_ID = P_INSTITUTION_SCHAC;
	    
        SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = v_internal_ounit_id;
        IF v_count_ounit = 0 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        ELSE 
            OCULTAR_O_MOSTRAR(v_internal_ounit_id, 1);
        END IF;
        COMMIT;
        RETURN v_cod_retorno;
        EXCEPTION 
			WHEN OTHERS THEN
			
			IF v_internal_ounit_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe o no está asociada a la institucion '||P_INSTITUTION_SCHAC||' ');
			END IF;
			
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END EXPONER_OUNIT;

END PKG_OUNITS;