/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

CREATE TABLE EWP.EWPCV_DELETED_ELEMENTS (
    ID VARCHAR2(255) PRIMARY KEY,
    ELEMENT_ID VARCHAR2(255) NOT NULL,
    ELEMENT_TYPE VARCHAR2(2) NOT NULL, -- Indica el tipo de entidad (0-IIA, 1-OMOBILITY, 2-IMOBILITY, 3-IMOBILITY_TOR, 4-IIA_APPROVAL, 5-LA)
    OWNER_SCHAC VARCHAR2(255) NOT NULL,
    DELETION_DATE DATE NOT NULL
);

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/


/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DELETED_ELEMENTS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DELETED_ELEMENTS TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_DELETED_ELEMENTS FOR EWP.EWPCV_DELETED_ELEMENTS;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS (ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v03.00.00', SYSDATE, '47_DELETED_ELEMENTS');