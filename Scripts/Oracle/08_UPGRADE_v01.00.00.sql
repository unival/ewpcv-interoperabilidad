create sequence EWP.SEC_EWPCV_SCRIPT_VERSION start with 1 increment by  1;

CREATE TABLE EWP.EWPCV_SCRIPT_VERSIONS (
   ID NUMBER(5,0) NOT NULL,
   EWPCV_VERSION VARCHAR2(255 CHAR) NOT NULL,
   VERSION_DATE DATE NOT NULL,
   SCRIPT VARCHAR2(255 CHAR),
   PRIMARY KEY (ID)
);

CREATE TABLE EWP.EWPCV_EVENT (
   ID VARCHAR2(255 CHAR) NOT NULL,
   EVENT_DATE DATE NOT NULL,
   TRIGGERING_HEI VARCHAR2(255 CHAR) NOT NULL,
   OWNER_HEI VARCHAR2(255 CHAR) NOT NULL,
   EVENT_TYPE VARCHAR2(255 CHAR) NOT NULL,
   CHANGED_ELEMENT_ID VARCHAR2(255 CHAR) NOT NULL,
   ELEMENT_TYPE NUMBER(10,0) NOT NULL,
   OBSERVATIONS VARCHAR2(4000 CHAR),
   PRIMARY KEY (ID)
);

COMMENT ON COLUMN EWP.EWPCV_EVENT.ELEMENT_TYPE IS 'Indica el tipo de entidad (0-IIA, 1-OMOBILITY, 2-IMOBILITY, 3-IMOBILITY_TOR, 4-IIA_APPROVAL, 5-LA)';
COMMENT ON COLUMN EWP.EWPCV_NOTIFICATION."TYPE" IS 'Indica el tipo de entidad que se debe actualizar (0-IIA, 1-OMOBILITY, 2-IMOBILITY, 3-IMOBILITY_TOR, 4-IIA_APPROVAL, 5-LA)';
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_EVENT TO APLEWP;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_EVENT FOR EWP.EWPCV_EVENT;
ALTER TABLE EWP.EWPCV_LEARNING_AGREEMENT ADD CHANGES_ID VARCHAR2(255 CHAR);
ALTER TABLE EWP.EWPCV_IIA ADD (IS_REMOTE NUMBER(1) DEFAULT 0);
ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST ADD (RETRIES NUMBER(5) DEFAULT 0);

ALTER TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL DROP CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2;
ALTER TABLE EWP.EWPCV_MOBILITY DROP CONSTRAINT FK_MOBILITY_2; 
ALTER TABLE EWP.EWPCV_SUBJECT_AREA DROP PRIMARY KEY;
ALTER TABLE EWP.EWPCV_SUBJECT_AREA ADD ID VARCHAR2(255 CHAR) NOT NULL;
ALTER TABLE EWP.EWPCV_SUBJECT_AREA ADD PRIMARY KEY (ID);
ALTER TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2 
FOREIGN KEY (ISCED_CODE) REFERENCES  EWP.EWPCV_SUBJECT_AREA (ID);
ALTER TABLE EWP.EWPCV_MOBILITY ADD CONSTRAINT FK_MOBILITY_2 
FOREIGN KEY (ISCED_CODE) REFERENCES  EWP.EWPCV_SUBJECT_AREA (ID);
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION(P_INSTITUTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_INSTITUTION I,
		EWP.EWPCV_INSTITUTION_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE I.ID = N.INSTITUTION_ID
		AND N.NAME_ID = LAIT.ID
		AND I.INSTITUTION_ID = p_id;
BEGIN
	OPEN C(P_INSTITUTION_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ORGANIZATION_UNIT O,
		EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE O.ID = N.ORGANIZATION_UNIT_ID
		AND N.NAME_ID = LAIT.ID
		AND O.ID = p_id;
BEGIN
	OPEN C(P_OUNIT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_NAME CN,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CN.CONTACT_ID
		AND CN.NAME_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPCIONES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_DESCRIPTION CD,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CD.CONTACT_ID
		AND CD.DESCRIPTION_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_URLS_CONTACTO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_CONTACT_URL U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE CD.ID = U.CONTACT_DETAILS_ID
		AND U.URL_ID = LAIT.ID
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_EMAILS(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EMAIL, ';')
		WITHIN GROUP (ORDER BY EMAIL)
    FROM EWP.EWPCV_CONTACT_DETAILS_EMAIL 
    WHERE CONTACT_DETAILS_ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_TELEFONO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(P.E164 || ':' || P.EXTENSION_NUMBER || ':' || P.OTHER_FORMAT, ';')
		WITHIN GROUP (ORDER BY P.E164)
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_PHONE_NUMBER P
    WHERE P.ID = CD.PHONE_NUMBER
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS_LINES(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(ADDRESS_LINE, ';')
		WITHIN GROUP (ORDER BY ADDRESS_LINE)
    FROM EWP.EWPCV_FLEXIBLE_ADDRESS_LINE 
    WHERE FLEXIBLE_ADDRESS_ID = p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(F.BUILDING_NUMBER|| ':' || F.BUILDING_NAME || ':' || F.STREET_NAME 
        || ':' || F.UNIT || ':' || F."FLOOR" || ':' || F.POST_OFFICE_BOX 
        || ':' || (
        SELECT LISTAGG(Delivery_point_code, ',') 
			WITHIN GROUP (ORDER BY Delivery_point_code)
        FROM EWPCV_FLEXAD_DELIV_POINT_COD
        WHERE FLEXIBLE_ADDRESS_ID = F.ID
        ), ';')
		WITHIN GROUP (ORDER BY F.STREET_NAME)
    FROM EWPCV_FLEXIBLE_ADDRESS F
    WHERE F.ID =  p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NIVELES_IDIOMAS(P_MOBILITY_ID IN VARCHAR2, P_REVISION IN NUMBER) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2, p_revision IN NUMBER) IS  SELECT LISTAGG(S."LANGUAGE"|| ':' || S.CEFR_LEVEL , ';')
		WITHIN GROUP (ORDER BY S."LANGUAGE")
    FROM EWP.EWPCV_MOBILITY_LANG_SKILL MLS,
		EWP.EWPCV_LANGUAGE_SKILL S
    WHERE MLS.LANGUAGE_SKILL_ID = S.ID
		AND MLS.MOBILITY_ID = p_id
		AND MLS.MOBILITY_REVISION = p_revision;
BEGIN
	OPEN C(P_MOBILITY_ID, P_REVISION);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_FIRMAS(P_FIRMA_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(SIGNER_NAME || ':' || SIGNER_POSITION 
        || ':' || SIGNER_EMAIL || ':' || "TIMESTAMP" || ':' || SIGNER_APP, ';')
			WITHIN GROUP (ORDER BY SIGNER_NAME)
    FROM EWP.EWPCV_SIGNATURE
    WHERE ID = p_id;
BEGIN
	OPEN C(P_FIRMA_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_CREDITS(P_COMPONENT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(CR.CREDIT_LEVEL || ':' || CR.SCHEME || ':' || CR.CREDIT_VALUE, ';')
		WITHIN GROUP (ORDER BY CR.CREDIT_LEVEL)
    FROM EWP.EWPCV_CREDIT CR,
		EWP.EWPCV_COMPONENT_CREDITS CCR
    WHERE CR.ID = CCR.CREDITS_ID
		AND CCR.COMPONENT_ID = p_id;
BEGIN
	OPEN C(P_COMPONENT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(TO_CHAR(ATR.START_DATE, 'YYYY.MM.DD')  || ':' || TO_CHAR(ATR.END_DATE, 'YYYY.MM.DD') 
		|| ':' || ATR.INSTITUTION_ID || ':' || o.ORGANIZATION_UNIT_CODE 
		|| ':' || ATR.TERM_NUMBER || ':' || ATR.TOTAL_TERMS, ';')
		WITHIN GROUP (ORDER BY ATR.ID )
    FROM EWP.EWPCV_ACADEMIC_TERM ATR
		LEFT JOIN EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
    WHERE ATR.ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ACADEMIC_TERM_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.DISP_NAME_ID = LAIT.ID
		AND N.ACADEMIC_TERM_ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.NAME_ID = LAIT.ID
		AND N.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPTIONS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_DESCRIPTION D,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE D.DESCRIPTION_ID = LAIT.ID
		AND D.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_URLS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_URLS U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE U.URL_ID = LAIT.ID
		AND U.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_S_AREA_L_SKILL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(S.ISCED_CODE || ':' || S.ISCED_CLARIFICATION || ':' || L."LANGUAGE" || ':' ||L.CEFR_LEVEL, ';')
		WITHIN GROUP (ORDER BY S.ISCED_CODE)
    FROM EWP.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
		LEFT JOIN EWP.EWPCV_LANGUAGE_SKILL L	
			ON CSL.LANGUAGE_SKILL_ID = L.ID
		LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
			ON CSL.ISCED_CODE = S.ID	
    WHERE CSL.COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_EQF_LEVEL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EQF_LEVEL, ';')
		WITHIN GROUP (ORDER BY EQF_LEVEL)
    FROM EWP.EWPCV_COOPCOND_EQFLVL
    WHERE COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
/*
	Obtiene la informacion general de los IIAs
	Se empleará para saber que IIAs se han modificado y se deben de actualizar
*/
CREATE OR REPLACE view EWP.IIAS_VIEW AS SELECT 
	ID                     AS IIA_ID,
	IIA_CODE               AS IIA_CODE,
	REMOTE_IIA_ID          AS REMOTE_IIA_ID,
	REMOTE_IIA_CODE        AS REMOTE_IIA_CODE,
	START_DATE             AS IIA_START_DATE,
	END_DATE               AS IIA_END_DATE,
	MODIFY_DATE            AS IIA_MODIFY_DATE,
	APPROVAL_DATE          AS IIA_APPROVAL_DATE,
	REMOTE_COP_COND_HASH   AS REMOTE_HASH,
	APPROVAL_COP_COND_HASH AS APPROVAL_HASH,
	IS_REMOTE			   AS IS_REMOTE
FROM EWPCV_IIA;

/*
	Obtiene la informacion de las condiciones de cooperacion de los IIAS 
	Se empleará para recuperar el detalle de un IIA
*/
CREATE OR REPLACE view EWP.IIA_COOP_CONDITIONS_VIEW AS SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	EXTRAE_EQF_LEVEL(CC.ID)                                                                                AS COOP_COND_EQF_LEVEL,
	D.NUMBERDURATION || ':' ||D.UNIT                                                                       AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY ||':'|| MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                           AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                          AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)        AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                          AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                       AS RECEIVER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                               AS RECEIVER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                         AS RECEIVER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(RPSC.ID)                                                                                 AS RECEIVER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                               AS RECEIVER_SIGNER_PHONES,
	EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                             AS RECEIVER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                                   AS RECEIVER_SIGNER_ADDRESS,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                          AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)        AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                          AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                       AS SENDER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                               AS SENDER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                         AS SENDER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(SPSC.ID)                                                                                 AS SENDER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                               AS SENDER_SIGNER_PHONES,
	EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                             AS SENDER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                                   AS SENDER_SIGNER_ADDRESS,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;



COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.COOP_COND_EQF_LEVEL IS 'FORMATO: EQF_LEVEL;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.COOP_COND_DURATION IS 'FORMATO: NUMBER:UNIT';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.MOBILITY_TYPE IS 'FORMATO: CATEGORY:GROUP';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SUBJECT_AREAS IS 'FORMATO: ISCED_CODE:ISCED_CLARIFICATION:LANGUAGE:CEFR_LVL;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';

/*
	Obtiene la informacion de las movilidades de las personas involucradas en la misma. 
	Para consultar las outgoing filtraremos por M.SENDING_INSTITUTION_ID = nuestro SCHAC
	Para consultar las incoming filtraremos por M.RECEIVING_INSTITUTION_ID = nuestro SCHAC
*/
CREATE OR REPLACE view EWP.MOBILITY_VIEW AS SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL                                                                                                AS EQF_LEVEL,
	I.IIA_CODE                                                                                                 AS IIA_CODE,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE || ':' || SA.ISCED_CLARIFICATION                                                             AS SUBJECT_AREA,
	M.STATUS                                                                                                   AS MOBILITY_STATUS,
	MT.MOBILITY_CATEGORY || ':' || MT.MOBILITY_GROUP                                                           AS MOBILITY_TYPE,
	EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.ID                                                                                                      AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
INNER JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
INNER JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
INNER JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
INNER JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
INNER JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
INNER JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
LEFT JOIN EWPCV_IIA I
	ON M.IIA_ID = I.ID
;

COMMENT ON COLUMN EWP.MOBILITY_VIEW.SUBJECT_AREA IS 'FORMATO: ISCED_CODE:CLARIFICATION';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';

/*
	Obtiene la informacion de los learning agreements. Se empleará para identificar que learning agreements se han modificado.
	La informacion del estudiante en esta tabla solo estará presente cuando suponga un cambio sobre la informacion original que figura en la movilidad.
*/
CREATE OR REPLACE view EWP.LEARNING_AGREEMENT_VIEW AS SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	M.MOBILITY_PARTICIPANT_ID                  AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS;


COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';


/*
	Obtiene la informacion de los componentes asociados a los learning agreements.
*/
CREATE OR REPLACE view EWP.LA_COMPONENTS_VIEW AS SELECT 
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	EXTRAE_CREDITS(C.id)                            AS CREDITS,
	EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	AY.START_YEAR || ':' || AY.END_YEAR             AS ACADEMIC_YEAR,
	C.STATUS                                        AS STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_CODE                                      AS LOS_CODE,
	LOS.INSTITUTION_ID                              AS INSTITUTION_ID,
	EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION
FROM EWPCV_LEARNING_AGREEMENT LA
LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC 
    ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_RECOGNIZED_LA_COMPONENT RC 
    ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT VC 
    ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_BLENDED_LA_COMPONENT BC 
    ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT DC 
    ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWPCV_LA_COMPONENT C
    ON C.ID = SC.STUDIED_LA_COMPONENT_ID
    OR C.ID = RC.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = VC.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = BC.BLENDED_LA_COMPONENT_ID
    OR C.ID = DC.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID    
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID    
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID
LEFT JOIN EWPCV_ORGANIZATION_UNIT O 
    ON LOS.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID;

COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.CREDITS IS 'FORMATO: LEVEL:SCHEME:VALUE;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_TERM IS 'FORMATO: STAR_DATE:END_DATE:INSTITUTION_ID:OUNIT_CODE:TERM_NUMBER:TOTAL_TERMS';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_TERM_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_YEAR IS 'FORMATO: START_YEAR:END_YEAR';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_DESCRIPTIONS IS 'FORMATO: DESCRIPTION:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_URL IS 'FORMATO: URL:LANG;...';


/*
	Obtiene la informacion obtenida de los servicios de diccionarios.
*/
CREATE OR REPLACE view EWP.DICTIONARIES_VIEW AS SELECT 
	ID 				AS ID,
	CODE 			AS CODE,
	DESCRIPTION 	AS DESCRIPTION,
	DICTIONARY_TYPE AS DICTIONARY_TYPE,
	CALL_YEAR 		AS DICTIONARY_YEAR
FROM EWP.EWPCV_DICTIONARY;

COMMENT ON COLUMN EWP.DICTIONARIES_VIEW.DICTIONARY_TYPE IS 'Posibles valores:key_actions, action_types, countries, country_regions, country_groups, distance_bands, education_fields, education_levels, field_groups, language_groups, languages, organization_types, seniorities, training_types, working_categories';
COMMENT ON COLUMN EWP.DICTIONARIES_VIEW.DICTIONARY_YEAR IS 'anyo pasado para la consulta de valores del diccionario';

/*
	Obtiene la informacion de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_VIEW AS SELECT 
	I.INSTITUTION_ID    				  	 	 	AS INSTITUTION_ID,
	I.ABBREVIATION	 					  	 	 	AS ABREVIATION,
	MAX(F.DECISION_WEEKS_LIMIT)			  	 	 	AS DECISION_WEEK_LIMIT,
	MAX(F.TOR_WEEKS_LIMIT) 					  	 	AS TOR_WEEK_LIMIT,
	MAX(F.NOMINATIONS_AUTUM_TERM)			  	 	AS NOMINATIONS_AUTUM_TERM,
	MAX(F.NOMINATIONS_SPRING_TERM)			  	 	AS NOMINATIONS_SPRING_TERM,
	MAX(F.APPLICATION_AUTUM_TERM)			  	 	AS APPLICATION_AUTUM_TERM,
	MAX(F.APPLICATION_SPRING_TERM)			  	 	AS APPLICATION_SPRING_TERM,
	EXTRAE_EMAILS(MAX(F.CONTACT_DETAILS_ID))	  	AS APPLICATION_EMAIL,
	EXTRAE_TELEFONO(MAX(F.CONTACT_DETAILS_ID)) 	 	AS APPLICATION_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(F.CONTACT_DETAILS_ID)) AS APPLICATION_URLS,
	EXTRAE_EMAILS(MAX(HI.CONTACT_DETAIL_ID))	  	AS HOUSING_EMAIL,
	EXTRAE_TELEFONO(MAX(HI.CONTACT_DETAIL_ID)) 	 	AS HOUSING_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(HI.CONTACT_DETAIL_ID))	AS HOUSING_URLS,
	EXTRAE_EMAILS(MAX(VI.CONTACT_DETAIL_ID))	  	AS VISA_EMAIL,
	EXTRAE_TELEFONO(MAX(VI.CONTACT_DETAIL_ID)) 	  	AS VISA_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID)) AS VISA_URLS,
	EXTRAE_EMAILS(MAX(II.CONTACT_DETAIL_ID))	  	AS INSURANCE_EMAIL,
	EXTRAE_TELEFONO(MAX(II.CONTACT_DETAIL_ID)) 	 	AS INSURANCE_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID))	AS INSURANCE_URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_FACT_SHEET F 
    ON I.FACT_SHEET = F.ID
LEFT JOIN EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
LEFT JOIN EWPCV_INFORMATION_ITEM HI
	ON IIT.INFORMATION_ID = HI.ID AND HI."TYPE" = 'HOUSING'
LEFT JOIN EWPCV_INFORMATION_ITEM VI
	ON IIT.INFORMATION_ID = VI.ID AND VI."TYPE" = 'VISA'
LEFT JOIN EWPCV_INFORMATION_ITEM II
	ON IIT.INFORMATION_ID = II.ID AND II."TYPE" = 'INSURANCE'
GROUP BY I.INSTITUTION_ID,I.ABBREVIATION
;

COMMENT ON COLUMN EWP.FACTSHEET_VIEW.APPLICATION_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.APPLICATION_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.HOUSING_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.HOUSING_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.VISA_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.VISA_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.INSURANCE_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.INSURANCE_URLS IS 'FORMATO: CONTACT_URL:LANG;...';

/*
	Obtiene la informacion de contacto adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_ADD_INFO_VIEW AS SELECT 
	I.INSTITUTION_ID 		 			        AS INSTITUTION_ID,
	AI."TYPE"		 					        AS INFO_TYPE,
	EXTRAE_EMAILS(AI.CONTACT_DETAIL_ID)	    AS EMAIL,
	EXTRAE_TELEFONO(AI.CONTACT_DETAIL_ID)      AS PHONE,
	EXTRAE_URLS_CONTACTO(AI.CONTACT_DETAIL_ID) AS URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
INNER JOIN EWPCV_INFORMATION_ITEM AI
	ON IIT.INFORMATION_ID = AI.ID
    AND AI."TYPE" NOT IN ('HOUSING','VISA','INSURANCE');
	
COMMENT ON COLUMN EWP.FACTSHEET_ADD_INFO_VIEW.PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_ADD_INFO_VIEW.URLS IS 'FORMATO: CONTACT_URL:LANG;...';

/*
	Obtiene la informacion de contacto sobre requerimientos adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_REQ_INFO_VIEW AS SELECT 
	I.INSTITUTION_ID 							 AS INSTITUTION_ID,
	REI."TYPE" 								     AS REQ_TYPE,
	REI.NAME 								    AS NAME,
	REI.DESCRIPTION 						     AS DESCRIPTION,
    EXTRAE_EMAILS(REI.CONTACT_DETAIL_ID)	    AS EMAIL,
	EXTRAE_TELEFONO(REI.CONTACT_DETAIL_ID)      AS PHONE,
	EXTRAE_URLS_CONTACTO(REI.CONTACT_DETAIL_ID) AS URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_INS_REQUIREMENTS IR
	ON IR.INSTITUTION_ID = I.ID 
INNER JOIN EWPCV_REQUIREMENTS_INFO REI
	ON IR.REQUIREMENT_ID = REI.ID;

COMMENT ON COLUMN EWP.FACTSHEET_REQ_INFO_VIEW.PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_REQ_INFO_VIEW.URLS IS 'FORMATO: CONTACT_URL:LANG;...';

/*
	Obtiene la informacion del registro de eventos de la aplicacion.
*/
CREATE OR REPLACE view EWP.EVENT_VIEW AS SELECT 

ID					AS ID,
EVENT_DATE			AS EVENT_DATE,
TRIGGERING_HEI		AS TRIGGERING_HEI,
OWNER_HEI			AS OWNER_HEI,
EVENT_TYPE			AS EVENT_TYPE,
CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
ELEMENT_TYPE		AS ELEMENT_TYPE,
OBSERVATIONS		AS OBSERVATIONS
FROM EWP.EWPCV_EVENT;
/
create or replace PACKAGE EWP.PKG_COMMON AS 

	/* 
		Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
	*/
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2);

	/* 
		Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
	*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2);

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2);

    /*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta datos de persona independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2;

	/*
		Inserta un telefono de contacto independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2;

    /*
		Inserta una direccion independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2;

    /*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2;

    /*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2;

    /*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2;    

    /*
		Inserta un nivel de idioma independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2;

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) ;
	
    /*
        Utilidad para pintar clobs por consola
    */
    procedure print_clob( p_clob in clob );
	
	
END PKG_COMMON;
/
create or replace PACKAGE BODY EWP.PKG_COMMON AS 
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	
	/* Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) */
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2) AS
		v_phone_id VARCHAR2(255CHAR);
		v_mailing_id VARCHAR2(255CHAR);
		v_street_id VARCHAR2(255CHAR);
		CURSOR c_contact_detail(p_contact_id IN VARCHAR2) IS 
			SELECT PHONE_NUMBER, MAILING_ADDRESS, STREET_ADDRESS
			FROM EWPCV_CONTACT_DETAILS
			WHERE ID = p_contact_id;
		CURSOR c_urls(p_contact_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_CONTACT_URL
			WHERE CONTACT_DETAILS_ID = p_contact_id;
	BEGIN
		OPEN c_contact_detail(P_ID);
		FETCH c_contact_detail INTO v_phone_id, v_mailing_id, v_street_id;
		CLOSE c_contact_detail;

		FOR url_rec IN c_urls(P_ID)
		LOOP 
			DELETE FROM EWPCV_CONTACT_URL WHERE URL_ID = url_rec.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = url_rec.URL_ID;
		END LOOP;
		DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = P_ID;

		DELETE FROM EWPCV_CONTACT_DETAILS WHERE ID = P_ID;

		DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_phone_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_mailing_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_street_id;
	END;

	/* Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2) AS
		v_person_id VARCHAR2(255CHAR);
		v_details_id VARCHAR2(255CHAR);
		CURSOR c_contact(p_contact_id IN VARCHAR2) IS 
			SELECT PERSON_ID, CONTACT_DETAILS_ID
			FROM EWPCV_CONTACT
			WHERE ID = p_contact_id;
		CURSOR c_names(p_contact_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = p_contact_id;
		CURSOR c_descs(p_contact_id IN VARCHAR2) IS 
			SELECT DESCRIPTION_ID
			FROM EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = p_contact_id;
	BEGIN
		IF P_ID IS NOT NULL THEN 
			OPEN c_contact(P_ID);
			FETCH c_contact INTO v_person_id, v_details_id;
			CLOSE c_contact;

			FOR name_rec IN c_names(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_NAME WHERE NAME_ID = name_rec.NAME_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = name_rec.NAME_ID;
			END LOOP;
			FOR desc_rec IN c_descs(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = desc_rec.DESCRIPTION_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = desc_rec.DESCRIPTION_ID;
			END LOOP;
			DELETE FROM EWPCV_CONTACT WHERE ID = P_ID;
			BORRA_CONTACT_DETAILS(v_details_id);
			DELETE FROM EWPCV_PERSON WHERE ID = v_person_id;
		END IF;
	END;

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2) AS
		v_ay_id VARCHAR2(255 CHAR);
		v_ay_count NUMBER;
		CURSOR c_ay(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_YEAR_ID
			FROM EWPCV_ACADEMIC_TERM
			WHERE ID = p_id;

		CURSOR c_n(p_id IN VARCHAR2) IS 
			SELECT DISP_NAME_ID
			FROM EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = p_id;
	BEGIN

		FOR rec IN c_n(P_ID) 
		LOOP
			DELETE FROM EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = rec.DISP_NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.DISP_NAME_ID;
		END LOOP;

		OPEN c_ay(P_ID);
		FETCH c_ay INTO v_ay_id;
		CLOSE c_ay;
		DELETE FROM EWPCV_ACADEMIC_TERM WHERE ID = P_ID;
		SELECT COUNT(1) INTO v_ay_count FROM EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = v_ay_id;
		IF v_ay_count = 0 THEN 
			DELETE FROM EWPCV_ACADEMIC_YEAR WHERE ID = v_ay_id;
		END IF;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_LANGUAGE_ITEM.LANG IS NOT NULL OR  P_LANGUAGE_ITEM.TEXT IS NOT NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (v_id, P_LANGUAGE_ITEM.LANG, P_LANGUAGE_ITEM.TEXT); 
		END IF;

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_inst_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_INSTITUTION_NAME INA ON LAIT.ID = INA.NAME_ID 
			WHERE INA.INSTITUTION_ID = p_inst_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_INSTITUTION_NAMES IS NOT NULL AND P_INSTITUTION_NAMES.COUNT > 0 THEN
			FOR i IN P_INSTITUTION_NAMES.FIRST .. P_INSTITUTION_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_INSTITUTION_ID, P_INSTITUTION_NAMES(i).LANG, P_INSTITUTION_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION_NAMES(i));
				END IF; 
				v_count := 1;
				SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION_NAME WHERE NAME_ID = v_lang_item_id AND INSTITUTION_ID = P_INSTITUTION_ID;
				IF v_lang_item_id IS NOT NULL AND v_count = 0 THEN 
					INSERT INTO EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES (v_lang_item_id, P_INSTITUTION_ID);
				END IF;
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_INSTITUTION 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_id);
	BEGIN

		OPEN exist_cursor(P_INSTITUTION_ID) ;
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET) VALUES (v_id, P_INSTITUTION_ID, P_ABREVIATION, P_LOGO_URL, P_FACTSHEET_ID); 
		ELSIF P_ABREVIATION IS NOT NULL OR P_LOGO_URL IS NOT NULL OR P_FACTSHEET_ID IS NOT NULL THEN
			UPDATE  EWPCV_INSTITUTION SET  ABBREVIATION = P_ABREVIATION, LOGO_URL = P_LOGO_URL, FACT_SHEET = P_FACTSHEET_ID WHERE ID = v_id; 
		END IF;

		INSERTA_INSTITUTION_NAMES(v_id, P_INSTITUTION_NAMES);

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_ounit_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_ORGANIZATION_UNIT_NAME ONA ON LAIT.ID = ONA.ORGANIZATION_UNIT_ID 
			WHERE ONA.ORGANIZATION_UNIT_ID = p_ounit_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_ORGANIZATION_UNIT_NAMES IS NOT NULL AND P_ORGANIZATION_UNIT_NAMES.COUNT > 0 THEN
			FOR i IN P_ORGANIZATION_UNIT_NAMES.FIRST .. P_ORGANIZATION_UNIT_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_OUNIT_ID, P_ORGANIZATION_UNIT_NAMES(i).LANG,P_ORGANIZATION_UNIT_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := INSERTA_LANGUAGE_ITEM(P_ORGANIZATION_UNIT_NAMES(i));
					v_count := 1;
				END IF;
				SELECT COUNT(1) INTO v_count FROM EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID = v_lang_item_id AND ORGANIZATION_UNIT_ID = P_OUNIT_ID;
				IF v_lang_item_id IS NOT NULL AND v_count = 0 THEN 
					INSERT INTO EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES (v_lang_item_id, P_OUNIT_ID);
				END IF;
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_hei_id IN VARCHAR, p_code IN VARCHAR) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_code)
			AND UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id);

	BEGIN
		OPEN exist_cursor(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ORGANIZATION_UNIT_CODE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF  v_id IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (v_id, P_INSTITUTION.ORGANIZATION_UNIT_CODE); 
		END IF;

		INSERTA_OUNIT_NAMES(v_id, P_INSTITUTION.ORGANIZATION_UNIT_NAME);
		RETURN v_id;
	END;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
		v_inst_id VARCHAR2(255);
		v_ou_id VARCHAR2(255);
		v_count VARCHAR2(255);
	BEGIN
		v_inst_id := INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, null, null, null, P_INSTITUTION.INSTITUTION_NAME);
		v_ou_id := INSERTA_OUNIT(P_INSTITUTION);
		SELECT COUNT(1) INTO v_count FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = v_ou_id;
		IF v_count = 0 THEN 
			INSERT INTO EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (v_inst_id, v_ou_id);
		END IF;
		RETURN v_ou_id;

	END;

	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PERSON.GIVEN_NAME IS NOT NULL 
			OR P_PERSON.FAMILY_NAME IS NOT NULL 
			OR P_PERSON.BIRTH_DATE IS NOT NULL 
			OR P_PERSON.CITIZENSHIP IS NOT NULL 
			OR P_PERSON.GENDER IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
					VALUES (v_id, P_PERSON.GIVEN_NAME, P_PERSON.FAMILY_NAME, P_PERSON.CITIZENSHIP, P_PERSON.BIRTH_DATE, P_PERSON.GENDER);
		END IF;
		return v_id;
	END;

	/*
		Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PHONE.E164 IS NOT NULL 
			OR P_PHONE.EXTENSION_NUMBER IS NOT NULL 
			OR P_PHONE.OTHER_FORMAT IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
					VALUES (v_id, P_PHONE.E164, P_PHONE.EXTENSION_NUMBER, P_PHONE.OTHER_FORMAT);
		END IF;
		return v_id;
	END;

	/*
		Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_ADDRESS.POSTAL_CODE IS NOT NULL 
			OR P_ADDRESS.LOCALITY IS NOT NULL 
			OR P_ADDRESS.REGION IS NOT NULL
			OR P_ADDRESS.COUNTRY IS NOT NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (v_id, P_ADDRESS.BUILDING_NUMBER, P_ADDRESS.BUILDING_NAME, P_ADDRESS.COUNTRY,
					P_ADDRESS.BUILDING_FLOOR, P_ADDRESS.LOCALITY, P_ADDRESS.POST_OFFICE_BOX, P_ADDRESS.POSTAL_CODE,
					P_ADDRESS.REGION, P_ADDRESS.STREET_NAME,P_ADDRESS.UNIT );

				IF P_ADDRESS.RECIPIENT_NAMES IS NOT NULL AND P_ADDRESS.RECIPIENT_NAMES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.RECIPIENT_NAMES.FIRST .. P_ADDRESS.RECIPIENT_NAMES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) VALUES (v_id, P_ADDRESS.RECIPIENT_NAMES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.ADDRESS_LINES IS NOT NULL AND P_ADDRESS.ADDRESS_LINES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.ADDRESS_LINES.FIRST .. P_ADDRESS.ADDRESS_LINES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE) VALUES (v_id, P_ADDRESS.ADDRESS_LINES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.DELIVERY_POINT_CODES IS NOT NULL AND P_ADDRESS.DELIVERY_POINT_CODES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.DELIVERY_POINT_CODES.FIRST .. P_ADDRESS.DELIVERY_POINT_CODES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE) VALUES (v_id, P_ADDRESS.DELIVERY_POINT_CODES(i));
					END LOOP;
				END IF;
		END IF;
		return v_id;
	END;

	/*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
	BEGIN
		v_p_id:= INSERTA_TELEFONO(P_CONTACT.PHONE);
		v_a_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.STREET_ADDRESS);

		IF v_p_id IS NOT NULL 
			OR v_a_id IS NOT NULL 
			OR (P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0) 
			OR (P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0) THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT_DETAILS (ID, PHONE_NUMBER, STREET_ADDRESS) VALUES (v_id, v_p_id, v_a_id);

				IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN
					FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST 
					LOOP
						INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (v_id, P_CONTACT.EMAIL(i));
					END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN
					FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST 
					LOOP
						v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_URL(i));
						INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (v_id, v_li_id);
					END LOOP;
				END IF;
		END IF;

		return v_id;
	END;	

	/*
		Inserta datos de contacto y de persona 
	*/
	FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN

		v_p_id := INSERTA_PERSONA(P_CONTACT_PERSON);
		v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT);

		IF v_p_id IS NOT NULL 
			OR v_c_id IS NOT NULL 
			OR (P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0) 
			OR (P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

				OPEN ounit_cursor(P_CONTACT_PERSON.CONTACT.INSTITUTION_ID, P_CONTACT_PERSON.CONTACT.ORGANIZATION_UNIT_CODE);
				FETCH ounit_cursor INTO v_ounit_id;
				CLOSE ounit_cursor;

				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (v_id, P_CONTACT_PERSON.CONTACT.INSTITUTION_ID, v_ounit_id, P_CONTACT_PERSON.CONTACT.CONTACT_ROLE, v_c_id, v_p_id);

				IF P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0 THEN
						FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_NAME.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_NAME.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_NAME(i));
							INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;

				IF P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
						FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION(i));
							INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;
		END IF;	
		RETURN v_id;
	END;

		/*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		
		IF P_SUBJECT_AREA.ISCED_CODE IS NOT NULL OR P_SUBJECT_AREA.ISCED_CLARIFICATION IS NOT NULL THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (v_id, P_SUBJECT_AREA.ISCED_CODE, P_SUBJECT_AREA.ISCED_CLARIFICATION);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_lang IN VARCHAR, p_cefr IN VARCHAR) IS SELECT ID
			FROM EWPCV_LANGUAGE_SKILL
			WHERE UPPER("LANGUAGE") = UPPER(p_lang)
			AND  UPPER(CEFR_LEVEL) = UPPER(p_cefr);
	BEGIN
		OPEN exist_cursor(P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL );
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_LANGUAGE_SKILL.LANG IS NOT NULL OR  P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (v_id, P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL);
		END IF;
		RETURN v_id;
	END;

    procedure print_clob( p_clob in clob ) is
          v_offset number default 1;
          v_chunk_size number := 10000;
      begin
          loop
              exit when v_offset > dbms_lob.getlength(p_clob);
              dbms_output.put_line( dbms_lob.substr( p_clob, v_chunk_size, v_offset ) );
              v_offset := v_offset +  v_chunk_size;
          end loop;
      end print_clob;  
	  
	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) AS
	BEGIN
		INSERT INTO EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (EWP.GENERATE_UUID(),P_ELEMENT_ID, UPPER(P_NOTIFY_HEI), SYSDATE, P_TYPE, 1, 0, UPPER(P_NOTIFIER_HEI) );
	END;
  
END PKG_COMMON;
/
CREATE OR REPLACE PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoño
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoño
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  
	
	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;

	
	  
	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;
	
	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );
	
	-- FUNCIONES 
	
	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
	
	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	
END PKG_FACTSHEET;
/
create or replace PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-DECISION_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-TOR_WEEK_LIMIT';
		END IF;
		
		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_AUTUM_TERM';
		END IF;
		
		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_SPRING_TERM';
		END IF;
		
		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_AUTUM_TERM';
		END IF;
		
		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_SPRING_TERM';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
	
		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-EMAIL';
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-PHONE';
		END IF;
		
		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-URL';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP
		
			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ADITIONAL_INFO'|| i || ':' ||v_mensaje_it;
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ACCESSIBILITY_REQUIREMENTS'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				REQUIREMENTS_INFO'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
	BEGIN

		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID';
		END IF;
        
		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			FACTSHEET:' || v_mensaje_factsheet;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			APPLICATION_INFO:' || v_mensaje_app_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			HOUSING_INFO:' || v_mensaje_hou_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			VISA_INFO:' || v_mensaje_vis_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			INSURANCE_INFO:' || v_mensaje_ins_info;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ADDITIONAL_INFO:' || v_mensaje_add_info;
			END IF;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ACCESSIBILITY_REQUIREMENTS:' || v_mensaje_acc_req;
			END IF;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			REQUIREMENTS_INFO:' || v_mensaje_add_req;
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;
	
	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;
	
	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;
	
	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
	BEGIN
		UPDATE EWPCV_INSTITUTION SET FACT_SHEET = NULL WHERE ID = P_INSTITUTION_ID;
		DELETE FROM EWPCV_FACT_SHEET WHERE ID = P_FACTSHEET_ID;
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;
	
	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************
	
	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;
		
		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);
		
		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;
		
		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;
	
	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);
		
		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,UPPER(P_INF_TYPE),v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN
		
		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);
		
		v_factsheet_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
			VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
				P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
		RETURN v_factsheet_id;
	END;
	
	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN
	
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		
		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, UPPER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;
	
	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);
		
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;
		
	END;
	
	
	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;
	
	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
	
		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);
		
		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;
	
	
	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);
		
	BEGIN
	
		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;
		
		BORRA_NOMBRES(P_INSTITUTION_ID);
		PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		
		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);
		
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);
		
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;
			
	END;
	
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		SELECT COUNT(1)INTO v_count FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(P_FACTSHEET.INSTITUTION_ID) AND FACT_SHEET IS NOT NULL;
		IF v_count > 0 THEN 
			P_ERROR_MESSAGE := 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET);
		END IF;
		COMMIT;
	
		RETURN v_cod_retorno;
		
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;
	
	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET 
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id;
		CLOSE c_inst;
		
		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;
		
		IF v_f_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;
		
		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET 
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id;
		CLOSE c_inst;
		
		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;
		
		IF v_f_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;
		
		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;
/
create or replace PACKAGE     EWP.PKG_IIAS AS 

	-- TIPOS DE DATOS
	
	/*Datos del tipo de movilidad. Campos obligatorios marcados con *.
			MOBILITY_CATEGORY*		Categoría de la movilidad, puede ser  teaching, studies, training, 
			MOBILITY_GROUP*			Grupo en que se engloba la movilidad puede ser student o staff
	*/  
	TYPE MOBILITY_TYPE IS RECORD
	  (
		MOBILITY_CATEGORY			VARCHAR2(255 CHAR),
		MOBILITY_GROUP				VARCHAR(255 CHAR)
	  ); 

	/*Informacion sobre cada una de las partes del IIA. Campos obligatorios marcados con *.
			INSTITUTION	*				Institucion a la que representa el partner
			SIGNING_DATE				Fecha en la que esta parte firmo el acuerdo
			SIGNER_PERSON				Datos de la persona que firmo el acuerdo en representacion de esta parte
			PARTNER_CONTACTS			Lista de personas de contacto de esta parte del acuerdo
	*/  
	TYPE PARTNER IS RECORD
	  (
		INSTITUTION					EWP.INSTITUTION,
		SIGNING_DATE				DATE,
		SIGNER_PERSON				EWP.CONTACT_PERSON,
		PARTNER_CONTACTS			EWP.CONTACT_PERSON_LIST
	  );

	/*Informacion sobre las areas de ensenyanza y el idioma que se recomienda para cada una.
		Campos obligatorios marcados con *.
			SUBJECT_AREA	 			Areas de ensenyanza
			LANGUAGE_SKILL				Idioma que se recomienda para el area indicada
	*/  
	TYPE SUBJECT_AREA_LANGUAGE IS RECORD
	  (
		SUBJECT_AREA	 			EWP.SUBJECT_AREA,
		LANGUAGE_SKILL	 			EWP.LANGUAGE_SKILL
	  );

	/* Lista con todas ensenyanza asociadas a una condicion de cooperacion y el idioma que se recomienda para cada una. */  
	TYPE SUBJECT_AREA_LANGUAGE_LIST IS TABLE OF SUBJECT_AREA_LANGUAGE INDEX BY BINARY_INTEGER ;

	/*Informacion sobre la duracion de una condicion de cooperacion
		Campos obligatorios marcados con *.
			NUMBER_DURATION	 			Cantidad de las unidades indicadas que dura la condicion de cooperacion.
			UNIT	 					Unidades en que se expresa la duracion de la condicion de cooperacion.
										0:HOURS, 1:DAYS, 2:WEEKS, 3:MONTHS, 4:YEARS
	*/  
	TYPE DURATION IS RECORD
	  (
		NUMBER_DURATION	 			NUMBER(10,0),
		UNIT	 					NUMBER(1,0)
	  );	  

	/* Lista de niveles de estudios relacionado con la condicion de cooperacion  */  
	TYPE EQF_LEVEL_LIST IS TABLE OF NUMBER(3,0) INDEX BY BINARY_INTEGER;  

	/*Informacion sobre cada uno de las condiciones de cooperacion de un acuerdo interinstitucional. 
		START_DATE *				Fecha en que comienza la vigencia de la condicion de cooperacion
		END_DATE *					Fecha en que termina la vigencia de la condicion de cooperacion
		EQF_LEVEL_LIST *			Niveles de estudios contemplados en la condicion de cooperacion 
		MOBILITY_NUMBER	*			Numero de participantes maximo que se puede beneficiar de esta condicion de cooperacion
		MOBILITY_TYPE *				Tipo de movilidad, indica si esde estudiantes o profesores y la actividad a desarrollar
		RECEIVING_PARTNER *			Informacion sobre la institucion destino
		SENDING_PARTNER *			Informacion sobre la institucion origen
        SUBJECT_AREA_LANGUAGE_LIST*	Informacion sobre areas de aprendizaje e idiomas relacionados a la condicion de cooperacion
        COP_COND_DURATION			Duracion de la condicion de cooperacion
		OTHER_INFO					Inforamcion adicional sobre la condicion de cooperacion
		BLENDED*					Indica si la condicion de cooperacion permite el formato de movilidad mixta
	*/	
	TYPE COOPERATION_CONDITION IS RECORD
	  (
		START_DATE					DATE,
		END_DATE					DATE,
		EQF_LEVEL_LIST       		PKG_IIAS.EQF_LEVEL_LIST,
		MOBILITY_NUMBER				NUMBER(10,0),
		MOBILITY_TYPE				PKG_IIAS.MOBILITY_TYPE,
		RECEIVING_PARTNER			PKG_IIAS.PARTNER,
		SENDING_PARTNER				PKG_IIAS.PARTNER,
		SUBJECT_AREA_LANGUAGE_LIST	PKG_IIAS.SUBJECT_AREA_LANGUAGE_LIST,
		COP_COND_DURATION			PKG_IIAS.DURATION,
		OTHER_INFO					VARCHAR2(255 CHAR),
		BLENDED						NUMBER(1,0)
	  );

	/* Lista con las condiciones de cooperacion de un acuerdo interinstitucional. */  
	TYPE COOPERATION_CONDITION_LIST IS TABLE OF COOPERATION_CONDITION INDEX BY BINARY_INTEGER ;


	/* Objeto que modela un acuerdo interinstitucional. Campos obligatorios marcados con *.
			START_DATE *					Fecha en que comienza la vigencia del IIA
			END_DATE *						Fecha en que termina la vigencia del IIA
			IIA_CODE						Codigo del IIA
			COOPERATION_CONDITION_LIST * 	Lista de condiciones de coperacion del IIA
			PDF								Documento pdf
	*/
	TYPE IIA IS RECORD
	  (
		START_DATE						DATE,
		END_DATE						DATE,
		IIA_CODE						VARCHAR2(255 CHAR),
		COOPERATION_CONDITION_LIST 		PKG_IIAS.COOPERATION_CONDITION_LIST,
		PDF								BLOB
	  );

	-- FUNCIONES 

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA, P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 
    
     /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
	FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

END PKG_IIAS;
/
create or replace PACKAGE BODY     EWP.PKG_IIAS AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	/*
		Valida las fechas obligatorias de un IIA 
	*/
	FUNCTION VALIDA_FECHAS_IIA(P_IIA IN PKG_IIAS.IIA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_IIA.START_DATE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-START_DATE';
		END IF;

		IF P_IIA.END_DATE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-END_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida mobility type
	*/
	FUNCTION VALIDA_MOBILITY_TYPE(P_MOBILITY_TYPE IN PKG_IIAS.MOBILITY_TYPE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_CATEGORY) NOT IN ('TEACHING', 'STUDIES', 'TRAINING') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY: Valor no permitido (teaching, studies, training)';
		END IF;

		IF P_MOBILITY_TYPE.MOBILITY_GROUP IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_GROUP) NOT IN ('STUDENT', 'STAFF') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP: Valor no permitido (student, staff)';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto Institution
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
		END IF;

		IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ORGANIZATION_UNIT_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto PARTNER de las COOP_COND del IIA
	*/
	FUNCTION VALIDA_PARTNER (P_PARTNER IN PKG_IIAS.PARTNER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_partner VARCHAR2(2000);
	BEGIN
		IF P_PARTNER.INSTITUTION IS NULL THEN
			v_cod_retorno := -1;
			v_mensaje_partner := P_ERROR_MESSAGE || '
				INSTITUTION:';
		ELSIF VALIDA_INSTITUTION(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || v_mensaje_partner;
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida las COOPERATION_CONDITION del IIA
	*/
	FUNCTION VALIDA_COOP_COND_IIA (P_COOP_COND_LIST IN PKG_IIAS.COOPERATION_CONDITION_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_send_partner VARCHAR2(2000);
		v_mensaje_receiv_partner VARCHAR2(2000);
		v_mensaje_mobility_type VARCHAR2(2000);
        v_is_error BOOLEAN;
	BEGIN
		FOR i IN P_COOP_COND_LIST.FIRST .. P_COOP_COND_LIST.LAST 
		LOOP
			IF P_COOP_COND_LIST(i).START_DATE IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-START_DATE [' || i || ']';
            END IF;
			IF P_COOP_COND_LIST(i).END_DATE IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-END_DATE [' || i || ']';
			END IF;
			IF P_COOP_COND_LIST(i).EQF_LEVEL_LIST IS NULL OR P_COOP_COND_LIST(i).EQF_LEVEL_LIST.COUNT = 0 THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-EQF_LEVEL_LIST [' || i || ']';	
			END IF;
            IF P_COOP_COND_LIST(i).MOBILITY_NUMBER IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_NUMBER [' || i || ']';	
			END IF;
			IF P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_TYPE [' || i || ']';
			ELSIF VALIDA_MOBILITY_TYPE(P_COOP_COND_LIST(i).MOBILITY_TYPE, v_mensaje_mobility_type) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					MOBILITY_TYPE [' || i || ']: ' || v_mensaje_mobility_type;
			END IF;	
			IF P_COOP_COND_LIST(i).BLENDED IS NULL THEN 	
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-BLENDED [' || i || ']';
			END IF;	
			IF P_COOP_COND_LIST(i).SENDING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SENDING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).SENDING_PARTNER, v_mensaje_send_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDING_PARTNER [' || i || ']: ' || v_mensaje_send_partner;
			END IF;
			IF P_COOP_COND_LIST(i).RECEIVING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-RECEIVING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).RECEIVING_PARTNER, v_mensaje_receiv_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_PARTNER [' || i || ']: ' || v_mensaje_receiv_partner;
			END IF;	

		END LOOP;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un IIA 
	*/
	FUNCTION VALIDA_IIA(P_IIA IN PKG_IIAS.IIA, ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_fechas VARCHAR2(2000);
		v_mensaje_coop_cond VARCHAR2(2000);
	BEGIN

		IF VALIDA_FECHAS_IIA(P_IIA, v_mensaje_fechas) <> 0 THEN
			v_cod_retorno := -1;
			ERROR_MESSAGE := '
			IIA:' || v_mensaje_fechas;
		END IF;

        IF P_IIA.IIA_CODE IS NULL THEN
            ERROR_MESSAGE := '
			IIA: 
                -IIA_CODE';
        END IF;

		IF P_IIA.COOPERATION_CONDITION_LIST IS NULL OR P_IIA.COOPERATION_CONDITION_LIST.COUNT = 0 THEN 
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:';
		ELSIF VALIDA_COOP_COND_IIA(P_IIA.COOPERATION_CONDITION_LIST, v_mensaje_coop_cond) <> 0 THEN
			v_cod_retorno := -1;
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:' || v_mensaje_coop_cond;
		END IF;


		IF v_cod_retorno <> 0 THEN
			ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/*
		Borra las subject areas asociadas a una condicion de cooperacion
	*/
	PROCEDURE BORRA_COOP_COND_SUBAREA_LANSKI(P_COOP_COND_ID IN VARCHAR2) AS
		CURSOR c_sub_area(p_coop_cond_id IN VARCHAR2) IS 
		SELECT ID, ISCED_CODE
		FROM EWPCV_COOPCOND_SUBAR_LANSKIL 
		WHERE COOPERATION_CONDITION_ID = p_coop_cond_id;
		
	BEGIN
	
		FOR rec IN c_sub_area(P_COOP_COND_ID) 
		LOOP
			DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = rec.ID;
			DELETE EWPCV_SUBJECT_AREA WHERE ID = rec.ISCED_CODE;
		END LOOP;
	END;
	
	/*
		Borra un IIA PARTNER y todos sus contactos asociados.
	*/
	PROCEDURE BORRA_IIA_PARTNER(P_IIA_PARTNER_ID IN VARCHAR2) AS
		v_s_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CONTACTS_ID
			FROM EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = p_id;
		CURSOR c_s(p_id IN VARCHAR2) IS 
			SELECT SIGNER_PERSON_CONTACT_ID
			FROM EWPCV_IIA_PARTNER
			WHERE ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_PARTNER_ID)
		LOOP 
			DELETE FROM EWPCV_IIA_PARTNER_CONTACTS 
				WHERE CONTACTS_ID = REC.CONTACTS_ID
				AND IIA_PARTNER_ID = P_IIA_PARTNER_ID;
			PKG_COMMON.BORRA_CONTACT(rec.CONTACTS_ID);
		END LOOP;

		OPEN c_s(P_IIA_PARTNER_ID);
		FETCH c_s into v_s_id;
		CLOSE c_s;

		DELETE FROM EWPCV_IIA_PARTNER WHERE ID = P_IIA_PARTNER_ID;
		PKG_COMMON.BORRA_CONTACT(v_s_id);

		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_PARTNER_ID;
	END;

	PROCEDURE BORRA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR2) AS 
		v_mn_count NUMBER;
		v_mt_count NUMBER;
		v_md_count NUMBER;
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM EWPCV_COOPERATION_CONDITION 
			WHERE IIA_ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_ID)
		LOOP 
			DELETE FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = rec.ID;
			BORRA_COOP_COND_SUBAREA_LANSKI(rec.ID);
			DELETE FROM EWPCV_COOPERATION_CONDITION WHERE ID = rec.ID;
			DELETE FROM EWPCV_MOBILITY_NUMBER WHERE ID = rec.MOBILITY_NUMBER_ID;

			DELETE FROM EWPCV_DURATION WHERE ID = rec.DURATION_ID;
			BORRA_IIA_PARTNER(rec.RECEIVING_PARTNER_ID);
			BORRA_IIA_PARTNER(rec.SENDING_PARTNER_ID);
		END LOOP;
	END;

	/*
		Borra un IIAS y todas sus condiciones de cooperacion
	*/
	PROCEDURE BORRA_IIA(P_IIA_ID IN VARCHAR2) AS
	BEGIN
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_ID;
	END;


	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_IIA_PARTNER(P_IIA_PARTNER IN PARTNER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255);
	BEGIN
		v_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_IIA_PARTNER.INSTITUTION);
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_IIA_PARTNER.SIGNER_PERSON);
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
				VALUES (v_id, P_IIA_PARTNER.INSTITUTION.INSTITUTION_ID, v_ounit_id, v_c_id, P_IIA_PARTNER.SIGNING_DATE);
		RETURN v_id;
	END;

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_PARTNER_CONTACTS(P_PARTNER_ID IN VARCHAR2, P_PARTNER_CONTACTS EWP.CONTACT_PERSON_LIST) AS 
		v_c_p_id VARCHAR2(255);
	BEGIN
		IF P_PARTNER_CONTACTS IS NOT NULL AND P_PARTNER_CONTACTS.COUNT > 0 THEN 
			FOR i IN P_PARTNER_CONTACTS.FIRST .. P_PARTNER_CONTACTS.LAST
			LOOP
				v_c_p_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_PARTNER_CONTACTS(i));
				INSERT INTO EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (P_PARTNER_ID, v_c_p_id);
			END LOOP;
		END IF;
	END;

	/*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_SUBAREA_LANSKIL(P_COP_COND_ID IN VARCHAR2, P_SUBAREA_LANSKIL_LIST SUBJECT_AREA_LANGUAGE_LIST) AS 
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN
		IF P_SUBAREA_LANSKIL_LIST IS NOT NULL AND P_SUBAREA_LANSKIL_LIST.COUNT > 0 THEN 
			FOR i IN P_SUBAREA_LANSKIL_LIST.FIRST .. P_SUBAREA_LANSKIL_LIST.LAST
			LOOP
			v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_SUBAREA_LANSKIL_LIST(i).SUBJECT_AREA);
			v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_SUBAREA_LANSKIL_LIST(i).LANGUAGE_SKILL);
			INSERT INTO EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(EWP.GENERATE_UUID(), P_COP_COND_ID, v_isced_code, v_lang_skill_id);
			END LOOP;
		END IF;
	END;

    /*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN MOBILITY_TYPE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR, p_group IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER(p_group);
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NOT NULL OR  P_MOBILITY_TYPE.MOBILITY_GROUP IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		END IF;
		RETURN v_id;
	END;


     /*
		Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_DURATION(P_DURATION IN PKG_IIAS.DURATION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (v_id, P_DURATION.NUMBER_DURATION, P_DURATION.UNIT);
		RETURN v_id;
	END;

     /*
		Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_MOBILITY_NUMBER(P_MOB_NUMBER IN VARCHAR) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (v_id, P_MOB_NUMBER);
		RETURN v_id;
	END;

	/*
		Inserta los EQUF LEVELS asociados a una condicion de cooperacion
	*/
	PROCEDURE INSERTA_EQFLEVELS(P_COOP_COND_ID IN VARCHAR2, P_COOP_COND IN COOPERATION_CONDITION) AS
	BEGIN
		IF P_COOP_COND.EQF_LEVEL_LIST IS NOT NULL AND P_COOP_COND.EQF_LEVEL_LIST .COUNT > 0 THEN 
			FOR i IN P_COOP_COND.EQF_LEVEL_LIST .FIRST .. P_COOP_COND.EQF_LEVEL_LIST .LAST
			LOOP
				INSERT INTO EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (P_COOP_COND_ID,P_COOP_COND.EQF_LEVEL_LIST(i));
			END LOOP;
		END IF;
	END;

	/*
		Persiste una lista de condiciones de cooperacion asociadas a un IIAS
	*/
	PROCEDURE INSERTA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR, P_COOP_COND IN COOPERATION_CONDITION_LIST) AS 
		v_s_iia_partner_id VARCHAR2(255);
        v_s_c_p_id VARCHAR2(255);
        v_r_iia_partner_id VARCHAR2(255);
        v_r_c_p_id VARCHAR2(255);
        v_mobility_type_id VARCHAR2(255);
        v_duration_id VARCHAR2(255);
        v_mob_number_id VARCHAR2(255);
		v_cc_id VARCHAR2(255);
	BEGIN 
		FOR i IN P_COOP_COND.FIRST .. P_COOP_COND.LAST 
		LOOP
			v_s_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).SENDING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).SENDING_PARTNER.PARTNER_CONTACTS);

			v_r_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).RECEIVING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).RECEIVING_PARTNER.PARTNER_CONTACTS);   

			v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_COOP_COND(i).MOBILITY_TYPE);
			v_duration_id := INSERTA_DURATION(P_COOP_COND(i).COP_COND_DURATION);
			v_mob_number_id := INSERTA_MOBILITY_NUMBER(P_COOP_COND(i).MOBILITY_NUMBER);

			v_cc_id :=	EWP.GENERATE_UUID();
			INSERT INTO EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO) 
				VALUES (v_cc_id, P_COOP_COND(i).END_DATE, P_COOP_COND(i).START_DATE, v_duration_id,
					v_mob_number_id, v_mobility_type_id, v_r_iia_partner_id, v_s_iia_partner_id, P_IIA_ID, P_COOP_COND(i).OTHER_INFO );

			INSERTA_SUBAREA_LANSKIL(v_cc_id, P_COOP_COND(i).SUBJECT_AREA_LANGUAGE_LIST);
			INSERTA_EQFLEVELS(v_cc_id,P_COOP_COND(i));
		END LOOP;
	END;

	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_IIA(P_IIA IN PKG_IIAS.IIA) RETURN VARCHAR2 AS
		v_iia_id VARCHAR2(255);
	BEGIN

        v_iia_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_IIA (ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE) 
            VALUES (v_iia_id, P_IIA.END_DATE, P_IIA.IIA_CODE, CURRENT_DATE, P_IIA.START_DATE);

		INSERTA_COOP_CONDITIONS(v_iia_id,P_IIA.COOPERATION_CONDITION_LIST);

        RETURN v_iia_id;
	END;


	FUNCTION OBTEN_NOTIFIER_HEI(P_IIA_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS 
		v_notifier_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT DISTINCT(P.INSTITUTION_ID)
			FROM EWPCV_IIA_PARTNER P 
				INNER JOIN EWPCV_COOPERATION_CONDITION CC
					ON CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID
			WHERE CC.IIA_ID = p_id;
	BEGIN 
		OPEN c(P_IIA_ID);
		FETCH c INTO v_notifier_hei;
		WHILE c%FOUND AND UPPER(v_notifier_hei) = UPPER(P_HEI_TO_NOTIFY)
		LOOP
			FETCH c INTO v_notifier_hei;
		END LOOP;
		CLOSE c;
		RETURN v_notifier_hei;
	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	PROCEDURE ACTUALIZA_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA) AS
	BEGIN

 		UPDATE EWPCV_IIA SET START_DATE = P_IIA.START_DATE, END_DATE = P_IIA.END_DATE, IIA_CODE = P_IIA.IIA_CODE, PDF = P_IIA.PDF, MODIFY_DATE = SYSDATE
			WHERE ID = P_IIA_ID;
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		INSERTA_COOP_CONDITIONS(P_IIA_ID, P_IIA.COOPERATION_CONDITION_LIST);

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA,  P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			P_IIA_ID := INSERTA_IIA(P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);

		END IF;
        COMMIT;
		RETURN v_cod_retorno;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END INSERT_IIA; 	

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS			
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count_iia NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			ACTUALIZA_IIA(P_IIA_ID, P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END UPDATE_IIA; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_count_iia NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count_mobility NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_mobility FROM EWPCV_MOBILITY WHERE IIA_ID = P_IIA_ID;
		IF v_count_mobility > 0 THEN
			P_ERROR_MESSAGE := 'Existen movilidadtes asociadas al IIA';
			RETURN -1;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
		BORRA_IIA(P_IIA_ID);
		PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		return 0;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_IIA; 
    
    /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
    FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_count_iia NUMBER := 0;
        v_remote_iia_id VARCHAR2(255);
        v_remote_coop_cond_hash VARCHAR2(255);
        v_remote_iia_code VARCHAR2(255);
        CURSOR c(p_remote_id IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID, IIA.REMOTE_COP_COND_HASH, IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_remote_id;
    BEGIN
        IF P_OWN_IIA_ID IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA propio';
			RETURN -1;
		ELSIF P_ID_FROM_REMOTE_IIA IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA remoto';
			RETURN -1;
		END IF;
        
        SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_OWN_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha infromado como parámetro P_OWN_IIA_ID';
			RETURN -1;
		END IF;
        
        OPEN c(P_ID_FROM_REMOTE_IIA);
		FETCH c INTO v_remote_iia_id, v_remote_coop_cond_hash, v_remote_iia_code;
		CLOSE c;
		IF v_remote_iia_id IS NULL THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
			RETURN -1;
		END IF;
        --Bindeamos el IIA nuestro con el remoto
        UPDATE EWPCV_IIA SET REMOTE_IIA_ID = v_remote_iia_id, REMOTE_COP_COND_HASH = v_remote_coop_cond_hash, REMOTE_IIA_CODE = v_remote_iia_code
            WHERE ID = P_OWN_IIA_ID;
        COMMIT;
        return 0;
        
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END BIND_IIA;
    
    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_remote_iia_id VARCHAR2(255);
        v_notifier_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR c(p_internal_id_iia_copy IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_internal_id_iia_copy;
    BEGIN
        IF P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id interno de la copia del IIA remoto';
			RETURN -1;
		ELSIF P_HEI_ID_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el hei-id de la institución a notificar';
			RETURN -1;
		END IF;
        
        OPEN c(P_INTERNAL_ID_REMOTE_IIA_COPY);
		FETCH c INTO v_remote_iia_id;
		CLOSE c;
        
        v_notifier_hei := OBTEN_NOTIFIER_HEI(P_INTERNAL_ID_REMOTE_IIA_COPY, P_HEI_ID_TO_NOTIFY);
        v_id := EWP.GENERATE_UUID();  
        INSERT INTO EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI) 
						VALUES (v_id, 0, v_remote_iia_id, P_HEI_ID_TO_NOTIFY, SYSDATE, 4, 1, 0, 0, v_notifier_hei);
        COMMIT;
        return 0;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END APPROVE_IIA;
    
END PKG_IIAS;
/
create or replace PACKAGE     EWP.PKG_MOBILITY_LA AS 

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  

	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
	*/	
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_CODE				   VARCHAR2(255 CHAR),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR)
	  );

	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );

	/*Datos del estudiante. Campos obligatorios marcados con *.
			GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
			CONTACT					Informacion del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON
	  );	 

	/* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
			SENDING_INSTITUTION*		Datos de la institución que envia al estudiante.
			RECEIVING_INSTITUTION*		Datos de la institución que recibe al estudiante.
			ACTUAL_ARRIVAL_DATE			Fecha real de llegada del estudiante aldestino.
			ACTUAL_DEPATURE_DATE		Fecha real de salida del estudiante del origen.
			PLANED_ARRIVAL_DATE*		Fecha prevista de llegada del estudiante aldestino.
			PLANED_DEPATURE_DATE*		Fecha prevista de salida del estudiante del origen.
			IIA_CODE*					Codigo del IIA asociado a la movilidad
			COOPERATION_CONDITION_ID*	identificador de la condicion de coperacion asociada la movilidad
			STUDENT*					Datos del estudiante
			EQF_LEVEL*       			Nivel de estudios del estudiante.	
			SUBJECT_AREA*				Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
			STUDENT_LANGUAGE_SKILLS*	Niveles de idiomas que declara el estudiante.
			SENDER_CONTACT*				Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
			SENDER_ADMV_CONTACT			Informacion de contacto de la administrativo en la institucion origen
			RECEIVER_CONTACT*			Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
			RECEIVER_ADMV_CONTACT		Informacion de contacto de la administrativo en la institucion destino
			MOBILITY_TYPE				Tipo de movilidad, puede tomar los siquientes valores:
										Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
			STATUS*						Estado de la nominacion
										0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED, 4- REJECTED;
	*/
	TYPE MOBILITY IS RECORD
	  (
		SENDING_INSTITUTION  		EWP.INSTITUTION,
		RECEIVING_INSTITUTION		EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE			DATE,
		ACTUAL_DEPATURE_DATE		DATE,
		PLANED_ARRIVAL_DATE			DATE,
		PLANED_DEPATURE_DATE		DATE,
		IIA_CODE					VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID	VARCHAR(255 CHAR),
		STUDENT						PKG_MOBILITY_LA.STUDENT,
		EQF_LEVEL       			NUMBER(3,0),	
		SUBJECT_AREA				EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS		EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT				EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT		EWP.CONTACT_PERSON,
		MOBILITY_TYPE				VARCHAR2 (255),
		STATUS						NUMBER(10,0)
	  );

	/* Objeto que modela un acuerdo de enseñanza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					TIMESTAMP,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					BLOB
	  );

	/* Objeto que modela un acuerdo de enseñanza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisión del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );

	-- FUNCIONES 

	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER;  	

END PKG_MOBILITY_LA;
/
create or replace PACKAGE BODY        EWP.PKG_MOBILITY_LA AS 
	
	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	/*
		Valida los campos obligatorios de una subject area: 
			isced_code 
	*/
	FUNCTION VALIDA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_SUBJECT_AREA.ISCED_CODE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-ISCED_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una language skill: 
			lang, cefr_level 
	*/
	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_LANGUAGE_SKILL.LANG	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-LANG';
		END IF;

		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-CEFR_LEVEL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una institucion: 
			Institution_id, organization_unit_code 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
		END IF;

		IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ORGANIZATION_UNIT_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una persona de contacto: 
			GIVEN_NAME, FAMILY_NAME, EMAIL_LIST
	*/
	FUNCTION VALIDA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CONTACT_PERSON.GIVEN_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GIVEN_NAME';
		END IF;

		IF P_CONTACT_PERSON.FAMILY_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-FAMILY_NAME';
		END IF;

		IF P_CONTACT_PERSON.CONTACT.EMAIL IS NULL OR P_CONTACT_PERSON.CONTACT.EMAIL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-CONTACT.EMAIL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un estudiante: 
			global_id
	*/
	FUNCTION VALIDA_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
	BEGIN

		IF P_STUDENT.GLOBAL_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GLOBAL_ID';
		END IF;
		IF VALIDA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_mensaje_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						CONTACT: ' || v_mensaje_contact;
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de una movilidad y de los objetos anidados: 
			planed_arrival_date,planed_depature_date, iia_code, status, sending_institution, receiving_institution, student
	*/
	FUNCTION VALIDA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_s_institution VARCHAR2(2000);
		v_mensaje_r_institution VARCHAR2(2000);
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_contact VARCHAR2(2000);
		v_mensaje_r_contact VARCHAR2(2000);
	BEGIN

		IF P_MOBILITY.PLANED_ARRIVAL_DATE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_ARRIVAL_DATE';
		END IF;

		IF P_MOBILITY.PLANED_DEPATURE_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_DEPATURE_DATE';
		END IF;

		IF P_MOBILITY.EQF_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-EQF_LEVEL';
		END IF;

		IF P_MOBILITY.IIA_CODE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-IIA_CODE';
		END IF;

		IF P_MOBILITY.COOPERATION_CONDITION_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-COOPERATION_CONDITION_ID';
		END IF;

		IF P_MOBILITY.STATUS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-STATUS';
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.SENDING_INSTITUTION, v_mensaje_s_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE|| '
					SENDING_INSTITUTION: ' || v_mensaje_s_institution;
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.RECEIVING_INSTITUTION, v_mensaje_r_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_INSTITUTION: ' || v_mensaje_r_institution;
		END IF;

		IF VALIDA_STUDENT(P_MOBILITY.STUDENT, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					STUDENT: ' || v_mensaje_student;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_mensaje_s_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDER_CONTACT: ' || v_mensaje_s_contact;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_mensaje_r_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVER_CONTACT: ' || v_mensaje_r_contact;
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una firma
			signer_name, signer_position, signer_email, timestamp,  signature
	*/
	FUNCTION VALIDA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_SIGNATURE.SIGNER_NAME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_NAME';
		END IF;

		IF P_SIGNATURE.SIGNER_POSITION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_POSITION';
		END IF;

		IF P_SIGNATURE.SIGNER_EMAIL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_EMAIL';
		END IF;

		IF P_SIGNATURE.SIGN_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGN_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los periodos academicos
			academic_year, institution_id, term_number, total_terms
	*/
	FUNCTION VALIDA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ACADEMIC_YEAR';
		END IF;

		IF P_ACADEMIC_TERM.INSTITUTION_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
		END IF;

		IF P_ACADEMIC_TERM.TERM_NUMBER IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TERM_NUMBER';
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TOTAL_TERMS';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los creditos
			scheme, value
	*/
	FUNCTION VALIDA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CREDIT.SCHEME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-SCHEME';
		END IF;

		IF P_CREDIT.CREDIT_VALUE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-VALUE';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_credit VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP

			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE NOT IN (0,1,2,3,4) THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE (valor erroneo ' || P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
				IF P_COMPONENTS_LIST(i).RECOGNITION_CONDITIONS IS NOT NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-RECOGNITION_CONDITIONS (No informar para el tipo de componente '|| P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')' ;
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;

			IF P_COMPONENTS_LIST(i).TITLE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-TITLE';
			END IF;

			IF P_COMPONENTS_LIST(i).STATUS IS NOT NULL THEN
				IF P_COMPONENTS_LIST(i).STATUS = 0 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (3,4,5) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).STATUS = 1 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (0,1,2) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSE 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-STATUS (valor erroneo'|| P_COMPONENTS_LIST(i).STATUS||')';
				END IF;

				IF P_COMPONENTS_LIST(i).REASON_CODE IS NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-REASON_CODE';
				END IF;
			END IF;

			IF VALIDA_CREDIT(P_COMPONENTS_LIST(i).CREDIT, v_mensaje_credit) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-CREDIT: ' || v_mensaje_credit;
				v_mensaje_credit := '';
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;

		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un Learning Agreement
			mobility, 
	*/
	FUNCTION VALIDA_LEARNING_AGREEMENT(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
	BEGIN

		IF P_LA.MOBILITY_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := '- MOBILITY_ID: ' || P_ERROR_MESSAGE;
		END IF;

		IF P_LA.COMPONENTS IS NULL OR p_LA.COMPONENTS.COUNT = 0 THEN 
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			COMPONENTS';
		ELSIF VALIDA_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				COMPONENTS: ' || v_mensaje_components;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.STUDENT_SIGNATURE, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				STUDENT_SIGNATURE: ' || v_mensaje_student;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE, v_mensaje_s_coord) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				SENDING_HEI_SIGNATURE: ' || v_mensaje_s_coord;
		END IF;		

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/* Borra el estudiante de la tabla mobility participant*/
	PROCEDURE BORRA_STUDENT(P_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255CHAR);
	BEGIN
		SELECT CONTACT_ID INTO v_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		DELETE FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		PKG_COMMON.BORRA_CONTACT(v_contact_id);
	END;

	/*
		Borra creditos asociados a un componente
	*/
	PROCEDURE BORRA_CREDITS(P_COMPONENT_ID IN VARCHAR2) AS
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CREDITS_ID
			FROM EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = p_id;
	BEGIN
		FOR rec IN c(P_COMPONENT_ID) 
		LOOP
			DELETE FROM EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = rec.CREDITS_ID;
			DELETE FROM EWPCV_CREDIT WHERE ID = rec.CREDITS_ID;
		END LOOP;
	END;

	/*
		Borra un componente
	*/
	PROCEDURE BORRA_COMPONENT(P_ID IN VARCHAR2) AS
		v_at_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_TERM_DISPLAY_NAME
			FROM EWPCV_LA_COMPONENT
			WHERE ID = p_id;
	BEGIN
		BORRA_CREDITS(P_ID);
		OPEN c(P_ID);
		FETCH c INTO v_at_id;
		CLOSE c;
		DELETE FROM EWPCV_LA_COMPONENT WHERE ID = P_ID;
		PKG_COMMON.BORRA_ACADEMIC_TERM(v_at_id);
	END;

	/*
		Borra un componente de tipo studied
	*/
	PROCEDURE BORRA_STUDIED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDIED_LA_COMPONENT_ID
			FROM EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = rec.STUDIED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.STUDIED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo recognized
	*/
	PROCEDURE BORRA_RECOGNIZED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = rec.RECOGNIZED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.RECOGNIZED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo virtual
	*/
	PROCEDURE BORRA_VIRTUAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT VIRTUAL_LA_COMPONENT_ID
			FROM EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = rec.VIRTUAL_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.VIRTUAL_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo blended
	*/
	PROCEDURE BORRA_BLENDED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT BLENDED_LA_COMPONENT_ID
			FROM EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = rec.BLENDED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.BLENDED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo doctoral
	*/
	PROCEDURE BORRA_DOCTORAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = rec.DOCTORAL_LA_COMPONENTS_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.DOCTORAL_LA_COMPONENTS_ID);
		END LOOP;
	END;

	/*
		Borra una revision de un LA
	*/
	PROCEDURE BORRA_LA(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		v_s_id VARCHAR2(255 CHAR);
		v_sc_id VARCHAR2(255 CHAR);
		v_rc_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDENT_SIGN, SENDER_COORDINATOR_SIGN, RECEIVER_COORDINATOR_SIGN
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;

	BEGIN

		BORRA_STUDIED_COMP(P_LA_ID, LA_REVISION);
		BORRA_RECOGNIZED_COMP(P_LA_ID, LA_REVISION);
		BORRA_VIRTUAL_COMP(P_LA_ID, LA_REVISION);
		BORRA_BLENDED_COMP(P_LA_ID, LA_REVISION);
		BORRA_DOCTORAL_COMP(P_LA_ID, LA_REVISION);

		OPEN c(P_LA_ID, LA_REVISION);
		FETCH c INTO v_s_id, v_sc_id, v_rc_id;
		CLOSE c;

		DELETE FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_s_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_sc_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_rc_id;
	END;

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/
	PROCEDURE BORRA_MOBILITY_LANSKILL(P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REVISION IN NUMBER) AS
		v_lskill_id VARCHAR2(255 CHAR);
		CURSOR c_lskill(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LANGUAGE_SKILL_ID
			FROM EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR l_rec IN c_lskill(P_MOBILITY_ID, P_MOBILITY_REVISION)
			LOOP
				DELETE FROM EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = l_rec.LANGUAGE_SKILL_ID;
				DELETE FROM EWPCV_LANGUAGE_SKILL WHERE ID = l_rec.LANGUAGE_SKILL_ID;
			END LOOP;	
	END;

	/*
		Borra una movilidad
	*/
	PROCEDURE BORRA_MOBILITY(P_MOBILITY_ID IN VARCHAR2) AS
		v_student_count NUMBER;
		v_mtype_count NUMBER;

		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_REVISION, 
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, ISCED_CODE
			FROM EWPCV_MOBILITY 
			WHERE ID = p_id;

		CURSOR c_la(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
			FROM EWPCV_MOBILITY_LA 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR mobility_rec IN c_mobility(P_MOBILITY_ID)
		LOOP
			BORRA_MOBILITY_LANSKILL(mobility_rec.ID, mobility_rec.MOBILITY_REVISION);

			FOR la_rec IN c_la(mobility_rec.ID, mobility_rec.MOBILITY_REVISION)
			LOOP
				BORRA_LA(la_rec.LEARNING_AGREEMENT_ID, la_rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;	

			DELETE FROM EWPCV_MOBILITY WHERE ID = mobility_rec.ID AND MOBILITY_REVISION = mobility_rec.MOBILITY_REVISION;
			
			DELETE FROM EWPCV_SUBJECT_AREA WHERE ID = mobility_rec.ISCED_CODE;
				
			SELECT COUNT(1) INTO v_student_count FROM EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = mobility_rec.MOBILITY_PARTICIPANT_ID;
			IF v_student_count = 0 THEN 
				BORRA_STUDENT(mobility_rec.MOBILITY_PARTICIPANT_ID);
			END IF;

			SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = mobility_rec.SENDER_CONTACT_ID;
			IF v_scontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = mobility_rec.SENDER_ADMV_CONTACT_ID;
			IF v_sacontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_ADMV_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = mobility_rec.RECEIVER_CONTACT_ID;
			IF v_rcontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = mobility_rec.RECEIVER_ADMV_CONTACT_ID;
			IF v_racontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_ADMV_CONTACT_ID);
			END IF;
		END LOOP;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_PARTICIPANT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT) RETURN VARCHAR2 AS
		v_s_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		CURSOR exist_cursor(p_id IN VARCHAR) IS SELECT ID, CONTACT_ID
			FROM EWPCV_MOBILITY_PARTICIPANT
			WHERE UPPER(ID) = UPPER(p_id);
	BEGIN
		OPEN exist_cursor(P_STUDENT.GLOBAL_ID);
		FETCH exist_cursor INTO v_s_id,v_c_id;
		CLOSE exist_cursor;

		IF v_s_id IS NOT NULL THEN
			UPDATE EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = null WHERE ID = v_s_id;
			PKG_COMMON.BORRA_CONTACT(v_c_id);
			v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON);
			UPDATE EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = v_c_id WHERE ID = v_s_id;
		ELSE
			v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON);
			INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, CONTACT_ID) VALUES (P_STUDENT.GLOBAL_ID, v_c_id);
		END IF;

		RETURN P_STUDENT.GLOBAL_ID;
	END;



	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA');
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE, 'MOBILITY_LA');
		END IF;
		RETURN v_id;
	END;


	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_s_ounit_id VARCHAR2(255);
		v_r_ounit_id VARCHAR2(255);
		v_student_id VARCHAR2(255);
		v_s_contact_id VARCHAR2(255);
		v_s_admv_contact_id VARCHAR2(255);
		v_r_contact_id VARCHAR2(255);
		v_r_admv_contact_id VARCHAR2(255);
		v_mobility_type_id VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN

		v_s_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.SENDING_INSTITUTION);
		v_r_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.RECEIVING_INSTITUTION);
		v_student_id := INSERTA_MOBILITY_PARTICIPANT(P_MOBILITY.STUDENT);
		v_s_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT);
		v_s_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT);
		v_r_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT);
		v_r_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT);

		v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_MOBILITY.SUBJECT_AREA);
		v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY (ID,
		ACTUAL_ARRIVAL_DATE,
		ACTUAL_DEPARTURE_DATE,
		COOPERATION_CONDITION_ID,
		EQF_LEVEL,
		IIA_ID,
		ISCED_CODE,
		MOBILITY_PARTICIPANT_ID,
		MOBILITY_REVISION,
		PLANNED_ARRIVAL_DATE,
		PLANNED_DEPARTURE_DATE,
		RECEIVING_INSTITUTION_ID,
		RECEIVING_ORGANIZATION_UNIT_ID,
		SENDING_INSTITUTION_ID,
		SENDING_ORGANIZATION_UNIT_ID,
		STATUS,
		MOBILITY_TYPE_ID,
		SENDER_CONTACT_ID,
		SENDER_ADMV_CONTACT_ID,
		RECEIVER_CONTACT_ID,
		RECEIVER_ADMV_CONTACT_ID)
		VALUES (v_id,
		P_MOBILITY.ACTUAL_ARRIVAL_DATE,
		P_MOBILITY.ACTUAL_DEPATURE_DATE,
		P_MOBILITY.COOPERATION_CONDITION_ID,
		P_MOBILITY.EQF_LEVEL,
		P_MOBILITY.IIA_CODE,
		v_isced_code,
		v_student_id,
		0,
		P_MOBILITY.PLANED_ARRIVAL_DATE,
		P_MOBILITY.PLANED_DEPATURE_DATE,
		P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID,
		v_r_ounit_id,
		P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID,
		v_s_ounit_id,
		P_MOBILITY.STATUS,
		v_mobility_type_id,
		v_s_contact_id,
		v_s_admv_contact_id,
		v_r_contact_id,
		v_r_admv_contact_id);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT >0 THEN 
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (v_id, 0, v_lang_skill_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (v_id, P_SIGNATURE.SIGNER_NAME, P_SIGNATURE.SIGNER_POSITION , P_SIGNATURE.SIGNER_EMAIL , P_SIGNATURE.SIGN_DATE , P_SIGNATURE.SIGNER_APP, P_SIGNATURE.SIGNATURE);
		RETURN v_id;
	END;

	/*
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_o_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR C(p_start IN VARCHAR2, p_end IN VARCHAR) IS 
			SELECT ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(p_start)
			AND	UPPER(START_YEAR) =  UPPER(p_end);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN
		OPEN C(SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4) ,SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		FETCH C INTO v_a_id;
		CLOSE C;
		IF v_a_id IS NULL THEN 
			v_a_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
				VALUES(v_a_id, SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4),SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		END IF;

		OPEN ounit_cursor(P_ACADEMIC_TERM.INSTITUTION_ID, P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
		FETCH ounit_cursor INTO v_o_id;
		CLOSE ounit_cursor;

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (v_id, P_ACADEMIC_TERM.END_DATE, P_ACADEMIC_TERM.START_DATE , P_ACADEMIC_TERM.INSTITUTION_ID , v_o_id, v_a_id, P_ACADEMIC_TERM.TOTAL_TERMS, P_ACADEMIC_TERM.TERM_NUMBER);

		IF P_ACADEMIC_TERM.DESCRIPTION IS NOT NULL AND P_ACADEMIC_TERM.DESCRIPTION.COUNT > 0 THEN
			FOR i IN P_ACADEMIC_TERM.DESCRIPTION.FIRST .. P_ACADEMIC_TERM.DESCRIPTION.LAST 
			LOOP
				v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ACADEMIC_TERM.DESCRIPTION(i));
				INSERT INTO EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (v_id, v_li_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		INSERT INTO EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
			VALUES (v_id, P_CREDIT.SCHEME, P_CREDIT.CREDIT_VALUE);
		RETURN v_id;
	END;

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_COMPONENTE(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_at_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		v_at_id:= INSERTA_ACADEMIC_TERM(P_COMPONENT.ACADEMIC_TERM);

		--FALTAN LOS Y LOIS

		INSERT INTO EWPCV_LA_COMPONENT (ID,ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (v_id, v_at_id, NULL,P_COMPONENT.LOS_CODE, NULL, P_COMPONENT.STATUS, P_COMPONENT.TITLE, P_COMPONENT.REASON_CODE, P_COMPONENT.REASON_TEXT,
				P_COMPONENT.LA_COMPONENT_TYPE, P_COMPONENT.RECOGNITION_CONDITIONS, P_COMPONENT.SHORT_DESCRIPTION);

		v_c_id := INSERTA_CREDIT(P_COMPONENT.CREDIT);
		INSERT INTO EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (v_id,v_c_id);

		IF P_COMPONENT.LA_COMPONENT_TYPE = 0 THEN
			INSERT INTO EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
			INSERT INTO EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 2 THEN
			INSERT INTO EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 3 THEN
			INSERT INTO EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 4 THEN
			INSERT INTO EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una revision de un learning agreement.
	*/
	Function INSERTA_LA_REVISION(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_LA_REVISION IN NUMBER, P_MOBILITY_REVISION IN NUMBER) RETURN VARCHAR2 AS 
		v_la_id varchar2(255);
		v_stud_sign_id varchar2(255);
		v_s_hei_sign_id varchar2(255);
		v_changes_id varchar2(255);
		v_c_id varchar2(255);
		v_indice NUMBER := 0;
	BEGIN 
		v_stud_sign_id := INSERTA_SIGNATURE(P_LA.STUDENT_SIGNATURE);
		v_s_hei_sign_id :=  INSERTA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE);

		IF P_LA_ID IS NULL THEN 
			v_la_id := EWP.GENERATE_UUID();
		ELSE 
			v_la_id := P_LA_ID;
		END IF;
		v_changes_id := v_la_id || '-' ||P_LA_REVISION;
		INSERT INTO EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (v_la_id,P_LA_REVISION,v_stud_sign_id,v_s_hei_sign_id, 0, SYSDATE, v_changes_id);

		INSERT INTO EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(P_LA.MOBILITY_ID, P_MOBILITY_REVISION, v_la_id, P_LA_REVISION);

		WHILE v_indice < P_LA.COMPONENTS.COUNT 
		LOOP
			v_c_id := INSERTA_COMPONENTE(P_LA.COMPONENTS(v_indice),v_la_id, P_LA_REVISION);
			v_indice := v_indice + 1 ;
		END LOOP;
		RETURN v_la_id;
	END;

	/*
		Genera el tag approve de un mobility update request
	*/
	FUNCTION CREA_TAG_APPROVE(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_approve dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:approve-proposal-v1' );
		l_node_approve := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_approve;
	END;

	/*
		Genera el tag comment de un mobility update request
	*/
	FUNCTION CREA_TAG_COMMENT(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_COMMENT IN VARCHAR2, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_comment dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment-proposal-v1' );
		l_node_comment := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja comment
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_COMMENT);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_comment;
	END;


	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_ACCEPT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE ) AS 
		v_id VARCHAR2(255);
		v_mobility_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_approve dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN

		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_approve := CREA_TAG_APPROVE(P_CHANGES_ID ,v_mobility_id,  P_SIGNATURE, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_approve);

		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 0, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_REJECT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2) AS 
		v_mobility_id VARCHAR2(255);
		v_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_comment dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN
		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_comment := CREA_TAG_COMMENT(P_CHANGES_ID, v_mobility_id, P_SIGNATURE, P_OBSERVATION, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_comment);
		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 1, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************

	PROCEDURE ACTUALIZA_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY) AS 
		v_m_revision VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_mtype_count NUMBER;
		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;

		v_mtype_id VARCHAR2(255);
		v_scontact_id VARCHAR2(255);
		v_sacontact_id VARCHAR2(255);
		v_rcontact_id VARCHAR2(255);
		v_racontact_id VARCHAR2(255);

		v_participant_id VARCHAR2(255);
		v_student_contact_id VARCHAR2(255);

		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT MOBILITY_REVISION, MOBILITY_TYPE_ID,
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, 
				RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID,
				MOBILITY_PARTICIPANT_ID
			FROM EWPCV_MOBILITY
			WHERE ID = P_ID
			ORDER BY MOBILITY_REVISION DESC;
	BEGIN 
		OPEN c_mobility(P_OMOBILITY_ID);
		FETCH c_mobility INTO v_m_revision, v_mtype_id, v_scontact_id, v_sacontact_id, v_rcontact_id, v_racontact_id, v_participant_id;
		CLOSE c_mobility;

		BORRA_MOBILITY_LANSKILL(P_OMOBILITY_ID, v_m_revision);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT > 0 THEN 
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (P_OMOBILITY_ID, v_m_revision, v_lang_skill_id);
			END LOOP;
		END IF;

		UPDATE EWPCV_MOBILITY SET MOBILITY_TYPE_ID = NULL, SENDER_CONTACT_ID = NULL, SENDER_ADMV_CONTACT_ID = NULL,
			RECEIVER_CONTACT_ID = NULL, RECEIVER_ADMV_CONTACT_ID = NULL
			WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

		SELECT CONTACT_ID INTO v_student_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = v_participant_id;
		UPDATE EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = NULL WHERE ID = v_participant_id;
		PKG_COMMON.BORRA_CONTACT(v_participant_id);
		v_student_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.STUDENT.CONTACT_PERSON);
		UPDATE EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = v_student_contact_id WHERE ID = v_participant_id;

		SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = v_scontact_id;
		IF v_scontact_count = 0 THEN 
			PKG_COMMON.BORRA_CONTACT(v_scontact_id);
		END IF;
		SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = v_sacontact_id;
		IF v_sacontact_count = 0 THEN 
			PKG_COMMON.BORRA_CONTACT(v_sacontact_id);
		END IF;
		SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = v_rcontact_id;
		IF v_rcontact_count = 0 THEN 
			PKG_COMMON.BORRA_CONTACT(v_rcontact_id);
		END IF;
		SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = v_racontact_id;
		IF v_racontact_count = 0 THEN 
			PKG_COMMON.BORRA_CONTACT(v_racontact_id);
		END IF;

		v_mtype_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);
		v_scontact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT);
		v_sacontact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT);
		v_rcontact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT);
		v_racontact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT);

	END;

	--  **********************************************
	--	************** NOTIFICACION ******************
	--	**********************************************

	FUNCTION OBTEN_NOTIFIER_HEI_LA(P_LA_ID IN VARCHAR, P_LA_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY M
			INNER JOIN EWPCV_MOBILITY_LA MLA 
				ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = p_id AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN 
		OPEN c(P_LA_ID, P_LA_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF v_r_hei = P_HEI_TO_NOTIFY THEN 
			RETURN v_s_hei;
		ELSE 
			RETURN NULL;
		END IF;
	END;

	FUNCTION OBTEN_NOTIFIER_HEI_MOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN 
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF v_r_hei = P_HEI_TO_NOTIFY THEN 
			RETURN  v_s_hei; 
		ELSE 
			RETURN NULL;
		END IF;
	END;
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_m_revision NUMBER := 0;
		v_la_id VARCHAR2(255);
		CURSOR C(p_id IN VARCHAR2) IS SELECT MAX(MOBILITY_REVISION)
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id;
		CURSOR C_LA(p_id IN VARCHAR2) IS 
		SELECT LEARNING_AGREEMENT_ID 
		FROM EWPCV_MOBILITY_LA 
		WHERE MOBILITY_ID = p_id;

	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		OPEN C(P_LA.MOBILITY_ID);
		FETCH C INTO v_m_revision;
		CLOSE C;
		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada no existe';
			RETURN -1;
		END IF;

		OPEN C_LA(P_LA.MOBILITY_ID);
		FETCH C_LA INTO v_la_id;
		CLOSE C_LA;
		IF v_la_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un Learning Agreement asociado a la mobilidad indicada, por favor actualice o elimine el learning agreement: '||v_la_id;
			RETURN -1;
		END IF;		

		P_LA_ID := INSERTA_LA_REVISION(null, P_LA, 0, v_m_revision);
		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, 0, P_HEI_TO_NOTIFY);
		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_LEARNING_AGREEMENT; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_no_firmado NUMBER:=0;
		v_la_revision NUMBER;
		v_m_revision NUMBER;
		v_la_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2) IS 
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;
		CURSOR c_m_revision(p_mobility_id IN VARCHAR2, p_la_id IN VARCHAR2, p_la_revision IN NUMBER) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_mobility_id
				AND LEARNING_AGREEMENT_ID = p_la_id
				AND LEARNING_AGREEMENT_REVISION = p_la_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		OPEN c_la(P_LA_ID);
		FETCH c_la INTO v_la_revision;
		CLOSE c_la;

		IF v_la_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado no existe';
			RETURN -1;
		END IF;

		Open c_m_revision(P_LA.MOBILITY_ID, P_LA_ID, v_la_revision);
		FETCH c_m_revision INTO v_m_revision;
		CLOSE c_m_revision;

		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada para el learning agreement no es correcta.';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_no_firmado FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND STATUS = 0;
		IF v_no_firmado > 0 THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, v_la_revision, P_HEI_TO_NOTIFY);
		
		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.';
			RETURN -1;
		END IF;
				
		v_la_id := INSERTA_LA_REVISION(P_LA_ID, P_LA, v_la_revision + 1, v_m_revision);
		
		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_LEARNING_AGREEMENT; 	

	/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;


		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;
            
			INSERTA_ACCEPT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE);
			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END ACCEPT_LEARNING_AGREEMENT; 	 

	/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;
            
			INSERTA_REJECT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE, P_OBSERVATION);

			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END REJECT_LEARNING_AGREEMENT; 	

	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER	AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count NUMBER := 0;
		v_m_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS SELECT LEARNING_AGREEMENT_REVISION
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;

		CURSOR c_mob(p_id IN VARCHAR2, p_revision IN NUMBER) IS SELECT MOBILITY_ID
			FROM EWPCV_MOBILITY_LA 
			WHERE LEARNING_AGREEMENT_ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID;
		IF v_count > 0 THEN 
			FOR rec IN c(P_LA_ID) 
			LOOP 
				IF v_notifier_hei IS NULL THEN 
					v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION, P_HEI_TO_NOTIFY);
				END IF;
				
				IF v_notifier_hei IS NULL THEN 
					P_ERROR_MESSAGE := 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.';
					RETURN -1;
				END IF;
				
				IF v_m_id IS NULL THEN 
					OPEN c_mob(P_LA_ID,rec.LEARNING_AGREEMENT_REVISION);
					FETCH c_mob INTO v_m_id;
					CLOSE c_mob;
				END IF;
				
				BORRA_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			PKG_COMMON.INSERTA_NOTIFICATION(v_m_id, 5, P_HEI_TO_NOTIFY, v_notifier_hei);	
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_LEARNING_AGREEMENT; 

	/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
	BEGIN


		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			P_OMOBILITY_ID := INSERTA_MOBILITY(P_MOBILITY);

		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_MOBILITY; 	

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_sending_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR C_SENDING_HEI IS SELECT ID, SENDING_INSTITUTION_ID
        FROM EWPCV_MOBILITY 
        WHERE ID = P_OMOBILITY_ID
        ORDER BY MOBILITY_REVISION DESC;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			
            OPEN C_SENDING_HEI;
            FETCH C_SENDING_HEI INTO v_id, v_sending_hei;
            CLOSE C_SENDING_HEI;

            IF v_id IS NULL THEN 
            	P_ERROR_MESSAGE := 'No existe la mobilidad indicada';
                RETURN -1;
			END IF;
            
			IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                RETURN -1;
			END IF;
            
            ACTUALIZA_MOBILITY(P_OMOBILITY_ID,P_MOBILITY);

			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_sending_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_MOBILITY; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
		v_m_revision NUMBER ;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			
			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;
			
			BORRA_MOBILITY(P_OMOBILITY_ID);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La movilidad no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_MOBILITY;  	



END PKG_MOBILITY_LA;
/
GRANT SELECT ON EWP.EVENT_VIEW TO EWP_APROVISIONAMIENTO;
INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.00.00', SYSDATE, 'UPGRADE_v01.00.00');