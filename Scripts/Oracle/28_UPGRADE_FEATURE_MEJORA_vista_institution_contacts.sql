
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
CREATE OR REPLACE VIEW EWP.INSTITUTION_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                          AS CONTACT_ID,
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    ins.LOGO_URL                                       AS INSTITUTION_LOGO_URL,
    EXTRAE_URLS_CONTACTO(condet.ID) 				   AS CONTACT_URL,
    cont.CONTACT_ROLE                                  AS CONTACT_ROLE,
    EXTRAE_NOMBRES_CONTACTO(cont.ID)				   AS CONTACT_NAME,
    person.FIRST_NAMES                                 AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                   AS CONTACT_LAST_NAME,
    person.GENDER                                      AS CONTACT_GENDER,
    EXTRAE_TELEFONO(condet.ID)                         AS CONTACT_PHONE_NUMBER,
    EXTRAE_FAX(condet.ID)                              AS CONTACT_FAX_NUMBER,
    EXTRAE_EMAILS(condet.ID) 		                   AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                  AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                 AS CONTACT_CITY,
	EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)        AS CONT_STREET_ADDR_LINES,
	EXTRAE_ADDRESS(condet.STREET_ADDRESS)              AS CONTACT_STREET_ADDRESS,
	EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)       AS CONT_MAILING_ADDR_LINES,
	EXTRAE_ADDRESS(condet.MAILING_ADDRESS)             AS CONTACT_MAILING_ADDRESS
    
        FROM EWP.EWPCV_INSTITUTION ins
        
        /*
            Datos de contactos
        */
        LEFT JOIN EWP.EWPCV_CONTACT cont ON cont.institution_id = ins.institution_id
        INNER JOIN EWP.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID 
			AND (ins.primary_contact_detail_id IS NULL OR condet.ID <> ins.primary_contact_detail_id )
        LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address
        LEFT JOIN EWP.EWPCV_PERSON person ON person.id = cont.person_id
         
         /*
            Datos de la institution
        */
        LEFT JOIN EWP.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id;

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '28_UPGRADE_FEATURE_MEJORA_vista_institution_contacts');
COMMIT;
    
    
    
    
    
    
    
    
    
