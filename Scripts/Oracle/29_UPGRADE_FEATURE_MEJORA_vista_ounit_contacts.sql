
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
CREATE OR REPLACE VIEW EWP.OUNIT_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                            AS CONTACT_ID,
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID									 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    EXTRAE_NOMBRES_OUNIT(ounit.ID)                       AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    EXTRAE_URLS_CONTACTO(condet.ID) 				     AS OUNIT_WEBSITE,
    cont.CONTACT_ROLE                                 	 AS CONTACT_ROLE,
    EXTRAE_NOMBRES_CONTACTO(cont.ID)				     AS CONTACT_NAME,
    person.FIRST_NAMES                                   AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                     AS CONTACT_LAST_NAME,
    person.GENDER                                        AS CONTACT_GENDER,
    EXTRAE_TELEFONO(condet.ID)                           AS CONTACT_PHONE_NUMBER,
    EXTRAE_FAX(condet.ID)                                AS CONTACT_FAX_NUMBER,
    EXTRAE_EMAILS(condet.ID) 		                     AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                    AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                   AS CONTACT_CITY,
	EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)          AS CONT_STREET_ADDR_LINES,
	EXTRAE_ADDRESS(condet.STREET_ADDRESS)                AS CONTACT_STREET_ADDRESS,
	EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)         AS CONT_MAILING_ADDR_LINES,
	EXTRAE_ADDRESS(condet.MAILING_ADDRESS)               AS CONTACT_MAILING_ADDRESS,
    ounit.IS_EXPOSED									 AS IS_EXPOSED 
    
		FROM EWP.EWPCV_ORGANIZATION_UNIT ounit
		
		INNER JOIN EWP.EWPCV_INST_ORG_UNIT iou ON ounit.ID = iou.ORGANIZATION_UNITS_ID
		INNER JOIN EWP.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
        
        /*
            Datos de contactos
        */
        LEFT JOIN EWP.EWPCV_CONTACT cont ON cont.organization_unit_id = ounit.id
        INNER JOIN EWP.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ounit.primary_contact_detail_id IS NULL OR condet.ID <> ounit.primary_contact_detail_id ) 
        LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address
        LEFT JOIN EWP.EWPCV_PERSON person ON person.id = cont.person_id
         
         /*
            Datos de la institution
        */
        LEFT JOIN EWP.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.id
        LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id;

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '29_UPGRADE_FEATURE_MEJORA_vista_ounit_contacts');
COMMIT;
    
    
    
    
    
    
    
    
    
