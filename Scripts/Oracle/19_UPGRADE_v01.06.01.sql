
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

	ALTER TABLE EWP.EWPCV_IIA ADD IS_EXPOSED NUMBER(1,0) DEFAULT 1;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
--  ***************************************************
--	************ Funciones para VISTAS  ***************
--	***************************************************
/*
*   Metodo recursivo empleado para la concatenacion hasta el limite de un varchar2
*/
CREATE OR REPLACE FUNCTION EWP.RECURSIVE_NON_OVERFLOW_CONCAT(BASE_PART IN OUT VARCHAR2, APPEND_PART VARCHAR2) RETURN NUMBER IS
    v_legnth number;
    v_ok number;
BEGIN
    IF SUBSTR(BASE_PART, -3) = '...' THEN 
        RETURN 1;
    ELSE
        BASE_PART := CONCAT(BASE_PART, APPEND_PART);
        RETURN 1;
    END IF;
    EXCEPTION
        WHEN OTHERS THEN 
        v_legnth := LENGTH(APPEND_PART);
        IF v_legnth > 1 THEN  
            v_ok := EWP.RECURSIVE_NON_OVERFLOW_CONCAT(BASE_PART, SUBSTR(APPEND_PART, 0, (v_legnth/2)));
            IF v_ok = 1 THEN
                v_ok := EWP.RECURSIVE_NON_OVERFLOW_CONCAT(BASE_PART, SUBSTR(APPEND_PART, (v_legnth/2)+1, v_legnth));
            END IF;
        ELSE
            BASE_PART := REGEXP_REPLACE(BASE_PART, '.{3}$' , '...');
        END IF;
        RETURN -1;
END;
/
/*
*   Concatena sobre la BASE_PART el texto incluido en APPEND_PART hasta el limite de desbordamiento, si se excede se incluiran ... al final
*/
CREATE OR REPLACE PROCEDURE EWP.CONCAT_WITHOUT_OVERFLOW(BASE_PART IN OUT VARCHAR2, APPEND_PART VARCHAR2) IS
v_ok number;
BEGIN
    v_ok := EWP.RECURSIVE_NON_OVERFLOW_CONCAT(BASE_PART, APPEND_PART);
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TO_CHAR(START_DATE, 'YYYY.MM.DD')  || '|:|' || TO_CHAR(END_DATE, 'YYYY.MM.DD') 
		|| '|:|' || INSTITUTION_ID || '|:|' || ORGANIZATION_UNIT_CODE 
		|| '|:|' || TERM_NUMBER || '|:|' || TOTAL_TERMS, '|;|') within group (order by ID)||
			'...'
	else
		listagg(TO_CHAR(START_DATE, 'YYYY.MM.DD')  || '|:|' || TO_CHAR(END_DATE, 'YYYY.MM.DD') 
		|| '|:|' || INSTITUTION_ID || '|:|' || ORGANIZATION_UNIT_CODE 
		|| '|:|' || TERM_NUMBER || '|:|' || TOTAL_TERMS, '|;|') within group (order by ID)
	end aux into v_lista
	from
	(
		select
			ATR.ID AS ID,
			ATR.START_DATE AS START_DATE,
			ATR.END_DATE AS END_DATE,
			ATR.INSTITUTION_ID AS INSTITUTION_ID,
			o.ORGANIZATION_UNIT_CODE AS ORGANIZATION_UNIT_CODE,
			ATR.TERM_NUMBER AS TERM_NUMBER,
			ATR.TOTAL_TERMS AS TOTAL_TERMS,
			sum(length(TO_CHAR(ATR.START_DATE, 'YYYY.MM.DD')  || '|:|' || TO_CHAR(ATR.END_DATE, 'YYYY.MM.DD')  || '|:|' || ATR.INSTITUTION_ID || '|:|' || o.ORGANIZATION_UNIT_CODE  || '|:|' || ATR.TERM_NUMBER || '|:|' || ATR.TOTAL_TERMS) + 3) over (order by ATR.ID) running_length,
			sum(length(TO_CHAR(ATR.START_DATE, 'YYYY.MM.DD')  || '|:|' || TO_CHAR(ATR.END_DATE, 'YYYY.MM.DD') || '|:|' || ATR.INSTITUTION_ID || '|:|' || o.ORGANIZATION_UNIT_CODE || '|:|' || ATR.TERM_NUMBER || '|:|' || ATR.TOTAL_TERMS) + 3) over (order by ATR.ID) total_length
		FROM EWP.EWPCV_ACADEMIC_TERM ATR
			LEFT JOIN EWP.EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
		WHERE ATR.ID = P_AT_ID
		order by ATR.ID
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ACADEMIC_YEAR(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(START_YEAR  || '-' || END_YEAR, '|;|') within group (order by ID)||
			'...'
	else
		listagg(START_YEAR  || '-' || END_YEAR, '|;|') within group (order by ID)
	end aux into v_lista
	from
	(
		select
			ATR.ID AS ID,
			AY.START_YEAR AS START_YEAR,
			AY.END_YEAR AS END_YEAR,
			sum(length(AY.START_YEAR  || '-' || AY.END_YEAR) + 3) over (order by ATR.ID) running_length,
			sum(length(AY.START_YEAR  || '-' || AY.END_YEAR) + 3) over (order by ATR.ID) total_length
		FROM EWP.EWPCV_ACADEMIC_TERM ATR
			LEFT JOIN EWP.EWPCV_ACADEMIC_YEAR AY ON ATR.ACADEMIC_YEAR_ID = AY.ID
		WHERE ATR.ID = P_AT_ID
		order by ATR.ID
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DELIVERY_POINT(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(Delivery_point_code, ',') within group (order by Delivery_point_code)||
			'...'
	else
		listagg(Delivery_point_code, ',') within group (order by Delivery_point_code)
	end aux into v_lista
	from
	(
		select
			Delivery_point_code,
			sum(length(Delivery_point_code) + 1) over (order by Delivery_point_code) running_length,
			sum(length(Delivery_point_code) + 1) over (order by Delivery_point_code) total_length
		FROM EWP.EWPCV_FLEXAD_DELIV_POINT_COD
				WHERE FLEXIBLE_ADDRESS_ID = P_FLEXIBLE_ADDRESS_ID
		order by Delivery_point_code
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(BUILDING_NUMBER|| '|:|' || BUILDING_NAME || '|:|' || STREET_NAME 
				|| '|:|' || UNIT || '|:|' || "FLOOR" || '|:|' || POST_OFFICE_BOX 
				|| '|:|' || DPC, '|;|') within group (order by ID)||
			'...'
	else
		listagg(BUILDING_NUMBER|| '|:|' || BUILDING_NAME || '|:|' || STREET_NAME 
				|| '|:|' || UNIT || '|:|' || "FLOOR" || '|:|' || POST_OFFICE_BOX 
				|| '|:|' || DPC, '|;|') within group (order by ID)
	end aux into v_lista
	from
	(
		select
			F.ID AS ID,
			F.BUILDING_NUMBER AS BUILDING_NUMBER,
			F.BUILDING_NAME AS BUILDING_NAME,
			F.STREET_NAME AS STREET_NAME,
			F.UNIT AS UNIT, 
			F."FLOOR" AS "FLOOR",
			F.POST_OFFICE_BOX AS POST_OFFICE_BOX,
			EXTRAE_DELIVERY_POINT(F.ID) AS DPC,
			sum(length(F.BUILDING_NUMBER|| '|:|' || F.BUILDING_NAME || '|:|' || F.STREET_NAME 
				|| '|:|' || F.UNIT || '|:|' || F."FLOOR" || '|:|' || F.POST_OFFICE_BOX 
				|| '|:|' || EXTRAE_DELIVERY_POINT(F.ID)) + 3) over (order by F.ID) running_length,
			sum(length(F.BUILDING_NUMBER|| '|:|' || F.BUILDING_NAME || '|:|' || F.STREET_NAME 
				|| '|:|' || F.UNIT || '|:|' || F."FLOOR" || '|:|' || F.POST_OFFICE_BOX 
				|| '|:|' || EXTRAE_DELIVERY_POINT(F.ID)) + 3) over (order by F.ID) total_length
		FROM EWP.EWPCV_FLEXIBLE_ADDRESS F
		WHERE F.ID = P_FLEXIBLE_ADDRESS_ID
		order by F.ID 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS_LINES(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ADDRESS_LINE, '|;|') within group (order by ADDRESS_LINE)||
			'...'
	else
		listagg(ADDRESS_LINE, '|;|') within group (order by ADDRESS_LINE)
	end aux into v_lista
	from
	(
		select
			ADDRESS_LINE,
			sum(length(ADDRESS_LINE) + 3) over (order by ADDRESS_LINE) running_length,
			sum(length(ADDRESS_LINE) + 3) over (order by ADDRESS_LINE) total_length
		from EWP.EWPCV_FLEXIBLE_ADDRESS_LINE
		order by ADDRESS_LINE
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_CREDITS(P_COMPONENT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(CREDIT_LEVEL || '|:|' || SCHEME || '|:|' || CREDIT_VALUE, '|;|') within group (order by CREDIT_LEVEL)||
			'...'
	else
		listagg(CREDIT_LEVEL || '|:|' || SCHEME || '|:|' || CREDIT_VALUE, '|;|') within group (order by CREDIT_LEVEL)
	end aux into v_lista
	from
	(
		select
			CR.ID AS ID,
			CR.CREDIT_LEVEL AS CREDIT_LEVEL,
			CR.SCHEME AS SCHEME, 
			CR.CREDIT_VALUE AS CREDIT_VALUE,
			sum(length(CR.CREDIT_LEVEL || '|:|' || CR.SCHEME || '|:|' || CR.CREDIT_VALUE) + 3) over (order by CR.CREDIT_LEVEL) running_length,
			sum(length(CR.CREDIT_LEVEL || '|:|' || CR.SCHEME || '|:|' || CR.CREDIT_VALUE) + 3) over (order by CR.CREDIT_LEVEL) total_length
		FROM EWP.EWPCV_CREDIT CR,
			EWP.EWPCV_COMPONENT_CREDITS CCR
		WHERE CR.ID = CCR.CREDITS_ID
		AND CCR.COMPONENT_ID = P_COMPONENT_ID
		order by CR.CREDIT_LEVEL 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPCIONES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_CONTACT C,
			EWP.EWPCV_CONTACT_DESCRIPTION CD,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE C.ID = CD.CONTACT_ID
			AND CD.DESCRIPTION_ID = LAIT.ID
			AND C.ID = P_CONTACTO_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_DESCRIPTIONS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LOS_DESCRIPTION D,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE D.DESCRIPTION_ID = LAIT.ID
			AND D.LOS_ID = P_LOS_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_EMAILS(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(EMAIL, '|;|') within group (order by EMAIL)||
			'...'
	else
		listagg(EMAIL, '|;|') within group (order by EMAIL)
	end aux into v_lista
	from
	(
		select
			EMAIL,
			sum(length(EMAIL) + 3) over (order by EMAIL) running_length,
			sum(length(EMAIL) + 3) over (order by EMAIL) total_length
		FROM EWP.EWPCV_CONTACT_DETAILS_EMAIL 
		WHERE CONTACT_DETAILS_ID = P_CONTACT_DETAILS_ID
		order by EMAIL 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;

/
create or replace FUNCTION EWP.EXTRAE_EQF_LEVEL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(EQF_LEVEL, '|;|') within group (order by EQF_LEVEL)||
			'...'
	else
		listagg(EQF_LEVEL, '|;|') within group (order by EQF_LEVEL)
	end aux into v_lista
	from
	(
		select
			EQF_LEVEL,
			sum(length(EQF_LEVEL) + 3) over (order by EQF_LEVEL) running_length,
			sum(length(EQF_LEVEL) + 3) over (order by EQF_LEVEL) total_length
		FROM EWP.EWPCV_COOPCOND_EQFLVL
		WHERE COOPERATION_CONDITION_ID = P_COOP_CON_ID
		order by EQF_LEVEL 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;

/
create or replace FUNCTION EWP.EXTRAE_FIRMAS(P_FIRMA_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || "TIMESTAMP" || '|:|' || SIGNER_APP, '|;|') within group (order by SIGNER_NAME)||
			'...'
	else
		listagg(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || "TIMESTAMP" || '|:|' || SIGNER_APP, '|;|') within group (order by SIGNER_NAME)
	end aux into v_lista
	from
	(
		select
			SIGNER_NAME AS SIGNER_NAME,
			SIGNER_POSITION AS SIGNER_POSITION,
			SIGNER_EMAIL AS SIGNER_EMAIL,
			"TIMESTAMP" AS "TIMESTAMP",
			SIGNER_APP AS SIGNER_APP,
			sum(length(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || "TIMESTAMP" || '|:|' || SIGNER_APP) + 3) over (order by SIGNER_NAME) running_length,
			sum(length(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || "TIMESTAMP" || '|:|' || SIGNER_APP) + 3) over (order by SIGNER_NAME) total_length
		FROM EWP.EWPCV_SIGNATURE
		WHERE ID = P_FIRMA_ID
		order by SIGNER_NAME 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NIVELES_IDIOMAS(P_MOBILITY_ID IN VARCHAR2, P_REVISION IN NUMBER) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg("LANGUAGE"|| '|:|' || CEFR_LEVEL , '|;|') within group (order by "LANGUAGE")||
			'...'
	else
		listagg("LANGUAGE"|| '|:|' || CEFR_LEVEL , '|;|') within group (order by "LANGUAGE")
	end aux into v_lista
	from
	(
		select
			S."LANGUAGE" AS "LANGUAGE",
			S.CEFR_LEVEL AS CEFR_LEVEL,
			sum(length(S."LANGUAGE"|| '|:|' || S.CEFR_LEVEL ) + 3) over (ORDER BY S."LANGUAGE") running_length,
			sum(length(S."LANGUAGE"|| '|:|' || S.CEFR_LEVEL ) + 3) over (ORDER BY S."LANGUAGE") total_length
		 FROM EWP.EWPCV_MOBILITY_LANG_SKILL MLS,
			EWP.EWPCV_LANGUAGE_SKILL S
		WHERE MLS.LANGUAGE_SKILL_ID = S.ID
			AND MLS.MOBILITY_ID = P_MOBILITY_ID
			AND MLS.MOBILITY_REVISION = P_REVISION
		ORDER BY S."LANGUAGE" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_ACADEMIC_TERM_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE N.DISP_NAME_ID = LAIT.ID
			AND N.ACADEMIC_TERM_ID = P_AT_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_CONTACT C,
			EWP.EWPCV_CONTACT_NAME CN,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE C.ID = CN.CONTACT_ID
			AND CN.NAME_ID = LAIT.ID
			AND C.ID = P_CONTACTO_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION(P_INSTITUTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		 FROM EWP.EWPCV_INSTITUTION I,
			EWP.EWPCV_INSTITUTION_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE I.ID = N.INSTITUTION_ID
			AND N.NAME_ID = LAIT.ID
			AND I.INSTITUTION_ID = P_INSTITUTION_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LOS_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE N.NAME_ID = LAIT.ID
			AND N.LOS_ID = P_LOS_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_ORGANIZATION_UNIT O,
			EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE O.ID = N.ORGANIZATION_UNIT_ID
			AND N.NAME_ID = LAIT.ID
			AND O.ID = P_OUNIT_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_S_AREA_L_SKILL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION || '|:|' || "LANGUAGE" || '|:|' ||CEFR_LEVEL, '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)||
			'...'
	else
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION || '|:|' || "LANGUAGE" || '|:|' ||CEFR_LEVEL, '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)
	end aux into v_lista
	from
	(
		select
			S.ISCED_CODE AS ISCED_CODE,
			S.ISCED_CLARIFICATION AS ISCED_CLARIFICATION, 
			L."LANGUAGE" AS "LANGUAGE",
			L.CEFR_LEVEL AS CEFR_LEVEL,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION || '|:|' || L."LANGUAGE" || '|:|' ||L.CEFR_LEVEL) + 3) over (order by S.ISCED_CODE) running_length,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION || '|:|' || L."LANGUAGE" || '|:|' ||L.CEFR_LEVEL) + 3) over (order by S.ISCED_CODE) total_length
		FROM EWP.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
			LEFT JOIN EWP.EWPCV_LANGUAGE_SKILL L	
				ON CSL.LANGUAGE_SKILL_ID = L.ID
			LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
				ON CSL.ISCED_CODE = S.ID	
		WHERE CSL.COOPERATION_CONDITION_ID = P_COOP_CON_ID
		order by S.ISCED_CODE
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_S_AREA(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION || '|:|', '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)||
			'...'
	else
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION || '|:|', '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)
	end aux into v_lista
	from
	(
		select
			S.ISCED_CODE AS ISCED_CODE,
			S.ISCED_CLARIFICATION AS ISCED_CLARIFICATION,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION || '|:|') + 3) over (order by S.ISCED_CODE) running_length,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION || '|:|') + 3) over (order by S.ISCED_CODE) total_length
		FROM EWP.EWPCV_COOPCOND_SUBAR CS
			LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
				ON CS.ISCED_CODE = S.ID	
		WHERE CS.COOPERATION_CONDITION_ID = P_COOP_CON_ID
		order by S.ISCED_CODE
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_TELEFONO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(E164 || '|:|' || EXTENSION_NUMBER || '|:|' || OTHER_FORMAT, '|;|') WITHIN GROUP (ORDER BY E164)||
			'...'
	else
		listagg(E164 || '|:|' || EXTENSION_NUMBER || '|:|' || OTHER_FORMAT, '|;|') WITHIN GROUP (ORDER BY E164)
	end aux into v_lista
	from
	(
		select
			P.E164 AS E164,
			P.EXTENSION_NUMBER AS EXTENSION_NUMBER, 
			P.OTHER_FORMAT AS OTHER_FORMAT,
			sum(length(P.E164 || '|:|' || P.EXTENSION_NUMBER || '|:|' || P.OTHER_FORMAT) + 3) over (order by P.E164 ) running_length,
			sum(length(P.E164 || '|:|' || P.EXTENSION_NUMBER || '|:|' || P.OTHER_FORMAT) + 3) over (order by P.E164 ) total_length
		FROM EWP.EWPCV_CONTACT_DETAILS CD,
			EWP.EWPCV_PHONE_NUMBER P
		WHERE P.ID = CD.PHONE_NUMBER
			AND CD.ID = P_CONTACT_DETAILS_ID
		order by P.E164 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_URLS_CONTACTO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_CONTACT_DETAILS CD,
			EWP.EWPCV_CONTACT_URL U,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE CD.ID = U.CONTACT_DETAILS_ID
			AND U.URL_ID = LAIT.ID
			AND CD.ID = P_CONTACT_DETAILS_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_URLS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LOS_URLS U,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE U.URL_ID = LAIT.ID
			AND U.LOS_ID = P_LOS_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION  EWP.GENERATE_UUID RETURN VARCHAR2 AS 
 v_return VARCHAR2(255 CHAR);
BEGIN
	SELECT regexp_replace(rawtohex(sys_guid()), 
    '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})', '\1-\2-\3-\4-\5') 
    INTO v_return FROM DUAL;
    return LOWER(v_return);
END;
/
create or replace FUNCTION EWP.ExtraerAnio(START_DATE TIMESTAMP) RETURN NUMBER AS
	 ANIO NUMBER;
	BEGIN
		ANIO := EXTRACT(YEAR FROM START_DATE);  
		RETURN ANIO;
	END;
/
create or replace FUNCTION EWP.EXTRAE_FAX(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(E164 || '|:|' || EXTENSION_NUMBER || '|:|' || OTHER_FORMAT, '|;|') WITHIN GROUP (ORDER BY E164)||
			'...'
	else
		listagg(E164 || '|:|' || EXTENSION_NUMBER || '|:|' || OTHER_FORMAT, '|;|') WITHIN GROUP (ORDER BY E164)
	end aux into v_lista
	from
	(
		select
			F.E164 AS E164,
			F.EXTENSION_NUMBER AS EXTENSION_NUMBER, 
			F.OTHER_FORMAT AS OTHER_FORMAT,
			sum(length(F.E164 || '|:|' || F.EXTENSION_NUMBER || '|:|' || F.OTHER_FORMAT) + 3) over (order by F.E164 ) running_length,
			sum(length(F.E164 || '|:|' || F.EXTENSION_NUMBER || '|:|' || F.OTHER_FORMAT) + 3) over (order by F.E164 ) total_length
		FROM EWP.EWPCV_CONTACT_DETAILS CD,
			EWP.EWPCV_PHONE_NUMBER F
		WHERE F.ID = CD.FAX_NUMBER
			AND CD.ID = P_CONTACT_DETAILS_ID
		order by F.E164 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_URLS_FACTSHEET(P_FACT_SHEET_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_FACT_SHEET_URL FSU,
			EWP.EWPCV_LANGUAGE_ITEM LAIT
		WHERE FSU.URL_ID = LAIT.ID
			AND FSU.FACT_SHEET_ID = P_FACT_SHEET_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_PHOTO_URL_LIST(P_PHOTO_URL_ID IN VARCHAR2) RETURN VARCHAR2 AS
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(URL|| '|:|' || PHOTO_SIZE || '|:|' || PHOTO_DATE_CHAR || '|:|' || PHOTO_PUBLIC, '|;|') WITHIN GROUP (ORDER BY CONTACT_DETAILS_ID)||
			'...'
	else
		listagg(URL|| '|:|' || PHOTO_SIZE || '|:|' || PHOTO_DATE_CHAR || '|:|' || PHOTO_PUBLIC, '|;|') WITHIN GROUP (ORDER BY CONTACT_DETAILS_ID)
	end aux into v_lista
	from
	(
		select
			CONTACT_DETAILS_ID,
			URL,
			PHOTO_SIZE,
			to_char(PHOTO_DATE, 'YYYY/MM/DD') AS PHOTO_DATE_CHAR,
			PHOTO_PUBLIC,
			sum(length(URL|| '|:|' || PHOTO_SIZE || '|:|' || to_char(PHOTO_DATE, 'YYYY/MM/DD') || '|:|' || PHOTO_PUBLIC) + 3) over (order by CONTACT_DETAILS_ID) running_length,
			sum(length(URL|| '|:|' || PHOTO_SIZE || '|:|' || to_char(PHOTO_DATE, 'YYYY/MM/DD') || '|:|' || PHOTO_PUBLIC) + 3) over (order by CONTACT_DETAILS_ID) total_length
		FROM EWPCV_PHOTO_URL PU
		WHERE CONTACT_DETAILS_ID = P_PHOTO_URL_ID
		order by CONTACT_DETAILS_ID
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_ACADEMIC_YEAR(P_ACADEMIC_YEAR IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_ACADEMIC_YEAR, '^[0-9]{4}/[0-9]{4}$') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Error de formato de academic_year. Formato: xxxx/xxxx, ej: 2020/2021');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_ACADEMIC_YEAR;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_CADENA(P_STRING IN VARCHAR2, P_PATTERN IN VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := -1;
BEGIN
	IF REGEXP_LIKE(P_STRING, P_PATTERN, 'cm') THEN
		v_cod_retorno := 0;
	END IF;

	RETURN v_cod_retorno;
END VALIDA_CADENA;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_CEFR_LEVEL(P_CEFR_LEVEL IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_CEFR_LEVEL, '^[ABC][12]|NS$') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de formato de cefr level. Formato: [ABC][12] o NS, ej: A1, C2, NS');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;

END VALIDA_CEFR_LEVEL;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_COUNTRY_CODE(P_COUNTRY_CODE IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_COUNTRY_CODE, '^[A-Z][A-Z]$') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de formato de country code. Formato: [A-Z][A-Z]');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_COUNTRY_CODE;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_CREDIT_LEVEL(P_CREDIT_LEVEL IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_CREDIT_LEVEL, '^(Bachelor|Master|PhD)$') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de datos de credit level. Valores: Bachelor, Master, PhD');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;

END VALIDA_CREDIT_LEVEL;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_EMAIL(P_EMAIL IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_EMAIL, '[^@]+@[^.]+\..+') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de formato de email. Formato: xxxx@xxxxx.xxx');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_EMAIL;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_EQFLEVEL(P_EQFLEVEL IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF P_EQFLEVEL < 1 AND P_EQFLEVEL > 8 THEN 
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de datos de eqf level. Valores: 1 a 8 ambos incluidos.');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;	
END VALIDA_EQFLEVEL;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_GENDER(P_GENDER IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
	v_valid_gender VARCHAR2(4):= '0129';
BEGIN
	IF INSTR(v_valid_gender, TO_CHAR(P_GENDER)) = 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Género no valido. Valores: 0 - not known; 1 - male; 2 - female; 9 - not applicable');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_GENDER;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_PHONE_NUMBER(P_PHONE_NUMBER IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF EWP.VALIDA_CADENA(P_PHONE_NUMBER, '^\+[0-9]{1,15}$') < 0 THEN
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error de formato de teléfono. Formato: [+][country code][area code][local phone number]');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_PHONE_NUMBER;
/
CREATE OR REPLACE FUNCTION EWP.VALIDA_URL(P_URL IN VARCHAR2, P_ONLY_HTTPS IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
	v_cod_retorno NUMBER(1) := 0;
BEGIN
	IF P_ONLY_HTTPS = 0 THEN
		IF EWP.VALIDA_CADENA(P_URL, '^https?://.+$') < 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'No es una url válida.');
			v_cod_retorno := -1;
		END IF;
	ELSIF P_ONLY_HTTPS = 1 THEN
		IF EWP.VALIDA_CADENA(P_URL, '^https://.+$') < 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'No es una url válida o https.');
			v_cod_retorno := -1;
		END IF;
	ELSE
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'El parámetro P_ONLY_HTTPS sólo acepta el valor 0 y 1');
		v_cod_retorno := -1;
	END IF;
	RETURN v_cod_retorno;
END VALIDA_URL;
/
create or replace FUNCTION EWP.RESET_M_UPDATE_REQUEST(P_IDENTIFICADOR IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_count NUMBER(5);
BEGIN

    SELECT COUNT(1)
    INTO v_count
    FROM EWP.EWPCV_MOBILITY_UPDATE_REQUEST MUR
    WHERE LOWER(MUR.ID)=LOWER(P_IDENTIFICADOR);

    IF v_count > 0
        THEN
        UPDATE EWPCV_MOBILITY_UPDATE_REQUEST SET RETRIES=0,IS_PROCESSING=0 WHERE LOWER(ID) = LOWER(P_IDENTIFICADOR);
        COMMIT;
        RETURN 0;
    ELSE
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,'Error: No se ha encontrado el id en la tabla EWPCV_MOBILITY_UPDATE_REQUEST.');
        RETURN  -1;
    END IF;
END RESET_M_UPDATE_REQUEST;
/
create or replace FUNCTION EWP.RESET_NOTIFICATION(P_IDENTIFICADOR IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_count NUMBER(5);
BEGIN

    SELECT COUNT(1)
    INTO v_count
    FROM EWP.EWPCV_NOTIFICATION NOTI
    WHERE LOWER(NOTI.ID) = LOWER(P_IDENTIFICADOR);

    IF v_count > 0
        THEN
        UPDATE EWPCV_NOTIFICATION SET RETRIES=0,PROCESSING=0 WHERE LOWER(ID) = LOWER(P_IDENTIFICADOR);
        COMMIT;
        RETURN 0;
    ELSE
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Error: No se ha encontrado el id en la tabla EWPCV_NOTIFICATION.');
        RETURN -1;
    END IF;

END RESET_NOTIFICATION;
/
create or replace FUNCTION EWP.EXTRAE_LANGUAGE_ITEM(P_LANG_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.TEXT AS TEXT,
			LAIT."LANG" AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_LANGUAGE_ITEM LAIT
        WHERE ID = P_LANG_ID
		order by LAIT."LANG" 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_LEVEL_DESC(P_LEVEL_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT || '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			li.text AS TEXT,
			li.lang AS "LANG",
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) running_length,
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) total_length
		FROM  EWP.EWPCV_LEVEL_DESCRIPTION ld 
				INNER JOIN EWP.EWPCV_LANGUAGE_ITEM li ON ld.level_description_id = li.id 
            WHERE ld.LEVEL_ID = P_LEVEL_ID
		order by li.lang
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_LEVELS(P_LOI_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(level_type || '|::|' || level_value || '|::|' || level_description, '|;;|') within group (order by level_type)||
			'...'
	else
		listagg(level_type || '|::|' || level_value || '|::|' || level_description, '|;;|') within group (order by level_type)
	end aux into v_lista
	from
	(
		select
			lvl.level_type AS level_type,
			lvl.level_value AS level_value,
			EXTRAE_LEVEL_DESC(lvl.ID) AS level_description,
			sum(length(lvl.level_type || '|::|' || lvl.level_value || '|::|' || EXTRAE_LEVEL_DESC(lvl.ID)) + 4) over (order by lvl.level_type) running_length,
			sum(length(lvl.level_type || '|::|' || lvl.level_value || '|::|' || EXTRAE_LEVEL_DESC(lvl.ID)) + 4) over (order by lvl.level_type) total_length
		FROM EWP.EWPCV_LEVEL lvl 
            INNER JOIN EWP.EWPCV_LEVELS lvls ON lvl.id = lvls.level_id
        WHERE lvls.LOI_ID = P_LOI_ID
		order by lvl.level_type 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_RES_DIST(P_RESULT_DIST_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(distrubtion_count || '|:|' || label, '|;|') within group (order by label)||
			'...'
	else
		listagg(distrubtion_count || '|:|' || label, '|;|') within group (order by label)
	end aux into v_lista
	from
	(
		select
			distrubtion_count,
			label,
			sum(length(distrubtion_count || '|:|' || label) + 3) over (order by label) running_length,
			sum(length(distrubtion_count || '|:|' || label) + 3) over (order by label) total_length
		FROM EWP.EWPCV_RESULT_DIST_CATEGORY 
			WHERE RESULT_DIST_ID = P_RESULT_DIST_ID
		order by label 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_GRAD_SCHEME_DESC(P_GRADING_SCHEME_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			lid.text AS TEXT,
			lid.lang AS "LANG",
			sum(length(lid.text || '|:|' || lid.lang) + 3) over (order by lid.lang) running_length,
			sum(length(lid.text || '|:|' || lid.lang) + 3) over (order by lid.lang) total_length
		FROM EWP.EWPCV_GRADING_SCHEME_DESC gsd
			LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM lid ON gsd.description_id = lid.id 
		WHERE gsd.GRADING_SCHEME_ID = P_GRADING_SCHEME_ID
		order by lid.lang 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_GRAD_SCHEME_LABEL(P_GRADING_SCHEME_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			lig.text AS TEXT,
			lig.lang AS "LANG",
			sum(length(lig.text || '|:|' || lig.lang) + 3) over (order by lig.lang) running_length,
			sum(length(lig.text || '|:|' || lig.lang) + 3) over (order by lig.lang) total_length
		FROM EWP.EWPCV_GRADING_SCHEME_LABEL gsl 
			LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM lig ON gsl.label_id = lig.id 
        WHERE gsl.GRADING_SCHEME_ID = P_GRADING_SCHEME_ID
		order by lig.lang 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_GROUP_TYPE(P_LOI_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			li.text AS TEXT,
			li.lang AS "LANG",
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) running_length,
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) total_length
		FROM EWP.EWPCV_LOI_GROUPING lg
            INNER JOIN EWP.EWPCV_GROUP_TYPE gt ON lg.GROUP_TYPE_ID = gt.ID
            INNER JOIN EWP.EWPCV_LANGUAGE_ITEM li ON gt.title = li.id
        WHERE lg.LOI_ID = P_LOI_ID
		order by li.lang 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_GROUP(P_LOI_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			li.text AS TEXT,
			li.lang AS "LANG",
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) running_length,
			sum(length(li.text || '|:|' || li.lang) + 3) over (order by li.lang) total_length
		FROM EWPCV_LOI_GROUPING lg
			INNER JOIN EWPCV_GROUP g ON lg.GROUP_ID = g.ID
			INNER JOIN EWPCV_LANGUAGE_ITEM li ON g.title = li.id
		WHERE lg.loi_id = P_LOI_ID
		order by li.lang 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_GRADE_FREC(P_ISCED_TABLE_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(label|| '|:|' || percentage, '|;|') within group (order by label)||
			'...'
	else
		listagg(label|| '|:|' || percentage, '|;|') within group (order by label)
	end aux into v_lista
	from
	(
		select
			g.label AS label,
			g.percentage AS percentage,
			sum(length(g.label || '|:|' || g.percentage) + 3) over (order by g.label) running_length,
			sum(length(g.label || '|:|' || g.percentage) + 3) over (order by g.label) total_length
		FROM  EWP.EWPCV_GRADE_FREQUENCY g WHERE g.isced_table_id = P_ISCED_TABLE_ID
		order by g.label
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_ISCED_GRADE(P_TOR_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ISCED_CODE || '|::|' || GRADE_FRECUENCY, '|;;|') within group (order by ISCED_CODE)||
			'...'
	else
		listagg(ISCED_CODE || '|::|' || GRADE_FRECUENCY, '|;;|') within group (order by ISCED_CODE)
	end aux into v_lista
	from
	(
		select
			i.isced_code AS ISCED_CODE,
			EXTRAE_GRADE_FREC(i.ID) AS GRADE_FRECUENCY,
			sum(length(i.isced_code || '|::|' || EXTRAE_GRADE_FREC(i.ID)) + 4) over (order by i.isced_code) running_length,
			sum(length(i.isced_code || '|::|' || EXTRAE_GRADE_FREC(i.ID)) + 4) over (order by i.isced_code) total_length
		FROM EWP.EWPCV_ISCED_TABLE i 
        WHERE i.TOR_ID = P_TOR_ID
		order by i.isced_code 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_ATTACH_TITLE(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.text AS TEXT,
			LAIT.lang AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_ATTACHMENT_TITLE ATTCH_TIT INNER JOIN
		EWP.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_TIT.ID = LAIT.ID
        WHERE ATTCH_TIT.ATTACHMENT_ID=P_AT_ID
		order by LAIT."LANG"
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_ATTACH_DESC(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")||
			'...'
	else
		listagg(TEXT|| '|:|' || "LANG", '|;|') within group (order by "LANG")
	end aux into v_lista
	from
	(
		select
			LAIT.text AS TEXT,
			LAIT.lang AS "LANG",
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") running_length,
			sum(length(LAIT.TEXT || '|:|' || LAIT."LANG") + 3) over (order by LAIT."LANG") total_length
		FROM EWP.EWPCV_ATTACHMENT_DESCRIPTION ATTCH_DESC INNER JOIN
		EWP.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_DESC.ID = LAIT.ID
        WHERE ATTCH_DESC.ATTACHMENT_ID=P_AT_ID
		order by LAIT."LANG"
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_ADDIT_INFO(P_SECTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ADDITIONAL_INFO, '|;|') within group (order by ADDITIONAL_INFO)||
			'...'
	else
		listagg(ADDITIONAL_INFO, '|;|') within group (order by ADDITIONAL_INFO)
	end aux into v_lista
	from
	(
		select
			add_inf.ADDITIONAL_INFO AS ADDITIONAL_INFO ,
			sum(length(add_inf.ADDITIONAL_INFO) + 3) over (order by add_inf.ADDITIONAL_INFO) running_length,
			sum(length(add_inf.ADDITIONAL_INFO) + 3) over (order by add_inf.ADDITIONAL_INFO) total_length
		FROM EWP.EWPCV_ADDITIONAL_INFO add_inf
        WHERE add_inf.SECTION_ID=P_SECTION_ID
		order by add_inf.ADDITIONAL_INFO
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.NOTIFICA_ELEMENTO(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_count NUMBER := 0;
	v_is_exposed number(1,0);
	CURSOR c(p_id IN VARCHAR2) IS
		SELECT IS_EXPOSED
		FROM EWPCV_IIA
		WHERE LOWER(ID) = LOWER(p_id);
BEGIN
    IF P_ELEMENT_ID IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message,'Dede indicar el identificador del elemento a notificar');
        RETURN -1;
    END IF; 
    IF P_TYPE IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'Dede indicar el tipo de elemento a notificar');
        RETURN -1;
    END IF; 
    IF P_NOTIFY_HEI IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'Dede indicar la institucion a notificar');
        RETURN -1;
    END IF; 
    IF P_NOTIFIER_HEI IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'Dede indicar la institucion que envia la notificacion');
        RETURN -1;
    END IF; 
    
    IF P_TYPE = 0 THEN -- IIA
        --para saber si hay que exponer un autogenerado que se confirma
        OPEN c(P_ELEMENT_ID);
        FETCH c INTO v_is_exposed;
        close c;
        SELECT COUNT(1) INTO v_count FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(P_ELEMENT_ID) AND IS_REMOTE = 0;
        IF v_count = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'El IIA a notificar no existe o no es local');
            RETURN -1;
        END IF;
    ELSIF P_TYPE = 1 THEN -- OMOBILITY
        SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(P_ELEMENT_ID);
        IF v_count = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'La omobility a notificar no existe');
            RETURN -1;
        END IF;
    ELSIF P_TYPE = 2 THEN --IMOBILITY
        SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(P_ELEMENT_ID);
        IF v_count = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'La imobility a notificar no existe');
            RETURN -1;
        END IF;
    ELSIF P_TYPE = 3 THEN -- TOR
        SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.ID
        INNER JOIN EWPCV_LEARNING_AGREEMENT LA ON ML.LEARNING_AGREEMENT_ID = LA.ID 
        INNER JOIN EWPCV_TOR T ON LA.TOR_ID = T.ID
        WHERE LOWER(M.ID) = LOWER(P_ELEMENT_ID);
        IF v_count = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'El TOR a notificar no existe');
            RETURN -1;
        END IF;
    ELSIF P_TYPE = 4 THEN --IIA APPROVAL
		EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'No se permite la generacion de notificaciones de tipo IIA Approval utiliza el metodo de aprobacion de IIAs en su lugar');
		RETURN -1;
    ELSIF P_TYPE = 5 THEN --LA
        SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.ID
        WHERE LOWER(M.ID) = LOWER(P_ELEMENT_ID);
        IF v_count = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'El Learning Agreement a notificar aprobacion no existe');
            RETURN -1;
        END IF;
    ELSE 
        EWP.CONCAT_WITHOUT_OVERFLOW(p_error_message, 'El tipo de elemento indicado no admite CNR');
        RETURN -1;
    END IF;
    	
    PKG_COMMON.INSERTA_NOTIFICATION(P_ELEMENT_ID, P_TYPE, P_NOTIFY_HEI, P_NOTIFIER_HEI);
	IF v_is_exposed IS NOT NULL AND v_is_exposed = 0 THEN
		UPDATE EWPCV_IIA SET IS_EXPOSED = 1 WHERE ID = P_ELEMENT_ID;
	END IF;
	
    COMMIT;
    RETURN 0;
END;
/
CREATE OR REPLACE FUNCTION EWP.CLOB_TO_BLOB(p_clob CLOB) RETURN BLOB IS
 l_blob BLOB;
 l_raw RAW(32767);
 l_amt NUMBER := 7700;
 l_offset NUMBER := 1;
 l_temp VARCHAR2(32767);
 BEGIN
     BEGIN
        DBMS_LOB.createtemporary (l_blob, true);
     LOOP
         DBMS_LOB.read(p_clob, l_amt, l_offset, l_temp);
         l_offset := l_offset + l_amt;
         l_raw := UTL_ENCODE.base64_decode(UTL_RAW.cast_to_raw(l_temp));
         DBMS_LOB.append (l_blob, TO_BLOB(l_raw));
     END LOOP;
     EXCEPTION
         WHEN NO_DATA_FOUND THEN
         NULL;
     END;
     RETURN l_blob;
 END;
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/


/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***PKG_COMMON
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_COMMON AS 

	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER;

	FUNCTION VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER;

	FUNCTION VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;
	
	FUNCTION VALIDA_DATOS_C_PERSON_LIST(P_CONTACT_PERSON_LIST IN EWP.CONTACT_PERSON_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;
	
	/* 
		Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
	*/
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2);

	/* 
		Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
	*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2);

    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2);

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2);

    /*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta datos de persona independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2;

	/*
		Inserta un telefono de contacto independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2;

    /*
		Inserta una direccion independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2;

    /*
		Inserta datos de contacto
	*/
    FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2;

    /*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2;    

    /*
		Inserta un nivel de idioma independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2;

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) ;

    /*
        Utilidad para pintar clobs por consola
    */
    procedure print_clob( p_clob in clob );


END PKG_COMMON;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_COMMON AS 
	
	-------------------------------------------------------------
	-----------------------VALIDACIONES--------------------------
	-------------------------------------------------------------

	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL AND EWP.VALIDA_CEFR_LEVEL(P_LANGUAGE_SKILL.CEFR_LEVEL, v_error_message) <> 0 THEN 
		v_cod_retorno := -1;
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
								-CEFR_LEVEL: ');
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(2000);
		v_error_message_country VARCHAR2(2000);
		v_error_message_phone VARCHAR2(2000);
	BEGIN
		IF P_CONTACT IS NOT NULL THEN
			IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN 
				FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST LOOP 
					IF EWP.VALIDA_URL(P_CONTACT.CONTACT_URL(i).TEXT, 0,v_error_message_url) <> 0 THEN
						v_cod_retorno := -1;
						
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-URL('|| i|| '): ');
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
						v_error_message_url := '';
					END IF;
				END LOOP;
			END IF;

			IF P_CONTACT.STREET_ADDRESS IS NOT NULL AND P_CONTACT.STREET_ADDRESS.COUNTRY IS NOT NULL THEN 
				IF EWP.VALIDA_COUNTRY_CODE(P_CONTACT.STREET_ADDRESS.COUNTRY, v_error_message_country) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-STREET_ADDRESS:' || ' 
								-COUNTRY: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_country);
					v_error_message_country := '';
				END IF;
			END IF;

			IF P_CONTACT.MAILING_ADDRESS IS NOT NULL AND P_CONTACT.MAILING_ADDRESS.COUNTRY IS NOT NULL THEN 
				IF EWP.VALIDA_COUNTRY_CODE(P_CONTACT.MAILING_ADDRESS.COUNTRY, v_error_message_country) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-MAILING_ADDRESS:' || '
								-COUNTRY: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_country);
					v_error_message_country := '';
				END IF;
			END IF;

			IF P_CONTACT.PHONE IS NOT NULL AND P_CONTACT.PHONE.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_CONTACT.PHONE.E164, v_error_message_phone) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-PHONE: ' || '
								-E164: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
				v_error_message_phone := '';
			END IF;

			IF P_CONTACT.FAX IS NOT NULL AND P_CONTACT.FAX.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_CONTACT.FAX.E164, v_error_message_phone) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-FAX:' || '
								-E164: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
				v_error_message_phone := '';
			END IF;
	
			IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN 
				FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST LOOP 
					IF EWP.VALIDA_EMAIL(P_CONTACT.EMAIL(i), v_error_message_phone) <> 0 THEN
						v_cod_retorno := -1;
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-EMAIL('|| i|| '): ');
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_phone);
						v_error_message_phone := '';
					END IF;
				END LOOP;
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_CONTACT_PERSON IS NOT NULL THEN
			IF P_CONTACT_PERSON.CONTACT IS NOT NULL AND VALIDA_DATOS_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-CONTACT: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;

			IF P_CONTACT_PERSON.GENDER IS NOT NULL AND EWP.VALIDA_GENDER(P_CONTACT_PERSON.GENDER, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-GENDER: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;

			IF P_CONTACT_PERSON.CITIZENSHIP IS NOT NULL AND EWP.VALIDA_COUNTRY_CODE(P_CONTACT_PERSON.CITIZENSHIP, v_error_message) <> 0 THEN 
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
							-CITIZENSHIP: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_C_PERSON_LIST(P_CONTACT_PERSON_LIST IN EWP.CONTACT_PERSON_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_CONTACT_PERSON_LIST IS NOT NULL AND P_CONTACT_PERSON_LIST.COUNT > 0 THEN 
			FOR i IN P_CONTACT_PERSON_LIST.FIRST .. P_CONTACT_PERSON_LIST.LAST LOOP 
				IF VALIDA_DATOS_CONTACT_PERSON(P_CONTACT_PERSON_LIST(i), v_error_message) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
					-CONTACT_PERSON_LIST('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
				END IF;
			END LOOP;
		END IF;
		RETURN v_cod_retorno;
	END;
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	
	/* Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) */
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2) AS
		v_phone_id VARCHAR2(255 CHAR);
        v_fax_id VARCHAR2(255 CHAR);
		v_mailing_id VARCHAR2(255 CHAR);
		v_street_id VARCHAR2(255 CHAR);
		CURSOR c_contact_detail(p_contact_id IN VARCHAR2) IS 
			SELECT PHONE_NUMBER, MAILING_ADDRESS, STREET_ADDRESS, FAX_NUMBER
			FROM EWPCV_CONTACT_DETAILS
			WHERE ID = p_contact_id;
		CURSOR c_urls(p_contact_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_CONTACT_URL
			WHERE CONTACT_DETAILS_ID = p_contact_id;
	BEGIN
		OPEN c_contact_detail(P_ID);
		FETCH c_contact_detail INTO v_phone_id, v_mailing_id, v_street_id, v_fax_id;
		CLOSE c_contact_detail;

		FOR url_rec IN c_urls(P_ID)
		LOOP 
			DELETE FROM EWPCV_CONTACT_URL WHERE URL_ID = url_rec.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = url_rec.URL_ID;
		END LOOP;
		DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = P_ID;

		DELETE FROM EWPCV_CONTACT_DETAILS WHERE ID = P_ID;

		DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_phone_id;

        DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_fax_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_mailing_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_street_id;

		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_street_id;
	END;

	/* Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2) AS
		v_person_id VARCHAR2(255 CHAR);
		v_details_id VARCHAR2(255 CHAR);
		CURSOR c_contact(p_contact_id IN VARCHAR2) IS 
			SELECT PERSON_ID, CONTACT_DETAILS_ID
			FROM EWPCV_CONTACT
			WHERE ID = p_contact_id;
		CURSOR c_names(p_contact_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = p_contact_id;
		CURSOR c_descs(p_contact_id IN VARCHAR2) IS 
			SELECT DESCRIPTION_ID
			FROM EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = p_contact_id;
	BEGIN
		IF P_ID IS NOT NULL THEN 
			OPEN c_contact(P_ID);
			FETCH c_contact INTO v_person_id, v_details_id;
			CLOSE c_contact;
			FOR name_rec IN c_names(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_NAME WHERE NAME_ID = name_rec.NAME_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = name_rec.NAME_ID;
			END LOOP;
			FOR desc_rec IN c_descs(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = desc_rec.DESCRIPTION_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = desc_rec.DESCRIPTION_ID;
			END LOOP;
			DELETE FROM EWPCV_CONTACT WHERE ID = P_ID;
			BORRA_CONTACT_DETAILS(v_details_id);
			DELETE FROM EWPCV_PERSON WHERE ID = v_person_id;
		END IF;
	END;

    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2) AS
        CURSOR c_fs_url(p_fs_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_FACT_SHEET_URL
			WHERE FACT_SHEET_ID = p_fs_id;
    BEGIN
        FOR fact_sheet_url IN c_fs_url(P_FACTSHEET_ID)
		LOOP 
			DELETE FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = fact_sheet_url.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = fact_sheet_url.URL_ID;
		END LOOP; 
    END;

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2) AS
		v_ay_id VARCHAR2(255 CHAR);
		v_ay_count NUMBER;
		CURSOR c_ay(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_YEAR_ID
			FROM EWPCV_ACADEMIC_TERM
			WHERE ID = p_id;

		CURSOR c_n(p_id IN VARCHAR2) IS 
			SELECT DISP_NAME_ID
			FROM EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = p_id;
	BEGIN

		FOR rec IN c_n(P_ID) 
		LOOP
			DELETE FROM EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = rec.DISP_NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.DISP_NAME_ID;
		END LOOP;

		OPEN c_ay(P_ID);
		FETCH c_ay INTO v_ay_id;
		CLOSE c_ay;
		DELETE FROM EWPCV_ACADEMIC_TERM WHERE ID = P_ID;
		SELECT COUNT(1) INTO v_ay_count FROM EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = v_ay_id;
		IF v_ay_count = 0 THEN 
			DELETE FROM EWPCV_ACADEMIC_YEAR WHERE ID = v_ay_id;
		END IF;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_LANGUAGE_ITEM.LANG IS NOT NULL OR  P_LANGUAGE_ITEM.TEXT IS NOT NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (v_id, P_LANGUAGE_ITEM.LANG, P_LANGUAGE_ITEM.TEXT); 
		END IF;

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_inst_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_INSTITUTION_NAME INA ON LAIT.ID = INA.NAME_ID 
			WHERE INA.INSTITUTION_ID = p_inst_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_INSTITUTION_NAMES IS NOT NULL AND P_INSTITUTION_NAMES.COUNT > 0 THEN
			FOR i IN P_INSTITUTION_NAMES.FIRST .. P_INSTITUTION_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_INSTITUTION_ID, P_INSTITUTION_NAMES(i).LANG, P_INSTITUTION_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION_NAMES(i));
                    INSERT INTO EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES (v_lang_item_id, P_INSTITUTION_ID);
				END IF; 
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_INSTITUTION 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_id);
	BEGIN

		OPEN exist_cursor(P_INSTITUTION_ID) ;
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID) VALUES (v_id, LOWER(P_INSTITUTION_ID), P_ABREVIATION, P_LOGO_URL, P_FACTSHEET_ID, P_PRIM_CONT_DETAIL_ID); 
		ELSIF P_ABREVIATION IS NOT NULL OR P_LOGO_URL IS NOT NULL OR P_FACTSHEET_ID IS NOT NULL THEN
			UPDATE  EWPCV_INSTITUTION SET  ABBREVIATION = P_ABREVIATION, LOGO_URL = P_LOGO_URL, FACT_SHEET = P_FACTSHEET_ID WHERE ID = v_id; 
		END IF;

		INSERTA_INSTITUTION_NAMES(v_id, P_INSTITUTION_NAMES);

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_ounit_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_ORGANIZATION_UNIT_NAME ONA ON LAIT.ID = ONA.NAME_ID 
			WHERE ONA.ORGANIZATION_UNIT_ID = p_ounit_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_ORGANIZATION_UNIT_NAMES IS NOT NULL AND P_ORGANIZATION_UNIT_NAMES.COUNT > 0 THEN
			FOR i IN P_ORGANIZATION_UNIT_NAMES.FIRST .. P_ORGANIZATION_UNIT_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_OUNIT_ID, P_ORGANIZATION_UNIT_NAMES(i).LANG,P_ORGANIZATION_UNIT_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := INSERTA_LANGUAGE_ITEM(P_ORGANIZATION_UNIT_NAMES(i));
					INSERT INTO EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES (v_lang_item_id, P_OUNIT_ID);
				END IF;       
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_hei_id IN VARCHAR, p_code IN VARCHAR) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_code)
			AND UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id);

	BEGIN
	    IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			OPEN exist_cursor(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ORGANIZATION_UNIT_CODE);
			FETCH exist_cursor INTO v_id;
			CLOSE exist_cursor;

			IF  v_id IS NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (v_id, P_INSTITUTION.ORGANIZATION_UNIT_CODE); 
			END IF;

			INSERTA_OUNIT_NAMES(v_id, P_INSTITUTION.ORGANIZATION_UNIT_NAME);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
        v_inst_id VARCHAR2(255);
        v_ou_id VARCHAR2(255);
        v_count VARCHAR2(255);
    BEGIN
        v_inst_id := INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, null, null, null, P_INSTITUTION.INSTITUTION_NAME, null);
        IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            v_ou_id := INSERTA_OUNIT(P_INSTITUTION);
            SELECT COUNT(1) INTO v_count FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = v_ou_id;
            IF v_count = 0 THEN 
                INSERT INTO EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (v_inst_id, v_ou_id);
            END IF;
        END IF;
        RETURN v_ou_id;



    END;

	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PERSON.GIVEN_NAME IS NOT NULL 
			OR P_PERSON.FAMILY_NAME IS NOT NULL 
			OR P_PERSON.BIRTH_DATE IS NOT NULL 
			OR P_PERSON.CITIZENSHIP IS NOT NULL 
			OR P_PERSON.GENDER IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
					VALUES (v_id, P_PERSON.GIVEN_NAME, P_PERSON.FAMILY_NAME, UPPER(P_PERSON.CITIZENSHIP), P_PERSON.BIRTH_DATE, P_PERSON.GENDER);
		END IF;
		return v_id;
	END;

	/*
		Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PHONE.E164 IS NOT NULL 
			OR P_PHONE.EXTENSION_NUMBER IS NOT NULL 
			OR P_PHONE.OTHER_FORMAT IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
					VALUES (v_id, P_PHONE.E164, P_PHONE.EXTENSION_NUMBER, P_PHONE.OTHER_FORMAT);
		END IF;
		return v_id;
	END;

	/*
		Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_ADDRESS.POSTAL_CODE IS NOT NULL 
			OR P_ADDRESS.LOCALITY IS NOT NULL 
			OR P_ADDRESS.REGION IS NOT NULL
			OR P_ADDRESS.COUNTRY IS NOT NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (v_id, P_ADDRESS.BUILDING_NUMBER, P_ADDRESS.BUILDING_NAME, UPPER(P_ADDRESS.COUNTRY),
					P_ADDRESS.BUILDING_FLOOR, P_ADDRESS.LOCALITY, P_ADDRESS.POST_OFFICE_BOX, P_ADDRESS.POSTAL_CODE,
					P_ADDRESS.REGION, P_ADDRESS.STREET_NAME,P_ADDRESS.UNIT );

				IF P_ADDRESS.RECIPIENT_NAMES IS NOT NULL AND P_ADDRESS.RECIPIENT_NAMES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.RECIPIENT_NAMES.FIRST .. P_ADDRESS.RECIPIENT_NAMES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) VALUES (v_id, P_ADDRESS.RECIPIENT_NAMES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.ADDRESS_LINES IS NOT NULL AND P_ADDRESS.ADDRESS_LINES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.ADDRESS_LINES.FIRST .. P_ADDRESS.ADDRESS_LINES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE) VALUES (v_id, P_ADDRESS.ADDRESS_LINES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.DELIVERY_POINT_CODES IS NOT NULL AND P_ADDRESS.DELIVERY_POINT_CODES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.DELIVERY_POINT_CODES.FIRST .. P_ADDRESS.DELIVERY_POINT_CODES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE) VALUES (v_id, P_ADDRESS.DELIVERY_POINT_CODES(i));
					END LOOP;
				END IF;
		END IF;
		return v_id;
	END;

	/*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
		v_f_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
        v_ma_id VARCHAR2(255);
	BEGIN
		v_p_id:= INSERTA_TELEFONO(P_CONTACT.PHONE);
		v_f_id:= INSERTA_TELEFONO(P_CONTACT.FAX);
		v_a_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.STREET_ADDRESS);
        v_ma_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.MAILING_ADDRESS);

		IF v_p_id IS NOT NULL 
			OR v_a_id IS NOT NULL 
			OR (P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0) 
			OR (P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0) THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT_DETAILS (ID, FAX_NUMBER, PHONE_NUMBER, STREET_ADDRESS, MAILING_ADDRESS) VALUES (v_id, v_f_id, v_p_id, v_a_id, v_ma_id);

				IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN
					FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST 
					LOOP
						INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (v_id, P_CONTACT.EMAIL(i));
					END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN
					FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST 
					LOOP
						v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_URL(i));
						INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (v_id, v_li_id);
					END LOOP;
				END IF;
		END IF;

		return v_id;
	END;

    /*
		Inserta datos de contacto y retorna id de contact detail id
	*/
	FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
		v_li_id VARCHAR2(255);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN

		v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT);

		IF v_c_id IS NOT NULL 
			OR (P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0) 
			OR (P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;

               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;

				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT.CONTACT_ROLE, v_c_id, null);

				IF P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_NAME.FIRST .. P_CONTACT.CONTACT_NAME.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_NAME(i));
							INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT.CONTACT_DESCRIPTION.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_DESCRIPTION(i));
							INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;
		END IF;	
		RETURN v_c_id;
	END;

	/*
        Inserta datos de contacto y de persona 
    */
    FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
        v_id VARCHAR2(255);
        v_p_id VARCHAR2(255);
        v_c_id VARCHAR2(255);
        v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
        v_li_id VARCHAR2(255);
        CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
            FROM EWPCV_ORGANIZATION_UNIT O 
            INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
            INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
            WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
            AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
    BEGIN

        v_p_id := INSERTA_PERSONA(P_CONTACT_PERSON);
        v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT);

        IF v_p_id IS NOT NULL 
            OR v_c_id IS NOT NULL 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0) 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;

               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;


                v_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
                    VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT_PERSON.CONTACT.CONTACT_ROLE, v_c_id, v_p_id);



                IF P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_NAME.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_NAME.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_NAME(i));
                            INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;



                IF P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION(i));
                            INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;
        END IF;    
        RETURN v_id;
    END;

		/*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN

		IF P_SUBJECT_AREA.ISCED_CODE IS NOT NULL OR P_SUBJECT_AREA.ISCED_CLARIFICATION IS NOT NULL THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (v_id, P_SUBJECT_AREA.ISCED_CODE, P_SUBJECT_AREA.ISCED_CLARIFICATION);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_lang IN VARCHAR, p_cefr IN VARCHAR) IS SELECT ID
			FROM EWPCV_LANGUAGE_SKILL
			WHERE UPPER("LANGUAGE") = UPPER(p_lang)
			AND  UPPER(CEFR_LEVEL) = UPPER(p_cefr);
	BEGIN
		OPEN exist_cursor(P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL );
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_LANGUAGE_SKILL.LANG IS NOT NULL OR  P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (v_id, P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL);
		END IF;
		RETURN v_id;
	END;

    procedure print_clob( p_clob in clob ) is
          v_offset number default 1;
          v_chunk_size number := 10000;
      begin
          loop
              exit when v_offset > dbms_lob.getlength(p_clob);
              dbms_output.put_line( dbms_lob.substr( p_clob, v_chunk_size, v_offset ) );
              v_offset := v_offset +  v_chunk_size;
          end loop;
      end print_clob;  

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) AS
	BEGIN
		INSERT INTO EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (EWP.GENERATE_UUID(),P_ELEMENT_ID, LOWER(P_NOTIFY_HEI), SYSDATE, P_TYPE, 1, 0, LOWER(P_NOTIFIER_HEI) );
	END;

END PKG_COMMON;
/*
    ***PKG_FACTSHEET
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoño
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoño
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  

	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );

	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;



	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;

	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );

	-- FUNCIONES 

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_FACTSHEET;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-DECISION_WEEK_LIMIT');
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-TOR_WEEK_LIMIT');
		END IF;

		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-NOMINATIONS_AUTUM_TERM');
		END IF;

		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-NOMINATIONS_SPRING_TERM');
		END IF;

		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-APPLICATION_AUTUM_TERM');
		END IF;

		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-APPLICATION_SPRING_TERM');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-EMAIL');
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-PHONE');
		END IF;

		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-URL');
		END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(2000);
	BEGIN
		IF EWP.VALIDA_EMAIL(P_INFORMATION_ITEM.EMAIL, v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '-EMAIL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;
	
		IF P_INFORMATION_ITEM.PHONE.E164 IS NOT NULL AND EWP.VALIDA_PHONE_NUMBER(P_INFORMATION_ITEM.PHONE.E164, v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-PHONE.E164: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;
	
		IF P_INFORMATION_ITEM.URL IS NOT NULL AND P_INFORMATION_ITEM.URL.COUNT > 0 THEN 
			FOR i IN P_INFORMATION_ITEM.URL.FIRST .. P_INFORMATION_ITEM.URL.LAST LOOP 
				IF EWP.VALIDA_URL(P_INFORMATION_ITEM.URL(i).TEXT, 0,v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-URL('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;			
		END IF;
		RETURN v_cod_retorno;
	END ;

	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP

			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it = 0 AND VALIDA_DATOS_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: 
						'|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				ADITIONAL_INFO['|| i || ']:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it = 0 AND VALIDA_DATOS_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: 
						'|| v_mensaje_it;
			END IF;
		
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				ACCESSIBILITY_REQUIREMENTS['|| i || ']:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				REQUIREMENTS_INFO'|| i || ':');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_it);
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
		v_max_mensaje VARCHAR2(8168 CHAR);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS 
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);       
	BEGIN
        OPEN exist_cursor_inst(P_FACTSHEET_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;

		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-INSTITUTION_ID');
        ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-INSTITUTION_ID: La institución no existe en el sistema.');
		END IF;

		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			FACTSHEET:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_factsheet);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			APPLICATION_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_app_info);
		END IF;
	
		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-APPLICATION_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_app_info);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			HOUSING_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_hou_info);
		END IF;
	
		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-HOUSING_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_hou_info);
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			VISA_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_vis_info);
		END IF;

		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-VISA_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_vis_info);
		END IF;	
	
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			INSURANCE_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_ins_info);
		END IF;

		IF VALIDA_DATOS_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-INSURANCE_INFO:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_ins_info);
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			ADDITIONAL_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_add_info);
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			ACCESSIBILITY_REQUIREMENTS:');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_acc_req);
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			REQUIREMENTS_INFO:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_add_req);
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			P_ERROR_MESSAGE := '';
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
		END IF;

		RETURN v_cod_retorno;
	END;


	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;

	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
		v_cd_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
		OPEN c(P_FACTSHEET_ID);
		FETCH c into v_cd_id;
		CLOSE c;

        UPDATE EWPCV_FACT_SHEET
        SET DECISION_WEEKS_LIMIT = null,
                TOR_WEEKS_LIMIT = null,
                NOMINATIONS_AUTUM_TERM = null,
                NOMINATIONS_SPRING_TERM = null,
                APPLICATION_AUTUM_TERM = null,
                APPLICATION_SPRING_TERM = null,
                CONTACT_DETAILS_ID = null
        WHERE ID = P_FACTSHEET_ID;

		PKG_COMMON.BORRA_CONTACT_DETAILS(v_cd_id);
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;

		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);

		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;

		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,LOWER(P_INF_TYPE),v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_FS_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

        IF P_FS_ID IS NULL THEN 
            v_factsheet_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
                VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
                    P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
        ELSE
            v_factsheet_id := P_FS_ID;
            UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT, 
                TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT,
                NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
                NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
                APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
                APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
                CONTACT_DETAILS_ID = v_contact_id
                WHERE ID = v_factsheet_id;
        END IF;

		RETURN v_factsheet_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);

		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, LOWER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;

	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_FS_ID IN VARCHAR2) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO, P_FS_ID);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME, null);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);

		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;

	END;


	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;

	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN

		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);

		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;


	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);

	BEGIN

		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;

		IF P_FACTSHEET.INSTITUTION_NAME IS NOT NULL AND P_FACTSHEET.INSTITUTION_NAME.COUNT > 0 THEN
			BORRA_NOMBRES(P_INSTITUTION_ID);
			PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		END IF;

		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);

		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);

		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
        v_fs_id VARCHAR2(255);
        v_fs_cd_id VARCHAR2(255);
        CURSOR c_fid(p_id IN VARCHAR2) IS 
		SELECT fs.id, fs.CONTACT_DETAILS_ID
		FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(p_id);
	BEGIN
        OPEN c_fid(P_FACTSHEET.INSTITUTION_ID);
        FETCH c_fid INTO v_fs_id, v_fs_cd_id;
        CLOSE c_fid;

		IF v_fs_cd_id IS NOT NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET, v_fs_id);
		END IF;
		COMMIT;

		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no existe');
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no tiene fact sheet insertelo antes de actualizarlo');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion no existe');
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no tiene fact sheet insertelo antes de actualizarlo');
			RETURN -1;
		END IF;

		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;

/*
    ***PKG_IIAS
*/
/
create or replace PACKAGE EWP.PKG_IIAS AS

	-- TIPOS DE DATOS
	
	/*Datos del tipo de movilidad. Campos obligatorios marcados con *.
			MOBILITY_CATEGORY*		Categoría de la movilidad, puede ser  teaching, studies, training,
			MOBILITY_GROUP*			Grupo en que se engloba la movilidad puede ser student o staff
	*/
	TYPE MOBILITY_TYPE IS RECORD
	  (
		MOBILITY_CATEGORY			VARCHAR2(255 CHAR),
		MOBILITY_GROUP				VARCHAR(255 CHAR)
	  );

	/*Informacion sobre cada una de las partes del IIA. Campos obligatorios marcados con *.
			INSTITUTION	*				Institucion a la que representa el partner
			SIGNING_DATE				Fecha en la que esta parte firmo el acuerdo
			SIGNER_PERSON				Datos de la persona que firmo el acuerdo en representacion de esta parte
			PARTNER_CONTACTS			Lista de personas de contacto de esta parte del acuerdo
	*/
	TYPE PARTNER IS RECORD
	  (
		INSTITUTION					EWP.INSTITUTION,
		SIGNING_DATE				DATE,
		SIGNER_PERSON				EWP.CONTACT_PERSON,
		PARTNER_CONTACTS			EWP.CONTACT_PERSON_LIST
	  );

	/*Informacion sobre las areas de ensenyanza y el idioma que se recomienda para cada una.
		Campos obligatorios marcados con *.
			SUBJECT_AREA	 			Areas de ensenyanza
			LANGUAGE_SKILL				Idioma que se recomienda para el area indicada
	*/
	TYPE SUBJECT_AREA_LANGUAGE IS RECORD
	  (
		SUBJECT_AREA	 			EWP.SUBJECT_AREA,
		LANGUAGE_SKILL	 			EWP.LANGUAGE_SKILL
	  );

	/* Lista con todas ensenyanza asociadas a una condicion de cooperacion y el idioma que se recomienda para cada una. */
	TYPE SUBJECT_AREA_LANGUAGE_LIST IS TABLE OF SUBJECT_AREA_LANGUAGE INDEX BY BINARY_INTEGER ;

	/*Informacion sobre la duracion de una condicion de cooperacion
		Campos obligatorios marcados con *.
			NUMBER_DURATION	 			Cantidad de las unidades indicadas que dura la condicion de cooperacion.
			UNIT	 					Unidades en que se expresa la duracion de la condicion de cooperacion.
										0:HOURS, 1:DAYS, 2:WEEKS, 3:MONTHS, 4:YEARS
	*/
	TYPE DURATION IS RECORD
	  (
		NUMBER_DURATION	 			NUMBER(5,2),
		UNIT	 					NUMBER(1,0)
	  );

	/* Lista de niveles de estudios relacionado con la condicion de cooperacion  */
	TYPE EQF_LEVEL_LIST IS TABLE OF NUMBER(3,0) INDEX BY BINARY_INTEGER;

	/*Informacion sobre cada uno de las condiciones de cooperacion de un acuerdo interinstitucional.
		START_DATE *				Fecha en que comienza la vigencia de la condicion de cooperacion
		END_DATE *					Fecha en que termina la vigencia de la condicion de cooperacion
		EQF_LEVEL_LIST *			Niveles de estudios contemplados en la condicion de cooperacion
		MOBILITY_NUMBER	*			Numero de participantes maximo que se puede beneficiar de esta condicion de cooperacion
		MOBILITY_TYPE *				Tipo de movilidad, indica si esde estudiantes o profesores y la actividad a desarrollar
		RECEIVING_PARTNER *			Informacion sobre la institucion destino
		SENDING_PARTNER *			Informacion sobre la institucion origen
        SUBJECT_AREA_LANGUAGE_LIST*	Informacion sobre areas de aprendizaje e idiomas relacionados a la condicion de cooperacion
        SUBJECT_AREA_LIST           Lista sobre subject area list
        COP_COND_DURATION			Duracion de la condicion de cooperacion
		OTHER_INFO					Inforamcion adicional sobre la condicion de cooperacion
		BLENDED*					Indica si la condicion de cooperacion permite el formato de movilidad mixta
	*/	
	TYPE COOPERATION_CONDITION IS RECORD
	  (
		START_DATE					DATE,
		END_DATE					DATE,
		EQF_LEVEL_LIST       		PKG_IIAS.EQF_LEVEL_LIST,
		MOBILITY_NUMBER				NUMBER(10,0),
		MOBILITY_TYPE				PKG_IIAS.MOBILITY_TYPE,
		RECEIVING_PARTNER			PKG_IIAS.PARTNER,
		SENDING_PARTNER				PKG_IIAS.PARTNER,
		SUBJECT_AREA_LANGUAGE_LIST	PKG_IIAS.SUBJECT_AREA_LANGUAGE_LIST,
        SUBJECT_AREA_LIST           EWP.SUBJECT_AREA_LIST,
		COP_COND_DURATION			PKG_IIAS.DURATION,
		OTHER_INFO					VARCHAR2(255 CHAR),
		BLENDED						NUMBER(1,0)
	  );

	/* Lista con las condiciones de cooperacion de un acuerdo interinstitucional. */
	TYPE COOPERATION_CONDITION_LIST IS TABLE OF COOPERATION_CONDITION INDEX BY BINARY_INTEGER ;


	/* Objeto que modela un acuerdo interinstitucional. Campos obligatorios marcados con *.
			START_DATE 						Fecha en que comienza la vigencia del IIA
			END_DATE 						Fecha en que termina la vigencia del IIA
			IIA_CODE						Codigo del IIA
			COOPERATION_CONDITION_LIST * 	Lista de condiciones de coperacion del IIA
			PDF								Documento pdf
	*/
	TYPE IIA IS RECORD
	  (
		START_DATE						DATE,
		END_DATE						DATE,
		IIA_CODE						VARCHAR2(255 CHAR),
		COOPERATION_CONDITION_LIST 		PKG_IIAS.COOPERATION_CONDITION_LIST,
		PDF								BLOB
	  );

	-- FUNCIONES

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA, P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

     /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
	FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;
	
	/*  Desvincula un IIA propio de un IIA remoto. 
		No se pueden desvincular IIAS que ya estén aprobados.
        Recibe como parámetro el identificador propio .
	*/
    FUNCTION UNBIND_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

END PKG_IIAS;
/
create or replace PACKAGE BODY EWP.PKG_IIAS AS

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************

	/*
		Valida mobility type
	*/
	FUNCTION VALIDA_MOBILITY_TYPE(P_MOBILITY_TYPE IN PKG_IIAS.MOBILITY_TYPE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY');
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_CATEGORY) NOT IN ('TEACHING', 'STUDIES', 'TRAINING') THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-MOBILITY_CATEGORY: Valor no permitido (teaching, studies, training)');
		END IF;

		IF P_MOBILITY_TYPE.MOBILITY_GROUP IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-MOBILITY_GROUP');
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_GROUP) NOT IN ('STUDENT', 'STAFF') THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-MOBILITY_GROUP: Valor no permitido (student, staff)');
		END IF;

		RETURN v_cod_retorno;
	END;

    /*
		Valida el el ounit asociado a la INSTITUTION
	*/
    FUNCTION VALIDA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN
        IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_INSTITUTION.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN
                v_cod_retorno := -1;
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                    -ORGANIZATION_UNIT_CODE: La organización asociada a esta institución no existe en el sistema.');
            END IF;
        END IF;

        RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto Institution
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_ins_id      VARCHAR2(255);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;

		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-INSTITUTION_ID');
		ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-INSTITUTION_ID: La institución no existe en el sistema.');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida el objecto PARTNER de las COOP_COND del IIA
	*/
	FUNCTION VALIDA_DATOS_PARTNER (P_PARTNER IN PKG_IIAS.PARTNER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_partner VARCHAR2(32767);
		v_error_message VARCHAR2(32767);
	BEGIN
       	IF P_PARTNER.PARTNER_CONTACTS IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_PARTNER.PARTNER_CONTACTS, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-PARTNER_CONTACTS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_PARTNER.SIGNER_PERSON IS NOT NULL AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_PARTNER.SIGNER_PERSON, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-SIGNER_PERSON: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto PARTNER de las COOP_COND del IIA
	*/
	FUNCTION VALIDA_PARTNER (P_PARTNER IN PKG_IIAS.PARTNER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_partner VARCHAR2(32767);
	BEGIN
		IF P_PARTNER.INSTITUTION IS NULL THEN
			v_cod_retorno := -1;
			v_mensaje_partner := P_ERROR_MESSAGE || '
				-INSTITUTION:';
		ELSIF VALIDA_INSTITUTION(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN		
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_partner);
		ELSIF P_PARTNER.INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL AND VALIDA_OUNIT(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_partner);
        END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_SUBJECT_AREA_LANGUAGE(P_SUBJECT_AREA_LANGUAGE IN PKG_IIAS.SUBJECT_AREA_LANGUAGE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF
		--P_SUBJECT_AREA_LANGUAGE IS NOT NULL AND
		P_SUBJECT_AREA_LANGUAGE.LANGUAGE_SKILL IS NOT NULL
		AND PKG_COMMON.VALIDA_LANGUAGE_SKILL(P_SUBJECT_AREA_LANGUAGE.LANGUAGE_SKILL, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
							-SUBJECT_AREA_LANGUAGE: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
		END IF;
		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_COOP_COND (P_COOP_COND IN PKG_IIAS.COOPERATION_CONDITION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		IF P_COOP_COND.SUBJECT_AREA_LANGUAGE_LIST IS NOT NULL AND P_COOP_COND.SUBJECT_AREA_LANGUAGE_LIST.COUNT > 0 THEN
			FOR I IN P_COOP_COND.SUBJECT_AREA_LANGUAGE_LIST.FIRST .. P_COOP_COND.SUBJECT_AREA_LANGUAGE_LIST.LAST
			LOOP
				IF VALIDA_SUBJECT_AREA_LANGUAGE(P_COOP_COND.SUBJECT_AREA_LANGUAGE_LIST(i), v_error_message) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-SUBJECT_AREA_LANGUAGE_LIST['|| i||']: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
				END IF;
			END LOOP;
		END IF;
		IF P_COOP_COND.EQF_LEVEL_LIST IS NOT NULL AND P_COOP_COND.EQF_LEVEL_LIST.COUNT > 0 THEN
			FOR I IN P_COOP_COND.EQF_LEVEL_LIST.FIRST .. P_COOP_COND.EQF_LEVEL_LIST.LAST
			LOOP
				IF EWP.VALIDA_EQFLEVEL(P_COOP_COND.EQF_LEVEL_LIST(i), v_error_message) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-EQF_LEVEL_LIST['|| i||']: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
				END IF;
			END LOOP;
		END IF;

		IF
			--P_COOP_COND.SENDING_PARTNER IS NOT NULL AND
		VALIDA_DATOS_PARTNER(P_COOP_COND.SENDING_PARTNER, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SENDING_PARTNER: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF
		-- P_COOP_COND.RECEIVING_PARTNER IS NOT NULL AND
		VALIDA_DATOS_PARTNER(P_COOP_COND.RECEIVING_PARTNER, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-RECEIVING_PARTNER: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los datos de las COOPERATION_CONDITION del IIA
	*/
	FUNCTION VALIDA_DATOS_COOP_COND_IIA (P_COOP_COND_LIST IN PKG_IIAS.COOPERATION_CONDITION_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN
		FOR i IN P_COOP_COND_LIST.FIRST .. P_COOP_COND_LIST.LAST
		LOOP
			IF VALIDA_DATOS_COOP_COND(P_COOP_COND_LIST(i), v_error_message) <> 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					- COOP_COND_LIST['|| i||']: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
				v_error_message := '';
			END IF;
		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida las COOPERATION_CONDITION del IIA
	*/
	FUNCTION VALIDA_COOP_COND_IIA (P_COOP_COND_LIST IN PKG_IIAS.COOPERATION_CONDITION_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_send_partner VARCHAR2(32767);
		v_mensaje_receiv_partner VARCHAR2(32767);
		v_mensaje_mobility_type VARCHAR2(32767);
        v_is_error BOOLEAN;
	BEGIN
		FOR i IN P_COOP_COND_LIST.FIRST .. P_COOP_COND_LIST.LAST
		LOOP
			IF P_COOP_COND_LIST(i).START_DATE IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-START_DATE [' || i || ']');
            END IF;
			IF P_COOP_COND_LIST(i).END_DATE IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-END_DATE [' || i || ']');
			END IF;
			IF P_COOP_COND_LIST(i).COP_COND_DURATION.NUMBER_DURATION IS NULL OR P_COOP_COND_LIST(i).COP_COND_DURATION.UNIT IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-COP_COND_DURATION [' || i || ']');
			END IF;
			IF P_COOP_COND_LIST(i).EQF_LEVEL_LIST IS NULL OR P_COOP_COND_LIST(i).EQF_LEVEL_LIST.COUNT = 0
                AND UPPER(P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_GROUP) = 'STUDENT'
                AND UPPER(P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY) = 'STUDIES' THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-EQF_LEVEL_LIST [' || i || ']');	
			END IF;
            IF P_COOP_COND_LIST(i).MOBILITY_NUMBER IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-MOBILITY_NUMBER [' || i || ']');	
			END IF;
			IF P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-MOBILITY_TYPE [' || i || ']');
			ELSIF VALIDA_MOBILITY_TYPE(P_COOP_COND_LIST(i).MOBILITY_TYPE, v_mensaje_mobility_type) <> 0 THEN		
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					MOBILITY_TYPE [' || i || ']: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_mobility_type);
			END IF;	
			IF P_COOP_COND_LIST(i).BLENDED IS NULL THEN 	
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-BLENDED [' || i || ']');
			END IF;	
			IF P_COOP_COND_LIST(i).SENDING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SENDING_PARTNER [' || i || ']');
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).SENDING_PARTNER, v_mensaje_send_partner) <> 0 THEN		
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					SENDING_PARTNER [' || i || ']: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_send_partner);
			END IF;
			IF P_COOP_COND_LIST(i).RECEIVING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-RECEIVING_PARTNER [' || i || ']');
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).RECEIVING_PARTNER, v_mensaje_receiv_partner) <> 0 THEN		
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					RECEIVING_PARTNER [' || i || ']: ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_receiv_partner);
			END IF;	

		END LOOP;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un IIA
	*/
	FUNCTION VALIDA_IIA(P_IIA IN PKG_IIAS.IIA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_fechas VARCHAR2(32767);
		v_mensaje_coop_cond VARCHAR2(32767);
		v_max_mensaje VARCHAR2(32767);
	BEGIN

        IF P_IIA.IIA_CODE IS NULL THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			IIA:
                -IIA_CODE');
        END IF;

		IF P_IIA.COOPERATION_CONDITION_LIST IS NULL OR P_IIA.COOPERATION_CONDITION_LIST.COUNT = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			COOP_COND:');
		ELSIF VALIDA_COOP_COND_IIA(P_IIA.COOPERATION_CONDITION_LIST, v_mensaje_coop_cond) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			COOP_COND:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_coop_cond);
		END IF;


		IF v_cod_retorno <> 0 THEN
			
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los datos de un IIA
	*/
	FUNCTION VALIDA_DATOS_IIA(P_IIA IN PKG_IIAS.IIA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_fechas VARCHAR2(32767);
		v_mensaje_coop_cond VARCHAR2(32767);
	BEGIN

		IF P_IIA.COOPERATION_CONDITION_LIST IS NOT NULL AND P_IIA.COOPERATION_CONDITION_LIST.COUNT > 0 AND
			VALIDA_DATOS_COOP_COND_IIA(P_IIA.COOPERATION_CONDITION_LIST, v_mensaje_coop_cond) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-COOP_COND:');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_coop_cond);
		END IF;
		RETURN v_cod_retorno;
	END;

    /*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_SUBAREA_LIST(P_COP_COND_ID IN VARCHAR2, P_SUBAREA_LIST EWP.SUBJECT_AREA_LIST) AS
		v_isced_code VARCHAR2(255);
	BEGIN
		IF P_SUBAREA_LIST IS NOT NULL AND P_SUBAREA_LIST.COUNT > 0 THEN
			FOR i IN P_SUBAREA_LIST.FIRST .. P_SUBAREA_LIST.LAST
			LOOP
			v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_SUBAREA_LIST(i));
			INSERT INTO EWPCV_COOPCOND_SUBAR(ID, COOPERATION_CONDITION_ID, ISCED_CODE)
				VALUES(EWP.GENERATE_UUID(), P_COP_COND_ID, v_isced_code);
			END LOOP;
		END IF;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/*
		Borra las subject areas asociadas a una condicion de cooperacion
	*/
	PROCEDURE BORRA_COOP_COND_SUBAREA_LANSKI(P_COOP_COND_ID IN VARCHAR2) AS
		CURSOR c_sub_area(p_coop_cond_id IN VARCHAR2) IS
		SELECT ID, ISCED_CODE
		FROM EWPCV_COOPCOND_SUBAR_LANSKIL
		WHERE COOPERATION_CONDITION_ID = p_coop_cond_id;

	BEGIN

		FOR rec IN c_sub_area(P_COOP_COND_ID)
		LOOP
			DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = rec.ID;
			DELETE EWPCV_SUBJECT_AREA WHERE ID = rec.ISCED_CODE;
		END LOOP;
	END;

	PROCEDURE BORRA_COOP_COND_SUBAREA(P_COOP_COND_ID IN VARCHAR2) AS
		CURSOR c_sub_area(P_COOP_COND_ID IN VARCHAR2) IS
		SELECT ID, ISCED_CODE
		FROM EWPCV_COOPCOND_SUBAR
		WHERE COOPERATION_CONDITION_ID = P_COOP_COND_ID;

	BEGIN

		FOR rec IN c_sub_area(P_COOP_COND_ID)
		LOOP
			DELETE FROM EWPCV_COOPCOND_SUBAR WHERE ID = rec.ID;
			DELETE EWPCV_SUBJECT_AREA WHERE ID = rec.ISCED_CODE;
		END LOOP;
	END;

	/*
		Borra un IIA PARTNER y todos sus contactos asociados.
	*/
	PROCEDURE BORRA_IIA_PARTNER(P_IIA_PARTNER_ID IN VARCHAR2) AS
		v_s_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT CONTACTS_ID
			FROM EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = p_id;
		CURSOR c_s(p_id IN VARCHAR2) IS
			SELECT SIGNER_PERSON_CONTACT_ID
			FROM EWPCV_IIA_PARTNER
			WHERE ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_PARTNER_ID)
		LOOP
			DELETE FROM EWPCV_IIA_PARTNER_CONTACTS
				WHERE CONTACTS_ID = REC.CONTACTS_ID
				AND IIA_PARTNER_ID = P_IIA_PARTNER_ID;
			PKG_COMMON.BORRA_CONTACT(rec.CONTACTS_ID);
		END LOOP;

		OPEN c_s(P_IIA_PARTNER_ID);
		FETCH c_s into v_s_id;
		CLOSE c_s;

		DELETE FROM EWPCV_IIA_PARTNER WHERE ID = P_IIA_PARTNER_ID;
		PKG_COMMON.BORRA_CONTACT(v_s_id);

		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_PARTNER_ID;
	END;

	PROCEDURE BORRA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR2) AS
		v_mn_count NUMBER;
		v_mt_count NUMBER;
		v_md_count NUMBER;
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM EWPCV_COOPERATION_CONDITION
			WHERE IIA_ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_ID)
		LOOP
			DELETE FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = rec.ID;
			BORRA_COOP_COND_SUBAREA_LANSKI(rec.ID);
            BORRA_COOP_COND_SUBAREA(rec.ID);
			DELETE FROM EWPCV_COOPERATION_CONDITION WHERE ID = rec.ID;
			DELETE FROM EWPCV_MOBILITY_NUMBER WHERE ID = rec.MOBILITY_NUMBER_ID;

			DELETE FROM EWPCV_DURATION WHERE ID = rec.DURATION_ID;
			BORRA_IIA_PARTNER(rec.RECEIVING_PARTNER_ID);
			BORRA_IIA_PARTNER(rec.SENDING_PARTNER_ID);
		END LOOP;
	END;

	/*
		Borra un IIAS y todas sus condiciones de cooperacion
	*/
	PROCEDURE BORRA_IIA(P_IIA_ID IN VARCHAR2) AS
	BEGIN
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_ID;
	END;


	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_IIA_PARTNER(P_IIA_PARTNER IN PARTNER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255);
	BEGIN
		v_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_IIA_PARTNER.INSTITUTION);
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_IIA_PARTNER.SIGNER_PERSON, null, null);
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE)
				VALUES (v_id, LOWER(P_IIA_PARTNER.INSTITUTION.INSTITUTION_ID), v_ounit_id, v_c_id, P_IIA_PARTNER.SIGNING_DATE);
		RETURN v_id;
	END;

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_PARTNER_CONTACTS(P_PARTNER_ID IN VARCHAR2, P_PARTNER_CONTACTS EWP.CONTACT_PERSON_LIST) AS
		v_c_p_id VARCHAR2(255);
	BEGIN
		IF P_PARTNER_CONTACTS IS NOT NULL AND P_PARTNER_CONTACTS.COUNT > 0 THEN
			FOR i IN P_PARTNER_CONTACTS.FIRST .. P_PARTNER_CONTACTS.LAST
			LOOP
				v_c_p_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_PARTNER_CONTACTS(i), null, null);
				INSERT INTO EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (P_PARTNER_ID, v_c_p_id);
			END LOOP;
		END IF;
	END;

	/*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_SUBAREA_LANSKIL(P_COP_COND_ID IN VARCHAR2, P_SUBAREA_LANSKIL_LIST SUBJECT_AREA_LANGUAGE_LIST) AS
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN
		IF P_SUBAREA_LANSKIL_LIST IS NOT NULL AND P_SUBAREA_LANSKIL_LIST.COUNT > 0 THEN
			FOR i IN P_SUBAREA_LANSKIL_LIST.FIRST .. P_SUBAREA_LANSKIL_LIST.LAST
			LOOP
			v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_SUBAREA_LANSKIL_LIST(i).SUBJECT_AREA);
			v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_SUBAREA_LANSKIL_LIST(i).LANGUAGE_SKILL);
			INSERT INTO EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(EWP.GENERATE_UUID(), P_COP_COND_ID, v_isced_code, v_lang_skill_id);
			END LOOP;
		END IF;
	END;

    /*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN MOBILITY_TYPE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR, p_group IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER(p_group);
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NOT NULL OR  P_MOBILITY_TYPE.MOBILITY_GROUP IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		END IF;
		RETURN v_id;
	END;


     /*
		Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_DURATION(P_DURATION IN PKG_IIAS.DURATION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (v_id, P_DURATION.NUMBER_DURATION, P_DURATION.UNIT);
		RETURN v_id;
	END;

     /*
		Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_MOBILITY_NUMBER(P_MOB_NUMBER IN VARCHAR) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (v_id, P_MOB_NUMBER);
		RETURN v_id;
	END;

	/*
		Inserta los EQUF LEVELS asociados a una condicion de cooperacion
	*/
	PROCEDURE INSERTA_EQFLEVELS(P_COOP_COND_ID IN VARCHAR2, P_COOP_COND IN COOPERATION_CONDITION) AS
	BEGIN
		IF P_COOP_COND.EQF_LEVEL_LIST IS NOT NULL AND P_COOP_COND.EQF_LEVEL_LIST .COUNT > 0 THEN
			FOR i IN P_COOP_COND.EQF_LEVEL_LIST .FIRST .. P_COOP_COND.EQF_LEVEL_LIST .LAST
			LOOP
				INSERT INTO EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (P_COOP_COND_ID,P_COOP_COND.EQF_LEVEL_LIST(i));
			END LOOP;
		END IF;
	END;

	/*
		Persiste una lista de condiciones de cooperacion asociadas a un IIAS
	*/
	PROCEDURE INSERTA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR, P_COOP_COND IN COOPERATION_CONDITION_LIST) AS
		v_s_iia_partner_id VARCHAR2(255);
        v_s_c_p_id VARCHAR2(255);
        v_r_iia_partner_id VARCHAR2(255);
        v_r_c_p_id VARCHAR2(255);
        v_mobility_type_id VARCHAR2(255);
        v_duration_id VARCHAR2(255);
        v_mob_number_id VARCHAR2(255);
		v_cc_id VARCHAR2(255);
	BEGIN
		FOR i IN P_COOP_COND.FIRST .. P_COOP_COND.LAST
		LOOP
			v_s_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).SENDING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).SENDING_PARTNER.PARTNER_CONTACTS);

			v_r_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).RECEIVING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_r_iia_partner_id, P_COOP_COND(i).RECEIVING_PARTNER.PARTNER_CONTACTS);

			v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_COOP_COND(i).MOBILITY_TYPE);
			v_duration_id := INSERTA_DURATION(P_COOP_COND(i).COP_COND_DURATION);
			v_mob_number_id := INSERTA_MOBILITY_NUMBER(P_COOP_COND(i).MOBILITY_NUMBER);

			v_cc_id :=	EWP.GENERATE_UUID();
			INSERT INTO EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO, BLENDED)
				VALUES (v_cc_id, P_COOP_COND(i).END_DATE, P_COOP_COND(i).START_DATE, v_duration_id,
					v_mob_number_id, v_mobility_type_id, v_r_iia_partner_id, v_s_iia_partner_id, P_IIA_ID, P_COOP_COND(i).OTHER_INFO, P_COOP_COND(i).BLENDED );

			INSERTA_SUBAREA_LANSKIL(v_cc_id, P_COOP_COND(i).SUBJECT_AREA_LANGUAGE_LIST);
			INSERTA_EQFLEVELS(v_cc_id,P_COOP_COND(i));
            INSERTA_SUBAREA_LIST(v_cc_id, P_COOP_COND(i).SUBJECT_AREA_LIST);
		END LOOP;
	END;

	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_IIA(P_IIA IN PKG_IIAS.IIA) RETURN VARCHAR2 AS
		v_iia_id VARCHAR2(255);
	BEGIN

        v_iia_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_IIA (ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE)
            VALUES (v_iia_id, P_IIA.END_DATE, P_IIA.IIA_CODE, CURRENT_DATE, P_IIA.START_DATE);

		INSERTA_COOP_CONDITIONS(v_iia_id,P_IIA.COOPERATION_CONDITION_LIST);

        RETURN v_iia_id;
	END;


	FUNCTION OBTEN_NOTIFIER_HEI(P_IIA_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_notifier_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT DISTINCT(P.INSTITUTION_ID)
			FROM EWPCV_IIA_PARTNER P
				INNER JOIN EWPCV_COOPERATION_CONDITION CC
					ON CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID
			WHERE CC.IIA_ID = p_id;
	BEGIN
		OPEN c(P_IIA_ID);
		FETCH c INTO v_notifier_hei;
		WHILE c%FOUND AND UPPER(v_notifier_hei) = UPPER(P_HEI_TO_NOTIFY)
		LOOP
			FETCH c INTO v_notifier_hei;
		END LOOP;
		CLOSE c;
		RETURN v_notifier_hei;
	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	PROCEDURE ACTUALIZA_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA) AS
		v_is_exposed number(1,0);
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT IS_EXPOSED
			FROM EWPCV_IIA
			WHERE LOWER(ID) = LOWER(p_id);
	BEGIN
		OPEN c(P_IIA_ID);
		FETCH c INTO v_is_exposed;
		close c;
	
		--era una copia autogenerada no expuesta y hay que exponerla
		IF v_is_exposed = 0 THEN 
			UPDATE EWPCV_IIA SET START_DATE = P_IIA.START_DATE, END_DATE = P_IIA.END_DATE, IIA_CODE = P_IIA.IIA_CODE, PDF = P_IIA.PDF, MODIFY_DATE = SYSDATE, IS_EXPOSED = 1
				WHERE ID = P_IIA_ID;
		END IF;
		
		UPDATE EWPCV_IIA SET START_DATE = P_IIA.START_DATE, END_DATE = P_IIA.END_DATE, IIA_CODE = P_IIA.IIA_CODE, PDF = P_IIA.PDF, MODIFY_DATE = SYSDATE
			WHERE ID = P_IIA_ID;
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		INSERTA_COOP_CONDITIONS(P_IIA_ID, P_IIA.COOPERATION_CONDITION_LIST);

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA,  P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_IIA(P_IIA, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				P_IIA_ID := INSERTA_IIA(P_IIA);
				v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
				PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
			END IF;
		END IF;
        COMMIT;
		RETURN v_cod_retorno;
        EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END INSERT_IIA; 	

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS			
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count_iia NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El IIA no existe');
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID AND IS_REMOTE = 1;
		IF v_count_iia > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar IIAs remotos');
			RETURN -1;
		END IF;
		
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_IIA(P_IIA, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				ACTUALIZA_IIA(P_IIA_ID, P_IIA);
				v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
				PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
			END IF;
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_IIA;

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_count_iia NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count_mobility NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El IIA no existe');
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID AND IS_REMOTE = 1;
		IF v_count_iia > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden borrar IIAs remotos');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_mobility FROM EWPCV_MOBILITY WHERE IIA_ID = P_IIA_ID;
		IF v_count_mobility > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Existen movilidadtes asociadas al IIA');
			RETURN -1;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
		BORRA_IIA(P_IIA_ID);
		PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		return 0;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_IIA;

    /*  Desvincula un IIA propio de un IIA remoto. 
		No se pueden desvincular IIAS que ya estén aprobados.
        Recibe como parámetro el identificador propio .
	*/
    FUNCTION UNBIND_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_count_iia NUMBER := 0;
        
    BEGIN
        IF P_IIA_ID IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado el id del IIA propio');
			RETURN -1;
		END IF;

        SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(P_IIA_ID) AND IS_REMOTE = 0;
		IF v_count_iia = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No existe ningún IIA local asociado al id que se ha infromado');
			RETURN -1;
		END IF;

        --Desbindeamos el IIA 
        UPDATE EWPCV_IIA SET REMOTE_IIA_ID = null, REMOTE_IIA_CODE = null
            WHERE LOWER(ID) = LOWER(P_IIA_ID) AND IS_REMOTE = 0;
        COMMIT;
        return 0;

        EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END UNBIND_IIA;
	
	 /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
    FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_count_iia NUMBER := 0;
        v_remote_iia_id VARCHAR2(255);
        v_remote_coop_cond_hash VARCHAR2(255);
        v_remote_iia_code VARCHAR2(255);
        CURSOR c(p_remote_id IN VARCHAR2) IS
			SELECT IIA.REMOTE_IIA_ID, IIA.REMOTE_COP_COND_HASH, IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA
			WHERE IIA.ID = p_remote_id;
    BEGIN
        IF P_OWN_IIA_ID IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado el id del IIA propio');
			RETURN -1;
		ELSIF P_ID_FROM_REMOTE_IIA IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado el id del IIA remoto');
			RETURN -1;
		END IF;

        SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_OWN_IIA_ID;
		IF v_count_iia = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No existe ningún IIA asociado al id que se ha infromado como parámetro P_OWN_IIA_ID');
			RETURN -1;
		END IF;

        OPEN c(P_ID_FROM_REMOTE_IIA);
		FETCH c INTO v_remote_iia_id, v_remote_coop_cond_hash, v_remote_iia_code;
		CLOSE c;
		IF v_remote_iia_id IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID');
			RETURN -1;
		END IF;
        --Bindeamos el IIA nuestro con el remoto
        UPDATE EWPCV_IIA SET REMOTE_IIA_ID = v_remote_iia_id, REMOTE_IIA_CODE = v_remote_iia_code
            WHERE ID = P_OWN_IIA_ID;
        COMMIT;
        return 0;

        EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END BIND_IIA;

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_remote_iia_id VARCHAR2(255);
        v_notifier_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR c(p_internal_id_iia_copy IN VARCHAR2) IS
			SELECT IIA.REMOTE_IIA_ID
			FROM EWPCV_IIA IIA
			WHERE IIA.ID = p_internal_id_iia_copy;
    BEGIN
        IF P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado el id interno de la copia del IIA remoto');
			RETURN -1;
		ELSIF P_HEI_ID_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado el hei-id de la institución a notificar');
			RETURN -1;
		END IF;

        OPEN c(P_INTERNAL_ID_REMOTE_IIA_COPY);
		FETCH c INTO v_remote_iia_id;
		CLOSE c;

        v_notifier_hei := OBTEN_NOTIFIER_HEI(P_INTERNAL_ID_REMOTE_IIA_COPY, P_HEI_ID_TO_NOTIFY);
        v_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI)
						VALUES (v_id, 0, v_remote_iia_id, P_HEI_ID_TO_NOTIFY, SYSDATE, 4, 1, 0, 0, v_notifier_hei);
        COMMIT;
        return 0;
        EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END APPROVE_IIA;

END PKG_IIAS;

/*
    ***PKG_INSTITUTION
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_INSTITUTION AS 

	-- TIPOS DE DATOS
	/* Objeto que modela una INSTITUTION. Campos obligatorios marcados con *.
			INSTITUTION_ID *				Código SCHAC de la institución
			INSTITUTION_NAME_LIST *			Lista con los nombres de la institución en varios idiomas
			ABBREVIATION					Abreviación de la universidad
			UNIVERSITY_CONTACT_DETAILS		Datos de contacto de la universidad (FK de Institution a EWPCV_CONTACT_DETAILS a travÃ©s del campo PRIMARY_CONTACT_DETAIL_ID)
			CONTACT_DETAILS_LIST			Lista de los datos de contacto
			FACTSHEET_URL_LIST				Lista de urls de los factsheet de la institución
			LOGO_URL						Url del logo de la institución
	*/
	TYPE INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		INSTITUTION_NAME_LIST			EWP.LANGUAGE_ITEM_LIST,
		ABBREVIATION					VARCHAR2(255 CHAR),
		UNIVERSITY_CONTACT_DETAILS		EWP.CONTACT,
		CONTACT_DETAILS_LIST			EWP.CONTACT_PERSON_LIST,
		FACTSHEET_URL_LIST				EWP.LANGUAGE_ITEM_LIST,
		LOGO_URL						VARCHAR2(255 CHAR)
	  );

	-- FUNCIONES 

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros: 
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_INSTITUTION;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_INSTITUTION AS 

	/*
		Valida los campos obligatorios de una institución 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID IS NULL THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_ID');
		END IF;
		IF  P_INSTITUTION.INSTITUTION_NAME_LIST IS NULL OR P_INSTITUTION.INSTITUTION_NAME_LIST.COUNT = 0 THEN 
            IF P_ERROR_MESSAGE IS NULL THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'INSTITUTION: ');
            END IF;
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-INSTITUTION_NAME_LIST');
		END IF;

		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_DATOS_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message_url VARCHAR2(32767);
		v_error_message VARCHAR2(32767);
	BEGIN

		IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST LOOP 
				IF EWP.VALIDA_URL(P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, 0, v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-FACTSHEET_URL_LIST('|| i|| '): ');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;
		END IF;

		IF P_INSTITUTION.LOGO_URL IS NOT NULL AND EWP.VALIDA_URL(P_INSTITUTION.LOGO_URL, 0,v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-LOGO_URL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;

		IF P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_DETAILS(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-UNIVERSITY_CONTACT_DETAILS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_INSTITUTION.CONTACT_DETAILS_LIST, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-CONTACT_DETAILS_LIST: '); 
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

    FUNCTION VALIDA_FS_OUNIT(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE  IN OUT VARCHAR2) RETURN VARCHAR2 IS
        v_tiene_ounit NUMBER := 0;
        v_tiene_fs NUMBER := 0;
        v_cod_retorno NUMBER := 0;
    BEGIN
        SELECT COUNT(1) INTO v_tiene_ounit FROM EWPCV_INST_ORG_UNIT iou 
            INNER JOIN EWPCV_INSTITUTION i ON iou.INSTITUTION_ID = i.ID 
            WHERE UPPER(i.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);

        SELECT COUNT(1) INTO v_tiene_fs FROM EWPCV_INSTITUTION ins
            INNER JOIN EWPCV_FACT_SHEET fs ON ins.FACT_SHEET = FS.ID
            WHERE UPPER(ins.INSTITUTION_ID) = P_INSTITUTION_ID
            AND fs.CONTACT_DETAILS_ID IS NOT NULL;

        IF v_tiene_ounit > 0 THEN 
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,  
                    'La institución tiene OUNITS asociadas y no se puede borrar.');
        ELSIF v_tiene_fs > 0 THEN
            v_cod_retorno := 1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                    'La institución tiene FACT_SHEETS asociadas y no se puede borrar.');
        END IF;

        RETURN v_cod_retorno;
    END;

    /*
		Actualiza la lista de fact_sheet_url
	*/
	PROCEDURE ACTUALIZA_FACT_SHEET_URL(P_INSTITUTION_ID IN VARCHAR2, FACTSHEET_URL_LIST IN EWP.LANGUAGE_ITEM_LIST) AS
		v_fact_sheet_id VARCHAR2(255);
		v_count NUMBER;
        v_lang_item_id VARCHAR2(255);
		CURSOR c_fact_sheet(p_inst_id IN VARCHAR2) IS SELECT fs.ID
			FROM EWPCV_FACT_SHEET fs
			INNER JOIN EWPCV_INSTITUTION i ON i.FACT_SHEET = fs.ID 
			WHERE UPPER(i.INSTITUTION_ID) = UPPER(p_inst_id);
		CURSOR c_fact_sheet_urls(p_f_s_id IN VARCHAR2) IS SELECT URL_ID
			FROM EWPCV_FACT_SHEET_URL
			WHERE FACT_SHEET_ID = p_f_s_id;
	BEGIN
		OPEN c_fact_sheet(P_INSTITUTION_ID);
			FETCH c_fact_sheet INTO v_fact_sheet_id;
			CLOSE c_fact_sheet;


		FOR fact_sheet_url IN c_fact_sheet_urls(v_fact_sheet_id)
		LOOP 
			DELETE FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = fact_sheet_url.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = fact_sheet_url.URL_ID;
		END LOOP;


		IF FACTSHEET_URL_LIST IS NOT NULL AND FACTSHEET_URL_LIST.COUNT > 0 THEN
			FOR i IN FACTSHEET_URL_LIST.FIRST .. FACTSHEET_URL_LIST.LAST 
			LOOP
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(FACTSHEET_URL_LIST(i));
				END IF; 
				v_count := 1;
				SELECT COUNT(1) INTO v_count FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = v_lang_item_id AND FACT_SHEET_ID = v_fact_sheet_id;
				IF v_lang_item_id IS NOT NULL AND v_count = 0 THEN 
					INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (v_lang_item_id, v_fact_sheet_id);
				END IF;
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Inserta las urls de factsheet de la institución 
	*/
	FUNCTION INSERTA_FACTSHEET_URLS(P_INSTITUTION IN INSTITUTION) RETURN VARCHAR2 AS 
	    v_lang_item_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		CURSOR exist_lang_it_cursor(p_lang IN VARCHAR2, p_text IN VARCHAR2, p_fs_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM lait
            INNER JOIN EWPCV_FACT_SHEET_URL fsu ON fsu.URL_ID = lait.ID
			WHERE UPPER(lait.LANG) = UPPER(p_lang)
			AND UPPER(lait.TEXT) = UPPER(p_text)
            AND fsu.FACT_SHEET_ID = p_fs_id;
        CURSOR exist_fact_sheet_cursor(p_ins_id IN VARCHAR2) IS SELECT FACT_SHEET
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
        OPEN exist_fact_sheet_cursor(P_INSTITUTION.INSTITUTION_ID);
        FETCH exist_fact_sheet_cursor INTO v_factsheet_id;
        CLOSE exist_fact_sheet_cursor;
        IF P_INSTITUTION.FACTSHEET_URL_LIST IS NOT NULL AND P_INSTITUTION.FACTSHEET_URL_LIST.COUNT > 0 THEN
            IF v_factsheet_id IS NULL THEN
                v_factsheet_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_FACT_SHEET (ID) VALUES (v_factsheet_id);
            END IF;

            FOR i IN P_INSTITUTION.FACTSHEET_URL_LIST.FIRST .. P_INSTITUTION.FACTSHEET_URL_LIST.LAST 
            LOOP
                OPEN exist_lang_it_cursor(P_INSTITUTION.FACTSHEET_URL_LIST(i).LANG, P_INSTITUTION.FACTSHEET_URL_LIST(i).TEXT, v_factsheet_id) ;
                FETCH exist_lang_it_cursor INTO v_lang_item_id;
                CLOSE exist_lang_it_cursor;
                IF v_lang_item_id IS NULL THEN 
                    v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION.FACTSHEET_URL_LIST(i));
                    INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (v_lang_item_id, v_factsheet_id);
                    v_lang_item_id := null;
                END IF; 		
            END LOOP;
		END IF;
		RETURN v_factsheet_id;
	END;

    /*
        Borra los nombres de la institution
    */
    PROCEDURE BORRA_INSTITUTION_NAMES (P_INST_ID_INTEROP IN VARCHAR2) AS
        CURSOR c_names(p_inst_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_INSTITUTION_NAME
			WHERE INSTITUTION_ID = p_inst_id;
    BEGIN
        FOR names_rec IN c_names(P_INST_ID_INTEROP)
		LOOP 
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE NAME_ID = names_rec.NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = names_rec.NAME_ID;
		END LOOP;
    END;   

     /*
		Borra una institución
	*/
	PROCEDURE BORRA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2) AS
        v_fact_sheet_id VARCHAR2(255);
		v_prim_cont_id VARCHAR2(255);
        v_prim_cont_det_id VARCHAR2(255);
        v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS 
			SELECT id, fact_sheet
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);           
        CURSOR c_contacts_inst(p_institution_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_institution_id);
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop, v_fact_sheet_id;
		CLOSE c_inst_p_id;			       
        BORRA_INSTITUTION_NAMES(v_inst_id_interop);    

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE INSTITUTION_ID = P_INSTITUTION_ID;  

        --Borramos Factsheet urls
        PKG_COMMON.BORRA_FACT_SHEET_URL(v_fact_sheet_id);
        DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_fact_sheet_id;

        --Borramos contacts
        FOR contact IN c_contacts_inst(P_INSTITUTION_ID)       
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;

        DELETE FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(P_INSTITUTION_ID);
	END;

	/* Inserta una INSTITUTION en el sistema
		Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_INSTITUTION(P_INSTITUTION IN INSTITUTION, P_INSTITUTION_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_prim_contact_id VARCHAR2(255);
		v_contact_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_inst_id_interop VARCHAR2(255);
		CURSOR c_inst_p_id(p_i_id IN VARCHAR2) IS 
			SELECT id
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_i_id);   
	BEGIN
		OPEN c_inst_p_id(P_INSTITUTION.INSTITUTION_ID);
		FETCH c_inst_p_id INTO v_inst_id_interop;
		CLOSE c_inst_p_id;
		
		IF v_inst_id_interop IS NOT NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución ya existe en el sistema');
			RETURN -1;
		END IF;
	
		v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

		IF v_cod_retorno = 0 THEN

			v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);

			IF v_cod_retorno = 0 THEN
	            -- Insertamos el contacto principal
				v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);

				-- Insertamos la lista de contactos
        IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN     
            FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST 
            LOOP
                v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
            END LOOP;
        END IF;

        -- Insertamos la lista de fact sheet urls
        v_factsheet_id := INSERTA_FACTSHEET_URLS(P_INSTITUTION);

				-- Insertamos la INSTITUTION en EWPCV_INSTITUTION
				P_INSTITUTION_ID := PKG_COMMON.INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ABBREVIATION, P_INSTITUTION.LOGO_URL, v_factsheet_id, P_INSTITUTION.INSTITUTION_NAME_LIST, v_prim_contact_id);
			END IF;

		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_INSTITUTION; 	

    PROCEDURE ACTUALIZA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION) IS
      v_prim_contact_id VARCHAR2(255);
      v_factsheet_id VARCHAR2(255);
      v_contact_id VARCHAR2(255);
      v_inst_id_interop VARCHAR2(255);
     CURSOR c_fsheet_id_inst_id (p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);     
    CURSOR c_contacts(p_inst_hei IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE INSTITUTION_ID = p_inst_hei;
    BEGIN      
        OPEN c_fsheet_id_inst_id(P_INSTITUTION_ID);
		FETCH c_fsheet_id_inst_id INTO v_inst_id_interop, v_factsheet_id;
		CLOSE c_fsheet_id_inst_id;

        BORRA_INSTITUTION_NAMES(v_inst_id_interop);
        PKG_COMMON.INSERTA_INSTITUTION_NAMES(v_inst_id_interop, P_INSTITUTION.INSTITUTION_NAME_LIST);

        UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE ID = v_inst_id_interop;

        FOR contact IN c_contacts(P_INSTITUTION.INSTITUTION_ID) 
        LOOP
            PKG_COMMON.BORRA_CONTACT(contact.id);
        END LOOP;
        -- Insertamos de nuevo el contacto principal
        v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_INSTITUTION.UNIVERSITY_CONTACT_DETAILS, P_INSTITUTION.INSTITUTION_ID, null);
        -- Insertamos de nuevo la lista de contactos
        IF P_INSTITUTION.CONTACT_DETAILS_LIST IS NOT NULL AND P_INSTITUTION.CONTACT_DETAILS_LIST.COUNT > 0 THEN     
            FOR i IN P_INSTITUTION.CONTACT_DETAILS_LIST.FIRST .. P_INSTITUTION.CONTACT_DETAILS_LIST.LAST 
            LOOP
                v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_INSTITUTION.CONTACT_DETAILS_LIST(i), P_INSTITUTION.INSTITUTION_ID, null);
            END LOOP;  
		END IF; 

		PKG_COMMON.BORRA_FACT_SHEET_URL(v_factsheet_id);
        DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_factsheet_id AND CONTACT_DETAILS_ID IS NULL;
        v_factsheet_id := INSERTA_FACTSHEET_URLS(P_INSTITUTION);

        UPDATE EWPCV_INSTITUTION SET 
            PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id, FACT_SHEET = v_factsheet_id, ABBREVIATION = P_INSTITUTION.ABBREVIATION, LOGO_URL = P_INSTITUTION.LOGO_URL 
            WHERE ID = v_inst_id_interop;
    END;

	/* Actualiza una INSTITUTION del sistema.
        Recibe como parametros: 
        El identificador (Código SCHAC) de la INSTITUTION
		Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION IN INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_prim_contact_id VARCHAR2(255);
        v_count NUMBER := 0;
	BEGIN
		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
	        v_cod_retorno := VALIDA_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	        IF v_cod_retorno = 0 THEN
	          v_cod_retorno := VALIDA_DATOS_INSTITUTION(P_INSTITUTION, P_ERROR_MESSAGE);
	          IF v_cod_retorno = 0 THEN
	              ACTUALIZA_INSTITUTION(P_INSTITUTION_ID, P_INSTITUTION);
	              COMMIT;
	          END IF;
	        END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_INSTITUTION;

	/* Elimina una INSTITUTION del sistema.
		Recibe como parametros: 
        El identificador (SCHAC) de la INSTITUTION
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_count NUMBER := 0;
        v_tiene_fs_o_ounit NUMBER := 0;
	BEGIN

		SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = P_INSTITUTION_ID;
		IF v_count > 0 THEN
            v_tiene_fs_o_ounit := VALIDA_FS_OUNIT(P_INSTITUTION_ID, P_ERROR_MESSAGE);
            IF v_tiene_fs_o_ounit = 0 THEN
                BORRA_INSTITUTION(P_INSTITUTION_ID);
            END IF;
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución no existe');
		END IF;
        COMMIT;
        return v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_INSTITUTION; 	 
END PKG_INSTITUTION;
/*
    ***PKG_LOS
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_LOS AS

  -- TIPOS DE DATOS
  
  /* Objeto que modela un LOS (Campos obligatorios marcados con *)
    	EQF_LEVEL					Nivel descrito por el código EQF
		INSTITUTION_ID *   			Identificador de la institución
		ISCEDF						El código ISCED-F del curso o programa
		LOS_CODE					Código del LOS
		LOS_NAME_LIST*				Lista de nombres. Al menos tiene que tener un nombre
		LOS_DESCRIPTION_LIST		Lista de desripciones
		LOS_URL_LIST				Lista de urls
		ORGANIZATION_UNIT_ID		Identificador de la Organization Unit
		SUBJECT_AREA				El código de área de estudio Erasmus del curso
		LOS_TYPE					Tipo del LOS: 0 - CR (Course) ; 1 - CLS (Class) ; 2 - MOD (Module); 3 - DEP (Degree Programme) 
  */

	TYPE LOS IS RECORD (
		EQF_LEVEL                       NUMBER(3),
		INSTITUTION_ID                  VARCHAR2(255),
		ISCEDF                          VARCHAR2(255),
		LOS_CODE                        VARCHAR2(255),
		LOS_NAME_LIST                   EWP.LANGUAGE_ITEM_LIST,
		LOS_DESCRIPTION_LIST            EWP.LANGUAGE_ITEM_LIST,
		LOS_URL_LIST                    EWP.LANGUAGE_ITEM_LIST,
		ORGANIZATION_UNIT_ID            VARCHAR2(255),
		SUBJECT_AREA                    VARCHAR2(255),
		LOS_TYPE                        NUMBER(10,0)
	);

	TYPE LOS_LIST IS TABLE OF LOS;

	-- FUNCIONES 

	/* Inserta un LOS en el sistema
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un id del padre del los
		Admite un parametro de salida con el identificador del LOS en el sistema
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_LOS(P_LOS IN LOS, P_LOS_PARENT_ID IN VARCHAR2, P_LOS_ID OUT VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un LOS en el sistema
	    Admite el id del LOS
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LOS (P_LOS_ID IN VARCHAR2, P_LOS IN LOS, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

	/* Elimina un LOS del sistema y sus hijos
	    Admite el id del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LOS(P_LOS_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Cambia de padre a un LOS del sistema.
	    Admite el id del LOS
	    Admite el id del antiguo padre del LOS
	    Admite el id del nuevo padre del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CHANGE_PARENT(P_LOS_ID IN VARCHAR2, P_LOS_OLD_PARENT_ID IN VARCHAR2, P_LOS_NEW_PARENT_ID IN VARCHAR2, 
    P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

	FUNCTION GET_LOS_TYPE(P_TYPE_ID NUMBER) RETURN VARCHAR2;
	
	FUNCTION VALIDA_LOS_TYPE(P_TYPE NUMBER) RETURN NUMBER;
	
	FUNCTION VALIDA_LOS_ID(P_LOS_ID VARCHAR2) RETURN NUMBER;
	
	FUNCTION GET_LOS_TYPE_BY_LOS_ID(P_LOS_ID IN VARCHAR2) RETURN NUMBER;
END PKG_LOS;

/

CREATE OR REPLACE PACKAGE BODY EWP.PKG_LOS AS

  /* Devuelve el código del tipo del LOS */
  FUNCTION GET_LOS_TYPE(P_TYPE_ID NUMBER) RETURN VARCHAR2 AS
    v_resultado VARCHAR2(10);
  BEGIN
    IF P_TYPE_ID = 3 THEN
      v_resultado := 'DEP';
    ELSIF P_TYPE_ID = 2 THEN
      v_resultado := 'MOD';
    ELSIF P_TYPE_ID = 0 THEN
      v_resultado := 'CR';
    ELSIF P_TYPE_ID = 1 THEN
      v_resultado := 'CLS';
    ELSE
      v_resultado := '';
    END IF;
    RETURN v_resultado;
  END GET_LOS_TYPE;

  FUNCTION GET_LOS_TYPE_BY_LOS_ID(P_LOS_ID IN VARCHAR2) RETURN NUMBER AS
    v_type_char VARCHAR2(255);
    v_type NUMBER(10,0);
  BEGIN
    v_type_char := REGEXP_SUBSTR(P_LOS_ID, '(DEP|MOD|CR|CLS)/', 1);
    v_type_char := REPLACE(v_type_char, '/', '');
    
    IF v_type_char = 'DEP' THEN
      v_type := 3;
    ELSIF v_type_char = 'MOD' THEN
      v_type := 2;
    ELSIF v_type_char = 'CR' THEN
      v_type := 0;
    ELSIF v_type_char = 'CLS' THEN
      v_type := 1;
    ELSE
      v_type := -1;
    END IF;
    RETURN v_type;
  END GET_LOS_TYPE_BY_LOS_ID;

   /* Comprueba si existe el LOS */
  FUNCTION EXISTE_LOS(P_LOS_ID VARCHAR2, P_LOS_PARENT_ID VARCHAR2) RETURN NUMBER AS
    v_count NUMBER(1);
    v_resultado NUMBER(1) := 0;
  BEGIN
    IF P_LOS_PARENT_ID IS NULL THEN
      SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LOS WHERE ID = P_LOS_ID;
    ELSE
      SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LOS_LOS WHERE LOS_ID = P_LOS_PARENT_ID AND OTHER_LOS_ID = P_LOS_ID;
    END IF;

    IF v_count = 0 THEN
      v_resultado := -1;
    END IF;

    RETURN v_resultado;
  END EXISTE_LOS;

  --- VALIDACIONES

 /*	Valida si el padre puede tener un hijo del tipo informado	*/
  FUNCTION VALIDA_PADRE(P_PARENT_TYPE_ID NUMBER, P_CHILD_TYPE_ID NUMBER) RETURN NUMBER AS
    v_resultado NUMBER(1) := -1;
    v_dep_allowed_childs_type VARCHAR2(10) := '023';
    v_mod_allowed_childs_type VARCHAR2(10) := '0';
    v_cr_allowed_childs_type VARCHAR2(10) := '1';
  BEGIN
    IF P_PARENT_TYPE_ID = 3 AND INSTR(v_dep_allowed_childs_type, P_CHILD_TYPE_ID) <> 0 THEN 
      v_resultado := 0;
    END IF;

    IF P_PARENT_TYPE_ID = 2 AND INSTR(v_mod_allowed_childs_type, P_CHILD_TYPE_ID) <> 0 THEN 
      v_resultado := 0;
    END IF;

    IF P_PARENT_TYPE_ID = 0 AND INSTR(v_cr_allowed_childs_type, P_CHILD_TYPE_ID) <> 0 THEN 
      v_resultado := 0;
    END IF;

    IF P_PARENT_TYPE_ID = 1 AND P_CHILD_TYPE_ID IS NULL THEN 
      v_resultado := 0;
    END IF;
    RETURN v_resultado;
  END VALIDA_PADRE;

 /* Valida las dependencias con el LOS */
  FUNCTION VALIDA_DEPENDENCIAS(P_LOS_ID VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_resultado NUMBER(1) := 0;
    v_count NUMBER(16);
  BEGIN
    SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LA_COMPONENT LA
    WHERE LA.LOS_ID = P_LOS_ID 
      OR LA.LOS_ID IN (
        SELECT LL.OTHER_LOS_ID FROM EWP.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = P_LOS_ID
    );

    IF v_count > 0 THEN
      v_resultado := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un Learning Agreement.');
    END IF;

    IF v_resultado = 0 THEN
      SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LOS_LOI LO
      WHERE LO.LOS_ID = P_LOS_ID 
        OR LO.LOS_ID IN (
          SELECT LL.OTHER_LOS_ID FROM EWP.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = P_LOS_ID
      );
      IF v_count > 0 THEN
        v_resultado := -1;
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un LOI.');
      END IF;
    END IF;
    RETURN v_resultado;
  END VALIDA_DEPENDENCIAS;

  /* Valida si el tipo introducido del LOS es correcto */
  FUNCTION VALIDA_LOS_TYPE(P_TYPE NUMBER) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_types VARCHAR2(10) := '0123'; 
  BEGIN
    IF P_TYPE IS NOT NULL AND INSTR(v_types, TO_CHAR(P_TYPE)) = 0 THEN
      v_cod_retorno := -1;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_LOS_TYPE;

  /* Valida si el tipo introducido del LOS es correcto */
  FUNCTION VALIDA_LOS_ID(P_LOS_ID VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF EWP.VALIDA_CADENA(P_LOS_ID, '(^(DEP|MOD|CR|CLS)\/.+$)') < 0 THEN
      v_cod_retorno := -1;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_LOS_ID;

  /* Valida los campos obligatorios del LOS:
   *  - LOS_NAME_LIST, INSTITUTION_ID 
   */
  FUNCTION VALIDA_LOS(P_LOS IN LOS, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_LOS.LOS_NAME_LIST IS NULL OR P_LOS.LOS_NAME_LIST.COUNT = 0 THEN
      v_cod_retorno := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        - LOS_NAME_LIST: Tiene que tener al menos un nombre.');
    END IF;
    
    IF P_LOS.INSTITUTION_ID IS NULL THEN
      v_cod_retorno := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        - INSTITUTION_ID: campo obligatorio.');
    END IF;

    RETURN v_cod_retorno;
  END VALIDA_LOS;

 /* Valida la calidad del dato en el objeto LOS  */
  FUNCTION VALIDA_DATOS_LOS(P_LOS IN LOS, P_LOS_PARENT_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno 	NUMBER(1) := 0;
    v_count_ins		NUMBER(10) := 0;
    v_count_ounit	NUMBER(10) := 0; 
    v_type_parent 	NUMBER(1) := 0;
    CURSOR c_los(p_los_id IN VARCHAR2) IS 
      SELECT "TYPE"
      FROM EWP.EWPCV_LOS
      WHERE ID = p_los_id;
  BEGIN
    IF VALIDA_LOS_TYPE(P_LOS.LOS_TYPE) < 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        - TYPE: El tipo introducido no es válido. 0 - "Course" , 1 - "Class" , 2 - "Module", 3 - "Degree Programme"');
      v_cod_retorno := -1;
    END IF;

    IF P_LOS.INSTITUTION_ID IS NOT NULL THEN
      SELECT COUNT(1) INTO v_count_ins FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS WHERE LOWER(SCHAC) = LOWER(P_LOS.INSTITUTION_ID);

      IF v_count_ins = 0 THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - INSTITUTION_ID: No existe la institución introducida en el sistema');
        v_cod_retorno := -1;
      ELSE
        IF P_LOS.ORGANIZATION_UNIT_ID IS NOT NULL THEN
          SELECT COUNT(1) INTO v_count_ounit FROM EWP.EWPCV_ORGANIZATION_UNIT OU 
          INNER JOIN EWP.EWPCV_INST_ORG_UNIT IOU ON IOU.ORGANIZATION_UNITS_ID = OU.ID 
          INNER JOIN EWP.EWPCV_INSTITUTION EI ON EI.ID = IOU.INSTITUTION_ID 
          WHERE OU.ID = P_LOS.ORGANIZATION_UNIT_ID
          AND LOWER(EI.INSTITUTION_ID) = LOWER(P_LOS.INSTITUTION_ID);
  
          IF v_count_ounit = 0 THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - ORGANIZATION_UNIT_ID: No existe la organization unit introducida en el sistema');
            v_cod_retorno := -1;
          END IF;
        END IF;
      END IF;
    END IF;
    
    IF P_LOS.ORGANIZATION_UNIT_ID IS NOT NULL THEN
        SELECT COUNT(1) INTO v_count_ounit FROM EWP.EWPCV_ORGANIZATION_UNIT OU
        WHERE OU.ID = P_LOS.ORGANIZATION_UNIT_ID;
  
        IF v_count_ounit = 0 THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - ORGANIZATION_UNIT_ID: No existe la organization unit introducida en el sistema');
          v_cod_retorno := -1;
        END IF;
    END IF;

    IF v_cod_retorno = 0 AND P_LOS_PARENT_ID IS NOT NULL THEN
        OPEN c_los(P_LOS_PARENT_ID);
        FETCH c_los INTO v_type_parent;
        CLOSE c_los;

        IF v_type_parent IS NOT NULL AND VALIDA_PADRE(v_type_parent, P_LOS.LOS_TYPE) < 0 THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - TYPE: El padre indicado no puede contener hijos con el tipo introducido');
          v_cod_retorno := -1;
        END IF;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_DATOS_LOS;


  -- INSERCIONES
 
  /* Inserta el listado de nombres del LOS */
  PROCEDURE INSERTA_NAMES(P_LOS_ID IN VARCHAR2, P_LOS_NAME_LIST IN EWP.LANGUAGE_ITEM_LIST) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_LOS_NAME_LIST IS NOT NULL AND P_LOS_NAME_LIST.COUNT > 0 THEN
      FOR i IN P_LOS_NAME_LIST.FIRST .. P_LOS_NAME_LIST.LAST
      LOOP
        v_id := EWP.PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_LOS_NAME_LIST(i));
        INSERT INTO EWP.EWPCV_LOS_NAME (LOS_ID, NAME_ID) VALUES (P_LOS_ID, v_id);
      END LOOP;
     END IF;
  END INSERTA_NAMES;

   /* Inserta el listado de descripciones del LOS */
  PROCEDURE INSERTA_DESCRIPTIONS(P_LOS_ID IN VARCHAR2, P_LOS_DESCRIPTION_LIST IN EWP.LANGUAGE_ITEM_LIST) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_LOS_DESCRIPTION_LIST IS NOT NULL AND P_LOS_DESCRIPTION_LIST.COUNT > 0 THEN
      FOR i IN P_LOS_DESCRIPTION_LIST.FIRST .. P_LOS_DESCRIPTION_LIST.LAST
      LOOP
        v_id := EWP.PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_LOS_DESCRIPTION_LIST(i));
        INSERT INTO EWP.EWPCV_LOS_DESCRIPTION (LOS_ID, DESCRIPTION_ID) VALUES (P_LOS_ID, v_id);
      END LOOP;
    END IF;
  END INSERTA_DESCRIPTIONS;

   /* Inserta el listado de urls del LOS */
  PROCEDURE INSERTA_URLS(P_LOS_ID IN VARCHAR2, P_LOS_URL_LIST IN EWP.LANGUAGE_ITEM_LIST) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_LOS_URL_LIST IS NOT NULL AND P_LOS_URL_LIST.COUNT > 0 THEN
	  FOR i IN P_LOS_URL_LIST.FIRST .. P_LOS_URL_LIST.LAST
	  LOOP
	    v_id := EWP.PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_LOS_URL_LIST(i));
	    INSERT INTO EWP.EWPCV_LOS_URLS (LOS_ID, URL_ID) VALUES (P_LOS_ID, v_id);
	  END LOOP;
    END IF;
  END INSERTA_URLS;

   /* Inserta el LOS en la tabla */
  FUNCTION INSERTA_LOS(P_LOS IN LOS, P_LOS_PARENT_ID VARCHAR2) RETURN VARCHAR2 AS
    v_id VARCHAR2(255);
    v_los_type VARCHAR2(10);
   	v_top_level_parent NUMBER(1);
  BEGIN
    v_los_type := GET_LOS_TYPE(P_LOS.LOS_TYPE);
    v_id := v_los_type || '/' || EWP.GENERATE_UUID();
   
   	IF P_LOS_PARENT_ID IS NULL THEN
   		v_top_level_parent := 0;
   	ELSE 
   		v_top_level_parent := 1;
   	END IF;

    INSERT INTO EWP.EWPCV_LOS 
    (ID, EQF_LEVEL, INSTITUTION_ID, 
      ISCEDF, LOS_CODE, ORGANIZATION_UNIT_ID, SUBJECT_AREA, TOP_LEVEL_PARENT, "TYPE")
    VALUES
    (v_id, P_LOS.EQF_LEVEL, P_LOS.INSTITUTION_ID, 
      P_LOS.ISCEDF, P_LOS.LOS_CODE, P_LOS.ORGANIZATION_UNIT_ID, P_LOS.SUBJECT_AREA, v_top_level_parent, P_LOS.LOS_TYPE
    );

    INSERTA_NAMES(v_id, P_LOS.LOS_NAME_LIST);

    INSERTA_DESCRIPTIONS(v_id, P_LOS.LOS_DESCRIPTION_LIST);

    INSERTA_URLS (v_id, P_LOS.LOS_URL_LIST);

    IF P_LOS_PARENT_ID IS NOT NULL THEN
      INSERT INTO EWP.EWPCV_LOS_LOS (LOS_ID, OTHER_LOS_ID) VALUES (P_LOS_PARENT_ID, v_id);
    END IF;

    RETURN v_id;
  END INSERTA_LOS;

	/* Inserta un LOS en el sistema
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un id del padre del los
		Admite un parametro de salida con el identificador del LOS en el sistema
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
  FUNCTION INSERT_LOS(P_LOS IN LOS, P_LOS_PARENT_ID IN VARCHAR2, P_LOS_ID OUT VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_c_id VARCHAR2(255);
    v_type NUMBER(10, 0);
    v_allowed_parent NUMBER := 0;
  BEGIN
    v_cod_retorno := VALIDA_LOS(P_LOS, P_ERROR_MESSAGE);

    IF v_cod_retorno = 0 THEN
      v_cod_retorno := VALIDA_DATOS_LOS(P_LOS, P_LOS_PARENT_ID, P_ERROR_MESSAGE);    
    END IF;

    IF v_cod_retorno = 0 THEN
      P_LOS_ID := INSERTA_LOS(P_LOS, P_LOS_PARENT_ID);
    END IF;

    IF v_cod_retorno = 0 THEN
      COMMIT;
    END IF;
    RETURN v_cod_retorno;
    EXCEPTION 
      WHEN OTHERS THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
      ROLLBACK;
      RETURN -1;
  END INSERT_LOS;


  ----- BORRADOS
 
   /* Borra el listado de nombres del LOS */
  PROCEDURE BORRA_NAMES(P_LOS_ID VARCHAR2) IS
    CURSOR c_names(p_los_id IN VARCHAR2) IS 
      SELECT NAME_ID
      FROM EWP.EWPCV_LOS_NAME
      WHERE LOS_ID = p_los_id;
  BEGIN
    FOR name_rec IN c_names(P_LOS_ID)
    LOOP 
      DELETE FROM EWP.EWPCV_LOS_NAME WHERE NAME_ID = name_rec.NAME_ID;
      DELETE FROM EWP.EWPCV_LANGUAGE_ITEM WHERE ID = name_rec.NAME_ID;
    END LOOP;
  END BORRA_NAMES;

   /* Borra el listado de descripciones del LOS */
  PROCEDURE BORRA_DESCRIPTIONS(P_LOS_ID VARCHAR2) IS
    CURSOR c_desc(p_los_id IN VARCHAR2) IS 
      SELECT DESCRIPTION_ID
      FROM EWP.EWPCV_LOS_DESCRIPTION
      WHERE LOS_ID = p_los_id;
  BEGIN
    FOR desc_rec IN c_desc(P_LOS_ID)
    LOOP 
      DELETE FROM EWP.EWPCV_LOS_DESCRIPTION WHERE DESCRIPTION_ID = desc_rec.DESCRIPTION_ID;
      DELETE FROM EWP.EWPCV_LANGUAGE_ITEM WHERE ID = desc_rec.DESCRIPTION_ID;
    END LOOP;
  END BORRA_DESCRIPTIONS;

   /* Borra el listado de urls del LOS */
  PROCEDURE BORRA_URLS(P_LOS_ID VARCHAR2) IS
    CURSOR c_urls(p_los_id IN VARCHAR2) IS 
      SELECT URL_ID
      FROM EWP.EWPCV_LOS_URLS
      WHERE LOS_ID = p_los_id;
  BEGIN
    FOR url_rec IN c_urls(P_LOS_ID)
    LOOP 
      DELETE FROM EWP.EWPCV_LOS_URLS WHERE URL_ID = url_rec.URL_ID;
      DELETE FROM EWP.EWPCV_LANGUAGE_ITEM WHERE ID = url_rec.URL_ID;
    END LOOP;
  END BORRA_URLS;


   /* Elimina el LOS de la base de datos */
  PROCEDURE BORRA_LOS(P_LOS_ID IN VARCHAR2) IS
    CURSOR c_los(p_los_id IN VARCHAR2) IS 
      SELECT OTHER_LOS_ID 
      FROM EWP.EWPCV_LOS_LOS
      WHERE LOS_ID = p_los_id;
  BEGIN
	IF P_LOS_ID IS NOT NULL THEN
      BORRA_NAMES(P_LOS_ID);

      BORRA_DESCRIPTIONS(P_LOS_ID);

      BORRA_URLS(P_LOS_ID);

      FOR los_rec IN c_los(P_LOS_ID)
      LOOP 
        DELETE FROM EWP.EWPCV_LOS_LOS WHERE OTHER_LOS_ID = los_rec.OTHER_LOS_ID;
        BORRA_LOS(los_rec.OTHER_LOS_ID);
      END LOOP;
     
      DELETE FROM EWP.EWPCV_LOS WHERE ID = P_LOS_ID;
	END IF;
  END BORRA_LOS;

 	/* Elimina un LOS del sistema y sus hijos
	    Admite el id del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
  FUNCTION DELETE_LOS(P_LOS_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_count NUMBER(1);
    v_cod_retorno NUMBER(1) := 0;
	v_max_mensaje VARCHAR2(32767 CHAR); 
  BEGIN
    SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LOS WHERE ID = P_LOS_ID;
    IF v_count > 0 THEN
      IF VALIDA_DEPENDENCIAS(P_LOS_ID, P_ERROR_MESSAGE) < 0 THEN
        v_max_mensaje := 'No se ha eliminado el LOS indicado' 
          || P_ERROR_MESSAGE;
		EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
        v_cod_retorno := -1;
      ELSE
        BORRA_LOS(P_LOS_ID);
      END IF;
    ELSE
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El LOS indicado no existe');
      v_cod_retorno := -1;
    END IF;
    IF v_cod_retorno = 0 THEN
      COMMIT;
    END IF;
    RETURN v_cod_retorno;
    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END DELETE_LOS;

  -- ACTUALIZACIONES

 /* Actualiza el LOS en la base de datos */
  PROCEDURE ACTUALIZA_LOS(P_LOS_ID IN VARCHAR2, P_LOS IN LOS) IS
  BEGIN
    BORRA_NAMES(P_LOS_ID);
    INSERTA_NAMES(P_LOS_ID, P_LOS.LOS_NAME_LIST);

    BORRA_DESCRIPTIONS(P_LOS_ID);
    INSERTA_DESCRIPTIONS(P_LOS_ID, P_LOS.LOS_DESCRIPTION_LIST);

    BORRA_URLS(P_LOS_ID);
    INSERTA_URLS (P_LOS_ID, P_LOS.LOS_URL_LIST);

    UPDATE EWP.EWPCV_LOS SET EQF_LEVEL = P_LOS.EQF_LEVEL, INSTITUTION_ID = P_LOS.INSTITUTION_ID, ISCEDF = P_LOS.ISCEDF, 
    LOS_CODE = P_LOS.LOS_CODE, ORGANIZATION_UNIT_ID = P_LOS.ORGANIZATION_UNIT_ID,
    SUBJECT_AREA = P_LOS.SUBJECT_AREA WHERE ID = P_LOS_ID;
  END ACTUALIZA_LOS;

	/* Actualiza un LOS en el sistema
	    Admite el id del LOS
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
  FUNCTION UPDATE_LOS (P_LOS_ID IN VARCHAR2, P_LOS IN LOS, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER(1) := 0;
    v_count NUMBER(1);
  BEGIN
    SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_LOS WHERE ID = P_LOS_ID AND "TYPE" = P_LOS.LOS_TYPE;
    IF v_count > 0 THEN
      v_cod_retorno := VALIDA_LOS(P_LOS, P_ERROR_MESSAGE);

      IF v_cod_retorno = 0 THEN
        v_cod_retorno := VALIDA_DATOS_LOS(P_LOS, NULL, P_ERROR_MESSAGE);    
      END IF;

      IF v_cod_retorno = 0 THEN
        ACTUALIZA_LOS(P_LOS_ID, P_LOS);
      END IF;
    ELSE 
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El LOS indicado no existe');
      v_cod_retorno := -1;
    END IF;

    IF v_cod_retorno = 0 THEN
      COMMIT;
    END IF;

    RETURN v_cod_retorno;
    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END UPDATE_LOS;


	/* Cambia de padre a un LOS del sistema.
	    Admite el id del LOS
	    Admite el id del antiguo padre del LOS
	    Admite el id del nuevo padre del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
  FUNCTION CHANGE_PARENT(P_LOS_ID IN VARCHAR2, P_LOS_OLD_PARENT_ID IN VARCHAR2, P_LOS_NEW_PARENT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno 	NUMBER(1) := 0;
    v_existe_los	NUMBER(1) := 0;
    v_type_parent 	NUMBER(1) := 0;
    v_type		 	NUMBER(1) := 0;
    CURSOR c_los(p_los_id IN VARCHAR2) IS 
      SELECT "TYPE"
      FROM EWP.EWPCV_LOS
      WHERE ID = p_los_id;
  BEGIN

    IF EXISTE_LOS(P_LOS_ID, P_LOS_OLD_PARENT_ID) = 0 THEN
      SELECT (
          SELECT "TYPE" FROM EWPCV_LOS WHERE ID = P_LOS_ID
      ) INTO v_type FROM DUAL;

      IF P_LOS_NEW_PARENT_ID IS NOT NULL THEN
        IF EXISTE_LOS(P_LOS_NEW_PARENT_ID, NULL) = 0 THEN
          OPEN c_los(P_LOS_NEW_PARENT_ID);
          FETCH c_los INTO v_type_parent;
          CLOSE c_los;

          IF v_type_parent IS NOT NULL AND VALIDA_PADRE(v_type_parent, v_type) < 0 THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - TYPE: El padre indicado no puede contener hijos con el tipo introducido');
            v_cod_retorno := -1;
          END IF;			

          IF v_cod_retorno = 0 THEN
            UPDATE EWP.EWPCV_LOS_LOS SET LOS_ID = P_LOS_NEW_PARENT_ID
            WHERE LOS_ID = P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = P_LOS_ID;
            UPDATE EWP.EWPCV_LOS SET TOP_LEVEL_PARENT = 0 WHERE ID = P_LOS_ID;
          END IF;
        ELSE
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - No existe el LOS indicado como nuevo padre en el sistema');	
		  v_cod_retorno := -1;
        END IF;
      ELSE
        IF v_type = 1 THEN
          DELETE FROM EWPCV_LOS_LOS WHERE LOS_ID = P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = P_LOS_ID;
          UPDATE EWPCV_LOS SET TOP_LEVEL_PARENT = 1 WHERE ID = P_LOS_ID;
        ELSE 
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - Se ha de indicar el id del nuevo padre del LOS');
          v_cod_retorno := -1;
        END IF;
      END IF;
    ELSE
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        - No existe el LOS indicado en el sistema');
      v_cod_retorno := -1;
    END IF;

    IF v_cod_retorno = 0 THEN
      COMMIT;
    END IF;

    RETURN v_cod_retorno;
    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END CHANGE_PARENT;	
END PKG_LOS;

/*
    ***PKG_MOBILITY_LA
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_MOBILITY_LA AS

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  

	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
            START_DATE                  Fecha inicio del LOI
            END_DATE                    Fecha fin del LOI
	*/
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_ID                     VARCHAR2(255),
		LOS_CODE				   VARCHAR2(255 CHAR),
		LOS_TYPE                   NUMBER(10,0),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR),
        START_DATE				   DATE,
        END_DATE				   DATE
	  );

	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );

	/* Objeto que modela el objeto con la foto del estudiante. Campos obligatorios marcados con *.
		URL*    Url del fichero con la foto.
		PHOTO_SIZE   Tamaño de la foto, formato width x height, ej:35x45.
		PHOTO_DATE     Fecha real en la que se agregó la foto.
		PHOTO_PUBLIC   Indica si la foto es pública, 0 - privada, 1 - pública

	*/
	TYPE PHOTO_URL IS RECORD
	(
	URL                VARCHAR2(255),
	PHOTO_SIZE         VARCHAR2(255),
	PHOTO_DATE         DATE,
	PHOTO_PUBLIC       NUMBER(1,0)
	);

	/* Lista con los objetos de las fotos del estudiante. */ 
	TYPE PHOTO_URL_LIST IS TABLE OF PHOTO_URL INDEX BY BINARY_INTEGER ;

	/*Datos del estudiante. Campos obligatorios marcados con *.
		GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
		CONTACT					Informacion del estudiante
		PHOTO_URL_LIST			Listado de objetos con las fotos del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON,
		"PHOTO_URL_LIST"  PKG_MOBILITY_LA.PHOTO_URL_LIST
	  );	 

  /* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
		SENDING_INSTITUTION*    Datos de la institución que envia al estudiante.
		RECEIVING_INSTITUTION*    Datos de la institución que recibe al estudiante.
		ACTUAL_ARRIVAL_DATE     Fecha real de llegada del estudiante aldestino.
		ACTUAL_DEPATURE_DATE    Fecha real de salida del estudiante del origen.
		PLANED_ARRIVAL_DATE*    Fecha prevista de llegada del estudiante aldestino.
		PLANED_DEPATURE_DATE*   Fecha prevista de salida del estudiante del origen.
		IIA_ID            UUID del IIA asociado a la movilidad
		RECEIVING_IIA_ID  UUID del IIA asociado a la movilidad
		COOPERATION_CONDITION_ID  identificador de la condicion de coperacion asociada la movilidad
		STUDENT*          Datos del estudiante
		SUBJECT_AREA*       Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
		STUDENT_LANGUAGE_SKILLS*  Niveles de idiomas que declara el estudiante.
		SENDER_CONTACT*       Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
		SENDER_ADMV_CONTACT     Informacion de contacto de la administrativo en la institucion origen
		RECEIVER_CONTACT*     Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
		RECEIVER_ADMV_CONTACT   Informacion de contacto de la administrativo en la institucion destino
		MOBILITY_TYPE       Tipo de movilidad, puede tomar los siquientes valores:
					Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
		EQF_LEVEL_NOMINATION * Nivel de estudios del estudiante. 
		EQF_LEVEL_DEPARTURE*   Nivel de estudios del estudiante. 
		ACADEMIC_TERM*  Informacion del periodo academico de la asignatura. 
					  Requerido en componentes estudiados y reconocidos y opcional en los virtuales
		STATUS_OUTGOING Estado de la movilidad Outgoing.
					  0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED. Si no se informa, por defecto es 2 - NOMINATION.
  */
	TYPE MOBILITY IS RECORD
	(
		SENDING_INSTITUTION     EWP.INSTITUTION,
		RECEIVING_INSTITUTION   EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE     DATE,
		ACTUAL_DEPATURE_DATE    DATE,
		PLANED_ARRIVAL_DATE     DATE,
		PLANED_DEPATURE_DATE    DATE,
		IIA_ID            VARCHAR(255 CHAR),
		RECEIVING_IIA_ID VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID  VARCHAR(255 CHAR),
		STUDENT           PKG_MOBILITY_LA.STUDENT,
		SUBJECT_AREA        EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS   EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT        EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT     EWP.CONTACT_PERSON,
		RECEIVER_CONTACT      EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT   EWP.CONTACT_PERSON,
		MOBILITY_TYPE       VARCHAR2 (255),
		EQF_LEVEL_NOMINATION NUMBER(3,0),
		EQF_LEVEL_DEPARTURE NUMBER(3,0),
		ACADEMIC_TERM EWP.ACADEMIC_TERM,
		STATUS_OUTGOING  NUMBER(7,0)
	);

	/* Objeto que modela un acuerdo de enseñanza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					TIMESTAMP,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					BLOB
	  );

	/* Objeto que modela un acuerdo de enseñanza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisión del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );

	-- FUNCIONES 

	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Cancela una movilidad
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;

	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2, 
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  	

END PKG_MOBILITY_LA;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_MOBILITY_LA AS

	FUNCTION ES_OMOBILITY(P_MOBILITY IN MOBILITY, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_resultado NUMBER(1,0) := 0;
	BEGIN
		IF P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID <> P_HEI_TO_NOTIFY THEN
			v_resultado := -1;
		END IF;

		RETURN v_resultado;
	END;
	
  FUNCTION GENERA_LOI_ID(P_LOS_TYPE IN VARCHAR2 ) RETURN VARCHAR2 AS
  BEGIN
    IF P_LOS_TYPE IS NOT NULL AND LENGTH(EWP.PKG_LOS.GET_LOS_TYPE(P_LOS_TYPE)) > 0 THEN
      RETURN	EWP.PKG_LOS.GET_LOS_TYPE(P_LOS_TYPE) || 'I/' || EWP.GENERATE_UUID();
    ELSE
      RETURN NULL;
    END IF;
  END GENERA_LOI_ID;

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************

	/*
		Valida los campos obligatorios de los periodos academicos
			academic_year, institution_id, term_number, total_terms
	*/
	FUNCTION VALIDA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
	BEGIN
		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-ACADEMIC_YEAR');
		END IF;

		IF P_ACADEMIC_TERM.TERM_NUMBER IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-TERM_NUMBER');
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-TOTAL_TERMS');
		END IF;

        IF P_ACADEMIC_TERM.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-INSTITUTION_ID');
        END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_error_msg VARCHAR2(2000);
       	v_term_number NUMBER(1);
        v_total_term_number NUMBER(1);
	BEGIN
		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NOT NULL AND VALIDA_ACADEMIC_YEAR(P_ACADEMIC_TERM.ACADEMIC_YEAR, v_error_msg) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
							-ACADEMIC_YEAR: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_msg);
			v_error_msg := '';
		END IF;

		IF P_ACADEMIC_TERM.TERM_NUMBER IS NOT NULL THEN
			v_term_number := P_ACADEMIC_TERM.TERM_NUMBER;
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NOT NULL THEN
			v_total_term_number := P_ACADEMIC_TERM.TOTAL_TERMS;
		END IF;

		IF v_term_number IS NOT NULL AND v_total_term_number IS NOT NULL AND v_term_number > v_total_term_number THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
							-TERM_NUMBER/TOTAL_TERMS: El valor de term_number no puede ser mayor al valor de total_terms');
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una subject area:
			isced_code
	*/
	FUNCTION VALIDA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_SUBJECT_AREA.ISCED_CODE	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-ISCED_CODE');
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una language skill:
			lang, cefr_level
	*/
	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_LANGUAGE_SKILL.LANG	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-LANG');
		END IF;

		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-CEFR_LEVEL');
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una institucion:
			Institution_id, organization_unit_code
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
        v_ounit_count NUMBER := 0;
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT ID
            FROM EWPCV_INSTITUTION
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;

		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-INSTITUTION_ID');
        END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una persona de contacto:
			GIVEN_NAME, FAMILY_NAME, EMAIL_LIST
	*/
	FUNCTION VALIDA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CONTACT_PERSON.GIVEN_NAME	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-GIVEN_NAME');
		END IF;

		IF P_CONTACT_PERSON.FAMILY_NAME	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-FAMILY_NAME');
		END IF;

		IF P_CONTACT_PERSON.CONTACT.EMAIL IS NULL OR P_CONTACT_PERSON.CONTACT.EMAIL.COUNT = 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-CONTACT.EMAIL');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
	Valida los campos obligatorios de la foto del estudiante:
	  url
	*/
	FUNCTION VALIDA_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.URL IS NULL OR LENGTH(P_PHOTO_URL.URL) = 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '-URL');
			v_cod_retorno := -1;
		END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.PHOTO_SIZE IS NOT NULL AND NOT REGEXP_LIKE(P_PHOTO_URL.PHOTO_SIZE, '^\d*x\d*$') THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '-PHOTO_SIZE');
		END IF;
		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_PHOTO_URL_LIST(P_PHOTO_URL_LIST IN PKG_MOBILITY_LA.PHOTO_URL_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
		v_error NUMBER := 0;
		v_mensaje VARCHAR2(2000);
	BEGIN
		IF P_PHOTO_URL_LIST IS NOT NULL AND P_PHOTO_URL_LIST.COUNT > 0 THEN
			FOR i IN P_PHOTO_URL_LIST.FIRST .. P_PHOTO_URL_LIST.LAST
			LOOP
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;

				v_cod_retorno := VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL_LIST(i), v_mensaje);
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;

				IF v_error = 1 THEN
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '-PHOTO_URL_LIST(' || i || ')');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje);
				END IF;
				v_mensaje := '';
			END LOOP;
		END IF;

		v_cod_retorno := CASE WHEN v_error = 1 THEN -1 ELSE 0 END;

		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_DATOS_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
	BEGIN

       	IF P_STUDENT.CONTACT_PERSON IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-CONTACT_PERSON: ' || v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un estudiante:
			global_id
	*/
	FUNCTION VALIDA_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
		v_mensaje_photo_url VARCHAR2(2000);
	BEGIN

		IF P_STUDENT.GLOBAL_ID	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-GLOBAL_ID');
		END IF;
		IF VALIDA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_mensaje_contact) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_contact);
		END IF;

		IF VALIDA_PHOTO_URL_LIST(P_STUDENT.PHOTO_URL_LIST, v_mensaje_photo_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				PHOTO_URL_LIST: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_photo_url);
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de una movilidad y de los objetos anidados:
			planed_arrival_date,planed_depature_date, iia_code, status, sending_institution, receiving_institution, student
	*/
	FUNCTION VALIDA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_s_institution VARCHAR2(2000);
		v_mensaje_r_institution VARCHAR2(2000);
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_contact VARCHAR2(2000);
		v_mensaje_r_contact VARCHAR2(2000);
		v_mensaje_acaterm VARCHAR2(2000);
	BEGIN

		IF P_MOBILITY.PLANED_ARRIVAL_DATE	IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-PLANED_ARRIVAL_DATE');
		END IF;

		IF P_MOBILITY.PLANED_DEPATURE_DATE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-PLANED_DEPATURE_DATE');
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.SENDING_INSTITUTION, v_mensaje_s_institution) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					SENDING_INSTITUTION: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_s_institution);
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.RECEIVING_INSTITUTION, v_mensaje_r_institution) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					RECEIVING_INSTITUTION: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_r_institution);
		END IF;

		IF VALIDA_STUDENT(P_MOBILITY.STUDENT, v_mensaje_student) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					STUDENT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_student);
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_mensaje_s_contact) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					SENDER_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_s_contact);
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_mensaje_r_contact) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					RECEIVER_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_r_contact);
		END IF;

		IF VALIDA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-ACADEMIC_TERM: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_acaterm);
		END IF;

		IF P_MOBILITY.EQF_LEVEL_NOMINATION IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-EQF_LEVEL_NOMINATION');
		END IF;

		IF P_MOBILITY.EQF_LEVEL_DEPARTURE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-EQF_LEVEL_DEPARTURE');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida que el iias y la coop condition indicadas existan
	*/
	FUNCTION VALIDA_MOBILITY_IIA(P_IIA_ID IN VARCHAR2, P_IS_REMOTE NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
	BEGIN
		IF P_IIA_ID IS NOT NULL THEN
		  SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_IIA WHERE UPPER(ID) = UPPER(P_IIA_ID) AND IS_REMOTE = P_IS_REMOTE;

			IF v_count = 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					El acuerdo interistitucional indicado no existe en el sistema.');
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;

	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
	FUNCTION VALIDA_DATOS_MOBILITY_INST(P_S_INSTITUTION IN EWP.INSTITUTION, P_R_INSTITUTION  IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		-- validacion de sender
		SELECT COUNT(1) INTO v_count
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS
			WHERE UPPER(SCHAC) = UPPER(P_S_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			La institución '||P_S_INSTITUTION.INSTITUTION_ID||' informada como sender institution no existe en el sistema.');
		ELSIF P_S_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_S_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_S_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				La unidad organizativa '||P_S_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_S_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.');
			END IF;
		END IF;

		-- validacion de receiver
		SELECT COUNT(1) INTO v_count
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS
			WHERE UPPER(SCHAC) = UPPER(P_R_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			La institución '||P_R_INSTITUTION.INSTITUTION_ID||' informada como receiver institution no existe en el sistema.');
		ELSIF P_R_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_R_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_R_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				La unidad organizativa '||P_R_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_R_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.');
			END IF;
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
	*/
	FUNCTION VALIDA_DATOS_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_error_message VARCHAR2(2000);
		v_mensaje_contact VARCHAR2(2000);
	BEGIN
		IF P_MOBILITY.IIA_ID IS NOT NULL THEN
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.IIA_ID, 0, P_ERROR_MESSAGE);
		END IF;

		IF v_cod_retorno = 0 AND P_MOBILITY.RECEIVING_IIA_ID IS NOT NULL THEN
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.RECEIVING_IIA_ID, 1, P_ERROR_MESSAGE);
		END IF;

		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_MOBILITY_INST(P_MOBILITY.SENDING_INSTITUTION, P_MOBILITY.RECEIVING_INSTITUTION, P_ERROR_MESSAGE);
		END IF;

		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_STUDENT(P_MOBILITY.STUDENT, P_ERROR_MESSAGE);
		END IF;

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT > 0 THEN
			FOR I IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				IF PKG_COMMON.VALIDA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i), v_error_message) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						- STUDENT_LANGUAGE_SKILLS['|| i||']: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
					v_error_message := '';
				END IF;
			END LOOP;
		END IF;

	    IF P_MOBILITY.SENDER_CONTACT IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE,  '
				-SENDER_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_MOBILITY.SENDER_ADMV_CONTACT IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-SENDER_ADMV_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_MOBILITY.RECEIVER_CONTACT IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-RECEIVER_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_MOBILITY.RECEIVER_ADMV_CONTACT IS NOT NULL
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT, v_error_message) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-RECEIVER_ADMV_CONTACT: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

--		IF v_cod_retorno = 0 THEN
--			v_cod_retorno := VALIDA_DATOS_STUDENT(P_MOBILITY.STUDENT, P_ERROR_MESSAGE);
--		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una firma
			signer_name, signer_position, signer_email, timestamp,  signature
	*/
	FUNCTION VALIDA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_SIGNATURE.SIGNER_NAME IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SIGNER_NAME');
		END IF;

		IF P_SIGNATURE.SIGNER_POSITION IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SIGNER_POSITION');
		END IF;

		IF P_SIGNATURE.SIGNER_EMAIL IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SIGNER_EMAIL');
		END IF;

		IF P_SIGNATURE.SIGN_DATE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-SIGN_DATE');
		END IF;

		RETURN v_cod_retorno;
	END;

    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN
        IF UPPER(P_INSTITUTION_ID) <> UPPER(P_ACADEMIC_TERM.INSTITUTION_ID) THEN
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                        -INSTITUTION_ID: La institucion '|| P_ACADEMIC_TERM.INSTITUTION_ID || ' del periodo academico del componente no coincide con la institucion ' || P_INSTITUTION_ID);
		ELSIF P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN
                v_cod_retorno := -1;
                 EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                        -OUNIT_CODE: La unidad organizativa ' || P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE || ' vinculada a ' || P_INSTITUTION_ID || ' no existe en el sistema.');
            END IF;
        END IF;
		RETURN v_cod_retorno;
	END;

    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_OUNIT_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REV IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
        v_send_inst_id VARCHAR2(255);
        v_receiv_inst_id VARCHAR2(255);
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
        CURSOR c_s_inst_r_inst(p_mob_id IN VARCHAR2, p_mob_rev IN NUMBER) IS
            SELECT SENDING_INSTITUTION_ID, RECEIVING_INSTITUTION_ID
            FROM EWPCV_MOBILITY
            WHERE ID = p_mob_id
            AND MOBILITY_REVISION = p_mob_rev;
	BEGIN
		IF P_COMPONENTS_LIST IS NOT NULL AND P_COMPONENTS_LIST.COUNT > 0 THEN
			OPEN c_s_inst_r_inst(P_MOBILITY_ID, P_MOBILITY_REV);
			FETCH c_s_inst_r_inst INTO v_send_inst_id, v_receiv_inst_id;
			CLOSE c_s_inst_r_inst;

			FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST
			LOOP
				IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 OR P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 5 THEN
					IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_receiv_inst_id, v_mensaje_acaterm) <> 0 THEN
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-ACADEMIC_TERM: '|| v_mensaje_acaterm;
						v_mensaje_acaterm := '';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
					IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_send_inst_id, v_mensaje_acaterm) <> 0 THEN
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-ACADEMIC_TERM: '|| v_mensaje_acaterm;
						v_mensaje_acaterm := '';
					END IF;
				END IF;
				IF v_cod_retorno_it <> 0 THEN
					v_cod_retorno := v_cod_retorno_it;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						COMPONENT'|| i || ':');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_component);
					v_mensaje_component := '';
					v_cod_retorno_it := 0;
				END IF;
			END LOOP;
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los creditos
			scheme, value
	*/
	FUNCTION VALIDA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CREDIT.SCHEME IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-SCHEME');
		END IF;

		IF P_CREDIT.CREDIT_VALUE IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-VALUE');
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_credit VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
		v_mensaje_error VARCHAR2(2000);
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST
		LOOP

			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE IS NULL THEN
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE NOT IN (0,1,2,3,4) THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE (valor erroneo ' || P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
				IF P_COMPONENTS_LIST(i).RECOGNITION_CONDITIONS IS NOT NULL THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-RECOGNITION_CONDITIONS (No informar para el tipo de componente '|| P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')' ;
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;

			IF P_COMPONENTS_LIST(i).TITLE IS NULL THEN
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-TITLE';
			END IF;

			IF P_COMPONENTS_LIST(i).STATUS IS NOT NULL THEN
				IF P_COMPONENTS_LIST(i).STATUS = 0 THEN
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (3,4,5) THEN
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).STATUS = 1 THEN
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (0,1,2) THEN
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSE
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-STATUS (valor erroneo'|| P_COMPONENTS_LIST(i).STATUS||')';
				END IF;

				IF P_COMPONENTS_LIST(i).REASON_CODE IS NULL THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-REASON_CODE';
				END IF;
			END IF;

			-- Comprobamos el credito
			IF VALIDA_CREDIT(P_COMPONENTS_LIST(i).CREDIT, v_mensaje_credit) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-CREDIT: ' || v_mensaje_credit;
				v_mensaje_credit := '';
			END IF;

			IF v_cod_retorno_it <> 0 THEN
				v_cod_retorno := v_cod_retorno_it;
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					COMPONENT('|| i || '):');
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_component);
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;

		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_DATOS_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_error VARCHAR2(2000);
		v_los_type NUMBER(10,0);
		CURSOR c_los(p_los_id VARCHAR2) IS SELECT "TYPE" FROM EWP.EWPCV_LOS WHERE ID = p_los_id;
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST
		LOOP
			IF VALIDA_DATOS_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_error) <> 0 THEN
				v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
					-P_COMPONENTS_LIST('||i ||'): ' || '
						-ACADEMIC_TERM: ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_error);
					v_mensaje_error := '';
			END IF;

			IF P_COMPONENTS_LIST(i).LOS_ID IS NOT NULL THEN
				OPEN c_los(P_COMPONENTS_LIST(i).LOS_ID);
				FETCH c_los INTO v_los_type;
				CLOSE c_los;

				IF v_los_type IS NULL THEN
					IF EWP.PKG_LOS.VALIDA_LOS_ID(P_COMPONENTS_LIST(i).LOS_ID) < 0 THEN
						v_cod_retorno := -1;
						EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
							-P_COMPONENTS_LIST('||i ||'): ' || '
								-LOS_ID: El LOS_ID introducido no es válido');
						v_mensaje_error := '';
					END IF;
				END IF;
			ELSE
				IF P_COMPONENTS_LIST(i).LOS_TYPE IS NOT NULL AND EWP.PKG_LOS.VALIDA_LOS_TYPE(P_COMPONENTS_LIST(i).LOS_TYPE) < 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
						-P_COMPONENTS_LIST('||i ||'): ' || '
							-LOS_TYPE: El tipo introducido no es válido. 0 - "Course" , 1 - "Class" , 2 - "Module", 3 - "Degree Programme"');
				END IF;
			END IF;
		END LOOP;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_STUDENT_LA(P_STUDENT_LA IN PKG_MOBILITY_LA.STUDENT_LA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
		v_mensaje_photo_url VARCHAR2(2000);
		v_error_message VARCHAR2(2000);
	BEGIN

       	IF P_STUDENT_LA.CITIZENSHIP IS NOT NULL
		AND EWP.VALIDA_COUNTRY_CODE(P_STUDENT_LA.CITIZENSHIP, v_mensaje_contact) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-CITIZENSHIP: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

       	IF P_STUDENT_LA.GENDER IS NOT NULL
		AND EWP.VALIDA_GENDER(P_STUDENT_LA.GENDER, v_mensaje_contact) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-GENDER: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida la calidad de datos de un Learning Agreement
			mobility,
	*/
	FUNCTION VALIDA_DATOS_LA(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
	BEGIN
		IF VALIDA_DATOS_STUDENT_LA(P_LA.STUDENT_LA, v_mensaje_student) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				- STUDENT_LA:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_student);
			v_mensaje_student := '';
		END IF;


		IF P_LA.STUDENT_SIGNATURE.SIGNER_EMAIL IS NOT NULL
		AND VALIDA_EMAIL(P_LA.STUDENT_SIGNATURE.SIGNER_EMAIL, v_mensaje_student) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				- STUDENT_SIGNATURE.SIGNER_EMAIL:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_student);
			v_mensaje_student := '';
		END IF;

		IF P_LA.SENDING_HEI_SIGNATURE.SIGNER_EMAIL IS NOT NULL
		AND VALIDA_EMAIL(P_LA.SENDING_HEI_SIGNATURE.SIGNER_EMAIL, v_mensaje_student) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				- SENDING_HEI_SIGNATURE.SIGNER_EMAIL:
						');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_student);
			v_mensaje_student := '';
		END IF;


		IF P_LA.COMPONENTS IS NOT NULL AND P_LA.COMPONENTS.COUNT > 0 AND
		VALIDA_DATOS_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				-COMPONENTS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_components);
			v_mensaje_components := '';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un Learning Agreement
			mobility,
	*/
	FUNCTION VALIDA_LEARNING_AGREEMENT(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
		v_max_mensaje VARCHAR2(32767 CHAR); 
	BEGIN

		IF P_LA.MOBILITY_ID IS NULL THEN
			v_cod_retorno := -1;
			v_max_mensaje := '- MOBILITY_ID: ' || P_ERROR_MESSAGE;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
		END IF;

		IF P_LA.COMPONENTS IS NULL OR p_LA.COMPONENTS.COUNT = 0 THEN
  		v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			COMPONENTS');
		ELSIF VALIDA_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				COMPONENTS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_components);
		END IF;

		IF VALIDA_SIGNATURE(P_LA.STUDENT_SIGNATURE, v_mensaje_student) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				STUDENT_SIGNATURE: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_student);
		END IF;

		IF VALIDA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE, v_mensaje_s_coord) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
				SENDING_HEI_SIGNATURE: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_mensaje_s_coord);
		END IF;

		IF v_cod_retorno <> 0 THEN
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/* Borra el estudiante de la tabla mobility participant*/
	PROCEDURE BORRA_STUDENT(P_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255CHAR);
		v_contact_details_id VARCHAR2(255CHAR);
	BEGIN
		SELECT CONTACT_ID INTO v_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		SELECT CONTACT_DETAILS_ID INTO v_contact_details_id FROM EWPCV_CONTACT WHERE ID = v_contact_id;
		DELETE FROM EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = v_contact_details_id;
		DELETE FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		PKG_COMMON.BORRA_CONTACT(v_contact_id);
	END;

	PROCEDURE BORRA_LOI(P_COMPONENT_ID IN VARCHAR2) IS
		v_loi_id VARCHAR2(255);
		CURSOR c(pc_component_id VARCHAR2) IS
			SELECT LOI_ID
			FROM EWP.EWPCV_LA_COMPONENT
			WHERE ID = pc_component_id;
	BEGIN
		OPEN c(P_COMPONENT_ID);
		FETCH c INTO v_loi_id;
		CLOSE c;
		IF v_loi_id IS NOT NULL THEN
			UPDATE EWP.EWPCV_LA_COMPONENT SET LOI_ID = NULL WHERE ID = P_COMPONENT_ID;
			DELETE FROM EWP.EWPCV_LOS_LOI WHERE LOI_ID = v_loi_id;
			DELETE FROM EWP.EWPCV_LOI WHERE ID = v_loi_id;
		END IF;
	END BORRA_LOI;

	/*
		Borra creditos asociados a un componente
	*/
	PROCEDURE BORRA_CREDITS(P_COMPONENT_ID IN VARCHAR2) AS
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT CREDITS_ID
			FROM EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = p_id;
	BEGIN
		FOR rec IN c(P_COMPONENT_ID)
		LOOP
			DELETE FROM EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = rec.CREDITS_ID;
			DELETE FROM EWPCV_CREDIT WHERE ID = rec.CREDITS_ID;
		END LOOP;
	END;

	/*
		Borra un componente
	*/
	PROCEDURE BORRA_COMPONENT(P_ID IN VARCHAR2) AS
		v_at_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS
			SELECT ACADEMIC_TERM_DISPLAY_NAME
			FROM EWPCV_LA_COMPONENT
			WHERE ID = p_id;
	BEGIN
		BORRA_CREDITS(P_ID);

		OPEN c(P_ID);
		FETCH c INTO v_at_id;
		CLOSE c;
		DELETE FROM EWPCV_LA_COMPONENT WHERE ID = P_ID;

		BORRA_LOI(P_ID);
		PKG_COMMON.BORRA_ACADEMIC_TERM(v_at_id);
	END;

	/*
		Borra un componente de tipo studied
	*/
	PROCEDURE BORRA_STUDIED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT STUDIED_LA_COMPONENT_ID
			FROM EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_STUDIED_LA_COMPONENT
				WHERE STUDIED_LA_COMPONENT_ID = rec.STUDIED_LA_COMPONENT_ID
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.STUDIED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo recognized
	*/
	PROCEDURE BORRA_RECOGNIZED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT
				WHERE RECOGNIZED_LA_COMPONENT_ID = rec.RECOGNIZED_LA_COMPONENT_ID
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.RECOGNIZED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo virtual
	*/
	PROCEDURE BORRA_VIRTUAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT VIRTUAL_LA_COMPONENT_ID
			FROM EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT
				WHERE VIRTUAL_LA_COMPONENT_ID = rec.VIRTUAL_LA_COMPONENT_ID
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.VIRTUAL_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo blended
	*/
	PROCEDURE BORRA_BLENDED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT BLENDED_LA_COMPONENT_ID
			FROM EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_BLENDED_LA_COMPONENT
				WHERE BLENDED_LA_COMPONENT_ID = rec.BLENDED_LA_COMPONENT_ID
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.BLENDED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo doctoral
	*/
	PROCEDURE BORRA_DOCTORAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT
				WHERE DOCTORAL_LA_COMPONENTS_ID = rec.DOCTORAL_LA_COMPONENTS_ID
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.DOCTORAL_LA_COMPONENTS_ID);
		END LOOP;
	END;

	/*
		Borra una revision de un LA
	*/
	PROCEDURE BORRA_LA(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		v_s_id VARCHAR2(255 CHAR);
		v_sc_id VARCHAR2(255 CHAR);
		v_rc_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT STUDENT_SIGN, SENDER_COORDINATOR_SIGN, RECEIVER_COORDINATOR_SIGN
			FROM EWPCV_LEARNING_AGREEMENT
			WHERE ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;

	BEGIN

		BORRA_STUDIED_COMP(P_LA_ID, LA_REVISION);
		BORRA_RECOGNIZED_COMP(P_LA_ID, LA_REVISION);
		BORRA_VIRTUAL_COMP(P_LA_ID, LA_REVISION);
		BORRA_BLENDED_COMP(P_LA_ID, LA_REVISION);
		BORRA_DOCTORAL_COMP(P_LA_ID, LA_REVISION);

		OPEN c(P_LA_ID, LA_REVISION);
		FETCH c INTO v_s_id, v_sc_id, v_rc_id;
		CLOSE c;

		DELETE FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_s_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_sc_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_rc_id;
	END;

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/
	PROCEDURE BORRA_MOBILITY_LANSKILL(P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REVISION IN NUMBER) AS
		v_lskill_id VARCHAR2(255 CHAR);
		CURSOR c_lskill(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS
			SELECT LANGUAGE_SKILL_ID
			FROM EWPCV_MOBILITY_LANG_SKILL
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR l_rec IN c_lskill(P_MOBILITY_ID, P_MOBILITY_REVISION)
			LOOP
				DELETE FROM EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = l_rec.LANGUAGE_SKILL_ID;
			END LOOP;
	END;

	/*
		Borra una movilidad
	*/
	PROCEDURE BORRA_MOBILITY(P_MOBILITY_ID IN VARCHAR2) AS
		v_student_count NUMBER;
		v_mtype_count NUMBER;

		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;
		CURSOR c_mobility(p_id IN VARCHAR2) IS
			SELECT ID, MOBILITY_REVISION,
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID,
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, ISCED_CODE
			FROM EWPCV_MOBILITY
			WHERE ID = p_id;

		CURSOR c_la(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR mobility_rec IN c_mobility(P_MOBILITY_ID)
		LOOP
			BORRA_MOBILITY_LANSKILL(mobility_rec.ID, mobility_rec.MOBILITY_REVISION);

			FOR la_rec IN c_la(mobility_rec.ID, mobility_rec.MOBILITY_REVISION)
			LOOP
				BORRA_LA(la_rec.LEARNING_AGREEMENT_ID, la_rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			DELETE FROM EWPCV_MOBILITY WHERE ID = mobility_rec.ID AND MOBILITY_REVISION = mobility_rec.MOBILITY_REVISION;

			DELETE FROM EWPCV_SUBJECT_AREA WHERE ID = mobility_rec.ISCED_CODE;

			SELECT COUNT(1) INTO v_student_count FROM EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = mobility_rec.MOBILITY_PARTICIPANT_ID;
			IF v_student_count = 0 THEN
				BORRA_STUDENT(mobility_rec.MOBILITY_PARTICIPANT_ID);
			END IF;

			SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = mobility_rec.SENDER_CONTACT_ID;
			IF v_scontact_count = 0 THEN
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = mobility_rec.SENDER_ADMV_CONTACT_ID;
			IF v_sacontact_count = 0 THEN
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_ADMV_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = mobility_rec.RECEIVER_CONTACT_ID;
			IF v_rcontact_count = 0 THEN
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = mobility_rec.RECEIVER_ADMV_CONTACT_ID;
			IF v_racontact_count = 0 THEN
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_ADMV_CONTACT_ID);
			END IF;
		END LOOP;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_o_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR C(p_start IN VARCHAR2, p_end IN VARCHAR) IS
			SELECT ID
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(p_start)
			AND	UPPER(START_YEAR) =  UPPER(p_end);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN
		OPEN C(SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4) ,SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		FETCH C INTO v_a_id;
		CLOSE C;
		IF v_a_id IS NULL THEN
			v_a_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ACADEMIC_YEAR (ID, START_YEAR, END_YEAR)
				VALUES(v_a_id, SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4),SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		END IF;

		OPEN ounit_cursor(P_ACADEMIC_TERM.INSTITUTION_ID, P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
		FETCH ounit_cursor INTO v_o_id;
		CLOSE ounit_cursor;

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (v_id, P_ACADEMIC_TERM.END_DATE, P_ACADEMIC_TERM.START_DATE , LOWER(P_ACADEMIC_TERM.INSTITUTION_ID) , v_o_id, v_a_id, P_ACADEMIC_TERM.TOTAL_TERMS, P_ACADEMIC_TERM.TERM_NUMBER);

		IF P_ACADEMIC_TERM.DESCRIPTION IS NOT NULL AND P_ACADEMIC_TERM.DESCRIPTION.COUNT > 0 THEN
			FOR i IN P_ACADEMIC_TERM.DESCRIPTION.FIRST .. P_ACADEMIC_TERM.DESCRIPTION.LAST
			LOOP
				v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ACADEMIC_TERM.DESCRIPTION(i));
				INSERT INTO EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (v_id, v_li_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;


	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_PARTICIPANT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT) RETURN VARCHAR2 AS
		v_m_p_id VARCHAR2(255);
		v_c_id   VARCHAR2(255);
		v_c_d_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
	BEGIN
		v_m_p_id := EWP.GENERATE_UUID();
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, null, null);
		INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, GLOBAL_ID, CONTACT_ID) VALUES (v_m_p_id, P_STUDENT.GLOBAL_ID, v_c_id);

		SELECT (
			SELECT CONTACT_DETAILS_ID FROM EWPCV_CONTACT C WHERE C.ID = v_c_id
		) INTO v_c_d_id FROM DUAL;

		IF v_c_d_id IS NOT NULL THEN
			IF P_STUDENT.PHOTO_URL_LIST IS NOT NULL AND P_STUDENT.PHOTO_URL_LIST.COUNT > 0 THEN
				FOR i IN P_STUDENT.PHOTO_URL_LIST.FIRST .. P_STUDENT.PHOTO_URL_LIST.LAST
				LOOP
					v_p_id := EWP.GENERATE_UUID();
					INSERT INTO EWP.EWPCV_PHOTO_URL (
						PHOTO_URL_ID, CONTACT_DETAILS_ID,
						URL, PHOTO_SIZE,
						PHOTO_DATE, PHOTO_PUBLIC)
					VALUES (v_p_id, v_c_d_id,
						P_STUDENT.PHOTO_URL_LIST(i).URL, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_SIZE,
						P_STUDENT.PHOTO_URL_LIST(i).PHOTO_DATE, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_PUBLIC);
				END LOOP;
			END IF;
		END IF;

		RETURN v_m_p_id;
	END;


	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA');
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE, 'MOBILITY_LA');
		END IF;
		RETURN v_id;
	END;


	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_REVISION IN NUMBER, P_OMOBILITY_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_s_ounit_id VARCHAR2(255);
		v_r_ounit_id VARCHAR2(255);
		v_student_id VARCHAR2(255);
		v_s_contact_id VARCHAR2(255);
		v_s_admv_contact_id VARCHAR2(255);
		v_r_contact_id VARCHAR2(255);
		v_r_admv_contact_id VARCHAR2(255);
		v_mobility_type_id VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
		v_academic_term_id VARCHAR2(255);
		v_status_incoming NUMBER;
		v_status_outgoing NUMBER;
	BEGIN
		v_s_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.SENDING_INSTITUTION);
		v_r_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.RECEIVING_INSTITUTION);
		v_student_id := INSERTA_MOBILITY_PARTICIPANT(P_MOBILITY.STUDENT);
		v_s_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, null, null);
		v_s_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT, null, null);
		v_r_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, null, null);
		v_r_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT, null, null);

		v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_MOBILITY.SUBJECT_AREA);
		v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);

		v_academic_term_id := INSERTA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM);


		-- Por defecto el status_outgoing es NOMINATION
		IF P_MOBILITY.STATUS_OUTGOING IS NULL THEN
			v_status_outgoing := 2;
		ELSE
			v_status_outgoing := P_MOBILITY.STATUS_OUTGOING;
		END IF;

		IF P_OMOBILITY_ID IS NULL THEN
			v_id := EWP.GENERATE_UUID();
		ELSE
			v_id := P_OMOBILITY_ID;
		END IF;

		INSERT INTO EWPCV_MOBILITY (ID,
		ACTUAL_ARRIVAL_DATE,
		ACTUAL_DEPARTURE_DATE,
		IIA_ID,
		RECEIVING_IIA_ID,
		ISCED_CODE,
		MOBILITY_PARTICIPANT_ID,
		MOBILITY_REVISION,
		PLANNED_ARRIVAL_DATE,
		PLANNED_DEPARTURE_DATE,
		RECEIVING_INSTITUTION_ID,
		RECEIVING_ORGANIZATION_UNIT_ID,
		SENDING_INSTITUTION_ID,
		SENDING_ORGANIZATION_UNIT_ID,
		MOBILITY_TYPE_ID,
		SENDER_CONTACT_ID,
		SENDER_ADMV_CONTACT_ID,
		RECEIVER_CONTACT_ID,
		RECEIVER_ADMV_CONTACT_ID,
		ACADEMIC_TERM_ID,
		EQF_LEVEL_DEPARTURE,
		EQF_LEVEL_NOMINATION,
		STATUS_OUTGOING,
		MODIFY_DATE)
		VALUES (v_id,
		P_MOBILITY.ACTUAL_ARRIVAL_DATE,
		P_MOBILITY.ACTUAL_DEPATURE_DATE,
		P_MOBILITY.IIA_ID,
		P_MOBILITY.RECEIVING_IIA_ID,
		v_isced_code,
		v_student_id,
		(P_REVISION +1),
		P_MOBILITY.PLANED_ARRIVAL_DATE,
		P_MOBILITY.PLANED_DEPATURE_DATE,
		P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID,
		v_r_ounit_id,
		P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID,
		v_s_ounit_id,
		v_mobility_type_id,
		v_s_contact_id,
		v_s_admv_contact_id,
		v_r_contact_id,
		v_r_admv_contact_id,
		v_academic_term_id,
		P_MOBILITY.EQF_LEVEL_DEPARTURE,
		P_MOBILITY.EQF_LEVEL_NOMINATION,
		v_status_outgoing,
		sysdate);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT >0 THEN
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (v_id, (P_REVISION + 1), v_lang_skill_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (v_id, P_SIGNATURE.SIGNER_NAME, P_SIGNATURE.SIGNER_POSITION , P_SIGNATURE.SIGNER_EMAIL , P_SIGNATURE.SIGN_DATE , P_SIGNATURE.SIGNER_APP, P_SIGNATURE.SIGNATURE);
		RETURN v_id;
	END;

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		INSERT INTO EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
			VALUES (v_id, P_CREDIT.SCHEME, P_CREDIT.CREDIT_VALUE);
		RETURN v_id;
	END;

    /*
		Persiste un LOI
	*/
	FUNCTION INSERTA_LOI(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_INSTITUTION_ID IN VARCHAR2, P_LOS_ID IN OUT VARCHAR2, P_LOS_CODE IN OUT VARCHAR2) RETURN VARCHAR2 AS
		v_loi_id VARCHAR2(255);
    v_start_date DATE;
    v_end_date DATE;
		v_los_type NUMBER(10,0);
    v_los_id VARCHAR2(255);
    v_los_code VARCHAR2(255);
		CURSOR c_los_type(pc_los_id IN VARCHAR2) IS
		SELECT "TYPE" FROM EWP.EWPCV_LOS WHERE LOWER(ID) = LOWER(pc_los_id);

    CURSOR c_los_id(pc_los_id IN VARCHAR2, pc_institution_id IN VARCHAR2) IS
    SELECT ID, LOS_CODE FROM EWP.EWPCV_LOS WHERE LOWER(ID) = LOWER(pc_los_id) AND LOWER(INSTITUTION_ID) = LOWER(pc_institution_id);
  
    CURSOR c_los_code(pc_los_code IN VARCHAR2, pc_institution_id IN VARCHAR2) IS
    SELECT ID, LOS_CODE, "TYPE" FROM EWP.EWPCV_LOS WHERE LOWER(LOS_CODE) = LOWER(pc_los_code) AND LOWER(INSTITUTION_ID) = LOWER(pc_institution_id);
	BEGIN

	    IF P_LOS_ID IS NOT NULL THEN
	      v_los_type := EWP.PKG_LOS.GET_LOS_TYPE_BY_LOS_ID(P_LOS_ID);
	
	      OPEN c_los_id(P_LOS_ID, P_INSTITUTION_ID);
	      FETCH c_los_id INTO v_los_id, v_los_code;
	      CLOSE c_los_id;
	      
	    ELSIF P_LOS_CODE IS NOT NULL THEN
	      OPEN c_los_code(P_LOS_CODE, P_INSTITUTION_ID);
	      FETCH c_los_code INTO v_los_id, v_los_code, v_los_type;
	      CLOSE c_los_code;
	    
	      IF v_los_type IS NULL THEN
	        v_los_type := P_COMPONENT.LOS_TYPE;
	      END IF;
	    END IF;
	  
	    IF v_los_type IS NOT NULL THEN
	      v_loi_id := GENERA_LOI_ID(v_los_type);
	    END IF;
	  
	    IF v_loi_id IS NOT NULL THEN
	      IF P_COMPONENT.START_DATE IS NULL THEN
	          v_start_date := P_COMPONENT.START_DATE;
	      ELSE
	          v_start_date := P_COMPONENT.ACADEMIC_TERM.START_DATE;
	      END IF;
	
	      IF P_COMPONENT.END_DATE IS NULL THEN
	          v_end_date := P_COMPONENT.END_DATE;
	      ELSE
	          v_end_date := P_COMPONENT.ACADEMIC_TERM.END_DATE;
	      END IF;
	    
	      INSERT INTO EWPCV_LOI (ID, START_DATE, END_DATE) VALUES (v_loi_id, v_start_date, v_end_date);
	    
	      IF v_los_id IS NOT NULL THEN
	        INSERT INTO EWPCV_LOS_LOI (LOS_ID, LOI_ID) VALUES (v_los_id, v_loi_id);
	      END IF;
	    END IF;
	  
	    IF v_los_id IS NOT NULL THEN
	      P_LOS_ID := v_los_id;
	    END IF;
	  
	    IF v_los_code IS NOT NULL THEN
	      P_LOS_CODE := v_los_code;
	    END IF;
	
	    RETURN v_loi_id;
	END;

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_COMPONENTE(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_at_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_loi_id VARCHAR2(255);
		v_los_id VARCHAR2(255);
		v_existe_los_id VARCHAR2(255);
		v_institution_id VARCHAR2(255);
		v_sending_inst_id VARCHAR2(255);
	    v_receiving_inst_id VARCHAR2(255);
		v_los_code VARCHAR2(255);

		CURSOR c_mobility(pc_la_id VARCHAR2, pc_la_revision NUMBER) IS
		SELECT M.SENDING_INSTITUTION_ID, M.RECEIVING_INSTITUTION_ID FROM EWP.EWPCV_MOBILITY_LA MLA
		INNER JOIN EWP.EWPCV_MOBILITY M ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = pc_la_id AND MLA.LEARNING_AGREEMENT_REVISION = pc_la_revision;

	BEGIN
		v_id := EWP.GENERATE_UUID();

		v_at_id:= INSERTA_ACADEMIC_TERM(P_COMPONENT.ACADEMIC_TERM);

	    OPEN c_mobility(P_LA_ID, P_LA_REVISION);
	    FETCH c_mobility INTO v_sending_inst_id, v_receiving_inst_id;
	    CLOSE c_mobility;
	
	    IF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
	        v_institution_id := v_sending_inst_id;
	    ELSE
	        v_institution_id := v_receiving_inst_id;
	    END IF;
	
	    v_los_code := P_COMPONENT.LOS_CODE;
	    v_los_id := P_COMPONENT.LOS_ID;
	  
	    v_loi_id := INSERTA_LOI(P_COMPONENT, v_institution_id, v_los_id, v_los_code);

		INSERT INTO EWPCV_LA_COMPONENT (ID,ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (v_id, v_at_id, v_loi_id,v_los_code, v_los_id, P_COMPONENT.STATUS, P_COMPONENT.TITLE, P_COMPONENT.REASON_CODE, P_COMPONENT.REASON_TEXT,
				P_COMPONENT.LA_COMPONENT_TYPE, P_COMPONENT.RECOGNITION_CONDITIONS, P_COMPONENT.SHORT_DESCRIPTION);

		IF P_COMPONENT.LA_COMPONENT_TYPE = 0 THEN
			INSERT INTO EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
			INSERT INTO EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 2 THEN
			INSERT INTO EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 3 THEN
			INSERT INTO EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 4 THEN
			INSERT INTO EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		END IF;

		v_c_id := INSERTA_CREDIT(P_COMPONENT.CREDIT);
		INSERT INTO EWP.EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (v_id, v_c_id);

		RETURN v_id;
	END;

	/*
		Persiste una revision de un learning agreement.
	*/
	Function INSERTA_LA_REVISION(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_LA_REVISION IN NUMBER, P_MOBILITY_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_la_id varchar2(255);
		v_stud_sign_id varchar2(255);
		v_s_hei_sign_id varchar2(255);
		v_changes_id varchar2(255);
		v_c_id varchar2(255);
		v_indice NUMBER := 0;
	BEGIN
		v_stud_sign_id := INSERTA_SIGNATURE(P_LA.STUDENT_SIGNATURE);
		v_s_hei_sign_id :=  INSERTA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE);

		IF P_LA_ID IS NULL THEN
			v_la_id := EWP.GENERATE_UUID();
		ELSE
			v_la_id := P_LA_ID;
		END IF;
		v_changes_id := v_la_id || '-' ||P_LA_REVISION;
		INSERT INTO EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (v_la_id,P_LA_REVISION,v_stud_sign_id,v_s_hei_sign_id, 0, SYSDATE, v_changes_id);

		INSERT INTO EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(P_LA.MOBILITY_ID, P_MOBILITY_REVISION, v_la_id, P_LA_REVISION);

		WHILE v_indice < P_LA.COMPONENTS.COUNT
		LOOP
			v_c_id := INSERTA_COMPONENTE(P_LA.COMPONENTS(v_indice),v_la_id, P_LA_REVISION);
			v_indice := v_indice + 1 ;
		END LOOP;
		RETURN v_la_id;
	END;

	/*
		Genera el tag approve de un mobility update request
	*/
	FUNCTION CREA_TAG_APPROVE(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, l_domdoc IN dbms_xmldom.DOMDocument,
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_approve dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:approve-proposal-v1' );
		l_node_approve := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_approve;
	END;

	/*
		Genera el tag comment de un mobility update request
	*/
	FUNCTION CREA_TAG_COMMENT(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_COMMENT IN VARCHAR2, l_domdoc IN dbms_xmldom.DOMDocument,
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_comment dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment-proposal-v1' );
		l_node_comment := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja comment
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_COMMENT);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_comment;
	END;


	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_ACCEPT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE ) AS
		v_id VARCHAR2(255);
		v_mobility_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_approve dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN

		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_approve := CREA_TAG_APPROVE(P_CHANGES_ID ,v_mobility_id,  P_SIGNATURE, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_approve);

		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, LOWER(P_SENDING_HEI), 0, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_REJECT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2) AS
		v_mobility_id VARCHAR2(255);
		v_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_comment dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN
		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_comment := CREA_TAG_COMMENT(P_CHANGES_ID, v_mobility_id, P_SIGNATURE, P_OBSERVATION, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_comment);
		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, LOWER(P_SENDING_HEI), 1, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************

	PROCEDURE ACTUALIZA_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY) AS
		v_m_revision VARCHAR2(255);
		v_id VARCHAR2(255);
		CURSOR c_mobility(p_id IN VARCHAR2) IS
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY
			WHERE ID = P_ID;
	BEGIN
		OPEN c_mobility(P_OMOBILITY_ID);
		FETCH c_mobility INTO v_m_revision;
		CLOSE c_mobility;

		--Insertamos la nueva revisión de la Movilidad
		v_id := INSERTA_MOBILITY(P_MOBILITY, v_m_revision, P_OMOBILITY_ID);

        -- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
        UPDATE EWPCV_MOBILITY_LA SET MOBILITY_ID = P_OMOBILITY_ID, MOBILITY_REVISION = (v_m_revision + 1) WHERE MOBILITY_ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

	END;

	--  **********************************************
	--	************** NOTIFICACION ******************
	--	**********************************************

	FUNCTION OBTEN_NOTIFIER_HEI_LA(P_LA_ID IN VARCHAR, P_LA_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY M
			INNER JOIN EWPCV_MOBILITY_LA MLA
				ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = p_id AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		OPEN c(P_LA_ID, P_LA_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
			RETURN UPPER(v_s_hei);
		ELSE
			RETURN NULL;
		END IF;
	END;

	FUNCTION OBTEN_NOTIFIER_HEI_OMOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF LOWER(v_r_hei) = LOWER(P_HEI_TO_NOTIFY) THEN
			RETURN  LOWER(v_s_hei);
		ELSE
			RETURN NULL;
		END IF;
	END;
	
	FUNCTION OBTEN_NOTIFIER_HEI_IMOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF LOWER(v_s_hei) = LOWER(P_HEI_TO_NOTIFY) THEN
			RETURN  LOWER(v_r_hei);
		ELSE
			RETURN NULL;
		END IF;
	END;
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_m_revision NUMBER := 0;
		v_la_id VARCHAR2(255);
		CURSOR C(p_id IN VARCHAR2) IS SELECT MAX(MOBILITY_REVISION)
		FROM EWPCV_MOBILITY
		WHERE ID = p_id;
		CURSOR C_LA(p_id IN VARCHAR2) IS
		SELECT LEARNING_AGREEMENT_ID
		FROM EWPCV_MOBILITY_LA
		WHERE MOBILITY_ID = p_id;

	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_cod_retorno := VALIDA_DATOS_LA(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		OPEN C(P_LA.MOBILITY_ID);
		FETCH C INTO v_m_revision;
		CLOSE C;
		IF v_m_revision IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad indicada no existe');
			RETURN -1;
		END IF;

		OPEN C_LA(P_LA.MOBILITY_ID);
		FETCH C_LA INTO v_la_id;
		CLOSE C_LA;
		IF v_la_id IS NOT NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: '||v_la_id);
			RETURN -1;
		END IF;

        v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		P_LA_ID := INSERTA_LA_REVISION(null, P_LA, 0, v_m_revision);
		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, 0, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion receptora');
			P_LA_ID := null;
			ROLLBACK;
			RETURN -1;
		END IF;

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_LEARNING_AGREEMENT;

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_no_firmado NUMBER:=0;
		v_la_revision NUMBER;
		v_m_revision NUMBER;
		v_la_id VARCHAR2(255);
		v_tor_count NUMBER;
		CURSOR c_la(p_id IN VARCHAR2) IS
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT
			WHERE ID = p_id;
		CURSOR c_m_revision(p_mobility_id IN VARCHAR2, p_la_id IN VARCHAR2, p_la_revision IN NUMBER) IS
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_mobility_id
				AND LEARNING_AGREEMENT_ID = p_la_id
				AND LEARNING_AGREEMENT_REVISION = p_la_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		OPEN c_la(P_LA_ID);
		FETCH c_la INTO v_la_revision;
		CLOSE c_la;

		IF v_la_revision IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El learning agreement indicado no existe');
			RETURN -1;
		END IF;

		Open c_m_revision(P_LA.MOBILITY_ID, P_LA_ID, v_la_revision);
		FETCH c_m_revision INTO v_m_revision;
		CLOSE c_m_revision;

		IF v_m_revision IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad indicada para el learning agreement no es correcta.');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_no_firmado FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND STATUS = 0;
		IF v_no_firmado > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_tor_count FROM EWP.EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND TOR_ID IS NOT NULL;

		IF v_tor_count > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar acuerdos de aprendizaje para las cuales ya se ha realizado un ToR (Transcripts of Records).');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_cod_retorno := VALIDA_DATOS_LA(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, v_la_revision, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_la_id := INSERTA_LA_REVISION(P_LA_ID, P_LA, v_la_revision + 1, v_m_revision);

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END UPDATE_LEARNING_AGREEMENT;

	/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		v_max_mensaje VARCHAR2(32767 CHAR); 
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT
			WHERE ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_LA_ID');
		END IF;
		IF P_LA_REVISION IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_LA_REVISION');
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_SIGNATURE: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_msg_val_firma);
		END IF;


		IF v_cod_retorno <> 0 THEN
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement no existe');
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La revision del Learning Agreement ya esta firmada');
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming');
                RETURN v_cod_retorno;
            END IF;

			INSERTA_ACCEPT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE);
			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END ACCEPT_LEARNING_AGREEMENT;

	/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		v_max_mensaje VARCHAR2(32767 CHAR); 
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT
			WHERE ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_LA_ID');
		END IF;
		IF P_LA_REVISION IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_LA_REVISION');
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
			-P_SIGNATURE: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_msg_val_firma);
		END IF;

		IF v_cod_retorno <> 0 THEN
			v_max_mensaje := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_max_mensaje);
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement no existe');
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La revision del Learning Agreement ya esta firmada');
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming');
                RETURN v_cod_retorno;
            END IF;

			INSERTA_REJECT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE, P_OBSERVATION);

			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END REJECT_LEARNING_AGREEMENT;

	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER	AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count NUMBER := 0;
		v_m_id VARCHAR2(255);
		v_tor_count NUMBER;
		CURSOR c(p_id IN VARCHAR2) IS SELECT LEARNING_AGREEMENT_REVISION
			FROM EWPCV_LEARNING_AGREEMENT
			WHERE ID = p_id;

		CURSOR c_mob(p_id IN VARCHAR2, p_revision IN NUMBER) IS SELECT MOBILITY_ID
			FROM EWPCV_MOBILITY_LA
			WHERE LEARNING_AGREEMENT_ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID;
		IF v_count > 0 THEN

		SELECT COUNT(1) INTO v_tor_count FROM EWP.EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND TOR_ID IS NOT NULL;

		IF v_tor_count > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden borrar acuerdos de aprendizaje para las cuales ya se ha realizado un ToR (Transcripts of Records).');
			RETURN -1;
		END IF;

			FOR rec IN c(P_LA_ID)
			LOOP
				IF v_notifier_hei IS NULL THEN
					v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION, P_HEI_TO_NOTIFY);
				END IF;

				IF v_notifier_hei IS NULL THEN
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.');
					RETURN -1;
				END IF;

				IF v_m_id IS NULL THEN
					OPEN c_mob(P_LA_ID,rec.LEARNING_AGREEMENT_REVISION);
					FETCH c_mob INTO v_m_id;
					CLOSE c_mob;
				END IF;

				BORRA_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			PKG_COMMON.INSERTA_NOTIFICATION(v_m_id, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement no existe');
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_LEARNING_AGREEMENT;

	FUNCTION VALIDA_STATUS(P_STATUS IN NUMBER, P_ES_OUTGOING IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	AS
		v_outgoing_statuses VARCHAR2(10) := '0123';
		v_incoming_statuses VARCHAR2(10) := '456';
		v_resultado NUMBER := 0;
	BEGIN
		IF P_ES_OUTGOING <> 0 AND P_ES_OUTGOING <> 1 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El campo status_outgoing no tiene un status válido');
			RETURN -1;
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = 0 AND INSTR(v_outgoing_statuses, P_STATUS) = 0 THEN
			v_resultado := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El campo status_outgoing no tiene un status válido');
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = -1 AND INSTR(v_incoming_statuses, P_STATUS) = 0 THEN
			v_resultado := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El campo status_incoming no tiene un status válido');
		END IF;
		RETURN v_resultado;
	END;

	/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_sending_hei VARCHAR2(255);
		v_es_outgoing NUMBER(1,0);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);

		v_sending_hei := P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID;

		IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.');
			RETURN -1;
		END IF;

		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		END IF;
		IF v_cod_retorno = 0 THEN
			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			P_OMOBILITY_ID := INSERTA_MOBILITY(P_MOBILITY, -1, null);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_MOBILITY;

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_sending_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR C_SENDING_HEI IS SELECT ID, SENDING_INSTITUTION_ID
        FROM EWPCV_MOBILITY
        WHERE ID = P_OMOBILITY_ID
        ORDER BY MOBILITY_REVISION DESC;
		v_es_outgoing NUMBER := 0;
		v_tor_count NUMBER;
		v_la_count NUMBER;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_tor_count FROM EWP.EWPCV_MOBILITY_LA MLA
		INNER JOIN EWP.EWPCV_LEARNING_AGREEMENT LA ON MLA.LEARNING_AGREEMENT_ID = LA.ID
		AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
		WHERE LOWER(MLA.MOBILITY_ID) = LOWER(P_OMOBILITY_ID) AND LA.TOR_ID IS NOT NULL;

		IF v_tor_count > 0 THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar movilidades para las cuales ya se ha realizado un ToR (Transcripts of Records) para los acuerdos de aprendizaje.');
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN

            OPEN C_SENDING_HEI;
            FETCH C_SENDING_HEI INTO v_id, v_sending_hei;
            CLOSE C_SENDING_HEI;

            IF v_id IS NULL THEN
            	EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No existe la movilidad indicada');
                RETURN -1;
			END IF;

			IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.');
                RETURN -1;
			END IF;

			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				ACTUALIZA_MOBILITY(P_OMOBILITY_ID,P_MOBILITY);
				
				SELECT COUNT(1) INTO v_la_count FROM EWP.EWPCV_MOBILITY_LA WHERE LOWER(MOBILITY_ID) = LOWER(P_OMOBILITY_ID);
				IF v_la_count > 0 THEN 
					PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_sending_hei);
				ELSE 
					PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
				END IF;
			END IF;
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END UPDATE_MOBILITY;

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
		v_m_revision NUMBER ;
		v_tor_count NUMBER;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_OMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.');
				RETURN -1;
			END IF;

			SELECT COUNT(1) INTO v_tor_count FROM EWP.EWPCV_MOBILITY_LA MLA
			INNER JOIN EWP.EWPCV_LEARNING_AGREEMENT LA ON MLA.LEARNING_AGREEMENT_ID = LA.ID
			AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
			WHERE MLA.MOBILITY_ID = P_OMOBILITY_ID AND LA.TOR_ID IS NOT NULL;

			IF v_tor_count > 0 THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden borrar movilidades para las cuales ya se ha realizado un ToR (Transcripts of Records) para los acuerdos de aprendizaje.');
				RETURN -1;
			END IF;

			BORRA_MOBILITY(P_OMOBILITY_ID);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_MOBILITY;

	/* Cancela una movilidad
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_OMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.');
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;

		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_OMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.');
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_OMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF v_notifier_hei IS NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.');
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_IMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NOT NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming');
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;

			v_notifier_hei := OBTEN_NOTIFIER_HEI_IMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF v_notifier_hei IS NOT NULL THEN
				EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming');
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2,
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institucion a la que notificar los cambios');
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_IMOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NOT NULL THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming');
			RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE,
			ACTUAL_ARRIVAL_DATE = P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = P_ACTUAL_DEPARTURE
			WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La movilidad no existe');
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END;


END PKG_MOBILITY_LA;


/*
    ***PKG_OUNIT
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_OUNITS AS 

	-- TIPOS DE DATOS

	/* Objeto que modela una OUNIT. Campos obligatorios marcados con *.
        HEI_ID *                        Código SCHAC de la institución
        OUNIT_CODE *					Código del Ounit
        OUNIT_NAME_LIST                 Lista con los nombres de la institución en varios idiomas
        ABBREVIATION					Abreviación de la universidad
        OUNIT_CONTACT_DETAILS			Datos de contacto de la universidad (FK de Institution a EWPCV_CONTACT_DETAILS a través del campo PRIMARY_CONTACT_DETAIL_ID)
        CONTACT_DETAILS_LIST			Lista de los datos de contacto
        FACTSHEET_URL_LIST				Lista de urls de los factsheet de la institución
        LOGO_URL						Url del logo de la institución
        IS_TREE_STRUCTURE  *            Flag que define si se desea introducir la estructura de ounits como un árbol de jerarquias
        PARENT_OUNIT_ID                 En caso de disponer las ounits en estructura de árbol, este campo representarÃ­a los ids del Ounit padre de la 
										OUNIT que estamos introduciendo.
                                        Si el OUNIT que estamos insertando es el root, este campo será null. Si por el contrario estamos insertando un "hijo", 
                                        deberemos informárlo con el id del padre.
                                        Si no se desea implementar la estructura de árbol bastará con no informar este campo e informar el campo IS_TREE_STRUCTURE a 0
	*/
	TYPE OUNIT IS RECORD
	  (
        HEI_ID                          VARCHAR2(255 CHAR),
		OUNIT_CODE						VARCHAR2(255 CHAR),
		OUNIT_NAME_LIST			        EWP.LANGUAGE_ITEM_LIST,
		ABBREVIATION					VARCHAR2(255 CHAR),
		OUNIT_CONTACT_DETAILS		    EWP.CONTACT,
		CONTACT_DETAILS_LIST			EWP.CONTACT_PERSON_LIST,
		FACTSHEET_URL_LIST				EWP.LANGUAGE_ITEM_LIST,
		LOGO_URL						VARCHAR2(255 CHAR),
        IS_TREE_STRUCTURE               NUMBER(1,0),
        PARENT_OUNIT_ID                 VARCHAR2(255 CHAR)
	  );

	-- FUNCIONES 

	/* Inserta una OUNIT en el sistema
		Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_OUNIT(P_OUNIT IN OUNIT, P_OUNIT_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza una OUNIT del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la OUNIT
		Admite un objeto de tipo OUNIT con toda la informacion actualizada de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina una OUNIT del sistema.
        En el caso de estar en estructura de árbol y ser una OUNIT padre, se borrarán también todas las hijas.
		Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

    /* Oculta una ounit de manera que esta deja de estar expuesta. 
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION OCULTAR_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

    /* Oculta una ounit de manera que esta deja de estar expuesta. 
       En el caso de estar en estructura de árbol y ser una OUNIT padre, se expondrán también todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION EXPONER_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;


END PKG_OUNITS;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_OUNITS AS 

    /*
        Valida los datos introducidos en el objeto
        La institucion indicada debe exisitir
        El ounit code no se puede repetir en la misma institucion
        No se pueden mezclar estructuras de representacion de unidades organizativas, o todas van sueltas o todas van en estructura de arbol
        La estructura de representacion de unidades organizativas solo debe tener un root si se representa como arbol
        El ounit code indicado como padre debe existir asociado a la institucion
    */
    FUNCTION VALIDA_OUNIT_DATOS(P_OUNIT IN OUNIT, P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
        v_cod_retorno 	       NUMBER := 0;
        v_count       	       NUMBER;
        v_ou_id       	       VARCHAR2(255);
        v_inst_id              VARCHAR2(255);
        v_ounit_code_anterior  VARCHAR2(255) := 'v_ounit_code_anterior';
        v_parent_ounit_id_antes VARCHAR2(255);
        v_check_db_tree_struct NUMBER;
        v_tot_db_tree_struct   NUMBER;
        v_error_message_url VARCHAR2(2000);
        v_error_message VARCHAR2(2000);
        CURSOR c_is_tree_structure(p_id_ins IN VARCHAR) IS SELECT DISTINCT(IS_TREE_STRUCTURE)
            FROM EWPCV_ORGANIZATION_UNIT ou
            INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
            INNER JOIN EWPCV_INSTITUTION i
                ON i.ID = iou.INSTITUTION_ID
            WHERE UPPER(i.INSTITUTION_ID) = UPPER(p_id_ins) 
            ORDER BY ou.IS_TREE_STRUCTURE ASC;
        CURSOR exist_cursor(p_code IN VARCHAR, p_hei_id IN VARCHAR) IS SELECT ou.ID
            FROM EWPCV_ORGANIZATION_UNIT ou
            INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
            INNER JOIN EWPCV_INSTITUTION i
                ON i.ID = iou.INSTITUTION_ID
            WHERE UPPER(ORGANIZATION_UNIT_CODE) = UPPER(p_code)
            AND UPPER(i.INSTITUTION_ID) = UPPER(p_hei_id);
        CURSOR exist_cursor_i(p_hei_id IN VARCHAR) IS SELECT ID
            FROM EWPCV_INSTITUTION
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_hei_id);
        CURSOR c_ounit_code(p_ounit_id IN VARCHAR2) IS SELECT ORGANIZATION_UNIT_CODE, PARENT_OUNIT_ID
            FROM EWPCV_ORGANIZATION_UNIT
            WHERE ID = p_ounit_id;
    BEGIN    

         OPEN exist_cursor(P_OUNIT.OUNIT_CODE, P_OUNIT.HEI_ID);
         FETCH exist_cursor INTO v_ou_id;
         CLOSE exist_cursor;

         OPEN exist_cursor_i(P_OUNIT.HEI_ID);
         FETCH exist_cursor_i INTO v_inst_id;
         CLOSE exist_cursor_i;

         IF v_inst_id IS NULL THEN 
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - HEI_ID. No existe ninguna institución con el código SCHAC ' || P_OUNIT.HEI_ID);
             RETURN -1;
         END IF;    

         IF (P_OUNIT_ID IS NOT NULL) THEN
			IF P_OUNIT_ID = P_OUNIT.PARENT_OUNIT_ID THEN 
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. No se puede establecer como padre de una unidad organizativa a sÃ­ misma.');
                RETURN -1;
            END IF;
           
            OPEN c_ounit_code(P_OUNIT_ID);
            FETCH c_ounit_code INTO v_ounit_code_anterior, v_parent_ounit_id_antes;
            CLOSE c_ounit_code;
            IF v_parent_ounit_id_antes IS NULL AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL AND P_OUNIT.IS_TREE_STRUCTURE = 1 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. Está actualizando una unidad organizativa declarada como root asignándole una unidad organizativa padre.');
                RETURN -1;
             END IF;
             IF v_parent_ounit_id_antes IS NOT NULL AND P_OUNIT.PARENT_OUNIT_ID IS NULL AND P_OUNIT.IS_TREE_STRUCTURE = 1 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - PARENT_OUNIT_ID. Está tratando de convertir en ROOT una unidad organizativa que no lo era. Actulice el ROOT en su lugar.');
                RETURN -1;
             END IF;
         END IF;

         -- Si es actualización hay que validar que el OUNITCODE que viene no esté duplicado solo si no es el que ya tenÃ­a
         IF v_ou_id IS NOT NULL AND P_OUNIT.OUNIT_CODE <> v_ounit_code_anterior THEN 
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_CODE. Ya existe una unidad organizativa con el código ' || P_OUNIT.OUNIT_CODE || ' para la institución ' || P_OUNIT.HEI_ID);
             RETURN -1;  
         END IF;

         OPEN c_is_tree_structure(P_OUNIT.HEI_ID);
         LOOP
            FETCH c_is_tree_structure INTO v_check_db_tree_struct;
            EXIT WHEN c_is_tree_structure%notfound;
         END LOOP;
         v_tot_db_tree_struct := c_is_tree_structure%rowcount;
         CLOSE c_is_tree_structure;

         IF v_tot_db_tree_struct > 1 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                            'IS_TREE_STRUCTURE: Existen unidades organizativas con estructura de árbol y sin estructura de árbol para esta la Institución idicada, la BBDD necesita revisión.');
            RETURN v_cod_retorno; 
         END IF;

         IF v_check_db_tree_struct <> P_OUNIT.IS_TREE_STRUCTURE THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 
                            'IS_TREE_STRUCTURE: Ya hay unidades organizativas introducidas para esta universidad con diferente estructura de representación a la indicada: ' || P_OUNIT.IS_TREE_STRUCTURE);
            RETURN v_cod_retorno;
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 0 AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - La estructura de representacion de la unidad organizativa no está como formato árbol. El atributo PARENT_OUNIT_ID debe ser nulo.');
              RETURN v_cod_retorno;
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 1 AND P_OUNIT.PARENT_OUNIT_ID IS NULL THEN
            SELECT COUNT(1)INTO v_count FROM EWPCV_ORGANIZATION_UNIT ou             
                INNER JOIN EWPCV_INST_ORG_UNIT iou ON ou.ID = iou.ORGANIZATION_UNITS_ID
                INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID) AND ou.PARENT_OUNIT_ID IS NULL;
                IF v_count > 0 THEN
                    v_cod_retorno := -1;
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            PARENT_OUNIT_ID: ');
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - Ya existe una unidad organizativa informada como "root".');
                    RETURN v_cod_retorno;
                END IF;            
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE = 1 AND P_OUNIT.PARENT_OUNIT_ID IS NOT NULL THEN
                SELECT COUNT(1)INTO v_count FROM EWPCV_ORGANIZATION_UNIT ou             
                    INNER JOIN EWPCV_INST_ORG_UNIT iou ON ou.ID = iou.ORGANIZATION_UNITS_ID
                    INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
                    WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID) AND ou.PARENT_OUNIT_ID IS NULL;
                    IF v_count = 0 THEN
                        v_cod_retorno := -1;
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - No existe ninguna unidad organizativa informada como root, inserte primero el "root".');
                    END IF;          
                SELECT COUNT(1)INTO v_count FROM EWPCV_INST_ORG_UNIT iou
                    INNER JOIN EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
                    WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_OUNIT.HEI_ID) 
                    AND UPPER(iou.ORGANIZATION_UNITS_ID) = UPPER(P_OUNIT.PARENT_OUNIT_ID);
                    IF v_count = 0 THEN
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT_PARENT_ID: ');
                        v_cod_retorno := -1;
                        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - La unidad organizativa informada como padre no existe.');
                    END IF;		
         END IF;

         IF P_OUNIT.FACTSHEET_URL_LIST IS NOT NULL AND P_OUNIT.FACTSHEET_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_OUNIT.FACTSHEET_URL_LIST.FIRST .. P_OUNIT.FACTSHEET_URL_LIST.LAST LOOP 
				IF EWP.VALIDA_URL(P_OUNIT.FACTSHEET_URL_LIST(i).TEXT, 0, v_error_message_url) <> 0 THEN
					v_cod_retorno := -1;
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
						-FACTSHEET_URL_LIST('|| i|| '): ');
					EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
					v_error_message_url := '';
				END IF;
			END LOOP;
		END IF;
	
		IF P_OUNIT.LOGO_URL IS NOT NULL AND EWP.VALIDA_URL(P_OUNIT.LOGO_URL, 0,v_error_message_url) <> 0 THEN
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-LOGO_URL: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message_url);
			v_error_message_url := '';
		END IF;

		IF P_OUNIT.OUNIT_CONTACT_DETAILS IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_CONTACT_DETAILS(P_OUNIT.OUNIT_CONTACT_DETAILS, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-OUNIT_CONTACT_DETAILS: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;

		IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL 
		AND PKG_COMMON.VALIDA_DATOS_C_PERSON_LIST(P_OUNIT.CONTACT_DETAILS_LIST, v_error_message) <> 0 THEN 
			v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '				
				-CONTACT_DETAILS_LIST: ');
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, v_error_message);
			v_error_message := '';
		END IF;
		
        RETURN v_cod_retorno;
    END;

	/*
		Valida los campos obligatorios de una OUNIT 
	*/
    FUNCTION VALIDA_OUNIT_STRUCT(P_OUNIT IN OUNIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
        v_cod_retorno 	    NUMBER := 0;
    BEGIN       
          IF P_OUNIT.HEI_ID IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - HEI_ID');
         END IF;

         IF P_OUNIT.OUNIT_CODE IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_CODE');
         END IF;

         IF  P_OUNIT.OUNIT_NAME_LIST IS NULL OR P_OUNIT.OUNIT_NAME_LIST.COUNT = 0 THEN 
              IF P_ERROR_MESSAGE IS NULL THEN
                    EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              END IF;
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - OUNIT_NAME_LIST');
         END IF;

         IF P_OUNIT.IS_TREE_STRUCTURE IS NULL THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            OUNIT: ');
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                            - IS_TREE_STRUCTURE');
         END IF;

         RETURN v_cod_retorno;
    END;

     /*
		Inserta las urls de factsheet de la organización 
	*/
	FUNCTION INSERTA_FACTSHEET_URLS(P_OUNIT IN OUNIT) RETURN VARCHAR2 AS 
	    v_lang_item_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		CURSOR exist_lang_it_cursor(p_lang IN VARCHAR2, p_text IN VARCHAR2, p_fs_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM lait
            INNER JOIN EWPCV_FACT_SHEET_URL fsu ON fsu.URL_ID = lait.ID
			WHERE UPPER(lait.LANG) = UPPER(p_lang)
			AND UPPER(lait.TEXT) = UPPER(p_text)
            AND fsu.FACT_SHEET_ID = p_fs_id;
        CURSOR exist_fact_sheet_cursor(p_ounit_code IN VARCHAR2, v_inst_id IN VARCHAR2) IS SELECT ou.FACT_SHEET
			FROM EWPCV_ORGANIZATION_UNIT ou
            INNER JOIN EWPCV_INST_ORG_UNIT iou
            ON ou.ID = iou.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION i
            ON i.ID = iou.INSTITUTION_ID
			WHERE UPPER(ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code)
			AND UPPER(i.INSTITUTION_ID) = v_inst_id;
	BEGIN
        OPEN exist_fact_sheet_cursor(P_OUNIT.OUNIT_CODE, P_OUNIT.HEI_ID);
        FETCH exist_fact_sheet_cursor INTO v_factsheet_id;
        CLOSE exist_fact_sheet_cursor;
        IF P_OUNIT.FACTSHEET_URL_LIST IS NOT NULL AND P_OUNIT.FACTSHEET_URL_LIST.COUNT > 0 THEN
            IF v_factsheet_id IS NULL THEN
                v_factsheet_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_FACT_SHEET (ID) VALUES (v_factsheet_id);
            END IF;

            FOR i IN P_OUNIT.FACTSHEET_URL_LIST.FIRST .. P_OUNIT.FACTSHEET_URL_LIST.LAST 
            LOOP
                OPEN exist_lang_it_cursor(P_OUNIT.FACTSHEET_URL_LIST(i).LANG, P_OUNIT.FACTSHEET_URL_LIST(i).TEXT, v_factsheet_id) ;
                FETCH exist_lang_it_cursor INTO v_lang_item_id;
                CLOSE exist_lang_it_cursor;
                IF v_lang_item_id IS NULL THEN 
                    v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_OUNIT.FACTSHEET_URL_LIST(i));
                    INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (v_lang_item_id, v_factsheet_id);
                    v_lang_item_id := null;
                END IF; 		
            END LOOP;
		END IF;
		RETURN v_factsheet_id;
	END;

    /*
        Borra los nombres de la institution
    */
    PROCEDURE BORRA_OUNIT_NAMES (P_OUNIT_ID IN VARCHAR2) AS
        CURSOR c_names(p_orgun_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_ORGANIZATION_UNIT_NAME
			WHERE ORGANIZATION_UNIT_ID = p_orgun_id;
    BEGIN
        FOR names_rec IN c_names(P_OUNIT_ID)
		LOOP 
			DELETE FROM EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID = names_rec.NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = names_rec.NAME_ID;
		END LOOP;
    END;

    /*
		 Borra una ounit
	 */
    PROCEDURE BORRA_OUNIT(P_OUNIT_ID IN VARCHAR2) AS	
        v_fact_sheet_id  VARCHAR2(255);
        v_ounit_code     VARCHAR2(255);
        v_prim_cont_id   VARCHAR2(255);
        v_prim_cont_det_id VARCHAR2(255);
        CURSOR c_fs_id(p_ou_id IN VARCHAR2) IS 
			SELECT fact_sheet
			FROM EWPCV_ORGANIZATION_UNIT 
			WHERE ID = p_ou_id;                         
        CURSOR c_contacts_inst(v_ounit_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT 
			WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(v_ounit_id);
		BEGIN
            OPEN c_fs_id(P_OUNIT_ID);
            FETCH c_fs_id INTO v_fact_sheet_id;
            CLOSE c_fs_id;

			BORRA_OUNIT_NAMES(P_OUNIT_ID);    

            UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE ID = P_OUNIT_ID;     

            --Borramos Factsheet urls
            PKG_COMMON.BORRA_FACT_SHEET_URL(v_fact_sheet_id);

            DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_fact_sheet_id;

            --Borramos contacts
            FOR contact IN c_contacts_inst(P_OUNIT_ID)       
            LOOP
                PKG_COMMON.BORRA_CONTACT(contact.id);
            END LOOP;

            DELETE FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = P_OUNIT_ID;
            DELETE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT_ID;

    END;

    /*
		Persiste una ounit en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_HEI_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		P_FACTSHEET_ID IN VARCHAR2, P_ORG_UNIT_CODE IN VARCHAR2, P_OUNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST, 
        P_PRIM_CONT_DETAIL_ID IN VARCHAR2, P_IS_TREE_STRUCTURE IN NUMBER,
        P_PARENT_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id        VARCHAR2(255);
        v_inst_id   VARCHAR2(255);
        CURSOR exist_cursor_i(p_hei_id IN VARCHAR) IS SELECT ID
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_hei_id);
	BEGIN 
        OPEN exist_cursor_i(P_HEI_ID) ;
        FETCH exist_cursor_i INTO v_inst_id;
        CLOSE exist_cursor_i;

        v_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID, IS_TREE_STRUCTURE, PARENT_OUNIT_ID) 
                                     VALUES (v_id, P_ORG_UNIT_CODE, P_ABREVIATION, P_LOGO_URL, P_FACTSHEET_ID, P_PRIM_CONT_DETAIL_ID, P_IS_TREE_STRUCTURE, P_PARENT_OUNIT_ID); 
        INSERT INTO EWPCV_INST_ORG_UNIT (ORGANIZATION_UNITS_ID, INSTITUTION_ID) VALUES (v_id, v_inst_id);                                     
		PKG_COMMON.INSERTA_OUNIT_NAMES(v_id, P_OUNIT_NAMES);

		RETURN v_id;
	END;

    /* Inserta una OUNIT en el sistema
		Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_OUNIT(P_OUNIT IN OUNIT, P_OUNIT_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_prim_contact_id VARCHAR2(255);
		v_contact_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN

		v_cod_retorno := VALIDA_OUNIT_STRUCT(P_OUNIT, P_ERROR_MESSAGE);
		
		IF v_cod_retorno = 0 THEN  

            v_cod_retorno := VALIDA_OUNIT_DATOS(P_OUNIT, null, P_ERROR_MESSAGE);
           
            IF v_cod_retorno = 0 THEN      

                -- Insertamos la OUNIT en EWPCV_ORGANIZATION_UNIT. Lo hacemos ya para tener el ounit_code disponible a la hora de hacer el insert del contacto principal
                P_OUNIT_ID := INSERTA_OUNIT(P_OUNIT.HEI_ID, P_OUNIT.ABBREVIATION, P_OUNIT.LOGO_URL, null, P_OUNIT.OUNIT_CODE, 
                                                       P_OUNIT.OUNIT_NAME_LIST, null, P_OUNIT.IS_TREE_STRUCTURE, P_OUNIT.PARENT_OUNIT_ID);   

                -- Insertamos el contacto principal
                v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_OUNIT.OUNIT_CONTACT_DETAILS, P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);

                -- Insertamos la lista de contactos
                IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL AND P_OUNIT.CONTACT_DETAILS_LIST.COUNT > 0 THEN                
                    FOR i IN P_OUNIT.CONTACT_DETAILS_LIST.FIRST .. P_OUNIT.CONTACT_DETAILS_LIST.LAST 
                    LOOP
                        v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_OUNIT.CONTACT_DETAILS_LIST(i), P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);
                    END LOOP;
                END IF;

                -- Insertamos la lista de fact sheet urls
                v_factsheet_id := INSERTA_FACTSHEET_URLS(P_OUNIT);       

                -- Hacemos update de la OUNIT en EWPCV_ORGANIZATION_UNIT para setear el factsheet y el primary_contact_detail_id
                UPDATE EWPCV_ORGANIZATION_UNIT SET FACT_SHEET = v_factsheet_id, PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id 
                    WHERE ID = P_OUNIT_ID;
            END IF;
		END IF;

		COMMIT;

		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END INSERT_OUNIT;	

    PROCEDURE ACTUALIZA_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT) IS
        v_prim_contact_id   VARCHAR2(255);
        v_factsheet_id      VARCHAR2(255);
        v_contact_id        VARCHAR2(255);
        CURSOR c_fs_id(p_ou_id IN VARCHAR2) IS 
			SELECT FACT_SHEET
			FROM EWPCV_ORGANIZATION_UNIT
			WHERE ID = p_ou_id;
       CURSOR c_contacts(p_ounit_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_CONTACT
			WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(p_ounit_id);
    BEGIN
            OPEN c_fs_id(P_OUNIT_ID);
            FETCH c_fs_id INTO v_factsheet_id;
            CLOSE c_fs_id;


            BORRA_OUNIT_NAMES(P_OUNIT_ID);
            PKG_COMMON.INSERTA_OUNIT_NAMES(P_OUNIT_ID, P_OUNIT.OUNIT_NAME_LIST);

            UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE ID = P_OUNIT_ID;
            FOR contact IN c_contacts(P_OUNIT_ID) 
            LOOP
                PKG_COMMON.BORRA_CONTACT(contact.id);
            END LOOP;
            -- Insertamos de nuevo el contacto principal
            v_prim_contact_id := PKG_COMMON.INSERTA_CONTACT(P_OUNIT.OUNIT_CONTACT_DETAILS, P_OUNIT.HEI_ID, P_OUNIT_ID);
            -- Insertamos de nuevo la lista de contactos
            IF P_OUNIT.CONTACT_DETAILS_LIST IS NOT NULL AND P_OUNIT.CONTACT_DETAILS_LIST.COUNT > 0 THEN                
                FOR i IN P_OUNIT.CONTACT_DETAILS_LIST.FIRST .. P_OUNIT.CONTACT_DETAILS_LIST.LAST 
                LOOP
                    v_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_OUNIT.CONTACT_DETAILS_LIST(i), P_OUNIT.HEI_ID, P_OUNIT.OUNIT_CODE);
                END LOOP;
            END IF;      

            PKG_COMMON.BORRA_FACT_SHEET_URL(v_factsheet_id);
            DELETE FROM EWPCV_FACT_SHEET WHERE ID = v_factsheet_id AND CONTACT_DETAILS_ID IS NULL;
            v_factsheet_id := INSERTA_FACTSHEET_URLS(P_OUNIT);

            UPDATE EWPCV_ORGANIZATION_UNIT SET 
               PRIMARY_CONTACT_DETAIL_ID = v_prim_contact_id, FACT_SHEET = v_factsheet_id, ABBREVIATION = P_OUNIT.ABBREVIATION, LOGO_URL = P_OUNIT.LOGO_URL, ORGANIZATION_UNIT_CODE = P_OUNIT.OUNIT_CODE 
                WHERE ID = P_OUNIT_ID;
    END;

	/* Actualiza una OUNIT del sistema.
        Recibe como parametros: 
        El identificador (ID de interoperabilidad) de la OUNIT
		Admite un objeto de tipo OUNIT con toda la informacion actualizada de la Ounit, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_OUNIT IN OUNIT, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_prim_contact_id VARCHAR2(255);
        v_count NUMBER := 0;
	BEGIN
        SELECT COUNT(1) INTO v_count FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT_ID;
		IF v_count > 0 THEN
            v_cod_retorno := VALIDA_OUNIT_STRUCT(P_OUNIT, P_ERROR_MESSAGE);
            IF v_cod_retorno = 0 THEN  
                v_cod_retorno := VALIDA_OUNIT_DATOS(P_OUNIT, P_OUNIT_ID, P_ERROR_MESSAGE);
                IF v_cod_retorno = 0 THEN      
                    ACTUALIZA_OUNIT(P_OUNIT_ID, P_OUNIT);
                    COMMIT;
                END IF;
            END IF;
        ELSE
            v_cod_retorno := -1;
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
            ROLLBACK;
            RETURN -1;
	END UPDATE_OUNIT;

	/* Elimina una OUNIT del sistema.
		Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
        v_count_ounit_root NUMBER := 0;
	BEGIN
		SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT_ID;
        SELECT COUNT(1) INTO v_count_ounit_root FROM EWPCV_ORGANIZATION_UNIT WHERE PARENT_OUNIT_ID = P_OUNIT_ID;
		IF v_count_ounit > 0 AND v_count_ounit_root = 0 THEN
			BORRA_OUNIT(P_OUNIT_ID);
		ELSE
			v_cod_retorno := -1;
            IF v_count_ounit = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
            ELSE
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se puede borrar una OUNIT padre sin antes borrar los hijos');
            END IF;
		END IF;
        COMMIT;
        return v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
	END DELETE_OUNIT; 	 

    /*
        Actualiza el campo IS_EXPOSED de forma recursiva
    */
   PROCEDURE OCULTAR_O_MOSTRAR(P_OUNIT_ID IN VARCHAR2, P_IS_EXPOSED IN NUMBER) AS
        CURSOR c_org_ou(p_id_ou IN VARCHAR2) IS SELECT ID
			FROM EWPCV_ORGANIZATION_UNIT
			WHERE PARENT_OUNIT_ID = p_id_ou;
   BEGIN
        UPDATE EWPCV_ORGANIZATION_UNIT SET IS_EXPOSED = P_IS_EXPOSED WHERE ID = P_OUNIT_ID;
        FOR org_ou IN c_org_ou(P_OUNIT_ID)
        LOOP
            OCULTAR_O_MOSTRAR(org_ou.ID, P_IS_EXPOSED);
        END LOOP;
   END;

     /* Oculta una ounit de manera que esta deja de estar expuesta. 
        En el caso de estar en estructura de árbol y ser una OUNIT padre, se ocultarán también todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION OCULTAR_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
    BEGIN
        SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT_ID;
        IF v_count_ounit = 0 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        ELSE 
            OCULTAR_O_MOSTRAR(P_OUNIT_ID, 0);
        END IF;
        COMMIT;
        RETURN v_cod_retorno;

        EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END OCULTAR_OUNIT;

    /* 	Muestra una ounit de manera que esta deja de estar expuesta. 
        En el caso de estar en estructura de árbol y ser una OUNIT padre, se expondrán también todas las hijas.
        Recibe como parametros: 
        El identificador de la OUNIT
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
    FUNCTION EXPONER_OUNIT(P_OUNIT_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_cod_retorno NUMBER := 0;
        v_count_ounit NUMBER := 0;
    BEGIN
        SELECT COUNT(1) INTO v_count_ounit FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P_OUNIT_ID;
        IF v_count_ounit = 0 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La ounit no existe');
        ELSE 
            OCULTAR_O_MOSTRAR(P_OUNIT_ID, 1);
        END IF;
        COMMIT;
        RETURN v_cod_retorno;
        EXCEPTION 
			WHEN OTHERS THEN
			EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, substr(SQLERRM,12));
			ROLLBACK;
			RETURN -1;
    END EXPONER_OUNIT;

END PKG_OUNITS;

/*
    ***PKG_TORS
*/
/
CREATE OR REPLACE PACKAGE EWP.PKG_TORS AS 

  -- TIPOS DE DATOS
  
  /*
   Objeto que modela un ATTACHMENT. Campos obligatorios marcados con *.
    ATTACH_REFERENCE                Case insensitive. Referencia del attachment que está vinculada a la lista de 
                                    attachment del LOI (ATTACHMENTS_REF_LIST)
                                    Si no está informado se entiende que es un attachment del ToR.
    TITLE *                         Titulo del attachment
    ATT_TYPE                        0 - Diploma, 1 - Diploma Supplement, 2 - Transcript of Records, 3 - EMREX transcript, 
                                    4 - Micro Credential, 5 - Letter of Nomination, 6 - Certificate of Training, 
                                    7 - Learning Agreement, 8 - Other
    DESCRIPTION                     Descripción
    CONTENT *                       Contenido
    EXTENSION                       Extensión
  */

  TYPE ATTACHMENT IS RECORD 
  (
    ATTACH_REFERENCE            VARCHAR2(255),
    TITLE                       EWP.LANGUAGE_ITEM_LIST,
    ATT_TYPE                    VARCHAR2(255),
    DESCRIPTION                 EWP.LANGUAGE_ITEM_LIST,
    CONTENT                     EWP.MULTILANGUAGE_BLOB_LIST,
    EXTENSION                   BLOB
  );

  /* 
    Objeto que modela un DIST_CATEGORIES. Campos obligatorios marcados con *.
    Modela cada uno de los registros de la distribución de resultados de una asignatura de acuerdo a lo establecido en las tablas EGRACONS.
    Este objeto representa cada uno de los registros del histograma de resultados alcanzados por todos los alumnos que cursan una asignatura. 
    LABEL                           Identificador del registro del histograma. 
                                    Debe coincidir con uno de los posibles valores del esquema de graduación empleado.
    DIST_CAT_COUNT                  Número de estudiantes encuadrados dentro de la categoría indicada
  */
  TYPE DIST_CATEGORIES IS RECORD
  (
    LABEL                       VARCHAR2(255),
    DIST_CAT_COUNT              NUMBER(10,0)
  );

  TYPE DIST_CATEGORIES_LIST IS TABLE OF DIST_CATEGORIES INDEX BY BINARY_INTEGER;

  /* 
    Objeto que modela un RESULT_DISTRIBUTION. Campos obligatorios marcados con *.
    DIST_CATEGORIES_LIST            Lista de categorías
    DESCRIPTION                     Descripción
  */
  TYPE RESULT_DISTRIBUTION IS RECORD
  (
    DIST_CATEGORIES_LIST        PKG_TORS.DIST_CATEGORIES_LIST,
    DESCRIPTION                 EWP.LANGUAGE_ITEM_LIST
  );

  /*
    Objeto que modela un EDUCATION_LEVEL. Campos obligatorios marcados con *. 
      LEVEL_TYPE *              Actualmente el nivel soportado es 0 - EQF
      DESCRIPTION *             Descripción
      LEVEL_VALUE *             Valor del nivel
  */
  TYPE EDUCATION_LEVEL IS RECORD
  (
    LEVEL_TYPE                  NUMBER(10,0),
    DESCRIPTION                 EWP.LANGUAGE_ITEM_LIST,
    LEVEL_VALUE                 VARCHAR2(255)
  );

  TYPE EDUCATION_LEVEL_LIST IS TABLE OF EDUCATION_LEVEL INDEX BY BINARY_INTEGER;

  TYPE ATTACHMENTS_REF_LIST IS TABLE OF VARCHAR2(255) INDEX BY BINARY_INTEGER;

  TYPE ADDITIONAL_INF_LIST IS TABLE OF VARCHAR2(255) INDEX BY BINARY_INTEGER;

  TYPE SECTION_ATTACHMENT_LIST IS TABLE OF ATTACHMENT INDEX BY BINARY_INTEGER;

  /* Objeto que modela un DIPLOMA_SECTION. Campos obligatorios marcados con *.
      TITLE *                     El título del diploma.
      CONTENT                     El contenido del diploma (Solo es obligatorio en la Top Section)
      ADDITIONAL_INF_LIST         La fecha en la que el Diploma fue generado
      SECTION_ATTACHMENT_LIST     La introducción estandar, en inglés comienza así: "This Diploma Supplement model was developed by the European Commission (...)
      SECTION_NUMBER *            El número de sección
  */
  TYPE DIPLOMA_SECTION_3 IS RECORD
  (
    TITLE                       VARCHAR2(255),
    CONTENT                     BLOB,
    ADDITIONAL_INF_LIST         PKG_TORS.ADDITIONAL_INF_LIST,
    SECTION_ATTACHMENT_LIST     PKG_TORS.SECTION_ATTACHMENT_LIST,
    SECTION_NUMBER              NUMBER(10,0)
  );


  TYPE SECTION_LIST_3 IS TABLE OF DIPLOMA_SECTION_3 INDEX BY BINARY_INTEGER;

  /* Objeto que modela un DIPLOMA_SECTION. Campos obligatorios marcados con *.
      TITLE *                     El título del diploma.
      CONTENT                     El contenido del diploma (Solo es obligatorio en la Top Section)
      ADDITIONAL_INF_LIST         La fecha en la que el Diploma fue generado
      SECTION_ATTACHMENT_LIST     La introducción estandar, en inglés comienza así: "This Diploma Supplement model was developed by the European Commission (...)
      SECTION_NUMBER *            El número de sección
      SECTION_LIST                Listado de secciones del diploma
  */
  TYPE DIPLOMA_SECTION_2 IS RECORD
  (
    TITLE                       VARCHAR2(255),
    CONTENT                     BLOB,
    ADDITIONAL_INF_LIST         PKG_TORS.ADDITIONAL_INF_LIST,
    SECTION_ATTACHMENT_LIST     PKG_TORS.SECTION_ATTACHMENT_LIST,
    SECTION_NUMBER              NUMBER(10,0),
    SECTION_LIST                PKG_TORS.SECTION_LIST_3
  );

  TYPE SECTION_LIST_2 IS TABLE OF DIPLOMA_SECTION_2 INDEX BY BINARY_INTEGER;


  /* Objeto que modela un DIPLOMA_SECTION. Campos obligatorios marcados con *.
      TITLE *                     El título del diploma.
      CONTENT *                   El contenido del diploma (Solo es obligatorio en la Top Section)
      ADDITIONAL_INF_LIST         Cualquier tipo de información adicional
      SECTION_ATTACHMENT_LIST     List de attachment vinculados a esta sección
      SECTION_NUMBER *            El número de sección
      SECTION_LIST                Listado de secciones del diploma
  */
  TYPE DIPLOMA_SECTION IS RECORD
  (
    TITLE                       VARCHAR2(255),
    CONTENT                     BLOB,
    ADDITIONAL_INF_LIST         PKG_TORS.ADDITIONAL_INF_LIST,
    SECTION_ATTACHMENT_LIST     PKG_TORS.SECTION_ATTACHMENT_LIST,
    SECTION_NUMBER              NUMBER(10,0),
    SECTION_LIST                PKG_TORS.SECTION_LIST_2
  );

  TYPE SECTION_LIST IS TABLE OF DIPLOMA_SECTION INDEX BY BINARY_INTEGER;

  /*Objeto que modela un DIPLOMA. Campos obligatorios marcados con *.
    VERSION *                   El número de versión se refiere a las reglas oficiales sobre lo que debe incluir un Diploma. 
                                2018 es la versión predeterminada, a menos que se especifique lo contrario.
    ISSUE_DATE  *               La fecha en la que el Diploma fue generado
    INTRODUCTION *              La introducción estandar, en inglés comienza así: "This Diploma Supplement model was developed by the European Commission (...)
    SIGNATURE                   Firma
    SECTION_LIST                Listado de secciones del diploma
  */
  TYPE DIPLOMA IS RECORD 
  (
    VERSION                     NUMBER(10,0),
    ISSUE_DATE                  DATE,
    INTRODUCTION                BLOB,
    SIGNATURE                   BLOB,
    SECTION_LIST                PKG_TORS.SECTION_LIST
  );

  TYPE GRADING_SCHEME IS RECORD 
  (
    LABEL                       EWP.LANGUAGE_ITEM_LIST,
    GRAD_DESCRIPTION            EWP.LANGUAGE_ITEM_LIST
  );

  /*  Objeto que modela un LOS. Campos obligatorios marcados con *.
      LOI_ID                  El id del LOI en interoperabilidad
      LOS_ID *                El id del LOS en interoperabilidad (Opcional LOS_ID/LOS_CODE) 
      LOS_CODE *              El code del LOS (Opcional LOS_ID/LOS_CODE)
      START_DATE              La fecha en la que este estudiante empezó a estudiar este LOI
      END_DATE                La fecha en la que el estudiante terminó de estudiar. Preferiblemente,
                                esta debe ser la fecha en la que el alumno haya aprobado su examen final.
      STATUS                  Valores posibles: 0 - PASSED, 1 - FAILED, 2 - IN-PROGRESS
      GRADING_SCHEME          Listado de nombres e identificadores de los esquemas de calificacion empleados en el informe
      RESULT_LABEL            La calificación final otorgada al estudiante. Este DEBE contener el mismo valor que el impreso en los documentos oficiales
      PERCENTAGE_LOWER        El porcentaje de estudiantes del mismo curso que obtuvieron una calificación más baja que nuestro alumno. Número decimal de 0 a 100.
      PERCENTAGE_EQUAL        El porcentaje de estudiantes del mismo curso que obtuvieron una calificación igual que nuestro alumno. Número decimal de 0 a 100.
      PERCENTAGE_HIGHER       El porcentaje de estudiantes del mismo curso que obtuvieron una calificación más alta que nuestro alumno. Número decimal de 0 a 100.
      RESULT_DISTRIBUTION     Describe un histograma de resultados obtenidos por todos los alumnos de esta instancia de curso.
      ENGAGEMENT_HOURS        El número de horas que el estudiante ha empleado
      ATTACHMENTS_REF_LIST    Referencia de los attachments relacionados con el LOI. Se corresponde con el atributo ATTACH_REFERENCE del objeto ATTACHMENT
      GROUP_TYPE_ID           El id del tipo de grupo por el que queremos agrupar
      GROUP_ID                El id del grupo por el que queremos agrupar
      DIPLOMA                 Objeto diploma
      EXTENSION               Extensión
  */
  TYPE LOI IS RECORD
  (
    LOI_ID                      VARCHAR2(255),
    LOS_ID                      VARCHAR2(255),
    LOS_CODE                    VARCHAR2(255),
    START_DATE                  DATE,
    END_DATE                    DATE,
    STATUS                      NUMBER(10,0),
    GRADING_SCHEME_ID           VARCHAR2(255),
    RESULT_LABEL                VARCHAR2(255),
    PERCENTAGE_LOWER            NUMBER(10,2),
    PERCENTAGE_EQUAL            NUMBER(10,2),
    PERCENTAGE_HIGHER           NUMBER(10,2),
    RESULT_DISTRIBUTION         PKG_TORS.RESULT_DISTRIBUTION,
    EDUCATION_LEVEL             PKG_TORS.EDUCATION_LEVEL_LIST, 
    LANGUAGE_OF_INSTRUCTION     VARCHAR2(255),
    ENGAGEMENT_HOURS            NUMBER(10,2), 
    ATTACHMENTS_REF_LIST        PKG_TORS.ATTACHMENTS_REF_LIST,
    GROUP_TYPE_ID               VARCHAR2(255),
    GROUP_ID                    VARCHAR2(255),
    DIPLOMA                     PKG_TORS.DIPLOMA,
    EXTENSION                   BLOB
  );

  /*
    Objeto que modela un GROUP. Campos obligatorios marcados con *. 
      TITLE *                   Name del tipo de grupo
      SORTING_KEY               Orden de criterio de ordenación
  */
  TYPE LOI_GROUP IS RECORD
  (
    TITLE                       EWP.LANGUAGE_ITEM,
    SORTING_KEY                 NUMBER(10,0)
  );

  TYPE GROUP_LIST IS TABLE OF LOI_GROUP INDEX BY BINARY_INTEGER;

  /*
    Objeto que modela un GROUP_TYPE. Campos obligatorios marcados con *.  
      TITLE *                   Name del tipo de agrupación
      GROUP_LIST                Lista de grupo
  */
  TYPE GROUP_TYPE IS RECORD
  (
    TITLE                       EWP.LANGUAGE_ITEM,
    GROUP_LIST                  PKG_TORS.GROUP_LIST
  );

  TYPE GROUP_TYPE_LIST IS TABLE OF GROUP_TYPE INDEX BY BINARY_INTEGER;

  TYPE TOR_ATTACHMENT_LIST IS TABLE OF ATTACHMENT INDEX BY BINARY_INTEGER;

  TYPE LOI_LIST IS TABLE OF LOI INDEX BY BINARY_INTEGER;

  TYPE MOBILITY_ATTACHMENT_LIST IS TABLE OF ATTACHMENT INDEX BY BINARY_INTEGER;

  /* 
    Objeto que modela un TOR_REPORT. Campos obligatorios marcados con *.
      ISSUE_DATE *              Fecha de la generación del report
      LOI_LIST *                Lista de LOIS cuya calificación se va a incluir en el TOR
      TOR_ATTACHMENT_LIST
  */
  TYPE TOR_REPORT IS RECORD
  (
    ISSUE_DATE                  DATE,
    LOI_LIST                    PKG_TORS.LOI_LIST,
    TOR_ATTACHMENT_LIST         PKG_TORS.TOR_ATTACHMENT_LIST -- Engancha con EWPCV_TOR
  );

  /* 
    Objeto que modela un TOR_REPORT. Campos obligatorios marcados con *.
      ISSUE_DATE *              Fecha de la generación del report
      TOR_ATTACHMENT_LIST *     Lista de attachment
  */
  TYPE SIMPLE_TOR_REPORT IS RECORD
  (
    ISSUE_DATE                  DATE,
    TOR_ATTACHMENT_LIST         PKG_TORS.TOR_ATTACHMENT_LIST
  );

  TYPE TOR_REPORT_LIST IS TABLE OF TOR_REPORT INDEX BY BINARY_INTEGER;

  TYPE SIMPLE_TOR_REPORT_LIST IS TABLE OF SIMPLE_TOR_REPORT INDEX BY BINARY_INTEGER;

  /* 
    Objeto que modela un GRADE_FREQUENCY. Campos obligatorios marcados con *.
      LABEL                     Label
      PERCENTAGE                Porcentajet
  */
  TYPE GRADE_FREQUENCY IS RECORD
  (
    LABEL                       VARCHAR2(255),
    PERCENTAGE                  VARCHAR2(255)
  );

  TYPE GRADE_FREQUENCY_LIST IS TABLE OF GRADE_FREQUENCY INDEX BY BINARY_INTEGER;


  /* 
    Objeto que modela un ISCED_TABLE. Campos obligatorios marcados con *.
      GRADE_FREQUENCY_LIST      Label
      ISCED_CODE                El código ISCED-F del curso o el programa
  */
  TYPE ISCED_TABLE IS RECORD
  (
    GRADE_FREQUENCY_LIST        PKG_TORS.GRADE_FREQUENCY_LIST,
    ISCED_CODE                  VARCHAR2(255)
  );

  TYPE ISCED_TABLE_LIST IS TABLE OF ISCED_TABLE INDEX BY BINARY_INTEGER;

  /* 
    Objeto que modela un TOR. Campos obligatorios marcados con *.
      GENERATED_DATE *          Fecha de la generación del report
      TOR_REPORT_LIST *         Lista de reports del ToR
      MOBILITY_ATTACHMENT_LIST  Lista de attachments del ToR
      EXTENSION                 Extensión
  */
  TYPE TOR IS RECORD
  (
    GENERATED_DATE              DATE,
    TOR_REPORT_LIST             PKG_TORS.TOR_REPORT_LIST,
    MOBILITY_ATTACHMENT_LIST    PKG_TORS.MOBILITY_ATTACHMENT_LIST,
    EXTENSION                   BLOB,
    ISCED_TABLE_LIST            PKG_TORS.ISCED_TABLE_LIST
  );

   /* 
    Objeto que modela un TOR. Campos obligatorios marcados con *.
      GENERATED_DATE *          Fecha de la generación del report
      SIMPLE_TOR_REPORT_LIST *  Lista de reports del ToR
  */
  TYPE TOR_SIMPLE_OBJECT IS RECORD
  (
    GENERATED_DATE              DATE,
    SIMPLE_TOR_REPORT_LIST      PKG_TORS.SIMPLE_TOR_REPORT_LIST
  );


   /* 
    Objeto que modela una lista temporal de attachment. Campos obligatorios marcados con *.
      ATTACH_REFERENCE *          Referencia
      ATTACHMENT_ID *             Id del attachment
  */
  TYPE TEMP_ATT_REF IS RECORD
  (
    ATTACH_REFERENCE            VARCHAR2(255),
    ATTACHMENT_ID               VARCHAR2(255)
  );

  TYPE TEMP_ATTACH_REF_LIST IS TABLE OF TEMP_ATT_REF INDEX BY BINARY_INTEGER;

  -- FUNCIONES 

  /* Inserta un ToR en el sistema.
    Admite un objeto de tipo TOR con toda la informacion del ToR.
    Admite un parámetro de salida con el identificador ToR en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificación o eliminación.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION INSERT_TOR(P_TOR IN PKG_TORS.TOR, P_LA_ID IN VARCHAR2, P_TOR_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

  /* Inserta un BASIC_TOR en el sistema.
    Admite un objeto de tipo TOR_SIMPLE_OBJECT con toda la informacion del ToR.
    Admite un parámetro de salida con el identificador ToR en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificación o eliminación.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION INSERT_BASIC_TOR(P_TOR_SIMPLE_OBJECT IN PKG_TORS.TOR_SIMPLE_OBJECT, P_LA_ID IN VARCHAR2, P_TOR_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

  /* Actualiza un ToR en el sistema.
    Recibe como parametro el identificador del ToR a actualizar.
    Admite un objeto de tipo ToR con la informacion actualizada.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION UPDATE_TOR(P_TOR_ID IN VARCHAR2, P_TOR IN PKG_TORS.TOR, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

  /* Actualiza un BASIC_TOR en el sistema.
    Recibe como parametro el identificador del ToR a actualizar.
    Admite un objeto de tipo BASIC_TOR con la informacion actualizada.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION UPDATE_BASIC_TOR(P_TOR_ID IN VARCHAR2, P_TOR_SIMPLE_OBJECT IN PKG_TORS.TOR_SIMPLE_OBJECT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;    

  /* Elimina un ToR del sistema.
    Recibe como parametro el identificador del ToR a eliminar.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION DELETE_TOR(P_TOR_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

  /* Elimina un BASIC_TOR del sistema.
    Recibe como parametro el identificador del ToR a eliminar.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  FUNCTION DELETE_BASIC_TOR(P_TOR_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

  /* 
  Inserta lista group type
  */
  FUNCTION INSERT_GROUP_TYPE_LIST(P_GROUP_TYPE_LIST IN PKG_TORS.GROUP_TYPE_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

  /* 
  Inserta lista group
  */
  FUNCTION INSERT_GROUP_LIST(P_GROUP_LIST IN PKG_TORS.GROUP_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;    

  /* 
  Inserta grading scheme
  */  
  FUNCTION INSERT_GRADING_SCHEME(P_GRADING_SCHEME IN PKG_TORS.GRADING_SCHEME, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

END PKG_TORS;
/
CREATE OR REPLACE PACKAGE BODY EWP.PKG_TORS AS 
 
  FUNCTION OBTEN_MOBILITY_ID(P_TOR_ID IN VARCHAR2) RETURN VARCHAR2 AS 
    v_m_id VARCHAR2(255);
   CURSOR c_mob(p_id IN VARCHAR2) IS
    SELECT ML.MOBILITY_ID 
    FROM EWPCV_LEARNING_AGREEMENT LA
    INNER JOIN EWPCV_MOBILITY_LA ML ON ML.LEARNING_AGREEMENT_ID = LA.ID
    WHERE LOWER(LA.TOR_ID) = LOWER(p_id);
  BEGIN
    open c_mob(P_TOR_ID);
    FETCH c_mob into v_m_id;
    close c_mob;
    Return v_m_id;
  END;
  
  FUNCTION OBTEN_NOTIFIER_HEI_TOR_INSERT(P_LA_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
    v_s_hei VARCHAR2(255);
    v_r_hei VARCHAR2(255);
    v_la_id VARCHAR2(255);
    v_la_revision NUMBER;
    
    CURSOR c_la_revision(pc_la_id IN VARCHAR2) IS
      SELECT MAX(LEARNING_AGREEMENT_REVISION)
      FROM EWP.EWPCV_LEARNING_AGREEMENT 
      WHERE LOWER(ID) = LOWER(pc_la_id)
    ;
  
    CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS
    SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
    FROM EWPCV_MOBILITY M
      INNER JOIN EWPCV_MOBILITY_LA MLA
        ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
    WHERE LOWER(MLA.LEARNING_AGREEMENT_ID) = LOWER(p_id) AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
  BEGIN 
    OPEN c_la_revision(P_LA_ID);
    FETCH c_la_revision INTO v_la_revision;
    CLOSE c_la_revision;
  
    OPEN c(P_LA_ID, v_la_revision);
    FETCH c into v_r_hei, v_s_hei;
    CLOSE c;
  
    IF LOWER(v_s_hei) = LOWER(P_HEI_TO_NOTIFY) THEN
      RETURN LOWER(v_r_hei);
    ELSE
      RETURN NULL;
    END IF;
  END OBTEN_NOTIFIER_HEI_TOR_INSERT;


  FUNCTION OBTEN_NOTIFIER_HEI_TOR(P_TOR_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
    v_s_hei VARCHAR2(255);
    v_r_hei VARCHAR2(255);
    v_la_id VARCHAR2(255);
    v_la_revision NUMBER;
    
    CURSOR c_la(pc_tor_id IN VARCHAR2) IS
    SELECT LA.ID FROM EWP.EWPCV_LEARNING_AGREEMENT LA
    WHERE LOWER(LA.TOR_ID) = LOWER(P_TOR_ID);
  
  BEGIN 
    OPEN c_la(P_TOR_ID);
    FETCH c_la INTO v_la_id;
    CLOSE c_la;
  
    RETURN OBTEN_NOTIFIER_HEI_TOR_INSERT(v_la_id, P_HEI_TO_NOTIFY);
  END OBTEN_NOTIFIER_HEI_TOR; 



/*
********************************************************************************************
**************************** VALIDACIONES 
**********************************************************************************************
**/
  /*
    Valida los campos obligatorios de un objeto EDUCATION_LEVEL_LIST
  */
  FUNCTION VALIDA_EDUCATION_LEVEL_LIST(P_EDUCATION_LEVEL_LIST IN EDUCATION_LEVEL_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    FOR i IN P_EDUCATION_LEVEL_LIST.FIRST .. P_EDUCATION_LEVEL_LIST.LAST 
    LOOP
      IF P_EDUCATION_LEVEL_LIST(i).LEVEL_TYPE IS NULL THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - P_EDUCATION_LEVEL_LIST(' || i || '):' ||'
              - LEVEL_TYPE');
          v_cod_retorno := -1;
      END IF;

      IF P_EDUCATION_LEVEL_LIST(i).DESCRIPTION IS NULL OR P_EDUCATION_LEVEL_LIST(i).DESCRIPTION.COUNT = 0 THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - P_EDUCATION_LEVEL_LIST(' || i || '):' ||'
              - DESCRIPTION');
          v_cod_retorno := -1;
      END IF;

      IF P_EDUCATION_LEVEL_LIST(i).LEVEL_VALUE IS NULL THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - P_EDUCATION_LEVEL_LIST(' || i || '):' ||'
              - LEVEL_VALUE');
          v_cod_retorno := -1;
      END IF;
    END LOOP;

    RETURN v_cod_retorno;
  END VALIDA_EDUCATION_LEVEL_LIST;  


  FUNCTION VALIDA_LOI_STATUS(P_STATUS IN NUMBER) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_valid_types VARCHAR2(5) := '012';
  BEGIN
    IF INSTR(v_valid_types, TO_CHAR(P_STATUS)) = 0 THEN
      v_cod_retorno := -1;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_LOI_STATUS;
  
  /* 
   * 
  */
  FUNCTION VALIDA_ATTACHMENT(P_ATTACHMENT IN PKG_TORS.ATTACHMENT, P_ERROR_MESSAGE IN OUT VARCHAR2 ) RETURN NUMBER AS
    v_valid_types VARCHAR2(20) := '012345678';
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF INSTR(v_valid_types, TO_CHAR(P_ATTACHMENT.ATT_TYPE)) = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Tipo no válido. 0 - Diploma, 1 - Diploma Supplement, 2 - Transcript of Records, 3 - EMREX transcript, 
              4 - Micro Credential, 5 - Letter of Nomination, 6 - Certificate of Training, 
              7 - Learning Agreement, 8 - Other');
      v_cod_retorno := -1;
    END IF;
  	RETURN v_cod_retorno;
  END VALIDA_ATTACHMENT;
  
  -- Función para validar el tercer nivel de las secciones del diploma
  FUNCTION VALIDA_DIPLOMA_SECTION3(P_DIPLOMA_SECTION IN PKG_TORS.DIPLOMA_SECTION_3, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_DIPLOMA_SECTION.TITLE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - TITLE : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST IS NOT NULL AND P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.COUNT > 0 THEN
        FOR i IN P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.FIRST .. P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.LAST 
         LOOP
            v_cod_retorno_att := VALIDA_ATTACHMENT(P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST(i), P_ERROR_MESSAGE);
            IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
         END LOOP;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_NUMBER IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - SECTION_NUMBER : Debe de ir informado');
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_DIPLOMA_SECTION3;
  
  -- Función para validar el segundo nivel de las secciones del diploma
  FUNCTION VALIDA_DIPLOMA_SECTION2(P_DIPLOMA_SECTION IN PKG_TORS.DIPLOMA_SECTION_2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_DIPLOMA_SECTION.TITLE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - TITLE : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST IS NOT NULL AND P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.COUNT > 0 THEN
        FOR i IN P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.FIRST .. P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.LAST 
         LOOP
            v_cod_retorno_att := VALIDA_ATTACHMENT(P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST(i), P_ERROR_MESSAGE);
            IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
         END LOOP;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_NUMBER IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - SECTION_NUMBER : Debe de ir informado');
    END IF;
    
    IF P_DIPLOMA_SECTION.SECTION_LIST IS NOT NULL AND P_DIPLOMA_SECTION.SECTION_LIST.COUNT > 0 THEN
        FOR i IN P_DIPLOMA_SECTION.SECTION_LIST.FIRST .. P_DIPLOMA_SECTION.SECTION_LIST.LAST 
         LOOP
            v_cod_retorno := VALIDA_DIPLOMA_SECTION3(P_DIPLOMA_SECTION.SECTION_LIST(i), P_ERROR_MESSAGE);
         END LOOP;
    END IF; 
    RETURN v_cod_retorno;
  END VALIDA_DIPLOMA_SECTION2;
  
  -- Función para validar el primer nivel de las secciones del diploma
  FUNCTION VALIDA_DIPLOMA_SECTION(P_DIPLOMA_SECTION IN PKG_TORS.DIPLOMA_SECTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_DIPLOMA_SECTION.TITLE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - TITLE : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA_SECTION.CONTENT IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - CONTENT : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST IS NOT NULL AND P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.COUNT > 0 THEN
        FOR i IN P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.FIRST .. P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST.LAST 
         LOOP
            v_cod_retorno_att := VALIDA_ATTACHMENT(P_DIPLOMA_SECTION.SECTION_ATTACHMENT_LIST(i), P_ERROR_MESSAGE);
            IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
         END LOOP;
    END IF;
    IF P_DIPLOMA_SECTION.SECTION_NUMBER IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA_SECTION:' ||'
                  - SECTION_NUMBER : Debe de ir informado');
    END IF;   
    IF P_DIPLOMA_SECTION.SECTION_LIST IS NOT NULL AND P_DIPLOMA_SECTION.SECTION_LIST.COUNT > 0 THEN
        FOR i IN P_DIPLOMA_SECTION.SECTION_LIST.FIRST .. P_DIPLOMA_SECTION.SECTION_LIST.LAST 
         LOOP
            v_cod_retorno := VALIDA_DIPLOMA_SECTION2(P_DIPLOMA_SECTION.SECTION_LIST(i), P_ERROR_MESSAGE);
         END LOOP;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_DIPLOMA_SECTION;
  
  FUNCTION VALIDA_DIPLOMA(P_DIPLOMA IN PKG_TORS.DIPLOMA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_DIPLOMA.VERSION IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA:' ||'
                  - VERSION : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA.ISSUE_DATE IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA:' ||'
                  - ISSUE_DATE : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA.INTRODUCTION IS NULL THEN 
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - P_DIPLOMA:' ||'
                  - INTRODUCTION : Debe de ir informado');
        v_cod_retorno := -1;
    END IF;
    IF P_DIPLOMA.SECTION_LIST IS NOT NULL AND P_DIPLOMA.SECTION_LIST.COUNT > 0 THEN 
         FOR i IN P_DIPLOMA.SECTION_LIST.FIRST .. P_DIPLOMA.SECTION_LIST.LAST 
         LOOP
            v_cod_retorno_att := VALIDA_DIPLOMA_SECTION(P_DIPLOMA.SECTION_LIST(i), P_ERROR_MESSAGE);
           IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
         END LOOP;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_DIPLOMA;

  /*
    Valida los campos obligatorios de un objeto LOI_LIST
  */
  FUNCTION VALIDA_LOI_LIST(P_LOI_LIST IN LOI_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_comprobacion NUMBER := 0;
  BEGIN
    FOR i IN P_LOI_LIST.FIRST .. P_LOI_LIST.LAST 
    LOOP
      IF P_LOI_LIST(i).LOS_ID IS NULL AND P_LOI_LIST(i).LOS_CODE IS NULL THEN

		  EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
              - LOI_LIST(' || i || '):' ||'
                  - LOS_ID/LOS_CODE : Debe de ir informado alguno de estos dos campos');
      END IF;      

      IF P_LOI_LIST(i).EDUCATION_LEVEL IS NOT NULL AND P_LOI_LIST(i).EDUCATION_LEVEL.COUNT > 0 THEN
          v_cod_retorno := VALIDA_EDUCATION_LEVEL_LIST(P_LOI_LIST(i).EDUCATION_LEVEL, P_ERROR_MESSAGE);
      END IF;
 
      IF P_LOI_LIST(i).DIPLOMA.VERSION IS NOT NULL OR P_LOI_LIST(i).DIPLOMA.ISSUE_DATE IS NOT NULL OR P_LOI_LIST(i).DIPLOMA.INTRODUCTION IS NOT NULL THEN
          v_cod_retorno := VALIDA_DIPLOMA(P_LOI_LIST(i).DIPLOMA, P_ERROR_MESSAGE);
      END IF;
      
    END LOOP;

    RETURN v_cod_retorno;
  END VALIDA_LOI_LIST;  


  /*
    Valida los campos obligatorios de un objeto TOR REPORT
  */
  FUNCTION VALIDA_TOR_REPORT(P_TOR_REPORT_LIST IN TOR_REPORT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_TOR_REPORT_LIST IS NOT NULL AND P_TOR_REPORT_LIST.COUNT > 0 THEN
      FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST 
      LOOP
        IF P_TOR_REPORT_LIST(i).ISSUE_DATE IS NULL THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - TOR_REPORT_LIST(' || i || '):' ||'
                - ISSUE_DATE');
            v_cod_retorno := -1;
        END IF;
        IF P_TOR_REPORT_LIST(i).LOI_LIST IS NULL OR P_TOR_REPORT_LIST(i).LOI_LIST.COUNT = 0 THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            - TOR_REPORT_LIST(' || i || '):' ||'
                - LOI_LIST: No puede ir vacía.');
          v_cod_retorno := -1;
        ELSE 
          v_cod_retorno := VALIDA_LOI_LIST(P_TOR_REPORT_LIST(i).LOI_LIST, P_ERROR_MESSAGE);
        END IF;
        IF P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST IS NOT NULL AND P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.COUNT > 0 THEN
          FOR j IN P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.FIRST .. P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.LAST 
          LOOP
              v_cod_retorno_att := VALIDA_ATTACHMENT(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST(j), P_ERROR_MESSAGE);
              IF v_cod_retorno_att = -1 THEN
                  v_cod_retorno := v_cod_retorno_att;
              END IF;
          END LOOP;
        END IF; 
      END LOOP;
    ELSE
      v_cod_retorno := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - TOR_REPORT_LIST:  La lista de informes no puede ir vacía.');
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_TOR_REPORT;  


  /*
    Valida los campos obligatorios de un objeto TOR
  */
  FUNCTION VALIDA_TOR(P_TOR IN TOR, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    IF P_TOR.GENERATED_DATE IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        TOR: ' || '
            - GENERATED_DATE');
      v_cod_retorno := -1;
    END IF;
    IF P_TOR.TOR_REPORT_LIST IS NOT NULL AND P_TOR.TOR_REPORT_LIST.COUNT > 0 THEN
        v_cod_retorno := VALIDA_TOR_REPORT(P_TOR.TOR_REPORT_LIST, P_ERROR_MESSAGE);
    ELSE 
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        TOR: ' || '
            - TOR_REPORT_LIST');
      v_cod_retorno := -1;
    END IF;
    IF P_TOR.MOBILITY_ATTACHMENT_LIST IS NOT NULL AND P_TOR.MOBILITY_ATTACHMENT_LIST.COUNT > 0 THEN
        FOR i IN P_TOR.MOBILITY_ATTACHMENT_LIST.FIRST .. P_TOR.MOBILITY_ATTACHMENT_LIST.LAST 
        LOOP
            v_cod_retorno_att := VALIDA_ATTACHMENT(P_TOR.MOBILITY_ATTACHMENT_LIST(i), P_ERROR_MESSAGE);
            IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
        END LOOP;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_TOR;

  FUNCTION VALIDA_DATOS_EDUCATION_LEVEL(P_LEVEL IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_valid_types VARCHAR2(4) := '0';
  BEGIN
    IF INSTR(v_valid_types, TO_CHAR(P_LEVEL)) = 0 THEN
      v_cod_retorno := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'Los tipo permitidos son: 0 - EQF (European Qualification Framework) y se ha informado: ' || P_LEVEL);
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_DATOS_EDUCATION_LEVEL;

  FUNCTION EXISTE_TOR_ATTACHMENT(P_REF_ATTACHMENT IN VARCHAR2, P_TOR_ATTACHMENT_LIST IN PKG_TORS.TOR_ATTACHMENT_LIST) RETURN NUMBER AS
    v_cod_retorno NUMBER := -1;
  BEGIN
    IF P_TOR_ATTACHMENT_LIST IS NOT NULL AND P_TOR_ATTACHMENT_LIST.COUNT > 0 THEN 
      FOR i IN  P_TOR_ATTACHMENT_LIST.FIRST .. P_TOR_ATTACHMENT_LIST.LAST
      LOOP
        IF LOWER(P_TOR_ATTACHMENT_LIST(i).ATTACH_REFERENCE) = LOWER(P_REF_ATTACHMENT) THEN
          v_cod_retorno := 0;
        END IF;
        EXIT WHEN v_cod_retorno = 0;
      END LOOP;
    END IF;
    RETURN v_cod_retorno;
  END EXISTE_TOR_ATTACHMENT;

  FUNCTION VALIDA_DATOS_LOI_TOR_ATT_LIST(P_TOR_ATTACHMENT_LIST IN TOR_ATTACHMENT_LIST, 
  P_ATTACHMENTS_REF_LIST IN PKG_TORS.ATTACHMENTS_REF_LIST) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
  	IF P_TOR_ATTACHMENT_LIST IS NOT NULL AND P_ATTACHMENTS_REF_LIST IS NOT NULL 
  	 AND P_TOR_ATTACHMENT_LIST.COUNT > 0 AND P_ATTACHMENTS_REF_LIST.COUNT > 0 THEN 
      FOR i IN P_ATTACHMENTS_REF_LIST.FIRST .. P_ATTACHMENTS_REF_LIST.LAST
      LOOP 
        v_cod_retorno := EXISTE_TOR_ATTACHMENT(P_ATTACHMENTS_REF_LIST(i), P_TOR_ATTACHMENT_LIST);
        EXIT WHEN v_cod_retorno = -1;
      END LOOP;
  	 END IF;
     RETURN v_cod_retorno;
  END VALIDA_DATOS_LOI_TOR_ATT_LIST;

  /*
    Valida datos un objeto LOI_LIST
  */
  FUNCTION VALIDA_DATOS_LOI_LIST(P_LOI_LIST IN LOI_LIST, P_TOR_ATTACHMENT_LIST IN TOR_ATTACHMENT_LIST, P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_iteration NUMBER := 0;
    v_count NUMBER := 0;
    v_error VARCHAR2(2000);
    v_cod_retorno_tor_attachment NUMBER := 0;
  
  BEGIN
      --Recorremos la lista de LOI del REPORT
      FOR i IN P_LOI_LIST.FIRST .. P_LOI_LIST.LAST 
      LOOP    
        -- Comprobamos que exista un componente no reconocido con el loi id informado en el LA
        IF P_LOI_LIST(i).LOI_ID IS NOT NULL THEN
			WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
			SELECT COUNT(1) INTO v_count FROM COMPONENTS  
			WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(P_LA_ID)
			AND LOWER(LOI_ID) = LOWER(P_LOI_LIST(i).LOI_ID);	  
			
            IF v_count = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                               LOI_LIST(' || i || '):' ||
                                '- LOI_ID: El identificador ' || P_LOI_LIST(i).LOI_ID || ' no está asociado a ningún componente del LA');
             v_cod_retorno := -1;
            END IF;
        END IF;
         -- Comprobamos que exista un componente no reconocido con el LOS_ID en el LA y con un LOI asociado
        IF P_LOI_LIST(i).LOS_ID IS NOT NULL THEN
			WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
			SELECT COUNT(1) INTO v_count FROM COMPONENTS  
			WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(P_LA_ID)
			AND LOI_ID IS NOT NULL
			AND LOWER(LOS_ID) = LOWER(P_LOI_LIST(i).LOS_ID);	  
		
			IF v_count = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                               LOI_LIST(' || i || '):' ||
                                '- LOS_ID: El identificador ' || P_LOI_LIST(i).LOS_ID || ' no está asociado a ningún componente del LA con LOI.');
             v_cod_retorno := -1;
            END IF;
        END IF;
        -- Comprobamos que exista un componente no reconocido con el LOS_CODE y con un LOI asociado
        IF P_LOI_LIST(i).LOS_CODE IS NOT NULL THEN
			WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM EWPCV_LA_COMPONENT c LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
			SELECT COUNT(1) INTO v_count FROM COMPONENTS  
			WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(P_LA_ID)
			AND LOI_ID IS NOT NULL
			AND LOWER(LOS_CODE) = LOWER(P_LOI_LIST(i).LOS_CODE);	  
		
            IF v_count = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                               LOI_LIST(' || i || '):' ||
                                '- LOS_CODE: El codigo ' || P_LOI_LIST(i).LOS_CODE || ' no está asociado a ningún componente del LA con LOI.');
             v_cod_retorno := -1;
            ELSE
              SELECT COUNT(1) INTO v_count FROM EWPCV_LOS WHERE LOWER(LOS_CODE) = LOWER(P_LOI_LIST(i).LOS_CODE);
              IF v_count = 0 THEN
                EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                               LOI_LIST(' || i || '):' ||
                                '- LOS_CODE: El codigo ' || P_LOI_LIST(i).LOS_CODE || ' no se corresponde con ningún LOS de la BBDD');
                v_cod_retorno := -1;
              END IF;
            END IF;
        END IF;


        -- Comprobamos el id del Group Type
        IF P_LOI_LIST(i).GROUP_TYPE_ID IS NOT NULL 
        THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_GROUP_TYPE where LOWER(ID) = LOWER(P_LOI_LIST(i).GROUP_TYPE_ID);
            IF v_count = 0 THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                                   LOI_LIST(' || i || '):' ||
                                    '- GROUP_TYPE_ID: El identificador ' || P_LOI_LIST(i).GROUP_TYPE_ID || ' no se corresponde con ningún GROUP_TYPE de la BBDD');
              v_cod_retorno := -1;
            END IF;
            -- Comprobamos el id del Group
            SELECT COUNT(1) INTO v_count FROM EWPCV_GROUP where LOWER(ID) = LOWER(P_LOI_LIST(i).GROUP_ID);
            IF v_count = 0 THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                                   LOI_LIST(' || i || '):' ||
                                    '- GROUP_ID: El identificador ' || P_LOI_LIST(i).GROUP_ID || ' no se corresponde con ningún GROUP de la BBDD');
              v_cod_retorno := -1;
            END IF;
        END IF;
          -- Comprobamos que la lista de attachment_refs coincida con las referencias de la lista de TOR_ATTACHMENT_LIST
        IF P_LOI_LIST(i).ATTACHMENTS_REF_LIST IS NOT NULL AND P_LOI_LIST(i).ATTACHMENTS_REF_LIST.COUNT > 0 THEN
          IF P_TOR_ATTACHMENT_LIST IS NULL OR P_TOR_ATTACHMENT_LIST.COUNT = 0 THEN 
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                                     TOR_ATTACHMENT_LIST: No puede ir vacía la lista de attachment "TOR_ATTACHMENT_LIST" si hemos informado el campo ATTACHMENTS_REF_LIST del LOI con referencias.');
            v_cod_retorno := -1;
          ELSE
            IF VALIDA_DATOS_LOI_TOR_ATT_LIST(P_TOR_ATTACHMENT_LIST, P_LOI_LIST(i).ATTACHMENTS_REF_LIST) < 0 THEN
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                    ATTACHMENTS_REF_LIST y TOR_ATTACHMENT_LIST: Alguna de las referencias informadas en el LOI ' || i || ' no existen en la lista TOR_ATTACHMENT_LIST');
              v_cod_retorno := -1;
            END IF;
          END IF;
        END IF;

        -- Comprobamos el status
        IF P_LOI_LIST(i).STATUS IS NOT NULL AND VALIDA_LOI_STATUS(P_LOI_LIST(i).STATUS) < 0 THEN
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                                 LOI_LIST(' || i || '):' ||
                                  '-STATUS: Los estados permitidos son: 0 - PASSED, 1 - FAILED, 2 - IN-PROGRESS y se ha informado ' || P_LOI_LIST(i).STATUS);
        END IF;

        -- Comprobamos la lista de education_level
        IF P_LOI_LIST(i).EDUCATION_LEVEL IS NOT NULL AND P_LOI_LIST(i).EDUCATION_LEVEL.COUNT > 0 THEN 
          FOR j IN P_LOI_LIST(i).EDUCATION_LEVEL.FIRST .. P_LOI_LIST(i).EDUCATION_LEVEL.LAST LOOP
            IF VALIDA_DATOS_EDUCATION_LEVEL(P_LOI_LIST(i).EDUCATION_LEVEL(j).LEVEL_TYPE, v_error) < 0 THEN 
              v_cod_retorno := -1;
              EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                                 LOI_LIST(' || i || '):' ||
                                  '-EDUCATION_LIST('|| j || '): ' || v_error);
              v_error := '';
            END IF;
          END LOOP;
        END IF;
      END LOOP;
    RETURN v_cod_retorno;
  END VALIDA_DATOS_LOI_LIST;  


  /*
    Valida datos un objeto TOR_ATTACHMENT_LIST
  */
  FUNCTION VALIDA_DATOS_TOR_ATT_LIST(P_TOR_ATTACHMENT_LIST IN TOR_ATTACHMENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    --Recorremos la lista de LOIS del REPORT y comprobamos que no haya 2 referencias iguales
    FOR i IN P_TOR_ATTACHMENT_LIST.FIRST .. P_TOR_ATTACHMENT_LIST.LAST 
    LOOP
      FOR j IN i + 1 .. P_TOR_ATTACHMENT_LIST.LAST 
      LOOP 
        IF LOWER(P_TOR_ATTACHMENT_LIST(i).ATTACH_REFERENCE) = LOWER(P_TOR_ATTACHMENT_LIST(j).ATTACH_REFERENCE) THEN
          EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
            Referencia ' || P_TOR_ATTACHMENT_LIST(i).ATTACH_REFERENCE || ' repetida');
        v_cod_retorno := -1;
        END IF;
      END LOOP;
    END LOOP;
    RETURN v_cod_retorno;
  END VALIDA_DATOS_TOR_ATT_LIST;  


  /*
    Valida datos un objeto TOR_REPORT_LIST
  */
  FUNCTION VALIDA_DATOS_TOR_REPORT_LIST(P_TOR_REPORT_LIST IN TOR_REPORT_LIST, P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    -- Recorremos la lista de report del TOR
    FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST 
    LOOP
      IF P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST IS NOT NULL AND P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.COUNT > 0 THEN
        v_cod_retorno := VALIDA_DATOS_TOR_ATT_LIST(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST, P_ERROR_MESSAGE);
      END IF;
      IF v_cod_retorno = 0 THEN
        IF P_TOR_REPORT_LIST(i).LOI_LIST IS NOT NULL AND P_TOR_REPORT_LIST(i).LOI_LIST.COUNT > 0 THEN
          v_cod_retorno := VALIDA_DATOS_LOI_LIST(P_TOR_REPORT_LIST(i).LOI_LIST, P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST, P_LA_ID, P_ERROR_MESSAGE);
        END IF;
      END IF;
    END LOOP;

    RETURN v_cod_retorno;
  END VALIDA_DATOS_TOR_REPORT_LIST;  

  /*
    Valida datos un objeto TOR
  */
  FUNCTION VALIDA_DATOS_TOR(P_TOR IN TOR, P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_TOR.TOR_REPORT_LIST IS NOT NULL AND P_TOR.TOR_REPORT_LIST.COUNT > 0 THEN
      v_cod_retorno := VALIDA_DATOS_TOR_REPORT_LIST(P_TOR.TOR_REPORT_LIST, P_LA_ID, P_ERROR_MESSAGE);
    END IF;

    RETURN v_cod_retorno;
  END VALIDA_DATOS_TOR;  

  /*
    Valida una lista de Group
  */
  FUNCTION VALIDA_GROUP_LIST(P_GROUP_LIST IN PKG_TORS.GROUP_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_GROUP_LIST IS NULL OR P_GROUP_LIST.COUNT = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La lista GROUP no puede estar vacía.');
      RETURN -1;
    END IF;

    FOR i IN P_GROUP_LIST.FIRST .. P_GROUP_LIST.LAST 
    LOOP
      IF P_GROUP_LIST(i).TITLE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'GROUP_LIST(' || i || '):' ||
                                '- TITLE');
        v_cod_retorno := -1;
      END IF;
    END LOOP;

    RETURN v_cod_retorno;
  END VALIDA_GROUP_LIST;  

  /*
    Valida una lista de GroupType
  */
  FUNCTION VALIDA_GROUP_TYPE_LIST(P_GROUP_TYPE_LIST IN PKG_TORS.GROUP_TYPE_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_GROUP_TYPE_LIST IS NULL OR P_GROUP_TYPE_LIST.COUNT = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La lista GROUP_TYPE no puede estar vacía.');
      RETURN -1;
    END IF;

    FOR i IN P_GROUP_TYPE_LIST.FIRST .. P_GROUP_TYPE_LIST.LAST 
    LOOP
      IF P_GROUP_TYPE_LIST(i).TITLE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'GROUP_TYPE_LIST(' || i || '):' ||
                                '- TITLE');
        v_cod_retorno := -1;
      END IF;
      IF P_GROUP_TYPE_LIST(i).GROUP_LIST IS NOT NULL AND P_GROUP_TYPE_LIST(i).GROUP_LIST.COUNT > 0 THEN
        v_cod_retorno := VALIDA_GROUP_LIST(P_GROUP_TYPE_LIST(i).GROUP_LIST, P_ERROR_MESSAGE);
      END IF;
    END LOOP;

    RETURN v_cod_retorno;
  END VALIDA_GROUP_TYPE_LIST;  

  /*
    Valida una lista de Group
  */
  FUNCTION VALIDA_GRADING_SCHEME(P_GRADING_SCHEME IN PKG_TORS.GRADING_SCHEME, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_GRADING_SCHEME.LABEL IS NULL OR P_GRADING_SCHEME.LABEL.COUNT = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'P_GRADING_SCHEME.LABEL debe contener datos');
      RETURN -1;
    END IF;

   IF P_GRADING_SCHEME.GRAD_DESCRIPTION IS NULL OR P_GRADING_SCHEME.GRAD_DESCRIPTION.COUNT = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'P_GRADING_SCHEME.GRAD_DESCRIPTION debe contener datos');
      RETURN -1;
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_GRADING_SCHEME;

  /*
    Valida los campos obligatorios de un objeto BASIC_TOR_REPORT
  */
  FUNCTION VALIDA_BASIC_TOR_REPORT(P_TOR_REPORT_LIST IN SIMPLE_TOR_REPORT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_cod_retorno_att NUMBER := 0;
  BEGIN
    FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST 
    LOOP
      IF P_TOR_REPORT_LIST(i).ISSUE_DATE IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - TOR_REPORT_LIST(' || i || '):' ||'
              - ISSUE_DATE');
          v_cod_retorno := -1;
      END IF;
      IF P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST IS NOT NULL AND P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.COUNT > 0 THEN
        FOR j IN P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.FIRST .. P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.LAST 
        LOOP
            v_cod_retorno_att := VALIDA_ATTACHMENT(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST(j), P_ERROR_MESSAGE);
            IF v_cod_retorno_att = -1 THEN
                v_cod_retorno := v_cod_retorno_att;
            END IF;
        END LOOP;
      ELSE
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - TOR_REPORT_LIST(' || i || '):' ||'
              - TOR_ATTACHMENT_LIST');
          v_cod_retorno := -1;
      END IF;
    END LOOP;
    RETURN v_cod_retorno;
  END VALIDA_BASIC_TOR_REPORT;  

  /*
    Valida los campos obligatorios de un objeto TOR
  */
  FUNCTION VALIDA_BASIC_TOR(P_TOR_SIMPLE_OBJECT IN TOR_SIMPLE_OBJECT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
  BEGIN
    IF P_TOR_SIMPLE_OBJECT.GENERATED_DATE IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        TOR: ' || '
            - GENERATED_DATE');
      v_cod_retorno := -1;
    END IF;
    IF P_TOR_SIMPLE_OBJECT.SIMPLE_TOR_REPORT_LIST IS NOT NULL AND P_TOR_SIMPLE_OBJECT.SIMPLE_TOR_REPORT_LIST.COUNT > 0 THEN
        v_cod_retorno := VALIDA_BASIC_TOR_REPORT(P_TOR_SIMPLE_OBJECT.SIMPLE_TOR_REPORT_LIST, P_ERROR_MESSAGE);
    ELSE 
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
        TOR: ' || '
            - TOR_REPORT_LIST');
      v_cod_retorno := -1;
    END IF;

    RETURN v_cod_retorno;
  END VALIDA_BASIC_TOR;


/*********************************************************************************************
**************************** INSERCCIONES
**********************************************************************************************
**/

  /*
    Inserción datos un objeto ATTACHMENT
  */
  FUNCTION INSERTA_ATTACHMENT(P_ATTACHMENT PKG_TORS.ATTACHMENT) RETURN VARCHAR2 AS
    v_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
    v_c_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID(); 

    INSERT INTO EWPCV_ATTACHMENT (ID, VERSION, ATTACH_TYPE, EXTENSION) 
    VALUES (v_id, 0, P_ATTACHMENT.ATT_TYPE, P_ATTACHMENT.EXTENSION);

    IF P_ATTACHMENT.TITLE IS NOT NULL AND P_ATTACHMENT.TITLE.COUNT > 0 THEN
      FOR i IN P_ATTACHMENT.TITLE.FIRST .. P_ATTACHMENT.TITLE.LAST 
      LOOP
        v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ATTACHMENT.TITLE(i));
        INSERT INTO EWPCV_ATTACHMENT_TITLE (ATTACHMENT_ID , ID) VALUES (v_id, v_li_id);
      END LOOP;
    END IF;

    IF P_ATTACHMENT.DESCRIPTION IS NOT NULL AND P_ATTACHMENT.DESCRIPTION.COUNT > 0 THEN
      FOR i IN P_ATTACHMENT.DESCRIPTION.FIRST .. P_ATTACHMENT.DESCRIPTION.LAST 
      LOOP
        v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ATTACHMENT.DESCRIPTION(i));
        INSERT INTO EWPCV_ATTACHMENT_DESCRIPTION (ATTACHMENT_ID , ID) VALUES (v_id, v_li_id);
      END LOOP;
    END IF;

    IF P_ATTACHMENT.CONTENT IS NOT NULL AND P_ATTACHMENT.CONTENT.COUNT > 0 THEN
      FOR i IN P_ATTACHMENT.CONTENT.FIRST .. P_ATTACHMENT.CONTENT.LAST 
      LOOP
        v_c_id:= EWP.GENERATE_UUID();
        INSERT INTO EWPCV_ATTACHMENT_CONTENT (ATTACHMENT_ID , ID, LANG, CONTENT) 
        VALUES (v_id, v_c_id, P_ATTACHMENT.CONTENT(i).LANG, P_ATTACHMENT.CONTENT(i).BLOB_CONTENT);
      END LOOP;
    END IF;
    RETURN v_id;
  END INSERTA_ATTACHMENT;


  /*
    Inserción datos un objeto con un listado de attachment en el objeto section
  */
  PROCEDURE INSERTA_SECTION_ATT_LIST(P_SECTION_ATTACHMENT_LIST IN PKG_TORS.SECTION_ATTACHMENT_LIST, P_SECTION_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_SECTION_ATTACHMENT_LIST IS NOT NULL AND P_SECTION_ATTACHMENT_LIST.COUNT > 0 THEN
      FOR i IN P_SECTION_ATTACHMENT_LIST.FIRST .. P_SECTION_ATTACHMENT_LIST.LAST
   	  LOOP
   	    v_id := INSERTA_ATTACHMENT(P_SECTION_ATTACHMENT_LIST(i));
   	    INSERT INTO EWPCV_DIPLOMA_SEC_ATTACH (ATTACHMENT_ID, SECTION_ID)
   	    VALUES (v_id, P_SECTION_ID);
   		END LOOP;
    END IF;
  END INSERTA_SECTION_ATT_LIST;


  /*
    Inserción datos un objeto con un listado de attachment en el objeto section
  */
  PROCEDURE INSERTA_TOR_ATTACHMENT_LIST(P_TOR_ATTACHMENT_LIST IN PKG_TORS.TOR_ATTACHMENT_LIST,P_TOR_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_TOR_ATTACHMENT_LIST IS NOT NULL AND P_TOR_ATTACHMENT_LIST.COUNT > 0 THEN
      FOR i IN P_TOR_ATTACHMENT_LIST.FIRST .. P_TOR_ATTACHMENT_LIST.LAST
      LOOP
        v_id := INSERTA_ATTACHMENT(P_TOR_ATTACHMENT_LIST(i));
        INSERT INTO EWPCV_TOR_ATTACHMENT (ATTACHMENT_ID, TOR_ID)
        VALUES (v_id, P_TOR_ID);
      END LOOP;
    END IF;
  END INSERTA_TOR_ATTACHMENT_LIST;


  /*
    Inserción datos un objeto con un listado de mobility attachment en el objeto tor
  */
  PROCEDURE INSERTA_MOBILITY_ATT_LIST(P_MOBILITY_ATTACHMENT_LIST IN PKG_TORS.MOBILITY_ATTACHMENT_LIST, P_TOR_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    IF P_MOBILITY_ATTACHMENT_LIST IS NOT NULL AND P_MOBILITY_ATTACHMENT_LIST.COUNT > 0 THEN
      FOR i IN P_MOBILITY_ATTACHMENT_LIST.FIRST .. P_MOBILITY_ATTACHMENT_LIST.LAST
      LOOP
        v_id := INSERTA_ATTACHMENT(P_MOBILITY_ATTACHMENT_LIST(i));
		    INSERT INTO EWPCV_TOR_ATTACHMENT(ATTACHMENT_ID, TOR_ID) VALUES (v_id, P_TOR_ID);
      END LOOP;
    END IF;
  END INSERTA_MOBILITY_ATT_LIST;


  /*
    Inserción datos un objeto con un listado de dist_categories en el objeto result_distribution
  */
  PROCEDURE INSERTA_DIST_CATEGORIES_LIST(P_DIST_CATEGORIES_LIST IN PKG_TORS.DIST_CATEGORIES_LIST, P_RESULT_DIST_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
    IF P_DIST_CATEGORIES_LIST IS NOT NULL AND P_DIST_CATEGORIES_LIST.COUNT > 0 THEN
      FOR i IN P_DIST_CATEGORIES_LIST.FIRST .. P_DIST_CATEGORIES_LIST.LAST 
      LOOP
		v_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_RESULT_DIST_CATEGORY (ID, VERSION, DISTRUBTION_COUNT, LABEL, RESULT_DIST_ID)
        VALUES(v_id, 0, P_DIST_CATEGORIES_LIST(i).DIST_CAT_COUNT, P_DIST_CATEGORIES_LIST(i).LABEL, P_RESULT_DIST_ID);

      END LOOP;
    END IF;
  END INSERTA_DIST_CATEGORIES_LIST;


  /*
    Inserción datos un objeto result_distribution
  */
  FUNCTION INSERTA_RESULT_DISTRIBUTION(P_RESULT_DISTRIBUTION IN PKG_TORS.RESULT_DISTRIBUTION ) RETURN VARCHAR2 AS
    v_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();
    INSERT INTO EWPCV_RESULT_DISTRIBUTION (ID, VERSION) VALUES (v_id, 0);

    INSERTA_DIST_CATEGORIES_LIST(P_RESULT_DISTRIBUTION.DIST_CATEGORIES_LIST, v_id);

    IF P_RESULT_DISTRIBUTION.DESCRIPTION IS NOT NULL AND P_RESULT_DISTRIBUTION.DESCRIPTION.COUNT > 0 THEN 
      FOR i IN P_RESULT_DISTRIBUTION.DESCRIPTION.FIRST .. P_RESULT_DISTRIBUTION.DESCRIPTION.LAST 
      LOOP
        v_li_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_RESULT_DISTRIBUTION.DESCRIPTION(i));
        INSERT INTO EWPCV_DISTR_DESCRIPTION (RESULT_DISTRIBUTION_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
      END LOOP;
    END IF;

    RETURN v_id;  
  END INSERTA_RESULT_DISTRIBUTION;


  /* 
   * Inserción datos un objeto con un listado de información adicional
   */
  PROCEDURE INSERTA_ADDITIONAL_INF_LIST(P_ADDITIONAL_INF_LIST IN PKG_TORS.ADDITIONAL_INF_LIST, P_DIPLOMA_SECTION_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();
    IF P_ADDITIONAL_INF_LIST IS NOT NULL AND P_ADDITIONAL_INF_LIST.COUNT > 0 THEN
    	FOR i IN P_ADDITIONAL_INF_LIST.FIRST .. P_ADDITIONAL_INF_LIST.LAST
    	LOOP
    		INSERT INTO EWPCV_ADDITIONAL_INFO (ID, VERSION, SECTION_ID, ADDITIONAL_INFO)
    		VALUES (v_id, 0, P_DIPLOMA_SECTION_ID, P_ADDITIONAL_INF_LIST(i));
    	END LOOP;
    END IF;
  END INSERTA_ADDITIONAL_INF_LIST;

  /* Inserción datos un objeto con un listado de secciones de diploma nivel 3 */
  PROCEDURE INSERTA_DIPLOMA_SECTION_LIST3(P_SECTION_LIST IN PKG_TORS.SECTION_LIST_3, P_DIPLOMA_ID IN VARCHAR2, P_SECTION_ID IN VARCHAR2) IS
    v_s_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
    IF P_SECTION_LIST IS NOT NULL AND P_SECTION_LIST.COUNT > 0 THEN
    FOR i IN P_SECTION_LIST.FIRST .. P_SECTION_LIST.LAST
    LOOP
      v_s_id := EWP.GENERATE_UUID();

      INSERT INTO EWPCV_DIPLOMA_SECTION (ID, VERSION, TITLE, CONTENT, SECTION_NUMBER, DIPLOMA_ID)
      VALUES (v_s_id, 0, P_SECTION_LIST(i).TITLE, P_SECTION_LIST(i).CONTENT, P_SECTION_LIST(i).SECTION_NUMBER , P_DIPLOMA_ID);

      INSERTA_ADDITIONAL_INF_LIST(P_SECTION_LIST(i).ADDITIONAL_INF_LIST, v_s_id);

      INSERTA_SECTION_ATT_LIST(P_SECTION_LIST(i).SECTION_ATTACHMENT_LIST, v_s_id);

      IF P_SECTION_ID IS NOT NULL THEN
        INSERT INTO EWPCV_DIPLOMA_SECTION_SECTION (ID, SECTION_ID) VALUES (v_s_id, P_SECTION_ID);
      END IF;

    END LOOP;
   END IF;
  END INSERTA_DIPLOMA_SECTION_LIST3;


  /* Inserción datos un objeto con un listado de secciones de diploma nivel 2 */
  PROCEDURE INSERTA_DIPLOMA_SECTION_LIST2(P_SECTION_LIST IN PKG_TORS.SECTION_LIST_2, P_DIPLOMA_ID IN VARCHAR2, P_SECTION_ID IN VARCHAR2) IS
    v_s_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
    IF P_SECTION_LIST IS NOT NULL AND P_SECTION_LIST.COUNT > 0 THEN
    FOR i IN P_SECTION_LIST.FIRST .. P_SECTION_LIST.LAST
    LOOP
      v_s_id := EWP.GENERATE_UUID();

      INSERT INTO EWPCV_DIPLOMA_SECTION (ID, VERSION, TITLE, CONTENT, SECTION_NUMBER, DIPLOMA_ID)
      VALUES (v_s_id, 0, P_SECTION_LIST(i).TITLE, P_SECTION_LIST(i).CONTENT, P_SECTION_LIST(i).SECTION_NUMBER , P_DIPLOMA_ID);

      INSERTA_ADDITIONAL_INF_LIST(P_SECTION_LIST(i).ADDITIONAL_INF_LIST, v_s_id);

      INSERTA_SECTION_ATT_LIST(P_SECTION_LIST(i).SECTION_ATTACHMENT_LIST, v_s_id);

      IF P_SECTION_ID IS NOT NULL THEN
        INSERT INTO EWPCV_DIPLOMA_SECTION_SECTION (ID, SECTION_ID) VALUES (v_s_id, P_SECTION_ID);
      END IF;

      INSERTA_DIPLOMA_SECTION_LIST3(P_SECTION_LIST(i).SECTION_LIST, P_DIPLOMA_ID, v_s_id);

    END LOOP;
   END IF;
  END INSERTA_DIPLOMA_SECTION_LIST2;


  /* Inserción datos un objeto con un listado de secciones de diploma nivel 1 */
  PROCEDURE INSERTA_DIPLOMA_SECTION_LIST(P_SECTION_LIST IN PKG_TORS.SECTION_LIST, P_DIPLOMA_ID IN VARCHAR2, P_SECTION_ID IN VARCHAR2) IS
    v_s_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
  	IF P_SECTION_LIST IS NOT NULL AND P_SECTION_LIST.COUNT > 0 THEN
   	FOR i IN P_SECTION_LIST.FIRST .. P_SECTION_LIST.LAST
   	LOOP
      v_s_id := EWP.GENERATE_UUID();

      INSERT INTO EWPCV_DIPLOMA_SECTION (ID, VERSION, TITLE, CONTENT, SECTION_NUMBER, DIPLOMA_ID)
      VALUES (v_s_id, 0, P_SECTION_LIST(i).TITLE, P_SECTION_LIST(i).CONTENT, P_SECTION_LIST(i).SECTION_NUMBER , P_DIPLOMA_ID);

      INSERTA_ADDITIONAL_INF_LIST(P_SECTION_LIST(i).ADDITIONAL_INF_LIST, v_s_id);

      INSERTA_SECTION_ATT_LIST(P_SECTION_LIST(i).SECTION_ATTACHMENT_LIST, v_s_id);

      INSERTA_DIPLOMA_SECTION_LIST2(P_SECTION_LIST(i).SECTION_LIST, P_DIPLOMA_ID, v_s_id);

   	END LOOP;
   END IF;
  END INSERTA_DIPLOMA_SECTION_LIST;


  /*
    Inserción datos un objeto diploma
  */
  FUNCTION INSERTA_DIPLOMA(P_DIPLOMA IN PKG_TORS.DIPLOMA) RETURN VARCHAR2 AS
    v_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();

    INSERT INTO EWPCV_DIPLOMA (ID, VERSION, DIPLOMA_VERSION, ISSUE_DATE, INTRODUCTION, SIGNATURE)
    VALUES(v_id, 0, P_DIPLOMA.VERSION, P_DIPLOMA.ISSUE_DATE, P_DIPLOMA.INTRODUCTION, P_DIPLOMA.SIGNATURE);

    INSERTA_DIPLOMA_SECTION_LIST(P_DIPLOMA.SECTION_LIST, v_id, NULL);

    RETURN v_id;
  END INSERTA_DIPLOMA;


  /*
    Inserción datos un objeto education_level
  */
  FUNCTION INSERTA_EDUCATION_LEVEL(P_EDUCATION_LEVEL IN PKG_TORS.EDUCATION_LEVEL) RETURN VARCHAR2 AS
    v_id VARCHAR2(255);
    v_li_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();

    INSERT INTO EWPCV_LEVEL (ID, VERSION, LEVEL_TYPE, LEVEL_VALUE) 
    VALUES (v_id, 0, P_EDUCATION_LEVEL.LEVEL_TYPE, P_EDUCATION_LEVEL.LEVEL_VALUE);

    IF P_EDUCATION_LEVEL.DESCRIPTION IS NOT NULL AND P_EDUCATION_LEVEL.DESCRIPTION.COUNT > 0 THEN 
      FOR i IN P_EDUCATION_LEVEL.DESCRIPTION.FIRST .. P_EDUCATION_LEVEL.DESCRIPTION.LAST 
      LOOP
        v_li_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_EDUCATION_LEVEL.DESCRIPTION(i));
        INSERT INTO EWPCV_LEVEL_DESCRIPTION (LEVEL_ID, LEVEL_DESCRIPTION_ID) VALUES (v_id, v_li_id);
      END LOOP;
    END IF;
    RETURN v_id;
  END INSERTA_EDUCATION_LEVEL;


  /*
    Inserción datos un objeto de lista de education level
  */
  PROCEDURE INSERTA_EDUCATION_LEVEL_LIST(P_EDUCATION_LEVEL_LIST IN PKG_TORS.EDUCATION_LEVEL_LIST, P_LOI_ID IN VARCHAR2) IS
    v_el_id VARCHAR2(255);
  BEGIN
  	IF P_EDUCATION_LEVEL_LIST IS NOT NULL AND P_EDUCATION_LEVEL_LIST.COUNT > 0 THEN
     	FOR i IN P_EDUCATION_LEVEL_LIST.FIRST .. P_EDUCATION_LEVEL_LIST.LAST 
     	LOOP
        v_el_id :=	INSERTA_EDUCATION_LEVEL(P_EDUCATION_LEVEL_LIST(i));
        INSERT INTO EWPCV_LEVELS (LOI_ID, LEVEL_ID) VALUES (P_LOI_ID, v_el_id);
     	END LOOP;
    END IF;
  END INSERTA_EDUCATION_LEVEL_LIST;

  /*
    Inserción datos un objeto grade_FREQUENCY
  */
  PROCEDURE INSERTA_GRADE_FREQUENCY(P_GRADE_FREQUENCY IN PKG_TORS.GRADE_FREQUENCY, P_ISCED_TABLE_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();
  	INSERT INTO EWPCV_GRADE_FREQUENCY (ID, LABEL, PERCENTAGE, ISCED_TABLE_ID)
    VALUES (v_id, P_GRADE_FREQUENCY.LABEL, P_GRADE_FREQUENCY.PERCENTAGE, P_ISCED_TABLE_ID);
  END INSERTA_GRADE_FREQUENCY;

  PROCEDURE INSERTA_GRADE_FREQUENCY_LIST(P_GRADE_FREQUENCY_LIST IN PKG_TORS.GRADE_FREQUENCY_LIST, P_ISCED_TABLE_ID IN VARCHAR2) IS
  BEGIN
    IF P_GRADE_FREQUENCY_LIST IS NOT NULL AND P_GRADE_FREQUENCY_LIST.COUNT > 0 THEN
    	FOR i IN P_GRADE_FREQUENCY_LIST.FIRST .. P_GRADE_FREQUENCY_LIST.LAST
    	LOOP
    		INSERTA_GRADE_FREQUENCY(P_GRADE_FREQUENCY_LIST(i), P_ISCED_TABLE_ID);
    	END LOOP;
    END IF;	
  END INSERTA_GRADE_FREQUENCY_LIST;


  /*
    Inserción datos un objeto isced_table
  */
  PROCEDURE INSERTA_ISCED_TABLE(P_ISCED_TABLE IN PKG_TORS.ISCED_TABLE, P_TOR_ID IN VARCHAR2) IS
    v_id VARCHAR2(255);
  BEGIN
    v_id := EWP.GENERATE_UUID();
    INSERT INTO EWPCV_ISCED_TABLE (ID, ISCED_CODE, TOR_ID)
    VALUES (v_id, P_ISCED_TABLE.ISCED_CODE, P_TOR_ID);

    INSERTA_GRADE_FREQUENCY_LIST(P_ISCED_TABLE.GRADE_FREQUENCY_LIST, v_id);

  END INSERTA_ISCED_TABLE;

  PROCEDURE INSERTA_ISCED_TABLE_LIST(P_ISCED_TABLE_LIST IN PKG_TORS.ISCED_TABLE_LIST, P_TOR_ID IN VARCHAR2) IS
  BEGIN
    IF P_ISCED_TABLE_LIST IS NOT NULL AND P_ISCED_TABLE_LIST.COUNT > 0 THEN
    	FOR i IN P_ISCED_TABLE_LIST.FIRST .. P_ISCED_TABLE_LIST.LAST
    	LOOP
    		INSERTA_ISCED_TABLE(P_ISCED_TABLE_LIST(i), P_TOR_ID);
    	END LOOP;
    END IF;
  END INSERTA_ISCED_TABLE_LIST;


  FUNCTION INSERTA_LISTA_TEMP(P_REFERENCIA IN PKG_TORS.TEMP_ATT_REF, P_TEMP_REF_LIST IN PKG_TORS.TEMP_ATTACH_REF_LIST) 
  RETURN PKG_TORS.TEMP_ATTACH_REF_LIST AS
    v_temp_list PKG_TORS.TEMP_ATTACH_REF_LIST;
    v_exit NUMBER := 0;
  BEGIN
    v_temp_list := P_TEMP_REF_LIST;

    IF P_TEMP_REF_LIST IS NOT NULL AND P_TEMP_REF_LIST.COUNT = 0 THEN 
      v_temp_list(0) := P_REFERENCIA;
    ELSE
      FOR i IN P_TEMP_REF_LIST.FIRST .. P_TEMP_REF_LIST.LAST
      LOOP
        IF LOWER(P_REFERENCIA.ATTACH_REFERENCE) = LOWER(P_TEMP_REF_LIST(i).ATTACH_REFERENCE) THEN
          v_exit := 1;
        ELSE
          v_temp_list(v_temp_list.COUNT) := P_REFERENCIA;
          v_exit := 1;
        END IF;
        EXIT WHEN v_exit = 1;
      END LOOP;
    END IF;
    RETURN v_temp_list;
  END INSERTA_LISTA_TEMP;

  FUNCTION EXISTE_REF_ATTACHMENT(P_REF_ATTACHMENT IN VARCHAR2, P_ATTACHMENTS_REF_LIST IN PKG_TORS.TEMP_ATTACH_REF_LIST) RETURN NUMBER AS
    v_cod_retorno NUMBER := -1;
  BEGIN
    IF P_ATTACHMENTS_REF_LIST IS NOT NULL AND P_ATTACHMENTS_REF_LIST.COUNT > 0 THEN 
      FOR i IN  P_ATTACHMENTS_REF_LIST.FIRST .. P_ATTACHMENTS_REF_LIST.LAST
      LOOP
        IF LOWER(P_ATTACHMENTS_REF_LIST(i).ATTACH_REFERENCE) = LOWER(P_REF_ATTACHMENT) THEN
          v_cod_retorno := 0;
        END IF;
        EXIT WHEN v_cod_retorno = 0;
      END LOOP;
    END IF;
    RETURN v_cod_retorno;
  END EXISTE_REF_ATTACHMENT;

  FUNCTION GET_ATTACHMENT_ID_BY_REF(P_TEMP_REF IN VARCHAR2, P_TEMP_ATTACH_REF_LIST IN PKG_TORS.TEMP_ATTACH_REF_LIST) RETURN VARCHAR2 AS
    v_resultado VARCHAR2(255) := NULL;
  BEGIN
    IF P_TEMP_ATTACH_REF_LIST IS NOT NULL AND P_TEMP_ATTACH_REF_LIST.COUNT > 0 THEN 
      FOR i IN  P_TEMP_ATTACH_REF_LIST.FIRST .. P_TEMP_ATTACH_REF_LIST.LAST
      LOOP
        IF LOWER(P_TEMP_ATTACH_REF_LIST(i).ATTACH_REFERENCE) = LOWER(P_TEMP_REF) THEN
          v_resultado := P_TEMP_ATTACH_REF_LIST(i).ATTACHMENT_ID;
        END IF;
        EXIT WHEN v_resultado IS NOT NULL;
      END LOOP;
    END IF;
    RETURN v_resultado;
  END GET_ATTACHMENT_ID_BY_REF;

  /*
    Inserción datos un objeto loi
  */
  PROCEDURE INSERTA_LOI(P_LOI IN PKG_TORS.LOI, P_TOR_ATTACHMENT_LIST IN PKG_TORS.TOR_ATTACHMENT_LIST, P_REPORT_ID IN VARCHAR2, 
  P_TEMP_REF_LIST IN OUT PKG_TORS.TEMP_ATTACH_REF_LIST) IS
    v_loi_id VARCHAR2(255);
    v_los_id VARCHAR2(255);
    v_attachment_id VARCHAR2(255);
    v_result_distibution_id VARCHAR2(255);
    v_diploma_id VARCHAR2(255);
    v_temp_ref PKG_TORS.TEMP_ATT_REF;
    v_count_los_loi NUMBER;
    CURSOR c_loi_los_code(p_los_code VARCHAR2) IS SELECT LOI_ID
        FROM EWPCV_LA_COMPONENT 
        WHERE LOS_CODE = p_los_code;
    CURSOR c_loi_los_id(p_los_id VARCHAR2) IS SELECT LOI_ID
        FROM EWPCV_LA_COMPONENT 
        WHERE LOS_ID = p_los_id;
    CURSOR c_los_id(p_los_code VARCHAR2) IS SELECT LOS_ID
        FROM EWPCV_LA_COMPONENT 
        WHERE LOS_CODE = p_los_code;
    CURSOR c_los_loi_id(p_loi_id VARCHAR2) IS SELECT LOS_ID
        FROM EWPCV_LA_COMPONENT 
        WHERE LOI_ID = p_loi_id;

    CURSOR c_los_id_los(p_los_code IN VARCHAR) IS SELECT ID
      FROM EWPCV_LOS 
      WHERE LOS_CODE = p_los_code;
  BEGIN

    IF P_LOI.LOI_ID IS NOT NULL THEN
        v_loi_id := P_LOI.LOI_ID;
        -- Recuperamos el id del LOS
        OPEN c_los_loi_id(v_loi_id);
        FETCH c_los_loi_id INTO v_los_id;
        CLOSE c_los_loi_id;
    ELSIF P_LOI.LOS_CODE IS NOT NULL THEN
        -- Recuperamos el id del LOI
        OPEN c_loi_los_code(P_LOI.LOS_CODE);
        FETCH c_loi_los_code INTO v_loi_id;
        CLOSE c_loi_los_code;
        -- Recuperamos el id del LOS
        OPEN c_los_id(P_LOI.LOS_CODE);
        FETCH c_los_id INTO v_los_id;
        CLOSE c_los_id;

        IF v_los_id IS NULL THEN
          OPEN c_los_id_los(P_LOI.LOS_CODE);
          FETCH c_los_id_los INTO v_los_id;
          CLOSE c_los_id_los;
        END IF;
    ELSE
        OPEN c_loi_los_id(P_LOI.LOS_ID);
        FETCH c_loi_los_id INTO v_loi_id;
        CLOSE c_loi_los_id;
        v_los_id := P_LOI.LOS_ID;
    END IF;

    v_result_distibution_id := INSERTA_RESULT_DISTRIBUTION(P_LOI.RESULT_DISTRIBUTION);

    v_diploma_id := INSERTA_DIPLOMA(P_LOI.DIPLOMA);

    UPDATE EWPCV_LOI SET 
        ENGAGEMENT_HOURS = P_LOI.ENGAGEMENT_HOURS, 
        LANGUAGE_OF_INSTRUCTION = P_LOI.LANGUAGE_OF_INSTRUCTION, 
        GRADING_SCHEME_ID = P_LOI.GRADING_SCHEME_ID, 
        RESULT_DISTRIBUTION = v_result_distibution_id, 
        START_DATE = P_LOI.START_DATE, 
        END_DATE = P_LOI.END_DATE, 
        PERCENTAGE_LOWER = P_LOI.PERCENTAGE_LOWER, 
        PERCENTAGE_EQUAL = P_LOI.PERCENTAGE_EQUAL, 
        PERCENTAGE_HIGHER = P_LOI.PERCENTAGE_HIGHER, 
        RESULT_LABEL = P_LOI.RESULT_LABEL, 
        STATUS = P_LOI.STATUS, 
        DIPLOMA_ID = v_diploma_id, 
        REPORT_ID = P_REPORT_ID, 
        EXTENSION = P_LOI.EXTENSION
        WHERE ID = v_loi_id;

    SELECT COUNT(1) INTO v_count_los_loi FROM EWP.EWPCV_LOS_LOI WHERE LOI_ID = v_loi_id AND LOS_ID = v_los_id;

    IF v_count_los_loi = 0 THEN
      INSERT INTO EWPCV_LOS_LOI(LOI_ID, LOS_ID)
          VALUES (v_loi_id, v_los_id);
    END IF;

    IF P_LOI.GROUP_ID IS NOT NULL AND P_LOI.GROUP_TYPE_ID IS NOT NULL 
    THEN
        INSERT INTO EWPCV_LOI_GROUPING(LOI_ID, GROUP_ID, GROUP_TYPE_ID)
        VALUES (v_loi_id, P_LOI.GROUP_ID, P_LOI.GROUP_TYPE_ID);
    END IF;

    INSERTA_EDUCATION_LEVEL_LIST(P_LOI.EDUCATION_LEVEL, v_loi_id);

    FOR i IN P_LOI.ATTACHMENTS_REF_LIST.FIRST .. P_LOI.ATTACHMENTS_REF_LIST.LAST 
    LOOP
      FOR j IN P_TOR_ATTACHMENT_LIST.FIRST .. P_TOR_ATTACHMENT_LIST.LAST 
      LOOP
        IF LOWER(P_TOR_ATTACHMENT_LIST(j).ATTACH_REFERENCE) = LOWER(P_LOI.ATTACHMENTS_REF_LIST(i)) THEN
          IF EXISTE_REF_ATTACHMENT(P_LOI.ATTACHMENTS_REF_LIST(i), P_TEMP_REF_LIST) < 0 THEN
            v_attachment_id := INSERTA_ATTACHMENT(P_TOR_ATTACHMENT_LIST(j));
            INSERT INTO EWPCV_LOI_ATT(ATTACHMENT_ID, LOI_ID)
            VALUES (v_attachment_id, v_loi_id);
			v_temp_ref.ATTACH_REFERENCE := P_TOR_ATTACHMENT_LIST(j).ATTACH_REFERENCE;
			v_temp_ref.ATTACHMENT_ID := v_attachment_id;
            P_TEMP_REF_LIST := INSERTA_LISTA_TEMP(v_temp_ref, P_TEMP_REF_LIST);
          ELSE
            v_attachment_id := GET_ATTACHMENT_ID_BY_REF(P_TOR_ATTACHMENT_LIST(j).ATTACH_REFERENCE, P_TEMP_REF_LIST);
            IF v_attachment_id IS NOT NULL THEN
              INSERT INTO EWPCV_LOI_ATT(ATTACHMENT_ID, LOI_ID) VALUES (v_attachment_id, v_loi_id);
            END IF;
          END IF;
        END IF;
      END LOOP;  
    END LOOP;

  END INSERTA_LOI;

  /*
    Inserción datos un objeto result_distribution
  */
  PROCEDURE INSERTA_LOI_LIST(P_LOI_LIST IN PKG_TORS.LOI_LIST, P_TOR_ATTACHMENT_LIST IN PKG_TORS.TOR_ATTACHMENT_LIST, P_REPORT_ID IN VARCHAR2, 
  P_TEMP_REF_LIST IN OUT PKG_TORS.TEMP_ATTACH_REF_LIST) IS
  BEGIN
    IF P_LOI_LIST IS NOT NULL AND P_LOI_LIST.COUNT > 0 THEN 
      FOR i IN P_LOI_LIST.FIRST .. P_LOI_LIST.LAST 
      LOOP
        INSERTA_LOI(P_LOI_LIST(i), P_TOR_ATTACHMENT_LIST, P_REPORT_ID, P_TEMP_REF_LIST);
      END LOOP;
    END IF;
  END INSERTA_LOI_LIST;

    /*
    Inserción datos un objeto tor_report
  */
  FUNCTION INSERTA_TOR_REPORT(P_ISSUE_DATE IN DATE, P_TOR_ID IN VARCHAR2) RETURN VARCHAR2 AS
    v_report_id VARCHAR2(255);
    v_attachment_id VARCHAR2(255);
  BEGIN
    v_report_id := EWP.GENERATE_UUID();

    INSERT INTO EWPCV_REPORT (ID, VERSION, ISSUE_DATE, TOR_ID) 
    VALUES (v_report_id, 0, P_ISSUE_DATE, P_TOR_ID);
	RETURN v_report_id; 
  END INSERTA_TOR_REPORT;

  PROCEDURE INSERTA_TOR_REPORT_LIST(P_TOR_REPORT_LIST IN PKG_TORS.TOR_REPORT_LIST, P_TOR_ID IN VARCHAR2) IS
    v_report_id VARCHAR2(255);
    v_temp_ref_list PKG_TORS.TEMP_ATTACH_REF_LIST;
    v_attachment_id VARCHAR2(255);
  BEGIN
    IF P_TOR_REPORT_LIST IS NOT NULL AND P_TOR_REPORT_LIST.COUNT > 0 THEN 
      FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST
      LOOP
        v_report_id := INSERTA_TOR_REPORT(P_TOR_REPORT_LIST(i).ISSUE_DATE, P_TOR_ID);
		    INSERTA_LOI_LIST(P_TOR_REPORT_LIST(i).LOI_LIST, P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST, v_report_id, v_temp_ref_list);
		    IF v_temp_ref_list IS NOT NULL AND v_temp_ref_list.COUNT > 0 
		    AND P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST IS NOT NULL 
		    AND P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.COUNT > 0 THEN
		      FOR j IN P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.FIRST .. P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.LAST
		      LOOP
            IF EXISTE_REF_ATTACHMENT(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST(j).ATTACH_REFERENCE, v_temp_ref_list) < 0 THEN 
              v_attachment_id := INSERTA_ATTACHMENT(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST(j));
              INSERT INTO EWPCV_REPORT_ATT(ATTACHMENT_ID, REPORT_ID) VALUES (v_attachment_id, v_report_id);
            END IF;
		      END LOOP;
		    END IF;
      END LOOP;
    END IF;
  END INSERTA_TOR_REPORT_LIST;

  PROCEDURE INSERTA_BASIC_TOR_REPORT_LIST(P_TOR_REPORT_LIST IN PKG_TORS.SIMPLE_TOR_REPORT_LIST, P_TOR_ID IN VARCHAR2) IS
    v_report_id VARCHAR2(255);
    v_attachment_id VARCHAR2(255);
  BEGIN
    IF P_TOR_REPORT_LIST IS NOT NULL AND P_TOR_REPORT_LIST.COUNT > 0 THEN 
      FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST
      LOOP
        v_report_id := INSERTA_TOR_REPORT(P_TOR_REPORT_LIST(i).ISSUE_DATE, P_TOR_ID);
        FOR j IN P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.FIRST .. P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST.LAST
        LOOP
            v_attachment_id := INSERTA_ATTACHMENT(P_TOR_REPORT_LIST(i).TOR_ATTACHMENT_LIST(j));
            INSERT INTO EWPCV_REPORT_ATT(ATTACHMENT_ID, REPORT_ID) VALUES (v_attachment_id, v_report_id);
        END LOOP;
      END LOOP;
    END IF;
  END INSERTA_BASIC_TOR_REPORT_LIST;


 /*
  * Inserta un TOR en el sistema 
  */
  FUNCTION INSERTA_TOR(P_TOR IN TOR, P_LA_ID IN VARCHAR2) RETURN VARCHAR2 AS
    v_tor_id VARCHAR2(255);
  BEGIN
    v_tor_id := EWP.GENERATE_UUID();

    INSERT INTO EWPCV_TOR (ID, VERSION, GENERATED_DATE, EXTENSION)
    VALUES (v_tor_id, 0, P_TOR.GENERATED_DATE, P_TOR.EXTENSION);

    UPDATE EWPCV_LEARNING_AGREEMENT SET TOR_ID = v_tor_id WHERE ID = P_LA_ID;

	INSERTA_TOR_REPORT_LIST(P_TOR.TOR_REPORT_LIST, v_tor_id);

    RETURN v_tor_id;
  END INSERTA_TOR;

  /*
   * Inserta un BASIC_TOR en el sistema 
   */
  FUNCTION INSERTA_BASIC_TOR(P_TOR IN TOR_SIMPLE_OBJECT, P_LA_ID IN VARCHAR2) RETURN VARCHAR2 AS
    v_tor_id VARCHAR2(255);
  BEGIN
    v_tor_id := EWP.GENERATE_UUID();

    INSERT INTO EWPCV_TOR (ID, VERSION, GENERATED_DATE)
    VALUES (v_tor_id, 0, P_TOR.GENERATED_DATE);

    UPDATE EWPCV_LEARNING_AGREEMENT SET TOR_ID = v_tor_id WHERE ID = P_LA_ID;

	INSERTA_BASIC_TOR_REPORT_LIST(P_TOR.SIMPLE_TOR_REPORT_LIST, v_tor_id);

    RETURN v_tor_id;
  END INSERTA_BASIC_TOR;


  FUNCTION VALIDA_NUM_LOI(P_TOR_REPORT_LIST IN PKG_TORS.TOR_REPORT_LIST, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, 
  P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno       NUMBER := 0;
    v_count             VARCHAR2(255);
    v_loi_message_error VARCHAR2(2000);
    v_loi_identifier    VARCHAR2(1000);
    v_report_loi_count  NUMBER;
    v_la_loi_count      NUMBER;
  BEGIN
    IF P_TOR_REPORT_LIST IS NOT NULL AND P_TOR_REPORT_LIST.COUNT > 0 THEN
      FOR i IN P_TOR_REPORT_LIST.FIRST .. P_TOR_REPORT_LIST.LAST LOOP
        v_report_loi_count := 0;
        v_loi_message_error := NULL; 

        -- Obtenemos el nÃºmero de lois que estÃ¡n asociados a componentes estudiados del LA
        WITH COMPONENTS AS (
          SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID AS LA_COMPONENT_ID 
            FROM EWPCV_LEARNING_AGREEMENT LA 
            LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
        )
        SELECT COUNT(1) INTO v_la_loi_count FROM COMPONENTS C
        INNER JOIN EWP.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.LA_COMPONENT_ID
        WHERE C.ID = P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

        -- Se recorre la lista de LOI del report para comprobar que todos los loi informados estÃ¡n en el LA asociado
        IF P_TOR_REPORT_LIST(i).LOI_LIST IS NOT NULL AND P_TOR_REPORT_LIST(i).LOI_LIST.COUNT > 0 THEN
          FOR j IN P_TOR_REPORT_LIST(i).LOI_LIST.FIRST .. P_TOR_REPORT_LIST(i).LOI_LIST.LAST LOOP
            WITH COMPONENTS AS (
              SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID AS LA_COMPONENT_ID 
                FROM EWPCV_LEARNING_AGREEMENT LA 
                LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
            )
            SELECT COUNT(1) INTO v_count FROM COMPONENTS C
            INNER JOIN EWP.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.LA_COMPONENT_ID
            WHERE C.ID = P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = P_LA_REVISION
            AND (LAC.LOI_ID = P_TOR_REPORT_LIST(i).LOI_LIST(j).loi_id OR 
                LAC.LOS_CODE =  P_TOR_REPORT_LIST(i).LOI_LIST(j).los_code OR 
                LAC.LOS_ID =  P_TOR_REPORT_LIST(i).LOI_LIST(j).los_id);

            IF v_count = 0 THEN
              IF P_TOR_REPORT_LIST(i).LOI_LIST(j).loi_id IS NOT NULL THEN
                v_loi_identifier := 'LOI_ID: ' || P_TOR_REPORT_LIST(i).LOI_LIST(j).loi_id;
              ELSE 
                IF P_TOR_REPORT_LIST(i).LOI_LIST(j).los_id IS NOT NULL THEN
                  v_loi_identifier := 'LOS_ID:' || P_TOR_REPORT_LIST(i).LOI_LIST(j).los_id;
                ELSE
                  v_loi_identifier := 'LOI_CODE: ' || P_TOR_REPORT_LIST(i).LOI_LIST(j).los_code;
                END IF;
              END IF;
              v_loi_message_error := v_loi_message_error || '
                - LOI_LIST('|| j ||') : El LOI con ' || v_loi_identifier || ' no existe en el acuerdo de aprendizaje.';
            ELSE
              v_report_loi_count := v_report_loi_count + 1;
            END IF;
          END LOOP;

          IF v_loi_message_error IS NOT NULL THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                TOR_REPORT_LIST('|| i ||'): 
                  ' || v_loi_message_error);
            v_cod_retorno := -1;
          END IF;

          -- Si no coincide el nÃºmero de LOI del LA y los informados en el report
          IF v_report_loi_count <> v_la_loi_count THEN
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                TOR_REPORT_LIST('|| i ||'): 
                  No coincide el nÃºmero de LOI de componentes estudiados del acuerdo de aprendizaje y los LOI informados en el ToR');
            v_cod_retorno := -1;
          END IF;
        ELSE
            v_cod_retorno := -1;
            EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
                - TOR_REPORT_LIST(' || i || '):' ||'
                    - LOI_LIST: No puede ir vacÃ­a.');
        END IF;

      END LOOP;
    ELSE
      v_cod_retorno := -1;
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
          - TOR_REPORT_LIST:  La lista de informes no puede ir vacÃ­a.');
    END IF;
    RETURN v_cod_retorno;
  END VALIDA_NUM_LOI;

  FUNCTION VALIDA_NUM_STUDIED_LOI(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, 
    P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno       NUMBER := 0;
    v_studied_la_loi_count  NUMBER;
    v_studied_la_count      NUMBER;
  BEGIN

    -- Obtenemos el nÃºmero de componentes estudiados del LA
    SELECT COUNT(1) INTO v_studied_la_count FROM EWPCV_STUDIED_LA_COMPONENT C
    INNER JOIN EWP.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.STUDIED_LA_COMPONENT_ID
    WHERE C.LEARNING_AGREEMENT_ID = P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = P_LA_REVISION;
    
     -- Obtenemos el nÃºmero de componentes estudiados del LA
    SELECT COUNT(1) INTO v_studied_la_loi_count FROM EWPCV_STUDIED_LA_COMPONENT C
    INNER JOIN EWP.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.STUDIED_LA_COMPONENT_ID
    WHERE C.LEARNING_AGREEMENT_ID = P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = P_LA_REVISION
    AND LAC.LOI_ID IS NOT NULL;
    
    -- Si no coincide el nÃºmero de LOI del LA y los informados en el report
    IF v_studied_la_loi_count <> v_studied_la_count THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, '
             Todos los componentes estudiados del LA deben tener un LOI');
      v_cod_retorno := -1;
    END IF;
       
    RETURN v_cod_retorno;
  END VALIDA_NUM_STUDIED_LOI;

  /* 
    Inserta un TOR
  */
  FUNCTION INSERT_TOR(P_TOR IN PKG_TORS.TOR, P_LA_ID IN VARCHAR2, P_TOR_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno       NUMBER := 0;
    v_total             NUMBER := 0;
    v_tor_id            VARCHAR2(255);
    v_la_id             VARCHAR2(255);
    v_la_revision       VARCHAR2(255);
    v_notifier_hei      VARCHAR2(255);
    v_existe_tor_id     VARCHAR2(255);

    CURSOR c_la(pc_la_id VARCHAR2) IS
    SELECT ID, TOR_ID, LEARNING_AGREEMENT_REVISION FROM EWP.EWPCV_LEARNING_AGREEMENT 
    WHERE ID = pc_la_id AND LEARNING_AGREEMENT_REVISION = (
      SELECT MAX(LEARNING_AGREEMENT_REVISION)
      FROM EWP.EWPCV_LEARNING_AGREEMENT 
      WHERE LOWER(ID) = LOWER(pc_la_id)
    );
  
  BEGIN
    
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;
    
    OPEN c_la(P_LA_ID);
    FETCH c_la INTO v_la_id, v_existe_tor_id, v_la_revision;
    CLOSE c_la;
 
    IF v_la_id IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement "' || P_LA_ID || '" no existe.');
      RETURN -1;
    ELSE
      IF v_existe_tor_id IS NOT NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement "' || P_LA_ID || '" ya tiene asociado el ToR "'|| v_existe_tor_id ||'", usa el método UPDATE_TOR para actualizarlo.');
        RETURN -1;
      END IF;
    
      v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR_INSERT(v_la_id, P_HEI_TO_NOTIFY);

      IF v_notifier_hei IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
        RETURN -1;
      END IF;
    
      IF VALIDA_NUM_STUDIED_LOI(v_la_id, v_la_revision, P_ERROR_MESSAGE) < 0 THEN
        RETURN -1;
      END IF;
    
      IF VALIDA_NUM_LOI(P_TOR.TOR_REPORT_LIST, v_la_id, v_la_revision, P_ERROR_MESSAGE) < 0 THEN
        RETURN -1;
      END IF;
    END IF;
  
    -- Validamos que vengan informados los campos obligatorios
    v_cod_retorno := VALIDA_TOR(P_TOR, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
      --Validamos la calidad del dato de los campos informados
      v_cod_retorno := VALIDA_DATOS_TOR(P_TOR, v_la_id, P_ERROR_MESSAGE);
      IF v_cod_retorno = 0 THEN
        --Insertamos el TOR
        v_tor_id := INSERTA_TOR(P_TOR, P_LA_ID);
        -- Insertamos la lista de attachment propios de la movilidad
        INSERTA_MOBILITY_ATT_LIST(P_TOR.MOBILITY_ATTACHMENT_LIST, v_tor_id);
        -- Insertamos la lista de isced_tables
        INSERTA_ISCED_TABLE_LIST(P_TOR.ISCED_TABLE_LIST, v_tor_id);
        v_cod_retorno := 0;
		P_TOR_ID := v_tor_id;    
		PKG_COMMON.INSERTA_NOTIFICATION(OBTEN_MOBILITY_ID(P_TOR_ID), 3, P_HEI_TO_NOTIFY, v_notifier_hei);
      END IF;
    END IF;

    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END INSERT_TOR;

    /* 
    Inserta un TOR
  */
  FUNCTION INSERT_BASIC_TOR(P_TOR_SIMPLE_OBJECT IN PKG_TORS.TOR_SIMPLE_OBJECT, P_LA_ID IN VARCHAR2, P_TOR_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_total       NUMBER := 0;
    v_tor_id      VARCHAR2(255);
    v_la_id             VARCHAR2(255);
    v_la_revision       VARCHAR2(255);
    v_notifier_hei      VARCHAR2(255);
    v_existe_tor_id     VARCHAR2(255);
  
    CURSOR c_la(pc_la_id VARCHAR2) IS
    SELECT ID, TOR_ID, LEARNING_AGREEMENT_REVISION FROM EWP.EWPCV_LEARNING_AGREEMENT 
    WHERE ID = pc_la_id AND LEARNING_AGREEMENT_REVISION = (
      SELECT MAX(LEARNING_AGREEMENT_REVISION)
      FROM EWP.EWPCV_LEARNING_AGREEMENT 
      WHERE ID = pc_la_id
    );
  BEGIN
    
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;
    
    OPEN c_la(P_LA_ID);
    FETCH c_la INTO v_la_id, v_existe_tor_id, v_la_revision;
    CLOSE c_la;

    IF v_la_id IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement "' || P_LA_ID || '" no existe.');
      RETURN -1;
    ELSE
      IF v_existe_tor_id IS NOT NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El Learning Agreement "' || P_LA_ID || '" ya tiene asociado el ToR "'|| v_existe_tor_id ||'", usa el método UPDATE_TOR para actualizarlo.');
        RETURN -1;
      END IF;
    
      v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR_INSERT(v_la_id, P_HEI_TO_NOTIFY);

      IF v_notifier_hei IS NULL THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
        RETURN -1;
      END IF;

    END IF;

    -- Validamos que vengan informados los campos obligatorios
    v_cod_retorno := VALIDA_BASIC_TOR(P_TOR_SIMPLE_OBJECT, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
        v_tor_id := INSERTA_BASIC_TOR(P_TOR_SIMPLE_OBJECT, P_LA_ID);
    END IF;

    P_TOR_ID := v_tor_id;
    
    PKG_COMMON.INSERTA_NOTIFICATION(OBTEN_MOBILITY_ID(P_TOR_ID), 3, P_HEI_TO_NOTIFY, v_notifier_hei);
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END INSERT_BASIC_TOR;


  /* 
    Inserta lista group type
  */
  FUNCTION INSERT_GROUP_TYPE_LIST(P_GROUP_TYPE_LIST IN PKG_TORS.GROUP_TYPE_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno   NUMBER := 0;
    v_group_id      VARCHAR2(255);
    v_group_type_id VARCHAR2(255);
    v_lang_item     VARCHAR2(255);
  BEGIN
    v_cod_retorno := VALIDA_GROUP_TYPE_LIST(P_GROUP_TYPE_LIST, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
      --Insertamos la lista de tipos de grupos
      FOR i IN P_GROUP_TYPE_LIST.FIRST .. P_GROUP_TYPE_LIST.LAST 
      LOOP
        v_group_type_id := EWP.GENERATE_UUID();
         v_lang_item := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_GROUP_TYPE_LIST(i).TITLE);
        INSERT INTO EWPCV_GROUP_TYPE(ID, TITLE) VALUES (v_group_type_id, v_lang_item);
        -- Insertamos la lista de grupos contenida dentro del tipo
        IF P_GROUP_TYPE_LIST(i).GROUP_LIST IS NOT NULL AND P_GROUP_TYPE_LIST(i).GROUP_LIST.COUNT > 0 THEN
          FOR j IN P_GROUP_TYPE_LIST(i).GROUP_LIST.FIRST .. P_GROUP_TYPE_LIST(i).GROUP_LIST.LAST 
          LOOP
            v_lang_item := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_GROUP_TYPE_LIST(i).GROUP_LIST(j).TITLE);
            v_group_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_GROUP
            (ID, TITLE, SORTING_KEY, GROUP_TYPE_ID) 
            VALUES 
            (v_group_id, v_lang_item, P_GROUP_TYPE_LIST(i).GROUP_LIST(j).SORTING_KEY, v_group_type_id);
          END LOOP;
        END IF;
      END LOOP;
    END IF;
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END INSERT_GROUP_TYPE_LIST; 

/* 
    Inserta lista group
  */
  FUNCTION INSERT_GROUP_LIST(P_GROUP_LIST IN PKG_TORS.GROUP_LIST, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_group_id    VARCHAR2(255);
    v_lang_item   VARCHAR2(255);
  BEGIN

    v_cod_retorno := VALIDA_GROUP_LIST(P_GROUP_LIST, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
      --Insertamos la lista de grupos
      FOR i IN P_GROUP_LIST.FIRST .. P_GROUP_LIST.LAST 
      LOOP
        v_group_id := EWP.GENERATE_UUID();
        v_lang_item := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_GROUP_LIST(i).TITLE);
        INSERT INTO EWPCV_GROUP(ID, TITLE, SORTING_KEY) VALUES (v_group_id, v_lang_item, P_GROUP_LIST(i).SORTING_KEY);
      END LOOP;
    END IF;
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END INSERT_GROUP_LIST;

  /* 
    Inserta grading scheme
  */
  FUNCTION INSERT_GRADING_SCHEME(P_GRADING_SCHEME IN PKG_TORS.GRADING_SCHEME, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_id          VARCHAR2(255);
    v_lang_item   VARCHAR2(255);
  BEGIN

    v_cod_retorno := VALIDA_GRADING_SCHEME(P_GRADING_SCHEME, P_ERROR_MESSAGE);

    IF v_cod_retorno = 0 THEN
      v_id := EWP.GENERATE_UUID();
      INSERT INTO EWPCV_GRADING_SCHEME(ID, VERSION) VALUES (v_id, 0);
      --Insertamos la lista de label
      FOR i IN P_GRADING_SCHEME.LABEL.FIRST .. P_GRADING_SCHEME.LABEL.LAST 
      LOOP
        v_lang_item := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_GRADING_SCHEME.LABEL(i));
        INSERT INTO EWPCV_GRADING_SCHEME_LABEL(LABEL_ID, GRADING_SCHEME_ID) VALUES (v_lang_item, v_id);
      END LOOP;
      --Insertamos la lista de description
      FOR i IN P_GRADING_SCHEME.GRAD_DESCRIPTION.FIRST .. P_GRADING_SCHEME.GRAD_DESCRIPTION.LAST 
      LOOP
        v_lang_item := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_GRADING_SCHEME.GRAD_DESCRIPTION(i));
        INSERT INTO EWPCV_GRADING_SCHEME_DESC(DESCRIPTION_ID, GRADING_SCHEME_ID) VALUES (v_lang_item, v_id);
      END LOOP;
    END IF;
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END INSERT_GRADING_SCHEME;


/*********************************************************************************************
**************************** BORRADOS
**********************************************************************************************
**/


  /*
    Borra los datos un objeto grading scheme
  */
  PROCEDURE BORRA_GRADING_SCHEME(P_ID IN VARCHAR2) IS
    CURSOR c_grading_description(p_c_id IN VARCHAR2) IS 
      SELECT DESCRIPTION_ID 
      FROM EWPCV_GRADING_SCHEME_DESC 
      WHERE GRADING_SCHEME_ID = p_c_id;
    CURSOR c_grading_label(p_c_id IN VARCHAR2) IS 
      SELECT LABEL_ID 
      FROM EWPCV_GRADING_SCHEME_LABEL 
      WHERE GRADING_SCHEME_ID = p_c_id;
  BEGIN
    FOR r_grading_description IN c_grading_description(P_ID) 
    LOOP
      DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = r_grading_description.description_id;
    END LOOP;

    FOR r_grading_label IN c_grading_label(P_ID) 
    LOOP
      DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = r_grading_label.label_id;
    END LOOP;

    DELETE FROM EWPCV_GRADING_SCHEME WHERE ID = P_ID;
  END BORRA_GRADING_SCHEME;

  /*
    Borra los datos un objeto result distribution
  */
  PROCEDURE BORRA_RESULT_DISTRIBUTION(P_ID IN VARCHAR2) IS
    CURSOR c_distr_description(p_c_id IN VARCHAR2) IS 
      SELECT DESCRIPTION_ID 
      FROM EWP.EWPCV_DISTR_DESCRIPTION 
      WHERE RESULT_DISTRIBUTION_ID = p_c_id;

  BEGIN
    FOR r_distr_description IN c_distr_description(P_ID) 
    LOOP
      DELETE FROM EWPCV_DISTR_DESCRIPTION WHERE DESCRIPTION_ID = r_distr_description.description_id;
      DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = r_distr_description.description_id;
    END LOOP;

    DELETE FROM EWPCV_RESULT_DIST_CATEGORY WHERE RESULT_DIST_ID = P_ID;  
    
    DELETE FROM EWPCV_RESULT_DISTRIBUTION WHERE ID = P_ID;
  END BORRA_RESULT_DISTRIBUTION;

  /*
    Borra los datos de un attachment
  */
  PROCEDURE BORRA_ATTACHMENT (P_ATTACHMENT_ID  IN VARCHAR2) IS
    v_attach_desc_id VARCHAR2(255);
    v_attach_tit_id  VARCHAR2(255);
    CURSOR c_att_desc(p_att_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_ATTACHMENT_DESCRIPTION
			WHERE ATTACHMENT_ID = p_att_id;
    CURSOR c_att_title(p_att_id IN VARCHAR2) IS 
			SELECT ID
			FROM EWPCV_ATTACHMENT_TITLE
			WHERE ATTACHMENT_ID = p_att_id;
  BEGIN
    OPEN c_att_desc(P_ATTACHMENT_ID);
    FETCH c_att_desc INTO v_attach_desc_id;
    CLOSE c_att_desc;
    IF v_attach_desc_id IS NOT NULL THEN
        DELETE FROM EWPCV_ATTACHMENT_DESCRIPTION WHERE ID = v_attach_desc_id;
        DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = v_attach_desc_id;
    END IF;
    OPEN c_att_title(P_ATTACHMENT_ID);
    FETCH c_att_title INTO v_attach_tit_id;
    CLOSE c_att_title;
    IF v_attach_tit_id IS NOT NULL THEN
        DELETE FROM EWPCV_ATTACHMENT_TITLE WHERE ID = v_attach_tit_id;
        DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = v_attach_tit_id;
    END IF;
    DELETE FROM EWPCV_TOR_ATTACHMENT WHERE ATTACHMENT_ID = P_ATTACHMENT_ID;
    DELETE FROM EWPCV_ATTACHMENT_CONTENT WHERE ATTACHMENT_ID = P_ATTACHMENT_ID;
    DELETE FROM EWPCV_ATTACHMENT WHERE ID = P_ATTACHMENT_ID;
  END;


  /*
    Borra los datos un objeto diploma
  */
  PROCEDURE BORRA_DIPLOMA(P_ID IN VARCHAR2) IS
    v_attach_id VARCHAR2(255);
    CURSOR c_diploma_section(p_diploma_id IN VARCHAR2) IS 
			SELECT ID FROM EWPCV_DIPLOMA_SECTION
			WHERE DIPLOMA_ID = p_diploma_id;
			
	CURSOR c_sect_att(p_diploma_section_id VARCHAR2) IS
		SELECT ATTACHMENT_ID FROM EWP.EWPCV_DIPLOMA_SEC_ATTACH 
		WHERE SECTION_ID = p_diploma_section_id;
  BEGIN
    FOR r_diploma_section IN c_diploma_section (P_ID)
		LOOP
		--attachments de la seccion
		FOR r_sect_att IN c_sect_att(r_diploma_section.id) LOOP
			DELETE FROM EWP.EWPCV_DIPLOMA_SEC_ATTACH WHERE ATTACHMENT_ID = r_sect_att.ATTACHMENT_ID;
			PKG_TORS.BORRA_ATTACHMENT(r_sect_att.ATTACHMENT_ID);
		END LOOP;
    END LOOP;
	
	--Borramos las relaciones de todas las secciones del diploma 
	DELETE FROM EWPCV_DIPLOMA_SECTION_SECTION WHERE ID IN (SELECT ID FROM EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = P_ID);
	--Borramos la informacion adicional de todas las secciones del diploma 
	DELETE FROM EWPCV_ADDITIONAL_INFO WHERE SECTION_ID IN (SELECT ID FROM EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = P_ID);
	--Borramos todas las secciones del diploma 
	DELETE FROM EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = P_ID;
	--Borramos el diploma
    DELETE FROM EWPCV_DIPLOMA WHERE ID = P_ID;
  END;

  PROCEDURE BORRA_EDUCATION_LEVEL(P_LOI_ID IN VARCHAR2 ) IS
    v_level_desc_id VARCHAR2(255);
    CURSOR c_levels(pc_loi_id IN VARCHAR2) IS
    SELECT LEVEL_ID FROM EWP.EWPCV_LEVELS WHERE LOI_ID = pc_loi_id;

    CURSOR c_level_desc(p_level_id IN VARCHAR2) IS 
    SELECT LEVEL_DESCRIPTION_ID FROM EWPCV_LEVEL_DESCRIPTION
    WHERE LEVEL_ID = p_level_id;
  BEGIN
  	FOR r_levels IN c_levels(P_LOI_ID)
  	LOOP
      DELETE FROM EWP.EWPCV_LEVELS WHERE LEVEL_ID = r_levels.LEVEL_ID;

      OPEN c_level_desc(r_levels.LEVEL_ID);
      FETCH c_level_desc INTO v_level_desc_id;
      CLOSE c_level_desc;
      IF v_level_desc_id IS NOT NULL THEN
        DELETE FROM EWPCV_LEVEL_DESCRIPTION WHERE LEVEL_DESCRIPTION_ID = v_level_desc_id;
        DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = v_level_desc_id;
      END IF;
      DELETE FROM EWP.EWPCV_LEVEL WHERE ID = r_levels.LEVEL_ID;
  	END LOOP;

  END BORRA_EDUCATION_LEVEL;

  /*
    Borra los datos un objeto loi
  */
  PROCEDURE REINICIA_LOI(P_LOI_ID IN VARCHAR2) IS
    CURSOR c_lois(p_loi_id IN VARCHAR2) IS 
      SELECT GRADING_SCHEME_ID, RESULT_DISTRIBUTION, DIPLOMA_ID
      FROM EWPCV_LOI
      WHERE ID = p_loi_id; 
    CURSOR c_loi_att(p_loi_id IN VARCHAR2) IS
      SELECT ATTACHMENT_ID FROM EWP.EWPCV_LOI_ATT WHERE LOI_ID = p_loi_id;
  BEGIN
    FOR r_loi IN c_lois(P_LOI_ID) 
    LOOP
      UPDATE EWPCV_LOI SET ENGAGEMENT_HOURS = NULL, LANGUAGE_OF_INSTRUCTION = NULL, 
      REPORT_ID = NULL,
      GRADING_SCHEME_ID = NULL, RESULT_DISTRIBUTION = NULL, DIPLOMA_ID = NULL , PERCENTAGE_LOWER = NULL, PERCENTAGE_EQUAL = NULL, 
      PERCENTAGE_HIGHER = NULL, RESULT_LABEL = NULL, STATUS = NULL WHERE ID = P_LOI_ID;

      BORRA_RESULT_DISTRIBUTION(r_loi.result_distribution);
      BORRA_DIPLOMA(r_loi.diploma_id);

      BORRA_EDUCATION_LEVEL(P_LOI_ID);

      DELETE FROM EWPCV_LOS_LOI WHERE LOI_ID = P_LOI_ID;  
	  DELETE FROM EWPCV_LOI_GROUPING WHERE LOI_ID = P_LOI_ID;  

      FOR r_loi_att IN c_loi_att(P_LOI_ID) LOOP
        DELETE FROM EWP.EWPCV_LOI_ATT WHERE ATTACHMENT_ID = r_loi_att.ATTACHMENT_ID;
      	BORRA_ATTACHMENT(r_loi_att.ATTACHMENT_ID);
      END LOOP;
    END LOOP;
  END REINICIA_LOI;


  /*
    Borra los datos un objeto report
  */
  PROCEDURE BORRA_REPORTS(P_TOR_ID IN VARCHAR2) IS
    CURSOR c_report(p_c_tor_id IN VARCHAR2) IS 
      SELECT ID
      FROM EWPCV_REPORT
      WHERE TOR_ID = p_c_tor_id;

    CURSOR c_lois(p_c_report_id IN VARCHAR2) IS 
      SELECT ID
      FROM EWPCV_LOI
      WHERE REPORT_ID = p_c_report_id;

    CURSOR c_rep_att(p_report_id VARCHAR2) IS
      SELECT ATTACHMENT_ID FROM EWP.EWPCV_REPORT_ATT WHERE REPORT_ID = p_report_id;
  BEGIN
    FOR r_report IN c_report(P_TOR_ID)       
    LOOP
      FOR r_loi IN c_lois(r_report.id) 
      LOOP
        REINICIA_LOI(r_loi.id);
      END LOOP;

      -- Borra attachments del report
      FOR r_rep_att IN c_rep_att(r_report.id) LOOP
        DELETE FROM EWP.EWPCV_REPORT_ATT WHERE ATTACHMENT_ID = r_rep_att.ATTACHMENT_ID;
        BORRA_ATTACHMENT(r_rep_att.ATTACHMENT_ID);      
      END LOOP;

      DELETE FROM EWPCV_REPORT WHERE TOR_ID = P_TOR_ID;
    END LOOP;
  END BORRA_REPORTS;


  /*
    Borra los datos un objeto ISCED_TABLE
  */
  PROCEDURE BORRA_ISCED_TABLE(P_ISCED_TABLE_ID IN VARCHAR2) IS
    CURSOR c_grade_FREQUENCY(p_isced_table_id IN VARCHAR2) IS 
      SELECT ID
      FROM EWPCV_GRADE_FREQUENCY
      WHERE ISCED_TABLE_ID = p_isced_table_id;
  BEGIN
    FOR r_grade_FREQUENCY IN c_grade_FREQUENCY(P_ISCED_TABLE_ID)       
    LOOP
      DELETE FROM EWPCV_GRADE_FREQUENCY WHERE ID = r_grade_FREQUENCY.id;
    END LOOP;
  END BORRA_ISCED_TABLE;


  /*
    Borra los datos una lista de objetos ISCED_TABLE
  */
  PROCEDURE BORRA_ISCED_TABLE_LIST(P_TOR_ID IN VARCHAR2) IS
    CURSOR c_isced_table(p_c_tor_id IN VARCHAR2) IS 
      SELECT ID
      FROM EWPCV_ISCED_TABLE
      WHERE TOR_ID = p_c_tor_id;
  BEGIN
    FOR isced_table IN c_isced_table(P_TOR_ID)       
    LOOP
        BORRA_ISCED_TABLE(isced_table.id);
    END LOOP;
    DELETE FROM EWPCV_ISCED_TABLE WHERE TOR_ID = P_TOR_ID;
  END BORRA_ISCED_TABLE_LIST;


  /* 
    Función para eliminar los mobilitity_attachment asociados a un TOR
  */
  PROCEDURE BORRA_MOBILITY_ATTACHMENT_LIST(P_TOR_ID IN VARCHAR2) IS
    CURSOR c_tor_att(p_c_tor_id IN VARCHAR2) IS 
      SELECT ATTACHMENT_ID
      FROM EWPCV_TOR_ATTACHMENT
      WHERE TOR_ID = p_c_tor_id;
  BEGIN
    FOR rec IN c_tor_att(P_TOR_ID)       
    LOOP
        BORRA_ATTACHMENT(rec.ATTACHMENT_ID);
    END LOOP;

  END BORRA_MOBILITY_ATTACHMENT_LIST;

  /* 
    Función para eliminar un TOR
  */
  PROCEDURE BORRA_TOR(P_TOR_ID IN VARCHAR2) IS
  BEGIN

    BORRA_ISCED_TABLE_LIST(P_TOR_ID);
    BORRA_REPORTS(P_TOR_ID); 
    BORRA_MOBILITY_ATTACHMENT_LIST(P_TOR_ID);

    UPDATE EWPCV_LEARNING_AGREEMENT SET TOR_ID = NULL WHERE LOWER(TOR_ID) = LOWER(P_TOR_ID);

  	DELETE FROM EWPCV_TOR WHERE LOWER(ID) = LOWER(P_TOR_ID);
  END BORRA_TOR;

   /* 
    Función para eliminar BASIC_TOR_ATTACHMENT
  */
  PROCEDURE BORRA_BASIC_TOR_ATTACH(P_TOR_ID IN VARCHAR2) IS
  CURSOR c_rep_att(p_tor_id VARCHAR2) IS
      SELECT rep_att.ATTACHMENT_ID 
        FROM EWP.EWPCV_REPORT repo 
        INNER JOIN EWP.EWPCV_REPORT_ATT rep_att ON repo.ID = rep_att.REPORT_ID 
        WHERE repo.TOR_ID = p_tor_id;
  BEGIN
    FOR r_rep_att IN c_rep_att(P_TOR_ID) LOOP
        DELETE FROM EWP.EWPCV_REPORT_ATT WHERE ATTACHMENT_ID = r_rep_att.ATTACHMENT_ID;
        BORRA_ATTACHMENT(r_rep_att.ATTACHMENT_ID);
    END LOOP;
    DELETE FROM EWPCV_REPORT WHERE TOR_ID = P_TOR_ID;
  END BORRA_BASIC_TOR_ATTACH;

  /* 
    Función para eliminar un TOR
  */
  PROCEDURE BORRA_BASIC_TOR(P_TOR_ID IN VARCHAR2) IS
  BEGIN
	BORRA_BASIC_TOR_ATTACH(P_TOR_ID);
    UPDATE EWPCV_LEARNING_AGREEMENT SET TOR_ID = NULL WHERE LOWER(TOR_ID) = LOWER(P_TOR_ID);
  	DELETE FROM EWPCV_TOR WHERE LOWER(ID) = LOWER(P_TOR_ID);
  END BORRA_BASIC_TOR;



  /* 
    Elimina un TOR
  */
  FUNCTION DELETE_TOR(P_TOR_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER(1) := 0;
    v_count NUMBER;
    v_notifier_hei VARCHAR2(255);
	v_mob_id VARCHAR2(255);
  BEGIN
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;

    SELECT COUNT(1) INTO v_count FROM EWPCV_TOR WHERE ID = P_TOR_ID;
    IF v_count = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El TOR ' || P_TOR_ID || ' no existe');    
      RETURN -1;
    END IF;
  
    v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR(P_TOR_ID, P_HEI_TO_NOTIFY);
    IF v_notifier_hei IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
      RETURN -1;
    END IF;
	v_mob_id := OBTEN_MOBILITY_ID(P_TOR_ID);
    BORRA_TOR(P_TOR_ID);
    PKG_COMMON.INSERTA_NOTIFICATION(v_mob_id, 3, P_HEI_TO_NOTIFY, v_notifier_hei);
      
    COMMIT;
    return v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
      ROLLBACK;
      RETURN -1;
  END DELETE_TOR;  

   /* 
    Elimina un BASIC_TOR
  */
  FUNCTION DELETE_BASIC_TOR(P_TOR_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER(1) := 0;
    v_count NUMBER;
    v_notifier_hei VARCHAR2(255);
	v_mob_id VARCHAR2(255);
  BEGIN
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;

    SELECT COUNT(1) INTO v_count FROM EWPCV_TOR WHERE ID = P_TOR_ID;
    IF v_count = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El TOR ' || P_TOR_ID || ' no existe');    
      RETURN -1;
    END IF;
  
    v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR(P_TOR_ID, P_HEI_TO_NOTIFY);
    IF v_notifier_hei IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
      RETURN -1;
    END IF;
  v_mob_id := OBTEN_MOBILITY_ID(P_TOR_ID);
    BORRA_BASIC_TOR(P_TOR_ID);
    PKG_COMMON.INSERTA_NOTIFICATION(v_mob_id, 3, P_HEI_TO_NOTIFY, v_notifier_hei);
      
    COMMIT;
    return v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
      ROLLBACK;
      RETURN -1;
  END DELETE_BASIC_TOR;


/*********************************************************************************************
**************************** ACTUALIZACIONES
**********************************************************************************************
**/

  PROCEDURE ACTUALIZA_TOR(P_TOR_ID IN VARCHAR2, P_TOR IN TOR) IS
  BEGIN  
    UPDATE EWP.EWPCV_TOR SET VERSION = VERSION + 1, GENERATED_DATE = P_TOR.GENERATED_DATE, EXTENSION = P_TOR.EXTENSION
    WHERE ID = P_TOR_ID;

    BORRA_ISCED_TABLE_LIST(P_TOR_ID);
    BORRA_REPORTS(P_TOR_ID); 
    BORRA_MOBILITY_ATTACHMENT_LIST(P_TOR_ID);

    INSERTA_TOR_REPORT_LIST(P_TOR.TOR_REPORT_LIST, P_TOR_ID);
    INSERTA_MOBILITY_ATT_LIST(P_TOR.MOBILITY_ATTACHMENT_LIST, P_TOR_ID);
    INSERTA_ISCED_TABLE_LIST(P_TOR.ISCED_TABLE_LIST, P_TOR_ID);

  END ACTUALIZA_TOR;

  PROCEDURE ACTUALIZA_BASIC_TOR(P_TOR_ID IN VARCHAR2, P_TOR_SIMPLE_OBJECT IN TOR_SIMPLE_OBJECT) IS
  BEGIN  
    UPDATE EWP.EWPCV_TOR SET VERSION = VERSION + 1, GENERATED_DATE = P_TOR_SIMPLE_OBJECT.GENERATED_DATE
    WHERE ID = P_TOR_ID;

    BORRA_BASIC_TOR_ATTACH(P_TOR_ID); 

    INSERTA_BASIC_TOR_REPORT_LIST(P_TOR_SIMPLE_OBJECT.SIMPLE_TOR_REPORT_LIST, P_TOR_ID);
  END ACTUALIZA_BASIC_TOR;

  /* 
    Actualiza un TOR
  */
  FUNCTION UPDATE_TOR(P_TOR_ID IN VARCHAR2, P_TOR IN PKG_TORS.TOR, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno     NUMBER := 0;
    v_total           NUMBER := 0;
    v_tor_id          VARCHAR2(255);
    v_notifier_hei    VARCHAR2(255);
    v_la_id           VARCHAR2(255);
    v_la_revision     NUMBER;
    
    CURSOR c_la(pc_tor_id VARCHAR2) IS
    SELECT ID, LEARNING_AGREEMENT_REVISION FROM EWP.EWPCV_LEARNING_AGREEMENT 
    WHERE TOR_ID = pc_tor_id AND LEARNING_AGREEMENT_REVISION = (
      SELECT MAX(LEARNING_AGREEMENT_REVISION)
      FROM EWP.EWPCV_LEARNING_AGREEMENT 
      WHERE TOR_ID = pc_tor_id
    );
  BEGIN
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;
    
    
    SELECT COUNT(1) INTO v_total FROM EWP.EWPCV_TOR WHERE ID = P_TOR_ID;  
    IF v_total = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El ToR "' || P_TOR_ID || '" no existe.');
      RETURN -1;
    END IF;
  
    v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR(P_TOR_ID, P_HEI_TO_NOTIFY);

    IF v_notifier_hei IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
      RETURN -1;
    END IF;
  
    OPEN c_la(P_TOR_ID);
    FETCH c_la INTO v_la_id, v_la_revision;
    CLOSE c_la;
  
    IF VALIDA_NUM_STUDIED_LOI( v_la_id, v_la_revision, P_ERROR_MESSAGE) < 0 THEN
      RETURN -1;
    END IF;
  
    IF VALIDA_NUM_LOI(P_TOR.TOR_REPORT_LIST, v_la_id, v_la_revision, P_ERROR_MESSAGE) < 0 THEN
      RETURN -1;
    END IF;

    -- Validamos que vengan informados los campos obligatorios
    v_cod_retorno := VALIDA_TOR(P_TOR, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
      --Validamos la calidad del dato de los campos informados
      v_cod_retorno := VALIDA_DATOS_TOR(P_TOR, v_la_id, P_ERROR_MESSAGE);
      IF v_cod_retorno = 0 THEN
        --Actualizamos el TOR
        ACTUALIZA_TOR(P_TOR_ID, P_TOR);
      END IF;
    END IF;
  
    PKG_COMMON.INSERTA_NOTIFICATION(OBTEN_MOBILITY_ID(P_TOR_ID), 3, P_HEI_TO_NOTIFY, v_notifier_hei);
      
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END UPDATE_TOR;


   /* 
    Actualiza un TOR
  */
  FUNCTION UPDATE_BASIC_TOR(P_TOR_ID IN VARCHAR2, P_TOR_SIMPLE_OBJECT IN PKG_TORS.TOR_SIMPLE_OBJECT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
    v_cod_retorno NUMBER := 0;
    v_total       NUMBER := 0;
    v_tor_id      VARCHAR2(255);
    v_notifier_hei VARCHAR2(255);
  BEGIN
    IF P_HEI_TO_NOTIFY IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios');
      RETURN -1;
    END IF;
    
    SELECT COUNT(1) INTO v_total FROM EWP.EWPCV_TOR WHERE ID = P_TOR_ID;  
    IF v_total = 0 THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'El ToR "' || P_TOR_ID || '" no existe.');
      RETURN -1;
    END IF;

    v_notifier_hei := OBTEN_NOTIFIER_HEI_TOR(P_TOR_ID, P_HEI_TO_NOTIFY);

    IF v_notifier_hei IS NULL THEN
      EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora');
      RETURN -1;
    END IF;
  
    -- Validamos que vengan informados los campos obligatorios
    v_cod_retorno := VALIDA_BASIC_TOR(P_TOR_SIMPLE_OBJECT, P_ERROR_MESSAGE);
    IF v_cod_retorno = 0 THEN
        --Actualizamos el TOR
        ACTUALIZA_BASIC_TOR(P_TOR_ID, P_TOR_SIMPLE_OBJECT);
    END IF;
  
    PKG_COMMON.INSERTA_NOTIFICATION(OBTEN_MOBILITY_ID(P_TOR_ID), 3, P_HEI_TO_NOTIFY, v_notifier_hei);
      
    COMMIT;
    RETURN v_cod_retorno;

    EXCEPTION 
      WHEN OTHERS THEN
        EWP.CONCAT_WITHOUT_OVERFLOW(P_ERROR_MESSAGE, SUBSTR(SQLERRM,12));
        ROLLBACK;
        RETURN -1;
  END UPDATE_BASIC_TOR;

END PKG_TORS;
/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.06.01', SYSDATE, '19_UPGRADE_v01.06.01');
COMMIT;
    
    
    
    
    
    
    
    
    
