
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/


/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOI_ATT TO APLEWP;
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_LOI_ATT FOR EWP.EWPCV_LOI_ATT;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.00.01', SYSDATE, '36_grants_synonym_loi_att.sql');
Commit;