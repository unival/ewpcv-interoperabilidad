
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
CREATE OR REPLACE view EWP.IIAS_PDF_VIEW AS SELECT 
	ID                     AS IIA_ID,
    IIA_CODE               AS IIA_CODE,
	REMOTE_IIA_ID          AS REMOTE_IIA_ID,
	REMOTE_IIA_CODE        AS REMOTE_IIA_CODE,
	PDF                    AS PDF
FROM EWPCV_IIA;

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT ON EWP.IIAS_PDF_VIEW              TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
	CREATE OR REPLACE SYNONYM APLEWP.IIAS_PDF_VIEW       FOR EWP.IIAS_PDF_VIEW;


/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '27_UPGRADE_FEATURE_EHU_MEJORA_vista_pdf_iias');
COMMIT;
    
    
    
    
    
    
    
    
    
