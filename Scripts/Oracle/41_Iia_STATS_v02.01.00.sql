/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

      create table EWP.EWPCV_STATS_IIA (
           ID varchar2(255 char) not null,
            VERSION number(10,0),
            FETCHABLE NUMBER(10,0),
            LOCAL_UNAPPR_PARTNER_APPR NUMBER(10,0),
            LOCAL_APPR_PARTNER_UNAPPR NUMBER(10,0),
            BOTH_APPROVED NUMBER(10,0),
    		DUMP_DATE date,
            primary key (ID)
        );

     INSERT INTO EWP.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.statisticsIia.audit', 'true');


/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_STATS_IIA                TO APLEWP;

	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_STATS_IIA                TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

	CREATE OR REPLACE SYNONYM APLEWP.EWPCV_STATS_IIA                    FOR EWP.EWPCV_STATS_IIA;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.01.00', SYSDATE, '41_Iia_STATS');
COMMIT;
