
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
create or replace FUNCTION EWP.EXTRAE_S_AREA(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION, '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)||
			'...'
	else
		listagg(ISCED_CODE || '|:|' || ISCED_CLARIFICATION, '|;|') WITHIN GROUP (ORDER BY ISCED_CODE)
	end aux into v_lista
	from
	(
		select
			S.ISCED_CODE AS ISCED_CODE,
			S.ISCED_CLARIFICATION AS ISCED_CLARIFICATION,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION) + 3) over (order by S.ISCED_CODE) running_length,
			sum(length(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION) + 3) over (order by S.ISCED_CODE) total_length
		FROM EWP.EWPCV_COOPCOND_SUBAR CS
			LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
				ON CS.ISCED_CODE = S.ID	
		WHERE CS.COOPERATION_CONDITION_ID = P_COOP_CON_ID
		order by S.ISCED_CODE
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/

CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS_LINES(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(ADDRESS_LINE, '|;|') within group (order by ADDRESS_LINE)||
			'...'
	else
		listagg(ADDRESS_LINE, '|;|') within group (order by ADDRESS_LINE)
	end aux into v_lista
	from
	(
		select
			ADDRESS_LINE,
			sum(length(ADDRESS_LINE) + 3) over (order by ADDRESS_LINE) running_length,
			sum(length(ADDRESS_LINE) + 3) over (order by ADDRESS_LINE) total_length
		from EWP.EWPCV_FLEXIBLE_ADDRESS_LINE
		where FLEXIBLE_ADDRESS_ID = P_FLEXIBLE_ADDRESS_ID
		order by ADDRESS_LINE
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
/*
	Obtiene la informacion de los componentes asociados a los learning agreements.
*/
CREATE OR REPLACE view EWP.LA_COMPONENTS_VIEW AS WITH COMPONENTS AS (
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, RC.RECOGNIZED_LA_COMPONENT_ID as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_RECOGNIZED_LA_COMPONENT RC ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, VC.VIRTUAL_LA_COMPONENT_ID as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_VIRTUAL_LA_COMPONENT VC ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, BC.BLENDED_LA_COMPONENT_ID as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_BLENDED_LA_COMPONENT BC ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, DC.DOCTORAL_LA_COMPONENTS_ID as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_DOCTORAL_LA_COMPONENT DC ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
)
SELECT
	C.ID                                            AS COMPONENT_ID,
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	EXTRAE_CREDITS(C.id)                            AS CREDITS,
	EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	AY.START_YEAR || '|:|' || AY.END_YEAR           AS ACADEMIC_YEAR,
	C.STATUS                                        AS COMPONENT_STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_ID                                        AS LOS_ID,
	C.LOS_CODE                                      AS LOS_CODE,
	C.LOI_ID                                        AS LOI_ID,
	ATR.INSTITUTION_ID                              AS INSTITUTION_ID,
	EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
    EXTRAE_LEVELS(LOI.ID)                           AS LOI_LEVELS,
	EXTRAE_GRAD_SCHEME_DESC(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_DESC,
	EXTRAE_GRAD_SCHEME_LABEL(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_LABEL,
	EXTRAE_RES_DIST(LOI.RESULT_DISTRIBUTION)        AS RESULT_DISTR,
	EXTRAE_GROUP_TYPE(LOI.ID)                       AS LOI_GROUP_TYPE,
	EXTRAE_GROUP(LOI.ID)                            AS LOI_GROUP,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION,
    LOI.START_DATE                                  AS START_DATE,
    LOI.END_DATE                                    AS END_DATE,
    LOI.PERCENTAGE_LOWER                            AS PERCENTAGE_LOWER,
    LOI.PERCENTAGE_EQUAL                            AS PERCENTAGE_EQUAL,
    LOI.PERCENTAGE_HIGHER                           AS PERCENTAGE_HIGHER,
    LOI.RESULT_LABEL                                AS RESULT_LABEL,
    LOI.STATUS                                      AS LOI_STATUS,
    LOI.EXTENSION                                   AS LOI_EXTENSION
FROM COMPONENTS LA
INNER JOIN EWPCV_LA_COMPONENT C
    ON C.ID = LA.STUDIED_LA_COMPONENT_ID
    OR C.ID = LA.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = LA.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = LA.BLENDED_LA_COMPONENT_ID
    OR C.ID = LA.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID
LEFT JOIN EWPCV_ORGANIZATION_UNIT O
    ON ATR.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID;
/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.06.02', SYSDATE, '20_UPGRADE_v01.06.02');
COMMIT;
    
    
    
    
    
    
    
    
    
