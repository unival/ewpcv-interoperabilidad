GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ACADEMIC_TERM_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ACADEMIC_TERM TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ACADEMIC_YEAR TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT_DESCRIPTION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT_URL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT_DETAILS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CONTACT_DETAILS_EMAIL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_COOPERATION_CONDITION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_CREDIT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DISTR_CATEGORIES TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DISTR_DESCRIPTION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DURATION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FACT_SHEET_URL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FACT_SHEET TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FLEXIBLE_ADDRESS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FLEXIBLE_ADDRESS_LINE TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FLEXAD_DELIV_POINT_COD TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FLEXAD_RECIPIENT_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_GRADING_SCHEME_DESC TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_GRADING_SCHEME_LABEL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_GRADING_SCHEME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_PARTNER_CONTACTS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_PARTNER TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INST_ORG_UNIT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INSTITUTION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INSTITUTION_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LANGUAGE_ITEM TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LEARNING_AGREEMENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOI TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOI_CREDITS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS_DESCRIPTION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS_LOI TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS_LOS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LOS_URLS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_NUMBER TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_PARTICIPANT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_TYPE TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_UPDATE_REQUEST TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_NOTIFICATION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ORG_UNIT_ORG_UNIT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ORGANIZATION_UNIT_NAME TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_ORGANIZATION_UNIT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_PERSON TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_PHONE_NUMBER TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_RESULT_DISTRIBUTION TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_RESULT_DIST_CATEGORY TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_STUDIED_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_RECOGNIZED_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_VIRTUAL_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_BLENDED_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DOCTORAL_LA_COMPONENT TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_SUBJECT_AREA TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_LANGUAGE_SKILL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_COOPCOND_SUBAR_LANSKIL TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_SIGNATURE TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_LA TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INFORMATION_ITEM TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INST_INF_ITEM TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_REQUIREMENTS_INFO TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_OU_INF_ITEM TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_OU_REQUIREMENT_INFO TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INS_REQUIREMENTS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_FACTSHEET_DATES TO APLEWP; 