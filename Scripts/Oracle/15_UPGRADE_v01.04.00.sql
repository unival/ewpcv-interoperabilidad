
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  

-------------------------
-- ALTER/CREATE TABLES -- 
-------------------------
/

	--EWPCV_MOBILITY
	ALTER TABLE EWP.EWPCV_MOBILITY ADD MOBILITY_COMMENT VARCHAR2(1000); 
	ALTER TABLE EWP.EWPCV_MOBILITY ADD EQF_LEVEL_NOMINATION NUMBER(3,0); 
	ALTER TABLE EWP.EWPCV_MOBILITY ADD EQF_LEVEL_DEPARTURE NUMBER(3,0); 
	ALTER TABLE EWP.EWPCV_MOBILITY ADD ACADEMIC_TERM_ID VARCHAR2(255); 
	ALTER TABLE EWP.EWPCV_MOBILITY ADD STATUS_OUTGOING NUMBER(7,0);
	ALTER TABLE EWP.EWPCV_MOBILITY ADD STATUS_INCOMING NUMBER(7,0);
	ALTER TABLE EWP.EWPCV_MOBILITY ADD RECEIVING_IIA_ID varchar2(255);
	ALTER TABLE EWP.EWPCV_MOBILITY ADD MODIFY_DATE DATE;
	
	ALTER TABLE EWP.EWPCV_MOBILITY 
	ADD CONSTRAINT FK_ACADEMIC_TERM_ID
	FOREIGN KEY (ACADEMIC_TERM_ID) 
	REFERENCES EWP.EWPCV_ACADEMIC_TERM;
	
	ALTER TABLE EWP.EWPCV_MOBILITY DROP (STATUS, EQF_LEVEL); 
	
	--EWPCV_PHOTO_URL
	CREATE TABLE EWP.EWPCV_PHOTO_URL (
		PHOTO_URL_ID VARCHAR2(255) NOT NULL, 
		CONTACT_DETAILS_ID VARCHAR(255) NOT NULL,
		URL VARCHAR2(255),
		PHOTO_SIZE VARCHAR2(255),
		PHOTO_DATE DATE,
		PHOTO_PUBLIC NUMBER(1) DEFAULT 0,

		PRIMARY KEY (PHOTO_URL_ID)
	);
 
	ALTER TABLE EWP.EWPCV_PHOTO_URL 
	ADD CONSTRAINT FK_CONTACT_DETAILS_6
	FOREIGN KEY (CONTACT_DETAILS_ID)
	REFERENCES EWP.EWPCV_CONTACT_DETAILS;
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

create or replace FUNCTION EWP.ExtraerAnio(START_DATE TIMESTAMP) RETURN NUMBER AS
	 ANIO NUMBER;
	BEGIN
		ANIO := EXTRACT(YEAR FROM START_DATE);  
		RETURN ANIO;
	END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_PHOTO_URL_LIST(P_PHOTO_URL_ID IN VARCHAR2) RETURN VARCHAR2 AS
	v_lista VARCHAR2(6000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS 
	SELECT LISTAGG(PU.URL|| '|:|' || PU.PHOTO_SIZE || '|:|' || to_char(PU.PHOTO_DATE, 'YYYY/MM/DD') || '|:|' || PU.PHOTO_PUBLIC, '|;|')
	WITHIN GROUP (ORDER BY PU.CONTACT_DETAILS_ID ASC)
	FROM EWPCV_PHOTO_URL PU
	WHERE PU.CONTACT_DETAILS_ID = p_id;
BEGIN
	OPEN C(P_PHOTO_URL_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
/*
	Obtiene la informacion de contacto adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_ADD_INFO_VIEW AS SELECT 
	AI.ID										AS ADD_INFO_ID,
	I.INSTITUTION_ID 		 			        AS INSTITUTION_ID,
	AI."TYPE"		 					        AS INFO_TYPE,
	EXTRAE_EMAILS(AI.CONTACT_DETAIL_ID)	    AS EMAIL,
	EXTRAE_TELEFONO(AI.CONTACT_DETAIL_ID)      AS PHONE,
	EXTRAE_URLS_CONTACTO(AI.CONTACT_DETAIL_ID) AS URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
INNER JOIN EWPCV_INFORMATION_ITEM AI
	ON IIT.INFORMATION_ID = AI.ID
    AND AI."TYPE" NOT IN ('HOUSING','VISA','INSURANCE');
	
/*
	Obtiene la informacion de contacto sobre requerimientos adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_REQ_INFO_VIEW AS SELECT 
	REI.ID										AS REQ_INFO_ID,
	I.INSTITUTION_ID 							 AS INSTITUTION_ID,
	REI."TYPE" 								     AS REQ_TYPE,
	REI.NAME 								    AS NAME,
	REI.DESCRIPTION 						     AS DESCRIPTION,
    EXTRAE_EMAILS(REI.CONTACT_DETAIL_ID)	    AS EMAIL,
	EXTRAE_TELEFONO(REI.CONTACT_DETAIL_ID)      AS PHONE,
	EXTRAE_URLS_CONTACTO(REI.CONTACT_DETAIL_ID) AS URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_INS_REQUIREMENTS IR
	ON IR.INSTITUTION_ID = I.ID 
INNER JOIN EWPCV_REQUIREMENTS_INFO REI
	ON IR.REQUIREMENT_ID = REI.ID;
	
--INSTITUTION_VIEW
    
CREATE OR REPLACE VIEW EWP.INSTITUTION_VIEW AS 
SELECT DISTINCT
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    ins.LOGO_URL                                       AS INSTITUTION_LOGO_URL,
    EXTRAE_URLS_CONTACTO(pricondet.ID)                    AS INSTITUTION_WEBSITE,
    EXTRAE_URLS_FACTSHEET(ins.FACT_SHEET)               AS INSTITUTION_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
    EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)     AS STREET_ADDR_LINES,
    EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)           AS STREET_ADDRESS,
    EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)    AS MAILING_ADDR_LINES,
    EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)          AS MAILING_ADDRESS
    
        /*
            Datos de la institution
        */
        FROM EWP.EWPCV_INSTITUTION ins
        LEFT JOIN EWP.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN EWP.EWPCV_CONTACT_DETAILS pricondet ON ins.primary_contact_detail_id = pricondet.ID
        LEFT JOIN EWP.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
        LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address;

 --OUNIT_VIEW

CREATE OR REPLACE VIEW EWP.OUNIT_VIEW AS 
SELECT DISTINCT
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID									 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    EXTRAE_NOMBRES_OUNIT(ounit.ID)                       AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    EXTRAE_URLS_CONTACTO(pricondet.ID) 				     AS OUNIT_WEBSITE,
	EXTRAE_URLS_FACTSHEET(ounit.FACT_SHEET)			     AS OUNIT_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
	EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)       AS STREET_ADDR_LINES,
	EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)             AS STREET_ADDRESS,
    EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)      AS MAILING_ADDR_LINES,
	EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)            AS MAILING_ADDRESS,
	ounit.PARENT_OUNIT_ID								 AS PARENT_OUNIT_ID,
    ounit.IS_EXPOSED									 AS IS_EXPOSED 
    
        /*
            Datos de la organización
        */
        FROM EWP.EWPCV_ORGANIZATION_UNIT ounit
        LEFT JOIN EWP.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.id
        LEFT JOIN EWP.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id
		
		INNER JOIN EWP.EWPCV_INST_ORG_UNIT iou ON ounit.ID = iou.ORGANIZATION_UNITS_ID
		INNER JOIN EWP.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN EWP.EWPCV_CONTACT_DETAILS pricondet ON ounit.primary_contact_detail_id = pricondet.ID
        LEFT JOIN EWP.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
		LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address;

 --MOBILITY_VIEW
CREATE OR REPLACE view EWP.MOBILITY_VIEW AS SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL_DEPARTURE                                                                                      AS EQF_LEVEL_DEPARTURE,
	M.EQF_LEVEL_NOMINATION                                                                                     AS EQF_LEVEL_NOMINATION,
	M.IIA_ID                                                                                                   AS IIA_ID,
	M.RECEIVING_IIA_ID                                                                                         AS RECEIVING_IIA_ID,
	SA.ISCED_CODE || '|:|' || SA.ISCED_CLARIFICATION                                                           AS SUBJECT_AREA,
	M.STATUS_OUTGOING                                                                                          AS MOBILITY_STATUS_OUTGOING,
	M.STATUS_INCOMING                                                                                          AS MOBILITY_STATUS_INCOMING,
	M.MOBILITY_COMMENT                                                                                         AS MOBILITY_COMMENT,
	MT.MOBILITY_CATEGORY || '|:|' || MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.GLOBAL_ID                                                                                               AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	EXTRAE_PHOTO_URL_LIST(STC.CONTACT_DETAILS_ID)                                                              AS STUDENT_PHOTO_URLS,
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	EXTRAE_FAX(SC.CONTACT_DETAILS_ID)                                                                    	   AS SENDER_CONTACT_FAXES,
	EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	EXTRAE_ADDRESS_LINES(SCD.MAILING_ADDRESS)                                                                  AS SENDER_CONT_M_ADDR_LINES,
	EXTRAE_ADDRESS(SCD.MAILING_ADDRESS)                                                                        AS SENDER_CONTACT_MAILING_ADDR,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	EXTRAE_FAX(SAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_SEN_CONTACT_FAXES,
	EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	EXTRAE_ADDRESS_LINES(SACD.MAILING_ADDRESS)                                                                 AS ADMV_SEN_CONT_M_ADDR_LINES,
	EXTRAE_ADDRESS(SACD.MAILING_ADDRESS)                                                                       AS ADMV_SEN_CONTACT_MAILING_ADDR,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	EXTRAE_FAX(RC.CONTACT_DETAILS_ID)                                                                    	   AS RECEIVER_CONTACT_FAXES,
	EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	EXTRAE_ADDRESS_LINES(RCD.MAILING_ADDRESS)                                                                  AS RECEIVER_CONT_M_ADDR_LINES,
	EXTRAE_ADDRESS(RCD.MAILING_ADDRESS)                                                                        AS RECEIVER_CONTACT_MAILING_ADDR,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	EXTRAE_FAX(RAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_REC_CONTACT_FAXES,
	EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	EXTRAE_ADDRESS_LINES(RACD.MAILING_ADDRESS)                                                                 AS ADMV_REC_CONTACT_M_ADDR_LINES,
	EXTRAE_ADDRESS(RACD.MAILING_ADDRESS)                                                                       AS ADMV_REC_CONTACT_MAILING_ADDR,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY,
	EXTRAE_ACADEMIC_TERM(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_TERM
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
LEFT JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;

COMMENT ON COLUMN EWP.MOBILITY_VIEW.SUBJECT_AREA IS 'FORMATO: ISCED_CODE|:|CLARIFICATION';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_PHONE IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_PHOTO_URLS IS 'FORMATO: URL:PHOTO_SIZE:PHOTO_DATE:PHOTO_PUBLIC;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_PHONES IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_PHONES IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_PHONES IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_PHONES IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_INSTITUTION_NAMES IS 'FORMATO: NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_OUNIT_NAMES IS 'FORMATO: NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ACADEMIC_TERM IS 'FORMATO: START_DATE:END_DATE:INSTITUTION_ID:ORGANIZATION_UNIT_CODE:TERM_NUMBER:TOTAL_TERMS';
	
/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/
create or replace PACKAGE EWP.PKG_MOBILITY_LA AS 

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  

	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
	*/	
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_CODE				   VARCHAR2(255 CHAR),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR)
	  );

	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );
	  
	/* Objeto que modela el objeto con la foto del estudiante. Campos obligatorios marcados con *.
		URL*    Url del fichero con la foto.
		PHOTO_SIZE   Tamaño de la foto, formato width x height, ej:35x45.
		PHOTO_DATE     Fecha real en la que se agregó la foto.
		PHOTO_PUBLIC   Indica si la foto es pública, 0 - privada, 1 - pública
	  
	*/
	TYPE PHOTO_URL IS RECORD
	(
	URL                VARCHAR2(255),
	PHOTO_SIZE         VARCHAR2(255),
	PHOTO_DATE         DATE,
	PHOTO_PUBLIC       NUMBER(1,0)
	);

	/* Lista con los objetos de las fotos del estudiante. */ 
	TYPE PHOTO_URL_LIST IS TABLE OF PHOTO_URL INDEX BY BINARY_INTEGER ;

	/*Datos del estudiante. Campos obligatorios marcados con *.
		GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
		CONTACT					Informacion del estudiante
		PHOTO_URL_LIST			Listado de objetos con las fotos del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON,
		"PHOTO_URL_LIST"  PKG_MOBILITY_LA.PHOTO_URL_LIST
	  );	 

  /* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
		SENDING_INSTITUTION*    Datos de la instituciÃ³n que envia al estudiante.
		RECEIVING_INSTITUTION*    Datos de la instituciÃ³n que recibe al estudiante.
		ACTUAL_ARRIVAL_DATE     Fecha real de llegada del estudiante aldestino.
		ACTUAL_DEPATURE_DATE    Fecha real de salida del estudiante del origen.
		PLANED_ARRIVAL_DATE*    Fecha prevista de llegada del estudiante aldestino.
		PLANED_DEPATURE_DATE*   Fecha prevista de salida del estudiante del origen.
		IIA_ID            UUID del IIA asociado a la movilidad
		RECEIVING_IIA_ID  UUID del IIA asociado a la movilidad
		COOPERATION_CONDITION_ID  identificador de la condicion de coperacion asociada la movilidad
		STUDENT*          Datos del estudiante
		SUBJECT_AREA*       Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
		STUDENT_LANGUAGE_SKILLS*  Niveles de idiomas que declara el estudiante.
		SENDER_CONTACT*       Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
		SENDER_ADMV_CONTACT     Informacion de contacto de la administrativo en la institucion origen
		RECEIVER_CONTACT*     Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
		RECEIVER_ADMV_CONTACT   Informacion de contacto de la administrativo en la institucion destino
		MOBILITY_TYPE       Tipo de movilidad, puede tomar los siquientes valores:
					Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
		EQF_LEVEL_NOMINATION * Nivel de estudios del estudiante. 
		EQF_LEVEL_DEPARTURE*   Nivel de estudios del estudiante. 
		ACADEMIC_TERM*  Informacion del periodo academico de la asignatura. 
					  Requerido en componentes estudiados y reconocidos y opcional en los virtuales
		STATUS_OUTGOING Estado de la movilidad Outgoing. 
					  0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED. Si no se informa, por defecto es 2 - NOMINATION.
  */
	TYPE MOBILITY IS RECORD
	(
		SENDING_INSTITUTION     EWP.INSTITUTION,
		RECEIVING_INSTITUTION   EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE     DATE,
		ACTUAL_DEPATURE_DATE    DATE,
		PLANED_ARRIVAL_DATE     DATE,
		PLANED_DEPATURE_DATE    DATE,
		IIA_ID            VARCHAR(255 CHAR),
		RECEIVING_IIA_ID VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID  VARCHAR(255 CHAR),
		STUDENT           PKG_MOBILITY_LA.STUDENT,
		SUBJECT_AREA        EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS   EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT        EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT     EWP.CONTACT_PERSON,
		RECEIVER_CONTACT      EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT   EWP.CONTACT_PERSON,
		MOBILITY_TYPE       VARCHAR2 (255),
		EQF_LEVEL_NOMINATION NUMBER(3,0),
		EQF_LEVEL_DEPARTURE NUMBER(3,0),
		ACADEMIC_TERM EWP.ACADEMIC_TERM,
		STATUS_OUTGOING  NUMBER(7,0)
	);

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					TIMESTAMP,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					BLOB
	  );

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisiÃ³n del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );

	-- FUNCIONES 

	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Cancela una movilidad
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2, 
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  	

END PKG_MOBILITY_LA;
/
create or replace PACKAGE BODY EWP.PKG_MOBILITY_LA AS 

	FUNCTION ES_OMOBILITY(P_MOBILITY IN MOBILITY, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_resultado NUMBER(1,0) := 0;
	BEGIN
		IF P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID <> P_HEI_TO_NOTIFY THEN
			v_resultado := -1;
		END IF;

		RETURN v_resultado;
	END;
	
	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de los periodos academicos
			academic_year, institution_id, term_number, total_terms
	*/
	FUNCTION VALIDA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
	BEGIN         
		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ACADEMIC_YEAR';
		END IF;
        
		IF P_ACADEMIC_TERM.TERM_NUMBER IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TERM_NUMBER';
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TOTAL_TERMS';
		END IF;
        
        IF P_ACADEMIC_TERM.INSTITUTION_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una subject area: 
			isced_code 
	*/
	FUNCTION VALIDA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_SUBJECT_AREA.ISCED_CODE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-ISCED_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una language skill: 
			lang, cefr_level 
	*/
	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_LANGUAGE_SKILL.LANG	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-LANG';
		END IF;

		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-CEFR_LEVEL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una institucion: 
			Institution_id, organization_unit_code 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
        v_ounit_count NUMBER := 0;
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT ID
            FROM EWPCV_INSTITUTION 
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);  
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una persona de contacto: 
			GIVEN_NAME, FAMILY_NAME, EMAIL_LIST
	*/
	FUNCTION VALIDA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CONTACT_PERSON.GIVEN_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GIVEN_NAME';
		END IF;

		IF P_CONTACT_PERSON.FAMILY_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-FAMILY_NAME';
		END IF;

		IF P_CONTACT_PERSON.CONTACT.EMAIL IS NULL OR P_CONTACT_PERSON.CONTACT.EMAIL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-CONTACT.EMAIL';
		END IF;

		RETURN v_cod_retorno;
	END;
	
 
	/*
	Valida los campos obligatorios de la foto del estudiante:
	  url
	*/
	FUNCTION VALIDA_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.URL IS NULL OR LENGTH(P_PHOTO_URL.URL) = 0 THEN
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-URL';
			v_cod_retorno := -1;
		END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.PHOTO_SIZE IS NOT NULL AND NOT REGEXP_LIKE(P_PHOTO_URL.PHOTO_SIZE, '^\d*x\d*$') THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-PHOTO_SIZE';
		END IF;
		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_PHOTO_URL_LIST(P_PHOTO_URL_LIST IN PKG_MOBILITY_LA.PHOTO_URL_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
		v_error NUMBER := 0;
		v_mensaje VARCHAR2(2000);
	BEGIN
		IF P_PHOTO_URL_LIST IS NOT NULL AND P_PHOTO_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_PHOTO_URL_LIST.FIRST .. P_PHOTO_URL_LIST.LAST
			LOOP
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;
	 
				v_cod_retorno := VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL_LIST(i), v_mensaje);
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;
	  
				IF v_error = 1 THEN
					P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-PHOTO_URL_LIST(' || i || ')' || v_mensaje;
				END IF;
				v_mensaje := '';
			END LOOP;
		END IF;

		v_cod_retorno := CASE WHEN v_error = 1 THEN -1 ELSE 0 END;

		RETURN v_cod_retorno;
	END;
  

	/*
		Valida los campos obligatorios de un estudiante: 
			global_id
	*/
	FUNCTION VALIDA_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
		v_mensaje_photo_url VARCHAR2(2000);
	BEGIN

		IF P_STUDENT.GLOBAL_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GLOBAL_ID';
		END IF;
		IF VALIDA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_mensaje_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						CONTACT: ' || v_mensaje_contact;
		END IF;
		
		IF VALIDA_PHOTO_URL_LIST(P_STUDENT.PHOTO_URL_LIST, v_mensaje_photo_url) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				PHOTO_URL_LIST: ' || v_mensaje_photo_url;
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de una movilidad y de los objetos anidados: 
			planed_arrival_date,planed_depature_date, iia_code, status, sending_institution, receiving_institution, student
	*/
	FUNCTION VALIDA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_s_institution VARCHAR2(2000);
		v_mensaje_r_institution VARCHAR2(2000);
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_contact VARCHAR2(2000);
		v_mensaje_r_contact VARCHAR2(2000);
		v_mensaje_acaterm VARCHAR2(2000);
	BEGIN

		IF P_MOBILITY.PLANED_ARRIVAL_DATE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_ARRIVAL_DATE';
		END IF;

		IF P_MOBILITY.PLANED_DEPATURE_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_DEPATURE_DATE';
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.SENDING_INSTITUTION, v_mensaje_s_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE|| '
					SENDING_INSTITUTION: ' || v_mensaje_s_institution;
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.RECEIVING_INSTITUTION, v_mensaje_r_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_INSTITUTION: ' || v_mensaje_r_institution;
		END IF;

		IF VALIDA_STUDENT(P_MOBILITY.STUDENT, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					STUDENT: ' || v_mensaje_student;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_mensaje_s_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDER_CONTACT: ' || v_mensaje_s_contact;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_mensaje_r_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVER_CONTACT: ' || v_mensaje_r_contact;
		END IF;
		
		IF VALIDA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-ACADEMIC_TERM: '|| v_mensaje_acaterm;
		END IF;

		IF P_MOBILITY.EQF_LEVEL_NOMINATION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-EQF_LEVEL_NOMINATION';
		END IF;
	  
		IF P_MOBILITY.EQF_LEVEL_DEPARTURE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-EQF_LEVEL_DEPARTURE';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida que el iias y la coop condition indicadas existan
	*/
	FUNCTION VALIDA_MOBILITY_IIA(P_IIA_ID IN VARCHAR2, P_IS_REMOTE NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
	BEGIN
		IF P_IIA_ID IS NOT NULL THEN
		  SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_IIA WHERE UPPER(ID) = UPPER(P_IIA_ID) AND IS_REMOTE = P_IS_REMOTE;

			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					El acuerdo interistitucional indicado no existe en el sistema.';
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
	FUNCTION VALIDA_DATOS_MOBILITY_INST(P_S_INSTITUTION IN EWP.INSTITUTION, P_R_INSTITUTION  IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		-- validacion de sender 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_S_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_S_INSTITUTION.INSTITUTION_ID||' informada como sender institution no existe en el sistema.';
		ELSIF P_S_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_S_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_S_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_S_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_S_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		-- validacion de receiver 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_R_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_R_INSTITUTION.INSTITUTION_ID||' informada como receiver institution no existe en el sistema.';
		ELSIF P_R_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_R_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_R_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_R_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_R_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
	*/
	FUNCTION VALIDA_DATOS_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;

	BEGIN
		IF P_MOBILITY.IIA_ID IS NOT NULL THEN
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.IIA_ID, 0, P_ERROR_MESSAGE);
		END IF;
		 
		IF v_cod_retorno = 0 AND P_MOBILITY.RECEIVING_IIA_ID IS NOT NULL THEN 
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.RECEIVING_IIA_ID, 1, P_ERROR_MESSAGE);
		END IF;
		
		IF v_cod_retorno = 0 THEN 
				v_cod_retorno := VALIDA_DATOS_MOBILITY_INST(P_MOBILITY.SENDING_INSTITUTION, P_MOBILITY.RECEIVING_INSTITUTION, P_ERROR_MESSAGE);
		END IF;
		
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una firma
			signer_name, signer_position, signer_email, timestamp,  signature
	*/
	FUNCTION VALIDA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_SIGNATURE.SIGNER_NAME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_NAME';
		END IF;

		IF P_SIGNATURE.SIGNER_POSITION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_POSITION';
		END IF;

		IF P_SIGNATURE.SIGNER_EMAIL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_EMAIL';
		END IF;

		IF P_SIGNATURE.SIGN_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGN_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN     
        IF UPPER(P_INSTITUTION_ID) <> UPPER(P_ACADEMIC_TERM.INSTITUTION_ID) THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -INSTITUTION_ID: La institucion '|| P_ACADEMIC_TERM.INSTITUTION_ID || ' del periodo academico del componente no coincide con la institucion ' || P_INSTITUTION_ID;
		ELSIF P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN 
                v_cod_retorno := -1;
                 P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -OUNIT_CODE: La unidad organizativa ' || P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE || ' vinculada a ' || P_INSTITUTION_ID || ' no existe en el sistema.';
            END IF;
        END IF;
		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_OUNIT_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REV IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
        v_send_inst_id VARCHAR2(255);
        v_receiv_inst_id VARCHAR2(255);
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
        CURSOR c_s_inst_r_inst(p_mob_id IN VARCHAR2, p_mob_rev IN NUMBER) IS 
            SELECT SENDING_INSTITUTION_ID, RECEIVING_INSTITUTION_ID
            FROM EWPCV_MOBILITY 
            WHERE ID = p_mob_id
            AND MOBILITY_REVISION = p_mob_rev;
	BEGIN
        OPEN c_s_inst_r_inst(P_MOBILITY_ID, P_MOBILITY_REV);
        FETCH c_s_inst_r_inst INTO v_send_inst_id, v_receiv_inst_id;
        CLOSE c_s_inst_r_inst;
        
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP
			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 OR P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 5 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_receiv_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_send_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;
            IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los creditos
			scheme, value
	*/
	FUNCTION VALIDA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CREDIT.SCHEME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-SCHEME';
		END IF;

		IF P_CREDIT.CREDIT_VALUE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-VALUE';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_credit VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP

			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE NOT IN (0,1,2,3,4) THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE (valor erroneo ' || P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
				IF P_COMPONENTS_LIST(i).RECOGNITION_CONDITIONS IS NOT NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-RECOGNITION_CONDITIONS (No informar para el tipo de componente '|| P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')' ;
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;

			IF P_COMPONENTS_LIST(i).TITLE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-TITLE';
			END IF;

			IF P_COMPONENTS_LIST(i).STATUS IS NOT NULL THEN
				IF P_COMPONENTS_LIST(i).STATUS = 0 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (3,4,5) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).STATUS = 1 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (0,1,2) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSE 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-STATUS (valor erroneo'|| P_COMPONENTS_LIST(i).STATUS||')';
				END IF;

				IF P_COMPONENTS_LIST(i).REASON_CODE IS NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-REASON_CODE';
				END IF;
			END IF;

			IF VALIDA_CREDIT(P_COMPONENTS_LIST(i).CREDIT, v_mensaje_credit) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-CREDIT: ' || v_mensaje_credit;
				v_mensaje_credit := '';
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;

		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un Learning Agreement
			mobility, 
	*/
	FUNCTION VALIDA_LEARNING_AGREEMENT(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
	BEGIN

		IF P_LA.MOBILITY_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := '- MOBILITY_ID: ' || P_ERROR_MESSAGE;
		END IF;

		IF P_LA.COMPONENTS IS NULL OR p_LA.COMPONENTS.COUNT = 0 THEN 
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			COMPONENTS';
		ELSIF VALIDA_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				COMPONENTS: ' || v_mensaje_components;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.STUDENT_SIGNATURE, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				STUDENT_SIGNATURE: ' || v_mensaje_student;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE, v_mensaje_s_coord) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				SENDING_HEI_SIGNATURE: ' || v_mensaje_s_coord;
		END IF;		

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/* Borra el estudiante de la tabla mobility participant*/
	PROCEDURE BORRA_STUDENT(P_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255CHAR);
		v_contact_details_id VARCHAR2(255CHAR);
	BEGIN
		SELECT CONTACT_ID INTO v_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		SELECT CONTACT_DETAILS_ID INTO v_contact_details_id FROM EWPCV_CONTACT WHERE ID = v_contact_id;
		DELETE FROM EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = v_contact_details_id;
		DELETE FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		PKG_COMMON.BORRA_CONTACT(v_contact_id);
	END;

	/*
		Borra creditos asociados a un componente
	*/
	PROCEDURE BORRA_CREDITS(P_COMPONENT_ID IN VARCHAR2) AS
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CREDITS_ID
			FROM EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = p_id;
	BEGIN
		FOR rec IN c(P_COMPONENT_ID) 
		LOOP
			DELETE FROM EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = rec.CREDITS_ID;
			DELETE FROM EWPCV_CREDIT WHERE ID = rec.CREDITS_ID;
		END LOOP;
	END;

	/*
		Borra un componente
	*/
	PROCEDURE BORRA_COMPONENT(P_ID IN VARCHAR2) AS
		v_at_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_TERM_DISPLAY_NAME
			FROM EWPCV_LA_COMPONENT
			WHERE ID = p_id;
	BEGIN
		BORRA_CREDITS(P_ID);
		OPEN c(P_ID);
		FETCH c INTO v_at_id;
		CLOSE c;
		DELETE FROM EWPCV_LA_COMPONENT WHERE ID = P_ID;
		PKG_COMMON.BORRA_ACADEMIC_TERM(v_at_id);
	END;

	/*
		Borra un componente de tipo studied
	*/
	PROCEDURE BORRA_STUDIED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDIED_LA_COMPONENT_ID
			FROM EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = rec.STUDIED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.STUDIED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo recognized
	*/
	PROCEDURE BORRA_RECOGNIZED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = rec.RECOGNIZED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.RECOGNIZED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo virtual
	*/
	PROCEDURE BORRA_VIRTUAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT VIRTUAL_LA_COMPONENT_ID
			FROM EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = rec.VIRTUAL_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.VIRTUAL_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo blended
	*/
	PROCEDURE BORRA_BLENDED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT BLENDED_LA_COMPONENT_ID
			FROM EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = rec.BLENDED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.BLENDED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo doctoral
	*/
	PROCEDURE BORRA_DOCTORAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = rec.DOCTORAL_LA_COMPONENTS_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.DOCTORAL_LA_COMPONENTS_ID);
		END LOOP;
	END;

	/*
		Borra una revision de un LA
	*/
	PROCEDURE BORRA_LA(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		v_s_id VARCHAR2(255 CHAR);
		v_sc_id VARCHAR2(255 CHAR);
		v_rc_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDENT_SIGN, SENDER_COORDINATOR_SIGN, RECEIVER_COORDINATOR_SIGN
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;

	BEGIN

		BORRA_STUDIED_COMP(P_LA_ID, LA_REVISION);
		BORRA_RECOGNIZED_COMP(P_LA_ID, LA_REVISION);
		BORRA_VIRTUAL_COMP(P_LA_ID, LA_REVISION);
		BORRA_BLENDED_COMP(P_LA_ID, LA_REVISION);
		BORRA_DOCTORAL_COMP(P_LA_ID, LA_REVISION);

		OPEN c(P_LA_ID, LA_REVISION);
		FETCH c INTO v_s_id, v_sc_id, v_rc_id;
		CLOSE c;

		DELETE FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_s_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_sc_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_rc_id;
	END;

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/
	PROCEDURE BORRA_MOBILITY_LANSKILL(P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REVISION IN NUMBER) AS
		v_lskill_id VARCHAR2(255 CHAR);
		CURSOR c_lskill(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LANGUAGE_SKILL_ID
			FROM EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR l_rec IN c_lskill(P_MOBILITY_ID, P_MOBILITY_REVISION)
			LOOP
				DELETE FROM EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = l_rec.LANGUAGE_SKILL_ID;
				DELETE FROM EWPCV_LANGUAGE_SKILL WHERE ID = l_rec.LANGUAGE_SKILL_ID;
			END LOOP;	
	END;

	/*
		Borra una movilidad
	*/
	PROCEDURE BORRA_MOBILITY(P_MOBILITY_ID IN VARCHAR2) AS
		v_student_count NUMBER;
		v_mtype_count NUMBER;

		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_REVISION, 
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, ISCED_CODE
			FROM EWPCV_MOBILITY 
			WHERE ID = p_id;

		CURSOR c_la(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
			FROM EWPCV_MOBILITY_LA 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR mobility_rec IN c_mobility(P_MOBILITY_ID)
		LOOP
			BORRA_MOBILITY_LANSKILL(mobility_rec.ID, mobility_rec.MOBILITY_REVISION);

			FOR la_rec IN c_la(mobility_rec.ID, mobility_rec.MOBILITY_REVISION)
			LOOP
				BORRA_LA(la_rec.LEARNING_AGREEMENT_ID, la_rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;	

			DELETE FROM EWPCV_MOBILITY WHERE ID = mobility_rec.ID AND MOBILITY_REVISION = mobility_rec.MOBILITY_REVISION;

			DELETE FROM EWPCV_SUBJECT_AREA WHERE ID = mobility_rec.ISCED_CODE;

			SELECT COUNT(1) INTO v_student_count FROM EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = mobility_rec.MOBILITY_PARTICIPANT_ID;
			IF v_student_count = 0 THEN 
				BORRA_STUDENT(mobility_rec.MOBILITY_PARTICIPANT_ID);
			END IF;

			SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = mobility_rec.SENDER_CONTACT_ID;
			IF v_scontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = mobility_rec.SENDER_ADMV_CONTACT_ID;
			IF v_sacontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_ADMV_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = mobility_rec.RECEIVER_CONTACT_ID;
			IF v_rcontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = mobility_rec.RECEIVER_ADMV_CONTACT_ID;
			IF v_racontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_ADMV_CONTACT_ID);
			END IF;
		END LOOP;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_o_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR C(p_start IN VARCHAR2, p_end IN VARCHAR) IS 
			SELECT ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(p_start)
			AND	UPPER(START_YEAR) =  UPPER(p_end);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN
		OPEN C(SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4) ,SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		FETCH C INTO v_a_id;
		CLOSE C;
		IF v_a_id IS NULL THEN 
			v_a_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
				VALUES(v_a_id, SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4),SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		END IF;

		OPEN ounit_cursor(P_ACADEMIC_TERM.INSTITUTION_ID, P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
		FETCH ounit_cursor INTO v_o_id;
		CLOSE ounit_cursor;

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (v_id, P_ACADEMIC_TERM.END_DATE, P_ACADEMIC_TERM.START_DATE , P_ACADEMIC_TERM.INSTITUTION_ID , v_o_id, v_a_id, P_ACADEMIC_TERM.TOTAL_TERMS, P_ACADEMIC_TERM.TERM_NUMBER);

		IF P_ACADEMIC_TERM.DESCRIPTION IS NOT NULL AND P_ACADEMIC_TERM.DESCRIPTION.COUNT > 0 THEN
			FOR i IN P_ACADEMIC_TERM.DESCRIPTION.FIRST .. P_ACADEMIC_TERM.DESCRIPTION.LAST 
			LOOP
				v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ACADEMIC_TERM.DESCRIPTION(i));
				INSERT INTO EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (v_id, v_li_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;
	
	
	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_PARTICIPANT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT) RETURN VARCHAR2 AS
		v_m_p_id VARCHAR2(255);
		v_c_id   VARCHAR2(255);
		v_c_d_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
	BEGIN
		v_m_p_id := EWP.GENERATE_UUID();
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, null, null);
		INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, GLOBAL_ID, CONTACT_ID) VALUES (v_m_p_id, P_STUDENT.GLOBAL_ID, v_c_id);

		SELECT (
			SELECT CONTACT_DETAILS_ID FROM EWPCV_CONTACT C WHERE C.ID = v_c_id
		) INTO v_c_d_id FROM DUAL;

		IF v_c_d_id IS NOT NULL THEN
			IF P_STUDENT.PHOTO_URL_LIST IS NOT NULL AND P_STUDENT.PHOTO_URL_LIST.COUNT > 0 THEN 
				FOR i IN P_STUDENT.PHOTO_URL_LIST.FIRST .. P_STUDENT.PHOTO_URL_LIST.LAST
				LOOP
					v_p_id := EWP.GENERATE_UUID();
					INSERT INTO EWP.EWPCV_PHOTO_URL (
						PHOTO_URL_ID, CONTACT_DETAILS_ID, 
						URL, PHOTO_SIZE, 
						PHOTO_DATE, PHOTO_PUBLIC) 
					VALUES (v_p_id, v_c_d_id, 
						P_STUDENT.PHOTO_URL_LIST(i).URL, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_SIZE, 
						P_STUDENT.PHOTO_URL_LIST(i).PHOTO_DATE, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_PUBLIC);
				END LOOP;
			END IF;
		END IF;
		
		RETURN v_m_p_id;
	END;


	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA');
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE, 'MOBILITY_LA');
		END IF;
		RETURN v_id;
	END;


	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_REVISION IN NUMBER, P_OMOBILITY_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_s_ounit_id VARCHAR2(255);
		v_r_ounit_id VARCHAR2(255);
		v_student_id VARCHAR2(255);
		v_s_contact_id VARCHAR2(255);
		v_s_admv_contact_id VARCHAR2(255);
		v_r_contact_id VARCHAR2(255);
		v_r_admv_contact_id VARCHAR2(255);
		v_mobility_type_id VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
		v_academic_term_id VARCHAR2(255);
		v_status_incoming NUMBER;
		v_status_outgoing NUMBER;
	BEGIN
		v_s_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.SENDING_INSTITUTION);
		v_r_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.RECEIVING_INSTITUTION);
		v_student_id := INSERTA_MOBILITY_PARTICIPANT(P_MOBILITY.STUDENT);
		v_s_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, null, null);
		v_s_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT, null, null);
		v_r_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, null, null);
		v_r_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT, null, null);

		v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_MOBILITY.SUBJECT_AREA);
		v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);
				
		v_academic_term_id := INSERTA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM);

		   
		-- Por defecto el status_outgoing es NOMINATION
		IF P_MOBILITY.STATUS_OUTGOING IS NULL THEN
			v_status_outgoing := 2; 
		ELSE
			v_status_outgoing := P_MOBILITY.STATUS_OUTGOING;
		END IF;

		IF P_OMOBILITY_ID IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
		ELSE 
			v_id := P_OMOBILITY_ID;
		END IF;
		
		INSERT INTO EWPCV_MOBILITY (ID,
		ACTUAL_ARRIVAL_DATE,
		ACTUAL_DEPARTURE_DATE,
		IIA_ID,
		RECEIVING_IIA_ID,
		ISCED_CODE,
		MOBILITY_PARTICIPANT_ID,
		MOBILITY_REVISION,
		PLANNED_ARRIVAL_DATE,
		PLANNED_DEPARTURE_DATE,
		RECEIVING_INSTITUTION_ID,
		RECEIVING_ORGANIZATION_UNIT_ID,
		SENDING_INSTITUTION_ID,
		SENDING_ORGANIZATION_UNIT_ID,
		MOBILITY_TYPE_ID,
		SENDER_CONTACT_ID,
		SENDER_ADMV_CONTACT_ID,
		RECEIVER_CONTACT_ID,
		RECEIVER_ADMV_CONTACT_ID,
		ACADEMIC_TERM_ID,
		EQF_LEVEL_DEPARTURE,
		EQF_LEVEL_NOMINATION,
		STATUS_OUTGOING,
		MODIFY_DATE)
		VALUES (v_id,
		P_MOBILITY.ACTUAL_ARRIVAL_DATE,
		P_MOBILITY.ACTUAL_DEPATURE_DATE,
		P_MOBILITY.IIA_ID,
		P_MOBILITY.RECEIVING_IIA_ID,
		v_isced_code,
		v_student_id,
		(P_REVISION +1),
		P_MOBILITY.PLANED_ARRIVAL_DATE,
		P_MOBILITY.PLANED_DEPATURE_DATE,
		P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID,
		v_r_ounit_id,
		P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID,
		v_s_ounit_id,
		v_mobility_type_id,
		v_s_contact_id,
		v_s_admv_contact_id,
		v_r_contact_id,
		v_r_admv_contact_id,
		v_academic_term_id,
		P_MOBILITY.EQF_LEVEL_DEPARTURE,
		P_MOBILITY.EQF_LEVEL_NOMINATION,
		v_status_outgoing,
		sysdate);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT >0 THEN 
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (v_id, (P_REVISION + 1), v_lang_skill_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (v_id, P_SIGNATURE.SIGNER_NAME, P_SIGNATURE.SIGNER_POSITION , P_SIGNATURE.SIGNER_EMAIL , P_SIGNATURE.SIGN_DATE , P_SIGNATURE.SIGNER_APP, P_SIGNATURE.SIGNATURE);
		RETURN v_id;
	END;

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		INSERT INTO EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
			VALUES (v_id, P_CREDIT.SCHEME, P_CREDIT.CREDIT_VALUE);
		RETURN v_id;
	END;

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_COMPONENTE(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_at_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		v_at_id:= INSERTA_ACADEMIC_TERM(P_COMPONENT.ACADEMIC_TERM);

		--FALTAN LOS Y LOIS

		INSERT INTO EWPCV_LA_COMPONENT (ID,ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (v_id, v_at_id, NULL,P_COMPONENT.LOS_CODE, NULL, P_COMPONENT.STATUS, P_COMPONENT.TITLE, P_COMPONENT.REASON_CODE, P_COMPONENT.REASON_TEXT,
				P_COMPONENT.LA_COMPONENT_TYPE, P_COMPONENT.RECOGNITION_CONDITIONS, P_COMPONENT.SHORT_DESCRIPTION);

		v_c_id := INSERTA_CREDIT(P_COMPONENT.CREDIT);
		INSERT INTO EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (v_id,v_c_id);

		IF P_COMPONENT.LA_COMPONENT_TYPE = 0 THEN
			INSERT INTO EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
			INSERT INTO EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 2 THEN
			INSERT INTO EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 3 THEN
			INSERT INTO EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 4 THEN
			INSERT INTO EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una revision de un learning agreement.
	*/
	Function INSERTA_LA_REVISION(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_LA_REVISION IN NUMBER, P_MOBILITY_REVISION IN NUMBER) RETURN VARCHAR2 AS 
		v_la_id varchar2(255);
		v_stud_sign_id varchar2(255);
		v_s_hei_sign_id varchar2(255);
		v_changes_id varchar2(255);
		v_c_id varchar2(255);
		v_indice NUMBER := 0;
	BEGIN 
		v_stud_sign_id := INSERTA_SIGNATURE(P_LA.STUDENT_SIGNATURE);
		v_s_hei_sign_id :=  INSERTA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE);

		IF P_LA_ID IS NULL THEN 
			v_la_id := EWP.GENERATE_UUID();
		ELSE 
			v_la_id := P_LA_ID;
		END IF;
		v_changes_id := v_la_id || '-' ||P_LA_REVISION;
		INSERT INTO EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (v_la_id,P_LA_REVISION,v_stud_sign_id,v_s_hei_sign_id, 0, SYSDATE, v_changes_id);

		INSERT INTO EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(P_LA.MOBILITY_ID, P_MOBILITY_REVISION, v_la_id, P_LA_REVISION);

		WHILE v_indice < P_LA.COMPONENTS.COUNT 
		LOOP
			v_c_id := INSERTA_COMPONENTE(P_LA.COMPONENTS(v_indice),v_la_id, P_LA_REVISION);
			v_indice := v_indice + 1 ;
		END LOOP;
		RETURN v_la_id;
	END;

	/*
		Genera el tag approve de un mobility update request
	*/
	FUNCTION CREA_TAG_APPROVE(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_approve dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:approve-proposal-v1' );
		l_node_approve := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_approve;
	END;

	/*
		Genera el tag comment de un mobility update request
	*/
	FUNCTION CREA_TAG_COMMENT(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_COMMENT IN VARCHAR2, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_comment dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment-proposal-v1' );
		l_node_comment := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja comment
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_COMMENT);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_comment;
	END;


	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_ACCEPT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE ) AS 
		v_id VARCHAR2(255);
		v_mobility_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_approve dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN

		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_approve := CREA_TAG_APPROVE(P_CHANGES_ID ,v_mobility_id,  P_SIGNATURE, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_approve);

		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 0, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_REJECT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2) AS 
		v_mobility_id VARCHAR2(255);
		v_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_comment dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN
		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_comment := CREA_TAG_COMMENT(P_CHANGES_ID, v_mobility_id, P_SIGNATURE, P_OBSERVATION, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_comment);
		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 1, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************

	PROCEDURE ACTUALIZA_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY) AS 
		v_m_revision VARCHAR2(255);
		v_id VARCHAR2(255);
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY
			WHERE ID = P_ID;
	BEGIN 
		OPEN c_mobility(P_OMOBILITY_ID);
		FETCH c_mobility INTO v_m_revision;
		CLOSE c_mobility;
		
		--Insertamos la nueva revisión de la Movilidad
		v_id := INSERTA_MOBILITY(P_MOBILITY, v_m_revision, P_OMOBILITY_ID);
         
        -- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
        UPDATE EWPCV_MOBILITY_LA SET MOBILITY_ID = P_OMOBILITY_ID, MOBILITY_REVISION = (v_m_revision + 1) WHERE MOBILITY_ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

	END;

	--  **********************************************
	--	************** NOTIFICACION ******************
	--	**********************************************

	FUNCTION OBTEN_NOTIFIER_HEI_LA(P_LA_ID IN VARCHAR, P_LA_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY M
			INNER JOIN EWPCV_MOBILITY_LA MLA 
				ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = p_id AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN 
		OPEN c(P_LA_ID, P_LA_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN UPPER(v_s_hei);
		ELSE 
			RETURN NULL;
		END IF;
	END;

	FUNCTION OBTEN_NOTIFIER_HEI_MOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN 
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN  UPPER(v_s_hei); 
		ELSE 
			RETURN NULL;
		END IF;
	END;
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_m_revision NUMBER := 0;
		v_la_id VARCHAR2(255);
		CURSOR C(p_id IN VARCHAR2) IS SELECT MAX(MOBILITY_REVISION)
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id;
		CURSOR C_LA(p_id IN VARCHAR2) IS 
		SELECT LEARNING_AGREEMENT_ID 
		FROM EWPCV_MOBILITY_LA 
		WHERE MOBILITY_ID = p_id;

	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		OPEN C(P_LA.MOBILITY_ID);
		FETCH C INTO v_m_revision;
		CLOSE C;
		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada no existe';
			RETURN -1;
		END IF;

		OPEN C_LA(P_LA.MOBILITY_ID);
		FETCH C_LA INTO v_la_id;
		CLOSE C_LA;
		IF v_la_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: '||v_la_id;
			RETURN -1;
		END IF;		

        v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
        
		P_LA_ID := INSERTA_LA_REVISION(null, P_LA, 0, v_m_revision);
		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, 0, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion receptora';
			P_LA_ID := null;
			ROLLBACK;
			RETURN -1;
		END IF;

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_LEARNING_AGREEMENT; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_no_firmado NUMBER:=0;
		v_la_revision NUMBER;
		v_m_revision NUMBER;
		v_la_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2) IS 
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;
		CURSOR c_m_revision(p_mobility_id IN VARCHAR2, p_la_id IN VARCHAR2, p_la_revision IN NUMBER) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_mobility_id
				AND LEARNING_AGREEMENT_ID = p_la_id
				AND LEARNING_AGREEMENT_REVISION = p_la_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		OPEN c_la(P_LA_ID);
		FETCH c_la INTO v_la_revision;
		CLOSE c_la;

		IF v_la_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado no existe';
			RETURN -1;
		END IF;

		Open c_m_revision(P_LA.MOBILITY_ID, P_LA_ID, v_la_revision);
		FETCH c_m_revision INTO v_m_revision;
		CLOSE c_m_revision;

		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada para el learning agreement no es correcta.';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_no_firmado FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND STATUS = 0;
		IF v_no_firmado > 0 THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, v_la_revision, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
		
		v_la_id := INSERTA_LA_REVISION(P_LA_ID, P_LA, v_la_revision + 1, v_m_revision);

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_LEARNING_AGREEMENT; 	

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;


		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_ACCEPT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE);
			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END ACCEPT_LEARNING_AGREEMENT; 	 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_REJECT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE, P_OBSERVATION);

			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END REJECT_LEARNING_AGREEMENT; 	

	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER	AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count NUMBER := 0;
		v_m_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS SELECT LEARNING_AGREEMENT_REVISION
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;

		CURSOR c_mob(p_id IN VARCHAR2, p_revision IN NUMBER) IS SELECT MOBILITY_ID
			FROM EWPCV_MOBILITY_LA 
			WHERE LEARNING_AGREEMENT_ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID;
		IF v_count > 0 THEN 
			FOR rec IN c(P_LA_ID) 
			LOOP 
				IF v_notifier_hei IS NULL THEN 
					v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION, P_HEI_TO_NOTIFY);
				END IF;

				IF v_notifier_hei IS NULL THEN 
					P_ERROR_MESSAGE := 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.';
					RETURN -1;
				END IF;

				IF v_m_id IS NULL THEN 
					OPEN c_mob(P_LA_ID,rec.LEARNING_AGREEMENT_REVISION);
					FETCH c_mob INTO v_m_id;
					CLOSE c_mob;
				END IF;

				BORRA_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			PKG_COMMON.INSERTA_NOTIFICATION(v_m_id, 5, P_HEI_TO_NOTIFY, v_notifier_hei);	
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_LEARNING_AGREEMENT; 
	
	FUNCTION VALIDA_STATUS(P_STATUS IN NUMBER, P_ES_OUTGOING IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER 
	AS
		v_outgoing_statuses VARCHAR2(10) := '0123';
		v_incoming_statuses VARCHAR2(10) := '456';
		v_resultado NUMBER := 0;
	BEGIN
		IF P_ES_OUTGOING <> 0 AND P_ES_OUTGOING <> 1 THEN
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_outgoing no tiene un status válido';
			RETURN -1;
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = 0 AND INSTR(v_outgoing_statuses, P_STATUS) = 0 THEN 
			v_resultado := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_outgoing no tiene un status válido';
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = -1 AND INSTR(v_incoming_statuses, P_STATUS) = 0 THEN 
			v_resultado := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_incoming no tiene un status válido';
		END IF;
		RETURN v_resultado;
	END;

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_sending_hei VARCHAR2(255);
		v_es_outgoing NUMBER(1,0);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);

		v_sending_hei := P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID;

		IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
			P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
			RETURN -1;
		END IF;

		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		END IF;
		IF v_cod_retorno = 0 THEN
			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			P_OMOBILITY_ID := INSERTA_MOBILITY(P_MOBILITY, -1, null);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_MOBILITY; 	

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_sending_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR C_SENDING_HEI IS SELECT ID, SENDING_INSTITUTION_ID
        FROM EWPCV_MOBILITY 
        WHERE ID = P_OMOBILITY_ID
        ORDER BY MOBILITY_REVISION DESC;
		v_es_outgoing NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN

            OPEN C_SENDING_HEI;
            FETCH C_SENDING_HEI INTO v_id, v_sending_hei;
            CLOSE C_SENDING_HEI;

            IF v_id IS NULL THEN 
            	P_ERROR_MESSAGE := 'No existe la movilidad indicada';
                RETURN -1;
			END IF;

			IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                RETURN -1;
			END IF;
			 
			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				ACTUALIZA_MOBILITY(P_OMOBILITY_ID,P_MOBILITY);
				PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
			END IF;
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_MOBILITY; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
		v_m_revision NUMBER ;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			BORRA_MOBILITY(P_OMOBILITY_ID);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La movilidad no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_MOBILITY;  	

	/* Cancela una movilidad
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		
		COMMIT;	
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.';
				RETURN -1;
			END IF;
		 
			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;

			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2, 
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
			P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
			RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE,
			ACTUAL_ARRIVAL_DATE = P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = P_ACTUAL_DEPARTURE
			WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;


END PKG_MOBILITY_LA;
/


/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_PHOTO_URL TO APLEWP;
	GRANT EXECUTE ON EWP.ExtraerAnio TO APLEWP;
	GRANT EXECUTE ON EWP.EXTRAE_PHOTO_URL_LIST TO APLEWP;
	GRANT EXECUTE ON EWP.ExtraerAnio TO EWP_APROVISIONAMIENTO;
	GRANT EXECUTE ON EWP.EXTRAE_PHOTO_URL_LIST TO EWP_APROVISIONAMIENTO;
/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

	CREATE OR REPLACE SYNONYM APLEWP.ExtraerAnio FOR EWP.ExtraerAnio;
	CREATE OR REPLACE SYNONYM APLEWP.EWPCV_PHOTO_URL FOR EWP.EWPCV_PHOTO_URL;
	CREATE OR REPLACE SYNONYM APLEWP.EXTRAE_PHOTO_URL_LIST FOR EWP.EXTRAE_PHOTO_URL_LIST;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.04.00', SYSDATE, '15_UPGRADE_v01.04.00');
COMMIT;
    
    
    
    
    
    
    
    
    
