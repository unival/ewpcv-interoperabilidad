CREATE OR REPLACE SYNONYM  APLEWP.IIAS_VIEW FOR EWP.IIAS_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.IIA_COOP_CONDITIONS_VIEW FOR EWP.IIA_COOP_CONDITIONS_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.MOBILITY_VIEW FOR EWP.MOBILITY_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.LEARNING_AGREEMENT_VIEW FOR EWP.LEARNING_AGREEMENT_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.LA_COMPONENTS_VIEW FOR EWP.LA_COMPONENTS_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.DICTIONARIES_VIEW FOR EWP.DICTIONARIES_VIEW; 
CREATE OR REPLACE SYNONYM  APLEWP.FACTSHEET_VIEW FOR EWP.FACTSHEET_VIEW; 
CREATE OR REPLACE SYNONYM  APLEWP.FACTSHEET_ADD_INFO_VIEW FOR EWP.FACTSHEET_ADD_INFO_VIEW;
CREATE OR REPLACE SYNONYM  APLEWP.FACTSHEET_REQ_INFO_VIEW FOR EWP.FACTSHEET_REQ_INFO_VIEW;

GRANT SELECT ON EWP.EVENT_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON EWP.DICTIONARIES_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DICTIONARY TO APLEWP; 
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_DICTIONARY FOR EWP.EWPCV_DICTIONARY;
CREATE OR REPLACE SYNONYM APLEWP.EVENT_VIEW FOR EWP.EVENT_VIEW;

ALTER TABLE EWP.EWPCV_EVENT ADD STATUS VARCHAR2(5 CHAR);

--  **************************************************************************
--	**************** MODIFICACIONES PARA EL BATCH DE PURGA *******************
--	**************************************************************************

ALTER TABLE EWP.EWPCV_NOTIFICATION ADD PROCESSING_DATE DATE;
COMMENT ON COLUMN EWP.EWPCV_NOTIFICATION.PROCESSING_DATE IS 'Fecha que indica cuando se comenzo a procesar una notificacion';

ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST ADD PROCESSING_DATE DATE;
COMMENT ON COLUMN EWP.EWPCV_MOBILITY_UPDATE_REQUEST.PROCESSING_DATE IS 'Fecha que indica cuando se comenzo a procesar una solicitud';

--  **************************************************************************
--	************ MODIFICACIONES PARA EL BATCH DE INSTITUCIONES ***************
--	**************************************************************************
create table EWP.EWPCV_INSTITUTION_IDENTIFIERS (
    SCHAC VARCHAR2(255 CHAR) NOT NULL,
	PREVIOUS_SCHAC VARCHAR2(255 CHAR),
	PIC VARCHAR2(255 CHAR),
	ERASMUS VARCHAR2(255 CHAR),
	EUC VARCHAR2(255 CHAR),
	ERASMUS_CHARTER VARCHAR2(255 CHAR),
    INSTITUTION_NAMES VARCHAR2(255 CHAR),
    OTHER_IDS VARCHAR2(255 CHAR),
    primary key (SCHAC)
);
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.SCHAC IS 'SCHAC CODE OF THE UNIVERSITY';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.PREVIOUS_SCHAC IS 'A previously used SCHAC identifier. Servers MUST provide all SCHACidentifiers their HEI have used in the past.';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.PIC IS ' PIC identifier.';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.ERASMUS IS 'Erasmus institutional code.';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.EUC IS 'DEPRECATED, use "erasmus-charter" instead. Erasmus University Charter number(aka EUC number).';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.ERASMUS_CHARTER IS 'Erasmus Charter number (EUC/ECHE/etc.) This MAY contain both EUC numbers (Erasmus University Charter) or ECHE numbers (Erasmus Charter for Higher Education).';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.INSTITUTION_NAMES IS 'THE NAMES OF THE UNIVERSITY IN THE INDICATED LANGUAGE. FORMAT: NAME|:|LANGUAGE|;|...NAME|:|LANGUAGE';
COMMENT ON COLUMN EWP.EWPCV_INSTITUTION_IDENTIFIERS.OTHER_IDS IS 'THE OTHER IDENTIFIERS SEPARATED BY "|;|" AND "|:|" EXAMPLE: "myCode|:|D EBERSWA01|;| MyOtherCode|:| 999877747|;| ..." ';
                  
CREATE OR REPLACE VIEW EWP.INSTITUTION_IDENTIFIERS_VIEW AS 
  SELECT 
	SCHAC AS SCHAC,
	PREVIOUS_SCHAC AS PREVIOUS_SCHAC,
	PIC AS PIC,
	ERASMUS AS ERASMUS,
	EUC AS EUC,
	ERASMUS_CHARTER AS ERASMUS_CHARTER,
    INSTITUTION_NAMES AS INSTITUTION_NAMES,
    OTHER_IDS AS OTHER_IDS
FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS;

COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.OTHER_IDS IS 'THE OTHER IDENTIFIERS SEPARATED BY "|;|" AND "|:|" EXAMPLE: "erasmus|:|D EBERSWA01|;| pic|:| 999877747|;| ..." ';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.SCHAC IS 'SCHAC CODE OF THE UNIVERSITY';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.PREVIOUS_SCHAC IS 'A previously used SCHAC identifier. Servers MUST provide all SCHACidentifiers their HEI have used in the past.';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.PIC IS ' PIC identifier.';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.ERASMUS IS 'Erasmus institutional code.';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.EUC IS 'DEPRECATED, use "erasmus-charter" instead. Erasmus University Charter number(aka EUC number).';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.ERASMUS_CHARTER IS 'Erasmus Charter number (EUC/ECHE/etc.) This MAY contain both EUC numbers (Erasmus University Charter) or ECHE numbers (Erasmus Charter for Higher Education).';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.INSTITUTION_NAMES IS 'THE NAMES OF THE UNIVERSITY IN THE INDICATED LANGUAGE. FORMAT: NAME|:|LANGUAGE|;|...NAME|:|LANGUAGE';
COMMENT ON COLUMN EWP.INSTITUTION_IDENTIFIERS_VIEW.OTHER_IDS IS 'THE OTHER IDENTIFIERS SEPARATED BY "|;|" AND "|:|" EXAMPLE: "myCode|:|D EBERSWA01|;| MyOtherCode|:| 999877747|;| ..." ';

GRANT SELECT ON EWP.INSTITUTION_IDENTIFIERS_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_INSTITUTION_IDENTIFIERS TO APLEWP; 
CREATE OR REPLACE SYNONYM APLEWP.INSTITUTION_IDENTIFIERS_VIEW FOR EWP.INSTITUTION_IDENTIFIERS_VIEW;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_INSTITUTION_IDENTIFIERS FOR EWP.EWPCV_INSTITUTION_IDENTIFIERS;

--  ***************************************************
--	************ Funciones para VISTAS  ***************
--	***************************************************
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(TO_CHAR(ATR.START_DATE, 'YYYY.MM.DD')  || '|:|' || TO_CHAR(ATR.END_DATE, 'YYYY.MM.DD') 
		|| '|:|' || ATR.INSTITUTION_ID || '|:|' || o.ORGANIZATION_UNIT_CODE 
		|| '|:|' || ATR.TERM_NUMBER || '|:|' || ATR.TOTAL_TERMS, '|;|')
		WITHIN GROUP (ORDER BY ATR.ID )
    FROM EWP.EWPCV_ACADEMIC_TERM ATR
		LEFT JOIN EWP.EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
    WHERE ATR.ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);

	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(F.BUILDING_NUMBER|| '|:|' || F.BUILDING_NAME || '|:|' || F.STREET_NAME 
        || '|:|' || F.UNIT || '|:|' || F."FLOOR" || '|:|' || F.POST_OFFICE_BOX 
        || '|:|' || (
        SELECT LISTAGG(Delivery_point_code, ',') 
			WITHIN GROUP (ORDER BY Delivery_point_code)
        FROM EWP.EWPCV_FLEXAD_DELIV_POINT_COD
        WHERE FLEXIBLE_ADDRESS_ID = F.ID
        ), '|;|')
		WITHIN GROUP (ORDER BY F.STREET_NAME)
    FROM EWP.EWPCV_FLEXIBLE_ADDRESS F
    WHERE F.ID =  p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS_LINES(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(ADDRESS_LINE, '|;|')
		WITHIN GROUP (ORDER BY ADDRESS_LINE)
    FROM EWP.EWPCV_FLEXIBLE_ADDRESS_LINE 
    WHERE FLEXIBLE_ADDRESS_ID = p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_CREDITS(P_COMPONENT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(CR.CREDIT_LEVEL || '|:|' || CR.SCHEME || '|:|' || CR.CREDIT_VALUE, '|;|')
		WITHIN GROUP (ORDER BY CR.CREDIT_LEVEL)
    FROM EWP.EWPCV_CREDIT CR,
		EWP.EWPCV_COMPONENT_CREDITS CCR
    WHERE CR.ID = CCR.CREDITS_ID
		AND CCR.COMPONENT_ID = p_id;
BEGIN
	OPEN C(P_COMPONENT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPCIONES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_DESCRIPTION CD,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CD.CONTACT_ID
		AND CD.DESCRIPTION_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_DESCRIPTIONS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_DESCRIPTION D,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE D.DESCRIPTION_ID = LAIT.ID
		AND D.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_EMAILS(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EMAIL, '|;|')
		WITHIN GROUP (ORDER BY EMAIL)
    FROM EWP.EWPCV_CONTACT_DETAILS_EMAIL 
    WHERE CONTACT_DETAILS_ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_EQF_LEVEL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EQF_LEVEL, '|;|')
		WITHIN GROUP (ORDER BY EQF_LEVEL)
    FROM EWP.EWPCV_COOPCOND_EQFLVL
    WHERE COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_FIRMAS(P_FIRMA_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(SIGNER_NAME || '|:|' || SIGNER_POSITION 
        || '|:|' || SIGNER_EMAIL || '|:|' || "TIMESTAMP" || '|:|' || SIGNER_APP, '|;|')
			WITHIN GROUP (ORDER BY SIGNER_NAME)
    FROM EWP.EWPCV_SIGNATURE
    WHERE ID = p_id;
BEGIN
	OPEN C(P_FIRMA_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NIVELES_IDIOMAS(P_MOBILITY_ID IN VARCHAR2, P_REVISION IN NUMBER) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2, p_revision IN NUMBER) IS  SELECT LISTAGG(S."LANGUAGE"|| '|:|' || S.CEFR_LEVEL , '|;|')
		WITHIN GROUP (ORDER BY S."LANGUAGE")
    FROM EWP.EWPCV_MOBILITY_LANG_SKILL MLS,
		EWP.EWPCV_LANGUAGE_SKILL S
    WHERE MLS.LANGUAGE_SKILL_ID = S.ID
		AND MLS.MOBILITY_ID = p_id
		AND MLS.MOBILITY_REVISION = p_revision;
BEGIN
	OPEN C(P_MOBILITY_ID, P_REVISION);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ACADEMIC_TERM_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.DISP_NAME_ID = LAIT.ID
		AND N.ACADEMIC_TERM_ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_NAME CN,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CN.CONTACT_ID
		AND CN.NAME_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION(P_INSTITUTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_INSTITUTION I,
		EWP.EWPCV_INSTITUTION_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE I.ID = N.INSTITUTION_ID
		AND N.NAME_ID = LAIT.ID
		AND I.INSTITUTION_ID = p_id;
BEGIN
	OPEN C(P_INSTITUTION_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.NAME_ID = LAIT.ID
		AND N.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ORGANIZATION_UNIT O,
		EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE O.ID = N.ORGANIZATION_UNIT_ID
		AND N.NAME_ID = LAIT.ID
		AND O.ID = p_id;
BEGIN
	OPEN C(P_OUNIT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_S_AREA_L_SKILL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(S.ISCED_CODE || '|:|' || S.ISCED_CLARIFICATION || '|:|' || L."LANGUAGE" || '|:|' ||L.CEFR_LEVEL, '|;|')
		WITHIN GROUP (ORDER BY S.ISCED_CODE)
    FROM EWP.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
		LEFT JOIN EWP.EWPCV_LANGUAGE_SKILL L	
			ON CSL.LANGUAGE_SKILL_ID = L.ID
		LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
			ON CSL.ISCED_CODE = S.ID	
    WHERE CSL.COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_TELEFONO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(P.E164 || '|:|' || P.EXTENSION_NUMBER || '|:|' || P.OTHER_FORMAT, '|;|')
		WITHIN GROUP (ORDER BY P.E164)
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_PHONE_NUMBER P
    WHERE P.ID = CD.PHONE_NUMBER
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_URLS_CONTACTO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_CONTACT_URL U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE CD.ID = U.CONTACT_DETAILS_ID
		AND U.URL_ID = LAIT.ID
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
create or replace FUNCTION EWP.EXTRAE_URLS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_URLS U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE U.URL_ID = LAIT.ID
		AND U.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
/*
	Obtiene la informacion de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_VIEW AS SELECT 
	I.INSTITUTION_ID    				  	 	 	AS INSTITUTION_ID,
	I.ABBREVIATION	 					  	 	 	AS ABREVIATION,
	MAX(F.DECISION_WEEKS_LIMIT)			  	 	 	AS DECISION_WEEK_LIMIT,
	MAX(F.TOR_WEEKS_LIMIT) 					  	 	AS TOR_WEEK_LIMIT,
	TO_CHAR(MAX(F.NOMINATIONS_AUTUM_TERM), '--MM-DD')			  	 	AS NOMINATIONS_AUTUM_TERM,
	TO_CHAR(MAX(F.NOMINATIONS_SPRING_TERM), '--MM-DD')			  	 	AS NOMINATIONS_SPRING_TERM,
	TO_CHAR(MAX(F.APPLICATION_AUTUM_TERM), '--MM-DD')			  	 	AS APPLICATION_AUTUM_TERM,
	TO_CHAR(MAX(F.APPLICATION_SPRING_TERM), '--MM-DD')			  	 	AS APPLICATION_SPRING_TERM,
	EXTRAE_EMAILS(MAX(F.CONTACT_DETAILS_ID))	  	AS APPLICATION_EMAIL,
	EXTRAE_TELEFONO(MAX(F.CONTACT_DETAILS_ID)) 	 	AS APPLICATION_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(F.CONTACT_DETAILS_ID)) AS APPLICATION_URLS,
	EXTRAE_EMAILS(MAX(HI.CONTACT_DETAIL_ID))	  	AS HOUSING_EMAIL,
	EXTRAE_TELEFONO(MAX(HI.CONTACT_DETAIL_ID)) 	 	AS HOUSING_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(HI.CONTACT_DETAIL_ID))	AS HOUSING_URLS,
	EXTRAE_EMAILS(MAX(VI.CONTACT_DETAIL_ID))	  	AS VISA_EMAIL,
	EXTRAE_TELEFONO(MAX(VI.CONTACT_DETAIL_ID)) 	  	AS VISA_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(VI.CONTACT_DETAIL_ID)) AS VISA_URLS,
	EXTRAE_EMAILS(MAX(II.CONTACT_DETAIL_ID))	  	AS INSURANCE_EMAIL,
	EXTRAE_TELEFONO(MAX(II.CONTACT_DETAIL_ID)) 	 	AS INSURANCE_PHONE,
	EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID))	AS INSURANCE_URLS
FROM EWPCV_INSTITUTION I
INNER JOIN EWPCV_FACT_SHEET F 
    ON I.FACT_SHEET = F.ID
LEFT JOIN EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
LEFT JOIN EWPCV_INFORMATION_ITEM HI
	ON IIT.INFORMATION_ID = HI.ID AND HI."TYPE" = 'HOUSING'
LEFT JOIN EWPCV_INFORMATION_ITEM VI
	ON IIT.INFORMATION_ID = VI.ID AND VI."TYPE" = 'VISA'
LEFT JOIN EWPCV_INFORMATION_ITEM II
	ON IIT.INFORMATION_ID = II.ID AND II."TYPE" = 'INSURANCE'
GROUP BY I.INSTITUTION_ID,I.ABBREVIATION
;

/*
	Obtiene la informacion del registro de eventos de la aplicacion.
*/
CREATE OR REPLACE view EWP.EVENT_VIEW AS SELECT 

ID					AS ID,
EVENT_DATE			AS EVENT_DATE,
TRIGGERING_HEI		AS TRIGGERING_HEI,
OWNER_HEI			AS OWNER_HEI,
EVENT_TYPE			AS EVENT_TYPE,
CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
ELEMENT_TYPE		AS ELEMENT_TYPE,
OBSERVATIONS		AS OBSERVATIONS,
STATUS				AS STATUS
FROM EWP.EWPCV_EVENT;

/*
	Obtiene la informacion de las movilidades de las personas involucradas en la misma. 
	Para consultar las outgoing filtraremos por M.SENDING_INSTITUTION_ID = nuestro SCHAC
	Para consultar las incoming filtraremos por M.RECEIVING_INSTITUTION_ID = nuestro SCHAC
*/
CREATE OR REPLACE view EWP.MOBILITY_VIEW AS SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL                                                                                                AS EQF_LEVEL,
	M.IIA_ID                                                                                                 AS IIA_CODE,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE || '|:|' || SA.ISCED_CLARIFICATION                                                             AS SUBJECT_AREA,
	M.STATUS                                                                                                   AS MOBILITY_STATUS,
	MT.MOBILITY_CATEGORY || '|:|' || MT.MOBILITY_GROUP                                                           AS MOBILITY_TYPE,
	EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.ID                                                                                                      AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
INNER JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
INNER JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
INNER JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
INNER JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
INNER JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
INNER JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;

/
CREATE OR REPLACE PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoño
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoño
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  
	
	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;

	
	  
	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;
	
	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );
	
	-- FUNCIONES 
	
	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
	
	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	
END PKG_FACTSHEET;
/
create or replace PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-DECISION_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-TOR_WEEK_LIMIT';
		END IF;
		
		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_AUTUM_TERM';
		END IF;
		
		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_SPRING_TERM';
		END IF;
		
		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_AUTUM_TERM';
		END IF;
		
		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_SPRING_TERM';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
	
		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-EMAIL';
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-PHONE';
		END IF;
		
		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-URL';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP
		
			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ADITIONAL_INFO'|| i || ':' ||v_mensaje_it;
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ACCESSIBILITY_REQUIREMENTS'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				REQUIREMENTS_INFO'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
	BEGIN

		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID';
		END IF;
        
		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			FACTSHEET:' || v_mensaje_factsheet;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			APPLICATION_INFO:' || v_mensaje_app_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			HOUSING_INFO:' || v_mensaje_hou_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			VISA_INFO:' || v_mensaje_vis_info;
		END IF;
		
		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			INSURANCE_INFO:' || v_mensaje_ins_info;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ADDITIONAL_INFO:' || v_mensaje_add_info;
			END IF;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ACCESSIBILITY_REQUIREMENTS:' || v_mensaje_acc_req;
			END IF;
		END IF;
		
		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			REQUIREMENTS_INFO:' || v_mensaje_add_req;
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;
	
	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;
	
	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN
		
		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;
	
	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
		v_cd_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
		OPEN c(P_FACTSHEET_ID);
		FETCH c into v_cd_id;
		CLOSE c;
		
		UPDATE EWPCV_INSTITUTION SET FACT_SHEET = NULL WHERE ID = P_INSTITUTION_ID;
		DELETE FROM EWPCV_FACT_SHEET WHERE ID = P_FACTSHEET_ID;
		PKG_COMMON.BORRA_CONTACT_DETAILS(v_cd_id);
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;
	
	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************
	
	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;
		
		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);
		
		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;
		
		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;
	
	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);
		
		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,UPPER(P_INF_TYPE),v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN
		
		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);
		
		v_factsheet_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
			VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
				P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
		RETURN v_factsheet_id;
	END;
	
	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN
	
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		
		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, UPPER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;
	
	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);
		
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;
		
	END;
	
	
	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;
	
	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
	
		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);
		
		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;
	
	
	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);
		
	BEGIN
	
		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;
		
		BORRA_NOMBRES(P_INSTITUTION_ID);
		PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		
		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);
		
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);
		
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;
		
		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;
			
	END;
	
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		SELECT COUNT(1)INTO v_count FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(P_FACTSHEET.INSTITUTION_ID) AND FACT_SHEET IS NOT NULL;
		IF v_count > 0 THEN 
			P_ERROR_MESSAGE := 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET);
		END IF;
		COMMIT;
	
		RETURN v_cod_retorno;
		
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;
	
	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET 
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id;
		CLOSE c_inst;
		
		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;
		
		IF v_f_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;
		
		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ID, FACT_SHEET 
			FROM EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id;
		CLOSE c_inst;
		
		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;
		
		IF v_f_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;
		
		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;
/
create or replace PACKAGE BODY EWP.PKG_IIAS AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	/*
		Valida las fechas obligatorias de un IIA 
	*/
	FUNCTION VALIDA_FECHAS_IIA(P_IIA IN PKG_IIAS.IIA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_IIA.START_DATE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-START_DATE';
		END IF;

		IF P_IIA.END_DATE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-END_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida mobility type
	*/
	FUNCTION VALIDA_MOBILITY_TYPE(P_MOBILITY_TYPE IN PKG_IIAS.MOBILITY_TYPE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_CATEGORY) NOT IN ('TEACHING', 'STUDIES', 'TRAINING') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY: Valor no permitido (teaching, studies, training)';
		END IF;

		IF P_MOBILITY_TYPE.MOBILITY_GROUP IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_GROUP) NOT IN ('STUDENT', 'STAFF') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP: Valor no permitido (student, staff)';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto Institution
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
		END IF;

		IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ORGANIZATION_UNIT_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto PARTNER de las COOP_COND del IIA
	*/
	FUNCTION VALIDA_PARTNER (P_PARTNER IN PKG_IIAS.PARTNER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_partner VARCHAR2(2000);
	BEGIN
		IF P_PARTNER.INSTITUTION IS NULL THEN
			v_cod_retorno := -1;
			v_mensaje_partner := P_ERROR_MESSAGE || '
				INSTITUTION:';
		ELSIF VALIDA_INSTITUTION(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || v_mensaje_partner;
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida las COOPERATION_CONDITION del IIA
	*/
	FUNCTION VALIDA_COOP_COND_IIA (P_COOP_COND_LIST IN PKG_IIAS.COOPERATION_CONDITION_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_send_partner VARCHAR2(2000);
		v_mensaje_receiv_partner VARCHAR2(2000);
		v_mensaje_mobility_type VARCHAR2(2000);
        v_is_error BOOLEAN;
	BEGIN
		FOR i IN P_COOP_COND_LIST.FIRST .. P_COOP_COND_LIST.LAST 
		LOOP
			IF P_COOP_COND_LIST(i).START_DATE IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-START_DATE [' || i || ']';
            END IF;
			IF P_COOP_COND_LIST(i).END_DATE IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-END_DATE [' || i || ']';
			END IF;
			IF P_COOP_COND_LIST(i).EQF_LEVEL_LIST IS NULL OR P_COOP_COND_LIST(i).EQF_LEVEL_LIST.COUNT = 0 THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-EQF_LEVEL_LIST [' || i || ']';	
			END IF;
            IF P_COOP_COND_LIST(i).MOBILITY_NUMBER IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_NUMBER [' || i || ']';	
			END IF;
			IF P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_TYPE [' || i || ']';
			ELSIF VALIDA_MOBILITY_TYPE(P_COOP_COND_LIST(i).MOBILITY_TYPE, v_mensaje_mobility_type) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					MOBILITY_TYPE [' || i || ']: ' || v_mensaje_mobility_type;
			END IF;	
			IF P_COOP_COND_LIST(i).BLENDED IS NULL THEN 	
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-BLENDED [' || i || ']';
			END IF;	
			IF P_COOP_COND_LIST(i).SENDING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SENDING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).SENDING_PARTNER, v_mensaje_send_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDING_PARTNER [' || i || ']: ' || v_mensaje_send_partner;
			END IF;
			IF P_COOP_COND_LIST(i).RECEIVING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-RECEIVING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).RECEIVING_PARTNER, v_mensaje_receiv_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_PARTNER [' || i || ']: ' || v_mensaje_receiv_partner;
			END IF;	

		END LOOP;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un IIA 
	*/
	FUNCTION VALIDA_IIA(P_IIA IN PKG_IIAS.IIA, ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_fechas VARCHAR2(2000);
		v_mensaje_coop_cond VARCHAR2(2000);
	BEGIN

		IF VALIDA_FECHAS_IIA(P_IIA, v_mensaje_fechas) <> 0 THEN
			v_cod_retorno := -1;
			ERROR_MESSAGE := '
			IIA:' || v_mensaje_fechas;
		END IF;

        IF P_IIA.IIA_CODE IS NULL THEN
            ERROR_MESSAGE := '
			IIA: 
                -IIA_CODE';
        END IF;

		IF P_IIA.COOPERATION_CONDITION_LIST IS NULL OR P_IIA.COOPERATION_CONDITION_LIST.COUNT = 0 THEN 
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:';
		ELSIF VALIDA_COOP_COND_IIA(P_IIA.COOPERATION_CONDITION_LIST, v_mensaje_coop_cond) <> 0 THEN
			v_cod_retorno := -1;
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:' || v_mensaje_coop_cond;
		END IF;


		IF v_cod_retorno <> 0 THEN
			ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/*
		Borra las subject areas asociadas a una condicion de cooperacion
	*/
	PROCEDURE BORRA_COOP_COND_SUBAREA_LANSKI(P_COOP_COND_ID IN VARCHAR2) AS
		CURSOR c_sub_area(p_coop_cond_id IN VARCHAR2) IS 
		SELECT ID, ISCED_CODE
		FROM EWPCV_COOPCOND_SUBAR_LANSKIL 
		WHERE COOPERATION_CONDITION_ID = p_coop_cond_id;

	BEGIN

		FOR rec IN c_sub_area(P_COOP_COND_ID) 
		LOOP
			DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = rec.ID;
			DELETE EWPCV_SUBJECT_AREA WHERE ID = rec.ISCED_CODE;
		END LOOP;
	END;

	/*
		Borra un IIA PARTNER y todos sus contactos asociados.
	*/
	PROCEDURE BORRA_IIA_PARTNER(P_IIA_PARTNER_ID IN VARCHAR2) AS
		v_s_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CONTACTS_ID
			FROM EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = p_id;
		CURSOR c_s(p_id IN VARCHAR2) IS 
			SELECT SIGNER_PERSON_CONTACT_ID
			FROM EWPCV_IIA_PARTNER
			WHERE ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_PARTNER_ID)
		LOOP 
			DELETE FROM EWPCV_IIA_PARTNER_CONTACTS 
				WHERE CONTACTS_ID = REC.CONTACTS_ID
				AND IIA_PARTNER_ID = P_IIA_PARTNER_ID;
			PKG_COMMON.BORRA_CONTACT(rec.CONTACTS_ID);
		END LOOP;

		OPEN c_s(P_IIA_PARTNER_ID);
		FETCH c_s into v_s_id;
		CLOSE c_s;

		DELETE FROM EWPCV_IIA_PARTNER WHERE ID = P_IIA_PARTNER_ID;
		PKG_COMMON.BORRA_CONTACT(v_s_id);

		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_PARTNER_ID;
	END;

	PROCEDURE BORRA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR2) AS 
		v_mn_count NUMBER;
		v_mt_count NUMBER;
		v_md_count NUMBER;
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM EWPCV_COOPERATION_CONDITION 
			WHERE IIA_ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_ID)
		LOOP 
			DELETE FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = rec.ID;
			BORRA_COOP_COND_SUBAREA_LANSKI(rec.ID);
			DELETE FROM EWPCV_COOPERATION_CONDITION WHERE ID = rec.ID;
			DELETE FROM EWPCV_MOBILITY_NUMBER WHERE ID = rec.MOBILITY_NUMBER_ID;

			DELETE FROM EWPCV_DURATION WHERE ID = rec.DURATION_ID;
			BORRA_IIA_PARTNER(rec.RECEIVING_PARTNER_ID);
			BORRA_IIA_PARTNER(rec.SENDING_PARTNER_ID);
		END LOOP;
	END;

	/*
		Borra un IIAS y todas sus condiciones de cooperacion
	*/
	PROCEDURE BORRA_IIA(P_IIA_ID IN VARCHAR2) AS
	BEGIN
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_ID;
	END;


	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_IIA_PARTNER(P_IIA_PARTNER IN PARTNER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255);
	BEGIN
		v_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_IIA_PARTNER.INSTITUTION);
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_IIA_PARTNER.SIGNER_PERSON);
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
				VALUES (v_id, P_IIA_PARTNER.INSTITUTION.INSTITUTION_ID, v_ounit_id, v_c_id, P_IIA_PARTNER.SIGNING_DATE);
		RETURN v_id;
	END;

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_PARTNER_CONTACTS(P_PARTNER_ID IN VARCHAR2, P_PARTNER_CONTACTS EWP.CONTACT_PERSON_LIST) AS 
		v_c_p_id VARCHAR2(255);
	BEGIN
		IF P_PARTNER_CONTACTS IS NOT NULL AND P_PARTNER_CONTACTS.COUNT > 0 THEN 
			FOR i IN P_PARTNER_CONTACTS.FIRST .. P_PARTNER_CONTACTS.LAST
			LOOP
				v_c_p_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_PARTNER_CONTACTS(i));
				INSERT INTO EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (P_PARTNER_ID, v_c_p_id);
			END LOOP;
		END IF;
	END;

	/*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_SUBAREA_LANSKIL(P_COP_COND_ID IN VARCHAR2, P_SUBAREA_LANSKIL_LIST SUBJECT_AREA_LANGUAGE_LIST) AS 
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN
		IF P_SUBAREA_LANSKIL_LIST IS NOT NULL AND P_SUBAREA_LANSKIL_LIST.COUNT > 0 THEN 
			FOR i IN P_SUBAREA_LANSKIL_LIST.FIRST .. P_SUBAREA_LANSKIL_LIST.LAST
			LOOP
			v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_SUBAREA_LANSKIL_LIST(i).SUBJECT_AREA);
			v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_SUBAREA_LANSKIL_LIST(i).LANGUAGE_SKILL);
			INSERT INTO EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(EWP.GENERATE_UUID(), P_COP_COND_ID, v_isced_code, v_lang_skill_id);
			END LOOP;
		END IF;
	END;

    /*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN MOBILITY_TYPE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR, p_group IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER(p_group);
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NOT NULL OR  P_MOBILITY_TYPE.MOBILITY_GROUP IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		END IF;
		RETURN v_id;
	END;


     /*
		Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_DURATION(P_DURATION IN PKG_IIAS.DURATION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (v_id, P_DURATION.NUMBER_DURATION, P_DURATION.UNIT);
		RETURN v_id;
	END;

     /*
		Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_MOBILITY_NUMBER(P_MOB_NUMBER IN VARCHAR) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (v_id, P_MOB_NUMBER);
		RETURN v_id;
	END;

	/*
		Inserta los EQUF LEVELS asociados a una condicion de cooperacion
	*/
	PROCEDURE INSERTA_EQFLEVELS(P_COOP_COND_ID IN VARCHAR2, P_COOP_COND IN COOPERATION_CONDITION) AS
	BEGIN
		IF P_COOP_COND.EQF_LEVEL_LIST IS NOT NULL AND P_COOP_COND.EQF_LEVEL_LIST .COUNT > 0 THEN 
			FOR i IN P_COOP_COND.EQF_LEVEL_LIST .FIRST .. P_COOP_COND.EQF_LEVEL_LIST .LAST
			LOOP
				INSERT INTO EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (P_COOP_COND_ID,P_COOP_COND.EQF_LEVEL_LIST(i));
			END LOOP;
		END IF;
	END;

	/*
		Persiste una lista de condiciones de cooperacion asociadas a un IIAS
	*/
	PROCEDURE INSERTA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR, P_COOP_COND IN COOPERATION_CONDITION_LIST) AS 
		v_s_iia_partner_id VARCHAR2(255);
        v_s_c_p_id VARCHAR2(255);
        v_r_iia_partner_id VARCHAR2(255);
        v_r_c_p_id VARCHAR2(255);
        v_mobility_type_id VARCHAR2(255);
        v_duration_id VARCHAR2(255);
        v_mob_number_id VARCHAR2(255);
		v_cc_id VARCHAR2(255);
	BEGIN 
		FOR i IN P_COOP_COND.FIRST .. P_COOP_COND.LAST 
		LOOP
			v_s_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).SENDING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).SENDING_PARTNER.PARTNER_CONTACTS);

			v_r_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).RECEIVING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).RECEIVING_PARTNER.PARTNER_CONTACTS);   

			v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_COOP_COND(i).MOBILITY_TYPE);
			v_duration_id := INSERTA_DURATION(P_COOP_COND(i).COP_COND_DURATION);
			v_mob_number_id := INSERTA_MOBILITY_NUMBER(P_COOP_COND(i).MOBILITY_NUMBER);

			v_cc_id :=	EWP.GENERATE_UUID();
			INSERT INTO EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO) 
				VALUES (v_cc_id, P_COOP_COND(i).END_DATE, P_COOP_COND(i).START_DATE, v_duration_id,
					v_mob_number_id, v_mobility_type_id, v_r_iia_partner_id, v_s_iia_partner_id, P_IIA_ID, P_COOP_COND(i).OTHER_INFO );

			INSERTA_SUBAREA_LANSKIL(v_cc_id, P_COOP_COND(i).SUBJECT_AREA_LANGUAGE_LIST);
			INSERTA_EQFLEVELS(v_cc_id,P_COOP_COND(i));
		END LOOP;
	END;

	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_IIA(P_IIA IN PKG_IIAS.IIA) RETURN VARCHAR2 AS
		v_iia_id VARCHAR2(255);
	BEGIN

        v_iia_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_IIA (ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE) 
            VALUES (v_iia_id, P_IIA.END_DATE, P_IIA.IIA_CODE, CURRENT_DATE, P_IIA.START_DATE);

		INSERTA_COOP_CONDITIONS(v_iia_id,P_IIA.COOPERATION_CONDITION_LIST);

        RETURN v_iia_id;
	END;


	FUNCTION OBTEN_NOTIFIER_HEI(P_IIA_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS 
		v_notifier_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT DISTINCT(P.INSTITUTION_ID)
			FROM EWPCV_IIA_PARTNER P 
				INNER JOIN EWPCV_COOPERATION_CONDITION CC
					ON CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID
			WHERE CC.IIA_ID = p_id;
	BEGIN 
		OPEN c(P_IIA_ID);
		FETCH c INTO v_notifier_hei;
		WHILE c%FOUND AND UPPER(v_notifier_hei) = UPPER(P_HEI_TO_NOTIFY)
		LOOP
			FETCH c INTO v_notifier_hei;
		END LOOP;
		CLOSE c;
		RETURN v_notifier_hei;
	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	PROCEDURE ACTUALIZA_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA) AS
	BEGIN

 		UPDATE EWPCV_IIA SET START_DATE = P_IIA.START_DATE, END_DATE = P_IIA.END_DATE, IIA_CODE = P_IIA.IIA_CODE, PDF = P_IIA.PDF, MODIFY_DATE = SYSDATE
			WHERE ID = P_IIA_ID;
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		INSERTA_COOP_CONDITIONS(P_IIA_ID, P_IIA.COOPERATION_CONDITION_LIST);

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA,  P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			P_IIA_ID := INSERTA_IIA(P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);

		END IF;
        COMMIT;
		RETURN v_cod_retorno;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END INSERT_IIA; 	

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS			
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count_iia NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			ACTUALIZA_IIA(P_IIA_ID, P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END UPDATE_IIA; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_count_iia NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count_mobility NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_mobility FROM EWPCV_MOBILITY WHERE IIA_ID = P_IIA_ID;
		IF v_count_mobility > 0 THEN
			P_ERROR_MESSAGE := 'Existen movilidadtes asociadas al IIA';
			RETURN -1;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
		BORRA_IIA(P_IIA_ID);
		PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		return 0;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_IIA; 

    /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
    FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_count_iia NUMBER := 0;
        v_remote_iia_id VARCHAR2(255);
        v_remote_coop_cond_hash VARCHAR2(255);
        v_remote_iia_code VARCHAR2(255);
        CURSOR c(p_remote_id IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID, IIA.REMOTE_COP_COND_HASH, IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_remote_id;
    BEGIN
        IF P_OWN_IIA_ID IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA propio';
			RETURN -1;
		ELSIF P_ID_FROM_REMOTE_IIA IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA remoto';
			RETURN -1;
		END IF;

        SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_OWN_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha infromado como parámetro P_OWN_IIA_ID';
			RETURN -1;
		END IF;

        OPEN c(P_ID_FROM_REMOTE_IIA);
		FETCH c INTO v_remote_iia_id, v_remote_coop_cond_hash, v_remote_iia_code;
		CLOSE c;
		IF v_remote_iia_id IS NULL THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
			RETURN -1;
		END IF;
        --Bindeamos el IIA nuestro con el remoto
        UPDATE EWPCV_IIA SET REMOTE_IIA_ID = v_remote_iia_id, REMOTE_IIA_CODE = v_remote_iia_code
            WHERE ID = P_OWN_IIA_ID;
        COMMIT;
        return 0;

        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END BIND_IIA;

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_remote_iia_id VARCHAR2(255);
        v_notifier_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR c(p_internal_id_iia_copy IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_internal_id_iia_copy;
    BEGIN
        IF P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id interno de la copia del IIA remoto';
			RETURN -1;
		ELSIF P_HEI_ID_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el hei-id de la institución a notificar';
			RETURN -1;
		END IF;

        OPEN c(P_INTERNAL_ID_REMOTE_IIA_COPY);
		FETCH c INTO v_remote_iia_id;
		CLOSE c;

        v_notifier_hei := OBTEN_NOTIFIER_HEI(P_INTERNAL_ID_REMOTE_IIA_COPY, P_HEI_ID_TO_NOTIFY);
        v_id := EWP.GENERATE_UUID();  
        INSERT INTO EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI) 
						VALUES (v_id, 0, v_remote_iia_id, P_HEI_ID_TO_NOTIFY, SYSDATE, 4, 1, 0, 0, v_notifier_hei);
        COMMIT;
        return 0;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END APPROVE_IIA;

END PKG_IIAS;
/
INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.01.00', SYSDATE, '10_UPGRADE_v01.01.00');
COMMIT;