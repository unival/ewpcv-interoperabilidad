
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
create or replace FUNCTION EWP.EXTRAE_FIRMAS(P_FIRMA_ID IN VARCHAR2) RETURN VARCHAR2 AS 
v_lista VARCHAR2(4000 CHAR);
BEGIN
	select
	case when max(total_length) > 3996 then
		listagg(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || TO_CHAR("TIMESTAMP", 'YYYY-MM-DD"T"HH24:MI:SS') || '|:|' || SIGNER_APP, '|;|') within group (order by SIGNER_NAME)||
			'...'
	else
		listagg(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || TO_CHAR("TIMESTAMP", 'YYYY-MM-DD"T"HH24:MI:SS') || '|:|' || SIGNER_APP, '|;|') within group (order by SIGNER_NAME)
	end aux into v_lista
	from
	(
		select
			SIGNER_NAME AS SIGNER_NAME,
			SIGNER_POSITION AS SIGNER_POSITION,
			SIGNER_EMAIL AS SIGNER_EMAIL,
			"TIMESTAMP" AS "TIMESTAMP",
			SIGNER_APP AS SIGNER_APP,
			sum(length(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || TO_CHAR("TIMESTAMP", 'YYYY-MM-DD"T"HH24:MI:SS') || '|:|' || SIGNER_APP) + 3) over (order by SIGNER_NAME) running_length,
			sum(length(SIGNER_NAME || '|:|' || SIGNER_POSITION || '|:|' || SIGNER_EMAIL || '|:|' || TO_CHAR("TIMESTAMP", 'YYYY-MM-DD"T"HH24:MI:SS') || '|:|' || SIGNER_APP) + 3) over (order by SIGNER_NAME) total_length
		FROM EWP.EWPCV_SIGNATURE
		WHERE ID = P_FIRMA_ID
		order by SIGNER_NAME 
	)
	where running_length <= 3996;
	RETURN v_lista;
END;
/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/


/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '26_UPGRADE_v01.08.04');
COMMIT;
    
    
    
    
    
    
    
    
    
