/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
CREATE TABLE EWP.EWPCV_IIA_APPROVALS (
    ID NUMBER NOT NULL,
    LOCAL_IIA_ID VARCHAR2(255 char) NOT NULL,
    REMOTE_IIA_ID VARCHAR2(255 char) NOT NULL,
    IIA_API_VERSION VARCHAR2(255 char) NOT NULL,
    LOCAL_HASH VARCHAR2(255 char) NOT NULL,
    REMOTE_HASH VARCHAR2(255 char) NOT NULL,
    DATE_IIA_APPROVAL DATE NOT NULL,
    CONSTRAINT fk_iia_approval FOREIGN KEY (LOCAL_IIA_ID) REFERENCES EWP.EWPCV_IIA(ID),
	PRIMARY KEY (ID)
);

CREATE SEQUENCE EWP.SEC_EWPCV_IIA_APPROVALS start with 1 increment by 1;

ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION ADD IIA_APPROVAL_ID NUMBER;

ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION 
   ADD CONSTRAINT FK_CONDITIONS_APPROVAL_ID
   FOREIGN KEY (IIA_APPROVAL_ID) 
   REFERENCES EWP.EWPCV_IIA_APPROVALS;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_APPROVALS                 TO APLEWP;
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_APPROVALS                 TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
	CREATE OR REPLACE SYNONYM APLEWP.EWPCV_IIA_APPROVALS                      FOR EWP.EWPCV_IIA_APPROVALS;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.03.00', SYSDATE, '44_IIA_APPROVALS_v02.03.00');
COMMIT;