
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

	ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION ADD ORDER_INDEX NUMBER;

	CREATE SEQUENCE EWP.SEC_EWPCV_COOPERATION_CONDITION INCREMENT BY 1 MINVALUE 0 MAXVALUE 99999999999999 NOCYCLE NOCACHE NOORDER ;

	CREATE OR REPLACE TRIGGER EWP.TRIGGER_COOPERATION_CONDITION_ORDER_INDEX
	  BEFORE INSERT ON EWP.EWPCV_COOPERATION_CONDITION
	  FOR EACH ROW
	BEGIN
	  :new.ORDER_INDEX := SEC_EWPCV_COOPERATION_CONDITION.nextval;
	END;
	/
	--PLSQL Para actualizar la columna ORDER_INDEX de las condiciones de cooperacion existentes
	DECLARE
	  CURSOR c_coop_cond  IS
		SELECT 
			ID, ORDER_INDEX 
		FROM 
			EWP.EWPCV_COOPERATION_CONDITION ecc
		FOR UPDATE OF ORDER_INDEX;
	   
	BEGIN	
		FOR ccid IN c_coop_cond
		LOOP
			UPDATE EWP.EWPCV_COOPERATION_CONDITION cc SET ORDER_INDEX = (EWP.SEC_EWPCV_COOPERATION_CONDITION.nextval)
			WHERE cc.ID = ccid.ID;
		END LOOP; 
	END;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
/
INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.00.00', SYSDATE, '33_UPGRADE_FEATURE_MEJORA_ordenacion_cooperation_condition');
COMMIT;
