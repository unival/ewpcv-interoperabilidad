
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
	ALTER TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL Modify ISCED_CODE NULL;
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/
create or replace FUNCTION  EWP.GENERATE_UUID RETURN VARCHAR2 AS 
 v_return VARCHAR2(255 CHAR);
BEGIN
	SELECT regexp_replace(rawtohex(sys_guid()), 
    '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})', '\1-\2-\3-\4-\5') 
    INTO v_return FROM DUAL;
    return LOWER(v_return);
END;
/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
/*
	Obtiene la informacion de los learning agreements. Se empleará para identificar que learning agreements se han modificado.
	La informacion del estudiante en esta tabla solo estará presente cuando suponga un cambio sobre la informacion original que figura en la movilidad.
*/
CREATE OR REPLACE view EWP.LEARNING_AGREEMENT_VIEW AS SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	MP.GLOBAL_ID                  			   AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
LEFT JOIN EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS;



COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME|:|LANG|;|...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC|:|LANG|;|...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL|:|LANG|;|...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1|;|...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_PHONE IS 'FORMATO: E164|:|EXTENSION_NUMBER|:|OTHER_FORMAT|;|...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1|;|...|;|ADDRES LINE4';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER|:|BUILDING_NAME|:|STREET_NAME|:|UNIT|:|FLOOR|:|POST_OFFICE_BOX|:|DELIVERY_POINT_CODES';


/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
--PKG COMON
/
create or replace PACKAGE EWP.PKG_COMMON AS 
	/* 
		Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
	*/
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2);

	/* 
		Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
	*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2);
    
    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2);

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2);

    /*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2;
    
	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST);

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2;

	/*
		Inserta datos de persona independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2;

	/*
		Inserta un telefono de contacto independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2;

    /*
		Inserta una direccion independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2;
    
    /*
		Inserta datos de contacto
	*/
    FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2;

    /*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2;

    /*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2;    

    /*
		Inserta un nivel de idioma independientemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2;

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) ;

    /*
        Utilidad para pintar clobs por consola
    */
    procedure print_clob( p_clob in clob );


END PKG_COMMON;
/
create or replace PACKAGE BODY EWP.PKG_COMMON AS 
	
	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	
	/* Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) */
	PROCEDURE BORRA_CONTACT_DETAILS(P_ID IN VARCHAR2) AS
		v_phone_id VARCHAR2(255 CHAR);
        v_fax_id VARCHAR2(255 CHAR);
		v_mailing_id VARCHAR2(255 CHAR);
		v_street_id VARCHAR2(255 CHAR);
		CURSOR c_contact_detail(p_contact_id IN VARCHAR2) IS 
			SELECT PHONE_NUMBER, MAILING_ADDRESS, STREET_ADDRESS, FAX_NUMBER
			FROM EWPCV_CONTACT_DETAILS
			WHERE ID = p_contact_id;
		CURSOR c_urls(p_contact_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_CONTACT_URL
			WHERE CONTACT_DETAILS_ID = p_contact_id;
	BEGIN
		OPEN c_contact_detail(P_ID);
		FETCH c_contact_detail INTO v_phone_id, v_mailing_id, v_street_id, v_fax_id;
		CLOSE c_contact_detail;

		FOR url_rec IN c_urls(P_ID)
		LOOP 
			DELETE FROM EWPCV_CONTACT_URL WHERE URL_ID = url_rec.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = url_rec.URL_ID;
		END LOOP;
		DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = P_ID;
  
		DELETE FROM EWPCV_CONTACT_DETAILS WHERE ID = P_ID;

		DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_phone_id;
        
        DELETE FROM EWPCV_PHONE_NUMBER WHERE ID = v_fax_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_mailing_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_mailing_id;

		DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = v_street_id;
		DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = v_street_id;

		DELETE FROM EWPCV_FLEXIBLE_ADDRESS WHERE ID = v_street_id;
	END;

	/* Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)*/
	PROCEDURE BORRA_CONTACT(P_ID IN VARCHAR2) AS
		v_person_id VARCHAR2(255 CHAR);
		v_details_id VARCHAR2(255 CHAR);
		CURSOR c_contact(p_contact_id IN VARCHAR2) IS 
			SELECT PERSON_ID, CONTACT_DETAILS_ID
			FROM EWPCV_CONTACT
			WHERE ID = p_contact_id;
		CURSOR c_names(p_contact_id IN VARCHAR2) IS 
			SELECT NAME_ID
			FROM EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = p_contact_id;
		CURSOR c_descs(p_contact_id IN VARCHAR2) IS 
			SELECT DESCRIPTION_ID
			FROM EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = p_contact_id;
	BEGIN
		IF P_ID IS NOT NULL THEN 
			OPEN c_contact(P_ID);
			FETCH c_contact INTO v_person_id, v_details_id;
			CLOSE c_contact;
			FOR name_rec IN c_names(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_NAME WHERE NAME_ID = name_rec.NAME_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = name_rec.NAME_ID;
			END LOOP;
			FOR desc_rec IN c_descs(P_ID)
			LOOP 
				DELETE FROM EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = desc_rec.DESCRIPTION_ID;
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = desc_rec.DESCRIPTION_ID;
			END LOOP;
			DELETE FROM EWPCV_CONTACT WHERE ID = P_ID;
			BORRA_CONTACT_DETAILS(v_details_id);
			DELETE FROM EWPCV_PERSON WHERE ID = v_person_id;
		END IF;
	END;
    
    /*
		Borra la lista de fact_sheet_url
	*/
	PROCEDURE BORRA_FACT_SHEET_URL(P_FACTSHEET_ID IN VARCHAR2) AS
        CURSOR c_fs_url(p_fs_id IN VARCHAR2) IS 
			SELECT URL_ID
			FROM EWPCV_FACT_SHEET_URL
			WHERE FACT_SHEET_ID = p_fs_id;
    BEGIN
        FOR fact_sheet_url IN c_fs_url(P_FACTSHEET_ID)
		LOOP 
			DELETE FROM EWPCV_FACT_SHEET_URL WHERE URL_ID = fact_sheet_url.URL_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = fact_sheet_url.URL_ID;
		END LOOP; 
    END;

	/*
		Borra un academic term
	*/
	PROCEDURE BORRA_ACADEMIC_TERM(P_ID IN VARCHAR2) AS
		v_ay_id VARCHAR2(255 CHAR);
		v_ay_count NUMBER;
		CURSOR c_ay(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_YEAR_ID
			FROM EWPCV_ACADEMIC_TERM
			WHERE ID = p_id;

		CURSOR c_n(p_id IN VARCHAR2) IS 
			SELECT DISP_NAME_ID
			FROM EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = p_id;
	BEGIN

		FOR rec IN c_n(P_ID) 
		LOOP
			DELETE FROM EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = rec.DISP_NAME_ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.DISP_NAME_ID;
		END LOOP;

		OPEN c_ay(P_ID);
		FETCH c_ay INTO v_ay_id;
		CLOSE c_ay;
		DELETE FROM EWPCV_ACADEMIC_TERM WHERE ID = P_ID;
		SELECT COUNT(1) INTO v_ay_count FROM EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = v_ay_id;
		IF v_ay_count = 0 THEN 
			DELETE FROM EWPCV_ACADEMIC_YEAR WHERE ID = v_ay_id;
		END IF;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_LANGUAGE_ITEM(P_LANGUAGE_ITEM IN EWP.LANGUAGE_ITEM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_LANGUAGE_ITEM.LANG IS NOT NULL OR  P_LANGUAGE_ITEM.TEXT IS NOT NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (v_id, P_LANGUAGE_ITEM.LANG, P_LANGUAGE_ITEM.TEXT); 
		END IF;

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de institucion
	*/
	PROCEDURE INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID IN VARCHAR2, P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_inst_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_INSTITUTION_NAME INA ON LAIT.ID = INA.NAME_ID 
			WHERE INA.INSTITUTION_ID = p_inst_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_INSTITUTION_NAMES IS NOT NULL AND P_INSTITUTION_NAMES.COUNT > 0 THEN
			FOR i IN P_INSTITUTION_NAMES.FIRST .. P_INSTITUTION_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_INSTITUTION_ID, P_INSTITUTION_NAMES(i).LANG, P_INSTITUTION_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_INSTITUTION_NAMES(i));
                    INSERT INTO EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES (v_lang_item_id, P_INSTITUTION_ID);
				END IF; 
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_ABREVIATION IN VARCHAR2, P_LOGO_URL IN VARCHAR2,
		 P_FACTSHEET_ID IN VARCHAR2,  P_INSTITUTION_NAMES IN EWP.LANGUAGE_ITEM_LIST, P_PRIM_CONT_DETAIL_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_INSTITUTION 
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_id);
	BEGIN

		OPEN exist_cursor(P_INSTITUTION_ID) ;
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID) VALUES (v_id, LOWER(P_INSTITUTION_ID), P_ABREVIATION, P_LOGO_URL, P_FACTSHEET_ID, P_PRIM_CONT_DETAIL_ID); 
		ELSIF P_ABREVIATION IS NOT NULL OR P_LOGO_URL IS NOT NULL OR P_FACTSHEET_ID IS NOT NULL THEN
			UPDATE  EWPCV_INSTITUTION SET  ABBREVIATION = P_ABREVIATION, LOGO_URL = P_LOGO_URL, FACT_SHEET = P_FACTSHEET_ID WHERE ID = v_id; 
		END IF;

		INSERTA_INSTITUTION_NAMES(v_id, P_INSTITUTION_NAMES);

		RETURN v_id;
	END;

	/*
		Persiste una lista de nombres de organizacion
	*/
	PROCEDURE INSERTA_OUNIT_NAMES(P_OUNIT_ID IN VARCHAR2, P_ORGANIZATION_UNIT_NAMES IN EWP.LANGUAGE_ITEM_LIST) AS
		v_lang_item_id VARCHAR2(255);
		v_count NUMBER;
		CURSOR exist_lang_it_cursor(p_ounit_id IN VARCHAR2, p_lang IN VARCHAR2, p_text IN VARCHAR2) IS SELECT ID
			FROM EWPCV_LANGUAGE_ITEM LAIT
			INNER JOIN EWPCV_ORGANIZATION_UNIT_NAME ONA ON LAIT.ID = ONA.NAME_ID 
			WHERE ONA.ORGANIZATION_UNIT_ID = p_ounit_id
			AND UPPER(LAIT.LANG) = UPPER(p_lang)
			AND UPPER(LAIT.TEXT) = UPPER(p_text);
	BEGIN
		IF P_ORGANIZATION_UNIT_NAMES IS NOT NULL AND P_ORGANIZATION_UNIT_NAMES.COUNT > 0 THEN
			FOR i IN P_ORGANIZATION_UNIT_NAMES.FIRST .. P_ORGANIZATION_UNIT_NAMES.LAST 
			LOOP
				OPEN exist_lang_it_cursor(P_OUNIT_ID, P_ORGANIZATION_UNIT_NAMES(i).LANG,P_ORGANIZATION_UNIT_NAMES(i).TEXT) ;
				FETCH exist_lang_it_cursor INTO v_lang_item_id;
				CLOSE exist_lang_it_cursor;
				IF v_lang_item_id IS NULL THEN 
					v_lang_item_id := INSERTA_LANGUAGE_ITEM(P_ORGANIZATION_UNIT_NAMES(i));
					INSERT INTO EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES (v_lang_item_id, P_OUNIT_ID);
				END IF;       
				v_lang_item_id := null;
			END LOOP;
		END IF;
	END;

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_hei_id IN VARCHAR, p_code IN VARCHAR) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_code)
			AND UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id);

	BEGIN
	    IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			OPEN exist_cursor(P_INSTITUTION.INSTITUTION_ID, P_INSTITUTION.ORGANIZATION_UNIT_CODE);
			FETCH exist_cursor INTO v_id;
			CLOSE exist_cursor;

			IF  v_id IS NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (v_id, P_INSTITUTION.ORGANIZATION_UNIT_CODE); 
			END IF;

			INSERTA_OUNIT_NAMES(v_id, P_INSTITUTION.ORGANIZATION_UNIT_NAME);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
	*/
	FUNCTION INSERTA_INST_OUNIT(P_INSTITUTION IN EWP.INSTITUTION) RETURN VARCHAR2 AS
        v_inst_id VARCHAR2(255);
        v_ou_id VARCHAR2(255);
        v_count VARCHAR2(255);
    BEGIN
        v_inst_id := INSERTA_INSTITUTION(P_INSTITUTION.INSTITUTION_ID, null, null, null, P_INSTITUTION.INSTITUTION_NAME, null);
        IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            v_ou_id := INSERTA_OUNIT(P_INSTITUTION);
            SELECT COUNT(1) INTO v_count FROM EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = v_ou_id;
            IF v_count = 0 THEN 
                INSERT INTO EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (v_inst_id, v_ou_id);
            END IF;
        END IF;
        RETURN v_ou_id;

 

    END;

	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_PERSONA(P_PERSON IN EWP.CONTACT_PERSON) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PERSON.GIVEN_NAME IS NOT NULL 
			OR P_PERSON.FAMILY_NAME IS NOT NULL 
			OR P_PERSON.BIRTH_DATE IS NOT NULL 
			OR P_PERSON.CITIZENSHIP IS NOT NULL 
			OR P_PERSON.GENDER IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
					VALUES (v_id, P_PERSON.GIVEN_NAME, P_PERSON.FAMILY_NAME, P_PERSON.CITIZENSHIP, P_PERSON.BIRTH_DATE, P_PERSON.GENDER);
		END IF;
		return v_id;
	END;

	/*
		Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_TELEFONO(P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_PHONE.E164 IS NOT NULL 
			OR P_PHONE.EXTENSION_NUMBER IS NOT NULL 
			OR P_PHONE.OTHER_FORMAT IS NOT NULL THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
					VALUES (v_id, P_PHONE.E164, P_PHONE.EXTENSION_NUMBER, P_PHONE.OTHER_FORMAT);
		END IF;
		return v_id;
	END;

	/*
		Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	
	FUNCTION INSERTA_FLEXIBLE_ADDRES(P_ADDRESS IN EWP.FLEXIBLE_ADDRESS) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		IF P_ADDRESS.POSTAL_CODE IS NOT NULL 
			OR P_ADDRESS.LOCALITY IS NOT NULL 
			OR P_ADDRESS.REGION IS NOT NULL
			OR P_ADDRESS.COUNTRY IS NOT NULL THEN 
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (v_id, P_ADDRESS.BUILDING_NUMBER, P_ADDRESS.BUILDING_NAME, P_ADDRESS.COUNTRY,
					P_ADDRESS.BUILDING_FLOOR, P_ADDRESS.LOCALITY, P_ADDRESS.POST_OFFICE_BOX, P_ADDRESS.POSTAL_CODE,
					P_ADDRESS.REGION, P_ADDRESS.STREET_NAME,P_ADDRESS.UNIT );

				IF P_ADDRESS.RECIPIENT_NAMES IS NOT NULL AND P_ADDRESS.RECIPIENT_NAMES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.RECIPIENT_NAMES.FIRST .. P_ADDRESS.RECIPIENT_NAMES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) VALUES (v_id, P_ADDRESS.RECIPIENT_NAMES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.ADDRESS_LINES IS NOT NULL AND P_ADDRESS.ADDRESS_LINES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.ADDRESS_LINES.FIRST .. P_ADDRESS.ADDRESS_LINES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE) VALUES (v_id, P_ADDRESS.ADDRESS_LINES(i));
					END LOOP;
				END IF;

				IF P_ADDRESS.DELIVERY_POINT_CODES IS NOT NULL AND P_ADDRESS.DELIVERY_POINT_CODES.COUNT > 0 THEN
					FOR i IN P_ADDRESS.DELIVERY_POINT_CODES.FIRST .. P_ADDRESS.DELIVERY_POINT_CODES.LAST 
					LOOP
						INSERT INTO EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE) VALUES (v_id, P_ADDRESS.DELIVERY_POINT_CODES(i));
					END LOOP;
				END IF;
		END IF;
		return v_id;
	END;

	/*
		Inserta detalles de contacto.
	*/
	FUNCTION INSERTA_CONTACT_DETAILS(P_CONTACT IN EWP.CONTACT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
		v_f_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
        v_ma_id VARCHAR2(255);
	BEGIN
		v_p_id:= INSERTA_TELEFONO(P_CONTACT.PHONE);
		v_f_id:= INSERTA_TELEFONO(P_CONTACT.FAX);
		v_a_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.STREET_ADDRESS);
        v_ma_id:= INSERTA_FLEXIBLE_ADDRES(P_CONTACT.MAILING_ADDRESS);

		IF v_p_id IS NOT NULL 
			OR v_a_id IS NOT NULL 
			OR (P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0) 
			OR (P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0) THEN
				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT_DETAILS (ID, FAX_NUMBER, PHONE_NUMBER, STREET_ADDRESS, MAILING_ADDRESS) VALUES (v_id, v_f_id, v_p_id, v_a_id, v_ma_id);

				IF P_CONTACT.EMAIL IS NOT NULL AND P_CONTACT.EMAIL.COUNT > 0 THEN
					FOR i IN P_CONTACT.EMAIL.FIRST .. P_CONTACT.EMAIL.LAST 
					LOOP
						INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (v_id, P_CONTACT.EMAIL(i));
					END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_URL IS NOT NULL AND P_CONTACT.CONTACT_URL.COUNT > 0 THEN
					FOR i IN P_CONTACT.CONTACT_URL.FIRST .. P_CONTACT.CONTACT_URL.LAST 
					LOOP
						v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_URL(i));
						INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (v_id, v_li_id);
					END LOOP;
				END IF;
		END IF;

		return v_id;
	END;
    
    /*
		Inserta datos de contacto y retorna id de contact detail id
	*/
	FUNCTION INSERTA_CONTACT(P_CONTACT IN EWP.CONTACT, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
		v_li_id VARCHAR2(255);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN

		v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT);

		IF v_c_id IS NOT NULL 
			OR (P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0) 
			OR (P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;
                
               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;

				v_id := EWP.GENERATE_UUID();
				INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT.CONTACT_ROLE, v_c_id, null);

				IF P_CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT.CONTACT_NAME.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_NAME.FIRST .. P_CONTACT.CONTACT_NAME.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_NAME(i));
							INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;

				IF P_CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
						FOR i IN P_CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT.CONTACT_DESCRIPTION.LAST 
						LOOP
							v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT.CONTACT_DESCRIPTION(i));
							INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
						END LOOP;
				END IF;
		END IF;	
		RETURN v_c_id;
	END;

	/*
        Inserta datos de contacto y de persona 
    */
    FUNCTION INSERTA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_INST_ID IN VARCHAR2, P_OUNIT_CODE IN VARCHAR2) RETURN VARCHAR2 AS
        v_id VARCHAR2(255);
        v_p_id VARCHAR2(255);
        v_c_id VARCHAR2(255);
        v_ounit_id VARCHAR2(255) := null;
        v_inst_id VARCHAR2(255) := null;
        v_li_id VARCHAR2(255);
        CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
            FROM EWPCV_ORGANIZATION_UNIT O 
            INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
            INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
            WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
            AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
    BEGIN

        v_p_id := INSERTA_PERSONA(P_CONTACT_PERSON);
        v_c_id := INSERTA_CONTACT_DETAILS(P_CONTACT_PERSON.CONTACT);

        IF v_p_id IS NOT NULL 
            OR v_c_id IS NOT NULL 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0) 
            OR (P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0) THEN 

                IF P_INST_ID IS NOT NULL AND P_OUNIT_CODE IS NULL THEN
                    v_inst_id := LOWER(P_INST_ID);
                END IF;
                
               IF P_OUNIT_CODE IS NOT NULL THEN
                    OPEN ounit_cursor(P_INST_ID, P_OUNIT_CODE);
                    FETCH ounit_cursor INTO v_ounit_id;
                    CLOSE ounit_cursor;
                END IF;
                
                
                v_id := EWP.GENERATE_UUID();
                INSERT INTO EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
                    VALUES (v_id, v_inst_id, v_ounit_id, P_CONTACT_PERSON.CONTACT.CONTACT_ROLE, v_c_id, v_p_id);

 

                IF P_CONTACT_PERSON.CONTACT.CONTACT_NAME IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_NAME.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_NAME.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_NAME.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_NAME(i));
                            INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;

 

                IF P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION IS NOT NULL AND P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.COUNT > 0 THEN
                        FOR i IN P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.FIRST .. P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION.LAST 
                        LOOP
                            v_li_id:= INSERTA_LANGUAGE_ITEM(P_CONTACT_PERSON.CONTACT.CONTACT_DESCRIPTION(i));
                            INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (v_id, v_li_id);
                        END LOOP;
                END IF;
        END IF;    
        RETURN v_id;
    END;

		/*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN

		IF P_SUBJECT_AREA.ISCED_CODE IS NOT NULL OR P_SUBJECT_AREA.ISCED_CLARIFICATION IS NOT NULL THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (v_id, P_SUBJECT_AREA.ISCED_CODE, P_SUBJECT_AREA.ISCED_CLARIFICATION);
		END IF;
		RETURN v_id;
	END;

	/*
		Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_lang IN VARCHAR, p_cefr IN VARCHAR) IS SELECT ID
			FROM EWPCV_LANGUAGE_SKILL
			WHERE UPPER("LANGUAGE") = UPPER(p_lang)
			AND  UPPER(CEFR_LEVEL) = UPPER(p_cefr);
	BEGIN
		OPEN exist_cursor(P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL );
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_LANGUAGE_SKILL.LANG IS NOT NULL OR  P_LANGUAGE_SKILL.CEFR_LEVEL IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (v_id, P_LANGUAGE_SKILL.LANG, P_LANGUAGE_SKILL.CEFR_LEVEL);
		END IF;
		RETURN v_id;
	END;

    procedure print_clob( p_clob in clob ) is
          v_offset number default 1;
          v_chunk_size number := 10000;
      begin
          loop
              exit when v_offset > dbms_lob.getlength(p_clob);
              dbms_output.put_line( dbms_lob.substr( p_clob, v_chunk_size, v_offset ) );
              v_offset := v_offset +  v_chunk_size;
          end loop;
      end print_clob;  

	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
	*/
	PROCEDURE INSERTA_NOTIFICATION(P_ELEMENT_ID IN VARCHAR2, P_TYPE IN NUMBER, P_NOTIFY_HEI IN VARCHAR2, P_NOTIFIER_HEI IN VARCHAR2) AS
	BEGIN
		INSERT INTO EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (EWP.GENERATE_UUID(),P_ELEMENT_ID, LOWER(P_NOTIFY_HEI), SYSDATE, P_TYPE, 1, 0, LOWER(P_NOTIFIER_HEI) );
	END;

END PKG_COMMON;
/
--PKG FACTSHEET
/
create or replace PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoÃ±o
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoÃ±o
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  

	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );

	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;



	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;

	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );

	-- FUNCIONES 

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_FACTSHEET;
/
create or replace PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-DECISION_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-TOR_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_AUTUM_TERM';
		END IF;

		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_SPRING_TERM';
		END IF;

		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_AUTUM_TERM';
		END IF;

		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_SPRING_TERM';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-EMAIL';
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-PHONE';
		END IF;

		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-URL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP

			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ADITIONAL_INFO'|| i || ':' ||v_mensaje_it;
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ACCESSIBILITY_REQUIREMENTS'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				REQUIREMENTS_INFO'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS 
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);       
	BEGIN
        OPEN exist_cursor_inst(P_FACTSHEET_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID';
        ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID: La institución no existe en el sistema.';
		END IF;

		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			FACTSHEET:' || v_mensaje_factsheet;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			APPLICATION_INFO:' || v_mensaje_app_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			HOUSING_INFO:' || v_mensaje_hou_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			VISA_INFO:' || v_mensaje_vis_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			INSURANCE_INFO:' || v_mensaje_ins_info;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ADDITIONAL_INFO:' || v_mensaje_add_info;
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ACCESSIBILITY_REQUIREMENTS:' || v_mensaje_acc_req;
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			REQUIREMENTS_INFO:' || v_mensaje_add_req;
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;


	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;

	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
		v_cd_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
		OPEN c(P_FACTSHEET_ID);
		FETCH c into v_cd_id;
		CLOSE c;

        UPDATE EWPCV_FACT_SHEET
        SET DECISION_WEEKS_LIMIT = null,
                TOR_WEEKS_LIMIT = null,
                NOMINATIONS_AUTUM_TERM = null,
                NOMINATIONS_SPRING_TERM = null,
                APPLICATION_AUTUM_TERM = null,
                APPLICATION_SPRING_TERM = null,
                CONTACT_DETAILS_ID = null
        WHERE ID = P_FACTSHEET_ID;

		PKG_COMMON.BORRA_CONTACT_DETAILS(v_cd_id);
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;

		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);

		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;

		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,LOWER(P_INF_TYPE),v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_FS_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

        IF P_FS_ID IS NULL THEN 
            v_factsheet_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
                VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
                    P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
        ELSE
            v_factsheet_id := P_FS_ID;
            UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT, 
                TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT,
                NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
                NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
                APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
                APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
                CONTACT_DETAILS_ID = v_contact_id
                WHERE ID = v_factsheet_id;
        END IF;
		
		RETURN v_factsheet_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);

		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, LOWER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;

	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_FS_ID IN VARCHAR2) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO, P_FS_ID);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME, null);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);

		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;

	END;


	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;

	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN

		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);

		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;


	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);

	BEGIN

		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;

		IF P_FACTSHEET.INSTITUTION_NAME IS NOT NULL AND P_FACTSHEET.INSTITUTION_NAME.COUNT > 0 THEN
			BORRA_NOMBRES(P_INSTITUTION_ID);
			PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		END IF;

		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);

		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);

		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
        v_fs_id VARCHAR2(255);
        v_fs_cd_id VARCHAR2(255);
        CURSOR c_fid(p_id IN VARCHAR2) IS 
		SELECT fs.id, fs.CONTACT_DETAILS_ID
		FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(p_id);
	BEGIN
        OPEN c_fid(P_FACTSHEET.INSTITUTION_ID);
        FETCH c_fid INTO v_fs_id, v_fs_cd_id;
        CLOSE c_fid;
        
		IF v_fs_cd_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET, v_fs_id);
		END IF;
		COMMIT;

		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;

		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;
/
--PKG IIAS
/
create or replace PACKAGE EWP.PKG_IIAS AS 

	-- TIPOS DE DATOS
	
	/*Datos del tipo de movilidad. Campos obligatorios marcados con *.
			MOBILITY_CATEGORY*		CategorÃ­a de la movilidad, puede ser  teaching, studies, training, 
			MOBILITY_GROUP*			Grupo en que se engloba la movilidad puede ser student o staff
	*/  
	TYPE MOBILITY_TYPE IS RECORD
	  (
		MOBILITY_CATEGORY			VARCHAR2(255 CHAR),
		MOBILITY_GROUP				VARCHAR(255 CHAR)
	  ); 

	/*Informacion sobre cada una de las partes del IIA. Campos obligatorios marcados con *.
			INSTITUTION	*				Institucion a la que representa el partner
			SIGNING_DATE				Fecha en la que esta parte firmo el acuerdo
			SIGNER_PERSON				Datos de la persona que firmo el acuerdo en representacion de esta parte
			PARTNER_CONTACTS			Lista de personas de contacto de esta parte del acuerdo
	*/  
	TYPE PARTNER IS RECORD
	  (
		INSTITUTION					EWP.INSTITUTION,
		SIGNING_DATE				DATE,
		SIGNER_PERSON				EWP.CONTACT_PERSON,
		PARTNER_CONTACTS			EWP.CONTACT_PERSON_LIST
	  );

	/*Informacion sobre las areas de ensenyanza y el idioma que se recomienda para cada una.
		Campos obligatorios marcados con *.
			SUBJECT_AREA	 			Areas de ensenyanza
			LANGUAGE_SKILL				Idioma que se recomienda para el area indicada
	*/  
	TYPE SUBJECT_AREA_LANGUAGE IS RECORD
	  (
		SUBJECT_AREA	 			EWP.SUBJECT_AREA,
		LANGUAGE_SKILL	 			EWP.LANGUAGE_SKILL
	  );

	/* Lista con todas ensenyanza asociadas a una condicion de cooperacion y el idioma que se recomienda para cada una. */  
	TYPE SUBJECT_AREA_LANGUAGE_LIST IS TABLE OF SUBJECT_AREA_LANGUAGE INDEX BY BINARY_INTEGER ;

	/*Informacion sobre la duracion de una condicion de cooperacion
		Campos obligatorios marcados con *.
			NUMBER_DURATION	 			Cantidad de las unidades indicadas que dura la condicion de cooperacion.
			UNIT	 					Unidades en que se expresa la duracion de la condicion de cooperacion.
										0:HOURS, 1:DAYS, 2:WEEKS, 3:MONTHS, 4:YEARS
	*/  
	TYPE DURATION IS RECORD
	  (
		NUMBER_DURATION	 			NUMBER(10,0),
		UNIT	 					NUMBER(1,0)
	  );	  

	/* Lista de niveles de estudios relacionado con la condicion de cooperacion  */  
	TYPE EQF_LEVEL_LIST IS TABLE OF NUMBER(3,0) INDEX BY BINARY_INTEGER;  

	/*Informacion sobre cada uno de las condiciones de cooperacion de un acuerdo interinstitucional. 
		START_DATE *				Fecha en que comienza la vigencia de la condicion de cooperacion
		END_DATE *					Fecha en que termina la vigencia de la condicion de cooperacion
		EQF_LEVEL_LIST *			Niveles de estudios contemplados en la condicion de cooperacion 
		MOBILITY_NUMBER	*			Numero de participantes maximo que se puede beneficiar de esta condicion de cooperacion
		MOBILITY_TYPE *				Tipo de movilidad, indica si esde estudiantes o profesores y la actividad a desarrollar
		RECEIVING_PARTNER *			Informacion sobre la institucion destino
		SENDING_PARTNER *			Informacion sobre la institucion origen
        SUBJECT_AREA_LANGUAGE_LIST*	Informacion sobre areas de aprendizaje e idiomas relacionados a la condicion de cooperacion
        COP_COND_DURATION			Duracion de la condicion de cooperacion
		OTHER_INFO					Inforamcion adicional sobre la condicion de cooperacion
		BLENDED*					Indica si la condicion de cooperacion permite el formato de movilidad mixta
	*/	
	TYPE COOPERATION_CONDITION IS RECORD
	  (
		START_DATE					DATE,
		END_DATE					DATE,
		EQF_LEVEL_LIST       		PKG_IIAS.EQF_LEVEL_LIST,
		MOBILITY_NUMBER				NUMBER(10,0),
		MOBILITY_TYPE				PKG_IIAS.MOBILITY_TYPE,
		RECEIVING_PARTNER			PKG_IIAS.PARTNER,
		SENDING_PARTNER				PKG_IIAS.PARTNER,
		SUBJECT_AREA_LANGUAGE_LIST	PKG_IIAS.SUBJECT_AREA_LANGUAGE_LIST,
		COP_COND_DURATION			PKG_IIAS.DURATION,
		OTHER_INFO					VARCHAR2(255 CHAR),
		BLENDED						NUMBER(1,0)
	  );

	/* Lista con las condiciones de cooperacion de un acuerdo interinstitucional. */  
	TYPE COOPERATION_CONDITION_LIST IS TABLE OF COOPERATION_CONDITION INDEX BY BINARY_INTEGER ;


	/* Objeto que modela un acuerdo interinstitucional. Campos obligatorios marcados con *.
			START_DATE 						Fecha en que comienza la vigencia del IIA
			END_DATE 						Fecha en que termina la vigencia del IIA
			IIA_CODE						Codigo del IIA
			COOPERATION_CONDITION_LIST * 	Lista de condiciones de coperacion del IIA
			PDF								Documento pdf
	*/
	TYPE IIA IS RECORD
	  (
		START_DATE						DATE,
		END_DATE						DATE,
		IIA_CODE						VARCHAR2(255 CHAR),
		COOPERATION_CONDITION_LIST 		PKG_IIAS.COOPERATION_CONDITION_LIST,
		PDF								BLOB
	  );

	-- FUNCIONES 

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA, P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

     /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parÃ¡metro el identificador propio y el identificador de la copia remota.
	*/
	FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

    /*  Genera un CNR de aceptaciÃ³n con el id del IIA remoto de la instituciÃ³n indicada.
        Este CNR desencadena todo el proceso de aceptaciÃ³n de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;

END PKG_IIAS;
/
create or replace PACKAGE BODY EWP.PKG_IIAS AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************

	/*
		Valida mobility type
	*/
	FUNCTION VALIDA_MOBILITY_TYPE(P_MOBILITY_TYPE IN PKG_IIAS.MOBILITY_TYPE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_CATEGORY) NOT IN ('TEACHING', 'STUDIES', 'TRAINING') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_CATEGORY: Valor no permitido (teaching, studies, training)';
		END IF;

		IF P_MOBILITY_TYPE.MOBILITY_GROUP IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP';
		ELSIF UPPER(P_MOBILITY_TYPE.MOBILITY_GROUP) NOT IN ('STUDENT', 'STAFF') THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_GROUP: Valor no permitido (student, staff)';
		END IF;

		RETURN v_cod_retorno;
	END;
    
    /*
		Valida el el ounit asociado a la INSTITUTION
	*/
    FUNCTION VALIDA_OUNIT(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN
        IF P_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_INSTITUTION.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN 
                v_cod_retorno := -1;
                P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                    -ORGANIZATION_UNIT_CODE: La organización asociada a esta institución no existe en el sistema.';
            END IF;
        END IF;
    
        RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto Institution
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id      VARCHAR2(255);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS 
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);   
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
		ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID: La institución no existe en el sistema.';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida el objecto PARTNER de las COOP_COND del IIA
	*/
	FUNCTION VALIDA_PARTNER (P_PARTNER IN PKG_IIAS.PARTNER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_partner VARCHAR2(2000);
	BEGIN
		IF P_PARTNER.INSTITUTION IS NULL THEN
			v_cod_retorno := -1;
			v_mensaje_partner := P_ERROR_MESSAGE || '
				INSTITUTION:';
		ELSIF VALIDA_INSTITUTION(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN		
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || v_mensaje_partner;
		ELSIF P_PARTNER.INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL AND VALIDA_OUNIT(P_PARTNER.INSTITUTION, v_mensaje_partner) <> 0 THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || v_mensaje_partner;
        END IF;
            

		RETURN v_cod_retorno;
	END;

	/*
		Valida las COOPERATION_CONDITION del IIA
	*/
	FUNCTION VALIDA_COOP_COND_IIA (P_COOP_COND_LIST IN PKG_IIAS.COOPERATION_CONDITION_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_send_partner VARCHAR2(2000);
		v_mensaje_receiv_partner VARCHAR2(2000);
		v_mensaje_mobility_type VARCHAR2(2000);
        v_is_error BOOLEAN;
	BEGIN
		FOR i IN P_COOP_COND_LIST.FIRST .. P_COOP_COND_LIST.LAST 
		LOOP
			IF P_COOP_COND_LIST(i).START_DATE IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-START_DATE [' || i || ']';
            END IF;
			IF P_COOP_COND_LIST(i).END_DATE IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-END_DATE [' || i || ']';
			END IF;
			IF P_COOP_COND_LIST(i).EQF_LEVEL_LIST IS NULL OR P_COOP_COND_LIST(i).EQF_LEVEL_LIST.COUNT = 0 
                AND UPPER(P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_GROUP) = 'STUDENT' 
                AND UPPER(P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY) = 'STUDIES' THEN       
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-EQF_LEVEL_LIST [' || i || ']';	
			END IF;
            IF P_COOP_COND_LIST(i).MOBILITY_NUMBER IS NULL THEN 
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_NUMBER [' || i || ']';	
			END IF;
			IF P_COOP_COND_LIST(i).MOBILITY_TYPE.MOBILITY_CATEGORY IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-MOBILITY_TYPE [' || i || ']';
			ELSIF VALIDA_MOBILITY_TYPE(P_COOP_COND_LIST(i).MOBILITY_TYPE, v_mensaje_mobility_type) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					MOBILITY_TYPE [' || i || ']: ' || v_mensaje_mobility_type;
			END IF;	
			IF P_COOP_COND_LIST(i).BLENDED IS NULL THEN 	
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-BLENDED [' || i || ']';
			END IF;	
			IF P_COOP_COND_LIST(i).SENDING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SENDING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).SENDING_PARTNER, v_mensaje_send_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDING_PARTNER [' || i || ']: ' || v_mensaje_send_partner;
			END IF;
			IF P_COOP_COND_LIST(i).RECEIVING_PARTNER.INSTITUTION IS NULL THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-RECEIVING_PARTNER [' || i || ']';
			ELSIF VALIDA_PARTNER(P_COOP_COND_LIST(i).RECEIVING_PARTNER, v_mensaje_receiv_partner) <> 0 THEN		
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_PARTNER [' || i || ']: ' || v_mensaje_receiv_partner;
			END IF;	

		END LOOP;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un IIA 
	*/
	FUNCTION VALIDA_IIA(P_IIA IN PKG_IIAS.IIA, ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_fechas VARCHAR2(2000);
		v_mensaje_coop_cond VARCHAR2(2000);
	BEGIN

        IF P_IIA.IIA_CODE IS NULL THEN
            ERROR_MESSAGE := '
			IIA: 
                -IIA_CODE';
        END IF;

		IF P_IIA.COOPERATION_CONDITION_LIST IS NULL OR P_IIA.COOPERATION_CONDITION_LIST.COUNT = 0 THEN 
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:';
		ELSIF VALIDA_COOP_COND_IIA(P_IIA.COOPERATION_CONDITION_LIST, v_mensaje_coop_cond) <> 0 THEN
			v_cod_retorno := -1;
			ERROR_MESSAGE := ERROR_MESSAGE || '
			COOP_COND:' || v_mensaje_coop_cond;
		END IF;


		IF v_cod_retorno <> 0 THEN
			ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/*
		Borra las subject areas asociadas a una condicion de cooperacion
	*/
	PROCEDURE BORRA_COOP_COND_SUBAREA_LANSKI(P_COOP_COND_ID IN VARCHAR2) AS
		CURSOR c_sub_area(p_coop_cond_id IN VARCHAR2) IS 
		SELECT ID, ISCED_CODE
		FROM EWPCV_COOPCOND_SUBAR_LANSKIL 
		WHERE COOPERATION_CONDITION_ID = p_coop_cond_id;

	BEGIN

		FOR rec IN c_sub_area(P_COOP_COND_ID) 
		LOOP
			DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = rec.ID;
			DELETE EWPCV_SUBJECT_AREA WHERE ID = rec.ISCED_CODE;
		END LOOP;
	END;

	/*
		Borra un IIA PARTNER y todos sus contactos asociados.
	*/
	PROCEDURE BORRA_IIA_PARTNER(P_IIA_PARTNER_ID IN VARCHAR2) AS
		v_s_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CONTACTS_ID
			FROM EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = p_id;
		CURSOR c_s(p_id IN VARCHAR2) IS 
			SELECT SIGNER_PERSON_CONTACT_ID
			FROM EWPCV_IIA_PARTNER
			WHERE ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_PARTNER_ID)
		LOOP 
			DELETE FROM EWPCV_IIA_PARTNER_CONTACTS 
				WHERE CONTACTS_ID = REC.CONTACTS_ID
				AND IIA_PARTNER_ID = P_IIA_PARTNER_ID;
			PKG_COMMON.BORRA_CONTACT(rec.CONTACTS_ID);
		END LOOP;

		OPEN c_s(P_IIA_PARTNER_ID);
		FETCH c_s into v_s_id;
		CLOSE c_s;

		DELETE FROM EWPCV_IIA_PARTNER WHERE ID = P_IIA_PARTNER_ID;
		PKG_COMMON.BORRA_CONTACT(v_s_id);

		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_PARTNER_ID;
	END;

	PROCEDURE BORRA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR2) AS 
		v_mn_count NUMBER;
		v_mt_count NUMBER;
		v_md_count NUMBER;
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM EWPCV_COOPERATION_CONDITION 
			WHERE IIA_ID = p_id;
	BEGIN

		FOR rec IN c(P_IIA_ID)
		LOOP 
			DELETE FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = rec.ID;
			BORRA_COOP_COND_SUBAREA_LANSKI(rec.ID);
			DELETE FROM EWPCV_COOPERATION_CONDITION WHERE ID = rec.ID;
			DELETE FROM EWPCV_MOBILITY_NUMBER WHERE ID = rec.MOBILITY_NUMBER_ID;

			DELETE FROM EWPCV_DURATION WHERE ID = rec.DURATION_ID;
			BORRA_IIA_PARTNER(rec.RECEIVING_PARTNER_ID);
			BORRA_IIA_PARTNER(rec.SENDING_PARTNER_ID);
		END LOOP;
	END;

	/*
		Borra un IIAS y todas sus condiciones de cooperacion
	*/
	PROCEDURE BORRA_IIA(P_IIA_ID IN VARCHAR2) AS
	BEGIN
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		DELETE FROM EWPCV_IIA WHERE ID = P_IIA_ID;
	END;


	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	FUNCTION INSERTA_IIA_PARTNER(P_IIA_PARTNER IN PARTNER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
		v_ounit_id VARCHAR2(255);
	BEGIN
		v_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_IIA_PARTNER.INSTITUTION);
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_IIA_PARTNER.SIGNER_PERSON, null, null);
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
				VALUES (v_id, LOWER(P_IIA_PARTNER.INSTITUTION.INSTITUTION_ID), v_ounit_id, v_c_id, P_IIA_PARTNER.SIGNING_DATE);
		RETURN v_id;
	END;

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_PARTNER_CONTACTS(P_PARTNER_ID IN VARCHAR2, P_PARTNER_CONTACTS EWP.CONTACT_PERSON_LIST) AS 
		v_c_p_id VARCHAR2(255);
	BEGIN
		IF P_PARTNER_CONTACTS IS NOT NULL AND P_PARTNER_CONTACTS.COUNT > 0 THEN 
			FOR i IN P_PARTNER_CONTACTS.FIRST .. P_PARTNER_CONTACTS.LAST
			LOOP
				v_c_p_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_PARTNER_CONTACTS(i), null, null);
				INSERT INTO EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (P_PARTNER_ID, v_c_p_id);
			END LOOP;
		END IF;
	END;

	/*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
	PROCEDURE INSERTA_SUBAREA_LANSKIL(P_COP_COND_ID IN VARCHAR2, P_SUBAREA_LANSKIL_LIST SUBJECT_AREA_LANGUAGE_LIST) AS 
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN
		IF P_SUBAREA_LANSKIL_LIST IS NOT NULL AND P_SUBAREA_LANSKIL_LIST.COUNT > 0 THEN 
			FOR i IN P_SUBAREA_LANSKIL_LIST.FIRST .. P_SUBAREA_LANSKIL_LIST.LAST
			LOOP
			v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_SUBAREA_LANSKIL_LIST(i).SUBJECT_AREA);
			v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_SUBAREA_LANSKIL_LIST(i).LANGUAGE_SKILL);
			INSERT INTO EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(EWP.GENERATE_UUID(), P_COP_COND_ID, v_isced_code, v_lang_skill_id);
			END LOOP;
		END IF;
	END;

    /*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN MOBILITY_TYPE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR, p_group IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER(p_group);
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE.MOBILITY_CATEGORY IS NOT NULL OR  P_MOBILITY_TYPE.MOBILITY_GROUP IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE.MOBILITY_CATEGORY, P_MOBILITY_TYPE.MOBILITY_GROUP);
		END IF;
		RETURN v_id;
	END;


     /*
		Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_DURATION(P_DURATION IN PKG_IIAS.DURATION) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (v_id, P_DURATION.NUMBER_DURATION, P_DURATION.UNIT);
		RETURN v_id;
	END;

     /*
		Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
	*/
	FUNCTION INSERTA_MOBILITY_NUMBER(P_MOB_NUMBER IN VARCHAR) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (v_id, P_MOB_NUMBER);
		RETURN v_id;
	END;

	/*
		Inserta los EQUF LEVELS asociados a una condicion de cooperacion
	*/
	PROCEDURE INSERTA_EQFLEVELS(P_COOP_COND_ID IN VARCHAR2, P_COOP_COND IN COOPERATION_CONDITION) AS
	BEGIN
		IF P_COOP_COND.EQF_LEVEL_LIST IS NOT NULL AND P_COOP_COND.EQF_LEVEL_LIST .COUNT > 0 THEN 
			FOR i IN P_COOP_COND.EQF_LEVEL_LIST .FIRST .. P_COOP_COND.EQF_LEVEL_LIST .LAST
			LOOP
				INSERT INTO EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (P_COOP_COND_ID,P_COOP_COND.EQF_LEVEL_LIST(i));
			END LOOP;
		END IF;
	END;

	/*
		Persiste una lista de condiciones de cooperacion asociadas a un IIAS
	*/
	PROCEDURE INSERTA_COOP_CONDITIONS(P_IIA_ID IN VARCHAR, P_COOP_COND IN COOPERATION_CONDITION_LIST) AS 
		v_s_iia_partner_id VARCHAR2(255);
        v_s_c_p_id VARCHAR2(255);
        v_r_iia_partner_id VARCHAR2(255);
        v_r_c_p_id VARCHAR2(255);
        v_mobility_type_id VARCHAR2(255);
        v_duration_id VARCHAR2(255);
        v_mob_number_id VARCHAR2(255);
		v_cc_id VARCHAR2(255);
	BEGIN 
		FOR i IN P_COOP_COND.FIRST .. P_COOP_COND.LAST 
		LOOP
			v_s_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).SENDING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).SENDING_PARTNER.PARTNER_CONTACTS);

			v_r_iia_partner_id := INSERTA_IIA_PARTNER(P_COOP_COND(i).RECEIVING_PARTNER);
			INSERTA_PARTNER_CONTACTS(v_s_iia_partner_id, P_COOP_COND(i).RECEIVING_PARTNER.PARTNER_CONTACTS);   

			v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_COOP_COND(i).MOBILITY_TYPE);
			v_duration_id := INSERTA_DURATION(P_COOP_COND(i).COP_COND_DURATION);
			v_mob_number_id := INSERTA_MOBILITY_NUMBER(P_COOP_COND(i).MOBILITY_NUMBER);

			v_cc_id :=	EWP.GENERATE_UUID();
			INSERT INTO EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO, BLENDED) 
				VALUES (v_cc_id, P_COOP_COND(i).END_DATE, P_COOP_COND(i).START_DATE, v_duration_id,
					v_mob_number_id, v_mobility_type_id, v_r_iia_partner_id, v_s_iia_partner_id, P_IIA_ID, P_COOP_COND(i).OTHER_INFO, P_COOP_COND(i).BLENDED );

			INSERTA_SUBAREA_LANSKIL(v_cc_id, P_COOP_COND(i).SUBJECT_AREA_LANGUAGE_LIST);
			INSERTA_EQFLEVELS(v_cc_id,P_COOP_COND(i));
		END LOOP;
	END;

	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_IIA(P_IIA IN PKG_IIAS.IIA) RETURN VARCHAR2 AS
		v_iia_id VARCHAR2(255);
	BEGIN

        v_iia_id := EWP.GENERATE_UUID();
        INSERT INTO EWPCV_IIA (ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE) 
            VALUES (v_iia_id, P_IIA.END_DATE, P_IIA.IIA_CODE, CURRENT_DATE, P_IIA.START_DATE);

		INSERTA_COOP_CONDITIONS(v_iia_id,P_IIA.COOPERATION_CONDITION_LIST);

        RETURN v_iia_id;
	END;


	FUNCTION OBTEN_NOTIFIER_HEI(P_IIA_ID IN VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS 
		v_notifier_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT DISTINCT(P.INSTITUTION_ID)
			FROM EWPCV_IIA_PARTNER P 
				INNER JOIN EWPCV_COOPERATION_CONDITION CC
					ON CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID
			WHERE CC.IIA_ID = p_id;
	BEGIN 
		OPEN c(P_IIA_ID);
		FETCH c INTO v_notifier_hei;
		WHILE c%FOUND AND UPPER(v_notifier_hei) = UPPER(P_HEI_TO_NOTIFY)
		LOOP
			FETCH c INTO v_notifier_hei;
		END LOOP;
		CLOSE c;
		RETURN v_notifier_hei;
	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	PROCEDURE ACTUALIZA_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA) AS
	BEGIN

 		UPDATE EWPCV_IIA SET START_DATE = P_IIA.START_DATE, END_DATE = P_IIA.END_DATE, IIA_CODE = P_IIA.IIA_CODE, PDF = P_IIA.PDF, MODIFY_DATE = SYSDATE
			WHERE ID = P_IIA_ID;
		BORRA_COOP_CONDITIONS(P_IIA_ID);
		INSERTA_COOP_CONDITIONS(P_IIA_ID, P_IIA.COOPERATION_CONDITION_LIST);

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA,  P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			P_IIA_ID := INSERTA_IIA(P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);

		END IF;
        COMMIT;
		RETURN v_cod_retorno;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END INSERT_IIA; 	

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS			
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count_iia NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_IIA(P_IIA, P_ERROR_MESSAGE);		
		IF v_cod_retorno = 0 THEN 
			ACTUALIZA_IIA(P_IIA_ID, P_IIA);
			v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
			PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
            ROLLBACK;
            RETURN -1;
	END UPDATE_IIA; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_count_iia NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count_mobility NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'El IIA no existe';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count_mobility FROM EWPCV_MOBILITY WHERE IIA_ID = P_IIA_ID;
		IF v_count_mobility > 0 THEN
			P_ERROR_MESSAGE := 'Existen movilidadtes asociadas al IIA';
			RETURN -1;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI(P_IIA_ID, P_HEI_TO_NOTIFY);
		BORRA_IIA(P_IIA_ID);
		PKG_COMMON.INSERTA_NOTIFICATION(P_IIA_ID, 0, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		return 0;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_IIA; 

    /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
	*/
    FUNCTION BIND_IIA(P_OWN_IIA_ID IN VARCHAR2, P_ID_FROM_REMOTE_IIA IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_count_iia NUMBER := 0;
        v_remote_iia_id VARCHAR2(255);
        v_remote_coop_cond_hash VARCHAR2(255);
        v_remote_iia_code VARCHAR2(255);
        CURSOR c(p_remote_id IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID, IIA.REMOTE_COP_COND_HASH, IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_remote_id;
    BEGIN
        IF P_OWN_IIA_ID IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA propio';
			RETURN -1;
		ELSIF P_ID_FROM_REMOTE_IIA IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id del IIA remoto';
			RETURN -1;
		END IF;

        SELECT COUNT(1) INTO v_count_iia FROM EWPCV_IIA WHERE ID = P_OWN_IIA_ID;
		IF v_count_iia = 0 THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha infromado como parámetro P_OWN_IIA_ID';
			RETURN -1;
		END IF;

        OPEN c(P_ID_FROM_REMOTE_IIA);
		FETCH c INTO v_remote_iia_id, v_remote_coop_cond_hash, v_remote_iia_code;
		CLOSE c;
		IF v_remote_iia_id IS NULL THEN
			P_ERROR_MESSAGE := 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
			RETURN -1;
		END IF;
        --Bindeamos el IIA nuestro con el remoto
        UPDATE EWPCV_IIA SET REMOTE_IIA_ID = v_remote_iia_id, REMOTE_IIA_CODE = v_remote_iia_code
            WHERE ID = P_OWN_IIA_ID;
        COMMIT;
        return 0;

        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END BIND_IIA;

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
    FUNCTION APPROVE_IIA(P_INTERNAL_ID_REMOTE_IIA_COPY IN VARCHAR2, P_HEI_ID_TO_NOTIFY IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS
        v_remote_iia_id VARCHAR2(255);
        v_notifier_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR c(p_internal_id_iia_copy IN VARCHAR2) IS 
			SELECT IIA.REMOTE_IIA_ID
			FROM EWPCV_IIA IIA 
			WHERE IIA.ID = p_internal_id_iia_copy;
    BEGIN
        IF P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el id interno de la copia del IIA remoto';
			RETURN -1;
		ELSIF P_HEI_ID_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado el hei-id de la institución a notificar';
			RETURN -1;
		END IF;

        OPEN c(P_INTERNAL_ID_REMOTE_IIA_COPY);
		FETCH c INTO v_remote_iia_id;
		CLOSE c;

        v_notifier_hei := OBTEN_NOTIFIER_HEI(P_INTERNAL_ID_REMOTE_IIA_COPY, P_HEI_ID_TO_NOTIFY);
        v_id := EWP.GENERATE_UUID();  
        INSERT INTO EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI) 
						VALUES (v_id, 0, v_remote_iia_id, P_HEI_ID_TO_NOTIFY, SYSDATE, 4, 1, 0, 0, v_notifier_hei);
        COMMIT;
        return 0;
        EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;  
    END APPROVE_IIA;

END PKG_IIAS;
/
--PKG MOBILITY LA
/
create or replace PACKAGE EWP.PKG_MOBILITY_LA AS 

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  

	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
	*/	
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_CODE				   VARCHAR2(255 CHAR),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR)
	  );

	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );
	  
	/* Objeto que modela el objeto con la foto del estudiante. Campos obligatorios marcados con *.
		URL*    Url del fichero con la foto.
		PHOTO_SIZE   Tamaño de la foto, formato width x height, ej:35x45.
		PHOTO_DATE     Fecha real en la que se agregó la foto.
		PHOTO_PUBLIC   Indica si la foto es pública, 0 - privada, 1 - pública
	  
	*/
	TYPE PHOTO_URL IS RECORD
	(
	URL                VARCHAR2(255),
	PHOTO_SIZE         VARCHAR2(255),
	PHOTO_DATE         DATE,
	PHOTO_PUBLIC       NUMBER(1,0)
	);

	/* Lista con los objetos de las fotos del estudiante. */ 
	TYPE PHOTO_URL_LIST IS TABLE OF PHOTO_URL INDEX BY BINARY_INTEGER ;

	/*Datos del estudiante. Campos obligatorios marcados con *.
		GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
		CONTACT					Informacion del estudiante
		PHOTO_URL_LIST			Listado de objetos con las fotos del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON,
		"PHOTO_URL_LIST"  PKG_MOBILITY_LA.PHOTO_URL_LIST
	  );	 

  /* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
		SENDING_INSTITUTION*    Datos de la instituciÃ³n que envia al estudiante.
		RECEIVING_INSTITUTION*    Datos de la instituciÃ³n que recibe al estudiante.
		ACTUAL_ARRIVAL_DATE     Fecha real de llegada del estudiante aldestino.
		ACTUAL_DEPATURE_DATE    Fecha real de salida del estudiante del origen.
		PLANED_ARRIVAL_DATE*    Fecha prevista de llegada del estudiante aldestino.
		PLANED_DEPATURE_DATE*   Fecha prevista de salida del estudiante del origen.
		IIA_ID            UUID del IIA asociado a la movilidad
		RECEIVING_IIA_ID  UUID del IIA asociado a la movilidad
		COOPERATION_CONDITION_ID  identificador de la condicion de coperacion asociada la movilidad
		STUDENT*          Datos del estudiante
		SUBJECT_AREA*       Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
		STUDENT_LANGUAGE_SKILLS*  Niveles de idiomas que declara el estudiante.
		SENDER_CONTACT*       Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
		SENDER_ADMV_CONTACT     Informacion de contacto de la administrativo en la institucion origen
		RECEIVER_CONTACT*     Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
		RECEIVER_ADMV_CONTACT   Informacion de contacto de la administrativo en la institucion destino
		MOBILITY_TYPE       Tipo de movilidad, puede tomar los siquientes valores:
					Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
		EQF_LEVEL_NOMINATION * Nivel de estudios del estudiante. 
		EQF_LEVEL_DEPARTURE*   Nivel de estudios del estudiante. 
		ACADEMIC_TERM*  Informacion del periodo academico de la asignatura. 
		STATUS_OUTGOING Estado de la movilidad Outgoing. 
					  0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED. Si no se informa, por defecto es 2 - NOMINATION.
  */
	TYPE MOBILITY IS RECORD
	(
		SENDING_INSTITUTION     EWP.INSTITUTION,
		RECEIVING_INSTITUTION   EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE     DATE,
		ACTUAL_DEPATURE_DATE    DATE,
		PLANED_ARRIVAL_DATE     DATE,
		PLANED_DEPATURE_DATE    DATE,
		IIA_ID            VARCHAR(255 CHAR),
		RECEIVING_IIA_ID VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID  VARCHAR(255 CHAR),
		STUDENT           PKG_MOBILITY_LA.STUDENT,
		SUBJECT_AREA        EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS   EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT        EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT     EWP.CONTACT_PERSON,
		RECEIVER_CONTACT      EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT   EWP.CONTACT_PERSON,
		MOBILITY_TYPE       VARCHAR2 (255),
		EQF_LEVEL_NOMINATION NUMBER(3,0),
		EQF_LEVEL_DEPARTURE NUMBER(3,0),
		ACADEMIC_TERM EWP.ACADEMIC_TERM,
		STATUS_OUTGOING  NUMBER(7,0)
	);

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					TIMESTAMP,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					BLOB
	  );

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisiÃ³n del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );

	-- FUNCIONES 

	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Cancela una movilidad
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
		Recibe como parametro el identificador de la movilidad
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;
	
	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
		Recibe como parametro el identificador de la movilidad
		Admite como parametro un comentario
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2, 
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  	

END PKG_MOBILITY_LA;
/
create or replace PACKAGE BODY EWP.PKG_MOBILITY_LA AS 

	FUNCTION ES_OMOBILITY(P_MOBILITY IN MOBILITY, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_resultado NUMBER(1,0) := 0;
	BEGIN
		IF P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID <> P_HEI_TO_NOTIFY THEN
			v_resultado := -1;
		END IF;

		RETURN v_resultado;
	END;
	
	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de los periodos academicos
			academic_year, institution_id, term_number, total_terms
	*/
	FUNCTION VALIDA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
	BEGIN         
		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ACADEMIC_YEAR';
		END IF;
        
		IF P_ACADEMIC_TERM.TERM_NUMBER IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TERM_NUMBER';
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TOTAL_TERMS';
		END IF;
        
        IF P_ACADEMIC_TERM.INSTITUTION_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;
	
	/*
		Valida los campos obligatorios de una subject area: 
			isced_code 
	*/
	FUNCTION VALIDA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_SUBJECT_AREA.ISCED_CODE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-ISCED_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una language skill: 
			lang, cefr_level 
	*/
	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_LANGUAGE_SKILL.LANG	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-LANG';
		END IF;

		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-CEFR_LEVEL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una institucion: 
			Institution_id, organization_unit_code 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
        v_ounit_count NUMBER := 0;
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT ID
            FROM EWPCV_INSTITUTION 
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);  
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una persona de contacto: 
			GIVEN_NAME, FAMILY_NAME, EMAIL_LIST
	*/
	FUNCTION VALIDA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CONTACT_PERSON.GIVEN_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GIVEN_NAME';
		END IF;

		IF P_CONTACT_PERSON.FAMILY_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-FAMILY_NAME';
		END IF;

		IF P_CONTACT_PERSON.CONTACT.EMAIL IS NULL OR P_CONTACT_PERSON.CONTACT.EMAIL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-CONTACT.EMAIL';
		END IF;

		RETURN v_cod_retorno;
	END;
	
 
	/*
	Valida los campos obligatorios de la foto del estudiante:
	  url
	*/
	FUNCTION VALIDA_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.URL IS NULL OR LENGTH(P_PHOTO_URL.URL) = 0 THEN
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-URL';
			v_cod_retorno := -1;
		END IF;

		RETURN v_cod_retorno;
	END;

	FUNCTION VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL IN PKG_MOBILITY_LA.PHOTO_URL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_PHOTO_URL.PHOTO_SIZE IS NOT NULL AND NOT REGEXP_LIKE(P_PHOTO_URL.PHOTO_SIZE, '^\d*x\d*$') THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-PHOTO_SIZE';
		END IF;
		RETURN v_cod_retorno;
	END;


	FUNCTION VALIDA_PHOTO_URL_LIST(P_PHOTO_URL_LIST IN PKG_MOBILITY_LA.PHOTO_URL_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER
	IS
		v_cod_retorno NUMBER := 0;
		v_error NUMBER := 0;
		v_mensaje VARCHAR2(2000);
	BEGIN
		IF P_PHOTO_URL_LIST IS NOT NULL AND P_PHOTO_URL_LIST.COUNT > 0 THEN 
			FOR i IN P_PHOTO_URL_LIST.FIRST .. P_PHOTO_URL_LIST.LAST
			LOOP
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;
	 
				v_cod_retorno := VALIDA_DATOS_PHOTO_URL(P_PHOTO_URL_LIST(i), v_mensaje);
				v_error := CASE WHEN v_cod_retorno = -1 OR v_error = 1 THEN 1 ELSE 0 END;
	  
				IF v_error = 1 THEN
					P_ERROR_MESSAGE := P_ERROR_MESSAGE || '-PHOTO_URL_LIST(' || i || ')' || v_mensaje;
				END IF;
				v_mensaje := '';
			END LOOP;
		END IF;

		v_cod_retorno := CASE WHEN v_error = 1 THEN -1 ELSE 0 END;

		RETURN v_cod_retorno;
	END;
  

	/*
		Valida los campos obligatorios de un estudiante: 
			global_id
	*/
	FUNCTION VALIDA_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
		v_mensaje_photo_url VARCHAR2(2000);
	BEGIN

		IF P_STUDENT.GLOBAL_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GLOBAL_ID';
		END IF;
		IF VALIDA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_mensaje_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						CONTACT: ' || v_mensaje_contact;
		END IF;
		
		IF VALIDA_PHOTO_URL_LIST(P_STUDENT.PHOTO_URL_LIST, v_mensaje_photo_url) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				PHOTO_URL_LIST: ' || v_mensaje_photo_url;
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de una movilidad y de los objetos anidados: 
			planed_arrival_date,planed_depature_date, iia_code, status, sending_institution, receiving_institution, student
	*/
	FUNCTION VALIDA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_s_institution VARCHAR2(2000);
		v_mensaje_r_institution VARCHAR2(2000);
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_contact VARCHAR2(2000);
		v_mensaje_r_contact VARCHAR2(2000);
		v_mensaje_acaterm VARCHAR2(2000);
	BEGIN

		IF P_MOBILITY.PLANED_ARRIVAL_DATE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_ARRIVAL_DATE';
		END IF;

		IF P_MOBILITY.PLANED_DEPATURE_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_DEPATURE_DATE';
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.SENDING_INSTITUTION, v_mensaje_s_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE|| '
					SENDING_INSTITUTION: ' || v_mensaje_s_institution;
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.RECEIVING_INSTITUTION, v_mensaje_r_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_INSTITUTION: ' || v_mensaje_r_institution;
		END IF;

		IF VALIDA_STUDENT(P_MOBILITY.STUDENT, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					STUDENT: ' || v_mensaje_student;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_mensaje_s_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDER_CONTACT: ' || v_mensaje_s_contact;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_mensaje_r_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVER_CONTACT: ' || v_mensaje_r_contact;
		END IF;
		
		IF VALIDA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-ACADEMIC_TERM: '|| v_mensaje_acaterm;
		END IF;

		IF P_MOBILITY.EQF_LEVEL_NOMINATION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-EQF_LEVEL_NOMINATION';
		END IF;
	  
		IF P_MOBILITY.EQF_LEVEL_DEPARTURE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-EQF_LEVEL_DEPARTURE';
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida que el iias y la coop condition indicadas existan
	*/
	FUNCTION VALIDA_MOBILITY_IIA(P_IIA_ID IN VARCHAR2, P_IS_REMOTE NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
	BEGIN
		IF P_IIA_ID IS NOT NULL THEN
		  SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_IIA WHERE UPPER(ID) = UPPER(P_IIA_ID) AND IS_REMOTE = P_IS_REMOTE;

			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					El acuerdo interistitucional indicado no existe en el sistema.';
			END IF;
		END IF;
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
	FUNCTION VALIDA_DATOS_MOBILITY_INST(P_S_INSTITUTION IN EWP.INSTITUTION, P_R_INSTITUTION  IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		-- validacion de sender 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_S_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_S_INSTITUTION.INSTITUTION_ID||' informada como sender institution no existe en el sistema.';
		ELSIF P_S_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_S_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_S_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_S_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_S_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		-- validacion de receiver 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_R_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_R_INSTITUTION.INSTITUTION_ID||' informada como receiver institution no existe en el sistema.';
		ELSIF P_R_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_R_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_R_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_R_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_R_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
	*/
	FUNCTION VALIDA_DATOS_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;

	BEGIN
		IF P_MOBILITY.IIA_ID IS NOT NULL THEN
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.IIA_ID, 0, P_ERROR_MESSAGE);
		END IF;
		 
		IF v_cod_retorno = 0 AND P_MOBILITY.RECEIVING_IIA_ID IS NOT NULL THEN 
			v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.RECEIVING_IIA_ID, 1, P_ERROR_MESSAGE);
		END IF;
		
		IF v_cod_retorno = 0 THEN 
				v_cod_retorno := VALIDA_DATOS_MOBILITY_INST(P_MOBILITY.SENDING_INSTITUTION, P_MOBILITY.RECEIVING_INSTITUTION, P_ERROR_MESSAGE);
		END IF;
		
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una firma
			signer_name, signer_position, signer_email, timestamp,  signature
	*/
	FUNCTION VALIDA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_SIGNATURE.SIGNER_NAME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_NAME';
		END IF;

		IF P_SIGNATURE.SIGNER_POSITION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_POSITION';
		END IF;

		IF P_SIGNATURE.SIGNER_EMAIL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_EMAIL';
		END IF;

		IF P_SIGNATURE.SIGN_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGN_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN     
        IF UPPER(P_INSTITUTION_ID) <> UPPER(P_ACADEMIC_TERM.INSTITUTION_ID) THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -INSTITUTION_ID: La institucion '|| P_ACADEMIC_TERM.INSTITUTION_ID || ' del periodo academico del componente no coincide con la institucion ' || P_INSTITUTION_ID;
		ELSIF P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN 
                v_cod_retorno := -1;
                 P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -OUNIT_CODE: La unidad organizativa ' || P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE || ' vinculada a ' || P_INSTITUTION_ID || ' no existe en el sistema.';
            END IF;
        END IF;
		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_OUNIT_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REV IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
        v_send_inst_id VARCHAR2(255);
        v_receiv_inst_id VARCHAR2(255);
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
        CURSOR c_s_inst_r_inst(p_mob_id IN VARCHAR2, p_mob_rev IN NUMBER) IS 
            SELECT SENDING_INSTITUTION_ID, RECEIVING_INSTITUTION_ID
            FROM EWPCV_MOBILITY 
            WHERE ID = p_mob_id
            AND MOBILITY_REVISION = p_mob_rev;
	BEGIN
        OPEN c_s_inst_r_inst(P_MOBILITY_ID, P_MOBILITY_REV);
        FETCH c_s_inst_r_inst INTO v_send_inst_id, v_receiv_inst_id;
        CLOSE c_s_inst_r_inst;
        
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP
			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 OR P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 5 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_receiv_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_send_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;
            IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los creditos
			scheme, value
	*/
	FUNCTION VALIDA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CREDIT.SCHEME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-SCHEME';
		END IF;

		IF P_CREDIT.CREDIT_VALUE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-VALUE';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_credit VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP

			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE NOT IN (0,1,2,3,4) THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE (valor erroneo ' || P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
				IF P_COMPONENTS_LIST(i).RECOGNITION_CONDITIONS IS NOT NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-RECOGNITION_CONDITIONS (No informar para el tipo de componente '|| P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')' ;
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;

			IF P_COMPONENTS_LIST(i).TITLE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-TITLE';
			END IF;

			IF P_COMPONENTS_LIST(i).STATUS IS NOT NULL THEN
				IF P_COMPONENTS_LIST(i).STATUS = 0 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (3,4,5) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).STATUS = 1 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (0,1,2) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSE 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-STATUS (valor erroneo'|| P_COMPONENTS_LIST(i).STATUS||')';
				END IF;

				IF P_COMPONENTS_LIST(i).REASON_CODE IS NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-REASON_CODE';
				END IF;
			END IF;

			IF VALIDA_CREDIT(P_COMPONENTS_LIST(i).CREDIT, v_mensaje_credit) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-CREDIT: ' || v_mensaje_credit;
				v_mensaje_credit := '';
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;

		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un Learning Agreement
			mobility, 
	*/
	FUNCTION VALIDA_LEARNING_AGREEMENT(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
	BEGIN

		IF P_LA.MOBILITY_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := '- MOBILITY_ID: ' || P_ERROR_MESSAGE;
		END IF;

		IF P_LA.COMPONENTS IS NULL OR p_LA.COMPONENTS.COUNT = 0 THEN 
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			COMPONENTS';
		ELSIF VALIDA_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				COMPONENTS: ' || v_mensaje_components;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.STUDENT_SIGNATURE, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				STUDENT_SIGNATURE: ' || v_mensaje_student;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE, v_mensaje_s_coord) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				SENDING_HEI_SIGNATURE: ' || v_mensaje_s_coord;
		END IF;		

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/* Borra el estudiante de la tabla mobility participant*/
	PROCEDURE BORRA_STUDENT(P_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255CHAR);
		v_contact_details_id VARCHAR2(255CHAR);
	BEGIN
		SELECT CONTACT_ID INTO v_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		SELECT CONTACT_DETAILS_ID INTO v_contact_details_id FROM EWPCV_CONTACT WHERE ID = v_contact_id;
		DELETE FROM EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = v_contact_details_id;
		DELETE FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		PKG_COMMON.BORRA_CONTACT(v_contact_id);
	END;

	/*
		Borra creditos asociados a un componente
	*/
	PROCEDURE BORRA_CREDITS(P_COMPONENT_ID IN VARCHAR2) AS
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CREDITS_ID
			FROM EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = p_id;
	BEGIN
		FOR rec IN c(P_COMPONENT_ID) 
		LOOP
			DELETE FROM EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = rec.CREDITS_ID;
			DELETE FROM EWPCV_CREDIT WHERE ID = rec.CREDITS_ID;
		END LOOP;
	END;

	/*
		Borra un componente
	*/
	PROCEDURE BORRA_COMPONENT(P_ID IN VARCHAR2) AS
		v_at_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_TERM_DISPLAY_NAME
			FROM EWPCV_LA_COMPONENT
			WHERE ID = p_id;
	BEGIN
		BORRA_CREDITS(P_ID);
		OPEN c(P_ID);
		FETCH c INTO v_at_id;
		CLOSE c;
		DELETE FROM EWPCV_LA_COMPONENT WHERE ID = P_ID;
		PKG_COMMON.BORRA_ACADEMIC_TERM(v_at_id);
	END;

	/*
		Borra un componente de tipo studied
	*/
	PROCEDURE BORRA_STUDIED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDIED_LA_COMPONENT_ID
			FROM EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = rec.STUDIED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.STUDIED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo recognized
	*/
	PROCEDURE BORRA_RECOGNIZED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = rec.RECOGNIZED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.RECOGNIZED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo virtual
	*/
	PROCEDURE BORRA_VIRTUAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT VIRTUAL_LA_COMPONENT_ID
			FROM EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = rec.VIRTUAL_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.VIRTUAL_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo blended
	*/
	PROCEDURE BORRA_BLENDED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT BLENDED_LA_COMPONENT_ID
			FROM EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = rec.BLENDED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.BLENDED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo doctoral
	*/
	PROCEDURE BORRA_DOCTORAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = rec.DOCTORAL_LA_COMPONENTS_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.DOCTORAL_LA_COMPONENTS_ID);
		END LOOP;
	END;

	/*
		Borra una revision de un LA
	*/
	PROCEDURE BORRA_LA(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		v_s_id VARCHAR2(255 CHAR);
		v_sc_id VARCHAR2(255 CHAR);
		v_rc_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDENT_SIGN, SENDER_COORDINATOR_SIGN, RECEIVER_COORDINATOR_SIGN
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;

	BEGIN

		BORRA_STUDIED_COMP(P_LA_ID, LA_REVISION);
		BORRA_RECOGNIZED_COMP(P_LA_ID, LA_REVISION);
		BORRA_VIRTUAL_COMP(P_LA_ID, LA_REVISION);
		BORRA_BLENDED_COMP(P_LA_ID, LA_REVISION);
		BORRA_DOCTORAL_COMP(P_LA_ID, LA_REVISION);

		OPEN c(P_LA_ID, LA_REVISION);
		FETCH c INTO v_s_id, v_sc_id, v_rc_id;
		CLOSE c;

		DELETE FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_s_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_sc_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_rc_id;
	END;

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/
	PROCEDURE BORRA_MOBILITY_LANSKILL(P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REVISION IN NUMBER) AS
		v_lskill_id VARCHAR2(255 CHAR);
		CURSOR c_lskill(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LANGUAGE_SKILL_ID
			FROM EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR l_rec IN c_lskill(P_MOBILITY_ID, P_MOBILITY_REVISION)
			LOOP
				DELETE FROM EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = l_rec.LANGUAGE_SKILL_ID;
				DELETE FROM EWPCV_LANGUAGE_SKILL WHERE ID = l_rec.LANGUAGE_SKILL_ID;
			END LOOP;	
	END;

	/*
		Borra una movilidad
	*/
	PROCEDURE BORRA_MOBILITY(P_MOBILITY_ID IN VARCHAR2) AS
		v_student_count NUMBER;
		v_mtype_count NUMBER;

		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_REVISION, 
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, ISCED_CODE
			FROM EWPCV_MOBILITY 
			WHERE ID = p_id;

		CURSOR c_la(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
			FROM EWPCV_MOBILITY_LA 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR mobility_rec IN c_mobility(P_MOBILITY_ID)
		LOOP
			BORRA_MOBILITY_LANSKILL(mobility_rec.ID, mobility_rec.MOBILITY_REVISION);

			FOR la_rec IN c_la(mobility_rec.ID, mobility_rec.MOBILITY_REVISION)
			LOOP
				BORRA_LA(la_rec.LEARNING_AGREEMENT_ID, la_rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;	

			DELETE FROM EWPCV_MOBILITY WHERE ID = mobility_rec.ID AND MOBILITY_REVISION = mobility_rec.MOBILITY_REVISION;

			DELETE FROM EWPCV_SUBJECT_AREA WHERE ID = mobility_rec.ISCED_CODE;

			SELECT COUNT(1) INTO v_student_count FROM EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = mobility_rec.MOBILITY_PARTICIPANT_ID;
			IF v_student_count = 0 THEN 
				BORRA_STUDENT(mobility_rec.MOBILITY_PARTICIPANT_ID);
			END IF;

			SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = mobility_rec.SENDER_CONTACT_ID;
			IF v_scontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = mobility_rec.SENDER_ADMV_CONTACT_ID;
			IF v_sacontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_ADMV_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = mobility_rec.RECEIVER_CONTACT_ID;
			IF v_rcontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = mobility_rec.RECEIVER_ADMV_CONTACT_ID;
			IF v_racontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_ADMV_CONTACT_ID);
			END IF;
		END LOOP;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_o_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR C(p_start IN VARCHAR2, p_end IN VARCHAR) IS 
			SELECT ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(p_start)
			AND	UPPER(START_YEAR) =  UPPER(p_end);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN
		OPEN C(SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4) ,SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		FETCH C INTO v_a_id;
		CLOSE C;
		IF v_a_id IS NULL THEN 
			v_a_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
				VALUES(v_a_id, SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4),SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		END IF;

		OPEN ounit_cursor(P_ACADEMIC_TERM.INSTITUTION_ID, P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
		FETCH ounit_cursor INTO v_o_id;
		CLOSE ounit_cursor;

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (v_id, P_ACADEMIC_TERM.END_DATE, P_ACADEMIC_TERM.START_DATE , LOWER(P_ACADEMIC_TERM.INSTITUTION_ID) , v_o_id, v_a_id, P_ACADEMIC_TERM.TOTAL_TERMS, P_ACADEMIC_TERM.TERM_NUMBER);

		IF P_ACADEMIC_TERM.DESCRIPTION IS NOT NULL AND P_ACADEMIC_TERM.DESCRIPTION.COUNT > 0 THEN
			FOR i IN P_ACADEMIC_TERM.DESCRIPTION.FIRST .. P_ACADEMIC_TERM.DESCRIPTION.LAST 
			LOOP
				v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ACADEMIC_TERM.DESCRIPTION(i));
				INSERT INTO EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (v_id, v_li_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;
	
	
	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_PARTICIPANT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT) RETURN VARCHAR2 AS
		v_m_p_id VARCHAR2(255);
		v_c_id   VARCHAR2(255);
		v_c_d_id VARCHAR2(255);
		v_p_id VARCHAR2(255);
	BEGIN
		v_m_p_id := EWP.GENERATE_UUID();
		v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, null, null);
		INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, GLOBAL_ID, CONTACT_ID) VALUES (v_m_p_id, P_STUDENT.GLOBAL_ID, v_c_id);

		SELECT (
			SELECT CONTACT_DETAILS_ID FROM EWPCV_CONTACT C WHERE C.ID = v_c_id
		) INTO v_c_d_id FROM DUAL;

		IF v_c_d_id IS NOT NULL THEN
			IF P_STUDENT.PHOTO_URL_LIST IS NOT NULL AND P_STUDENT.PHOTO_URL_LIST.COUNT > 0 THEN 
				FOR i IN P_STUDENT.PHOTO_URL_LIST.FIRST .. P_STUDENT.PHOTO_URL_LIST.LAST
				LOOP
					v_p_id := EWP.GENERATE_UUID();
					INSERT INTO EWP.EWPCV_PHOTO_URL (
						PHOTO_URL_ID, CONTACT_DETAILS_ID, 
						URL, PHOTO_SIZE, 
						PHOTO_DATE, PHOTO_PUBLIC) 
					VALUES (v_p_id, v_c_d_id, 
						P_STUDENT.PHOTO_URL_LIST(i).URL, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_SIZE, 
						P_STUDENT.PHOTO_URL_LIST(i).PHOTO_DATE, P_STUDENT.PHOTO_URL_LIST(i).PHOTO_PUBLIC);
				END LOOP;
			END IF;
		END IF;
		
		RETURN v_m_p_id;
	END;


	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA');
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE, 'MOBILITY_LA');
		END IF;
		RETURN v_id;
	END;


	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_REVISION IN NUMBER, P_OMOBILITY_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_s_ounit_id VARCHAR2(255);
		v_r_ounit_id VARCHAR2(255);
		v_student_id VARCHAR2(255);
		v_s_contact_id VARCHAR2(255);
		v_s_admv_contact_id VARCHAR2(255);
		v_r_contact_id VARCHAR2(255);
		v_r_admv_contact_id VARCHAR2(255);
		v_mobility_type_id VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
		v_academic_term_id VARCHAR2(255);
		v_status_incoming NUMBER;
		v_status_outgoing NUMBER;
	BEGIN
		v_s_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.SENDING_INSTITUTION);
		v_r_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.RECEIVING_INSTITUTION);
		v_student_id := INSERTA_MOBILITY_PARTICIPANT(P_MOBILITY.STUDENT);
		v_s_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, null, null);
		v_s_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT, null, null);
		v_r_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, null, null);
		v_r_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT, null, null);

		v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_MOBILITY.SUBJECT_AREA);
		v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);
				
		v_academic_term_id := INSERTA_ACADEMIC_TERM(P_MOBILITY.ACADEMIC_TERM);

		   
		-- Por defecto el status_outgoing es NOMINATION
		IF P_MOBILITY.STATUS_OUTGOING IS NULL THEN
			v_status_outgoing := 2; 
		ELSE
			v_status_outgoing := P_MOBILITY.STATUS_OUTGOING;
		END IF;

		IF P_OMOBILITY_ID IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
		ELSE 
			v_id := P_OMOBILITY_ID;
		END IF;
		
		INSERT INTO EWPCV_MOBILITY (ID,
		ACTUAL_ARRIVAL_DATE,
		ACTUAL_DEPARTURE_DATE,
		IIA_ID,
		RECEIVING_IIA_ID,
		ISCED_CODE,
		MOBILITY_PARTICIPANT_ID,
		MOBILITY_REVISION,
		PLANNED_ARRIVAL_DATE,
		PLANNED_DEPARTURE_DATE,
		RECEIVING_INSTITUTION_ID,
		RECEIVING_ORGANIZATION_UNIT_ID,
		SENDING_INSTITUTION_ID,
		SENDING_ORGANIZATION_UNIT_ID,
		MOBILITY_TYPE_ID,
		SENDER_CONTACT_ID,
		SENDER_ADMV_CONTACT_ID,
		RECEIVER_CONTACT_ID,
		RECEIVER_ADMV_CONTACT_ID,
		ACADEMIC_TERM_ID,
		EQF_LEVEL_DEPARTURE,
		EQF_LEVEL_NOMINATION,
		STATUS_OUTGOING,
		MODIFY_DATE)
		VALUES (v_id,
		P_MOBILITY.ACTUAL_ARRIVAL_DATE,
		P_MOBILITY.ACTUAL_DEPATURE_DATE,
		P_MOBILITY.IIA_ID,
		P_MOBILITY.RECEIVING_IIA_ID,
		v_isced_code,
		v_student_id,
		(P_REVISION +1),
		P_MOBILITY.PLANED_ARRIVAL_DATE,
		P_MOBILITY.PLANED_DEPATURE_DATE,
		LOWER(P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID),
		v_r_ounit_id,
		LOWER(P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID),
		v_s_ounit_id,
		v_mobility_type_id,
		v_s_contact_id,
		v_s_admv_contact_id,
		v_r_contact_id,
		v_r_admv_contact_id,
		v_academic_term_id,
		P_MOBILITY.EQF_LEVEL_DEPARTURE,
		P_MOBILITY.EQF_LEVEL_NOMINATION,
		v_status_outgoing,
		sysdate);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT >0 THEN 
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (v_id, (P_REVISION + 1), v_lang_skill_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (v_id, P_SIGNATURE.SIGNER_NAME, P_SIGNATURE.SIGNER_POSITION , P_SIGNATURE.SIGNER_EMAIL , P_SIGNATURE.SIGN_DATE , P_SIGNATURE.SIGNER_APP, P_SIGNATURE.SIGNATURE);
		RETURN v_id;
	END;

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		INSERT INTO EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
			VALUES (v_id, P_CREDIT.SCHEME, P_CREDIT.CREDIT_VALUE);
		RETURN v_id;
	END;

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_COMPONENTE(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_at_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		v_at_id:= INSERTA_ACADEMIC_TERM(P_COMPONENT.ACADEMIC_TERM);

		--FALTAN LOS Y LOIS

		INSERT INTO EWPCV_LA_COMPONENT (ID,ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (v_id, v_at_id, NULL,P_COMPONENT.LOS_CODE, NULL, P_COMPONENT.STATUS, P_COMPONENT.TITLE, P_COMPONENT.REASON_CODE, P_COMPONENT.REASON_TEXT,
				P_COMPONENT.LA_COMPONENT_TYPE, P_COMPONENT.RECOGNITION_CONDITIONS, P_COMPONENT.SHORT_DESCRIPTION);

		v_c_id := INSERTA_CREDIT(P_COMPONENT.CREDIT);
		INSERT INTO EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (v_id,v_c_id);

		IF P_COMPONENT.LA_COMPONENT_TYPE = 0 THEN
			INSERT INTO EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
			INSERT INTO EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 2 THEN
			INSERT INTO EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 3 THEN
			INSERT INTO EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 4 THEN
			INSERT INTO EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una revision de un learning agreement.
	*/
	Function INSERTA_LA_REVISION(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_LA_REVISION IN NUMBER, P_MOBILITY_REVISION IN NUMBER) RETURN VARCHAR2 AS 
		v_la_id varchar2(255);
		v_stud_sign_id varchar2(255);
		v_s_hei_sign_id varchar2(255);
		v_changes_id varchar2(255);
		v_c_id varchar2(255);
		v_indice NUMBER := 0;
	BEGIN 
		v_stud_sign_id := INSERTA_SIGNATURE(P_LA.STUDENT_SIGNATURE);
		v_s_hei_sign_id :=  INSERTA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE);

		IF P_LA_ID IS NULL THEN 
			v_la_id := EWP.GENERATE_UUID();
		ELSE 
			v_la_id := P_LA_ID;
		END IF;
		v_changes_id := v_la_id || '-' ||P_LA_REVISION;
		INSERT INTO EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (v_la_id,P_LA_REVISION,v_stud_sign_id,v_s_hei_sign_id, 0, SYSDATE, v_changes_id);

		INSERT INTO EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(P_LA.MOBILITY_ID, P_MOBILITY_REVISION, v_la_id, P_LA_REVISION);

		WHILE v_indice < P_LA.COMPONENTS.COUNT 
		LOOP
			v_c_id := INSERTA_COMPONENTE(P_LA.COMPONENTS(v_indice),v_la_id, P_LA_REVISION);
			v_indice := v_indice + 1 ;
		END LOOP;
		RETURN v_la_id;
	END;

	/*
		Genera el tag approve de un mobility update request
	*/
	FUNCTION CREA_TAG_APPROVE(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_approve dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:approve-proposal-v1' );
		l_node_approve := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_approve;
	END;

	/*
		Genera el tag comment de un mobility update request
	*/
	FUNCTION CREA_TAG_COMMENT(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_COMMENT IN VARCHAR2, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_comment dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment-proposal-v1' );
		l_node_comment := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja comment
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_COMMENT);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_comment;
	END;


	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_ACCEPT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE ) AS 
		v_id VARCHAR2(255);
		v_mobility_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_approve dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN

		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_approve := CREA_TAG_APPROVE(P_CHANGES_ID ,v_mobility_id,  P_SIGNATURE, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_approve);

		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, LOWER(P_SENDING_HEI), 0, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_REJECT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2) AS 
		v_mobility_id VARCHAR2(255);
		v_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_comment dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN
		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_comment := CREA_TAG_COMMENT(P_CHANGES_ID, v_mobility_id, P_SIGNATURE, P_OBSERVATION, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_comment);
		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, LOWER(P_SENDING_HEI), 1, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************

	PROCEDURE ACTUALIZA_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY) AS 
		v_m_revision VARCHAR2(255);
		v_id VARCHAR2(255);
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY
			WHERE ID = P_ID;
	BEGIN 
		OPEN c_mobility(P_OMOBILITY_ID);
		FETCH c_mobility INTO v_m_revision;
		CLOSE c_mobility;
		
		--Insertamos la nueva revisión de la Movilidad
		v_id := INSERTA_MOBILITY(P_MOBILITY, v_m_revision, P_OMOBILITY_ID);
         
        -- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
        UPDATE EWPCV_MOBILITY_LA SET MOBILITY_ID = P_OMOBILITY_ID, MOBILITY_REVISION = (v_m_revision + 1) WHERE MOBILITY_ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

	END;

	--  **********************************************
	--	************** NOTIFICACION ******************
	--	**********************************************

	FUNCTION OBTEN_NOTIFIER_HEI_LA(P_LA_ID IN VARCHAR, P_LA_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY M
			INNER JOIN EWPCV_MOBILITY_LA MLA 
				ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = p_id AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN 
		OPEN c(P_LA_ID, P_LA_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN LOWER(v_s_hei);
		ELSE 
			RETURN NULL;
		END IF;
	END;

	FUNCTION OBTEN_NOTIFIER_HEI_MOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN 
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN  LOWER(v_s_hei); 
		ELSE 
			RETURN NULL;
		END IF;
	END;
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_m_revision NUMBER := 0;
		v_la_id VARCHAR2(255);
		CURSOR C(p_id IN VARCHAR2) IS SELECT MAX(MOBILITY_REVISION)
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id;
		CURSOR C_LA(p_id IN VARCHAR2) IS 
		SELECT LEARNING_AGREEMENT_ID 
		FROM EWPCV_MOBILITY_LA 
		WHERE MOBILITY_ID = p_id;

	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		OPEN C(P_LA.MOBILITY_ID);
		FETCH C INTO v_m_revision;
		CLOSE C;
		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada no existe';
			RETURN -1;
		END IF;

		OPEN C_LA(P_LA.MOBILITY_ID);
		FETCH C_LA INTO v_la_id;
		CLOSE C_LA;
		IF v_la_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: '||v_la_id;
			RETURN -1;
		END IF;		

        v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
        
		P_LA_ID := INSERTA_LA_REVISION(null, P_LA, 0, v_m_revision);
		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, 0, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion receptora';
			P_LA_ID := null;
			ROLLBACK;
			RETURN -1;
		END IF;

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_LEARNING_AGREEMENT; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_no_firmado NUMBER:=0;
		v_la_revision NUMBER;
		v_m_revision NUMBER;
		v_la_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2) IS 
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;
		CURSOR c_m_revision(p_mobility_id IN VARCHAR2, p_la_id IN VARCHAR2, p_la_revision IN NUMBER) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_mobility_id
				AND LEARNING_AGREEMENT_ID = p_la_id
				AND LEARNING_AGREEMENT_REVISION = p_la_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		OPEN c_la(P_LA_ID);
		FETCH c_la INTO v_la_revision;
		CLOSE c_la;

		IF v_la_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado no existe';
			RETURN -1;
		END IF;

		Open c_m_revision(P_LA.MOBILITY_ID, P_LA_ID, v_la_revision);
		FETCH c_m_revision INTO v_m_revision;
		CLOSE c_m_revision;

		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada para el learning agreement no es correcta.';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_no_firmado FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND STATUS = 0;
		IF v_no_firmado > 0 THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, v_la_revision, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
		
		v_la_id := INSERTA_LA_REVISION(P_LA_ID, P_LA, v_la_revision + 1, v_m_revision);

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_LEARNING_AGREEMENT; 	

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;


		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_ACCEPT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE);
			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END ACCEPT_LEARNING_AGREEMENT; 	 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_REJECT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE, P_OBSERVATION);

			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END REJECT_LEARNING_AGREEMENT; 	

	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER	AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count NUMBER := 0;
		v_m_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS SELECT LEARNING_AGREEMENT_REVISION
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;

		CURSOR c_mob(p_id IN VARCHAR2, p_revision IN NUMBER) IS SELECT MOBILITY_ID
			FROM EWPCV_MOBILITY_LA 
			WHERE LEARNING_AGREEMENT_ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID;
		IF v_count > 0 THEN 
			FOR rec IN c(P_LA_ID) 
			LOOP 
				IF v_notifier_hei IS NULL THEN 
					v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION, P_HEI_TO_NOTIFY);
				END IF;

				IF v_notifier_hei IS NULL THEN 
					P_ERROR_MESSAGE := 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.';
					RETURN -1;
				END IF;

				IF v_m_id IS NULL THEN 
					OPEN c_mob(P_LA_ID,rec.LEARNING_AGREEMENT_REVISION);
					FETCH c_mob INTO v_m_id;
					CLOSE c_mob;
				END IF;

				BORRA_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			PKG_COMMON.INSERTA_NOTIFICATION(v_m_id, 5, P_HEI_TO_NOTIFY, v_notifier_hei);	
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_LEARNING_AGREEMENT; 
	
	FUNCTION VALIDA_STATUS(P_STATUS IN NUMBER, P_ES_OUTGOING IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER 
	AS
		v_outgoing_statuses VARCHAR2(10) := '0123';
		v_incoming_statuses VARCHAR2(10) := '456';
		v_resultado NUMBER := 0;
	BEGIN
		IF P_ES_OUTGOING <> 0 AND P_ES_OUTGOING <> 1 THEN
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_outgoing no tiene un status válido';
			RETURN -1;
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = 0 AND INSTR(v_outgoing_statuses, P_STATUS) = 0 THEN 
			v_resultado := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_outgoing no tiene un status válido';
		END IF;

		IF P_STATUS IS NOT NULL AND P_ES_OUTGOING = -1 AND INSTR(v_incoming_statuses, P_STATUS) = 0 THEN 
			v_resultado := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || 'El campo status_incoming no tiene un status válido';
		END IF;
		RETURN v_resultado;
	END;

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_sending_hei VARCHAR2(255);
		v_es_outgoing NUMBER(1,0);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);

		v_sending_hei := P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID;

		IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
			P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
			RETURN -1;
		END IF;

		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		END IF;
		IF v_cod_retorno = 0 THEN
			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			P_OMOBILITY_ID := INSERTA_MOBILITY(P_MOBILITY, -1, null);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_MOBILITY; 	

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_sending_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR C_SENDING_HEI IS SELECT ID, SENDING_INSTITUTION_ID
        FROM EWPCV_MOBILITY 
        WHERE ID = P_OMOBILITY_ID
        ORDER BY MOBILITY_REVISION DESC;
		v_es_outgoing NUMBER := 0;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN

            OPEN C_SENDING_HEI;
            FETCH C_SENDING_HEI INTO v_id, v_sending_hei;
            CLOSE C_SENDING_HEI;

            IF v_id IS NULL THEN 
            	P_ERROR_MESSAGE := 'No existe la movilidad indicada';
                RETURN -1;
			END IF;

			IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                RETURN -1;
			END IF;
			 
			v_es_outgoing := ES_OMOBILITY(P_MOBILITY, P_HEI_TO_NOTIFY);

			IF VALIDA_STATUS(P_MOBILITY.STATUS_OUTGOING, v_es_outgoing, P_ERROR_MESSAGE) = -1 THEN
				RETURN -1;
			END IF;

			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				ACTUALIZA_MOBILITY(P_OMOBILITY_ID,P_MOBILITY);
				PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_sending_hei);
			END IF;
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_MOBILITY; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
		v_m_revision NUMBER ;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			BORRA_MOBILITY(P_OMOBILITY_ID);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La movilidad no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_MOBILITY;  	

	/* Cancela una movilidad
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION CANCEL_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		
		COMMIT;	
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_DEPARTURE(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.';
				RETURN -1;
			END IF;
		 
			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION STUDENT_ARRIVAL(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 1, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION APPROVE_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_NOMINATION(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY_COMMENT IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;

			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);
			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
				RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_ACTUAL_DATES(P_OMOBILITY_ID IN VARCHAR2, P_ACTUAL_DEPARTURE IN DATE, P_ACTUAL_ARRIVAL IN DATE, P_MOBILITY_COMMENT IN VARCHAR2, 
	P_ERROR_MESSAGE IN OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER
	AS
		v_notifier_hei VARCHAR2(255);
		v_resultado NUMBER(1,0) := 0;
		v_mobility MOBILITY;
		v_count NUMBER := 0;
		v_m_revision VARCHAR2(255);
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWP.EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF UPPER(v_notifier_hei) <> UPPER(P_HEI_TO_NOTIFY) THEN
			P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo Incomming';
			RETURN -1;
			END IF;

			UPDATE EWP.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATE,
			ACTUAL_ARRIVAL_DATE = P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = P_ACTUAL_DEPARTURE
			WHERE ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 2, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			P_ERROR_MESSAGE := 'La movilidad no existe';
			v_resultado := -1;
		END IF;
		COMMIT;
		RETURN v_resultado;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;


END PKG_MOBILITY_LA;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	
/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.04.01', SYSDATE, '16_UPGRADE_v01.04.01');
COMMIT;
    
    
    
    
    
    
    
    
    
