/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

CREATE TABLE EWP.EWPCV_IIA_REGRESSIONS (
    ID VARCHAR2(255) PRIMARY KEY,
    IIA_ID VARCHAR2(255) NOT NULL,
    IIA_HASH VARCHAR2(255) NOT NULL,
    OWNER_SCHAC VARCHAR2(255) NOT NULL,
    REGRESSION_DATE DATE NOT NULL
);
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_REGRESSIONS TO APLEWP;
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_IIA_REGRESSIONS TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_IIA_REGRESSIONS FOR EWP.EWPCV_IIA_REGRESSIONS;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v03.00.00', SYSDATE, '52_IIA_REGRESSIONS_v03.00.00');
COMMIT;
