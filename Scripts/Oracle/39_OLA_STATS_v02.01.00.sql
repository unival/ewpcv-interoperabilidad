/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
create table EWP.EWPCV_STATS_OLA (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        START_YEAR varchar2(255 char),
        END_YEAR varchar2(255 char),
        SENDING_INSTITUTION varchar2(255 char),
        TOTAL NUMBER(10,0),
        APPROVAL_UNMODIFIED NUMBER(10,0),
        APPROVAL_MODIFIED NUMBER(10,0),
        LAST_APPROVED NUMBER(10,0),
        LAST_REJECTED NUMBER(10,0),
        LAST_PENDING NUMBER(10,0),
		DUMP_DATE date,
        primary key (ID)
    );

    INSERT INTO EWP.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.statisticsOla.audit', 'true');



/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_STATS_OLA                  TO APLEWP;

	GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_STATS_OLA                 TO EWP_APROVISIONAMIENTO;

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/
	CREATE OR REPLACE SYNONYM APLEWP.EWPCV_STATS_OLA                      FOR EWP.EWPCV_STATS_OLA;

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.01.00', SYSDATE, '39_OLA_STATS');
COMMIT;