CREATE TABLE EWP.EWPCV_DICTIONARY (
	ID VARCHAR2(255 CHAR) NOT NULL ENABLE, 
    VERSION NUMBER(10,0), 
    Code varchar(255),
    Description varchar(255),
    Dictionary_Type varchar(255),
	Call_Year varchar(255),
	Primary Key (Id)
);

COMMENT ON COLUMN EWP.EWPCV_DICTIONARY.Code IS 'The word code';
COMMENT ON COLUMN EWP.EWPCV_DICTIONARY.Description IS 'The word value';
COMMENT ON COLUMN EWP.EWPCV_DICTIONARY.Dictionary_Type IS 'The name of the collection dictionary';

GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_DICTIONARY TO APLEWP; 
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_DICTIONARY FOR EWP.EWPCV_DICTIONARY;

DROP TABLE  EWP.EWPCV_FACTSHEET_DATES;
ALTER TABLE EWP.EWPCV_FACT_SHEET ADD NOMINATIONS_AUTUM_TERM DATE;
ALTER TABLE EWP.EWPCV_FACT_SHEET ADD NOMINATIONS_SPRING_TERM DATE;
ALTER TABLE EWP.EWPCV_FACT_SHEET ADD APPLICATION_AUTUM_TERM DATE;
ALTER TABLE EWP.EWPCV_FACT_SHEET ADD APPLICATION_SPRING_TERM DATE;

ALTER TABLE EWP.EWPCV_ACADEMIC_TERM MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_ACADEMIC_YEAR MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_CONTACT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_CONTACT_DETAILS MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_CREDIT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_DURATION MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_FACT_SHEET MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_FLEXIBLE_ADDRESS MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_GRADING_SCHEME MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_IIA MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_IIA_PARTNER MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_INSTITUTION MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_LANGUAGE_ITEM MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_LEARNING_AGREEMENT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_LOI MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_LOS MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_MOBILITY MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_MOBILITY_NUMBER MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_MOBILITY_PARTICIPANT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_MOBILITY_TYPE MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_NOTIFICATION MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_ORGANIZATION_UNIT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_PERSON MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_PHONE_NUMBER MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_RESULT_DISTRIBUTION MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_RESULT_DIST_CATEGORY MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_LA_COMPONENT MODIFY VERSION DEFAULT 0;
ALTER TABLE EWP.EWPCV_INFORMATION_ITEM MODIFY VERSION DEFAULT 0;

CREATE TABLE EWP.EWPCV_COMPONENT_CREDITS (
   COMPONENT_ID VARCHAR2(255 CHAR) NOT NULL,
   CREDITS_ID VARCHAR2(255 CHAR) NOT NULL
);

ALTER TABLE EWP.EWPCV_COMPONENT_CREDITS 
   ADD CONSTRAINT UK_COMPONENT_CREDITS_1 UNIQUE (CREDITS_ID);
   
ALTER TABLE EWP.EWPCV_COMPONENT_CREDITS 
   ADD CONSTRAINT FK_COMPONENT_CREDITS_1
   FOREIGN KEY (CREDITS_ID) 
   REFERENCES EWP.EWPCV_CREDIT;

ALTER TABLE EWP.EWPCV_COMPONENT_CREDITS 
   ADD CONSTRAINT FK_COMPONENT_CREDITS_2
   FOREIGN KEY (COMPONENT_ID) 
   REFERENCES EWP.EWPCV_LA_COMPONENT;

ALTER TABLE  EWP.EWPCV_LA_COMPONENT 
	ADD CONSTRAINT FK_LA_COMPONENT_3
	FOREIGN KEY (ACADEMIC_TERM_DISPLAY_NAME) REFERENCES  EWP.EWPCV_ACADEMIC_TERM (ID);


ALTER TABLE EWP.EWPCV_MOBILITY DROP CONSTRAINT FK_MOBILITY_3;
ALTER TABLE EWP.EWPCV_MOBILITY DROP COLUMN LANGUAGE_SKILL;

CREATE TABLE EWP.EWPCV_MOBILITY_LANG_SKILL (
   MOBILITY_ID VARCHAR2(255 CHAR) NOT NULL,
   MOBILITY_REVISION NUMBER(10,0) NOT NULL,
   LANGUAGE_SKILL_ID VARCHAR2(255 CHAR) NOT NULL
);

ALTER TABLE  EWP.EWPCV_MOBILITY_LANG_SKILL 
	ADD CONSTRAINT FK_MOBILITY_LANG_SKILL_1
	FOREIGN KEY (MOBILITY_ID, MOBILITY_REVISION) references EWP.EWPCV_MOBILITY (ID, MOBILITY_REVISION);
	
ALTER TABLE  EWP.EWPCV_MOBILITY_LANG_SKILL 
		ADD CONSTRAINT FK_MOBILITY_LANG_SKILL_2
		FOREIGN KEY (LANGUAGE_SKILL_ID) references EWP.EWPCV_LANGUAGE_SKILL (ID);

CREATE TABLE EWP.EWPCV_COOPCOND_EQFLVL (
   COOPERATION_CONDITION_ID VARCHAR2(255 CHAR) NOT NULL,
   EQF_LEVEL NUMBER(3,0) NOT NULL
);

ALTER TABLE  EWP.EWPCV_COOPCOND_EQFLVL 
	ADD CONSTRAINT FK_COOPCOND_EQFLVL_1
	FOREIGN KEY (COOPERATION_CONDITION_ID) references EWP.EWPCV_COOPERATION_CONDITION (ID);
	
ALTER TABLE EWP.EWPCV_DURATION DROP COLUMN UNIT;
ALTER TABLE EWP.EWPCV_DURATION ADD UNIT NUMBER(1,0);
COMMENT ON COLUMN EWP.EWPCV_DURATION.UNIT IS '0:HOURS, 1:DAYS, 2:WEEKS, 3:MONTHS, 4:YEARS';

ALTER TABLE EWP.EWPCV_MOBILITY_NUMBER DROP COLUMN VARIANT;
ALTER TABLE EWP.EWPCV_MOBILITY_NUMBER ADD VARIANT NUMBER(1,0);
COMMENT ON COLUMN EWP.EWPCV_MOBILITY_NUMBER.VARIANT IS '0:AVERAGE, 1:TOTAL';

ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION ADD BLENDED NUMBER (1,0);
ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION DROP COLUMN EQF_LEVEL;

ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST
ADD IS_PROCESSING NUMBER(1,0);

ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST
ADD MOBILITY_TYPE NUMBER(10,0);

COMMENT ON COLUMN EWP.EWPCV_MOBILITY_UPDATE_REQUEST.MOBILITY_TYPE IS '0: Outgoing || 1: Incoming';

ALTER TABLE EWP.EWPCV_LEARNING_AGREEMENT
ADD COMMENT_REJECT NUMBER(1,0);

ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST DROP COLUMN UPDATE_INFORMATION;
ALTER TABLE EWP.EWPCV_MOBILITY_UPDATE_REQUEST ADD UPDATE_INFORMATION BLOB;

ALTER TABLE EWP.EWPCV_LEARNING_AGREEMENT DROP COLUMN PDF;
ALTER TABLE EWP.EWPCV_LEARNING_AGREEMENT ADD PDF BLOB;

ALTER TABLE EWP.EWPCV_IIA DROP COLUMN PDF;
ALTER TABLE EWP.EWPCV_IIA ADD PDF BLOB;
	
GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_COMPONENT_CREDITS TO APLEWP;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_COMPONENT_CREDITS FOR EWP.EWPCV_COMPONENT_CREDITS;

GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_MOBILITY_LANG_SKILL TO APLEWP;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_MOBILITY_LANG_SKILL FOR EWP.EWPCV_MOBILITY_LANG_SKILL;

GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_COOPCOND_EQFLVL TO APLEWP;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_COOPCOND_EQFLVL FOR EWP.EWPCV_COOPCOND_EQFLVL;

GRANT SELECT,INSERT,UPDATE,DELETE ON EWP.EWPCV_COOPCOND_EQFLVL TO APLEWP;
CREATE OR REPLACE SYNONYM APLEWP.EWPCV_COOPCOND_EQFLVL FOR EWP.EWPCV_COOPCOND_EQFLVL;

CREATE OR REPLACE FUNCTION EWP.GENERATE_UUID RETURN VARCHAR2 AS 
 v_return VARCHAR2(255 CHAR);
BEGIN
	SELECT regexp_replace(rawtohex(sys_guid()), 
    '([A-F0-9]{8})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{4})([A-F0-9]{12})', '\1-\2-\3-\4-\5') 
    INTO v_return FROM DUAL;
    return v_return;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_INSTITUCION(P_INSTITUTION_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_INSTITUTION I,
		EWP.EWPCV_INSTITUTION_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE I.ID = N.INSTITUTION_ID
		AND N.NAME_ID = LAIT.ID
		AND I.INSTITUTION_ID = p_id;
BEGIN
	OPEN C(P_INSTITUTION_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ORGANIZATION_UNIT O,
		EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE O.ID = N.ORGANIZATION_UNIT_ID
		AND N.NAME_ID = LAIT.ID
		AND O.ID = p_id;
BEGIN
	OPEN C(P_OUNIT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_NAME CN,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CN.CONTACT_ID
		AND CN.NAME_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPCIONES_CONTACTO(P_CONTACTO_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT C,
		EWP.EWPCV_CONTACT_DESCRIPTION CD,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE C.ID = CD.CONTACT_ID
		AND CD.DESCRIPTION_ID = LAIT.ID
		AND C.ID = p_id;
BEGIN
	OPEN C(P_CONTACTO_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_URLS_CONTACTO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_CONTACT_URL U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE CD.ID = U.CONTACT_DETAILS_ID
		AND U.URL_ID = LAIT.ID
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_EMAILS(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EMAIL, ';')
		WITHIN GROUP (ORDER BY EMAIL)
    FROM EWP.EWPCV_CONTACT_DETAILS_EMAIL 
    WHERE CONTACT_DETAILS_ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_TELEFONO(P_CONTACT_DETAILS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(P.E164 || ':' || P.EXTENSION_NUMBER || ':' || P.OTHER_FORMAT, ';')
		WITHIN GROUP (ORDER BY P.E164)
    FROM EWP.EWPCV_CONTACT_DETAILS CD,
		EWP.EWPCV_PHONE_NUMBER P
    WHERE P.ID = CD.PHONE_NUMBER
		AND CD.ID = p_id;
BEGIN
	OPEN C(P_CONTACT_DETAILS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS_LINES(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(ADDRESS_LINE, ';')
		WITHIN GROUP (ORDER BY ADDRESS_LINE)
    FROM EWP.EWPCV_FLEXIBLE_ADDRESS_LINE 
    WHERE FLEXIBLE_ADDRESS_ID = p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ADDRESS(P_FLEXIBLE_ADDRESS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(F.BUILDING_NUMBER|| ':' || F.BUILDING_NAME || ':' || F.STREET_NAME 
        || ':' || F.UNIT || ':' || F."FLOOR" || ':' || F.POST_OFFICE_BOX 
        || ':' || (
        SELECT LISTAGG(Delivery_point_code, ',') 
			WITHIN GROUP (ORDER BY Delivery_point_code)
        FROM EWPCV_FLEXAD_DELIV_POINT_COD
        WHERE FLEXIBLE_ADDRESS_ID = F.ID
        ), ';')
		WITHIN GROUP (ORDER BY F.STREET_NAME)
    FROM EWPCV_FLEXIBLE_ADDRESS F
    WHERE F.ID =  p_id;
BEGIN
	OPEN C(P_FLEXIBLE_ADDRESS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NIVELES_IDIOMAS(P_MOBILITY_ID IN VARCHAR2, P_REVISION IN NUMBER) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2, p_revision IN NUMBER) IS  SELECT LISTAGG(S."LANGUAGE"|| ':' || S.CEFR_LEVEL , ';')
		WITHIN GROUP (ORDER BY S."LANGUAGE")
    FROM EWP.EWPCV_MOBILITY_LANG_SKILL MLS,
		EWP.EWPCV_LANGUAGE_SKILL S
    WHERE MLS.LANGUAGE_SKILL_ID = S.ID
		AND MLS.MOBILITY_ID = p_id
		AND MLS.MOBILITY_REVISION = p_revision;
BEGIN
	OPEN C(P_MOBILITY_ID, P_REVISION);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_FIRMAS(P_FIRMA_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(SIGNER_NAME || ':' || SIGNER_POSITION 
        || ':' || SIGNER_EMAIL || ':' || "TIMESTAMP" || ':' || SIGNER_APP, ';')
			WITHIN GROUP (ORDER BY SIGNER_NAME)
    FROM EWP.EWPCV_SIGNATURE
    WHERE ID = p_id;
BEGIN
	OPEN C(P_FIRMA_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_CREDITS(P_COMPONENT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(CR.CREDIT_LEVEL || ':' || CR.SCHEME || ':' || CR.CREDIT_VALUE, ';')
		WITHIN GROUP (ORDER BY CR.CREDIT_LEVEL)
    FROM EWP.EWPCV_CREDIT CR,
		EWP.EWPCV_COMPONENT_CREDITS CCR
    WHERE CR.ID = CCR.CREDITS_ID
		AND CCR.COMPONENT_ID = p_id;
BEGIN
	OPEN C(P_COMPONENT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(TO_CHAR(ATR.START_DATE, 'YYYY.MM.DD')  || ':' || TO_CHAR(ATR.END_DATE, 'YYYY.MM.DD') 
		|| ':' || ATR.INSTITUTION_ID || ':' || o.ORGANIZATION_UNIT_CODE 
		|| ':' || ATR.TERM_NUMBER || ':' || ATR.TOTAL_TERMS, ';')
		WITHIN GROUP (ORDER BY ATR.ID )
    FROM EWP.EWPCV_ACADEMIC_TERM ATR
		LEFT JOIN EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
    WHERE ATR.ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_ACADEMIC_TERM(P_AT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ACADEMIC_TERM_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.DISP_NAME_ID = LAIT.ID
		AND N.ACADEMIC_TERM_ID = p_id;
BEGIN
	OPEN C(P_AT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_NOMBRES_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE N.NAME_ID = LAIT.ID
		AND N.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_DESCRIPTIONS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_DESCRIPTION D,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE D.DESCRIPTION_ID = LAIT.ID
		AND D.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_URLS_LOS(P_LOS_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || ':' || LAIT."LANG", ';')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_LOS_URLS U,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE U.URL_ID = LAIT.ID
		AND U.LOS_ID = p_id;
BEGIN
	OPEN C(P_LOS_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_S_AREA_L_SKILL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(S.ISCED_CODE || ':' || S.ISCED_CLARIFICATION || ':' || L."LANGUAGE" || ':' ||L.CEFR_LEVEL, ';')
		WITHIN GROUP (ORDER BY S.ISCED_CODE)
    FROM EWP.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
		LEFT JOIN EWP.EWPCV_LANGUAGE_SKILL L	
			ON CSL.LANGUAGE_SKILL_ID = L.ID
		LEFT JOIN EWP.EWPCV_SUBJECT_AREA S	
			ON CSL.ISCED_CODE = S.ISCED_CODE	
    WHERE CSL.COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE FUNCTION EWP.EXTRAE_EQF_LEVEL(P_COOP_CON_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(2000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(EQF_LEVEL, ';')
		WITHIN GROUP (ORDER BY EQF_LEVEL)
    FROM EWP.EWPCV_COOPCOND_EQFLVL
    WHERE COOPERATION_CONDITION_ID = p_id;
BEGIN
	OPEN C(P_COOP_CON_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/
CREATE OR REPLACE TYPE EWP.LANGUAGE_ITEM AS OBJECT
  (
	/*Objeto que representa un texto en un idioma determinado
		LANG 				Codigo del idioma en formato BCP 47 (en, es, en-US).
		TEXT					Texto en el idioma indicado.
	*/
	LANG 					VARCHAR2(255 CHAR),
	TEXT					VARCHAR2(255 CHAR)
  );
/  
CREATE OR REPLACE TYPE EWP.LANGUAGE_ITEM_LIST AS TABLE OF EWP.LANGUAGE_ITEM;
/
CREATE OR REPLACE TYPE EWP.SUBJECT_AREA AS OBJECT
  (
	/* Informacion del area de ensenyanza de un programa de estudio
		ISCED_CODE*				Codigo de educacion ISCED-F
		ISCED_CLARIFICATION		Informacion adicional relativa al codigo ISCED-F
	*/ 
    ISCED_CODE				VARCHAR(255 CHAR),
	ISCED_CLARIFICATION		VARCHAR(255 CHAR)
  );
/
CREATE OR REPLACE TYPE EWP.LANGUAGE_SKILL AS OBJECT
  (
	/* Informacion de niveles de idioma
		LANG*				Codigo del idioma en formato BCP 47 (en, es, en-US).
		CEFR_LEVEL*				Nivel de dominio del idioma
	*/ 
    LANG				VARCHAR(255 CHAR),
	CEFR_LEVEL				VARCHAR(255 CHAR)
  );
/
CREATE OR REPLACE TYPE EWP.LANGUAGE_SKILL_LIST AS TABLE OF EWP.LANGUAGE_SKILL;
/ 
CREATE OR REPLACE TYPE EWP.ACADEMIC_TERM AS OBJECT
  (
	/*Informacion referente a un periodo academico
		ACADEMIC_YEAR*			Anyo en que tiene lugar el periodo academico.
		INSTITUTION_ID*			Identificador de la institucion asociada al periodo academico
		ORGANIZATION_UNIT_CODE	CODIGO de la unidad organizativa asociada al periodo academicoVARCHAR2(255 CHAR),
		START_DATE				Fecha de inicio del periodo academico,
		END_DATE				Fecha de fin del periodo academico,,
		DESCRIPTION				Nombre del periodo academico, admite una lista de nombres en varios idiomas
		TERM_NUMBER*			Numero del periodo en un anyo, si es el primer semestre sera 1, si es el segundo ser� 2, etc
		TOTAL_TERMS*			Cantidad total de periodos de este tipo comprendidos en un anyo, si son semestres sera 2, si son trimestres ser� 3, etc...
	*/
	ACADEMIC_YEAR			VARCHAR2(255 CHAR),
	INSTITUTION_ID			VARCHAR2(255 CHAR),
	ORGANIZATION_UNIT_CODE	VARCHAR2(255 CHAR),
	START_DATE				DATE,
	END_DATE				DATE,
	DESCRIPTION				EWP.LANGUAGE_ITEM_LIST,
	TERM_NUMBER				NUMBER(10,0),
	TOTAL_TERMS				NUMBER(10,0)
  );
/
CREATE OR REPLACE TYPE EWP.PHONE_NUMBER AS OBJECT
  (
    /* Informacion de numeros de telefono. Campos obligatorios marcados con *.
		E164					Numero de telefono en formato E.164, con el + delante.
		EXTENSION_NUMBER		Extension telefonica.
		OTHER_FORMAT			Numero en otro formato si no fuese suficiente con los otros campos.
	*/
    E164					VARCHAR2(255 CHAR),
	EXTENSION_NUMBER		VARCHAR2(255 CHAR),
	OTHER_FORMAT			VARCHAR2(255 CHAR)
  );
/  
CREATE OR REPLACE TYPE EWP.PHONE_NUMBER_LIST AS TABLE OF EWP.PHONE_NUMBER;
/
CREATE OR REPLACE TYPE EWP.RECIPIENT_NAME_LIST AS TABLE OF VARCHAR2(255 CHAR);
/
CREATE OR REPLACE TYPE EWP.ADDRESS_LINE_LIST AS VARRAY(4) OF VARCHAR2(255 CHAR);
/
CREATE OR REPLACE TYPE EWP.DELIVERY_POINT_CODE_LIST AS TABLE OF VARCHAR2(255 CHAR);
/
CREATE OR REPLACE TYPE EWP.FLEXIBLE_ADDRESS AS OBJECT
  (
	/*	Informacion relativa a una direccion. Campos obligatorios marcados con *.
		RECIPIENT_NAMES			Nombres que se asignan a la direccion
		ADDRESS_LINES			Lineas que componen una direccion, pueden ser hasta 4.
								Contienen la informacion de la direccion (calle, numero, portal, etc...) agregada y separada por comas o lineas
								Se puede informar este campo o los 7 siguientes.
		BUILDING_NUMBER			 Numero del edificio en la calle.
		BUILDING_NAME			 Nombre del edificio en la calle.
		STREET_NAME				 Nombre de la calle.
		UNIT					 Numero de apartamento u oficina.
		FLOOR					 Piso.
		POST_OFFICE_BOX			 Apartado de correos.
		DELIVERY_POINT_CODES	 Identificador del buzon.
		POSTAL_CODE*			Codigo postal
		LOCALITY				Localidad.
		REGION					Provincia.
		COUNTRY*				Pais.
	*/
	RECIPIENT_NAMES			EWP.RECIPIENT_NAME_LIST,
    ADDRESS_LINES			EWP.ADDRESS_LINE_LIST ,
	BUILDING_NUMBER			VARCHAR2(255 CHAR),
	BUILDING_NAME			VARCHAR2(255 CHAR),
	STREET_NAME				VARCHAR2(255 CHAR),
	UNIT					VARCHAR2(255 CHAR),
	BUILDING_FLOOR			VARCHAR2(255 CHAR),
	POST_OFFICE_BOX			VARCHAR2(255 CHAR),
	DELIVERY_POINT_CODES	EWP.DELIVERY_POINT_CODE_LIST,
	POSTAL_CODE				VARCHAR2(255 CHAR),
	LOCALITY				VARCHAR2(255 CHAR),
	REGION					VARCHAR2(255 CHAR),
	COUNTRY					VARCHAR2(255 CHAR)
  );
/  
CREATE OR REPLACE TYPE EWP.EMAIL_LIST AS TABLE OF VARCHAR2(255 CHAR);
/
CREATE OR REPLACE TYPE EWP.CONTACT AS OBJECT
(
	/*Datos del contacto. Campos obligatorios marcados con *.
		CONTACT_NAME			Nombre del contacto, admite una lista de textos multiidioma
		CONTACT_DESCRIPTION		Descripcion del contacto, admite una lista de textos multiidioma
		CONTACT_URL				URL del contacto, admite una lista de textos multiidioma
		CONTACT_ROLE			Cargo asignado al contacto.
		INSTITUTION_ID			Institucion relacionada con el contacto.
		ORGANIZATION_UNIT_CODE	Unidad organizativa relacionada con el contacto.
		EMAIL*					Lista de emails
								Obligatorio para las personas de contacto de los LAs
		STREET_ADDRESS			Direccion
		PHONE					Numero de telefono
	*/
	CONTACT_NAME			EWP.LANGUAGE_ITEM_LIST,
	CONTACT_DESCRIPTION		EWP.LANGUAGE_ITEM_LIST,
	CONTACT_URL				EWP.LANGUAGE_ITEM_LIST,
	CONTACT_ROLE			VARCHAR2(255 CHAR),
	INSTITUTION_ID			VARCHAR2(255 CHAR),
	ORGANIZATION_UNIT_CODE	VARCHAR2(255 CHAR),
	EMAIL					EWP.EMAIL_LIST,
	STREET_ADDRESS			EWP.FLEXIBLE_ADDRESS,
	PHONE					EWP.PHONE_NUMBER
);
/
CREATE OR REPLACE TYPE EWP.CONTACT_PERSON AS OBJECT
  (
	/*Datos del estudiante. Campos obligatorios marcados con *.
		GIVEN_NAME*				Nombre de la persona
								Obligatorio para las personas de contacto de los LAs
		FAMILY_NAME*			Apellidos de la persona
								Obligatorio para las personas de contacto de los LAs
		BIRTH_DATE				Fecha de nacimiento de la persona
		CITIZENSHIP			    Codigo del pais del que la persona depende administrativamente
		GENDER					Genero de la persona 
									0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
		CONTACT					Informacion de contacto de la persona
	*/
	GIVEN_NAME				VARCHAR2(255 CHAR),
	FAMILY_NAME				VARCHAR2(255 CHAR),
	BIRTH_DATE				DATE,
	CITIZENSHIP				VARCHAR2(255 CHAR),
	GENDER					NUMBER(10,0),
	CONTACT					EWP.CONTACT
  );
/ 
CREATE OR REPLACE TYPE EWP.CONTACT_PERSON_LIST AS TABLE OF CONTACT_PERSON;
/
CREATE OR REPLACE TYPE EWP.INSTITUTION AS OBJECT
  (
	/*Datos de institucion. Campos obligatorios marcados con *.
		INSTITUTION_ID*				Identificador de la institucion en el entorno ewp (HEI_ID)
		INSTITUTION_NAME	    	Nombre de la institucion, admite una lista de nombres en varios idiomas
		ORGANIZATION_UNIT_NAME		Nombre de la unidad organizativa, admite una lista de nombres en varios idiomas
		ORGANIZATION_UNIT_CODE*		Identificador de la unidad organizativa en el entorno ewp
	*/	
	INSTITUTION_ID			VARCHAR(255 CHAR),
	INSTITUTION_NAME		EWP.LANGUAGE_ITEM_LIST,
	ORGANIZATION_UNIT_NAME	EWP.LANGUAGE_ITEM_LIST,
	ORGANIZATION_UNIT_CODE	VARCHAR(255 CHAR)
  );
/
CREATE OR REPLACE PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en oto�o
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en oto�o
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  
	
	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;

	
	  
	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;
	
	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE					VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );
	  
	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );
	
	-- FUNCIONES 
	
	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
	
	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, LA IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	
END PKG_FACTSHEET;
/
create or replace PACKAGE EWP.PKG_IIAS AS 

	-- TIPOS DE DATOS
	
	/*Datos del tipo de movilidad. Campos obligatorios marcados con *.
			MOBILITY_CATEGORY*		Categor�a de la movilidad, puede ser  teaching, studies, training, 
			MOBILITY_GROUP*			Grupo en que se engloba la movilidad puede ser student o staff
	*/  
	TYPE MOBILITY_TYPE IS RECORD
	  (
		MOBILITY_CATEGORY			VARCHAR2(255 CHAR),
		MOBILITY_GROUP				VARCHAR(255 CHAR)
	  ); 

	/*Informacion sobre cada una de las partes del IIA. Campos obligatorios marcados con *.
			INSTITUTION	*				Institucion a la que representa el partner
			SIGNING_DATE				Fecha en la que esta parte firmo el acuerdo
			SIGNER_PERSON				Datos de la persona que firmo el acuerdo en representacion de esta parte
			PARTNER_CONTACTS			Lista de personas de contacto de esta parte del acuerdo
	*/  
	TYPE PARTNER IS RECORD
	  (
		INSTITUTION					EWP.INSTITUTION,
		SIGNING_DATE				DATE,
		SIGNER_PERSON				EWP.CONTACT_PERSON,
		PARTNER_CONTACTS			EWP.CONTACT_PERSON_LIST
	  );

	/*Informacion sobre las areas de ensenyanza y el idioma que se recomienda para cada una.
		Campos obligatorios marcados con *.
			SUBJECT_AREA	 			Areas de ensenyanza
			LANGUAGE_SKILL				Idioma que se recomienda para el area indicada
	*/  
	TYPE SUBJECT_AREA_LANGUAGE IS RECORD
	  (
		SUBJECT_AREA	 			EWP.SUBJECT_AREA,
		LANGUAGE_SKILL	 			EWP.LANGUAGE_SKILL
	  );
	  
	/* Lista con todas ensenyanza asociadas a una condicion de cooperacion y el idioma que se recomienda para cada una. */  
	TYPE SUBJECT_AREA_LANGUAGE_LIST IS TABLE OF SUBJECT_AREA_LANGUAGE INDEX BY BINARY_INTEGER ;

	/*Informacion sobre la duracion de una condicion de cooperacion
		Campos obligatorios marcados con *.
			NUMBER_DURATION	 			Cantidad de las unidades indicadas que dura la condicion de cooperacion.
			UNIT	 					Unidades en que se expresa la duracion de la condicion de cooperacion.
										0:HOURS, 1:DAYS, 2:WEEKS, 3:MONTHS, 4:YEARS
	*/  
	TYPE DURATION IS RECORD
	  (
		NUMBER_DURATION	 			NUMBER(10,0),
		UNIT	 					NUMBER(1,0)
	  );	  

	/* Lista de niveles de estudios relacionado con la condicion de cooperacion  */  
	TYPE EQF_LEVEL_LIST IS TABLE OF NUMBER(3,0) INDEX BY BINARY_INTEGER;  
	  
	/*Informacion sobre cada uno de las condiciones de cooperacion de un acuerdo interinstitucional. 
		START_DATE *				Fecha en que comienza la vigencia de la condicion de cooperacion
		END_DATE *					Fecha en que termina la vigencia de la condicion de cooperacion
		EQF_LEVEL_LIST *			Niveles de estudios contemplados en la condicion de cooperacion 
		MOBILITY_NUMBER	*			Numero de participantes maximo que se puede beneficiar de esta condicion de cooperacion
		MOBILITY_TYPE *				Tipo de movilidad, indica si esde estudiantes o profesores y la actividad a desarrollar
		RECEIVING_PARTNER *			Informacion sobre la institucion destino
		SENDING_PARTNER *			Informacion sobre la institucion origen
        SUBJECT_AREA_LANGUAGE_LIST*	Informacion sobre areas de aprendizaje e idiomas relacionados a la condicion de cooperacion
        COP_COND_DURATION			Duracion de la condicion de cooperacion
		BLENDED						Indica si la condicion de cooperacion permite el formato de movilidad mixta
	*/	
	TYPE COOPERATION_CONDITION IS RECORD
	  (
		START_DATE					DATE,
		END_DATE					DATE,
		EQF_LEVEL_LIST       		PKG_IIAS.EQF_LEVEL_LIST,
		MOBILITY_NUMBER				NUMBER(10,0),
		MOBILITY_TYPE				PKG_IIAS.MOBILITY_TYPE,
		RECEIVING_PARTNER			PKG_IIAS.PARTNER,
		SENDING_PARTNER				PKG_IIAS.PARTNER,
		SUBJECT_AREA_LANGUAGE_LIST	PKG_IIAS.SUBJECT_AREA_LANGUAGE_LIST,
		COP_COND_DURATION			PKG_IIAS.DURATION,
		BLENDED						NUMBER(1,0)
	  );

	/* Lista con las condiciones de cooperacion de un acuerdo interinstitucional. */  
	TYPE COOPERATION_CONDITION_LIST IS TABLE OF COOPERATION_CONDITION INDEX BY BINARY_INTEGER ;


	/* Objeto que modela un acuerdo interinstitucional. Campos obligatorios marcados con *.
			START_DATE *					Fecha en que comienza la vigencia del IIA
			END_DATE *						Fecha en que termina la vigencia del IIA
			IIA_CODE						Codigo del IIA
			COOPERATION_CONDITION_LIST * 	Lista de condiciones de coperacion del IIA
			PDF								Documento pdf
	*/
	TYPE IIA IS RECORD
	  (
		START_DATE						DATE,
		END_DATE						DATE,
		IIA_CODE						VARCHAR2(255 CHAR),
		COOPERATION_CONDITION_LIST 		PKG_IIAS.COOPERATION_CONDITION_LIST,
		PDF								CLOB
	  );

	-- FUNCIONES 

	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_IIA(P_IIA IN IIA,  P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_IIAS;
/
CREATE OR REPLACE PACKAGE EWP.PKG_MOBILITY_LA AS 

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  
	
	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
	*/	
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_CODE				   VARCHAR2(255 CHAR),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR)
	  );
	  
	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );
	 
	/*Datos del estudiante. Campos obligatorios marcados con *.
			GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
			CONTACT					Informacion del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON
	  );	 

	/* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
			SENDING_INSTITUTION*		Datos de la instituci�n que envia al estudiante.
			RECEIVING_INSTITUTION*		Datos de la instituci�n que recibe al estudiante.
			ACTUAL_ARRIVAL_DATE			Fecha real de llegada del estudiante aldestino.
			ACTUAL_DEPATURE_DATE		Fecha real de salida del estudiante del origen.
			PLANED_ARRIVAL_DATE*		Fecha prevista de llegada del estudiante aldestino.
			PLANED_DEPATURE_DATE*		Fecha prevista de salida del estudiante del origen.
			IIA_CODE*					Codigo del IIA asociado a la movilidad
			COOPERATION_CONDITION_ID*	identificador de la condicion de coperacion asociada la movilidad
			STUDENT*					Datos del estudiante
			EQF_LEVEL*       			Nivel de estudios del estudiante.	
			SUBJECT_AREA*				Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
			STUDENT_LANGUAGE_SKILLS*	Niveles de idiomas que declara el estudiante.
			SENDER_CONTACT*				Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
			SENDER_ADMV_CONTACT			Informacion de contacto de la administrativo en la institucion origen
			RECEIVER_CONTACT*			Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
			RECEIVER_ADMV_CONTACT		Informacion de contacto de la administrativo en la institucion destino
			MOBILITY_TYPE				Tipo de movilidad, puede tomar los siquientes valores:
										Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
			STATUS*						Estado de la nominacion
										0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED, 4- REJECTED;
	*/
	TYPE MOBILITY IS RECORD
	  (
		SENDING_INSTITUTION  		EWP.INSTITUTION,
		RECEIVING_INSTITUTION		EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE			DATE,
		ACTUAL_DEPATURE_DATE		DATE,
		PLANED_ARRIVAL_DATE			DATE,
		PLANED_DEPATURE_DATE		DATE,
		IIA_CODE					VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID	VARCHAR(255 CHAR),
		STUDENT						PKG_MOBILITY_LA.STUDENT,
		EQF_LEVEL       			NUMBER(3,0),	
		SUBJECT_AREA				EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS		EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT				EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT		EWP.CONTACT_PERSON,
		MOBILITY_TYPE				VARCHAR2 (255),
		STATUS						NUMBER(10,0)
	  );

	/* Objeto que modela un acuerdo de ense�anza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					DATE,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					CLOB
	  );
	  
	/* Objeto que modela un acuerdo de ense�anza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisi�n del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );
	
	-- FUNCIONES 
	
	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estar� registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar �nicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
		
	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
	
	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar �nicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER;  
	
	/* Aprueba una revision de un learning agreement, se emplear� para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se emplear� para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	
	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se emplear� para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER; 
	
	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER;  	
	
END PKG_MOBILITY_LA;
/
/*
	Obtiene la informacion general de los IIAs
	Se emplear� para saber que IIAs se han modificado y se deben de actualizar
*/
CREATE OR REPLACE view EWP.IIAS_VIEW AS SELECT 
	ID                   AS IIA_ID,
	IIA_CODE             AS IIA_CODE,
	REMOTE_IIA_ID        AS REMOTE_IIA_ID,
	REMOTE_IIA_CODE      AS REMOTE_IIA_CODE,
	START_DATE           AS IIA_START_DATE,
	END_DATE             AS IIA_END_DATE,
	MODIFY_DATE          AS IIA_MODIFY_DATE,
	APPROVAL_DATE        AS IIA_APPROVAL_DATE
FROM EWP.EWPCV_IIA;
/
/*
	Obtiene la informacion de las condiciones de cooperacion de los IIAS 
	Se emplear� para recuperar el detalle de un IIA
*/
CREATE OR REPLACE view EWP.IIA_COOP_CONDITIONS_VIEW AS SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	EXTRAE_EQF_LEVEL(CC.ID)                                                                                AS COOP_COND_EQF_LEVEL,
	D.NUMBERDURATION || ':' ||D.UNIT                                                                       AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	MT.MOBILITY_CATEGORY ||':'|| MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                           AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                          AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)        AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                          AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                       AS RECEIVER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                               AS RECEIVER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                         AS RECEIVER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(RPSC.ID)                                                                                 AS RECEIVER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                               AS RECEIVER_SIGNER_PHONES,
	EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                             AS RECEIVER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                                   AS RECEIVER_SIGNER_ADDRESS,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                          AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)        AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                          AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                       AS SENDER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                               AS SENDER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                         AS SENDER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(SPSC.ID)                                                                                 AS SENDER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                               AS SENDER_SIGNER_PHONES,
	EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                             AS SENDER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                                   AS SENDER_SIGNER_ADDRESS,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM EWP.EWPCV_IIA I
INNER JOIN EWP.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN EWP.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN EWP.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN EWP.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN EWP.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN EWP.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN EWP.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN EWP.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN EWP.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN EWP.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
/
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.COOP_COND_EQF_LEVEL IS 'FORMATO: EQF_LEVEL;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.COOP_COND_DURATION IS 'FORMATO: NUMBER:UNIT';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.MOBILITY_TYPE IS 'FORMATO: CATEGORY:GROUP';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SUBJECT_AREAS IS 'FORMATO: ISCED_CODE:ISCED_CLARIFICATION:LANGUAGE:CEFR_LVL;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.RECEIVER_SIGNER_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.IIA_COOP_CONDITIONS_VIEW.SENDER_SIGNER_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';
/
/*
	Obtiene la informacion de las movilidades de las personas involucradas en la misma. 
	Para consultar las outgoing filtraremos por M.SENDING_INSTITUTION_ID = nuestro SCHAC
	Para consultar las incoming filtraremos por M.RECEIVING_INSTITUTION_ID = nuestro SCHAC
*/
CREATE OR REPLACE view EWP.MOBILITY_VIEW AS SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL                                                                                                AS EQF_LEVEL,
	I.IIA_CODE                                                                                                 AS IIA_CODE,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE || ':' || SA.ISCED_CLARIFICATION                                                             AS SUBJECT_AREA,
	M.STATUS                                                                                                   AS MOBILITY_STATUS,
	MT.MOBILITY_CATEGORY || ':' || MT.MOBILITY_GROUP                                                           AS MOBILITY_TYPE,
	EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.ID                                                                                                      AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY	
FROM EWP.EWPCV_MOBILITY M
INNER JOIN EWP.EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ISCED_CODE
INNER JOIN EWP.EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWP.EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWP.EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWP.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWP.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
INNER JOIN EWP.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
INNER JOIN EWP.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
INNER JOIN EWP.EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
INNER JOIN EWP.EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
INNER JOIN EWP.EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
INNER JOIN EWP.EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWP.EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWP.EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWP.EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWP.EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
LEFT JOIN EWP.EWPCV_IIA I
	ON M.IIA_ID = I.ID
;
/
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SUBJECT_AREA IS 'FORMATO: ISCED_CODE:CLARIFICATION';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_SEN_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVER_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_DESCS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_CONT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_PHONES IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONT_ADDR_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.ADMV_REC_CONTACT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODE1,DELIVERY_POINT_CODE2...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.MOBILITY_VIEW.SENDING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
/
/*
	Obtiene la informacion de los learning agreements. Se emplear� para identificar que learning agreements se han modificado.
	La informacion del estudiante en esta tabla solo estar� presente cuando suponga un cambio sobre la informacion original que figura en la movilidad.
*/
CREATE OR REPLACE view EWP.LEARNING_AGREEMENT_VIEW AS SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.COMMENT_REJECT						   AS COMMENT_RECJECT,
	EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	M.MOBILITY_PARTICIPANT_ID                  AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM EWP.EWPCV_MOBILITY M
INNER JOIN EWP.EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN EWP.EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN EWP.EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN EWP.EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN EWP.EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN EWP.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWP.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWP.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWP.EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWP.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS;
/
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_DESCRIPTIONS IS 'FORMATO: CONTACT_DESC:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_CONTACT_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_EMAILS IS 'FORMATO: EMAIL1;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT;...';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS_LINES IS 'FORMATO: ADDRES LINE1;...;ADDRES LINE4';
COMMENT ON COLUMN EWP.LEARNING_AGREEMENT_VIEW.STUDENT_ADDRESS IS 'FORMATO: BUILDING_NUMBER:BUILDING_NAME:STREET_NAME:UNIT:FLOOR:POST_OFFICE_BOX:DELIVERY_POINT_CODES';
/
/*
	Obtiene la informacion de los componentes asociados a los learning agreements.
*/
CREATE OR REPLACE view EWP.LA_COMPONENTS_VIEW AS SELECT 
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	EXTRAE_CREDITS(C.id)                            AS CREDITS,
	EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	AY.START_YEAR || ':' || AY.END_YEAR             AS ACADEMIC_YEAR,
	C.STATUS                                        AS STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONITIONS,
	C.LOS_CODE                                      AS LOS_CODE,
	LOS.INSTITUTION_ID                              AS INSTITUTION_ID,
	EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION
FROM EWP.EWPCV_LEARNING_AGREEMENT LA
LEFT JOIN EWP.EWPCV_STUDIED_LA_COMPONENT SC 
    ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_RECOGNIZED_LA_COMPONENT RC 
    ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_VIRTUAL_LA_COMPONENT VC 
    ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_BLENDED_LA_COMPONENT BC 
    ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_DOCTORAL_LA_COMPONENT DC 
    ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN EWP.EWPCV_LA_COMPONENT C
    ON C.ID = SC.STUDIED_LA_COMPONENT_ID
    OR C.ID = RC.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = VC.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = BC.BLENDED_LA_COMPONENT_ID
    OR C.ID = DC.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWP.EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWP.EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID    
LEFT JOIN EWP.EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID    
LEFT JOIN EWP.EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID
LEFT JOIN EWP.EWPCV_ORGANIZATION_UNIT O 
    ON LOS.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN EWP.EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID;
/
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.CREDITS IS 'FORMATO: LEVEL:SCHEME:VALUE;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_TERM IS 'FORMATO: STAR_DATE:END_DATE:INSTITUTION_ID:OUNIT_CODE:TERM_NUMBER:TOTAL_TERMS';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_TERM_NAMES IS 'FORMATO: CONTACT_NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.ACADEMIC_YEAR IS 'FORMATO: START_YEAR:END_YEAR';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.RECEIVING_INSTITUTION_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.RECEIVING_OUNIT_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_NAMES IS 'FORMATO: NAME:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_DESCRIPTIONS IS 'FORMATO: DESCRIPTION:LANG;...';
COMMENT ON COLUMN EWP.LA_COMPONENTS_VIEW.LOS_URL IS 'FORMATO: URL:LANG;...';
/
/*
	Obtiene la informacion obtenida de los servicios de diccionarios.
*/
CREATE OR REPLACE view EWP.DICTIONARIES_VIEW AS SELECT 
	ID 			AS ID,
	CODE 			AS CODE,
	DESCRIPTION 	AS DESCRIPTION,
	DICTIONARY_TYPE AS DICTIONARY_TYPE,
	CALL_YEAR 		AS DICTIONARY_YEAR
FROM EWP.EWPCV_DICTIONARY;
/
COMMENT ON COLUMN EWP.DICTIONARIES_VIEW.DICTIONARY_TYPE IS 'Posibles valores:key_actions, action_types, countries, country_regions, country_groups, distance_bands, education_fields, education_levels, field_groups, language_groups, languages, organization_types, seniorities, training_types, working_categories';
COMMENT ON COLUMN EWP.DICTIONARIES_VIEW.DICTIONARY_YEAR IS 'anyo pasado para la consulta de valores del diccionario';
/
/*
	Obtiene la informacion de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_VIEW AS SELECT 
	'HEI_ID' 							 AS INSTITUTION_ID,
	'ABREVIATION'	 					 AS ABREVIATION,
	0 									 AS DECISION_WEEK_LIMIT,
	0 									 AS TOR_WEEK_LIMIT,
	SYSDATE								 AS NOMINATIONS_AUTUM_TERM,
	SYSDATE								 AS NOMINATIONS_SPRING_TERM,
	SYSDATE								 AS APPLICATION_AUTUM_TERM,
	SYSDATE								 AS APPLICATION_SPRING_TERM,
	'EMAIL' 							 AS APPLICATION_EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS APPLICATION_PHONE,
	'CONTACT_URL:LANG;...' 				 AS APPLICATION_URLS,
	'EMAIL' 							 AS HOUSING_EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS HOUSING_PHONE,
	'CONTACT_URL:LANG;...' 				 AS HOUSING_URLS,
	'EMAIL' 							 AS VISA_EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS VISA_PHONE,
	'CONTACT_URL:LANG;...'				 AS VISA_URLS,
	'EMAIL'								 AS INSURANCE_EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS INSURANCE_PHONE,
	'CONTACT_URL:LANG;...'				 AS INSURANCE_URLS
FROM DUAL;
/
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.APPLICATION_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.APPLICATION_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.HOUSING_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.HOUSING_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.VISA_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.VISA_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.INSURANCE_PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_VIEW.INSURANCE_URLS IS 'FORMATO: CONTACT_URL:LANG;...';
/
/*
	Obtiene la informacion de contacto adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_ADD_INFO_VIEW AS SELECT 
	'HEI_ID' 		 					 AS INSTITUTION_ID,
	'TYPE'			 					 AS INFO_TYPE,
	'EMAIL'			 					 AS EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS PHONE,
	'CONTACT_URL:LANG;...'   			 AS URLS
FROM DUAL;
COMMENT ON COLUMN EWP.FACTSHEET_ADD_INFO_VIEW.PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_ADD_INFO_VIEW.URLS IS 'FORMATO: CONTACT_URL:LANG;...';
/
/*
	Obtiene la informacion de contacto sobre requerimientos adicional de FACTSHEET
*/
CREATE OR REPLACE view EWP.FACTSHEET_REQ_INFO_VIEW AS SELECT 
	'HEI_ID' 							 AS INSTITUTION_ID,
	'TYPE' 								 AS REQ_TYPE,
	'NAME' 								 AS NAME,
	'DESCRIPTION' 						 AS DESCRIPTION,
	'EMAIL' 							 AS EMAIL,
	'E164:EXTENSION_NUMBER:OTHER_FORMAT' AS PHONE,
	'CONTACT_URL:LANG;...' 				 AS URLS
FROM DUAL;
/
COMMENT ON COLUMN EWP.FACTSHEET_REQ_INFO_VIEW.PHONE IS 'FORMATO: E164:EXTENSION_NUMBER:OTHER_FORMAT';
COMMENT ON COLUMN EWP.FACTSHEET_REQ_INFO_VIEW.URLS IS 'FORMATO: CONTACT_URL:LANG;...';
/

GRANT EXECUTE ON EWP.PKG_IIAS TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PKG_MOBILITY_LA TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PKG_FACTSHEET TO EWP_APROVISIONAMIENTO;

GRANT EXECUTE ON EWP.INSTITUTION TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT_PERSON_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT_PERSON TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.EMAIL_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.FLEXIBLE_ADDRESS TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.DELIVERY_POINT_CODE_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.ADDRESS_LINE_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.RECIPIENT_NAME_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PHONE_NUMBER_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PHONE_NUMBER TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.ACADEMIC_TERM TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_SKILL_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_SKILL TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.SUBJECT_AREA TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_ITEM_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_ITEM TO EWP_APROVISIONAMIENTO;

GRANT SELECT ON  EWP.IIAS_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.IIA_COOP_CONDITIONS_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.MOBILITY_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.LEARNING_AGREEMENT_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.LA_COMPONENTS_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.DICTIONARIES_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.FACTSHEET_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.FACTSHEET_ADD_INFO_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON  EWP.FACTSHEET_REQ_INFO_VIEW TO EWP_APROVISIONAMIENTO;