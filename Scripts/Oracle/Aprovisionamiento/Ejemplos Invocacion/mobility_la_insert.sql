SET SERVEROUTPUT ON;
/*
******** PRUEBA INSERCION **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_la PKG_MOBILITY_LA.LEARNING_AGREEMENT;
v_components PKG_MOBILITY_LA.LA_COMPONENT_LIST;
v_inst_schac         VARCHAR2(255 char);
v_inst_schac_partner VARCHAR2(255 char);
v_mobility_id         VARCHAR2(255 char);
BEGIN
v_inst_schac := :p_inst_schac;
v_inst_schac_partner := :p_inst_schac_partner;
v_mobility_id := :p_mobility_id;

v_components(0).la_component_type := 0;
v_components(0).los_code := 'LOS Code';
v_components(0).los_type := 0;
v_components(0).title := 'Component Title';
v_components(0).academic_term := new EWP.ACADEMIC_TERM('2020/2021',v_inst_schac_partner, null,sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
v_components(0).credit.scheme := 'ECTS';
v_components(0).credit.CREDIT_VALUE := 6;

v_components(1).la_component_type := 1;
v_components(1).los_code := 'Recognized LOS Code';
v_components(1).los_type := 0;
v_components(1).title := 'Component Title';
v_components(1).academic_term := new EWP.ACADEMIC_TERM('2020/2021',v_inst_schac, null,sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
v_components(1).credit.scheme := 'ECTS';
v_components(1).credit.CREDIT_VALUE := 6;

v_la.components := v_components;

v_la.STUDENT_SIGNATURE.SIGNER_NAME := 'Student Signer Name Test';
v_la.STUDENT_SIGNATURE.SIGNER_POSITION := 'student';
v_la.STUDENT_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
v_la.STUDENT_SIGNATURE.SIGN_DATE := SYSDATE; 

v_la.SENDING_HEI_SIGNATURE.SIGNER_NAME := 'Coordinator Signer Name Test';
v_la.SENDING_HEI_SIGNATURE.SIGNER_POSITION := 'coordinator';
v_la.SENDING_HEI_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
v_la.SENDING_HEI_SIGNATURE.SIGN_DATE := SYSDATE;

v_la.mobility_id := v_mobility_id;

v_return_code := PKG_MOBILITY_LA.INSERT_LEARNING_AGREEMENT(v_la, v_id_retorno, v_error_message, v_inst_schac_partner);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('id_retorno '|| v_id_retorno);
END;