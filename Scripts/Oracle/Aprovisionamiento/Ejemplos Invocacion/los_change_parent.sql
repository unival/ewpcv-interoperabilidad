DECLARE
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
   	v_los_id VARCHAR2(255); 
  	v_los_old_parent_id VARCHAR2(255); 
 	v_los_new_parent_id VARCHAR2(255); 
   
    CURSOR c_los(p_los_id IN VARCHAR2) IS 
    	SELECT * FROM EWP.EWPCV_LOS 
    	WHERE ID = p_los_id;
BEGIN
	v_los_id := 'CR/CD97791E-8B98-67F4-E053-1FC616ACF1F8';
	v_los_old_parent_id := 'MOD/CD97791E-8B94-67F4-E053-1FC616ACF1F8';
	v_los_new_parent_id := 'DEP/CD97791E-8B90-67F4-E053-1FC616ACF1F8';
	v_cod_retorno := EWP.PKG_LOS.CHANGE_PARENT(v_los_id, v_los_old_parent_id, v_los_new_parent_id, v_error);
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;