SET SERVEROUTPUT ON;

DECLARE
v_return_code NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN
v_return_code := NOTIFICA_ELEMENTO(:element_id,:element_type,:hei_to_notify, :notifier_hei, v_error);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;