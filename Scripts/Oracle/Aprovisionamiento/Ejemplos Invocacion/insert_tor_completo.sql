-- ACTUALIZA TOR
DECLARE
  v_return_code       NUMBER;
  v_error_message     VARCHAR2(20000 CHAR);

  -- Tipos principales
  v_tor                   PKG_TORS2.TOR;
  v_tor_report_list       PKG_TORS2.TOR_REPORT_LIST;
  v_loi_list              PKG_TORS2.LOI_LIST;
  v_tor_att_list          PKG_TORS2.TOR_ATTACHMENT_LIST;
  v_mob_att_list          PKG_TORS2.MOBILITY_ATTACHMENT_LIST;
  v_diploma               PKG_TORS2.DIPLOMA;

  -- Tipos secundarios
  v_result_distribution   PKG_TORS2.RESULT_DISTRIBUTION;
  v_dist_categories_list  PKG_TORS2.DIST_CATEGORIES_LIST;
  v_education_level_1     PKG_TORS2.EDUCATION_LEVEL;
  v_education_level_2     PKG_TORS2.EDUCATION_LEVEL;
BEGIN
  -- RESULT_DISTRIBUTION
  v_dist_categories_list(0).LABEL := new OWNEWP.LANGUAGE_ITEM('ES','Dist Category Update');
  v_dist_categories_list(0).DIST_CAT_COUNT := 1;
  v_result_distribution.DIST_CATEGORIES_LIST := v_dist_categories_list;
  v_result_distribution.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Dist Category Update'));
  
 --EDUCATION_LEVEL
  v_education_level_1.LEVEL_TYPE := 0;
  v_education_level_1.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 1 Update'));
  v_education_level_1.LEVEL_VALUE := 'Education level value 1 Update';

  v_education_level_2.LEVEL_TYPE := 0;
  v_education_level_2.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 2 Update'));
  v_education_level_2.LEVEL_VALUE := 'Education level value 2 Update';

  -- DIPLOMA
  v_diploma.VERSION := 0;
  v_diploma.ISSUE_DATE := sysdate;
  v_diploma.SECTION_LIST(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 1 Update');
  v_diploma.SECTION_LIST(0).SECTION_NUMBER := 100;
  v_diploma.SECTION_LIST(0).SECTION_ATTACHMENT_LIST(0).ATTACH_REFERENCE := NULL;
  v_diploma.SECTION_LIST(0).SECTION_ATTACHMENT_LIST(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment SECTION 100 A'));
  v_diploma.SECTION_LIST(0).SECTION_ATTACHMENT_LIST(0).ATT_TYPE := 1;
  -- Test File A Section 100 Update
  v_diploma.SECTION_LIST(0).SECTION_ATTACHMENT_LIST(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742046696C6520412053656374696F6E2031303020557064617465')));
  
  v_diploma.SECTION_LIST(0).SECTION_LIST(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 1-2-1 Update');
  v_diploma.SECTION_LIST(0).SECTION_LIST(0).SECTION_NUMBER := 121;
  v_diploma.SECTION_LIST(0).SECTION_LIST(1).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 1-2-2 Update');
  v_diploma.SECTION_LIST(0).SECTION_LIST(1).SECTION_NUMBER := 122;

  v_diploma.SECTION_LIST(0).SECTION_LIST(0).SECTION_LIST(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 1-3-1 Update');
  v_diploma.SECTION_LIST(0).SECTION_LIST(0).SECTION_LIST(0).SECTION_NUMBER := 131;
  v_diploma.SECTION_LIST(0).SECTION_LIST(0).SECTION_LIST(1).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 1-3-2 Update');
  v_diploma.SECTION_LIST(0).SECTION_LIST(0).SECTION_LIST(1).SECTION_NUMBER := 132;

  v_diploma.SECTION_LIST(1).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION 2 Update');
  v_diploma.SECTION_LIST(1).SECTION_NUMBER := 200;

    -- LOI_LIST
  v_loi_list(0).START_DATE := sysdate;
  v_loi_list(0).END_DATE := sysdate;
  -- TODO grading_scheme_id
  v_loi_list(0).STATUS := 1;
  v_loi_list(0).PERCENTAGE_LOWER := '1';
  v_loi_list(0).PERCENTAGE_EQUAL := '1';
  v_loi_list(0).PERCENTAGE_HIGHER := '1';
  v_loi_list(0).RESULT_DISTRIBUTION := v_result_distribution;

  v_loi_list(0).EDUCATION_LEVEL(0) := v_education_level_1;
  v_loi_list(0).EDUCATION_LEVEL(1) := v_education_level_2;

  v_loi_list(0).LANGUAGE_OF_INSTRUCTION := 'es';
  v_loi_list(0).ENGAGEMENT_HOURS := 100;
  v_loi_list(0).ATTACHMENTS_REF_LIST(0) := 'A';
  v_loi_list(0).ATTACHMENTS_REF_LIST(1) := 'C';
  v_loi_list(0).GROUP_TYPE_ID := 'CEA06BCF-95BF-040B-E053-1FC616ACC714';
  v_loi_list(0).GROUP_ID := 'CEA06BCF-95C4-040B-E053-1FC616ACC714';
  v_loi_list(0).DIPLOMA := v_diploma;
  -- TODO extension
  v_loi_list(0).LOS_CODE := 'LOS-CODE-TEST1';


  v_loi_list(1).START_DATE := sysdate;
  v_loi_list(1).END_DATE := sysdate;
  -- TODO grading_scheme_id
  v_loi_list(1).STATUS := 2;
  v_loi_list(1).PERCENTAGE_LOWER := '2';
  v_loi_list(1).PERCENTAGE_EQUAL := '2';
  v_loi_list(1).PERCENTAGE_HIGHER := '2';
  v_loi_list(1).RESULT_DISTRIBUTION := v_result_distribution;

  v_loi_list(1).EDUCATION_LEVEL(0) := v_education_level_1;
  v_loi_list(1).EDUCATION_LEVEL(1) := v_education_level_2;

  v_loi_list(1).LANGUAGE_OF_INSTRUCTION := 'es';
  v_loi_list(1).ENGAGEMENT_HOURS := 120;
  v_loi_list(1).ATTACHMENTS_REF_LIST(0) := 'A';
  v_loi_list(1).GROUP_TYPE_ID := 'CEA06BCF-95BF-040B-E053-1FC616ACC714';
  v_loi_list(1).GROUP_ID := 'CEA06BCF-95C4-040B-E053-1FC616ACC714';
  v_loi_list(1).DIPLOMA := v_diploma;
  -- TODO extension

  v_loi_list(1).LOS_CODE := 'LOS-CODE-TEST2';

  
  --TOR_ATTACHMENT_LIST
  v_tor_att_list(0).ATTACH_REFERENCE := 'A'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI A Update'));
  v_tor_att_list(0).ATT_TYPE := 1;
  --Test File A Update
  v_tor_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742046696C65204120557064617465')));
  v_tor_att_list(0).DESCRIPTION :=  new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor DESC LOI A Update'));

  v_tor_att_list(1).ATTACH_REFERENCE := 'B'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(1).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor SIN LOI Update'));
  v_tor_att_list(1).ATT_TYPE := 1;
  -- Test File B Update
  v_tor_att_list(1).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742046696C65204220557064617465')));
  v_tor_att_list(1).DESCRIPTION :=  new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor DESC SIN LOI Update'));

  v_tor_att_list(2).ATTACH_REFERENCE := 'C'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(2).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI C Update'));
  v_tor_att_list(2).ATT_TYPE := 1;
  -- Test File C Update
  v_tor_att_list(2).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742046696C65204320557064617465')));
  v_tor_att_list(2).DESCRIPTION :=  new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor DESC LOI C Update'));
  
  -- TOR_REPORT_LIST 
  v_tor_report_list(0).ISSUE_DATE := sysdate; 
  v_tor_report_list(0).loi_list :=  v_loi_list;
  v_tor_report_list(0).tor_attachment_list :=  v_tor_att_list;
  
  
  --MOBILITY_ATTACHMENT_LIST
  v_mob_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment mobility 1 Update'));
  v_mob_att_list(0).ATT_TYPE := 1;
  -- Test 1 attachment mobility Update
  v_mob_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742031206174746163686D656E74206D6F62696C69747920557064617465')));
    
  -- TOR
  v_tor.GENERATED_DATE := sysdate;
  v_tor.TOR_REPORT_LIST := v_tor_report_list;
  v_tor.MOBILITY_ATTACHMENT_LIST := v_mob_att_list;
      
  -- LANZAMOS INVOCACIÓN  
  v_return_code := PKG_TORS2.UPDATE_TOR(:p_tor_id, v_tor, v_error_message);
  
  DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
  DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);

END;




-- INSERTA TOR
DECLARE
  v_return_code       NUMBER;
  v_error_message     VARCHAR2(20000 CHAR);

  -- Tipos principales
  v_tor                   PKG_TORS2.TOR;
  v_tor_report_list       PKG_TORS2.TOR_REPORT_LIST;
  v_loi_list              PKG_TORS2.LOI_LIST;
  v_tor_att_list          PKG_TORS2.TOR_ATTACHMENT_LIST;
  v_mob_att_list          PKG_TORS2.MOBILITY_ATTACHMENT_LIST;
  v_diploma               PKG_TORS2.DIPLOMA;

  -- Tipos secundarios
  v_result_distribution   PKG_TORS2.RESULT_DISTRIBUTION;
  v_dist_categories_list  PKG_TORS2.DIST_CATEGORIES_LIST;
  v_education_level_1     PKG_TORS2.EDUCATION_LEVEL;
  v_education_level_2     PKG_TORS2.EDUCATION_LEVEL;
  v_tor_id                VARCHAR2(255);
BEGIN
  -- RESULT_DISTRIBUTION
  v_dist_categories_list(0).LABEL := new OWNEWP.LANGUAGE_ITEM('ES','Dist Category');
  v_dist_categories_list(0).DIST_CAT_COUNT := 1;
  v_result_distribution.DIST_CATEGORIES_LIST := v_dist_categories_list;
  v_result_distribution.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Dist Category'));
  
 --EDUCATION_LEVEL
  v_education_level_1.LEVEL_TYPE := 0;
  v_education_level_1.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 1'));
  v_education_level_1.LEVEL_VALUE := 'Education level value 1';

 --EDUCATION_LEVEL
  v_education_level_2.LEVEL_TYPE := 0;
  v_education_level_2.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 2'));
  v_education_level_2.LEVEL_VALUE := 'Education level value 2';
  
  
  -- DIPLOMA
  v_diploma.VERSION := 1;
  v_diploma.ISSUE_DATE := sysdate;
  v_diploma.SECTION_LIST(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','DIPLOMA_SECTION');
  v_diploma.SECTION_LIST(0).SECTION_NUMBER := 0;

    -- LOI_LIST
  v_loi_list(0).START_DATE := sysdate;
  v_loi_list(0).END_DATE := sysdate;
  -- TODO: grading_scheme_id
  v_loi_list(0).STATUS := 1;
  v_loi_list(0).PERCENTAGE_LOWER := '1';
  v_loi_list(0).PERCENTAGE_EQUAL := '1';
  v_loi_list(0).PERCENTAGE_HIGHER := '1';
  v_loi_list(0).RESULT_DISTRIBUTION := v_result_distribution;

  v_loi_list(0).EDUCATION_LEVEL(0) := v_education_level_1;
  v_loi_list(0).EDUCATION_LEVEL(1) := v_education_level_2;

  v_loi_list(0).LANGUAGE_OF_INSTRUCTION := 'es';
  v_loi_list(0).ENGAGEMENT_HOURS := 100;
  v_loi_list(0).ATTACHMENTS_REF_LIST(0) := 'A';
  v_loi_list(0).ATTACHMENTS_REF_LIST(1) := 'C';
  v_loi_list(0).GROUP_TYPE_ID := 'CEA06BCF-95BF-040B-E053-1FC616ACC714';
  v_loi_list(0).GROUP_ID := 'CEA06BCF-95C4-040B-E053-1FC616ACC714';
  v_loi_list(0).DIPLOMA := v_diploma;
  -- TODO extension
  v_loi_list(0).LOS_CODE := 'LOS-CODE-TEST1';

  v_loi_list(1).START_DATE := sysdate;
  v_loi_list(1).END_DATE := sysdate;
  -- TODO grading_scheme_id
  v_loi_list(1).STATUS := 2;
  v_loi_list(1).PERCENTAGE_LOWER := '2';
  v_loi_list(1).PERCENTAGE_EQUAL := '2';
  v_loi_list(1).PERCENTAGE_HIGHER := '2';
  v_loi_list(1).RESULT_DISTRIBUTION := v_result_distribution;

  v_loi_list(1).EDUCATION_LEVEL(0) := v_education_level_1;
  v_loi_list(1).EDUCATION_LEVEL(1) := v_education_level_2;

  v_loi_list(1).LANGUAGE_OF_INSTRUCTION := 'es';
  v_loi_list(1).ENGAGEMENT_HOURS := 120;
  v_loi_list(1).ATTACHMENTS_REF_LIST(0) := 'A';
  v_loi_list(1).GROUP_TYPE_ID := 'CEA06BCF-95BF-040B-E053-1FC616ACC714';
  v_loi_list(1).GROUP_ID := 'CEA06BCF-95C4-040B-E053-1FC616ACC714';
  v_loi_list(1).DIPLOMA := v_diploma;
  -- TODO extension

  v_loi_list(1).LOS_CODE := 'LOS-CODE-TEST2';

  
  --TOR_ATTACHMENT_LIST
  v_tor_att_list(0).ATTACH_REFERENCE := 'A'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI A'));
  v_tor_att_list(0).ATT_TYPE := 1;
  --Test Prueba Insercion A
  v_tor_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2041')));

  v_tor_att_list(1).ATTACH_REFERENCE := 'B'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(1).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor SIN LOI'));
  v_tor_att_list(1).ATT_TYPE := 1;
  --Test Prueba Insercion B
  v_tor_att_list(1).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2042')));

  v_tor_att_list(2).ATTACH_REFERENCE := 'C'; -- !!!! CAMBIAR para comprobar método de comprobación referencias
  v_tor_att_list(2).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI C'));
  v_tor_att_list(2).ATT_TYPE := 1;
  --Test Prueba Insercion C
  v_tor_att_list(2).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2043')));
  
  -- TOR_REPORT_LIST 
  v_tor_report_list(0).ISSUE_DATE := sysdate; 
  v_tor_report_list(0).loi_list :=  v_loi_list;
  v_tor_report_list(0).tor_attachment_list :=  v_tor_att_list;
  
  
  --MOBILITY_ATTACHMENT_LIST
  v_mob_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment mobility 1'));
  v_mob_att_list(0).ATT_TYPE := 1;
  -- Test 1 attachment mobility insert
  v_mob_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742031206174746163686D656E74206D6F62696C69747920696E73657274')));
    
  -- TOR
  v_tor.GENERATED_DATE := sysdate;
  v_tor.TOR_REPORT_LIST := v_tor_report_list;
  v_tor.MOBILITY_ATTACHMENT_LIST := v_mob_att_list;
      
  -- LANZAMOS INVOCACIÓN  
  v_return_code := PKG_TORS2.INSERT_TOR(v_tor, :p_la_id, v_tor_id, v_error_message);
  
  DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
  DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
  DBMS_OUTPUT.PUT_LINE('v_tor_id: ' || v_tor_id);
END;



DECLARE
 v_r NUMBER;
 v_e VARCHAR2(2000);
 
BEGIN
  
 v_r := PKG_TORS2.DELETE_TOR('CEEFA6FB-9003-376F-E053-1FC616ACEF02',v_e);
 
 DBMS_OUTPUT.PUT_LINE('v_salida: '|| v_r);
 DBMS_OUTPUT.PUT_LINE('v_error: '|| v_e);
 
END;

BEGIN
  DELETE FROM OWNEWP.EWPCV_LOI_ATT;
  DELETE FROM OWNEWP.EWPCV_REPORT_ATT;
  DELETE FROM OWNEWP.EWPCV_TOR_ATTACHMENT;
  DELETE FROM OWNEWP.EWPCV_DIPLOMA_SEC_ATTACH;
  DELETE FROM OWNEWP.EWPCV_ATTACHMENT_TITLE;
  DELETE FROM OWNEWP.EWPCV_ATTACHMENT_DESCRIPTION;
  DELETE FROM OWNEWP.EWPCV_ATTACHMENT_CONTENT;
  DELETE FROM OWNEWP.EWPCV_ATTACHMENT;
END;


BEGIN
  DELETE FROM OWNEWP.EWPCV_LEVEL_DESCRIPTION;
  DELETE FROM OWNEWP.EWPCV_LEVELS;
  DELETE FROM OWNEWP.EWPCV_LEVEL;
END;
