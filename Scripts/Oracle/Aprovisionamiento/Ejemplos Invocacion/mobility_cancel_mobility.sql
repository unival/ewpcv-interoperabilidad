
-- CANCELAMOS UNA MOVILIDAD
DECLARE
	v_mobility_id VARCHAR2(255);
	v_error VARCHAR2(2000);
	hei_to_notify VARCHAR2(200);
	v_resultado NUMBER(1);
BEGIN
	v_mobility_id := 'CC6E1926-0A6E-2C18-E053-1FC616ACCC68';
    hei_to_notify := 'tlu.ee';
   
	v_resultado := EWP.PKG_MOBILITY_LA.CANCEL_MOBILITY(v_mobility_id, v_error, hei_to_notify);
	dbms_output.put_line('v_resultado: ' || v_resultado);
	dbms_output.put_line('v_error: ' || v_error);
END;
