SET SERVEROUTPUT ON;

DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_ounit PKG_OUNITS.OUNIT;
v_inst_schac         VARCHAR2(255 char);

-- Contact
v_contact EWP.CONTACT;
v_contact_emails EWP.EMAIL_LIST;
v_contact_names EWP.LANGUAGE_ITEM_LIST;
v_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_address_lines EWP.ADDRESS_LINE_LIST;

v_contact_list EWP.CONTACT;
v_contact_list_emails EWP.EMAIL_LIST;
v_contact_list_names EWP.LANGUAGE_ITEM_LIST;
v_contact_list_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_list_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_list_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_list_address_lines EWP.ADDRESS_LINE_LIST;

BEGIN

v_inst_schac:= :p_inst_schac;


/* 
	INICIO
*/

--Primary contact
v_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
v_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Jon Test'));
v_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
v_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
v_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');

v_contact := new EWP.CONTACT(v_contact_names,v_contact_descriptions,v_contact_urls,'coordinator',null,null, v_contact_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...

-- Contact para Contact Detail List

v_contact_list_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
v_contact_list_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','SJon Test'));
v_contact_list_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
v_contact_list_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_list_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
v_contact_list_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');

v_contact_list := new EWP.CONTACT(v_contact_list_names,v_contact_list_descriptions,v_contact_list_urls,'coordinator',null,null, v_contact_list_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...
	
--OUNIT
v_ounit.HEI_ID := v_inst_schac;
v_ounit.OUNIT_CODE := 'ounit.code';
v_ounit.OUNIT_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Ounit name'));
v_ounit.ABBREVIATION := 'Ounit Abbreviation';
v_ounit.OUNIT_CONTACT_DETAILS := v_contact;
v_ounit.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Ounit Contact','','01/01/1960','ES',0,v_contact_list));
v_ounit.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.ounit-factsheet.es'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_ounit.LOGO_URL := 'https://www.ounit-logo.es';-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_ounit.IS_TREE_STRUCTURE := 0;

/*
	FIN
*/


v_return_code := PKG_OUNITS.INSERT_OUNIT(v_ounit, v_id_retorno, v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('v_id_retorno '|| v_id_retorno);

END;
