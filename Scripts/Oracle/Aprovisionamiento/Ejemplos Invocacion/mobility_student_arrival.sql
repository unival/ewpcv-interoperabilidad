-- STUDENT_ARRIVAL
DECLARE
	v_mobility_id VARCHAR2(255);
	v_comment VARCHAR2(2000);
	v_error VARCHAR2(2000);
	hei_to_notify VARCHAR2(200);
	v_resultado NUMBER(1);
BEGIN
	v_mobility_id := 'CC6E1926-0A6E-2C18-E053-1FC616ACCC68';
	v_comment := '';
    hei_to_notify := 'tlu.ee';
   
	v_resultado := EWP.PKG_MOBILITY_LA.STUDENT_ARRIVAL(v_mobility_id, v_error, hei_to_notify);
END;