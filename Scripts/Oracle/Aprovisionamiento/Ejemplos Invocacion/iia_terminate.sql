SET SERVEROUTPUT ON;
/*
******** PRUEBA BORRADO **********
*/
DECLARE 
v_status NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN 
    v_status := pkg_iias.TERMINATE_IIA(:p_iia_id, v_error, :p_hei_to_notify);
    DBMS_OUTPUT.PUT_LINE('v_status '|| v_status);
    DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;