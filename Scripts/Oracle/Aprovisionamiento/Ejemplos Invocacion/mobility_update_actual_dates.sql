-- UPDATE_ACTUAL_DATES
DECLARE
	v_mobility_id VARCHAR2(255);
	v_comment VARCHAR2(2000);
	v_error VARCHAR2(2000);
	hei_to_notify VARCHAR2(200);
	v_resultado NUMBER(1);
	v_actual_departure DATE;
	v_actual_arrival DATE;
BEGIN
	v_mobility_id := 'CC6E1926-0A6E-2C18-E053-1FC616ACCC68';
	v_comment := '';
    hei_to_notify := 'tlu.ee';
   	v_actual_departure := TO_DATE('25/12/2021', 'dd/mm/yyyy');
   	v_actual_arrival := TO_DATE('15/10/2021', 'dd/mm/yyyy');
	v_resultado := EWP.PKG_MOBILITY_LA.UPDATE_ACTUAL_DATES(v_mobility_id, v_actual_departure, v_actual_arrival, v_comment, v_error, hei_to_notify);
	dbms_output.put_line('v_resultado: ' || v_resultado);
	dbms_output.put_line('v_error: ' || v_error);
END;