
-- INSERTA UN DEGREE PROGRAMME
DECLARE
	v_los EWP.PKG_LOS.LOS;
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
    v_names EWP.LANGUAGE_ITEM_LIST;
    v_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_urls EWP.LANGUAGE_ITEM_LIST;
    v_ounit_code VARCHAR2(255);
    v_hei_id VARCHAR2(255);
	v_id VARCHAR2(255);
    v_ounit_id VARCHAR2(255);
    CURSOR c_ounit(p_ounit_code IN VARCHAR2, p_hei IN VARCHAR2) IS 
    	SELECT OUNIT_ID FROM EWP.OUNIT_VIEW  
    	WHERE OUNIT_CODE = p_ounit_code
      	AND INSTITUTION_ID = p_hei;
BEGIN
	v_ounit_code := 'ounit.code';	
	v_hei_id := 'uv.es';
	
  	v_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Maths - Test1'));
    v_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Desc Maths - Test1'), 
		new EWP.LANGUAGE_ITEM('es', 'Desc Matemáticas - Test1'));
    v_urls := new EWP.LANGUAGE_ITEM_LIST();
   
	OPEN c_ounit(v_ounit_code, v_hei_id);
	FETCH c_ounit INTO v_ounit_id;
	CLOSE c_ounit;

	v_los := NEW EWP.PKG_LOS.LOS(5, v_hei_id, 'iscedf-test1', 'code-pl-test1', v_names, v_descriptions, v_urls, v_ounit_id, 'subject-test1', 3);

	v_cod_retorno := EWP.PKG_LOS.INSERT_LOS(v_los, NULL, v_id, v_error);

	DBMS_OUTPUT.PUT_LINE('v_id: ' || v_id);
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;
/
SET SERVEROUTPUT ON;
-- INSERTA UN MODULE CON PADRE DEGREE PROGRAMME
DECLARE
	v_los EWP.PKG_LOS.LOS;
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
    v_names EWP.LANGUAGE_ITEM_LIST;
    v_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_urls EWP.LANGUAGE_ITEM_LIST;
    v_ounit_code VARCHAR2(255);
    v_hei_id VARCHAR2(255);
	v_id VARCHAR2(255);
    v_ounit_id VARCHAR2(255);
    CURSOR c_ounit(p_ounit_code IN VARCHAR2, p_hei IN VARCHAR2) IS 
    	SELECT OUNIT_ID FROM EWP.OUNIT_VIEW  
    	WHERE OUNIT_CODE = p_ounit_code
      	AND INSTITUTION_ID = p_hei;
BEGIN
	v_ounit_code := 'ounit.code';	
	v_hei_id := 'uv.es';
	
  	v_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Maths Module - Test2'));
    v_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Desc Maths Module - Test2'), 
		new EWP.LANGUAGE_ITEM('es', 'Desc Matematicas Modulo- Test2'));
    v_urls := new EWP.LANGUAGE_ITEM_LIST();
   
	OPEN c_ounit(v_ounit_code, v_hei_id);
	FETCH c_ounit INTO v_ounit_id;
	CLOSE c_ounit;

	v_los := NEW EWP.PKG_LOS.LOS(5, v_hei_id, 'iscedf-modulo-test2', 'LOS Code', v_names, v_descriptions, v_urls, null, 'subject-modulo-test2', 2);

	v_cod_retorno := EWP.PKG_LOS.INSERT_LOS(v_los, null, v_id, v_error);

	DBMS_OUTPUT.PUT_LINE('v_id: ' || v_id);
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;
/
SET SERVEROUTPUT ON;
-- INSERTA UN COURSE CON PADRE MODULO
DECLARE
	v_los EWP.PKG_LOS.LOS;
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
    v_names EWP.LANGUAGE_ITEM_LIST;
    v_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_urls EWP.LANGUAGE_ITEM_LIST;
    v_ounit_code VARCHAR2(255);
    v_hei_id VARCHAR2(255);
	v_id VARCHAR2(255);
    v_ounit_id VARCHAR2(255);
    CURSOR c_ounit(p_ounit_code IN VARCHAR2, p_hei IN VARCHAR2) IS 
    	SELECT OUNIT_ID FROM EWP.OUNIT_VIEW  
    	WHERE OUNIT_CODE = p_ounit_code
      	AND INSTITUTION_ID = p_hei;
BEGIN
	v_ounit_code := 'ounit.code';	
	v_hei_id := 'um.es';
	
  	v_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Maths Course - Test3'));
    v_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Desc Maths Course - Test3'), 
		new EWP.LANGUAGE_ITEM('es', 'Desc Matem�ticas Curso- Test3'));
    v_urls := new EWP.LANGUAGE_ITEM_LIST();
   
	OPEN c_ounit(v_ounit_code, v_hei_id);
	FETCH c_ounit INTO v_ounit_id;
	CLOSE c_ounit;

	v_los := NEW EWP.PKG_LOS.LOS(3, v_hei_id, 'iscedf-course-test3', 'LOS Code', v_names, v_descriptions, v_urls, null, 'subject-course-test3', 0);

	v_cod_retorno := EWP.PKG_LOS.INSERT_LOS(v_los, null, v_id, v_error);

	DBMS_OUTPUT.PUT_LINE('v_id: ' || v_id);
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;
/
-- INSERTA UN CLASS CON PADRE COURSE
DECLARE
	v_los EWP.PKG_LOS.LOS;
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
    v_names EWP.LANGUAGE_ITEM_LIST;
    v_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_urls EWP.LANGUAGE_ITEM_LIST;
    v_ounit_code VARCHAR2(255);
    v_hei_id VARCHAR2(255);
	v_id VARCHAR2(255);
    v_ounit_id VARCHAR2(255);
    CURSOR c_ounit(p_ounit_code IN VARCHAR2, p_hei IN VARCHAR2) IS 
    	SELECT OUNIT_ID FROM EWP.OUNIT_VIEW  
    	WHERE OUNIT_CODE = p_ounit_code
      	AND INSTITUTION_ID = p_hei;
BEGIN
	v_ounit_code := 'ounit.code';	
	v_hei_id := 'uv.es';
	
  	v_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Maths Class - Test4'));
    v_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Desc Maths Class - Test4'), 
		new EWP.LANGUAGE_ITEM('es', 'Desc Matemáticas Clase - Test4'));
    v_urls := new EWP.LANGUAGE_ITEM_LIST();
   
	OPEN c_ounit(v_ounit_code, v_hei_id);
	FETCH c_ounit INTO v_ounit_id;
	CLOSE c_ounit;

	v_los := NEW EWP.PKG_LOS.LOS(4, v_hei_id, 'iscedf-class-test4', 'code-pl-class-test4', v_names, v_descriptions, v_urls, v_ounit_id, 'subject-class-test4', 1);

	v_cod_retorno := EWP.PKG_LOS.INSERT_LOS(v_los, 'CR/CD97791E-8B98-67F4-E053-1FC616ACF1F8', v_id, v_error);

	DBMS_OUTPUT.PUT_LINE('v_id: ' || v_id);
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;
