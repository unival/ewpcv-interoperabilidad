SET SERVEROUTPUT ON;
--APPROVAL

DECLARE
v_return_code NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN
v_return_code := PKG_IIAS.APPROVE_IIA(:internal_id_remote_iia_copy,:hei_to_notify, v_error);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;