SET SERVEROUTPUT ON;
/*
******** PRUEBA INSERCION OWNEWP **********
*/
DECLARE
v_return_code           NUMBER;
v_error_message         VARCHAR2(20000 CHAR);
v_id_retorno            VARCHAR2(255 char);
v_iia                   PKG_IIAS.IIA;
v_coop_cond             PKG_IIAS.COOPERATION_CONDITION_LIST;
v_first_partner         PKG_IIAS.PARTNER;
v_second_partner        PKG_IIAS.PARTNER;
v_sending_partner       PKG_IIAS.PARTNER;
v_receiving_partner     PKG_IIAS.PARTNER;
v_institution_send      EWP.INSTITUTION;
v_institution_receiv    EWP.INSTITUTION;
v_inst_schac            VARCHAR2(255 char);
v_inst_rec_schac        VARCHAR2(255 char);

-- Contact para firstPartener signer
v_fps_contact EWP.CONTACT;
v_fps_contact_emails EWP.EMAIL_LIST;
v_fps_contact_names EWP.LANGUAGE_ITEM_LIST;
v_fps_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_fps_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_fps_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_fps_contact_address_lines EWP.ADDRESS_LINE_LIST;

-- Contact para secondPartener signer
v_sps_contact EWP.CONTACT;
v_sps_contact_emails EWP.EMAIL_LIST;
v_sps_contact_names EWP.LANGUAGE_ITEM_LIST;
v_sps_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_sps_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_sps_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_sps_contact_address_lines EWP.ADDRESS_LINE_LIST;

-- Contact para firstPartener lista de contactos
v_fp_contact EWP.CONTACT;
v_fp_contact_emails EWP.EMAIL_LIST;
v_fp_contact_names EWP.LANGUAGE_ITEM_LIST;
v_fp_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_fp_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_fp_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_fp_contact_address_lines EWP.ADDRESS_LINE_LIST;

-- Contact para secondPartener lista de contactos
v_sp_contact EWP.CONTACT;
v_sp_contact_emails EWP.EMAIL_LIST;
v_sp_contact_names EWP.LANGUAGE_ITEM_LIST;
v_sp_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_sp_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_sp_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_sp_contact_address_lines EWP.ADDRESS_LINE_LIST;

-- Contact para sender de coopCond lista de contactos
v_send_contact EWP.CONTACT;
v_send_contact_emails EWP.EMAIL_LIST;
v_send_contact_names EWP.LANGUAGE_ITEM_LIST;
v_send_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_send_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_send_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_send_contact_address_lines EWP.ADDRESS_LINE_LIST;

-- Contact para receiver de coopCond lista de contactos
v_rec_contact EWP.CONTACT;
v_rec_contact_emails EWP.EMAIL_LIST;
v_rec_contact_names EWP.LANGUAGE_ITEM_LIST;
v_rec_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_rec_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_rec_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_rec_contact_address_lines EWP.ADDRESS_LINE_LIST;
BEGIN

v_inst_schac:= :p_inst_schac;
v_inst_rec_schac:= :p_inst_rec_schac;

-- INICIO CONTACTOS
     -- Signer Contact para firstPartener
    v_fps_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_fps_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','First Partner Signer'));
    v_fps_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_fps_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_fps_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_fps_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_fps_contact := new EWP.CONTACT(v_fps_contact_names,v_fps_contact_descriptions,v_fps_contact_urls,'First Partner Signer',null,null, v_fps_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_fps_contact_recipient_name,v_fps_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_fps_contact_recipient_name,v_fps_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...

    -- Signer Contact para secondPartener
    v_sps_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_sps_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Second Partner Signer'));
    v_sps_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_sps_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_sps_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_sps_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_sps_contact := new EWP.CONTACT(v_sps_contact_names,v_sps_contact_descriptions,v_sps_contact_urls,'Second Partner Signer',null,null, v_sps_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_sps_contact_recipient_name,v_sps_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_sps_contact_recipient_name,v_sps_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...

    -- Contact para FirstPartener
    v_fp_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_fp_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','First Partner Contact'));
    v_fp_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_fp_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_fp_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_fp_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_fp_contact := new EWP.CONTACT(v_fp_contact_names,v_fp_contact_descriptions,v_fp_contact_urls,'First Partner contact',null,null, v_fp_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_fp_contact_recipient_name,v_fp_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_fp_contact_recipient_name,v_fp_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...

   	-- Contact para SecondPartener
    v_sp_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_sp_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Second Partner Contact'));
    v_sp_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_sp_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_sp_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_sp_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_sp_contact := new EWP.CONTACT(v_sp_contact_names,v_sp_contact_descriptions,v_sp_contact_urls,'Second Partner contact',null,null, v_sp_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_sp_contact_recipient_name,v_sp_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_sp_contact_recipient_name,v_sp_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...

    -- Contact para CoopCond SendingPartener
    v_send_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_send_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Coop Cond Sender Contact'));
    v_send_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_send_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_send_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_send_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_send_contact := new EWP.CONTACT(v_send_contact_names,v_send_contact_descriptions,v_send_contact_urls,'Sender Partner contact',null,null, v_send_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_send_contact_recipient_name,v_send_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_send_contact_recipient_name,v_send_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...
    
    -- Contact para CoopCond ReceivingPartener
    v_rec_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_rec_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Coop Cond Receiving Contact'));
    v_rec_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_rec_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_rec_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_rec_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_rec_contact := new EWP.CONTACT(v_rec_contact_names,v_rec_contact_descriptions,v_rec_contact_urls,'Receiver Partner contact',null,null, v_rec_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_rec_contact_recipient_name,v_rec_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_rec_contact_recipient_name,v_rec_contact_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Tel�fonos han de ser en formato +x...

-- FIN CONTACTOS

--INSTITUION_SEND
v_institution_send := new EWP.INSTITUTION(v_inst_schac,null,null,null);

--INSTITUION_RECEIV
v_institution_receiv := new EWP.INSTITUTION(v_inst_rec_schac,null,null,null);

--IIA
v_iia.IIA_CODE := 'IIA_TEST_CODE';
v_iia.start_date := sysdate;
v_iia.end_date := sysdate;

--FIRST PARTNER
v_first_partner.institution := v_institution_send;
v_first_partner.signing_date := sysdate;
v_first_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('John','Doe FPS','01/01/1960','ES',0, v_fps_contact);
v_first_partner.PARTNER_CONTACTS := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('John','Doe FP','01/01/1960','ES',0, v_fp_contact));
 
--SECOND PARTNER
v_second_partner.institution := v_institution_receiv;
v_second_partner.signing_date := sysdate;
v_second_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('John','Doe SPS','01/01/1960','ES',0, v_sps_contact);
v_second_partner.PARTNER_CONTACTS := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('John','Doe SP','01/01/1960','ES',0, v_sp_contact));

v_iia.first_partner := v_first_partner;
v_iia.second_partner := v_second_partner;

--COOP COND
v_coop_cond(1).START_DATE := sysdate;
v_coop_cond(1).END_DATE := sysdate;
v_coop_cond(1).EQF_LEVEL_LIST(1):= 5;
v_coop_cond(1).mobility_number := 1;
v_coop_cond(1).MOBILITY_TYPE.MOBILITY_CATEGORY := 'STUDIES';
v_coop_cond(1).MOBILITY_TYPE.MOBILITY_GROUP := 'STUDENT';
v_coop_cond(1).COP_COND_DURATION.NUMBER_DURATION := 1;
v_coop_cond(1).COP_COND_DURATION.UNIT := 3;
v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
v_coop_cond(1).SUBJECT_AREA_LIST := NEW EWP.SUBJECT_AREA_LIST(new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)'));
v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).LANGUAGE_SKILL := new EWP.LANGUAGE_SKILL('es','B2');
v_coop_cond(1).blended := 0;



--SENDING PARTNER
v_sending_partner.institution := v_institution_send;
v_sending_partner.signing_date := sysdate;
v_sending_partner.PARTNER_CONTACTS := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('John','Doe CCSP','01/01/1960','ES',0, v_send_contact));
 
--RECEIVING PARTNER
v_receiving_partner.institution := v_institution_receiv;
v_receiving_partner.signing_date := sysdate;
v_receiving_partner.PARTNER_CONTACTS := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('John','Doe CCRP','01/01/1960','ES',0, v_rec_contact));

v_coop_cond(1).RECEIVING_PARTNER := v_receiving_partner;
v_coop_cond(1).SENDING_PARTNER := v_sending_partner;

v_iia.COOPERATION_CONDITION_LIST := v_coop_cond;

--DESCOMENTAR SI SE QUIERE INSERTAR UN FICHERO
--v_iia.PDF := utl_raw.cast_to_raw('Texto de prueba');
--v_iia.FILE_MIME_TYPE := 'text/plain';

v_return_code := PKG_IIAS.INSERT_IIA(v_iia, v_id_retorno, v_error_message, :p_hei_to_notify);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('v_id_retorno '|| v_id_retorno);

END;