/*
******** PRUEBA UPDATE **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_la PKG_MOBILITY_LA.LEARNING_AGREEMENT;
v_components PKG_MOBILITY_LA.LA_COMPONENT_LIST;
BEGIN


v_components(0).la_component_type := 0;
v_components(0).los_code := 'Mates 101';
v_components(0).title := 'MATEMATICAS';
v_components(0).status := 1;
v_components(0).reason_code := 0;
v_components(0).academic_term := new EWP.ACADEMIC_TERM('2020/2021','UM.ES','OunitCodeUM',sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
v_components(0).credit.scheme := 'ECTS';
v_components(0).credit.CREDIT_VALUE := 6;


v_components(1).la_component_type := 0;
v_components(1).los_code := 'Mates 202';
v_components(1).title := 'MATEMATICAS DISPONIBLES';
v_components(1).status := 0;
v_components(1).reason_code := 3;
v_components(1).academic_term := new EWP.ACADEMIC_TERM('2020/2021','UV.ES','OunitCodeUV',sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
v_components(1).credit.scheme := 'ECTS';
v_components(1).credit.CREDIT_VALUE := 6;


v_la.mobility_id := :p_mobility_id;
v_la.components := v_components;
v_la.STUDENT_SIGNATURE.SIGNER_NAME := 'Luis';
v_la.STUDENT_SIGNATURE.SIGNER_POSITION := 'student';
v_la.STUDENT_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
v_la.STUDENT_SIGNATURE.SIGN_DATE := SYSDATE;

v_la.SENDING_HEI_SIGNATURE.SIGNER_NAME := 'PEPE';
v_la.SENDING_HEI_SIGNATURE.SIGNER_POSITION := 'coordinator';
v_la.SENDING_HEI_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
v_la.SENDING_HEI_SIGNATURE.SIGN_DATE := SYSDATE;

v_return_code := PKG_MOBILITY_LA.UPDATE_LEARNING_AGREEMENT(:p_la_id, v_la, v_error_message, :p_hei_to_notify);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
END;