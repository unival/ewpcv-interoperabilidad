/*
******** PRUEBA BORRADO **********
*/
DECLARE 
v_status NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN 
    v_status := pkg_mobility_la.DELETE_LEARNING_AGREEMENT(:p_id_la, v_error, :p_hei_to_notify);
    DBMS_OUTPUT.PUT_LINE('v_status '|| v_status);
    DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;
