SET SERVEROUTPUT ON;
-- Regresco a través de una notificación entrante

-- Institución a notificar
VARIABLE p_notify_hei VARCHAR2(255 char);
EXEC :p_notify_hei := 'unican.es';

-- Institución que notifica
VARIABLE p_notifier_hei VARCHAR2(255 char);
EXEC :p_notifier_hei := 'uv.es';

DECLARE
    v_error VARCHAR2(500);
    v_return NUMBER;
BEGIN
    v_return := EWP.INSERTA_NOTIFICATION_ENTRANTE('2bd60037-9ac5-fd88-e063-011a160ae96d', 5, :p_notify_hei, :p_notifier_hei, v_error);

    DBMS_OUTPUT.PUT_LINE('error ' || v_error);
    DBMS_OUTPUT.PUT_LINE('return ' || v_return);
END;
/

