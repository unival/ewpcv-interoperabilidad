SET SERVEROUTPUT ON;
DECLARE
 v_r NUMBER;
 v_e VARCHAR2(2000);
 v_f ewp.PKG_FACTSHEET.FACTSHEET_INSTITUTION;
BEGIN
 v_f.INSTITUTION_ID := 'UV.ES';
 v_f.ABREVIATION := 'uni de valencia';
 --v_f.LOGO_URL := '';
 v_f.INSTITUTION_NAME := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('VL','Universitat de Valencia'),new EWP.LANGUAGE_ITEM('EN','University of Valencia'));
 v_f.FACTSHEET.DECISION_WEEK_LIMIT := 3;
 v_f.FACTSHEET.TOR_WEEK_LIMIT := 9;
 v_f.FACTSHEET.NOMINATIONS_AUTUM_TERM := SYSDATE;
 v_f.FACTSHEET.NOMINATIONS_SPRING_TERM := SYSDATE;
 v_f.FACTSHEET.APPLICATION_AUTUM_TERM := SYSDATE;
 v_f.FACTSHEET.APPLICATION_SPRING_TERM := SYSDATE;
 
 v_f.APPLICATION_INFO.EMAIL := 'k@a.com'; -- Ha de ser en formato x@x.x
 v_f.APPLICATION_INFO.PHONE := new EWP.PHONE_NUMBER('+34669966666',null,null);-- Teléfonos han de ser en formato +x...
 v_f.APPLICATION_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.a.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.HOUSING_INFO.EMAIL := 'b@b.fr';-- Ha de ser en formato x@x.x
 v_f.HOUSING_INFO.PHONE := new EWP.PHONE_NUMBER('+34788877777',null,null);-- Teléfonos han de ser en formato +x...
 v_f.HOUSING_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('FR','https://www.b.fr'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.VISA_INFO.EMAIL := 'c@c.com'; -- Ha de ser en formato x@x.x
 v_f.VISA_INFO.PHONE := new EWP.PHONE_NUMBER('+34885558888',null,null);-- Teléfonos han de ser en formato +x...
 v_f.VISA_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('EN','https://www.s.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.INSURANCE_INFO.EMAIL := 'd@d.com';-- Ha de ser en formato x@x.x
 v_f.INSURANCE_INFO.PHONE := new EWP.PHONE_NUMBER('+34999555999',null,null);-- Teléfonos han de ser en formato +x...
 v_f.INSURANCE_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.es'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.ADDITIONAL_INFO(1).INFO_TYPE := 'service';
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.EMAIL := 'd@d.com';-- Ha de ser en formato x@x.x
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34999999999',null,null);-- Teléfonos han de ser en formato +x...
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.ACCESSIBILITY_REQUIREMENTS(1).REQ_TYPE := 'infrastructure';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).NAME := 'CASA ACOGIDA ESPECIAL';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).DESCRIPTION := 'CASA BONITA PARA PERSONAS CON DISCAPACIDAD';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.EMAIL := 'E@E.com';-- Ha de ser en formato x@x.x
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34000000000',null,null);-- Teléfonos han de ser en formato +x...
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.E.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx

 v_f.ADITIONAL_REQUIREMENTS(1).NAME := 'REQUERIMIENTO ADICIONAL';
 v_f.ADITIONAL_REQUIREMENTS(1).DESCRIPTION := 'descripcion del nuevo requerimiento';
 v_f.ADITIONAL_REQUIREMENTS(1).URLS :=new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('GR','https://www.E.gr'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_r := ewp.PKG_FACTSHEET.UPDATE_FACTSHEET('UV.ES',v_f,v_e);
 
 DBMS_OUTPUT.PUT_LINE('v_salida: '|| v_r);
 DBMS_OUTPUT.PUT_LINE('v_error: '|| v_e);
 
END;