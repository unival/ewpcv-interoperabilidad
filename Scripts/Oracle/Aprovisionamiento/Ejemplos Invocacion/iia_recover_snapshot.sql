SET SERVEROUTPUT ON;
/*
******** PRUEBA RECUPERAR SNAPSHOTS OWNEWP **********
*/
DECLARE
v_return_code NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN
v_return_code := PKG_IIAS.RECOVER_IIA_SNAPSHOT(:iia_id, v_error);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;
