SET SERVEROUTPUT ON;
DECLARE
 v_r NUMBER;
 v_e VARCHAR2(2000);
 v_f PKG_FACTSHEET.FACTSHEET_INSTITUTION;
 v_inst_schac         VARCHAR2(255 char);
BEGIN
 v_inst_schac:= :p_inst_schac;
 v_f.INSTITUTION_ID := v_inst_schac;
 v_f.FACTSHEET.DECISION_WEEK_LIMIT := 3;
 v_f.FACTSHEET.TOR_WEEK_LIMIT := 3;
 v_f.FACTSHEET.NOMINATIONS_AUTUM_TERM := SYSDATE;
 v_f.FACTSHEET.NOMINATIONS_SPRING_TERM := SYSDATE;
 v_f.FACTSHEET.APPLICATION_AUTUM_TERM := SYSDATE;
 v_f.FACTSHEET.APPLICATION_SPRING_TERM := SYSDATE;
 
 v_f.APPLICATION_INFO.EMAIL := 'a@a.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.APPLICATION_INFO.PHONE := new EWP.PHONE_NUMBER('+34666666666',null,null);-- Teléfonos han de ser en formato +x...
 v_f.APPLICATION_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.a.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.HOUSING_INFO.EMAIL := 'b@b.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.HOUSING_INFO.PHONE := new EWP.PHONE_NUMBER('+34777777777',null,null);
 v_f.HOUSING_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.b.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.VISA_INFO.EMAIL := 'c@c.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.VISA_INFO.PHONE := new EWP.PHONE_NUMBER('+34888888888',null,null);
 v_f.VISA_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.c.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.INSURANCE_INFO.EMAIL := 'd@d.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.INSURANCE_INFO.PHONE := new EWP.PHONE_NUMBER('+34999999999',null,null);
 v_f.INSURANCE_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.ADDITIONAL_INFO(1).INFO_TYPE := 'Additional info';
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.EMAIL := 'd@d.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34999999999',null,null);
 v_f.ADDITIONAL_INFO(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
 
 v_f.ACCESSIBILITY_REQUIREMENTS(1).REQ_TYPE := 'infrastructure';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).NAME := 'Infrastructure name';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).DESCRIPTION := 'Infrastructure description';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.EMAIL := 'e@e.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34000000000',null,null);
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.infrastucture.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx

 v_f.ACCESSIBILITY_REQUIREMENTS(1).REQ_TYPE := 'service';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).NAME := 'Service name';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).DESCRIPTION := 'Service name';
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.EMAIL := 'E@E.com'; -- Ha de ser en formato xxx@xxx.xx
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34000000000',null,null);
 v_f.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.service.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
    
 v_r := PKG_FACTSHEET.INSERT_FACTSHEET(v_f,v_e);
 
 DBMS_OUTPUT.PUT_LINE('v_salida: '|| v_r);
 DBMS_OUTPUT.PUT_LINE('v_error: '|| v_e);
 
END;