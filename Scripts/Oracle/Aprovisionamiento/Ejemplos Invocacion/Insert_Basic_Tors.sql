-- INSERTA TOR
DECLARE
  v_return_code       NUMBER;
  v_error_message     VARCHAR2(20000 CHAR);

  -- Tipos principales
  v_tor                   PKG_TORS_FINAL.TOR_SIMPLE_OBJECT;
  v_tor_report_list       PKG_TORS_FINAL.SIMPLE_TOR_REPORT_LIST;
  v_tor_att_list          PKG_TORS_FINAL.TOR_ATTACHMENT_LIST;

  v_tor_id                VARCHAR2(255);
BEGIN
  
  --TOR_ATTACHMENT_LIST
  v_tor_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment BASIC TOR A'));
  v_tor_att_list(0).ATT_TYPE := 3;
  --Test Prueba Insercion A
  v_tor_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2041')));

  v_tor_att_list(1).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment BASIC TOR B'));
  v_tor_att_list(1).ATT_TYPE := 3;
  --Test Prueba Insercion B
  v_tor_att_list(1).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2042')));

  v_tor_att_list(2).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment BASIC TOR C'));
  v_tor_att_list(2).ATT_TYPE := 3;
  --Test Prueba Insercion C
  v_tor_att_list(2).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2043')));
  
  -- TOR_REPORT_LIST 
  v_tor_report_list(0).ISSUE_DATE := sysdate; 
  v_tor_report_list(0).tor_attachment_list :=  v_tor_att_list;
  
   
  -- TOR
  v_tor.GENERATED_DATE := sysdate;
  v_tor.SIMPLE_TOR_REPORT_LIST := v_tor_report_list;
      
  -- LANZAMOS INVOCACIÓN  
  v_return_code := PKG_TORS_FINAL.INSERT_BASIC_TOR(v_tor, :p_la_id, v_tor_id, v_error_message);
  
  DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
  DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
  DBMS_OUTPUT.PUT_LINE('v_tor_id: ' || v_tor_id);
END;