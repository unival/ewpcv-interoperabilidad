SET SERVEROUTPUT ON;

-- INSERTA TOR
DECLARE
  v_return_code       NUMBER;
  v_error_message     VARCHAR2(20000 CHAR);

  -- Tipos principales
  v_tor                   PKG_TORS.TOR;
  v_tor_report_list       PKG_TORS.TOR_REPORT_LIST;
  v_loi_list              PKG_TORS.LOI_LIST;
  v_tor_att_list          PKG_TORS.TOR_ATTACHMENT_LIST;
  v_mob_att_list          PKG_TORS.MOBILITY_ATTACHMENT_LIST;
  v_diploma               PKG_TORS.DIPLOMA;

  -- Tipos secundarios
  v_result_distribution   PKG_TORS.RESULT_DISTRIBUTION;
  v_dist_categories_list  PKG_TORS.DIST_CATEGORIES_LIST;
  v_education_level_1     PKG_TORS.EDUCATION_LEVEL;
  v_education_level_2     PKG_TORS.EDUCATION_LEVEL;
  p_tor_id                VARCHAR2(255);
BEGIN
  -- RESULT_DISTRIBUTION
  v_dist_categories_list(0).LABEL := new OWNEWP.LANGUAGE_ITEM('ES','Dist Category');
  v_dist_categories_list(0).DIST_CAT_COUNT := 1;
  v_result_distribution.DIST_CATEGORIES_LIST := v_dist_categories_list;
  v_result_distribution.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Dist Category'));
  
 --EDUCATION_LEVEL
  v_education_level_1.LEVEL_TYPE := 0;
  v_education_level_1.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 1'));
  v_education_level_1.LEVEL_VALUE := 'Education level value 1';

 --EDUCATION_LEVEL
  v_education_level_2.LEVEL_TYPE := 0;
  v_education_level_2.DESCRIPTION := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Education level DESCRIPTION 2'));
  v_education_level_2.LEVEL_VALUE := 'Education level value 2';
  
  
  -- DIPLOMA
  v_diploma.VERSION := 1;
  v_diploma.ISSUE_DATE := sysdate;
  v_diploma.INTRODUCTION :=hextoraw('546573742050727565626120496E73657263696F6E2041');
  v_diploma.SECTION_LIST(0).TITLE := 'DIPLOMA_SECTION';
  v_diploma.SECTION_LIST(0).SECTION_NUMBER := 0;
  v_diploma.SECTION_LIST(0).CONTENT := hextoraw('546573742050727565626120496E73657263696F6E2041');

    -- LOI_LIST
  v_loi_list(0).START_DATE := sysdate;
  v_loi_list(0).END_DATE := sysdate;
  -- TODO: grading_scheme_id
  v_loi_list(0).STATUS := 1;
  v_loi_list(0).PERCENTAGE_LOWER := '1';
  v_loi_list(0).PERCENTAGE_EQUAL := '1';
  v_loi_list(0).PERCENTAGE_HIGHER := '1';
  v_loi_list(0).RESULT_DISTRIBUTION := v_result_distribution;

  v_loi_list(0).EDUCATION_LEVEL(0) := v_education_level_1;
  v_loi_list(0).EDUCATION_LEVEL(1) := v_education_level_2;

  v_loi_list(0).LANGUAGE_OF_INSTRUCTION := 'es';
  v_loi_list(0).ENGAGEMENT_HOURS := 100;
  v_loi_list(0).ATTACHMENTS_REF_LIST(0) := 'A';
  v_loi_list(0).ATTACHMENTS_REF_LIST(1) := 'C';
  --v_loi_list(0).GROUP_TYPE_ID := 'CEA06BCF-95BF-040B-E053-1FC616ACC714';
  --v_loi_list(0).GROUP_ID := 'CEA06BCF-95C4-040B-E053-1FC616ACC714';
  v_loi_list(0).DIPLOMA := v_diploma;
  -- TODO extension
  v_loi_list(0).LOS_CODE := 'LOS Code';


  
  --TOR_ATTACHMENT_LIST
  v_tor_att_list(0).ATTACH_REFERENCE := 'A'; -- !!!! CAMBIAR para comprobar mÃ©todo de comprobaciÃ³n referencias
  v_tor_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI A'));
  v_tor_att_list(0).ATT_TYPE := 1;
  --Test Prueba Insercion A
  v_tor_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2041')));

  v_tor_att_list(1).ATTACH_REFERENCE := 'B'; -- !!!! CAMBIAR para comprobar mÃ©todo de comprobaciÃ³n referencias
  v_tor_att_list(1).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor SIN LOI'));
  v_tor_att_list(1).ATT_TYPE := 1;
  --Test Prueba Insercion B
  v_tor_att_list(1).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2042')));

  v_tor_att_list(2).ATTACH_REFERENCE := 'C'; -- !!!! CAMBIAR para comprobar mÃ©todo de comprobaciÃ³n referencias
  v_tor_att_list(2).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment tor LOI C'));
  v_tor_att_list(2).ATT_TYPE := 1;
  --Test Prueba Insercion C
  v_tor_att_list(2).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742050727565626120496E73657263696F6E2043')));
  
  -- TOR_REPORT_LIST 
  v_tor_report_list(0).ISSUE_DATE := sysdate; 
  v_tor_report_list(0).loi_list :=  v_loi_list;
  v_tor_report_list(0).tor_attachment_list :=  v_tor_att_list;
  
  
  --MOBILITY_ATTACHMENT_LIST
  v_mob_att_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES','Attachment mobility 1'));
  v_mob_att_list(0).ATT_TYPE := 1;
  -- Test 1 attachment mobility insert
  v_mob_att_list(0).CONTENT := NEW OWNEWP.MULTILANGUAGE_BLOB_LIST(NEW OWNEWP.MULTILANGUAGE_BLOB('ES', hextoraw('546573742031206174746163686D656E74206D6F62696C69747920696E73657274')));
    
  -- TOR
  v_tor.GENERATED_DATE := sysdate;
  v_tor.TOR_REPORT_LIST := v_tor_report_list;
  v_tor.MOBILITY_ATTACHMENT_LIST := v_mob_att_list;
      
  -- LANZAMOS INVOCACIÃ“N  
  v_return_code := PKG_TORS.UPDATE_TOR(:p_tor_id, v_tor, v_error_message, :hei_to_notify);
  
  DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
  DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
  DBMS_OUTPUT.PUT_LINE('p_tor_id: ' || p_tor_id);
END;

