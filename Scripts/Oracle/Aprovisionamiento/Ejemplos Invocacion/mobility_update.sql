SET SERVEROUTPUT ON;
/*
******** PRUEBA UPDATE**********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_la PKG_MOBILITY_LA.LEARNING_AGREEMENT;
v_mobility PKG_MOBILITY_LA.MOBILITY;

--institution
v_s_i_names EWP.LANGUAGE_ITEM_LIST;
v_s_o_names EWP.LANGUAGE_ITEM_LIST;
v_r_i_names EWP.LANGUAGE_ITEM_LIST;
v_r_o_names EWP.LANGUAGE_ITEM_LIST;

--student
v_st_contact EWP.CONTACT;
v_st_emails EWP.EMAIL_LIST;
v_st_contact_names EWP.LANGUAGE_ITEM_LIST;
v_st_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_st_recipient_name EWP.RECIPIENT_NAME_LIST;
v_st_address_lines EWP.ADDRESS_LINE_LIST;

--sender Contact
v_s_i_c_contact EWP.CONTACT;
v_s_i_c_emails EWP.EMAIL_LIST;
v_s_i_c_contact_names EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_recipient_name EWP.RECIPIENT_NAME_LIST;
v_s_i_c_address_lines EWP.ADDRESS_LINE_LIST;

--receiver Contact
v_r_i_c_contact EWP.CONTACT;
v_r_i_c_emails EWP.EMAIL_LIST;
v_r_i_c_contact_names EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_recipient_name EWP.RECIPIENT_NAME_LIST;
v_r_i_c_address_lines EWP.ADDRESS_LINE_LIST;



v_components PKG_MOBILITY_LA.LA_COMPONENT_LIST;
BEGIN

v_mobility.PLANED_ARRIVAL_DATE := sysdate;
v_mobility.PLANED_DEPATURE_DATE := sysdate;
v_mobility.IIA_ID:='CA61E129-6F17-5C32-E053-1FC616AC6157';
v_mobility.COOPERATION_CONDITION_ID:='CA61E4EF-BED7-5C36-E053-1FC616AC2288';
v_mobility.EQF_LEVEL_DEPARTURE:=5;
v_mobility.EQF_LEVEL_NOMINATION:=8;
--v_mobility.STATUS_OUTGOING:=0;

v_mobility_academic_term := new EWP.ACADEMIC_TERM('2020/2021','tlu.ee', NULL,sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre Test 1 Javi')),1,2);
v_mobility.ACADEMIC_TERM := v_mobility_academic_term;

v_s_i_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Universidad de Valencia'),new EWP.LANGUAGE_ITEM('EN','University of Valencia'));
v_s_o_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Departamento de prueba'));
v_r_i_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','universidad de Murcia'));
v_r_o_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Departamento de pruebah'));
v_mobility.sending_institution  := new EWP.INSTITUTION('UV.ES',v_s_i_names,v_s_o_names,'ounit.uv');
v_mobility.receiving_institution  := new EWP.INSTITUTION('UM.ES', v_r_i_names,v_r_o_names,'ounit.um');

v_mobility.student.global_id := 'ID_LUIS_ACTUALIZADO';
v_st_emails := new EWP.EMAIL_LIST('lsanchezpe@minsait.com');-- Ha de ser en formato xxx@xxx.xx
v_st_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Mi contacto'));
v_st_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto de Luis'));
v_st_recipient_name := new EWP.RECIPIENT_NAME_LIST('Casa');
v_st_address_lines := new EWP.ADDRESS_LINE_LIST('Avda. Jean Claude Combaldieu, s/n');
v_st_contact := new EWP.CONTACT(v_st_contact_names,v_st_contact_descriptions,null,'student','UV.ES','ounit.uv', v_st_emails,
	new EWP.FLEXIBLE_ADDRESS (v_st_recipient_name,v_st_address_lines,null,null,null,null,null,null,null,'03008','Alicante', 'Comunidad Valenciana', 'ES'),
    null,
	new EWP.PHONE_NUMBER ('+34666666666',null,null),-- Teléfonos han de ser en formato +x...
    null);

v_mobility.student.contact_person := new EWP.CONTACT_PERSON('Luis','Sanchez Perez','21/02/1993','ES',1,v_st_contact);

v_st_photo_urls(1).URL := 'http://www.testjaviurlfoto.com/fotogeneral.jpg';-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_st_photo_urls(1).PHOTO_SIZE := '800x600';
v_st_photo_urls(1).PHOTO_DATE := TO_DATE('22/01/2021', 'dd/MM/yyyy');
v_st_photo_urls(1).PHOTO_PUBLIC := 0;

v_st_photo_urls(2).URL := 'http://www.testjaviurlfoto.com/fotocarnet.jpg';-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_st_photo_urls(2).PHOTO_SIZE := '35x45';
v_st_photo_urls(2).PHOTO_DATE := TO_DATE('22/01/2021', 'dd/MM/yyyy');
v_st_photo_urls(2).PHOTO_PUBLIC := 1;

v_mobility.student.photo_url_list := v_st_photo_urls;

v_s_i_c_emails := new EWP.EMAIL_LIST('sender_coordinator@email.com');-- Ha de ser en formato xxx@xxx.xx
v_s_i_c_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto s i c'));
v_s_i_c_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto del coordinador de la institucion emisora'));
v_s_i_c_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uv.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_s_i_c_recipient_name := new EWP.RECIPIENT_NAME_LIST('institucion emisora');
v_s_i_c_address_lines := new EWP.ADDRESS_LINE_LIST('Av Blasco Ibañez, 13');

v_s_i_c_contact := new EWP.CONTACT(v_s_i_c_contact_names,v_s_i_c_contact_descriptions,v_s_i_c_contact_urls,'coordinator','UV.ES','ounit.uv', v_s_i_c_emails,
	new EWP.FLEXIBLE_ADDRESS (v_s_i_c_recipient_name,v_s_i_c_address_lines,null,null,null,null,null,null,null,'460010','Valencia', 'Comunidad Valenciana', 'ES'),
    null,
	new EWP.PHONE_NUMBER ('+34111111111',null,null),-- Teléfonos han de ser en formato +x...
    null);

v_r_i_c_emails := new EWP.EMAIL_LIST('receiver_coordinator@email.com');-- Ha de ser en formato xxx@xxx.xx
v_r_i_c_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto r i c'));
v_r_i_c_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto del coordinador de la institucion receptora'));
v_r_i_c_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.um.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_r_i_c_recipient_name := new EWP.RECIPIENT_NAME_LIST('institucion receptora');
v_r_i_c_address_lines := new EWP.ADDRESS_LINE_LIST('Paseo del Teniente Flomesta, 3');

v_r_i_c_contact := new EWP.CONTACT(v_r_i_c_contact_names,v_r_i_c_contact_descriptions,v_r_i_c_contact_urls,'coordinator','EM.ES','ounit.um', v_s_i_c_emails,
	new EWP.FLEXIBLE_ADDRESS (v_s_i_c_recipient_name,v_s_i_c_address_lines,null,null,null,null,null,null,null,'30003','Murcia', 'Region de murcia', 'ES'),
    null,
	new EWP.PHONE_NUMBER ('+34222222222',null,null),-- Teléfonos han de ser en formato +x...
    null);

	
v_mobility.SENDER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Sender Institution',TO_DATE('01/01/1960', 'dd/MM/yyyy'),'ES',0,v_s_i_c_contact);
v_mobility.RECEIVER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Receiver Institution',TO_DATE('01/01/1960', 'dd/MM/yyyy'),'ES',0,v_r_i_c_contact);
v_mobility.SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
v_mobility.STUDENT_LANGUAGE_SKILLS := new EWP.LANGUAGE_SKILL_LIST (new EWP.LANGUAGE_SKILL('es','C2'), new EWP.LANGUAGE_SKILL('en','B2'));
v_mobility.MOBILITY_TYPE := 'Semester';

v_return_code := PKG_MOBILITY_LA.UPDATE_MOBILITY(:p_mobility_id, v_mobility, v_error_message, :p_hei_to_notify);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('id_retorno '|| v_id_retorno);
END;