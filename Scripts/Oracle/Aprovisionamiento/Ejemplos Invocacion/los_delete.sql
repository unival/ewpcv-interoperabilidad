SET SERVEROUTPUT ON;
DECLARE
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
   	v_los_id VARCHAR2(255); 
   
    
BEGIN
	v_los_id := '';

	v_cod_retorno := EWP.PKG_LOS.DELETE_LOS(:v_los_id, v_error);
	
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;