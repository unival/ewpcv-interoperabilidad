SET SERVEROUTPUT ON;
/*
******** PRUEBA INSERCION EWP **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_institution PKG_INSTITUTION.INSTITUTION;

-- Contact
v_contact EWP.CONTACT;
v_contact_emails EWP.EMAIL_LIST;
v_contact_names EWP.LANGUAGE_ITEM_LIST;
v_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_address_lines EWP.ADDRESS_LINE_LIST;

v_contact_list EWP.CONTACT;
v_contact_list_emails EWP.EMAIL_LIST;
v_contact_list_names EWP.LANGUAGE_ITEM_LIST;
v_contact_list_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_list_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_list_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_list_address_lines EWP.ADDRESS_LINE_LIST;

BEGIN

/* 
	INICIO
*/

--Primary contact
v_contact_emails := new EWP.EMAIL_LIST('primarycontactModif@valencia.com');-- Ha de ser en formato xxx@xxx.xx
v_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Modif Primary Contact Valencia'));
v_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Modif Contacto primary para institution'));
v_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uvModif.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Modif Institution Primary Contact');
v_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Modif Av Blasco Ibañez(Primary), 13');

v_contact := new EWP.CONTACT(v_contact_names,v_contact_descriptions,v_contact_urls,'coordinator',null,null, v_contact_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Modif Street 57(Primary)','Modif Street Edificio nombre(Primary)','Street Calle nombre(Primary)','Street 2(Primary)','Street 3(Primary)','Street Post Office Box(Primary)',null,'Street 03300','Street Orihuela', 'Street Comunidad Valenciana', 'ES'),
    new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Modif Mailing 53(Primary)','Modif Mailing Edificio nombre(Primary)','Mailing Calle nombre(Primary)','Mailing 2(Primary)','Mailing 3(Primary)','Mailing Post Office Box(Primary)',null,'Mailing 03300','Mailing Orihuela', 'Mailing Comunidad Valenciana', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null));-- Teléfonos han de ser en formato +x...

-- Contact para Contact Detail List

v_contact_list_emails := new EWP.EMAIL_LIST('contactlistModif@valencia.com');-- Ha de ser en formato xxx@xxx.xx
v_contact_list_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Santiago(List)'));
v_contact_list_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto para List'));
v_contact_list_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uvModif.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_list_recipient_name := new EWP.RECIPIENT_NAME_LIST('Modif (List)Institution contact list');
v_contact_list_address_lines := new EWP.ADDRESS_LINE_LIST('Modif (List)Av Blasco Ibañez, 13');

v_contact_list := new EWP.CONTACT(v_contact_list_names,v_contact_list_descriptions,v_contact_list_urls,'coordinator',null,null, v_contact_list_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Street 57(List)','Modif Street Edificio nombre(List)','Modif Street Calle nombre(List)','Modif Street 2(List)','Street 3(List)','Street Post Office Box(List)',null,'Street 03300','Street Orihuela', 'Street Comunidad Valenciana', 'ES'),
    new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Mailing 53(List)','Modif Mailing Edificio nombre(List)','Modif Mailing Calle nombre(List)','Modif Mailing 2(List)','Mailing 3(List)','Mailing Post Office Box(List)',null,'Mailing 03300','Mailing Orihuela', 'Mailing Comunidad Valenciana', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null));-- Teléfonos han de ser en formato +x...

--INSTITUTION
v_institution.INSTITUTION_ID := 'uv.es';
v_institution.INSTITUTION_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Modif Institución de pruebas Valencia (PL SANTIAGO)'));
v_institution.ABBREVIATION := 'Univ. Valencia';
v_institution.UNIVERSITY_CONTACT_DETAILS := v_contact;
v_institution.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Modif Contact','Modif Person List Valencia','01/01/1960','ES',0,v_contact_list));
v_institution.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','http://www.factsheetmodif.es'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
/*
	FIN
*/


v_return_code := PKG_INSTITUTION.UPDATE_INSTITUTION(:v_id, v_institution, v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('v_id_retorno '|| v_id_retorno);

END;