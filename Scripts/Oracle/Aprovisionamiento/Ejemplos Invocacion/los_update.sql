DECLARE
	v_los EWP.PKG_LOS.LOS;
	v_error VARCHAR2(4000);
	v_cod_retorno NUMBER(1);
    v_names EWP.LANGUAGE_ITEM_LIST;
    v_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_urls EWP.LANGUAGE_ITEM_LIST;
    v_los_row EWP.EWPCV_LOS%ROWTYPE;
   	v_los_id VARCHAR2(255); 
	v_ounit_code VARCHAR2(255);
	v_hei_id VARCHAR2(255);
   	v_ounit_id VARCHAR2(255);
	
    CURSOR c_los(p_los_id IN VARCHAR2) IS 
    	SELECT * FROM EWP.EWPCV_LOS 
    	WHERE ID = p_los_id;
		
	CURSOR c_ounit(p_ounit_code IN VARCHAR2, p_hei IN VARCHAR2) IS 
		SELECT OUNIT_ID FROM EWP.OUNIT_VIEW  
		WHERE OUNIT_CODE = p_ounit_code
		AND INSTITUTION_ID = p_hei;
BEGIN
	v_los_id := 'DEP/CD97791E-8B90-67F4-E053-1FC616ACF1F8';
	OPEN c_los(v_los_id);
	FETCH c_los INTO v_los_row;
	CLOSE c_los;

	v_ounit_code := 'ounit.code';	
	v_hei_id := 'uv.es';
	
  	v_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Maths - Test1'));
    v_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('en', 'Desc Maths - Test1'), 
		new EWP.LANGUAGE_ITEM('es', 'Desc Matemáticas - Test1'));
    v_urls := new EWP.LANGUAGE_ITEM_LIST();
   
	OPEN c_ounit(v_ounit_code, v_hei_id);
	FETCH c_ounit INTO v_ounit_id;
	CLOSE c_ounit;

	v_los := NEW EWP.PKG_LOS.LOS(5, v_hei_id, 'iscedf-test1', 'code-pl-test1', v_names, v_descriptions, v_urls, v_ounit_id, 'subject-test1', 3);
	v_names.extend(1);
	v_names(v_names.COUNT) := new EWP.LANGUAGE_ITEM('es', 'Matemáticas - Test1');
	v_los.LOS_NAME_LIST := v_names;

	v_cod_retorno := EWP.PKG_LOS.UPDATE_LOS(v_los_id, v_los, v_error);
	
	DBMS_OUTPUT.PUT_LINE('v_cod_retorno: ' || v_cod_retorno);
	DBMS_OUTPUT.PUT_LINE('v_error: ' || v_error);
END;