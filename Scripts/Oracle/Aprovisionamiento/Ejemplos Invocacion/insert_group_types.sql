DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_group_list PKG_TORS.GROUP_LIST;
v_group_type_list PKG_TORS.GROUP_TYPE_LIST;
BEGIN

v_group_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','Obligatory');
v_group_list(0).SORTING_KEY := 1; 
v_group_list(1).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','Optional');
v_group_list(1).SORTING_KEY := 2;

v_group_type_list(0).TITLE := new OWNEWP.LANGUAGE_ITEM('ES','Mandatory');
v_group_type_list(0).GROUP_LIST := v_group_list;

v_return_code := PKG_TORS.INSERT_GROUP_TYPE_LIST(v_group_type_list, v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);

END;