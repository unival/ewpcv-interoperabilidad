SET SERVEROUTPUT ON;
/*
******** PRUEBA ACTUALIZACION **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_iia PKG_IIAS.IIA;
v_coop_cond PKG_IIAS.COOPERATION_CONDITION_LIST;
v_sending_partner PKG_IIAS.PARTNER;
v_receiving_partner PKG_IIAS.PARTNER;
v_institution_send EWP.INSTITUTION;
v_institution_receiv EWP.INSTITUTION;

--sender Contact
v_s_i_c_contact EWP.CONTACT;
v_s_i_c_emails EWP.EMAIL_LIST;
v_s_i_c_contact_names EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_s_i_c_recipient_name EWP.RECIPIENT_NAME_LIST;
v_s_i_c_address_lines EWP.ADDRESS_LINE_LIST;

--receiver Contact
v_r_i_c_contact EWP.CONTACT;
v_r_i_c_emails EWP.EMAIL_LIST;
v_r_i_c_contact_names EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_r_i_c_recipient_name EWP.RECIPIENT_NAME_LIST;
v_r_i_c_address_lines EWP.ADDRESS_LINE_LIST;

BEGIN

--IIA
v_iia.IIA_CODE := 'IIA_EWP_modificado';
v_iia.start_date := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_iia.end_date := TO_DATE('2022/07/09', 'yyyy/mm/dd');

--COOP COND
v_coop_cond(1).START_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_coop_cond(1).END_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_coop_cond(1).EQF_LEVEL_LIST(1):= 1;
v_coop_cond(1).mobility_number := 7;
v_coop_cond(1).MOBILITY_TYPE.MOBILITY_CATEGORY := 'STUDIES';
v_coop_cond(1).MOBILITY_TYPE.MOBILITY_GROUP := 'STUDENT';
v_coop_cond(1).COP_COND_DURATION.NUMBER_DURATION := 6;
v_coop_cond(1).COP_COND_DURATION.UNIT := 3;
v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).LANGUAGE_SKILL := new EWP.LANGUAGE_SKILL('es','B2');
v_coop_cond(1).blended := 0;

v_coop_cond(2).START_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_coop_cond(2).END_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_coop_cond(2).EQF_LEVEL_LIST(1) := 1;
v_coop_cond(2).mobility_number := 1;
v_coop_cond(2).MOBILITY_TYPE.MOBILITY_CATEGORY := 'STUDIES';
v_coop_cond(2).MOBILITY_TYPE.MOBILITY_GROUP := 'STUDENT';
v_coop_cond(2).COP_COND_DURATION.NUMBER_DURATION := 2;
v_coop_cond(2).COP_COND_DURATION.UNIT := 3;
v_coop_cond(2).SUBJECT_AREA_LANGUAGE_LIST(1).SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
v_coop_cond(2).SUBJECT_AREA_LANGUAGE_LIST(1).LANGUAGE_SKILL := new EWP.LANGUAGE_SKILL('es','B2');
v_coop_cond(2).blended := 0;

--INSTITUION_SEND
v_institution_send := new EWP.INSTITUTION('UV.ES',
    new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Universidad de Valencia')),
    new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Departamento de prueba')),
    'OunitCodeUV');

--INSTITUION_RECEIV
v_institution_receiv := new EWP.INSTITUTION('UM.ES',
    new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Universidad de Murcia')),
    new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Departamento de prueba murcia')),
    'OunitCodeUM');




/* INICIO
Datos para sender y receiver contacts
*/
v_s_i_c_emails := new EWP.EMAIL_LIST('sender_coordinator@email.com'); -- Ha de ser en formato xxx@xxx.xx
v_s_i_c_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto s i c'));
v_s_i_c_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto del coordinador de la institucion emisora'));
v_s_i_c_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uv.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_s_i_c_recipient_name := new EWP.RECIPIENT_NAME_LIST('institucion emisora');
v_s_i_c_address_lines := new EWP.ADDRESS_LINE_LIST('Av Blasco Iba�ez, 13');

v_s_i_c_contact := new EWP.CONTACT(v_s_i_c_contact_names,v_s_i_c_contact_descriptions,v_s_i_c_contact_urls,'coordinator','UV.ES','ounit.uv', v_s_i_c_emails,
    new EWP.FLEXIBLE_ADDRESS (v_s_i_c_recipient_name,v_s_i_c_address_lines,null,null,null,null,null,null,null,'460010','Valencia', 'Comunidad Valenciana', 'ES'), null,
    new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34111111111',null,null));-- Tel�fonos han de ser en formato +x...

v_r_i_c_emails := new EWP.EMAIL_LIST('receiver_coordinator@email.com'); -- Ha de ser en formato xxx@xxx.xx
v_r_i_c_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto r i c'));
v_r_i_c_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto del coordinador de la institucion receptora'));
v_r_i_c_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.um.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_r_i_c_recipient_name := new EWP.RECIPIENT_NAME_LIST('institucion receptora');
v_r_i_c_address_lines := new EWP.ADDRESS_LINE_LIST('Paseo del Teniente Flomesta, 3');

v_r_i_c_contact := new EWP.CONTACT(v_r_i_c_contact_names,v_r_i_c_contact_descriptions,v_r_i_c_contact_urls,'coordinator','UM.ES','ounit.uv', v_s_i_c_emails,
    new EWP.FLEXIBLE_ADDRESS (v_s_i_c_recipient_name,v_s_i_c_address_lines,null,null,null,null,null,null,null,'30003','Murcia', 'Region de murcia', 'ES'), null,
    new EWP.PHONE_NUMBER ('+34222222222',null,null), new EWP.PHONE_NUMBER ('+34111111111',null,null));-- Tel�fonos han de ser en formato +x...
	
/*
 FIN
*/

--RECEIVING PARTNER
v_receiving_partner.institution := v_institution_receiv;
v_receiving_partner.signing_date := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_receiving_partner.PARTNER_CONTACTS := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Coordinador receiver','Receiver Institution','01/01/1960','ES',0,v_r_i_c_contact));
v_receiving_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('Coordinador receiver','Receiver Institution','01/01/1960','ES',0,v_r_i_c_contact);

--SENDING PARTNER
v_sending_partner.signing_date := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_sending_partner.institution := v_institution_send;
v_sending_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('Coordinador sender','Sender Institution','01/01/1960','ES',0,v_s_i_c_contact);


v_coop_cond(1).RECEIVING_PARTNER := v_receiving_partner;
v_coop_cond(1).SENDING_PARTNER := v_sending_partner;

v_coop_cond(2).RECEIVING_PARTNER := v_sending_partner;
v_coop_cond(2).SENDING_PARTNER := v_receiving_partner;

v_iia.COOPERATION_CONDITION_LIST := v_coop_cond;

--DESCOMENTAR SI SE QUIERE INSERTAR UN FICHERO
--v_iia.PDF := utl_raw.cast_to_raw('Texto de prueba');
--v_iia.FILE_MIME_TYPE := 'text/plain';

v_return_code := PKG_IIAS.UPDATE_IIA(:p_id_actualizar, v_iia, v_error_message, :p_hei_to_notify);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('v_id_retorno '|| v_id_retorno);

END;