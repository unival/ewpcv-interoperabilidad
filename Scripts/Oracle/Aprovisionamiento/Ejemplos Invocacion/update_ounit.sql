SET SERVEROUTPUT ON;

DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_ounit PKG_OUNITS.OUNIT;

-- Contact
v_contact EWP.CONTACT;
v_contact_emails EWP.EMAIL_LIST;
v_contact_names EWP.LANGUAGE_ITEM_LIST;
v_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_address_lines EWP.ADDRESS_LINE_LIST;

v_contact_list EWP.CONTACT;
v_contact_list_emails EWP.EMAIL_LIST;
v_contact_list_names EWP.LANGUAGE_ITEM_LIST;
v_contact_list_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_list_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_list_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_list_address_lines EWP.ADDRESS_LINE_LIST;

BEGIN

/* 
	INICIO
*/

--Primary contact
v_contact_emails := new EWP.EMAIL_LIST('primarycontactOUNIT@email.com');-- Ha de ser en formato xxx@xxx.xx
v_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primary Contact OUNIT'));
v_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto primary para institution OUNIT'));
v_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uvOUNIT.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Institution Primary Contact OUNIT');
v_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Av Blasco Ibañez OUNIT, 13');

v_contact := new EWP.CONTACT(v_contact_names,v_contact_descriptions,v_contact_urls,'coordinatorOUNIT',null,null, v_contact_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_recipient_name,v_contact_address_lines,'57(Primary)', 'Street OUNIT Edificio nombre(Primary)','Street Calle nombre(Primary)','2(Primary)','3(Primary)','Post Office Box(Primary)',null,'460010','Valencia', 'Comunidad Valenciana', 'ES'),
    new EWP.FLEXIBLE_ADDRESS (v_contact_recipient_name,v_contact_address_lines,'57(Primary)', 'Mailing OUNIT Edificio nombre(Primary)','Mailing Calle nombre(Primary)','2(Primary)','3(Primary)','Post Office Box(Primary)',null,'460010','Valencia', 'Comunidad Valenciana', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null));-- Teléfonos han de ser en formato +x...

-- Contact para Contact Detail List

v_contact_list_emails := new EWP.EMAIL_LIST('ssernaOUNIT@minsait.com');-- Ha de ser en formato xxx@xxx.xx
v_contact_list_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Santiago OUNIT'));
v_contact_list_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contacto para List OUNIT'));
v_contact_list_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.uvOUNIT.es/'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_list_recipient_name := new EWP.RECIPIENT_NAME_LIST('OUNIT contact list ');
v_contact_list_address_lines := new EWP.ADDRESS_LINE_LIST('Av Blasco Ibañez OUNIT, 13');

v_contact_list := new EWP.CONTACT(v_contact_list_names,v_contact_list_descriptions,v_contact_list_urls,'coordinatorOUNIT',null,null, v_contact_list_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'57OUNIT(List)','Street OUNITEdificio nombre(List)','Street OUNITCalle nombre(List)','2(List)','3(List)','Post Office Box(List)',null,'03300','Orihuela', 'Comunidad Valenciana', 'ES'),
    new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'57OUNIT(List)','Mailing OUNITEdificio nombre(List)','Mailing OUNITCalle nombre(List)','2(List)','3(List)','Post Office Box(List)',null,'03300','Orihuela', 'Comunidad Valenciana', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null));-- Teléfonos han de ser en formato +x...

--INSTITUTION
v_ounit.HEI_ID := 'uv.es';
v_ounit.OUNIT_CODE := 'ounit.CHILD2.uv';
v_ounit.OUNIT_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','OUNIT Faculty of Mathematics, Informatics, and Mechanics'));
v_ounit.ABBREVIATION := 'Maths';
v_ounit.OUNIT_CONTACT_DETAILS := v_contact;
v_ounit.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('ContactOUNIT','Person List OUNIT','01/01/1960','ES',0,v_contact_list));
v_ounit.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','http://www.factsheetOUNIT.es'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_ounit.LOGO_URL := 'http://www.logoOUNIT.es';-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_ounit.IS_TREE_STRUCTURE := 0;
v_ounit.PARENT_OUNIT_ID := ('C9F8E7A6-F7B6-591B-E053-1FC616AC5543');

/*
	FIN
*/


v_return_code := PKG_OUNITS.UPDATE_OUNIT(:v_id_ounit, v_ounit, v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('v_id_retorno '|| v_id_retorno);

END;
