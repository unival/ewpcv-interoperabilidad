SET SERVEROUTPUT ON;
/*
******** PRUEBA PREPARAR SNPSHOTS OWNEWP **********
*/
DECLARE
v_return_code           NUMBER;
v_error_message         VARCHAR2(20000 CHAR);
BEGIN

v_return_code := PKG_IIAS.PREPARE_SNAPSHOTS(v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);

END;