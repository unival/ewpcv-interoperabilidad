SET SERVEROUTPUT ON;
/*
******** PRUEBA INSERCION **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_id_retorno VARCHAR2(255 char);
v_la PKG_MOBILITY_LA.LEARNING_AGREEMENT;
v_mobility PKG_MOBILITY_LA.MOBILITY;
v_inst_schac         VARCHAR2(255 char);
v_r_inst_schac         VARCHAR2(255 char);

--institution
v_s_i_names EWP.LANGUAGE_ITEM_LIST;
v_s_o_names EWP.LANGUAGE_ITEM_LIST;
v_r_i_names EWP.LANGUAGE_ITEM_LIST;
v_r_o_names EWP.LANGUAGE_ITEM_LIST;

v_mobility_academic_term    EWP.ACADEMIC_TERM;

--student
v_st_contact EWP.CONTACT;
v_st_emails EWP.EMAIL_LIST;
v_st_contact_names EWP.LANGUAGE_ITEM_LIST;
v_st_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_st_recipient_name EWP.RECIPIENT_NAME_LIST;
v_st_address_lines EWP.ADDRESS_LINE_LIST;
v_st_photo_urls PKG_MOBILITY_LA.PHOTO_URL_LIST;

-- Contact
v_contact EWP.CONTACT;
v_contact_emails EWP.EMAIL_LIST;
v_contact_names EWP.LANGUAGE_ITEM_LIST;
v_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_address_lines EWP.ADDRESS_LINE_LIST;

v_contact_list EWP.CONTACT;
v_contact_list_emails EWP.EMAIL_LIST;
v_contact_list_names EWP.LANGUAGE_ITEM_LIST;
v_contact_list_descriptions EWP.LANGUAGE_ITEM_LIST;
v_contact_list_urls EWP.LANGUAGE_ITEM_LIST;
v_contact_list_recipient_name EWP.RECIPIENT_NAME_LIST;
v_contact_list_address_lines EWP.ADDRESS_LINE_LIST;

v_components PKG_MOBILITY_LA.LA_COMPONENT_LIST;

BEGIN

--Primary contact
v_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
v_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Jon Test'));
v_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
v_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
v_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');

v_contact := new EWP.CONTACT(v_contact_names,v_contact_descriptions,v_contact_urls,'coordinator',null,null, v_contact_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...

-- Contact para Contact Detail List

v_contact_list_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
v_contact_list_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','SJon Test'));
v_contact_list_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
v_contact_list_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
v_contact_list_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
v_contact_list_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');

v_contact_list := new EWP.CONTACT(v_contact_list_names,v_contact_list_descriptions,v_contact_list_urls,'coordinator',null,null, v_contact_list_emails,
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
	new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...


v_inst_schac:= :p_inst_schac;
v_r_inst_schac:= :p_r_inst_schac;
 -- MOBILITY
v_mobility.PLANED_ARRIVAL_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_mobility.PLANED_DEPATURE_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
v_mobility.EQF_LEVEL_DEPARTURE:=6;
v_mobility.EQF_LEVEL_NOMINATION:=7;
v_mobility_academic_term := new EWP.ACADEMIC_TERM('2021/2021', v_inst_schac, null, sysdate, sysdate, 
                            new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Academic Term')),1,2);
v_mobility.ACADEMIC_TERM := v_mobility_academic_term;
v_mobility.sending_institution  := new EWP.INSTITUTION(v_inst_schac, null, null, null);
v_mobility.receiving_institution  := new EWP.INSTITUTION(v_r_inst_schac, null, null, null);
v_mobility.student.global_id := 'Student_Test_Global_id';
v_mobility.student.contact_person := new EWP.CONTACT_PERSON('Jon Test','Doe Test','21/02/1993','ES', 1, v_contact);
v_st_photo_urls(1).URL := 'https://www.mobility_foto_urls.com/foto.jpg';-- Urls han de ser en formato http?s://xxxx.xxx.xx
v_st_photo_urls(1).PHOTO_SIZE := '800x600';
v_st_photo_urls(1).PHOTO_DATE := sysdate;
v_st_photo_urls(1).PHOTO_PUBLIC := 0;
v_mobility.student.photo_url_list := v_st_photo_urls;
v_mobility.SENDER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Sender Institution', sysdate,'ES',0 ,v_contact);
v_mobility.RECEIVER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Receiver Institution',sysdate,'ES', 0, v_contact);
v_mobility.SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
v_mobility.STUDENT_LANGUAGE_SKILLS := new EWP.LANGUAGE_SKILL_LIST (new EWP.LANGUAGE_SKILL('es','C2'), new EWP.LANGUAGE_SKILL('en','B2'));
v_mobility.MOBILITY_TYPE := 'Semester';

v_return_code := PKG_MOBILITY_LA.INSERT_mobility(v_mobility, v_id_retorno, v_error_message, v_r_inst_schac);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
DBMS_OUTPUT.PUT_LINE('id_retorno '|| v_id_retorno);
END;