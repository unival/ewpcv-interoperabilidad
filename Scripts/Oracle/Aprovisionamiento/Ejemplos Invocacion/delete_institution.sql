/*
******** PRUEBA BORRADO **********
*/
DECLARE 
v_status NUMBER;
v_error VARCHAR2(2000 CHAR);
BEGIN 
    v_status := PKG_INSTITUTION.DELETE_INSTITUTION(:p_inst_id, v_error);
    DBMS_OUTPUT.PUT_LINE('v_status '|| v_status);
    DBMS_OUTPUT.PUT_LINE('v_error '|| v_error);
END;