/*
******** PRUEBA ACEPTACION **********
*/
DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_signature PKG_MOBILITY_LA.SIGNATURE;
BEGIN


v_signature.SIGNER_NAME := 'mr receiver';
v_signature.SIGNER_POSITION := 'coordinator';
v_signature.SIGNER_EMAIL := ' c@c.com';-- Ha de ser en formato xxx@xxx.xx
v_signature.SIGN_DATE := SYSTIMESTAMP;

v_return_code := PKG_MOBILITY_LA.ACCEPT_LEARNING_AGREEMENT(:p_la_id, :p_la_revision, v_signature, v_error_message, :hei_to_notify);
DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);
END;