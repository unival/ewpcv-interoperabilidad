DECLARE
v_return_code NUMBER;
v_error_message VARCHAR2(20000 CHAR);
v_grading_scheme PKG_TORS.GRADING_SCHEME;
BEGIN

v_grading_scheme.label := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES', 'EQF'));
v_grading_scheme.description := new OWNEWP.LANGUAGE_ITEM_LIST(new OWNEWP.LANGUAGE_ITEM('ES', 'EQF Description'));


v_return_code := PKG_TORS.INSERT_GRADING_SCHEME(v_grading_scheme, v_error_message);

DBMS_OUTPUT.PUT_LINE('return code '|| v_return_code);
DBMS_OUTPUT.PUT_LINE('error '|| v_error_message);

END;