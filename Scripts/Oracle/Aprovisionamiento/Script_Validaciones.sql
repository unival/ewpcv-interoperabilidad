DECLARE
    v_id                        VARCHAR2(255 char);
    ----------------------------------------------------------
    -- Declaraciones de ids devueltos por aprovisionamiento --
    ----------------------------------------------------------
    v_institution_id            VARCHAR2(255 char);
    v_institution_partner_id    VARCHAR2(255 char);
    v_ounit_id                  VARCHAR2(255 char);
    v_factsheet_id              VARCHAR2(255 char);
    v_iia_id                    VARCHAR2(255 char);
    v_coop_cond_id              VARCHAR2(255 char);
    v_mobility_id               VARCHAR2(255 char);
    v_la_id                     VARCHAR2(255 char);
    
    --------------------------------------------
    -- Declaraciones de types de los paquetes --
    --------------------------------------------
    
    -- INSTITUTION
    v_institution               PKG_INSTITUTION.INSTITUTION;
    v_institution_partner       PKG_INSTITUTION.INSTITUTION;
    
    -- OUNIT
    v_ounit                     PKG_OUNITS.OUNIT;
    
    -- FACTSHEET
    v_factsheet                 PKG_FACTSHEET.FACTSHEET_INSTITUTION;
    
    -- IIAS
    v_iia                       PKG_IIAS.IIA;
    v_coop_cond                 PKG_IIAS.COOPERATION_CONDITION_LIST;
    v_sending_partner           PKG_IIAS.PARTNER;
    v_receiving_partner         PKG_IIAS.PARTNER;
    v_inst_iia_send             EWP.INSTITUTION;
    v_inst_iia_receiv           EWP.INSTITUTION;
    
    -- MOBILITY
    v_mobility                  PKG_MOBILITY_LA.MOBILITY;
    v_st_photo_urls             PKG_MOBILITY_LA.PHOTO_URL_LIST;
    v_mobility_academic_term    EWP.ACADEMIC_TERM;
    
    -- LEARNING AGREEMENT
    v_la                        PKG_MOBILITY_LA.LEARNING_AGREEMENT;
    v_components                PKG_MOBILITY_LA.LA_COMPONENT_LIST;
    
    -------------------
    -- Types comunes --
    -------------------
    
    -- Contact
    v_contact EWP.CONTACT;
    v_contact_emails EWP.EMAIL_LIST;
    v_contact_names EWP.LANGUAGE_ITEM_LIST;
    v_contact_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_contact_urls EWP.LANGUAGE_ITEM_LIST;
    v_contact_recipient_name EWP.RECIPIENT_NAME_LIST;
    v_contact_address_lines EWP.ADDRESS_LINE_LIST;
    
    v_contact_list EWP.CONTACT;
    v_contact_list_emails EWP.EMAIL_LIST;
    v_contact_list_names EWP.LANGUAGE_ITEM_LIST;
    v_contact_list_descriptions EWP.LANGUAGE_ITEM_LIST;
    v_contact_list_urls EWP.LANGUAGE_ITEM_LIST;
    v_contact_list_recipient_name EWP.RECIPIENT_NAME_LIST;
    v_contact_list_address_lines EWP.ADDRESS_LINE_LIST;
    
    ----------------------------------------------------------
    -- Variables necesarias durante la ejecución del script --
    ----------------------------------------------------------
    
    v_inst_schac         VARCHAR2(255 char);
    v_inst_schac_partner VARCHAR2(255 char) := 'validator-hei01.developers.erasmuswithoutpaper.eu';
    v_ounit_code         VARCHAR2(255 char);
    v_exist              NUMBER := 0;
    v_return_code        NUMBER;
    v_error_message      VARCHAR2(20000 CHAR);
    CURSOR c_coop_cond(p_iia_id IN VARCHAR2) IS SELECT ID
			FROM EWPCV_COOPERATION_CONDITION
			WHERE IIA_ID = p_iia_id;
BEGIN
    DELETE FROM EWPCV_DIPLOMA_SEC_ATTACH CASCADE;
	DELETE FROM EWPCV_DIPLOMA_SECTION_SECTION CASCADE;
	DELETE FROM EWPCV_ADDITIONAL_INFO CASCADE;
	DELETE FROM EWPCV_DIPLOMA_SECTION CASCADE;
	DELETE FROM EWPCV_DIPLOMA CASCADE;
	DELETE FROM EWPCV_TOR_ATTACHMENT CASCADE;
	DELETE FROM EWPCV_LOI_GROUPING CASCADE;
	DELETE FROM EWPCV_GROUP CASCADE;
	DELETE FROM EWPCV_GROUP_TYPE CASCADE;
	DELETE FROM EWPCV_LOI_ATT CASCADE;
	DELETE FROM EWPCV_LOI CASCADE;
	DELETE FROM EWPCV_REPORT CASCADE;
	DELETE FROM EWPCV_GRADE_FREQUENCY CASCADE;
	DELETE FROM EWPCV_ISCED_TABLE CASCADE;
	DELETE FROM EWPCV_TOR CASCADE;
	DELETE FROM EWPCV_ATTACHMENT_CONTENT CASCADE;
	DELETE FROM EWPCV_ATTACHMENT_DESCRIPTION CASCADE;
	DELETE FROM EWPCV_ATTACHMENT_TITLE CASCADE;
	DELETE FROM EWPCV_ATTACHMENT CASCADE;
	DELETE FROM EWPCV_FACT_SHEET_URL CASCADE;
	DELETE FROM EWPCV_INSTITUTION_NAME CASCADE;
	DELETE FROM EWPCV_ORGANIZATION_UNIT_NAME CASCADE;
	DELETE FROM EWPCV_INST_ORG_UNIT CASCADE;
	DELETE FROM EWPCV_INST_INF_ITEM CASCADE;
	DELETE FROM EWPCV_OU_INF_ITEM CASCADE;
	DELETE FROM EWPCV_OU_REQUIREMENT_INFO CASCADE;
	DELETE FROM EWPCV_INS_REQUIREMENTS CASCADE;
	DELETE FROM EWPCV_INSTITUTION CASCADE;
	DELETE FROM EWPCV_ORGANIZATION_UNIT CASCADE;
	DELETE FROM EWPCV_FACT_SHEET CASCADE;
	DELETE FROM EWPCV_REQUIREMENTS_INFO CASCADE;
	DELETE FROM EWPCV_INFORMATION_ITEM CASCADE;
	DELETE FROM EWPCV_MOBILITY_LANG_SKILL CASCADE;
	DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL CASCADE;
	DELETE FROM EWPCV_LANGUAGE_SKILL CASCADE;
	DELETE FROM EWPCV_COOPCOND_EQFLVL CASCADE;
	DELETE FROM EWPCV_COOPERATION_CONDITION CASCADE;
	DELETE FROM EWPCV_MOBILITY_NUMBER CASCADE;
	DELETE FROM EWPCV_DURATION CASCADE;
	DELETE FROM EWPCV_IIA CASCADE;
	DELETE FROM EWPCV_IIA_PARTNER_CONTACTS CASCADE;
	DELETE FROM EWPCV_IIA_PARTNER CASCADE;
	DELETE FROM EWPCV_MOBILITY_LA CASCADE;
	DELETE FROM EWPCV_MOBILITY CASCADE;
	DELETE FROM EWPCV_MOBILITY_PARTICIPANT CASCADE;
	DELETE FROM EWPCV_MOBILITY_TYPE CASCADE;
	DELETE FROM EWPCV_SUBJECT_AREA CASCADE;
	DELETE FROM EWPCV_STUDIED_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_BLENDED_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_LEARNING_AGREEMENT CASCADE;
	DELETE FROM EWPCV_SIGNATURE CASCADE;
	DELETE FROM EWPCV_CONTACT_NAME CASCADE;
	DELETE FROM EWPCV_CONTACT_DESCRIPTION CASCADE;
	DELETE FROM EWPCV_CONTACT CASCADE;
	DELETE FROM EWPCV_PERSON CASCADE;
	DELETE FROM EWPCV_CONTACT_URL CASCADE;
	DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL CASCADE;
	DELETE FROM EWPCV_PHOTO_URL CASCADE;
	DELETE FROM EWPCV_CONTACT_DETAILS CASCADE;
	DELETE FROM EWPCV_PHONE_NUMBER CASCADE;
	DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE;
	DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD CASCADE;
	DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME CASCADE;
	DELETE FROM EWPCV_FLEXIBLE_ADDRESS;
	DELETE FROM EWPCV_LOI_CREDITS CASCADE;
	DELETE FROM EWPCV_CREDIT CASCADE;
	DELETE FROM EWPCV_RESULT_DIST_CATEGORY CASCADE;
	DELETE FROM EWPCV_DISTR_DESCRIPTION CASCADE;
	DELETE FROM EWPCV_LA_COMPONENT CASCADE;
	DELETE FROM EWPCV_LOS_LOI CASCADE;
	DELETE FROM EWPCV_LOI CASCADE;
	DELETE FROM EWPCV_RESULT_DISTRIBUTION CASCADE;
	DELETE FROM EWPCV_ACADEMIC_TERM_NAME CASCADE;
	DELETE FROM EWPCV_ACADEMIC_TERM CASCADE;
	DELETE FROM EWPCV_ACADEMIC_YEAR CASCADE;
	DELETE FROM EWPCV_GRADING_SCHEME_DESC CASCADE;
	DELETE FROM EWPCV_GRADING_SCHEME_LABEL CASCADE;
	DELETE FROM EWPCV_GRADING_SCHEME CASCADE;
	DELETE FROM EWPCV_LOS_DESCRIPTION CASCADE;
	DELETE FROM EWPCV_LOS_LOS CASCADE;
	DELETE FROM EWPCV_LOS_NAME CASCADE;
	DELETE FROM EWPCV_LOS_URLS CASCADE;
	DELETE FROM EWPCV_LOS CASCADE;
	DELETE FROM EWPCV_MOBILITY_UPDATE_REQUEST CASCADE;
	DELETE FROM EWPCV_NOTIFICATION CASCADE;
	DELETE FROM EWPCV_LANGUAGE_ITEM CASCADE;
	DELETE FROM EWPCV_EVENT CASCADE;
    
    v_id := EWP.GENERATE_UUID();
    INSERT INTO EWPCV_LOS (ID, VERSION, EQF_LEVEL, LOS_CODE, TOP_LEVEL_PARENT) VALUES (v_id, 0, 1, 'LOS Code', 1);
    
    -- Recogemos los valores de las variables introducidas por el usuario
 
    v_inst_schac:= :p_inst_schac;
    v_ounit_code := 'ounit.code'; 
    
    --------------------------------------------------
    --- Definimos los objetos que vamos a utilizar ---
    --------------------------------------------------
    
    --Primary contact
    v_contact_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_contact_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Jon Test'));
    v_contact_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_contact_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_contact_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_contact_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_contact := new EWP.CONTACT(v_contact_names,v_contact_descriptions,v_contact_urls,'coordinator',null,null, v_contact_emails,
        new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...
    
    -- Contact para Contact Detail List
    
    v_contact_list_emails := new EWP.EMAIL_LIST('contact@mail.com'); -- Ha de ser en formato xxx@xxx.xx
    v_contact_list_names := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','SJon Test'));
    v_contact_list_descriptions := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Contact Description'));
    v_contact_list_urls := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.contact.es/')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_contact_list_recipient_name := new EWP.RECIPIENT_NAME_LIST('Recipient Name');
    v_contact_list_address_lines := new EWP.ADDRESS_LINE_LIST('Address Lines');
    
    v_contact_list := new EWP.CONTACT(v_contact_list_names,v_contact_list_descriptions,v_contact_list_urls,'coordinator',null,null, v_contact_list_emails,
        new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.FLEXIBLE_ADDRESS (v_contact_list_recipient_name,v_contact_list_address_lines,'Building Number','Building Name','Street','Unit','Floor','Post Office',null,'Postal Code','Locality', 'Region', 'ES'),
        new EWP.PHONE_NUMBER ('+34111111111',null,null), new EWP.PHONE_NUMBER ('+34222222222',null,null)); -- Teléfonos han de ser en formato +x...
    
    --INSTITUTION--
    v_institution.INSTITUTION_ID := v_inst_schac;
    v_institution.INSTITUTION_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Institución Name'));
    v_institution.ABBREVIATION := 'Univ. Abbreviation';
    v_institution.UNIVERSITY_CONTACT_DETAILS := v_contact;
    v_institution.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Jon Test','Doe Test','01/01/2000','ES',0,v_contact_list));
    v_institution.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.institution-factsheet.es')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    
    --INSTITUTION PARTNER--
    v_institution_partner.INSTITUTION_ID := v_inst_schac_partner;
    v_institution_partner.INSTITUTION_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Institución Partner Name'));
    v_institution_partner.ABBREVIATION := 'Univ. Partner Abbreviation';
    v_institution_partner.UNIVERSITY_CONTACT_DETAILS := v_contact;
    v_institution_partner.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Jon Test','Doe Test','01/01/2000','ES',0,v_contact_list));
    v_institution_partner.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.institution-factsheet.es')); -- Urls han de ser en formato http?s://xxxx.xxx.xx
    
    --OUNIT
    v_ounit.HEI_ID := v_inst_schac;
    v_ounit.OUNIT_CODE := v_ounit_code;
    v_ounit.OUNIT_NAME_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Ounit name'));
    v_ounit.ABBREVIATION := 'Ounit Abbreviation';
    v_ounit.OUNIT_CONTACT_DETAILS := v_contact;
    v_ounit.CONTACT_DETAILS_LIST := new EWP.CONTACT_PERSON_LIST(new EWP.CONTACT_PERSON('Ounit Contact','','01/01/1960','ES',0,v_contact_list));
    v_ounit.FACTSHEET_URL_LIST := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.ounit-factsheet.es'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_ounit.LOGO_URL := 'https://www.ounit-logo.es';-- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_ounit.IS_TREE_STRUCTURE := 1;
    
    --FACTSHEET  
    v_factsheet.INSTITUTION_ID := v_inst_schac;
    v_factsheet.FACTSHEET.DECISION_WEEK_LIMIT := 3;
    v_factsheet.FACTSHEET.TOR_WEEK_LIMIT := 3;
    v_factsheet.FACTSHEET.NOMINATIONS_AUTUM_TERM := SYSDATE;
    v_factsheet.FACTSHEET.NOMINATIONS_SPRING_TERM := SYSDATE;
    v_factsheet.FACTSHEET.APPLICATION_AUTUM_TERM := SYSDATE;
    v_factsheet.FACTSHEET.APPLICATION_SPRING_TERM := SYSDATE;
     
    v_factsheet.APPLICATION_INFO.EMAIL := 'a@a.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.APPLICATION_INFO.PHONE := new EWP.PHONE_NUMBER('+34666666666',null,null);-- Teléfonos han de ser en formato +x...
    v_factsheet.APPLICATION_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.a.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
     
    v_factsheet.HOUSING_INFO.EMAIL := 'b@b.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.HOUSING_INFO.PHONE := new EWP.PHONE_NUMBER('+34777777777',null,null);
    v_factsheet.HOUSING_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.b.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
     
    v_factsheet.VISA_INFO.EMAIL := 'c@c.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.VISA_INFO.PHONE := new EWP.PHONE_NUMBER('+34888888888',null,null);
    v_factsheet.VISA_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.c.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
     
    v_factsheet.INSURANCE_INFO.EMAIL := 'd@d.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.INSURANCE_INFO.PHONE := new EWP.PHONE_NUMBER('+34999999999',null,null);
    v_factsheet.INSURANCE_INFO.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
     
    v_factsheet.ADDITIONAL_INFO(1).INFO_TYPE := 'Additional info';
    v_factsheet.ADDITIONAL_INFO(1).INFORMATION_ITEM.EMAIL := 'd@d.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.ADDITIONAL_INFO(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34999999999',null,null);
    v_factsheet.ADDITIONAL_INFO(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.d.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
     
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).REQ_TYPE := 'infrastructure';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).NAME := 'Infrastructure name';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).DESCRIPTION := 'Infrastructure description';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.EMAIL := 'e@e.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34000000000',null,null);
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.infrastucture.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
    
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).REQ_TYPE := 'service';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).NAME := 'Service name';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).DESCRIPTION := 'Service name';
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.EMAIL := 'E@E.com'; -- Ha de ser en formato xxx@xxx.xx
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.PHONE := new EWP.PHONE_NUMBER('+34000000000',null,null);
    v_factsheet.ACCESSIBILITY_REQUIREMENTS(1).INFORMATION_ITEM.URL := new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','https://www.service.com'));-- Urls han de ser en formato http?s://xxxx.xxx.xx
    
    --IIA  
    
    v_iia.IIA_CODE := 'IIA_TEST_CODE';
    v_iia.start_date := sysdate;
    v_iia.end_date := sysdate;
    -- IIA COOP COND
    v_coop_cond(1).START_DATE := sysdate;
    v_coop_cond(1).END_DATE := sysdate;
    v_coop_cond(1).EQF_LEVEL_LIST(1):= 1;
    v_coop_cond(1).mobility_number := 1;
    v_coop_cond(1).MOBILITY_TYPE.MOBILITY_CATEGORY := 'STUDIES';
    v_coop_cond(1).MOBILITY_TYPE.MOBILITY_GROUP := 'STUDENT';
    v_coop_cond(1).COP_COND_DURATION.NUMBER_DURATION := 2;
    v_coop_cond(1).COP_COND_DURATION.UNIT := 3;
    v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
    v_coop_cond(1).SUBJECT_AREA_LANGUAGE_LIST(1).LANGUAGE_SKILL := new EWP.LANGUAGE_SKILL('ES','B2');
    v_coop_cond(1).blended := 0;
    -- IIA Institutions
    v_inst_iia_send := new EWP.INSTITUTION(v_inst_schac, null, null, v_ounit_code);
    v_inst_iia_receiv := new EWP.INSTITUTION(v_inst_schac_partner, null, null, null);
    -- IIA Sending Partner
    v_sending_partner.institution := v_inst_iia_send;
    v_sending_partner.signing_date := sysdate;
    v_sending_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('Coordinador sender','Sender Institution','01/01/1960','ES',0, v_contact);
    -- IIA Receiving Partner
    v_receiving_partner.institution := v_inst_iia_receiv;
    v_receiving_partner.signing_date := sysdate;
    v_receiving_partner.SIGNER_PERSON := new EWP.CONTACT_PERSON('Coordinador receiver','Receiver Institution','01/01/1960','ES',0, v_contact);
    v_coop_cond(1).SENDING_PARTNER := v_sending_partner;
    v_coop_cond(1).RECEIVING_PARTNER := v_receiving_partner;
    v_iia.COOPERATION_CONDITION_LIST := v_coop_cond;
    
    -- MOBILITY
    v_mobility.PLANED_ARRIVAL_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
    v_mobility.PLANED_DEPATURE_DATE := TO_DATE('2021/07/09', 'yyyy/mm/dd');
    v_mobility.EQF_LEVEL_DEPARTURE:=6;
    v_mobility.EQF_LEVEL_NOMINATION:=7;
    v_mobility_academic_term := new EWP.ACADEMIC_TERM('2021/2021', v_inst_schac, null, sysdate, sysdate, 
                                new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Academic Term')),1,2);
    v_mobility.ACADEMIC_TERM := v_mobility_academic_term;
    v_mobility.sending_institution  := new EWP.INSTITUTION(v_inst_schac, null, null, v_ounit_code);
    v_mobility.receiving_institution  := new EWP.INSTITUTION(v_inst_schac_partner, null, null, null);
    v_mobility.student.global_id := 'Student_Test_Global_id';
    v_mobility.student.contact_person := new EWP.CONTACT_PERSON('Jon Test','Doe Test','21/02/1993','ES', 1, v_contact);
    v_st_photo_urls(1).URL := 'https://www.mobility_foto_urls.com/foto.jpg';-- Urls han de ser en formato http?s://xxxx.xxx.xx
    v_st_photo_urls(1).PHOTO_SIZE := '800x600';
    v_st_photo_urls(1).PHOTO_DATE := sysdate;
    v_st_photo_urls(1).PHOTO_PUBLIC := 0;
    v_mobility.student.photo_url_list := v_st_photo_urls;
    v_mobility.SENDER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Sender Institution', sysdate,'ES',0 ,v_contact);
    v_mobility.RECEIVER_CONTACT := new EWP.CONTACT_PERSON('Coordinador','Receiver Institution',sysdate,'ES', 0, v_contact);
    v_mobility.SUBJECT_AREA := new EWP.SUBJECT_AREA('06','Information and Communication Technologies (ICTs)');
    v_mobility.STUDENT_LANGUAGE_SKILLS := new EWP.LANGUAGE_SKILL_LIST (new EWP.LANGUAGE_SKILL('es','C2'), new EWP.LANGUAGE_SKILL('en','B2'));
    v_mobility.MOBILITY_TYPE := 'Semester';
    
    --LEARNING AGREEMENT
    v_components(0).la_component_type := 0;
    v_components(0).los_code := 'LOS Code';
    v_components(0).title := 'Component Title';
    v_components(0).academic_term := new EWP.ACADEMIC_TERM('2020/2021',v_inst_schac_partner, null,sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
    v_components(0).credit.scheme := 'ECTS';
    v_components(0).credit.CREDIT_VALUE := 6;
    
    v_components(1).la_component_type := 1;
    v_components(1).los_code := 'LOS Code';
    v_components(1).title := 'Component Title';
    v_components(1).academic_term := new EWP.ACADEMIC_TERM('2020/2021',v_inst_schac, null,sysdate,sysdate,new EWP.LANGUAGE_ITEM_LIST(new EWP.LANGUAGE_ITEM('ES','Primer semestre')),1,2);
    v_components(1).credit.scheme := 'ECTS';
    v_components(1).credit.CREDIT_VALUE := 6;
    
    v_la.components := v_components;
    
    v_la.STUDENT_SIGNATURE.SIGNER_NAME := 'Student Signer Name Test';
    v_la.STUDENT_SIGNATURE.SIGNER_POSITION := 'student';
    v_la.STUDENT_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
    v_la.STUDENT_SIGNATURE.SIGN_DATE := SYSDATE; 
    
    v_la.SENDING_HEI_SIGNATURE.SIGNER_NAME := 'Coordinator Signer Name Test';
    v_la.SENDING_HEI_SIGNATURE.SIGNER_POSITION := 'coordinator';
    v_la.SENDING_HEI_SIGNATURE.SIGNER_EMAIL := ' a@a.com';-- Ha de ser en formato xxx@xxx.xx
    v_la.SENDING_HEI_SIGNATURE.SIGN_DATE := SYSDATE;
    
    
    ----------------------------------------
    -- Comenzamos el proceso de inserción --
    ----------------------------------------
    
    -- Comprobamos si existe el identificador de la institución
    
    SELECT COUNT(1) INTO v_exist FROM EWPCV_INSTITUTION_IDENTIFIERS WHERE SCHAC = v_inst_schac;
    IF v_exist = 0 THEN 
        DBMS_OUTPUT.PUT_LINE('Institution: Como no existe insertamos el identificador ' || v_inst_schac || ' en la tabla EWPCV_INSTITUTION_IDENTIFIERS');
        INSERT INTO EWPCV_INSTITUTION_IDENTIFIERS (SCHAC) VALUES (v_inst_schac);
    END IF;
    -- Comprobamos si existe el identificador de la institución partner
    SELECT COUNT(1) INTO v_exist FROM EWPCV_INSTITUTION_IDENTIFIERS WHERE SCHAC = v_inst_schac_partner;
    IF v_exist = 0 THEN 
        DBMS_OUTPUT.PUT_LINE('Institution partner: Como no existe insertamos el identificador ' || v_inst_schac_partner || ' en la tabla EWPCV_INSTITUTION_IDENTIFIERS');
        INSERT INTO EWPCV_INSTITUTION_IDENTIFIERS (SCHAC) VALUES (v_inst_schac_partner);
    END IF;
   
    -- Comenzamos las inserciones
    DBMS_OUTPUT.PUT_LINE('Insertamos la institución con el identificador: ' || v_inst_schac || '.');
    v_return_code := PKG_INSTITUTION.INSERT_INSTITUTION(v_institution, v_institution_id, v_error_message);
    DBMS_OUTPUT.PUT_LINE(v_error_message);
    IF v_return_code = 0 THEN
        DBMS_OUTPUT.PUT_LINE('Insertamos la ounit con el código: ' || v_ounit_code || '.');
        v_return_code := PKG_OUNITS.INSERT_OUNIT(v_ounit, v_ounit_id, v_error_message);
        DBMS_OUTPUT.PUT_LINE(v_error_message);
        IF v_return_code = 0 THEN
            DBMS_OUTPUT.PUT_LINE('Insertamos factsheet.');
            v_return_code := PKG_FACTSHEET.INSERT_FACTSHEET(v_factsheet,v_error_message);   
            DBMS_OUTPUT.PUT_LINE(v_error_message);
            IF v_return_code = 0 THEN
                DBMS_OUTPUT.PUT_LINE('Insertamos IIA de acuerdo entre ' || v_inst_schac || ' y ' || v_inst_schac_partner);     
                v_return_code := PKG_IIAS.INSERT_IIA(v_iia, v_iia_id, v_error_message, v_inst_schac_partner);
                DBMS_OUTPUT.PUT_LINE(v_error_message);
                IF v_return_code = 0 THEN
                    DBMS_OUTPUT.PUT_LINE('Insertamos Mobility');   
                    v_mobility.IIA_ID := v_iia_id;
                    OPEN c_coop_cond(v_iia_id);
                    FETCH c_coop_cond INTO v_coop_cond_id;
                    CLOSE c_coop_cond;
                    v_mobility.COOPERATION_CONDITION_ID:=v_coop_cond_id;
                    v_return_code := PKG_MOBILITY_LA.INSERT_mobility(v_mobility, v_mobility_id, v_error_message, v_inst_schac_partner);
                    DBMS_OUTPUT.PUT_LINE(v_error_message);
                    IF v_return_code = 0 THEN
                        DBMS_OUTPUT.PUT_LINE('Insertamos Learning Agreement.');   
                        v_la.mobility_id := v_mobility_id;
                        v_return_code := PKG_MOBILITY_LA.INSERT_LEARNING_AGREEMENT(v_la, v_la_id, v_error_message, v_inst_schac_partner);
                        DBMS_OUTPUT.PUT_LINE(v_error_message);
                        IF v_return_code = 0 THEN
                            DBMS_OUTPUT.PUT_LINE('Todos los datos se han insertado correctamente.'); 
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;
END;
