
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
	
---------------	
-- FUNCIONES --
---------------
/	
create or replace FUNCTION EWP.EXTRAE_NOMBRES_OUNIT(P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS 
	v_lista VARCHAR2(6000 CHAR);
	CURSOR C(p_id IN VARCHAR2) IS  SELECT LISTAGG(LAIT.TEXT || '|:|' || LAIT."LANG", '|;|')
		WITHIN GROUP (ORDER BY LAIT."LANG")
    FROM EWP.EWPCV_ORGANIZATION_UNIT O,
		EWP.EWPCV_ORGANIZATION_UNIT_NAME N,
		EWP.EWPCV_LANGUAGE_ITEM LAIT
    WHERE O.ID = N.ORGANIZATION_UNIT_ID
		AND N.NAME_ID = LAIT.ID
		AND O.ID = p_id;
BEGIN
	OPEN C(P_OUNIT_ID);
	FETCH C INTO v_lista;
	CLOSE c;
    RETURN v_lista;
END;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/


/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
	Obtiene la informacion de las condiciones de cooperacion de los IIAS 
	Se empleará para recuperar el detalle de un IIA
*/
CREATE OR REPLACE view EWP.IIA_COOP_CONDITIONS_VIEW AS SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
    CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	EXTRAE_EQF_LEVEL(CC.ID)                                                                                AS COOP_COND_EQF_LEVEL,
	D.NUMBERDURATION || '|:|' ||D.UNIT                                                                     AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY ||'|:|'|| MT.MOBILITY_GROUP                                                       AS MOBILITY_TYPE,
	EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                           AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                          AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)        AS RECEIVING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                          AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	RPSC.CONTACT_ROLE                                                                       			   AS RECEIVER_SIGNER_ROLE,
	EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                       AS RECEIVER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                               AS RECEIVER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                         AS RECEIVER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(RPSCD.ID)                                                                                AS RECEIVER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                               AS RECEIVER_SIGNER_PHONES,
	EXTRAE_FAX(RPSC.CONTACT_DETAILS_ID)                                                               	   AS RECEIVER_SIGNER_FAXES,
	EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                             AS RECEIVER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                                   AS RECEIVER_SIGNER_ADDRESS,
	EXTRAE_ADDRESS_LINES(RPSCD.MAILING_ADDRESS)                                                            AS RECEIVER_SIGNER_M_ADD_LINES,
	EXTRAE_ADDRESS(RPSCD.MAILING_ADDRESS)                                                                  AS RECEIVER_SIGNER_MAILING_ADDR,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                          AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)        AS SENDING_OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                          AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	SPSC.CONTACT_ROLE                                                                       			   AS SENDER_SIGNER_ROLE,
	EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                       AS SENDER_SIGNER_CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                               AS SENDER_SIGNER_CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                         AS SENDER_SIGNER_CONTACT_URLS,
	EXTRAE_EMAILS(SPSCD.ID)                                                                                AS SENDER_SIGNER_EMAILS,
	EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                               AS SENDER_SIGNER_PHONES,
	EXTRAE_FAX(SPSC.CONTACT_DETAILS_ID)                                                               	   AS SENDER_SIGNER_FAXES,
	EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                             AS SENDER_SIGNER_ADDRESS_LINES,
	EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                                   AS SENDER_SIGNER_ADDRESS,
	EXTRAE_ADDRESS_LINES(SPSCD.MAILING_ADDRESS)                                                            AS SENDER_SIGNER_M_ADDRESS_LINES,
	EXTRAE_ADDRESS(SPSCD.MAILING_ADDRESS)                                                                  AS SENDER_SIGNER_MAILING_ADDR,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
	
	
	/*
	Obtiene la informacion de los contactos asociados a cada condicion de cooperacion. 
*/
CREATE OR REPLACE VIEW EWP.IIA_COOP_COND_CONTACTS_VIEW AS SELECT
	C.ID                                                                                                   AS CONTACT_ID,
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
    P.INSTITUTION_ID                                                                                       AS INSTITUTION,
	EXTRAE_NOMBRES_INSTITUCION(P.INSTITUTION_ID)                                                           AS INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P.ORGANIZATION_UNIT_ID)         AS OUNIT_CODE,
	EXTRAE_NOMBRES_OUNIT(P.ORGANIZATION_UNIT_ID)                                                           AS OUNIT_NAMES,
	PE.FIRST_NAMES                                                                                      AS CONTACT_NAME,
	PE.LAST_NAME                                                                                        AS LAST_NAME,
	PE.BIRTH_DATE                                                                                       AS BIRTH_DATE,
	PE.GENDER                                                                                           AS GENDER,
	PE.COUNTRY_CODE                                                                                     AS CITIZENSHIP,
	C.CONTACT_ROLE                                                                       			   AS CONTACT_ROLE,
	EXTRAE_NOMBRES_CONTACTO(C.ID)                                                                       AS CONTACT_NAMES,
	EXTRAE_DESCRIPCIONES_CONTACTO(C.ID)	                                                               AS CONTACT_DESCS,
	EXTRAE_URLS_CONTACTO(CD.ID)                                                                         AS CONTACT_URLS,
	EXTRAE_EMAILS(CD.ID)                                                                                AS EMAILS,
	EXTRAE_TELEFONO(C.CONTACT_DETAILS_ID)                                                               AS PHONES,
	EXTRAE_FAX(C.CONTACT_DETAILS_ID)                                                               	   AS FAXES,
	EXTRAE_ADDRESS_LINES(CD.STREET_ADDRESS)                                                             AS ADDRESS_LINES,
	EXTRAE_ADDRESS(CD.STREET_ADDRESS)                                                                   AS ADDRESS,
	EXTRAE_ADDRESS_LINES(CD.MAILING_ADDRESS)                                                            AS M_ADD_LINES,
	EXTRAE_ADDRESS(CD.MAILING_ADDRESS)                                                                  AS MAILING_ADDR,
	FA.POSTAL_CODE                                                                                     AS POSTAL_CODE,
	FA.LOCALITY                                                                                        AS LOCALITY,
	FA.REGION                                                                                          AS REGION,	
	FA.COUNTRY                                                                                         AS COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
INNER JOIN EWPCV_IIA_PARTNER P
    ON (CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID)
INNER JOIN EWPCV_IIA_PARTNER_CONTACTS PC
    ON P.ID = PC.IIA_PARTNER_ID
INNER JOIN EWPCV_CONTACT C
    ON PC.CONTACTS_ID = C.ID
LEFT JOIN EWPCV_PERSON PE 
    ON C.PERSON_ID = PE.ID
LEFT JOIN EWPCV_CONTACT_DETAILS CD 
    ON C.CONTACT_DETAILS_ID = CD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS FA 
    ON CD.STREET_ADDRESS = FA.ID ;
/*
    ***********************************
     FIN MODIFICACIONES SOBRE VISTAS
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/
create or replace PACKAGE EWP.PKG_FACTSHEET AS 

	-- TIPOS DE DATOS
	
	/*Informacion sobre fechas importantes del proceso de movilidades asociado a la institucion
		Campos obligatorios marcados con *.
			DECISION_WEEK_LIMIT*	 		Numero de semanas limite para la resolucion de solicitudes (No deberia ser mas de 5)
			TOR_WEEK_LIMIT*	 				Numero de semanas limite para expedir el expediente academico (No deberia ser mas de 5)
			NOMINATIONS_AUTUM_TERM*			Fecha en el que las nominaciones deben llegar en otoÃ±o
			NOMINATIONS_SPRING_TERM* 		Fecha en el que las nominaciones deben llegar en primavera
			APPLICATION_AUTUM_TERM*			Fecha en el que las solicitudes deben llegar en otoÃ±o
			APPLICATION_SPRING_TERM* 		Fecha en el que las solicitudes deben llegar en primavera
	*/  
	TYPE FACTSHEET IS RECORD
	  (
		DECISION_WEEK_LIMIT	 		NUMBER(1,0),
		TOR_WEEK_LIMIT	 			NUMBER(1,0),
		NOMINATIONS_AUTUM_TERM		DATE,
		NOMINATIONS_SPRING_TERM 	DATE,
		APPLICATION_AUTUM_TERM		DATE,
		APPLICATION_SPRING_TERM 	DATE
	  );	  

	/*Informacion de contacto proporcionada al usuario. 
		Campos obligatorios marcados con *.
			EMAIL*					email
			PHONE*					telefono 
			URL* 					listado de urls con informacion
	*/	
	TYPE INFORMATION_ITEM IS RECORD
	  (
		EMAIL					VARCHAR2(255 CHAR),
		PHONE					EWP.PHONE_NUMBER,
		URL 					EWP.LANGUAGE_ITEM_LIST
	  );

	/*Informacion de contacto proporcionada al usuario asociada a un ambito determinado.
		Campos obligatorios marcados con *.
			INFO_TYPE*					Ambito al que esta dedicado este elemento de informacion
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE TYPED_INFORMATION_ITEM IS RECORD
	  (
		INFO_TYPE				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE INFORMATION_ITEM_LIST IS TABLE OF TYPED_INFORMATION_ITEM INDEX BY BINARY_INTEGER ;



	/*Informacion de contacto para requerimientos adicionales.
		Campos obligatorios marcados con *.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			URLS					Urls con informacion
	*/	
	TYPE REQUIREMENTS_INFO IS RECORD
	  (
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		URLS					EWP.LANGUAGE_ITEM_LIST
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE REQUIREMENTS_INFO_LIST IS TABLE OF REQUIREMENTS_INFO INDEX BY BINARY_INTEGER ;

	/*Informacion de contacto para requerimientos de accesibilidad .
		Campos obligatorios marcados con *.
			REQ_TYPE*					Ambito al que esta dedicado este elemento de informacion.
									Valores admitidos infrastructure, service.
			NAME*					Nombre del requerimiento,
			DESCRIPTION*			Descripcion del requerimiento,
			INFORMATION_ITEM*		Informacion de contacto
	*/	
	TYPE ACCESSIBILITY_REQ_INFO IS RECORD
	  (
		REQ_TYPE				VARCHAR2(255 CHAR),
		NAME					VARCHAR2(255 CHAR),
		DESCRIPTION				VARCHAR2(255 CHAR),
		INFORMATION_ITEM		PKG_FACTSHEET.INFORMATION_ITEM
	  );

	/* Lista con informaciones de contacto tipadas. */  
	TYPE ACCESSIBILITY_REQ_INFO_LIST IS TABLE OF ACCESSIBILITY_REQ_INFO INDEX BY BINARY_INTEGER ;


	/* Objeto que modela una institucion. Campos obligatorios marcados con *.
			INSTITUTION_ID*					Identificador de la institucion en el entorno ewp (HEI_ID)
			LOGO_URL						Url al logo de la universidad
			ABREVIATION						Nombre abreviado de la universidad
			INSTITUTION_NAME	    		Nombre de la institucion, admite una lista de nombres en varios idiomas
			FACTSHEET* 						Hoja de datos de la intitucion
			APPLICATION_INFO*				Detalles de contacto para consultas sobre solicitudes
			HOUSING_INFO*					Detalles de contacto para consultas sobre hospedaje
			VISA_INFO*						Detalles de contacto para consultas sobre visados
			INSURANCE_INFO*					Detalles de contacto para consultas sobre seguros
			ADDITIONAL_INFO					Detalles de contacto para consultas sobre diferentes ambitos especificados
			ACCESSIBILITY_REQUIREMENTS		Informacion sobre accesibilidad para participantes con necesidades especiales
			ADITIONAL_REQUIREMENTS			Requerimientos adicionales
	*/
	TYPE FACTSHEET_INSTITUTION IS RECORD
	  (
		INSTITUTION_ID					VARCHAR2(255 CHAR),
		ABREVIATION						VARCHAR2(255 CHAR),
		LOGO_URL						VARCHAR2(255 CHAR),
		INSTITUTION_NAME				EWP.LANGUAGE_ITEM_LIST,
		FACTSHEET 						PKG_FACTSHEET.FACTSHEET,
		APPLICATION_INFO				PKG_FACTSHEET.INFORMATION_ITEM,
		HOUSING_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		VISA_INFO						PKG_FACTSHEET.INFORMATION_ITEM,
		INSURANCE_INFO					PKG_FACTSHEET.INFORMATION_ITEM,
		ADDITIONAL_INFO					PKG_FACTSHEET.INFORMATION_ITEM_LIST,
		ACCESSIBILITY_REQUIREMENTS		PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,
		ADITIONAL_REQUIREMENTS			PKG_FACTSHEET.REQUIREMENTS_INFO_LIST
	  );

	-- FUNCIONES 

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 


END PKG_FACTSHEET;
/
create or replace PACKAGE BODY EWP.PKG_FACTSHEET AS 

	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	
	/*
		Valida los campos obligatorios de un factsheet 
	*/
	FUNCTION VALIDA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_FACTSHEET.DECISION_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-DECISION_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.TOR_WEEK_LIMIT IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-TOR_WEEK_LIMIT';
		END IF;

		IF P_FACTSHEET.NOMINATIONS_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_AUTUM_TERM';
		END IF;

		IF P_FACTSHEET.NOMINATIONS_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-NOMINATIONS_SPRING_TERM';
		END IF;

		IF P_FACTSHEET.APPLICATION_AUTUM_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_AUTUM_TERM';
		END IF;

		IF P_FACTSHEET.APPLICATION_SPRING_TERM IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-APPLICATION_SPRING_TERM';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un infoitem
	*/
	FUNCTION VALIDA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_INFORMATION_ITEM.EMAIL IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-EMAIL';
		END IF;

		IF P_INFORMATION_ITEM.PHONE IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-PHONE';
		END IF;

		IF P_INFORMATION_ITEM.URL IS NULL OR P_INFORMATION_ITEM.URL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-URL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de infoitem adicionales
	*/
	FUNCTION VALIDA_ADITIONAL_INFO(P_ADDITIONAL_INFO IN PKG_FACTSHEET.INFORMATION_ITEM_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ADDITIONAL_INFO.FIRST .. P_ADDITIONAL_INFO.LAST 
		LOOP

			IF P_ADDITIONAL_INFO(i).INFO_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
					INFO_TYPE:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ADDITIONAL_INFO(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
					INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ADITIONAL_INFO'|| i || ':' ||v_mensaje_it;
				v_mensaje_it := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de requerimientos de accesibilidad
	*/
	FUNCTION VALIDA_ACCESSIBILITY_REQS(P_ACCESSIBILITY_REQUIREMENTS IN PKG_FACTSHEET.ACCESSIBILITY_REQ_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_ACCESSIBILITY_REQUIREMENTS.FIRST .. P_ACCESSIBILITY_REQUIREMENTS.LAST 
		LOOP
			IF P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE:' || v_mensaje_it;
			ELSIF UPPER(P_ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE) NOT IN ('INFRASTRUCTURE','SERVICE') THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				REQ_TYPE: VALOR NO ADMITIDO' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;
			IF VALIDA_INFORMATION_ITEM(P_ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM, v_mensaje_it) <> 0 THEN
				v_cod_retorno_it := -1;
				v_mensaje_it := '
				INFORMATION_ITEM: '|| v_mensaje_it;
			END IF;
			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				ACCESSIBILITY_REQUIREMENTS'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una coleccion de informacion sobre requerimientos
	*/
	FUNCTION VALIDA_REQUIREMENTS_INFO(P_REQUIREMENTS_INFO IN PKG_FACTSHEET.REQUIREMENTS_INFO_LIST,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno_it NUMBER := 0;
		v_cod_retorno NUMBER := 0;
		v_mensaje_it VARCHAR2(2000);
		v_mensaje_info VARCHAR2(2000);
	BEGIN
		FOR i IN P_REQUIREMENTS_INFO.FIRST .. P_REQUIREMENTS_INFO.LAST 
		LOOP
			IF P_REQUIREMENTS_INFO(i).NAME IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				NAME:' || v_mensaje_it;
			END IF;
			IF P_REQUIREMENTS_INFO(i).DESCRIPTION IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_it := v_mensaje_it || '
				DESCRIPTION:' || v_mensaje_it;
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
			v_cod_retorno := v_cod_retorno_it;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				REQUIREMENTS_INFO'|| i || ':' ||v_mensaje_it;
			v_mensaje_it := '';
			v_cod_retorno_it := 0;
			END IF;
		END LOOP;
		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de un factsheet institution
	*/
	FUNCTION VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET_INSTITUTION IN PKG_FACTSHEET.FACTSHEET_INSTITUTION,P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
		v_mensaje_factsheet VARCHAR2(2000);
		v_mensaje_app_info VARCHAR2(2000);
		v_mensaje_hou_info VARCHAR2(2000);
		v_mensaje_vis_info VARCHAR2(2000);
		v_mensaje_ins_info VARCHAR2(2000);
		v_mensaje_add_info VARCHAR2(2000);
		v_mensaje_acc_req VARCHAR2(2000);
		v_mensaje_add_req VARCHAR2(2000);
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT SCHAC
            FROM EWPCV_INSTITUTION_IDENTIFIERS 
            WHERE UPPER(SCHAC) = UPPER(p_ins_id);       
	BEGIN
        OPEN exist_cursor_inst(P_FACTSHEET_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_FACTSHEET_INSTITUTION.INSTITUTION_ID IS NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID';
        ELSIF v_ins_id IS NULL THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				-INSTITUTION_ID: La institución no existe en el sistema.';
		END IF;

		IF VALIDA_FACTSHEET(P_FACTSHEET_INSTITUTION.FACTSHEET, v_mensaje_factsheet) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			FACTSHEET:' || v_mensaje_factsheet;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.APPLICATION_INFO, v_mensaje_app_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			APPLICATION_INFO:' || v_mensaje_app_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.HOUSING_INFO, v_mensaje_hou_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			HOUSING_INFO:' || v_mensaje_hou_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.VISA_INFO, v_mensaje_vis_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			VISA_INFO:' || v_mensaje_vis_info;
		END IF;

		IF VALIDA_INFORMATION_ITEM(P_FACTSHEET_INSTITUTION.INSURANCE_INFO, v_mensaje_ins_info) <> 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			INSURANCE_INFO:' || v_mensaje_ins_info;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO.COUNT > 0 THEN 
			IF VALIDA_ADITIONAL_INFO(P_FACTSHEET_INSTITUTION.ADDITIONAL_INFO, v_mensaje_add_info) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ADDITIONAL_INFO:' || v_mensaje_add_info;
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_ACCESSIBILITY_REQS(P_FACTSHEET_INSTITUTION.ACCESSIBILITY_REQUIREMENTS, v_mensaje_acc_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			ACCESSIBILITY_REQUIREMENTS:' || v_mensaje_acc_req;
			END IF;
		END IF;

		IF P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			IF VALIDA_REQUIREMENTS_INFO(P_FACTSHEET_INSTITUTION.ADITIONAL_REQUIREMENTS, v_mensaje_add_req) <> 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE :=P_ERROR_MESSAGE || '
			REQUIREMENTS_INFO:' || v_mensaje_add_req;
			END IF;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;


	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************
	/*
		Borra los nombres de una institucion
	*/
	PROCEDURE BORRA_NOMBRES(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT LI.ID AS ID
		FROM EWPCV_LANGUAGE_ITEM LI 
			INNER JOIN EWPCV_INSTITUTION_NAME INA 
				ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = P_INSTITUTION_ID AND NAME_ID = rec.ID;
			DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = rec.ID;
		END LOOP;
	END;

	/*
		Borra los information item asociados a una institucion
	*/
	PROCEDURE BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ii(p_id IN VARCHAR2) IS 
		SELECT II.ID AS ID , II.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INST_INF_ITEM III
			INNER JOIN EWPCV_INFORMATION_ITEM II 
				ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ii(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = P_INSTITUTION_ID AND INFORMATION_ID = rec.ID;
			DELETE FROM EWPCV_INFORMATION_ITEM WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra los requirements info asociados a una institucion
	*/
	PROCEDURE BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID IN VARCHAR2) AS
		CURSOR c_ri(p_id IN VARCHAR2) IS 
		SELECT RI.ID AS ID , RI.CONTACT_DETAIL_ID AS CONTACT_DETAIL_ID
		FROM EWPCV_INS_REQUIREMENTS IR
			INNER JOIN EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = p_id;
	BEGIN

		FOR rec IN c_ri(P_INSTITUTION_ID)
		LOOP
			DELETE FROM EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = P_INSTITUTION_ID AND REQUIREMENT_ID = rec.ID;
			DELETE FROM EWPCV_REQUIREMENTS_INFO WHERE ID = rec.ID;
			PKG_COMMON.BORRA_CONTACT_DETAILS(rec.CONTACT_DETAIL_ID);
		END LOOP;
	END;

	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
	PROCEDURE BORRA_FACTSHEET_INSTITUTION(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2) AS
		v_cd_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN
		OPEN c(P_FACTSHEET_ID);
		FETCH c into v_cd_id;
		CLOSE c;

        UPDATE EWPCV_FACT_SHEET
        SET DECISION_WEEKS_LIMIT = null,
                TOR_WEEKS_LIMIT = null,
                NOMINATIONS_AUTUM_TERM = null,
                NOMINATIONS_SPRING_TERM = null,
                APPLICATION_AUTUM_TERM = null,
                APPLICATION_SPRING_TERM = null,
                CONTACT_DETAILS_ID = null
        WHERE ID = P_FACTSHEET_ID;

		PKG_COMMON.BORRA_CONTACT_DETAILS(v_cd_id);
		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Persiste informacion de contacto 
	*/
	FUNCTION INSERTA_CONTACT_DETAIL(P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS 
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);	
		v_litem_id VARCHAR2(255);
	BEGIN
		IF P_PHONE IS NOT NULL THEN 
			v_tlf_id := PKG_COMMON.INSERTA_TELEFONO(P_PHONE);
		END IF;

		v_contact_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (v_contact_id, v_tlf_id);

		IF P_EMAIL IS NOT NULL THEN 
			INSERT INTO EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (v_contact_id, P_EMAIL);
		END IF;

		IF P_URL IS NOT NULL AND P_URL.COUNT > 0 THEN 
			FOR i IN P_URL.FIRST .. P_URL.LAST
			LOOP 
				v_litem_id := PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_URL(i));
				INSERT INTO EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(v_litem_id,v_contact_id);
			END LOOP;
		END IF;
		RETURN v_contact_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_INFORMATION_ITEM(P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_INF_TYPE IN VARCHAR2, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_infoitem_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

		v_infoitem_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (v_infoitem_id,UPPER(P_INF_TYPE),v_contact_id);
		INSERT INTO EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (P_INSTITUTION_ID,v_infoitem_id);
	END;

	/*
		Persiste un information item
	*/
	FUNCTION INSERTA_FACTSHEET(P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFORMATION_ITEM IN PKG_FACTSHEET.INFORMATION_ITEM, P_FS_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_INFORMATION_ITEM.URL, P_INFORMATION_ITEM.EMAIL, P_INFORMATION_ITEM.PHONE);

        IF P_FS_ID IS NULL THEN 
            v_factsheet_id := EWP.GENERATE_UUID();
            INSERT INTO EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
                VALUES(v_factsheet_id, P_FACTSHEET.DECISION_WEEK_LIMIT, P_FACTSHEET.TOR_WEEK_LIMIT, P_FACTSHEET.NOMINATIONS_AUTUM_TERM, 
                    P_FACTSHEET.NOMINATIONS_SPRING_TERM, P_FACTSHEET.APPLICATION_AUTUM_TERM, P_FACTSHEET.APPLICATION_SPRING_TERM, v_contact_id);
        ELSE
            v_factsheet_id := P_FS_ID;
            UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT, 
                TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT,
                NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
                NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
                APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
                APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
                CONTACT_DETAILS_ID = v_contact_id
                WHERE ID = v_factsheet_id;
        END IF;
		
		RETURN v_factsheet_id;
	END;

	/*
		Persiste un information item
	*/
	PROCEDURE INSERTA_REQUIREMENT_INFO(P_TYPE IN VARCHAR2, P_NAME IN VARCHAR2,  P_DESCRIPTION IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST,
		P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER, P_INSTITUTION_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255);
		v_tlf_id VARCHAR2(255);
		v_litem_id VARCHAR2(255);
		v_requinf_id VARCHAR2(255);
	BEGIN

		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);

		v_requinf_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) VALUES (v_requinf_id, UPPER(P_TYPE), P_NAME, P_DESCRIPTION, v_contact_id);
		INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) VALUES (P_INSTITUTION_ID,v_requinf_id);
	END;

	/*
		Persiste una hoja de contactos asociada a una institucion en el sistema.
	*/
	PROCEDURE INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_FS_ID IN VARCHAR2) AS
		v_id VARCHAR2(255);
		v_inst_id VARCHAR2(255);
		v_factsheet_id VARCHAR2(255);
		v_add_info_id VARCHAR2(255);
	BEGIN
		v_factsheet_id := INSERTA_FACTSHEET(P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO, P_FS_ID);
		v_inst_id := PKG_COMMON.INSERTA_INSTITUTION(P_FACTSHEET.INSTITUTION_ID, P_FACTSHEET.ABREVIATION, P_FACTSHEET.LOGO_URL,v_factsheet_id, P_FACTSHEET.INSTITUTION_NAME, null);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', v_inst_id);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', v_inst_id);

		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					v_inst_id);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,v_inst_id);
			END LOOP;
		END IF;

	END;


	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************
	/*
		Actualiza la informacion de contacto
	*/
	FUNCTION ACTUALIZA_CONTACT_DETAILS(P_CONTACT_ID IN VARCHAR2, P_URL IN EWP.LANGUAGE_ITEM_LIST, P_EMAIL IN VARCHAR2, P_PHONE IN EWP.PHONE_NUMBER) RETURN VARCHAR2 AS
		v_contact_id VARCHAR2(255);
	BEGIN
		PKG_COMMON.BORRA_CONTACT_DETAILS(P_CONTACT_ID);
		v_contact_id := INSERTA_CONTACT_DETAIL(P_URL, P_EMAIL, P_PHONE);
		RETURN v_contact_id;
	END;

	/*
		Actualiza el factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET(P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN PKG_FACTSHEET.FACTSHEET, P_INFO IN PKG_FACTSHEET.INFORMATION_ITEM) AS
		v_c_id VARCHAR2(255);
		CURSOR c_fcd(p_id IN VARCHAR2) IS 
		SELECT CONTACT_DETAILS_ID
		FROM EWPCV_FACT_SHEET 
		WHERE ID = p_id;
	BEGIN

		UPDATE EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = P_FACTSHEET_ID;
		v_c_id := ACTUALIZA_CONTACT_DETAILS(v_c_id, P_INFO.URL, P_INFO.EMAIL, P_INFO.PHONE);

		UPDATE EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = P_FACTSHEET.DECISION_WEEK_LIMIT,
			TOR_WEEKS_LIMIT = P_FACTSHEET.TOR_WEEK_LIMIT, 
			NOMINATIONS_AUTUM_TERM = P_FACTSHEET.NOMINATIONS_AUTUM_TERM,
			NOMINATIONS_SPRING_TERM = P_FACTSHEET.NOMINATIONS_SPRING_TERM,
			APPLICATION_AUTUM_TERM = P_FACTSHEET.APPLICATION_AUTUM_TERM,
			APPLICATION_SPRING_TERM = P_FACTSHEET.APPLICATION_SPRING_TERM,
			CONTACT_DETAILS_ID = v_c_id
			WHERE ID = P_FACTSHEET_ID;
	END;


	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	PROCEDURE ACTUALIZA_FACTSHEET_INST(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION) AS
		v_c_id VARCHAR2(255);

	BEGIN

		UPDATE EWPCV_INSTITUTION SET ABBREVIATION = P_FACTSHEET.ABREVIATION, LOGO_URL = P_FACTSHEET.LOGO_URL WHERE ID = P_INSTITUTION_ID;

		IF P_FACTSHEET.INSTITUTION_NAME IS NOT NULL AND P_FACTSHEET.INSTITUTION_NAME.COUNT > 0 THEN
			BORRA_NOMBRES(P_INSTITUTION_ID);
			PKG_COMMON.INSERTA_INSTITUTION_NAMES(P_INSTITUTION_ID, P_FACTSHEET.INSTITUTION_NAME);
		END IF;

		ACTUALIZA_FACTSHEET(P_FACTSHEET_ID,  P_FACTSHEET.FACTSHEET, P_FACTSHEET.APPLICATION_INFO);

		BORRA_INFORMATION_ITEMS(P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.HOUSING_INFO, 'HOUSING', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.VISA_INFO, 'VISA', P_INSTITUTION_ID);
		INSERTA_INFORMATION_ITEM(P_FACTSHEET.INSURANCE_INFO, 'INSURANCE', P_INSTITUTION_ID);

		BORRA_REQUIREMENTS_INFO(P_INSTITUTION_ID);
		IF P_FACTSHEET.ADDITIONAL_INFO IS NOT NULL AND P_FACTSHEET.ADDITIONAL_INFO.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADDITIONAL_INFO.FIRST .. P_FACTSHEET.ADDITIONAL_INFO.LAST 
			LOOP
				INSERTA_INFORMATION_ITEM(P_FACTSHEET.ADDITIONAL_INFO(i).INFORMATION_ITEM, P_FACTSHEET.ADDITIONAL_INFO(i).INFO_TYPE, P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.FIRST .. P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO(P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).REQ_TYPE, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).NAME,
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.URL, 
					P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.EMAIL, P_FACTSHEET.ACCESSIBILITY_REQUIREMENTS(i).INFORMATION_ITEM.PHONE,
					P_INSTITUTION_ID);
			END LOOP;
		END IF;

		IF P_FACTSHEET.ADITIONAL_REQUIREMENTS IS NOT NULL AND P_FACTSHEET.ADITIONAL_REQUIREMENTS.COUNT > 0 THEN 
			FOR i IN P_FACTSHEET.ADITIONAL_REQUIREMENTS.FIRST .. P_FACTSHEET.ADITIONAL_REQUIREMENTS.LAST 
			LOOP
				INSERTA_REQUIREMENT_INFO('REQUIREMENT', P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).NAME, 
				P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).DESCRIPTION, P_FACTSHEET.ADITIONAL_REQUIREMENTS(i).URLS,
				null, null,P_INSTITUTION_ID);
			END LOOP;
		END IF;

	END;

	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
        v_fs_id VARCHAR2(255);
        v_fs_cd_id VARCHAR2(255);
        CURSOR c_fid(p_id IN VARCHAR2) IS 
		SELECT fs.id, fs.CONTACT_DETAILS_ID
		FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(p_id);
	BEGIN
        OPEN c_fid(P_FACTSHEET.INSTITUTION_ID);
        FETCH c_fid INTO v_fs_id, v_fs_cd_id;
        CLOSE c_fid;
        
		IF v_fs_cd_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			INSERTA_FACTSHEET_INSTITUTION(P_FACTSHEET, v_fs_id);
		END IF;
		COMMIT;

		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_FACTSHEET_INSTITUTION(P_FACTSHEET, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			ACTUALIZA_FACTSHEET_INST(v_ins_id, v_f_id, P_FACTSHEET);
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END; 

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_f_id VARCHAR2(255);
		v_ins_id VARCHAR2(255);
		v_fs_cd_id VARCHAR2(255);
		CURSOR c_inst(p_ins_id IN VARCHAR2) IS 
			SELECT ins.ID, ins.FACT_SHEET, fs.CONTACT_DETAILS_ID 
			FROM EWPCV_INSTITUTION ins LEFT JOIN EWPCV_FACT_SHEET fs 
            ON ins.FACT_SHEET = fs.ID
			WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);
	BEGIN
		OPEN c_inst(P_INSTITUTION_ID);
		FETCH c_inst INTO v_ins_id, v_f_id, v_fs_cd_id;
		CLOSE c_inst;

		IF v_ins_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no existe';
			RETURN -1;
		END IF;

		IF v_f_id IS NULL OR v_fs_cd_id IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion no tiene fact sheet insertelo antes de actualizarlo';
			RETURN -1;
		END IF;

		BORRA_FACTSHEET_INSTITUTION(v_ins_id, v_f_id);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END;

END PKG_FACTSHEET;
/
create or replace PACKAGE EWP.PKG_MOBILITY_LA AS 

	-- TIPOS DE DATOS
	/*Informacion relativa a los creditos de una asignatura. Campos obligatorios marcados con *.
			SCHEME*				Nombre del esquema de creditos empleado, normalmente ects 
			CREDIT_VALUE*				Cantidad de creditos esperados
	*/	
	TYPE CREDIT IS RECORD
	  (
		SCHEME				VARCHAR2(255 CHAR),
		CREDIT_VALUE		NUMBER(5,1)
	  );  

	/*Informacion sobre cada uno de los componentes o asignaturas del acuerdo de aprendizaje. 
			LA_COMPONENT_TYPE*			Tipo de componente puede tomar los siguientes valores: 
											0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT,
			LOS_CODE					Codigo de la especificacion de la asignatura
			TITLE*						Titulo de la asignatura introducido a mano
			ACADEMIC_TERM*				Informacion del periodo academico de la asignatura. 
											Requerido en componentes estudiados y reconocidos y opcional en los virtuales
			CREDIT*						Informacion sobre lla cantidad y el tipo de creditos necesarios para completar la asignatura
			RECOGNITION_CONDITIONS		Condiciones que deben alcanzarse para reconocer la asignatura en la institucion emisora. 	
											Obligatorio NO informar en componentes estudiados o para reconocimiento automatico. 
			SHORT_DESCRIPTION			Descripcion del componente virtual. 
											Obligatorio en componentes virtuales, en al menos uno de los blended, opcional en los doctorales y no informado en el resto
			STATUS						Indica el estado del componente. Solo se emplea cuando representa un cambio, puede tomar los valores:
											0- inserted, 1- deleted
			REASON_CODE					Codigo predefinido del motivo del cambio, obligatorio si tiene status. 
											Los valores que puede tomar para el estado deleted son:
											0 -NOT_AVAILABLE, 1- LANGUAGE_MISMATCH, 2- TIMETABLE_CONFLICT, 
											Los valores que puede tomar para el estado inserted son:
											3- SUBTITUTING_DELETED, 4- EXTENDING_MOBILITY, 5- ADDING_VIRTUAL_COMPONENT
			REASON_TEXT					Texto que indica el motivo del cambio, se debe proporcionar si no se indica un codigo predefinido de cambio.
	*/	
	TYPE LA_COMPONENT IS RECORD
	  (
		LA_COMPONENT_TYPE		   NUMBER(10,0),
		LOS_CODE				   VARCHAR2(255 CHAR),
		TITLE					   VARCHAR2(255 CHAR),
		ACADEMIC_TERM			   EWP.ACADEMIC_TERM,
		CREDIT					   PKG_MOBILITY_LA.CREDIT,
		RECOGNITION_CONDITIONS	   VARCHAR2(255 CHAR),
		SHORT_DESCRIPTION		   VARCHAR2(255 CHAR),
		STATUS					   NUMBER(10,0),
		REASON_CODE				   NUMBER(10,0),
		REASON_TEXT				   VARCHAR2(255 CHAR)
	  );

	/* Lista con los componentes o asignaturas del acuerdo de aprendizaje. */  
	TYPE LA_COMPONENT_LIST IS TABLE OF LA_COMPONENT INDEX BY BINARY_INTEGER ;

	/*Actualizacion de datos del estudiante que se incluyen en una nueva revision del LA.
		En caso de venir informado todos los campos son obligatorios.
			GIVEN_NAME*				Nombre de la persona
			FAMILY_NAME*			Apellidos de la persona
			BIRTH_DATE*				Fecha de nacimiento de la persona
			CITIZENSHIP*			Codigo del pais del que la persona depende administrativamente
			GENDER*					Genero de la persona 
										0-Desconocido, 1- Masculino, 2- Femenino, 9-No aplica
	*/  
	TYPE STUDENT_LA IS RECORD
	  (
		GIVEN_NAME				VARCHAR2(255 CHAR),
		FAMILY_NAME				VARCHAR2(255 CHAR),
		BIRTH_DATE				DATE,
		CITIZENSHIP				VARCHAR2(255 CHAR),
		GENDER					NUMBER(10,0)
	  );

	/*Datos del estudiante. Campos obligatorios marcados con *.
			GLOBAL_ID*				Identificador global del estudiante de acuerdo a la especificacion del European Student Identifier
			CONTACT					Informacion del estudiante
	*/  
	TYPE STUDENT IS RECORD
	  (
		GLOBAL_ID					VARCHAR2(255 CHAR),
		CONTACT_PERSON				EWP.CONTACT_PERSON
	  );	 

	/* Objeto que modela una movilidad es decir una nominacion. Campos obligatorios marcados con *.
			SENDING_INSTITUTION*		Datos de la instituciÃ³n que envia al estudiante.
			RECEIVING_INSTITUTION*		Datos de la instituciÃ³n que recibe al estudiante.
			ACTUAL_ARRIVAL_DATE			Fecha real de llegada del estudiante aldestino.
			ACTUAL_DEPATURE_DATE		Fecha real de salida del estudiante del origen.
			PLANED_ARRIVAL_DATE*		Fecha prevista de llegada del estudiante aldestino.
			PLANED_DEPATURE_DATE*		Fecha prevista de salida del estudiante del origen.
			IIA_ID						UUID del IIA asociado a la movilidad
			COOPERATION_CONDITION_ID	identificador de la condicion de coperacion asociada la movilidad
			STUDENT*					Datos del estudiante
			EQF_LEVEL*       			Nivel de estudios del estudiante.	
			SUBJECT_AREA*				Informacion sobre el area de ensenyanza del programa de estudio en que se engloba la movilidad. 
			STUDENT_LANGUAGE_SKILLS*	Niveles de idiomas que declara el estudiante.
			SENDER_CONTACT*				Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion origen
			SENDER_ADMV_CONTACT			Informacion de contacto de la administrativo en la institucion origen
			RECEIVER_CONTACT*			Informacion de contacto de la persona encargada de coordinar la movilidad en la institucion destino
			RECEIVER_ADMV_CONTACT		Informacion de contacto de la administrativo en la institucion destino
			MOBILITY_TYPE				Tipo de movilidad, puede tomar los siquientes valores:
										Semester,Blended mobility with short-term physical mobility, Short-term doctoral mobility
			STATUS*						Estado de la nominacion
										0- CANCELLED, 1- LIVE, 2- NOMINATION, 3- RECOGNIZED, 4- REJECTED;
	*/
	TYPE MOBILITY IS RECORD
	  (
		SENDING_INSTITUTION  		EWP.INSTITUTION,
		RECEIVING_INSTITUTION		EWP.INSTITUTION,
		ACTUAL_ARRIVAL_DATE			DATE,
		ACTUAL_DEPATURE_DATE		DATE,
		PLANED_ARRIVAL_DATE			DATE,
		PLANED_DEPATURE_DATE		DATE,
		IIA_ID						VARCHAR(255 CHAR),
		COOPERATION_CONDITION_ID	VARCHAR(255 CHAR),
		STUDENT						PKG_MOBILITY_LA.STUDENT,
		EQF_LEVEL       			NUMBER(3,0),	
		SUBJECT_AREA				EWP.SUBJECT_AREA,
		STUDENT_LANGUAGE_SKILLS		EWP.LANGUAGE_SKILL_LIST,
		SENDER_CONTACT				EWP.CONTACT_PERSON,
		SENDER_ADMV_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_CONTACT			EWP.CONTACT_PERSON,
		RECEIVER_ADMV_CONTACT		EWP.CONTACT_PERSON,
		MOBILITY_TYPE				VARCHAR2 (255),
		STATUS						NUMBER(10,0)
	  );

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			SIGNER_NAME*				Nombre del firmante
			SIGNER_POSITION*			Posicion o cargo que ostenta el firmante
			SIGNER_EMAIL*				Email del firmante
			SIGN_DATE*					Fecha y hora de firma
			SIGNER_APP					Aplicacion empleada para la firma
			SIGNATURE					Imagen digitalizada de la firma
	*/
	TYPE SIGNATURE IS RECORD
	  (
		SIGNER_NAME					VARCHAR2(255 CHAR),
		SIGNER_POSITION				VARCHAR2(255 CHAR),
		SIGNER_EMAIL				VARCHAR2(255 CHAR),
		SIGN_DATE					TIMESTAMP,
		SIGNER_APP					VARCHAR2(255 CHAR),
		SIGNATURE					BLOB
	  );

	/* Objeto que modela un acuerdo de enseÃ±anza. Campos obligatorios marcados con *.
			MOBILITY_ID*				Identificador de la movilidad.
			STUDENT_LA 					Modificaciones sobre los Datos del estudiante implicado en la movilidad que implican una nueva revisiÃ³n del LA.
			COMPONENTS*					Informacion sobre los componentes o asignaturas del acuerdo de aprendizaje. 
			STUDENT_SIGNATURE*			Datos de la firma del estudiante
			SENDING_HEI_SIGNATURE*		Datos de la firma del responsable de la institucion que envia al estudiante.
	*/
	TYPE LEARNING_AGREEMENT IS RECORD
	  (
		MOBILITY_ID					VARCHAR(255 CHAR),
		STUDENT_LA					PKG_MOBILITY_LA.STUDENT_LA,
		COMPONENTS					PKG_MOBILITY_LA.LA_COMPONENT_LIST,
		STUDENT_SIGNATURE			PKG_MOBILITY_LA.SIGNATURE,
		SENDING_HEI_SIGNATURE		PKG_MOBILITY_LA.SIGNATURE
	  );

	-- FUNCIONES 

	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER; 

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER;  	

END PKG_MOBILITY_LA;
/
create or replace PACKAGE BODY EWP.PKG_MOBILITY_LA AS 
	
	--  **********************************************
	--	*****************VALIDACIONES*****************
	--	**********************************************
	/*
		Valida los campos obligatorios de una subject area: 
			isced_code 
	*/
	FUNCTION VALIDA_SUBJECT_AREA(P_SUBJECT_AREA IN EWP.SUBJECT_AREA, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_SUBJECT_AREA.ISCED_CODE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-ISCED_CODE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una language skill: 
			lang, cefr_level 
	*/
	FUNCTION VALIDA_LANGUAGE_SKILL(P_LANGUAGE_SKILL IN EWP.LANGUAGE_SKILL, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN
		IF P_LANGUAGE_SKILL.LANG	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-LANG';
		END IF;

		IF P_LANGUAGE_SKILL.CEFR_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-CEFR_LEVEL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una institucion: 
			Institution_id, organization_unit_code 
	*/
	FUNCTION VALIDA_INSTITUTION(P_INSTITUTION IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
        v_ounit_count NUMBER := 0;
        CURSOR exist_cursor_inst(p_ins_id IN VARCHAR2) IS SELECT ID
            FROM EWPCV_INSTITUTION 
            WHERE UPPER(INSTITUTION_ID) = UPPER(p_ins_id);  
	BEGIN
        OPEN exist_cursor_inst(P_INSTITUTION.INSTITUTION_ID);
		FETCH exist_cursor_inst INTO v_ins_id;
		CLOSE exist_cursor_inst;
        
		IF P_INSTITUTION.INSTITUTION_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una persona de contacto: 
			GIVEN_NAME, FAMILY_NAME, EMAIL_LIST
	*/
	FUNCTION VALIDA_CONTACT_PERSON(P_CONTACT_PERSON IN EWP.CONTACT_PERSON, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CONTACT_PERSON.GIVEN_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GIVEN_NAME';
		END IF;

		IF P_CONTACT_PERSON.FAMILY_NAME	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-FAMILY_NAME';
		END IF;

		IF P_CONTACT_PERSON.CONTACT.EMAIL IS NULL OR P_CONTACT_PERSON.CONTACT.EMAIL.COUNT = 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-CONTACT.EMAIL';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un estudiante: 
			global_id
	*/
	FUNCTION VALIDA_STUDENT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_contact VARCHAR2(2000);
	BEGIN

		IF P_STUDENT.GLOBAL_ID	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-GLOBAL_ID';
		END IF;
		IF VALIDA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, v_mensaje_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						CONTACT: ' || v_mensaje_contact;
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de una movilidad y de los objetos anidados: 
			planed_arrival_date,planed_depature_date, iia_code, status, sending_institution, receiving_institution, student
	*/
	FUNCTION VALIDA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_s_institution VARCHAR2(2000);
		v_mensaje_r_institution VARCHAR2(2000);
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_contact VARCHAR2(2000);
		v_mensaje_r_contact VARCHAR2(2000);
	BEGIN

		IF P_MOBILITY.PLANED_ARRIVAL_DATE	IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_ARRIVAL_DATE';
		END IF;

		IF P_MOBILITY.PLANED_DEPATURE_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-PLANED_DEPATURE_DATE';
		END IF;

		IF P_MOBILITY.EQF_LEVEL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-EQF_LEVEL';
		END IF;

		IF P_MOBILITY.STATUS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-STATUS';
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.SENDING_INSTITUTION, v_mensaje_s_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE|| '
					SENDING_INSTITUTION: ' || v_mensaje_s_institution;
		END IF;

		IF VALIDA_INSTITUTION(P_MOBILITY.RECEIVING_INSTITUTION, v_mensaje_r_institution) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVING_INSTITUTION: ' || v_mensaje_r_institution;
		END IF;

		IF VALIDA_STUDENT(P_MOBILITY.STUDENT, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					STUDENT: ' || v_mensaje_student;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, v_mensaje_s_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					SENDER_CONTACT: ' || v_mensaje_s_contact;
		END IF;

		IF VALIDA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, v_mensaje_r_contact) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					RECEIVER_CONTACT: ' || v_mensaje_r_contact;
		END IF;

		RETURN v_cod_retorno;
	END;
	
	
	/*
		Valida que el iias y la coop condition indicadas existan
	*/
	FUNCTION VALIDA_MOBILITY_IIA(P_IIA_ID IN VARCHAR2,P_COP_COND_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
	BEGIN
		IF P_IIA_ID IS NOT NULL THEN 
			SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_IIA WHERE UPPER(ID) = UPPER(P_IIA_ID);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				El acuerdo interistitucional indicado no existe en el sistema.';
			ELSIF P_COP_COND_ID IS NOT NULL THEN 
				SELECT COUNT(1) INTO v_count FROM EWP.EWPCV_COOPERATION_CONDITION WHERE UPPER(ID) = UPPER(P_COP_COND_ID) AND UPPER(IIA_ID) = UPPER(P_IIA_ID);
				IF v_count = 0 THEN
					v_cod_retorno := -1;
					P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					La condicion de cooperacion indicada no existe en el sistema o no está asociada al acuerdo interistitucional indicado.';
				END IF;
			END IF; 
		END IF; 
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
	FUNCTION VALIDA_DATOS_MOBILITY_INST(P_S_INSTITUTION IN EWP.INSTITUTION, P_R_INSTITUTION  IN EWP.INSTITUTION, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_count NUMBER;
	BEGIN
		-- validacion de sender 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_S_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_S_INSTITUTION.INSTITUTION_ID||' informada como sender institution no existe en el sistema.';
		ELSIF P_S_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_S_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_S_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_S_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_S_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		-- validacion de receiver 
		SELECT COUNT(1) INTO v_count 
			FROM EWP.EWPCV_INSTITUTION_IDENTIFIERS 
			WHERE UPPER(SCHAC) = UPPER(P_R_INSTITUTION.INSTITUTION_ID);
		IF v_count = 0 THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			La institución '||P_R_INSTITUTION.INSTITUTION_ID||' informada como receiver institution no existe en el sistema.';
		ELSIF P_R_INSTITUTION.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
			SELECT COUNT(1) INTO v_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_R_INSTITUTION.INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_R_INSTITUTION.ORGANIZATION_UNIT_CODE);
			IF v_count = 0 THEN
				v_cod_retorno := -1;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				La unidad organizativa '||P_R_INSTITUTION.ORGANIZATION_UNIT_CODE||' vinculada a '||P_R_INSTITUTION.INSTITUTION_ID||' no existe en el sistema.';
			END IF;
		END IF;
		
		RETURN v_cod_retorno;
	END;
	
	/*
		Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
	*/
	FUNCTION VALIDA_DATOS_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;

	BEGIN
		v_cod_retorno := VALIDA_MOBILITY_IIA(P_MOBILITY.IIA_ID, P_MOBILITY.COOPERATION_CONDITION_ID, P_ERROR_MESSAGE);
		
		IF v_cod_retorno = 0 THEN 
				v_cod_retorno := VALIDA_DATOS_MOBILITY_INST(P_MOBILITY.SENDING_INSTITUTION, P_MOBILITY.RECEIVING_INSTITUTION, P_ERROR_MESSAGE);
		END IF;
		
		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de una firma
			signer_name, signer_position, signer_email, timestamp,  signature
	*/
	FUNCTION VALIDA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_SIGNATURE.SIGNER_NAME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_NAME';
		END IF;

		IF P_SIGNATURE.SIGNER_POSITION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_POSITION';
		END IF;

		IF P_SIGNATURE.SIGNER_EMAIL IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGNER_EMAIL';
		END IF;

		IF P_SIGNATURE.SIGN_DATE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					-SIGN_DATE';
		END IF;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los periodos academicos
			academic_year, institution_id, term_number, total_terms
	*/
	FUNCTION VALIDA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ins_id VARCHAR2(255);
	BEGIN         
		IF P_ACADEMIC_TERM.ACADEMIC_YEAR IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-ACADEMIC_YEAR';
		END IF;
        
		IF P_ACADEMIC_TERM.TERM_NUMBER IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TERM_NUMBER';
		END IF;

		IF P_ACADEMIC_TERM.TOTAL_TERMS IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-TOTAL_TERMS';
		END IF;
        
        IF P_ACADEMIC_TERM.INSTITUTION_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-INSTITUTION_ID';
        END IF;

		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM, P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
        v_ounit_count NUMBER := 0;
	BEGIN     
        IF UPPER(P_INSTITUTION_ID) <> UPPER(P_ACADEMIC_TERM.INSTITUTION_ID) THEN
            v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -INSTITUTION_ID: La institucion '|| P_ACADEMIC_TERM.INSTITUTION_ID || ' del periodo academico del componente no coincide con la institucion ' || P_INSTITUTION_ID;
		ELSIF P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE IS NOT NULL THEN 
            SELECT COUNT(1) INTO v_ounit_count FROM EWPCV_INSTITUTION ins
                INNER JOIN EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(P_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
            IF v_ounit_count = 0 THEN 
                v_cod_retorno := -1;
                 P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
                        -OUNIT_CODE: La unidad organizativa ' || P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE || ' vinculada a ' || P_INSTITUTION_ID || ' no existe en el sistema.';
            END IF;
        END IF;
		RETURN v_cod_retorno;
	END;
    
    /*
        Valida la existencia de las instituciones y ounits relacionadas con los componentes
	*/
	FUNCTION VALIDA_INST_OUNIT_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REV IN NUMBER, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
        v_send_inst_id VARCHAR2(255);
        v_receiv_inst_id VARCHAR2(255);
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
        CURSOR c_s_inst_r_inst(p_mob_id IN VARCHAR2, p_mob_rev IN NUMBER) IS 
            SELECT SENDING_INSTITUTION_ID, RECEIVING_INSTITUTION_ID
            FROM EWPCV_MOBILITY 
            WHERE ID = p_mob_id
            AND MOBILITY_REVISION = p_mob_rev;
	BEGIN
        OPEN c_s_inst_r_inst(P_MOBILITY_ID, P_MOBILITY_REV);
        FETCH c_s_inst_r_inst INTO v_send_inst_id, v_receiv_inst_id;
        CLOSE c_s_inst_r_inst;
        
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP
			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 OR P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 5 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_receiv_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_INST_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_send_inst_id, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;
            IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;
		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de los creditos
			scheme, value
	*/
	FUNCTION VALIDA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
	BEGIN

		IF P_CREDIT.SCHEME IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-SCHEME';
		END IF;

		IF P_CREDIT.CREDIT_VALUE IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
						-VALUE';
		END IF;

		RETURN v_cod_retorno;
	END;


	/*
		Valida los campos obligatorios de los componentes
			la_component_type, title, academic_term, credit
	*/
	FUNCTION VALIDA_COMPONENTS(P_COMPONENTS_LIST IN PKG_MOBILITY_LA.LA_COMPONENT_LIST, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_cod_retorno_it NUMBER := 0;
		v_mensaje_acaterm VARCHAR2(2000);
		v_mensaje_credit VARCHAR2(2000);
		v_mensaje_component VARCHAR2(2000);
	BEGIN
		FOR i IN P_COMPONENTS_LIST.FIRST .. P_COMPONENTS_LIST.LAST 
		LOOP

			IF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE NOT IN (0,1,2,3,4) THEN
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-LA_COMPONENT_TYPE (valor erroneo ' || P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')';
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 0 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
				IF P_COMPONENTS_LIST(i).RECOGNITION_CONDITIONS IS NOT NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-RECOGNITION_CONDITIONS (No informar para el tipo de componente '|| P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE||')' ;
				END IF;
			ELSIF P_COMPONENTS_LIST(i).LA_COMPONENT_TYPE = 1 THEN
				IF VALIDA_ACADEMIC_TERM(P_COMPONENTS_LIST(i).ACADEMIC_TERM, v_mensaje_acaterm) <> 0 THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-ACADEMIC_TERM: '|| v_mensaje_acaterm;
					v_mensaje_acaterm := '';
				END IF;
			END IF;

			IF P_COMPONENTS_LIST(i).TITLE IS NULL THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-TITLE';
			END IF;

			IF P_COMPONENTS_LIST(i).STATUS IS NOT NULL THEN
				IF P_COMPONENTS_LIST(i).STATUS = 0 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (3,4,5) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSIF P_COMPONENTS_LIST(i).STATUS = 1 THEN 
					IF P_COMPONENTS_LIST(i).REASON_CODE NOT IN (0,1,2) THEN 
						v_cod_retorno_it := -1;
						v_mensaje_component := v_mensaje_component || '
						-REASON_CODE (valor erroneo ' || P_COMPONENTS_LIST(i).REASON_CODE ||'para el estado ' || P_COMPONENTS_LIST(i).STATUS || ')';
					END IF;
				ELSE 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-STATUS (valor erroneo'|| P_COMPONENTS_LIST(i).STATUS||')';
				END IF;

				IF P_COMPONENTS_LIST(i).REASON_CODE IS NULL THEN 
					v_cod_retorno_it := -1;
					v_mensaje_component := v_mensaje_component || '
					-REASON_CODE';
				END IF;
			END IF;

			IF VALIDA_CREDIT(P_COMPONENTS_LIST(i).CREDIT, v_mensaje_credit) <> 0 THEN 
				v_cod_retorno_it := -1;
				v_mensaje_component := v_mensaje_component || '
					-CREDIT: ' || v_mensaje_credit;
				v_mensaje_credit := '';
			END IF;

			IF v_cod_retorno_it <> 0 THEN 
				v_cod_retorno := v_cod_retorno_it;
				P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
					COMPONENT'|| i || ':' ||v_mensaje_component;
				v_mensaje_component := '';
				v_cod_retorno_it := 0;
			END IF;

		END LOOP;

		RETURN v_cod_retorno;
	END;

	/*
		Valida los campos obligatorios de un Learning Agreement
			mobility, 
	*/
	FUNCTION VALIDA_LEARNING_AGREEMENT(P_LA IN PKG_MOBILITY_LA.LEARNING_AGREEMENT, P_ERROR_MESSAGE IN OUT VARCHAR2) RETURN NUMBER IS 
		v_cod_retorno NUMBER := 0;
		v_mensaje_student VARCHAR2(2000);
		v_mensaje_s_coord VARCHAR2(2000);
		v_mensaje_components VARCHAR2(2000);
	BEGIN

		IF P_LA.MOBILITY_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := '- MOBILITY_ID: ' || P_ERROR_MESSAGE;
		END IF;

		IF P_LA.COMPONENTS IS NULL OR p_LA.COMPONENTS.COUNT = 0 THEN 
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			COMPONENTS';
		ELSIF VALIDA_COMPONENTS(P_LA.COMPONENTS, v_mensaje_components) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				COMPONENTS: ' || v_mensaje_components;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.STUDENT_SIGNATURE, v_mensaje_student) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				STUDENT_SIGNATURE: ' || v_mensaje_student;
		END IF;

		IF VALIDA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE, v_mensaje_s_coord) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
				SENDING_HEI_SIGNATURE: ' || v_mensaje_s_coord;
		END IF;		

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
		END IF;

		RETURN v_cod_retorno;
	END;

	--  **********************************************
	--	*******************BORRADOS*******************
	--	**********************************************

	/* Borra el estudiante de la tabla mobility participant*/
	PROCEDURE BORRA_STUDENT(P_ID IN VARCHAR2) AS
		v_contact_id VARCHAR2(255CHAR);
	BEGIN
		SELECT CONTACT_ID INTO v_contact_id FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		DELETE FROM EWPCV_MOBILITY_PARTICIPANT WHERE ID = P_ID;
		PKG_COMMON.BORRA_CONTACT(v_contact_id);
	END;

	/*
		Borra creditos asociados a un componente
	*/
	PROCEDURE BORRA_CREDITS(P_COMPONENT_ID IN VARCHAR2) AS
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT CREDITS_ID
			FROM EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = p_id;
	BEGIN
		FOR rec IN c(P_COMPONENT_ID) 
		LOOP
			DELETE FROM EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = rec.CREDITS_ID;
			DELETE FROM EWPCV_CREDIT WHERE ID = rec.CREDITS_ID;
		END LOOP;
	END;

	/*
		Borra un componente
	*/
	PROCEDURE BORRA_COMPONENT(P_ID IN VARCHAR2) AS
		v_at_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2) IS 
			SELECT ACADEMIC_TERM_DISPLAY_NAME
			FROM EWPCV_LA_COMPONENT
			WHERE ID = p_id;
	BEGIN
		BORRA_CREDITS(P_ID);
		OPEN c(P_ID);
		FETCH c INTO v_at_id;
		CLOSE c;
		DELETE FROM EWPCV_LA_COMPONENT WHERE ID = P_ID;
		PKG_COMMON.BORRA_ACADEMIC_TERM(v_at_id);
	END;

	/*
		Borra un componente de tipo studied
	*/
	PROCEDURE BORRA_STUDIED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDIED_LA_COMPONENT_ID
			FROM EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = rec.STUDIED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.STUDIED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo recognized
	*/
	PROCEDURE BORRA_RECOGNIZED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = rec.RECOGNIZED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.RECOGNIZED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo virtual
	*/
	PROCEDURE BORRA_VIRTUAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT VIRTUAL_LA_COMPONENT_ID
			FROM EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = rec.VIRTUAL_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.VIRTUAL_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo blended
	*/
	PROCEDURE BORRA_BLENDED_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT BLENDED_LA_COMPONENT_ID
			FROM EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = rec.BLENDED_LA_COMPONENT_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.BLENDED_LA_COMPONENT_ID);
		END LOOP;
	END;

	/*
		Borra un componente de tipo doctoral
	*/
	PROCEDURE BORRA_DOCTORAL_COMP(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		CURSOR c_com(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		FOR rec IN c_com (P_LA_ID, LA_REVISION)
		LOOP
			DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = rec.DOCTORAL_LA_COMPONENTS_ID 
				AND LEARNING_AGREEMENT_ID = P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
			BORRA_COMPONENT(rec.DOCTORAL_LA_COMPONENTS_ID);
		END LOOP;
	END;

	/*
		Borra una revision de un LA
	*/
	PROCEDURE BORRA_LA(P_LA_ID IN VARCHAR2, LA_REVISION IN NUMBER) AS
		v_s_id VARCHAR2(255 CHAR);
		v_sc_id VARCHAR2(255 CHAR);
		v_rc_id VARCHAR2(255 CHAR);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT STUDENT_SIGN, SENDER_COORDINATOR_SIGN, RECEIVER_COORDINATOR_SIGN
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id
				AND LEARNING_AGREEMENT_REVISION = p_revision;

	BEGIN

		BORRA_STUDIED_COMP(P_LA_ID, LA_REVISION);
		BORRA_RECOGNIZED_COMP(P_LA_ID, LA_REVISION);
		BORRA_VIRTUAL_COMP(P_LA_ID, LA_REVISION);
		BORRA_BLENDED_COMP(P_LA_ID, LA_REVISION);
		BORRA_DOCTORAL_COMP(P_LA_ID, LA_REVISION);

		OPEN c(P_LA_ID, LA_REVISION);
		FETCH c INTO v_s_id, v_sc_id, v_rc_id;
		CLOSE c;

		DELETE FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = LA_REVISION;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_s_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_sc_id;
		DELETE FROM EWPCV_SIGNATURE WHERE ID = v_rc_id;
	END;

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/
	PROCEDURE BORRA_MOBILITY_LANSKILL(P_MOBILITY_ID IN VARCHAR2, P_MOBILITY_REVISION IN NUMBER) AS
		v_lskill_id VARCHAR2(255 CHAR);
		CURSOR c_lskill(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LANGUAGE_SKILL_ID
			FROM EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR l_rec IN c_lskill(P_MOBILITY_ID, P_MOBILITY_REVISION)
			LOOP
				DELETE FROM EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = l_rec.LANGUAGE_SKILL_ID;
				DELETE FROM EWPCV_LANGUAGE_SKILL WHERE ID = l_rec.LANGUAGE_SKILL_ID;
			END LOOP;	
	END;

	/*
		Borra una movilidad
	*/
	PROCEDURE BORRA_MOBILITY(P_MOBILITY_ID IN VARCHAR2) AS
		v_student_count NUMBER;
		v_mtype_count NUMBER;

		v_scontact_count NUMBER;
		v_sacontact_count NUMBER;
		v_rcontact_count NUMBER;
		v_racontact_count NUMBER;
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT ID, MOBILITY_REVISION, 
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, ISCED_CODE
			FROM EWPCV_MOBILITY 
			WHERE ID = p_id;

		CURSOR c_la(p_m_id IN VARCHAR2, p_m_revision IN NUMBER) IS 
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
			FROM EWPCV_MOBILITY_LA 
			WHERE MOBILITY_ID = p_m_id
			AND MOBILITY_REVISION = p_m_revision;
	BEGIN
		FOR mobility_rec IN c_mobility(P_MOBILITY_ID)
		LOOP
			BORRA_MOBILITY_LANSKILL(mobility_rec.ID, mobility_rec.MOBILITY_REVISION);

			FOR la_rec IN c_la(mobility_rec.ID, mobility_rec.MOBILITY_REVISION)
			LOOP
				BORRA_LA(la_rec.LEARNING_AGREEMENT_ID, la_rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;	

			DELETE FROM EWPCV_MOBILITY WHERE ID = mobility_rec.ID AND MOBILITY_REVISION = mobility_rec.MOBILITY_REVISION;

			DELETE FROM EWPCV_SUBJECT_AREA WHERE ID = mobility_rec.ISCED_CODE;

			SELECT COUNT(1) INTO v_student_count FROM EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = mobility_rec.MOBILITY_PARTICIPANT_ID;
			IF v_student_count = 0 THEN 
				BORRA_STUDENT(mobility_rec.MOBILITY_PARTICIPANT_ID);
			END IF;

			SELECT COUNT(1) INTO v_scontact_count FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = mobility_rec.SENDER_CONTACT_ID;
			IF v_scontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_sacontact_count FROM EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = mobility_rec.SENDER_ADMV_CONTACT_ID;
			IF v_sacontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.SENDER_ADMV_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_rcontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = mobility_rec.RECEIVER_CONTACT_ID;
			IF v_rcontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_CONTACT_ID);
			END IF;
			SELECT COUNT(1) INTO v_racontact_count FROM EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = mobility_rec.RECEIVER_ADMV_CONTACT_ID;
			IF v_racontact_count = 0 THEN 
				PKG_COMMON.BORRA_CONTACT(mobility_rec.RECEIVER_ADMV_CONTACT_ID);
			END IF;
		END LOOP;
	END;

	--  **********************************************
	--	*****************INSERCIONES******************
	--	**********************************************

	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_PARTICIPANT(P_STUDENT IN PKG_MOBILITY_LA.STUDENT) RETURN VARCHAR2 AS
		v_m_p_id VARCHAR2(255);
		v_c_id   VARCHAR2(255);
	BEGIN
        v_m_p_id := EWP.GENERATE_UUID();
        v_c_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_STUDENT.CONTACT_PERSON, null, null);
        INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, GLOBAL_ID, CONTACT_ID) VALUES (v_m_p_id, P_STUDENT.GLOBAL_ID, v_c_id);

		RETURN v_m_p_id;
	END;



	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
	FUNCTION INSERTA_MOBILITY_TYPE(P_MOBILITY_TYPE IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		CURSOR exist_cursor(p_category IN VARCHAR) IS SELECT ID
			FROM EWPCV_MOBILITY_TYPE
			WHERE UPPER(MOBILITY_CATEGORY) = UPPER(p_category)
			AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA');
	BEGIN
		OPEN exist_cursor(P_MOBILITY_TYPE);
		FETCH exist_cursor INTO v_id;
		CLOSE exist_cursor;

		IF v_id IS NULL AND (P_MOBILITY_TYPE IS NOT NULL) THEN
			v_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (v_id, P_MOBILITY_TYPE, 'MOBILITY_LA');
		END IF;
		RETURN v_id;
	END;


	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_MOBILITY(P_MOBILITY IN PKG_MOBILITY_LA.MOBILITY, P_REVISION IN NUMBER, P_OUNIT_ID IN VARCHAR2) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_s_ounit_id VARCHAR2(255);
		v_r_ounit_id VARCHAR2(255);
		v_student_id VARCHAR2(255);
		v_s_contact_id VARCHAR2(255);
		v_s_admv_contact_id VARCHAR2(255);
		v_r_contact_id VARCHAR2(255);
		v_r_admv_contact_id VARCHAR2(255);
		v_mobility_type_id VARCHAR2(255);
		v_lang_skill_id VARCHAR2(255);
		v_isced_code VARCHAR2(255);
	BEGIN
        v_s_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.SENDING_INSTITUTION);
		v_r_ounit_id := PKG_COMMON.INSERTA_INST_OUNIT(P_MOBILITY.RECEIVING_INSTITUTION);
		v_student_id := INSERTA_MOBILITY_PARTICIPANT(P_MOBILITY.STUDENT);
		v_s_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_CONTACT, null, null);
		v_s_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.SENDER_ADMV_CONTACT, null, null);
		v_r_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_CONTACT, null, null);
		v_r_admv_contact_id := PKG_COMMON.INSERTA_CONTACT_PERSON(P_MOBILITY.RECEIVER_ADMV_CONTACT, null, null);

		v_isced_code := PKG_COMMON.INSERTA_SUBJECT_AREA(P_MOBILITY.SUBJECT_AREA);
		v_mobility_type_id := INSERTA_MOBILITY_TYPE(P_MOBILITY.MOBILITY_TYPE);

		IF P_OUNIT_ID IS NULL THEN 
			v_id := EWP.GENERATE_UUID();
		ELSE 
			v_id := P_OUNIT_ID;
		END IF;
		
		INSERT INTO EWPCV_MOBILITY (ID,
		ACTUAL_ARRIVAL_DATE,
		ACTUAL_DEPARTURE_DATE,
		COOPERATION_CONDITION_ID,
		EQF_LEVEL,
		IIA_ID,
		ISCED_CODE,
		MOBILITY_PARTICIPANT_ID,
		MOBILITY_REVISION,
		PLANNED_ARRIVAL_DATE,
		PLANNED_DEPARTURE_DATE,
		RECEIVING_INSTITUTION_ID,
		RECEIVING_ORGANIZATION_UNIT_ID,
		SENDING_INSTITUTION_ID,
		SENDING_ORGANIZATION_UNIT_ID,
		STATUS,
		MOBILITY_TYPE_ID,
		SENDER_CONTACT_ID,
		SENDER_ADMV_CONTACT_ID,
		RECEIVER_CONTACT_ID,
		RECEIVER_ADMV_CONTACT_ID)
		VALUES (v_id,
		P_MOBILITY.ACTUAL_ARRIVAL_DATE,
		P_MOBILITY.ACTUAL_DEPATURE_DATE,
		P_MOBILITY.COOPERATION_CONDITION_ID,
		P_MOBILITY.EQF_LEVEL,
		P_MOBILITY.IIA_ID,
		v_isced_code,
		v_student_id,
		(P_REVISION +1),
		P_MOBILITY.PLANED_ARRIVAL_DATE,
		P_MOBILITY.PLANED_DEPATURE_DATE,
		P_MOBILITY.RECEIVING_INSTITUTION.INSTITUTION_ID,
		v_r_ounit_id,
		P_MOBILITY.SENDING_INSTITUTION.INSTITUTION_ID,
		v_s_ounit_id,
		P_MOBILITY.STATUS,
		v_mobility_type_id,
		v_s_contact_id,
		v_s_admv_contact_id,
		v_r_contact_id,
		v_r_admv_contact_id);

		IF P_MOBILITY.STUDENT_LANGUAGE_SKILLS IS NOT NULL AND P_MOBILITY.STUDENT_LANGUAGE_SKILLS.COUNT >0 THEN 
			FOR i IN P_MOBILITY.STUDENT_LANGUAGE_SKILLS.FIRST .. P_MOBILITY.STUDENT_LANGUAGE_SKILLS.LAST
			LOOP
				v_lang_skill_id := PKG_COMMON.INSERTA_LANGUAGE_SKILL(P_MOBILITY.STUDENT_LANGUAGE_SKILLS(i));
				INSERT INTO EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (v_id, (P_REVISION + 1), v_lang_skill_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_SIGNATURE(P_SIGNATURE IN PKG_MOBILITY_LA.SIGNATURE) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (v_id, P_SIGNATURE.SIGNER_NAME, P_SIGNATURE.SIGNER_POSITION , P_SIGNATURE.SIGNER_EMAIL , P_SIGNATURE.SIGN_DATE , P_SIGNATURE.SIGNER_APP, P_SIGNATURE.SIGNATURE);
		RETURN v_id;
	END;

	/*
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_ACADEMIC_TERM(P_ACADEMIC_TERM IN EWP.ACADEMIC_TERM) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_a_id VARCHAR2(255);
		v_o_id VARCHAR2(255);
		v_li_id VARCHAR2(255);
		CURSOR C(p_start IN VARCHAR2, p_end IN VARCHAR) IS 
			SELECT ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(p_start)
			AND	UPPER(START_YEAR) =  UPPER(p_end);
		CURSOR ounit_cursor(p_hei_id IN VARCHAR2, p_ounit_code IN VARCHAR2) IS SELECT O.ID
			FROM EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(p_hei_id)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(p_ounit_code);
	BEGIN
		OPEN C(SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4) ,SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		FETCH C INTO v_a_id;
		CLOSE C;
		IF v_a_id IS NULL THEN 
			v_a_id := EWP.GENERATE_UUID();
			INSERT INTO EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
				VALUES(v_a_id, SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 1, 4),SUBSTR(P_ACADEMIC_TERM.ACADEMIC_YEAR, 6, 9));
		END IF;

		OPEN ounit_cursor(P_ACADEMIC_TERM.INSTITUTION_ID, P_ACADEMIC_TERM.ORGANIZATION_UNIT_CODE);
		FETCH ounit_cursor INTO v_o_id;
		CLOSE ounit_cursor;

		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (v_id, P_ACADEMIC_TERM.END_DATE, P_ACADEMIC_TERM.START_DATE , P_ACADEMIC_TERM.INSTITUTION_ID , v_o_id, v_a_id, P_ACADEMIC_TERM.TOTAL_TERMS, P_ACADEMIC_TERM.TERM_NUMBER);

		IF P_ACADEMIC_TERM.DESCRIPTION IS NOT NULL AND P_ACADEMIC_TERM.DESCRIPTION.COUNT > 0 THEN
			FOR i IN P_ACADEMIC_TERM.DESCRIPTION.FIRST .. P_ACADEMIC_TERM.DESCRIPTION.LAST 
			LOOP
				v_li_id:= PKG_COMMON.INSERTA_LANGUAGE_ITEM(P_ACADEMIC_TERM.DESCRIPTION(i));
				INSERT INTO EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (v_id, v_li_id);
			END LOOP;
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_CREDIT(P_CREDIT IN PKG_MOBILITY_LA.CREDIT) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		INSERT INTO EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
			VALUES (v_id, P_CREDIT.SCHEME, P_CREDIT.CREDIT_VALUE);
		RETURN v_id;
	END;

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/
	FUNCTION INSERTA_COMPONENTE(P_COMPONENT IN PKG_MOBILITY_LA.LA_COMPONENT, P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER) RETURN VARCHAR2 AS
		v_id VARCHAR2(255);
		v_at_id VARCHAR2(255);
		v_c_id VARCHAR2(255);
	BEGIN
		v_id := EWP.GENERATE_UUID();

		v_at_id:= INSERTA_ACADEMIC_TERM(P_COMPONENT.ACADEMIC_TERM);

		--FALTAN LOS Y LOIS

		INSERT INTO EWPCV_LA_COMPONENT (ID,ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (v_id, v_at_id, NULL,P_COMPONENT.LOS_CODE, NULL, P_COMPONENT.STATUS, P_COMPONENT.TITLE, P_COMPONENT.REASON_CODE, P_COMPONENT.REASON_TEXT,
				P_COMPONENT.LA_COMPONENT_TYPE, P_COMPONENT.RECOGNITION_CONDITIONS, P_COMPONENT.SHORT_DESCRIPTION);

		v_c_id := INSERTA_CREDIT(P_COMPONENT.CREDIT);
		INSERT INTO EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (v_id,v_c_id);

		IF P_COMPONENT.LA_COMPONENT_TYPE = 0 THEN
			INSERT INTO EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 1 THEN
			INSERT INTO EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 2 THEN
			INSERT INTO EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 3 THEN
			INSERT INTO EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		ELSIF P_COMPONENT.LA_COMPONENT_TYPE = 4 THEN
			INSERT INTO EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(P_LA_ID, P_LA_REVISION, v_id);
		END IF;
		RETURN v_id;
	END;

	/*
		Persiste una revision de un learning agreement.
	*/
	Function INSERTA_LA_REVISION(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_LA_REVISION IN NUMBER, P_MOBILITY_REVISION IN NUMBER) RETURN VARCHAR2 AS 
		v_la_id varchar2(255);
		v_stud_sign_id varchar2(255);
		v_s_hei_sign_id varchar2(255);
		v_changes_id varchar2(255);
		v_c_id varchar2(255);
		v_indice NUMBER := 0;
	BEGIN 
		v_stud_sign_id := INSERTA_SIGNATURE(P_LA.STUDENT_SIGNATURE);
		v_s_hei_sign_id :=  INSERTA_SIGNATURE(P_LA.SENDING_HEI_SIGNATURE);

		IF P_LA_ID IS NULL THEN 
			v_la_id := EWP.GENERATE_UUID();
		ELSE 
			v_la_id := P_LA_ID;
		END IF;
		v_changes_id := v_la_id || '-' ||P_LA_REVISION;
		INSERT INTO EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (v_la_id,P_LA_REVISION,v_stud_sign_id,v_s_hei_sign_id, 0, SYSDATE, v_changes_id);

		INSERT INTO EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(P_LA.MOBILITY_ID, P_MOBILITY_REVISION, v_la_id, P_LA_REVISION);

		WHILE v_indice < P_LA.COMPONENTS.COUNT 
		LOOP
			v_c_id := INSERTA_COMPONENTE(P_LA.COMPONENTS(v_indice),v_la_id, P_LA_REVISION);
			v_indice := v_indice + 1 ;
		END LOOP;
		RETURN v_la_id;
	END;

	/*
		Genera el tag approve de un mobility update request
	*/
	FUNCTION CREA_TAG_APPROVE(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_approve dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:approve-proposal-v1' );
		l_node_approve := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_approve, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_approve;
	END;

	/*
		Genera el tag comment de un mobility update request
	*/
	FUNCTION CREA_TAG_COMMENT(P_CHANGES_ID IN VARCHAR2, P_MOBILITY_ID IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_COMMENT IN VARCHAR2, l_domdoc IN dbms_xmldom.DOMDocument, 
		l_root_node IN dbms_xmldom.DOMNode) RETURN dbms_xmldom.DOMNode AS 

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node dbms_xmldom.DOMNode;
		l_node_text dbms_xmldom.DOMNode;

		l_node_comment dbms_xmldom.DOMNode;
		l_node_signature dbms_xmldom.DOMNode;
	BEGIN

		--tag rama aprove-proposal-v1
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment-proposal-v1' );
		l_node_comment := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja omobility-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_MOBILITY_ID );
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja changes-proposal-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:changes-proposal-id' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_CHANGES_ID);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja comment
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:comment' );
		l_node := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_COMMENT);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag rama signature
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:signature' );
		l_node_signature := dbms_xmldom.appendChild(l_node_comment, dbms_xmldom.makeNode(l_element));

		--tag hoja signer-name
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-name' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_NAME);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-position
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-position' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_POSITION);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-email
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-email' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_EMAIL);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja timestamp
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:timestamp' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, TO_CHAR(P_SIGNATURE.SIGN_DATE, 'YYYY-MM-DD')||'T'||TO_CHAR(P_SIGNATURE.SIGN_DATE,'HH:MI:SS'));
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		--tag hoja signer-app
		l_element := dbms_xmldom.createElement(l_domdoc, 'la:signer-app' );
		l_node := dbms_xmldom.appendChild(l_node_signature, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SIGNATURE.SIGNER_APP);
		l_node_text := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_text));

		return l_node_comment;
	END;


	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_ACCEPT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE ) AS 
		v_id VARCHAR2(255);
		v_mobility_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_approve dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN

		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_approve := CREA_TAG_APPROVE(P_CHANGES_ID ,v_mobility_id,  P_SIGNATURE, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_approve);

		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 0, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	/*
		Genera el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests
	*/
	PROCEDURE INSERTA_REJECT_REQUEST(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_CHANGES_ID IN VARCHAR2, P_SENDING_HEI IN VARCHAR2, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2) AS 
		v_mobility_id VARCHAR2(255);
		v_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		l_domdoc dbms_xmldom.DOMDocument;
		l_root_node dbms_xmldom.DOMNode;

		l_element dbms_xmldom.DOMElement;
		l_text   dbms_xmldom.DOMText;
		l_node_text dbms_xmldom.DOMNode;
		l_node_s_hei dbms_xmldom.DOMNode;

		l_node dbms_xmldom.DOMNode;
		l_node_comment dbms_xmldom.DOMNode;

		bufc CLOB;
		bufb BLOB;
		v_clob_offset NUMBER;
		v_blob_offset NUMBER;
		v_length NUMBER;
		v_lang_context NUMBER :=0;
		v_warning NUMBER;
	BEGIN
		SELECT MAX(MOBILITY_ID) INTO v_mobility_id FROM EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = P_LA_ID AND LEARNING_AGREEMENT_REVISION = P_LA_REVISION;

		l_domdoc := dbms_xmldom.newDomDocument;
		l_root_node := dbms_xmldom.makeNode(l_domdoc);

		--tag raiz omobility-las-update-request
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:omobility-las-update-request' );
		dbms_xmldom.setattribute(l_element,'xmlns:req','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd');
		dbms_xmldom.setattribute(l_element,'xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
		dbms_xmldom.setattribute(l_element,'xmlns:la','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd');
		dbms_xmldom.setattribute(l_element,'xsi:schemaLocation','https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd');
		l_node := dbms_xmldom.appendChild(l_root_node, dbms_xmldom.makeNode(l_element));

		--tag hoja sending-hei-id
		l_element := dbms_xmldom.createElement(l_domdoc, 'req:sending-hei-id' );
		l_node_s_hei := dbms_xmldom.appendChild(l_node, dbms_xmldom.makeNode(l_element));
		l_text := dbms_xmldom.createTextNode(l_domdoc, P_SENDING_HEI );
		l_node_text := dbms_xmldom.appendChild(l_node_s_hei, dbms_xmldom.makeNode(l_text));

		l_node_comment := CREA_TAG_COMMENT(P_CHANGES_ID, v_mobility_id, P_SIGNATURE, P_OBSERVATION, l_domdoc, l_root_node);
		l_node_text := dbms_xmldom.appendChild(l_node, l_node_comment);
		-- conversion del documento a blob
		dbms_lob.createtemporary(bufc, FALSE);
		dbms_lob.createtemporary(bufb,FALSE);

		DBMS_LOB.OPEN(bufc,DBMS_LOB.LOB_READWRITE);
		DBMS_LOB.OPEN(bufb,DBMS_LOB.LOB_READWRITE);

		xmldom.writeToClob(l_domdoc,bufc,4,0);

		v_clob_offset :=1;
		v_blob_offset :=1;
		v_length := DBMS_LOB.GETLENGTH(bufc);

		DBMS_LOB.CONVERTTOBLOB(bufb, bufc, v_length , v_blob_offset, v_clob_offset, 1, v_lang_context, v_warning);

		--persistimos la info
		v_id := EWP.GENERATE_UUID();
		INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (v_id, P_SENDING_HEI, 1, 1, bufb, SYSDATE);

		--liberamos los recursos
		DBMS_LOB.CLOSE(bufc);
		DBMS_LOB.CLOSE(bufb);
		xmldom.freeDocument(l_domdoc);

	END;

	--  **********************************************
	--	**************ACTUALIZACIONES*****************
	--	**********************************************

	PROCEDURE ACTUALIZA_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY) AS 
		v_m_revision VARCHAR2(255);
		v_id VARCHAR2(255);
		CURSOR c_mobility(p_id IN VARCHAR2) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY
			WHERE ID = P_ID;
	BEGIN 
		OPEN c_mobility(P_OMOBILITY_ID);
		FETCH c_mobility INTO v_m_revision;
		CLOSE c_mobility;
		
		--Insertamos la nueva revisión de la Movilidad
		v_id := INSERTA_MOBILITY(P_MOBILITY, v_m_revision, P_OMOBILITY_ID);
         
        -- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
        UPDATE EWPCV_MOBILITY_LA SET MOBILITY_ID = P_OMOBILITY_ID, MOBILITY_REVISION = (v_m_revision + 1) WHERE MOBILITY_ID = P_OMOBILITY_ID AND MOBILITY_REVISION = v_m_revision;

	END;

	--  **********************************************
	--	************** NOTIFICACION ******************
	--	**********************************************

	FUNCTION OBTEN_NOTIFIER_HEI_LA(P_LA_ID IN VARCHAR, P_LA_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR2 AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY M
			INNER JOIN EWPCV_MOBILITY_LA MLA 
				ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = p_id AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN 
		OPEN c(P_LA_ID, P_LA_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN UPPER(v_s_hei);
		ELSE 
			RETURN NULL;
		END IF;
	END;

	FUNCTION OBTEN_NOTIFIER_HEI_MOB(P_MOB_ID IN VARCHAR, P_MOBLITY_REVISION IN NUMBER, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN VARCHAR AS
		v_s_hei VARCHAR2(255);
		v_r_hei VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
		SELECT RECEIVING_INSTITUTION_ID, SENDING_INSTITUTION_ID
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id
			AND MOBILITY_REVISION = p_revision
		ORDER BY MOBILITY_REVISION DESC;

	BEGIN 
		OPEN c(P_MOB_ID, P_MOBLITY_REVISION);
		FETCH c into v_r_hei, v_s_hei;
		CLOSE c;

		IF UPPER(v_r_hei) = UPPER(P_HEI_TO_NOTIFY) THEN 
			RETURN  UPPER(v_s_hei); 
		ELSE 
			RETURN NULL;
		END IF;
	END;
	--  **********************************************
	--	**************** FUNCIONES *******************
	--	**********************************************


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_m_revision NUMBER := 0;
		v_la_id VARCHAR2(255);
		CURSOR C(p_id IN VARCHAR2) IS SELECT MAX(MOBILITY_REVISION)
		FROM EWPCV_MOBILITY 
		WHERE ID = p_id;
		CURSOR C_LA(p_id IN VARCHAR2) IS 
		SELECT LEARNING_AGREEMENT_ID 
		FROM EWPCV_MOBILITY_LA 
		WHERE MOBILITY_ID = p_id;

	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;
		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		OPEN C(P_LA.MOBILITY_ID);
		FETCH C INTO v_m_revision;
		CLOSE C;
		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada no existe';
			RETURN -1;
		END IF;

		OPEN C_LA(P_LA.MOBILITY_ID);
		FETCH C_LA INTO v_la_id;
		CLOSE C_LA;
		IF v_la_id IS NOT NULL THEN 
			P_ERROR_MESSAGE := 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: '||v_la_id;
			RETURN -1;
		END IF;		

        v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
        
		P_LA_ID := INSERTA_LA_REVISION(null, P_LA, 0, v_m_revision);
		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, 0, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion receptora';
			P_LA_ID := null;
			ROLLBACK;
			RETURN -1;
		END IF;

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_LEARNING_AGREEMENT; 

	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_no_firmado NUMBER:=0;
		v_la_revision NUMBER;
		v_m_revision NUMBER;
		v_la_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2) IS 
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;
		CURSOR c_m_revision(p_mobility_id IN VARCHAR2, p_la_id IN VARCHAR2, p_la_revision IN NUMBER) IS 
			SELECT MAX(MOBILITY_REVISION)
			FROM EWPCV_MOBILITY_LA
			WHERE MOBILITY_ID = p_mobility_id
				AND LEARNING_AGREEMENT_ID = p_la_id
				AND LEARNING_AGREEMENT_REVISION = p_la_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		OPEN c_la(P_LA_ID);
		FETCH c_la INTO v_la_revision;
		CLOSE c_la;

		IF v_la_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado no existe';
			RETURN -1;
		END IF;

		Open c_m_revision(P_LA.MOBILITY_ID, P_LA_ID, v_la_revision);
		FETCH c_m_revision INTO v_m_revision;
		CLOSE c_m_revision;

		IF v_m_revision IS NULL THEN 
			P_ERROR_MESSAGE := 'La movilidad indicada para el learning agreement no es correcta.';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_no_firmado FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID AND STATUS = 0;
		IF v_no_firmado > 0 THEN 
			P_ERROR_MESSAGE := 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_LEARNING_AGREEMENT(P_LA, P_ERROR_MESSAGE);
		IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;

		v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, v_la_revision, P_HEI_TO_NOTIFY);

		IF v_notifier_hei IS NULL THEN 
			P_ERROR_MESSAGE := 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_INST_OUNIT_COMPONENTS(P_LA.COMPONENTS, P_LA.MOBILITY_ID, v_m_revision, P_ERROR_MESSAGE);
        IF v_cod_retorno <> 0 THEN
			RETURN v_cod_retorno;
		END IF;
		
		v_la_id := INSERTA_LA_REVISION(P_LA_ID, P_LA, v_la_revision + 1, v_m_revision);

		PKG_COMMON.INSERTA_NOTIFICATION(P_LA.MOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_LEARNING_AGREEMENT; 	

	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;


		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_ACCEPT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE);
			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END ACCEPT_LEARNING_AGREEMENT; 	 

	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
        Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER AS		v_cod_retorno NUMBER := 0;
		v_la_id VARCHAR2(255);
		v_sign_id VARCHAR2(255);
		v_msg_val_firma VARCHAR2(255);
		v_sending_hei_id VARCHAR2(255);
		v_changes_id VARCHAR2(255);
		CURSOR c_la(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT ID, RECEIVER_COORDINATOR_SIGN, CHANGES_ID
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id 
			AND LEARNING_AGREEMENT_REVISION = p_revision;

		CURSOR c_s_ins(p_id IN VARCHAR2, p_revision IN NUMBER) IS 
			SELECT M.SENDING_INSTITUTION_ID
			FROM EWPCV_MOBILITY_LA MLA
				INNER JOIN EWPCV_MOBILITY M
					ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = p_id 
				AND MLA.LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN

		IF P_LA_ID IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_ID';
		END IF;
		IF P_LA_REVISION IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_LA_REVISION';
		END IF;
		IF VALIDA_SIGNATURE(P_SIGNATURE, v_msg_val_firma) <> 0 THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := P_ERROR_MESSAGE || '
			-P_SIGNATURE: ' || v_msg_val_firma;
		END IF;

		IF v_cod_retorno <> 0 THEN
			P_ERROR_MESSAGE := 'Faltan los siguientes campos obligatorios: ' || P_ERROR_MESSAGE;
			RETURN v_cod_retorno;
		END IF;

		OPEN c_la(P_LA_ID,P_LA_REVISION);
		FETCH c_la into v_la_id, v_sign_id, v_changes_id;
		CLOSE c_la;

		IF v_la_id IS NULL THEN 
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		ELSIF v_sign_id IS NOT NULL THEN
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La revision del Learning Agreement ya esta firmada';
		ELSE
			OPEN c_s_ins(P_LA_ID,P_LA_REVISION);
			FETCH c_s_ins into v_sending_hei_id;
			CLOSE c_s_ins;

            IF UPPER(v_sending_hei_id) <> UPPER(P_HEI_TO_NOTIFY) THEN
                P_ERROR_MESSAGE := 'La institucion a notificar no coincide con la institucion emisora del acuerdo de aprendizaje. Unicamente se deben aceptar o rechazar LAs de tipo Incomming';
                RETURN v_cod_retorno;
            END IF;

			INSERTA_REJECT_REQUEST(P_LA_ID, P_LA_REVISION, v_changes_id, v_sending_hei_id, P_SIGNATURE, P_OBSERVATION);

			COMMIT;
		END IF;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END REJECT_LEARNING_AGREEMENT; 	

	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER	AS
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
		v_count NUMBER := 0;
		v_m_id VARCHAR2(255);
		CURSOR c(p_id IN VARCHAR2) IS SELECT LEARNING_AGREEMENT_REVISION
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = p_id;

		CURSOR c_mob(p_id IN VARCHAR2, p_revision IN NUMBER) IS SELECT MOBILITY_ID
			FROM EWPCV_MOBILITY_LA 
			WHERE LEARNING_AGREEMENT_ID = p_id
			AND LEARNING_AGREEMENT_REVISION = p_revision;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_LEARNING_AGREEMENT WHERE ID = P_LA_ID;
		IF v_count > 0 THEN 
			FOR rec IN c(P_LA_ID) 
			LOOP 
				IF v_notifier_hei IS NULL THEN 
					v_notifier_hei := OBTEN_NOTIFIER_HEI_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION, P_HEI_TO_NOTIFY);
				END IF;

				IF v_notifier_hei IS NULL THEN 
					P_ERROR_MESSAGE := 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.';
					RETURN -1;
				END IF;

				IF v_m_id IS NULL THEN 
					OPEN c_mob(P_LA_ID,rec.LEARNING_AGREEMENT_REVISION);
					FETCH c_mob INTO v_m_id;
					CLOSE c_mob;
				END IF;

				BORRA_LA(P_LA_ID, rec.LEARNING_AGREEMENT_REVISION);
			END LOOP;

			PKG_COMMON.INSERTA_NOTIFICATION(v_m_id, 5, P_HEI_TO_NOTIFY, v_notifier_hei);	
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'El Learning Agreement no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_LEARNING_AGREEMENT; 

	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER AS 
		v_cod_retorno NUMBER := 0;
		v_notifier_hei VARCHAR2(255);
	BEGIN


		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN
			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		END IF;
		IF v_cod_retorno = 0 THEN
			P_OMOBILITY_ID := INSERTA_MOBILITY(P_MOBILITY, -1, null);

		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END INSERT_MOBILITY; 	

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_cod_retorno NUMBER := 0;
        v_sending_hei VARCHAR2(255);
        v_id VARCHAR2(255);
        CURSOR C_SENDING_HEI IS SELECT ID, SENDING_INSTITUTION_ID
        FROM EWPCV_MOBILITY 
        WHERE ID = P_OMOBILITY_ID
        ORDER BY MOBILITY_REVISION DESC;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		v_cod_retorno := VALIDA_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
		IF v_cod_retorno = 0 THEN

            OPEN C_SENDING_HEI;
            FETCH C_SENDING_HEI INTO v_id, v_sending_hei;
            CLOSE C_SENDING_HEI;

            IF v_id IS NULL THEN 
            	P_ERROR_MESSAGE := 'No existe la movilidad indicada';
                RETURN -1;
			END IF;

			IF UPPER(v_sending_hei) = UPPER(P_HEI_TO_NOTIFY) THEN
				P_ERROR_MESSAGE := 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                RETURN -1;
			END IF;
			
			v_cod_retorno := VALIDA_DATOS_MOBILITY(P_MOBILITY, P_ERROR_MESSAGE);
			IF v_cod_retorno = 0 THEN
				ACTUALIZA_MOBILITY(P_OMOBILITY_ID,P_MOBILITY);
				PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_sending_hei);
			END IF;
		END IF;
		COMMIT;
		RETURN v_cod_retorno;

		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END UPDATE_MOBILITY; 

	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER AS
		v_notifier_hei VARCHAR2(255);
		v_cod_retorno NUMBER := 0;
		v_count NUMBER := 0;
		v_m_revision NUMBER ;
	BEGIN
		IF P_HEI_TO_NOTIFY IS NULL THEN
			P_ERROR_MESSAGE := 'No se ha indicado la institucion a la que notificar los cambios';
			RETURN -1;
		END IF;

		SELECT COUNT(1) INTO v_count FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
		IF v_count > 0 THEN
			SELECT MAX(MOBILITY_REVISION) INTO v_m_revision FROM EWPCV_MOBILITY WHERE ID = P_OMOBILITY_ID;
			v_notifier_hei := OBTEN_NOTIFIER_HEI_MOB(P_OMOBILITY_ID, v_m_revision, P_HEI_TO_NOTIFY);

			IF v_notifier_hei IS NULL THEN 
				P_ERROR_MESSAGE := 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.';
				RETURN -1;
			END IF;

			BORRA_MOBILITY(P_OMOBILITY_ID);
			PKG_COMMON.INSERTA_NOTIFICATION(P_OMOBILITY_ID, 5, P_HEI_TO_NOTIFY, v_notifier_hei);
		ELSE
			v_cod_retorno := -1;
			P_ERROR_MESSAGE := 'La movilidad no existe';
		END IF;
		COMMIT;
		RETURN v_cod_retorno;
		EXCEPTION 
			WHEN OTHERS THEN
			P_ERROR_MESSAGE := substr(SQLERRM,12);
			ROLLBACK;
			RETURN -1;
	END DELETE_MOBILITY;  	



END PKG_MOBILITY_LA;
/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/


/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

GRANT SELECT ON EWP.INSTITUTION_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON EWP.INSTITUTION_CONTACTS_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON EWP.OUNIT_VIEW TO EWP_APROVISIONAMIENTO;
GRANT SELECT ON EWP.OUNIT_CONTACTS_VIEW TO EWP_APROVISIONAMIENTO;


GRANT SELECT ON EWP.IIA_COOP_COND_CONTACTS_VIEW TO EWP_APROVISIONAMIENTO;

GRANT EXECUTE ON EWP.PKG_COMMON TO EWP_APROVISIONAMIENTO;

GRANT EXECUTE ON EWP.ACADEMIC_TERM TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.ADDRESS_LINE_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT_PERSON TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.CONTACT_PERSON_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.DELIVERY_POINT_CODE_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.EMAIL_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.FLEXIBLE_ADDRESS TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.INSTITUTION TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_ITEM TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_ITEM_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_SKILL TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.LANGUAGE_SKILL_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PHONE_NUMBER TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.PHONE_NUMBER_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.RECIPIENT_NAME_LIST TO EWP_APROVISIONAMIENTO;
GRANT EXECUTE ON EWP.SUBJECT_AREA TO EWP_APROVISIONAMIENTO;


/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

CREATE OR REPLACE SYNONYM APLEWP.IIA_COOP_COND_CONTACTS_VIEW FOR EWP.IIA_COOP_COND_CONTACTS_VIEW;

CREATE OR REPLACE SYNONYM APLEWP.PKG_FACTSHEET FOR EWP.PKG_FACTSHEET;
CREATE OR REPLACE SYNONYM APLEWP.PKG_IIAS FOR EWP.PKG_IIAS;
CREATE OR REPLACE SYNONYM APLEWP.PKG_INSTITUTION FOR EWP.PKG_INSTITUTION;
CREATE OR REPLACE SYNONYM APLEWP.PKG_MOBILITY_LA FOR EWP.PKG_MOBILITY_LA;
CREATE OR REPLACE SYNONYM APLEWP.PKG_OUNITS FOR EWP.PKG_OUNITS;
CREATE OR REPLACE SYNONYM APLEWP.PKG_COMMON FOR EWP.PKG_COMMON;


CREATE OR REPLACE SYNONYM APLEWP.ACADEMIC_TERM FOR EWP.ACADEMIC_TERM;
CREATE OR REPLACE SYNONYM APLEWP.ADDRESS_LINE_LIST FOR EWP.ADDRESS_LINE_LIST;
CREATE OR REPLACE SYNONYM APLEWP.CONTACT FOR EWP.CONTACT;
CREATE OR REPLACE SYNONYM APLEWP.CONTACT_PERSON FOR EWP.CONTACT_PERSON;
CREATE OR REPLACE SYNONYM APLEWP.CONTACT_PERSON_LIST FOR EWP.CONTACT_PERSON_LIST;
CREATE OR REPLACE SYNONYM APLEWP.DELIVERY_POINT_CODE_LIST FOR EWP.DELIVERY_POINT_CODE_LIST;
CREATE OR REPLACE SYNONYM APLEWP.EMAIL_LIST FOR EWP.EMAIL_LIST;
CREATE OR REPLACE SYNONYM APLEWP.FLEXIBLE_ADDRESS FOR EWP.FLEXIBLE_ADDRESS;
CREATE OR REPLACE SYNONYM APLEWP.INSTITUTION FOR EWP.INSTITUTION;
CREATE OR REPLACE SYNONYM APLEWP.LANGUAGE_ITEM FOR EWP.LANGUAGE_ITEM;
CREATE OR REPLACE SYNONYM APLEWP.LANGUAGE_ITEM_LIST FOR EWP.LANGUAGE_ITEM_LIST;
CREATE OR REPLACE SYNONYM APLEWP.LANGUAGE_SKILL FOR EWP.LANGUAGE_SKILL;
CREATE OR REPLACE SYNONYM APLEWP.LANGUAGE_SKILL_LIST FOR EWP.LANGUAGE_SKILL_LIST;
CREATE OR REPLACE SYNONYM APLEWP.PHONE_NUMBER FOR EWP.PHONE_NUMBER;
CREATE OR REPLACE SYNONYM APLEWP.PHONE_NUMBER_LIST FOR EWP.PHONE_NUMBER_LIST;
CREATE OR REPLACE SYNONYM APLEWP.RECIPIENT_NAME_LIST FOR EWP.RECIPIENT_NAME_LIST;
CREATE OR REPLACE SYNONYM APLEWP.SUBJECT_AREA FOR EWP.SUBJECT_AREA;



/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.03.01', SYSDATE, '13_UPGRADE_v01.03.01');
COMMIT;