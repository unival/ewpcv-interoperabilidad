/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

 ALTER TABLE EWP.EWPCV_LA_COMPONENT MODIFY (RECOGNITION_CONDITIONS VARCHAR2(4000 CHAR));
 ALTER TABLE EWP.EWPCV_LA_COMPONENT MODIFY (SHORT_DESCRIPTION VARCHAR2(4000 CHAR));
 ALTER TABLE EWP.EWPCV_LA_COMPONENT MODIFY (REASON_TEXT VARCHAR2(4000 CHAR));
 ALTER TABLE EWP.EWPCV_CONTACT_DETAILS_EMAIL MODIFY (EMAIL VARCHAR2(4000));

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.03.02', SYSDATE, '46_LONGITUD_CAMPOS_LAS.sql');
COMMIT;