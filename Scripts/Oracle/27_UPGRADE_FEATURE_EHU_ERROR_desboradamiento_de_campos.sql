
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

ALTER TABLE EWP.EWPCV_DICTIONARY MODIFY DESCRIPTION VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_INSTITUTION_IDENTIFIERS MODIFY INSTITUTION_NAMES VARCHAR2(4000 BYTE);
ALTER TABLE EWP.EWPCV_ADDITIONAL_INFO MODIFY ADDITIONAL_INFO VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_DIPLOMA_SECTION MODIFY TITLE VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_LA_COMPONENT MODIFY TITLE VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_LANGUAGE_ITEM MODIFY TEXT VARCHAR2(4000 BYTE);
ALTER TABLE EWP.EWPCV_REQUIREMENTS_INFO MODIFY NAME VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_REQUIREMENTS_INFO MODIFY DESCRIPTION VARCHAR2(4000 BYTE);
ALTER TABLE EWP.EWPCV_SUBJECT_AREA MODIFY ISCED_CLARIFICATION  VARCHAR2(1000 BYTE);
ALTER TABLE EWP.EWPCV_COOPERATION_CONDITION MODIFY OTHER_INFO VARCHAR2(4000 BYTE);

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v01.08.04', SYSDATE, '27_UPGRADE_FEATURE_EHU_ERROR_desboradamiento_de_campos');
COMMIT;
    
    
    
    
    
    
    
    
    
