ALTER TABLE ewp.EWPCV_IIA MODIFY approval_date timestamp;

ALTER TABLE ewp.EWPCV_IIA_APPROVALS MODIFY date_iia_approval timestamp;