create sequence EWP.SEC_EWPCV_NOTIFICACTION start with 1 increment by  1;

    create table EWP.EWPCV_ACADEMIC_TERM_NAME (
       ACADEMIC_TERM_ID varchar2(255 char) not null,
        DISP_NAME_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_ACADEMIC_TERM (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        END_DATE date,
        INSTITUTION_ID varchar2(255 char),
        ORGANIZATION_UNIT_ID varchar2(255 char),
        START_DATE date,
        ACADEMIC_YEAR_ID varchar2(255 char),
		TOTAL_TERMS NUMBER(10,0),
		TERM_NUMBER  NUMBER(10,0),
        primary key (ID)
    );

    create table EWP.EWPCV_ACADEMIC_YEAR (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        END_YEAR varchar2(255 char),
        START_YEAR varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_CONTACT (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        INSTITUTION_ID varchar2(255 char),
        ORGANIZATION_UNIT_ID varchar2(255 char),
        CONTACT_ROLE varchar2(255 char),
        CONTACT_DETAILS_ID varchar2(255 char),
        PERSON_ID varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_CONTACT_DESCRIPTION (
       CONTACT_ID varchar2(255 char) not null,
        DESCRIPTION_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_CONTACT_NAME (
       CONTACT_ID varchar2(255 char) not null,
        NAME_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_CONTACT_URL (
       CONTACT_DETAILS_ID varchar2(255 char) not null,
        URL_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_CONTACT_DETAILS (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        FAX_NUMBER varchar2(255 char),
        MAILING_ADDRESS varchar2(255 char),
        PHONE_NUMBER varchar2(255 char),
        STREET_ADDRESS varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_CONTACT_DETAILS_EMAIL (
       CONTACT_DETAILS_ID varchar2(255 char) not null,
        EMAIL varchar2(255 char)
    );

    create table EWP.EWPCV_COOPERATION_CONDITION (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        END_DATE date,
        EQF_LEVEL number(3,0) not null,
        START_DATE date,
        DURATION_ID varchar2(255 char),
        MOBILITY_NUMBER_ID varchar2(255 char),
        MOBILITY_TYPE_ID varchar2(255 char),
        RECEIVING_PARTNER_ID varchar2(255 char),
        SENDING_PARTNER_ID varchar2(255 char),
        IIA_ID varchar2(255 char),
		OTHER_INFO VARCHAR2(255 CHAR),
        primary key (ID)
    );

    create table EWP.EWPCV_CREDIT (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        CREDIT_LEVEL varchar2(255 char),
        SCHEME varchar2(255 char),
        CREDIT_VALUE number(5,1),
        primary key (ID)
    );

    create table EWP.EWPCV_DISTR_CATEGORIES (
       RESULT_DISTRIBUTION_ID varchar2(255 char) not null,
        RESULT_DIST_CATEGORY_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_DISTR_DESCRIPTION (
       RESULT_DISTRIBUTION_ID varchar2(255 char) not null,
        DESCRIPTION_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_DURATION (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        NUMBERDURATION number(5,1),
        UNIT varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_FACT_SHEET_URL (
       FACT_SHEET_ID varchar2(255 char) not null,
        URL_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_FACT_SHEET (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        CONTACT_DETAILS_ID varchar2(255 char),
		DECISION_WEEKS_LIMIT NUMBER(10,0),
		TOR_WEEKS_LIMIT NUMBER(10,0),
        primary key (ID)
    );

    create table EWP.EWPCV_FLEXIBLE_ADDRESS (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        BUILDING_NAME varchar2(255 char),
        BUILDING_NUMBER varchar2(255 char),
        COUNTRY varchar2(255 char),
        "FLOOR" varchar2(255 char),
        LOCALITY varchar2(255 char),
        POST_OFFICE_BOX varchar2(255 char),
        POSTAL_CODE varchar2(255 char),
        REGION varchar2(255 char),
        STREET_NAME varchar2(255 char),
        UNIT varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_FLEXIBLE_ADDRESS_LINE (
       FLEXIBLE_ADDRESS_ID varchar2(255 char) not null,
        ADDRESS_LINE varchar2(255 char)
    );

    create table EWP.EWPCV_FLEXAD_DELIV_POINT_COD (
       FLEXIBLE_ADDRESS_ID varchar2(255 char) not null,
        DELIVERY_POINT_CODE varchar2(255 char)
    );

    create table EWP.EWPCV_FLEXAD_RECIPIENT_NAME (
       FLEXIBLE_ADDRESS_ID varchar2(255 char) not null,
        RECIPIENT_NAME varchar2(255 char)
    );

    create table EWP.EWPCV_GRADING_SCHEME_DESC (
       GRADING_SCHEME_ID varchar2(255 char) not null,
        DESCRIPTION_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_GRADING_SCHEME_LABEL (
       GRADING_SCHEME_ID varchar2(255 char) not null,
        LABEL_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_GRADING_SCHEME (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        primary key (ID)
    );

    create table EWP.EWPCV_IIA (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        END_DATE date,
        IIA_CODE varchar2(255 char),
        MODIFY_DATE date,
        START_DATE date,
		REMOTE_IIA_ID VARCHAR2(255 CHAR),
		REMOTE_IIA_CODE VARCHAR2(255 CHAR),
		REMOTE_COP_COND_HASH VARCHAR2(255 CHAR),
		APPROVAL_DATE DATE,
		APPROVAL_COP_COND_HASH VARCHAR2(255 CHAR),
		PDF CLOB,
        primary key (ID)
    );

    create table EWP.EWPCV_IIA_PARTNER_CONTACTS (
       IIA_PARTNER_ID varchar2(255 char) not null,
        CONTACTS_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_IIA_PARTNER (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        INSTITUTION_ID varchar2(255 char),
        ORGANIZATION_UNIT_ID varchar2(255 char),
		SIGNER_PERSON_CONTACT_ID VARCHAR2(255 CHAR),
		SIGNING_DATE DATE,
        primary key (ID)
    );

    create table EWP.EWPCV_INST_ORG_UNIT (
       INSTITUTION_ID varchar2(255 char) not null,
        ORGANIZATION_UNITS_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_INSTITUTION (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        ABBREVIATION varchar2(255 char),
        INSTITUTION_ID varchar2(255 char),
        LOGO_URL varchar2(255 char),
        FACT_SHEET varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_INSTITUTION_NAME (
       INSTITUTION_ID varchar2(255 char) not null,
        NAME_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LANGUAGE_ITEM (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        LANG varchar2(255 char),
        TEXT varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_LEARNING_AGREEMENT (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        LEARNING_AGREEMENT_REVISION number(10,0) not null,
		STUDENT_SIGN VARCHAR2(255 CHAR),
		SENDER_COORDINATOR_SIGN VARCHAR2(255 CHAR),
		RECEIVER_COORDINATOR_SIGN VARCHAR2(255 CHAR),
		STATUS NUMBER(10,0) NOT NULL,
		MODIFIED_DATE DATE,
		MODIFIED_STUDENT_CONTACT_ID VARCHAR2(255 CHAR),
		PDF CLOB,
        primary key (ID, LEARNING_AGREEMENT_REVISION)
    );

    create table EWP.EWPCV_LOI (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        ENGAGEMENT_HOURS number(19,2),
        LANGUAGE_OF_INSTRUCTION varchar2(255 char),
        ACADEMIC_TERM_ID varchar2(255 char),
        GRADING_SCHEME_ID varchar2(255 char),
        RESULT_DISTRIBUTION varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_LOI_CREDITS (
       LOI_ID varchar2(255 char) not null,
        CREDITS_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LOS (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        EQF_LEVEL number(3,0) not null,
        INSTITUTION_ID varchar2(255 char),
        ISCEDF varchar2(255 char),
        LOS_CODE varchar2(255 char),
        ORGANIZATION_UNIT_ID varchar2(255 char),
        SUBJECT_AREA varchar2(255 char),
        TOP_LEVEL_PARENT number(1,0) not null,
        "TYPE" number(10,0),
        primary key (ID)
    );

    create table EWP.EWPCV_LOS_DESCRIPTION (
       LOS_ID varchar2(255 char) not null,
        DESCRIPTION_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LOS_LOI (
       LOS_ID varchar2(255 char) not null,
        LOI_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LOS_LOS (
       LOS_ID varchar2(255 char) not null,
        OTHER_LOS_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LOS_NAME (
       LOS_ID varchar2(255 char) not null,
        NAME_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_LOS_URLS (
       LOS_ID varchar2(255 char) not null,
        URL_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_MOBILITY (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        ACTUAL_ARRIVAL_DATE date,
        ACTUAL_DEPARTURE_DATE date,
        COOPERATION_CONDITION_ID varchar2(255 char),
        EQF_LEVEL number(3,0) not null,
        IIA_ID varchar2(255 char),
        ISCED_CODE varchar2(255 char),
        MOBILITY_PARTICIPANT_ID varchar2(255 char),
        MOBILITY_REVISION number(10,0) not null,
        PLANNED_ARRIVAL_DATE date,
        PLANNED_DEPARTURE_DATE date,
        RECEIVING_INSTITUTION_ID varchar2(255 char),
        RECEIVING_ORGANIZATION_UNIT_ID varchar2(255 char),
        SENDING_INSTITUTION_ID varchar2(255 char),
        SENDING_ORGANIZATION_UNIT_ID varchar2(255 char),
        STATUS number(10,0),
        MOBILITY_TYPE_ID varchar2(255 char),
		LANGUAGE_SKILL VARCHAR2(255 CHAR),
		SENDER_CONTACT_ID VARCHAR2(255 CHAR),
		SENDER_ADMV_CONTACT_ID VARCHAR2(255 CHAR),
		RECEIVER_CONTACT_ID VARCHAR2(255 CHAR),
		RECEIVER_ADMV_CONTACT_ID VARCHAR2(255 CHAR),
		REMOTE_MOBILITY_ID VARCHAR2(255 CHAR),
        primary key (ID, MOBILITY_REVISION)
    );

    create table EWP.EWPCV_MOBILITY_NUMBER (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        NUMBERMOBILITY number(10,0) not null,
        VARIANT varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_MOBILITY_PARTICIPANT (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
		CONTACT_ID VARCHAR2(255 CHAR),
        primary key (ID)
    );

    create table EWP.EWPCV_MOBILITY_TYPE (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        MOBILITY_CATEGORY varchar2(255 char),
        MOBILITY_GROUP varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_MOBILITY_UPDATE_REQUEST (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        SENDING_HEI_ID varchar2(255 char),
        "TYPE" number(10,0),
        UPDATE_INFORMATION clob,
        UPDATE_REQUEST_DATE date,
        primary key (ID)
    );

    create table EWP.EWPCV_NOTIFICATION (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        CHANGED_ELEMENT_IDS varchar2(255 char),
        HEI_ID varchar2(255 char),
        NOTIFICATION_DATE date,
        "TYPE" number(10,0),
		CNR_TYPE NUMBER(2, 0) NOT NULL,
		RETRIES NUMBER(5, 0) DEFAULT 0,
		PROCESSING NUMBER(1, 0),
		OWNER_HEI VARCHAR2(255 CHAR) NOT NULL,
        primary key (ID)
    );

    create table EWP.EWPCV_ORG_UNIT_ORG_UNIT (
       ORGANIZATION_UNIT_ID varchar2(255 char) not null,
        ORGANIZATION_UNITS_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_ORGANIZATION_UNIT_NAME (
       ORGANIZATION_UNIT_ID varchar2(255 char) not null,
        NAME_ID varchar2(255 char) not null
    );

    create table EWP.EWPCV_ORGANIZATION_UNIT (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        ABBREVIATION varchar2(255 char),
        LOGO_URL varchar2(255 char),
        ORGANIZATION_UNIT_CODE varchar2(255 char),
        FACT_SHEET varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_PERSON (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        BIRTH_DATE date,
        COUNTRY_CODE varchar2(255 char),
        FIRST_NAMES varchar2(255 char),
        GENDER number(10,0),
        LAST_NAME varchar2(255 char),
        PERSON_ID varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_PHONE_NUMBER (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        E164 varchar2(255 char),
        EXTENSION_NUMBER varchar2(255 char),
        OTHER_FORMAT varchar2(255 char),
        primary key (ID)
    );

    create table EWP.EWPCV_RESULT_DISTRIBUTION (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        primary key (ID)
    );

    create table EWP.EWPCV_RESULT_DIST_CATEGORY (
       ID varchar2(255 char) not null,
        VERSION number(10,0),
        DISTRUBTION_COUNT number(19,2),
        LABEL varchar2(255 char),
        primary key (ID)
    );

	CREATE TABLE  EWP.EWPCV_LA_COMPONENT(
		ID VARCHAR2(255 CHAR) NOT NULL,
		VERSION NUMBER(10) NULL,
		ACADEMIC_TERM_DISPLAY_NAME VARCHAR2(255 CHAR) NULL,
		LOI_ID VARCHAR2(255 CHAR) NULL,
		LOS_CODE VARCHAR2(255 CHAR) NULL,
		LOS_ID VARCHAR2(255 CHAR) NULL,
		STATUS NUMBER(10) NULL,
		TITLE VARCHAR2(255 CHAR) NULL,
		REASON_CODE NUMBER(10) NULL,
		REASON_TEXT VARCHAR2(255) NULL,
		LA_COMPONENT_TYPE NUMBER(10) NULL,
		RECOGNITION_CONDITIONS VARCHAR2(255) NULL,
		SHORT_DESCRIPTION VARCHAR2(255) NULL,
		primary key (ID)
	);

	CREATE TABLE EWP.EWPCV_STUDIED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR2(255 CHAR) NOT NULL,
		STUDIED_LA_COMPONENT_ID VARCHAR2(255 CHAR) NOT NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_RECOGNIZED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR2(255 CHAR) NOT NULL,
		RECOGNIZED_LA_COMPONENT_ID VARCHAR2(255 CHAR) NOT NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_VIRTUAL_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR2(255) NOT NULL,
		VIRTUAL_LA_COMPONENT_ID VARCHAR2(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_BLENDED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR2(255) NOT NULL,
		BLENDED_LA_COMPONENT_ID VARCHAR2(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_DOCTORAL_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR2(255) NOT NULL,
		DOCTORAL_LA_COMPONENTS_ID VARCHAR2(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NOT NULL
	);
	
	CREATE TABLE EWP.EWPCV_SUBJECT_AREA(
		ISCED_CODE VARCHAR2(255) NOT NULL,
		ISCED_CLARIFICATION VARCHAR2(255) NULL,
		PRIMARY KEY (ISCED_CODE)
	);

	CREATE TABLE EWP.EWPCV_LANGUAGE_SKILL(
		ID VARCHAR2(255) NOT NULL,
		"LANGUAGE" VARCHAR2(255) NULL,
		CEFR_LEVEL VARCHAR2(255) NULL,
		PRIMARY KEY (ID) 
	);
	
	CREATE TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL(
		ID VARCHAR2(255) NOT NULL,
		COOPERATION_CONDITION_ID VARCHAR2(255) NOT NULL,
		ISCED_CODE VARCHAR2(255) NOT NULL,
		LANGUAGE_SKILL_ID VARCHAR2(255) NULL,
		PRIMARY KEY (ID)
	);
	
	CREATE TABLE EWP.EWPCV_SIGNATURE(
		ID VARCHAR2(255) NOT NULL,
		SIGNER_NAME VARCHAR2(255) NULL,
		SIGNER_POSITION VARCHAR2(255) NULL,
		SIGNER_EMAIL VARCHAR2(255) NULL,
		"TIMESTAMP" DATE NULL,
		SIGNER_APP VARCHAR2(255) NULL,
		SIGNATURE CLOB NULL,
		PRIMARY KEY (ID)
	);
	
	CREATE TABLE  EWP.EWPCV_MOBILITY_LA(
		MOBILITY_ID VARCHAR2(255) NULL,
		MOBILITY_REVISION NUMBER(10) NULL,
		LEARNING_AGREEMENT_ID VARCHAR2(255) NULL,
		LEARNING_AGREEMENT_REVISION NUMBER(10) NULL
	);
	
	CREATE TABLE EWP.EWPCV_INFORMATION_ITEM(
		ID VARCHAR2(255 CHAR) NOT NULL,
		VERSION NUMBER(10) NULL,
		"TYPE" VARCHAR2(500) NULL,
		CONTACT_DETAIL_ID VARCHAR2(255) NULL,
		PRIMARY KEY (ID) 
	);

	CREATE TABLE EWP.EWPCV_INST_INF_ITEM(
		INSTITUTION_ID VARCHAR2(255 CHAR) NOT NULL,
		INFORMATION_ID VARCHAR2(255 CHAR) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_REQUIREMENTS_INFO(
		ID VARCHAR2(255) NOT NULL,
		"TYPE" VARCHAR2(255) NULL,
		NAME VARCHAR2(255) NULL,
		DESCRIPTION VARCHAR2(255) NULL,
		CONTACT_DETAIL_ID VARCHAR2(255) NULL,
		PRIMARY KEY (ID) 
	);

	CREATE TABLE EWP.EWPCV_OU_INF_ITEM(
		ORGANIZATION_UNIT_ID VARCHAR2(255 CHAR) NOT NULL,
		INFORMATION_ID VARCHAR2(255 CHAR) NOT NULL
	);

	CREATE TABLE EWP.EWPCV_OU_REQUIREMENT_INFO(
		OU_ID VARCHAR2(255) NULL,
		REQUERIMENT_ID VARCHAR2(255) NULL
	);

	CREATE TABLE EWP.EWPCV_INS_REQUIREMENTS(
		INSTITUTION_ID VARCHAR2(255) NULL,
		REQUIREMENT_ID VARCHAR2(255) NULL
	);

	CREATE TABLE EWP.EWPCV_FACTSHEET_DATES(
		FACTSHEET_ID VARCHAR2(255) NULL,
		ACADEMIC_TERM_ID VARCHAR2(255) NULL,
		"TYPE" NUMBER(10) NULL
	);

    alter table EWP.EWPCV_ACADEMIC_TERM_NAME 
       add constraint UK_ACADEMIC_TERM_NAME_1 unique (DISP_NAME_ID);

    alter table EWP.EWPCV_CONTACT_DESCRIPTION 
       add constraint UK_CONTACT_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table EWP.EWPCV_CONTACT_NAME 
       add constraint UK_CONTACT_NAME_1 unique (NAME_ID);

    alter table EWP.EWPCV_CONTACT_URL 
       add constraint UK_CONTACT_URL_1 unique (URL_ID);

    alter table EWP.EWPCV_DISTR_CATEGORIES 
       add constraint UK_DISTR_CATEGORIES_1 unique (RESULT_DIST_CATEGORY_ID);

    alter table EWP.EWPCV_DISTR_DESCRIPTION 
       add constraint UK_DISTR_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table EWP.EWPCV_FACT_SHEET_URL 
       add constraint UK_FACT_SHEET_URL_1 unique (URL_ID);

    alter table EWP.EWPCV_GRADING_SCHEME_DESC 
       add constraint UK_GRADING_SCHEME_DESC_1 unique (DESCRIPTION_ID);

    alter table EWP.EWPCV_GRADING_SCHEME_LABEL 
       add constraint UK_GRADING_SCHEME_LABEL_1 unique (LABEL_ID);

    alter table EWP.EWPCV_INST_ORG_UNIT 
       add constraint UK_INST_ORG_UNIT_1 unique (ORGANIZATION_UNITS_ID);

    alter table EWP.EWPCV_INSTITUTION_NAME 
       add constraint UK_INSTITUTION_NAME_1 unique (NAME_ID);

    alter table EWP.EWPCV_LOI_CREDITS 
       add constraint UK_LOI_CREDITS_1 unique (CREDITS_ID);

    alter table EWP.EWPCV_LOS_DESCRIPTION 
       add constraint UK_LOS_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table EWP.EWPCV_LOS_LOI 
       add constraint UK_LOS_LOI_1 unique (LOI_ID);

    alter table EWP.EWPCV_LOS_LOS 
       add constraint UK_LOS_LOS_1 unique (OTHER_LOS_ID);

    alter table EWP.EWPCV_LOS_NAME 
       add constraint UK_LOS_NAME_1 unique (NAME_ID);

    alter table EWP.EWPCV_LOS_URLS 
       add constraint UK_LOS_URLS_1 unique (URL_ID);

    alter table EWP.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint UK_ORG_UNIT_ORG_UNIT_1 unique (ORGANIZATION_UNITS_ID);

    alter table EWP.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint UK_ORGANIZATION_UNIT_NAME_1 unique (NAME_ID);

    alter table EWP.EWPCV_RECOGNIZED_LA_COMPONENT 
       add constraint UK_RECOGNIZED_LA_COMPONENT_1 unique (RECOGNIZED_LA_COMPONENT_ID);

    alter table EWP.EWPCV_STUDIED_LA_COMPONENT 
       add constraint UK_STUDIED_LA_COMPONENT_1 unique (STUDIED_LA_COMPONENT_ID);

    alter table EWP.EWPCV_ACADEMIC_TERM_NAME 
       add constraint FK_ACADEMIC_TERM_NAME_1
       foreign key (DISP_NAME_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_ACADEMIC_TERM_NAME 
       add constraint FK_ACADEMIC_TERM_NAME_2
       foreign key (ACADEMIC_TERM_ID) 
       references EWP.EWPCV_ACADEMIC_TERM;

    alter table EWP.EWPCV_ACADEMIC_TERM 
       add constraint FK_ACADEMIC_TERM_NAME_3
       foreign key (ACADEMIC_YEAR_ID) 
       references EWP.EWPCV_ACADEMIC_YEAR;
	   
	alter table EWP.EWPCV_CONTACT
       add constraint FK_CONTACT_1
       foreign key (CONTACT_DETAILS_ID)
       references EWP.EWPCV_CONTACT_DETAILS;
	   
    alter table EWP.EWPCV_CONTACT
       add constraint FK_CONTACT_2
       foreign key (PERSON_ID)
       references EWP.EWPCV_PERSON;

    alter table EWP.EWPCV_CONTACT_DESCRIPTION 
       add constraint FK_CONTACT_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_CONTACT_DESCRIPTION 
       add constraint FK_CONTACT_DESCRIPTION_2
       foreign key (CONTACT_ID) 
       references EWP.EWPCV_CONTACT;

    alter table EWP.EWPCV_CONTACT_NAME 
       add constraint FK_CONTACT_NAME_1
       foreign key (NAME_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_CONTACT_NAME 
       add constraint FK_CONTACT_NAME_2
       foreign key (CONTACT_ID) 
       references EWP.EWPCV_CONTACT;

    alter table EWP.EWPCV_CONTACT_URL 
       add constraint FK_CONTACT_URL_1
       foreign key (URL_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_CONTACT_URL 
       add constraint FK_CONTACT_URL_2
       foreign key (CONTACT_DETAILS_ID) 
       references EWP.EWPCV_CONTACT_DETAILS;

    alter table EWP.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_1
       foreign key (FAX_NUMBER) 
       references EWP.EWPCV_PHONE_NUMBER;

    alter table EWP.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_2
       foreign key (MAILING_ADDRESS) 
       references EWP.EWPCV_FLEXIBLE_ADDRESS;

    alter table EWP.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_3
       foreign key (PHONE_NUMBER) 
       references EWP.EWPCV_PHONE_NUMBER;

    alter table EWP.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_4
       foreign key (STREET_ADDRESS) 
       references EWP.EWPCV_FLEXIBLE_ADDRESS;

    alter table EWP.EWPCV_CONTACT_DETAILS_EMAIL 
       add constraint FK_CONTACT_DETAILS_5
       foreign key (CONTACT_DETAILS_ID) 
       references EWP.EWPCV_CONTACT_DETAILS;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_1
       foreign key (DURATION_ID) 
       references EWP.EWPCV_DURATION;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_2
       foreign key (MOBILITY_NUMBER_ID) 
       references EWP.EWPCV_MOBILITY_NUMBER;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_3
       foreign key (MOBILITY_TYPE_ID) 
       references EWP.EWPCV_MOBILITY_TYPE;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_4
       foreign key (RECEIVING_PARTNER_ID) 
       references EWP.EWPCV_IIA_PARTNER;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_5
       foreign key (SENDING_PARTNER_ID) 
       references EWP.EWPCV_IIA_PARTNER;

    alter table EWP.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_6
       foreign key (IIA_ID) 
       references EWP.EWPCV_IIA;

    alter table EWP.EWPCV_DISTR_CATEGORIES 
       add constraint FK_DISTR_CATEGORIES_1
       foreign key (RESULT_DIST_CATEGORY_ID) 
       references EWP.EWPCV_RESULT_DIST_CATEGORY;

    alter table EWP.EWPCV_DISTR_CATEGORIES 
       add constraint FK_DISTR_CATEGORIES_2
       foreign key (RESULT_DISTRIBUTION_ID) 
       references EWP.EWPCV_RESULT_DISTRIBUTION;

    alter table EWP.EWPCV_DISTR_DESCRIPTION 
       add constraint FK_DISTR_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_DISTR_DESCRIPTION 
       add constraint FK_DISTR_DESCRIPTION_2
       foreign key (RESULT_DISTRIBUTION_ID) 
       references EWP.EWPCV_RESULT_DISTRIBUTION;

    alter table EWP.EWPCV_FACT_SHEET_URL 
       add constraint FK_FACT_SHEET_URL_1
       foreign key (URL_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_FACT_SHEET_URL 
       add constraint FK_FACT_SHEET_URL_2
       foreign key (FACT_SHEET_ID) 
       references EWP.EWPCV_FACT_SHEET;

    alter table EWP.EWPCV_FACT_SHEET 
       add constraint FK_FACT_SHEET_1
       foreign key (CONTACT_DETAILS_ID) 
       references EWP.EWPCV_CONTACT_DETAILS;

    alter table EWP.EWPCV_FLEXIBLE_ADDRESS_LINE
       add constraint FK_FLEXIBLE_ADDRESS_LINE_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references EWP.EWPCV_FLEXIBLE_ADDRESS;

    alter table EWP.EWPCV_FLEXAD_DELIV_POINT_COD 
       add constraint FK_FLEXAD_DELIV_POINT_COD_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references EWP.EWPCV_FLEXIBLE_ADDRESS;

    alter table EWP.EWPCV_FLEXAD_RECIPIENT_NAME
       add constraint FK_FLEXAD_RECIPIENT_NAME_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references EWP.EWPCV_FLEXIBLE_ADDRESS;

    alter table EWP.EWPCV_GRADING_SCHEME_DESC 
       add constraint FK_GRADING_SCHEM_DESC_DESC_1
       foreign key (DESCRIPTION_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_GRADING_SCHEME_DESC 
       add constraint FK_GRADING_SCHEM_DESC_DESC_2
       foreign key (GRADING_SCHEME_ID) 
       references EWP.EWPCV_GRADING_SCHEME;

    alter table EWP.EWPCV_GRADING_SCHEME_LABEL 
       add constraint FK_GRADING_SCHEME_LABEL_1
       foreign key (LABEL_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_GRADING_SCHEME_LABEL 
       add constraint FK_GRADING_SCHEME_LABEL_2
       foreign key (GRADING_SCHEME_ID) 
       references EWP.EWPCV_GRADING_SCHEME;

    alter table EWP.EWPCV_IIA_PARTNER_CONTACTS 
       add constraint FK_IIA_PARTNER_CONTACTS_1
       foreign key (CONTACTS_ID) 
       references EWP.EWPCV_CONTACT;

    alter table EWP.EWPCV_IIA_PARTNER_CONTACTS 
       add constraint FK_IIA_PARTNER_CONTACTS_2
       foreign key (IIA_PARTNER_ID) 
       references EWP.EWPCV_IIA_PARTNER;

    alter table EWP.EWPCV_INST_ORG_UNIT 
       add constraint FK_INST_ORG_UNIT_1
       foreign key (ORGANIZATION_UNITS_ID) 
       references EWP.EWPCV_ORGANIZATION_UNIT;

    alter table EWP.EWPCV_INST_ORG_UNIT 
       add constraint FK_INST_ORG_UNIT_2
       foreign key (INSTITUTION_ID) 
       references EWP.EWPCV_INSTITUTION;

    alter table EWP.EWPCV_INSTITUTION 
       add constraint FK_INSTITUTION_1
       foreign key (FACT_SHEET) 
       references EWP.EWPCV_FACT_SHEET;

    alter table EWP.EWPCV_INSTITUTION_NAME 
       add constraint FK_INSTITUTION_NAME_1
       foreign key (NAME_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_INSTITUTION_NAME 
       add constraint FK_INSTITUTION_NAME_2
       foreign key (INSTITUTION_ID) 
       references EWP.EWPCV_INSTITUTION;

    alter table EWP.EWPCV_LOI 
       add constraint FK_LOI_1
       foreign key (ACADEMIC_TERM_ID) 
       references EWP.EWPCV_ACADEMIC_TERM;

    alter table EWP.EWPCV_LOI 
       add constraint FK_LOI_2
       foreign key (GRADING_SCHEME_ID) 
       references EWP.EWPCV_GRADING_SCHEME;

    alter table EWP.EWPCV_LOI 
       add constraint FK_LOI_3
       foreign key (RESULT_DISTRIBUTION) 
       references EWP.EWPCV_RESULT_DISTRIBUTION;

    alter table EWP.EWPCV_LOI_CREDITS 
       add constraint FK_LOI_CREDITS_1
       foreign key (CREDITS_ID) 
       references EWP.EWPCV_CREDIT;

    alter table EWP.EWPCV_LOI_CREDITS 
       add constraint FK_LOI_CREDITS_2
       foreign key (LOI_ID) 
       references EWP.EWPCV_LOI;

    alter table EWP.EWPCV_LOS_DESCRIPTION 
       add constraint FK_LOS_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_LOS_DESCRIPTION 
       add constraint FK_LOS_DESCRIPTION_2
       foreign key (LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_LOS_LOI 
       add constraint FK_LOS_LOI_1
       foreign key (LOI_ID) 
       references EWP.EWPCV_LOI;

    alter table EWP.EWPCV_LOS_LOI 
       add constraint FK_LOS_LOI_2
       foreign key (LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_LOS_LOS 
       add constraint FK_LOS_LOS_1
       foreign key (OTHER_LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_LOS_LOS 
       add constraint FK_LOS_LOS_2
       foreign key (LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_LOS_NAME 
       add constraint FK_LOS_NAME_1
       foreign key (NAME_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_LOS_NAME 
       add constraint FK_LOS_NAME_2
       foreign key (LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_LOS_URLS 
       add constraint FK_LOS_URLS_1
       foreign key (URL_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_LOS_URLS 
       add constraint FK_LOS_URLS_2
       foreign key (LOS_ID) 
       references EWP.EWPCV_LOS;

    alter table EWP.EWPCV_MOBILITY 
       add constraint FK_MOBILITY_1
       foreign key (MOBILITY_TYPE_ID) 
       references EWP.EWPCV_MOBILITY_TYPE;

    alter table EWP.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint FK_ORG_UNIT_ORG_UNIT_1
       foreign key (ORGANIZATION_UNITS_ID) 
       references EWP.EWPCV_ORGANIZATION_UNIT;

    alter table EWP.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint FK_ORG_UNIT_ORG_UNIT_2
       foreign key (ORGANIZATION_UNIT_ID) 
       references EWP.EWPCV_ORGANIZATION_UNIT;

    alter table EWP.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint FK_ORG_UNIT_ORG_UNIT_NAME_1
       foreign key (NAME_ID) 
       references EWP.EWPCV_LANGUAGE_ITEM;

    alter table EWP.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint FK_ORGANIZATION_UNIT_NAME_2
       foreign key (ORGANIZATION_UNIT_ID) 
       references EWP.EWPCV_ORGANIZATION_UNIT;

    alter table EWP.EWPCV_ORGANIZATION_UNIT 
       add constraint FK_ORGANIZATION_UNIT_1
       foreign key (FACT_SHEET) 
       references EWP.EWPCV_FACT_SHEET;

	ALTER TABLE  EWP.EWPCV_IIA_PARTNER 
		ADD CONSTRAINT FK_IIA_PARTNER_1
		FOREIGN KEY (SIGNER_PERSON_CONTACT_ID) REFERENCES  EWP.EWPCV_CONTACT (ID);
		
	ALTER TABLE  EWP.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_1
		FOREIGN KEY (MODIFIED_STUDENT_CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT (ID);

	ALTER TABLE  EWP.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_2
		FOREIGN KEY (RECEIVER_COORDINATOR_SIGN) REFERENCES  EWP.EWPCV_SIGNATURE (ID);

	ALTER TABLE  EWP.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_3
		FOREIGN KEY (STUDENT_SIGN) REFERENCES  EWP.EWPCV_SIGNATURE (ID);

	ALTER TABLE  EWP.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_4
		FOREIGN KEY (SENDER_COORDINATOR_SIGN) REFERENCES  EWP.EWPCV_SIGNATURE (ID);
		
	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_2
		FOREIGN KEY (ISCED_CODE) references EWP.EWPCV_SUBJECT_AREA (ISCED_CODE);

	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_3
		FOREIGN KEY (LANGUAGE_SKILL) references EWP.EWPCV_LANGUAGE_SKILL (ID);
		
	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_4
		FOREIGN KEY (RECEIVER_ADMV_CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT (ID);

	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_5
		FOREIGN KEY (RECEIVER_CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT (ID);

	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_6
		FOREIGN KEY (SENDER_CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT (ID);

	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_7
		FOREIGN KEY (SENDER_ADMV_CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT (ID);

	ALTER TABLE  EWP.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_8
		FOREIGN KEY (MOBILITY_PARTICIPANT_ID) REFERENCES EWP.EWPCV_MOBILITY_PARTICIPANT(ID);
	
	ALTER TABLE  EWP.EWPCV_MOBILITY_PARTICIPANT 
		ADD CONSTRAINT FK_MOBILITY_PARTICIPANT_1
		FOREIGN KEY (CONTACT_ID) REFERENCES EWP.EWPCV_CONTACT(ID);
		
	ALTER TABLE  EWP.EWPCV_LA_COMPONENT 
		ADD CONSTRAINT FK_LA_COMPONENT_1
		FOREIGN KEY (LOI_ID) REFERENCES  EWP.EWPCV_LOI (ID);
	
	ALTER TABLE  EWP.EWPCV_LA_COMPONENT 
		ADD CONSTRAINT FK_LA_COMPONENT_2
		FOREIGN KEY (LOS_ID) REFERENCES  EWP.EWPCV_LOS (ID);

	ALTER TABLE  EWP.EWPCV_STUDIED_LA_COMPONENT 
		ADD CONSTRAINT FK_STUDIED_LA_COMPONENT_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  EWP.EWPCV_STUDIED_LA_COMPONENT 
		ADD CONSTRAINT FK_STUDIED_LA_COMPONENT_2
		FOREIGN KEY (STUDIED_LA_COMPONENT_ID) REFERENCES  EWP.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  EWP.EWPCV_RECOGNIZED_LA_COMPONENT 
		ADD CONSTRAINT FK_RECOGNIZED_LA_COMPONENT_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  EWP.EWPCV_RECOGNIZED_LA_COMPONENT 
		ADD CONSTRAINT FK_RECOGNIZED_LA_COMPONENT_2
		FOREIGN KEY (RECOGNIZED_LA_COMPONENT_ID) REFERENCES  EWP.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  EWP.EWPCV_VIRTUAL_LA_COMPONENT 
		ADD CONSTRAINT FK_VIRTUAL_LA_COMPONENT_1
		FOREIGN KEY (VIRTUAL_LA_COMPONENT_ID) REFERENCES  EWP.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  EWP.EWPCV_VIRTUAL_LA_COMPONENT 
		ADD CONSTRAINT FK_VIRTUAL_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  EWP.EWPCV_BLENDED_LA_COMPONENT 
		ADD CONSTRAINT FK_BLENDED_LA_COMPONENT_1
		FOREIGN KEY (BLENDED_LA_COMPONENT_ID) REFERENCES  EWP.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  EWP.EWPCV_BLENDED_LA_COMPONENT 
		ADD CONSTRAINT FK_BLENDED_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  EWP.EWPCV_DOCTORAL_LA_COMPONENT 
		ADD CONSTRAINT FK_DOCTORAL_LA_COMPONENT_1
		FOREIGN KEY (DOCTORAL_LA_COMPONENTS_ID) REFERENCES  EWP.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  EWP.EWPCV_DOCTORAL_LA_COMPONENT 
		ADD CONSTRAINT FK_DOCTORAL_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);
		
	ALTER TABLE  EWP.EWPCV_COOPCOND_SUBAR_LANSKIL 
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_1
		FOREIGN KEY (COOPERATION_CONDITION_ID) REFERENCES  EWP.EWPCV_COOPERATION_CONDITION (ID);

	ALTER TABLE  EWP.EWPCV_COOPCOND_SUBAR_LANSKIL 
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2
		FOREIGN KEY (ISCED_CODE) REFERENCES  EWP.EWPCV_SUBJECT_AREA (ISCED_CODE);

	ALTER TABLE  EWP.EWPCV_COOPCOND_SUBAR_LANSKIL
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_3
		FOREIGN KEY (LANGUAGE_SKILL_ID) REFERENCES  EWP.EWPCV_LANGUAGE_SKILL (ID);

	ALTER TABLE  EWP.EWPCV_MOBILITY_LA 
		ADD CONSTRAINT FK_MOBILITY_LA_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  EWP.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);
	
	ALTER TABLE  EWP.EWPCV_MOBILITY_LA 
		ADD CONSTRAINT FK_MOBILITY_LA_2
		FOREIGN KEY (MOBILITY_ID,MOBILITY_REVISION) REFERENCES  EWP.EWPCV_MOBILITY (ID,MOBILITY_REVISION);
	
	ALTER TABLE  EWP.EWPCV_INFORMATION_ITEM 
		ADD CONSTRAINT FK_IDENTIFICATION_ITEM_1
		FOREIGN KEY (CONTACT_DETAIL_ID) REFERENCES EWP.EWPCV_CONTACT_DETAILS(ID);
		
	ALTER TABLE  EWP.EWPCV_INST_INF_ITEM 
		ADD CONSTRAINT FK_INST_INF_ITEM_1
		FOREIGN KEY (INFORMATION_ID) REFERENCES  EWP.EWPCV_INFORMATION_ITEM (ID);

	ALTER TABLE  EWP.EWPCV_INST_INF_ITEM 
		ADD CONSTRAINT FK_INST_INF_ITEM_2
		FOREIGN KEY (INSTITUTION_ID) REFERENCES  EWP.EWPCV_INSTITUTION (ID);
		
	ALTER TABLE  EWP.EWPCV_OU_INF_ITEM 
		ADD CONSTRAINT FK_OU_INF_ITEM_1
		FOREIGN KEY (ORGANIZATION_UNIT_ID) REFERENCES  EWP.EWPCV_ORGANIZATION_UNIT (ID);

	ALTER TABLE  EWP.EWPCV_OU_INF_ITEM 
		ADD CONSTRAINT FK_OU_INF_ITEM_2
		FOREIGN KEY (INFORMATION_ID) REFERENCES  EWP.EWPCV_INFORMATION_ITEM (ID);

	ALTER TABLE  EWP.EWPCV_REQUIREMENTS_INFO 
		ADD CONSTRAINT FK_REQUIREMENTS_INFO_1
		FOREIGN KEY (CONTACT_DETAIL_ID) REFERENCES  EWP.EWPCV_CONTACT_DETAILS (ID);
	
	ALTER TABLE  EWP.EWPCV_OU_REQUIREMENT_INFO 
		ADD CONSTRAINT FK_OU_REQUIREMENT_INFO_1
		FOREIGN KEY (OU_ID) REFERENCES  EWP.EWPCV_ORGANIZATION_UNIT (ID);

	ALTER TABLE  EWP.EWPCV_OU_REQUIREMENT_INFO 
		ADD CONSTRAINT FK_OU_REQUIREMENT_INFO_2
		FOREIGN KEY (REQUERIMENT_ID) REFERENCES  EWP.EWPCV_REQUIREMENTS_INFO (ID);

	ALTER TABLE  EWP.EWPCV_INS_REQUIREMENTS 
		ADD CONSTRAINT FK_INS_REQUIREMENTS_1
		FOREIGN KEY (INSTITUTION_ID) REFERENCES EWP.EWPCV_INSTITUTION (ID);

	ALTER TABLE  EWP.EWPCV_INS_REQUIREMENTS 
		ADD CONSTRAINT FK_INS_REQUIREMENTS_2
		FOREIGN KEY (REQUIREMENT_ID) REFERENCES  EWP.EWPCV_REQUIREMENTS_INFO (ID);

	ALTER TABLE  EWP.EWPCV_FACTSHEET_DATES 
		ADD CONSTRAINT FK_FACTSHEET_DATES_1
		FOREIGN KEY (ACADEMIC_TERM_ID) REFERENCES EWP.EWPCV_ACADEMIC_TERM (ID);

	ALTER TABLE  EWP.EWPCV_FACTSHEET_DATES 
		ADD CONSTRAINT FK_DATE_FACTSHEET_2
		FOREIGN KEY (FACTSHEET_ID) REFERENCES  EWP.EWPCV_FACT_SHEET (ID);
		
	create or replace FUNCTION EWP.ExtraerAnio(START_DATE TIMESTAMP) RETURN NUMBER AS
	 ANIO NUMBER;
	BEGIN
		ANIO := EXTRACT(YEAR FROM START_DATE);  
		RETURN ANIO;
	END;
	