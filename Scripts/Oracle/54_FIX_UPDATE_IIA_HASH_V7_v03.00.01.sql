/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
--teoricamente la informacion debería estar alineada por lo que deberiamos poder coger los hashes de la tabla de aprobaciones y actualizar los hashes de los IIAs
--ahora actualizamos los hashes de los IIAs locales
UPDATE EWP.EWPCV_IIA iia SET
        iia.REMOTE_COP_COND_HASH = (SELECT app.LOCAL_HASH_V7 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.ID = app.LOCAL_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.LOCAL_HASH_V6),
        iia.APPROVAL_COP_COND_HASH = (SELECT app.LOCAL_HASH_V7 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.ID = app.LOCAL_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.LOCAL_HASH_V6)
        WHERE EXISTS (SELECT 1 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.ID = app.LOCAL_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.LOCAL_HASH_V6);

--ahora actualizamos los hashes de los IIAs remotos
UPDATE EWP.EWPCV_IIA iia SET
        iia.REMOTE_COP_COND_HASH = (SELECT app.REMOTE_HASH_V7 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.REMOTE_IIA_ID = app.REMOTE_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.REMOTE_HASH_V6),
        iia.APPROVAL_COP_COND_HASH = (SELECT app.REMOTE_HASH_V7 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.REMOTE_IIA_ID = app.REMOTE_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.REMOTE_HASH_V6)
        WHERE EXISTS (SELECT 1 FROM EWP.EWPCV_IIA_APPROVALS app WHERE iia.REMOTE_IIA_ID = app.REMOTE_IIA_ID AND iia.APPROVAL_COP_COND_HASH = app.REMOTE_HASH_V6) AND iia.IS_REMOTE = 1;
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v03.00.01', SYSDATE, '54_FIX_UPDATE_IIA_HASH_V7_v03.00.01');
COMMIT;
