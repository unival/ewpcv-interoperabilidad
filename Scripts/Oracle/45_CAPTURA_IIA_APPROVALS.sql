/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

ALTER TABLE EWP.EWPCV_IIA_APPROVALS 
ADD (LOCAL_HASH_V7 VARCHAR2(255));

ALTER TABLE EWP.EWPCV_IIA_APPROVALS 
ADD (REMOTE_HASH_V7 VARCHAR2(255));

ALTER TABLE EWP.EWPCV_IIA_APPROVALS
ADD (LOCAL_MESSAGE BLOB);

ALTER TABLE EWP.EWPCV_IIA_APPROVALS
ADD (REMOTE_MESSAGE BLOB);


/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.03.00', SYSDATE, '45_CAPTURA_IIA_APPROVALS');
COMMIT;