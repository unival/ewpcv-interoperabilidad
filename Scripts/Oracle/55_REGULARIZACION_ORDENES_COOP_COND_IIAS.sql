
ALTER TABLE EWP.EWPCV_COOPCOND_SUBAR_LANSKIL ADD ORDER_INDEX_LANG NUMBER;
ALTER TABLE EWP.EWPCV_COOPCOND_SUBAR ADD ORDER_INDEX_SUB_AR NUMBER;

DECLARE

	v_iia_id 						VARCHAR2(255);
	v_cooperation_condition_id 		VARCHAR2(255);
    
    v_id_subar_lang                 VARCHAR2(255);
    v_id_subAr                      VARCHAR2(255);
    
	v_order_index_lang 				NUMBER;
    v_order_index_sub_ar            NUMBER;
    
	v_hei_to_notify 		        VARCHAR2(255);
    v_notifier_hei                  VARCHAR2(255);

BEGIN

    v_order_index_lang := 1;
    v_order_index_sub_ar := 1;

  -- Se seleccionan todos los IIAS
  FOR iia IN (SELECT * FROM EWPCV_IIA) LOOP
    v_iia_id := iia.id;

    -- Consulta para seleccionar todas las CooperationCondition para este IIA
    FOR coopCondition IN (SELECT * FROM EWPCV_COOPERATION_CONDITION WHERE iia_id = v_iia_id) LOOP
      v_cooperation_condition_id := coopCondition.id;

      -- Consulta para seleccionar todas las CooperationConditionSubjAreaLangSkill para esta CooperationCondition
      FOR subj_area_lang_skill IN (SELECT * FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE cooperation_condition_id = v_cooperation_condition_id order by ID asc) LOOP  
        v_id_subar_lang := subj_area_lang_skill.ID;

        UPDATE EWPCV_COOPCOND_SUBAR_LANSKIL SET ORDER_INDEX_LANG = v_order_index_lang
        WHERE ID = v_id_subar_lang AND ORDER_INDEX_LANG IS NULL;
        
        v_order_index_lang := v_order_index_lang + 1;
      END LOOP;
      
       -- Consulta para seleccionar todas las CooperationConditionSubjArea para esta CooperationCondition
      FOR subj_area IN (SELECT * FROM EWPCV_COOPCOND_SUBAR WHERE cooperation_condition_id = v_cooperation_condition_id order by ID asc) LOOP  
        v_id_subAr := subj_area.ID;

        UPDATE EWPCV_COOPCOND_SUBAR SET ORDER_INDEX_SUB_AR = v_order_index_sub_ar
        WHERE ID = v_id_subAr AND ORDER_INDEX_SUB_AR IS NULL;
        
        v_order_index_sub_ar := v_order_index_sub_ar + 1;
      END LOOP;
      
    END LOOP;
  END LOOP;
  
   -- Se seleccionan todos los IIAS REMOTOS
  FOR iia IN (SELECT * FROM EWPCV_IIA where IS_REMOTE = 1) LOOP
    v_iia_id := iia.id;
    v_hei_to_notify := 'uv.es';                               
    v_notifier_hei := 'unican.es';                                 

    INSERT INTO EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
    VALUES (EWP.GENERATE_UUID(), v_iia_id, LOWER(v_notifier_hei), SYSDATE, 0, 1, 0, LOWER(v_hei_to_notify));

  END LOOP;
  
END;



