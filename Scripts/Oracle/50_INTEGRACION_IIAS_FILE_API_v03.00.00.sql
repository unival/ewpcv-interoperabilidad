/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
--     añadimos una columna a la tabla de iias para guardar el id del pdf
	ALTER TABLE EWP.EWPCV_IIA ADD PDF_ID VARCHAR2(255);

--     tenemos que llevarnos todos los pdfs que tenemos en la tabla de iias a la nueva tabla EWPCV_FILES antes de cambiar la columna
    DECLARE
        CURSOR c_iias IS
            SELECT ID, PDF, FIRST_PARTNER_ID FROM EWP.EWPCV_IIA WHERE PDF IS NOT NULL AND IS_REMOTE = 0;
        v_iia_id VARCHAR2(255);
        v_pdf BLOB;
        v_file_id VARCHAR2(255);
        v_first_partner_id VARCHAR2(255);
        v_schac VARCHAR2(255);
    BEGIN
        FOR iia IN c_iias LOOP
            v_iia_id := iia.ID;
            v_pdf := iia.PDF;
            v_first_partner_id := iia.FIRST_PARTNER_ID;
            v_file_id := EWP.GENERATE_UUID();

            SELECT INSTITUTION_ID INTO v_schac FROM EWP.EWPCV_IIA_PARTNER WHERE ID = v_first_partner_id;

            INSERT INTO EWP.EWPCV_FILES (ID, FILE_ID, FILE_CONTENT, SCHAC, MIME_TYPE) VALUES (EWP.GENERATE_UUID(), v_file_id, v_pdf, v_schac, 'application/pdf');
            UPDATE EWP.EWPCV_IIA SET PDF_ID = v_file_id WHERE ID = v_iia_id;
        END LOOP;
    END;
/
-- ahora que ya tenemos todos los pdfs en la nueva tabla, y referenciados podemos eliminar la columna de la tabla de iias
	ALTER TABLE EWP.EWPCV_IIA DROP COLUMN PDF;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/



/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v03.00.00', SYSDATE, '50_INTEGRACION_IIAS_FILE_API_v03.00.00');
COMMIT;