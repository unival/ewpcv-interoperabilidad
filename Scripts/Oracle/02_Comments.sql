
COMMENT ON COLUMN "EWP"."EWPCV_ACADEMIC_TERM"."TOTAL_TERMS" IS 'Cantidad total de periodos de este tipo en un anio';
COMMENT ON COLUMN "EWP"."EWPCV_ACADEMIC_TERM"."TERM_NUMBER" IS 'Numero de periodo dentro del total de periodos de un anio';

COMMENT ON COLUMN "EWP"."EWPCV_COOPERATION_CONDITION"."OTHER_INFO" IS 'Otra informacion de la condicion de cooperacion';

COMMENT ON COLUMN "EWP"."EWPCV_FACT_SHEET"."DECISION_WEEKS_LIMIT" IS 'Numero maximo de semanas para dar respuesta a las nominaciones de incoming mobilities';
COMMENT ON COLUMN "EWP"."EWPCV_FACT_SHEET"."TOR_WEEKS_LIMIT" IS 'Numero maximo de semanas para dar respuesta a las convalidaciones de incoming mobilities';

COMMENT ON COLUMN "EWP"."EWPCV_IIA"."REMOTE_IIA_ID" IS 'Almacena el id del iia dentro de el sistema del partner';
COMMENT ON COLUMN "EWP"."EWPCV_IIA"."REMOTE_IIA_CODE" IS 'Almacena el Code del iia dentro de el sistema del partner';
COMMENT ON COLUMN "EWP"."EWPCV_IIA"."APPROVAL_DATE" IS 'Fecha de aprovacion del IIAs';
COMMENT ON COLUMN "EWP"."EWPCV_IIA"."REMOTE_COP_COND_HASH" IS 'Almacena el Hash de las condiciones de coperacion enviadas por la otra insitucion';
COMMENT ON COLUMN "EWP"."EWPCV_IIA"."PDF" IS 'Almacena PDF del iia';

COMMENT ON COLUMN "EWP"."EWPCV_IIA_PARTNER"."SIGNER_PERSON_CONTACT_ID" IS 'Referencia a la persona firmante';
COMMENT ON COLUMN "EWP"."EWPCV_IIA_PARTNER"."SIGNING_DATE" IS 'Fecha en que el participante firma el iia';

COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."STUDENT_SIGN" IS 'Referencia a la firma del estudiante';	
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."SENDER_COORDINATOR_SIGN" IS 'Referencia a la firma del coordinador de la entidad emisora';	
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."RECEIVER_COORDINATOR_SIGN" IS 'Referencia a la firma del coordinador de la entidad receptora';		
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."STATUS" IS 'Estado del acuerdo de enseñanaza 0-CHANGES_PROPOSED, 1-FIRST_VERSION, 2-APROVED, 3-REJECTED';
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."MODIFIED_DATE" IS 'Ultima fecha de modificación de la version del acuerdo de aprendizaje';		
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."MODIFIED_STUDENT_CONTACT_ID" IS 'Referencia a los datos del estudiante que han sufrido modificaciones desde la first version';
COMMENT ON COLUMN "EWP"."EWPCV_LEARNING_AGREEMENT"."PDF" IS 'Documento PDF con el acuerdo de aprendizaje';



COMMENT ON COLUMN "EWP"."EWPCV_MOBILITY"."SENDER_CONTACT_ID" IS 'Referencia a la persona de contacto de la institucion origen';
COMMENT ON COLUMN "EWP"."EWPCV_MOBILITY"."SENDER_ADMV_CONTACT_ID" IS 'Referencia a la persona de contacto administrativo de la institucion origen';
COMMENT ON COLUMN "EWP"."EWPCV_MOBILITY"."RECEIVER_CONTACT_ID" IS 'Referencia a la persona de contacto de la institucion destino';	
COMMENT ON COLUMN "EWP"."EWPCV_MOBILITY"."RECEIVER_ADMV_CONTACT_ID" IS 'Referencia a la persona de contacto administrativo de la institucion destino';	
COMMENT ON COLUMN "EWP"."EWPCV_MOBILITY"."REMOTE_MOBILITY_ID" IS 'Identificador de la mobilidad en el entorno de la insititucion remota';	


COMMENT ON COLUMN "EWP"."EWPCV_NOTIFICATION"."TYPE" IS 'Indica el tipo de entidad que se debe actualizar (0-IIA, 1-OMOBILITY, 2-IMOBILITY, 3-TOR, 4-LA...)';
COMMENT ON COLUMN "EWP"."EWPCV_NOTIFICATION"."CNR_TYPE" IS 'Indica si hemos recibido la notificacion(0-REFRESH) o la debemos enviar(1-NOTIFY)';
COMMENT ON COLUMN "EWP"."EWPCV_NOTIFICATION"."RETRIES" IS 'Cantidad de intentos de procesar la notificacion';
COMMENT ON COLUMN "EWP"."EWPCV_NOTIFICATION"."PROCESSING" IS 'Flag de reserva del registro para procesarse por los batch';
COMMENT ON COLUMN "EWP"."EWPCV_NOTIFICATION"."OWNER_HEI" IS 'Indica el propietario de la notificacion es decir la universidad de nuestro host involucrada en la relacion';


COMMENT ON COLUMN "EWP"."EWPCV_LA_COMPONENT"."LA_COMPONENT_TYPE" IS 'Types are:0- STUDIED_COMPONENT, 1- RECOGNIZED_COMPONENT, 2- VIRTUAL_COMPONENT, 3- BLENDED_COMPONENT, 4- SHORT_TER_DOCTORAL_COMPONENT';

COMMENT ON TABLE  "EWP"."EWPCV_SIGNATURE" IS 'Tabla para almacenar firmas de las partes involucradas en los MOBILITY y LEARNING AGREEMENT';
