
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
	ALTER TABLE EWP.EWPCV_EVENT ADD REQUEST_BODY BLOB;
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
	*/
	CREATE OR REPLACE FORCE EDITIONABLE VIEW "EWP"."EVENT_VIEW" ("ID", "EVENT_DATE", "TRIGGERING_HEI", "OWNER_HEI", "EVENT_TYPE", 
	"CHANGED_ELEMENT_ID", "ELEMENT_TYPE", "OBSERVATIONS", "STATUS", "REQUEST_PARAMS", "MESSAGE", "X_REQUEST_ID","REQUEST_BODY") 
	AS SELECT 

	ID					AS ID,
	EVENT_DATE			AS EVENT_DATE,
	TRIGGERING_HEI		AS TRIGGERING_HEI,
	OWNER_HEI			AS OWNER_HEI,
	EVENT_TYPE			AS EVENT_TYPE,
	CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
	ELEMENT_TYPE		AS ELEMENT_TYPE,
	OBSERVATIONS		AS OBSERVATIONS,
	STATUS				AS STATUS,
	REQUEST_PARAMS		AS REQUEST_PARAMS,
	MESSAGE				AS MESSAGE,
	X_REQUEST_ID		AS X_REQUEST_ID,
	REQUEST_BODY 		AS REQUEST_BODY 
	
	FROM EWP.EWPCV_EVENT;

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (EWP.SEC_EWPCV_SCRIPT_VERSION.NEXTVAL, 'v02.00.00', SYSDATE, '35_UPGRADE_FEATURE_Mejoras_auditoria_cnr');
COMMIT;
    
    
    
    
    
    
    
    
    
