DELETE FROM EWP.dbo.EWPCV_ACADEMIC_TERM_NAME                      ;
DELETE FROM EWP.dbo.EWPCV_BLENDED_LA_COMPONENT                    ;
DELETE FROM EWP.dbo.EWPCV_CONTACT_DESCRIPTION                     ;
DELETE FROM EWP.dbo.EWPCV_CONTACT_DETAILS_EMAIL                   ;
DELETE FROM EWP.dbo.EWPCV_CONTACT_NAME                            ;
DELETE FROM EWP.dbo.EWPCV_CONTACT_URL                             ;
DELETE FROM EWP.dbo.EWPCV_COOPCOND_SUBAR_LANSKIL	              ;
DELETE FROM EWP.dbo.EWPCV_COOPCOND_EQFLVL	             		  ;
DELETE FROM dbo.EWPCV_COOPCOND_SUBAR							  ;
DELETE FROM EWP.dbo.EWPCV_COOPERATION_CONDITION                   ;
DELETE FROM EWP.dbo.EWPCV_IIA_APPROVALS							  ;
DELETE FROM EWP.dbo.EWPCV_DISTR_DESCRIPTION                       ;
DELETE FROM EWP.dbo.EWPCV_DOCTORAL_LA_COMPONENT                   ;
DELETE FROM EWP.dbo.EWPCV_DURATION                                ;
DELETE FROM EWP.dbo.EWPCV_FACT_SHEET_URL                          ;
DELETE FROM EWP.dbo.EWPCV_FLEXIBLE_ADDRESS_LINE            		  ;
DELETE FROM EWP.dbo.EWPCV_FLEXAD_DELIV_POINT_COD				  ;
DELETE FROM EWP.dbo.EWPCV_FLEXAD_RECIPIENT_NAME			          ;
DELETE FROM EWP.dbo.EWPCV_GRADING_SCHEME_DESC                     ;
DELETE FROM EWP.dbo.EWPCV_GRADING_SCHEME_LABEL                    ;
DELETE FROM EWP.dbo.EWPCV_IIA                                     ;
DELETE FROM EWP.dbo.EWPCV_IIA_PARTNER_CONTACTS                    ;
DELETE FROM EWP.dbo.EWPCV_INS_REQUIREMENTS                        ;
DELETE FROM EWP.dbo.EWPCV_INST_INF_ITEM                           ;
DELETE FROM EWP.dbo.EWPCV_INST_ORG_UNIT                           ;
DELETE FROM EWP.dbo.EWPCV_INSTITUTION_NAME                        ;
DELETE FROM EWP.dbo.EWPCV_LOI_CREDITS                             ;
DELETE FROM EWP.dbo.EWPCV_LOS_DESCRIPTION                         ;
DELETE FROM EWP.dbo.EWPCV_LOS_LOI                                 ;
DELETE FROM EWP.dbo.EWPCV_LOS_LOS                                 ;
DELETE FROM EWP.dbo.EWPCV_LOS_NAME                                ;
DELETE FROM EWP.dbo.EWPCV_LOS_URLS                                ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_LA                             ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_LANG_SKILL					  ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_NUMBER                         ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_UPDATE_REQUEST                 ;
DELETE FROM EWP.dbo.EWPCV_NOTIFICATION                            ;
DELETE FROM EWP.dbo.EWPCV_ORGANIZATION_UNIT_NAME                  ;
DELETE FROM EWP.dbo.EWPCV_OU_INF_ITEM                             ;
DELETE FROM EWP.dbo.EWPCV_OU_REQUIREMENT_INFO                     ;
DELETE FROM EWP.dbo.EWPCV_RECOGNIZED_LA_COMPONENT                 ;
DELETE FROM EWP.dbo.EWPCV_REQUIREMENTS_INFO                       ;
DELETE FROM EWP.dbo.EWPCV_RESULT_DIST_CATEGORY		              ;
DELETE FROM EWP.dbo.EWPCV_STUDIED_LA_COMPONENT                    ;
DELETE FROM EWP.dbo.EWPCV_VIRTUAL_LA_COMPONENT                    ;
DELETE FROM EWP.dbo.EWPCV_COMPONENT_CREDITS                       ;
DELETE FROM EWP.dbo.EWPCV_CREDIT                                  ;
DELETE FROM EWP.dbo.EWPCV_IIA_PARTNER                             ;
DELETE FROM EWP.dbo.EWPCV_INFORMATION_ITEM                        ;
DELETE FROM EWP.dbo.EWPCV_INSTITUTION                             ;
DELETE FROM EWP.dbo.EWPCV_LA_COMPONENT                            ;
DELETE FROM EWP.dbo.EWPCV_LEARNING_AGREEMENT                      ;
DELETE FROM EWP.dbo.EWPCV_LEVEL_DESCRIPTION;
DELETE FROM EWP.dbo.EWPCV_LEVELS;
DELETE FROM EWP.dbo.EWPCV_LEVEL;
DELETE FROM EWP.dbo.EWPCV_LOI_GROUPING;
DELETE FROM EWP.dbo.EWPCV_GROUP;
DELETE FROM EWP.dbo.EWPCV_GROUP_TYPE;
DELETE FROM EWP.dbo.EWPCV_ATTACHMENT_CONTENT;
DELETE FROM EWP.dbo.EWPCV_ATTACHMENT_TITLE;
DELETE FROM EWP.dbo.EWPCV_ATTACHMENT_DESCRIPTION;
DELETE FROM EWP.dbo.EWPCV_ADDITIONAL_INFO;
DELETE FROM EWP.dbo.EWPCV_DIPLOMA_SECTION_SECTION;
DELETE FROM EWP.dbo.EWPCV_DIPLOMA_SEC_ATTACH;
DELETE FROM EWP.dbo.EWPCV_DIPLOMA_SECTION;
DELETE FROM EWP.dbo.EWPCV_LOI_ATT;
DELETE FROM EWP.dbo.EWPCV_REPORT_ATT;
DELETE FROM EWP.dbo.EWPCV_TOR_ATTACHMENT;
DELETE FROM EWP.dbo.EWPCV_LOI                                     ;
DELETE FROM EWP.dbo.EWPCV_DIPLOMA;
DELETE FROM EWP.dbo.EWPCV_GRADE_FREQUENCY;
DELETE FROM EWP.dbo.EWPCV_ISCED_TABLE;
DELETE FROM EWP.dbo.EWPCV_ATTACHMENT;
DELETE FROM EWP.dbo.EWPCV_REPORT;
DELETE FROM EWP.dbo.EWPCV_TOR;
DELETE FROM EWP.dbo.EWPCV_LOS                                     ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY                                ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_PARTICIPANT                    ;
DELETE FROM EWP.dbo.EWPCV_MOBILITY_TYPE                           ;
DELETE FROM EWP.dbo.EWPCV_ORGANIZATION_UNIT                       ;
DELETE FROM EWP.dbo.EWPCV_RESULT_DISTRIBUTION                     ;
DELETE FROM EWP.dbo.EWPCV_SIGNATURE                               ;
DELETE FROM EWP.dbo.EWPCV_SUBJECT_AREA                            ;
DELETE FROM EWP.dbo.EWPCV_CONTACT                                 ;
DELETE FROM EWP.dbo.EWPCV_FACT_SHEET                              ;
DELETE FROM EWP.dbo.EWPCV_GRADING_SCHEME                          ;
DELETE FROM EWP.dbo.EWPCV_LANGUAGE_SKILL                          ;
DELETE FROM EWP.dbo.EWPCV_ACADEMIC_TERM                           ;
DELETE FROM EWP.dbo.EWPCV_ACADEMIC_YEAR                           ;
DELETE FROM EWP.dbo.EWPCV_PHOTO_URL;
DELETE FROM EWP.dbo.EWPCV_CONTACT_DETAILS                         ;
DELETE FROM EWP.dbo.EWPCV_FLEXIBLE_ADDRESS                        ;
DELETE FROM EWP.dbo.EWPCV_PHONE_NUMBER                            ;
DELETE FROM EWP.dbo.EWPCV_PERSON                                  ;
DELETE FROM EWP.dbo.EWPCV_LANGUAGE_ITEM                           ;
DELETE FROM EWP.dbo.EWPCV_STATS_OLA                           	  ;
DELETE FROM EWP.dbo.EWPCV_STATS_ILA                           	  ;
DELETE FROM EWP.dbo.EWPCV_STATS_IIA                           	  ;
DELETE FROM EWP.dbo.EWP_FILES;
--
DELETE FROM EWP.dbo.EWPCV_DICTIONARY                              ;
DELETE FROM EWP.dbo.EWPCV_EVENT									  ;
DELETE FROM EWP.dbo.EWPCV_INSTITUTION_IDENTIFIERS				  ;
DELETE FROM EWP.dbo.EWPCV_SCRIPT_VERSIONS                         ;
DELETE FROM EWP.dbo.EWPCV_QRTZ_FIRED_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_PAUSED_TRIGGER_GRPS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_SCHEDULER_STATE;
DELETE FROM EWP.dbo.EWPCV_QRTZ_LOCKS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_SIMPLE_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_SIMPROP_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_CRON_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_BLOB_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_TRIGGERS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_JOB_DETAILS;
DELETE FROM EWP.dbo.EWPCV_QRTZ_CALENDARS;
DELETE FROM EWP.dbo.EWPCV_AUDIT_CONFIG;

