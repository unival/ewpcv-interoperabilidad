
/*
--------------------------------------------
-- Esqueletos de procedures para EWP - SQL SERVER
-------------------------------------------
*/


	-- FACTSHEET
	-------------------------------------------

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION INSERT_FACTSHEET(P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.INSERT_FACTSHEET(@P_FACTSHEET XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION UPDATE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_FACTSHEET IN FACTSHEET_INSTITUTION, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.UPDATE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_FACTSHEET XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO
	
	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION DELETE_FACTSHEET(P_INSTITUTION_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
	
	CREATE PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO



	-- IIAS
	-------------------------------------------
		
	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION INSERT_IIA(P_IIA IN IIA, P_IIA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 
	
	CREATE PROCEDURE dbo.INSERT_IIA(@P_IIA XML, @P_IIA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @P_IIA_ID = '0C0AE0BD-EE72-43DE-B4E5-1F8272B01BCF'
		SET @return_value = 0
	END
	;	
	GO
	
	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION UPDATE_IIA(P_IIA_ID IN VARCHAR2, P_IIA IN IIA, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.UPDATE_IIA(@P_IIA_ID uniqueidentifier, @P_IIA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO


	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION DELETE_IIA(P_IIA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.DELETE_IIA(@P_IIA_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO
	
	
	
	-- MOBILITY_LA
	-------------------------------------------
  
                       
	/* Inserta una movilidad en el sistema, habitualmente se emplearÃ¡ para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION INSERT_MOBILITY(P_MOBILITY IN MOBILITY, P_OMOBILITY_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2)  RETURN NUMBER; 
	CREATE PROCEDURE dbo.INSERT_MOBILITY(@P_MOBILITY xml, @P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @P_OMOBILITY_ID = '0C0AE0BD-EE72-43DE-B4E5-1F8272B01BCF'
		SET @return_value = 0
	END
	;	
	GO
		
	

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION UPDATE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_MOBILITY IN MOBILITY, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER; 
	
	CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO
		
	
	
	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION DELETE_MOBILITY(P_OMOBILITY_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2)  RETURN NUMBER;  	

	CREATE PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO
		

	
	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estarÃ¡ registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/
	--FUNCTION INSERT_LEARNING_AGREEMENT(P_LA IN LEARNING_AGREEMENT, P_LA_ID OUT VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.INSERT_LEARNING_AGREEMENT(@P_LA XML, @P_LA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @P_LA_ID = '0C0AE0BD-EE72-43DE-B4E5-1F8272B01BCF'
		SET @return_value = 0
	END
	;	
	GO
	


	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar Ãºnicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION UPDATE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA IN LEARNING_AGREEMENT, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER;  

	CREATE PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_LA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO



	/* Aprueba una revision de un learning agreement, se emplearÃ¡ para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION ACCEPT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID uniqueidentifier, @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO



	/* Rechaza una revision de un learning agreement, se emplearÃ¡ para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION REJECT_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_LA_REVISION IN NUMBER, P_SIGNATURE IN SIGNATURE, P_OBSERVATION IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2) RETURN NUMBER; 
  
	CREATE PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID uniqueidentifier, @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO


	/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	--FUNCTION DELETE_LEARNING_AGREEMENT(P_LA_ID IN VARCHAR2, P_ERROR_MESSAGE OUT VARCHAR2, P_HEI_TO_NOTIFY IN VARCHAR2) RETURN NUMBER; 

	CREATE PROCEDURE dbo.DELETE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
	BEGIN
		SET @return_value = 0
	END
	;	
	GO
	
	
	--Vistas de Aprovisionamiento
	
--EWP.DICTIONARIES_VIEW

CREATE   VIEW [dbo].[DICTIONARIES_VIEW]
(
 ID,CODE,DESCRIPTION,DICTIONARY_TYPE,DICTIONARY_YEAR
) as
select
 '8adb90bf79406e1f017940700cea052a','022','Humanities (except languages)','EDUCATION_FIELDS','2020'
union select
'8adb90bf79406e1f017940700d13052b','0716','Motor vehicles, ships and aircraft','EDUCATION_FIELDS','2020'
union select 
'8adb90bf79406e1f017940700d45052c','059','Natural sciences, mathematics and statistics not elsewhere classified','EDUCATION_FIELDS','2020'
GO






--EWP.EVENT_VIEW

CREATE   VIEW [dbo].[EVENT_VIEW] 
(
    id,
    event_date,
    triggering_hei,
    owner_hei,
    event_type,
    changed_element_id,
    element_type,
    observations
) as
select
	'386dfada-4823-4526-b605-27249448c22c',
    cast('17/06/2021 12:07:00' as datetime),
    'UM.ES',
    'UV.ES',
    'INSERT',
    'C4F451BF-C621-70CC-E053-1FC616ACB616',
    '5',
    NULL
	union select
	 '78f4e45f-9b83-4def-936f-adc8a1ca1a08',
    cast('17/06/2021 12:04:00' as datetime),
    'UV.ES',
    'UM.ES',
    'NOTIFY',
    'C4F40C34-F571-6FEC-E053-1FC616ACFAF5',
    '5',
    NULL
GO





--EWP.FACTSHEET_ADD_INFO_VIEW

CREATE   VIEW [dbo].[FACTSHEET_ADD_INFO_VIEW]
(
    institution_id,
    info_type,
    email,
    phone,
    urls
) as
select
    'UV.ES',
    'PRUEBAS',
    'd@d.com',
    '+34999999999|:||:|',
    'd.com|:|ES'
	
GO






--EWP.FACTSHEET_REQ_INFO_VIEW

CREATE   VIEW [dbo].[FACTSHEET_REQ_INFO_VIEW]
(
    institution_id,
    req_type,
    name,
    description,
    email,
    phone,
    urls
) as
select
 'UV.ES',
    'INFRASTRUCTURE',
    'CASA ACOGIDA ESPECIAL',
    'CASA BONITA PARA PERSONAS CON DISCAPACIDAD',
    'E@E.com',
    '+34000000000|:||:|',
    'E.com|:|ES'
	
GO






--EWP.EWPCV_FACTSHEET_VIEW

CREATE   VIEW [dbo].[FACTSHEET_VIEW]
(
    institution_id,
    abreviation,
    decision_week_limit,
    tor_week_limit,
    nominations_autum_term,
    nominations_spring_term,
    application_autum_term,
    application_spring_term,
    application_email,
    application_phone,
    application_urls,
    housing_email,
    housing_phone,
    housing_urls,
    visa_email,
    visa_phone,
    visa_urls,
    insurance_email,
    insurance_phone,
    insurance_urls
) as
select
'UV.ES',
    'universidad valencia',
    '3',
    '3',
    cast('17/06/2021 18:00:05' as datetime),
    cast('17/06/2021 18:00:05' as datetime),
    cast('17/06/2021 18:00:05' as datetime),
    cast('17/06/2021 18:00:05' as datetime),
    'a@a.com',
    '+34666666666|:||:|',
    'a.com|:|ES',
    'b@b.com',
    '+34777777777|:||:|',
    'b.com|:|ES',
    'c@c.com',
    '+34888888888|:||:|',
    'd.com|:|ES',
    'd@d.com',
    '+34999999999|:||:|',
    'd.com|:|ES'
GO







--EWP.IIA_COOP_CONDITIONS_VIEW

CREATE   VIEW [dbo].[IIA_COOP_CONDITIONS_VIEW]
(
    cooperation_condition_id,
    iia_id,
    iia_code,
    remote_iia_id,
    remote_iia_code,
    iia_start_date,
    iia_end_date,
    iia_modify_date,
    iia_approval_date,
    coop_cond_start_date,
    coop_cond_end_date,
    coop_cond_eqf_level,
    coop_cond_duration,
    coop_cond_participants,
    coop_cond_other_info,
    mobility_type,
    subject_areas,
    receiving_institution,
    receiving_institution_names,
    receiving_ounit_code,
    receiving_ounit_names,
    receiver_signing_date,
    receiver_signer_name,
    receiver_signer_last_name,
    receiver_signer_birth_date,
    receiver_signer_gender,
    receiver_signer_citizenship,
    receiver_signer_contact_names,
    receiver_signer_contact_descs,
    receiver_signer_contact_urls,
    receiver_signer_emails,
    receiver_signer_phones,
    receiver_signer_address_lines,
    receiver_signer_address,
    receiver_signer_postal_code,
    receiver_signer_locality,
    receiver_signer_region,
    receiver_signer_country,
    sending_institution,
    sending_institution_names,
    sending_ounit_code,
    sending_ounit_names,
    sender_signing_date,
    sender_signer_name,
    sender_signer_last_name,
    sender_signer_birth_date,
    sender_signer_gender,
    sender_signer_citizenship,
    sender_signer_contact_names,
    sender_signer_contact_descs,
    sender_signer_contact_urls,
    sender_signer_emails,
    sender_signer_phones,
    sender_signer_address_lines,
    sender_signer_address,
    sender_signer_postal_code,
    sender_signer_locality,
    sender_signer_region,
    sender_signer_country

) as
select
'C4F94C1F-2E5D-7EBB-E053-1FC616AC548D',
    'C4F94C1F-2E45-7EBB-E053-1FC616AC548D',
    'IIA_EWP',
    NULL,
    NULL,
    cast('09/07/2021 00:00:00' as datetime),
    cast('09/07/2022 00:00:00' as datetime),
    cast('17/06/2021 17:59:51' as datetime),
    NULL,
    cast('09/07/2021 00:00:00' as datetime),
    cast('09/07/2021 00:00:00' as datetime),
    '1',
    '1|:|3',
    '1',
    NULL,
    'STUDIES|:|STUDENT',
    '06|:|Information and Communication Technologies (ICTs)|:|es|:|B2',
    'UM.ES',
    'universidad de Murcia|:|ES',
    'OunitCodeUM',
    'Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES'
,
    cast('09/07/2021 00:00:00' as datetime),
    'Coordinador receiver',
    'Receiver Institution',
    cast('01/01/1960 00:00:00' as datetime),
    '0',
    'ES',
    'Contacto r i c|:|ES',
    'Contacto del coordinador de la institucion receptora|:|ES',
    'https://www.um.es/|:|ES',
    NULL,
    '+34 111111111|:||:|',
    'Paseo del Teniente Flomesta, 3',
    '|:||:||:||:||:||:|',
    '30003',
    'Murcia',
    'Region de murcia',
    'ES',
    'UV.ES',
    'University of Valencia|:|EN|;|Universidad de Valencia|:|ES',
    'OunitCodeUV',
    'Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES'
,
    cast('09/07/2021 00:00:00' as datetime),
    'Coordinador sender',
    'Sender Institution',
    cast('01/01/1960 00:00:00' as datetime),
    '0',
    'ES',
    'Contacto s i c|:|ES',
    'Contacto del coordinador de la institucion emisora|:|ES',
    'https://www.uv.es/|:|ES',
    NULL,
    '+34 222222222|:||:|',
    'Av Blasco Ibañez, 13',
    '|:||:||:||:||:||:|',
    '460010',
    'Valencia',
    'Comunidad Valenciana',
    'ES'

GO







--EWP.IIAS_VIEW

CREATE   VIEW [dbo].[IIAS_VIEW]
(
   iia_id,
    iia_code,
    remote_iia_id,
    remote_iia_code,
    iia_start_date,
    iia_end_date,
    iia_modify_date,
    iia_approval_date,
    remote_hash,
    approval_hash,
    is_remote

) as
select
'C4F94C1F-2E45-7EBB-E053-1FC616AC548D',
    'IIA_EWP',
    NULL,
    NULL,
    cast('09/07/2021 00:00:00' as datetime),
    cast('09/07/2022 00:00:00' as datetime),
    cast('17/06/2021 17:59:51' as datetime),
    NULL,
    NULL,
    NULL,
    '0'

GO







--EWP.LA_COMPONENTS_VIEW

CREATE   VIEW [dbo].[LA_COMPONENTS_VIEW]
(
    la_id,
    la_revision,
    component_type,
    title,
    description,
    credits,
    academic_term,
    academic_term_names,
    academic_year,
    status,
    reason_code,
    reason_text,
    recognition_conditions,
    los_code,
    institution_id,
    receiving_institution_names,
    ounit_code,
    receiving_ounit_names,
    los_names,
    los_descriptions,
    los_url,
    engagement_hours,
    language_of_instruction
) as
select
'5a10eb2c-be18-4191-8746-5a1ed6a4415f',
    '1',
    '0',
    'MATEMATICAS',
    NULL,
    '|:|ECTS|:|6',
    '|:||:||:||:|1|:|2',
    NULL,
    '|:|',
    '1',
    '0',
    NULL,
    NULL,
    'Mates 101',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL

union select 
'C4F40C34-F576-6FEC-E053-1FC616ACFAF5','1','0','MATEMATICAS DISPONIBLES',null,'|:|ECTS|:|6','2021.06.17|:|2021.06.17|:|UV.ES|:|OunitCodeUV|:|1|:|2','Primer semestre|:|ES','2021|:|2020','0','3',null,null,'Mates 202',null,null,null,null,null,null,null,null,null
GO







--EWP.LEARNING_AGREEMENT_VIEW

CREATE   VIEW [dbo].[LEARNING_AGREEMENT_VIEW]
(
   mobility_id,
    mobility_revision,
    la_id,
    la_revision,
    la_status,
    la_changes_id,
    comment_reject,
    student_sign,
    sender_sign,
    receiver_sign,
    modified_date,
    student_global_id,
    student_name,
    student_last_name,
    student_birth_date,
    student_gender,
    student_citizenship,
    student_contact_names,
    student_contact_descriptions,
    student_contact_urls,
    student_emails,
    student_phone,
    student_address_lines,
    student_address,
    student_postal_code,
    student_locality,
    student_region,
    student_country

) as
select
 'C4F40C34-F571-6FEC-E053-1FC616ACFAF5',
    '0',
    'C4F40C34-F576-6FEC-E053-1FC616ACFAF5',
    '0',
    '3',
    'C4F40C34-F576-6FEC-E053-1FC616ACFAF5-0',
    'te va a romper',
    'Luis|:|student|:| a@a.com|:|17/06/2021 12:03:23|:|',
    'PEPE|:|coordinator|:| a@a.com|:|17/06/2021 12:03:23|:|',
    'mr receiver|:|coordinator|:| c@c.com|:|17/06/2021 12:11:05|:|',
    cast('17/06/2021 12:03:23' as datetime),
    'ID_LUIS',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL

GO








--EWP.MOBILITY_VIEW

CREATE   VIEW [dbo].[MOBILITY_VIEW]
(
    mobility_id,
    mobility_revision,
    planned_arrival_date,
    actual_arrival_date,
    planned_departure_date,
    actual_departure_date,
    eqf_level,
    iia_code,
    cooperation_condition_id,
    subject_area,
    mobility_status,
    mobility_type,
    language_skill,
    student_global_id,
    student_name,
    student_last_name,
    student_birth_date,
    student_gender,
    student_citizenship,
    student_contact_names,
    student_contact_descriptions,
    student_contact_urls,
    student_emails,
    student_phone,
    student_address_lines,
    student_address,
    student_postal_code,
    student_locality,
    student_region,
    student_country,
    receiving_institution,
    receiving_institution_names,
    receiving_ounit_code,
    receiving_ounit_names,
    sending_institution,
    sending_institution_names,
    sending_ounit_code,
    sending_ounit_names,
    sender_contact_name,
    sender_contact_last_name,
    sender_contact_birth_date,
    sender_contact_gender,
    sender_contact_citizenship,
    sender_cont_cont_names,
    sender_cont_cont_descs,
    sender_cont_cont_urls,
    sender_contact_emails,
    sender_contact_phones,
    sender_cont_addr_lines,
    sender_contact_address,
    sender_contact_postal_code,
    sender_contact_locality,
    sender_contact_region,
    sender_contact_country,
    admv_sen_contact_name,
    admv_sen_contact_last_name,
    admv_sen_contact_birth_date,
    admv_sen_contact_gender,
    admv_sen_contact_citizenship,
    admv_sen_cont_cont_names,
    admv_sen_cont_cont_descs,
    admv_sen_cont_cont_urls,
    admv_sen_contact_emails,
    admv_sen_contact_phones,
    admv_sen_cont_addr_lines,
    admv_sen_contact_address,
    admv_sen_contact_postal_code,
    admv_sen_contact_locality,
    admv_sen_contact_region,
    admv_sen_contact_country,
    receiver_contact_name,
    receiver_contact_last_name,
    receiver_contact_birth_date,
    receiver_contact_gender,
    receiver_contact_citizenship,
    receiver_cont_cont_names,
    receiver_cont_cont_descs,
    receiver_cont_cont_urls,
    receiver_contact_emails,
    receiver_contact_phones,
    receiver_cont_addr_lines,
    receiver_contact_address,
    receiver_contact_postal_code,
    receiver_contact_locality,
    receiver_contact_region,
    receiver_contact_country,
    admv_rec_contact_name,
    admv_rec_contact_last_name,
    admv_rec_contact_birth_date,
    admv_rec_contact_gender,
    admv_rec_contact_citizenship,
    admv_rec_cont_cont_names,
    admv_rec_cont_cont_descs,
    admv_rec_cont_cont_urls,
    admv_rec_contact_emails,
    admv_rec_contact_phones,
    admv_rec_cont_addr_lines,
    admv_rec_contact_address,
    admv_rec_contact_postal_code,
    admv_rec_contact_locality,
    admv_rec_contact_region,
    admv_rec_contact_country

) as
select
 'C4F40C34-F571-6FEC-E053-1FC616ACFAF5',
    '0',
    cast('17/06/2021 12:03:15' as datetime),
    NULL,
    cast('17/06/2021 12:03:15' as datetime),
    NULL,
    '0',
    NULL,
    '1',
    '06|:|Information and Communication Technologies (ICTs)',
    '0',
    'Semester|:|MOBILITY_LA',
    'en|:|B2|;|es|:|C2',
    'ID_LUIS',
    'Luis',
    'Sanchez Perez',
    cast('21/02/1993 00:00:00' as datetime),
    '1',
    'ES',
    'Mi contacto|:|ES',
    'Contacto de Luis|:|ES',
    NULL,
    'lsanchezpe@minsait.com',
    '+34 666666666|:||:|',
    'Avda. Jean Claude Combaldieu, s/n',
    '|:||:||:||:||:||:|',
    '03008',
    'Alicante',
    'Comunidad Valenciana',
    'ES',
    'UM.ES',
    'universidad de Murcia|:|ES',
    'OunitCodeUM',
    'Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES|;|Departamento de prueba murcia|:|ES'
,
    'UV.ES',
    'University of Valencia|:|EN|;|Universidad de Valencia|:|ES',
    'OunitCodeUV',
    'Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES|;|Departamento de prueba|:|ES'
,
    'Coordinador',
    'Sender Institution',
    cast('01/01/1960 00:00:00' as datetime),
    '0',
    'ES',
    'Contacto s i c|:|ES',
    'Contacto del coordinador de la institucion emisora|:|ES',
    'https://www.uv.es/|:|ES',
    'sender_coordinator@email.com',
    '+34 111111111|:||:|',
    'Av Blasco IbaÃ±ez, 13',
    '|:||:||:||:||:||:|',
    '460010',
    'Valencia',
    'Comunidad Valenciana',
    'ES',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    'Coordinador',
    'Receiver Institution',
    cast('01/01/1960 00:00:00' as datetime),
    '0',
    'ES',
    'Contacto r i c|:|ES',
    'Contacto del coordinador de la institucion receptora|:|ES',
    'https://www.um.es/|:|ES',
    'sender_coordinator@email.com',
    '+34 222222222|:||:|',
    'Av Blasco IbaÃ±ez, 13',
    '|:||:||:||:||:||:|',
    '30003',
    'Murcia',
    'Region de murcia',
    'ES',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL

GO

	



