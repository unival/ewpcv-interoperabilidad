create sequence SEC_EWPCV_NOTIFICACTION start with 1 increment by  1;

    create table ewp.dbo.EWPCV_ACADEMIC_TERM_NAME (
       ACADEMIC_TERM_ID varchar(255) not null,
        DISP_NAME_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_ACADEMIC_TERM (
       ID varchar(255) not null,
        VERSION bigint,
        END_DATE date,
        INSTITUTION_ID varchar(255),
        ORGANIZATION_UNIT_ID varchar(255),
        START_DATE date,
        ACADEMIC_YEAR_ID varchar(255),
		TOTAL_TERMS BIGINT,
		TERM_NUMBER  BIGINT,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_ACADEMIC_YEAR (
       ID varchar(255) not null,
        VERSION bigint,
        END_YEAR varchar(255),
        START_YEAR varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_CONTACT (
       ID varchar(255) not null,
        VERSION bigint,
        INSTITUTION_ID varchar(255),
        ORGANIZATION_UNIT_ID varchar(255),
        CONTACT_ROLE varchar(255),
        CONTACT_DETAILS_ID varchar(255),
        PERSON_ID varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_CONTACT_DESCRIPTION (
       CONTACT_ID varchar(255) not null,
        DESCRIPTION_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_CONTACT_NAME (
       CONTACT_ID varchar(255) not null,
        NAME_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_CONTACT_URL (
       CONTACT_DETAILS_ID varchar(255) not null,
        URL_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_CONTACT_DETAILS (
       ID varchar(255) not null,
        VERSION bigint,
        FAX_NUMBER varchar(255),
        MAILING_ADDRESS varchar(255),
        PHONE_NUMBER varchar(255),
        STREET_ADDRESS varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_CONTACT_DETAILS_EMAIL (
       CONTACT_DETAILS_ID varchar(255) not null,
        EMAIL varchar(255)
    );

    create table ewp.dbo.EWPCV_COOPERATION_CONDITION (
       ID varchar(255) not null,
        VERSION bigint,
        END_DATE date,
        EQF_LEVEL smallint not null,
        START_DATE date,
        DURATION_ID varchar(255),
        MOBILITY_NUMBER_ID varchar(255),
        MOBILITY_TYPE_ID varchar(255),
        RECEIVING_PARTNER_ID varchar(255),
        SENDING_PARTNER_ID varchar(255),
        IIA_ID varchar(255),
		OTHER_INFO VARCHAR(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_CREDIT (
       ID varchar(255) not null,
        VERSION bigint,
        CREDIT_LEVEL varchar(255),
        SCHEME varchar(255),
        CREDIT_VALUE decimal(5,1),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_DISTR_CATEGORIES (
       RESULT_DISTRIBUTION_ID varchar(255) not null,
        RESULT_DIST_CATEGORY_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_DISTR_DESCRIPTION (
       RESULT_DISTRIBUTION_ID varchar(255) not null,
        DESCRIPTION_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_DURATION (
       ID varchar(255) not null,
        VERSION bigint,
        NUMBERDURATION decimal(5,1),
        UNIT varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_FACT_SHEET_URL (
       FACT_SHEET_ID varchar(255) not null,
        URL_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_FACT_SHEET (
       ID varchar(255) not null,
        VERSION bigint,
        CONTACT_DETAILS_ID varchar(255),
		DECISION_WEEKS_LIMIT BIGINT,
		TOR_WEEKS_LIMIT BIGINT,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_FLEXIBLE_ADDRESS (
       ID varchar(255) not null,
        VERSION bigint,
        BUILDING_NAME varchar(255),
        BUILDING_NUMBER varchar(255),
        COUNTRY varchar(255),
        "FLOOR" varchar(255),
        LOCALITY varchar(255),
        POST_OFFICE_BOX varchar(255),
        POSTAL_CODE varchar(255),
        REGION varchar(255),
        STREET_NAME varchar(255),
        UNIT varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_FLEXIBLE_ADDRESS_LINE (
       FLEXIBLE_ADDRESS_ID varchar(255) not null,
        ADDRESS_LINE varchar(255)
    );

    create table ewp.dbo.EWPCV_FLEXAD_DELIV_POINT_COD (
       FLEXIBLE_ADDRESS_ID varchar(255) not null,
        DELIVERY_POINT_CODE varchar(255)
    );

    create table ewp.dbo.EWPCV_FLEXAD_RECIPIENT_NAME (
       FLEXIBLE_ADDRESS_ID varchar(255) not null,
        RECIPIENT_NAME varchar(255)
    );

    create table ewp.dbo.EWPCV_GRADING_SCHEME_DESC (
       GRADING_SCHEME_ID varchar(255) not null,
        DESCRIPTION_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_GRADING_SCHEME_LABEL (
       GRADING_SCHEME_ID varchar(255) not null,
        LABEL_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_GRADING_SCHEME (
       ID varchar(255) not null,
        VERSION bigint,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_IIA (
       ID varchar(255) not null,
        VERSION bigint,
        END_DATE date,
        IIA_CODE varchar(255),
        MODIFY_DATE date,
        START_DATE date,
		REMOTE_IIA_ID VARCHAR(255),
		REMOTE_IIA_CODE VARCHAR(255),
		REMOTE_COP_COND_HASH VARCHAR(255),
		APPROVAL_DATE DATE,
		APPROVAL_COP_COND_HASH VARCHAR(255),
		PDF VARCHAR(max),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_IIA_PARTNER_CONTACTS (
       IIA_PARTNER_ID varchar(255) not null,
        CONTACTS_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_IIA_PARTNER (
       ID varchar(255) not null,
        VERSION bigint,
        INSTITUTION_ID varchar(255),
        ORGANIZATION_UNIT_ID varchar(255),
		SIGNER_PERSON_CONTACT_ID VARCHAR(255),
		SIGNING_DATE DATE,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_INST_ORG_UNIT (
       INSTITUTION_ID varchar(255) not null,
        ORGANIZATION_UNITS_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_INSTITUTION (
       ID varchar(255) not null,
        VERSION bigint,
        ABBREVIATION varchar(255),
        INSTITUTION_ID varchar(255),
        LOGO_URL varchar(255),
        FACT_SHEET varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_INSTITUTION_NAME (
       INSTITUTION_ID varchar(255) not null,
        NAME_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LANGUAGE_ITEM (
       ID varchar(255) not null,
        VERSION bigint,
        LANG varchar(255),
        TEXT varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_LEARNING_AGREEMENT (
       ID varchar(255) not null,
        VERSION bigint,
        LEARNING_AGREEMENT_REVISION bigint not null,
		STUDENT_SIGN VARCHAR(255),
		SENDER_COORDINATOR_SIGN VARCHAR(255),
		RECEIVER_COORDINATOR_SIGN VARCHAR(255),
		STATUS BIGINT NOT NULL,
		MODIFIED_DATE DATE,
		MODIFIED_STUDENT_CONTACT_ID VARCHAR(255),
		PDF VARCHAR(max),
        primary key (ID, LEARNING_AGREEMENT_REVISION)
    );

    create table ewp.dbo.EWPCV_LOI (
       ID varchar(255) not null,
        VERSION bigint,
        ENGAGEMENT_HOURS decimal(19,2),
        LANGUAGE_OF_INSTRUCTION varchar(255),
        ACADEMIC_TERM_ID varchar(255),
        GRADING_SCHEME_ID varchar(255),
        RESULT_DISTRIBUTION varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_LOI_CREDITS (
       LOI_ID varchar(255) not null,
        CREDITS_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LOS (
       ID varchar(255) not null,
        VERSION bigint,
        EQF_LEVEL smallint not null,
        INSTITUTION_ID varchar(255),
        ISCEDF varchar(255),
        LOS_CODE varchar(255),
        ORGANIZATION_UNIT_ID varchar(255),
        SUBJECT_AREA varchar(255),
        TOP_LEVEL_PARENT smallint not null,
        "TYPE" bigint,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_LOS_DESCRIPTION (
       LOS_ID varchar(255) not null,
        DESCRIPTION_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LOS_LOI (
       LOS_ID varchar(255) not null,
        LOI_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LOS_LOS (
       LOS_ID varchar(255) not null,
       OTHER_LOS_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LOS_NAME (
       LOS_ID varchar(255) not null,
        NAME_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_LOS_URLS (
       LOS_ID varchar(255) not null,
        URL_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_MOBILITY (
       ID varchar(255) not null,
        VERSION bigint,
        ACTUAL_ARRIVAL_DATE date,
        ACTUAL_DEPARTURE_DATE date,
        COOPERATION_CONDITION_ID varchar(255),
        EQF_LEVEL smallint not null,
        IIA_ID varchar(255),
        ISCED_CODE varchar(255),
        MOBILITY_PARTICIPANT_ID varchar(255),
        MOBILITY_REVISION bigint not null,
        PLANNED_ARRIVAL_DATE date,
        PLANNED_DEPARTURE_DATE date,
        RECEIVING_INSTITUTION_ID varchar(255),
        RECEIVING_ORGANIZATION_UNIT_ID varchar(255),
        SENDING_INSTITUTION_ID varchar(255),
        SENDING_ORGANIZATION_UNIT_ID varchar(255),
        STATUS bigint,
        MOBILITY_TYPE_ID varchar(255),
		LANGUAGE_SKILL VARCHAR(255),
		SENDER_CONTACT_ID VARCHAR(255),
		SENDER_ADMV_CONTACT_ID VARCHAR(255),
		RECEIVER_CONTACT_ID VARCHAR(255),
		RECEIVER_ADMV_CONTACT_ID VARCHAR(255),
		REMOTE_MOBILITY_ID VARCHAR(255),
        primary key (ID, MOBILITY_REVISION)
    );

    create table ewp.dbo.EWPCV_MOBILITY_NUMBER (
       ID varchar(255) not null,
        VERSION bigint,
        NUMBERMOBILITY bigint not null,
        VARIANT varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_MOBILITY_PARTICIPANT (
       ID varchar(255) not null,
        VERSION bigint,
		CONTACT_ID VARCHAR(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_MOBILITY_TYPE (
       ID varchar(255) not null,
        VERSION bigint,
        MOBILITY_CATEGORY varchar(255),
        MOBILITY_GROUP varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_MOBILITY_UPDATE_REQUEST (
       ID varchar(255) not null,
        VERSION bigint,
        SENDING_HEI_ID varchar(255),
        "TYPE" bigint,
        UPDATE_INFORMATION varchar(max),
        UPDATE_REQUEST_DATE date,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_NOTIFICATION (
       ID varchar(255) not null,
        VERSION bigint,
        CHANGED_ELEMENT_IDS varchar(255),
        HEI_ID varchar(255),
        NOTIFICATION_DATE date,
        "TYPE" bigint,
		CNR_TYPE SMALLINT NOT NULL,
		RETRIES INT DEFAULT 0,
		PROCESSING SMALLINT,
		OWNER_HEI VARCHAR(255) NOT NULL,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_ORG_UNIT_ORG_UNIT (
       ORGANIZATION_UNIT_ID varchar(255) not null,
        ORGANIZATION_UNITS_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_ORGANIZATION_UNIT_NAME (
       ORGANIZATION_UNIT_ID varchar(255) not null,
        NAME_ID varchar(255) not null
    );

    create table ewp.dbo.EWPCV_ORGANIZATION_UNIT (
       ID varchar(255) not null,
        VERSION bigint,
        ABBREVIATION varchar(255),
        LOGO_URL varchar(255),
        ORGANIZATION_UNIT_CODE varchar(255),
        FACT_SHEET varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_PERSON (
       ID varchar(255) not null,
        VERSION bigint,
        BIRTH_DATE date,
        COUNTRY_CODE varchar(255),
        FIRST_NAMES varchar(255),
        GENDER bigint,
        LAST_NAME varchar(255),
        PERSON_ID varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_PHONE_NUMBER (
       ID varchar(255) not null,
        VERSION bigint,
        E164 varchar(255),
        EXTENSION_NUMBER varchar(255),
        OTHER_FORMAT varchar(255),
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_RESULT_DISTRIBUTION (
       ID varchar(255) not null,
        VERSION bigint,
        primary key (ID)
    );

    create table ewp.dbo.EWPCV_RESULT_DIST_CATEGORY (
       ID varchar(255) not null,
        VERSION bigint,
        DISTRUBTION_COUNT decimal(19,2),
        LABEL varchar(255),
        primary key (ID)
    );

	CREATE TABLE  ewp.dbo.EWPCV_LA_COMPONENT(
		ID VARCHAR(255) NOT NULL,
		VERSION BIGINT NULL,
		ACADEMIC_TERM_DISPLAY_NAME VARCHAR(255) NULL,
		LOI_ID VARCHAR(255) NULL,
		LOS_CODE VARCHAR(255) NULL,
		LOS_ID VARCHAR(255) NULL,
		STATUS BIGINT NULL,
		TITLE VARCHAR(255) NULL,
		REASON_CODE BIGINT NULL,
		REASON_TEXT VARCHAR(255) NULL,
		LA_COMPONENT_TYPE BIGINT NULL,
		RECOGNITION_CONDITIONS VARCHAR(255) NULL,
		SHORT_DESCRIPTION VARCHAR(255) NULL,
		primary key (ID)
	);

	CREATE TABLE ewp.dbo.EWPCV_STUDIED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR(255) NOT NULL,
		STUDIED_LA_COMPONENT_ID VARCHAR(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_RECOGNIZED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR(255) NOT NULL,
		RECOGNIZED_LA_COMPONENT_ID VARCHAR(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_VIRTUAL_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR(255) NOT NULL,
		VIRTUAL_LA_COMPONENT_ID VARCHAR(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_BLENDED_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR(255) NOT NULL,
		BLENDED_LA_COMPONENT_ID VARCHAR(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_DOCTORAL_LA_COMPONENT(
		LEARNING_AGREEMENT_ID VARCHAR(255) NOT NULL,
		DOCTORAL_LA_COMPONENTS_ID VARCHAR(255) NOT NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NOT NULL
	);
	
	CREATE TABLE ewp.dbo.EWPCV_SUBJECT_AREA(
		ISCED_CODE VARCHAR(255) NOT NULL,
		ISCED_CLARIFICATION VARCHAR(255) NULL,
		PRIMARY KEY (ISCED_CODE)
	);

	CREATE TABLE ewp.dbo.EWPCV_LANGUAGE_SKILL(
		ID VARCHAR(255) NOT NULL,
		"LANGUAGE" VARCHAR(255) NULL,
		CEFR_LEVEL VARCHAR(255) NULL,
		PRIMARY KEY (ID) 
	);
	
	CREATE TABLE ewp.dbo.EWPCV_COOPCOND_SUBAR_LANSKIL(
		ID VARCHAR(255) NOT NULL,
		COOPERATION_CONDITION_ID VARCHAR(255) NOT NULL,
		ISCED_CODE VARCHAR(255) NOT NULL,
		LANGUAGE_SKILL_ID VARCHAR(255) NULL,
		PRIMARY KEY (ID)
	);
	
	CREATE TABLE ewp.dbo.EWPCV_SIGNATURE(
		ID VARCHAR(255) NOT NULL,
		SIGNER_NAME VARCHAR(255) NULL,
		SIGNER_POSITION VARCHAR(255) NULL,
		SIGNER_EMAIL VARCHAR(255) NULL,
		"TIMESTAMP" DATE NULL,
		SIGNER_APP VARCHAR(255) NULL,
		SIGNATURE VARCHAR(max) NULL,
		PRIMARY KEY (ID)
	);
	
	CREATE TABLE  ewp.dbo.EWPCV_MOBILITY_LA(
		MOBILITY_ID VARCHAR(255) NULL,
		MOBILITY_REVISION BIGINT NULL,
		LEARNING_AGREEMENT_ID VARCHAR(255) NULL,
		LEARNING_AGREEMENT_REVISION BIGINT NULL
	);
	
	CREATE TABLE ewp.dbo.EWPCV_INFORMATION_ITEM(
		ID VARCHAR(255) NOT NULL,
		VERSION BIGINT NULL,
		"TYPE" VARCHAR(500) NULL,
		CONTACT_DETAIL_ID VARCHAR(255) NULL,
		PRIMARY KEY (ID) 
	);

	CREATE TABLE ewp.dbo.EWPCV_INST_INF_ITEM(
		INSTITUTION_ID VARCHAR(255) NOT NULL,
		INFORMATION_ID VARCHAR(255) NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_REQUIREMENTS_INFO(
		ID VARCHAR(255) NOT NULL,
		"TYPE" VARCHAR(255) NULL,
		NAME VARCHAR(255) NULL,
		DESCRIPTION VARCHAR(255) NULL,
		CONTACT_DETAIL_ID VARCHAR(255) NULL,
		PRIMARY KEY (ID) 
	);

	CREATE TABLE ewp.dbo.EWPCV_OU_INF_ITEM(
		ORGANIZATION_UNIT_ID VARCHAR(255) NOT NULL,
		INFORMATION_ID VARCHAR(255) NOT NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_OU_REQUIREMENT_INFO(
		OU_ID VARCHAR(255) NULL,
		REQUERIMENT_ID VARCHAR(255) NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_INS_REQUIREMENTS(
		INSTITUTION_ID VARCHAR(255) NULL,
		REQUIREMENT_ID VARCHAR(255) NULL
	);

	CREATE TABLE ewp.dbo.EWPCV_FACTSHEET_DATES(
		FACTSHEET_ID VARCHAR(255) NULL,
		ACADEMIC_TERM_ID VARCHAR(255) NULL,
		"TYPE" BIGINT NULL
	);

     alter table ewp.dbo.EWPCV_ACADEMIC_TERM_NAME 
       add constraint UK_ACADEMIC_TERM_NAME_1 unique (DISP_NAME_ID);

    alter table ewp.dbo.EWPCV_CONTACT_DESCRIPTION 
       add constraint UK_CONTACT_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table ewp.dbo.EWPCV_CONTACT_NAME 
       add constraint UK_CONTACT_NAME_1 unique (NAME_ID);

    alter table ewp.dbo.EWPCV_CONTACT_URL 
       add constraint UK_CONTACT_URL_1 unique (URL_ID);

    alter table ewp.dbo.EWPCV_DISTR_CATEGORIES 
       add constraint UK_DISTR_CATEGORIES_1 unique (RESULT_DIST_CATEGORY_ID);

    alter table ewp.dbo.EWPCV_DISTR_DESCRIPTION 
       add constraint UK_DISTR_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table ewp.dbo.EWPCV_FACT_SHEET_URL 
       add constraint UK_FACT_SHEET_URL_1 unique (URL_ID);

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_DESC 
       add constraint UK_GRADING_SCHEME_DESC_1 unique (DESCRIPTION_ID);

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_LABEL 
       add constraint UK_GRADING_SCHEME_LABEL_1 unique (LABEL_ID);

    alter table ewp.dbo.EWPCV_INST_ORG_UNIT 
       add constraint UK_INST_ORG_UNIT_1 unique (ORGANIZATION_UNITS_ID);

    alter table ewp.dbo.EWPCV_INSTITUTION_NAME 
       add constraint UK_INSTITUTION_NAME_1 unique (NAME_ID);

    alter table ewp.dbo.EWPCV_LOI_CREDITS 
       add constraint UK_LOI_CREDITS_1 unique (CREDITS_ID);

    alter table ewp.dbo.EWPCV_LOS_DESCRIPTION 
       add constraint UK_LOS_DESCRIPTION_1 unique (DESCRIPTION_ID);

    alter table ewp.dbo.EWPCV_LOS_LOI 
       add constraint UK_LOS_LOI_1 unique (LOI_ID);

    alter table ewp.dbo.EWPCV_LOS_LOS 
       add constraint UK_LOS_LOS_1 unique (OTHER_LOS_ID);

    alter table ewp.dbo.EWPCV_LOS_NAME 
       add constraint UK_LOS_NAME_1 unique (NAME_ID);

    alter table ewp.dbo.EWPCV_LOS_URLS 
       add constraint UK_LOS_URLS_1 unique (URL_ID);

    alter table ewp.dbo.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint UK_ORG_UNIT_ORG_UNIT_1 unique (ORGANIZATION_UNITS_ID);

    alter table ewp.dbo.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint UK_ORGANIZATION_UNIT_NAME_1 unique (NAME_ID);

    alter table ewp.dbo.EWPCV_RECOGNIZED_LA_COMPONENT 
       add constraint UK_RECOGNIZED_LA_COMPONENT_1 unique (RECOGNIZED_LA_COMPONENT_ID);

    alter table ewp.dbo.EWPCV_STUDIED_LA_COMPONENT 
       add constraint UK_STUDIED_LA_COMPONENT_1 unique (STUDIED_LA_COMPONENT_ID);

    alter table ewp.dbo.EWPCV_ACADEMIC_TERM_NAME 
       add constraint FK_ACADEMIC_TERM_NAME_1
       foreign key (DISP_NAME_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_ACADEMIC_TERM_NAME 
       add constraint FK_ACADEMIC_TERM_NAME_2
       foreign key (ACADEMIC_TERM_ID) 
       references ewp.dbo.EWPCV_ACADEMIC_TERM;

    alter table ewp.dbo.EWPCV_ACADEMIC_TERM 
       add constraint FK_ACADEMIC_TERM_NAME_3
       foreign key (ACADEMIC_YEAR_ID) 
       references ewp.dbo.EWPCV_ACADEMIC_YEAR;
	   
	alter table ewp.dbo.EWPCV_CONTACT
       add constraint FK_CONTACT_1
       foreign key (CONTACT_DETAILS_ID)
       references ewp.dbo.EWPCV_CONTACT_DETAILS;
	   
    alter table ewp.dbo.EWPCV_CONTACT
       add constraint FK_CONTACT_2
       foreign key (PERSON_ID)
       references ewp.dbo.EWPCV_PERSON;

    alter table ewp.dbo.EWPCV_CONTACT_DESCRIPTION 
       add constraint FK_CONTACT_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_CONTACT_DESCRIPTION 
       add constraint FK_CONTACT_DESCRIPTION_2
       foreign key (CONTACT_ID) 
       references ewp.dbo.EWPCV_CONTACT;

    alter table ewp.dbo.EWPCV_CONTACT_NAME 
       add constraint FK_CONTACT_NAME_1
       foreign key (NAME_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_CONTACT_NAME 
       add constraint FK_CONTACT_NAME_2
       foreign key (CONTACT_ID) 
       references ewp.dbo.EWPCV_CONTACT;

    alter table ewp.dbo.EWPCV_CONTACT_URL 
       add constraint FK_CONTACT_URL_1
       foreign key (URL_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_CONTACT_URL 
       add constraint FK_CONTACT_URL_2
       foreign key (CONTACT_DETAILS_ID) 
       references ewp.dbo.EWPCV_CONTACT_DETAILS;

    alter table ewp.dbo.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_1
       foreign key (FAX_NUMBER) 
       references ewp.dbo.EWPCV_PHONE_NUMBER;

    alter table ewp.dbo.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_2
       foreign key (MAILING_ADDRESS) 
       references ewp.dbo.EWPCV_FLEXIBLE_ADDRESS;

    alter table ewp.dbo.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_3
       foreign key (PHONE_NUMBER) 
       references ewp.dbo.EWPCV_PHONE_NUMBER;

    alter table ewp.dbo.EWPCV_CONTACT_DETAILS 
       add constraint FK_CONTACT_DETAILS_4
       foreign key (STREET_ADDRESS) 
       references ewp.dbo.EWPCV_FLEXIBLE_ADDRESS;

    alter table ewp.dbo.EWPCV_CONTACT_DETAILS_EMAIL 
       add constraint FK_CONTACT_DETAILS_5
       foreign key (CONTACT_DETAILS_ID) 
       references ewp.dbo.EWPCV_CONTACT_DETAILS;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_1
       foreign key (DURATION_ID) 
       references ewp.dbo.EWPCV_DURATION;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_2
       foreign key (MOBILITY_NUMBER_ID) 
       references ewp.dbo.EWPCV_MOBILITY_NUMBER;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_3
       foreign key (MOBILITY_TYPE_ID) 
       references ewp.dbo.EWPCV_MOBILITY_TYPE;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_4
       foreign key (RECEIVING_PARTNER_ID) 
       references ewp.dbo.EWPCV_IIA_PARTNER;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_5
       foreign key (SENDING_PARTNER_ID) 
       references ewp.dbo.EWPCV_IIA_PARTNER;

    alter table ewp.dbo.EWPCV_COOPERATION_CONDITION 
       add constraint FK_COOPERATION_CONDITION_6
       foreign key (IIA_ID) 
       references ewp.dbo.EWPCV_IIA;

    alter table ewp.dbo.EWPCV_DISTR_CATEGORIES 
       add constraint FK_DISTR_CATEGORIES_1
       foreign key (RESULT_DIST_CATEGORY_ID) 
       references ewp.dbo.EWPCV_RESULT_DIST_CATEGORY;

    alter table ewp.dbo.EWPCV_DISTR_CATEGORIES 
       add constraint FK_DISTR_CATEGORIES_2
       foreign key (RESULT_DISTRIBUTION_ID) 
       references ewp.dbo.EWPCV_RESULT_DISTRIBUTION;

    alter table ewp.dbo.EWPCV_DISTR_DESCRIPTION 
       add constraint FK_DISTR_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_DISTR_DESCRIPTION 
       add constraint FK_DISTR_DESCRIPTION_2
       foreign key (RESULT_DISTRIBUTION_ID) 
       references ewp.dbo.EWPCV_RESULT_DISTRIBUTION;

    alter table ewp.dbo.EWPCV_FACT_SHEET_URL 
       add constraint FK_FACT_SHEET_URL_1
       foreign key (URL_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_FACT_SHEET_URL 
       add constraint FK_FACT_SHEET_URL_2
       foreign key (FACT_SHEET_ID) 
       references ewp.dbo.EWPCV_FACT_SHEET;

    alter table ewp.dbo.EWPCV_FACT_SHEET 
       add constraint FK_FACT_SHEET_1
       foreign key (CONTACT_DETAILS_ID) 
       references ewp.dbo.EWPCV_CONTACT_DETAILS;

    alter table ewp.dbo.EWPCV_FLEXIBLE_ADDRESS_LINE
       add constraint FK_FLEXIBLE_ADDRESS_LINE_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references ewp.dbo.EWPCV_FLEXIBLE_ADDRESS;

    alter table ewp.dbo.EWPCV_FLEXAD_DELIV_POINT_COD 
       add constraint FK_FLEXAD_DELIV_POINT_COD_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references ewp.dbo.EWPCV_FLEXIBLE_ADDRESS;

    alter table ewp.dbo.EWPCV_FLEXAD_RECIPIENT_NAME
       add constraint FK_FLEXAD_RECIPIENT_NAME_1
       foreign key (FLEXIBLE_ADDRESS_ID) 
       references ewp.dbo.EWPCV_FLEXIBLE_ADDRESS;

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_DESC 
       add constraint FK_GRADING_SCHEM_DESC_DESC_1
       foreign key (DESCRIPTION_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_DESC 
       add constraint FK_GRADING_SCHEM_DESC_DESC_2
       foreign key (GRADING_SCHEME_ID) 
       references ewp.dbo.EWPCV_GRADING_SCHEME;

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_LABEL 
       add constraint FK_GRADING_SCHEME_LABEL_1
       foreign key (LABEL_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_GRADING_SCHEME_LABEL 
       add constraint FK_GRADING_SCHEME_LABEL_2
       foreign key (GRADING_SCHEME_ID) 
       references ewp.dbo.EWPCV_GRADING_SCHEME;

    alter table ewp.dbo.EWPCV_IIA_PARTNER_CONTACTS 
       add constraint FK_IIA_PARTNER_CONTACTS_1
       foreign key (CONTACTS_ID) 
       references ewp.dbo.EWPCV_CONTACT;

    alter table ewp.dbo.EWPCV_IIA_PARTNER_CONTACTS 
       add constraint FK_IIA_PARTNER_CONTACTS_2
       foreign key (IIA_PARTNER_ID) 
       references ewp.dbo.EWPCV_IIA_PARTNER;

    alter table ewp.dbo.EWPCV_INST_ORG_UNIT 
       add constraint FK_INST_ORG_UNIT_1
       foreign key (ORGANIZATION_UNITS_ID) 
       references ewp.dbo.EWPCV_ORGANIZATION_UNIT;

    alter table ewp.dbo.EWPCV_INST_ORG_UNIT 
       add constraint FK_INST_ORG_UNIT_2
       foreign key (INSTITUTION_ID) 
       references ewp.dbo.EWPCV_INSTITUTION;

    alter table ewp.dbo.EWPCV_INSTITUTION 
       add constraint FK_INSTITUTION_1
       foreign key (FACT_SHEET) 
       references ewp.dbo.EWPCV_FACT_SHEET;

    alter table ewp.dbo.EWPCV_INSTITUTION_NAME 
       add constraint FK_INSTITUTION_NAME_1
       foreign key (NAME_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_INSTITUTION_NAME 
       add constraint FK_INSTITUTION_NAME_2
       foreign key (INSTITUTION_ID) 
       references ewp.dbo.EWPCV_INSTITUTION;

    alter table ewp.dbo.EWPCV_LOI 
       add constraint FK_LOI_1
       foreign key (ACADEMIC_TERM_ID) 
       references ewp.dbo.EWPCV_ACADEMIC_TERM;

    alter table ewp.dbo.EWPCV_LOI 
       add constraint FK_LOI_2
       foreign key (GRADING_SCHEME_ID) 
       references ewp.dbo.EWPCV_GRADING_SCHEME;

    alter table ewp.dbo.EWPCV_LOI 
       add constraint FK_LOI_3
       foreign key (RESULT_DISTRIBUTION) 
       references ewp.dbo.EWPCV_RESULT_DISTRIBUTION;

    alter table ewp.dbo.EWPCV_LOI_CREDITS 
       add constraint FK_LOI_CREDITS_1
       foreign key (CREDITS_ID) 
       references ewp.dbo.EWPCV_CREDIT;

    alter table ewp.dbo.EWPCV_LOI_CREDITS 
       add constraint FK_LOI_CREDITS_2
       foreign key (LOI_ID) 
       references ewp.dbo.EWPCV_LOI;

    alter table ewp.dbo.EWPCV_LOS_DESCRIPTION 
       add constraint FK_LOS_DESCRIPTION_1
       foreign key (DESCRIPTION_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_LOS_DESCRIPTION 
       add constraint FK_LOS_DESCRIPTION_2
       foreign key (LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_LOS_LOI 
       add constraint FK_LOS_LOI_1
       foreign key (LOI_ID) 
       references ewp.dbo.EWPCV_LOI;

    alter table ewp.dbo.EWPCV_LOS_LOI 
       add constraint FK_LOS_LOI_2
       foreign key (LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_LOS_LOS 
       add constraint FK_LOS_LOS_1
       foreign key (OTHER_LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_LOS_LOS 
       add constraint FK_LOS_LOS_2
       foreign key (LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_LOS_NAME 
       add constraint FK_LOS_NAME_1
       foreign key (NAME_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_LOS_NAME 
       add constraint FK_LOS_NAME_2
       foreign key (LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_LOS_URLS 
       add constraint FK_LOS_URLS_1
       foreign key (URL_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_LOS_URLS 
       add constraint FK_LOS_URLS_2
       foreign key (LOS_ID) 
       references ewp.dbo.EWPCV_LOS;

    alter table ewp.dbo.EWPCV_MOBILITY 
       add constraint FK_MOBILITY_1
       foreign key (MOBILITY_TYPE_ID) 
       references ewp.dbo.EWPCV_MOBILITY_TYPE;

    alter table ewp.dbo.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint FK_ORG_UNIT_ORG_UNIT_1
       foreign key (ORGANIZATION_UNITS_ID) 
       references ewp.dbo.EWPCV_ORGANIZATION_UNIT;

    alter table ewp.dbo.EWPCV_ORG_UNIT_ORG_UNIT 
       add constraint FK_ORG_UNIT_ORG_UNIT_2
       foreign key (ORGANIZATION_UNIT_ID) 
       references ewp.dbo.EWPCV_ORGANIZATION_UNIT;

    alter table ewp.dbo.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint FK_ORG_UNIT_ORG_UNIT_NAME_1
       foreign key (NAME_ID) 
       references ewp.dbo.EWPCV_LANGUAGE_ITEM;

    alter table ewp.dbo.EWPCV_ORGANIZATION_UNIT_NAME 
       add constraint FK_ORGANIZATION_UNIT_NAME_2
       foreign key (ORGANIZATION_UNIT_ID) 
       references ewp.dbo.EWPCV_ORGANIZATION_UNIT;

    alter table ewp.dbo.EWPCV_ORGANIZATION_UNIT 
       add constraint FK_ORGANIZATION_UNIT_1
       foreign key (FACT_SHEET) 
       references ewp.dbo.EWPCV_FACT_SHEET;

	ALTER TABLE  ewp.dbo.EWPCV_IIA_PARTNER 
		ADD CONSTRAINT FK_IIA_PARTNER_1
		FOREIGN KEY (SIGNER_PERSON_CONTACT_ID) REFERENCES  ewp.dbo.EWPCV_CONTACT (ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_1
		FOREIGN KEY (MODIFIED_STUDENT_CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_2
		FOREIGN KEY (RECEIVER_COORDINATOR_SIGN) REFERENCES  ewp.dbo.EWPCV_SIGNATURE (ID);

	ALTER TABLE  ewp.dbo.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_3
		FOREIGN KEY (STUDENT_SIGN) REFERENCES  ewp.dbo.EWPCV_SIGNATURE (ID);

	ALTER TABLE  ewp.dbo.EWPCV_LEARNING_AGREEMENT 
		ADD CONSTRAINT FK_LEARNING_AGREEMENT_4
		FOREIGN KEY (SENDER_COORDINATOR_SIGN) REFERENCES  ewp.dbo.EWPCV_SIGNATURE (ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_2
		FOREIGN KEY (ISCED_CODE) references ewp.dbo.EWPCV_SUBJECT_AREA (ISCED_CODE);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_3
		FOREIGN KEY (LANGUAGE_SKILL) references ewp.dbo.EWPCV_LANGUAGE_SKILL (ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_4
		FOREIGN KEY (RECEIVER_ADMV_CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_5
		FOREIGN KEY (RECEIVER_CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_6
		FOREIGN KEY (SENDER_CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_7
		FOREIGN KEY (SENDER_ADMV_CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY 
		ADD CONSTRAINT FK_MOBILITY_8
		FOREIGN KEY (MOBILITY_PARTICIPANT_ID) REFERENCES ewp.dbo.EWPCV_MOBILITY_PARTICIPANT(ID);
	
	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY_PARTICIPANT 
		ADD CONSTRAINT FK_MOBILITY_PARTICIPANT_1
		FOREIGN KEY (CONTACT_ID) REFERENCES ewp.dbo.EWPCV_CONTACT(ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_LA_COMPONENT 
		ADD CONSTRAINT FK_LA_COMPONENT_1
		FOREIGN KEY (LOI_ID) REFERENCES  ewp.dbo.EWPCV_LOI (ID);
	
	ALTER TABLE  ewp.dbo.EWPCV_LA_COMPONENT 
		ADD CONSTRAINT FK_LA_COMPONENT_2
		FOREIGN KEY (LOS_ID) REFERENCES  ewp.dbo.EWPCV_LOS (ID);

	ALTER TABLE  ewp.dbo.EWPCV_STUDIED_LA_COMPONENT 
		ADD CONSTRAINT FK_STUDIED_LA_COMPONENT_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  ewp.dbo.EWPCV_STUDIED_LA_COMPONENT 
		ADD CONSTRAINT FK_STUDIED_LA_COMPONENT_2
		FOREIGN KEY (STUDIED_LA_COMPONENT_ID) REFERENCES  ewp.dbo.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_RECOGNIZED_LA_COMPONENT 
		ADD CONSTRAINT FK_RECOGNIZED_LA_COMPONENT_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  ewp.dbo.EWPCV_RECOGNIZED_LA_COMPONENT 
		ADD CONSTRAINT FK_RECOGNIZED_LA_COMPONENT_2
		FOREIGN KEY (RECOGNIZED_LA_COMPONENT_ID) REFERENCES  ewp.dbo.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_VIRTUAL_LA_COMPONENT 
		ADD CONSTRAINT FK_VIRTUAL_LA_COMPONENT_1
		FOREIGN KEY (VIRTUAL_LA_COMPONENT_ID) REFERENCES  ewp.dbo.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_VIRTUAL_LA_COMPONENT 
		ADD CONSTRAINT FK_VIRTUAL_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  ewp.dbo.EWPCV_BLENDED_LA_COMPONENT 
		ADD CONSTRAINT FK_BLENDED_LA_COMPONENT_1
		FOREIGN KEY (BLENDED_LA_COMPONENT_ID) REFERENCES  ewp.dbo.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_BLENDED_LA_COMPONENT 
		ADD CONSTRAINT FK_BLENDED_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);

	ALTER TABLE  ewp.dbo.EWPCV_DOCTORAL_LA_COMPONENT 
		ADD CONSTRAINT FK_DOCTORAL_LA_COMPONENT_1
		FOREIGN KEY (DOCTORAL_LA_COMPONENTS_ID) REFERENCES  ewp.dbo.EWPCV_LA_COMPONENT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_DOCTORAL_LA_COMPONENT 
		ADD CONSTRAINT FK_DOCTORAL_LA_COMPONENT_2
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);
		
	ALTER TABLE  ewp.dbo.EWPCV_COOPCOND_SUBAR_LANSKIL 
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_1
		FOREIGN KEY (COOPERATION_CONDITION_ID) REFERENCES  ewp.dbo.EWPCV_COOPERATION_CONDITION (ID);

	ALTER TABLE  ewp.dbo.EWPCV_COOPCOND_SUBAR_LANSKIL 
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2
		FOREIGN KEY (ISCED_CODE) REFERENCES  ewp.dbo.EWPCV_SUBJECT_AREA (ISCED_CODE);

	ALTER TABLE  ewp.dbo.EWPCV_COOPCOND_SUBAR_LANSKIL
		ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_3
		FOREIGN KEY (LANGUAGE_SKILL_ID) REFERENCES  ewp.dbo.EWPCV_LANGUAGE_SKILL (ID);

	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY_LA 
		ADD CONSTRAINT FK_MOBILITY_LA_1
		FOREIGN KEY (LEARNING_AGREEMENT_ID,LEARNING_AGREEMENT_REVISION) REFERENCES  ewp.dbo.EWPCV_LEARNING_AGREEMENT (ID,LEARNING_AGREEMENT_REVISION);
	
	ALTER TABLE  ewp.dbo.EWPCV_MOBILITY_LA 
		ADD CONSTRAINT FK_MOBILITY_LA_2
		FOREIGN KEY (MOBILITY_ID,MOBILITY_REVISION) REFERENCES  ewp.dbo.EWPCV_MOBILITY (ID,MOBILITY_REVISION);
	
	ALTER TABLE  ewp.dbo.EWPCV_INFORMATION_ITEM 
		ADD CONSTRAINT FK_IDENTIFICATION_ITEM_1
		FOREIGN KEY (CONTACT_DETAIL_ID) REFERENCES ewp.dbo.EWPCV_CONTACT_DETAILS(ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_INST_INF_ITEM 
		ADD CONSTRAINT FK_INST_INF_ITEM_1
		FOREIGN KEY (INFORMATION_ID) REFERENCES  ewp.dbo.EWPCV_INFORMATION_ITEM (ID);

	ALTER TABLE  ewp.dbo.EWPCV_INST_INF_ITEM 
		ADD CONSTRAINT FK_INST_INF_ITEM_2
		FOREIGN KEY (INSTITUTION_ID) REFERENCES  ewp.dbo.EWPCV_INSTITUTION (ID);
		
	ALTER TABLE  ewp.dbo.EWPCV_OU_INF_ITEM 
		ADD CONSTRAINT FK_OU_INF_ITEM_1
		FOREIGN KEY (ORGANIZATION_UNIT_ID) REFERENCES  ewp.dbo.EWPCV_ORGANIZATION_UNIT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_OU_INF_ITEM 
		ADD CONSTRAINT FK_OU_INF_ITEM_2
		FOREIGN KEY (INFORMATION_ID) REFERENCES  ewp.dbo.EWPCV_INFORMATION_ITEM (ID);

	ALTER TABLE  ewp.dbo.EWPCV_REQUIREMENTS_INFO 
		ADD CONSTRAINT FK_REQUIREMENTS_INFO_1
		FOREIGN KEY (CONTACT_DETAIL_ID) REFERENCES  ewp.dbo.EWPCV_CONTACT_DETAILS (ID);
	
	ALTER TABLE  ewp.dbo.EWPCV_OU_REQUIREMENT_INFO 
		ADD CONSTRAINT FK_OU_REQUIREMENT_INFO_1
		FOREIGN KEY (OU_ID) REFERENCES  ewp.dbo.EWPCV_ORGANIZATION_UNIT (ID);

	ALTER TABLE  ewp.dbo.EWPCV_OU_REQUIREMENT_INFO 
		ADD CONSTRAINT FK_OU_REQUIREMENT_INFO_2
		FOREIGN KEY (REQUERIMENT_ID) REFERENCES  ewp.dbo.EWPCV_REQUIREMENTS_INFO (ID);

	ALTER TABLE  ewp.dbo.EWPCV_INS_REQUIREMENTS 
		ADD CONSTRAINT FK_INS_REQUIREMENTS_1
		FOREIGN KEY (INSTITUTION_ID) REFERENCES ewp.dbo.EWPCV_INSTITUTION (ID);

	ALTER TABLE  ewp.dbo.EWPCV_INS_REQUIREMENTS 
		ADD CONSTRAINT FK_INS_REQUIREMENTS_2
		FOREIGN KEY (REQUIREMENT_ID) REFERENCES  ewp.dbo.EWPCV_REQUIREMENTS_INFO (ID);

	ALTER TABLE  ewp.dbo.EWPCV_FACTSHEET_DATES 
		ADD CONSTRAINT FK_FACTSHEET_DATES_1
		FOREIGN KEY (ACADEMIC_TERM_ID) REFERENCES ewp.dbo.EWPCV_ACADEMIC_TERM (ID);

	ALTER TABLE  ewp.dbo.EWPCV_FACTSHEET_DATES 
		ADD CONSTRAINT FK_DATE_FACTSHEET_2
		FOREIGN KEY (FACTSHEET_ID) REFERENCES  ewp.dbo.EWPCV_FACT_SHEET (ID);
		
		
IF OBJECT_ID (N'ewp.dbo.ExtraerAnio', N'FN') IS NOT NULL  
    DROP FUNCTION ExtraerAnio;  
GO  
CREATE FUNCTION ExtraerAnio(@START_DATE DATETIME2)  
RETURNS int  
AS   
BEGIN  
    DECLARE @ANIO int
    SET @ANIO = YEAR(@START_DATE)    
    RETURN @ANIO 
END; 
GO  

