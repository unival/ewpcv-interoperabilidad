
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
	ALTER TABLE dbo.EWPCV_DICTIONARY ALTER COLUMN DESCRIPTION VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_INSTITUTION_IDENTIFIERS ALTER COLUMN INSTITUTION_NAMES VARCHAR(4000);
	ALTER TABLE dbo.EWPCV_ADDITIONAL_INFO ALTER COLUMN ADDITIONAL_INFO VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_DIPLOMA_SECTION ALTER COLUMN TITLE VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_LA_COMPONENT ALTER COLUMN TITLE VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_LANGUAGE_ITEM ALTER COLUMN TEXT VARCHAR(4000);
	ALTER TABLE dbo.EWPCV_REQUIREMENTS_INFO ALTER COLUMN NAME VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_REQUIREMENTS_INFO ALTER COLUMN DESCRIPTION VARCHAR(4000);
	ALTER TABLE dbo.EWPCV_SUBJECT_AREA ALTER COLUMN ISCED_CLARIFICATION  VARCHAR(1000);
	ALTER TABLE dbo.EWPCV_COOPERATION_CONDITION ALTER COLUMN OTHER_INFO VARCHAR(4000);
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.04', GETDATE(), '23_UPGRADE_FEATURE_EHU_ERROR_desboradamiento_de_campos');


