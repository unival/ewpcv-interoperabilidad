
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FIRMAS'
) DROP FUNCTION dbo.EXTRAE_FIRMAS;
GO 	
CREATE FUNCTION dbo.EXTRAE_FIRMAS (@P_FIRMA_ID varchar(255)) RETURNS varchar(max)  as
BEGIN
    DECLARE @v_lista VARCHAR(max)
    SELECT @v_lista = stuff( (select concat('|;|' , COALESCE(SIGNER_NAME,'') , '|' , COALESCE(SIGNER_POSITION,'') 
				, '|' , COALESCE(SIGNER_EMAIL,'') , '|' , CONVERT(VARCHAR(19), COALESCE(null, ''), 126 ) , '|' , COALESCE(SIGNER_APP,''))
			    FROM dbo.EWPCV_SIGNATURE
				WHERE ID = @P_FIRMA_ID
               for xml path ('')
              ), 1, 3, ''
            );

    RETURN @v_lista

END
;
GO
/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_IIA'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	    <xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" minOccurs="0"/>
				<xs:element name="end_date" type="xs:date" minOccurs="0"/>
				<xs:element name="first_partner" type="partnerType"/>
				<xs:element name="second_partner" type="partnerType"/>
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded">
														<xs:simpleType>
													        <xs:restriction base="xs:integer">
																<xs:minInclusive value="1"/>
																<xs:maxInclusive value="8"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="1"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="subject_area_list"  minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area" minOccurs="0" maxOccurs="unbounded" type="subjectAreaType"/>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="1">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="1" type="xs:decimal" />
													<xs:element name="unit" minOccurs="1">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO


/*
    *************************************
    FIN MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/
/* 
	Elimina una INSTITUTION del sistema.
	Recibe como parametros: 
    El identificador (SCHAC) de la INSTITUTION
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_INSTITUTION'
) DROP PROCEDURE dbo.DELETE_INSTITUTION;
GO 
CREATE PROCEDURE dbo.DELETE_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer

		SET @return_value = 0
		--validacion existe institucion
		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION WHERE INSTITUTION_ID = @P_INSTITUTION_ID
		IF @v_count = 0 
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = 'La institución no existe'
			END 
			ELSE 
			BEGIN
				--validacion intitucion	sin factsheet
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
					AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución tiene un factsheet asociado, bórrelo antes de borrar la institución')
					END
				--validacion intitucion	sin ounits
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución tiene unidades organizativas dependientes, bórrelas antes de borrar la institución')
					END
					
				IF @return_value = 0
					BEGIN
						EXECUTE BORRA_INSTITUTION @P_INSTITUTION_ID
					END		
			END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_INSTITUTION'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END 
;
GO

/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.04', GETDATE(), '21_UPGRADE_v01.08.04');



