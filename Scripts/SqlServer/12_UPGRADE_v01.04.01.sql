
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
	ALTER TABLE EWPCV_COOPCOND_SUBAR_LANSKIL DROP CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2;
	ALTER TABLE EWPCV_COOPCOND_SUBAR_LANSKIL ALTER COLUMN ISCED_CODE varchar(255) NULL;
	ALTER TABLE  dbo.EWPCV_COOPCOND_SUBAR_LANSKIL ADD CONSTRAINT FK_COOPCOND_SUBAR_LANSKIL_2 FOREIGN KEY (ISCED_CODE) REFERENCES  dbo.EWPCV_SUBJECT_AREA (ID);

 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_PHOTO_URL_LIST'
) DROP FUNCTION dbo.EXTRAE_PHOTO_URL_LIST;
GO
CREATE FUNCTION dbo.EXTRAE_PHOTO_URL_LIST(@CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + PU.URL + '|:|' + PU.PHOTO_SIZE + '|:|' + CONVERT(VARCHAR(10), PU.PHOTO_DATE, 20) + '|:|' + convert(varchar(1),PU.PHOTO_PUBLIC)
			   FROM dbo.EWPCV_PHOTO_URL PU
				WHERE PU.CONTACT_DETAILS_ID = @CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_EQF_LEVEL'
) DROP FUNCTION dbo.EXTRAE_EQF_LEVEL;
GO
CREATE FUNCTION dbo.EXTRAE_EQF_LEVEL(@P_COOP_CON_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + CONVERT(VARCHAR(5),EQF_LEVEL)
			   FROM dbo.EWPCV_COOPCOND_EQFLVL
			   WHERE COOPERATION_CONDITION_ID = @P_COOP_CON_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

--Funciones de common 
/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.INSERTA_INSTITUTION(@P_INSTITUTION_ID  VARCHAR(255), @P_ABREVIATION VARCHAR(255), @P_LOGO_URL VARCHAR(255),
		 @P_FACTSHEET_ID VARCHAR(255),  @P_INSTITUTION_NAMES xml, @P_PRIMARY_CONTACT_DETAIL_ID VARCHAR(255), @return_value uniqueidentifier OUTPUT) AS

BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_count integer

	SELECT @v_count = count(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count = 0
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID)
				VALUES (@v_id, LOWER(@P_INSTITUTION_ID), @P_ABREVIATION, @P_LOGO_URL, @P_FACTSHEET_ID, @P_PRIMARY_CONTACT_DETAIL_ID); 
		END
	ELSE
		BEGIN
			SELECT @v_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			UPDATE  dbo.EWPCV_INSTITUTION SET  ABBREVIATION = @P_ABREVIATION, LOGO_URL = @P_LOGO_URL, FACT_SHEET = @P_FACTSHEET_ID WHERE ID = @v_id; 
		END
	
	EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_id, @P_INSTITUTION_NAMES

	SET @return_value = @v_id

END
;
GO

/*
	Inserta datos de contacto y retorna id de contact detail id
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT'
) DROP PROCEDURE dbo.INSERTA_CONTACT;
GO 
CREATE PROCEDURE dbo.INSERTA_CONTACT(@P_CONTACT xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value uniqueidentifier OUTPUT) AS
BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_c_id uniqueidentifier

	DECLARE @v_ounit_id uniqueidentifier
	DECLARE @v_inst_id varchar(255)
	DECLARE @v_li_id uniqueidentifier

	DECLARE @CONTACT_NAME xml
	DECLARE @CONTACT_DESCRIPTION xml
	DECLARE @INSTITUTION_ID varchar(255) --deprecated
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255) --deprecated
	DECLARE @CONTACT_ROLE varchar(255)
	DECLARE @item xml

	SELECT 
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('(contact_role)[1]', 'varchar(255)')
	FROM @P_CONTACT.nodes('*') T(c)

	
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @P_CONTACT, @v_c_id OUTPUT

	IF @v_c_id IS NOT NULL AND @CONTACT_NAME IS NOT NULL AND  @CONTACT_DESCRIPTION IS NOT NULL  
		BEGIN
			
			IF @P_INSTITUTION_ID IS NOT NULL AND @P_OUNIT_CODE IS NULL
				BEGIN
					SET @v_inst_id = LOWER(@P_INSTITUTION_ID);
				END;
						
			
			IF @P_OUNIT_CODE IS NOT NULL 
				BEGIN 
					SELECT @v_ounit_id = tabO.ID
						FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.ID
						INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
						WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
						AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
				END 
			
			SET @v_id  = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, null)

			IF @CONTACT_NAME is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_NAME.nodes('contact_name/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

			IF @CONTACT_DESCRIPTION is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

		END 

	SET @return_value = @v_c_id

END
; 

GO

/*
	Inserta datos de contacto y de persona 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_PERSON'
) DROP PROCEDURE dbo.INSERTA_CONTACT_PERSON;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_PERSON(@P_PERSON xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value uniqueidentifier OUTPUT) as
BEGIN
DECLARE @GENDER varchar(255)

DECLARE @CONTACT xml
DECLARE @CONTACT_NAME xml
DECLARE @CONTACT_DESCRIPTION xml
DECLARE @INSTITUTION_ID varchar(255)
DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
DECLARE @CONTACT_ROLE varchar(255)
DECLARE @v_p_id uniqueidentifier
DECLARE @v_c_id uniqueidentifier
DECLARE @v_ounit_id uniqueidentifier
DECLARE @v_inst_id varchar(255)
DECLARE @v_id uniqueidentifier
DECLARE @v_li_id uniqueidentifier
DECLARE @item xml

	SELECT
		@CONTACT = T.c.query('.'),
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('contact_role[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*/*') T(c) 

	EXECUTE dbo.INSERTA_PERSONA @P_PERSON, @v_p_id OUTPUT
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @CONTACT, @v_c_id OUTPUT

	IF @v_p_id is not null or @v_c_id is not null or 
		@CONTACT_NAME is not null or @CONTACT_DESCRIPTION is not null 
	BEGIN

		IF @P_INSTITUTION_ID IS NOT NULL AND @P_OUNIT_CODE IS NULL
		BEGIN
			SET @v_inst_id = LOWER(@P_INSTITUTION_ID);
		END;
                
	
		IF @P_OUNIT_CODE IS NOT NULL 
		BEGIN 
			SELECT @v_ounit_id = tabO.ID
				FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.ID
				INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
				WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
				AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
		END 

		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, @v_p_id);

		-- Lista de contact name
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_NAME.nodes('contact_name/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

		-- Lista de contact description
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

	END

	SET @return_value = @v_id

END
;
GO

/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_NOTIFICATION'
) DROP PROCEDURE dbo.INSERTA_NOTIFICATION;
GO 
CREATE PROCEDURE  dbo.INSERTA_NOTIFICATION(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255)) as
BEGIN
	INSERT INTO dbo.EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (NEWID(), @P_ELEMENT_ID, LOWER(@P_NOTIFY_HEI), SYSDATETIME(), @P_TYPE, 1, 0, LOWER(@P_NOTIFIER_HEI) );
END 
;


GO

--Funciones de factsheet

/*
	Persiste un information item
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INFORMATION_ITEM'
) DROP PROCEDURE dbo.INSERTA_INFORMATION_ITEM;
GO 
CREATE PROCEDURE  dbo.INSERTA_INFORMATION_ITEM(@P_INFORMATION_ITEM xml,
	@P_INF_TYPE VARCHAR(255), @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_infoitem_id uniqueidentifier

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('*') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	SET @v_infoitem_id =  NEWID()

	INSERT INTO dbo.EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (@v_infoitem_id, LOWER(@P_INF_TYPE), @v_contact_id)
	INSERT INTO dbo.EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (@P_INSTITUTION_ID, @v_infoitem_id)

END
;
GO

/*
	Inserta informacion sobre requerimientos
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_REQUIREMENT_INFO'
) DROP PROCEDURE dbo.INSERTA_REQUIREMENT_INFO;
GO 
CREATE PROCEDURE  dbo.INSERTA_REQUIREMENT_INFO(@P_TYPE VARCHAR(255), @P_NAME VARCHAR(255),  @P_DESCRIPTION VARCHAR(255), 
		@P_URL xml,	@P_EMAIL VARCHAR(255), @P_PHONE xml, @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_requinf_id uniqueidentifier

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @P_URL, @P_EMAIL, @P_PHONE, @v_contact_id OUTPUT

	SET @v_requinf_id = NEWID()
	INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) 
		VALUES (@v_requinf_id, LOWER(@P_TYPE), @P_NAME, @P_DESCRIPTION, @v_contact_id)
	INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) 
		VALUES (@P_INSTITUTION_ID, @v_requinf_id)

END
;
GO
	

--Funciones de IIAs
/*
	Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_IIA_PARTNER'
) DROP PROCEDURE dbo.INSERTA_IIA_PARTNER;
GO 
CREATE PROCEDURE  dbo.INSERTA_IIA_PARTNER(@P_IIA_PARTNER xml, @return_value uniqueidentifier output) AS
BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_c_id uniqueidentifier
	DECLARE @v_ounit_id uniqueidentifier
	DECLARE @INSTITUTION xml
	DECLARE @SIGNER_PERSON xml
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @SIGNING_DATE date


	SELECT 
		@INSTITUTION = T.c.query('institution'),
		@SIGNER_PERSON = T.c.query('signer_person'),
		@INSTITUTION_ID = T.c.value('(institution/institution_id)[1]', 'varchar(255)'),
		@SIGNING_DATE = T.c.value('(signing_date)[1]', 'date')
	FROM @P_IIA_PARTNER.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @INSTITUTION, @v_ounit_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SIGNER_PERSON, null, null, @v_c_id output
	SET @v_id = NEWID()

	INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
			VALUES (@v_id, LOWER(@INSTITUTION_ID), @v_ounit_id, @v_c_id, @SIGNING_DATE)

	SET @return_value = @v_id
	
END
;
GO

--Funciones de Mobility LA

/*
	Persiste una revision de un learning agreement.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LA_REVISION'
) DROP PROCEDURE dbo.INSERTA_LA_REVISION;
GO 
CREATE PROCEDURE  dbo.INSERTA_LA_REVISION(@P_LA_ID uniqueidentifier, @P_LA xml, @P_LA_REVISION integer, @P_MOBILITY_REVISION integer, @return_value uniqueidentifier output)  AS 

BEGIN
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_stud_sign_id uniqueidentifier
	DECLARE @v_s_hei_sign_id uniqueidentifier
	DECLARE @v_changes_id varchar(300)
	DECLARE @v_c_id uniqueidentifier

	DECLARE @MOBILITY_ID uniqueidentifier
	DECLARE @STUDENT_SIGNATURE xml
	DECLARE @SENDING_HEI_SIGNATURE xml
	DECLARE @LA_COMPONENT xml

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier'),
		@STUDENT_SIGNATURE = T.c.query('student_signature'),
		@SENDING_HEI_SIGNATURE = T.c.query('sending_hei_signature')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.INSERTA_SIGNATURE @STUDENT_SIGNATURE, @v_stud_sign_id output
	EXECUTE dbo.INSERTA_SIGNATURE @SENDING_HEI_SIGNATURE, @v_s_hei_sign_id output

	IF @P_LA_ID IS NULL  
		SET @v_la_id = NEWID()
	ELSE 
		SET @v_la_id = @P_LA_ID

	SET @v_changes_id = CONVERT(varchar(255),@v_la_id ) + '-' + CONVERT(varchar(255),@P_LA_REVISION)

	INSERT INTO dbo.EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (@v_la_id, @P_LA_REVISION, @v_stud_sign_id, @v_s_hei_sign_id, 0, SYSDATETIME(), @v_changes_id);

	INSERT INTO dbo.EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(@MOBILITY_ID, @P_MOBILITY_REVISION, @v_la_id, @P_LA_REVISION);

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_LA.nodes('learning_agreement/la_component_list/la_component') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @LA_COMPONENT
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_COMPONENTE @LA_COMPONENT, @v_la_id, @P_LA_REVISION, @v_c_id output
			FETCH NEXT FROM cur INTO @LA_COMPONENT
		END
	CLOSE cur
	DEALLOCATE cur
	
	SET @return_value = @v_la_id

END 
; 
GO

/*	
	Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACADEMIC_TERM'
) DROP PROCEDURE dbo.INSERTA_ACADEMIC_TERM;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACADEMIC_TERM(@P_ACADEMIC_TERM xml, @v_id uniqueidentifier output) as
BEGIN
	DECLARE @v_a_id uniqueidentifier
	DECLARE @v_o_id uniqueidentifier
	DECLARE @v_li_id uniqueidentifier

	DECLARE @ACADEMIC_YEAR varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
	DECLARE @TOTAL_TERMS varchar(255)
	DECLARE @TERM_NUMBER varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @DESCRIPTION xml
	DECLARE @TEXT_ITEM xml
		
	SELECT 
		@ACADEMIC_YEAR = T.c.value('(academic_year)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@TOTAL_TERMS = T.c.value('(total_terms)[1]', 'varchar(255)'),
		@TERM_NUMBER = T.c.value('(term_number)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_ACADEMIC_TERM.nodes('academic_term') T(c)

	SELECT @v_a_id = ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(SUBSTRING(@ACADEMIC_YEAR, 1, 4))
			AND	UPPER(START_YEAR) =  UPPER(SUBSTRING(@ACADEMIC_YEAR, 6, 4))

	IF (@v_a_id is null)
	BEGIN
		SET @v_a_id = NEWID()
		INSERT INTO dbo.EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
			VALUES(@v_a_id, SUBSTRING(@ACADEMIC_YEAR, 1, 4), SUBSTRING(@ACADEMIC_YEAR, 6, 4))
	END 


	SELECT @v_o_id = O.ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN dbo.EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)

	SET @v_id = NEWID()

	INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (@v_id, @END_DATE, @START_DATE , LOWER(@INSTITUTION_ID) , @v_o_id, @v_a_id, @TOTAL_TERMS, @TERM_NUMBER)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @TEXT_ITEM
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TEXT_ITEM , @v_li_id output
			INSERT INTO dbo.EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @TEXT_ITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;

GO

/*
	Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY'
) DROP PROCEDURE dbo.INSERTA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY(@P_MOBILITY xml, @P_REVISION integer, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @v_s_ounit_id uniqueidentifier
	DECLARE @v_r_ounit_id uniqueidentifier
	DECLARE @v_student_id varchar(255)
	DECLARE @v_s_contact_id uniqueidentifier
	DECLARE @v_s_admv_contact_id uniqueidentifier
	DECLARE @v_r_contact_id uniqueidentifier
	DECLARE @v_r_admv_contact_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_isced_code varchar(255)
	DECLARE @v_academic_term_id varchar(255)
	DECLARE @v_status_incoming integer
	DECLARE @v_status_outgoing integer

	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	DECLARE @STUDENT xml
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml
	DECLARE @SUBJECT_AREA xml
	DECLARE @MOBILITY_TYPE varchar(255)
	DECLARE @ACTUAL_ARRIVAL_DATE date
	DECLARE @ACTUAL_DEPATURE_DATE date
	DECLARE @IIA_ID varchar(255)
	DECLARE @PLANED_ARRIVAL_DATE date
	DECLARE @PLANED_DEPATURE_DATE date
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @STATUS_OUTGOING integer
	DECLARE @STUDENT_LANGUAGE_SKILL_LIST xml
	DECLARE @STUDENT_LANGUAGE_SKILL xml
	DECLARE @ACADEMIC_TERM xml
	DECLARE @EQF_LEVEL_DEPARTURE integer
	DECLARE @EQF_LEVEL_NOMINATION integer
	DECLARE @RECEIVING_IIA_ID varchar(255)

	SELECT 
		@SENDING_INSTITUTION = T.c.query('sending_institution'),
		@RECEIVING_INSTITUTION = T.c.query('receiving_institution'),
		@STUDENT = T.c.query('student'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@SUBJECT_AREA = T.c.query('subject_area'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)'),
		@ACTUAL_ARRIVAL_DATE = T.c.value('(actual_arrival_date)[1]', 'date'),
		@ACTUAL_DEPATURE_DATE = T.c.value('(actual_depature_date)[1]', 'date'),
		@PLANED_ARRIVAL_DATE = T.c.value('(planed_arrival_date)[1]', 'date'),
		@PLANED_DEPATURE_DATE = T.c.value('(planed_depature_date)[1]', 'date'),
		@IIA_ID = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@SENDING_INSTITUTION_ID = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]', 'integer'),
		@STUDENT_LANGUAGE_SKILL_LIST = T.c.query('student_language_skill_list'),
		@ACADEMIC_TERM = T.c.query('academic_term'),
		@EQF_LEVEL_DEPARTURE = T.c.value('(eqf_level_departure)[1]', 'integer'),
		@EQF_LEVEL_NOMINATION = T.c.value('(eqf_level_nomination)[1]', 'integer'),
		@RECEIVING_IIA_ID = T.c.value('(receiving_iia_id)[1]', 'varchar(255)')

	FROM @P_MOBILITY.nodes('mobility') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @SENDING_INSTITUTION , @v_s_ounit_id output
	EXECUTE dbo.INSERTA_INST_OUNIT @RECEIVING_INSTITUTION , @v_r_ounit_id output
	EXECUTE dbo.INSERTA_MOBILITY_PARTICIPANT @STUDENT , @v_student_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT , null, null, @v_s_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT , null, null, @v_s_admv_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT , null, null, @v_r_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT , null, null, @v_r_admv_contact_id output

	EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA , @v_isced_code output
	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE , @v_mobility_type_id output
	
	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM , @v_academic_term_id output

	-- Por defecto el status_outgoing es NOMINATION
	IF @STATUS_OUTGOING is null 
		SET @v_status_outgoing = 2
	ELSE
		SET @v_status_outgoing = @STATUS_OUTGOING

	IF @v_id is null
		SET @v_id = NEWID()


	INSERT INTO dbo.EWPCV_MOBILITY (ID,
			ACTUAL_ARRIVAL_DATE,
			ACTUAL_DEPARTURE_DATE,
			IIA_ID,
			RECEIVING_IIA_ID,
			ISCED_CODE,
			MOBILITY_PARTICIPANT_ID,
			MOBILITY_REVISION,
			PLANNED_ARRIVAL_DATE,
			PLANNED_DEPARTURE_DATE,
			RECEIVING_INSTITUTION_ID,
			RECEIVING_ORGANIZATION_UNIT_ID,
			SENDING_INSTITUTION_ID,
			SENDING_ORGANIZATION_UNIT_ID,
			MOBILITY_TYPE_ID,
			SENDER_CONTACT_ID,
			SENDER_ADMV_CONTACT_ID,
			RECEIVER_CONTACT_ID,
			RECEIVER_ADMV_CONTACT_ID,
			ACADEMIC_TERM_ID,
			EQF_LEVEL_DEPARTURE,
			EQF_LEVEL_NOMINATION,
			STATUS_OUTGOING,
			MODIFY_DATE)
			VALUES (@v_id,
			@ACTUAL_ARRIVAL_DATE,
			@ACTUAL_DEPATURE_DATE,
			@IIA_ID,
			@RECEIVING_IIA_ID,
			@v_isced_code,
			@v_student_id,
			(@P_REVISION +1),
			@PLANED_ARRIVAL_DATE,
			@PLANED_DEPATURE_DATE,
			LOWER(@RECEIVING_INSTITUTION_ID),
			@v_r_ounit_id,
			LOWER(@SENDING_INSTITUTION_ID),
			@v_s_ounit_id,
			@v_mobility_type_id,
			@v_s_contact_id,
			@v_s_admv_contact_id,
			@v_r_contact_id,
			@v_r_admv_contact_id,
			@v_academic_term_id,
			@EQF_LEVEL_DEPARTURE,
			@EQF_LEVEL_NOMINATION,
			@v_status_outgoing,
			SYSDATETIME())


	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @STUDENT_LANGUAGE_SKILL_LIST.nodes('student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (@v_id, (@P_REVISION + 1), @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*

Guarda el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACCEPT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml) AS

BEGIN

	DECLARE @v_id uniqueidentifier
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 0, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME())

END
;
GO

/*

Guarda el xml de update la de tipo rechazo y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_REJECT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_REJECT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_REJECT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml) AS

BEGIN

	DECLARE @v_id uniqueidentifier
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 1, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME())

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_LA'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_LA;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_LA(@P_LA_ID VARCHAR(255), @P_LA_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY M
			INNER JOIN dbo.EWPCV_MOBILITY_LA MLA 
			ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_MOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_MOB;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_MOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LEARNING_AGREEMENT_VIEW'
) DROP VIEW dbo.LEARNING_AGREEMENT_VIEW;
GO 
CREATE VIEW dbo.LEARNING_AGREEMENT_VIEW AS 
  SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	dbo.EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	dbo.EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	dbo.EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	MP.GLOBAL_ID			           AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM dbo.EWPCV_MOBILITY M
INNER JOIN dbo.EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN dbo.EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
LEFT JOIN dbo.EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN dbo.EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN dbo.EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN dbo.EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN dbo.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN dbo.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS;
GO
/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/
	
/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.04.01', GETDATE(), '12_UPGRADE_v01.04.01');
    
    
    
    
    
    
    
    
    
