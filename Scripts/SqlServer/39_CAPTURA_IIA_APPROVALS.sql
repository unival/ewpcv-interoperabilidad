/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD LOCAL_HASH_V7 varchar(255);
ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD REMOTE_HASH_V7 varchar(255);

ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD LOCAL_MESSAGE VARBINARY(MAX);
ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD REMOTE_MESSAGE VARBINARY(MAX);

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v02.03.00', GETDATE(), '39_CAPTURA_IIA_APPROVALS');
