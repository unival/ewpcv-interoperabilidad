
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
	ALTER TABLE dbo.EWPCV_MOBILITY_UPDATE_REQUEST ADD LEARNING_AGREEMENT_ID VARCHAR(255);
	ALTER TABLE dbo.EWPCV_MOBILITY_UPDATE_REQUEST ADD LEARNING_AGREEMENT_REVISION integer;

 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'UPDATE_REQUESTS_VIEW'
) DROP VIEW dbo.UPDATE_REQUESTS_VIEW;
GO 
CREATE VIEW dbo.UPDATE_REQUESTS_VIEW AS SELECT 
ID                   AS ID,
VERSION              AS VERSION,
SENDING_HEI_ID       AS SENDING_HEI_ID,
"TYPE"               AS "TYPE",
UPDATE_REQUEST_DATE  AS UPDATE_REQUEST_DATE,
IS_PROCESSING        AS IS_PROCESSING,
MOBILITY_TYPE        AS MOBILITY_TYPE,
UPDATE_INFORMATION   AS UPDATE_INFORMATION,
RETRIES              AS RETRIES,
PROCESSING_DATE      AS PROCESSING_DATE,
LEARNING_AGREEMENT_ID AS LA_ID,
LEARNING_AGREEMENT_REVISION AS LA_REVISION
FROM dbo.EWPCV_MOBILITY_UPDATE_REQUEST
;
GO

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/


/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
	Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACCEPT_LEARNING_AGREEMENT_PROPOSAL'
) DROP PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL;
GO 
CREATE PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID varchar(255), @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id varchar(255)
	DECLARE @v_sign_id varchar(255)
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_ACCEPT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT TOP 1 @v_mobility_id = MOBILITY_ID FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] ('+@sending_hei_id+') no es coherente con los datos registrados ('+@v_sending_hei_id+').'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] ('+@mobility_id+') no es coherente con los datos registrados ('+@v_mobility_id+').'
		END

	END


	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL, @P_LA_ID, @P_LA_REVISION
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;

;
GO

/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
	Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REJECT_LEARNING_AGREEMENT_PROPOSAL'
) DROP PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL;
GO 
CREATE PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID varchar(255), @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN


	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id varchar(255)
	DECLARE @v_sign_id varchar(255)
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_COMMENT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT TOP 1 @v_mobility_id = MOBILITY_ID FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] no es coherente con los datos registrados.'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] no es coherente con los datos registrados.'
		END

	END
		
	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_REJECT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL, @P_LA_ID, @P_LA_REVISION
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;
GO

/*

Guarda el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACCEPT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml, @P_LA_ID varchar(255), @P_LA_REVISION integer) AS

BEGIN

	DECLARE @v_id varchar(255)
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)  
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 0, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME(), @P_LA_ID, @P_LA_REVISION)

END
;
GO


/*

Guarda el xml de update la de tipo rechazo y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_REJECT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_REJECT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_REJECT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml, @P_LA_ID varchar(255), @P_LA_REVISION integer) AS

BEGIN

	DECLARE @v_id varchar(255)
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)  
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 1, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME(), @P_LA_ID, @P_LA_REVISION)

END
;
GO

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.06.03', GETDATE(), '17_UPGRADE_v01.06.03');



