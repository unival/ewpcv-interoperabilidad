
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_CONTACTS_VIEW'
) DROP VIEW dbo.INSTITUTION_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                          AS CONTACT_ID,
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				   AS CONTACT_URL,
    cont.CONTACT_ROLE                                  AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				   AS CONTACT_NAME,
    person.FIRST_NAMES                                 AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                   AS CONTACT_LAST_NAME,
    person.GENDER                                      AS CONTACT_GENDER,
    dbo.EXTRAE_TELEFONO(condet.ID)                         AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                              AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                   AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                  AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                 AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)        AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)              AS STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS MAILING_ADDRESS
    
        
        FROM dbo.EWPCV_INSTITUTION ins
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON cont.institution_id = ins.institution_id
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ins.primary_contact_detail_id IS NULL OR condet.ID <> ins.primary_contact_detail_id )
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address
        LEFT JOIN dbo.EWPCV_PERSON person ON person.id = cont.person_id
		
		/*
            Datos de la institution
        */
		LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
;
GO

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.04', GETDATE(), '24_UPGRADE_FEATURE_MEJORA_vista_institution_contacts');


