/*
	**********************************
	INICIO MODIFICACIONES SOBRE TABLAS 
	**********************************
*/

	--[EWPCV_MOBILITY]
		
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD MOBILITY_COMMENT varchar(255) NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY DROP COLUMN STATUS;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD STATUS_OUTGOING bigint NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD STATUS_INCOMING bigint NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD ACADEMIC_TERM_ID varchar(255) NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY DROP COLUMN EQF_LEVEL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD EQF_LEVEL_NOMINATION smallint NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD EQF_LEVEL_DEPARTURE smallint NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD RECEIVING_IIA_ID varchar(255) NULL;
	ALTER TABLE EWP.dbo.EWPCV_MOBILITY ADD MODIFY_DATE date NULL;
	
	GO
	
	
			
	--Nueva tabla de photo url que engancha con contact_details
	
	CREATE TABLE dbo.EWPCV_PHOTO_URL(
	PHOTO_URL_ID varchar(255) NOT NULL,
	CONTACT_DETAILS_ID varchar(255) NOT NULL,
	URL varchar(255) NULL,
	PHOTO_SIZE varchar(255) NULL,
	PHOTO_DATE date NULL,
	PHOTO_PUBLIC int NULL,
	PRIMARY KEY (PHOTO_URL_ID),
	FOREIGN KEY (CONTACT_DETAILS_ID) REFERENCES dbo.EWPCV_CONTACT_DETAILS(ID)
	);
	
GO


/*
	*********************************
	 FIN MODIFICACIONES SOBRE TABLAS 
	*********************************
*/
/*
	*************************************
	INICIO MODIFICACIONES SOBRE FUNCIONES
	*************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'ExtraerAnio'
) DROP FUNCTION dbo.ExtraerAnio;
GO
CREATE FUNCTION dbo.ExtraerAnio(@START_DATE DATETIME) RETURNS INTEGER
BEGIN
	DECLARE @anio integer
	SELECT @anio = YEAR(@START_DATE)
	RETURN @anio
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_PHOTO_URL_LIST'
) DROP FUNCTION dbo.EXTRAE_PHOTO_URL_LIST;
GO
CREATE FUNCTION dbo.EXTRAE_PHOTO_URL_LIST(@CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + PU.URL + ':' + PU.PHOTO_SIZE + ':' + CONVERT(VARCHAR(10), PU.PHOTO_DATE, 20) + ':' + PU.PHOTO_PUBLIC
			   FROM dbo.EWPCV_PHOTO_URL PU
				WHERE PU.CONTACT_DETAILS_ID = @CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

/*
	*************************************
	FIN MODIFICACIONES SOBRE FUNCIONES
	*************************************
*/

/*
	***********************************
	 INICIO MODIFICACIONES SOBRE VISTAS 
	***********************************
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_ADD_INFO_VIEW'
) DROP VIEW dbo.FACTSHEET_ADD_INFO_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_ADD_INFO_VIEW AS 
  SELECT 
	AI.ID										AS ADD_INFO_ID,
	I.INSTITUTION_ID 		 			        AS INSTITUTION_ID,
	AI."TYPE"		 					        AS INFO_TYPE,
	dbo.EXTRAE_EMAILS(AI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(AI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(AI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_INFORMATION_ITEM AI
	ON IIT.INFORMATION_ID = AI.ID
    AND AI."TYPE" NOT IN ('HOUSING','VISA','INSURANCE');
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_REQ_INFO_VIEW'
) DROP VIEW dbo.FACTSHEET_REQ_INFO_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_REQ_INFO_VIEW AS 
 SELECT 
	REI.ID										AS REQ_INFO_ID,
	I.INSTITUTION_ID 							 AS INSTITUTION_ID,
	REI."TYPE" 								     AS REQ_TYPE,
	REI.NAME 								    AS NAME,
	REI.DESCRIPTION 						     AS DESCRIPTION,
    dbo.EXTRAE_EMAILS(REI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(REI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(REI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INS_REQUIREMENTS IR
	ON IR.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_REQUIREMENTS_INFO REI
	ON IR.REQUIREMENT_ID = REI.ID;
GO

/*
	Obtiene la informacion de Institutucion y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_VIEW'
) DROP VIEW dbo.INSTITUTION_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_VIEW AS 
SELECT DISTINCT
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    ins.LOGO_URL                                       AS INSTITUTION_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID)                    AS INSTITUTION_WEBSITE,
    dbo.EXTRAE_URLS_FACTSHEET(ins.FACT_SHEET)               AS INSTITUTION_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)     AS STREET_ADDR_LINES,
    dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)           AS STREET_ADDRESS,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)    AS MAILING_ADDR_LINES,
    dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)          AS MAILING_ADDRESS
    
        /*
            Datos de la institution
        */
        FROM dbo.EWPCV_INSTITUTION ins
        LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ins.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address;
GO

/*
	Obtiene la informacion de Unidad Organizativa y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_VIEW'
) DROP VIEW dbo.OUNIT_VIEW;
GO 
CREATE VIEW dbo.OUNIT_VIEW AS 
SELECT DISTINCT
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID									 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    dbo.EXTRAE_NOMBRES_OUNIT(ounit.ID)                       AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID) 				     AS OUNIT_WEBSITE,
	dbo.EXTRAE_URLS_FACTSHEET(ounit.FACT_SHEET)			     AS OUNIT_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)       AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)             AS STREET_ADDRESS,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)      AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)            AS MAILING_ADDRESS,
	ounit.PARENT_OUNIT_ID								 AS PARENT_OUNIT_ID,
    ounit.IS_EXPOSED									 AS IS_EXPOSED 
    
        /*
            Datos de la organización
        */
        FROM dbo.EWPCV_ORGANIZATION_UNIT ounit
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id
		
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON ounit.ID = iou.ORGANIZATION_UNITS_ID
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ounit.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
		LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address;
GO	

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'MOBILITY_VIEW'
) DROP VIEW dbo.MOBILITY_VIEW;
GO 
CREATE VIEW dbo.MOBILITY_VIEW AS 
 SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL_DEPARTURE                                                                                      AS EQF_LEVEL_DEPARTURE,
	M.EQF_LEVEL_NOMINATION                                                                                     AS EQF_LEVEL_NOMINATION,
	M.IIA_ID                                                                                                   AS IIA_ID,
	M.RECEIVING_IIA_ID                                                                                         AS RECEIVING_IIA_ID,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE + '|:|' + SA.ISCED_CLARIFICATION                                                           AS SUBJECT_AREA,
	M.STATUS_OUTGOING                                                                                          AS MOBILITY_STATUS_OUTGOING,
	M.STATUS_INCOMING                                                                                          AS MOBILITY_STATUS_INCOMING,
	M.MOBILITY_COMMENT                                                                                         AS MOBILITY_COMMENT,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	dbo.EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.GLOBAL_ID                                                                                               AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	dbo.EXTRAE_PHOTO_URL_LIST(STC.CONTACT_DETAILS_ID)                                                              AS STUDENT_PHOTO_URLS,
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SC.CONTACT_DETAILS_ID)                                                                    	   AS SENDER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SCD.MAILING_ADDRESS)                                                                  AS SENDER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.MAILING_ADDRESS)                                                                        AS SENDER_CONTACT_MAILING_ADDR,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_SEN_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SACD.MAILING_ADDRESS)                                                                 AS ADMV_SEN_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.MAILING_ADDRESS)                                                                       AS ADMV_SEN_CONTACT_MAILING_ADDR,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RC.CONTACT_DETAILS_ID)                                                                    	   AS RECEIVER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RCD.MAILING_ADDRESS)                                                                  AS RECEIVER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.MAILING_ADDRESS)                                                                        AS RECEIVER_CONTACT_MAILING_ADDR,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_REC_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RACD.MAILING_ADDRESS)                                                                 AS ADMV_REC_CONTACT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.MAILING_ADDRESS)                                                                       AS ADMV_REC_CONTACT_MAILING_ADDR,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY,
	dbo.EXTRAE_ACADEMIC_TERM(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_TERM
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
LEFT JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;
GO
/*
	*********************************
	 FIN MODIFICACIONES SOBRE VISTAS 
	*********************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_MOBILITY' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="photoUrlType">
		<xs:sequence>
			<xs:element name="url" type="notEmptyStringType" minOccurs="0" />
			<xs:element name="photo_size" type="photoSizeType" minOccurs="0" />
			<xs:element name="photo_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="photo_public" >
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:enumeration value="0"/>
						<xs:enumeration value="1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="photoSizeType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d+x\d+"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="cooperation_condition_id" type="xs:string" minOccurs="0" />
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
							<xs:element name="photo_url_list" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="photo_url" maxOccurs="unbounded" minOccurs="0" type="photoUrlType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>			
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="eqf_level_nomination" type="xs:integer" minOccurs="0" />
				<xs:element name="eqf_level_departure" type="xs:integer" minOccurs="0" />
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="academic_term">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="academic_year" type="academicYearType" />
							<xs:element name="institution_id" type="notEmptyStringType" />
							<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
							<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="description" type="languageItemListType" minOccurs="0" />
							<xs:element name="term_number" type="xs:integer" />
							<xs:element name="total_terms" type="xs:integer" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="status_outgoing">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>'

GO

/*
	Valida que el iias y la coop condition indicadas existan
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY_IIA'
) DROP PROCEDURE dbo.VALIDA_MOBILITY_IIA;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY_IIA(@P_IIA_ID uniqueidentifier, @P_IS_REMOTE integer, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	DECLARE @v_count int

	SET @return_value=0

	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END

	IF @P_IIA_ID is not NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE UPPER(ID) = UPPER(@P_IIA_ID) AND IS_REMOTE = @P_IS_REMOTE
		IF @v_count = 0
		BEGIN 
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' El acuerdo interistitucional indicado no existe en el sistema.')
		END 
	END 
END
;
GO


/*
	Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_MOBILITY(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	DECLARE @IIA_ID uniqueidentifier
	DECLARE @RECEIVING_IIA_ID uniqueidentifier
	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml

	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END

	SELECT
		 @IIA_ID = T.c.value('(iia_id)[1]','uniqueidentifier'),
		 @RECEIVING_IIA_ID = T.c.value('(receiving_iia_id)[1]','uniqueidentifier'),
		 @SENDING_INSTITUTION =  T.c.query('sending_institution'),
		 @RECEIVING_INSTITUTION =  T.c.query('receiving_institution')
	FROM @P_MOBILITY_XML.nodes('/mobility') T(c) 

	IF @IIA_ID is not null 
		EXECUTE dbo.VALIDA_MOBILITY_IIA @IIA_ID, 0, @P_ERROR_MESSAGE output, @return_value output
	
	IF @return_value = 0 and @RECEIVING_IIA_ID is not null
		EXECUTE dbo.VALIDA_MOBILITY_IIA @RECEIVING_IIA_ID, 1, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY_INST @SENDING_INSTITUTION, @RECEIVING_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
	END 
	
END
;
GO

/*
	Valida que existan los institution id y ounit id de los academic term de los componentes del LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_ACADEMIC_TERM_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_ACADEMIC_TERM_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_ACADEMIC_TERM_INSTITUTION(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
DECLARE @COMPONENTS_LIST xml
DECLARE @v_mobility_id uniqueidentifier
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_r_inst_id varchar(255)

DECLARE @v_component_type integer
DECLARE @v_inst_id varchar(255)
DECLARE @v_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value=0
	
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	--obtenemos datos del xml
	SELECT 
		@v_mobility_id =T.c.value('(mobility_id)[1]', 'uniqueidentifier'),
		@COMPONENTS_LIST = T.c.query('la_component_list')
	FROM @P_LA_XML.nodes('learning_agreement') T(c)
	
	--obtenemos sender y reciver de la ultima version de la movilidad
	SELECT @v_s_inst_id  = SENDING_INSTITUTION_ID, @v_r_inst_id = RECEIVING_INSTITUTION_ID 
	FROM EWPCV_MOBILITY 
	WHERE ID = @v_mobility_id AND MOBILITY_REVISION = (
		SELECT MAX(MOBILITY_REVISION) FROM EWPCV_MOBILITY 
		WHERE ID = @v_mobility_id)
	
	--cursor para extraer informacion de cada componente
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(la_component_type)[1]', 'integer'),
		T.c.value('(academic_term/institution_id)[1]', 'varchar(255)'),
		T.c.value('(academic_term/organization_unit_code)[1]', 'varchar(255)')
		FROM @COMPONENTS_LIST.nodes('la_component_list/la_component') T(c)
	
	OPEN cur
	FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--componentes estudiados o virtuales
			IF @v_component_type = 0 OR @v_component_type = 5 
			BEGIN
				
				IF @v_inst_id != @v_r_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo estudiado/virtual no coincide con la institucion ' + @v_r_inst_id + ' de la entidad receptora')
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.')
					END
				END
			END
			--componentes reconocidos
			ELSE IF @v_component_type = 1
			BEGIN
				IF @v_inst_id != @v_s_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo reconocido no coincide con la institucion ' + @v_s_inst_id + ' de la entidad emisora')
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.')
					END
				END
			END
			FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
		END
	CLOSE cur
	DEALLOCATE cur

END 
;
GO

/*
	Valida la calidad del dato de la entrada al aprovisionamiento de learning agreements
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LA'
) DROP PROCEDURE dbo.VALIDA_DATOS_LA;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_LA(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	EXECUTE dbo.VALIDA_ACADEMIC_TERM_INSTITUTION @P_LA_XML, @P_ERROR_MESSAGE output, @return_value output
	
END
;
GO

/*
	Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_PARTICIPANT'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_PARTICIPANT;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_PARTICIPANT(@P_STUDENT xml, @return_value varchar(255) output) AS
BEGIN

	DECLARE @v_c_id uniqueidentifier
	DECLARE @v_m_p_id uniqueidentifier
	DECLARE @v_c_d_id uniqueidentifier
	DECLARE @v_p_id uniqueidentifier

	DECLARE @GLOBAL_ID varchar(255)
	DECLARE @CONTACT_PERSON xml
	DECLARE @PHOTO_URL_LIST xml

	DECLARE @PHOTO_URL varchar(255)
	DECLARE @PHOTO_SIZE varchar(255) 
	DECLARE	@PHOTO_DATE date
	DECLARE @PHOTO_PUBLIC integer

	SET @v_m_p_id = NEWID()

	SELECT 
		@GLOBAL_ID = T.c.value('(global_id)[1]','varchar(255)'),
		@CONTACT_PERSON = T.c.query('contact_person'),
		@PHOTO_URL_LIST = T.c.query('photo_url_list')
	FROM @P_STUDENT.nodes('student') T(c)

	EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, null, null, @v_c_id output
	INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, CONTACT_ID, GLOBAL_ID) VALUES (@v_m_p_id, @v_c_id, @GLOBAL_ID)

	SELECT @v_c_d_id = CONTACT_DETAILS_ID FROM EWPCV_CONTACT C WHERE C.ID = @v_c_id

	IF @v_c_d_id IS NOT NULL
	BEGIN
		-- FOTOS
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @PHOTO_URL_LIST.nodes('photo_url_list/photo_url') T(c) 		
		OPEN cur
		FETCH NEXT FROM cur INTO @PHOTO_URL_LIST
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_p_id = NEWID()

				SELECT
					 @PHOTO_URL = T.c.value('(url)[1]','varchar(255)'),
					 @PHOTO_SIZE = T.c.value('(photo_size)[1]','varchar(255)'),
					 @PHOTO_DATE = T.c.value('(photo_date)[1]','date'),
					 @PHOTO_PUBLIC = T.c.value('(photo_public)[1]','int')
				FROM @PHOTO_URL_LIST.nodes('/photo_url') T(c) 

				INSERT INTO dbo.EWPCV_PHOTO_URL (
					PHOTO_URL_ID, CONTACT_DETAILS_ID, 
					URL, PHOTO_SIZE, 
					PHOTO_DATE, PHOTO_PUBLIC)
				VALUES (@v_p_id, @v_c_d_id, 
					@PHOTO_URL, @PHOTO_SIZE, 
					@PHOTO_DATE, @PHOTO_PUBLIC)

				FETCH NEXT FROM cur INTO @PHOTO_URL_LIST
			END
		CLOSE cur
		DEALLOCATE cur

	END
	   
	SET @return_value = @v_m_p_id

END
;
GO

/*
	Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY'
) DROP PROCEDURE dbo.INSERTA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY(@P_MOBILITY xml, @P_REVISION integer, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @v_s_ounit_id uniqueidentifier
	DECLARE @v_r_ounit_id uniqueidentifier
	DECLARE @v_student_id varchar(255)
	DECLARE @v_s_contact_id uniqueidentifier
	DECLARE @v_s_admv_contact_id uniqueidentifier
	DECLARE @v_r_contact_id uniqueidentifier
	DECLARE @v_r_admv_contact_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_isced_code varchar(255)
	DECLARE @v_academic_term_id varchar(255)
	DECLARE @v_status_incoming integer
	DECLARE @v_status_outgoing integer

	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	DECLARE @STUDENT xml
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml
	DECLARE @SUBJECT_AREA xml
	DECLARE @MOBILITY_TYPE varchar(255)
	DECLARE @ACTUAL_ARRIVAL_DATE date
	DECLARE @ACTUAL_DEPATURE_DATE date
	DECLARE @IIA_ID varchar(255)
	DECLARE @PLANED_ARRIVAL_DATE date
	DECLARE @PLANED_DEPATURE_DATE date
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @STATUS_OUTGOING integer
	DECLARE @STUDENT_LANGUAGE_SKILL_LIST xml
	DECLARE @STUDENT_LANGUAGE_SKILL xml
	DECLARE @ACADEMIC_TERM xml
	DECLARE @EQF_LEVEL_DEPARTURE integer
	DECLARE @EQF_LEVEL_NOMINATION integer
	DECLARE @RECEIVING_IIA_ID varchar(255)

	SELECT 
		@SENDING_INSTITUTION = T.c.query('sending_institution'),
		@RECEIVING_INSTITUTION = T.c.query('receiving_institution'),
		@STUDENT = T.c.query('student'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@SUBJECT_AREA = T.c.query('subject_area'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)'),
		@ACTUAL_ARRIVAL_DATE = T.c.value('(actual_arrival_date)[1]', 'date'),
		@ACTUAL_DEPATURE_DATE = T.c.value('(actual_depature_date)[1]', 'date'),
		@PLANED_ARRIVAL_DATE = T.c.value('(planed_arrival_date)[1]', 'date'),
		@PLANED_DEPATURE_DATE = T.c.value('(planed_depature_date)[1]', 'date'),
		@IIA_ID = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@SENDING_INSTITUTION_ID = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]', 'integer'),
		@STUDENT_LANGUAGE_SKILL_LIST = T.c.query('student_language_skill_list'),
		@ACADEMIC_TERM = T.c.query('academic_term'),
		@EQF_LEVEL_DEPARTURE = T.c.value('(eqf_level_departure)[1]', 'integer'),
		@EQF_LEVEL_NOMINATION = T.c.value('(eqf_level_nomination)[1]', 'integer'),
		@RECEIVING_IIA_ID = T.c.value('(receiving_iia_id)[1]', 'varchar(255)')

	FROM @P_MOBILITY.nodes('mobility') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @SENDING_INSTITUTION , @v_s_ounit_id output
	EXECUTE dbo.INSERTA_INST_OUNIT @RECEIVING_INSTITUTION , @v_r_ounit_id output
	EXECUTE dbo.INSERTA_MOBILITY_PARTICIPANT @STUDENT , @v_student_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT , null, null, @v_s_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT , null, null, @v_s_admv_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT , null, null, @v_r_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT , null, null, @v_r_admv_contact_id output

	EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA , @v_isced_code output
	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE , @v_mobility_type_id output
	
	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM , @v_academic_term_id output

	-- Por defecto el status_outgoing es NOMINATION
	IF @STATUS_OUTGOING is null 
		SET @v_status_outgoing = 2
	ELSE
		SET @v_status_outgoing = @STATUS_OUTGOING

	IF @v_id is null
		SET @v_id = NEWID()


	INSERT INTO dbo.EWPCV_MOBILITY (ID,
			ACTUAL_ARRIVAL_DATE,
			ACTUAL_DEPARTURE_DATE,
			IIA_ID,
			RECEIVING_IIA_ID,
			ISCED_CODE,
			MOBILITY_PARTICIPANT_ID,
			MOBILITY_REVISION,
			PLANNED_ARRIVAL_DATE,
			PLANNED_DEPARTURE_DATE,
			RECEIVING_INSTITUTION_ID,
			RECEIVING_ORGANIZATION_UNIT_ID,
			SENDING_INSTITUTION_ID,
			SENDING_ORGANIZATION_UNIT_ID,
			MOBILITY_TYPE_ID,
			SENDER_CONTACT_ID,
			SENDER_ADMV_CONTACT_ID,
			RECEIVER_CONTACT_ID,
			RECEIVER_ADMV_CONTACT_ID,
			ACADEMIC_TERM_ID,
			EQF_LEVEL_DEPARTURE,
			EQF_LEVEL_NOMINATION,
			STATUS_OUTGOING,
			MODIFY_DATE)
			VALUES (@v_id,
			@ACTUAL_ARRIVAL_DATE,
			@ACTUAL_DEPATURE_DATE,
			@IIA_ID,
			@RECEIVING_IIA_ID,
			@v_isced_code,
			@v_student_id,
			(@P_REVISION +1),
			@PLANED_ARRIVAL_DATE,
			@PLANED_DEPATURE_DATE,
			@RECEIVING_INSTITUTION_ID,
			@v_r_ounit_id,
			@SENDING_INSTITUTION_ID,
			@v_s_ounit_id,
			@v_mobility_type_id,
			@v_s_contact_id,
			@v_s_admv_contact_id,
			@v_r_contact_id,
			@v_r_admv_contact_id,
			@v_academic_term_id,
			@EQF_LEVEL_DEPARTURE,
			@EQF_LEVEL_NOMINATION,
			@v_status_outgoing,
			SYSDATETIME())


	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @STUDENT_LANGUAGE_SKILL_LIST.nodes('student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (@v_id, (@P_REVISION + 1), @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/* 
	Borra el estudiante de la tabla mobility participant
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_STUDENT'
) DROP PROCEDURE dbo.BORRA_STUDENT;
GO 
CREATE PROCEDURE  dbo.BORRA_STUDENT (@P_ID VARCHAR(255)) AS
BEGIN
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_contact_details_id uniqueidentifier

	SELECT @v_contact_id = CONTACT_ID FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	SELECT @v_contact_details_id = CONTACT_DETAILS_ID FROM dbo.EWPCV_CONTACT WHERE ID = @v_contact_id
	DELETE FROM dbo.EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = @v_contact_details_id
	DELETE FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	EXECUTE dbo.BORRA_CONTACT @v_contact_id

END
;
GO


/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
	Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
	Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_MOBILITY'
) DROP PROCEDURE dbo.INSERT_MOBILITY;
GO 
CREATE PROCEDURE dbo.INSERT_MOBILITY(@P_MOBILITY xml, @P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer
	
	IF @P_HEI_TO_NOTIFY IS NULL
	BEGIN
	  SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios';
	  SET @return_value = -1
	END
    ELSE
	BEGIN
		EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
		SELECT
			@v_sending_hei = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]', 'integer')
		FROM @P_MOBILITY.nodes('mobility') T(c)	

		IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY)
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.'
			SET @return_value = -1
		END

		IF @return_value = 0
		BEGIN
			EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
		END 

		IF @return_value = 0
		BEGIN
			SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)
			EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
		END 

		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION
				EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, -1, @P_OMOBILITY_ID OUTPUT
				EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_MOBILITY'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
END
;
GO

/* Elimina una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a eliminar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_MOBILITY'
) DROP PROCEDURE dbo.DELETE_MOBILITY;
GO 
CREATE PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_m_revision integer
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		SET @return_value = 0
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count >0
			BEGIN
				SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.'
					SET @return_value = -1
				END
				ELSE
				BEGIN
					EXECUTE dbo.BORRA_MOBILITY @P_OMOBILITY_ID
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END
			END
		ELSE
			BEGIN
				SET @P_ERROR_MESSAGE = 'La movilidad no existe'
				SET @return_value = -1
			END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_MOBILITY'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/* Actualiza una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a actualizar
	Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_MOBILITY'
) DROP PROCEDURE dbo.UPDATE_MOBILITY;
GO 
CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE
		BEGIN
			IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			BEGIN
				SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                SET @return_value = -1
			END
 		END
	END 

	IF @return_value = 0
	BEGIN
		SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)

		SELECT
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]','integer')
		FROM @P_MOBILITY.nodes('/mobility') T(c) 

		EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
	END 
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


-- Funcion ES_OMOBILITY
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'ES_OMOBILITY'
) DROP FUNCTION dbo.ES_OMOBILITY;
GO  
CREATE FUNCTION dbo.ES_OMOBILITY(@P_MOBILITY XML, @P_HEI_TO_NOTIFY varchar(255)) RETURNS integer
BEGIN
	DECLARE @return_value integer
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	SELECT
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)')
	FROM @P_MOBILITY.nodes('mobility') T(c)	

	IF @RECEIVING_INSTITUTION_ID <> @P_HEI_TO_NOTIFY
		SET @return_value = -1
	ELSE
		SET @return_value = 0
	return @return_value
END
;

GO

-- Procedure VALIDA_STATUS
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_STATUS'
) DROP PROCEDURE dbo.VALIDA_STATUS;
GO 
CREATE PROCEDURE dbo.VALIDA_STATUS(@P_STATUS int, @P_ES_OUTGOING int, @P_ERROR_MESSAGE varchar(255) output, @return_value int output) AS
BEGIN
	SET @return_value = 0
	IF @P_ES_OUTGOING = 0 
	BEGIN
		IF @P_STATUS NOT in (0, 1, 2, 3)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_outgoing no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING = -1
	BEGIN
		IF @P_STATUS NOT in (4, 5, 6)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_incoming no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING NOT in (-1, 0)
	BEGIN
		SET @P_ERROR_MESSAGE = '@P_ES_OUTGOING debe especificar si es un status_incoming (-1) o status_outgoing (0).'
		SET @return_value = -1
	END 
END
;
GO


	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY_INST'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST(@P_S_INSTITUTION xml, @P_R_INSTITUTION  xml, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS 
BEGIN
	DECLARE @v_count int
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_OUnit_CODE varchar(255)	
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @RECEIVING_OUnit_CODE varchar(255)

	SET @return_value = 0

	SELECT 
		@SENDING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@SENDING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_S_INSTITUTION.nodes('sending_institution') T(c)	
	SELECT 
		@RECEIVING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@RECEIVING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_R_INSTITUTION.nodes('receiving_institution') T(c)

	-- validacion de sender 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@SENDING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @SENDING_INSTITUTION_ID + ' informada como sender institution no existe en el sistema.')
	END 
	ELSE IF @SENDING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@SENDING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@SENDING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @SENDING_OUnit_CODE + 'vinculada a ' + @SENDING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

	-- validacion de receiver 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@RECEIVING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @RECEIVING_INSTITUTION_ID + ' informada como receiver institution no existe en el sistema.')
	END 
	ELSE IF @RECEIVING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@RECEIVING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@RECEIVING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @RECEIVING_OUnit_CODE + 'vinculada a ' + @RECEIVING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

END
;
GO
	


/* Cancela una movilidad
    Recibe como parametro el identificador de la movilidad
    Admite un parametro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CANCEL_MOBILITY'
) DROP PROCEDURE dbo.CANCEL_MOBILITY;
GO 
CREATE PROCEDURE dbo.CANCEL_MOBILITY(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output)
  AS
   
  BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_resultado integer
	DECLARE @v_count integer
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = count(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count > 0
		BEGIN
		  SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		  EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
		  IF @v_notifier_hei IS NULL 
		  BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.'
			SET @return_value = -1
		  END
	  
		  UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
    
		  EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
		END
		ELSE
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
  END
;
GO




/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_DEPARTURE'
) DROP PROCEDURE dbo.STUDENT_DEPARTURE;
GO 
CREATE PROCEDURE dbo.STUDENT_DEPARTURE(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value <> 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_DEPARTURE'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO



	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
            
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_ARRIVAL'
) DROP PROCEDURE dbo.STUDENT_ARRIVAL;
GO 
CREATE PROCEDURE dbo.STUDENT_ARRIVAL(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value <> 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_ARRIVAL'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO



	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'APPROVE_NOMINATION'
) DROP PROCEDURE dbo.APPROVE_NOMINATION;
GO 
CREATE PROCEDURE dbo.APPROVE_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF UPPER(@v_notifier_hei) <> UPPER(@P_HEI_TO_NOTIFY)
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value <> 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.APPROVE_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO




	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REJECT_NOMINATION'
) DROP PROCEDURE dbo.REJECT_NOMINATION;
GO 
CREATE PROCEDURE dbo.REJECT_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF UPPER(@v_notifier_hei) <> UPPER(@P_HEI_TO_NOTIFY)
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value <> 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO




	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_ACTUAL_DATES'
) DROP PROCEDURE dbo.UPDATE_ACTUAL_DATES;
GO 
CREATE PROCEDURE dbo.UPDATE_ACTUAL_DATES(@P_OMOBILITY_ID varchar(255), @P_ACTUAL_DEPARTURE date, @P_ACTUAL_ARRIVAL date, @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF UPPER(@v_notifier_hei) <> UPPER(@P_HEI_TO_NOTIFY)
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value <> 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME(), 
							ACTUAL_ARRIVAL_DATE = @P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = @P_ACTUAL_DEPARTURE
							WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_ACTUAL_DATES'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.04.00', GETDATE(), '11_UPGRADE_v01.04.00');


