
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/


/*  
	Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
	y hash de las cooperation condition del iia remoto en nuestro IIA propio.
	Recibe como parámetro el identificador propio y el identificador de la copia remota.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BIND_IIA'
) DROP PROCEDURE dbo.BIND_IIA;
GO 
CREATE PROCEDURE  dbo.BIND_IIA(@P_OWN_IIA_ID varchar(255), @P_ID_FROM_REMOTE_IIA varchar(255), 
	@P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_remote_iia_id varchar(255)
		DECLARE @v_remote_coop_cond_hash varchar(255)
		DECLARE @v_remote_iia_code varchar(255)
        DECLARE @v_id varchar(255)
		DECLARE @v_count_iia integer

		SET @return_value = 0

		IF @P_OWN_IIA_ID IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END
		ELSE
			IF @P_ID_FROM_REMOTE_IIA IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA remoto';
				SET @return_value = -1
			END 

		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_OWN_IIA_ID
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_OWN_IIA_ID';
				SET @return_value = -1
			END 
		END 

		IF @return_value = 0 
		BEGIN
			SELECT
				@v_remote_iia_id = IIA.REMOTE_IIA_ID, 
				@v_remote_coop_cond_hash = IIA.REMOTE_COP_COND_HASH, 
				@v_remote_iia_code = IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA 
				WHERE IIA.ID = @P_ID_FROM_REMOTE_IIA 

			IF @v_remote_iia_id is null 
				BEGIN
					SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
					SET @return_value = -1
				END 
			ELSE
				UPDATE EWPCV_IIA SET 
					REMOTE_IIA_ID = @v_remote_iia_id, 
					REMOTE_IIA_CODE = @v_remote_iia_code,
					MODIFY_DATE =  SYSDATETIME()
					WHERE ID = @P_OWN_IIA_ID
		END 
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.BIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/*  
	Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
	y hash de las cooperation condition del iia remoto en nuestro IIA propio.
	Recibe como parámetro el identificador propio y el identificador de la copia remota.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UNBIND_IIA'
) DROP PROCEDURE dbo.UNBIND_IIA;
GO 
CREATE PROCEDURE  dbo.UNBIND_IIA(@P_IIA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @v_count_iia integer
		SET @return_value = 0

		IF @P_IIA_ID IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END
		
		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 0;
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA LOCAL asociado al id que se ha informado';
				SET @return_value = -1
			END 
		END 

		IF @return_value = 0 
		BEGIN
				UPDATE EWPCV_IIA SET 
					REMOTE_IIA_ID = null,
					REMOTE_IIA_CODE = null,
					MODIFY_DATE =  SYSDATETIME()
					WHERE ID = @P_IIA_ID
		END 
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UNBIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO


/*
    *************************************
    FIN MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/


/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.02', GETDATE(), '19_UPGRADE_v01.08.02');



