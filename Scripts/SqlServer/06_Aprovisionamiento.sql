/*
***************************************
******** Funciones Comunes ************
***************************************
*/
	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_LANGUAGE_ITEM(@P_LANGUAGE_ITEM xml, @v_id uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255)
	SET @lang = @P_LANGUAGE_ITEM.value('(/text/@lang)[1]','varchar(255)')
	SET @text = @P_LANGUAGE_ITEM.value('(/text)[1]','varchar(255)')
	if (@lang is not null) OR (@text is not null)
	BEGIN
		SET @v_id = NEWID()  
		INSERT INTO dbo.EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (@v_id, @lang, @text)
	END
END
;
GO


	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
*/


CREATE PROCEDURE  dbo.INSERTA_NOTIFICATION(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255)) as
BEGIN
	INSERT INTO dbo.EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (NEWID(), @P_ELEMENT_ID, UPPER(@P_NOTIFY_HEI), SYSDATETIME(), @P_TYPE, 1, 0, UPPER(@P_NOTIFIER_HEI) );
END 
;


GO


	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
*/

CREATE PROCEDURE  dbo.INSERTA_PERSONA(@P_PERSON xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
DECLARE @GIVEN_NAME varchar(255)
DECLARE @FAMILY_NAME varchar(255)
DECLARE @CITIZENSHIP varchar(255)
DECLARE @BIRTH_DATE date
DECLARE @GENDER varchar(255)
DECLARE @v_id uniqueidentifier

	SELECT
		@GIVEN_NAME = T.c.value('given_name[1]','varchar(255)'),
		@FAMILY_NAME = T.c.value('family_name[1]','varchar(255)'),
		@CITIZENSHIP = T.c.value('citizenship[1]','varchar(255)'),
		@BIRTH_DATE = T.c.value('birth_date[1]','date'),
		@GENDER = T.c.value('gender[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*') T(c) 

	IF @GIVEN_NAME IS NOT NULL 
			OR @FAMILY_NAME IS NOT NULL 
			OR @BIRTH_DATE IS NOT NULL 
			OR @CITIZENSHIP IS NOT NULL 
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
				VALUES (@v_id, @GIVEN_NAME, @FAMILY_NAME, @CITIZENSHIP, @BIRTH_DATE, @GENDER)
			SET @return_value = @v_id
		END
END
;
GO

	/*
		Inserta detalles de contacto.
*/

CREATE PROCEDURE  dbo.INSERTA_CONTACT_DETAILS(@P_CONTACT xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
		DECLARE @v_id uniqueidentifier
		DECLARE @v_p_id uniqueidentifier
		DECLARE @v_a_id uniqueidentifier
		DECLARE @v_li_id uniqueidentifier
		DECLARE @PHONE XML
		DECLARE @STREET_ADDRESS XML
		DECLARE @CONTACT_URL XML
		DECLARE @EMAIL_LIST XML
		DECLARE @email varchar(255)
		DECLARE @item xml

		SELECT 
			 @PHONE = T.c.query('phone'),
			 @STREET_ADDRESS = T.c.query('street_address'),	
			 @EMAIL_LIST = T.c.query('email_list'),
			 @CONTACT_URL = T.c.query('contact_url')
		FROM  @P_CONTACT.nodes('contact') T(c)

		EXECUTE dbo.INSERTA_TELEFONO @PHONE, @v_p_id OUTPUT
		EXECUTE dbo.INSERTA_FLEXIBLE_ADDRES @STREET_ADDRESS, @v_a_id OUTPUT

		IF @v_p_id IS NOT NULL OR @v_a_id IS NOT NULL OR @CONTACT_URL IS NOT NULL OR @EMAIL_LIST IS NOT NULL
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT_DETAILS (ID, PHONE_NUMBER, STREET_ADDRESS) VALUES (@v_id, @v_p_id, @v_a_id)

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('contact/email_list/email') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @EMAIL_LIST
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@email = T.c.value('(.)[1]','varchar(255)')
					FROM @EMAIL_LIST.nodes('/email') T(c) 
					INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (@v_id, @email);
					FETCH NEXT FROM cur INTO @EMAIL_LIST
				END
			CLOSE cur
			DEALLOCATE cur

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('contact/contact_url/text') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @CONTACT_URL
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.query('.')
					FROM @CONTACT_URL.nodes('/text') T(c) 
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
					INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (@v_id, @v_li_id);
					FETCH NEXT FROM cur INTO @CONTACT_URL
				END
			CLOSE cur
			DEALLOCATE cur

		END
		SET @return_value = @v_id

END
;
GO

	/*
		Inserta datos de contacto y de persona 
	*/

CREATE PROCEDURE  dbo.INSERTA_CONTACT_PERSON(@P_PERSON xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
DECLARE @GENDER varchar(255)

DECLARE @CONTACT xml
DECLARE @CONTACT_NAME xml
DECLARE @CONTACT_DESCRIPTION xml
DECLARE @INSTITUTION_ID varchar(255)
DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
DECLARE @CONTACT_ROLE varchar(255)
DECLARE @v_p_id uniqueidentifier
DECLARE @v_c_id uniqueidentifier
DECLARE @v_ounit_id uniqueidentifier
DECLARE @v_id uniqueidentifier
DECLARE @v_li_id uniqueidentifier
DECLARE @item xml

	SELECT
		@CONTACT = T.c.query('.'),
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@INSTITUTION_ID = T.c.value('institution_id[1]','varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('organization_unit_code[1]','varchar(255)'),
		@CONTACT_ROLE = T.c.value('contact_role[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*/*') T(c) 

	EXECUTE dbo.INSERTA_PERSONA @P_PERSON, @v_p_id OUTPUT
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @CONTACT, @v_c_id OUTPUT

	IF @v_p_id is not null or @v_c_id is not null or 
		@CONTACT_NAME is not null or @CONTACT_DESCRIPTION is not null 
	BEGIN

		SELECT @v_ounit_id = tabO.ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.ID
			INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
			WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
			AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)

		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @INSTITUTION_ID, @v_ounit_id, @CONTACT_ROLE, @v_c_id, @v_p_id);

		-- Lista de contact name
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_NAME.nodes('contact_name/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

		-- Lista de contact description
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

	END

	SET @return_value = @v_id

END
;
GO

/* Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
*/


CREATE PROCEDURE  dbo.BORRA_CONTACT_DETAILS(@P_ID uniqueidentifier) AS 
BEGIN

	DECLARE @v_phone_id uniqueidentifier
	DECLARE @v_mailing_id uniqueidentifier
	DECLARE @v_street_id uniqueidentifier
	DECLARE @v_url_id uniqueidentifier


	SELECT @v_phone_id=PHONE_NUMBER, @v_mailing_id=MAILING_ADDRESS, @v_street_id=STREET_ADDRESS
		FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DECLARE cur CURSOR LOCAL FOR
		SELECT URL_ID FROM dbo.EWPCV_CONTACT_URL WHERE CONTACT_DETAILS_ID=@P_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @v_url_id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_CONTACT_URL WHERE URL_ID = @v_url_id
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @v_url_id
		FETCH NEXT FROM cur INTO @v_url_id
	END
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = @P_ID

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DELETE FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @v_phone_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_mailing_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_street_id

END
;
GO



/* Persiste una lista de nombres de institucion
	*/

CREATE PROCEDURE  dbo.INSERTA_INSTITUTION_NAMES(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id uniqueidentifier
	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	DECLARE @v_count integer
	
	IF @P_INSTITUTION_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_INSTITUTION_NAMES.nodes('/institution_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')
			
			SELECT @v_count=count(1) FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_INSTITUTION_NAME ina on lait.ID = ina.NAME_ID
				WHERE lait.text=@text and lait.lang=@lang and ina.INSTITUTION_ID = @P_INSTITUTION_ID
			if @v_lang_item_id is null
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				SET @v_count = 1
			END
			-- Comprobar si el texto ya está asignado a la institución
			SELECT @v_count=count(1) FROM dbo.EWPCV_INSTITUTION_NAME WHERE NAME_ID=@v_lang_item_id and INSTITUTION_ID=@P_INSTITUTION_ID
			IF @v_lang_item_id is not null AND @v_count = 0 
			BEGIN
				INSERT INTO dbo.EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES(@v_lang_item_id, @P_INSTITUTION_ID)
			END
		
			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;

GO


	/*
		Persiste una lista de nombres de organizacion
*/

CREATE PROCEDURE  dbo.INSERTA_OUNIT_NAMES(@P_OUNIT_ID uniqueidentifier, @P_ORGANIZATION_UNIT_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id uniqueidentifier
	DECLARE @v_count integer

	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	
	IF @P_ORGANIZATION_UNIT_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_ORGANIZATION_UNIT_NAMES.nodes('/organization_unit_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')

			SELECT @v_lang_item_id = ID FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_ORGANIZATION_UNIT_NAME ona on lait.ID = ona.ORGANIZATION_UNIT_ID
				WHERE lait.text=@text and lait.lang=@lang and ona.ORGANIZATION_UNIT_ID = @P_OUNIT_ID

			if @v_lang_item_id is null
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				SET @v_count = 1
			END

			-- Comprobar si el texto ya está asignado a la institución
			SELECT @v_count=count(1) FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID=@v_lang_item_id and ORGANIZATION_UNIT_ID=@P_OUNIT_ID

			IF @v_lang_item_id is not null AND @v_count = 0 
			BEGIN
				INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES(@v_lang_item_id, @P_OUNIT_ID)
			END

			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;
GO

/* Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/

CREATE PROCEDURE  dbo.INSERTA_TELEFONO(@P_PHONE xml, @v_id uniqueidentifier OUTPUT) as
BEGIN
	IF (@P_PHONE.value('(/phone/e164)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/phone/extension_number)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/phone/other_format)[1]', 'varchar(255)') IS NOT NULL)
	BEGIN
		SET @v_id = NEWID()  
		INSERT INTO dbo.EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
						VALUES (@v_id, @P_PHONE.value('(/phone/e164)[1]', 'varchar(255)'), @P_PHONE.value('(/phone/extension_number)[1]', 'varchar(255)'), @P_PHONE.value('(/phone/other_format)[1]', 'varchar(255)'))
	END 
END
;
GO

	/*
		Borra un academic term
*/	

CREATE PROCEDURE  dbo.BORRA_ACADEMIC_TERM(@P_ID  uniqueidentifier) AS
BEGIN
	DECLARE @DISP_NAME_ID uniqueidentifier
	DECLARE @v_ay_id uniqueidentifier
	DECLARE @v_ay_count integer

	DECLARE cur CURSOR LOCAL FOR
		SELECT DISP_NAME_ID
			FROM dbo.EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = @P_ID
	
	OPEN cur
	FETCH NEXT FROM cur INTO @DISP_NAME_ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = @DISP_NAME_ID
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DISP_NAME_ID
		FETCH NEXT FROM cur INTO @DISP_NAME_ID
	END 
	CLOSE cur
	DEALLOCATE cur

	SELECT @v_ay_id = ACADEMIC_YEAR_ID
			FROM dbo.EWPCV_ACADEMIC_TERM WHERE ID = @P_ID
	
	DELETE FROM dbo.EWPCV_ACADEMIC_TERM WHERE ID = @P_ID

	SELECT @v_ay_count = COUNT(1) FROM dbo.EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = @v_ay_id
	IF @v_ay_count = 0 
	BEGIN
		DELETE FROM dbo.EWPCV_ACADEMIC_YEAR WHERE ID = @v_ay_id
	END

END
;
GO


	/*
		Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
	*/	

CREATE PROCEDURE  dbo.INSERTA_FLEXIBLE_ADDRES(@P_ADDRESS xml, @return_value varchar(255) OUTPUT) as
BEGIN
DECLARE @building_number integer
DECLARE @building_name varchar(255)
DECLARE @street_name varchar(255)
DECLARE @unit varchar(255)
DECLARE @building_floor varchar(255)
DECLARE @post_office_box varchar(255)
DECLARE @postal_code varchar(255)
DECLARE @locality varchar(255)
DECLARE @region varchar(255)
DECLARE @country varchar(255)
DECLARE @list xml
DECLARE @item varchar(255)
DECLARE @ID uniqueidentifier

	SELECT
		@building_number = T.c.value('building_number[1]','varchar(255)'),
		@building_name = T.c.value('building_name[1]','varchar(255)'),
		@street_name = T.c.value('street_name[1]','varchar(255)'),
		@unit = T.c.value('unit[1]','varchar(255)'),
		@building_floor = T.c.value('building_floor[1]','varchar(255)'),
		@post_office_box = T.c.value('post_office_box[1]','varchar(255)'),
		@postal_code = T.c.value('postal_code[1]','varchar(255)'),
		@locality = T.c.value('locality[1]','varchar(255)'),
		@region = T.c.value('region[1]','varchar(255)'),
		@country = T.c.value('country[1]','varchar(255)')
	FROM  @P_ADDRESS.nodes('/street_address') T(c) 

	IF @postal_code is not null 
		or @locality is not null 
		or @region is not null 
		or @country is not null
		BEGIN 
			SET @ID = NEWID()
			-- INSERT
			INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (@ID, @building_number, @building_name, @country,
					@building_floor, @locality, @post_office_box, @postal_code,
					@region, @street_name, @unit )

			-- Lista recipient_name
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/street_address/recipient_name_list/recipient_name') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/recipient_name') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) 
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista address_line
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/street_address/address_line_list/address_line') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/address_line') T(c) 
					FETCH NEXT FROM cur INTO @list
					INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista delivery_point
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/street_address/delivery_point_code_list/delivery_point_code') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/delivery_point_code') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

		END 
		SET @return_value = @ID
END
;

GO


	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
*/
CREATE PROCEDURE  dbo.INSERTA_INST_OUNIT(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_inst_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @INSTITUTION_NAME xml

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@INSTITUTION_NAME = T.c.query('(institution_name)')
	FROM @P_INSTITUTION.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, null, null, null, @INSTITUTION_NAME, @v_inst_id OUTPUT
	EXECUTE dbo.INSERTA_OUNIT @P_INSTITUTION, @v_ou_id OUTPUT
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @v_ou_id
	IF @v_count = 0
		INSERT INTO dbo.EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (@v_inst_id, @v_ou_id)

	SET @return_value = @v_ou_id

END
;
GO


	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/

CREATE PROCEDURE  dbo.INSERTA_OUNIT(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_CODE VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_NAME xml

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_NAME = T.c.query('(organization_unit_name)')
	FROM @P_INSTITUTION.nodes('*') T(c)
	
	SELECT @v_id = O.ID
			FROM EWPCV_ORGANIZATION_UNIT O
			INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
			INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)
			AND UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)

	IF @v_id is null 
	BEGIN
		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (@v_id, @ORGANIZATION_UNIT_CODE)
	END

	EXECUTE dbo.INSERTA_OUNIT_NAMES @v_id, @ORGANIZATION_UNIT_NAME
	SET @return_value = @v_id

END
;
GO


	/*
	 Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
	 */

CREATE PROCEDURE  dbo.INSERTA_LANGUAGE_SKILL(@P_LANGUAGE_SKILL xml, @return_value uniqueidentifier OUTPUT) as
BEGIN

	DECLARE @v_id VARCHAR(255)
	DECLARE @LANG VARCHAR(255) 
	DECLARE @CEFR_LEVEL VARCHAR(255)

		SELECT 
		@LANG = T.c.value('(language)[1]','varchar(255)'),
		@CEFR_LEVEL = T.c.value('(cefr_level)[1]','varchar(255)')
	FROM @P_LANGUAGE_SKILL.nodes('*') T(c)
	
	SELECT @v_id = ID
		FROM dbo.EWPCV_LANGUAGE_SKILL
		WHERE UPPER("LANGUAGE") = UPPER(@LANG)
		AND  UPPER(CEFR_LEVEL) = UPPER(@CEFR_LEVEL)

	IF @v_id is null and (@LANG is not null or @CEFR_LEVEL is not null)
	BEGIN
		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (@v_id, @LANG, @CEFR_LEVEL)
	END

	SET @return_value = @v_id
	
END 
;
GO



	/*
		Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
*/

CREATE PROCEDURE  dbo.INSERTA_SUBJECT_AREA(@P_SUBJECT_AREA xml, @return_value uniqueidentifier OUTPUT) as
BEGIN

	DECLARE @v_id uniqueidentifier
	DECLARE @ISCED_CODE VARCHAR(255) 
	DECLARE @ISCED_CLARIFICATION VARCHAR(255)

	SELECT 
		@ISCED_CODE = T.c.value('(isced_code)[1]','varchar(255)'),
		@ISCED_CLARIFICATION = T.c.value('(isced_clarification)[1]','varchar(255)')
	FROM @P_SUBJECT_AREA.nodes('subject_area') T(c)
	
	IF (@ISCED_CODE is not null) or (@ISCED_CLARIFICATION is not null)
	BEGIN
		SET @v_id =  NEWID()
		INSERT INTO dbo.EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (@v_id, @ISCED_CODE, @ISCED_CLARIFICATION)
    END
		
	SET @return_value = @v_id
	
END 
;
GO

	/* Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
*/
CREATE PROCEDURE  dbo.BORRA_CONTACT(@P_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_person_id VARCHAR(255)
	DECLARE @v_details_id VARCHAR(255)
	DECLARE @NAME_ID uniqueidentifier
	DECLARE @DESCRIPTION_ID uniqueidentifier

	IF @P_ID IS NOT NULL
	BEGIN

		SELECT @v_person_id = PERSON_ID, @v_details_id = CONTACT_DETAILS_ID
			FROM dbo.EWPCV_CONTACT
			WHERE ID = @P_ID

		DECLARE cur CURSOR LOCAL FOR
			SELECT NAME_ID
			FROM dbo.EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @NAME_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_NAME WHERE NAME_ID = @NAME_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
				FETCH NEXT FROM cur INTO @NAME_ID
			END
		CLOSE cur
		DEALLOCATE cur


		DECLARE cur CURSOR LOCAL FOR
			SELECT DESCRIPTION_ID
			FROM dbo.EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
				FETCH NEXT FROM cur INTO @DESCRIPTION_ID
			END
		CLOSE cur
		DEALLOCATE cur

		DELETE FROM dbo.EWPCV_CONTACT WHERE ID = @P_ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @v_details_id
		DELETE FROM dbo.EWPCV_PERSON WHERE ID = @v_person_id

	END 

END 
;
GO

/*
***************************************
******** Funciones FACTSHEET **********
***************************************
*/

/*
	Persiste informacion de contacto 
*/

CREATE PROCEDURE  dbo.INSERTA_CONTACT_DETAIL(@P_URL xml, @P_EMAIL varchar(255), @P_PHONE xml, 
		@v_contact_id uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_tlf_id uniqueidentifier
	DECLARE @v_litem_id uniqueidentifier
	DECLARE @xml xml

	IF @P_PHONE is not null
	BEGIN
		EXECUTE dbo.INSERTA_TELEFONO @P_PHONE, @v_tlf_id OUTPUT
	END

	SET @v_contact_id = NEWID()
	INSERT INTO dbo.EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (@v_contact_id, @v_tlf_id);


	IF @P_EMAIL is not null
	BEGIN
		INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (@v_contact_id, @P_EMAIL)
	END 

	IF @P_URL is not null
	BEGIN
		DECLARE cur CURSOR FOR
		SELECT
			T.c.query('.')
		FROM   @P_URL.nodes('/url/text') T(c) 

		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_litem_id OUTPUT
			INSERT INTO dbo.EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(@v_litem_id, @v_contact_id)
			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
	END

END
;
GO

/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
CREATE PROCEDURE  dbo.INSERTA_FACTSHEET(@P_FACTSHEET xml, @P_INFORMATION_ITEM xml, @return_value varchar(255) OUTPUT) as

BEGIN
	DECLARE	@v_contact_id VARCHAR(255)
	DECLARE	@v_factsheet_id uniqueidentifier

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('/application_info') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	SET @v_factsheet_id = NEWID()

	INSERT INTO dbo.EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
				VALUES(@v_factsheet_id, 
				@P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
				@P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
				@P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
				@P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
				@P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
				@P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
				@v_contact_id)

	SET @return_value = @v_factsheet_id

END
;
GO

	/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
	*/
	
CREATE PROCEDURE  dbo.INSERTA_INSTITUTION(@P_INSTITUTION_ID  VARCHAR(255), @P_ABREVIATION VARCHAR(255), @P_LOGO_URL VARCHAR(255),
		 @P_FACTSHEET_ID VARCHAR(255),  @P_INSTITUTION_NAMES xml,  @return_value uniqueidentifier OUTPUT) AS

BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_count integer

	SELECT @v_count = count(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count = 0
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET)
				VALUES (@v_id, @P_INSTITUTION_ID, @P_ABREVIATION, @P_LOGO_URL, @P_FACTSHEET_ID); 
		END
	ELSE
		BEGIN
			SELECT @v_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			UPDATE  dbo.EWPCV_INSTITUTION SET  ABBREVIATION = @P_ABREVIATION, LOGO_URL = @P_LOGO_URL, FACT_SHEET = @P_FACTSHEET_ID WHERE ID = @v_id; 
		END
	
	EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_id, @P_INSTITUTION_NAMES

	SET @return_value = @v_id

END
;
GO

	/*
		Persiste un information item
	*/

CREATE PROCEDURE  dbo.INSERTA_INFORMATION_ITEM(@P_INFORMATION_ITEM xml,
	@P_INF_TYPE VARCHAR(255), @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_infoitem_id uniqueidentifier

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('*') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	SET @v_infoitem_id =  NEWID()

	INSERT INTO dbo.EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (@v_infoitem_id, UPPER(@P_INF_TYPE), @v_contact_id)
	INSERT INTO dbo.EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (@P_INSTITUTION_ID, @v_infoitem_id)

END
;
GO


/*
	Inserta informacion sobre requerimientos

	*/
CREATE PROCEDURE  dbo.INSERTA_REQUIREMENT_INFO(@P_TYPE VARCHAR(255), @P_NAME VARCHAR(255),  @P_DESCRIPTION VARCHAR(255), 
		@P_URL xml,	@P_EMAIL VARCHAR(255), @P_PHONE xml, @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_requinf_id uniqueidentifier

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @P_URL, @P_EMAIL, @P_PHONE, @v_contact_id OUTPUT

	SET @v_requinf_id = NEWID()
	INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) 
		VALUES (@v_requinf_id, UPPER(@P_TYPE), @P_NAME, @P_DESCRIPTION, @v_contact_id)
	INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) 
		VALUES (@P_INSTITUTION_ID, @v_requinf_id)

END
;
GO
		
/*
	Persiste una hoja de contactos asociada a una institucion en el sistema.
*/

CREATE PROCEDURE  dbo.INSERTA_FACTSHEET_INSTITUTION(@P_FACTSHEET xml) as

BEGIN

	DECLARE @v_id uniqueidentifier
	DECLARE @v_inst_id uniqueidentifier
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @v_add_info_id uniqueidentifier

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c) 

	EXECUTE dbo.INSERTA_FACTSHEET @FACTSHEET, @APPLICATION_INFO, @v_factsheet_id OUTPUT
	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, @ABREVIATION, @LOGO_URL, @v_factsheet_id, @INSTITUTION_NAME, @v_inst_id OUTPUT

	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @v_inst_id


	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @v_inst_id
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo


	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @v_inst_id
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @v_inst_id
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO

	/* Inserta un factsheet en el sistema
		Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

ALTER PROCEDURE dbo.INSERT_FACTSHEET(@P_FACTSHEET xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer
			DECLARE @INSTITUTION_ID varchar(255)


				SET @INSTITUTION_ID = @P_FACTSHEET.value('(factsheet_institution/institution_id)[1]', 'varchar(255)')

				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND FACT_SHEET IS NOT NULL;
				IF  @v_count > 0 
					BEGIN
						SET @P_ERROR_MESSAGE = 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.'
						SET @return_value = -1
					END 
				ELSE
					BEGIN
						EXECUTE dbo.INSERTA_FACTSHEET_INSTITUTION @P_FACTSHEET
						SET @return_value = 0 
					END
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

	/*
		Borra los information item asociados a una institucion
	*/
	
CREATE PROCEDURE  dbo.BORRA_INFORMATION_ITEMS(@P_INSTITUTION_ID uniqueidentifier) as
BEGIN
	DECLARE @ID uniqueidentifier
	DECLARE @CONTACT_DETAIL_ID uniqueidentifier

	DECLARE cur CURSOR FOR
	SELECT II.ID, II.CONTACT_DETAIL_ID
		FROM dbo.EWPCV_INST_INF_ITEM III 
			INNER JOIN dbo.EWPCV_INFORMATION_ITEM II 
			ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND INFORMATION_ID = @ID
		DELETE FROM dbo.EWPCV_INFORMATION_ITEM WHERE ID = @ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID

		FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID
	END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra los requirements info asociados a una institucion
	*/
CREATE PROCEDURE  dbo.BORRA_REQUIREMENTS_INFO(@P_INSTITUTION_ID uniqueidentifier) as
BEGIN
	DECLARE @ID uniqueidentifier
	DECLARE @CONTACT_DETAIL_ID uniqueidentifier

	DECLARE cur CURSOR FOR
		SELECT RI.ID, RI.CONTACT_DETAIL_ID
		FROM dbo.EWPCV_INS_REQUIREMENTS IR
			INNER JOIN dbo.EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND REQUIREMENT_ID = @ID
		DELETE FROM dbo.EWPCV_REQUIREMENTS_INFO WHERE ID = @ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID

		FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID
	END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra un factsheet asociado a una institucion no borra la institucion.
	*/
CREATE PROCEDURE  dbo.BORRA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID uniqueidentifier, @P_FACTSHEET_ID uniqueidentifier) as
BEGIN
	DECLARE @CONTACT_DETAIL_ID uniqueidentifier
	SELECT @CONTACT_DETAIL_ID = CONTACT_DETAILS_ID FROM dbo.EWPCV_FACT_SHEET WHERE ID = @P_FACTSHEET_ID;
	
	UPDATE dbo.EWPCV_INSTITUTION SET FACT_SHEET = NULL WHERE ID = @P_INSTITUTION_ID
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @P_FACTSHEET_ID
	EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID
	EXECUTE dbo.BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID	
END;
GO

	/* Elimina un FACTSHEET del sistema.
		Recibe como parametro el identificador de la institucion
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
ALTER PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @ID uniqueidentifier
		DECLARE @FACT_SHEET uniqueidentifier

		SET @return_value = 0

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
		IF  @v_count = 0 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
			SET @return_value = -1
		END
		ELSE
		BEGIN
			SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			IF @ID is null
			BEGIN
				SET @P_ERROR_MESSAGE = 'La institucion no existe.'
				SET @return_value = -1
			END 
			IF @FACT_SHEET is null
			BEGIN
				SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
				SET @return_value = -1
			END 
		END

		IF @return_value = 0
		BEGIN
			EXECUTE dbo.BORRA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_FACTSHEET'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

	/*
		Borra los nombres de una institucion
	*/
CREATE PROCEDURE  dbo.BORRA_NOMBRES(@P_INSTITUTION_ID uniqueidentifier) as
BEGIN
	DECLARE @ID uniqueidentifier

	DECLARE curNam CURSOR FOR
		SELECT LI.ID FROM dbo.EWPCV_LANGUAGE_ITEM LI 
		INNER JOIN dbo.EWPCV_INSTITUTION_NAME INA 
		ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN curNam
	FETCH NEXT FROM curNam INTO @ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND NAME_ID = @ID
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @ID
		FETCH NEXT FROM curNam INTO @ID
	END
	CLOSE curNam
	DEALLOCATE curNam

END
;
GO

	/*
		Actualiza la informacion de contacto
	*/
	
CREATE PROCEDURE  dbo.ACTUALIZA_CONTACT_DETAILS(@P_CONTACT_ID uniqueidentifier, @P_URL xml, @P_EMAIL VARCHAR(255), @P_PHONE xml, @return_value VARCHAR(255) OUTPUT) as
BEGIN

	DECLARE @v_contact_id VARCHAR(255)
	EXECUTE dbo.BORRA_CONTACT_DETAILS @P_CONTACT_ID
	EXECUTE dbo.INSERTA_CONTACT_DETAIL @P_URL, @P_EMAIL, @P_PHONE, @v_contact_id OUTPUT
	SET @return_value = @v_contact_id
END 
;
GO

	/*
		Actualiza el factsheet de una institucion
	*/

CREATE PROCEDURE  dbo.ACTUALIZA_FACTSHEET(@P_FACTSHEET_ID uniqueidentifier, @P_FACTSHEET xml, @P_INFO xml) AS
BEGIN

	DECLARE @ID uniqueidentifier
	DECLARE @url xml
	DECLARE @email varchar(255)
	DECLARE @phone xml
	   
	UPDATE dbo.EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = @P_FACTSHEET_ID

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFO.nodes('/application_info') T(c) 

	EXECUTE dbo.ACTUALIZA_CONTACT_DETAILS @P_FACTSHEET_ID, @url, @email, @phone, @ID OUTPUT

	UPDATE dbo.EWPCV_FACT_SHEET SET
			DECISION_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
			TOR_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
			NOMINATIONS_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
			NOMINATIONS_SPRING_TERM = @P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
			APPLICATION_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
			APPLICATION_SPRING_TERM = @P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
			CONTACT_DETAILS_ID = @ID
			WHERE ID = @P_FACTSHEET_ID

END 
;
GO

	/*
		Actualiza toda la informacion referente al factsheet de una institucion
	*/
	
CREATE PROCEDURE  dbo.ACTUALIZA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET_ID uniqueidentifier, @P_FACTSHEET xml) as
BEGIN

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c) 

	UPDATE dbo.EWPCV_INSTITUTION SET ABBREVIATION = @ABREVIATION, LOGO_URL = @LOGO_URL WHERE ID = @P_INSTITUTION_ID
	
	EXECUTE dbo.BORRA_NOMBRES @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INSTITUTION_NAMES @P_INSTITUTION_ID, @INSTITUTION_NAME
	
	EXECUTE dbo.ACTUALIZA_FACTSHEET @P_FACTSHEET_ID, @FACTSHEET, @APPLICATION_INFO
	
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @P_INSTITUTION_ID

	EXECUTE BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID

	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @P_INSTITUTION_ID
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo

	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @P_INSTITUTION_ID
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @P_INSTITUTION_ID
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO

	/* Actualiza un FACTSHEET del sistema.
		Recibe como parametro el identificador de la Institucion a actualizar
		Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
ALTER PROCEDURE dbo.UPDATE_FACTSHEET( @P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET xml, 
 @P_ERROR_MESSAGE VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_count integer
			DECLARE @ID uniqueidentifier
			DECLARE @FACT_SHEET uniqueidentifier
	
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
				SET @return_value = -1
			END
			ELSE
			BEGIN
				SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
				IF @ID is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no existe.'
					SET @return_value = -1
				END 
				IF @FACT_SHEET is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
					SET @return_value = -1
				END 
			END

			IF @return_value = 0
			BEGIN
				EXECUTE dbo.ACTUALIZA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET, @P_FACTSHEET ;
			END
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/*
	VALIDACION VIA XSD
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_FACTSHEET_INSTITUTION' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="factsheet_institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="logo_url" type="xs:string" minOccurs="0" />
				<xs:element name="abreviation" type="xs:string" minOccurs="0" />
				<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="factsheet">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="decision_week_limit" type="xs:integer" />
							<xs:element name="tor_week_limit" type="xs:integer" />
							<xs:element name="nominations_autum_term" type="xs:date" />
							<xs:element name="nominations_spring_term" type="xs:date" />
							<xs:element name="application_autum_term" type="xs:date" />
							<xs:element name="application_spring_term" type="xs:date" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="application_info" type="informationItemType" />
				<xs:element name="housing_info" type="informationItemType" />
				<xs:element name="visa_info" type="informationItemType" />
				<xs:element name="insurance_info" type="informationItemType" />
				<xs:element name="additional_info_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="additional_info" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="info_type" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="accessibility_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="accessibility_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="req_type">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:enumeration value="infrastructure"/>
													<xs:enumeration value="service"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="additional_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element  name="additional_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="url" type="languageItemListType" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION

CREATE PROCEDURE  dbo.VALIDA_FACTSHEET_INSTITUTION(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_FACTSHEET_INSTITUTION)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;
GO

/*
***************************************
******** Funciones IIAS ***************
***************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_IIA' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" />
				<xs:element name="end_date" type="xs:date" />
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded" >
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="0" type="xs:integer" />
													<xs:element name="unit" minOccurs="0">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO



CREATE PROCEDURE  dbo.VALIDA_IIA(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_IIA)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 

/*
	Busca el codigo SCHAC de la institucion que genera la notificacion
	Para un EWPCV_IIA.ID, buscar entre sus EWPCV_COOPERATION_CONDITION los RECEIVING_PARTNER_ID y SENDING_PARTNER_ID,
	y de esta pareja devolver el INSTITUTION_ID correspondiente que NO sea el P_HEI_TO_NOTIFY
*/
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI(@P_IIA_ID uniqueidentifier, @P_HEI_TO_NOTIFY varchar(255), @return_value varchar(255) OUTPUT) as
BEGIN
	DECLARE @receiving_institution_id varchar(255)
	DECLARE @sending_institution_id varchar(255)

	SELECT TOP 1
		@receiving_institution_id = receiving_partner.INSTITUTION_ID, 
		@sending_institution_id = sending_partner.INSTITUTION_ID 
	FROM 
		dbo.EWPCV_COOPERATION_CONDITION coco, dbo.EWPCV_IIA_PARTNER sending_partner, dbo.EWPCV_IIA_PARTNER receiving_partner 
	WHERE 
		coco.RECEIVING_PARTNER_ID = receiving_partner.ID and
		coco.SENDING_PARTNER_ID = sending_partner.ID and
		coco.IIA_ID =  @P_IIA_ID

	IF @P_HEI_TO_NOTIFY = @receiving_institution_id
		SET @return_value = @sending_institution_id
	ELSE
		SET @return_value = @receiving_institution_id

END
;
GO




	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
CREATE PROCEDURE  dbo.INSERTA_IIA(@P_IIA XML, @return_value uniqueidentifier OUTPUT) as 
BEGIN
	DECLARE @v_iia_id uniqueidentifier
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @IIA_CODE varchar(255)
	DECLARE @COOPERATION_CONDITION_LIST xml

	SELECT 
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)
	
	SET @v_iia_id = NEWID()

	INSERT INTO dbo.EWPCV_IIA (ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE) 
		VALUES (@v_iia_id, @END_DATE, @IIA_CODE, SYSDATETIME(), @START_DATE)

	EXECUTE dbo.INSERTA_COOP_CONDITIONS @v_iia_id, @COOPERATION_CONDITION_LIST

    SET @return_value = @v_iia_id

END
;
GO


	/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/
ALTER PROCEDURE dbo.INSERT_IIA(@P_IIA XML, @P_IIA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS

BEGIN
	DECLARE @v_notifier_hei varchar(255)

	IF @P_HEI_TO_NOTIFY IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END
	ELSE
	BEGIN

		EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_IIA @P_IIA, @P_IIA_ID OUTPUT
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_IIA'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
END
;
GO

	/*
		Borra un IIA PARTNER y todos sus contactos asociados.
	*/
CREATE PROCEDURE  dbo.BORRA_IIA_PARTNER(@P_IIA_PARTNER_ID uniqueidentifier) AS
BEGIN

	DECLARE @v_s_id VARCHAR(255)
	DECLARE @CONTACTS_ID uniqueidentifier
	
	DECLARE cur CURSOR LOCAL FOR
		SELECT CONTACTS_ID
			FROM dbo.EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = @P_IIA_PARTNER_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACTS_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_IIA_PARTNER_CONTACTS 
				WHERE CONTACTS_ID = @CONTACTS_ID 
				AND IIA_PARTNER_ID = @P_IIA_PARTNER_ID
			EXECUTE dbo.BORRA_CONTACT @CONTACTS_ID
			FETCH NEXT FROM cur INTO @CONTACTS_ID
		END
	CLOSE cur
	DEALLOCATE cur

	SELECT @v_s_id = SIGNER_PERSON_CONTACT_ID
			FROM dbo.EWPCV_IIA_PARTNER
			WHERE ID = @P_IIA_PARTNER_ID

	DELETE FROM dbo.EWPCV_IIA_PARTNER WHERE ID = @P_IIA_PARTNER_ID
	EXECUTE dbo.BORRA_CONTACT @v_s_id

	DELETE FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_PARTNER_ID

END
;
GO

/*
	Borra las condiciones de cooperacion de un IIAs
*/
CREATE PROCEDURE  dbo.BORRA_COOP_CONDITIONS(@P_IIA_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_mn_count integer
	DECLARE @v_mt_count integer
	DECLARE @v_md_count integer

	DECLARE @ID uniqueidentifier
	DECLARE @MOBILITY_NUMBER_ID uniqueidentifier
	DECLARE @DURATION_ID uniqueidentifier
	DECLARE @RECEIVING_PARTNER_ID uniqueidentifier
	DECLARE @SENDING_PARTNER_ID uniqueidentifier
	
	DECLARE cur CURSOR LOCAL FOR
		SELECT ID, MOBILITY_NUMBER_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM dbo.EWPCV_COOPERATION_CONDITION 
			WHERE IIA_ID = @P_IIA_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA_LANSKILL @ID
			DELETE FROM dbo.EWPCV_COOPERATION_CONDITION WHERE ID = @ID
			DELETE FROM dbo.EWPCV_MOBILITY_NUMBER WHERE ID = @MOBILITY_NUMBER_ID
		
			DELETE FROM EWPCV_DURATION WHERE ID = @DURATION_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @RECEIVING_PARTNER_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @SENDING_PARTNER_ID

			FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

	/*
		Borra un IIAS y todas sus condiciones de cooperacion
	*/
CREATE PROCEDURE  dbo.BORRA_IIA(@P_IIA_ID uniqueidentifier) AS
BEGIN
	EXECUTE dbo.BORRA_COOP_CONDITIONS @P_IIA_ID
	DELETE FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
END
;
GO

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
CREATE PROCEDURE  dbo.INSERTA_IIA_PARTNER(@P_IIA_PARTNER xml, @return_value uniqueidentifier output) AS
BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_c_id uniqueidentifier
	DECLARE @v_ounit_id uniqueidentifier
	DECLARE @INSTITUTION xml
	DECLARE @SIGNER_PERSON xml
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @SIGNING_DATE date


	SELECT 
		@INSTITUTION = T.c.query('institution'),
		@SIGNER_PERSON = T.c.query('signer_person'),
		@INSTITUTION_ID = T.c.value('(institution/institution_id)[1]', 'varchar(255)'),
		@SIGNING_DATE = T.c.value('(signing_date)[1]', 'date')
	FROM @P_IIA_PARTNER.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @INSTITUTION, @v_ounit_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SIGNER_PERSON, @v_c_id output
	SET @v_id = NEWID()

	INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
			VALUES (@v_id, @INSTITUTION_ID, @v_ounit_id, @v_c_id, @SIGNING_DATE)

	SET @return_value = @v_id
	
END
;
GO

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
*/
CREATE PROCEDURE  dbo.INSERTA_PARTNER_CONTACTS(@P_PARTNER_ID uniqueidentifier, @P_PARTNER_CONTACTS xml) AS
BEGIN
	DECLARE @partner_contact xml
	DECLARE @v_c_p_id uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_PARTNER_CONTACTS.nodes('partner_contacts_list/partner_contact') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @partner_contact
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @partner_contact, @v_c_p_id output
			INSERT INTO dbo.EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (@P_PARTNER_ID, @v_c_p_id)
			FETCH NEXT FROM cur INTO @partner_contact
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
	*/
CREATE PROCEDURE  dbo.INSERTA_SUBAREA_LANSKIL(@P_COP_COND_ID uniqueidentifier, @P_SUBAREA_LANSKIL_LIST xml) AS
BEGIN
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_isced_code varchar(255)
	DECLARE @SUBJECT_AREA xml
	DECLARE @LANGUAGE_SKILL xml
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('subject_area'),
		T.c.query('language_skill')
	FROM @P_SUBAREA_LANSKIL_LIST.nodes('subject_area_language_list/subject_area_language') T(c) 
	OPEN cur
	FETCH NEXT FROM cur INTO @SUBJECT_AREA, @LANGUAGE_SKILL
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA, @v_isced_code output
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(NEWID(), @P_COP_COND_ID, @v_isced_code, @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @SUBJECT_AREA, @LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO

    /*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/
CREATE PROCEDURE  dbo.INSERTA_IIA_MOBILITY_TYPE(@P_MOBILITY_TYPE xml, @v_id uniqueidentifier OUTPUT) AS
BEGIN
	DECLARE @MOBILITY_CATEGORY VARCHAR(255)
	DECLARE @MOBILITY_GROUP VARCHAR(255)

	SELECT 
		@MOBILITY_CATEGORY = T.c.value('(mobility_category)[1]', 'varchar(255)'),
		@MOBILITY_GROUP = T.c.value('(mobility_group)[1]', 'varchar(255)')
	FROM @P_MOBILITY_TYPE.nodes('mobility_type') T(c)

	SELECT @v_id = ID
		FROM dbo.EWPCV_MOBILITY_TYPE
		WHERE UPPER(MOBILITY_CATEGORY) = UPPER(@MOBILITY_CATEGORY)
		AND  UPPER(MOBILITY_GROUP) = UPPER(@MOBILITY_GROUP)

	IF @v_id is null 
	BEGIN
		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (@v_id, @MOBILITY_CATEGORY, @MOBILITY_GROUP)
	END 
END
;
GO

     /*
		Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
	*/
CREATE PROCEDURE  dbo.INSERTA_DURATION(@DURATION xml, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @NUMBER_DURATION VARCHAR(255)
	DECLARE @UNIT VARCHAR(255)

	SELECT 
		@NUMBER_DURATION = T.c.value('(number_duration)[1]', 'varchar(255)'),
		@UNIT = T.c.value('(unit)[1]', 'varchar(255)')
	FROM @DURATION.nodes('cop_cond_duration') T(c)

	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (@v_id, @NUMBER_DURATION, @UNIT)

END
;
GO

     /*
		Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
	*/
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_NUMBER(@P_MOB_NUMBER varchar(255), @v_id uniqueidentifier output) AS
BEGIN
	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (@v_id, @P_MOB_NUMBER)

END
;
GO

	/*
		Inserta los EQUF LEVELS asociados a una condicion de cooperacion
	*/				
CREATE PROCEDURE  dbo.INSERTA_EQFLEVELS(@P_COOP_COND_ID varchar(255), @P_COOP_COND xml) AS
BEGIN

	DECLARE @EQF_LEVEL varchar(255)
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(.)[1]', 'varchar(255)')
	FROM @P_COOP_COND.nodes('cooperation_condition/eqf_level_list/eqf_level') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @EQF_LEVEL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (@P_COOP_COND_ID, @EQF_LEVEL)
			FETCH NEXT FROM cur INTO @EQF_LEVEL
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

	/*
		Persiste una lista de condiciones de cooperacion asociadas a un IIAS
	*/
CREATE PROCEDURE  dbo.INSERTA_COOP_CONDITIONS(@P_IIA_ID uniqueidentifier, @P_COOP_COND xml) AS
BEGIN
	DECLARE @v_s_iia_partner_id uniqueidentifier
	DECLARE @v_s_c_p_id uniqueidentifier
	DECLARE @v_r_iia_partner_id uniqueidentifier
	DECLARE @v_r_c_p_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_duration_id uniqueidentifier
	DECLARE @v_mob_number_id uniqueidentifier
	DECLARE @v_cc_id uniqueidentifier

	DECLARE @sp xml, @sp_contacts xml, @rp xml, @rp_contacts xml, @mobility_type xml, @cop_cond_duration xml
	DECLARE @mobility_number integer
	DECLARE @start_date date
	DECLARE @end_date date
	DECLARE @subject_area_language_list xml
	DECLARE @cooperation_condition xml
	DECLARE @other_info varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('sending_partner'),
		T.c.query('sending_partner/partner_contacts_list'),
		T.c.query('receiving_partner'),
		T.c.query('receiving_partner/partner_contacts_list'),
		T.c.query('mobility_type'),
		T.c.query('cop_cond_duration'),
		T.c.value('(mobility_number)[1]', 'integer'),
		T.c.value('(start_date)[1]', 'date'),
		T.c.value('(end_date)[1]', 'date'),
		T.c.query('subject_area_language_list'),
		T.c.value('(other_info)[1]', 'varchar(255)'),
		T.c.query('.')
		FROM @P_COOP_COND.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration,
		@mobility_number, @start_date, @end_date, @subject_area_language_list, @other_info, @cooperation_condition 
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_IIA_PARTNER @sp, @v_s_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_s_iia_partner_id, @sp_contacts

			EXECUTE dbo.INSERTA_IIA_PARTNER @rp, @v_r_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_r_iia_partner_id, @rp_contacts

			---

			EXECUTE dbo.INSERTA_IIA_MOBILITY_TYPE @mobility_type, @v_mobility_type_id output
			EXECUTE dbo.INSERTA_DURATION @cop_cond_duration, @v_duration_id output
			EXECUTE dbo.INSERTA_MOBILITY_NUMBER @mobility_number, @v_mob_number_id output

			SET @v_cc_id = NEWID()

			INSERT INTO dbo.EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO) 
				VALUES (@v_cc_id, @END_DATE, @START_DATE, @v_duration_id,
					@v_mob_number_id, @v_mobility_type_id, @v_r_iia_partner_id, @v_s_iia_partner_id, @P_IIA_ID, @other_info)

			EXECUTE dbo.INSERTA_SUBAREA_LANSKIL @v_cc_id, @subject_area_language_list
			EXECUTE dbo.INSERTA_EQFLEVELS @v_cc_id, @cooperation_condition

			FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration, 
				@mobility_number, @start_date, @end_date, @subject_area_language_list, @other_info, @cooperation_condition 
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
	Actualiza un IIA 
	*/
CREATE PROCEDURE  dbo.ACTUALIZA_IIA(@P_IIA_ID varchar(255), @P_IIA xml) AS

BEGIN

	DECLARE @IIA_CODE varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @PDF varchar(255)

	SELECT 
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list'),
		@PDF = T.c.value('(pdf)[1]', 'varchar(255)')
	FROM @P_IIA.nodes('iia') T(c)

	UPDATE EWPCV_IIA SET START_DATE = @START_DATE, END_DATE = @END_DATE, IIA_CODE = @IIA_CODE, MODIFY_DATE = SYSDATETIME()
			WHERE ID = @P_IIA_ID

	EXECUTE dbo.BORRA_COOP_CONDITIONS @P_IIA_ID
	EXECUTE dbo.INSERTA_COOP_CONDITIONS @P_IIA_ID, @COOPERATION_CONDITION_LIST

	IF (@PDF='' OR @PDF is null)
	BEGIN
		UPDATE dbo.EWPCV_IIA SET PDF = null WHERE ID = @P_IIA_ID
	END
	ELSE BEGIN
		BEGIN TRY
			DECLARE @sql varchar(255)
			SET @sql = 'UPDATE dbo.EWPCV_IIA SET PDF = (SELECT * FROM OPENROWSET(BULK '''+@PDF+''', SINGLE_BLOB) AS BLOB) WHERE ID = '''+CAST(@P_IIA_ID AS varchar(255))+''''
			EXEC(@sql)
		END TRY
		BEGIN CATCH
			PRINT 'ERROR a intentar guardar PDF en dbo.EWPCV_IIA'
		END CATCH	
	END

END
;
GO

	/* Elimina un IIA del sistema.
		Recibe como parametro el identificador del Acuerdo Interinstitucional
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
  */
    
ALTER PROCEDURE dbo.DELETE_IIA(@P_IIA_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @v_notifier_hei varchar(255)

		SET @return_value = 0

		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		IF @return_value = 0 
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
			IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'El IIA no existe.'
				SET @return_value = -1
			END
		END

		IF @return_value = 0 
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE IIA_ID = @P_IIA_ID
			IF  @v_count > 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'Existen movilidades asociadas al IIA.'
				SET @return_value = -1
			END
		END

		IF @return_value = 0 
		BEGIN
			EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
			EXECUTE dbo.BORRA_IIA @P_IIA_ID
			EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO 

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
ALTER PROCEDURE dbo.UPDATE_IIA (@P_IIA_ID uniqueidentifier, @P_IIA xml, @P_ERROR_MESSAGE VARCHAR(255) OUTPUT, 
	@P_HEI_TO_NOTIFY VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_notifier_hei varchar(255)
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer

			IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END

			IF @return_value = 0 
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
				IF  @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = 'El IIA no existe.'
					SET @return_value = -1
				END
			END

			IF @return_value = 0 
			BEGIN
				EXECUTE dbo.ACTUALIZA_IIA @P_IIA_ID, @P_IIA
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
			END 
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_IIA'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

    /*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
        Este CNR desencadena todo el proceso de aceptación de IIAs.
	*/
CREATE PROCEDURE  dbo.APPROVE_IIA(@P_INTERNAL_ID_REMOTE_IIA_COPY varchar(255), @P_HEI_ID_TO_NOTIFY varchar(255), 
	@P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_remote_iia_id uniqueidentifier
		DECLARE @v_notifier_hei varchar(255)
        DECLARE @v_id uniqueidentifier

		SET @return_value = 0

		IF @P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id interno de la copia del IIA remoto';
				SET @return_value = -1
			END
		ELSE
			IF @P_HEI_ID_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el hei-id de la institución a notificar';
				SET @return_value = -1
			END 

		IF @return_value = 0
		BEGIN
			SELECT @v_remote_iia_id = REMOTE_IIA_ID
				FROM dbo.EWPCV_IIA WHERE ID = @P_INTERNAL_ID_REMOTE_IIA_COPY
			EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_INTERNAL_ID_REMOTE_IIA_COPY, @P_HEI_ID_TO_NOTIFY, @v_notifier_hei output
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI) 
				VALUES (@v_id, 0, @v_remote_iia_id, @P_HEI_ID_TO_NOTIFY, SYSDATETIME(), 4, 1, 0, 0, @v_notifier_hei)
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.APPROVE_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

 /*  Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
        y hash de las cooperation condition del iia remoto en nuestro IIA propio.
        Recibe como parámetro el identificador propio y el identificador de la copia remota.
   */

CREATE PROCEDURE  dbo.BIND_IIA(@P_OWN_IIA_ID varchar(255), @P_ID_FROM_REMOTE_IIA varchar(255), 
	@P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_remote_iia_id uniqueidentifier
		DECLARE @v_remote_coop_cond_hash varchar(255)
		DECLARE @v_remote_iia_code varchar(255)
        DECLARE @v_id uniqueidentifier
		DECLARE @v_count_iia integer

		SET @return_value = 0

		IF @P_OWN_IIA_ID IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END
		ELSE
			IF @P_ID_FROM_REMOTE_IIA IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA remoto';
				SET @return_value = -1
			END 

		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_OWN_IIA_ID
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_OWN_IIA_ID';
				SET @return_value = -1
			END 
		END 

		IF @return_value = 0 
		BEGIN
			SELECT
				@v_remote_iia_id = IIA.REMOTE_IIA_ID, 
				@v_remote_coop_cond_hash = IIA.REMOTE_COP_COND_HASH, 
				@v_remote_iia_code = IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA 
				WHERE IIA.ID = @P_ID_FROM_REMOTE_IIA 

			IF @v_remote_iia_id is null 
				BEGIN
					SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
					SET @return_value = -1
				END 
			ELSE
				UPDATE EWPCV_IIA SET 
					REMOTE_IIA_ID = @v_remote_iia_id, 
					REMOTE_IIA_CODE = @v_remote_iia_code
					WHERE ID = @P_OWN_IIA_ID
		END 
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.BIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

	/*
		Borra las subject areas asociadas a una condicion de cooperacion
	*/
CREATE PROCEDURE  dbo.BORRA_COOP_COND_SUBAREA_LANSKILL(@P_COOP_COND_ID uniqueidentifier) AS
BEGIN
	DECLARE @ID uniqueidentifier
	DECLARE @ISCED_CODE varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID, ISCED_CODE
		FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL 
		WHERE COOPERATION_CONDITION_ID = @P_COOP_COND_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @ISCED_CODE 

	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = @ID;
			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			FETCH NEXT FROM cur INTO @ID, @ISCED_CODE 
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
***************************************
******** Funciones Mobility ***********
***************************************
*/


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_SIGNATURE' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>
	<xs:element name="signature" type="signatureType" />
</xs:schema>'

GO



IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LEARNING_AGREEMENT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="creditType">
		<xs:sequence>
			<xs:element name="scheme" type="notEmptyStringType" />
			<xs:element name="credit_value">
				<xs:simpleType>
					<xs:restriction base="xs:decimal">
						<xs:totalDigits value="5" />
						<xs:fractionDigits value="1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language_skill" maxOccurs="unbounded">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="language" type="notEmptyStringType" />
						<xs:element name="cefr_level" type="notEmptyStringType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="learning_agreement">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="mobility_id" type="notEmptyStringType" />
				<xs:element name="student_la" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="given_name" type="xs:string" />
							<xs:element name="family_name" type="xs:string" />
							<xs:element name="birth_date" type="xs:string" />
							<xs:element name="citizenship" type="xs:string" />
							<xs:element name="gender" type="genderType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="la_component_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="la_component" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="la_component_type">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_code" type="xs:string" minOccurs="0" />
										<xs:element name="title" type="notEmptyStringType" />
										<xs:element name="academic_term">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="academic_year" type="academicYearType" />
													<xs:element name="institution_id" type="notEmptyStringType" />
													<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
													<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="description" type="languageItemListType" minOccurs="0" />
													<xs:element name="term_number" type="xs:integer" />
													<xs:element name="total_terms" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="credit" type="creditType" />
										<xs:element name="recognition_conditions" type="xs:string" minOccurs="0" />
										<xs:element name="short_description" type="xs:string" minOccurs="0" />
										<xs:element name="status" type="xs:string" minOccurs="0" />
										<xs:element name="reason_code" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
													<xs:enumeration value="5"/>
												</xs:restriction>
											</xs:simpleType>													
										</xs:element>
										<xs:element name="reason_text" type="xs:string" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="student_signature" type="signatureType" />
				<xs:element name="sending_hei_signature" type="signatureType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'
;
GO



IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_MOBILITY' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>


	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="cooperation_condition_id" type="notEmptyStringType" />
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="eqf_level" type="xs:integer" />
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>			
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="status">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
							<xs:enumeration value="4"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'

GO

CREATE PROCEDURE  dbo.VALIDA_SIGNATURE(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_SIGNATURE)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 



CREATE PROCEDURE  dbo.VALIDA_MOBILITY(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_MOBILITY)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 




CREATE PROCEDURE  dbo.VALIDA_LEARNING_AGREEMENT(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_LEARNING_AGREEMENT)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO 







	/*
		Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
	*/

CREATE PROCEDURE  dbo.INSERTA_MOBILITY_TYPE(@P_MOBILITY_TYPE VARCHAR(255), @v_id uniqueidentifier output) AS
BEGIN
	SELECT @v_id=ID
		FROM EWPCV_MOBILITY_TYPE
		WHERE UPPER(MOBILITY_CATEGORY) = UPPER(@P_MOBILITY_TYPE)
		AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA')

	IF @v_id is null and @P_MOBILITY_TYPE is not null 
	BEGIN
		SET @v_id=NEWID()
		INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (@v_id, @P_MOBILITY_TYPE, 'MOBILITY_LA')
	END
END
; 
GO


	/*
		Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
	*/

CREATE PROCEDURE  dbo.INSERTA_MOBILITY_PARTICIPANT(@P_STUDENT xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_s_id VARCHAR(255)
	DECLARE @v_c_id VARCHAR(255)
	DECLARE @GLOBAL_ID varchar(255)
	DECLARE @CONTACT_PERSON xml

	SELECT 
		@GLOBAL_ID = T.c.value('(global_id)[1]','varchar(255)'),
		@CONTACT_PERSON = T.c.query('contact_person')
	FROM @P_STUDENT.nodes('student') T(c)

	SELECT @v_s_id = ID, @v_c_id = CONTACT_ID
				FROM EWPCV_MOBILITY_PARTICIPANT
				WHERE UPPER(ID) = UPPER(@GLOBAL_ID)

	IF @v_s_id IS NOT NULL 
		BEGIN
			UPDATE dbo.EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = null WHERE ID = @v_s_id
			EXECUTE dbo.BORRA_CONTACT @v_c_id
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @v_c_id output
			UPDATE EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = @v_c_id WHERE ID = @v_s_id
		END
	ELSE
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @v_c_id output
			INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, CONTACT_ID) VALUES (@GLOBAL_ID, @v_c_id)
		END

	SET @return_value = @GLOBAL_ID

END
;
GO

	/*
		Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_MOBILITY(@P_MOBILITY xml, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @v_s_ounit_id uniqueidentifier
	DECLARE @v_r_ounit_id uniqueidentifier
	DECLARE @v_student_id varchar(255)
	DECLARE @v_s_contact_id uniqueidentifier
	DECLARE @v_s_admv_contact_id uniqueidentifier
	DECLARE @v_r_contact_id uniqueidentifier
	DECLARE @v_r_admv_contact_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_isced_code varchar(255)

	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	DECLARE @STUDENT xml
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml
	DECLARE @SUBJECT_AREA xml
	DECLARE @MOBILITY_TYPE varchar(255)
	DECLARE @ACTUAL_ARRIVAL_DATE date
	DECLARE @ACTUAL_DEPATURE_DATE date
	DECLARE @COOPERATION_CONDITION_ID varchar(255)
	DECLARE @EQF_LEVEL integer
	DECLARE @IIA_CODE varchar(255)
	DECLARE @PLANED_ARRIVAL_DATE date
	DECLARE @PLANED_DEPATURE_DATE date
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @STATUS integer
	DECLARE @STUDENT_LANGUAGE_SKILL_LIST xml
	DECLARE @STUDENT_LANGUAGE_SKILL xml

	SELECT 
		@SENDING_INSTITUTION = T.c.query('sending_institution'),
		@RECEIVING_INSTITUTION = T.c.query('receiving_institution'),
		@STUDENT = T.c.query('student'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@SUBJECT_AREA = T.c.query('subject_area'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)'),
		@ACTUAL_ARRIVAL_DATE = T.c.value('(actual_arrival_date)[1]', 'date'),
		@ACTUAL_DEPATURE_DATE = T.c.value('(actual_depature_date)[1]', 'date'),
		@PLANED_ARRIVAL_DATE = T.c.value('(planed_arrival_date)[1]', 'date'),
		@PLANED_DEPATURE_DATE = T.c.value('(planed_depature_date)[1]', 'date'),
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@COOPERATION_CONDITION_ID = T.c.value('(cooperation_condition_id)[1]', 'varchar(255)'),
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'integer'),
		@SENDING_INSTITUTION_ID = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@STATUS = T.c.value('(status)[1]', 'integer'),
		@STUDENT_LANGUAGE_SKILL_LIST = T.c.query('student_language_skill_list')

	FROM @P_MOBILITY.nodes('mobility') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @SENDING_INSTITUTION , @v_s_ounit_id output
	EXECUTE dbo.INSERTA_INST_OUNIT @RECEIVING_INSTITUTION , @v_r_ounit_id output
	EXECUTE dbo.INSERTA_MOBILITY_PARTICIPANT @STUDENT , @v_student_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT , @v_s_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT , @v_s_admv_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT , @v_r_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT , @v_r_admv_contact_id output

	EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA , @v_isced_code output
	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE , @v_mobility_type_id output

	SET @v_id = NEWID()

	INSERT INTO dbo.EWPCV_MOBILITY (ID,
			ACTUAL_ARRIVAL_DATE,
			ACTUAL_DEPARTURE_DATE,
			COOPERATION_CONDITION_ID,
			EQF_LEVEL,
			IIA_ID,
			ISCED_CODE,
			MOBILITY_PARTICIPANT_ID,
			MOBILITY_REVISION,
			PLANNED_ARRIVAL_DATE,
			PLANNED_DEPARTURE_DATE,
			RECEIVING_INSTITUTION_ID,
			RECEIVING_ORGANIZATION_UNIT_ID,
			SENDING_INSTITUTION_ID,
			SENDING_ORGANIZATION_UNIT_ID,
			STATUS,
			MOBILITY_TYPE_ID,
			SENDER_CONTACT_ID,
			SENDER_ADMV_CONTACT_ID,
			RECEIVER_CONTACT_ID,
			RECEIVER_ADMV_CONTACT_ID)
			VALUES (@v_id,
			@ACTUAL_ARRIVAL_DATE,
			@ACTUAL_DEPATURE_DATE,
			@COOPERATION_CONDITION_ID,
			@EQF_LEVEL,
			@IIA_CODE,
			@v_isced_code,
			@v_student_id,
			0,
			@PLANED_ARRIVAL_DATE,
			@PLANED_DEPATURE_DATE,
			@RECEIVING_INSTITUTION_ID,
			@v_r_ounit_id,
			@SENDING_INSTITUTION_ID,
			@v_s_ounit_id,
			@STATUS,
			@v_mobility_type_id,
			@v_s_contact_id,
			@v_s_admv_contact_id,
			@v_r_contact_id,
			@v_r_admv_contact_id)

	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @STUDENT_LANGUAGE_SKILL_LIST.nodes('student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (@v_id, 0, @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


	/* Borra el estudiante de la tabla mobility participant
	*/
	
CREATE PROCEDURE  dbo.BORRA_STUDENT (@P_ID VARCHAR(255)) AS
BEGIN
	DECLARE @v_contact_id uniqueidentifier

	SELECT @v_contact_id = CONTACT_ID FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	DELETE FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	EXECUTE dbo.BORRA_CONTACT @v_contact_id

END
;
GO


	/*
		Borra creditos asociados a un componente
	*/

CREATE PROCEDURE  dbo.BORRA_CREDITS(@P_COMPONENT_ID uniqueidentifier) AS
BEGIN

	DECLARE @CREDITS_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT CREDITS_ID
			FROM dbo.EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = @P_COMPONENT_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @CREDITS_ID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = @CREDITS_ID
			DELETE FROM dbo.EWPCV_CREDIT WHERE ID = @CREDITS_ID
			FETCH NEXT FROM cur INTO @CREDITS_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra un componente
	*/

CREATE PROCEDURE  dbo.BORRA_COMPONENT(@P_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_at_id uniqueidentifier
	EXECUTE dbo.BORRA_CREDITS @P_ID

	SELECT @v_at_id = ACADEMIC_TERM_DISPLAY_NAME
			FROM dbo.EWPCV_LA_COMPONENT
			WHERE ID = @P_ID
	DELETE FROM dbo.EWPCV_LA_COMPONENT WHERE ID = @P_ID;
	EXECUTE dbo.BORRA_ACADEMIC_TERM @v_at_id

END
;
GO

	/*
		Borra un componente de tipo studied
	*/

CREATE PROCEDURE  dbo.BORRA_STUDIED_COMP(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN

	DECLARE @ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT STUDIED_LA_COMPONENT_ID
			FROM dbo.EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


	/*
		Borra un componente de tipo recognized
	*/

CREATE PROCEDURE  dbo.BORRA_RECOGNIZED_COMP(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN

	DECLARE @ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM dbo.EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra un componente de tipo virtual
	*/

CREATE PROCEDURE  dbo.BORRA_VIRTUAL_COMP(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN

	DECLARE @ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT VIRTUAL_LA_COMPONENT_ID
			FROM dbo.EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra un componente de tipo blended
	*/

CREATE PROCEDURE  dbo.BORRA_BLENDED_COMP(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN

	DECLARE @ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT BLENDED_LA_COMPONENT_ID
			FROM dbo.EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


	/*
		Borra un componente de tipo doctoral
	*/

CREATE PROCEDURE  dbo.BORRA_DOCTORAL_COMP(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN

	DECLARE @ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM dbo.EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


	/*
		Borra una revision de un LA
	*/

CREATE PROCEDURE  dbo.BORRA_LA(@P_LA_ID uniqueidentifier, @LA_REVISION integer) AS
BEGIN
	DECLARE @v_s_id uniqueidentifier
	DECLARE @v_sc_id uniqueidentifier
	DECLARE @v_rc_id uniqueidentifier

	EXECUTE dbo.BORRA_STUDIED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_RECOGNIZED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_VIRTUAL_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_BLENDED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_DOCTORAL_COMP @P_LA_ID, @LA_REVISION

	SELECT @v_s_id = STUDENT_SIGN, @v_sc_id = SENDER_COORDINATOR_SIGN, @v_rc_id = RECEIVER_COORDINATOR_SIGN
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	DELETE FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LA_REVISION
	DELETE FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LA_REVISION
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_s_id
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_sc_id
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_rc_id

END
; 
GO

	/*
		Borra los language skill asociados a una revision de una mobilidad
	*/

CREATE PROCEDURE  dbo.BORRA_MOBILITY_LANSKILL(@P_MOBILITY_ID uniqueidentifier, @P_MOBILITY_REVISION integer) AS
BEGIN
	DECLARE @LANGUAGE_SKILL_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT LANGUAGE_SKILL_ID
			FROM dbo.EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = @P_MOBILITY_ID
			AND MOBILITY_REVISION = @P_MOBILITY_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGUAGE_SKILL_ID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = @LANGUAGE_SKILL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_SKILL WHERE ID = @LANGUAGE_SKILL_ID
			FETCH NEXT FROM cur INTO @LANGUAGE_SKILL_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

	/*
		Borra una movilidad
	*/

CREATE PROCEDURE  dbo.BORRA_MOBILITY(@P_MOBILITY_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_count integer

	DECLARE @ID uniqueidentifier
	DECLARE @MOBILITY_REVISION integer			
	DECLARE @MOBILITY_PARTICIPANT_ID varchar(255)
	DECLARE @MOBILITY_TYPE_ID uniqueidentifier
	DECLARE @SENDER_CONTACT_ID uniqueidentifier
	DECLARE @SENDER_ADMV_CONTACT_ID uniqueidentifier
	DECLARE @RECEIVER_CONTACT_ID uniqueidentifier
	DECLARE @RECEIVER_ADMV_CONTACT_ID uniqueidentifier
	DECLARE @ISCED_CODE varchar(255)

	DECLARE @LEARNING_AGREEMENT_ID uniqueidentifier
	DECLARE @LEARNING_AGREEMENT_REVISION integer	

	DECLARE c_mobility CURSOR LOCAL FOR
			SELECT ID, MOBILITY_REVISION, 
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, 
				ISCED_CODE
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_MOBILITY_ID

	OPEN c_mobility
	FETCH NEXT FROM c_mobility INTO @ID, @MOBILITY_REVISION, 
				@MOBILITY_PARTICIPANT_ID, @MOBILITY_TYPE_ID, 
				@SENDER_CONTACT_ID, @SENDER_ADMV_CONTACT_ID, @RECEIVER_CONTACT_ID, @RECEIVER_ADMV_CONTACT_ID, 
				@ISCED_CODE
	WHILE @@FETCH_STATUS = 0
		BEGIN

			EXECUTE dbo.BORRA_MOBILITY_LANSKILL @ID, @MOBILITY_REVISION

			----------------------------------			
			DECLARE c_la CURSOR LOCAL FOR
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
					FROM dbo.EWPCV_MOBILITY_LA 
					WHERE MOBILITY_ID = @ID	AND MOBILITY_REVISION = @MOBILITY_REVISION
			OPEN c_la
			FETCH NEXT FROM c_la INTO @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.BORRA_LA @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
					FETCH NEXT FROM c_la INTO @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
				END
			CLOSE c_la
			DEALLOCATE c_la
			----------------------------------

			DELETE FROM dbo.EWPCV_MOBILITY WHERE ID = @ID AND MOBILITY_REVISION = @MOBILITY_REVISION

			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = @MOBILITY_PARTICIPANT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_STUDENT @MOBILITY_PARTICIPANT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @SENDER_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @SENDER_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = @SENDER_ADMV_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @SENDER_ADMV_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = @RECEIVER_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @RECEIVER_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = @RECEIVER_ADMV_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @RECEIVER_ADMV_CONTACT_ID

			FETCH NEXT FROM c_mobility INTO @ID, @MOBILITY_REVISION, 
				@MOBILITY_PARTICIPANT_ID, @MOBILITY_TYPE_ID, 
				@SENDER_CONTACT_ID, @SENDER_ADMV_CONTACT_ID, @RECEIVER_CONTACT_ID, @RECEIVER_ADMV_CONTACT_ID, 
				@ISCED_CODE

		END
	CLOSE c_mobility
	DEALLOCATE c_mobility

END
;
GO

	/*
		Persiste una firma en el sistema y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_SIGNATURE(@P_SIGNATURE xml, @return_value uniqueidentifier output) AS
BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @SIGNER_NAME varchar(255)
	DECLARE @SIGNER_POSITION varchar(255)
	DECLARE @SIGNER_EMAIL varchar(255)
	DECLARE @SIGN_DATE datetime
	DECLARE @SIGNER_APP varchar(255)
	DECLARE @SIGNATURE varchar(max)

	SET @v_id = NEWID()

	SELECT 
		@SIGNER_NAME = T.c.value('(signer_name)[1]', 'varchar(255)'),
		@SIGNER_POSITION = T.c.value('(signer_position)[1]', 'varchar(255)'),
		@SIGNER_EMAIL = T.c.value('(signer_email)[1]', 'varchar(255)'),
		@SIGN_DATE = T.c.value('(sign_date)[1]', 'datetime'),
		@SIGNER_APP = T.c.value('(signer_app)[1]', 'varchar(255)'),
		@SIGNATURE = T.c.value('(signature)[1]', 'varchar(max)')
	FROM @P_SIGNATURE.nodes('*') T(c)

	 INSERT INTO dbo.EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (@v_id, @SIGNER_NAME, @SIGNER_POSITION , @SIGNER_EMAIL , @SIGN_DATE , @SIGNER_APP, @SIGNATURE)

	SET @return_value = @v_id

	
END	
;

GO


	/*	
		Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_ACADEMIC_TERM(@P_ACADEMIC_TERM xml, @v_id uniqueidentifier output) as
BEGIN
	DECLARE @v_a_id uniqueidentifier
	DECLARE @v_o_id uniqueidentifier
	DECLARE @v_li_id uniqueidentifier

	DECLARE @ACADEMIC_YEAR varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
	DECLARE @TOTAL_TERMS varchar(255)
	DECLARE @TERM_NUMBER varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @DESCRIPTION xml
	DECLARE @TEXT_ITEM xml
		
	SELECT 
		@ACADEMIC_YEAR = T.c.value('(academic_year)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@TOTAL_TERMS = T.c.value('(total_terms)[1]', 'varchar(255)'),
		@TERM_NUMBER = T.c.value('(term_number)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_ACADEMIC_TERM.nodes('academic_term') T(c)

	SELECT @v_a_id = ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(SUBSTRING(@ACADEMIC_YEAR, 1, 4))
			AND	UPPER(START_YEAR) =  UPPER(SUBSTRING(@ACADEMIC_YEAR, 6, 4))

	IF (@v_a_id is null)
	BEGIN
		SET @v_a_id = NEWID()
		INSERT INTO dbo.EWPCV_ACADEMIC_YEAR (ID, END_YEAR, START_YEAR)
			VALUES(@v_a_id, SUBSTRING(@ACADEMIC_YEAR, 1, 4), SUBSTRING(@ACADEMIC_YEAR, 6, 4))
	END 


	SELECT @v_o_id = O.ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN dbo.EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)

	SET @v_id = NEWID()

	INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (@v_id, @END_DATE, @START_DATE , @INSTITUTION_ID , @v_o_id, @v_a_id, @TOTAL_TERMS, @TERM_NUMBER)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @TEXT_ITEM
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TEXT_ITEM , @v_li_id output
			INSERT INTO dbo.EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @TEXT_ITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;

GO

	/*
		Persiste creditos en el sistema y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_CREDIT(@P_CREDIT xml, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @SCHEME varchar(255)
	DECLARE @CREDIT_VALUE decimal(5,1)

	SELECT 
		@SCHEME = T.c.value('(scheme)[1]', 'varchar(255)'),
		@CREDIT_VALUE = T.c.value('(credit_value)[1]', 'decimal(5,1)')
	FROM @P_CREDIT.nodes('credit') T(c) 
	
	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
		VALUES (@v_id, @SCHEME, @CREDIT_VALUE)

END

;
GO

	/*
		Persiste un componente en el sistema y retorna el UUID generado para el registro.
	*/

CREATE PROCEDURE  dbo.INSERTA_COMPONENTE(@P_COMPONENT xml, @P_LA_ID uniqueidentifier, @P_LA_REVISION integer,  @v_id uniqueidentifier output) AS

BEGIN
	DECLARE @v_at_id uniqueidentifier
	DECLARE	@v_c_id uniqueidentifier

	DECLARE @LOS_CODE varchar(255)
	DECLARE @STATUS integer
	DECLARE @TITLE varchar(255)
	DECLARE @REASON_CODE integer
	DECLARE @REASON_TEXT varchar(255)
	DECLARE @LA_COMPONENT_TYPE integer
	DECLARE @RECOGNITION_CONDITIONS varchar(255)
	DECLARE @SHORT_DESCRIPTION varchar(255)
	DECLARE @CREDIT xml
	DECLARE @ACADEMIC_TERM xml
	   
	SELECT
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@STATUS = T.c.value('(status)[1]', 'integer'),
		@TITLE = T.c.value('(title)[1]', 'varchar(255)'),
		@REASON_CODE = T.c.value('(reason_code)[1]', 'integer'),
		@REASON_TEXT = T.c.value('(reason_text)[1]', 'varchar(255)'),
		@LA_COMPONENT_TYPE = T.c.value('(la_component_type)[1]', 'integer'),
		@RECOGNITION_CONDITIONS = T.c.value('(recognition_conditions)[1]', 'varchar(255)'),
		@SHORT_DESCRIPTION = T.c.value('(short_description)[1]', 'varchar(255)'),
		@CREDIT = T.c.query('credit'),
		@ACADEMIC_TERM = T.c.query('academic_term')
	FROM @P_COMPONENT.nodes('la_component') T(c)
	SET @v_id = NEWID()

	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM, @v_at_id output

	-------------------
	--FALTAN LOS Y LOIS
	-------------------

	INSERT INTO dbo.EWPCV_LA_COMPONENT (ID, ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (@v_id, @v_at_id, NULL, @LOS_CODE, NULL, @STATUS, @TITLE, @REASON_CODE, @REASON_TEXT,
				@LA_COMPONENT_TYPE, @RECOGNITION_CONDITIONS, @SHORT_DESCRIPTION)

	EXECUTE dbo.INSERTA_CREDIT @CREDIT, @v_c_id output
	INSERT INTO dbo.EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (@v_id, @v_c_id)

	IF @LA_COMPONENT_TYPE = 0
		INSERT INTO dbo.EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 1
		INSERT INTO dbo.EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 2
		INSERT INTO dbo.EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 3
		INSERT INTO dbo.EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 4
		INSERT INTO dbo.EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

END
;
GO

	/*
		Persiste una revision de un learning agreement.
	*/

CREATE PROCEDURE  dbo.INSERTA_LA_REVISION(@P_LA_ID uniqueidentifier, @P_LA xml, @P_LA_REVISION integer, @P_MOBILITY_REVISION integer, @return_value uniqueidentifier output)  AS 

BEGIN
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_stud_sign_id uniqueidentifier
	DECLARE @v_s_hei_sign_id uniqueidentifier
	DECLARE @v_changes_id varchar(255)
	DECLARE @v_c_id uniqueidentifier

	DECLARE @MOBILITY_ID uniqueidentifier
	DECLARE @STUDENT_SIGNATURE xml
	DECLARE @SENDING_HEI_SIGNATURE xml
	DECLARE @LA_COMPONENT xml

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier'),
		@STUDENT_SIGNATURE = T.c.query('student_signature'),
		@SENDING_HEI_SIGNATURE = T.c.query('sending_hei_signature')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.INSERTA_SIGNATURE @STUDENT_SIGNATURE, @v_stud_sign_id output
	EXECUTE dbo.INSERTA_SIGNATURE @SENDING_HEI_SIGNATURE, @v_s_hei_sign_id output

	IF @P_LA_ID IS NULL  
		SET @v_la_id = NEWID()
	ELSE 
		SET @v_la_id = @P_LA_ID

	SET @v_changes_id = CAST(@v_la_id AS varchar(255)) + '-' + CAST(@P_LA_REVISION AS varchar(255))

	INSERT INTO dbo.EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (@v_la_id, @P_LA_REVISION, @v_stud_sign_id, @v_s_hei_sign_id, 0, SYSDATETIME(), @v_changes_id);

	INSERT INTO dbo.EWPCV_MOBILITY_LA (MOBILITY_ID, MOBILITY_REVISION, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(@MOBILITY_ID, @P_MOBILITY_REVISION, @v_la_id, @P_LA_REVISION);

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_LA.nodes('learning_agreement/la_component_list/la_component') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @LA_COMPONENT
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_COMPONENTE @LA_COMPONENT, @v_la_id, @P_LA_REVISION, @v_c_id output
			FETCH NEXT FROM cur INTO @LA_COMPONENT
		END
	CLOSE cur
	DEALLOCATE cur
	
	SET @return_value = @v_la_id

END 
; 
GO

/*
	Actualiza una movilidad 
*/

CREATE PROCEDURE  dbo.ACTUALIZA_MOBILITY(@P_OMOBILITY_ID uniqueidentifier, @P_MOBILITY xml) AS 
BEGIN

	DECLARE @v_m_revision VARCHAR(255);
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_count integer;

	DECLARE @v_mtype_id uniqueidentifier
	DECLARE @v_scontact_id uniqueidentifier
	DECLARE @v_sacontact_id uniqueidentifier
	DECLARE @v_rcontact_id uniqueidentifier
	DECLARE @v_racontact_id uniqueidentifier

	DECLARE @v_participant_id VARCHAR(255);
	DECLARE @v_student_contact_id VARCHAR(255);

	DECLARE @STUDENT_LANGUAGE_SKILL xml
	DECLARE @STUDENT_CONTACT_PERSON xml
	DECLARE @MOBILITY_TYPE VARCHAR(255)
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml

	SELECT 
		@STUDENT_CONTACT_PERSON = T.c.query('student/contact_person'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)')
	FROM @P_MOBILITY.nodes('mobility') T(c)
	SET @STUDENT_CONTACT_PERSON = @P_MOBILITY.query('mobility/student/contact_person')


	SELECT 
		@v_m_revision = MOBILITY_REVISION, 
		@v_mtype_id = MOBILITY_TYPE_ID,
		@v_scontact_id = SENDER_CONTACT_ID, 
		@v_sacontact_id = SENDER_ADMV_CONTACT_ID, 
		@v_rcontact_id = RECEIVER_CONTACT_ID, 
		@v_racontact_id = RECEIVER_ADMV_CONTACT_ID,
		@v_participant_id = MOBILITY_PARTICIPANT_ID
	FROM dbo.EWPCV_MOBILITY
		WHERE ID = @P_OMOBILITY_ID
		ORDER BY MOBILITY_REVISION DESC

	EXECUTE dbo.BORRA_MOBILITY_LANSKILL @P_OMOBILITY_ID, @v_m_revision

	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @P_MOBILITY.nodes('mobility/student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (@P_OMOBILITY_ID, @v_m_revision, @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

	UPDATE dbo.EWPCV_MOBILITY SET MOBILITY_TYPE_ID = NULL, SENDER_CONTACT_ID = NULL, SENDER_ADMV_CONTACT_ID = NULL,
			RECEIVER_CONTACT_ID = NULL, RECEIVER_ADMV_CONTACT_ID = NULL
			WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision

	SELECT @v_student_contact_id = CONTACT_ID FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @v_participant_id

	UPDATE dbo.EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = NULL WHERE ID = @v_participant_id
	
	EXECUTE dbo.BORRA_CONTACT @v_participant_id
	
	EXECUTE dbo.INSERTA_CONTACT_PERSON @STUDENT_CONTACT_PERSON, @v_student_contact_id output
	
	UPDATE dbo.EWPCV_MOBILITY_PARTICIPANT SET CONTACT_ID = @v_student_contact_id WHERE ID = @v_participant_id
	

	SELECT @v_count = COUNT(1)  FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @v_scontact_id
	IF @v_count = 0
		EXECUTE dbo.BORRA_CONTACT @v_scontact_id
	
	SELECT @v_count = COUNT(1)  FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @v_sacontact_id
	IF @v_count = 0
		EXECUTE dbo.BORRA_CONTACT @v_sacontact_id
	
	SELECT @v_count = COUNT(1)  FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @v_rcontact_id
	IF @v_count = 0
		EXECUTE dbo.BORRA_CONTACT @v_rcontact_id
		
	SELECT @v_count = COUNT(1)  FROM EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @v_racontact_id
	IF @v_count = 0
		EXECUTE dbo.BORRA_CONTACT @v_racontact_id

	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE, @v_mtype_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT, @v_scontact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT, @v_sacontact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT, @v_rcontact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT, @v_racontact_id output

END 
;
GO


/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/

CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_LA(@P_LA_ID VARCHAR(255), @P_LA_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY M
			INNER JOIN dbo.EWPCV_MOBILITY_LA MLA 
			ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		IF @v_r_hei = @P_HEI_TO_NOTIFY 
			SET @return_value = @v_s_hei
		ELSE 
			SET @return_value = null

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/

CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_MOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF @v_r_hei = @P_HEI_TO_NOTIFY 
			SET @return_value = @v_s_hei
		ELSE 
			SET @return_value = null

END
;
GO



	/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
		Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
		Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

ALTER PROCEDURE dbo.INSERT_MOBILITY(@P_MOBILITY xml, @P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, @P_OMOBILITY_ID OUTPUT

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


	/* Elimina una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a eliminar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/

ALTER PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_m_revision integer
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		SET @return_value = 0
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count >0
			BEGIN
				SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.'
					SET @return_value = -1
				END
				ELSE
				BEGIN
					EXECUTE dbo.BORRA_MOBILITY @P_OMOBILITY_ID
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END
			END
		ELSE
			BEGIN
				SET @P_ERROR_MESSAGE = 'La movilidad no existe'
				SET @return_value = -1
			END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_MOBILITY'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

	/* Actualiza una movilidad del sistema.
		Recibe como parametro el identificador de la movilidad a actualizar
		Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
	
*/

ALTER PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE
		BEGIN
			IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			BEGIN
				SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                SET @return_value = -1
			END
 		END
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_sending_hei
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


	/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
		Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

*/

ALTER PROCEDURE dbo.INSERT_LEARNING_AGREEMENT(@P_LA XML, @P_LA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_m_revision integer
	DECLARE @v_la_id uniqueidentifier
	DECLARE @MOBILITY_ID varchar(255)

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
					FROM dbo.EWPCV_MOBILITY 
					WHERE ID = @MOBILITY_ID
		IF @v_m_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = LEARNING_AGREEMENT_ID 
					FROM dbo.EWPCV_MOBILITY_LA 
					WHERE MOBILITY_ID = @MOBILITY_ID
		IF @v_la_id IS NOT NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'Ya existe un Learning Agreement asociado a la mobilidad indicada, por favor actualice o elimine el learning agreement: ' + CAST(@v_la_id as varchar(255)) + '.'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_LA_REVISION NULL, @P_LA, 0, @v_m_revision, @P_LA_ID output
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion receptora'
					SET @P_LA_ID = NULL
					SET @return_value = -1
					ROLLBACK TRANSACTION
				END 
				ELSE
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					COMMIT TRANSACTION
				END
		END TRY
		
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


	/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
		Recibe como parametro el identificador del learning agreement a actualizar.
		El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
		Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.

	*/

ALTER PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_LA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_la_revision integer
	DECLARE @v_m_revision integer
	DECLARE @v_no_firmado integer
	DECLARE @MOBILITY_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_la_revision_siguiente integer

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END
	END
	   	 
	IF @return_value = 0
	BEGIN
		SELECT @v_la_revision = MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
		IF @v_la_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
			FROM dbo.EWPCV_MOBILITY_LA
				WHERE MOBILITY_ID = @MOBILITY_ID
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @v_la_revision;
		IF @v_m_revision IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada para el learning agreement no es correcta.'
			SET @return_value = -1
		END 
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_no_firmado = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND STATUS = 0
		IF @v_no_firmado > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.'
			SET @return_value = -1
		END 
	END


	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @v_la_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_la_revision_siguiente = @v_la_revision + 1
			EXECUTE dbo.INSERTA_LA_REVISION @P_LA_ID, @P_LA, @v_la_revision_siguiente, @v_m_revision, @v_la_id output
			EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					   			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

	/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/

ALTER PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID uniqueidentifier, @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_sign_id uniqueidentifier
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_ACCEPT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT TOP 1 @v_mobility_id = MOBILITY_ID FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] ('+@sending_hei_id+') no es coherente con los datos registrados ('+@v_sending_hei_id+').'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] ('+@mobility_id+') no es coherente con los datos registrados ('+@v_mobility_id+').'
		END

	END


	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;

;
GO

	/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
		Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

ALTER PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID uniqueidentifier, @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN


	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_sign_id uniqueidentifier
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_COMMENT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.ID = MLA.MOBILITY_ID AND M.MOBILITY_REVISION = MLA.MOBILITY_REVISION
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT TOP 1 @v_mobility_id = MOBILITY_ID FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] no es coherente con los datos registrados.'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] no es coherente con los datos registrados.'
		END

	END
		
	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_REJECT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;

;
GO


/*

Guarda el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests

*/
CREATE PROCEDURE  dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml) AS

BEGIN

	DECLARE @v_id uniqueidentifier
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (@v_id, @P_SENDING_HEI, 0, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME())

END
;
GO


/*

Guarda el xml de update la de tipo rechazo y lo persiste en la tabla de update requests

*/
CREATE PROCEDURE  dbo.INSERTA_REJECT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml) AS

BEGIN

	DECLARE @v_id uniqueidentifier
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE) 
			VALUES (@v_id, @P_SENDING_HEI, 1, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME())

END
;
GO



/*
valida el xml de tipo rechazo de learning agreement

*/

CREATE PROCEDURE  VALIDA_COMMENT_PROPOSAL(@P_PROPOSAL xml,  @errMsg varchar(255) output, @errNum integer output) as 
BEGIN

	DECLARE @sid varchar(255),  @mid varchar(255),  @cid varchar(255),  @comment varchar(255)
	DECLARE @signam varchar(255),  @sigpos varchar(255), @sigmail varchar(255),  @sigtime varchar(255),  @sigapp varchar(255)
	;
	WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req,
		'https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd' as la)

	SELECT
		@sid=T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
		@mid=T.c.value('(req:comment-proposal-v1/req:omobility-id)[1]', 'varchar(255)'),
		@cid=T.c.value('(req:comment-proposal-v1/req:changes-proposal-id)[1]', 'varchar(255)'),
		@comment=T.c.value('(req:comment-proposal-v1/req:comment)[1]', 'varchar(255)'),
		@signam=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-name)[1]', 'varchar(255)'),
		@sigpos=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-position)[1]', 'varchar(255)'),
		@sigmail=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-email)[1]', 'varchar(255)'),
		@sigtime=T.c.value('(req:comment-proposal-v1/req:signature/la:timestamp)[1]', 'varchar(255)'),
		@sigapp=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-app)[1]', 'varchar(255)')
	FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

	IF @sid is null or @sid = ''
		SET @errMsg = @errMsg + ' sending-hei-id'
	IF @mid is null or @mid = ''
		SET @errMsg = @errMsg + ' omobility-id'
	IF @cid is null or @cid = ''
		SET @errMsg = @errMsg + ' changes-proposal-id'
	IF @comment is null or @comment = ''
		SET @errMsg = @errMsg + ' comment'
	IF @signam is null or @signam = ''
		SET @errMsg = @errMsg + ' signer-name'
	IF @sigpos is null or @sigpos = ''
		SET @errMsg = @errMsg + ' signer-position'
	IF @sigmail is null or @sigmail = ''
		SET @errMsg = @errMsg + ' signer-email'
	IF @sigtime is null or @sigtime = ''
		SET @errMsg = @errMsg + ' timestamp'
	IF @sigapp is null or @sigapp = ''
		SET @errMsg = @errMsg + ' signer-app'

	if @errMsg is null
		set @errNum = 0
	ELSE
	BEGIN
		set @errNum = -1
		set @errMsg = ' Valores nulos en: ' + @errMsg + ']' 
	END

END
;
GO





/*
valida el xml de tipo aceptacion de learning agreement

*/

CREATE PROCEDURE  VALIDA_ACCEPT_PROPOSAL(@P_PROPOSAL xml,  @errMsg varchar(255) output, @errNum integer output) as 
BEGIN

	DECLARE @sid varchar(255),  @mid varchar(255),  @cid varchar(255),  @comment varchar(255)
	DECLARE @signam varchar(255),  @sigpos varchar(255), @sigmail varchar(255),  @sigtime varchar(255),  @sigapp varchar(255)
	;
	WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req,
		'https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd' as la)

	SELECT
		@sid=T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
		@mid=T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)'),
		@cid=T.c.value('(req:approve-proposal-v1/req:changes-proposal-id)[1]', 'varchar(255)'),
		@signam=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-name)[1]', 'varchar(255)'),
		@sigpos=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-position)[1]', 'varchar(255)'),
		@sigmail=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-email)[1]', 'varchar(255)'),
		@sigtime=T.c.value('(req:approve-proposal-v1/req:signature/la:timestamp)[1]', 'varchar(255)'),
		@sigapp=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-app)[1]', 'varchar(255)')
	FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

	IF @sid is null or @sid = ''
		SET @errMsg = @errMsg + ' sending-hei-id'
	IF @mid is null or @mid = ''
		SET @errMsg = @errMsg + ' omobility-id'
	IF @cid is null or @cid = ''
		SET @errMsg = @errMsg + ' changes-proposal-id'
	IF @comment is null or @comment = ''
		SET @errMsg = @errMsg + ' comment'
	IF @signam is null or @signam = ''
		SET @errMsg = @errMsg + ' signer-name'
	IF @sigpos is null or @sigpos = ''
		SET @errMsg = @errMsg + ' signer-position'
	IF @sigmail is null or @sigmail = ''
		SET @errMsg = @errMsg + ' signer-email'
	IF @sigtime is null or @sigtime = ''
		SET @errMsg = @errMsg + ' timestamp'
	IF @sigapp is null or @sigapp = ''
		SET @errMsg = @errMsg + ' signer-app'

	if @errMsg is null
		set @errNum = 0
	ELSE
	BEGIN
		set @errNum = -1
		set @errMsg = ' Valores nulos en: ' + @errMsg + ']' 
	END

END
;
GO


/* Elimina un learning agreement del sistema.
		Recibe como parametro el identificador del learning agreement a actualizar
		Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Se debe indicar el codigo SCHAC de la institucion a notificar
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/

ALTER PROCEDURE dbo.DELETE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @LEARNING_AGREEMENT_REVISION integer
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_m_id uniqueidentifier
	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY IS NULL  
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
		IF @v_count = 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'
			SET @return_value = -1
		END
	END

	SELECT @LEARNING_AGREEMENT_REVISION = LEARNING_AGREEMENT_REVISION FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION, @P_HEI_TO_NOTIFY, @v_notifier_hei output

	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1	
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			IF @v_m_id is NULL
				SELECT @v_m_id = MOBILITY_ID
					FROM dbo.EWPCV_MOBILITY_LA 
					WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LEARNING_AGREEMENT_REVISION

			EXECUTE dbo.BORRA_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION
			EXECUTE dbo.INSERTA_NOTIFICATION @v_m_id, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;
GO

/*
***************************************
************* VISTAS ******************
***************************************
*/
---------------------------------------------
-- FUNCIONES PARA LAS VISTAS
---------------------------------------------  

CREATE FUNCTION dbo.EXTRAE_EMAILS(@P_CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
select @return_value = stuff( (select '|;|' + EMAIL
               FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL 
			   WHERE CONTACT_DETAILS_ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_TELEFONO(@P_CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + P.E164 + '|:|' + P.EXTENSION_NUMBER + '|:|' + P.OTHER_FORMAT
			   FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_PHONE_NUMBER P
				WHERE P.ID = CD.PHONE_NUMBER
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_URLS_CONTACTO(@P_CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			   FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_CONTACT_URL U,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE CD.ID = U.CONTACT_DETAILS_ID
					AND U.URL_ID = LAIT.ID
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_EQF_LEVEL(@P_COOP_CON_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + EQF_LEVEL
			   FROM dbo.EWPCV_COOPCOND_EQFLVL
			   WHERE COOPERATION_CONDITION_ID = @P_COOP_CON_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_CREDITS(@P_COMPONENT_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + COALESCE(CR.CREDIT_LEVEL,'') + '|:|' + CR.SCHEME + '|:|' + CAST(CR.CREDIT_VALUE AS VARCHAR)
			FROM dbo.EWPCV_CREDIT CR,
				dbo.EWPCV_COMPONENT_CREDITS CCR
			WHERE CR.ID = CCR.CREDITS_ID
				AND CCR.COMPONENT_ID = @P_COMPONENT_ID
               for xml path ('')
              ), 1, 3, ''
            );

	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_ACADEMIC_TERM(@P_AT_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + CONVERT(varchar, ATR.START_DATE, 102)  + '|:|' + CONVERT(varchar, ATR.END_DATE, 102) 
				+ '|:|' + ATR.INSTITUTION_ID + '|:|' + o.ORGANIZATION_UNIT_CODE 
				+ '|:|' + CONVERT(varchar, ATR.TERM_NUMBER) + '|:|' + CONVERT(varchar, ATR.TOTAL_TERMS)
			FROM dbo.EWPCV_ACADEMIC_TERM ATR
			LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
			WHERE ATR.ID = @P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(@P_AT_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value =stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_ACADEMIC_TERM_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE N.DISP_NAME_ID = LAIT.ID
					AND N.ACADEMIC_TERM_ID = @P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_NOMBRES_OUNIT(@P_OUNIT_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value =stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
		FROM dbo.EWPCV_ORGANIZATION_UNIT O,
			dbo.EWPCV_ORGANIZATION_UNIT_NAME N,
			dbo.EWPCV_LANGUAGE_ITEM LAIT
		WHERE O.ID = N.ORGANIZATION_UNIT_ID
			AND N.NAME_ID = LAIT.ID
			AND O.ID = @P_OUNIT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_NOMBRES_LOS(@P_LOS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_LOS_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE N.NAME_ID = LAIT.ID
					AND N.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_DESCRIPTIONS_LOS(@P_LOS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_LOS_DESCRIPTION D,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE D.DESCRIPTION_ID = LAIT.ID
					AND D.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_URLS_LOS(@P_LOS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_LOS_URLS U,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE U.URL_ID = LAIT.ID
					AND U.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO
 
CREATE FUNCTION dbo.EXTRAE_S_AREA_L_SKILL(@P_COOP_CON_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + S.ISCED_CODE + '|:|' + COALESCE(S.ISCED_CLARIFICATION,'') + '|:|' + L."LANGUAGE" + '|:|' + L.CEFR_LEVEL
			    FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
				LEFT JOIN dbo.EWPCV_LANGUAGE_SKILL L	
					ON CSL.LANGUAGE_SKILL_ID = L.ID
				LEFT JOIN dbo.EWPCV_SUBJECT_AREA S	
					ON CSL.ISCED_CODE = S.ID	
				WHERE CSL.COOPERATION_CONDITION_ID = @P_COOP_CON_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_NOMBRES_INSTITUCION(@P_INSTITUTION_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_INSTITUTION I,
					dbo.EWPCV_INSTITUTION_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE I.ID = N.INSTITUTION_ID
					AND N.NAME_ID = LAIT.ID
					AND I.INSTITUTION_ID = @P_INSTITUTION_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_NOMBRES_CONTACTO(@P_CONTACTO_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_CONTACT C,
					dbo.EWPCV_CONTACT_NAME CN,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE C.ID = CN.CONTACT_ID
					AND CN.NAME_ID = LAIT.ID
					AND C.ID = @P_CONTACTO_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_DESCRIPCIONES_CONTACTO(@P_CONTACTO_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			    FROM dbo.EWPCV_CONTACT C,
					dbo.EWPCV_CONTACT_DESCRIPTION CD,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE C.ID = CD.CONTACT_ID
					AND CD.DESCRIPTION_ID = LAIT.ID
					AND C.ID = @P_CONTACTO_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_ADDRESS_LINES(@P_FLEXIBLE_ADDRESS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + COALESCE(ADDRESS_LINE,'')
			    FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE 
				WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO 

CREATE FUNCTION dbo.EXTRAE_FLEXAD_DELIV_POINT(@P_FLEXIBLE_ADDRESS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + COALESCE(Delivery_point_code,'')
			    FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD
        WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

CREATE FUNCTION dbo.EXTRAE_ADDRESS(@P_FLEXIBLE_ADDRESS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + COALESCE(F.BUILDING_NUMBER,'') + '|:|' + COALESCE(F.BUILDING_NAME,'') + '|:|' + COALESCE(F.STREET_NAME ,'')
					+ '|:|' + COALESCE(F.UNIT,'') + '|:|' + COALESCE(F."FLOOR",'') + '|:|' + COALESCE(F.POST_OFFICE_BOX,'')
					+ '|:|' + (
					dbo.EXTRAE_FLEXAD_DELIV_POINT(F.ID)
					)
			    FROM dbo.EWPCV_FLEXIBLE_ADDRESS F
				WHERE F.ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

 
CREATE FUNCTION  dbo.EXTRAE_NIVELES_IDIOMAS(@P_MOBILITY_ID uniqueidentifier, @P_REVISION integer) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + S."LANGUAGE" + '|:|' + S.CEFR_LEVEL
			    FROM dbo.EWPCV_MOBILITY_LANG_SKILL MLS,
					dbo.EWPCV_LANGUAGE_SKILL S
				WHERE MLS.LANGUAGE_SKILL_ID = S.ID
					AND MLS.MOBILITY_ID = @P_MOBILITY_ID
					AND MLS.MOBILITY_REVISION = @P_REVISION
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO
 
CREATE FUNCTION dbo.EXTRAE_FIRMAS (@P_FIRMA_ID uniqueidentifier) RETURNS varchar(max)  as
BEGIN
    DECLARE @v_lista VARCHAR(max)
    SELECT @v_lista = stuff( (select '|;|' + COALESCE(SIGNER_NAME,'') + '|' + COALESCE(SIGNER_POSITION,'') 
				+ '|' + COALESCE(SIGNER_EMAIL,'') + '|' + COALESCE(CAST("TIMESTAMP" AS VARCHAR(max)),'') + '|' + COALESCE(SIGNER_APP,'')
			    FROM dbo.EWPCV_SIGNATURE
				WHERE ID = @P_FIRMA_ID
               for xml path ('')
              ), 1, 3, ''
            );

    RETURN @v_lista

END
;
GO
 
---------------------------------------------
-- VISTAS
---------------------------------------------  

ALTER VIEW dbo.DICTIONARIES_VIEW as 
  SELECT 
	ID 				AS ID,
	CODE 			AS CODE,
	DESCRIPTION 	AS DESCRIPTION,
	DICTIONARY_TYPE AS DICTIONARY_TYPE,
	CALL_YEAR 		AS DICTIONARY_YEAR
FROM dbo.EWPCV_DICTIONARY
;
GO


ALTER VIEW dbo.EVENT_VIEW ("ID", "EVENT_DATE", "TRIGGERING_HEI", "OWNER_HEI", "EVENT_TYPE", "CHANGED_ELEMENT_ID", "ELEMENT_TYPE", "OBSERVATIONS", "STATUS") AS 
  SELECT 
ID					AS ID,
EVENT_DATE			AS EVENT_DATE,
TRIGGERING_HEI		AS TRIGGERING_HEI,
OWNER_HEI			AS OWNER_HEI,
EVENT_TYPE			AS EVENT_TYPE,
CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
ELEMENT_TYPE		AS ELEMENT_TYPE,
OBSERVATIONS		AS OBSERVATIONS,
STATUS				AS STATUS
FROM dbo.EWPCV_EVENT
;
GO

ALTER VIEW dbo.FACTSHEET_ADD_INFO_VIEW ("INSTITUTION_ID", "INFO_TYPE", "EMAIL", "PHONE", "URLS") AS 

  SELECT 
	I.INSTITUTION_ID 		 			        AS INSTITUTION_ID,
	AI."TYPE"		 					        AS INFO_TYPE,
	dbo.EXTRAE_EMAILS(AI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(AI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(AI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_INFORMATION_ITEM AI
	ON IIT.INFORMATION_ID = AI.ID
    AND AI."TYPE" NOT IN ('HOUSING','VISA','INSURANCE');
GO

ALTER VIEW dbo.FACTSHEET_REQ_INFO_VIEW ("INSTITUTION_ID", "REQ_TYPE", "NAME", "DESCRIPTION", "EMAIL", "PHONE", "URLS") AS 

 SELECT 
	I.INSTITUTION_ID 							 AS INSTITUTION_ID,
	REI."TYPE" 								     AS REQ_TYPE,
	REI.NAME 								    AS NAME,
	REI.DESCRIPTION 						     AS DESCRIPTION,
    dbo.EXTRAE_EMAILS(REI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(REI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(REI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INS_REQUIREMENTS IR
	ON IR.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_REQUIREMENTS_INFO REI
	ON IR.REQUIREMENT_ID = REI.ID;
GO

ALTER VIEW dbo.FACTSHEET_VIEW ("INSTITUTION_ID", "ABREVIATION", "DECISION_WEEK_LIMIT", "TOR_WEEK_LIMIT", "NOMINATIONS_AUTUM_TERM", "NOMINATIONS_SPRING_TERM", "APPLICATION_AUTUM_TERM", "APPLICATION_SPRING_TERM", "APPLICATION_EMAIL", "APPLICATION_PHONE", "APPLICATION_URLS", "HOUSING_EMAIL", "HOUSING_PHONE", "HOUSING_URLS", "VISA_EMAIL", "VISA_PHONE", "VISA_URLS", "INSURANCE_EMAIL", "INSURANCE_PHONE", "INSURANCE_URLS") AS 
  SELECT 
	I.INSTITUTION_ID    				  	 	 	AS INSTITUTION_ID,
	I.ABBREVIATION	 					  	 	 	AS ABREVIATION,
	MAX(F.DECISION_WEEKS_LIMIT)			  	 	 	AS DECISION_WEEK_LIMIT,
	MAX(F.TOR_WEEKS_LIMIT) 					  	 	AS TOR_WEEK_LIMIT,
	FORMAT(MAX(F.NOMINATIONS_AUTUM_TERM), '--MM-dd' )  	AS NOMINATIONS_AUTUM_TERM,
	FORMAT(MAX(F.NOMINATIONS_SPRING_TERM), '--MM-dd' ) 	AS NOMINATIONS_SPRING_TERM,
	FORMAT(MAX(F.APPLICATION_AUTUM_TERM), '--MM-dd' ) 	AS APPLICATION_AUTUM_TERM,
	FORMAT(MAX(F.APPLICATION_SPRING_TERM), '--MM-dd' ) 	AS APPLICATION_SPRING_TERM,
	dbo.EXTRAE_EMAILS(MAX(F.CONTACT_DETAILS_ID))	  	AS APPLICATION_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(F.CONTACT_DETAILS_ID)) 	 	AS APPLICATION_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(F.CONTACT_DETAILS_ID)) AS APPLICATION_URLS,
	dbo.EXTRAE_EMAILS(MAX(HI.CONTACT_DETAIL_ID))	  	AS HOUSING_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(HI.CONTACT_DETAIL_ID)) 	 	AS HOUSING_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(HI.CONTACT_DETAIL_ID))	AS HOUSING_URLS,
	dbo.EXTRAE_EMAILS(MAX(VI.CONTACT_DETAIL_ID))	  	AS VISA_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(VI.CONTACT_DETAIL_ID)) 	  	AS VISA_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(VI.CONTACT_DETAIL_ID)) AS VISA_URLS,
	dbo.EXTRAE_EMAILS(MAX(II.CONTACT_DETAIL_ID))	  	AS INSURANCE_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(II.CONTACT_DETAIL_ID)) 	 	AS INSURANCE_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID))	AS INSURANCE_URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_FACT_SHEET F 
    ON I.FACT_SHEET = F.ID
LEFT JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM HI
	ON IIT.INFORMATION_ID = HI.ID AND HI."TYPE" = 'HOUSING'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM VI
	ON IIT.INFORMATION_ID = VI.ID AND VI."TYPE" = 'VISA'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM II
	ON IIT.INFORMATION_ID = II.ID AND II."TYPE" = 'INSURANCE'
GROUP BY I.INSTITUTION_ID,I.ABBREVIATION;

GO

ALTER VIEW dbo.IIA_COOP_CONDITIONS_VIEW ("COOPERATION_CONDITION_ID", "IIA_ID", "IIA_CODE", "REMOTE_IIA_ID", "REMOTE_IIA_CODE", "IIA_START_DATE", "IIA_END_DATE", "IIA_MODIFY_DATE", "IIA_APPROVAL_DATE", "COOP_COND_START_DATE", "COOP_COND_END_DATE", "COOP_COND_EQF_LEVEL", "COOP_COND_DURATION", "COOP_COND_PARTICIPANTS", "COOP_COND_OTHER_INFO", "MOBILITY_TYPE", "SUBJECT_AREAS", "RECEIVING_INSTITUTION", "RECEIVING_INSTITUTION_NAMES", "RECEIVING_OUNIT_CODE", "RECEIVING_OUNIT_NAMES", "RECEIVER_SIGNING_DATE", "RECEIVER_SIGNER_NAME", "RECEIVER_SIGNER_LAST_NAME", "RECEIVER_SIGNER_BIRTH_DATE", "RECEIVER_SIGNER_GENDER", "RECEIVER_SIGNER_CITIZENSHIP", "RECEIVER_SIGNER_CONTACT_NAMES", "RECEIVER_SIGNER_CONTACT_DESCS", "RECEIVER_SIGNER_CONTACT_URLS", "RECEIVER_SIGNER_EMAILS", "RECEIVER_SIGNER_PHONES", "RECEIVER_SIGNER_ADDRESS_LINES", "RECEIVER_SIGNER_ADDRESS", "RECEIVER_SIGNER_POSTAL_CODE", "RECEIVER_SIGNER_LOCALITY", "RECEIVER_SIGNER_REGION", "RECEIVER_SIGNER_COUNTRY", "SENDING_INSTITUTION", "SENDING_INSTITUTION_NAMES", "SENDING_OUNIT_CODE", "SENDING_OUNIT_NAMES", "SENDER_SIGNING_DATE", "SENDER_SIGNER_NAME", "SENDER_SIGNER_LAST_NAME", "SENDER_SIGNER_BIRTH_DATE", "SENDER_SIGNER_GENDER", "SENDER_SIGNER_CITIZENSHIP", "SENDER_SIGNER_CONTACT_NAMES", "SENDER_SIGNER_CONTACT_DESCS", "SENDER_SIGNER_CONTACT_URLS", "SENDER_SIGNER_EMAILS", "SENDER_SIGNER_PHONES", "SENDER_SIGNER_ADDRESS_LINES", "SENDER_SIGNER_ADDRESS", "SENDER_SIGNER_POSTAL_CODE", "SENDER_SIGNER_LOCALITY", "SENDER_SIGNER_REGION", "SENDER_SIGNER_COUNTRY") AS 

  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                                AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                           AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                          AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)        AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                          AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                       AS RECEIVER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                               AS RECEIVER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                         AS RECEIVER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(RPSC.ID)                                                                                 AS RECEIVER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                               AS RECEIVER_SIGNER_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                             AS RECEIVER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                                   AS RECEIVER_SIGNER_ADDRESS,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                          AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)        AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                          AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                       AS SENDER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                               AS SENDER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                         AS SENDER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSC.ID)                                                                                 AS SENDER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                               AS SENDER_SIGNER_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                             AS SENDER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                                   AS SENDER_SIGNER_ADDRESS,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN dbo.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN dbo.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
GO

ALTER VIEW dbo.IIAS_VIEW ("IIA_ID", "IIA_CODE", "REMOTE_IIA_ID", "REMOTE_IIA_CODE", "IIA_START_DATE", "IIA_END_DATE", "IIA_MODIFY_DATE", "IIA_APPROVAL_DATE", "REMOTE_HASH", "APPROVAL_HASH", "IS_REMOTE") AS 
  SELECT 
	ID                     AS IIA_ID,
	IIA_CODE               AS IIA_CODE,
	REMOTE_IIA_ID          AS REMOTE_IIA_ID,
	REMOTE_IIA_CODE        AS REMOTE_IIA_CODE,
	START_DATE             AS IIA_START_DATE,
	END_DATE               AS IIA_END_DATE,
	MODIFY_DATE            AS IIA_MODIFY_DATE,
	APPROVAL_DATE          AS IIA_APPROVAL_DATE,
	REMOTE_COP_COND_HASH   AS REMOTE_HASH,
	APPROVAL_COP_COND_HASH AS APPROVAL_HASH,
	IS_REMOTE			   AS IS_REMOTE
FROM dbo.EWPCV_IIA
;
GO

CREATE VIEW dbo.INSTITUTION_IDENTIFIERS_VIEW ("SCHAC","PREVIOUS_SCHAC","PIC","ERASMUS","EUC","ERASMUS_CHARTER", "INSTITUTION_NAMES", "OTHER_IDS") AS 
  SELECT 
	SCHAC AS SCHAC,
	PREVIOUS_SCHAC AS PREVIOUS_SCHAC,
	PIC AS PIC,
	ERASMUS AS ERASMUS,
	EUC AS EUC,
	ERASMUS_CHARTER AS ERASMUS_CHARTER,
    INSTITUTION_NAMES AS INSTITUTION_NAMES,
    OTHER_IDS AS OTHER_IDS
FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS;
GO

ALTER VIEW dbo.LA_COMPONENTS_VIEW ("LA_ID", "LA_REVISION", "COMPONENT_TYPE", "TITLE", "DESCRIPTION", "CREDITS", "ACADEMIC_TERM", "ACADEMIC_TERM_NAMES", "ACADEMIC_YEAR", "STATUS", "REASON_CODE", "REASON_TEXT", "RECOGNITION_CONDITIONS", "LOS_CODE", "INSTITUTION_ID", "RECEIVING_INSTITUTION_NAMES", "OUNIT_CODE", "RECEIVING_OUNIT_NAMES", "LOS_NAMES", "LOS_DESCRIPTIONS", "LOS_URL", "ENGAGEMENT_HOURS", "LANGUAGE_OF_INSTRUCTION") AS 

  SELECT 
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	dbo.EXTRAE_CREDITS(C.id)                            AS CREDITS,
	dbo.EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	AY.START_YEAR + '|:|' + AY.END_YEAR             AS ACADEMIC_YEAR,
	C.STATUS                                        AS STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_CODE                                      AS LOS_CODE,
	LOS.INSTITUTION_ID                              AS INSTITUTION_ID,
	dbo.EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	dbo.EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	dbo.EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	dbo.EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION
FROM dbo.EWPCV_LEARNING_AGREEMENT LA
LEFT JOIN dbo.EWPCV_STUDIED_LA_COMPONENT SC 
    ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_RECOGNIZED_LA_COMPONENT RC 
    ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_VIRTUAL_LA_COMPONENT VC 
    ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_BLENDED_LA_COMPONENT BC 
    ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_DOCTORAL_LA_COMPONENT DC 
    ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_LA_COMPONENT C
    ON C.ID = SC.STUDIED_LA_COMPONENT_ID
    OR C.ID = RC.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = VC.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = BC.BLENDED_LA_COMPONENT_ID
    OR C.ID = DC.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN dbo.EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID    
LEFT JOIN dbo.EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID    
LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT O 
    ON LOS.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN dbo.EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID
	;
GO

ALTER VIEW dbo.LEARNING_AGREEMENT_VIEW ("MOBILITY_ID", "MOBILITY_REVISION", "LA_ID", "LA_REVISION", "LA_STATUS", "LA_CHANGES_ID", "COMMENT_REJECT", "STUDENT_SIGN", "STUDENT_SIGNATURE", "SENDER_SIGN", "SENDER_SIGNATURE", "RECEIVER_SIGN", "RECEIVER_SIGNATURE", "MODIFIED_DATE", "STUDENT_GLOBAL_ID", "STUDENT_NAME", "STUDENT_LAST_NAME", "STUDENT_BIRTH_DATE", "STUDENT_GENDER", "STUDENT_CITIZENSHIP", "STUDENT_CONTACT_NAMES", "STUDENT_CONTACT_DESCRIPTIONS", "STUDENT_CONTACT_URLS", "STUDENT_EMAILS", "STUDENT_PHONE", "STUDENT_ADDRESS_LINES", "STUDENT_ADDRESS", "STUDENT_POSTAL_CODE", "STUDENT_LOCALITY", "STUDENT_REGION", "STUDENT_COUNTRY") AS 
  SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	dbo.EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	dbo.EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	dbo.EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	M.MOBILITY_PARTICIPANT_ID                  AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM dbo.EWPCV_MOBILITY M
INNER JOIN dbo.EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN dbo.EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
LEFT JOIN dbo.EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN dbo.EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN dbo.EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN dbo.EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN dbo.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN dbo.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS;
GO

ALTER VIEW dbo.MOBILITY_VIEW ("MOBILITY_ID", "MOBILITY_REVISION", "PLANNED_ARRIVAL_DATE", "ACTUAL_ARRIVAL_DATE", "PLANNED_DEPARTURE_DATE", "ACTUAL_DEPARTURE_DATE", "EQF_LEVEL", "IIA_CODE", "COOPERATION_CONDITION_ID", "SUBJECT_AREA", "MOBILITY_STATUS", "MOBILITY_TYPE", "LANGUAGE_SKILL", "STUDENT_GLOBAL_ID", "STUDENT_NAME", "STUDENT_LAST_NAME", "STUDENT_BIRTH_DATE", "STUDENT_GENDER", "STUDENT_CITIZENSHIP", "STUDENT_CONTACT_NAMES", "STUDENT_CONTACT_DESCRIPTIONS", "STUDENT_CONTACT_URLS", "STUDENT_EMAILS", "STUDENT_PHONE", "STUDENT_ADDRESS_LINES", "STUDENT_ADDRESS", "STUDENT_POSTAL_CODE", "STUDENT_LOCALITY", "STUDENT_REGION", "STUDENT_COUNTRY", "RECEIVING_INSTITUTION", "RECEIVING_INSTITUTION_NAMES", "RECEIVING_OUNIT_CODE", "RECEIVING_OUNIT_NAMES", "SENDING_INSTITUTION", "SENDING_INSTITUTION_NAMES", "SENDING_OUNIT_CODE", "SENDING_OUNIT_NAMES", "SENDER_CONTACT_NAME", "SENDER_CONTACT_LAST_NAME", "SENDER_CONTACT_BIRTH_DATE", "SENDER_CONTACT_GENDER", "SENDER_CONTACT_CITIZENSHIP", "SENDER_CONT_CONT_NAMES", "SENDER_CONT_CONT_DESCS", "SENDER_CONT_CONT_URLS", "SENDER_CONTACT_EMAILS", "SENDER_CONTACT_PHONES", "SENDER_CONT_ADDR_LINES", "SENDER_CONTACT_ADDRESS", "SENDER_CONTACT_POSTAL_CODE", "SENDER_CONTACT_LOCALITY", "SENDER_CONTACT_REGION", "SENDER_CONTACT_COUNTRY", "ADMV_SEN_CONTACT_NAME", "ADMV_SEN_CONTACT_LAST_NAME", "ADMV_SEN_CONTACT_BIRTH_DATE", "ADMV_SEN_CONTACT_GENDER", "ADMV_SEN_CONTACT_CITIZENSHIP", "ADMV_SEN_CONT_CONT_NAMES", "ADMV_SEN_CONT_CONT_DESCS", "ADMV_SEN_CONT_CONT_URLS", "ADMV_SEN_CONTACT_EMAILS", "ADMV_SEN_CONTACT_PHONES", "ADMV_SEN_CONT_ADDR_LINES", "ADMV_SEN_CONTACT_ADDRESS", "ADMV_SEN_CONTACT_POSTAL_CODE", "ADMV_SEN_CONTACT_LOCALITY", "ADMV_SEN_CONTACT_REGION", "ADMV_SEN_CONTACT_COUNTRY", "RECEIVER_CONTACT_NAME", "RECEIVER_CONTACT_LAST_NAME", "RECEIVER_CONTACT_BIRTH_DATE", "RECEIVER_CONTACT_GENDER", "RECEIVER_CONTACT_CITIZENSHIP", "RECEIVER_CONT_CONT_NAMES", "RECEIVER_CONT_CONT_DESCS", "RECEIVER_CONT_CONT_URLS", "RECEIVER_CONTACT_EMAILS", "RECEIVER_CONTACT_PHONES", "RECEIVER_CONT_ADDR_LINES", "RECEIVER_CONTACT_ADDRESS", "RECEIVER_CONTACT_POSTAL_CODE", "RECEIVER_CONTACT_LOCALITY", "RECEIVER_CONTACT_REGION", "RECEIVER_CONTACT_COUNTRY", "ADMV_REC_CONTACT_NAME", "ADMV_REC_CONTACT_LAST_NAME", "ADMV_REC_CONTACT_BIRTH_DATE", "ADMV_REC_CONTACT_GENDER", "ADMV_REC_CONTACT_CITIZENSHIP", "ADMV_REC_CONT_CONT_NAMES", "ADMV_REC_CONT_CONT_DESCS", "ADMV_REC_CONT_CONT_URLS", "ADMV_REC_CONTACT_EMAILS", "ADMV_REC_CONTACT_PHONES", "ADMV_REC_CONT_ADDR_LINES", "ADMV_REC_CONTACT_ADDRESS", "ADMV_REC_CONTACT_POSTAL_CODE", "ADMV_REC_CONTACT_LOCALITY", "ADMV_REC_CONTACT_REGION", "ADMV_REC_CONTACT_COUNTRY") AS 
  SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL                                                                                                AS EQF_LEVEL,
	M.IIA_ID                                                                                                 AS IIA_CODE,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE + '|:|' + SA.ISCED_CLARIFICATION                                                             AS SUBJECT_AREA,
	M.STATUS                                                                                                   AS MOBILITY_STATUS,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                           AS MOBILITY_TYPE,
	dbo.EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.ID                                                                                                      AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY	
FROM dbo.EWPCV_MOBILITY M
INNER JOIN dbo.EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN dbo.EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN dbo.EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN dbo.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
INNER JOIN dbo.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
INNER JOIN dbo.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
INNER JOIN dbo.EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
INNER JOIN dbo.EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN dbo.EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN dbo.EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS

GO