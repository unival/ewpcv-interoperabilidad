/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
CREATE TABLE ewp.dbo.EWPCV_IIA_APPROVALS (
    ID BIGINT NOT NULL,
    LOCAL_IIA_ID varchar(255) NOT NULL,
    REMOTE_IIA_ID varchar(255) NOT NULL,
    IIA_API_VERSION varchar(255) NOT NULL,
    LOCAL_HASH varchar(255) NOT NULL,
    REMOTE_HASH varchar(255) NOT NULL,
    DATE_IIA_APPROVAL DATE NOT NULL,
    CONSTRAINT fk_iia_approval FOREIGN KEY (LOCAL_IIA_ID) REFERENCES EWPCV_IIA(ID),
	PRIMARY KEY (ID)
);

CREATE SEQUENCE SEC_EWPCV_IIA_APPROVALS start with 1 increment by 1;

ALTER TABLE ewp.dbo.EWPCV_COOPERATION_CONDITION ADD IIA_APPROVAL_ID BIGINT;

ALTER TABLE ewp.dbo.EWPCV_COOPERATION_CONDITION 
   ADD CONSTRAINT FK_CONDITIONS_APPROVAL_ID
   FOREIGN KEY (IIA_APPROVAL_ID) 
   REFERENCES ewp.dbo.EWPCV_IIA_APPROVALS;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v02.03.00', GETDATE(), '38_IIA_APPROVALS_v02.03.00');
