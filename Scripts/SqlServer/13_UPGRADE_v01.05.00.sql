
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
	--EWPCV_LEVEL
	create table dbo.EWPCV_LEVEL (
       ID varchar(255) not null,
	   VERSION integer,
       LEVEL_TYPE integer not null,
	   LEVEL_VALUE varchar(255) not null,
       primary key (ID)
    );

	--EWPCV_LEVELS
	create table dbo.EWPCV_LEVELS (
       LOI_ID varchar(255) not null,
       LEVEL_ID varchar(255) not null
    );

	alter table dbo.EWPCV_LEVELS
	add constraint FK_LEVEL1
	foreign key (LOI_ID)
	references dbo.EWPCV_LOI;

	alter table dbo.EWPCV_LEVELS
	add constraint FK_LEVEL2
	foreign key (LEVEL_ID)
	references dbo.EWPCV_LEVEL;

	--EWPCV_LEVEL_DESCRIPTION
	create table dbo.EWPCV_LEVEL_DESCRIPTION (
       LEVEL_DESCRIPTION_ID varchar(255) not null,
	   LEVEL_ID varchar(255) not null
    );

	alter table dbo.EWPCV_LEVEL_DESCRIPTION
	add constraint FK_LEVEL_DESC
	foreign key (LEVEL_ID)
	references dbo.EWPCV_LEVEL;

	alter table dbo.EWPCV_LEVEL_DESCRIPTION
	add constraint FK_LEVEL_DESC_LANG_IT
	foreign key (LEVEL_DESCRIPTION_ID)
	references dbo.EWPCV_LANGUAGE_ITEM;

	--EWPCV_GROUP_TYPE
	create table dbo.EWPCV_GROUP_TYPE (
       ID varchar(255) not null,
       TITLE varchar(255),
       primary key (ID)
    );

	--EWPCV_GROUP
	create table dbo.EWPCV_GROUP (
       ID varchar(255) not null,
	   GROUP_TYPE_ID varchar(255),
       TITLE varchar(255) not null,
	   SORTING_KEY varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_GROUP
	add constraint FK_GROUP_TYPE
	foreign key (GROUP_TYPE_ID)
	references dbo.EWPCV_GROUP_TYPE;

	--EWPCV_LOI_GROUPING
	create table dbo.EWPCV_LOI_GROUPING (
       LOI_ID varchar(255) not null,
       GROUP_ID varchar(255),
	   GROUP_TYPE_ID varchar(255)
    );

	alter table dbo.EWPCV_LOI_GROUPING
	add constraint FK_GROUPING_G
	foreign key (GROUP_ID)
	references dbo.EWPCV_GROUP;

	alter table dbo.EWPCV_LOI_GROUPING
	add constraint FK_GROUPING_GT
	foreign key (GROUP_TYPE_ID)
	references dbo.EWPCV_GROUP_TYPE;

	alter table dbo.EWPCV_LOI_GROUPING
	add constraint FK_GROUPING_LOI
	foreign key (LOI_ID)
	references dbo.EWPCV_LOI;

	--EWPCV_LOS
	ALTER TABLE dbo.EWPCV_LOS ADD EXTENSION VARBINARY(max);
	ALTER TABLE dbo.EWPCV_LOS ALTER COLUMN EQF_LEVEL SMALLINT NULL;
	

	--EWPCV_LA_COMPONENT
	ALTER TABLE dbo.EWPCV_LA_COMPONENT DROP CONSTRAINT FK_LA_COMPONENT_3;
	ALTER TABLE dbo.EWPCV_LA_COMPONENT DROP CONSTRAINT FK_LA_COMPONENT_2;
	

	--EWPCV_TOR
	create table dbo.EWPCV_TOR (
       ID varchar(255) not null,
	   VERSION integer,
	   GENERATED_DATE DATE,
	   EXTENSION VARBINARY(max),
       primary key (ID)
    );
	--EWPCV_LEARNING_AGREEMENT
	ALTER TABLE dbo.EWPCV_LEARNING_AGREEMENT ADD TOR_ID varchar(255);

	alter table dbo.EWPCV_LEARNING_AGREEMENT
	add constraint FK_LEA_TOR
	foreign key (TOR_ID)
	references dbo.EWPCV_TOR;

	--EWPCV_REPORT
	create table dbo.EWPCV_REPORT (
       ID varchar(255) not null,
	   VERSION integer,
	   ISSUE_DATE DATE,
	   TOR_ID varchar(255) not null,
       primary key (ID)
    );

   	alter table dbo.EWPCV_REPORT
	add constraint FK_REPOT_TOR
	foreign key (TOR_ID)
	references dbo.EWPCV_TOR;

	--EWPCV_LOI

	ALTER TABLE dbo.EWPCV_LOI ADD START_DATE DATE;
	ALTER TABLE dbo.EWPCV_LOI ADD END_DATE DATE;
	ALTER TABLE dbo.EWPCV_LOI ADD PERCENTAGE_LOWER decimal(10,2);
	ALTER TABLE dbo.EWPCV_LOI ADD PERCENTAGE_EQUAL decimal(10,2);
	ALTER TABLE dbo.EWPCV_LOI ADD PERCENTAGE_HIGHER decimal(10,2);
	ALTER TABLE dbo.EWPCV_LOI ADD RESULT_LABEL varchar(255);
	ALTER TABLE dbo.EWPCV_LOI ADD STATUS integer;
	ALTER TABLE dbo.EWPCV_LOI ADD DIPLOMA_ID varchar(255);
	ALTER TABLE dbo.EWPCV_LOI ADD REPORT_ID varchar(255);
	ALTER TABLE dbo.EWPCV_LOI ADD EXTENSION VARBINARY(max);

	alter table dbo.EWPCV_LOI
	add constraint FK_LOI_TOR
	foreign key (REPORT_ID)
	references dbo.EWPCV_REPORT;

	--EWPCV_DISTR_CATEGORIES
	DROP TABLE dbo.EWPCV_DISTR_CATEGORIES;

	--dbo.EWPCV_RESULT_DIST_CATEGORY
	alter table dbo.EWPCV_RESULT_DIST_CATEGORY ADD RESULT_DIST_ID varchar(255);

	alter table dbo.EWPCV_RESULT_DIST_CATEGORY
	add constraint FK_RESULT_DIST_CAT
	foreign key (RESULT_DIST_ID)
	references dbo.EWPCV_RESULT_DISTRIBUTION;

	--EWPCV_DIPLOMA
	create table dbo.EWPCV_DIPLOMA (
       ID varchar(255) not null,
	   VERSION integer,
	   DIPLOMA_VERSION integer,
	   ISSUE_DATE DATE,
	   INTRODUCTION VARBINARY(max),
	   SIGNATURE VARBINARY(max),
       primary key (ID)
    );

	alter table dbo.EWPCV_LOI
	add constraint FK_LOI_DIPL
	foreign key (DIPLOMA_ID)
	references dbo.EWPCV_DIPLOMA;

	--EWPCV_DIPLOMA_SECTION
	create table dbo.EWPCV_DIPLOMA_SECTION (
       ID varchar(255) not null,
	   VERSION integer,
	   TITLE varchar(255),
	   CONTENT VARBINARY(max),
	   SECTION_NUMBER integer,
	   DIPLOMA_ID varchar (255),
       primary key (ID)
    );

	alter table dbo.EWPCV_DIPLOMA_SECTION
	add constraint FK_RESULT_DIPLOMA
	foreign key (DIPLOMA_ID)
	references dbo.EWPCV_DIPLOMA;

	--EWPCV_DIPLOMA_SECTION_SECTION
	create table dbo.EWPCV_DIPLOMA_SECTION_SECTION (
       ID varchar(255) not null,
	   SECTION_ID varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_DIPLOMA_SECTION_SECTION
	add constraint FK_DIP_SECT_SECT
	foreign key (SECTION_ID)
	references dbo.EWPCV_DIPLOMA_SECTION;

	--EWPCV_ADDITIONAL_INFO
	create table dbo.EWPCV_ADDITIONAL_INFO (
       ID varchar(255) not null,
	   VERSION integer,
	   SECTION_ID varchar(255),
	   ADDITIONAL_INFO varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_ADDITIONAL_INFO
	add constraint FK_ADD_INF_SECTION
	foreign key (SECTION_ID)
	references dbo.EWPCV_DIPLOMA_SECTION;

	--EWPCV_ATTACHMENT
	create table dbo.EWPCV_ATTACHMENT (
       ID varchar(255) not null,
	   VERSION integer,
	   ATTACH_TYPE integer,
	   EXTENSION VARBINARY(max),
       primary key (ID)
    );

	--EWPCV_LOI_ATT
	create table dbo.EWPCV_LOI_ATT (
       ATTACHMENT_ID varchar(255) not null,
	   LOI_ID varchar(255)
    );

	alter table dbo.EWPCV_LOI_ATT
	add constraint FK_LOI_ATT1
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	alter table dbo.EWPCV_LOI_ATT
	add constraint FK_LOI_ATT2
	foreign key (LOI_ID)
	references dbo.EWPCV_LOI;

	--EWPCV_REPORT_ATT
	create table dbo.EWPCV_REPORT_ATT(
	   ATTACHMENT_ID varchar(255) not null,
	   REPORT_ID varchar(255)
	);

	alter table dbo.EWPCV_REPORT_ATT
	add constraint FK_REPORT_ATT1
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	alter table dbo.EWPCV_REPORT_ATT
	add constraint FK_REPORT_ATT2
	foreign key (REPORT_ID)
	references dbo.EWPCV_REPORT;
	
	--EWPCV_DIPLOMA_SEC_ATTACH
	create table dbo.EWPCV_DIPLOMA_SEC_ATTACH (
       ATTACHMENT_ID varchar(255) not null,
	   SECTION_ID varchar(255)
    );

	alter table dbo.EWPCV_DIPLOMA_SEC_ATTACH
	add constraint FK_ATTACH_SECTION
	foreign key (SECTION_ID)
	references dbo.EWPCV_DIPLOMA_SECTION;

	alter table dbo.EWPCV_DIPLOMA_SEC_ATTACH
	add constraint FK_ATTACH_ATTACH
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	--EWPCV_DIPLOMA_SEC_ATTACH
	create table dbo.EWPCV_TOR_ATTACHMENT (
       ATTACHMENT_ID varchar(255) not null,
	   TOR_ID varchar(255)
    );

	alter table dbo.EWPCV_TOR_ATTACHMENT
	add constraint FK_ATTACH_TOR
	foreign key (TOR_ID)
	references dbo.EWPCV_TOR;

	alter table dbo.EWPCV_TOR_ATTACHMENT
	add constraint FK_ATTACH_ATTACH2
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	--EWPCV_ATTACHMENT_CONTENT
	create table dbo.EWPCV_ATTACHMENT_CONTENT (
       ID varchar(255) not null,
	   ATTACHMENT_ID varchar(255),
	   LANG varchar(255),
	   CONTENT VARBINARY(max),
       primary key (ID)
    );

	alter table dbo.EWPCV_ATTACHMENT_CONTENT
	add constraint FK_ATTACH_CONTENT
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	--EWPCV_ATTACHMENT_TITLE
	create table dbo.EWPCV_ATTACHMENT_TITLE (
       ID varchar(255) not null,
	   ATTACHMENT_ID varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_ATTACHMENT_TITLE
	add constraint FK_ATTACH_TITLE
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	alter table dbo.EWPCV_ATTACHMENT_TITLE
	add constraint FK_ATTACH_TITLE_LANG
	foreign key (ID)
	references dbo.EWPCV_LANGUAGE_ITEM;

	--EWPCV_ATTACHMENT_DESCRIPTION
	create table dbo.EWPCV_ATTACHMENT_DESCRIPTION (
       ID varchar(255) not null,
	   ATTACHMENT_ID varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_ATTACHMENT_DESCRIPTION
	add constraint FK_ATTACH_DESC
	foreign key (ATTACHMENT_ID)
	references dbo.EWPCV_ATTACHMENT;

	alter table dbo.EWPCV_ATTACHMENT_DESCRIPTION
	add constraint FK_ATTACH_DESC_LANG
	foreign key (ID)
	references dbo.EWPCV_LANGUAGE_ITEM;

	alter table dbo.EWPCV_GROUP
	add constraint FK_GROUP_LANG
	foreign key (TITLE)
	references dbo.EWPCV_LANGUAGE_ITEM;

	alter table dbo.EWPCV_GROUP_TYPE
	add constraint FK_GROUP_LANG_TYPE
	foreign key (TITLE)
	references dbo.EWPCV_LANGUAGE_ITEM;

	--ISCED TABLE
	create table dbo.EWPCV_ISCED_TABLE (
       ID varchar(255) not null,
	   ISCED_CODE varchar(255),
	   TOR_ID varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_ISCED_TABLE
	add constraint FK_ISCED_TABLE_TOR
	foreign key (TOR_ID)
	references dbo.EWPCV_TOR;
	
	--GRADE FREQUENCY
	create table dbo.EWPCV_GRADE_FREQUENCY (
       ID varchar(255) not null,
	   LABEL varchar(255),
	   PERCENTAGE decimal(10, 2),
	   ISCED_TABLE_ID varchar(255),
       primary key (ID)
    );

	alter table dbo.EWPCV_GRADE_FREQUENCY
	add constraint FK_GRADE_FREQUENCY_TABLE
	foreign key (ISCED_TABLE_ID)
	references dbo.EWPCV_ISCED_TABLE;

	--PARA AUDITORIA DE EVENTOS
	ALTER TABLE dbo.EWPCV_EVENT ADD REQUEST_PARAMS VARCHAR(4000);
	ALTER TABLE dbo.EWPCV_EVENT ALTER COLUMN TRIGGERING_HEI varchar(255) NULL;
	ALTER TABLE dbo.EWPCV_EVENT ALTER COLUMN OWNER_HEI varchar(255) NULL;
	ALTER TABLE dbo.EWPCV_EVENT ALTER COLUMN CHANGED_ELEMENT_ID varchar(255) NULL;
	ALTER TABLE dbo.EWPCV_EVENT ADD MESSAGE VARBINARY(max);
	ALTER TABLE dbo.EWPCV_EVENT ADD X_REQUEST_ID VARCHAR(500) ;

	CREATE TABLE dbo.EWPCV_AUDIT_CONFIG
	(
	  NAME varchar(100),
	 VALUE varchar(1000)
	);

	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.echo.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.factsheet.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.iiaApproval.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.iia.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.iMobility.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.institutions.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.oMobility.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.oMobilityLas.audit', 'true');
	INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.organizationUnit.audit', 'true');
	
	CREATE TABLE dbo.EWPCV_COOPCOND_SUBAR
   (
    ID VARCHAR(255) NOT NULL ,
	COOPERATION_CONDITION_ID VARCHAR(255) NOT NULL ,
	ISCED_CODE VARCHAR(255),
	PRIMARY KEY (ID)
	);

	ALTER TABLE dbo.EWPCV_COOPCOND_SUBAR
	ADD CONSTRAINT FK_CC_COOP_CONDITION
	FOREIGN KEY (COOPERATION_CONDITION_ID)
	REFERENCES dbo.EWPCV_COOPERATION_CONDITION;

	ALTER TABLE dbo.EWPCV_DURATION ADD NUMBERDURATION2 decimal(5, 2) ;
	GO
	UPDATE dbo.EWPCV_DURATION SET NUMBERDURATION2 = NUMBERDURATION;
	UPDATE dbo.EWPCV_DURATION SET NUMBERDURATION = null;
	ALTER TABLE dbo.EWPCV_DURATION ALTER COLUMN NUMBERDURATION decimal(5, 2);
	UPDATE dbo.EWPCV_DURATION SET NUMBERDURATION = NUMBERDURATION2;
	ALTER TABLE dbo.EWPCV_DURATION DROP COLUMN NUMBERDURATION2;

GO
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/
	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_EMAILS'
	) DROP FUNCTION dbo.EXTRAE_EMAILS;
	GO
	CREATE FUNCTION dbo.EXTRAE_EMAILS(@P_CONTACT_DETAILS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	select @return_value = stuff( (select '|;|' + EMAIL
				   FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL 
				   WHERE CONTACT_DETAILS_ID = @P_CONTACT_DETAILS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_TELEFONO'
	) DROP FUNCTION dbo.EXTRAE_TELEFONO;
	GO
	CREATE FUNCTION dbo.EXTRAE_TELEFONO(@P_CONTACT_DETAILS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + P.E164 + '|:|' + P.EXTENSION_NUMBER + '|:|' + P.OTHER_FORMAT
				   FROM dbo.EWPCV_CONTACT_DETAILS CD,
						dbo.EWPCV_PHONE_NUMBER P
					WHERE P.ID = CD.PHONE_NUMBER
						AND CD.ID = @P_CONTACT_DETAILS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_CONTACTO'
	) DROP FUNCTION dbo.EXTRAE_URLS_CONTACTO;
	GO
	CREATE FUNCTION dbo.EXTRAE_URLS_CONTACTO(@P_CONTACT_DETAILS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
				   FROM dbo.EWPCV_CONTACT_DETAILS CD,
						dbo.EWPCV_CONTACT_URL U,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE CD.ID = U.CONTACT_DETAILS_ID
						AND U.URL_ID = LAIT.ID
						AND CD.ID = @P_CONTACT_DETAILS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_EQF_LEVEL'
	) DROP FUNCTION dbo.EXTRAE_EQF_LEVEL;
	GO
	CREATE FUNCTION dbo.EXTRAE_EQF_LEVEL(@P_COOP_CON_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + CONVERT(VARCHAR(5),EQF_LEVEL)
				   FROM dbo.EWPCV_COOPCOND_EQFLVL
				   WHERE COOPERATION_CONDITION_ID = @P_COOP_CON_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_CREDITS'
	) DROP FUNCTION dbo.EXTRAE_CREDITS;
	GO
	CREATE FUNCTION dbo.EXTRAE_CREDITS(@P_COMPONENT_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + COALESCE(CR.CREDIT_LEVEL,'') + '|:|' + CR.SCHEME + '|:|' + CAST(CR.CREDIT_VALUE AS VARCHAR)
				FROM dbo.EWPCV_CREDIT CR,
					dbo.EWPCV_COMPONENT_CREDITS CCR
				WHERE CR.ID = CCR.CREDITS_ID
					AND CCR.COMPONENT_ID = @P_COMPONENT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);

		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ACADEMIC_TERM'
	) DROP FUNCTION dbo.EXTRAE_ACADEMIC_TERM;
	GO
	CREATE FUNCTION dbo.EXTRAE_ACADEMIC_TERM(@P_AT_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + CONVERT(varchar, ATR.START_DATE, 102)  + '|:|' + CONVERT(varchar, ATR.END_DATE, 102) 
					+ '|:|' + ATR.INSTITUTION_ID + '|:|' + o.ORGANIZATION_UNIT_CODE 
					+ '|:|' + CONVERT(varchar, ATR.TERM_NUMBER) + '|:|' + CONVERT(varchar, ATR.TOTAL_TERMS)
				FROM dbo.EWPCV_ACADEMIC_TERM ATR
				LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT O ON ATR.ORGANIZATION_UNIT_ID = O.ID
				WHERE ATR.ID = @P_AT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO
	
	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ACADEMIC_YEAR'
	) DROP FUNCTION dbo.EXTRAE_ACADEMIC_YEAR;
	GO
	CREATE FUNCTION dbo.EXTRAE_ACADEMIC_YEAR(@P_AT_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , AY.START_YEAR  , '-' , AY.END_YEAR)
				FROM dbo.EWPCV_ACADEMIC_TERM ATR
					LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR AY ON ATR.ACADEMIC_YEAR_ID = AY.ID
				WHERE ATR.ID = @P_AT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_ACADEMIC_TERM'
	) DROP FUNCTION dbo.EXTRAE_NOMBRES_ACADEMIC_TERM;
	GO
	CREATE FUNCTION dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(@P_AT_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value =stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_ACADEMIC_TERM_NAME N,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE N.DISP_NAME_ID = LAIT.ID
						AND N.ACADEMIC_TERM_ID = @P_AT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_OUNIT'
	) DROP FUNCTION dbo.EXTRAE_NOMBRES_OUNIT;
	GO
	CREATE FUNCTION dbo.EXTRAE_NOMBRES_OUNIT(@P_OUNIT_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value =stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			FROM dbo.EWPCV_ORGANIZATION_UNIT O,
				dbo.EWPCV_ORGANIZATION_UNIT_NAME N,
				dbo.EWPCV_LANGUAGE_ITEM LAIT
			WHERE O.ID = N.ORGANIZATION_UNIT_ID
				AND N.NAME_ID = LAIT.ID
				AND O.ID = @P_OUNIT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_LOS'
	) DROP FUNCTION dbo.EXTRAE_NOMBRES_LOS;
	GO
	CREATE FUNCTION dbo.EXTRAE_NOMBRES_LOS(@P_LOS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_LOS_NAME N,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE N.NAME_ID = LAIT.ID
						AND N.LOS_ID = @P_LOS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_DESCRIPTIONS_LOS'
	) DROP FUNCTION dbo.EXTRAE_DESCRIPTIONS_LOS;
	GO
	CREATE FUNCTION dbo.EXTRAE_DESCRIPTIONS_LOS(@P_LOS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_LOS_DESCRIPTION D,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE D.DESCRIPTION_ID = LAIT.ID
						AND D.LOS_ID = @P_LOS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_LOS'
	) DROP FUNCTION dbo.EXTRAE_URLS_LOS;
	GO
	CREATE FUNCTION dbo.EXTRAE_URLS_LOS(@P_LOS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_LOS_URLS U,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE U.URL_ID = LAIT.ID
						AND U.LOS_ID = @P_LOS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO
	 
	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_S_AREA_L_SKILL'
	) DROP FUNCTION dbo.EXTRAE_S_AREA_L_SKILL;
	GO
	CREATE FUNCTION dbo.EXTRAE_S_AREA_L_SKILL(@P_COOP_CON_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + S.ISCED_CODE + '|:|' + COALESCE(S.ISCED_CLARIFICATION,'') + '|:|' + L."LANGUAGE" + '|:|' + L.CEFR_LEVEL
					FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
					LEFT JOIN dbo.EWPCV_LANGUAGE_SKILL L	
						ON CSL.LANGUAGE_SKILL_ID = L.ID
					LEFT JOIN dbo.EWPCV_SUBJECT_AREA S	
						ON CSL.ISCED_CODE = S.ID	
					WHERE CSL.COOPERATION_CONDITION_ID = @P_COOP_CON_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_S_AREA'
	) DROP FUNCTION dbo.EXTRAE_S_AREA;
	GO
	CREATE FUNCTION dbo.EXTRAE_S_AREA(@P_COOP_CON_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + S.ISCED_CODE + '|:|' + COALESCE(S.ISCED_CLARIFICATION,'')
					FROM dbo.EWPCV_COOPCOND_SUBAR CS
					LEFT JOIN dbo.EWPCV_SUBJECT_AREA S	
						ON CS.ISCED_CODE = S.ID	
					WHERE CS.COOPERATION_CONDITION_ID = @P_COOP_CON_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_INSTITUCION'
	) DROP FUNCTION dbo.EXTRAE_NOMBRES_INSTITUCION;
	GO
	CREATE FUNCTION dbo.EXTRAE_NOMBRES_INSTITUCION(@P_INSTITUTION_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_INSTITUTION I,
						dbo.EWPCV_INSTITUTION_NAME N,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE I.ID = N.INSTITUTION_ID
						AND N.NAME_ID = LAIT.ID
						AND I.INSTITUTION_ID = @P_INSTITUTION_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_CONTACTO'
	) DROP FUNCTION dbo.EXTRAE_NOMBRES_CONTACTO;
	GO
	CREATE FUNCTION dbo.EXTRAE_NOMBRES_CONTACTO(@P_CONTACTO_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_CONTACT C,
						dbo.EWPCV_CONTACT_NAME CN,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE C.ID = CN.CONTACT_ID
						AND CN.NAME_ID = LAIT.ID
						AND C.ID = @P_CONTACTO_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_DESCRIPCIONES_CONTACTO'
	) DROP FUNCTION dbo.EXTRAE_DESCRIPCIONES_CONTACTO;
	GO
	CREATE FUNCTION dbo.EXTRAE_DESCRIPCIONES_CONTACTO(@P_CONTACTO_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
					FROM dbo.EWPCV_CONTACT C,
						dbo.EWPCV_CONTACT_DESCRIPTION CD,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE C.ID = CD.CONTACT_ID
						AND CD.DESCRIPTION_ID = LAIT.ID
						AND C.ID = @P_CONTACTO_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDRESS_LINES'
	) DROP FUNCTION dbo.EXTRAE_ADDRESS_LINES;
	GO
	CREATE FUNCTION dbo.EXTRAE_ADDRESS_LINES(@P_FLEXIBLE_ADDRESS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + COALESCE(ADDRESS_LINE,'')
					FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE 
					WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO 

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FLEXAD_DELIV_POINT'
	) DROP FUNCTION dbo.EXTRAE_FLEXAD_DELIV_POINT;
	GO 	
	CREATE FUNCTION dbo.EXTRAE_FLEXAD_DELIV_POINT(@P_FLEXIBLE_ADDRESS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + COALESCE(Delivery_point_code,'')
					FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD
			WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDRESS'
	) DROP FUNCTION dbo.EXTRAE_ADDRESS;
	GO 	
	CREATE FUNCTION dbo.EXTRAE_ADDRESS(@P_FLEXIBLE_ADDRESS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + COALESCE(F.BUILDING_NUMBER,'') + '|:|' + COALESCE(F.BUILDING_NAME,'') + '|:|' + COALESCE(F.STREET_NAME ,'')
						+ '|:|' + COALESCE(F.UNIT,'') + '|:|' + COALESCE(F."FLOOR",'') + '|:|' + COALESCE(F.POST_OFFICE_BOX,'')
						+ '|:|' + (
						dbo.EXTRAE_FLEXAD_DELIV_POINT(F.ID)
						)
					FROM dbo.EWPCV_FLEXIBLE_ADDRESS F
					WHERE F.ID = @P_FLEXIBLE_ADDRESS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NIVELES_IDIOMAS'
	) DROP FUNCTION dbo.EXTRAE_NIVELES_IDIOMAS;
	GO 	
	CREATE FUNCTION  dbo.EXTRAE_NIVELES_IDIOMAS(@P_MOBILITY_ID varchar, @P_REVISION integer) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + S."LANGUAGE" + '|:|' + S.CEFR_LEVEL
					FROM dbo.EWPCV_MOBILITY_LANG_SKILL MLS,
						dbo.EWPCV_LANGUAGE_SKILL S
					WHERE MLS.LANGUAGE_SKILL_ID = S.ID
						AND MLS.MOBILITY_ID = @P_MOBILITY_ID
						AND MLS.MOBILITY_REVISION = @P_REVISION
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO
	 
	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FIRMAS'
	) DROP FUNCTION dbo.EXTRAE_FIRMAS;
	GO 	
	CREATE FUNCTION dbo.EXTRAE_FIRMAS (@P_FIRMA_ID varchar) RETURNS varchar(max)  as
	BEGIN
		DECLARE @v_lista VARCHAR(max)
		SELECT @v_lista = stuff( (select '|;|' + COALESCE(SIGNER_NAME,'') + '|' + COALESCE(SIGNER_POSITION,'') 
					+ '|' + COALESCE(SIGNER_EMAIL,'') + '|' + COALESCE(CAST("TIMESTAMP" AS VARCHAR(max)),'') + '|' + COALESCE(SIGNER_APP,'')
					FROM dbo.EWPCV_SIGNATURE
					WHERE ID = @P_FIRMA_ID
				   for xml path ('')
				  ), 1, 3, ''
				);

		RETURN @v_lista

	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FAX'
	) DROP FUNCTION dbo.EXTRAE_FAX;
	GO 	
	CREATE FUNCTION dbo.EXTRAE_FAX(@P_CONTACT_DETAILS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	select @return_value = stuff( (select '|;|' + F.E164 + '|:|' + F.EXTENSION_NUMBER + '|:|' + F.OTHER_FORMAT
					FROM dbo.EWPCV_CONTACT_DETAILS CD,
						dbo.EWPCV_PHONE_NUMBER F
					WHERE F.ID = CD.FAX_NUMBER
						AND CD.ID = @P_CONTACT_DETAILS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_FACTSHEET'
	) DROP FUNCTION dbo.EXTRAE_URLS_FACTSHEET;
	GO
	CREATE FUNCTION dbo.EXTRAE_URLS_FACTSHEET(@P_FACTSHEET_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
				   FROM dbo.EWPCV_FACT_SHEET_URL FSU,
						dbo.EWPCV_LANGUAGE_ITEM LAIT
					WHERE FSU.URL_ID = LAIT.ID
						AND FSU.FACT_SHEET_ID = @P_FACTSHEET_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO


	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_PHOTO_URL_LIST'
	) DROP FUNCTION dbo.EXTRAE_PHOTO_URL_LIST;
	GO
	CREATE FUNCTION dbo.EXTRAE_PHOTO_URL_LIST(@CONTACT_DETAILS_ID varchar) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select '|;|' + PU.URL + '|:|' + PU.PHOTO_SIZE + '|:|' + CONVERT(VARCHAR(10), PU.PHOTO_DATE, 20) + '|:|' + convert(varchar(1),PU.PHOTO_PUBLIC)
				   FROM dbo.EWPCV_PHOTO_URL PU
					WHERE PU.CONTACT_DETAILS_ID = @CONTACT_DETAILS_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO
	
	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_LANGUAGE_ITEM'
	) DROP FUNCTION dbo.EXTRAE_LANGUAGE_ITEM;
	GO
	CREATE FUNCTION dbo.EXTRAE_LANGUAGE_ITEM(@P_LANG_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , text , '|:|' , lang , '|:|' )
				   FROM dbo.EWPCV_LANGUAGE_ITEM PU
					WHERE ID = @P_LANG_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ISCED_GRADE'
	) DROP FUNCTION dbo.EXTRAE_ISCED_GRADE;
	GO
	CREATE FUNCTION dbo.EXTRAE_ISCED_GRADE(@P_TOR_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;;|' , i.isced_code , '|::|',  
					   stuff( (select concat('|;|', g.label, '|:|', g.percentage ) 
						FROM dbo.EWPCV_GRADE_FREQUENCY g WHERE i.ID = g.isced_table_id
					  for xml path ('')
				  ), 1, 3, ''
				  ))
					FROM dbo.EWPCV_ISCED_TABLE i 
					WHERE i.TOR_ID = @P_TOR_ID
				   for xml path ('')
				  ), 1, 4, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_LEVELS'
	) DROP FUNCTION dbo.EXTRAE_LEVELS;
	GO
	CREATE FUNCTION dbo.EXTRAE_LEVELS(@P_LOI_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;;|' , lvl.level_type , '|::|' , lvl.level_value , '|::|', 
					   stuff( (select concat('|;|', li.text, '|:|', li.lang) 
						FROM dbo.EWPCV_LEVEL_DESCRIPTION ld INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON ld.level_description_id = li.id 
					WHERE ld.LEVEL_ID = lvl.ID
					  for xml path ('')
				  ), 1, 3, ''
				  ))
				   FROM dbo.EWPCV_LEVEL lvl
					INNER JOIN dbo.EWPCV_LEVELS lvls ON lvl.id = lvls.level_id
					 WHERE lvls.LOI_ID = @P_LOI_ID
				   for xml path ('')
				  ), 1, 4, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_RES_DIST'
	) DROP FUNCTION dbo.EXTRAE_RES_DIST;
	GO
	CREATE FUNCTION dbo.EXTRAE_RES_DIST(@P_RESULT_DIST_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , rdc.distrubtion_count , '|:|' , rdc.label)
				   FROM dbo.EWPCV_RESULT_DIST_CATEGORY rdc WHERE rdc.RESULT_DIST_ID = @P_RESULT_DIST_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GRAD_SCHEME_DESC'
	) DROP FUNCTION dbo.EXTRAE_GRAD_SCHEME_DESC;
	GO
	CREATE FUNCTION dbo.EXTRAE_GRAD_SCHEME_DESC(@P_GRADING_SCHEME_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , lid.text , '|:|' , lid.lang , '|:|' )
				   FROM dbo.EWPCV_GRADING_SCHEME_DESC gsd
					LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM lid 
					ON gsd.description_id = lid.id 
					WHERE gsd.GRADING_SCHEME_ID = @P_GRADING_SCHEME_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GRAD_SCHEME_LABEL'
	) DROP FUNCTION dbo.EXTRAE_GRAD_SCHEME_LABEL;
	GO
	CREATE FUNCTION dbo.EXTRAE_GRAD_SCHEME_LABEL(@P_GRADING_SCHEME_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , lig.text , '|:|' , lig.lang , '|:|' )
				   FROM dbo.EWPCV_GRADING_SCHEME_LABEL gsl
					LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM lig 
					ON gsl.label_id = lig.id 
					WHERE gsl.GRADING_SCHEME_ID = @P_GRADING_SCHEME_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GROUP_TYPE'
	) DROP FUNCTION dbo.EXTRAE_GROUP_TYPE;
	GO
	CREATE FUNCTION dbo.EXTRAE_GROUP_TYPE(@P_LOI_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , li.text , '|:|' , li.lang , '|:|' )
				   FROM dbo.EWPCV_LOI_GROUPING lg
				   INNER JOIN dbo.EWPCV_GROUP_TYPE gt ON lg.GROUP_TYPE_ID = gt.ID
				   INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON gt.title = li.id
				   WHERE lg.LOI_ID = @P_LOI_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GROUP'
	) DROP FUNCTION dbo.EXTRAE_GROUP;
	GO
	CREATE FUNCTION dbo.EXTRAE_GROUP(@P_LOI_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , li.text , '|:|' , li.lang , '|:|' )
			   FROM dbo.EWPCV_LOI_GROUPING lg
			   INNER JOIN dbo.EWPCV_GROUP g ON lg.GROUP_ID = g.ID
			   INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON g.title = li.id
			   WHERE lg.LOI_ID = @P_LOI_ID
			   for xml path ('')
			  ), 1, 3, ''
			);
	RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ATTACH_TITLE'
	) DROP FUNCTION dbo.EXTRAE_ATTACH_TITLE;
	GO
	CREATE FUNCTION dbo.EXTRAE_ATTACH_TITLE(@P_AT_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT.LANG , '|:|' )
				   FROM dbo.EWPCV_ATTACHMENT_TITLE ATTCH_TIT INNER JOIN
				   dbo.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_TIT.ID = LAIT.ID
				   WHERE ATTCH_TIT.ATTACHMENT_ID=@P_AT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ATTACH_DESC'
	) DROP FUNCTION dbo.EXTRAE_ATTACH_DESC;
	GO
	CREATE FUNCTION dbo.EXTRAE_ATTACH_DESC(@P_AT_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT.LANG , '|:|' )
				   FROM dbo.EWPCV_ATTACHMENT_DESCRIPTION ATTCH_DESC INNER JOIN
				   dbo.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_DESC.ID = LAIT.ID
				   WHERE ATTCH_DESC.ATTACHMENT_ID=@P_AT_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO

	IF EXISTS (
	Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDIT_INFO'
	) DROP FUNCTION dbo.EXTRAE_ADDIT_INFO;
	GO
	CREATE FUNCTION dbo.EXTRAE_ADDIT_INFO(@P_SECTION_ID varchar(255)) RETURNS varchar(max)
	BEGIN
	DECLARE @return_value varchar(max)
	SELECT @return_value = stuff( (select concat('|;|' , add_inf.ADDITIONAL_INFO )
				   FROM dbo.EWPCV_ADDITIONAL_INFO add_inf
				   WHERE add_inf.SECTION_ID=@P_SECTION_ID
				   for xml path ('')
				  ), 1, 3, ''
				);
		RETURN @return_value
	END
	;
	GO


/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'EVENT_VIEW'
) DROP VIEW dbo.EVENT_VIEW;
GO 
CREATE VIEW dbo.EVENT_VIEW AS 
  SELECT 
	ID					AS ID,
	EVENT_DATE			AS EVENT_DATE,
	TRIGGERING_HEI		AS TRIGGERING_HEI,
	OWNER_HEI			AS OWNER_HEI,
	EVENT_TYPE			AS EVENT_TYPE,
	CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
	ELEMENT_TYPE		AS ELEMENT_TYPE,
	OBSERVATIONS		AS OBSERVATIONS,
	STATUS				AS STATUS,
	REQUEST_PARAMS		AS REQUEST_PARAMS,
	MESSAGE				AS MESSAGE,
	X_REQUEST_ID		AS X_REQUEST_ID
FROM dbo.EWPCV_EVENT
;
GO

/*
	Vista con los datos de las asignaturas
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LOS_VIEW'
) DROP VIEW dbo.LOS_VIEW;
GO
CREATE VIEW dbo.LOS_VIEW AS
SELECT
ID 												AS LOS_ID,
LOS_CODE 										AS LOS_CODE,
EQF_LEVEL 										AS LOS_EQF_LEVEL,
INSTITUTION_ID 									AS LOS_INSTITUTION,
ISCEDF 											AS LOS_ISCEDF,
dbo.EXTRAE_NOMBRES_OUNIT(ORGANIZATION_UNIT_ID) 	AS LOS_OUNIT,
SUBJECT_AREA 									AS LOS_SUBJECT_AREA,
TOP_LEVEL_PARENT 								AS TOP_LEVLE_PARENT,
"TYPE" 											AS LOS_TYPE,
dbo.EXTRAE_NOMBRES_LOS(ID) 						AS LOS_NAMES,
dbo.EXTRAE_DESCRIPTIONS_LOS(ID) 				AS LOS_DESCRIPTIONS,
dbo.EXTRAE_URLS_LOS(ID) 						AS LOS_URLS
FROM EWPCV_LOS;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_COMPONENTS_VIEW'
) DROP VIEW dbo.LA_COMPONENTS_VIEW;
GO 
/*
	Obtiene la informacion de los componentes asociados a los learning agreements.
*/
CREATE VIEW dbo.LA_COMPONENTS_VIEW AS WITH COMPONENTS AS (
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, RC.RECOGNIZED_LA_COMPONENT_ID as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_RECOGNIZED_LA_COMPONENT RC ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, VC.VIRTUAL_LA_COMPONENT_ID as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT VC ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, BC.BLENDED_LA_COMPONENT_ID as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_BLENDED_LA_COMPONENT BC ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, DC.DOCTORAL_LA_COMPONENTS_ID as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT DC ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
)
SELECT
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	dbo.EXTRAE_CREDITS(C.id)                            AS CREDITS,
	dbo.EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	concat(AY.START_YEAR , '|:|' , AY.END_YEAR)           AS ACADEMIC_YEAR,
	C.STATUS                                        AS COMPONENT_STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_ID                                        AS LOS_ID,
	C.LOS_CODE                                      AS LOS_CODE,
	C.LOI_ID                                        AS LOI_ID,
	ATR.INSTITUTION_ID                              AS INSTITUTION_ID,
	dbo.EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	dbo.EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	dbo.EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	dbo.EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
    dbo.EXTRAE_LEVELS(LOI.ID)                           AS LOI_LEVELS,
	dbo.EXTRAE_GRAD_SCHEME_DESC(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_DESC,
	dbo.EXTRAE_GRAD_SCHEME_LABEL(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_LABEL,
	dbo.EXTRAE_RES_DIST(LOI.RESULT_DISTRIBUTION)        AS RESULT_DISTR,
	dbo.EXTRAE_GROUP_TYPE(LOI.ID)                       AS LOI_GROUP_TYPE,
	dbo.EXTRAE_GROUP(LOI.ID)                            AS LOI_GROUP,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION,
    LOI.START_DATE                                  AS START_DATE,
    LOI.END_DATE                                    AS END_DATE,
    LOI.PERCENTAGE_LOWER                            AS PERCENTAGE_LOWER,
    LOI.PERCENTAGE_EQUAL                            AS PERCENTAGE_EQUAL,
    LOI.PERCENTAGE_HIGHER                           AS PERCENTAGE_HIGHER,
    LOI.RESULT_LABEL                                AS RESULT_LABEL,
    LOI.STATUS                                      AS LOI_STATUS,
    LOI.EXTENSION                                   AS LOI_EXTENSION
FROM COMPONENTS LA
INNER JOIN EWPCV_LA_COMPONENT C
    ON C.ID = LA.STUDIED_LA_COMPONENT_ID
    OR C.ID = LA.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = LA.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = LA.BLENDED_LA_COMPONENT_ID
    OR C.ID = LA.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID
LEFT JOIN EWPCV_ORGANIZATION_UNIT O
    ON ATR.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'MOBILITY_VIEW'
) DROP VIEW dbo.MOBILITY_VIEW;
GO 
CREATE VIEW dbo.MOBILITY_VIEW AS 
 SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL_DEPARTURE                                                                                      AS EQF_LEVEL_DEPARTURE,
	M.EQF_LEVEL_NOMINATION                                                                                     AS EQF_LEVEL_NOMINATION,
	M.IIA_ID                                                                                                   AS IIA_ID,
	M.RECEIVING_IIA_ID                                                                                         AS RECEIVING_IIA_ID,
	SA.ISCED_CODE + '|:|' + SA.ISCED_CLARIFICATION                                                           AS SUBJECT_AREA,
	M.STATUS_OUTGOING                                                                                          AS MOBILITY_STATUS_OUTGOING,
	M.STATUS_INCOMING                                                                                          AS MOBILITY_STATUS_INCOMING,
	M.MOBILITY_COMMENT                                                                                         AS MOBILITY_COMMENT,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	dbo.EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.GLOBAL_ID                                                                                               AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	dbo.EXTRAE_PHOTO_URL_LIST(STC.CONTACT_DETAILS_ID)                                                              AS STUDENT_PHOTO_URLS,
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SC.CONTACT_DETAILS_ID)                                                                    	   AS SENDER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SCD.MAILING_ADDRESS)                                                                  AS SENDER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.MAILING_ADDRESS)                                                                        AS SENDER_CONTACT_MAILING_ADDR,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_SEN_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SACD.MAILING_ADDRESS)                                                                 AS ADMV_SEN_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.MAILING_ADDRESS)                                                                       AS ADMV_SEN_CONTACT_MAILING_ADDR,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RC.CONTACT_DETAILS_ID)                                                                    	   AS RECEIVER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RCD.MAILING_ADDRESS)                                                                  AS RECEIVER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.MAILING_ADDRESS)                                                                        AS RECEIVER_CONTACT_MAILING_ADDR,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_REC_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RACD.MAILING_ADDRESS)                                                                 AS ADMV_REC_CONTACT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.MAILING_ADDRESS)                                                                       AS ADMV_REC_CONTACT_MAILING_ADDR,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY,
	dbo.EXTRAE_ACADEMIC_TERM(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_TERM,
	dbo.EXTRAE_ACADEMIC_YEAR(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_YEAR
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
LEFT JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;
GO


IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LEARNING_AGREEMENT_VIEW'
) DROP VIEW dbo.LEARNING_AGREEMENT_VIEW;
GO 
CREATE view dbo.LEARNING_AGREEMENT_VIEW AS SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	dbo.EXTRAE_ISCED_GRADE(TOR.ID)                 AS GRADE_CONVERSION_TABLES,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	dbo.EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	dbo.EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	dbo.EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	MP.GLOBAL_ID                  			   AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
INNER JOIN EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
LEFT JOIN EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_TOR TOR
    ON LA.TOR_ID = TOR.ID;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_CONDITIONS_VIEW'
) DROP VIEW dbo.IIA_COOP_CONDITIONS_VIEW;
GO 
CREATE VIEW dbo.IIA_COOP_CONDITIONS_VIEW AS 
  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                            AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                       AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                       AS SUBJECT_AREAS,
	dbo.EXTRAE_S_AREA(CC.ID) 																			   AS SUBJECT_AREAS_LIST,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                      AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)    AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                      AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	RPSC.CONTACT_ROLE                                                                       			   AS RECEIVER_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                   AS RECEIVER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                           AS RECEIVER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                     AS RECEIVER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(RPSCD.ID)                                                                             AS RECEIVER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                           AS RECEIVER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(RPSC.CONTACT_DETAILS_ID)                                                           	   AS RECEIVER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                         AS RECEIVER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                               AS RECEIVER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.MAILING_ADDRESS)                                                        AS RECEIVER_SIGNER_M_ADD_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.MAILING_ADDRESS)                                                              AS RECEIVER_SIGNER_MAILING_ADDR,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                      AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)    AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                      AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	SPSC.CONTACT_ROLE                                                                       			   AS SENDER_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                   AS SENDER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                           AS SENDER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                     AS SENDER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSCD.ID)                                                                            AS SENDER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                           AS SENDER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(SPSC.CONTACT_DETAILS_ID)                                                          	   AS SENDER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                         AS SENDER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                               AS SENDER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.MAILING_ADDRESS)                                                        AS SENDER_SIGNER_M_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.MAILING_ADDRESS)                                                              AS SENDER_SIGNER_MAILING_ADDR,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN dbo.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN dbo.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'GRADING_SCHEME_VIEW'
) DROP VIEW dbo.GRADING_SCHEME_VIEW;
GO 
CREATE VIEW dbo.GRADING_SCHEME_VIEW AS SELECT  
    g.id                                AS GRADING_SCHEME_ID,
    dbo.EXTRAE_GRAD_SCHEME_DESC(g.id)       AS GRADING_SCHEME_DESC,
    dbo.EXTRAE_GRAD_SCHEME_LABEL(g.id)      AS GRADING_SCHEME_LABELS
FROM EWPCV_GRADING_SCHEME g;
GO

/*
	Vista con los datos de agrupaciones GROUP_TYPES con sus GROUP asociados, GROUP_TYPES sin groups asociados y GROUPS sin asociar a GROUP TYPES si los hubiera
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'GROUPING_VIEW'
) DROP VIEW dbo.GROUPING_VIEW;
GO 
CREATE VIEW dbo.GROUPING_VIEW AS SELECT 
    gt.id                           AS GROUP_TYPE_ID,
    dbo.EXTRAE_LANGUAGE_ITEM(gt.title)    AS GROUP_TYPE_NAME,
    g.id                            AS GROUP_ID,
    dbo.EXTRAE_LANGUAGE_ITEM(g.title)         AS GROUP_NAME,
    g.sorting_key                   AS SORTING_KEY
FROM EWPCV_GROUP_TYPE gt 
FULL OUTER JOIN EWPCV_GROUP g ON gt.ID = g.group_type_id;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'TOR_ATTACHMENT_VIEW'
) DROP VIEW dbo.TOR_ATTACHMENT_VIEW;
GO 
CREATE view dbo.TOR_ATTACHMENT_VIEW AS WITH IDS AS (
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, null AS REPORT_ID, null AS LOI_ID, tor_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_TOR_ATTACHMENT tor_a ON tor_a.TOR_ID = tor.ID
    UNION 
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, re.ID AS REPORT_ID, null AS LOI_ID, report_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_REPORT re ON re.tor_id = tor.id INNER JOIN EWPCV_REPORT_ATT report_a ON report_a.REPORT_ID = re.ID
    UNION
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, re.ID AS REPORT_ID, loi.ID AS LOI_ID, loi_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_REPORT re ON re.tor_id = tor.id INNER JOIN EWPCV_LOI loi ON loi.report_id = re.id INNER JOIN EWPCV_LOI_ATT loi_a ON loi_a.LOI_ID = loi.ID
)SELECT 
	ids.LA_ID								        AS LA_ID, 
	ids.TOR_ID							            AS TOR_ID,
	ids.REPORT_ID								    AS REPORT_ID,
	ids.LOI_ID								        AS LOI_ID,
	att.ATTACH_TYPE					                AS ATTACH_TYPE,
	att.EXTENSION						            AS ATTACH_EXTENSION,
	dbo.EXTRAE_ATTACH_TITLE(att.id)		                AS ATTACHMENT_TITLES,
	dbo.EXTRAE_ATTACH_DESC(att.id)		                AS ATTACHMENT_DESC,
    att_cont.LANG                                   AS ATTACHMENT_LANG,
    att_cont."CONTENT"                              AS ATTACHMENT_CONTENT
    
	FROM IDS ids
    INNER JOIN EWPCV_ATTACHMENT att
	ON att.ID = ids.ATTACHMENT_ID
	INNER JOIN EWPCV_ATTACHMENT_CONTENT att_cont
	ON att_cont.ATTACHMENT_ID = att.ID;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'DIPLOMA_VIEW'
) DROP VIEW dbo.DIPLOMA_VIEW;
GO
CREATE view dbo.DIPLOMA_VIEW AS SELECT  
    loi.ID                                  AS LOI_ID,
    dip.ID                                  AS DIPLOMA_ID,
    dip.DIPLOMA_VERSION                     AS DIPLOMA_VERSION,
    dip.ISSUE_DATE                          AS ISSUE_DATE,
    dip.INTRODUCTION                        AS INTRODUCTION,
    dip.SIGNATURE                           AS SIGNATURE,
    dip_sec.TITLE                           AS DIPLOMA_SECTION_TITLE,
    dip_sec.SECTION_NUMBER                  AS DIPLOMA_SECTION_NUMBER,
    dip_sec."CONTENT"                       AS DIPLOMA_SECTION_CONTENT,
    dbo.EXTRAE_ADDIT_INFO(dip_sec.id)           AS ADDITIONAL_INFO,
    att.ATTACH_TYPE                         AS ATTACHMENT_TYPE,
    att.EXTENSION                           AS EXTENSION
	
    FROM EWPCV_DIPLOMA dip
    INNER JOIN EWPCV_LOI loi ON dip.ID = loi.DIPLOMA_ID
    LEFT JOIN EWPCV_DIPLOMA_SECTION dip_sec ON dip_sec.DIPLOMA_ID = dip.ID
    LEFT JOIN EWPCV_DIPLOMA_SEC_ATTACH dip_sec_at ON dip_sec.ID = dip_sec_at.section_id
    LEFT JOIN EWPCV_ATTACHMENT att ON att.ID = dip_sec_at.attachment_id
    LEFT JOIN EWPCV_ATTACHMENT_CONTENT att_content ON att_content.ATTACHMENT_ID = att.ID;
GO

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/
/*
	****************************************
	INICIO MODIFICACIONES SOBRE FACTSHEET_INSTITUTION
	****************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_FACTSHEET_INSTITUTION'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="factsheet_institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="logo_url" type="HTTPS" minOccurs="0" />
				<xs:element name="abreviation" type="xs:string" minOccurs="0" />
				<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="factsheet">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="decision_week_limit" type="xs:integer" />
							<xs:element name="tor_week_limit" type="xs:integer" />
							<xs:element name="nominations_autum_term" type="xs:date" />
							<xs:element name="nominations_spring_term" type="xs:date" />
							<xs:element name="application_autum_term" type="xs:date" />
							<xs:element name="application_spring_term" type="xs:date" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="application_info" type="informationItemType" />
				<xs:element name="housing_info" type="informationItemType" />
				<xs:element name="visa_info" type="informationItemType" />
				<xs:element name="insurance_info" type="informationItemType" />
				<xs:element name="additional_info_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="additional_info" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="info_type" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="accessibility_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="accessibility_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="req_type">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:enumeration value="infrastructure"/>
													<xs:enumeration value="service"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="additional_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element  name="additional_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="url" type="httpListWithOptionalLang" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE FACTSHEET_INSTITUTION
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE IIA
	****************************************
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_IIA'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	    <xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" minOccurs="0"/>
				<xs:element name="end_date" type="xs:date" minOccurs="0"/>
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded">
														<xs:simpleType>
													        <xs:restriction base="xs:integer">
																<xs:minInclusive value="1"/>
																<xs:maxInclusive value="8"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="subject_area_list"  minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area" minOccurs="0" maxOccurs="unbounded" type="subjectAreaType"/>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="0" type="xs:decimal" />
													<xs:element name="unit" minOccurs="0">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO

/*
	Persiste la lista de areas de ensenanza, si la lista está vacia no hace nada
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SUBAREA_LIST'
) DROP PROCEDURE dbo.INSERTA_SUBAREA_LIST;
GO 
CREATE PROCEDURE  dbo.INSERTA_SUBAREA_LIST(@P_COP_COND_ID uniqueidentifier, @P_SUBAREA_LIST xml) AS
BEGIN
	DECLARE @v_isced_code varchar(255)
	DECLARE @SUBJECT_AREA xml
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_SUBAREA_LIST.nodes('subject_area_list/subject_area') T(c) 
	OPEN cur
	FETCH NEXT FROM cur INTO @SUBJECT_AREA
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA, @v_isced_code output
			
			INSERT INTO dbo.EWPCV_COOPCOND_SUBAR(ID, COOPERATION_CONDITION_ID, ISCED_CODE)
				VALUES(NEWID(), @P_COP_COND_ID, @v_isced_code)
			FETCH NEXT FROM cur INTO @SUBJECT_AREA
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO

/*
	Persiste una lista de condiciones de cooperacion asociadas a un IIAS
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_COOP_CONDITIONS'
) DROP PROCEDURE dbo.INSERTA_COOP_CONDITIONS;
GO 
CREATE PROCEDURE  dbo.INSERTA_COOP_CONDITIONS(@P_IIA_ID uniqueidentifier, @P_COOP_COND xml) AS
BEGIN
	DECLARE @v_s_iia_partner_id uniqueidentifier
	DECLARE @v_s_c_p_id uniqueidentifier
	DECLARE @v_r_iia_partner_id uniqueidentifier
	DECLARE @v_r_c_p_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_duration_id uniqueidentifier
	DECLARE @v_mob_number_id uniqueidentifier
	DECLARE @v_cc_id uniqueidentifier

	DECLARE @sp xml, @sp_contacts xml, @rp xml, @rp_contacts xml, @mobility_type xml, @cop_cond_duration xml
	DECLARE @mobility_number integer
	DECLARE @start_date date
	DECLARE @end_date date
	DECLARE @subject_area_language_list xml
	DECLARE @subject_area_list xml
	DECLARE @cooperation_condition xml
	DECLARE @other_info varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('sending_partner'),
		T.c.query('sending_partner/partner_contacts_list'),
		T.c.query('receiving_partner'),
		T.c.query('receiving_partner/partner_contacts_list'),
		T.c.query('mobility_type'),
		T.c.query('cop_cond_duration'),
		T.c.value('(mobility_number)[1]', 'integer'),
		T.c.value('(start_date)[1]', 'date'),
		T.c.value('(end_date)[1]', 'date'),
		T.c.query('subject_area_language_list'),
		T.c.query('subject_area_list'),
		T.c.value('(other_info)[1]', 'varchar(255)'),
		T.c.query('.')
		FROM @P_COOP_COND.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration,
		@mobility_number, @start_date, @end_date, @subject_area_language_list, @subject_area_list, @other_info, @cooperation_condition 
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_IIA_PARTNER @sp, @v_s_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_s_iia_partner_id, @sp_contacts

			EXECUTE dbo.INSERTA_IIA_PARTNER @rp, @v_r_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_r_iia_partner_id, @rp_contacts

			---

			EXECUTE dbo.INSERTA_IIA_MOBILITY_TYPE @mobility_type, @v_mobility_type_id output
			EXECUTE dbo.INSERTA_DURATION @cop_cond_duration, @v_duration_id output
			EXECUTE dbo.INSERTA_MOBILITY_NUMBER @mobility_number, @v_mob_number_id output

			SET @v_cc_id = NEWID()

			INSERT INTO dbo.EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO) 
				VALUES (@v_cc_id, @END_DATE, @START_DATE, @v_duration_id,
					@v_mob_number_id, @v_mobility_type_id, @v_r_iia_partner_id, @v_s_iia_partner_id, @P_IIA_ID, @other_info)

			EXECUTE dbo.INSERTA_SUBAREA_LANSKIL @v_cc_id, @subject_area_language_list
			EXECUTE dbo.INSERTA_SUBAREA_LIST @v_cc_id, @subject_area_list
			EXECUTE dbo.INSERTA_EQFLEVELS @v_cc_id, @cooperation_condition

			FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration, 
				@mobility_number, @start_date, @end_date, @subject_area_language_list, @subject_area_list, @other_info, @cooperation_condition 
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra las subject areas asociadas a una condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_COND_SUBAREA'
) DROP PROCEDURE dbo.BORRA_COOP_COND_SUBAREA;
GO 
CREATE PROCEDURE  dbo.BORRA_COOP_COND_SUBAREA(@P_COOP_COND_ID uniqueidentifier) AS
BEGIN
	DECLARE @ID uniqueidentifier
	DECLARE @ISCED_CODE varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID, ISCED_CODE
		FROM dbo.EWPCV_COOPCOND_SUBAR 
		WHERE COOPERATION_CONDITION_ID = @P_COOP_COND_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @ISCED_CODE 

	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_SUBAR WHERE ID = @ID;
			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			FETCH NEXT FROM cur INTO @ID, @ISCED_CODE 
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra las condiciones de cooperacion de un IIAs
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_CONDITIONS'
) DROP PROCEDURE dbo.BORRA_COOP_CONDITIONS;
GO 
CREATE PROCEDURE  dbo.BORRA_COOP_CONDITIONS(@P_IIA_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_mn_count integer
	DECLARE @v_mt_count integer
	DECLARE @v_md_count integer

	DECLARE @ID uniqueidentifier
	DECLARE @MOBILITY_NUMBER_ID uniqueidentifier
	DECLARE @DURATION_ID uniqueidentifier
	DECLARE @RECEIVING_PARTNER_ID uniqueidentifier
	DECLARE @SENDING_PARTNER_ID uniqueidentifier
	
	DECLARE cur CURSOR LOCAL FOR
		SELECT ID, MOBILITY_NUMBER_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM dbo.EWPCV_COOPERATION_CONDITION 
			WHERE IIA_ID = @P_IIA_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA_LANSKILL @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA @ID
			DELETE FROM dbo.EWPCV_COOPERATION_CONDITION WHERE ID = @ID
			DELETE FROM dbo.EWPCV_MOBILITY_NUMBER WHERE ID = @MOBILITY_NUMBER_ID
		
			DELETE FROM EWPCV_DURATION WHERE ID = @DURATION_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @RECEIVING_PARTNER_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @SENDING_PARTNER_ID

			FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_IIA'
) DROP PROCEDURE dbo.INSERT_IIA;
GO 
CREATE PROCEDURE dbo.INSERT_IIA(@P_IIA XML, @P_IIA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS

BEGIN
	DECLARE @v_notifier_hei varchar(255)

	IF @P_HEI_TO_NOTIFY IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END
	ELSE
	BEGIN
		EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		--validacion, institution id y ounit code de los partners existen
		IF @return_value = 0
		BEGIN
			EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		END
		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_IIA @P_IIA, @P_IIA_ID OUTPUT
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_IIA'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
END
;
GO

/* Actualiza un IIA del sistema.
	Recibe como parametro del Acuerdo Interinstitucional a actualizar
	Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_IIA'
) DROP PROCEDURE dbo.UPDATE_IIA;
GO 
CREATE PROCEDURE dbo.UPDATE_IIA (@P_IIA_ID uniqueidentifier, @P_IIA xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT, 
	@P_HEI_TO_NOTIFY VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	--validacion, institution id y ounit code de los partners existen
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	END 
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_notifier_hei varchar(255)
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer

			IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END

			IF @return_value = 0 
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
				IF  @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = 'El IIA no existe.'
					SET @return_value = -1
				END
			END

			IF @return_value = 0 
			BEGIN
				EXECUTE dbo.ACTUALIZA_IIA @P_IIA_ID, @P_IIA
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
			END 
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_IIA'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO



/*
	****************************************
	FIN MODIFICACIONES SOBRE IIA
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE INSTITUTION
	****************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_INSTITUTION'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="institution_name" type="languageItemListType" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="university_contact_details" minOccurs="0" >
					<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="httpListWithOptionalLang"  minOccurs="0" />
				<xs:element name="logo_url" type="HTTPS"  minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE INSTITUTION
	****************************************
*/


/*
	****************************************
	INICIO MODIFICACIONES SOBRE LEARNING_AGREEMENT
	****************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LEARNING_AGREEMENT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="creditType">
		<xs:sequence>
			<xs:element name="scheme" type="notEmptyStringType" />
			<xs:element name="credit_value">
				<xs:simpleType>
					<xs:restriction base="xs:decimal">
						<xs:totalDigits value="5" />
						<xs:fractionDigits value="1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language_skill" maxOccurs="unbounded">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="language" type="notEmptyStringType" />
						<xs:element name="cefr_level" type="notEmptyStringType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="learning_agreement">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="mobility_id" type="notEmptyStringType" />
				<xs:element name="student_la" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="given_name" type="xs:string" />
							<xs:element name="family_name" type="xs:string" />
							<xs:element name="birth_date" type="xs:string" />
							<xs:element name="citizenship" minOccurs="0">
								<xs:simpleType>
									<xs:restriction base="xs:string">
										<xs:pattern value="[A-Z][A-Z]"/>
									</xs:restriction>
								</xs:simpleType>
							</xs:element>
							<xs:element name="gender" type="genderType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="la_component_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="la_component"  maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="la_component_type">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_id" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:pattern value="(CR|CLS|MOD|DEP)/(.{1,40})"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_code" type="xs:string" minOccurs="0" />
										<xs:element name="los_type" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/> <!-- Course-->
													<xs:enumeration value="1"/> <!-- Class-->
													<xs:enumeration value="2"/> <!-- Module-->
													<xs:enumeration value="3"/> <!-- Degree Programe-->
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="title" type="notEmptyStringType" />
										<xs:element name="academic_term">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="academic_year" type="academicYearType" />
													<xs:element name="institution_id" type="notEmptyStringType" />
													<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
													<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="description" type="languageItemListType" minOccurs="0" />
													<xs:element name="term_number" type="xs:integer" />
													<xs:element name="total_terms" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="credit" type="creditType" />
										<xs:element name="recognition_conditions" type="xs:string" minOccurs="0" />
										<xs:element name="short_description" type="xs:string" minOccurs="0" />
										<xs:element name="status" type="xs:string" minOccurs="0" />
										<xs:element name="reason_code" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
													<xs:enumeration value="5"/>
												</xs:restriction>
											</xs:simpleType>													
										</xs:element>
										<xs:element name="reason_text" type="xs:string" minOccurs="0" />
										<xs:element name="start_date" type="xs:date" minOccurs="0" />
										<xs:element name="end_date" type="xs:date" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="student_signature" type="signatureType" />
				<xs:element name="sending_hei_signature" type="signatureType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'
;

GO

/*
	Persiste un LOI si procede en el sistema a partir de los datos del LOS
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOI'
) DROP PROCEDURE dbo.INSERTA_LOI;
GO 
CREATE PROCEDURE  dbo.INSERTA_LOI(@P_LOS_ID varchar(255) output, @P_LOS_CODE varchar(255) output, @P_LOS_TYPE integer, @P_START_DATE date, @P_END_DATE date, @P_INSTITUTION_ID varchar(255), @v_loi_id varchar(255) output) AS
BEGIN
	DECLARE @v_loi_prefix varchar(255)
	DECLARE @v_los_prefix varchar(255)
	DECLARE @v_los_id varchar(255)
	DECLARE @v_los_code varchar(255)

	IF @P_LOS_ID IS NOT NULL 
		BEGIN
			--Obtencion del prefijo del LOI a partir de los datos del LOS
			SET @v_los_prefix = SUBSTRING(@P_LOS_ID,0,4)
			IF @v_los_prefix = 'CR/' 
				SET @v_loi_prefix = 'CRI/'
			ELSE IF @v_los_prefix = 'CLS' 
				SET @v_loi_prefix = 'CLSI/'
			ELSE IF @v_los_prefix = 'MOD' 
				SET @v_loi_prefix = 'MODI/'
			ELSE IF @v_los_prefix = 'DEP' 
				SET @v_loi_prefix = 'DEPI/'
				
			--Obtencion del LOS si existe para relacionar el LOI
			SELECT @v_los_id = ID, @v_los_code = LOS_CODE FROM EWPCV_LOS WHERE ID = @P_LOS_ID AND INSTITUTION_ID = @P_INSTITUTION_ID
		END
	ELSE IF @P_LOS_CODE IS NOT NULL 
		BEGIN
			--Obtencion del LOS si existe para relacionar el LOI
			SELECT @v_los_id = ID, @v_los_code = LOS_CODE FROM EWPCV_LOS WHERE LOS_CODE = @P_LOS_CODE AND INSTITUTION_ID = @P_INSTITUTION_ID
			
			IF @P_LOS_TYPE IS NOT NULL 
			BEGIN
				--Obtencion del prefijo del LOI a partir de los datos del LOS
				IF @P_LOS_TYPE = 0 
					SET @v_loi_prefix = 'CRI/'
				ELSE IF @P_LOS_TYPE = 1
					SET @v_loi_prefix = 'CLSI/'
				ELSE IF @P_LOS_TYPE = 2
					SET @v_loi_prefix = 'MODI/'
				ELSE IF @P_LOS_TYPE = 3
					SET @v_loi_prefix = 'DEPI/'
			END			
		END
	
	--Insertamos el LOI y su relacion con el los si este existe	
	IF @v_loi_prefix IS NOT NULL 
	BEGIN
		SET @v_loi_id = CONCAT(@v_loi_prefix, NEWID())
		--informamos id y fechas
		INSERT INTO dbo.EWPCV_LOI (ID, START_DATE, END_DATE) VALUES (@v_loi_id, @P_START_DATE, @P_END_DATE)
		--si el los estaba en bbdd insertamos la realacion LOS_LOI
		IF @v_los_id IS NOT NULL 
		BEGIN
			INSERT INTO dbo.EWPCV_LOS_LOI (LOI_ID, LOS_ID) VALUES (@v_loi_id,@v_los_id)
		END
	END

	--Si hemos encontrado algo en BBDD seteamos la informacion para completar el componente
	IF @v_los_id IS NOT NULL 
		SET @P_LOS_ID = @v_los_id 
	IF @v_los_code IS NOT NULL 
		SET @P_LOS_CODE = @v_los_code
END
;
GO


/*
	Persiste un componente en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_COMPONENTE'
) DROP PROCEDURE dbo.INSERTA_COMPONENTE;
GO 
CREATE PROCEDURE  dbo.INSERTA_COMPONENTE(@P_COMPONENT xml, @P_LA_ID uniqueidentifier, @P_LA_REVISION integer,  @v_id uniqueidentifier output) AS

BEGIN
	DECLARE @v_at_id uniqueidentifier
	DECLARE	@v_c_id uniqueidentifier
	DECLARE @v_loi_id varchar(255)
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @LOS_ID varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @LOS_TYPE integer
	DECLARE @STATUS integer
	DECLARE @TITLE varchar(255)
	DECLARE @REASON_CODE integer
	DECLARE @REASON_TEXT varchar(255)
	DECLARE @LA_COMPONENT_TYPE integer
	DECLARE @RECOGNITION_CONDITIONS varchar(255)
	DECLARE @SHORT_DESCRIPTION varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @CREDIT xml
	DECLARE @ACADEMIC_TERM xml
	   
	SELECT
		@LOS_ID = T.c.value('(los_id)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@LOS_TYPE = T.c.value('(los_type)[1]', 'integer'),
		@STATUS = T.c.value('(status)[1]', 'integer'),
		@TITLE = T.c.value('(title)[1]', 'varchar(255)'),
		@REASON_CODE = T.c.value('(reason_code)[1]', 'integer'),
		@REASON_TEXT = T.c.value('(reason_text)[1]', 'varchar(255)'),
		@LA_COMPONENT_TYPE = T.c.value('(la_component_type)[1]', 'integer'),
		@RECOGNITION_CONDITIONS = T.c.value('(recognition_conditions)[1]', 'varchar(255)'),
		@SHORT_DESCRIPTION = T.c.value('(short_description)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@CREDIT = T.c.query('credit'),
		@ACADEMIC_TERM = T.c.query('academic_term')
	FROM @P_COMPONENT.nodes('la_component') T(c)
	SET @v_id = NEWID()

	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM, @v_at_id output

	
	---- obtencion de fechas del LOI si vienen en el componente se cogen esas si no del academic term
		IF @START_DATE IS NULL 
		SELECT 
			@START_DATE = T.c.value('(start_date)[1]', 'date')
		FROM @ACADEMIC_TERM.nodes('academic_term') T(c)
		
		IF @END_DATE IS NULL 
		SELECT 
			@END_DATE = T.c.value('(end_date)[1]', 'date')
		FROM @ACADEMIC_TERM.nodes('academic_term') T(c)
	-- obtenemos el institution_id el sending si es reconocido o el receiving si cualquier otro tipo
		IF @LA_COMPONENT_TYPE = 1
			SELECT @INSTITUTION_ID = M.SENDING_INSTITUTION_ID 
			FROM dbo.EWPCV_MOBILITY M 
			INNER JOIN dbo.EWPCV_MOBILITY_LA ML ON M.ID = ML.MOBILITY_ID AND M.MOBILITY_REVISION = ML.MOBILITY_REVISION
			WHERE ML.LEARNING_AGREEMENT_ID = @P_LA_ID AND ML.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
		ELSE
			SELECT @INSTITUTION_ID = M.RECEIVING_INSTITUTION_ID 
			FROM dbo.EWPCV_MOBILITY M 
			INNER JOIN dbo.EWPCV_MOBILITY_LA ML ON M.ID = ML.MOBILITY_ID AND M.MOBILITY_REVISION = ML.MOBILITY_REVISION
			WHERE ML.LEARNING_AGREEMENT_ID = @P_LA_ID AND ML.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			
	EXECUTE dbo.INSERTA_LOI @LOS_ID output, @LOS_CODE output, @LOS_TYPE, @START_DATE, @END_DATE, @INSTITUTION_ID, @v_loi_id output
	
	INSERT INTO dbo.EWPCV_LA_COMPONENT (ID, ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (@v_id, @v_at_id, @v_loi_id, @LOS_CODE, @LOS_ID, @STATUS, @TITLE, @REASON_CODE, @REASON_TEXT,
				@LA_COMPONENT_TYPE, @RECOGNITION_CONDITIONS, @SHORT_DESCRIPTION)

	EXECUTE dbo.INSERTA_CREDIT @CREDIT, @v_c_id output
	INSERT INTO dbo.EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (@v_id, @v_c_id)

	IF @LA_COMPONENT_TYPE = 0
		INSERT INTO dbo.EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 1
		INSERT INTO dbo.EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 2
		INSERT INTO dbo.EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 3
		INSERT INTO dbo.EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 4
		INSERT INTO dbo.EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

END
;
GO

/*
	Borra un LOI 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOI'
) DROP PROCEDURE dbo.BORRA_LOI;
GO 
CREATE PROCEDURE  dbo.BORRA_LOI(@P_LOI_ID varchar(255)) AS
BEGIN
	DELETE FROM dbo.EWPCV_LOS_LOI WHERE LOI_ID = @P_LOI_ID
	DELETE FROM dbo.EWPCV_LOI WHERE ID = @P_LOI_ID
	
END
;
GO

/*
	Borra un componente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COMPONENT'
) DROP PROCEDURE dbo.BORRA_COMPONENT;
GO 
CREATE PROCEDURE  dbo.BORRA_COMPONENT(@P_ID uniqueidentifier) AS
BEGIN
	DECLARE @v_at_id uniqueidentifier
	EXECUTE dbo.BORRA_CREDITS @P_ID
	
	DECLARE @v_loi_id varchar(255)
	SELECT @v_loi_id = LOI_ID FROM dbo.EWPCV_LA_COMPONENT WHERE ID = @P_ID
	
	SELECT @v_at_id = ACADEMIC_TERM_DISPLAY_NAME
			FROM dbo.EWPCV_LA_COMPONENT
			WHERE ID = @P_ID
	DELETE FROM dbo.EWPCV_LA_COMPONENT WHERE ID = @P_ID;
	
	EXECUTE dbo.BORRA_ACADEMIC_TERM @v_at_id
	EXECUTE dbo.BORRA_LOI @v_loi_id

END
;
GO


/* Elimina un learning agreement del sistema.
	Recibe como parametro el identificador del learning agreement a actualizar
	Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.DELETE_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.DELETE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @LEARNING_AGREEMENT_REVISION integer
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_m_id uniqueidentifier
	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY IS NULL  
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
		IF @v_count = 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND TOR_ID IS NOT NULL;
		IF @v_count > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden borrar LA que ya tengan un TOR'
			SET @return_value = -1
		END
	END
	
	SELECT @LEARNING_AGREEMENT_REVISION = MAX(LEARNING_AGREEMENT_REVISION) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1	
	END
	
	IF @return_value = 0
	BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		--cursor aqui para cada revision	
		DECLARE cur CURSOR LOCAL FOR
		SELECT LEARNING_AGREEMENT_REVISION FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
		
		OPEN cur
		FETCH NEXT FROM cur INTO  @LEARNING_AGREEMENT_REVISION 
		WHILE @@FETCH_STATUS = 0
			BEGIN
				
				IF @v_m_id is NULL
					SELECT @v_m_id = MOBILITY_ID
						FROM dbo.EWPCV_MOBILITY_LA 
						WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LEARNING_AGREEMENT_REVISION

				EXECUTE dbo.BORRA_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION
				
				FETCH NEXT FROM cur INTO  @LEARNING_AGREEMENT_REVISION 
			END
		CLOSE cur
		DEALLOCATE cur
		
		EXECUTE dbo.INSERTA_NOTIFICATION @v_m_id, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_LEARNING_AGREEMENT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
	END

END 
;
GO

/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
	Recibe como parametro el identificador del learning agreement a actualizar.
	El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
	Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_LA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_la_revision integer
	DECLARE @v_m_revision integer
	DECLARE @v_no_firmado integer
	DECLARE @MOBILITY_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_la_revision_siguiente integer
	DECLARE @v_count integer

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END
	END
	   	 
	IF @return_value = 0
	BEGIN
		SELECT @v_la_revision = MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
		IF @v_la_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado no existe'
			SET @return_value = -1
		END
	END
	
	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
			FROM dbo.EWPCV_MOBILITY_LA
				WHERE MOBILITY_ID = @MOBILITY_ID
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @v_la_revision;
		IF @v_m_revision IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada para el learning agreement no es correcta.'
			SET @return_value = -1
		END 
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_no_firmado = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND STATUS = 0
		IF @v_no_firmado > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.'
			SET @return_value = -1
		END 
	END


	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @v_la_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1
	END
	
	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND TOR_ID IS NOT NULL;
		IF @v_count > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se puede actualizar un LA que ya tengan un TOR'
			SET @return_value = -1
		END
	END
	
	IF @return_value = 0
	BEGIN
		--validamos que los institution y ounit de los componentes sean correctos
		EXECUTE dbo.VALIDA_DATOS_LA @P_LA, @P_ERROR_MESSAGE output, @return_value output
	END
	
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_la_revision_siguiente = @v_la_revision + 1
			EXECUTE dbo.INSERTA_LA_REVISION @P_LA_ID, @P_LA, @v_la_revision_siguiente, @v_m_revision, @v_la_id output
			EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					   			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE LEARNING_AGREEMENT
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE MOBILITY
	****************************************
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_MOBILITY'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="photoUrlType">
		<xs:sequence>
			<xs:element name="url" type="HTTPS" minOccurs="0" />
			<xs:element name="photo_size" type="photoSizeType" minOccurs="0" />
			<xs:element name="photo_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="photo_public" >
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:enumeration value="0"/>
						<xs:enumeration value="1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="photoSizeType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d+x\d+"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="cooperation_condition_id" type="xs:string" minOccurs="0" />
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
							<xs:element name="photo_url_list" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="photo_url" maxOccurs="unbounded" minOccurs="0" type="photoUrlType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="eqf_level_nomination" minOccurs="1">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:minInclusive value="1"/>
							<xs:maxInclusive value="8"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="eqf_level_departure" minOccurs="1">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:minInclusive value="1"/>
							<xs:maxInclusive value="8"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="academic_term">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="academic_year" type="academicYearType" />
							<xs:element name="institution_id" type="notEmptyStringType" />
							<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
							<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="description" type="languageItemListType" minOccurs="0" />
							<xs:element name="term_number" type="xs:integer" />
							<xs:element name="total_terms" type="xs:integer" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="status_outgoing">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>';

GO

/* Actualiza una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a actualizar
	Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_MOBILITY'
) DROP PROCEDURE dbo.UPDATE_MOBILITY;
GO 
CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer
	DECLARE @v_count integer

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 
		BEGIN
			SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
			SET @return_value = -1
		END
		ELSE 
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
				INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
				WHERE MLA.MOBILITY_ID = @P_OMOBILITY_ID
					AND LA.TOR_ID IS NOT NULL;
			IF @v_count > 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
				SET @return_value = -1
			END
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)

		SELECT
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]','integer')
		FROM @P_MOBILITY.nodes('/mobility') T(c) 

		EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
	END 
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


-- Procedure VALIDA_STATUS
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_STATUS'
) DROP PROCEDURE dbo.VALIDA_STATUS;
GO 
CREATE PROCEDURE dbo.VALIDA_STATUS(@P_STATUS int, @P_ES_OUTGOING int, @P_ERROR_MESSAGE varchar(255) output, @return_value int output) AS
BEGIN
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	SET @return_value = 0
	IF @P_ES_OUTGOING = 0 
	BEGIN
		IF @P_STATUS NOT in (0, 1, 2, 3)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_outgoing no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING = -1
	BEGIN
		IF @P_STATUS NOT in (4, 5, 6)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_incoming no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING NOT in (-1, 0)
	BEGIN
		SET @P_ERROR_MESSAGE = '@P_ES_OUTGOING debe especificar si es un status_incoming (-1) o status_outgoing (0).'
		SET @return_value = -1
	END 
END
;
GO


	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY_INST'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST(@P_S_INSTITUTION xml, @P_R_INSTITUTION  xml, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS 
BEGIN
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	DECLARE @v_count int
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_OUnit_CODE varchar(255)	
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @RECEIVING_OUnit_CODE varchar(255)

	SET @SENDING_INSTITUTION_ID = ''
	SET @RECEIVING_INSTITUTION_ID = ''
	SET @return_value = 0

	SELECT 
		@SENDING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@SENDING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_S_INSTITUTION.nodes('sending_institution') T(c)	
	SELECT 
		@RECEIVING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@RECEIVING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_R_INSTITUTION.nodes('receiving_institution') T(c)

	-- validacion de sender 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@SENDING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @SENDING_INSTITUTION_ID + ' informada como sender institution no existe en el sistema.')
	END 
	ELSE IF @SENDING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@SENDING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@SENDING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @SENDING_OUnit_CODE + 'vinculada a ' + @SENDING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

	-- validacion de receiver 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@RECEIVING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @RECEIVING_INSTITUTION_ID + ' informada como receiver institution no existe en el sistema.')
	END 
	ELSE IF @RECEIVING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@RECEIVING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@RECEIVING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @RECEIVING_OUnit_CODE + 'vinculada a ' + @RECEIVING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

END
;
GO

/* Elimina una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a eliminar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_MOBILITY'
) DROP PROCEDURE dbo.DELETE_MOBILITY;
GO 
CREATE PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_m_revision integer
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		SET @return_value = 0
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count >0
			BEGIN
				SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_MOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.'
					SET @return_value = -1
				END
				ELSE 
				BEGIN
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
					INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
					WHERE MLA.MOBILITY_ID = @P_OMOBILITY_ID
						AND LA.TOR_ID IS NOT NULL;
					IF @v_count > 0
					BEGIN
						SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
						SET @return_value = -1
					END
				END
				IF @return_value = 0
				BEGIN
					EXECUTE dbo.BORRA_MOBILITY @P_OMOBILITY_ID
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END
			END
		ELSE
			BEGIN
				SET @P_ERROR_MESSAGE = 'La movilidad no existe'
				SET @return_value = -1
			END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_MOBILITY'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE MOBILITY
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE OUNITS
	****************************************
*/

IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_OUNIT'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="ceroOneType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="organization_unit">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="organization_unit_code" type="notEmptyStringType" />
				<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="parent-ounit-id" type="xs:string" minOccurs="0" />
				<xs:element name="is-tree-structure" type="ceroOneType" minOccurs="1" />
				<xs:element name="ounit_contact_details" minOccurs="0" >
				<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="httpListWithOptionalLang"  minOccurs="0" />
				<xs:element name="logo_url" type="HTTPS"  minOccurs="0" />

			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE OUNITS
	****************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
 /*
***************************************
******** Objetos Los ******************
***************************************
*/




/* VALIDAR estructura datos LOS */
/*

	VALIDACION VIA XSD

*/
-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA LOS

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LOS' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LOS;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LOS
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="losTypeType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="3"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="los">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="eqf_level" type="xs:integer" />
				<xs:element name="institution_id" type="notEmptyStringType" minOccurs="0" />
				<xs:element name="iscedf" type="xs:string" />
				<xs:element name="los_code" type="xs:string" />
				<xs:element name="los_name_list" type="languageItemListType" minOccurs="0" />
				<xs:element name="los_description_list" type="languageItemListType" />
				<xs:element name="los_url_list" type="languageItemListType" />				
				<xs:element name="organization_unit_id" type="xs:string" />
				<xs:element name="subject_area" type="xs:string" />
				<xs:element name="los_type" type="losTypeType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_LOS'
) DROP PROCEDURE dbo.VALIDA_LOS;
GO 
CREATE PROCEDURE dbo.VALIDA_LOS(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_LOS)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO



 
/*
    *************************************
     FUNCIONES
    *************************************
*/

  /* Devuelve el codigo del tipo del LOS */
  
  
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'GET_LOS_TYPE'
) DROP FUNCTION dbo.GET_LOS_TYPE;
GO
CREATE FUNCTION [dbo].[GET_LOS_TYPE](@P_TYPE_ID integer) RETURNS varchar(255)
BEGIN
	DECLARE @return_value varchar(255)

	IF @P_TYPE_ID = 0 
		set @return_value = 'CR'
	IF @P_TYPE_ID = 1 
		set @return_value = 'CLS'
	IF @P_TYPE_ID = 2 
		set @return_value = 'MOD'
	IF @P_TYPE_ID = 3 
		set @return_value = 'DEP'

	return @return_value
END
;

GO



   /* Comprueba si existe el LOS */


IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXISTE_LOS'
) DROP FUNCTION dbo.EXISTE_LOS;
GO
CREATE FUNCTION [dbo].[EXISTE_LOS](@P_LOS_ID varchar(255),  @P_LOS_PARENT_ID varchar(255)) RETURNS integer
BEGIN
	DECLARE @v_count integer
	DECLARE @return_value integer
	SET @return_value = 0

	IF @P_LOS_ID is null 
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
	ELSE
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID

	IF @v_count = 0 
		set @return_value = -1

	return @return_value
END
;

GO


 /*	Valida si el padre puede tener un hijo del tipo informado	*/

 
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'VALIDA_PADRE'
) DROP FUNCTION dbo.VALIDA_PADRE;
GO
CREATE FUNCTION dbo.VALIDA_PADRE(@P_PARENT_TYPE_ID integer, @P_CHILD_TYPE_ID integer) RETURNS integer AS
BEGIN
  	DECLARE @return_value integer
	DECLARE @v_dep_allowed_childs_type varchar(255)
	DECLARE @v_mod_allowed_childs_type varchar(255)
	DECLARE @v_cr_allowed_childs_type varchar(255)
	SET @return_value = -1
	SET @v_dep_allowed_childs_type = '123'
	SET @v_mod_allowed_childs_type = '3'
	SET @v_cr_allowed_childs_type = '4'
	
	IF @P_PARENT_TYPE_ID = 1 AND CHARINDEX(STR(@P_CHILD_TYPE_ID), @v_dep_allowed_childs_type) <> 0 
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 2 AND CHARINDEX(STR(@P_CHILD_TYPE_ID), @v_mod_allowed_childs_type) <> 0 
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 3 AND CHARINDEX(STR(@P_CHILD_TYPE_ID), @v_cr_allowed_childs_type) <> 0 
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 4 AND @P_CHILD_TYPE_ID is null 
		SET @return_value = 0

	return @return_value

END 
;
GO
   
   
   
 /* Valida si el tipo introducido del LOS es correcto */
 

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'VALIDA_LOS_TYPE'
) DROP FUNCTION dbo.VALIDA_LOS_TYPE;
GO
CREATE FUNCTION dbo.VALIDA_LOS_TYPE(@P_TYPE varchar(255)) RETURNS integer AS
BEGIN
  	DECLARE @return_value integer
	DECLARE @v_types varchar(255)
	SET @return_value = 0
	SET @v_types = '0123'

	IF @P_TYPE is not null AND CHARINDEX(@P_TYPE, @v_types) = 0 
		SET @return_value = -1

	return @return_value

END 
;
GO





  
/*
    *************************************
    PROCEDURES
    *************************************
*/


/* Cambia de padre a un LOS del sistema.
	    Admite el id del LOS
	    Admite el id del antiguo padre del LOS
	    Admite el id del nuevo padre del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/
 
      
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CHANGE_PARENT'
) DROP PROCEDURE dbo.CHANGE_PARENT;
GO
CREATE PROCEDURE [dbo].[CHANGE_PARENT](@P_LOS_ID varchar(255), @P_LOS_OLD_PARENT_ID varchar(255), @P_LOS_NEW_PARENT_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_existe_los integer
	DECLARE @v_type_parent integer
	DECLARE @v_type integer

	SET @v_existe_los = 0
	SET @v_type_parent = 0
	SET @v_type = 0

	BEGIN TRY
		BEGIN TRANSACTION

		IF dbo.EXISTE_LOS(@P_LOS_ID, @P_LOS_OLD_PARENT_ID) = 0
		BEGIN
			SELECT @v_type = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID

			IF @P_LOS_NEW_PARENT_ID is not null
			BEGIN
				IF dbo.EXISTE_LOS(@P_LOS_ID, @P_LOS_NEW_PARENT_ID) = 0
				BEGIN
					SELECT @v_type_parent = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_NEW_PARENT_ID

					IF @v_type_parent is not null and dbo.VALIDA_PADRE(@v_type_parent, @v_type) < 0 
					BEGIN
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El padre indicado no puede contener hijos con el tipo introducido.')
						SET @return_value = -1
					END 

					IF @return_value = 0 
					BEGIN
						UPDATE dbo.EWPCV_LOS_LOS SET LOS_ID = @P_LOS_NEW_PARENT_ID WHERE LOS_ID = @P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID
						UPDATE dbo.EWPCV_LOS SET TOP_LEVEL_PARENT = 0 WHERE ID = @P_LOS_ID
					END 

				END
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - No existe el LOS indicado como nuevo padre en el sistema.')
					SET @return_value = -1
				END 
			END
			ELSE
			BEGIN
				IF @v_type = 1
				BEGIN
					DELETE FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID
					UPDATE dbo.EWPCV_LOS SET TOP_LEVEL_PARENT = 1 WHERE ID = @P_LOS_ID
				END 
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - Se ha de indicar el id del nuevo padre del LOS')
					SET @return_value = -1
				END
			END 
		END
		ELSE
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - No existe el LOS indicado en el sistema')
			SET @return_value = -1
		END 
	
		IF @return_value = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.CHANGE_PARENT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END
;
GO



 /* Valida las dependencias con el LOS */


      
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DEPENDENCIAS'
) DROP PROCEDURE dbo.VALIDA_DEPENDENCIAS;
GO
CREATE PROCEDURE [dbo].[VALIDA_DEPENDENCIAS](@P_LOS_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_count integer
	SET @v_count = 0

	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LA_COMPONENT LA 
		WHERE LA.LOS_ID = @P_LOS_ID 
		  OR LA.LOS_ID IN (
			SELECT LL.OTHER_LOS_ID FROM dbo.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = @P_LOS_ID
		)

	IF @v_count > 0
	BEGIN
		set @return_value = -1
		set @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un Learning Agreement.')
	END

	IF @return_value = 0 
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS_LOI LO
		  WHERE LO.LOS_ID = @P_LOS_ID 
			OR LO.LOS_ID IN (
			  SELECT LL.OTHER_LOS_ID FROM dbo.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = @P_LOS_ID
		  )
		IF @v_count > 0 
		BEGIN
			set @return_value = -1
			set @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un LOI.')
		END
	END 

END
;
GO





 /* Valida la calidad del dato en el objeto LOS  */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LOS'
) DROP PROCEDURE dbo.VALIDA_DATOS_LOS;
GO
CREATE PROCEDURE dbo.VALIDA_DATOS_LOS(@P_LOS XML, @P_LOS_PARENT_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS

BEGIN
    DECLARE @v_count_ins integer
    DECLARE @v_count_ounit integer
    DECLARE @v_type_parent integer
	SET @v_count_ins = 0
	SET @v_count_ounit = 0
	SET @v_type_parent = 0

	DECLARE @LOS_TYPE varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID uniqueidentifier

	SELECT
		@LOS_TYPE = T.c.value('(los_type)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'uniqueidentifier')
	FROM @P_LOS.nodes('los') T(c)


	IF dbo.VALIDA_LOS_TYPE(@LOS_TYPE) < 0
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El tipo introducido no es válido. 0 - "Course", 1 - "Class", 2 - "Module", 3 - "Degree Programme".')
		SET @return_value = -1
	END

	IF @INSTITUTION_ID is not null
	BEGIN
		SELECT @v_count_ins = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE LOWER(SCHAC)= LOWER(@INSTITUTION_ID)

		IF @v_count_ins = 0
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - INSTITUTION_ID: No existe la institución introducida en el sistema.')
			SET @return_value = -1
		END
		ELSE
		BEGIN
			IF @ORGANIZATION_UNIT_ID is not null 
			BEGIN
				 SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT OU 
				  INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON IOU.ORGANIZATION_UNITS_ID = OU.ID 
				  INNER JOIN dbo.EWPCV_INSTITUTION EI ON EI.ID = IOU.INSTITUTION_ID 
				  WHERE OU.ID = @ORGANIZATION_UNIT_ID
				  AND LOWER(EI.INSTITUTION_ID) = LOWER(@INSTITUTION_ID)

				IF @v_count_ounit = 0
				BEGIN
					SET @return_value = -1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No existe la organization unit introducida en el sistema.')
				END
			END 
		END
	END

	IF @ORGANIZATION_UNIT_ID is not null 
	BEGIN
		SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT OU
			WHERE OU.ID = @ORGANIZATION_UNIT_ID
  
        IF @v_count_ounit = 0 
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - ORGANIZATION_UNIT_ID: No existe la organization unit introducida en el sistema.')
        END
	END

	IF @return_value = 0 AND @P_LOS_PARENT_ID IS NOT NULL
	BEGIN
		SELECT @v_type_parent = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_PARENT_ID
        IF @v_type_parent IS NOT NULL AND dbo.VALIDA_PADRE(@v_type_parent, @LOS_TYPE) < 0 
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El padre indicado no puede contener hijos con el tipo introducido.')
        END
	END
END
;
GO




-----------------------------------
-- Procedimientos para borrado
-----------------------------------

   /* Borra el listado de nombres del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_NAMES'
) DROP PROCEDURE dbo.BORRA_LOS_NAMES;
GO
CREATE PROCEDURE dbo.BORRA_LOS_NAMES(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @NAME_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT NAME_ID
		  FROM dbo.EWPCV_LOS_NAME
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO

   /* Borra el listado de urls del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_URLS'
) DROP PROCEDURE dbo.BORRA_LOS_URLS;
GO
CREATE PROCEDURE dbo.BORRA_LOS_URLS(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @URL_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT URL_ID
		  FROM dbo.EWPCV_LOS_URLS
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @URL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_URLS WHERE URL_ID = @URL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @URL_ID
			FETCH NEXT FROM cur INTO @URL_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;

GO




   /* Borra el listado de descripciones del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_DESCRIPTIONS'
) DROP PROCEDURE dbo.BORRA_LOS_DESCRIPTIONS;
GO
CREATE PROCEDURE dbo.BORRA_LOS_DESCRIPTIONS(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @DESCRIPTION_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
		SELECT DESCRIPTION_ID
		  FROM dbo.EWPCV_LOS_DESCRIPTION
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @DESCRIPTION_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
			FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO



   /* Elimina el LOS de la base de datos */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS'
) DROP PROCEDURE dbo.BORRA_LOS;
GO
CREATE PROCEDURE dbo.BORRA_LOS(@P_LOS_ID varchar(255)) AS
BEGIN

	DECLARE @OTHER_LOS_ID varchar(255)
  
	IF @P_LOS_ID is not null 
	BEGIN
		EXECUTE dbo.BORRA_LOS_NAMES @P_LOS_ID
		EXECUTE dbo.BORRA_LOS_DESCRIPTIONS @P_LOS_ID
		EXECUTE dbo.BORRA_LOS_URLS @P_LOS_ID

		DECLARE cur CURSOR LOCAL FOR
		SELECT OTHER_LOS_ID 
			FROM dbo.EWPCV_LOS_LOS
			WHERE LOS_ID = @P_LOS_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @OTHER_LOS_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_LOS_LOS WHERE OTHER_LOS_ID = @OTHER_LOS_ID
				EXECUTE dbo.BORRA_LOS @OTHER_LOS_ID
				FETCH NEXT FROM cur INTO @OTHER_LOS_ID
			END
		CLOSE cur
		DEALLOCATE cur

		DELETE FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
			   
	END
END
;
GO


  
 	/* Elimina un LOS del sistema y sus hijos
	    Admite el id del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_LOS'
) DROP PROCEDURE dbo.DELETE_LOS;
GO
CREATE PROCEDURE DELETE_LOS(@P_LOS_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS 
BEGIN
	DECLARE @v_count integer
	DECLARE @valida integer
	SET @return_value = 0


	BEGIN TRY
		BEGIN TRANSACTION
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
		IF @v_count > 0
		BEGIN
			EXECUTE dbo.VALIDA_DEPENDENCIAS @P_LOS_ID, @P_ERROR_MESSAGE output, @valida output
			IF @valida < 0
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = CONCAT('No se ha eliminado el LOS indicado.', @P_ERROR_MESSAGE )
			END
			ELSE
			BEGIN
				EXECUTE dbo.BORRA_LOS @P_LOS_ID
			END
		END
		ELSE
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El LOS indicado no existe'
		END
		
		IF @return_value = 0
		BEGIN
			COMMIT
		END

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO


---------------------------
/* Funciones insert */
---------------------------


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_DESCRIPTIONS'
) DROP PROCEDURE dbo.INSERTA_LOS_DESCRIPTIONS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_DESCRIPTIONS(@P_LOS_ID varchar(255), @P_LOS_DESCRIPTION_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_DESCRIPTION_LIST.nodes('los_description_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_DESCRIPTION (LOS_ID, DESCRIPTION_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO  


  /* Inserta el listado de nombres del LOS */
  
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_NAMES'
) DROP PROCEDURE dbo.INSERTA_LOS_NAMES;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_NAMES(@P_LOS_ID varchar(255), @P_LOS_NAME_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_NAME_LIST.nodes('los_name_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_NAME (LOS_ID, NAME_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


   /* Inserta el listado de urls del LOS */
   
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_URLS'
) DROP PROCEDURE dbo.INSERTA_LOS_URLS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_URLS(@P_LOS_ID varchar(255), @P_LOS_URL_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_URL_LIST.nodes('los_url_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_URLS (LOS_ID, URL_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


----------------------------------
  -- ACTUALIZACIONES
----------------------------------

 /* Actualiza el LOS en la base de datos */
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_LOS'
) DROP PROCEDURE dbo.ACTUALIZA_LOS;
GO
CREATE PROCEDURE dbo.ACTUALIZA_LOS(@P_LOS_ID varchar(255), @P_LOS xml) AS
BEGIN
	DECLARE @EQF_LEVEL varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ISCEDF varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID uniqueidentifier
	DECLARE @SUBJECT_AREA varchar(255)
	DECLARE @LOS_NAME_LIST xml
	DECLARE @LOS_DESCRIPTION_LIST xml 
	DECLARE @LOS_URL_LIST xml
	   
	SELECT
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ISCEDF = T.c.value('(iscedf)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'uniqueidentifier'),
		@SUBJECT_AREA = T.c.value('(subject_area)[1]', 'varchar(255)'),
		@LOS_NAME_LIST = T.c.query('los_name_list'),
		@LOS_DESCRIPTION_LIST = T.c.query('los_description_list'),
		@LOS_URL_LIST = T.c.query('los_url_list')
	FROM @P_LOS.nodes('los') T(c)

	EXECUTE dbo.BORRA_LOS_NAMES @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_NAMES  @P_LOS_ID, @LOS_NAME_LIST

	EXECUTE dbo.BORRA_LOS_DESCRIPTIONS @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_DESCRIPTIONS  @P_LOS_ID, @LOS_DESCRIPTION_LIST

	EXECUTE dbo.BORRA_LOS_URLS @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_URLS  @P_LOS_ID, @LOS_URL_LIST


	UPDATE dbo.EWPCV_LOS SET EQF_LEVEL = @EQF_LEVEL, INSTITUTION_ID = @INSTITUTION_ID, ISCEDF = @ISCEDF, 
		LOS_CODE = @LOS_CODE, ORGANIZATION_UNIT_ID = @ORGANIZATION_UNIT_ID,
		SUBJECT_AREA = @SUBJECT_AREA WHERE ID = @P_LOS_ID
		
END
;
GO


  /* Inserta el LOS en la tabla */
  
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS'
) DROP PROCEDURE dbo.INSERTA_LOS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS(@P_LOS xml, @P_LOS_PARENT_ID varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_los_type varchar(255)
	DECLARE @v_top_level_parent integer

	DECLARE @EQF_LEVEL varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ISCEDF varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID uniqueidentifier
	DECLARE @SUBJECT_AREA varchar(255)
	DECLARE @LOS_TYPE varchar(255)
	DECLARE @LOS_NAME_LIST xml
	DECLARE @LOS_DESCRIPTION_LIST xml 
	DECLARE @LOS_URL_LIST xml

	
	SELECT
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ISCEDF = T.c.value('(iscedf)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'uniqueidentifier'),
		@SUBJECT_AREA = T.c.value('(subject_area)[1]', 'varchar(255)'),
		@LOS_TYPE = T.c.value('(los_type)[1]', 'integer'),
		@LOS_NAME_LIST = T.c.query('los_name_list'),
		@LOS_DESCRIPTION_LIST = T.c.query('los_description_list'),
		@LOS_URL_LIST = T.c.query('los_url_list')
	FROM @P_LOS.nodes('los') T(c)

	SET @v_los_type = dbo.GET_LOS_TYPE(@LOS_TYPE)
	SET @v_id = CONCAT(@v_los_type + '/' , NEWID())

	IF @P_LOS_PARENT_ID is null
		SET @v_top_level_parent = 0
	ELSE 
		SET @v_top_level_parent = 1

	INSERT INTO dbo.EWPCV_LOS 
    (ID, EQF_LEVEL, INSTITUTION_ID, 
      ISCEDF, LOS_CODE, ORGANIZATION_UNIT_ID, SUBJECT_AREA, TOP_LEVEL_PARENT, "TYPE")
    VALUES
    (@v_id, @EQF_LEVEL, @INSTITUTION_ID, 
      @ISCEDF, @LOS_CODE, @ORGANIZATION_UNIT_ID, @SUBJECT_AREA, @v_top_level_parent, @LOS_TYPE
    )

	EXECUTE dbo.INSERTA_LOS_NAMES  @v_id, @LOS_NAME_LIST
	EXECUTE dbo.INSERTA_LOS_DESCRIPTIONS  @v_id, @LOS_DESCRIPTION_LIST
	EXECUTE dbo.INSERTA_LOS_URLS  @v_id, @LOS_URL_LIST

	IF @P_LOS_PARENT_ID IS NOT NULL
		INSERT INTO dbo.EWPCV_LOS_LOS (LOS_ID, OTHER_LOS_ID) VALUES (@P_LOS_PARENT_ID, @v_id)
 
	SET @return_value = @v_id

END
;
GO




	/* Inserta un LOS en el sistema
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un id del padre del los
		Admite un parametro de salida con el identificador del LOS en el sistema
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_LOS'
) DROP PROCEDURE dbo.INSERT_LOS;
GO
CREATE PROCEDURE dbo.INSERT_LOS(@P_LOS xml, @P_LOS_PARENT_ID varchar(255), @P_LOS_ID varchar(255) output, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS
BEGIN
	DECLARE @v_c_id varchar(255)
	DECLARE @v_type integer
	DECLARE @v_allowed_parent int

	BEGIN TRY
		BEGIN TRANSACTION
		
		EXECUTE dbo.VALIDA_LOS @P_LOS, @P_ERROR_MESSAGE output, @return_value output

		IF @return_value = 0 
			EXECUTE dbo.VALIDA_DATOS_LOS @P_LOS, @P_LOS_PARENT_ID, @P_ERROR_MESSAGE output, @return_value output	

		IF @return_value = 0 
			EXECUTE dbo.INSERTA_LOS @P_LOS, @P_LOS_PARENT_ID, @P_LOS_ID output	

		IF @return_value = 0
			COMMIT TRANSACTION
		ELSE 
			ROLLBACK TRANSACTION

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END

;
GO
  
  

	/* Actualiza un LOS en el sistema
	    Admite el id del LOS
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_LOS'
) DROP PROCEDURE dbo.UPDATE_LOS;
GO
CREATE PROCEDURE dbo.UPDATE_LOS(@P_LOS_ID varchar(255), @P_LOS xml, @P_ERROR_MESSAGE varchar(255) output, @return_value integer) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @LOS_TYPE integer

	BEGIN TRY
		BEGIN TRANSACTION

		SELECT
			@LOS_TYPE = T.c.value('(los_type)[1]', 'integer')
		FROM @P_LOS.nodes('los') T(c)

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID AND [TYPE] = @LOS_TYPE
	
		IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_LOS @P_LOS, @P_ERROR_MESSAGE output, @return_value output

			IF @return_value = 0
				EXECUTE dbo.VALIDA_DATOS_LOS @P_LOS, NULL, @P_ERROR_MESSAGE output, @return_value output	

			IF @return_value = 0
				EXECUTE dbo.ACTUALIZA_LOS @P_LOS_ID, @P_LOS

		END 
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'El LOS indicado no existe'
			SET @return_value = -1
		END

		IF @return_value = 0
			COMMIT

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END

;
GO


   
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.05.00', GETDATE(), '13_UPGRADE_v01.05.00');



