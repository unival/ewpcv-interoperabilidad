
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_CONDITIONS_VIEW'
) DROP VIEW dbo.IIA_COOP_CONDITIONS_VIEW;
GO 
CREATE VIEW dbo.IIA_COOP_CONDITIONS_VIEW AS 
  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                            AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                       AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                       AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                      AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)    AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                      AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	RPSC.CONTACT_ROLE                                                                       			   AS RECEIVER_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                   AS RECEIVER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                           AS RECEIVER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                     AS RECEIVER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(RPSCD.ID)                                                                             AS RECEIVER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                           AS RECEIVER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(RPSC.CONTACT_DETAILS_ID)                                                           	   AS RECEIVER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                         AS RECEIVER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                               AS RECEIVER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.MAILING_ADDRESS)                                                        AS RECEIVER_SIGNER_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.MAILING_ADDRESS)                                                              AS RECEIVER_SIGNER_MAILING_ADDR,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                      AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)    AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                      AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	SPSC.CONTACT_ROLE                                                                       			   AS SENDER_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                   AS SENDER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                           AS SENDER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                     AS SENDER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSCD.ID)                                                                            AS SENDER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                           AS SENDER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(SPSC.CONTACT_DETAILS_ID)                                                          	   AS SENDER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                         AS SENDER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                               AS SENDER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.MAILING_ADDRESS)                                                        AS SENDER_SIGNER_M_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.MAILING_ADDRESS)                                                              AS SENDER_SIGNER_MAILING_ADDR,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN dbo.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN dbo.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
GO

	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_COND_CONTACTS_VIEW'
) DROP VIEW dbo.IIA_COOP_COND_CONTACTS_VIEW;
GO 
CREATE VIEW IIA_COOP_COND_CONTACTS_VIEW AS SELECT
	C.ID                                                                                                   AS CONTACT_ID,
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
    P.INSTITUTION_ID                                                                                       AS INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(P.INSTITUTION_ID)                                                           AS INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM EWPCV_ORGANIZATION_UNIT WHERE ID = P.ORGANIZATION_UNIT_ID)         AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(P.ORGANIZATION_UNIT_ID)                                                           AS OUNIT_NAMES,
	PE.FIRST_NAMES                                                                                      AS CONTACT_NAME,
	PE.LAST_NAME                                                                                        AS LAST_NAME,
	PE.BIRTH_DATE                                                                                       AS BIRTH_DATE,
	PE.GENDER                                                                                           AS GENDER,
	PE.COUNTRY_CODE                                                                                     AS CITIZENSHIP,
	C.CONTACT_ROLE                                                                       			   AS CONTACT_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(C.ID)                                                                       AS CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(C.ID)	                                                               AS CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(CD.ID)                                                                         AS CONTACT_URLS,
	dbo.EXTRAE_EMAILS(CD.ID)                                                                                AS EMAILS,
	dbo.EXTRAE_TELEFONO(C.CONTACT_DETAILS_ID)                                                               AS PHONES,
	dbo.EXTRAE_FAX(C.CONTACT_DETAILS_ID)                                                               	   AS FAXES,
	dbo.EXTRAE_ADDRESS_LINES(CD.STREET_ADDRESS)                                                             AS ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(CD.STREET_ADDRESS)                                                                   AS ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(CD.MAILING_ADDRESS)                                                            AS M_ADD_LINES,
	dbo.EXTRAE_ADDRESS(CD.MAILING_ADDRESS)                                                                  AS MAILING_ADDR,
	FA.POSTAL_CODE                                                                                     AS POSTAL_CODE,
	FA.LOCALITY                                                                                        AS LOCALITY,
	FA.REGION                                                                                          AS REGION,	
	FA.COUNTRY                                                                                         AS COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
INNER JOIN EWPCV_IIA_PARTNER P
    ON (CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID)
INNER JOIN EWPCV_IIA_PARTNER_CONTACTS PC
    ON P.ID = PC.IIA_PARTNER_ID
INNER JOIN EWPCV_CONTACT C
    ON PC.CONTACTS_ID = C.ID
LEFT JOIN EWPCV_PERSON PE 
    ON C.PERSON_ID = PE.ID
LEFT JOIN EWPCV_CONTACT_DETAILS CD 
    ON C.CONTACT_DETAILS_ID = CD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS FA 
    ON CD.STREET_ADDRESS = FA.ID ;
GO

/*
    ***********************************
     FIN MODIFICACIONES SOBRE VISTAS
    ***********************************
*/


/*
	**************************************
	 INICIO MODIFICACIONES SOBRE FACTSHEET 
	**************************************
*/
/*
	Actualiza toda la informacion referente al factsheet de una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.ACTUALIZA_FACTSHEET_INSTITUTION;
GO 	
CREATE PROCEDURE  dbo.ACTUALIZA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET_ID uniqueidentifier, @P_FACTSHEET xml) as
BEGIN

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @INSTITUTION_NAME_TEXT xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@INSTITUTION_NAME_TEXT = T.c.query('institution_name/text'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c) 

	UPDATE dbo.EWPCV_INSTITUTION SET ABBREVIATION = @ABREVIATION, LOGO_URL = @LOGO_URL WHERE ID = @P_INSTITUTION_ID
	
	IF @INSTITUTION_NAME_TEXT IS NOT NULL AND @INSTITUTION_NAME_TEXT.exist('*') = 1
	BEGIN
		EXECUTE dbo.BORRA_NOMBRES @P_INSTITUTION_ID
		EXECUTE dbo.INSERTA_INSTITUTION_NAMES @P_INSTITUTION_ID, @INSTITUTION_NAME
	END;
	
	EXECUTE dbo.ACTUALIZA_FACTSHEET @P_FACTSHEET_ID, @FACTSHEET, @APPLICATION_INFO
	
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @P_INSTITUTION_ID

	EXECUTE BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID

	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @P_INSTITUTION_ID
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo

	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @P_INSTITUTION_ID
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @P_INSTITUTION_ID
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO
/*
	**************************************
	 FIN MODIFICACIONES SOBRE FACTSHEET 
	**************************************
*/
/*
	****************************************
	INICIO MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/
/*
	Valida que el iias y la coop condition indicadas existan
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY_IIA'
) DROP PROCEDURE dbo.VALIDA_MOBILITY_IIA;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY_IIA(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	
	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	
	DECLARE @v_iia_id varchar(255)
	DECLARE @v_cc_id varchar(255)
	DECLARE @v_count integer

	SELECT 
		@v_iia_id = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@v_cc_id = T.c.value('(cooperation_condition_id)[1]', 'varchar(255)')
	FROM @P_MOBILITY_XML.nodes('mobility') T(c)
	
	IF @v_iia_id IS NOT NULL 
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE UPPER(ID) = UPPER(@v_iia_id);
		
		IF @v_count = 0 
		BEGIN 
			SET @return_value = -1;
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
			El acuerdo interistitucional indicado no existe en el sistema.'
		END 
		ELSE IF @v_cc_id IS NOT NULL 
		BEGIN 
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_COOPERATION_CONDITION WHERE UPPER(ID) = UPPER(@v_cc_id) AND UPPER(IIA_ID) = UPPER(@v_iia_id);
			IF @v_count = 0 
			BEGIN 
				SET @return_value = -1;
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				La condicion de cooperacion indicada no existe en el sistema o no está asociada al acuerdo interistitucional indicado.'
			END 
		END;
	END;
	
END
;
GO

/*
	****************************************
	FIN MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.03.01', GETDATE(), '10_UPGRADE_v01.03.01');
