CREATE VIEW dbo.NOTIFICATIONS_VIEW AS SELECT 
	ID 				   	AS ID,
	CHANGED_ELEMENT_IDS	AS CHANGED_ELEMENT_ID,
	HEI_ID 				AS HEI_ID,
	NOTIFICATION_DATE 	AS NOTIFICATION_DATE,
	"TYPE" 				AS "TYPE",
	CNR_TYPE 			AS CNR_TYPE,
	RETRIES 			AS RETRIES,
	PROCESSING 			AS PROCESSING,
	OWNER_HEI 			AS OWNER_HEI,
	PROCESSING_DATE 	AS PROCESSING_DATE
FROM dbo.EWPCV_NOTIFICATION;
GO

CREATE VIEW dbo.UPDATE_REQUESTS_VIEW AS SELECT 
ID                   AS ID,
VERSION              AS VERSION,
SENDING_HEI_ID       AS SENDING_HEI_ID,
"TYPE"               AS "TYPE",
UPDATE_REQUEST_DATE  AS UPDATE_REQUEST_DATE,
IS_PROCESSING        AS IS_PROCESSING,
MOBILITY_TYPE        AS MOBILITY_TYPE,
UPDATE_INFORMATION   AS UPDATE_INFORMATION,
RETRIES              AS RETRIES,
PROCESSING_DATE      AS PROCESSING_DATE
FROM dbo.EWPCV_MOBILITY_UPDATE_REQUEST;
GO

ALTER VIEW dbo.LA_COMPONENTS_VIEW AS 
WITH COMPONENTS AS (
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, RC.RECOGNIZED_LA_COMPONENT_ID as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_RECOGNIZED_LA_COMPONENT RC ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, VC.VIRTUAL_LA_COMPONENT_ID as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_VIRTUAL_LA_COMPONENT VC ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, BC.BLENDED_LA_COMPONENT_ID as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_BLENDED_LA_COMPONENT BC ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, DC.DOCTORAL_LA_COMPONENTS_ID as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA LEFT JOIN EWPCV_DOCTORAL_LA_COMPONENT DC ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
)
SELECT 
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	dbo.EXTRAE_CREDITS(C.id)                            AS CREDITS,
	dbo.EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	AY.START_YEAR + '|:|' +  AY.END_YEAR             AS ACADEMIC_YEAR,
	C.STATUS                                        AS STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_CODE                                      AS LOS_CODE,
	LOS.INSTITUTION_ID                              AS INSTITUTION_ID,
	dbo.EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	O.ORGANIZATION_UNIT_CODE	                    AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(O.ID)                      AS RECEIVING_OUNIT_NAMES,
	dbo.EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	dbo.EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	dbo.EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION
FROM COMPONENTS LA
INNER JOIN EWPCV_LA_COMPONENT C
    ON C.ID = LA.STUDIED_LA_COMPONENT_ID
    OR C.ID = LA.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = LA.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = LA.BLENDED_LA_COMPONENT_ID
    OR C.ID = LA.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID    
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID    
LEFT JOIN EWPCV_ORGANIZATION_UNIT O 
    ON LOS.ORGANIZATION_UNIT_ID = O.ID
LEFT JOIN EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID;
GO

ALTER VIEW dbo.IIA_COOP_CONDITIONS_VIEW AS 
  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                                AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                           AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                          AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)        AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                          AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                       AS RECEIVER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                               AS RECEIVER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                         AS RECEIVER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(RPSC.ID)                                                                                 AS RECEIVER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                               AS RECEIVER_SIGNER_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                             AS RECEIVER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                                   AS RECEIVER_SIGNER_ADDRESS,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                          AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)        AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                          AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                       AS SENDER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                               AS SENDER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                         AS SENDER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSC.ID)                                                                                 AS SENDER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                               AS SENDER_SIGNER_PHONES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                             AS SENDER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                                   AS SENDER_SIGNER_ADDRESS,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN dbo.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN dbo.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
GO

ALTER PROCEDURE  dbo.INSERTA_OUNIT_NAMES(@P_OUNIT_ID uniqueidentifier, @P_ORGANIZATION_UNIT_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id uniqueidentifier
	DECLARE @v_count integer

	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	
	IF @P_ORGANIZATION_UNIT_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_ORGANIZATION_UNIT_NAMES.nodes('/organization_unit_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')

			SELECT @v_lang_item_id = ID FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_ORGANIZATION_UNIT_NAME ona on lait.ID = ona.NAME_ID
				WHERE lait.text=@text and lait.lang=@lang and ona.ORGANIZATION_UNIT_ID = @P_OUNIT_ID

			if @v_lang_item_id is null
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				SET @v_count = 1
			END

			-- Comprobar si el texto ya está asignado a la institución
			SELECT @v_count=count(1) FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID=@v_lang_item_id and ORGANIZATION_UNIT_ID=@P_OUNIT_ID

			IF @v_lang_item_id is not null AND @v_count = 0 
			BEGIN
				INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES(@v_lang_item_id, @P_OUNIT_ID)
			END

			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;
GO

	/*
		Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
		Devuelve el id de la ounit
*/
ALTER PROCEDURE  dbo.INSERTA_INST_OUNIT(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_inst_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @v_ou_code VARCHAR(255)

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@INSTITUTION_NAME = T.c.query('(institution_name)'),
		@v_ou_code = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_INSTITUTION.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, null, null, null, @INSTITUTION_NAME, @v_inst_id OUTPUT
	IF @v_ou_code IS NOT NULL 
	BEGIN
		EXECUTE dbo.INSERTA_OUNIT @P_INSTITUTION, @v_ou_id OUTPUT
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @v_ou_id
		IF @v_count = 0
			INSERT INTO dbo.EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (@v_inst_id, @v_ou_id)

		SET @return_value = @v_ou_id
	END 
END
;
GO

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_IIA' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" minOccurs="0"/>
				<xs:element name="end_date" type="xs:date" minOccurs="0"/>
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded" >
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="0" type="xs:integer" />
													<xs:element name="unit" minOccurs="0">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.02.01', GETDATE(), '08_UPGRADE_v01.02.01');