/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/


    create table dbo.EWPCV_STATS_ILA (
               ID varchar(255) not null,
                VERSION integer,
    			RECEIVING_INSTITUTION varchar(255),
                START_YEAR varchar(255),
                END_YEAR varchar(255),
                TOTAL integer,
                SOME_VERSION_APPROVED integer,
                LAST_APPROVED integer,
                LAST_REJECTED integer,
                LAST_PENDING integer,
    			APPROVAL_UNMODIFIED integer,
    			APPROVAL_MODIFIED integer,
        		DUMP_DATE date,
                primary key (ID)
            );

    INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.statisticsILA.audit', 'true');
go
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/



/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v02.01.00', GETDATE(), '34_ILA_STATS');
