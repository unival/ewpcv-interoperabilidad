/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

     create table dbo.EWPCV_STATS_IIA (
           ID varchar(255 ) not null,
            VERSION integer,
            FETCHABLE integer,
            LOCAL_UNAPPR_PARTNER_APPR integer,
            LOCAL_APPR_PARTNER_UNAPPR integer,
            BOTH_APPROVED integer,
    		DUMP_DATE date,
            primary key (ID)
        );

     INSERT INTO dbo.EWPCV_AUDIT_CONFIG (NAME, VALUE) VALUES ('ewp.statisticsIia.audit', 'true');
GO

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/


/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/
/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v02.01.00', GETDATE(), '35_Iia_STATS');