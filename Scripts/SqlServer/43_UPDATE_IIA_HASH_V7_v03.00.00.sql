/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
--añadimos una nueva columna para guardar los hashes v6 que tenemos guardados y que serán reemplazados para no perderelos
ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD LOCAL_HASH_V6 VARCHAR(255);
ALTER TABLE ewp.dbo.EWPCV_IIA_APPROVALS ADD REMOTE_HASH_V6 VARCHAR(255);

--Solo por si acaso vamos a guardar los hashes que tenemos de los iias en una nueva tabla iia_hash_backup
SELECT *  INTO ewp.dbo.EWPCV_IIA_HASH_BACKUP FROM ewp.dbo.EWPCV_IIA;
GO

--actualizamos los hashes v6 de las aprobaciones
UPDATE ewp.dbo.EWPCV_IIA_APPROVALS SET LOCAL_HASH_V6 = LOCAL_HASH, REMOTE_HASH_V6 = REMOTE_HASH;

--ahora alineamos los hashes de las aprobadas con la v7
UPDATE ewp.dbo.EWPCV_IIA_APPROVALS SET  LOCAL_HASH = LOCAL_HASH_V7, REMOTE_HASH = REMOTE_HASH_V7;

--teoricamente la informacion debería estar alineada por lo que deberiamos poder coger los hashes de la tabla de aprobaciones y actualizar los hashes de los IIAs
--ahora actualizamos los hashes de los IIAs locales
UPDATE ewp.dbo.EWPCV_IIA SET APPROVAL_COP_COND_HASH = app.LOCAL_HASH_V7, REMOTE_COP_COND_HASH = app.LOCAL_HASH_V7 FROM ewp.dbo.EWPCV_IIA iia
	JOIN ewp.dbo.EWPCV_IIA_APPROVALS app ON iia.ID = app.LOCAL_IIA_ID WHERE iia.APPROVAL_COP_COND_HASH = app.LOCAL_HASH;

--ahora actualizamos los hashes de los IIAs remotos
UPDATE ewp.dbo.EWPCV_IIA SET APPROVAL_COP_COND_HASH = app.REMOTE_HASH_V7, REMOTE_COP_COND_HASH = app.REMOTE_HASH_V7 FROM ewp.dbo.EWPCV_IIA iia
	JOIN ewp.dbo.EWPCV_IIA_APPROVALS app ON iia.REMOTE_IIA_ID = app.REMOTE_IIA_ID WHERE iia.APPROVAL_COP_COND_HASH = app.REMOTE_HASH AND iia.IS_REMOTE = 1;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v03.00.00', GETDATE(), '43_UPDATE_IIA_HASH_V7_v03.00.00');
