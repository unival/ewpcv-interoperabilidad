/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/
	--     añadimos una columna a la tabla de iias para guardar el id del pdf
	ALTER TABLE dbo.EWPCV_IIA ADD PDF_ID VARCHAR(255);
	GO

    --     tenemos que llevarnos todos los pdfs que tenemos en la tabla de iias a la nueva tabla EWPCV_FILES antes de cambiar la columna
    DECLARE @v_iia_id VARCHAR(255);
    DECLARE @v_pdf varchar(MAX);
    DECLARE @v_file_id VARCHAR(255);
    DECLARE @v_first_partner_id VARCHAR(255);
    DECLARE @v_schac VARCHAR(255);
    DECLARE c_iias CURSOR FOR
        SELECT ID, PDF, FIRST_PARTNER_ID FROM dbo.EWPCV_IIA WHERE PDF IS NOT NULL AND IS_REMOTE = 0;
    OPEN c_iias;
    FETCH NEXT FROM c_iias INTO @v_iia_id, @v_pdf, @v_first_partner_id;
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @v_file_id = NEWID();
        SELECT @v_schac = INSTITUTION_ID FROM dbo.EWPCV_IIA_PARTNER WHERE ID = @v_first_partner_id;
        INSERT INTO dbo.EWPCV_FILES (ID, FILE_ID, FILE_CONTENT, SCHAC, MIME_TYPE) VALUES (NEWID(), @v_file_id, @v_pdf, @v_schac, 'application/pdf');
        UPDATE dbo.EWPCV_IIA SET PDF_ID = @v_file_id WHERE ID = @v_iia_id;
        FETCH NEXT FROM c_iias INTO @v_iia_id, @v_pdf, @v_first_partner_id;
    END
    CLOSE c_iias;
    DEALLOCATE c_iias;

    GO

    -- ahora que ya tenemos todos los pdfs en la nueva tabla, y referenciados podemos eliminar la columna de la tabla de iias
	ALTER TABLE dbo.EWPCV_IIA DROP COLUMN PDF;

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/


/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/
/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/
INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v03.00.00', GETDATE(), '44_INTEGRACION_IIAs_FILE_API_v03.00.00');