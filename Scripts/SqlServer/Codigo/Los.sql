/*
***************************************
******** Objetos Los ******************
***************************************
*/




/* VALIDAR estructura datos LOS */
/*

	VALIDACION VIA XSD

*/
-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA LOS

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LOS' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LOS;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LOS
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="losTypeType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="3"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="los">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="eqf_level" type="xs:integer" />
				<xs:element name="institution_id" type="notEmptyStringType" minOccurs="0" />
				<xs:element name="iscedf" type="xs:string" />
				<xs:element name="los_code" type="xs:string" />
				<xs:element name="los_name_list" type="languageItemListType" minOccurs="0" />
				<xs:element name="los_description_list" type="languageItemListType" />
				<xs:element name="los_url_list" type="languageItemListType" />				
				<xs:element name="organization_unit_id" type="xs:string" minOccurs="0"/>
				<xs:element name="subject_area" type="xs:string" />
				<xs:element name="los_type" type="losTypeType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_LOS'
) DROP PROCEDURE dbo.VALIDA_LOS;
GO 
CREATE PROCEDURE dbo.VALIDA_LOS(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_LOS)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO



 
/*
    *************************************
     FUNCIONES
    *************************************
*/

  /* Devuelve el codigo del tipo del LOS */
  
  
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'GET_LOS_TYPE'
) DROP FUNCTION dbo.GET_LOS_TYPE;
GO
CREATE FUNCTION [dbo].[GET_LOS_TYPE](@P_TYPE_ID integer) RETURNS varchar(255)
BEGIN
	DECLARE @return_value varchar(255)

	IF @P_TYPE_ID = 0 
		set @return_value = 'CR'
	IF @P_TYPE_ID = 1 
		set @return_value = 'CLS'
	IF @P_TYPE_ID = 2 
		set @return_value = 'MOD'
	IF @P_TYPE_ID = 3 
		set @return_value = 'DEP'

	return @return_value
END
;

GO



   /* Comprueba si existe el LOS */


IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXISTE_LOS'
) DROP FUNCTION dbo.EXISTE_LOS;
GO
CREATE FUNCTION [dbo].[EXISTE_LOS](@P_LOS_ID varchar(255),  @P_LOS_PARENT_ID varchar(255)) RETURNS integer
BEGIN
	DECLARE @v_count integer
	DECLARE @return_value integer
	SET @return_value = 0

	IF @P_LOS_PARENT_ID is null
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
	ELSE
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID

	IF @v_count = 0 
		set @return_value = -1

	return @return_value
END
;

GO


 /*	Valida si el padre puede tener un hijo del tipo informado	*/

 
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'VALIDA_PADRE'
) DROP FUNCTION dbo.VALIDA_PADRE;
GO
CREATE FUNCTION dbo.VALIDA_PADRE(@P_PARENT_TYPE_ID integer, @P_CHILD_TYPE_ID integer) RETURNS integer AS
BEGIN
  	DECLARE @return_value integer
	DECLARE @v_dep_allowed_childs_type varchar(255)
	DECLARE @v_mod_allowed_childs_type varchar(255)
	DECLARE @v_cr_allowed_childs_type varchar(255)
	SET @return_value = -1
	SET @v_dep_allowed_childs_type = '023'
	SET @v_mod_allowed_childs_type = '0'
	SET @v_cr_allowed_childs_type = '1'

	IF @P_PARENT_TYPE_ID = 0 AND CHARINDEX(LTRIM(RTRIM(STR(@P_CHILD_TYPE_ID))), @v_cr_allowed_childs_type) <> 0
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 1 AND @P_CHILD_TYPE_ID is null
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 2 AND CHARINDEX(LTRIM(RTRIM(STR(@P_CHILD_TYPE_ID))), @v_mod_allowed_childs_type) <> 0
		SET @return_value = 0

	IF @P_PARENT_TYPE_ID = 3 AND CHARINDEX(LTRIM(RTRIM(STR(@P_CHILD_TYPE_ID))), @v_dep_allowed_childs_type) <> 0
		SET @return_value = 0

	return @return_value

END 
;
GO
   
   
   
 /* Valida si el tipo introducido del LOS es correcto */
 

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'VALIDA_LOS_TYPE'
) DROP FUNCTION dbo.VALIDA_LOS_TYPE;
GO
CREATE FUNCTION dbo.VALIDA_LOS_TYPE(@P_TYPE varchar(255)) RETURNS integer AS
BEGIN
  	DECLARE @return_value integer
	DECLARE @v_types varchar(255)
	SET @return_value = 0
	SET @v_types = '0123'

	IF @P_TYPE is not null AND CHARINDEX(@P_TYPE, @v_types) = 0 
		SET @return_value = -1

	return @return_value

END 
;
GO





  
/*
    *************************************
    PROCEDURES
    *************************************
*/


/* Cambia de padre a un LOS del sistema.
	    Admite el id del LOS
	    Admite el id del antiguo padre del LOS
	    Admite el id del nuevo padre del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/
 
      
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CHANGE_PARENT'
) DROP PROCEDURE dbo.CHANGE_PARENT;
GO
CREATE PROCEDURE [dbo].[CHANGE_PARENT](@P_LOS_ID varchar(255), @P_LOS_OLD_PARENT_ID varchar(255), @P_LOS_NEW_PARENT_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_existe_los integer
	DECLARE @v_type_parent integer
	DECLARE @v_type integer

	SET @v_existe_los = 0
	SET @v_type_parent = 0
	SET @v_type = 0
	SET @return_value = 0

	BEGIN TRY
		BEGIN TRANSACTION

		IF dbo.EXISTE_LOS(@P_LOS_ID, @P_LOS_OLD_PARENT_ID) = 0
		BEGIN
			SELECT @v_type = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID

			IF @P_LOS_NEW_PARENT_ID is not null
			BEGIN
				IF dbo.EXISTE_LOS(@P_LOS_NEW_PARENT_ID, NULL) = 0
				BEGIN
					SELECT @v_type_parent = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_NEW_PARENT_ID

					IF @v_type_parent is not null and dbo.VALIDA_PADRE(@v_type_parent, @v_type) < 0 
					BEGIN
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El padre indicado no puede contener hijos con el tipo introducido.')
						SET @return_value = -1
					END 

					IF @return_value = 0 
					BEGIN
					    IF @P_LOS_OLD_PARENT_ID is not null
						    UPDATE dbo.EWPCV_LOS_LOS SET LOS_ID = @P_LOS_NEW_PARENT_ID WHERE LOS_ID = @P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID
						ELSE
						    INSERT INTO dbo.EWPCV_LOS_LOS VALUES (@P_LOS_NEW_PARENT_ID, @P_LOS_ID)
						UPDATE dbo.EWPCV_LOS SET TOP_LEVEL_PARENT = 0 WHERE ID = @P_LOS_ID
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_LOS_ID, 8, 1)
					END 

				END
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - No existe el LOS indicado como nuevo padre en el sistema.')
					SET @return_value = -1
				END 
			END
			ELSE
			BEGIN
				IF @v_type = 1
				BEGIN
					DELETE FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_OLD_PARENT_ID AND OTHER_LOS_ID = @P_LOS_ID
					UPDATE dbo.EWPCV_LOS SET TOP_LEVEL_PARENT = 1 WHERE ID = @P_LOS_ID
				END 
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - Se ha de indicar el id del nuevo padre del LOS')
					SET @return_value = -1
				END
			END 
		END
		ELSE
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - No existe el LOS indicado en el sistema')
			SET @return_value = -1
		END 
	
		IF @return_value = 0
			COMMIT TRANSACTION
		ELSE
			ROLLBACK TRANSACTION

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.CHANGE_PARENT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END
;
GO



 /* Valida las dependencias con el LOS */


      
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DEPENDENCIAS'
) DROP PROCEDURE dbo.VALIDA_DEPENDENCIAS;
GO
CREATE PROCEDURE [dbo].[VALIDA_DEPENDENCIAS](@P_LOS_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_count integer
	SET @v_count = 0

	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_ID

	IF @v_count > 0
	BEGIN
        set @return_value = -1
        set @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El LOS indicado tiene almenos un hijo.')
    END

	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LA_COMPONENT LA 
		WHERE LA.LOS_ID = @P_LOS_ID 
		  OR LA.LOS_ID IN (
			SELECT LL.OTHER_LOS_ID FROM dbo.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = @P_LOS_ID
		)

	IF @v_count > 0
	BEGIN
		set @return_value = -1
		set @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un Learning Agreement.')
	END

	IF @return_value = 0 
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS_LOI LO
		  WHERE LO.LOS_ID = @P_LOS_ID 
			OR LO.LOS_ID IN (
			  SELECT LL.OTHER_LOS_ID FROM dbo.EWPCV_LOS_LOS LL WHERE LL.LOS_ID = @P_LOS_ID
		  )
		IF @v_count > 0 
		BEGIN
			set @return_value = -1
			set @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El LOS indicado o algún LOS hijo está asociado al menos a un LOI.')
		END
	END 

END
;
GO





 /* Valida la calidad del dato en el objeto LOS  */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LOS'
) DROP PROCEDURE dbo.VALIDA_DATOS_LOS;
GO
CREATE PROCEDURE dbo.VALIDA_DATOS_LOS(@P_LOS XML, @P_LOS_PARENT_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS

BEGIN
    DECLARE @v_count_ins integer
    DECLARE @v_count_ounit integer
    DECLARE @v_type_parent integer
	SET @v_count_ins = 0
	SET @v_count_ounit = 0
	SET @v_type_parent = 0

	DECLARE @LOS_TYPE varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID varchar(255)

	SELECT
		@LOS_TYPE = T.c.value('(los_type)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'varchar(255)')
	FROM @P_LOS.nodes('los') T(c)


	IF dbo.VALIDA_LOS_TYPE(@LOS_TYPE) < 0
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El tipo introducido no es válido. 0 - "Course", 1 - "Class", 2 - "Module", 3 - "Degree Programme".')
		SET @return_value = -1
	END

	IF @INSTITUTION_ID is not null
	BEGIN
		SELECT @v_count_ins = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE LOWER(SCHAC)= LOWER(@INSTITUTION_ID)

		IF @v_count_ins = 0
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - INSTITUTION_ID: No existe la institución introducida en el sistema.')
			SET @return_value = -1
		END
		ELSE
		BEGIN
			IF @ORGANIZATION_UNIT_ID is not null 
			BEGIN
				 SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT OU 
				  INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON IOU.ORGANIZATION_UNITS_ID = OU.ID 
				  INNER JOIN dbo.EWPCV_INSTITUTION EI ON EI.ID = IOU.INSTITUTION_ID 
				  WHERE OU.ID = @ORGANIZATION_UNIT_ID
				  AND LOWER(EI.INSTITUTION_ID) = LOWER(@INSTITUTION_ID)

				IF @v_count_ounit = 0
				BEGIN
					SET @return_value = -1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No existe la organization unit introducida en el sistema.')
				END
			END 
		END
	END

	IF @ORGANIZATION_UNIT_ID is not null 
	BEGIN
		SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT OU
			WHERE OU.ID = @ORGANIZATION_UNIT_ID
  
        IF @v_count_ounit = 0 
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - ORGANIZATION_UNIT_ID: No existe la organization unit introducida en el sistema.')
        END
	END

	IF @return_value = 0 AND @P_LOS_PARENT_ID IS NOT NULL
	BEGIN
		SELECT @v_type_parent = [TYPE] FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_PARENT_ID
        IF @v_type_parent IS NOT NULL AND dbo.VALIDA_PADRE(@v_type_parent, @LOS_TYPE) < 0 
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' - TYPE: El padre indicado no puede contener hijos con el tipo introducido.')
        END
	END
END
;
GO




-----------------------------------
-- Procedimientos para borrado
-----------------------------------

   /* Borra el listado de nombres del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_NAMES'
) DROP PROCEDURE dbo.BORRA_LOS_NAMES;
GO
CREATE PROCEDURE dbo.BORRA_LOS_NAMES(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @NAME_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT NAME_ID
		  FROM dbo.EWPCV_LOS_NAME
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO

   /* Borra el listado de urls del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_URLS'
) DROP PROCEDURE dbo.BORRA_LOS_URLS;
GO
CREATE PROCEDURE dbo.BORRA_LOS_URLS(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @URL_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT URL_ID
		  FROM dbo.EWPCV_LOS_URLS
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @URL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_URLS WHERE URL_ID = @URL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @URL_ID
			FETCH NEXT FROM cur INTO @URL_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;

GO




   /* Borra el listado de descripciones del LOS */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS_DESCRIPTIONS'
) DROP PROCEDURE dbo.BORRA_LOS_DESCRIPTIONS;
GO
CREATE PROCEDURE dbo.BORRA_LOS_DESCRIPTIONS(@P_LOS_ID varchar(255)) AS
BEGIN
	DECLARE @DESCRIPTION_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT DESCRIPTION_ID
		  FROM dbo.EWPCV_LOS_DESCRIPTION
		  WHERE LOS_ID = @P_LOS_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @DESCRIPTION_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LOS_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
			FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
END
;
GO



   /* Elimina el LOS de la base de datos */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOS'
) DROP PROCEDURE dbo.BORRA_LOS;
GO
CREATE PROCEDURE dbo.BORRA_LOS(@P_LOS_ID varchar(255)) AS
BEGIN
  
	IF @P_LOS_ID is not null 
	BEGIN
		EXECUTE dbo.BORRA_LOS_NAMES @P_LOS_ID
		EXECUTE dbo.BORRA_LOS_DESCRIPTIONS @P_LOS_ID
		EXECUTE dbo.BORRA_LOS_URLS @P_LOS_ID

        DELETE FROM dbo.EWPCV_LOS_LOS WHERE OTHER_LOS_ID = @P_LOS_ID
		DELETE FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
			   
	END
END
;
GO


  
 	/* Elimina un LOS del sistema y sus hijos
	    Admite el id del LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_LOS'
) DROP PROCEDURE dbo.DELETE_LOS;
GO
CREATE PROCEDURE DELETE_LOS(@P_LOS_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS 
BEGIN
	DECLARE @v_count integer
	DECLARE @v_has_childs integer
	DECLARE @valida integer
	SET @return_value = 0


	BEGIN TRY
		BEGIN TRANSACTION
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID
		SELECT @v_has_childs = COUNT(1) FROM dbo.EWPCV_LOS_LOS WHERE LOS_ID = @P_LOS_ID
		IF @v_count > 0
		BEGIN
			EXECUTE dbo.VALIDA_DEPENDENCIAS @P_LOS_ID, @P_ERROR_MESSAGE output, @valida output
			IF @valida < 0
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = CONCAT('No se ha eliminado el LOS indicado.', @P_ERROR_MESSAGE )
			END
			IF @v_has_childs > 0
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = CONCAT('El LOS indicado está asociado con al menos a un LOS.', @P_ERROR_MESSAGE )
			END

			IF @return_value = 0
			BEGIN
				EXECUTE dbo.BORRA_LOS @P_LOS_ID
				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_LOS_ID, 8, 2)
			END
		END
		ELSE
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El LOS indicado no existe'
		END
		
		IF @return_value = 0
		BEGIN
			COMMIT
		END

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO


---------------------------
/* Funciones insert */
---------------------------


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_DESCRIPTIONS'
) DROP PROCEDURE dbo.INSERTA_LOS_DESCRIPTIONS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_DESCRIPTIONS(@P_LOS_ID varchar(255), @P_LOS_DESCRIPTION_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_DESCRIPTION_LIST.nodes('los_description_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_DESCRIPTION (LOS_ID, DESCRIPTION_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO  


  /* Inserta el listado de nombres del LOS */
  
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_NAMES'
) DROP PROCEDURE dbo.INSERTA_LOS_NAMES;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_NAMES(@P_LOS_ID varchar(255), @P_LOS_NAME_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_NAME_LIST.nodes('los_name_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_NAME (LOS_ID, NAME_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


   /* Inserta el listado de urls del LOS */
   
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS_URLS'
) DROP PROCEDURE dbo.INSERTA_LOS_URLS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS_URLS(@P_LOS_ID varchar(255), @P_LOS_URL_LIST xml) AS
BEGIN
	DECLARE @LANGITEM xml
	DECLARE @v_id varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_LOS_URL_LIST.nodes('los_url_list/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_id output
			INSERT INTO dbo.EWPCV_LOS_URLS (LOS_ID, URL_ID) VALUES (@P_LOS_ID, @v_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


----------------------------------
  -- ACTUALIZACIONES
----------------------------------

 /* Actualiza el LOS en la base de datos */
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_LOS'
) DROP PROCEDURE dbo.ACTUALIZA_LOS;
GO
CREATE PROCEDURE dbo.ACTUALIZA_LOS(@P_LOS_ID varchar(255), @P_LOS xml) AS
BEGIN
	DECLARE @EQF_LEVEL varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ISCEDF varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID varchar(255)
	DECLARE @SUBJECT_AREA varchar(255)
	DECLARE @LOS_NAME_LIST xml
	DECLARE @LOS_DESCRIPTION_LIST xml 
	DECLARE @LOS_URL_LIST xml
	   
	SELECT
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ISCEDF = T.c.value('(iscedf)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'varchar(255)'),
		@SUBJECT_AREA = T.c.value('(subject_area)[1]', 'varchar(255)'),
		@LOS_NAME_LIST = T.c.query('los_name_list'),
		@LOS_DESCRIPTION_LIST = T.c.query('los_description_list'),
		@LOS_URL_LIST = T.c.query('los_url_list')
	FROM @P_LOS.nodes('los') T(c)

	EXECUTE dbo.BORRA_LOS_NAMES @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_NAMES  @P_LOS_ID, @LOS_NAME_LIST

	EXECUTE dbo.BORRA_LOS_DESCRIPTIONS @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_DESCRIPTIONS  @P_LOS_ID, @LOS_DESCRIPTION_LIST

	EXECUTE dbo.BORRA_LOS_URLS @P_LOS_ID
	EXECUTE dbo.INSERTA_LOS_URLS  @P_LOS_ID, @LOS_URL_LIST


	UPDATE dbo.EWPCV_LOS SET EQF_LEVEL = @EQF_LEVEL, INSTITUTION_ID = @INSTITUTION_ID, ISCEDF = @ISCEDF, 
		LOS_CODE = @LOS_CODE, ORGANIZATION_UNIT_ID = @ORGANIZATION_UNIT_ID,
		SUBJECT_AREA = @SUBJECT_AREA WHERE ID = @P_LOS_ID
		
END
;
GO


  /* Inserta el LOS en la tabla */
  
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOS'
) DROP PROCEDURE dbo.INSERTA_LOS;
GO
CREATE PROCEDURE dbo.INSERTA_LOS(@P_LOS xml, @P_LOS_PARENT_ID varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_los_type varchar(255)
	DECLARE @v_top_level_parent integer

	DECLARE @EQF_LEVEL varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ISCEDF varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @ORGANIZATION_UNIT_ID varchar(255)
	DECLARE @SUBJECT_AREA varchar(255)
	DECLARE @LOS_TYPE varchar(255)
	DECLARE @LOS_NAME_LIST xml
	DECLARE @LOS_DESCRIPTION_LIST xml 
	DECLARE @LOS_URL_LIST xml

	
	SELECT
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ISCEDF = T.c.value('(iscedf)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_ID = T.c.value('(organization_unit_id)[1]', 'varchar(255)'),
		@SUBJECT_AREA = T.c.value('(subject_area)[1]', 'varchar(255)'),
		@LOS_TYPE = T.c.value('(los_type)[1]', 'integer'),
		@LOS_NAME_LIST = T.c.query('los_name_list'),
		@LOS_DESCRIPTION_LIST = T.c.query('los_description_list'),
		@LOS_URL_LIST = T.c.query('los_url_list')
	FROM @P_LOS.nodes('los') T(c)

	SET @v_los_type = dbo.GET_LOS_TYPE(@LOS_TYPE)
	SET @v_id = CONCAT(@v_los_type + '/' , NEWID())

	IF @P_LOS_PARENT_ID is null
		SET @v_top_level_parent = 1
	ELSE 
		SET @v_top_level_parent = 0

	INSERT INTO dbo.EWPCV_LOS 
    (ID, EQF_LEVEL, INSTITUTION_ID, 
      ISCEDF, LOS_CODE, ORGANIZATION_UNIT_ID, SUBJECT_AREA, TOP_LEVEL_PARENT, "TYPE")
    VALUES
    (@v_id, @EQF_LEVEL, @INSTITUTION_ID, 
      @ISCEDF, @LOS_CODE, @ORGANIZATION_UNIT_ID, @SUBJECT_AREA, @v_top_level_parent, @LOS_TYPE
    )

	EXECUTE dbo.INSERTA_LOS_NAMES  @v_id, @LOS_NAME_LIST
	EXECUTE dbo.INSERTA_LOS_DESCRIPTIONS  @v_id, @LOS_DESCRIPTION_LIST
	EXECUTE dbo.INSERTA_LOS_URLS  @v_id, @LOS_URL_LIST

	IF @P_LOS_PARENT_ID IS NOT NULL
		INSERT INTO dbo.EWPCV_LOS_LOS (LOS_ID, OTHER_LOS_ID) VALUES (@P_LOS_PARENT_ID, @v_id)
 
	SET @return_value = @v_id

END
;
GO




	/* Inserta un LOS en el sistema
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un id del padre del los
		Admite un parametro de salida con el identificador del LOS en el sistema
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_LOS'
) DROP PROCEDURE dbo.INSERT_LOS;
GO
CREATE PROCEDURE dbo.INSERT_LOS(@P_LOS xml, @P_LOS_PARENT_ID varchar(255), @P_LOS_ID varchar(255) output, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS
BEGIN
	DECLARE @v_c_id varchar(255)
	DECLARE @v_type integer
	DECLARE @v_allowed_parent int

	BEGIN TRY
		BEGIN TRANSACTION
		
		EXECUTE dbo.VALIDA_LOS @P_LOS, @P_ERROR_MESSAGE output, @return_value output

		IF @return_value = 0 
			EXECUTE dbo.VALIDA_DATOS_LOS @P_LOS, @P_LOS_PARENT_ID, @P_ERROR_MESSAGE output, @return_value output	

		IF @return_value = 0 
			EXECUTE dbo.INSERTA_LOS @P_LOS, @P_LOS_PARENT_ID, @P_LOS_ID output

        IF @return_value = 0
		    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_LOS_ID, 8, 0)

		IF @return_value = 0
			COMMIT TRANSACTION
		ELSE 
			ROLLBACK TRANSACTION

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END

;
GO
  
  

	/* Actualiza un LOS en el sistema
	    Admite el id del LOS
		Admite un objeto de tipo LOS con toda la informacion deL LOS
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_LOS'
) DROP PROCEDURE dbo.UPDATE_LOS;
GO
CREATE PROCEDURE dbo.UPDATE_LOS(@P_LOS_ID varchar(255), @P_LOS xml, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @LOS_TYPE integer

	BEGIN TRY
		BEGIN TRANSACTION

		SELECT
			@LOS_TYPE = T.c.value('(los_type)[1]', 'integer')
		FROM @P_LOS.nodes('los') T(c)

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE ID = @P_LOS_ID AND [TYPE] = @LOS_TYPE
	
		IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_LOS @P_LOS, @P_ERROR_MESSAGE output, @return_value output

			IF @return_value = 0
				EXECUTE dbo.VALIDA_DATOS_LOS @P_LOS, NULL, @P_ERROR_MESSAGE output, @return_value output	

			IF @return_value = 0
				EXECUTE dbo.ACTUALIZA_LOS @P_LOS_ID, @P_LOS

            IF @return_value = 0
			    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_LOS_ID, 8, 1)

		END 
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'El LOS indicado no existe'
			SET @return_value = -1
		END

		IF @return_value = 0
			COMMIT

	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LOS'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END

;
GO


   