---------------------------------------------
-- FUNCIONES PARA LAS VISTAS
---------------------------------------------  

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_EMAILS'
) DROP FUNCTION dbo.EXTRAE_EMAILS;
GO
CREATE FUNCTION dbo.EXTRAE_EMAILS(@P_CONTACT_DETAILS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
select @return_value = stuff( (select concat('|;|' , EMAIL)
               FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL 
			   WHERE CONTACT_DETAILS_ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_TELEFONO'
) DROP FUNCTION dbo.EXTRAE_TELEFONO;
GO
CREATE FUNCTION dbo.EXTRAE_TELEFONO(@P_CONTACT_DETAILS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , P.E164 , '|:|' , P.EXTENSION_NUMBER , '|:|' , P.OTHER_FORMAT)
			   FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_PHONE_NUMBER P
				WHERE P.ID = CD.PHONE_NUMBER
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_CONTACTO'
) DROP FUNCTION dbo.EXTRAE_URLS_CONTACTO;
GO
CREATE FUNCTION dbo.EXTRAE_URLS_CONTACTO(@P_CONTACT_DETAILS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			   FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_CONTACT_URL U,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE CD.ID = U.CONTACT_DETAILS_ID
					AND U.URL_ID = LAIT.ID
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_EQF_LEVEL'
) DROP FUNCTION dbo.EXTRAE_EQF_LEVEL;
GO
CREATE FUNCTION dbo.EXTRAE_EQF_LEVEL(@P_COOP_CON_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , CONVERT(VARCHAR(5),EQF_LEVEL))
			   FROM dbo.EWPCV_COOPCOND_EQFLVL
			   WHERE COOPERATION_CONDITION_ID = @P_COOP_CON_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_CREDITS'
) DROP FUNCTION dbo.EXTRAE_CREDITS;
GO
CREATE FUNCTION dbo.EXTRAE_CREDITS(@P_COMPONENT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , COALESCE(CR.CREDIT_LEVEL,'') , '|:|' , CR.SCHEME , '|:|' , CAST(CR.CREDIT_VALUE AS VARCHAR))
			FROM dbo.EWPCV_CREDIT CR,
				dbo.EWPCV_COMPONENT_CREDITS CCR
			WHERE CR.ID = CCR.CREDITS_ID
				AND CCR.COMPONENT_ID = @P_COMPONENT_ID
               for xml path ('')
              ), 1, 3, ''
            );

	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_CODIGO_OUNIT'
) DROP FUNCTION dbo.EXTRAE_CODIGO_OUNIT;
GO
CREATE FUNCTION dbo.EXTRAE_CODIGO_OUNIT(@P_OUNIT_ID varchar(255), @P_INSTITUTION_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = O.ORGANIZATION_UNIT_CODE
		FROM dbo.EWPCV_INST_ORG_UNIT IOU
			LEFT JOIN dbo.EWPCV_INSTITUTION INST ON IOU.INSTITUTION_ID = INST.ID
			LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT O ON IOU.ORGANIZATION_UNITS_ID = O.INTERNAL_ID
		WHERE O.ID = @P_OUNIT_ID AND INST.INSTITUTION_ID = @P_INSTITUTION_ID
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ACADEMIC_TERM'
) DROP FUNCTION dbo.EXTRAE_ACADEMIC_TERM;
GO
CREATE FUNCTION dbo.EXTRAE_ACADEMIC_TERM(@P_AT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , CONVERT(varchar, ATR.START_DATE, 102)  , '|:|' , CONVERT(varchar, ATR.END_DATE, 102) 
				, '|:|' , ATR.INSTITUTION_ID , '|:|' , dbo.EXTRAE_CODIGO_OUNIT(ATR.ORGANIZATION_UNIT_ID, ATR.INSTITUTION_ID)
				, '|:|' , CONVERT(varchar, ATR.TERM_NUMBER) , '|:|' , CONVERT(varchar, ATR.TOTAL_TERMS))
			FROM dbo.EWPCV_ACADEMIC_TERM ATR
			WHERE ATR.ID = @P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ACADEMIC_YEAR'
) DROP FUNCTION dbo.EXTRAE_ACADEMIC_YEAR;
GO
CREATE FUNCTION dbo.EXTRAE_ACADEMIC_YEAR(@P_AT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , AY.START_YEAR  , '-' , AY.END_YEAR)
			FROM dbo.EWPCV_ACADEMIC_TERM ATR
				LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR AY ON ATR.ACADEMIC_YEAR_ID = AY.ID
			WHERE ATR.ID = @P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_ACADEMIC_TERM'
) DROP FUNCTION dbo.EXTRAE_NOMBRES_ACADEMIC_TERM;
GO
CREATE FUNCTION dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(@P_AT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value =stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_ACADEMIC_TERM_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE N.DISP_NAME_ID = LAIT.ID
					AND N.ACADEMIC_TERM_ID = @P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_OUNIT'
) DROP FUNCTION dbo.EXTRAE_NOMBRES_OUNIT;
GO
CREATE FUNCTION dbo.EXTRAE_NOMBRES_OUNIT(@P_OUNIT_ID varchar(255), @P_INSTITUTION_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value =stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
		FROM dbo.EWPCV_INST_ORG_UNIT IOU
			LEFT JOIN dbo.EWPCV_INSTITUTION INST ON IOU.INSTITUTION_ID = INST.ID
			LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT O ON IOU.ORGANIZATION_UNITS_ID = O.INTERNAL_ID
			LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME N ON O.INTERNAL_ID = N.ORGANIZATION_UNIT_ID
			LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM LAIT ON N.NAME_ID = LAIT.ID
		WHERE O.ID = @P_OUNIT_ID AND INST.INSTITUTION_ID = @P_INSTITUTION_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_LOS'
) DROP FUNCTION dbo.EXTRAE_NOMBRES_LOS;
GO
CREATE FUNCTION dbo.EXTRAE_NOMBRES_LOS(@P_LOS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_LOS_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE N.NAME_ID = LAIT.ID
					AND N.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_DESCRIPTIONS_LOS'
) DROP FUNCTION dbo.EXTRAE_DESCRIPTIONS_LOS;
GO
CREATE FUNCTION dbo.EXTRAE_DESCRIPTIONS_LOS(@P_LOS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_LOS_DESCRIPTION D,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE D.DESCRIPTION_ID = LAIT.ID
					AND D.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_LOS'
) DROP FUNCTION dbo.EXTRAE_URLS_LOS;
GO
CREATE FUNCTION dbo.EXTRAE_URLS_LOS(@P_LOS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_LOS_URLS U,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE U.URL_ID = LAIT.ID
					AND U.LOS_ID = @P_LOS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO
 
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_S_AREA_L_SKILL'
) DROP FUNCTION dbo.EXTRAE_S_AREA_L_SKILL;
GO
CREATE FUNCTION dbo.EXTRAE_S_AREA_L_SKILL(@P_COOP_CON_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , S.ISCED_CODE , '|:|' ,S.ISCED_CLARIFICATION , '|:|' , L."LANGUAGE" , '|:|' , L.CEFR_LEVEL)
			    FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL CSL
				LEFT JOIN dbo.EWPCV_LANGUAGE_SKILL L	
					ON CSL.LANGUAGE_SKILL_ID = L.ID
				LEFT JOIN dbo.EWPCV_SUBJECT_AREA S	
					ON CSL.ISCED_CODE = S.ID	
				WHERE CSL.COOPERATION_CONDITION_ID = @P_COOP_CON_ID
				ORDER BY CSL.ORDER_INDEX_LANG
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_S_AREA'
) DROP FUNCTION dbo.EXTRAE_S_AREA;
GO
CREATE FUNCTION dbo.EXTRAE_S_AREA(@P_COOP_CON_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , S.ISCED_CODE , '|:|' , COALESCE(S.ISCED_CLARIFICATION,''))
			    FROM dbo.EWPCV_COOPCOND_SUBAR CS
				LEFT JOIN dbo.EWPCV_SUBJECT_AREA S	
					ON CS.ISCED_CODE = S.ID	
				WHERE CS.COOPERATION_CONDITION_ID = @P_COOP_CON_ID
				ORDER BY CS.ORDER_INDEX_SUB_AR
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_INSTITUCION'
) DROP FUNCTION dbo.EXTRAE_NOMBRES_INSTITUCION;
GO
CREATE FUNCTION dbo.EXTRAE_NOMBRES_INSTITUCION(@P_INSTITUTION_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_INSTITUTION I,
					dbo.EWPCV_INSTITUTION_NAME N,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE I.ID = N.INSTITUTION_ID
					AND N.NAME_ID = LAIT.ID
					AND UPPER(I.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NOMBRES_CONTACTO'
) DROP FUNCTION dbo.EXTRAE_NOMBRES_CONTACTO;
GO
CREATE FUNCTION dbo.EXTRAE_NOMBRES_CONTACTO(@P_CONTACTO_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_CONTACT C,
					dbo.EWPCV_CONTACT_NAME CN,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE C.ID = CN.CONTACT_ID
					AND CN.NAME_ID = LAIT.ID
					AND C.ID = @P_CONTACTO_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_DESCRIPCIONES_CONTACTO'
) DROP FUNCTION dbo.EXTRAE_DESCRIPCIONES_CONTACTO;
GO
CREATE FUNCTION dbo.EXTRAE_DESCRIPCIONES_CONTACTO(@P_CONTACTO_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			    FROM dbo.EWPCV_CONTACT C,
					dbo.EWPCV_CONTACT_DESCRIPTION CD,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE C.ID = CD.CONTACT_ID
					AND CD.DESCRIPTION_ID = LAIT.ID
					AND C.ID = @P_CONTACTO_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDRESS_LINES'
) DROP FUNCTION dbo.EXTRAE_ADDRESS_LINES;
GO
CREATE FUNCTION dbo.EXTRAE_ADDRESS_LINES(@P_FLEXIBLE_ADDRESS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , COALESCE(ADDRESS_LINE,''))
			    FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE 
				WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO 

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FLEXAD_DELIV_POINT'
) DROP FUNCTION dbo.EXTRAE_FLEXAD_DELIV_POINT;
GO 	
CREATE FUNCTION dbo.EXTRAE_FLEXAD_DELIV_POINT(@P_FLEXIBLE_ADDRESS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , COALESCE(Delivery_point_code,''))
			    FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD
        WHERE FLEXIBLE_ADDRESS_ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDRESS'
) DROP FUNCTION dbo.EXTRAE_ADDRESS;
GO 	
CREATE FUNCTION dbo.EXTRAE_ADDRESS(@P_FLEXIBLE_ADDRESS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , COALESCE(F.BUILDING_NUMBER,'') , '|:|' , COALESCE(F.BUILDING_NAME,'') , '|:|' , COALESCE(F.STREET_NAME ,'')
					, '|:|' , COALESCE(F.UNIT,'') , '|:|' , COALESCE(F."FLOOR",'') , '|:|' , COALESCE(F.POST_OFFICE_BOX,'')
					, '|:|' , (
					dbo.EXTRAE_FLEXAD_DELIV_POINT(F.ID)
					))
			    FROM dbo.EWPCV_FLEXIBLE_ADDRESS F
				WHERE F.ID = @P_FLEXIBLE_ADDRESS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_NIVELES_IDIOMAS'
) DROP FUNCTION dbo.EXTRAE_NIVELES_IDIOMAS;
GO 	
CREATE FUNCTION  dbo.EXTRAE_NIVELES_IDIOMAS(@P_MOBILITY_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , S."LANGUAGE" , '|:|' , S.CEFR_LEVEL)
			    FROM dbo.EWPCV_MOBILITY_LANG_SKILL MLS,
					dbo.EWPCV_LANGUAGE_SKILL S
				WHERE MLS.LANGUAGE_SKILL_ID = S.ID
					AND MLS.MOBILITY_ID = @P_MOBILITY_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO
 
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FIRMAS'
) DROP FUNCTION dbo.EXTRAE_FIRMAS;
GO 	
CREATE FUNCTION dbo.EXTRAE_FIRMAS (@P_FIRMA_ID varchar(255)) RETURNS varchar(max)  as
BEGIN
    DECLARE @v_lista VARCHAR(max)
    SELECT @v_lista = stuff( (select concat('|;|' , COALESCE(SIGNER_NAME,'') , '|' , COALESCE(SIGNER_POSITION,'') 
				, '|' , COALESCE(SIGNER_EMAIL,'') , '|' , CONVERT(VARCHAR(19), COALESCE(null, ''), 126 ) , '|' , COALESCE(SIGNER_APP,''))
			    FROM dbo.EWPCV_SIGNATURE
				WHERE ID = @P_FIRMA_ID
               for xml path ('')
              ), 1, 3, ''
            );

    RETURN @v_lista

END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FAX'
) DROP FUNCTION dbo.EXTRAE_FAX;
GO 	
CREATE FUNCTION dbo.EXTRAE_FAX(@P_CONTACT_DETAILS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
select @return_value = stuff( (select concat('|;|' , F.E164 , '|:|' , F.EXTENSION_NUMBER , '|:|' , F.OTHER_FORMAT)
				FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_PHONE_NUMBER F
				WHERE F.ID = CD.FAX_NUMBER
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_INST_URLS_FACTSHEET'
) DROP FUNCTION dbo.EXTRAE_INST_URLS_FACTSHEET;
GO
CREATE FUNCTION dbo.EXTRAE_INST_URLS_FACTSHEET(@P_INST_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			   FROM dbo.EWPCV_INST_FACT_SHEET_URL FSU,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE FSU.URL_ID = LAIT.ID
					AND FSU.INSTITUTION_ID = @P_INST_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_OUNIT_URLS_FACTSHEET'
) DROP FUNCTION dbo.EXTRAE_OUNIT_URLS_FACTSHEET;
GO
CREATE FUNCTION dbo.EXTRAE_OUNIT_URLS_FACTSHEET(@P_OUNIT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT."LANG")
			   FROM dbo.EWPCV_OUNIT_FACT_SHEET_URL FSU,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE FSU.URL_ID = LAIT.ID
					AND FSU.OUNIT_ID = @P_OUNIT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO


IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_PHOTO_URL_LIST'
) DROP FUNCTION dbo.EXTRAE_PHOTO_URL_LIST;
GO
CREATE FUNCTION dbo.EXTRAE_PHOTO_URL_LIST(@CONTACT_DETAILS_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , PU.URL , '|:|' , PU.PHOTO_SIZE , '|:|' , CONVERT(VARCHAR(10), PU.PHOTO_DATE, 20) , '|:|' , convert(varchar(1),PU.PHOTO_PUBLIC))
			   FROM dbo.EWPCV_PHOTO_URL PU
				WHERE PU.CONTACT_DETAILS_ID = @CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'ExtraerAnio'
) DROP FUNCTION dbo.ExtraerAnio;
GO
CREATE FUNCTION dbo.ExtraerAnio(@START_DATE DATETIME) RETURNS INTEGER
BEGIN
	DECLARE @anio integer
	SELECT @anio = YEAR(@START_DATE)
	RETURN @anio
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_LANGUAGE_ITEM'
) DROP FUNCTION dbo.EXTRAE_LANGUAGE_ITEM;
GO
CREATE FUNCTION dbo.EXTRAE_LANGUAGE_ITEM(@P_LANG_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , text , '|:|' , lang , '|:|' )
			   FROM dbo.EWPCV_LANGUAGE_ITEM PU
				WHERE ID = @P_LANG_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ISCED_GRADE'
) DROP FUNCTION dbo.EXTRAE_ISCED_GRADE;
GO
CREATE FUNCTION dbo.EXTRAE_ISCED_GRADE(@P_TOR_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;;|' , i.isced_code , '|::|',  
				   stuff( (select concat('|;|', g.label, '|:|', g.percentage ) 
					FROM dbo.EWPCV_GRADE_FREQUENCY g WHERE i.ID = g.isced_table_id
				  for xml path ('')
              ), 1, 3, ''
			  ))
				FROM dbo.EWPCV_ISCED_TABLE i 
				WHERE i.TOR_ID = @P_TOR_ID
               for xml path ('')
              ), 1, 4, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_LEVELS'
) DROP FUNCTION dbo.EXTRAE_LEVELS;
GO
CREATE FUNCTION dbo.EXTRAE_LEVELS(@P_LOI_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;;|' , lvl.level_type , '|::|' , lvl.level_value , '|::|', 
				   stuff( (select concat('|;|', li.text, '|:|', li.lang) 
					FROM dbo.EWPCV_LEVEL_DESCRIPTION ld INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON ld.level_description_id = li.id 
				WHERE ld.LEVEL_ID = lvl.ID
				  for xml path ('')
              ), 1, 3, ''
			  ))
			   FROM dbo.EWPCV_LEVEL lvl
				INNER JOIN dbo.EWPCV_LEVELS lvls ON lvl.id = lvls.level_id
				 WHERE lvls.LOI_ID = @P_LOI_ID
               for xml path ('')
              ), 1, 4, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_RES_DIST'
) DROP FUNCTION dbo.EXTRAE_RES_DIST;
GO
CREATE FUNCTION dbo.EXTRAE_RES_DIST(@P_RESULT_DIST_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , rdc.distrubtion_count , '|:|' , rdc.label)
			   FROM dbo.EWPCV_RESULT_DIST_CATEGORY rdc WHERE rdc.RESULT_DIST_ID = @P_RESULT_DIST_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GRAD_SCHEME_DESC'
) DROP FUNCTION dbo.EXTRAE_GRAD_SCHEME_DESC;
GO
CREATE FUNCTION dbo.EXTRAE_GRAD_SCHEME_DESC(@P_GRADING_SCHEME_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , lid.text , '|:|' , lid.lang , '|:|' )
			   FROM dbo.EWPCV_GRADING_SCHEME_DESC gsd
				LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM lid 
				ON gsd.description_id = lid.id 
				WHERE gsd.GRADING_SCHEME_ID = @P_GRADING_SCHEME_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GRAD_SCHEME_LABEL'
) DROP FUNCTION dbo.EXTRAE_GRAD_SCHEME_LABEL;
GO
CREATE FUNCTION dbo.EXTRAE_GRAD_SCHEME_LABEL(@P_GRADING_SCHEME_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , lig.text , '|:|' , lig.lang , '|:|' )
			   FROM dbo.EWPCV_GRADING_SCHEME_LABEL gsl
				LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM lig 
				ON gsl.label_id = lig.id 
				WHERE gsl.GRADING_SCHEME_ID = @P_GRADING_SCHEME_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GROUP_TYPE'
) DROP FUNCTION dbo.EXTRAE_GROUP_TYPE;
GO
CREATE FUNCTION dbo.EXTRAE_GROUP_TYPE(@P_LOI_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , li.text , '|:|' , li.lang , '|:|' )
			   FROM dbo.EWPCV_LOI_GROUPING lg
               INNER JOIN dbo.EWPCV_GROUP_TYPE gt ON lg.GROUP_TYPE_ID = gt.ID
               INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON gt.title = li.id
			   WHERE lg.LOI_ID = @P_LOI_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_GROUP'
) DROP FUNCTION dbo.EXTRAE_GROUP;
GO
CREATE FUNCTION dbo.EXTRAE_GROUP(@P_LOI_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , li.text , '|:|' , li.lang , '|:|' )
			   FROM dbo.EWPCV_LOI_GROUPING lg
               INNER JOIN dbo.EWPCV_GROUP g ON lg.GROUP_ID = g.ID
               INNER JOIN dbo.EWPCV_LANGUAGE_ITEM li ON g.title = li.id
			   WHERE lg.LOI_ID = @P_LOI_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO


IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ATTACH_TITLE'
) DROP FUNCTION dbo.EXTRAE_ATTACH_TITLE;
GO
CREATE FUNCTION dbo.EXTRAE_ATTACH_TITLE(@P_AT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT.LANG , '|:|' )
			   FROM dbo.EWPCV_ATTACHMENT_TITLE ATTCH_TIT INNER JOIN
			   dbo.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_TIT.ID = LAIT.ID
			   WHERE ATTCH_TIT.ATTACHMENT_ID=@P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ATTACH_DESC'
) DROP FUNCTION dbo.EXTRAE_ATTACH_DESC;
GO
CREATE FUNCTION dbo.EXTRAE_ATTACH_DESC(@P_AT_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , LAIT.TEXT , '|:|' , LAIT.LANG , '|:|' )
			   FROM dbo.EWPCV_ATTACHMENT_DESCRIPTION ATTCH_DESC INNER JOIN
			   dbo.EWPCV_LANGUAGE_ITEM LAIT ON ATTCH_DESC.ID = LAIT.ID
			   WHERE ATTCH_DESC.ATTACHMENT_ID=@P_AT_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_ADDIT_INFO'
) DROP FUNCTION dbo.EXTRAE_ADDIT_INFO;
GO
CREATE FUNCTION dbo.EXTRAE_ADDIT_INFO(@P_SECTION_ID varchar(255)) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select concat('|;|' , add_inf.ADDITIONAL_INFO )
			   FROM dbo.EWPCV_ADDITIONAL_INFO add_inf
			   WHERE add_inf.SECTION_ID=@P_SECTION_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'RESET_M_UPDATE_REQUEST'
) DROP PROCEDURE dbo.RESET_M_UPDATE_REQUEST;
GO
CREATE PROCEDURE dbo.RESET_M_UPDATE_REQUEST(@P_IDENTIFICADOR varchar(255),  @P_ERROR_MESSAGE VARCHAR(500) OUTPUT, @RETURN_VALUE INTEGER OUTPUT) AS
BEGIN
	DECLARE @v_count integer;
	SET @RETURN_VALUE = 0; 

	SELECT @v_count = COUNT (1) FROM dbo.EWPCV_MOBILITY_UPDATE_REQUEST WHERE LOWER(ID)=LOWER(@P_IDENTIFICADOR);
	IF @v_count > 0
        UPDATE dbo.EWPCV_MOBILITY_UPDATE_REQUEST SET RETRIES=0,IS_PROCESSING=0 WHERE LOWER(ID) = LOWER(@P_IDENTIFICADOR);
    ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'Error: No se ha encontrado el id en la tabla EWPCV_MOBILITY_UPDATE_REQUEST.';
		SET @RETURN_VALUE = 0; 
	END
	
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'RESET_NOTIFICATION'
) DROP PROCEDURE dbo.RESET_NOTIFICATION;
GO
CREATE PROCEDURE dbo.RESET_NOTIFICATION(@P_IDENTIFICADOR varchar(255),  @P_ERROR_MESSAGE VARCHAR(500) OUTPUT, @RETURN_VALUE INTEGER OUTPUT) AS
BEGIN
	DECLARE @v_count integer;
	SET @RETURN_VALUE = 0; 

	SELECT @v_count = COUNT (1) FROM dbo.EWPCV_NOTIFICATION WHERE LOWER(ID)=LOWER(@P_IDENTIFICADOR);
	IF @v_count > 0
        UPDATE dbo.EWPCV_NOTIFICATION SET RETRIES=0,PROCESSING=0 WHERE LOWER(ID) = LOWER(@P_IDENTIFICADOR);
    ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'Error: No se ha encontrado el id en la tabla EWPCV_NOTIFICATION.';
		SET @RETURN_VALUE = 0; 
	END
	
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'NOTIFICA_ELEMENTO'
) DROP PROCEDURE dbo.NOTIFICA_ELEMENTO;
GO
CREATE PROCEDURE dbo.NOTIFICA_ELEMENTO(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255), @P_ERROR_MESSAGE VARCHAR(500) OUTPUT, @RETURN_VALUE INTEGER OUTPUT) AS
BEGIN

	DECLARE @v_is_exposed integer;
	DECLARE @v_count integer
	SET @v_count = 0; 
	SET @RETURN_VALUE = 0;
	

	IF @P_ELEMENT_ID IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'Dede indicar el identificador del elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
	IF @P_TYPE IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar el tipo de elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
    IF @P_NOTIFY_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion a notificar')
        SET @RETURN_VALUE = -1;
	END
    IF @P_NOTIFIER_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion que envia la notificacion')
        SET @RETURN_VALUE = -1;
	END

	IF @P_TYPE = 0 AND @RETURN_VALUE = 0 -- IIA
    BEGIN    
		SELECT @v_is_exposed = IS_EXPOSED FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
		SELECT @v_count = COUNT(1)  FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID) AND IS_REMOTE = 0;
        IF @v_count = 0  
		BEGIN
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El IIA a notificar no existe o no es local')
            SET @RETURN_VALUE = -1;
        END 
	END
    ELSE IF @P_TYPE = 1 AND @RETURN_VALUE = 0 -- OMOBILITY
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La omobility a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 2 AND @RETURN_VALUE = 0--IMOBILITY
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'La imobility a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 3 AND @RETURN_VALUE = 0-- TOR
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.MOBILITY_ID
        INNER JOIN EWPCV_LEARNING_AGREEMENT LA ON ML.LEARNING_AGREEMENT_ID = LA.ID 
        INNER JOIN EWPCV_TOR T ON LA.TOR_ID = T.ID
        WHERE LOWER(M.ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El TOR a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 4 AND @RETURN_VALUE = 0--IIA APPROVAL
	BEGIN    
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'No se permite la generacion de notificaciones de tipo IIA Approval utiliza el metodo de aprobacion de IIAs en su lugar')
		SET @RETURN_VALUE = -1;
	END
    ELSE IF @P_TYPE = 5 AND @RETURN_VALUE = 0 --LA
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.MOBILITY_ID
        WHERE LOWER(M.ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El Learning Agreement a notificar aprobacion no existe')
            SET @RETURN_VALUE = -1;
        END
    END
	ELSE IF @RETURN_VALUE = 0
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El tipo de elemento indicado no admite CNR')
        SET @RETURN_VALUE = -1;
	END 

	IF @RETURN_VALUE = 0
	BEGIN
	    DECLARE @v_id VARCHAR(255) = NEWID();
		INSERT INTO dbo.EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
        	VALUES (@v_id, @P_ELEMENT_ID, LOWER(@P_NOTIFY_HEI), SYSDATETIME(), @P_TYPE, 1, 0, LOWER(@P_NOTIFIER_HEI) );

		IF @v_is_exposed IS NOT NULL AND @v_is_exposed = 0
		BEGIN
		UPDATE EWPCV_IIA SET IS_EXPOSED = 1
			WHERE ID = @P_ELEMENT_ID
		INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_ELEMENT_ID, @P_ELEMENT_ID, 0, 1)
		END
		ELSE
		BEGIN
		INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_id, 10, 0)
		END
	END
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_IS_TERMINATED'
) DROP FUNCTION dbo.EXTRAE_IS_TERMINATED;
GO
CREATE FUNCTION dbo.EXTRAE_IS_TERMINATED(@P_IIA_ID VARCHAR(255), @P_REMOTE_IIA_ID VARCHAR(255)) RETURNS INTEGER
BEGIN
DECLARE @v_is_terminated INTEGER
	DECLARE @v_count INTEGER
	DECLARE @v_iia_approval_id VARCHAR(255)

SET @v_is_terminated = 0

SELECT @v_count = COUNT(1) FROM EWPCV_COOPERATION_CONDITION WHERE IIA_ID = @P_IIA_ID AND IIA_APPROVAL_ID IS NULL AND TERMINATED = 1

    IF @v_count > 0
BEGIN
SET @v_is_terminated = 1
END
    ELSE
BEGIN
IF @P_REMOTE_IIA_ID IS NOT NULL
		BEGIN
SELECT @v_count = COUNT(1) FROM EWPCV_IIA_APPROVALS WHERE REMOTE_IIA_ID = @P_REMOTE_IIA_ID;
IF @v_count > 0
			BEGIN
SELECT TOP 1 @v_iia_approval_id = ID FROM EWPCV_IIA_APPROVALS WHERE REMOTE_IIA_ID = @P_REMOTE_IIA_ID ORDER BY DATE_IIA_APPROVAL DESC
SELECT @v_count = COUNT(1) FROM EWPCV_COOPERATION_CONDITION WHERE IIA_ID = @P_IIA_ID AND IIA_APPROVAL_ID = @v_iia_approval_id AND TERMINATED = 1
    IF @v_count > 0
BEGIN
SET @v_is_terminated = 1
END
END
END
END

    RETURN @v_is_terminated
END
;
GO

IF EXISTS (
select * from sysobjects where type = 'P' and category = 0 and name = 'PREPARE_SINCRONIZACION'
) DROP PROCEDURE dbo.PREPARE_SINCRONIZACION;
GO
CREATE PROCEDURE dbo.PREPARE_SINCRONIZACION(@v_inst_schac varchar(255), @v_date_from DATE) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_element_id VARCHAR(255)
	DECLARE @v_revision integer
    SELECT @v_count = COUNT(*) FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = @v_inst_schac;

    IF @v_count > 0
	BEGIN
        -- INSTITUTIONS
        INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
            VALUES (NEWID(), @v_inst_schac, 0, 6);

        -- OUNITS
		DECLARE cur CURSOR LOCAL FOR
			SELECT iou.ORGANIZATION_UNITS_ID FROM EWPCV_INSTITUTION i
			INNER JOIN EWPCV_INST_ORG_UNIT iou ON i.ID = iou.INSTITUTION_ID
			WHERE i.INSTITUTION_ID = @v_inst_schac
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 7);
			FETCH NEXT FROM cur INTO @v_element_id
		END
		CLOSE cur
		DEALLOCATE cur

        -- FACTS SHEET
		SELECT @v_count = COUNT(*) FROM EWPCV_INSTITUTION WHERE
			INSTITUTION_ID = @v_inst_schac AND FACT_SHEET IS NOT NULL;
        IF @v_count > 0
		BEGIN
			INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
				VALUES (NEWID(),(SELECT FACT_SHEET FROM EWPCV_INSTITUTION WHERE INSTITUTION_ID = @v_inst_schac), 0, 5);
			FETCH NEXT FROM cur INTO @v_element_id
        END

        -- IIA
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_IIA
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 0);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- LOS
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_LOS WHERE INSTITUTION_ID = @v_inst_schac
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 8);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- MOBILITY
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID, MOBILITY_REVISION FROM EWPCV_MOBILITY
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id, @v_revision
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, @v_revision, 0, 1);
			FETCH NEXT FROM cur INTO @v_element_id, @v_revision
        END
		CLOSE cur
		DEALLOCATE cur

        -- LA
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID, LEARNING_AGREEMENT_REVISION FROM EWPCV_LEARNING_AGREEMENT
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id, @v_revision
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, @v_element_id, 0, 4);
			FETCH NEXT FROM cur INTO @v_element_id, @v_revision
        END
		CLOSE cur
		DEALLOCATE cur

        -- TOR
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_TOR
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 2);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- NOTIFICATION
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_NOTIFICATION
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 10);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- UPDATE REQUEST
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_MOBILITY_UPDATE_REQUEST
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 11);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- IIA_APPROVALS
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_IIA_APPROVALS
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 3);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- IIA_REGRESSIONS
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_IIA_REGRESSIONS
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 13);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- DICTIONARY
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_DICTIONARY
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 9);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

        -- EVENTS
		DECLARE cur CURSOR LOCAL FOR
			SELECT ID FROM EWPCV_EVENT WHERE EVENT_DATE > @v_date_from AND ELEMENT_TYPE NOT IN (6, 7, 8, 9, 11, 13, 14, 15)
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 12);
			FETCH NEXT FROM cur INTO @v_element_id
        END
		CLOSE cur
		DEALLOCATE cur

		-- DELETED_ELEMENTS
		DECLARE cur CURSOR LOCAL FOR
		    SELECT ID FROM EWPCV_DELETED_ELEMENTS
		OPEN cur
		FETCH NEXT FROM cur INTO @v_element_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
            INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ACTION, ELEMENT_TYPE)
                VALUES (NEWID(), @v_element_id, 0, 14);
            FETCH NEXT FROM cur INTO @v_element_id
        END
        CLOSE cur
        DEALLOCATE cur
    END
END
;

/*
	Inserta un registro en la tabla de Notificaciones para poder refrescar el elemento
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_NOTIFICATION_ENTRANTE'
) DROP PROCEDURE dbo.INSERTA_NOTIFICATION_ENTRANTE;
GO
CREATE PROCEDURE dbo.INSERTA_NOTIFICATION_ENTRANTE(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255), @P_ERROR_MESSAGE VARCHAR(500) OUTPUT, @RETURN_VALUE INTEGER OUTPUT) AS
BEGIN

	SET @RETURN_VALUE = 0;
	
	IF @P_ELEMENT_ID IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'Dede indicar el identificador del elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
	IF @P_TYPE IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar el tipo de elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
	--  Validación para no permitir la aprobación (IIA APPROVAL) 
	IF @P_TYPE NOT IN (0, 1, 2, 3, 5)
    BEGIN
        SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El tipo de elemento indicado no admite CNR');
        SET @RETURN_VALUE = -1;
        RETURN;
    END
    IF @P_NOTIFY_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion a notificar')
        SET @RETURN_VALUE = -1;
	END
    IF @P_NOTIFIER_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion que envia la notificacion')
        SET @RETURN_VALUE = -1;
	END
	
	-- Comprueba si P_NOTIFY_HEI coincide con alguna institución existente
    DECLARE @v_count INT;
    SELECT @v_count = COUNT(*)
    FROM EWPCV_INSTITUTION_IDENTIFIERS
    WHERE SCHAC = @P_NOTIFY_HEI;
    IF @v_count = 0
    BEGIN
        SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no existe');
    END

    -- Comprueba si P_NOTIFIER_HEI coincide con alguna institución existente
    SELECT @v_count = COUNT(*)
    FROM EWPCV_INSTITUTION_IDENTIFIERS
    WHERE SCHAC = @P_NOTIFIER_HEI;
    IF @v_count = 0
    BEGIN
        SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución que envía la notificación no existe');
        SET @RETURN_VALUE = -1;
        RETURN;
    END
	
	BEGIN
	    DECLARE @v_id VARCHAR(255) = NEWID();
		INSERT INTO dbo.EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", RETRIES, CNR_TYPE, PROCESSING, OWNER_HEI, PROCESSING_DATE, SHOULD_RETRY, LAST_ERROR)
			VALUES (NEWID(), @P_ELEMENT_ID, LOWER(@P_NOTIFY_HEI), SYSDATETIME(), @P_TYPE, 0, 0, 0, LOWER(@P_NOTIFIER_HEI), null, 1, null);
	END
	
	PRINT 'Se ha procesado correctamente el refresco, no olvides commitear los cambios tras revisarlo en la base de datos';
	
	COMMIT;
	RETURN @RETURN_VALUE;
END
;
GO