/*
***************************************
******** Funciones Mobility ***********
***************************************
*/


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_SIGNATURE' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>
	<xs:element name="signature" type="signatureType" />
</xs:schema>'

GO

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_MOBILITY' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>
		
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="photoUrlType">
		<xs:sequence>
			<xs:element name="url" type="HTTPS" minOccurs="0" />
			<xs:element name="photo_size" type="photoSizeType" minOccurs="0" />
			<xs:element name="photo_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="photo_public" >
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:enumeration value="0"/>
						<xs:enumeration value="1"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="photoSizeType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d+x\d+"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="cooperation_condition_id" type="xs:string" minOccurs="0" />
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
							<xs:element name="photo_url_list" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="photo_url" maxOccurs="unbounded" minOccurs="0" type="photoUrlType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>			
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="eqf_level_nomination" minOccurs="1">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:minInclusive value="1"/>
							<xs:maxInclusive value="8"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="eqf_level_departure" minOccurs="1">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:minInclusive value="1"/>
							<xs:maxInclusive value="8"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="receiving_iia_id" type="xs:string" minOccurs="0" />
				<xs:element name="academic_term">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="academic_year" type="academicYearType" />
							<xs:element name="institution_id" type="notEmptyStringType" />
							<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
							<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
							<xs:element name="description" type="languageItemListType" minOccurs="0" />
							<xs:element name="term_number" type="xs:integer" />
							<xs:element name="total_terms" type="xs:integer" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="status_outgoing">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>';

GO


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LEARNING_AGREEMENT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="creditType">
		<xs:sequence>
			<xs:element name="scheme" type="notEmptyStringType" />
			<xs:element name="credit_value">
				<xs:simpleType>
					<xs:restriction base="xs:decimal">
						<xs:totalDigits value="5" />
						<xs:fractionDigits value="1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language_skill" maxOccurs="unbounded">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="language" type="notEmptyStringType" />
						<xs:element name="cefr_level" type="notEmptyStringType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="learning_agreement">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="mobility_id" type="notEmptyStringType" />
				<xs:element name="student_la" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="given_name" type="xs:string" />
							<xs:element name="family_name" type="xs:string" />
							<xs:element name="birth_date" type="xs:string" />
							<xs:element name="citizenship" minOccurs="0">
								<xs:simpleType>
									<xs:restriction base="xs:string">
										<xs:pattern value="[A-Z][A-Z]"/>
									</xs:restriction>
								</xs:simpleType>
							</xs:element>
							<xs:element name="gender" type="genderType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="la_component_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="la_component" minOccurs="0" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="la_component_type">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_id" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:pattern value="(CR|CLS|MOD|DEP)/(.{1,40})"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_code" type="xs:string" minOccurs="0" />
										<xs:element name="los_type" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/> <!-- Course-->
													<xs:enumeration value="1"/> <!-- Class-->
													<xs:enumeration value="2"/> <!-- Module-->
													<xs:enumeration value="3"/> <!-- Degree Programe-->
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="title" type="notEmptyStringType" />
										<xs:element name="academic_term">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="academic_year" type="academicYearType" />
													<xs:element name="institution_id" type="notEmptyStringType" />
													<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
													<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="description" type="languageItemListType" minOccurs="0" />
													<xs:element name="term_number" type="xs:integer" />
													<xs:element name="total_terms" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="credit" type="creditType" />
										<xs:element name="recognition_conditions" type="xs:string" minOccurs="0" />
										<xs:element name="short_description" type="xs:string" minOccurs="0" />
										<xs:element name="status" type="xs:string" minOccurs="0" />
										<xs:element name="reason_code" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
													<xs:enumeration value="5"/>
												</xs:restriction>
											</xs:simpleType>													
										</xs:element>
										<xs:element name="reason_text" type="xs:string" minOccurs="0" />
										<xs:element name="start_date" type="xs:date" minOccurs="0" />
										<xs:element name="end_date" type="xs:date" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="student_signature" type="signatureType" />
				<xs:element name="sending_hei_signature" type="signatureType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'
;

GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_SIGNATURE'
) DROP PROCEDURE dbo.VALIDA_SIGNATURE;
GO 
CREATE PROCEDURE  dbo.VALIDA_SIGNATURE(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_SIGNATURE)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY'
) DROP PROCEDURE dbo.VALIDA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_MOBILITY)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.VALIDA_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE  dbo.VALIDA_LEARNING_AGREEMENT(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_LEARNING_AGREEMENT)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO 

/*
	Valida que el iias y la coop condition indicadas existan
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY_IIA'
) DROP PROCEDURE dbo.VALIDA_MOBILITY_IIA;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY_IIA(@P_IIA_ID varchar(255), @P_IS_REMOTE integer, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	DECLARE @v_count int

	SET @return_value=0
	
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END

	IF @P_IIA_ID is not NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE UPPER(ID) = UPPER(@P_IIA_ID) AND IS_REMOTE = @P_IS_REMOTE
		IF @v_count = 0
		BEGIN 
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' El acuerdo interistitucional indicado no existe en el sistema.')
		END 
	END 
END
;
GO




/*
	Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_MOBILITY(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	DECLARE @IIA_ID varchar(255)
	DECLARE @RECEIVING_IIA_ID varchar(255)
	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END

	SELECT
		 @IIA_ID = T.c.value('(iia_id)[1]','varchar(255)'),
		 @RECEIVING_IIA_ID = T.c.value('(receiving_iia_id)[1]','varchar(255)'),
		 @SENDING_INSTITUTION =  T.c.query('sending_institution'),
		 @RECEIVING_INSTITUTION =  T.c.query('receiving_institution')
	FROM @P_MOBILITY_XML.nodes('/mobility') T(c) 

	IF @IIA_ID is not null 
		EXECUTE dbo.VALIDA_MOBILITY_IIA @IIA_ID, 0, @P_ERROR_MESSAGE output, @return_value output
	
	IF @return_value = 0 and @RECEIVING_IIA_ID is not null
		EXECUTE dbo.VALIDA_MOBILITY_IIA @RECEIVING_IIA_ID, 1, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY_INST @SENDING_INSTITUTION, @RECEIVING_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
	END 
	
END
;
GO
 
 
/*
	Valida que existan los institution id y ounit id de los academic term de los componentes del LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_ACADEMIC_TERM_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_ACADEMIC_TERM_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_ACADEMIC_TERM_INSTITUTION(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
DECLARE @COMPONENTS_LIST xml
DECLARE @v_mobility_id varchar(255)
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_r_inst_id varchar(255)

DECLARE @v_component_type integer
DECLARE @v_inst_id varchar(255)
DECLARE @v_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value=0
	
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	--obtenemos datos del xml
	SELECT 
		@v_mobility_id =T.c.value('(mobility_id)[1]', 'varchar(255)'),
		@COMPONENTS_LIST = T.c.query('la_component_list')
	FROM @P_LA_XML.nodes('learning_agreement') T(c)
	
	--obtenemos sender y reciver de la ultima version de la movilidad
	SELECT @v_s_inst_id  = SENDING_INSTITUTION_ID, @v_r_inst_id = RECEIVING_INSTITUTION_ID 
	FROM EWPCV_MOBILITY 
	WHERE ID = @v_mobility_id AND MOBILITY_REVISION = (
		SELECT MAX(MOBILITY_REVISION) FROM EWPCV_MOBILITY 
		WHERE ID = @v_mobility_id)
	
	--cursor para extraer informacion de cada componente
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(la_component_type)[1]', 'integer'),
		T.c.value('(academic_term/institution_id)[1]', 'varchar(255)'),
		T.c.value('(academic_term/organization_unit_code)[1]', 'varchar(255)')
		FROM @COMPONENTS_LIST.nodes('la_component_list/la_component') T(c)
	
	OPEN cur
	FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--componentes estudiados o virtuales
			IF @v_component_type = 0 OR @v_component_type = 5 
			BEGIN
				
				IF @v_inst_id != @v_r_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo estudiado/virtual no coincide con la institucion ' + @v_r_inst_id + ' de la entidad receptora')
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.INTERNAL_ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.')
					END
				END
			END
			--componentes reconocidos
			ELSE IF @v_component_type = 1
			BEGIN
				IF @v_inst_id != @v_s_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo reconocido no coincide con la institucion ' + @v_s_inst_id + ' de la entidad emisora')
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.INTERNAL_ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.')
					END
				END
			END
			FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
		END
	CLOSE cur
	DEALLOCATE cur

END 
;
GO

/*
	Valida la calidad del dato de la entrada al aprovisionamiento de learning agreements
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LA'
) DROP PROCEDURE dbo.VALIDA_DATOS_LA;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_LA(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	EXECUTE dbo.VALIDA_ACADEMIC_TERM_INSTITUTION @P_LA_XML, @P_ERROR_MESSAGE output, @return_value output
	
END
;
GO

/*
	Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_TYPE'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_TYPE;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_TYPE(@P_MOBILITY_TYPE VARCHAR(255), @v_id varchar(255) output) AS
BEGIN
	SELECT @v_id=ID
		FROM EWPCV_MOBILITY_TYPE
		WHERE UPPER(MOBILITY_CATEGORY) = UPPER(@P_MOBILITY_TYPE)
		AND  UPPER(MOBILITY_GROUP) = UPPER('MOBILITY_LA')

	IF @v_id is null and @P_MOBILITY_TYPE is not null 
	BEGIN
		SET @v_id=NEWID()
		INSERT INTO EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (@v_id, @P_MOBILITY_TYPE, 'MOBILITY_LA')
	END
END
; 
GO


/*
	Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_PARTICIPANT'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_PARTICIPANT;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_PARTICIPANT(@P_STUDENT xml, @return_value varchar(255) output) AS
BEGIN

	DECLARE @v_c_id varchar(255)
	DECLARE @v_m_p_id varchar(255)
	DECLARE @v_c_d_id varchar(255)
	DECLARE @v_p_id varchar(255)

	DECLARE @GLOBAL_ID varchar(255)
	DECLARE @CONTACT_PERSON xml
	DECLARE @PHOTO_URL_LIST xml

	DECLARE @PHOTO_URL varchar(255)
	DECLARE @PHOTO_SIZE varchar(255) 
	DECLARE	@PHOTO_DATE date
	DECLARE @PHOTO_PUBLIC integer

	SET @v_m_p_id = NEWID()

	SELECT 
		@GLOBAL_ID = T.c.value('(global_id)[1]','varchar(255)'),
		@CONTACT_PERSON = T.c.query('contact_person'),
		@PHOTO_URL_LIST = T.c.query('photo_url_list')
	FROM @P_STUDENT.nodes('student') T(c)

	EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, null, null, @v_c_id output
	INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, CONTACT_ID, GLOBAL_ID) VALUES (@v_m_p_id, @v_c_id, @GLOBAL_ID)

	SELECT @v_c_d_id = CONTACT_DETAILS_ID FROM EWPCV_CONTACT C WHERE C.ID = @v_c_id

	IF @v_c_d_id IS NOT NULL
	BEGIN
		-- FOTOS
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @PHOTO_URL_LIST.nodes('photo_url_list/photo_url') T(c) 		
		OPEN cur
		FETCH NEXT FROM cur INTO @PHOTO_URL_LIST
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_p_id = NEWID()

				SELECT
					 @PHOTO_URL = T.c.value('(url)[1]','varchar(255)'),
					 @PHOTO_SIZE = T.c.value('(photo_size)[1]','varchar(255)'),
					 @PHOTO_DATE = T.c.value('(photo_date)[1]','date'),
					 @PHOTO_PUBLIC = T.c.value('(photo_public)[1]','int')
				FROM @PHOTO_URL_LIST.nodes('/photo_url') T(c) 

				INSERT INTO dbo.EWPCV_PHOTO_URL (
					PHOTO_URL_ID, CONTACT_DETAILS_ID, 
					URL, PHOTO_SIZE, 
					PHOTO_DATE, PHOTO_PUBLIC)
				VALUES (@v_p_id, @v_c_d_id, 
					@PHOTO_URL, @PHOTO_SIZE, 
					@PHOTO_DATE, @PHOTO_PUBLIC)

				FETCH NEXT FROM cur INTO @PHOTO_URL_LIST
			END
		CLOSE cur
		DEALLOCATE cur

	END
	   
	SET @return_value = @v_m_p_id

END
;
GO

/*
	Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY'
) DROP PROCEDURE dbo.INSERTA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY(@P_MOBILITY xml, @P_REVISION integer, @v_id varchar(255) output) AS
BEGIN
    DECLARE @v_internal_id varchar(255)
	DECLARE @v_s_ounit_id varchar(255)
	DECLARE @v_r_ounit_id varchar(255)
	DECLARE @v_student_id varchar(255)
	DECLARE @v_s_contact_id varchar(255)
	DECLARE @v_s_admv_contact_id varchar(255)
	DECLARE @v_r_contact_id varchar(255)
	DECLARE @v_r_admv_contact_id varchar(255)
	DECLARE @v_mobility_type_id varchar(255)
	DECLARE @v_lang_skill_id varchar(255)
	DECLARE @v_isced_code varchar(255)
	DECLARE @v_academic_term_id varchar(255)
	DECLARE @v_status_incoming integer
	DECLARE @v_status_outgoing integer

	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	DECLARE @STUDENT xml
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml
	DECLARE @SUBJECT_AREA xml
	DECLARE @MOBILITY_TYPE varchar(255)
	DECLARE @ACTUAL_ARRIVAL_DATE date
	DECLARE @ACTUAL_DEPATURE_DATE date
	DECLARE @IIA_ID varchar(255)
	DECLARE @PLANED_ARRIVAL_DATE date
	DECLARE @PLANED_DEPATURE_DATE date
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @STATUS_OUTGOING integer
	DECLARE @STUDENT_LANGUAGE_SKILL_LIST xml
	DECLARE @STUDENT_LANGUAGE_SKILL xml
	DECLARE @ACADEMIC_TERM xml
	DECLARE @EQF_LEVEL_DEPARTURE integer
	DECLARE @EQF_LEVEL_NOMINATION integer
	DECLARE @RECEIVING_IIA_ID varchar(255)

	SELECT 
		@SENDING_INSTITUTION = T.c.query('sending_institution'),
		@RECEIVING_INSTITUTION = T.c.query('receiving_institution'),
		@STUDENT = T.c.query('student'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@SUBJECT_AREA = T.c.query('subject_area'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)'),
		@ACTUAL_ARRIVAL_DATE = T.c.value('(actual_arrival_date)[1]', 'date'),
		@ACTUAL_DEPATURE_DATE = T.c.value('(actual_depature_date)[1]', 'date'),
		@PLANED_ARRIVAL_DATE = T.c.value('(planed_arrival_date)[1]', 'date'),
		@PLANED_DEPATURE_DATE = T.c.value('(planed_depature_date)[1]', 'date'),
		@IIA_ID = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@SENDING_INSTITUTION_ID = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]', 'integer'),
		@STUDENT_LANGUAGE_SKILL_LIST = T.c.query('student_language_skill_list'),
		@ACADEMIC_TERM = T.c.query('academic_term'),
		@EQF_LEVEL_DEPARTURE = T.c.value('(eqf_level_departure)[1]', 'integer'),
		@EQF_LEVEL_NOMINATION = T.c.value('(eqf_level_nomination)[1]', 'integer'),
		@RECEIVING_IIA_ID = T.c.value('(receiving_iia_id)[1]', 'varchar(255)')

	FROM @P_MOBILITY.nodes('mobility') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @SENDING_INSTITUTION , @v_s_ounit_id output
	EXECUTE dbo.INSERTA_INST_OUNIT @RECEIVING_INSTITUTION , @v_r_ounit_id output
	EXECUTE dbo.INSERTA_MOBILITY_PARTICIPANT @STUDENT , @v_student_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT , null, null, @v_s_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT , null, null, @v_s_admv_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT , null, null, @v_r_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT , null, null, @v_r_admv_contact_id output

	EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA , @v_isced_code output
	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE , @v_mobility_type_id output
	
	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM , @v_academic_term_id output

	-- Por defecto el status_outgoing es NOMINATION
	IF @STATUS_OUTGOING is null 
		SET @v_status_outgoing = 2
	ELSE
		SET @v_status_outgoing = @STATUS_OUTGOING

	IF @v_id is null
		SET @v_id = NEWID()

    IF @v_internal_id is null
        SET @v_internal_id = NEWID()

	INSERT INTO dbo.EWPCV_MOBILITY (MOBILITY_ID,
	        ID,
			ACTUAL_ARRIVAL_DATE,
			ACTUAL_DEPARTURE_DATE,
			IIA_ID,
			RECEIVING_IIA_ID,
			ISCED_CODE,
			MOBILITY_PARTICIPANT_ID,
			MOBILITY_REVISION,
			PLANNED_ARRIVAL_DATE,
			PLANNED_DEPARTURE_DATE,
			RECEIVING_INSTITUTION_ID,
			RECEIVING_ORGANIZATION_UNIT_ID,
			SENDING_INSTITUTION_ID,
			SENDING_ORGANIZATION_UNIT_ID,
			MOBILITY_TYPE_ID,
			SENDER_CONTACT_ID,
			SENDER_ADMV_CONTACT_ID,
			RECEIVER_CONTACT_ID,
			RECEIVER_ADMV_CONTACT_ID,
			ACADEMIC_TERM_ID,
			EQF_LEVEL_DEPARTURE,
			EQF_LEVEL_NOMINATION,
			STATUS_OUTGOING,
			MODIFY_DATE)
			VALUES (@v_internal_id,
			@v_id,
			@ACTUAL_ARRIVAL_DATE,
			@ACTUAL_DEPATURE_DATE,
			@IIA_ID,
			@RECEIVING_IIA_ID,
			@v_isced_code,
			@v_student_id,
			(@P_REVISION +1),
			@PLANED_ARRIVAL_DATE,
			@PLANED_DEPATURE_DATE,
			LOWER(@RECEIVING_INSTITUTION_ID),
			@v_r_ounit_id,
			LOWER(@SENDING_INSTITUTION_ID),
			@v_s_ounit_id,
			@v_mobility_type_id,
			@v_s_contact_id,
			@v_s_admv_contact_id,
			@v_r_contact_id,
			@v_r_admv_contact_id,
			@v_academic_term_id,
			@EQF_LEVEL_DEPARTURE,
			@EQF_LEVEL_NOMINATION,
			@v_status_outgoing,
			SYSDATETIME())

    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
        VALUES (NEWID(), @v_id, (@P_REVISION +1), @v_id, 1, 0)


	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @STUDENT_LANGUAGE_SKILL_LIST.nodes('student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, LANGUAGE_SKILL_ID) VALUES (@v_internal_id, @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/* 
	Borra el estudiante de la tabla mobility participant
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_STUDENT'
) DROP PROCEDURE dbo.BORRA_STUDENT;
GO 
CREATE PROCEDURE  dbo.BORRA_STUDENT (@P_ID VARCHAR(255)) AS
BEGIN
	DECLARE @v_contact_id varchar(255)
	DECLARE @v_contact_details_id varchar(255)

	SELECT @v_contact_id = CONTACT_ID FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	SELECT @v_contact_details_id = CONTACT_DETAILS_ID FROM dbo.EWPCV_CONTACT WHERE ID = @v_contact_id
	DELETE FROM dbo.EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = @v_contact_details_id
	DELETE FROM dbo.EWPCV_MOBILITY_PARTICIPANT WHERE ID = @P_ID
	EXECUTE dbo.BORRA_CONTACT @v_contact_id

END
;
GO


/*
	Borra creditos asociados a un componente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_CREDITS'
) DROP PROCEDURE dbo.BORRA_CREDITS;
GO 
CREATE PROCEDURE  dbo.BORRA_CREDITS(@P_COMPONENT_ID varchar(255)) AS
BEGIN

	DECLARE @CREDITS_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT CREDITS_ID
			FROM dbo.EWPCV_COMPONENT_CREDITS
			WHERE COMPONENT_ID = @P_COMPONENT_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @CREDITS_ID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COMPONENT_CREDITS WHERE CREDITS_ID = @CREDITS_ID
			DELETE FROM dbo.EWPCV_CREDIT WHERE ID = @CREDITS_ID
			FETCH NEXT FROM cur INTO @CREDITS_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/*
	Borra un LOI 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LOI'
) DROP PROCEDURE dbo.BORRA_LOI;
GO 
CREATE PROCEDURE  dbo.BORRA_LOI(@P_LOI_ID varchar(255)) AS
BEGIN
	DELETE FROM dbo.EWPCV_LOS_LOI WHERE LOI_ID = @P_LOI_ID
	DELETE FROM dbo.EWPCV_LOI WHERE ID = @P_LOI_ID
	
END
;
GO

/*
	Borra un componente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COMPONENT'
) DROP PROCEDURE dbo.BORRA_COMPONENT;
GO 
CREATE PROCEDURE  dbo.BORRA_COMPONENT(@P_ID varchar(255)) AS
BEGIN
	DECLARE @v_at_id varchar(255)
	EXECUTE dbo.BORRA_CREDITS @P_ID
	
	DECLARE @v_loi_id varchar(255)
	SELECT @v_loi_id = LOI_ID FROM dbo.EWPCV_LA_COMPONENT WHERE ID = @P_ID
	
	SELECT @v_at_id = ACADEMIC_TERM_DISPLAY_NAME
			FROM dbo.EWPCV_LA_COMPONENT
			WHERE ID = @P_ID
	DELETE FROM dbo.EWPCV_LA_COMPONENT WHERE ID = @P_ID;
	
	EXECUTE dbo.BORRA_ACADEMIC_TERM @v_at_id
	EXECUTE dbo.BORRA_LOI @v_loi_id

END
;
GO

/*
	Borra un componente de tipo studied
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_STUDIED_COMP'
) DROP PROCEDURE dbo.BORRA_STUDIED_COMP;
GO 
CREATE PROCEDURE  dbo.BORRA_STUDIED_COMP(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN

	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT STUDIED_LA_COMPONENT_ID
			FROM dbo.EWPCV_STUDIED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_STUDIED_LA_COMPONENT 
				WHERE STUDIED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/*
	Borra un componente de tipo recognized
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_RECOGNIZED_COMP'
) DROP PROCEDURE dbo.BORRA_RECOGNIZED_COMP;
GO 
CREATE PROCEDURE  dbo.BORRA_RECOGNIZED_COMP(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN

	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT RECOGNIZED_LA_COMPONENT_ID
			FROM dbo.EWPCV_RECOGNIZED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_RECOGNIZED_LA_COMPONENT 
				WHERE RECOGNIZED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra un componente de tipo virtual
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_VIRTUAL_COMP'
) DROP PROCEDURE dbo.BORRA_VIRTUAL_COMP;
GO 
CREATE PROCEDURE  dbo.BORRA_VIRTUAL_COMP(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN

	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT VIRTUAL_LA_COMPONENT_ID
			FROM dbo.EWPCV_VIRTUAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_VIRTUAL_LA_COMPONENT 
				WHERE VIRTUAL_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra un componente de tipo blended
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_BLENDED_COMP'
) DROP PROCEDURE dbo.BORRA_BLENDED_COMP;
GO 
CREATE PROCEDURE  dbo.BORRA_BLENDED_COMP(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN

	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT BLENDED_LA_COMPONENT_ID
			FROM dbo.EWPCV_BLENDED_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_BLENDED_LA_COMPONENT 
				WHERE BLENDED_LA_COMPONENT_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/*
	Borra un componente de tipo doctoral
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_DOCTORAL_COMP'
) DROP PROCEDURE dbo.BORRA_DOCTORAL_COMP;
GO 
CREATE PROCEDURE  dbo.BORRA_DOCTORAL_COMP(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN

	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT DOCTORAL_LA_COMPONENTS_ID
			FROM dbo.EWPCV_DOCTORAL_LA_COMPONENT
			WHERE LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DELETE FROM dbo.EWPCV_DOCTORAL_LA_COMPONENT 
				WHERE DOCTORAL_LA_COMPONENTS_ID = @ID 
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

			EXECUTE dbo.BORRA_COMPONENT @ID

			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/*
	Borra una revision de un LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_LA'
) DROP PROCEDURE dbo.BORRA_LA;
GO 
CREATE PROCEDURE  dbo.BORRA_LA(@P_LA_ID varchar(255), @LA_REVISION integer) AS
BEGIN
	DECLARE @v_s_id varchar(255)
	DECLARE @v_sc_id varchar(255)
	DECLARE @v_rc_id varchar(255)

	EXECUTE dbo.BORRA_STUDIED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_RECOGNIZED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_VIRTUAL_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_BLENDED_COMP @P_LA_ID, @LA_REVISION
	EXECUTE dbo.BORRA_DOCTORAL_COMP @P_LA_ID, @LA_REVISION

	SELECT @v_s_id = STUDENT_SIGN, @v_sc_id = SENDER_COORDINATOR_SIGN, @v_rc_id = RECEIVER_COORDINATOR_SIGN
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @LA_REVISION

	DELETE FROM dbo.EWPCV_MOBILITY_LA WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LA_REVISION
	DELETE FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @LA_REVISION
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_s_id
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_sc_id
	DELETE FROM dbo.EWPCV_SIGNATURE WHERE ID = @v_rc_id

END
; 
GO

/*
	Borra los language skill asociados a una revision de una mobilidad
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_MOBILITY_LANSKILL'
) DROP PROCEDURE dbo.BORRA_MOBILITY_LANSKILL;
GO 
CREATE PROCEDURE  dbo.BORRA_MOBILITY_LANSKILL(@P_MOBILITY_ID varchar(255)) AS
BEGIN
    DELETE FROM dbo.EWPCV_MOBILITY_LANG_SKILL WHERE MOBILITY_ID = @P_MOBILITY_ID
END
;
GO

/*
	Borra una movilidad
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_MOBILITY'
) DROP PROCEDURE dbo.BORRA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.BORRA_MOBILITY(@P_MOBILITY_ID varchar(255)) AS
BEGIN
	DECLARE @v_count integer
    DECLARE @v_internal_id varchar(255)
	DECLARE @ID varchar(255)
	DECLARE @MOBILITY_REVISION integer			
	DECLARE @MOBILITY_PARTICIPANT_ID varchar(255)
	DECLARE @MOBILITY_TYPE_ID varchar(255)
	DECLARE @SENDER_CONTACT_ID varchar(255)
	DECLARE @SENDER_ADMV_CONTACT_ID varchar(255)
	DECLARE @RECEIVER_CONTACT_ID varchar(255)
	DECLARE @RECEIVER_ADMV_CONTACT_ID varchar(255)
	DECLARE @ISCED_CODE varchar(255)

	DECLARE @LEARNING_AGREEMENT_ID varchar(255)
	DECLARE @LEARNING_AGREEMENT_REVISION integer	

	DECLARE c_mobility CURSOR LOCAL FOR
			SELECT MOBILITY_ID, ID, MOBILITY_REVISION,
				MOBILITY_PARTICIPANT_ID, MOBILITY_TYPE_ID, 
				SENDER_CONTACT_ID, SENDER_ADMV_CONTACT_ID, RECEIVER_CONTACT_ID, RECEIVER_ADMV_CONTACT_ID, 
				ISCED_CODE
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_MOBILITY_ID

	OPEN c_mobility
	FETCH NEXT FROM c_mobility INTO @v_internal_id, @ID, @MOBILITY_REVISION,
				@MOBILITY_PARTICIPANT_ID, @MOBILITY_TYPE_ID, 
				@SENDER_CONTACT_ID, @SENDER_ADMV_CONTACT_ID, @RECEIVER_CONTACT_ID, @RECEIVER_ADMV_CONTACT_ID, 
				@ISCED_CODE
	WHILE @@FETCH_STATUS = 0
		BEGIN

			EXECUTE dbo.BORRA_MOBILITY_LANSKILL @v_internal_id

			----------------------------------			
			DECLARE c_la CURSOR LOCAL FOR
			SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION
					FROM dbo.EWPCV_MOBILITY_LA 
					WHERE MOBILITY_ID = @v_internal_id
			OPEN c_la
			FETCH NEXT FROM c_la INTO @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.BORRA_LA @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
					FETCH NEXT FROM c_la INTO @LEARNING_AGREEMENT_ID, @LEARNING_AGREEMENT_REVISION
				END
			CLOSE c_la
			DEALLOCATE c_la
			----------------------------------

			DELETE FROM dbo.EWPCV_MOBILITY WHERE ID = @ID AND MOBILITY_REVISION = @MOBILITY_REVISION

			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE MOBILITY_PARTICIPANT_ID = @MOBILITY_PARTICIPANT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_STUDENT @MOBILITY_PARTICIPANT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE SENDER_CONTACT_ID = @SENDER_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @SENDER_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE SENDER_ADMV_CONTACT_ID = @SENDER_ADMV_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @SENDER_ADMV_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE RECEIVER_CONTACT_ID = @RECEIVER_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @RECEIVER_CONTACT_ID

			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE RECEIVER_ADMV_CONTACT_ID = @RECEIVER_ADMV_CONTACT_ID
			IF @v_count = 0 
				EXECUTE dbo.BORRA_CONTACT @RECEIVER_ADMV_CONTACT_ID

		    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
		        VALUES (NEWID(), @ID, @MOBILITY_REVISION, @ID, 1, 2)

			FETCH NEXT FROM c_mobility INTO @ID, @MOBILITY_REVISION, 
				@MOBILITY_PARTICIPANT_ID, @MOBILITY_TYPE_ID, 
				@SENDER_CONTACT_ID, @SENDER_ADMV_CONTACT_ID, @RECEIVER_CONTACT_ID, @RECEIVER_ADMV_CONTACT_ID, 
				@ISCED_CODE

		END
	CLOSE c_mobility
	DEALLOCATE c_mobility

END
;
GO

/*
	Persiste una firma en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SIGNATURE'
) DROP PROCEDURE dbo.INSERTA_SIGNATURE;
GO 
CREATE PROCEDURE  dbo.INSERTA_SIGNATURE(@P_SIGNATURE xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @SIGNER_NAME varchar(255)
	DECLARE @SIGNER_POSITION varchar(255)
	DECLARE @SIGNER_EMAIL varchar(255)
	DECLARE @SIGN_DATE datetime
	DECLARE @SIGNER_APP varchar(255)
	DECLARE @SIGNATURE varchar(max)

	SET @v_id = NEWID()

	SELECT 
		@SIGNER_NAME = T.c.value('(signer_name)[1]', 'varchar(255)'),
		@SIGNER_POSITION = T.c.value('(signer_position)[1]', 'varchar(255)'),
		@SIGNER_EMAIL = T.c.value('(signer_email)[1]', 'varchar(255)'),
		@SIGN_DATE = T.c.value('(sign_date)[1]', 'datetime'),
		@SIGNER_APP = T.c.value('(signer_app)[1]', 'varchar(255)'),
		@SIGNATURE = T.c.value('(signature)[1]', 'varchar(max)')
	FROM @P_SIGNATURE.nodes('*') T(c)

	 INSERT INTO dbo.EWPCV_SIGNATURE (ID,SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, "TIMESTAMP", SIGNER_APP, SIGNATURE)
		VALUES (@v_id, @SIGNER_NAME, @SIGNER_POSITION , @SIGNER_EMAIL , @SIGN_DATE , @SIGNER_APP, @SIGNATURE)

	SET @return_value = @v_id

	
END	
;

GO


/*	
	Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACADEMIC_TERM'
) DROP PROCEDURE dbo.INSERTA_ACADEMIC_TERM;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACADEMIC_TERM(@P_ACADEMIC_TERM xml, @v_id varchar(255) output) as
BEGIN
	DECLARE @v_a_id varchar(255)
	DECLARE @v_o_id varchar(255)
	DECLARE @v_li_id varchar(255)

	DECLARE @ACADEMIC_YEAR varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
	DECLARE @TOTAL_TERMS varchar(255)
	DECLARE @TERM_NUMBER varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @DESCRIPTION xml
	DECLARE @TEXT_ITEM xml
		
	SELECT 
		@ACADEMIC_YEAR = T.c.value('(academic_year)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@TOTAL_TERMS = T.c.value('(total_terms)[1]', 'varchar(255)'),
		@TERM_NUMBER = T.c.value('(term_number)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_ACADEMIC_TERM.nodes('academic_term') T(c)

	SELECT @v_a_id = ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(SUBSTRING(@ACADEMIC_YEAR, 1, 4))
			AND	UPPER(START_YEAR) =  UPPER(SUBSTRING(@ACADEMIC_YEAR, 6, 4))

	IF (@v_a_id is null)
	BEGIN
		SET @v_a_id = NEWID()
		INSERT INTO dbo.EWPCV_ACADEMIC_YEAR (ID, START_YEAR, END_YEAR)
			VALUES(@v_a_id, SUBSTRING(@ACADEMIC_YEAR, 1, 4), SUBSTRING(@ACADEMIC_YEAR, 6, 4))
	END 


	SELECT @v_o_id = O.ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN dbo.EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)

	SET @v_id = NEWID()

	INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (@v_id, @END_DATE, @START_DATE , LOWER(@INSTITUTION_ID) , @v_o_id, @v_a_id, @TOTAL_TERMS, @TERM_NUMBER)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @TEXT_ITEM
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TEXT_ITEM , @v_li_id output
			INSERT INTO dbo.EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @TEXT_ITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;

GO

/*
	Persiste creditos en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CREDIT'
) DROP PROCEDURE dbo.INSERTA_CREDIT;
GO 
CREATE PROCEDURE  dbo.INSERTA_CREDIT(@P_CREDIT xml, @v_id varchar(255) output) AS
BEGIN
	DECLARE @SCHEME varchar(255)
	DECLARE @CREDIT_VALUE decimal(5,1)

	SELECT 
		@SCHEME = T.c.value('(scheme)[1]', 'varchar(255)'),
		@CREDIT_VALUE = T.c.value('(credit_value)[1]', 'decimal(5,1)')
	FROM @P_CREDIT.nodes('credit') T(c) 
	
	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_CREDIT (ID,SCHEME, CREDIT_VALUE)
		VALUES (@v_id, @SCHEME, @CREDIT_VALUE)

END

;
GO

/*
	Persiste un LOI si procede en el sistema a partir de los datos del LOS
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOI_LA'
) DROP PROCEDURE dbo.INSERTA_LOI_LA;
GO 
CREATE PROCEDURE  dbo.INSERTA_LOI_LA(@P_LOS_ID varchar(255) output, @P_LOS_CODE varchar(255) output, @P_LOS_TYPE integer, @P_START_DATE date, @P_END_DATE date, @P_INSTITUTION_ID varchar(255), @v_loi_id varchar(255) output) AS
BEGIN
	DECLARE @v_loi_prefix varchar(255)
	DECLARE @v_los_prefix varchar(255)
	DECLARE @v_los_id varchar(255)
	DECLARE @v_los_code varchar(255)
	DECLARE @v_los_type varchar(255)

	IF @P_LOS_ID IS NOT NULL 
		BEGIN
			--Obtencion del prefijo del LOI a partir de los datos del LOS
			SET @v_los_prefix = SUBSTRING(@P_LOS_ID,0,4)
			IF @v_los_prefix = 'CR/' 
				SET @v_loi_prefix = 'CRI/'
			ELSE IF @v_los_prefix = 'CLS' 
				SET @v_loi_prefix = 'CLSI/'
			ELSE IF @v_los_prefix = 'MOD' 
				SET @v_loi_prefix = 'MODI/'
			ELSE IF @v_los_prefix = 'DEP' 
				SET @v_loi_prefix = 'DEPI/'
				
			--Obtencion del LOS si existe para relacionar el LOI
			SELECT @v_los_id = ID, @v_los_code = LOS_CODE FROM EWPCV_LOS WHERE ID = @P_LOS_ID AND INSTITUTION_ID = @P_INSTITUTION_ID
		END
	ELSE IF @P_LOS_CODE IS NOT NULL 
		BEGIN
			--Obtencion del LOS si existe para relacionar el LOI
			SELECT @v_los_id = ID, @v_los_code = LOS_CODE, @v_los_type = "type" FROM EWPCV_LOS WHERE LOS_CODE = @P_LOS_CODE AND INSTITUTION_ID = @P_INSTITUTION_ID
			
			IF @v_los_type IS NULL 
				SET @v_los_type = @P_LOS_TYPE
				
			IF @v_los_type IS NOT NULL 
			BEGIN
				--Obtencion del prefijo del LOI a partir de los datos del LOS
				IF @v_los_type  = 0 
					SET @v_loi_prefix = 'CRI/'
				ELSE IF @v_los_type  = 1
					SET @v_loi_prefix = 'CLSI/'
				ELSE IF @v_los_type  = 2
					SET @v_loi_prefix = 'MODI/'
				ELSE IF @v_los_type  = 3
					SET @v_loi_prefix = 'DEPI/'
			END			
		END
	
	--Insertamos el LOI y su relacion con el los si este existe	
	IF @v_loi_prefix IS NOT NULL 
	BEGIN
		SET @v_loi_id = CONCAT(@v_loi_prefix, NEWID())
		--informamos id y fechas
		INSERT INTO dbo.EWPCV_LOI (ID, START_DATE, END_DATE) VALUES (@v_loi_id, @P_START_DATE, @P_END_DATE)
		--si el los estaba en bbdd insertamos la realacion LOS_LOI
		IF @v_los_id IS NOT NULL 
		BEGIN
			INSERT INTO dbo.EWPCV_LOS_LOI (LOI_ID, LOS_ID) VALUES (@v_loi_id,@v_los_id)
		END
	END

	--Si hemos encontrado algo en BBDD seteamos la informacion para completar el componente
	IF @v_los_id IS NOT NULL 
		SET @P_LOS_ID = @v_los_id 
	IF @v_los_code IS NOT NULL 
		SET @P_LOS_CODE = @v_los_code
END
;
GO


/*
	Persiste un componente en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_COMPONENTE'
) DROP PROCEDURE dbo.INSERTA_COMPONENTE;
GO 
CREATE PROCEDURE  dbo.INSERTA_COMPONENTE(@P_COMPONENT xml, @P_LA_ID varchar(255), @P_LA_REVISION integer,  @v_id varchar(255) output) AS

BEGIN
	DECLARE @v_at_id varchar(255)
	DECLARE	@v_c_id varchar(255)
	DECLARE @v_loi_id varchar(255)
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @LOS_ID varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @LOS_TYPE integer
	DECLARE @STATUS integer
	DECLARE @TITLE varchar(255)
	DECLARE @REASON_CODE integer
	DECLARE @REASON_TEXT varchar(255)
	DECLARE @LA_COMPONENT_TYPE integer
	DECLARE @RECOGNITION_CONDITIONS varchar(255)
	DECLARE @SHORT_DESCRIPTION varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @CREDIT xml
	DECLARE @ACADEMIC_TERM xml
	   
	SELECT
		@LOS_ID = T.c.value('(los_id)[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('(los_code)[1]', 'varchar(255)'),
		@LOS_TYPE = T.c.value('(los_type)[1]', 'integer'),
		@STATUS = T.c.value('(status)[1]', 'integer'),
		@TITLE = T.c.value('(title)[1]', 'varchar(255)'),
		@REASON_CODE = T.c.value('(reason_code)[1]', 'integer'),
		@REASON_TEXT = T.c.value('(reason_text)[1]', 'varchar(255)'),
		@LA_COMPONENT_TYPE = T.c.value('(la_component_type)[1]', 'integer'),
		@RECOGNITION_CONDITIONS = T.c.value('(recognition_conditions)[1]', 'varchar(255)'),
		@SHORT_DESCRIPTION = T.c.value('(short_description)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@CREDIT = T.c.query('credit'),
		@ACADEMIC_TERM = T.c.query('academic_term')
	FROM @P_COMPONENT.nodes('la_component') T(c)
	SET @v_id = NEWID()

	EXECUTE dbo.INSERTA_ACADEMIC_TERM @ACADEMIC_TERM, @v_at_id output

	
	---- obtencion de fechas del LOI si vienen en el componente se cogen esas si no del academic term
		IF @START_DATE IS NULL 
		SELECT 
			@START_DATE = T.c.value('(start_date)[1]', 'date')
		FROM @ACADEMIC_TERM.nodes('academic_term') T(c)
		
		IF @END_DATE IS NULL 
		SELECT 
			@END_DATE = T.c.value('(end_date)[1]', 'date')
		FROM @ACADEMIC_TERM.nodes('academic_term') T(c)
	-- obtenemos el institution_id el sending si es reconocido o el receiving si cualquier otro tipo
		IF @LA_COMPONENT_TYPE = 1
			SELECT @INSTITUTION_ID = M.SENDING_INSTITUTION_ID 
			FROM dbo.EWPCV_MOBILITY M 
			INNER JOIN dbo.EWPCV_MOBILITY_LA ML ON M.MOBILITY_ID = ML.MOBILITY_ID
			WHERE ML.LEARNING_AGREEMENT_ID = @P_LA_ID AND ML.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
		ELSE
			SELECT @INSTITUTION_ID = M.RECEIVING_INSTITUTION_ID 
			FROM dbo.EWPCV_MOBILITY M 
			INNER JOIN dbo.EWPCV_MOBILITY_LA ML ON M.ID = ML.MOBILITY_ID
			WHERE ML.LEARNING_AGREEMENT_ID = @P_LA_ID AND ML.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			
	EXECUTE dbo.INSERTA_LOI_LA @LOS_ID output, @LOS_CODE output, @LOS_TYPE, @START_DATE, @END_DATE, @INSTITUTION_ID, @v_loi_id output
	
	INSERT INTO dbo.EWPCV_LA_COMPONENT (ID, ACADEMIC_TERM_DISPLAY_NAME, LOI_ID, LOS_CODE, LOS_ID, STATUS, TITLE, REASON_CODE, REASON_TEXT, LA_COMPONENT_TYPE, RECOGNITION_CONDITIONS, SHORT_DESCRIPTION)
			VALUES (@v_id, @v_at_id, @v_loi_id, @LOS_CODE, @LOS_ID, @STATUS, @TITLE, @REASON_CODE, @REASON_TEXT,
				@LA_COMPONENT_TYPE, @RECOGNITION_CONDITIONS, @SHORT_DESCRIPTION)

	EXECUTE dbo.INSERTA_CREDIT @CREDIT, @v_c_id output
	INSERT INTO dbo.EWPCV_COMPONENT_CREDITS (COMPONENT_ID, CREDITS_ID) VALUES (@v_id, @v_c_id)

	IF @LA_COMPONENT_TYPE = 0
		INSERT INTO dbo.EWPCV_STUDIED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, STUDIED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 1
		INSERT INTO dbo.EWPCV_RECOGNIZED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, RECOGNIZED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 2
		INSERT INTO dbo.EWPCV_VIRTUAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, VIRTUAL_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 3
		INSERT INTO dbo.EWPCV_BLENDED_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, BLENDED_LA_COMPONENT_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

	IF @LA_COMPONENT_TYPE = 4
		INSERT INTO dbo.EWPCV_DOCTORAL_LA_COMPONENT (LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION, DOCTORAL_LA_COMPONENTS_ID)
				VALUES(@P_LA_ID, @P_LA_REVISION, @v_id)

END
;
GO

/*
	Persiste una revision de un learning agreement.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LA_REVISION'
) DROP PROCEDURE dbo.INSERTA_LA_REVISION;
GO 
CREATE PROCEDURE  dbo.INSERTA_LA_REVISION(@P_LA_ID varchar(255), @P_LA xml, @P_LA_REVISION integer, @P_MOBILITY_REVISION integer, @return_value varchar(255) output)  AS 

BEGIN
	DECLARE @v_la_id varchar(255)
	DECLARE @v_stud_sign_id varchar(255)
	DECLARE @v_s_hei_sign_id varchar(255)
	DECLARE @v_changes_id varchar(300)
	DECLARE @v_c_id varchar(255)
	DECLARE @v_mob_internal_id varchar(255)

	DECLARE @MOBILITY_ID varchar(255)
	DECLARE @STUDENT_SIGNATURE xml
	DECLARE @SENDING_HEI_SIGNATURE xml
	DECLARE @LA_COMPONENT xml

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'varchar(255)'),
		@STUDENT_SIGNATURE = T.c.query('student_signature'),
		@SENDING_HEI_SIGNATURE = T.c.query('sending_hei_signature')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.INSERTA_SIGNATURE @STUDENT_SIGNATURE, @v_stud_sign_id output
	EXECUTE dbo.INSERTA_SIGNATURE @SENDING_HEI_SIGNATURE, @v_s_hei_sign_id output

	IF @P_LA_ID IS NULL  
		SET @v_la_id = NEWID()
	ELSE 
		SET @v_la_id = @P_LA_ID

	SET @v_changes_id = CONVERT(varchar(255),@v_la_id ) + '-' + CONVERT(varchar(255),@P_LA_REVISION)

	INSERT INTO dbo.EWPCV_LEARNING_AGREEMENT (ID, LEARNING_AGREEMENT_REVISION, STUDENT_SIGN, SENDER_COORDINATOR_SIGN, STATUS, MODIFIED_DATE, CHANGES_ID)
			VALUES (@v_la_id, @P_LA_REVISION, @v_stud_sign_id, @v_s_hei_sign_id, 0, SYSDATETIME(), @v_changes_id);

	SELECT @v_mob_internal_id = MOBILITY_ID FROM dbo.EWPCV_MOBILITY WHERE ID = @MOBILITY_ID AND MOBILITY_REVISION = @P_MOBILITY_REVISION

	INSERT INTO dbo.EWPCV_MOBILITY_LA (MOBILITY_ID, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)
			VALUES(@v_mob_internal_id, @v_la_id, @P_LA_REVISION);

    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
        VALUES (NEWID(), @v_la_id, @P_LA_REVISION, @MOBILITY_ID, 4, 0);

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_LA.nodes('learning_agreement/la_component_list/la_component') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @LA_COMPONENT
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_COMPONENTE @LA_COMPONENT, @v_la_id, @P_LA_REVISION, @v_c_id output
			FETCH NEXT FROM cur INTO @LA_COMPONENT
		END
	CLOSE cur
	DEALLOCATE cur
	
	SET @return_value = @v_la_id

END 
; 
GO

/*
	Actualiza una movilidad 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_MOBILITY'
) DROP PROCEDURE dbo.ACTUALIZA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.ACTUALIZA_MOBILITY(@P_OMOBILITY_ID varchar(255), @P_MOBILITY xml) AS 
BEGIN

    DECLARE @v_internal_id varchar(255)
    DECLARE @v_prev_internal_id varchar(255)

	DECLARE @v_m_revision VARCHAR(255);
	SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID;
	
	--insertamos la nueva revision de la mobility
	EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, @v_m_revision, @P_OMOBILITY_ID OUTPUT

	SELECT @v_prev_internal_id = ID FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
	SELECT @v_internal_id = ID FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = (@v_m_revision + 1)
	
	-- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
    UPDATE dbo.EWPCV_MOBILITY_LA SET MOBILITY_ID = @v_internal_id WHERE MOBILITY_ID = @v_prev_internal_id;

    DECLARE @v_la_id VARCHAR(255)
    DECLARE @v_la_revision INTEGER

    DECLARE cur CURSOR LOCAL FOR
        SELECT LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION FROM dbo.EWPCV_MOBILITY_LA WHERE MOBILITY_ID = @v_internal_id
    OPEN cur
    FETCH NEXT FROM cur INTO @v_la_id, @v_la_revision
    WHILE @@FETCH_STATUS = 0
    BEGIN
        INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
                VALUES (NEWID(), @v_la_id, @v_la_revision, @P_OMOBILITY_ID, 4, 1);
    END
    CLOSE cur
    DEALLOCATE cur

END 
;
GO


/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_LA'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_LA;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_LA(@P_LA_ID VARCHAR(255), @P_LA_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY M
			INNER JOIN dbo.EWPCV_MOBILITY_LA MLA 
			ON MLA.MOBILITY_ID = M.MOBILITY_ID
		WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de una mobility outgoing
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_OMOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_OMOB;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_OMOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de una mobility incoming
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_IMOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_IMOB;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_IMOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_s_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_r_hei)
		ELSE 
			SET @return_value = null

END
;
GO



/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
	Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
	Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_MOBILITY'
) DROP PROCEDURE dbo.INSERT_MOBILITY;
GO 
CREATE PROCEDURE dbo.INSERT_MOBILITY(@P_MOBILITY xml, @P_OMOBILITY_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer
	
	IF @P_HEI_TO_NOTIFY IS NULL
	BEGIN
	  SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios';
	  SET @return_value = -1
	END
    ELSE
	BEGIN
		EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
		SELECT
			@v_sending_hei = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]', 'integer')
		FROM @P_MOBILITY.nodes('mobility') T(c)	

		IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY)
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.'
			SET @return_value = -1
		END

		IF @return_value = 0
		BEGIN
			EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
		END 

		IF @return_value = 0
		BEGIN
			SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)
			EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
		END 

		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION
				EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, -1, @P_OMOBILITY_ID OUTPUT
				EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_MOBILITY'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
END
;
GO


/* Elimina una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a eliminar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_MOBILITY'
) DROP PROCEDURE dbo.DELETE_MOBILITY;
GO 
CREATE PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_m_revision integer
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		SET @return_value = 0
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count >0
			BEGIN
				SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.'
					SET @return_value = -1
				END
				ELSE 
				BEGIN
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA
					INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
					INNER JOIN dbo.EWPCV_MOBILITY M ON M.MOBILITY_ID = MLA.MOBILITY_ID
					WHERE M.ID = @P_OMOBILITY_ID
						AND LA.TOR_ID IS NOT NULL;
					IF @v_count > 0
					BEGIN
						SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
						SET @return_value = -1
					END
				END
				IF @return_value = 0
				BEGIN
					EXECUTE dbo.BORRA_MOBILITY @P_OMOBILITY_ID
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END
			END
		ELSE
			BEGIN
				SET @P_ERROR_MESSAGE = 'La movilidad no existe'
				SET @return_value = -1
			END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_MOBILITY'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/* Actualiza una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a actualizar
	Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_MOBILITY'
) DROP PROCEDURE dbo.UPDATE_MOBILITY;
GO 
CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID varchar(255) OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer
	DECLARE @v_count integer
	DECLARE @v_la_count integer

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 		
		BEGIN
			SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
			SET @return_value = -1
		END
		ELSE 
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA
				INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
			    INNER JOIN dbo.EWPCV_MOBILITY M ON M.MOBILITY_ID = MLA.MOBILITY_ID
				WHERE M.ID = @P_OMOBILITY_ID
					AND LA.TOR_ID IS NOT NULL;
			IF @v_count > 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
				SET @return_value = -1
			END
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)

		SELECT
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]','integer')
		FROM @P_MOBILITY.nodes('/mobility') T(c) 

		EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
	END 
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			
			SELECT @v_la_count = COUNT(1) FROM dbo.EWPCV_MOBILITY_LA MLA
			    INNER JOIN dbo.EWPCV_MOBILITY M ON M.MOBILITY_ID = MLA.MOBILITY_ID
			    WHERE LOWER(M.ID) = LOWER(@P_OMOBILITY_ID);
			IF @v_la_count > 0 
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_sending_hei
				END
			ELSE 
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei
				END;
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
	Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
	El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
	Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.INSERT_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.INSERT_LEARNING_AGREEMENT(@P_LA XML, @P_LA_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
		DECLARE @SENDING_INSTITUTION_ID varchar(255)
		DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
		DECLARE @v_notifier_hei varchar(255)
		DECLARE @v_m_revision integer
		DECLARE @v_la_id varchar(255)
		DECLARE @MOBILITY_ID varchar(255)
		DECLARE @v_count integer
		DECLARE @v_status integer

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'varchar(255)')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output
	
	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
					FROM dbo.EWPCV_MOBILITY 
					WHERE ID = @MOBILITY_ID
		IF @v_m_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada no existe'
			SET @return_value = -1
		END
	END

		IF @return_value = 0
		BEGIN
			SELECT @v_la_id = LEARNING_AGREEMENT_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
			INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
			WHERE M.ID = @MOBILITY_ID
			IF @v_la_id IS NOT NULL
			BEGIN
				SET @v_count = 0
				DECLARE C_LA CURSOR FOR
				SELECT LEARNING_AGREEMENT_ID
				FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
				WHERE M.ID = @MOBILITY_ID

				OPEN C_LA
				FETCH NEXT FROM C_LA INTO @v_la_id
				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @v_count = @v_count + 1
					FETCH NEXT FROM C_LA INTO @v_la_id
				END
				CLOSE C_LA
				DEALLOCATE C_LA

				IF @v_count = 1
				BEGIN
					SELECT @v_status = STATUS
					FROM dbo.EWPCV_LEARNING_AGREEMENT
					WHERE ID = @v_la_id

					UPDATE dbo.EWPCV_LEARNING_AGREEMENT
					SET STATUS = 4
					WHERE ID = @v_la_id
				END
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: ' + CAST(@v_la_id as varchar(255)) + '.'
					SET @return_value = -1
				END
			END
		END
		IF @return_value = 0
		BEGIN
			SELECT @v_la_id = LEARNING_AGREEMENT_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
			INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
			WHERE M.ID = @MOBILITY_ID
			IF @v_la_id IS NOT NULL
			BEGIN
				SET @v_count = 0
				DECLARE C_LA CURSOR FOR
				SELECT LEARNING_AGREEMENT_ID
				FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
				WHERE M.ID = @MOBILITY_ID

				OPEN C_LA
				FETCH NEXT FROM C_LA INTO @v_la_id
				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @v_count = @v_count + 1
					FETCH NEXT FROM C_LA INTO @v_la_id
				END
				CLOSE C_LA
				DEALLOCATE C_LA

				IF @v_count = 1
				BEGIN
					SELECT @v_status = STATUS
					FROM dbo.EWPCV_LEARNING_AGREEMENT
					WHERE ID = @v_la_id

					UPDATE dbo.EWPCV_LEARNING_AGREEMENT
					SET STATUS = 4
					WHERE ID = @v_la_id
				END
				ELSE
				BEGIN
					SET @P_ERROR_MESSAGE = 'Ya existe un Learning Agreement asociado a la movilidad indicada, por favor actualice o elimine el learning agreement: ' + CAST(@v_la_id as varchar(255)) + '.'
					SET @return_value = -1
				END
			END
		END

		IF @return_value = 0
		BEGIN
			-- Validamos que los institution y ounit de los componentes sean correctos
			EXECUTE dbo.VALIDA_DATOS_LA @P_LA, @P_ERROR_MESSAGE output, @return_value output
		END

		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_LA_REVISION NULL, @P_LA, 0, @v_m_revision, @P_LA_ID output
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion receptora'
					SET @P_LA_ID = NULL
					SET @return_value = -1
					ROLLBACK TRANSACTION
				END 
				ELSE
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					COMMIT TRANSACTION
				END
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_LEARNING_AGREEMENT'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
	;
	GO


/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
	Recibe como parametro el identificador del learning agreement a actualizar.
	El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
	Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT(@P_LA_ID varchar(255), @P_LA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_la_id varchar(255)
	DECLARE @v_la_revision integer
	DECLARE @v_m_revision integer
	DECLARE @v_no_firmado integer
	DECLARE @MOBILITY_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_la_revision_siguiente integer
	DECLARE @v_count integer


	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'varchar(255)')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END
	END

	-- Validacion Componente LA ya tiene una FirstVersion y el STATUS del Componente NO puede ser nulo
    IF @return_value = 0
    BEGIN
        -- Verificar si existe al menos un LA con STATUS = 1
        SELECT @v_la_status_count = COUNT(*)
        FROM ewp.dbo.EWPCV_LEARNING_AGREEMENT
        WHERE ID = @P_LA_ID AND STATUS = 1;

        IF @v_la_status_count > 0
        BEGIN
            DECLARE @i INT = 0;
            DECLARE @component_status INT;
            DECLARE @component_cursor CURSOR;
            SET @component_cursor = CURSOR FOR
                SELECT T.c.value('(status)[1]', 'INT')
                FROM @P_LA.nodes('learning_agreement/components/component') T(c);

            OPEN @component_cursor;
            FETCH NEXT FROM @component_cursor INTO @component_status;

            WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @component_status IS NULL
                BEGIN
                    SET @P_ERROR_MESSAGE = 'El status del componente no puede ser nulo.'
                    SET @return_value = -1
                    CLOSE @component_cursor;
                    DEALLOCATE @component_cursor;
                    RETURN;
                END
                FETCH NEXT FROM @component_cursor INTO @component_status;
            END

            CLOSE @component_cursor;
            DEALLOCATE @component_cursor;
        END
    END
	   	 
	IF @return_value = 0
	BEGIN
		SELECT @v_la_revision = MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
		IF @v_la_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado no existe'
			SET @return_value = -1
		END
	END
	
	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
			FROM dbo.EWPCV_MOBILITY_LA MLA
			INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
				WHERE M.ID = @MOBILITY_ID
				AND MLA.LEARNING_AGREEMENT_ID = @P_LA_ID
				AND MLA.LEARNING_AGREEMENT_REVISION = @v_la_revision;
		IF @v_m_revision IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada para el learning agreement no es correcta.'
			SET @return_value = -1
		END 
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_no_firmado = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND STATUS = 0
		IF @v_no_firmado > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.'
			SET @return_value = -1
		END 
	END


	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @v_la_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1
	END
	
	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND TOR_ID IS NOT NULL;
		IF @v_count > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se puede actualizar un LA que ya tengan un TOR'
			SET @return_value = -1
		END
	END
	
	IF @return_value = 0
	BEGIN
		--validamos que los institution y ounit de los componentes sean correctos
		EXECUTE dbo.VALIDA_DATOS_LA @P_LA, @P_ERROR_MESSAGE output, @return_value output
	END
	
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_la_revision_siguiente = @v_la_revision + 1
			EXECUTE dbo.INSERTA_LA_REVISION @P_LA_ID, @P_LA, @v_la_revision_siguiente, @v_m_revision, @v_la_id output
			EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					   			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/* Aprueba una revision de un learning agreement, se empleará para aprobar los las de tipo incoming.
	Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua la aprobacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACCEPT_LEARNING_AGREEMENT_PROPOSAL'
) DROP PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL;
GO 
CREATE PROCEDURE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID varchar(255), @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id varchar(255)
	DECLARE @v_sign_id varchar(255)
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_ACCEPT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.MOBILITY_ID = MLA.MOBILITY_ID
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT @v_mobility_id = M.ID FROM dbo.EWPCV_MOBILITY M INNER JOIN dbo.EWPCV_MOBILITY_LA MLA ON MLA.MOBILITY_ID = M.MOBILITY_ID
		    WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] ('+@sending_hei_id+') no es coherente con los datos registrados ('+@v_sending_hei_id+').'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] ('+@mobility_id+') no es coherente con los datos registrados ('+@v_mobility_id+').'
		END

	END


	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL, @P_LA_ID, @P_LA_REVISION
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;

;
GO

/* Rechaza una revision de un learning agreement, se empleará para rechazar los las de tipo incoming.
	Recibe como parametros el identificador y la revision del learning agreement a aprobar, asi como la firma del coordinador que efectua el rechazo y un campo de observaciones con el motivo del rechazo.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REJECT_LEARNING_AGREEMENT_PROPOSAL'
) DROP PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL;
GO 
CREATE PROCEDURE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL(@P_LA_ID varchar(255), @P_LA_REVISION integer, @P_PROPOSAL XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN


	DECLARE @v_msg_val_firma varchar(255)
	DECLARE @v_err_val_firma integer
	DECLARE @v_la_id varchar(255)
	DECLARE @v_sign_id varchar(255)
	DECLARE @v_changes_id varchar(255)

	DECLARE @v_mobility_id varchar(255)
	DECLARE @v_sending_hei_id varchar(255)	
	DECLARE @mobility_id varchar(255)
	DECLARE @sending_hei_id varchar(255)

	SET @return_value = 0
	SET @P_ERROR_MESSAGE = ''

	IF @P_LA_ID IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_ID'
	END
	IF @P_LA_REVISION IS NULL  
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_LA_REVISION'
	END

	-- VALIDAR FIRMA
	EXECUTE dbo.VALIDA_COMMENT_PROPOSAL @P_PROPOSAL, @v_msg_val_firma output, @v_err_val_firma output
	IF @v_err_val_firma <> 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:' + @v_msg_val_firma
	END
	ELSE
	BEGIN
		-- VALORES sending-hei-id y omobility-id

		;WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req)
		SELECT
			@sending_hei_id = T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
			@mobility_id = T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)')
		FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

		SELECT @v_sending_hei_id = M.SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY_LA MLA
				INNER JOIN dbo.EWPCV_MOBILITY M	ON M.MOBILITY_ID = MLA.MOBILITY_ID
			WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID 
				AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		SELECT @v_mobility_id = M.ID FROM dbo.EWPCV_MOBILITY M INNER JOIN dbo.EWPCV_MOBILITY_LA MLA ON MLA.MOBILITY_ID = M.MOBILITY_ID
        		    WHERE LEARNING_AGREEMENT_ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
        			ORDER BY MOBILITY_REVISION DESC

		IF (@v_sending_hei_id <> @sending_hei_id) OR (@v_mobility_id <> @mobility_id)
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' -P_SIGNATURE:'
			IF @v_sending_hei_id <> @sending_hei_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [sending-hei-id] no es coherente con los datos registrados.'
			IF @v_mobility_id <> @mobility_id
				 SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + ' El valor de [omobility-id] no es coherente con los datos registrados.'
		END

	END
		
	IF @return_value <> 0
		SET @P_ERROR_MESSAGE = 'Faltan los siguientes campos obligatorios: ' + @P_ERROR_MESSAGE

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_sign_id = RECEIVER_COORDINATOR_SIGN, @v_changes_id = CHANGES_ID
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = @P_LA_REVISION;
		IF @v_la_id IS NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'	
		END 
		ELSE
		IF @v_sign_id IS NOT NULL
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = 'La revision del Learning Agreement ya esta firmada'
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @P_ERROR_MESSAGE = null
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.INSERTA_REJECT_REQUEST_PROPOSAL @v_sending_hei_id, @P_PROPOSAL, @P_LA_ID, @P_LA_REVISION
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END;
GO


/*

Guarda el xml de update la de tipo aceptacion y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACCEPT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACCEPT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml, @P_LA_ID varchar(255), @P_LA_REVISION integer) AS

BEGIN

	DECLARE @v_id varchar(255)
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)  
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 0, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME(), @P_LA_ID, @P_LA_REVISION)

	INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_id, 11, 0)

END
;
GO


/*

Guarda el xml de update la de tipo rechazo y lo persiste en la tabla de update requests

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_REJECT_REQUEST_PROPOSAL'
) DROP PROCEDURE dbo.INSERTA_REJECT_REQUEST_PROPOSAL;
GO 
CREATE PROCEDURE  dbo.INSERTA_REJECT_REQUEST_PROPOSAL(@P_SENDING_HEI varchar(255), @P_PROPOSAL xml, @P_LA_ID varchar(255), @P_LA_REVISION integer) AS

BEGIN

	DECLARE @v_id varchar(255)
	SET @v_id = NEWID()

	INSERT INTO EWPCV_MOBILITY_UPDATE_REQUEST(ID, SENDING_HEI_ID, "TYPE", MOBILITY_TYPE, UPDATE_INFORMATION, UPDATE_REQUEST_DATE, LEARNING_AGREEMENT_ID, LEARNING_AGREEMENT_REVISION)  
			VALUES (@v_id, LOWER(@P_SENDING_HEI), 1, 1, CONVERT(VARCHAR(MAX),@P_PROPOSAL), SYSDATETIME(), @P_LA_ID, @P_LA_REVISION)

    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_id, 11, 0)

END
;
GO



/*
valida el xml de tipo rechazo de learning agreement

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_COMMENT_PROPOSAL'
) DROP PROCEDURE dbo.VALIDA_COMMENT_PROPOSAL;
GO 
CREATE PROCEDURE  VALIDA_COMMENT_PROPOSAL(@P_PROPOSAL xml,  @errMsg varchar(255) output, @errNum integer output) as 
BEGIN

	DECLARE @sid varchar(255),  @mid varchar(255),  @cid varchar(255),  @comment varchar(255)
	DECLARE @signam varchar(255),  @sigpos varchar(255), @sigmail varchar(255),  @sigtime varchar(255),  @sigapp varchar(255)
	;
	WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req,
		'https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd' as la)

	SELECT
		@sid=T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
		@mid=T.c.value('(req:comment-proposal-v1/req:omobility-id)[1]', 'varchar(255)'),
		@cid=T.c.value('(req:comment-proposal-v1/req:changes-proposal-id)[1]', 'varchar(255)'),
		@comment=T.c.value('(req:comment-proposal-v1/req:comment)[1]', 'varchar(255)'),
		@signam=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-name)[1]', 'varchar(255)'),
		@sigpos=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-position)[1]', 'varchar(255)'),
		@sigmail=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-email)[1]', 'varchar(255)'),
		@sigtime=T.c.value('(req:comment-proposal-v1/req:signature/la:timestamp)[1]', 'varchar(255)'),
		@sigapp=T.c.value('(req:comment-proposal-v1/req:signature/la:signer-app)[1]', 'varchar(255)')
	FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

	IF @sid is null or @sid = ''
		SET @errMsg = @errMsg + ' sending-hei-id'
	IF @mid is null or @mid = ''
		SET @errMsg = @errMsg + ' omobility-id'
	IF @cid is null or @cid = ''
		SET @errMsg = @errMsg + ' changes-proposal-id'
	IF @comment is null or @comment = ''
		SET @errMsg = @errMsg + ' comment'
	IF @signam is null or @signam = ''
		SET @errMsg = @errMsg + ' signer-name'
	IF @sigpos is null or @sigpos = ''
		SET @errMsg = @errMsg + ' signer-position'
	IF @sigmail is null or @sigmail = ''
		SET @errMsg = @errMsg + ' signer-email'
	IF @sigtime is null or @sigtime = ''
		SET @errMsg = @errMsg + ' timestamp'
	IF @sigapp is null or @sigapp = ''
		SET @errMsg = @errMsg + ' signer-app'

	if @errMsg is null
		set @errNum = 0
	ELSE
	BEGIN
		set @errNum = -1
		set @errMsg = ' Valores nulos en: ' + @errMsg + ']' 
	END

END
;
GO





/*
valida el xml de tipo aceptacion de learning agreement

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_ACCEPT_PROPOSAL'
) DROP PROCEDURE dbo.VALIDA_ACCEPT_PROPOSAL;
GO 
CREATE PROCEDURE  VALIDA_ACCEPT_PROPOSAL(@P_PROPOSAL xml,  @errMsg varchar(255) output, @errNum integer output) as 
BEGIN

	DECLARE @sid varchar(255),  @mid varchar(255),  @cid varchar(255),  @comment varchar(255)
	DECLARE @signam varchar(255),  @sigpos varchar(255), @sigmail varchar(255),  @sigtime varchar(255),  @sigapp varchar(255)
	;
	WITH XMLNAMESPACES ('https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd' as req,
		'https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd' as la)

	SELECT
		@sid=T.c.value('(req:sending-hei-id)[1]', 'varchar(255)'),
		@mid=T.c.value('(req:approve-proposal-v1/req:omobility-id)[1]', 'varchar(255)'),
		@cid=T.c.value('(req:approve-proposal-v1/req:changes-proposal-id)[1]', 'varchar(255)'),
		@signam=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-name)[1]', 'varchar(255)'),
		@sigpos=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-position)[1]', 'varchar(255)'),
		@sigmail=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-email)[1]', 'varchar(255)'),
		@sigtime=T.c.value('(req:approve-proposal-v1/req:signature/la:timestamp)[1]', 'varchar(255)'),
		@sigapp=T.c.value('(req:approve-proposal-v1/req:signature/la:signer-app)[1]', 'varchar(255)')
	FROM @P_PROPOSAL.nodes('req:omobility-las-update-request') T(c)

	IF @sid is null or @sid = ''
		SET @errMsg = @errMsg + ' sending-hei-id'
	IF @mid is null or @mid = ''
		SET @errMsg = @errMsg + ' omobility-id'
	IF @cid is null or @cid = ''
		SET @errMsg = @errMsg + ' changes-proposal-id'
	IF @comment is null or @comment = ''
		SET @errMsg = @errMsg + ' comment'
	IF @signam is null or @signam = ''
		SET @errMsg = @errMsg + ' signer-name'
	IF @sigpos is null or @sigpos = ''
		SET @errMsg = @errMsg + ' signer-position'
	IF @sigmail is null or @sigmail = ''
		SET @errMsg = @errMsg + ' signer-email'
	IF @sigtime is null or @sigtime = ''
		SET @errMsg = @errMsg + ' timestamp'
	IF @sigapp is null or @sigapp = ''
		SET @errMsg = @errMsg + ' signer-app'

	if @errMsg is null
		set @errNum = 0
	ELSE
	BEGIN
		set @errNum = -1
		set @errMsg = ' Valores nulos en: ' + @errMsg + ']' 
	END

END
;
GO


/* Elimina un learning agreement del sistema.
	Recibe como parametro el identificador del learning agreement a actualizar
	Retorna el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.DELETE_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.DELETE_LEARNING_AGREEMENT(@P_LA_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @LEARNING_AGREEMENT_REVISION integer
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_m_id varchar(255)
	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY IS NULL  
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
		IF @v_count = 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El Learning Agreement no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND TOR_ID IS NOT NULL;
		IF @v_count > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden borrar LA que ya tengan un TOR'
			SET @return_value = -1
		END
	END
	
	SELECT @LEARNING_AGREEMENT_REVISION = MAX(LEARNING_AGREEMENT_REVISION) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden borrar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden borrar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1	
	END
	
	IF @return_value = 0
	BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		--cursor aqui para cada revision	
		DECLARE cur CURSOR LOCAL FOR
		SELECT LEARNING_AGREEMENT_REVISION FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID
		
		OPEN cur
		FETCH NEXT FROM cur INTO  @LEARNING_AGREEMENT_REVISION 
		WHILE @@FETCH_STATUS = 0
			BEGIN
				
				IF @v_m_id is NULL
					SELECT @v_m_id = M.ID
						FROM dbo.EWPCV_MOBILITY_LA MLA
						INNER JOIN dbo.EWPCV_MOBILITY M ON M.MOBILITY_ID = MLA.MOBILITY_ID
						WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = @LEARNING_AGREEMENT_REVISION

				EXECUTE dbo.BORRA_LA @P_LA_ID, @LEARNING_AGREEMENT_REVISION
				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
                    VALUES (NEWID(), @P_LA_ID, @LEARNING_AGREEMENT_REVISION, @v_m_id, 4, 2);
				
				FETCH NEXT FROM cur INTO  @LEARNING_AGREEMENT_REVISION 
			END
		CLOSE cur
		DEALLOCATE cur
		
		EXECUTE dbo.INSERTA_NOTIFICATION @v_m_id, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_LEARNING_AGREEMENT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
	END

END 
;
GO


-- Funcion ES_OMOBILITY
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'ES_OMOBILITY'
) DROP FUNCTION dbo.ES_OMOBILITY;
GO  
CREATE FUNCTION dbo.ES_OMOBILITY(@P_MOBILITY XML, @P_HEI_TO_NOTIFY varchar(255)) RETURNS integer
BEGIN
	DECLARE @return_value integer
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	SELECT
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)')
	FROM @P_MOBILITY.nodes('mobility') T(c)	

	IF @RECEIVING_INSTITUTION_ID <> @P_HEI_TO_NOTIFY
		SET @return_value = -1
	ELSE
		SET @return_value = 0
	return @return_value
END
;

GO

-- Procedure VALIDA_STATUS
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_STATUS'
) DROP PROCEDURE dbo.VALIDA_STATUS;
GO 
CREATE PROCEDURE dbo.VALIDA_STATUS(@P_STATUS int, @P_ES_OUTGOING int, @P_ERROR_MESSAGE varchar(255) output, @return_value int output) AS
BEGIN
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	SET @return_value = 0
	IF @P_ES_OUTGOING = 0 
	BEGIN
		IF @P_STATUS NOT in (0, 1, 2, 3)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_outgoing no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING = -1
	BEGIN
		IF @P_STATUS NOT in (4, 5, 6)
		BEGIN
			SET @P_ERROR_MESSAGE = 'El campo status_incoming no tiene un status válido.'
			SET @return_value = -1
		END 
	END

	IF @P_ES_OUTGOING NOT in (-1, 0)
	BEGIN
		SET @P_ERROR_MESSAGE = '@P_ES_OUTGOING debe especificar si es un status_incoming (-1) o status_outgoing (0).'
		SET @return_value = -1
	END 
END
;
GO


	/*
		Valida que existan los institution id y ounit id de la mobility
	*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY_INST'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_MOBILITY_INST(@P_S_INSTITUTION xml, @P_R_INSTITUTION  xml, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS 
BEGIN
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	DECLARE @v_count int
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_OUnit_CODE varchar(255)	
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @RECEIVING_OUnit_CODE varchar(255)

	SET @SENDING_INSTITUTION_ID = ''
	SET @RECEIVING_INSTITUTION_ID = ''
	SET @return_value = 0

	SELECT 
		@SENDING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@SENDING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_S_INSTITUTION.nodes('sending_institution') T(c)	
	SELECT 
		@RECEIVING_INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@RECEIVING_OUnit_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_R_INSTITUTION.nodes('receiving_institution') T(c)

	-- validacion de sender 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@SENDING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @SENDING_INSTITUTION_ID + ' informada como sender institution no existe en el sistema.')
	END 
	ELSE IF @SENDING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@SENDING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@SENDING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @SENDING_OUnit_CODE + 'vinculada a ' + @SENDING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

	-- validacion de receiver 
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@RECEIVING_INSTITUTION_ID)
	IF @v_count = 0 
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La instituticion ' + @RECEIVING_INSTITUTION_ID + ' informada como receiver institution no existe en el sistema.')
	END 
	ELSE IF @RECEIVING_OUnit_CODE IS NOT NULL
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION ins 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou
                ON ins.ID = iou.INSTITUTION_ID
                INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou
                ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
                WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@RECEIVING_INSTITUTION_ID)
                AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@RECEIVING_OUnit_CODE)
		IF @v_count = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' La unidad organizatriva  ' + @RECEIVING_OUnit_CODE + 'vinculada a ' + @RECEIVING_INSTITUTION_ID + ' no existe en el sistema.')
		END 
	END

END
;
GO
	


/* Cancela una movilidad
    Recibe como parametro el identificador de la movilidad
    Admite un parametro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CANCEL_MOBILITY'
) DROP PROCEDURE dbo.CANCEL_MOBILITY;
GO 
CREATE PROCEDURE dbo.CANCEL_MOBILITY(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output)
  AS
   
  BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_resultado integer
	DECLARE @v_count integer
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = count(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count > 0
		BEGIN
		  SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		  EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
		  IF @v_notifier_hei IS NULL 
		  BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.'
			SET @return_value = -1
		  END
	  
		  UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
    
		  EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei

		  INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
		    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
		END
		ELSE
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
  END
;
GO




/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_DEPARTURE'
) DROP PROCEDURE dbo.STUDENT_DEPARTURE;
GO 
CREATE PROCEDURE dbo.STUDENT_DEPARTURE(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
						    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_DEPARTURE'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO



	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
            
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_ARRIVAL'
) DROP PROCEDURE dbo.STUDENT_ARRIVAL;
GO 
CREATE PROCEDURE dbo.STUDENT_ARRIVAL(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
						    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_ARRIVAL'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO



	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'APPROVE_NOMINATION'
) DROP PROCEDURE dbo.APPROVE_NOMINATION;
GO 
CREATE PROCEDURE dbo.APPROVE_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
						    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.APPROVE_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO




	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REJECT_NOMINATION'
) DROP PROCEDURE dbo.REJECT_NOMINATION;
GO 
CREATE PROCEDURE dbo.REJECT_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
						    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO




	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_ACTUAL_DATES'
) DROP PROCEDURE dbo.UPDATE_ACTUAL_DATES;
GO 
CREATE PROCEDURE dbo.UPDATE_ACTUAL_DATES(@P_OMOBILITY_ID varchar(255), @P_ACTUAL_DEPARTURE date, @P_ACTUAL_ARRIVAL date, @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME(), 
							ACTUAL_ARRIVAL_DATE = @P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = @P_ACTUAL_DEPARTURE
							WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, REVISION, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
						    VALUES (NEWID(), @P_OMOBILITY_ID, @v_m_revision, @P_OMOBILITY_ID, 1, 1)
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_ACTUAL_DATES'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO

/*
    Este procedimiento se encarga de actualizar el registro de la tabla LEARNING_AGREEMENT con el estado APPROVE, la fecha de modificacion, las observaciones y la firma del firmante.
    La informacion la tenemos que extraer del xml que esta almacenado en el campo UPDATE_INFORMATION de la tabla EWPCV_MOBILITY_UPDATE_REQUEST
    El xml tiene el siguiente formato:
    <req:omobility-las-update-request xmlns:req="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:la="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd" xsi:schemaLocation="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd">
    	<req:sending-hei-id>uv.es</req:sending-hei-id>
    	<req:approve-proposal-v1>
    		<req:omobility-id>10df6450-ff3e-35d1-e063-011a160aa656</req:omobility-id>
    		<req:changes-proposal-id>10df6450-ff44-35d1-e063-011a160aa656-0</req:changes-proposal-id>
    		<req:signature>
    			<la:signer-name>mr receiver</la:signer-name>
    			<la:signer-position>coordinator</la:signer-position>
    			<la:signer-email> c@c.com</la:signer-email>
    			<la:timestamp>2024-02-08T02:01:57</la:timestamp>
    			<la:signer-app/>
    		</req:signature>
    	</req:approve-proposal-v1>
    </req:omobility-las-update-request>


    Partiremos del ID del registro de la tabla EWPCV_MOBILITY_UPDATE_REQUEST y de ahi procesaremos el xml
    para localizar el registro a actualizar en la tabla EWPCV_LEARNING_AGREEMENT emplearemos el campo LEARNING_AGREEMENT_ID y el LEARNING_AGREEMENT_REVISION que esta en el registro la tabla
    la firma la generamos con el siguiente codigo:
    INSERT INTO EWPCV_SIGNATURE (ID, SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, TIMESTAMP, SIGNER_APP) VALUES (EWP.GENERATE_UUID(), v_signer_name, v_signer_position, v_signer_email, v_timestamp, v_signer_app );
    La tabla a actualizar es EWPCV_LEARNING_AGREEMENT y debemos actualizar los campos STATUS, MODIFIED_DATE,  RECEIVER_COORDINATOR_SIGN
    En este caso el status puede ser 1 (first version) o 2 (approved-changes) dependiendo de si el LA tiene ya otra revision en estado 1 o no.
*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'FIX_LA_APPROVE'
) DROP PROCEDURE dbo.FIX_LA_APPROVE;
GO 
CREATE PROCEDURE dbo.FIX_LA_APPROVE(@P_MOBILITY_UPDATE_REQUEST_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN	
	DECLARE @v_mobility_update_request_id varchar(255) = @P_MOBILITY_UPDATE_REQUEST_ID;
    DECLARE @v_learning_agreement_id varchar(255)
    DECLARE @v_learning_agreement_revision integer
    DECLARE @v_signer_name NVARCHAR(MAX)
    DECLARE @v_signer_position NVARCHAR(MAX)
    DECLARE @v_signer_email NVARCHAR(MAX)
    DECLARE @v_timestamp DATETIME2
    DECLARE @v_signer_app NVARCHAR(MAX)
    DECLARE @v_signature_id varchar(255)
    DECLARE @v_xml XML
    DECLARE @v_status integer
    DECLARE @v_count integer

	SET @return_value = 0

	IF @v_mobility_update_request_id IS NULL
		BEGIN
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + 'No se ha indicado el ID de la tabla EWPCV_MOBILITY_UPDATE_REQUEST';
			SET @return_value = -1
		END

	--Obtenemos el registro a procesar
	SELECT @v_xml = UPDATE_INFORMATION, @v_learning_agreement_id = LEARNING_AGREEMENT_ID, @v_learning_agreement_revision = LEARNING_AGREEMENT_REVISION
	FROM ewp.dbo.EWPCV_MOBILITY_UPDATE_REQUEST WHERE ID = @v_mobility_update_request_id;

	-- Extraer 'signer-name'
	SET @v_signer_name = @v_xml.value('(/*[local-name()="signer-name"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'signer-position'
	SET @v_signer_position = @v_xml.value('(/*[local-name()="signer-position"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'signer-email'
	SET @v_signer_email = @v_xml.value('(/*[local-name()="signer-email"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'timestamp'
	SET @v_timestamp = @v_xml.value('(/*[local-name()="timestamp"])[1]', 'DATETIME2');

	-- Extraer 'signer-app'
	SET @v_signer_app = @v_xml.value('(/*[local-name()="signer-app"])[1]', 'NVARCHAR(MAX)');

	--Generamos la firma
	SET @v_signature_id = NEWID();
	INSERT INTO ewp.dbo.EWPCV_SIGNATURE (ID, SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, TIMESTAMP, SIGNER_APP)
	VALUES (@v_signature_id, @v_signer_name, @v_signer_position, @v_signer_email, @v_timestamp, @v_signer_app);

	--determinamos el estado del LA
	SELECT @v_count = COUNT(*) FROM ewp.dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @v_learning_agreement_id AND STATUS = 1;
	IF @v_count > 0
		SET @v_status = 2;
	ELSE
		SET @v_status = 1;

	--Actualizamos el registro
	UPDATE ewp.dbo.EWPCV_LEARNING_AGREEMENT SET STATUS = @v_status, MODIFIED_DATE = GETDATE(), RECEIVER_COORDINATOR_SIGN = @v_signature_id
	WHERE ID = @v_learning_agreement_id AND LEARNING_AGREEMENT_REVISION = @v_learning_agreement_revision;

	--eliminamos el registro de la tabla EWPCV_MOBILITY_UPDATE_REQUEST
	DELETE FROM ewp.dbo.EWPCV_MOBILITY_UPDATE_REQUEST WHERE ID = @v_mobility_update_request_id;

	PRINT 'Se ha procesado correctamente el registro, no olvides commitear los cambios tras revisarlo en la base de datos';

	COMMIT;

	RETURN @return_value;

END
;
GO


/*
    Este procedimiento se encarga de actualizar el registro de la tabla LEARNING_AGREEMENT con el estado REJECTED, la fecha de modificacion, las observaciones y la firma del firmante.
    La informacion la tenemos que extraer del xml que esta almacenado en el campo UPDATE_INFORMATION de la tabla EWPCV_MOBILITY_UPDATE_REQUEST
    El xml tiene el siguiente formato:
    <req:omobility-las-update-request xmlns:req="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:la="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd" xsi:schemaLocation="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd">
        <req:sending-hei-id>upv.es</req:sending-hei-id>
        <req:comment-proposal-v1>
            <req:omobility-id>10546ce9-2908-4ef6-e063-011a160ae5ec</req:omobility-id>
            <req:changes-proposal-id>10c939f5-3931-97ab-e063-011a160a9a08-0</req:changes-proposal-id>
            <req:comment>rechazo</req:comment>
            <req:signature>
                <la:signer-name>mr receiver</la:signer-name>
                <la:signer-position>coordinator</la:signer-position>
                <la:signer-email> c@c.com</la:signer-email>
                <la:timestamp>2024-02-09T08:20:37</la:timestamp>
                <la:signer-app/>
            </req:signature>
        </req:comment-proposal-v1>
    </req:omobility-las-update-request>


    Partiremos del ID del registro de la tabla EWPCV_MOBILITY_UPDATE_REQUEST y de ahi procesaremos el xml
    para localizar el registro a actualizar en la tabla EWPCV_LEARNING_AGREEMENT emplearemos el campo LEARNING_AGREEMENT_ID y el LEARNING_AGREEMENT_REVISION que esta en el registro la tabla
    la firma la generamos con el siguiente codigo:
    INSERT INTO EWPCV_SIGNATURE (ID, SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, TIMESTAMP, SIGNER_APP) VALUES (EWP.GENERATE_UUID(), v_signer_name, v_signer_position, v_signer_email, v_timestamp, v_signer_app );
    La tabla a actualizar es EWPCV_LEARNING_AGREEMENT y debemos actualizar los campos STATUS = 3, MODIFIED_DATE, COMMENT_REJECT, RECEIVER_COORDINATOR_SIGN
*/


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'FIX_LA_REJECT'
) DROP PROCEDURE dbo.FIX_LA_REJECT;
GO 
CREATE PROCEDURE dbo.FIX_LA_REJECT(@P_MOBILITY_UPDATE_REQUEST_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN	
	DECLARE @v_mobility_update_request_id varchar(255) = @P_MOBILITY_UPDATE_REQUEST_ID;
    DECLARE @v_learning_agreement_id varchar(255)
    DECLARE @v_learning_agreement_revision integer
	DECLARE @v_comment varchar(255)
    DECLARE @v_signer_name NVARCHAR(MAX)
    DECLARE @v_signer_position NVARCHAR(MAX)
    DECLARE @v_signer_email NVARCHAR(MAX)
    DECLARE @v_timestamp DATETIME2
    DECLARE @v_signer_app NVARCHAR(MAX)
    DECLARE @v_signature_id varchar(255)
    DECLARE @v_xml XML

	SET @return_value = 0

	IF @v_mobility_update_request_id IS NULL
		BEGIN
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + 'No se ha indicado el ID de la tabla EWPCV_MOBILITY_UPDATE_REQUEST';
			SET @return_value = -1
		END

	--Obtenemos el registro a procesar
	SELECT @v_xml = UPDATE_INFORMATION, @v_learning_agreement_id = LEARNING_AGREEMENT_ID, @v_learning_agreement_revision = LEARNING_AGREEMENT_REVISION
	FROM ewp.dbo.EWPCV_MOBILITY_UPDATE_REQUEST WHERE ID = @v_mobility_update_request_id;

	-- Extraer 'signer-name'
	SET @v_signer_name = @v_xml.value('(/*[local-name()="signer-name"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'signer-position'
	SET @v_signer_position = @v_xml.value('(/*[local-name()="signer-position"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'signer-email'
	SET @v_signer_email = @v_xml.value('(/*[local-name()="signer-email"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'timestamp'
	SET @v_timestamp = @v_xml.value('(/*[local-name()="timestamp"])[1]', 'DATETIME2');

	-- Extraer 'signer-app'
	SET @v_signer_app = @v_xml.value('(/*[local-name()="signer-app"])[1]', 'NVARCHAR(MAX)');

	-- Extraer 'comment'
    SET @v_comment = @v_xml.value('(/*[local-name()="@comment"])[1]', 'NVARCHAR(MAX)');
   
   --Generamos la firma
	SET @v_signature_id = NEWID();
	INSERT INTO ewp.dbo.EWPCV_SIGNATURE (ID, SIGNER_NAME, SIGNER_POSITION, SIGNER_EMAIL, TIMESTAMP, SIGNER_APP)
	VALUES (@v_signature_id, @v_signer_name, @v_signer_position, @v_signer_email, @v_timestamp, @v_signer_app);
	
	--Actualizamos el registro
	UPDATE ewp.dbo.EWPCV_LEARNING_AGREEMENT SET STATUS = 3, MODIFIED_DATE = GETDATE(), COMMENT_REJECT = @v_comment, RECEIVER_COORDINATOR_SIGN = @v_signature_id
	WHERE ID = @v_learning_agreement_id AND LEARNING_AGREEMENT_REVISION = @v_learning_agreement_revision;

	--eliminamos el registro de la tabla EWPCV_MOBILITY_UPDATE_REQUEST
	DELETE FROM ewp.dbo.EWPCV_MOBILITY_UPDATE_REQUEST WHERE ID = @v_mobility_update_request_id;

	PRINT 'Se ha procesado correctamente el registro, no olvides commitear los cambios tras revisarlo en la base de datos';

	COMMIT;

	RETURN @return_value;

END
;

GO


