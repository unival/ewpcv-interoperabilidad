/*
***************************************
******** Funciones FACTSHEET **********
***************************************
*/

/*
	Persiste informacion de contacto 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_DETAIL'
) DROP PROCEDURE dbo.INSERTA_CONTACT_DETAIL;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_DETAIL(@P_URL xml, @P_EMAIL varchar(255), @P_PHONE xml, 
		@v_contact_id varchar(255) OUTPUT) as
BEGIN
	DECLARE @v_tlf_id varchar(255)
	DECLARE @v_litem_id varchar(255)
	DECLARE @xml xml

	IF @P_PHONE is not null
	BEGIN
		EXECUTE dbo.INSERTA_TELEFONO @P_PHONE, @v_tlf_id OUTPUT
	END

	SET @v_contact_id = NEWID()
	INSERT INTO dbo.EWPCV_CONTACT_DETAILS(ID, PHONE_NUMBER) VALUES (@v_contact_id, @v_tlf_id);


	IF @P_EMAIL is not null
	BEGIN
		INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL(CONTACT_DETAILS_ID, EMAIL) VALUES (@v_contact_id, @P_EMAIL)
	END 

	IF @P_URL is not null
	BEGIN
		DECLARE cur CURSOR FOR
		SELECT
			T.c.query('.')
		FROM   @P_URL.nodes('/url/text') T(c) 

		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_litem_id OUTPUT
			INSERT INTO dbo.EWPCV_CONTACT_URL(URL_ID, CONTACT_DETAILS_ID) VALUES(@v_litem_id, @v_contact_id)
			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
	END

END
;
GO

/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FACTSHEET'
) DROP PROCEDURE dbo.INSERTA_FACTSHEET;
GO 
CREATE PROCEDURE  dbo.INSERTA_FACTSHEET(@P_FACTSHEET xml, @P_INFORMATION_ITEM xml, @P_FACTSHEET_ID VARCHAR(255), @return_value varchar(255) OUTPUT) as

BEGIN
	DECLARE	@v_contact_id VARCHAR(255)
	DECLARE	@v_factsheet_id varchar(255)

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('/application_info') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	IF  @P_FACTSHEET_ID IS NULL
		BEGIN
			SET @v_factsheet_id = NEWID()
			INSERT INTO dbo.EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
						VALUES(@v_factsheet_id, 
						@P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
						@P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
						@P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
						@v_contact_id)
            INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION)
                VALUES (NEWID(), @v_factsheet_id, 5, 0)
		END;
	ELSE
		BEGIN
			SET @v_factsheet_id = @P_FACTSHEET_ID
			UPDATE dbo.EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
                TOR_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
                NOMINATIONS_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
                NOMINATIONS_SPRING_TERM = @P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
                APPLICATION_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
                APPLICATION_SPRING_TERM = @P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
                CONTACT_DETAILS_ID = @v_contact_id
                WHERE ID = @v_factsheet_id;
            INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION)
                VALUES (NEWID(), @v_factsheet_id, 5, 1)
		END;
	SET @return_value = @v_factsheet_id

END
;
GO

/*
	Persiste un information item
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INFORMATION_ITEM'
) DROP PROCEDURE dbo.INSERTA_INFORMATION_ITEM;
GO 
CREATE PROCEDURE  dbo.INSERTA_INFORMATION_ITEM(@P_INFORMATION_ITEM xml,
	@P_INF_TYPE VARCHAR(255), @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id varchar(255)
	DECLARE @v_infoitem_id varchar(255)

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('*') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	SET @v_infoitem_id =  NEWID()

	INSERT INTO dbo.EWPCV_INFORMATION_ITEM(ID, "TYPE", CONTACT_DETAIL_ID) VALUES (@v_infoitem_id, @P_INF_TYPE, @v_contact_id)
	INSERT INTO dbo.EWPCV_INST_INF_ITEM(INSTITUTION_ID, INFORMATION_ID) VALUES (@P_INSTITUTION_ID, @v_infoitem_id)

END
;
GO


/*
	Inserta informacion sobre requerimientos
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_REQUIREMENT_INFO'
) DROP PROCEDURE dbo.INSERTA_REQUIREMENT_INFO;
GO 
CREATE PROCEDURE  dbo.INSERTA_REQUIREMENT_INFO(@P_TYPE VARCHAR(255), @P_NAME VARCHAR(255),  @P_DESCRIPTION VARCHAR(255), 
		@P_URL xml,	@P_EMAIL VARCHAR(255), @P_PHONE xml, @P_INSTITUTION_ID VARCHAR(255)) as

BEGIN
	DECLARE @v_contact_id varchar(255)
	DECLARE @v_requinf_id varchar(255)

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @P_URL, @P_EMAIL, @P_PHONE, @v_contact_id OUTPUT

	SET @v_requinf_id = NEWID()
	INSERT INTO EWPCV_REQUIREMENTS_INFO(ID, "TYPE", NAME, DESCRIPTION, CONTACT_DETAIL_ID) 
		VALUES (@v_requinf_id, LOWER(@P_TYPE), @P_NAME, @P_DESCRIPTION, @v_contact_id)
	INSERT INTO EWPCV_INS_REQUIREMENTS(INSTITUTION_ID, REQUIREMENT_ID) 
		VALUES (@P_INSTITUTION_ID, @v_requinf_id)

END
;
GO
		
/*
	Persiste una hoja de contactos asociada a una institucion en el sistema.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.INSERTA_FACTSHEET_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.INSERTA_FACTSHEET_INSTITUTION(@P_FACTSHEET xml) as

BEGIN

	DECLARE @v_id varchar(255)
	DECLARE @v_inst_id varchar(255)
	DECLARE @v_factsheet_id varchar(255)
	DECLARE @v_add_info_id varchar(255)

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c)

	SELECT @v_inst_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);

	EXECUTE dbo.INSERTA_FACTSHEET @FACTSHEET, @APPLICATION_INFO, NULL, @v_factsheet_id OUTPUT
	UPDATE dbo.EWPCV_INSTITUTION SET FACT_SHEET = @v_factsheet_id WHERE ID = @v_inst_id

	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @v_inst_id


	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @v_inst_id
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo


	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @v_inst_id
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @v_inst_id
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO

/* Inserta un factsheet en el sistema
	Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_FACTSHEET'
) DROP PROCEDURE dbo.INSERT_FACTSHEET;
GO 
CREATE PROCEDURE dbo.INSERT_FACTSHEET(@P_FACTSHEET xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer
			DECLARE @INSTITUTION_ID varchar(255)
			DECLARE @FACTSHEET_ID varchar(255)
			
				--validacion existe la institucion indicada
				SET @INSTITUTION_ID = @P_FACTSHEET.value('(factsheet_institution/institution_id)[1]', 'varchar(255)')
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@INSTITUTION_ID); 
				
				IF  @v_count = 0 
				BEGIN
					SET @return_value = -1
					SET @P_ERROR_MESSAGE = 'La institución '+ @INSTITUTION_ID +' vinculada al factsheet no existe en el sistema.'
				END
				ELSE 
				BEGIN
					SELECT @v_count = COUNT(1) 
						FROM dbo.EWPCV_INSTITUTION INS 
						INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
					
					IF  @v_count > 0 
					BEGIN
						SET @P_ERROR_MESSAGE = 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.'
						SET @return_value = -1
					END 
					ELSE
					BEGIN
						EXECUTE dbo.INSERTA_FACTSHEET_INSTITUTION @P_FACTSHEET
						SET @return_value = 0 
					END
				END
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	Borra los information item asociados a una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INFORMATION_ITEMS'
) DROP PROCEDURE dbo.BORRA_INFORMATION_ITEMS;
GO 	
CREATE PROCEDURE  dbo.BORRA_INFORMATION_ITEMS(@P_INSTITUTION_ID varchar(255)) as
BEGIN
	DECLARE @ID varchar(255)
	DECLARE @CONTACT_DETAIL_ID varchar(255)

	DECLARE cur CURSOR FOR
	SELECT II.ID, II.CONTACT_DETAIL_ID
		FROM dbo.EWPCV_INST_INF_ITEM III 
			INNER JOIN dbo.EWPCV_INFORMATION_ITEM II 
			ON III.INFORMATION_ID = II.ID
		WHERE III.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INST_INF_ITEM WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND INFORMATION_ID = @ID
		DELETE FROM dbo.EWPCV_INFORMATION_ITEM WHERE ID = @ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID

		FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID
	END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra los requirements info asociados a una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_REQUIREMENTS_INFO'
) DROP PROCEDURE dbo.BORRA_REQUIREMENTS_INFO;
GO 
CREATE PROCEDURE  dbo.BORRA_REQUIREMENTS_INFO(@P_INSTITUTION_ID varchar(255)) as
BEGIN
	DECLARE @ID varchar(255)
	DECLARE @CONTACT_DETAIL_ID varchar(255)

	DECLARE cur CURSOR FOR
		SELECT RI.ID, RI.CONTACT_DETAIL_ID
		FROM dbo.EWPCV_INS_REQUIREMENTS IR
			INNER JOIN dbo.EWPCV_REQUIREMENTS_INFO RI 
				ON IR.REQUIREMENT_ID = RI.ID
		WHERE IR.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INS_REQUIREMENTS WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND REQUIREMENT_ID = @ID
		DELETE FROM dbo.EWPCV_REQUIREMENTS_INFO WHERE ID = @ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID

		FETCH NEXT FROM cur INTO @ID, @CONTACT_DETAIL_ID
	END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra un factsheet asociado a una institucion no borra la institucion.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.BORRA_FACTSHEET_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.BORRA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_FACTSHEET_ID varchar(255)) as
BEGIN
	DECLARE @CONTACT_DETAIL_ID varchar(255)
	SELECT @CONTACT_DETAIL_ID = CONTACT_DETAILS_ID FROM dbo.EWPCV_FACT_SHEET WHERE ID = @P_FACTSHEET_ID;
	
	UPDATE dbo.EWPCV_INSTITUTION SET FACT_SHEET = NULL WHERE ID = @P_INSTITUTION_ID;

	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @P_FACTSHEET_ID;

	EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID
	EXECUTE dbo.BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID	
END;
GO

/* Elimina un FACTSHEET del sistema.
	Recibe como parametro el identificador de la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_FACTSHEET'
) DROP PROCEDURE dbo.DELETE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @ID varchar(255)
		DECLARE @FACT_SHEET varchar(255)

		SET @return_value = 0

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
		IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
				SET @return_value = -1
			END
		ELSE
			BEGIN
				SELECT @v_count = COUNT(1) 
					FROM dbo.EWPCV_INSTITUTION INS 
					INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
					SET @return_value = -1
				END 
			END

		IF @return_value = 0
		BEGIN
			SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			EXECUTE dbo.BORRA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET
            INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION)
                VALUES (NEWID(), @FACT_SHEET, 5, 2)
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_FACTSHEET'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/*
	Borra los nombres de una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_NOMBRES'
) DROP PROCEDURE dbo.BORRA_NOMBRES;
GO 
CREATE PROCEDURE  dbo.BORRA_NOMBRES(@P_INSTITUTION_ID varchar(255)) as
BEGIN
	DECLARE @ID varchar(255)

	DECLARE curNam CURSOR FOR
		SELECT LI.ID FROM dbo.EWPCV_LANGUAGE_ITEM LI 
		INNER JOIN dbo.EWPCV_INSTITUTION_NAME INA 
		ON LI.ID = INA.NAME_ID
		WHERE INA.INSTITUTION_ID = @P_INSTITUTION_ID
	OPEN curNam
	FETCH NEXT FROM curNam INTO @ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_INSTITUTION_NAME WHERE INSTITUTION_ID = @P_INSTITUTION_ID AND NAME_ID = @ID
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @ID
		FETCH NEXT FROM curNam INTO @ID
	END
	CLOSE curNam
	DEALLOCATE curNam

END
;
GO

/*
	Actualiza la informacion de contacto
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_CONTACT_DETAILS'
) DROP PROCEDURE dbo.ACTUALIZA_CONTACT_DETAILS;
GO 
CREATE PROCEDURE  dbo.ACTUALIZA_CONTACT_DETAILS(@P_CONTACT_ID varchar(255), @P_URL xml, @P_EMAIL VARCHAR(255), @P_PHONE xml, @return_value VARCHAR(255) OUTPUT) as
BEGIN

	DECLARE @v_contact_id VARCHAR(255)
	EXECUTE dbo.BORRA_CONTACT_DETAILS @P_CONTACT_ID
	EXECUTE dbo.INSERTA_CONTACT_DETAIL @P_URL, @P_EMAIL, @P_PHONE, @v_contact_id OUTPUT
	SET @return_value = @v_contact_id
END 
;
GO

/*
	Actualiza el factsheet de una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_FACTSHEET'
) DROP PROCEDURE dbo.ACTUALIZA_FACTSHEET;
GO 
CREATE PROCEDURE  dbo.ACTUALIZA_FACTSHEET(@P_FACTSHEET_ID varchar(255), @P_FACTSHEET xml, @P_INFO xml) AS
BEGIN

	DECLARE @ID varchar(255)
	DECLARE @url xml
	DECLARE @email varchar(255)
	DECLARE @phone xml
	   
	UPDATE dbo.EWPCV_FACT_SHEET SET CONTACT_DETAILS_ID = NULL WHERE ID = @P_FACTSHEET_ID

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFO.nodes('/application_info') T(c) 

	EXECUTE dbo.ACTUALIZA_CONTACT_DETAILS @P_FACTSHEET_ID, @url, @email, @phone, @ID OUTPUT

	UPDATE dbo.EWPCV_FACT_SHEET SET
			DECISION_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
			TOR_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
			NOMINATIONS_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
			NOMINATIONS_SPRING_TERM = @P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
			APPLICATION_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
			APPLICATION_SPRING_TERM = @P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
			CONTACT_DETAILS_ID = @ID
			WHERE ID = @P_FACTSHEET_ID

END 
;
GO

/*
	Actualiza toda la informacion referente al factsheet de una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.ACTUALIZA_FACTSHEET_INSTITUTION;
GO 	
CREATE PROCEDURE  dbo.ACTUALIZA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET_ID varchar(255), @P_FACTSHEET xml) as
BEGIN

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @INSTITUTION_NAME_TEXT xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@INSTITUTION_NAME_TEXT = T.c.query('institution_name/text'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c)
	
	EXECUTE dbo.ACTUALIZA_FACTSHEET @P_FACTSHEET_ID, @FACTSHEET, @APPLICATION_INFO
	
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @P_INSTITUTION_ID
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @P_INSTITUTION_ID

	EXECUTE BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID

	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @P_INSTITUTION_ID
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo

	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @P_INSTITUTION_ID
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @P_INSTITUTION_ID
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO

/* Actualiza un FACTSHEET del sistema.
	Recibe como parametro el identificador de la Institucion a actualizar
	Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_FACTSHEET'
) DROP PROCEDURE dbo.UPDATE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.UPDATE_FACTSHEET( @P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET xml, 
 @P_ERROR_MESSAGE VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_count integer
			DECLARE @ID varchar(255)
			DECLARE @FACT_SHEET varchar(255)
			DECLARE @INSTITUTION_ID_XML varchar(255)

			SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			
			IF  @ID IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
					SET @return_value = -1
				END
			ELSE
				BEGIN 
					--validacion existe la institucion indicada en el xml de entrada
					SET @INSTITUTION_ID_XML = @P_FACTSHEET.value('(factsheet_institution/institution_id)[1]', 'varchar(255)')
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@INSTITUTION_ID_XML); 
					IF  @v_count = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = 'La institución '+ @INSTITUTION_ID_XML +' vinculada al factsheet no existe en el sistema.'
					END
					ELSE
					BEGIN
						SELECT @v_count = COUNT(1) 
							FROM dbo.EWPCV_INSTITUTION INS 
							INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
							WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
						IF @v_count = 0
						BEGIN
							SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
							SET @return_value = -1
						END 
					END
				END

			IF @return_value = 0
			BEGIN
				EXECUTE dbo.ACTUALIZA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET, @P_FACTSHEET ;
				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @FACT_SHEET, 5, 1);
			END
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/*
	VALIDACION VIA XSD
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_FACTSHEET_INSTITUTION' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_FACTSHEET_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
		
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="factsheet_institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="logo_url" type="HTTPS" minOccurs="0" />
				<xs:element name="abreviation" type="xs:string" minOccurs="0" />
				<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="factsheet">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="decision_week_limit" type="xs:integer" />
							<xs:element name="tor_week_limit" type="xs:integer" />
							<xs:element name="nominations_autum_term" type="xs:date" />
							<xs:element name="nominations_spring_term" type="xs:date" />
							<xs:element name="application_autum_term" type="xs:date" />
							<xs:element name="application_spring_term" type="xs:date" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="application_info" type="informationItemType" />
				<xs:element name="housing_info" type="informationItemType" />
				<xs:element name="visa_info" type="informationItemType" />
				<xs:element name="insurance_info" type="informationItemType" />
				<xs:element name="additional_info_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="additional_info" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="info_type" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="accessibility_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="accessibility_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="req_type">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:enumeration value="infrastructure"/>
													<xs:enumeration value="service"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="information_item" type="informationItemType" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="additional_requirements_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element  name="additional_requirement" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="name" type="notEmptyStringType" />
										<xs:element name="description" type="notEmptyStringType" />
										<xs:element name="url" type="httpListWithOptionalLang" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_FACTSHEET_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_FACTSHEET_INSTITUTION(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_FACTSHEET_INSTITUTION)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;
GO