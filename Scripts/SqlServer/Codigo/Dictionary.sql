       /*
    		Valida que no haya en BD un DICTIONARY con los mismos datos a introducir
    	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_DICTIONARY'
) DROP PROCEDURE dbo.VALIDA_DATOS_DICTIONARY;
GO

CREATE PROCEDURE dbo.VALIDA_DATOS_DICTIONARY(@P_DICTIONARY_CODE varchar(255) ,@P_DICTIONARY_DESCRIPTION varchar(255) ,@P_DICTIONARY_DICTIONARY_TYPE varchar(255) , @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN

    DECLARE @v_count integer

    SELECT @v_count = COUNT(*) FROM dbo.EWPCV_DICTIONARY
    WHERE UPPER(CODE) = UPPER(@P_DICTIONARY_CODE)
    AND UPPER(DESCRIPTION) = UPPER(@P_DICTIONARY_DESCRIPTION)
    AND UPPER(DICTIONARY_TYPE) = UPPER(@P_DICTIONARY_DICTIONARY_TYPE)

        IF @v_count > 0
            BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'Ya existe DICTIONARY con esos datos')
            END
    END;
    GO


    /*

     Borra DICTIONARY

     */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_DICTIONARY'
) DROP PROCEDURE dbo.BORRA_DICTIONARY;
GO
CREATE PROCEDURE dbo.BORRA_DICTIONARY (@P_DICTIONARY_ID varchar(255)) AS

   BEGIN

	    DELETE FROM dbo.EWPCV_DICTIONARY WHERE ID = @P_DICTIONARY_ID

END;
GO


/* Elimina un DICTIONARY del sistema.
		Recibe como parametros:
        El identificador del DICTIONARY
		Un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_DICTIONARY'
) DROP PROCEDURE dbo.DELETE_DICTIONARY;
GO
CREATE PROCEDURE dbo.DELETE_DICTIONARY(@P_DICTIONARY_ID varchar(255) ,@P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

	BEGIN

        DECLARE @v_count integer

		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_DICTIONARY WHERE ID = @P_DICTIONARY_ID

		IF @v_count > 0
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
					EXECUTE dbo.BORRA_DICTIONARY @P_DICTIONARY_ID
					INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_DICTIONARY_ID, 9, 2)
					COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
        			THROW
        			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_DICTIONARY'
        			SET @return_value = -1
        			ROLLBACK TRANSACTION
				END CATCH
			END;
		ELSE
			BEGIN
           		SET @return_value = -1
           		SET @P_ERROR_MESSAGE = 'El DICTIONARY no existe'
           	END

	END;
	GO



   /*

     Actualiza DICTIONARY

    */
 IF EXISTS (
 Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_DICTIONARY'
 ) DROP PROCEDURE dbo.ACTUALIZA_DICTIONARY;
 GO
CREATE PROCEDURE dbo.ACTUALIZA_DICTIONARY(@P_DICTIONARY_ID varchar(255) ,@P_DICTIONARY_CODE varchar(255) ,@P_DICTIONARY_DESCRIPTION varchar(255) ,@P_DICTIONARY_DICTIONARY_TYPE varchar(255) ,@P_DICTIONARY_CALL_YEAR varchar(255) ) as

     BEGIN

        UPDATE dbo.EWPCV_DICTIONARY SET
              VERSION = (version +1), CODE = @P_DICTIONARY_CODE, DESCRIPTION = @P_DICTIONARY_DESCRIPTION, DICTIONARY_TYPE=@P_DICTIONARY_DICTIONARY_TYPE,CALL_YEAR = @P_DICTIONARY_CALL_YEAR
              WHERE ID = @P_DICTIONARY_ID;
     END;
GO
/* Actualiza un DICTIONARY del sistema.
        Recibe como parametros:
        El identificador DICTIONARY
		Admite un objeto de tipo DICTIONARY con toda la informacion actualizada de DICTIONARY, sus contactos y sus factsheet
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_DICTIONARY'
) DROP PROCEDURE dbo.UPDATE_DICTIONARY;
GO
CREATE PROCEDURE dbo.UPDATE_DICTIONARY(@P_DICTIONARY_ID varchar(255) ,@P_DICTIONARY_CODE varchar(255) ,@P_DICTIONARY_DESCRIPTION varchar(255) ,@P_DICTIONARY_DICTIONARY_TYPE varchar(255) ,@P_DICTIONARY_CALL_YEAR varchar(255) , @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

	BEGIN
	    DECLARE @v_count integer
    	SET @return_value = 0

		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_DICTIONARY WHERE ID = @P_DICTIONARY_ID

		IF @v_count > 0
		    BEGIN
	          EXECUTE dbo.VALIDA_DATOS_DICTIONARY @P_DICTIONARY_CODE, @P_DICTIONARY_DESCRIPTION,@P_DICTIONARY_DICTIONARY_TYPE , @P_ERROR_MESSAGE output,@return_value output
	          IF @return_value = 0
	            BEGIN
                  BEGIN TRY
                   BEGIN TRANSACTION
	               EXECUTE dbo.ACTUALIZA_DICTIONARY @P_DICTIONARY_ID,@P_DICTIONARY_CODE, @P_DICTIONARY_DESCRIPTION,@P_DICTIONARY_DICTIONARY_TYPE,@P_DICTIONARY_CALL_YEAR;
	               INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_DICTIONARY_ID, 9, 1)
	               COMMIT TRANSACTION
	              END TRY

	              BEGIN CATCH
                  	THROW
                  	SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.ACTUALIZA_DICTIONARY'
                  	SET @return_value = -1
                  	ROLLBACK TRANSACTION
                  END CATCH

	           END
			END
		ELSE
		BEGIN
            SET @P_ERROR_MESSAGE = 'El DICTIONARY no existe'
            SET @return_value = -1
        END
END;
go

/* Funcion introduce DICTIONARY */

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_DICTIONARY'
) DROP PROCEDURE dbo.INSERT_DICTIONARY;
GO
CREATE PROCEDURE dbo.INSERT_DICTIONARY(@P_DICTIONARY_CODE varchar(255) ,@P_DICTIONARY_DESCRIPTION varchar(255) ,@P_DICTIONARY_DICTIONARY_TYPE varchar(255) ,@P_DICTIONARY_CALL_YEAR varchar(255) , @P_DICTIONARY_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_DATOS_DICTIONARY @P_DICTIONARY_CODE,@P_DICTIONARY_DESCRIPTION,@P_DICTIONARY_DICTIONARY_TYPE ,@P_ERROR_MESSAGE output, @return_value output

	if @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

        			DECLARE @version INTEGER
					DECLARE @v_id varchar(255)
					SET @version = 0
					SET @v_id = NEWID()

					INSERT INTO dbo.EWPCV_DICTIONARY (ID, VERSION,CODE,DESCRIPTION, DICTIONARY_TYPE, CALL_YEAR) VALUES (@v_id, @version,@P_DICTIONARY_CODE,@P_DICTIONARY_DESCRIPTION,@P_DICTIONARY_DICTIONARY_TYPE,@P_DICTIONARY_CALL_YEAR);

					INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_id, 9, 0)

				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
        			THROW
        			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_DICTIONARY'
        			SET @return_value = -1
        			ROLLBACK TRANSACTION
			END CATCH
		END
		SET @return_value = 0
		SET @P_DICTIONARY_ID = @v_id
END ;
GO













