/*
***************************************
******** Funciones IIAS ***************
***************************************
*/


IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_IIA'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	    <xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[0-9]{4}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" minOccurs="0"/>
				<xs:element name="end_date" type="xs:date" minOccurs="0"/>
				<xs:element name="first_partner" type="partnerType"/>
				<xs:element name="second_partner" type="partnerType"/>
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" minOccurs="0" maxOccurs="unbounded">
														<xs:simpleType>
													        <xs:restriction base="xs:integer">
																<xs:minInclusive value="1"/>
																<xs:maxInclusive value="8"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="1"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="subject_area_list"  minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area" minOccurs="0" maxOccurs="unbounded" type="subjectAreaType"/>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="1" type="xs:decimal" />
													<xs:element name="unit" minOccurs="1">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
				<xs:element name="file_mime_type" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_IIA'
) DROP PROCEDURE dbo.VALIDA_IIA;
GO
CREATE PROCEDURE  dbo.VALIDA_IIA(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_IIA)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH

	--ha validado contra el xsd pero tenemos que comprobar que las condiciones de cooperacion de tipo Student-studies y staff-teaching tengan subject_area_language_list
	DECLARE @CooperationConditionList XML
    SELECT @CooperationConditionList = T.c.query('cooperation_condition_list')
    	FROM @XML.nodes('iia') T(c)

    DECLARE @MobilityCategory NVARCHAR(255)
    DECLARE @MobilityGroup NVARCHAR(255)
    DECLARE @SubjectAreaLanguageListExists INT

    DECLARE cur CURSOR FOR
        SELECT  T.c.value('(mobility_type/mobility_category)[1]', 'nvarchar(255)'),
                T.c.value('(mobility_type/mobility_group)[1]', 'nvarchar(255)'),
                T.c.exist('subject_area_language_list')
        FROM @CooperationConditionList.nodes('cooperation_condition_list/cooperation_condition') T(c)

    OPEN cur
    FETCH NEXT FROM cur INTO @MobilityCategory, @MobilityGroup, @SubjectAreaLanguageListExists

    WHILE @@FETCH_STATUS = 0
    BEGIN
        IF ((@MobilityCategory = 'Studies' AND @MobilityGroup = 'Student') OR (@MobilityCategory = 'Teaching' AND @MobilityGroup = 'Staff')) AND @SubjectAreaLanguageListExists = 0
        BEGIN
            SET @errNum = -1
            SET @errMsg = 'Las condiciones de cooperación de tipo Student-studies y Staff-teaching deben tener un subject_area_language_list'
            BREAK
        END

        FETCH NEXT FROM cur INTO @MobilityCategory, @MobilityGroup, @SubjectAreaLanguageListExists
    END

    CLOSE cur
    DEALLOCATE cur
    END
;
GO

/*
	Valida que el eqf level solo venga informado cuando mobility_category = Studies y mobility_group = Student
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_EQF_LEVEL'
) DROP PROCEDURE dbo.VALIDA_EQF_LEVEL;
GO
CREATE PROCEDURE  dbo.VALIDA_EQF_LEVEL(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @v_m_category varchar(255)
	DECLARE @v_m_group varchar(255)
	DECLARE @v_eqf_level varchar(255)

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END


	SELECT
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('(mobility_type/mobility_category)[1]', 'varchar(255)'),
		T.c.value('(mobility_type/mobility_group)[1]', 'varchar(255)'),
		T.c.value('(eqf_level_list/eqf_level)[1]', 'varchar(255)')
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_eqf_level
	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF UPPER(@v_m_category) = 'STUDIES' AND UPPER(@v_m_group) = 'STUDENT' AND @v_eqf_level IS NULL
			BEGIN
				SET @return_value=-1
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				Se debe informar al menos un eqf_level cuando la condicion de cooperacion es de tipo Student-Studies'
			END
		FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_eqf_level
		END;
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Valida que el blended solo venga informado cuando mobility_group = Student
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_BLENDED'
) DROP PROCEDURE dbo.VALIDA_BLENDED;
GO
CREATE PROCEDURE  dbo.VALIDA_BLENDED(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @v_m_category varchar(255)
	DECLARE @v_m_group varchar(255)
	DECLARE @v_blended varchar(255)

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END


	SELECT
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('(mobility_type/mobility_category)[1]', 'varchar(255)'),
		T.c.value('(mobility_type/mobility_group)[1]', 'varchar(255)'),
		T.c.value('(blended)[1]', 'varchar(255)')
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_blended
	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF UPPER(@v_m_group) = 'STUDENT' AND @v_blended IS NULL
			BEGIN
				SET @return_value=-1
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				Se debe informar el campo blended cuando la condicion de cooperacion es de tipo Student'
			END
		FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_blended
		END;
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Valida que las condiciones de cooperación vengan informadas en el orden correcto
	Student Studies -> Student Training -> Staff Teaching -> Staff Training
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_ORDEN_COOP_COND'
) DROP PROCEDURE dbo.VALIDA_ORDEN_COOP_COND;
GO
CREATE PROCEDURE dbo.VALIDA_ORDEN_COOP_COND(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @v_m_type varchar(255)
	DECLARE @cc_passed integer
	DECLARE @is_correct bit
	DECLARE @first bit

	SET @is_correct = 1
	SET @P_ERROR_MESSAGE = ''

	SET @cc_passed = 0
	SET @first = 1

	SELECT
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		UPPER(CONCAT (
			T.c.value('(mobility_type/mobility_group)[1]', 'varchar(255)'),
			' ',
			T.c.value('(mobility_type/mobility_category)[1]', 'varchar(255)')
		))
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @v_m_type
	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @first = 1
			BEGIN
				IF @v_m_type = 'STUDENT TRAINING'
					SET @cc_passed = 1
				ELSE IF @v_m_type = 'STAFF TEACHING'
					SET @cc_passed = 2
				ELSE IF @v_m_type = 'STAFF TRAINING'
					SET @cc_passed = 3
				SET @first = 0
			END

			IF @cc_passed = 0
			BEGIN
				IF @v_m_type = 'STUDENT TRAINING'
					SET @cc_passed = 1
				ELSE IF @v_m_type = 'STAFF TEACHING'
					SET @cc_passed = 2
				ELSE IF @v_m_type = 'STAFF TRAINING'
					SET @cc_passed = 3
			END
			ELSE IF @cc_passed = 1
			BEGIN
				IF @v_m_type = 'STUDENT STUDIES'
					SET @is_correct = 0
				ELSE IF @v_m_type = 'STAFF TEACHING'
					SET @cc_passed = 2
				ELSE IF @v_m_type = 'STAFF TRAINING'
					SET @cc_passed = 3
			END
			ELSE IF @cc_passed = 2
			BEGIN
				IF @v_m_type = 'STUDENT STUDIES' OR @v_m_type = 'STUDENT TRAINING'
					SET @is_correct = 0
				ELSE IF @v_m_type = 'STAFF TRAINING'
					SET @cc_passed = 3
			END
			ELSE IF @cc_passed = 3 AND @v_m_type != 'STAFF TRAINING'
				SET @is_correct = 0
			FETCH NEXT FROM cur INTO  @v_m_type
		END;
	CLOSE cur
	DEALLOCATE cur
	IF @is_correct = 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
		Se deben informar las condiciones de cooperación en el orden de la especificación: Student Studies -> Student Training -> Staff Teaching -> Staff Training'
	END
END
;
GO

/*
	Valida que existan los institution id y ounit id de un partner
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_PARTNER_INSTITUTION_OUNIT'
) DROP PROCEDURE dbo.VALIDA_PARTNER_INSTITUTION_OUNIT;
GO
CREATE PROCEDURE  dbo.VALIDA_PARTNER_INSTITUTION_OUNIT(@P_INSTITUTION_ID VARCHAR(255),@P_OUNIT_CODE VARCHAR(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN

DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value = 0
	IF @P_ERROR_MESSAGE IS NULL
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END

	--validacion de institution
	SELECT @v_count_inst = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@P_INSTITUTION_ID);
	IF  @v_count_inst = 0
	BEGIN
		SET @return_value = -1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
		La institución '+ @P_INSTITUTION_ID +' vinculada a un partner no existe en el sistema.'
	END
	ELSE IF @P_OUNIT_CODE IS NOT NULL
	BEGIN
		--validacion de ounit
		SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
			INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.INTERNAL_ID
			WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE);
		IF  @v_count_ounit = 0
		BEGIN
			SET @return_value = -1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
			La unidad organizativa '+ @P_OUNIT_CODE +' vinculada a ' + @P_INSTITUTION_ID  + ' no existe en el sistema.'
		END
	END

END
;
GO


/*
	Valida que existan los institution id y ounit id de todos los partners del iia y de las coop cond
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_IIA_PARTNER_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_IIA_PARTNER_INSTITUTION;
GO
CREATE PROCEDURE  dbo.VALIDA_IIA_PARTNER_INSTITUTION(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
DECLARE @COOPERATION_CONDITION_LIST xml
DECLARE @v_fp_inst_id varchar(255)
DECLARE @v_fp_ounit_code varchar(255)
DECLARE @v_fp_factsheet varchar(max)
DECLARE @v_sp_inst_id varchar(255)
DECLARE @v_sp_ounit_code varchar(255)
DECLARE @v_sp_factsheet varchar(max)
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_s_ounit_code varchar(255)
DECLARE @v_r_inst_id varchar(255)
DECLARE @v_r_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer
DECLARE @v_validation_return integer

	SET @return_value = 0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	SELECT 
		@v_fp_inst_id = T.c.value('(first_partner/institution/institution_id)[1]', 'varchar(255)'),
		@v_fp_ounit_code = T.c.value('(first_partner/institution/organization_unit_code)[1]', 'varchar(255)'),
		@v_fp_factsheet = T.c.value('(first_partner/institution/institution_factsheet_url)[1]', 'varchar(max)'),
		@v_sp_inst_id = T.c.value('(second_partner/institution/institution_id)[1]', 'varchar(255)'),
		@v_sp_ounit_code = T.c.value('(second_partner/institution/organization_unit_code)[1]', 'varchar(255)'),
		@v_sp_factsheet = T.c.value('(second_partner/institution/institution_factsheet_url)[1]', 'varchar(max)')

	FROM @P_IIA.nodes('*') T(c)

	--validacion de first partner
	EXECUTE dbo.VALIDA_PARTNER_INSTITUTION_OUNIT @v_fp_inst_id, @v_fp_ounit_code, @P_ERROR_MESSAGE output, @v_validation_return output;
	IF @v_validation_return < 0 BEGIN
		SET @return_value = -1
	END
	IF @v_fp_factsheet IS NULL BEGIN
		SET @return_value = -1
	END

	--validacion de second partner
	EXECUTE dbo.VALIDA_PARTNER_INSTITUTION_OUNIT @v_sp_inst_id, @v_sp_ounit_code, @P_ERROR_MESSAGE output, @v_validation_return output;
	IF @v_validation_return < 0 BEGIN
		SET @return_value = -1
	END
	IF @v_sp_factsheet IS NULL BEGIN
		SET @return_value = -1
	END

	--validacion de los partners de las coop condition
	SELECT
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('(sending_partner/institution/institution_id)[1]', 'varchar(255)'),
		T.c.value('(sending_partner/institution/organization_unit_code)[1]', 'varchar(255)'),
		T.c.value('(receiving_partner/institution/institution_id)[1]', 'varchar(255)'),
		T.c.value('(receiving_partner/institution/organization_unit_code)[1]', 'varchar(255)')
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO  @v_s_inst_id, @v_s_ounit_code, @v_r_inst_id, @v_r_ounit_code
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--validacion de institution del sender
			EXECUTE dbo.VALIDA_PARTNER_INSTITUTION_OUNIT @v_s_inst_id, @v_s_ounit_code, @P_ERROR_MESSAGE output, @v_validation_return output;
			IF @v_validation_return < 0 BEGIN
				SET @return_value = -1
			END

			--validacion de institution del receiver
			EXECUTE dbo.VALIDA_PARTNER_INSTITUTION_OUNIT @v_r_inst_id, @v_r_ounit_code, @P_ERROR_MESSAGE output, @v_validation_return output;
			IF @v_validation_return < 0 BEGIN
				SET @return_value = -1
			END

			FETCH NEXT FROM cur INTO  @v_s_inst_id, @v_s_ounit_code, @v_r_inst_id, @v_r_ounit_code
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Valida la calidad de los datos de entrada al aprovisionamiento
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_IIA'
) DROP PROCEDURE dbo.VALIDA_DATOS_IIA;
GO
CREATE PROCEDURE  dbo.VALIDA_DATOS_IIA(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN

	EXECUTE dbo.VALIDA_BLENDED @P_IIA, @P_ERROR_MESSAGE output, @return_value output;

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_ORDEN_COOP_COND @P_IIA, @P_ERROR_MESSAGE output, @return_value output;
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_IIA_PARTNER_INSTITUTION @P_IIA, @P_ERROR_MESSAGE output, @return_value output;
	END
END
;
GO

/*
	Busca el codigo SCHAC de la institucion que genera la notificacion
	Para un EWPCV_IIA.ID, buscar entre sus EWPCV_COOPERATION_CONDITION los RECEIVING_PARTNER_ID y SENDING_PARTNER_ID,
	y de esta pareja devolver el INSTITUTION_ID correspondiente que NO sea el P_HEI_TO_NOTIFY
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI;
GO
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI(@P_IIA_ID varchar(255), @P_HEI_TO_NOTIFY varchar(255), @return_value varchar(255) OUTPUT) as
BEGIN
	DECLARE @receiving_institution_id varchar(255)
	DECLARE @sending_institution_id varchar(255)
	DECLARE @v_internal_iia_id varchar(255)

	SELECT @v_internal_iia_id = IIA_ID FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID

	SELECT TOP 1
		@receiving_institution_id = receiving_partner.INSTITUTION_ID,
		@sending_institution_id = sending_partner.INSTITUTION_ID
	FROM
		dbo.EWPCV_COOPERATION_CONDITION coco, dbo.EWPCV_IIA_PARTNER sending_partner, dbo.EWPCV_IIA_PARTNER receiving_partner
	WHERE
		coco.RECEIVING_PARTNER_ID = receiving_partner.ID and
		coco.SENDING_PARTNER_ID = sending_partner.ID and
		coco.IIA_ID =  @v_internal_iia_id

	IF @P_HEI_TO_NOTIFY = @receiving_institution_id
		SET @return_value = @sending_institution_id
	ELSE
		SET @return_value = @receiving_institution_id

END
;
GO

	/*
		Persiste un IIA en el sistema y retorna el UUID generado para el registro.
	*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_IIA'
) DROP PROCEDURE dbo.INSERTA_IIA;
GO
CREATE PROCEDURE  dbo.INSERTA_IIA(@P_IIA XML, @return_value varchar(255) OUTPUT) as
BEGIN
    DECLARE @v_internal_iia_id varchar(255)
	DECLARE @v_iia_id varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @IIA_CODE varchar(255)
	DECLARE @PDF varchar(max)
	DECLARE @PDF_DECODED varchar(max)
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @FIRST_PARTNER xml
	DECLARE @FIRST_PARTNER_CONTACTS xml
	DECLARE @SECOND_PARTNER xml
	DECLARE @SECOND_PARTNER_CONTACTS xml
	DECLARE @v_fp_id varchar(255)
	DECLARE @v_sp_id varchar(255)
	DECLARE @v_file_id varchar(255)
	DECLARE @v_file_type varchar(255)

	SELECT
		@FIRST_PARTNER = T.c.query('first_partner'),
		@FIRST_PARTNER_CONTACTS = T.c.query('first_partner/partner_contacts_list'),
		@SECOND_PARTNER = T.c.query('second_partner'),
		@SECOND_PARTNER_CONTACTS = T.c.query('second_partner/partner_contacts_list')
	FROM @P_IIA.nodes('iia') T(c)
	EXECUTE dbo.INSERTA_IIA_PARTNER @FIRST_PARTNER, @v_fp_id output
	EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_fp_id, @FIRST_PARTNER_CONTACTS

	EXECUTE dbo.INSERTA_IIA_PARTNER @SECOND_PARTNER, @v_sp_id output
	EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_sp_id, @SECOND_PARTNER_CONTACTS

	SELECT
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@PDF = T.c.value('(pdf)[1]', 'varchar(max)'),
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list'),
		@v_file_type = T.c.value('(file_mime_type)[1]', 'varchar(255)')
	FROM @P_IIA.nodes('iia') T(c)

    SET @v_internal_iia_id = NEWID()
	SET @v_iia_id = NEWID()

	IF (@PDF is not null AND @PDF<>'')
	BEGIN TRY
        SET @PDF_DECODED = CONVERT (varchar(max), cast('' as xml).value('xs:base64Binary(sql:variable("@PDF"))', 'varbinary(max)'))
		EXECUTE dbo.INSERTA_FICHERO @PDF, @v_fp_id, @v_file_type, @v_file_id output
    END TRY
    BEGIN CATCH
        PRINT 'ERROR a intentar guardar PDF en dbo.EWPCV_IIA, se omite'
    END CATCH

	INSERT INTO dbo.EWPCV_IIA (IIA_ID, ID, END_DATE, IIA_CODE, MODIFY_DATE, START_DATE, FIRST_PARTNER_ID, SECOND_PARTNER_ID, PDF_ID)
		VALUES (@v_internal_iia_id, @v_iia_id, @END_DATE, @IIA_CODE, SYSDATETIME(), @START_DATE, @v_fp_id, @v_sp_id,@v_file_id)

	EXECUTE dbo.INSERTA_COOP_CONDITIONS @v_internal_iia_id, @COOPERATION_CONDITION_LIST

    SET @return_value = @v_iia_id

END
;
GO


/* Inserta un IIA en el sistema
		Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
		Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_IIA'
) DROP PROCEDURE dbo.INSERT_IIA;
GO
CREATE PROCEDURE dbo.INSERT_IIA(@P_IIA XML, @P_IIA_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS

BEGIN

	DECLARE @v_notifier_hei varchar(255)

	IF @P_HEI_TO_NOTIFY IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END
	ELSE
	BEGIN
		EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		--validacion, institution id y ounit code de los partners existen
		IF @return_value = 0
		BEGIN
			EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		END
		IF @return_value = 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_IIA @P_IIA, @P_IIA_ID OUTPUT
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei

				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
				    VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 0)

				COMMIT TRANSACTION
			END TRY

			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_IIA'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
END
;
GO

/*
	Borra un IIA PARTNER y todos sus contactos asociados.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_IIA_PARTNER'
) DROP PROCEDURE dbo.BORRA_IIA_PARTNER;
GO
CREATE PROCEDURE  dbo.BORRA_IIA_PARTNER(@P_IIA_PARTNER_ID varchar(255)) AS
BEGIN

	DECLARE @v_s_id VARCHAR(255)
	DECLARE @CONTACTS_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT CONTACTS_ID
			FROM dbo.EWPCV_IIA_PARTNER_CONTACTS
			WHERE IIA_PARTNER_ID = @P_IIA_PARTNER_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACTS_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_IIA_PARTNER_CONTACTS
				WHERE CONTACTS_ID = @CONTACTS_ID
				AND IIA_PARTNER_ID = @P_IIA_PARTNER_ID
			EXECUTE dbo.BORRA_CONTACT @CONTACTS_ID
			FETCH NEXT FROM cur INTO @CONTACTS_ID
		END
	CLOSE cur
	DEALLOCATE cur

	SELECT @v_s_id = SIGNER_PERSON_CONTACT_ID
			FROM dbo.EWPCV_IIA_PARTNER
			WHERE ID = @P_IIA_PARTNER_ID

	DELETE FROM dbo.EWPCV_IIA_PARTNER WHERE ID = @P_IIA_PARTNER_ID
	IF @v_s_id IS NOT NULL
	BEGIN
		EXECUTE dbo.BORRA_CONTACT @v_s_id
	END
END
;
GO

/*
	Borra las condiciones de cooperacion de un IIAs
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_CONDITIONS'
) DROP PROCEDURE dbo.BORRA_COOP_CONDITIONS;
GO
CREATE PROCEDURE  dbo.BORRA_COOP_CONDITIONS(@P_IIA_ID varchar(255)) AS
BEGIN
	DECLARE @v_mn_count integer
	DECLARE @v_mt_count integer
	DECLARE @v_md_count integer

	DECLARE @ID varchar(255)
	DECLARE @MOBILITY_NUMBER_ID varchar(255)
	DECLARE @DURATION_ID varchar(255)
	DECLARE @RECEIVING_PARTNER_ID varchar(255)
	DECLARE @SENDING_PARTNER_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT ID, MOBILITY_NUMBER_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM dbo.EWPCV_COOPERATION_CONDITION
			WHERE IIA_ID = @P_IIA_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA_LANSKILL @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA @ID
			DELETE FROM dbo.EWPCV_COOPERATION_CONDITION WHERE ID = @ID
			DELETE FROM dbo.EWPCV_MOBILITY_NUMBER WHERE ID = @MOBILITY_NUMBER_ID

			DELETE FROM EWPCV_DURATION WHERE ID = @DURATION_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @RECEIVING_PARTNER_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @SENDING_PARTNER_ID

			FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Borra las condiciones de cooperacion no asociadas a ninguna snapshot de un IIAs
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_COND_SIN_SNAPSHOT'
) DROP PROCEDURE dbo.BORRA_COOP_COND_SIN_SNAPSHOT;
GO
CREATE PROCEDURE  dbo.BORRA_COOP_COND_SIN_SNAPSHOT(@P_IIA_ID varchar(255)) AS
BEGIN
	DECLARE @v_mn_count integer
	DECLARE @v_mt_count integer
	DECLARE @v_md_count integer

	DECLARE @ID varchar(255)
	DECLARE @MOBILITY_NUMBER_ID varchar(255)
	DECLARE @DURATION_ID varchar(255)
	DECLARE @RECEIVING_PARTNER_ID varchar(255)
	DECLARE @SENDING_PARTNER_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
		SELECT ID, MOBILITY_NUMBER_ID, DURATION_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID
			FROM dbo.EWPCV_COOPERATION_CONDITION
			WHERE IIA_ID = @P_IIA_ID
			AND IIA_APPROVAL_ID IS NULL

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA_LANSKILL @ID
			EXECUTE dbo.BORRA_COOP_COND_SUBAREA @ID
			DELETE FROM dbo.EWPCV_COOPERATION_CONDITION WHERE ID = @ID
			DELETE FROM dbo.EWPCV_MOBILITY_NUMBER WHERE ID = @MOBILITY_NUMBER_ID

			DELETE FROM EWPCV_DURATION WHERE ID = @DURATION_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @RECEIVING_PARTNER_ID
			EXECUTE dbo.BORRA_IIA_PARTNER @SENDING_PARTNER_ID

			FETCH NEXT FROM cur INTO @ID, @MOBILITY_NUMBER_ID, @DURATION_ID, @RECEIVING_PARTNER_ID, @SENDING_PARTNER_ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Borra un IIAS y todas sus condiciones de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_IIA'
) DROP PROCEDURE dbo.BORRA_IIA;
GO
CREATE PROCEDURE  dbo.BORRA_IIA(@P_IIA_ID varchar(255)) AS
BEGIN
	DECLARE @v_f_partner_id VARCHAR(255);
	DECLARE @v_s_partner_id VARCHAR(255);

	SELECT @v_f_partner_id = FIRST_PARTNER_ID, @v_s_partner_id = SECOND_PARTNER_ID
	FROM dbo.EWPCV_IIA WHERE IIA_ID = @P_IIA_ID;

	EXECUTE dbo.BORRA_COOP_CONDITIONS @P_IIA_ID
	DELETE FROM dbo.EWPCV_IIA WHERE IIA_ID = @P_IIA_ID

	IF @v_f_partner_id IS NOT NULL
	BEGIN
		EXECUTE dbo.BORRA_IIA_PARTNER @v_f_partner_id;
	END
	IF @v_s_partner_id IS NOT NULL
	BEGIN
		EXECUTE dbo.BORRA_IIA_PARTNER @v_s_partner_id;
	END

END
;
GO

/*
	Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_IIA_PARTNER'
) DROP PROCEDURE dbo.INSERTA_IIA_PARTNER;
GO
CREATE PROCEDURE  dbo.INSERTA_IIA_PARTNER(@P_IIA_PARTNER xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_c_id varchar(255)
	DECLARE @v_ounit_id varchar(255)
	DECLARE @INSTITUTION xml
	DECLARE @SIGNER_PERSON xml
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @SIGNING_DATE date


	SELECT
		@INSTITUTION = T.c.query('institution'),
		@SIGNER_PERSON = T.c.query('signer_person'),
		@INSTITUTION_ID = T.c.value('(institution/institution_id)[1]', 'varchar(255)'),
		@SIGNING_DATE = T.c.value('(signing_date)[1]', 'date')
	FROM @P_IIA_PARTNER.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @INSTITUTION, @v_ounit_id output
	IF @SIGNER_PERSON IS NOT NULL
	BEGIN
		EXECUTE dbo.INSERTA_CONTACT_PERSON @SIGNER_PERSON, null, null, @v_c_id output
	END
	SET @v_id = NEWID()

	INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE)
			VALUES (@v_id, @INSTITUTION_ID, @v_ounit_id, @v_c_id, @SIGNING_DATE)

	SET @return_value = @v_id

END
;
GO

/*
	Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_PARTNER_CONTACTS'
) DROP PROCEDURE dbo.INSERTA_PARTNER_CONTACTS;
GO
CREATE PROCEDURE  dbo.INSERTA_PARTNER_CONTACTS(@P_PARTNER_ID varchar(255), @P_PARTNER_CONTACTS xml) AS
BEGIN
	DECLARE @partner_contact xml
	DECLARE @v_c_p_id varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_PARTNER_CONTACTS.nodes('partner_contacts_list/partner_contact') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @partner_contact
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @partner_contact, null, null, @v_c_p_id output
			INSERT INTO dbo.EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (@P_PARTNER_ID, @v_c_p_id)
			FETCH NEXT FROM cur INTO @partner_contact
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Persiste la lista de areas de ensenanza y niveles de idiomas, si la lista está vacia no hace nada
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SUBAREA_LANSKIL'
) DROP PROCEDURE dbo.INSERTA_SUBAREA_LANSKIL;
GO
CREATE PROCEDURE  dbo.INSERTA_SUBAREA_LANSKIL(@P_COP_COND_ID varchar(255), @P_SUBAREA_LANSKIL_LIST xml) AS
BEGIN
	DECLARE @v_lang_skill_id varchar(255)
	DECLARE @v_isced_code varchar(255)
	DECLARE @SUBJECT_AREA xml
	DECLARE @LANGUAGE_SKILL xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('subject_area'),
		T.c.query('language_skill')
	FROM @P_SUBAREA_LANSKIL_LIST.nodes('subject_area_language_list/subject_area_language') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @SUBJECT_AREA, @LANGUAGE_SKILL
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA, @v_isced_code output
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_COOPCOND_SUBAR_LANSKIL(ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID)
				VALUES(NEWID(), @P_COP_COND_ID, @v_isced_code, @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @SUBJECT_AREA, @LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Persiste la lista de areas de ensenanza, si la lista está vacia no hace nada
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SUBAREA_LIST'
) DROP PROCEDURE dbo.INSERTA_SUBAREA_LIST;
GO
CREATE PROCEDURE  dbo.INSERTA_SUBAREA_LIST(@P_COP_COND_ID varchar(255), @P_SUBAREA_LIST xml) AS
BEGIN
	DECLARE @v_isced_code varchar(255)
	DECLARE @SUBJECT_AREA xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_SUBAREA_LIST.nodes('subject_area_list/subject_area') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @SUBJECT_AREA
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA, @v_isced_code output

			INSERT INTO dbo.EWPCV_COOPCOND_SUBAR(ID, COOPERATION_CONDITION_ID, ISCED_CODE)
				VALUES(NEWID(), @P_COP_COND_ID, @v_isced_code)
			FETCH NEXT FROM cur INTO @SUBJECT_AREA
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Inserta un tipo de movilidad si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_IIA_MOBILITY_TYPE'
) DROP PROCEDURE dbo.INSERTA_IIA_MOBILITY_TYPE;
GO
CREATE PROCEDURE  dbo.INSERTA_IIA_MOBILITY_TYPE(@P_MOBILITY_TYPE xml, @v_id varchar(255) OUTPUT) AS
BEGIN
	DECLARE @MOBILITY_CATEGORY VARCHAR(255)
	DECLARE @MOBILITY_GROUP VARCHAR(255)

	SELECT
		@MOBILITY_CATEGORY = T.c.value('(mobility_category)[1]', 'varchar(255)'),
		@MOBILITY_GROUP = T.c.value('(mobility_group)[1]', 'varchar(255)')
	FROM @P_MOBILITY_TYPE.nodes('mobility_type') T(c)

	SELECT @v_id = ID
		FROM dbo.EWPCV_MOBILITY_TYPE
		WHERE UPPER(MOBILITY_CATEGORY) = UPPER(@MOBILITY_CATEGORY)
		AND  UPPER(MOBILITY_GROUP) = UPPER(@MOBILITY_GROUP)

	IF @v_id is null
	BEGIN
		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_MOBILITY_TYPE (ID, MOBILITY_CATEGORY, MOBILITY_GROUP) VALUES (@v_id, @MOBILITY_CATEGORY, @MOBILITY_GROUP)
	END
END
;
GO

 /*
	Inserta una duración si no existe ya en el sistema y devuelve el identificador generado
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_DURATION'
) DROP PROCEDURE dbo.INSERTA_DURATION;
GO
CREATE PROCEDURE  dbo.INSERTA_DURATION(@DURATION xml, @v_id varchar(255) output) AS
BEGIN
	DECLARE @NUMBER_DURATION VARCHAR(255)
	DECLARE @UNIT VARCHAR(255)

	SELECT
		@NUMBER_DURATION = T.c.value('(number_duration)[1]', 'varchar(255)'),
		@UNIT = T.c.value('(unit)[1]', 'varchar(255)')
	FROM @DURATION.nodes('cop_cond_duration') T(c)

	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (@v_id, @NUMBER_DURATION, @UNIT)

END
;
GO

/*
	Inserta una mobility number si no existe ya en el sistema y devuelve el identificador generado
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_NUMBER'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_NUMBER;
GO
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_NUMBER(@P_MOB_NUMBER varchar(255), @v_id varchar(255) output) AS
BEGIN
	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY) VALUES (@v_id, @P_MOB_NUMBER)

END
;
GO

/*
	Inserta los EQUF LEVELS asociados a una condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_EQFLEVELS'
) DROP PROCEDURE dbo.INSERTA_EQFLEVELS;
GO
CREATE PROCEDURE  dbo.INSERTA_EQFLEVELS(@P_COOP_COND_ID varchar(255), @P_COOP_COND xml) AS
BEGIN

	DECLARE @EQF_LEVEL varchar(255)
	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('(.)[1]', 'varchar(255)')
	FROM @P_COOP_COND.nodes('cooperation_condition/eqf_level_list/eqf_level') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @EQF_LEVEL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO dbo.EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (@P_COOP_COND_ID, @EQF_LEVEL)
			FETCH NEXT FROM cur INTO @EQF_LEVEL
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Persiste una lista de condiciones de cooperacion asociadas a un IIAS
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_COOP_CONDITIONS'
) DROP PROCEDURE dbo.INSERTA_COOP_CONDITIONS;
GO
CREATE PROCEDURE  dbo.INSERTA_COOP_CONDITIONS(@P_IIA_ID varchar(255), @P_COOP_COND xml) AS
BEGIN
	DECLARE @v_s_iia_partner_id varchar(255)
	DECLARE @v_s_c_p_id varchar(255)
	DECLARE @v_r_iia_partner_id varchar(255)
	DECLARE @v_r_c_p_id varchar(255)
	DECLARE @v_mobility_type_id varchar(255)
	DECLARE @v_duration_id varchar(255)
	DECLARE @v_mob_number_id varchar(255)
	DECLARE @v_cc_id varchar(255)

	DECLARE @sp xml, @sp_contacts xml, @rp xml, @rp_contacts xml, @mobility_type xml, @cop_cond_duration xml
	DECLARE @mobility_number integer
	DECLARE @start_date date
	DECLARE @end_date date
	DECLARE @subject_area_language_list xml
	DECLARE @subject_area_list xml
	DECLARE @cooperation_condition xml
	DECLARE @other_info varchar(255)
	DECLARE @order_index integer

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('sending_partner'),
		T.c.query('sending_partner/partner_contacts_list'),
		T.c.query('receiving_partner'),
		T.c.query('receiving_partner/partner_contacts_list'),
		T.c.query('mobility_type'),
		T.c.query('cop_cond_duration'),
		T.c.value('(mobility_number)[1]', 'integer'),
		T.c.value('(start_date)[1]', 'date'),
		T.c.value('(end_date)[1]', 'date'),
		T.c.query('subject_area_language_list'),
		T.c.query('subject_area_list'),
		T.c.value('(other_info)[1]', 'varchar(255)'),
		T.c.query('.')
		FROM @P_COOP_COND.nodes('cooperation_condition_list/cooperation_condition') T(c)

	SET @order_index = 1

	OPEN cur
	FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration,
		@mobility_number, @start_date, @end_date, @subject_area_language_list, @subject_area_list, @other_info, @cooperation_condition
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_IIA_PARTNER @sp, @v_s_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_s_iia_partner_id, @sp_contacts

			EXECUTE dbo.INSERTA_IIA_PARTNER @rp, @v_r_iia_partner_id output
			EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_r_iia_partner_id, @rp_contacts

			---

			EXECUTE dbo.INSERTA_IIA_MOBILITY_TYPE @mobility_type, @v_mobility_type_id output
			EXECUTE dbo.INSERTA_DURATION @cop_cond_duration, @v_duration_id output
			EXECUTE dbo.INSERTA_MOBILITY_NUMBER @mobility_number, @v_mob_number_id output

			SET @v_cc_id = NEWID()

			INSERT INTO dbo.EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID,
				MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO, ORDER_INDEX)
				VALUES (@v_cc_id, @END_DATE, @START_DATE, @v_duration_id,
					@v_mob_number_id, @v_mobility_type_id, @v_r_iia_partner_id, @v_s_iia_partner_id, @P_IIA_ID, @other_info, @order_index)

			EXECUTE dbo.INSERTA_SUBAREA_LANSKIL @v_cc_id, @subject_area_language_list
			EXECUTE dbo.INSERTA_SUBAREA_LIST @v_cc_id, @subject_area_list
			EXECUTE dbo.INSERTA_EQFLEVELS @v_cc_id, @cooperation_condition
			SET @order_index = @order_index  +1
			FETCH NEXT FROM cur INTO @sp, @sp_contacts, @rp, @rp_contacts, @mobility_type, @cop_cond_duration,
				@mobility_number, @start_date, @end_date, @subject_area_language_list, @subject_area_list, @other_info, @cooperation_condition
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


/*
	Actualiza un IIA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_IIA'
) DROP PROCEDURE dbo.ACTUALIZA_IIA;
GO
CREATE PROCEDURE  dbo.ACTUALIZA_IIA(@P_IIA_ID varchar(255), @P_IIA xml) AS

BEGIN

	DECLARE @IIA_CODE varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @PDF varchar(max)
	DECLARE @PDF_DECODED varchar(max)
	DECLARE @v_is_exposed integer;
	DECLARE @v_f_partner_id varchar(255)
	DECLARE @v_s_partner_id varchar(255)
	DECLARE @FIRST_PARTNER xml
	DECLARE @FIRST_PARTNER_CONTACTS xml
	DECLARE @SECOND_PARTNER xml
	DECLARE @SECOND_PARTNER_CONTACTS xml
	DECLARE @v_file_id varchar(255)
	DECLARE @v_file_type varchar(255)
	DECLARE @v_internal_iia_id varchar(255)


	SELECT
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list'),
		@PDF = T.c.value('(pdf)[1]', 'varchar(max)'),
		@FIRST_PARTNER = T.c.query('first_partner'),
		@FIRST_PARTNER_CONTACTS = T.c.query('first_partner/partner_contacts_list'),
		@SECOND_PARTNER = T.c.query('second_partner'),
		@SECOND_PARTNER_CONTACTS = T.c.query('second_partner/partner_contacts_list'),
		@v_file_type = T.c.value('(file_mime_type)[1]', 'varchar(255)')
	FROM @P_IIA.nodes('iia') T(c)

	SELECT @v_internal_iia_id = IIA_ID, @v_is_exposed = IS_EXPOSED, @v_f_partner_id = FIRST_PARTNER_ID, @v_s_partner_id  = SECOND_PARTNER_ID FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_IIA_ID);

	IF @v_f_partner_id IS NOT NULL
	BEGIN
		UPDATE EWPCV_IIA  SET FIRST_PARTNER_ID = null WHERE LOWER(ID) = LOWER(@P_IIA_ID);
		EXECUTE dbo.BORRA_IIA_PARTNER @v_f_partner_id;
	END
	IF @v_s_partner_id IS NOT NULL
	BEGIN
		UPDATE EWPCV_IIA  SET SECOND_PARTNER_ID = null WHERE LOWER(ID) = LOWER(@P_IIA_ID);
		EXECUTE dbo.BORRA_IIA_PARTNER @v_s_partner_id;
	END

	EXECUTE dbo.INSERTA_IIA_PARTNER @FIRST_PARTNER, @v_f_partner_id output
	EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_f_partner_id, @FIRST_PARTNER_CONTACTS

	EXECUTE dbo.INSERTA_IIA_PARTNER @SECOND_PARTNER, @v_s_partner_id output
	EXECUTE dbo.INSERTA_PARTNER_CONTACTS @v_s_partner_id, @SECOND_PARTNER_CONTACTS

	IF (@PDF is not null AND @PDF<>'')
	BEGIN TRY
        SET @PDF_DECODED = CONVERT (varchar(max), cast('' as xml).value('xs:base64Binary(sql:variable("@PDF"))', 'varbinary(max)'))
		EXECUTE dbo.INSERTA_FICHERO @PDF, @v_f_partner_id, @v_file_type, @v_file_id output
    END TRY
    BEGIN CATCH
        PRINT 'ERROR a intentar guardar PDF en dbo.EWPCV_IIA, se omite'
    END CATCH

	IF @v_is_exposed = 0
		BEGIN
		UPDATE EWPCV_IIA SET START_DATE = @START_DATE, END_DATE = @END_DATE, IIA_CODE = @IIA_CODE, MODIFY_DATE = SYSDATETIME(), IS_EXPOSED = 1,  FIRST_PARTNER_ID = @v_f_partner_id, SECOND_PARTNER_ID = @v_s_partner_id, PDF_ID = @v_file_id
			WHERE ID = @P_IIA_ID
		END
	ELSE
		BEGIN
		UPDATE EWPCV_IIA SET START_DATE = @START_DATE, END_DATE = @END_DATE, IIA_CODE = @IIA_CODE, MODIFY_DATE = SYSDATETIME(), FIRST_PARTNER_ID = @v_f_partner_id, SECOND_PARTNER_ID = @v_s_partner_id, PDF_ID = @v_file_id
			WHERE ID = @P_IIA_ID
		END

	EXECUTE dbo.BORRA_COOP_COND_SIN_SNAPSHOT @v_internal_iia_id
	EXECUTE dbo.INSERTA_COOP_CONDITIONS @v_internal_iia_id, @COOPERATION_CONDITION_LIST

END
;
GO

/* Elimina un IIA del sistema.
	Recibe como parametro el identificador del Acuerdo Interinstitucional
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_IIA'
) DROP PROCEDURE dbo.DELETE_IIA;
GO
CREATE PROCEDURE dbo.DELETE_IIA(@P_IIA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @v_is_exposed integer
		DECLARE @v_notifier_hei varchar(255)
		DECLARE @v_internal_iia_id varchar(255)

		SET @return_value = 0

		IF @P_HEI_TO_NOTIFY IS NULL
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		IF @return_value = 0
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
			IF  @v_count = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'El IIA no existe.'
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 1
			IF  @v_count > 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se pueden borrar IIAs remotos.'
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE IIA_ID = @P_IIA_ID
			IF  @v_count > 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'Existen movilidades asociadas al IIA.'
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
		    SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA_APPROVALS WHERE LOCAL_IIA_ID = @P_IIA_ID
            IF  @v_count > 0
            BEGIN
                SET @P_ERROR_MESSAGE = 'El IIA ya ha sido aprobado, no se puede borrar.'
                SET @return_value = -1
            END
		END

		IF @return_value = 0
		BEGIN
			EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
			SELECT @v_is_exposed = IS_EXPOSED FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
			SELECT @v_internal_iia_id = IIA_ID FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
			EXECUTE dbo.BORRA_IIA @v_internal_iia_id
			INSERT INTO dbo.EWPCV_DELETED_ELEMENTS(ID, ELEMENT_ID, ELEMENT_TYPE, OWNER_SCHAC, DELETION_DATE)
            		VALUES (NEWID(), @P_IIA_ID, '0', @v_notifier_hei, SYSDATETIME());
            IF @v_is_exposed = 1
            BEGIN
			    EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
			END
			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 2)
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/* Actualiza un IIA del sistema.
	Recibe como parametro del Acuerdo Interinstitucional a actualizar
	Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_IIA'
) DROP PROCEDURE dbo.UPDATE_IIA;
GO
CREATE PROCEDURE dbo.UPDATE_IIA (@P_IIA_ID varchar(255), @P_IIA xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT,
	@P_HEI_TO_NOTIFY VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	--validacion, institution id y ounit code de los partners existen
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	END
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_notifier_hei varchar(255)
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer

			IF @P_HEI_TO_NOTIFY IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END

			IF @return_value = 0
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
				IF  @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'El IIA no existe.'
					SET @return_value = -1
				END
			END

			IF @return_value = 0
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 1
				IF  @v_count > 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden actualizar IIAs remotos.'
					SET @return_value = -1
				END
			END

			IF @return_value = 0
			BEGIN
				EXECUTE dbo.ACTUALIZA_IIA @P_IIA_ID, @P_IIA
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei

				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
				    VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 1)

			END 
			COMMIT TRANSACTION

		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_IIA'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*  Genera un CNR de aceptación con el id del IIA remoto de la institución indicada.
	Este CNR desencadena todo el proceso de aceptación de IIAs.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'APPROVE_IIA'
) DROP PROCEDURE dbo.APPROVE_IIA;
GO
CREATE PROCEDURE  dbo.APPROVE_IIA(@P_INTERNAL_ID_REMOTE_IIA_COPY varchar(255), @P_HEI_ID_TO_NOTIFY varchar(255),
	@P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_remote_iia_id varchar(255)
		DECLARE @v_notifier_hei varchar(255)
        DECLARE @v_id varchar(255)

		SET @return_value = 0

		IF @P_INTERNAL_ID_REMOTE_IIA_COPY IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id interno de la copia del IIA remoto';
				SET @return_value = -1
			END
		ELSE
			IF @P_HEI_ID_TO_NOTIFY IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el hei-id de la institución a notificar';
				SET @return_value = -1
			END

		IF @return_value = 0
		BEGIN
			SELECT @v_remote_iia_id = REMOTE_IIA_ID
				FROM dbo.EWPCV_IIA WHERE ID = @P_INTERNAL_ID_REMOTE_IIA_COPY
			EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_INTERNAL_ID_REMOTE_IIA_COPY, @P_HEI_ID_TO_NOTIFY, @v_notifier_hei output
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_NOTIFICATION (ID, VERSION, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, RETRIES, PROCESSING, OWNER_HEI)
				VALUES (@v_id, 0, @v_remote_iia_id, @P_HEI_ID_TO_NOTIFY, SYSDATETIME(), 4, 1, 0, 0, @v_notifier_hei)
			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_id, 10, 0)
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.APPROVE_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/*
	Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto
	y hash de las cooperation condition del iia remoto en nuestro IIA propio.
	Recibe como parámetro el identificador propio y el identificador de la copia remota.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BIND_IIA'
) DROP PROCEDURE dbo.BIND_IIA;
GO
CREATE PROCEDURE  dbo.BIND_IIA(@P_OWN_IIA_ID varchar(255), @P_ID_FROM_REMOTE_IIA varchar(255),
	@P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_remote_iia_id varchar(255)
		DECLARE @v_remote_coop_cond_hash varchar(255)
		DECLARE @v_remote_iia_code varchar(255)
        DECLARE @v_id varchar(255)
		DECLARE @v_count_iia integer

		SET @return_value = 0

		IF @P_OWN_IIA_ID IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END
		ELSE
			IF @P_ID_FROM_REMOTE_IIA IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA remoto';
				SET @return_value = -1
			END

		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_OWN_IIA_ID
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_OWN_IIA_ID';
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
			SELECT
				@v_remote_iia_id = IIA.REMOTE_IIA_ID,
				@v_remote_coop_cond_hash = IIA.REMOTE_COP_COND_HASH,
				@v_remote_iia_code = IIA.REMOTE_IIA_CODE
			FROM EWPCV_IIA IIA
				WHERE IIA.ID = @P_ID_FROM_REMOTE_IIA

			IF @v_remote_iia_id is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No existe ningún IIA asociado al id que se ha informado como parámetro P_REMOTE_IIA_ID';
					SET @return_value = -1
				END
			ELSE
			    BEGIN
                    UPDATE EWPCV_IIA SET
                        REMOTE_IIA_ID = @v_remote_iia_id,
                        REMOTE_IIA_CODE = @v_remote_iia_code,
                        MODIFY_DATE =  SYSDATETIME()
                        WHERE ID = @P_OWN_IIA_ID
                    INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
                        VALUES (NEWID(), @P_OWN_IIA_ID, @P_OWN_IIA_ID, 0, 1)
				END
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.BIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/*
	Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto
	y hash de las cooperation condition del iia remoto en nuestro IIA propio.
	Recibe como parámetro el identificador propio y el identificador de la copia remota.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UNBIND_IIA'
) DROP PROCEDURE dbo.UNBIND_IIA;
GO
CREATE PROCEDURE  dbo.UNBIND_IIA(@P_IIA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @v_count_iia integer
		SET @return_value = 0

		IF @P_IIA_ID IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END

		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 0;
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA LOCAL asociado al id que se ha informado';
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
				UPDATE EWPCV_IIA SET
					REMOTE_IIA_ID = null,
					REMOTE_IIA_CODE = null,
					MODIFY_DATE =  SYSDATETIME()
					WHERE ID = @P_IIA_ID
				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
				    VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 1)
		END
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UNBIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'RECOVER_IIA_SNAPSHOT'
) DROP PROCEDURE dbo.RECOVER_IIA_SNAPSHOT;
GO
CREATE PROCEDURE dbo.RECOVER_IIA_SNAPSHOT
    (@P_IIA_ID varchar(255),
	@P_ERROR_MESSAGE varchar(MAX) OUTPUT,
    @P_ERROR_NUMBER integer OUTPUT)
AS
BEGIN
    BEGIN TRY
        BEGIN TRANSACTION
        DECLARE @v_internal_iia_id varchar(255)
		DECLARE @V_IIA_COOP_COND_HASH varchar(255)
		DECLARE @V_COUNT integer
        IF @P_ERROR_MESSAGE IS NULL
            SET @P_ERROR_MESSAGE = '';

        SELECT @v_internal_iia_id = IIA_ID FROM EWPCV_IIA WHERE ID = @P_IIA_ID;

        SELECT @V_COUNT = COUNT(1) FROM EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = '0';
        IF @V_COUNT = 0
        BEGIN
            SET @P_ERROR_MESSAGE = 'El IIA introducido no existe como IIA propio';
            SET @P_ERROR_NUMBER = -1
            RETURN
        END

        SELECT @V_COUNT = COUNT(1) FROM EWPCV_IIA_APPROVALS WHERE LOCAL_IIA_ID = @P_IIA_ID;
        IF @V_COUNT = 0
        BEGIN
            SET @P_ERROR_MESSAGE = 'El IIA introducido no tiene ninguna snapshot a la que revertir';
            SET @P_ERROR_NUMBER = -1
            RETURN
        END

        SELECT @V_COUNT = COUNT(1) FROM EWPCV_COOPERATION_CONDITION WHERE IIA_ID = @v_internal_iia_id AND IIA_APPROVAL_ID IS NULL;
        IF @V_COUNT = 0
        BEGIN
            SET @P_ERROR_MESSAGE = 'El IIA introducido no tiene cambios que revertir desde la ultima snapshot tomada';
            SET @P_ERROR_NUMBER = -1
            RETURN
        END

        --borramos las ccs en negociacion negociacion
        EXECUTE dbo.BORRA_COOP_COND_SIN_SNAPSHOT @v_internal_iia_id;

        SELECT TOP 1 @V_IIA_COOP_COND_HASH = IIA_APPROVAL.local_hash
        FROM EWPCV_IIA_APPROVALS IIA_APPROVAL
        WHERE IIA_APPROVAL.LOCAL_IIA_ID = @P_IIA_ID
        ORDER BY IIA_APPROVAL.date_iia_approval DESC;

        --revertimos el hash, esto no es realmente necesario en este punto ya que lo calcularemos de nuevo cuando nos hagan un get pero lo actualizamos para evitar tener informacion desalineada.
        UPDATE EWPCV_IIA SET REMOTE_COP_COND_HASH = @V_IIA_COOP_COND_HASH, MODIFY_DATE = CURRENT_TIMESTAMP WHERE ID = @P_IIA_ID;

        INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
            VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 1)

        --obtenemos las instituciones para crear la notificacion.
		DECLARE @FIRST_INSTITUTION varchar(255)
        DECLARE @SECOND_INSTITUTION varchar(255)

        SELECT @FIRST_INSTITUTION = FP.INSTITUTION_ID, @SECOND_INSTITUTION = SP.INSTITUTION_ID
        FROM EWPCV_IIA I
        LEFT JOIN EWPCV_IIA_PARTNER FP
            ON I.FIRST_PARTNER_ID = FP.ID
        LEFT JOIN EWPCV_IIA_PARTNER SP
            ON I.SECOND_PARTNER_ID = SP.ID
        WHERE I.ID = @P_IIA_ID

        DECLARE @v_regression_id varchar(255);
        SET @v_regression_id = NEWID();
        INSERT INTO EWPCV_IIA_REGRESSIONS(ID, IIA_ID, IIA_HASH, OWNER_SCHAC, REGRESSION_DATE) VALUES (@v_regression_id, @P_IIA_ID, @V_IIA_COOP_COND_HASH, LOWER(@FIRST_INSTITUTION), CURRENT_TIMESTAMP);
        INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_regression_id, 13, 0)


        DECLARE @v_notification_id varchar(255);
        SET @v_notification_id = NEWID();
	    INSERT INTO EWPCV_NOTIFICATION (ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, PROCESSING, OWNER_HEI)
    				VALUES (@v_notification_id, @P_IIA_ID, LOWER(@SECOND_INSTITUTION), SYSDATETIME(), 0, 1, 0, LOWER(@FIRST_INSTITUTION))

        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        SET @P_ERROR_MESSAGE = CONCAT('Error: ', ERROR_MESSAGE())
        SET @P_ERROR_NUMBER = -1
        ROLLBACK TRANSACTION
    END CATCH
END
GO


/*
	Borra las subject areas asociadas a una condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_COND_SUBAREA_LANSKILL'
) DROP PROCEDURE dbo.BORRA_COOP_COND_SUBAREA_LANSKILL;
GO
CREATE PROCEDURE  dbo.BORRA_COOP_COND_SUBAREA_LANSKILL(@P_COOP_COND_ID varchar(255)) AS
BEGIN
	DECLARE @ID varchar(255)
	DECLARE @ISCED_CODE varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID, ISCED_CODE
		FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL
		WHERE COOPERATION_CONDITION_ID = @P_COOP_COND_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @ISCED_CODE

	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_SUBAR_LANSKIL WHERE ID = @ID;
			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			FETCH NEXT FROM cur INTO @ID, @ISCED_CODE
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Borra las subject areas asociadas a una condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_COOP_COND_SUBAREA'
) DROP PROCEDURE dbo.BORRA_COOP_COND_SUBAREA;
GO
CREATE PROCEDURE  dbo.BORRA_COOP_COND_SUBAREA(@P_COOP_COND_ID varchar(255)) AS
BEGIN
	DECLARE @ID varchar(255)
	DECLARE @ISCED_CODE varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID, ISCED_CODE
		FROM dbo.EWPCV_COOPCOND_SUBAR
		WHERE COOPERATION_CONDITION_ID = @P_COOP_COND_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID, @ISCED_CODE

	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_COOPCOND_SUBAR WHERE ID = @ID;
			DELETE FROM dbo.EWPCV_SUBJECT_AREA WHERE ID = @ISCED_CODE

			FETCH NEXT FROM cur INTO @ID, @ISCED_CODE
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'PREPARE_SNAPSHOTS'
) DROP PROCEDURE dbo.PREPARE_SNAPSHOTS;
GO
CREATE PROCEDURE dbo.PREPARE_SNAPSHOTS
    (@P_ERROR_MESSAGE varchar(MAX) OUTPUT,
	@P_ERROR_NUMBER integer OUTPUT)
AS
BEGIN
	DECLARE @REMOTE_IIA_ID varchar(255);

    BEGIN TRY
		BEGIN TRANSACTION;

		DECLARE cursor_IIA CURSOR FOR
		SELECT IIA.REMOTE_IIA_ID
		FROM EWPCV_IIA IIA
		WHERE IIA.approval_date IS NOT NULL
			AND IIA.REMOTE_IIA_ID NOT IN (SELECT REMOTE_IIA_ID FROM EWPCV_IIA_APPROVALS)
			AND IIA.REMOTE_IIA_ID IS NOT NULL
		GROUP BY IIA.REMOTE_IIA_ID
		HAVING COUNT(1) > 1;

		OPEN cursor_IIA;
		FETCH NEXT FROM cursor_IIA INTO @REMOTE_IIA_ID;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @ID varchar(255);
			DECLARE @FIRST_INSTITUTION varchar(255);
			DECLARE @SECOND_INSTITUTION varchar(255);

			SELECT @ID = IIA.ID
			FROM EWPCV_IIA IIA
			WHERE IIA.approval_date IS NOT NULL
				AND IIA.REMOTE_IIA_ID NOT IN (SELECT REMOTE_IIA_ID FROM EWPCV_IIA_APPROVALS)
				AND IS_REMOTE = 0
				AND IIA.REMOTE_IIA_ID = @REMOTE_IIA_ID;

			SELECT @FIRST_INSTITUTION = FP.INSTITUTION_ID, @SECOND_INSTITUTION = SP.INSTITUTION_ID
			FROM EWPCV_IIA I
			LEFT JOIN EWPCV_IIA_PARTNER FP
				ON I.FIRST_PARTNER_ID = FP.ID
			LEFT JOIN EWPCV_IIA_PARTNER SP
				ON I.SECOND_PARTNER_ID = SP.ID
			WHERE I.ID = @ID;

            DECLARE @v_notification_id varchar(255);
            SET @v_notification_id = NEWID();
			INSERT INTO EWPCV_NOTIFICATION (ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, TYPE, CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (@v_notification_id, @REMOTE_IIA_ID, LOWER(@SECOND_INSTITUTION), SYSDATETIME(), 0, 0, 0, LOWER(@FIRST_INSTITUTION));
			INSERT INTO EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_notification_id, 10, 0)

			FETCH NEXT FROM cursor_IIA INTO @REMOTE_IIA_ID;
		END

		CLOSE cursor_IIA;
		DEALLOCATE cursor_IIA;

		COMMIT;
    END TRY
    BEGIN CATCH
		SET @P_ERROR_MESSAGE = CONCAT('Error: ', ERROR_MESSAGE());
        SET @P_ERROR_NUMBER = -1
		ROLLBACK;
    END CATCH;
END;
GO


/*
 Clona la duracion de una condicion de cooperacion de un IIA y retorna el id del nuevo registro
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_DURATION'
) DROP PROCEDURE dbo.CLONE_DURATION;
GO
CREATE PROCEDURE dbo.CLONE_DURATION
    (@P_DURATION_ID varchar(255), @P_NEW_DURATION_ID varchar(255) OUTPUT)
AS
BEGIN
    DECLARE @v_number_duration float;
    DECLARE @v_unit integer;
    IF @P_DURATION_ID IS NOT NULL
    BEGIN
        SELECT @v_number_duration = NUMBERDURATION, @v_unit = UNIT
        FROM EWPCV_DURATION
        WHERE ID = @P_DURATION_ID;

        IF @v_number_duration IS NOT NULL
        BEGIN
            SET @P_NEW_DURATION_ID = NEWID();
            INSERT INTO EWPCV_DURATION (ID, NUMBERDURATION, UNIT) VALUES (@P_NEW_DURATION_ID, @v_number_duration, @v_unit);
        END
    END
END;
GO

/*
  Clona el mobility_number de una condicion de cooperacion de un IIA y retorna el id del nuevo registro
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_MOBILITY_NUMBER'
) DROP PROCEDURE dbo.CLONE_MOBILITY_NUMBER;
GO
CREATE PROCEDURE dbo.CLONE_MOBILITY_NUMBER
    (@P_MOBILITY_NUMBER_ID varchar(255), @P_NEW_MOBILITY_NUMBER_ID varchar(255) OUTPUT)
AS
BEGIN
    DECLARE @v_number float;
    DECLARE @v_variant integer;
    IF @P_MOBILITY_NUMBER_ID IS NOT NULL
    BEGIN
        SELECT @v_number = NUMBERMOBILITY, @v_variant = VARIANT
        FROM EWPCV_MOBILITY_NUMBER
        WHERE ID = @P_MOBILITY_NUMBER_ID;

        IF @v_number IS NOT NULL
        BEGIN
            SET @P_NEW_MOBILITY_NUMBER_ID = NEWID();
            INSERT INTO EWPCV_MOBILITY_NUMBER (ID, NUMBERMOBILITY, VARIANT) VALUES (@P_NEW_MOBILITY_NUMBER_ID, @v_number, @v_variant);
        END
    END
END;
GO

/*
  Clona los eqflevels de una condicion de cooperacion de un IIA y retorna el id del nuevo registro
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_COOPCOND_EQFLEVEL'
) DROP PROCEDURE dbo.CLONE_COOPCOND_EQFLEVEL;
GO
CREATE PROCEDURE dbo.CLONE_COOPCOND_EQFLEVEL
    (@P_NEW_COOP_COND_ID varchar(255), @P_OLD_COOP_COND_ID varchar(255))
AS
BEGIN
    DECLARE @v_count integer;
    DECLARE @v_eqf_level varchar(255);
    SELECT @v_count = COUNT(1) FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;
    IF @v_count > 0
    BEGIN
        DECLARE cur CURSOR LOCAL FOR
        SELECT EQF_LEVEL FROM EWPCV_COOPCOND_EQFLVL WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;

        OPEN cur;
        FETCH NEXT FROM cur INTO @v_eqf_level;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO EWPCV_COOPCOND_EQFLVL (COOPERATION_CONDITION_ID, EQF_LEVEL) VALUES (@P_NEW_COOP_COND_ID, @v_eqf_level);
            FETCH NEXT FROM cur INTO @v_eqf_level;
        END
        CLOSE cur;
        DEALLOCATE cur;
    END
END;
GO

/*
     Clona un subject area y retorna su ID
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_SUBJECT_AREA'
) DROP PROCEDURE dbo.CLONE_SUBJECT_AREA;
GO
CREATE PROCEDURE dbo.CLONE_SUBJECT_AREA
    (@P_SUBJECT_AREA_ID varchar(255), @P_NEW_SUBJECT_AREA_ID varchar(255) OUTPUT)
AS
BEGIN
    DECLARE @v_isced_code varchar(255);
    DECLARE @v_isced_clarification varchar(1000);
    IF @P_SUBJECT_AREA_ID IS NOT NULL
    BEGIN
        SELECT @v_isced_code = ISCED_CODE, @v_isced_clarification = ISCED_CLARIFICATION
        FROM EWPCV_SUBJECT_AREA
        WHERE ID = @P_SUBJECT_AREA_ID;

        IF @v_isced_code IS NOT NULL
        BEGIN
            SET @P_NEW_SUBJECT_AREA_ID = NEWID();
            INSERT INTO EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (@P_NEW_SUBJECT_AREA_ID, @v_isced_code, @v_isced_clarification);
        END
    END
END;
GO

/*
 Clona las subareas de una condicion de cooperacion de un IIA y las asocia a la nueva condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_COOPCOND_SUBAREA'
) DROP PROCEDURE dbo.CLONE_COOPCOND_SUBAREA;
GO
CREATE PROCEDURE dbo.CLONE_COOPCOND_SUBAREA
    (@P_NEW_COOP_COND_ID varchar(255), @P_OLD_COOP_COND_ID varchar(255))
AS
BEGIN
    DECLARE @v_count integer;
    DECLARE @v_old_subarea_id varchar(255);
    DECLARE @v_subarea_id varchar(255);
    DECLARE @v_cc_subarea_id varchar(255);
    SELECT @v_count = COUNT(1) FROM EWPCV_COOPCOND_SUBAR WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;
    IF @v_count > 0
    BEGIN
        DECLARE cur CURSOR LOCAL FOR
        SELECT ISCED_CODE FROM EWPCV_COOPCOND_SUBAR WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;

        OPEN cur;
        FETCH NEXT FROM cur INTO @v_old_subarea_id;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @v_cc_subarea_id = NEWID();
            EXECUTE dbo.CLONE_SUBJECT_AREA @v_old_subarea_id, @v_subarea_id OUTPUT;
            INSERT INTO EWPCV_COOPCOND_SUBAR (ID, COOPERATION_CONDITION_ID, ISCED_CODE) VALUES (@v_cc_subarea_id, @P_NEW_COOP_COND_ID, @v_subarea_id);
            FETCH NEXT FROM cur INTO @v_subarea_id;
        END
        CLOSE cur;
        DEALLOCATE cur;
    END
END;
GO

/*
 Clona los language skills de una condicion de cooperacion de un IIA y los asocia a la nueva condicion de cooperacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_COOPCOND_SUBARLANSKIL'
) DROP PROCEDURE dbo.CLONE_COOPCOND_SUBARLANSKIL;
GO
CREATE PROCEDURE dbo.CLONE_COOPCOND_SUBARLANSKIL
    (@P_NEW_COOP_COND_ID varchar(255), @P_OLD_COOP_COND_ID varchar(255))
AS
BEGIN
    DECLARE @v_count integer;
    DECLARE @v_old_subarea_id varchar(255);
    DECLARE @v_subarea_id varchar(255);
    DECLARE @v_cc_subarlanskil_id varchar(255);
    DECLARE @v_language_skill_id varchar(255);
    SELECT @v_count = COUNT(1) FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;
    IF @v_count > 0
    BEGIN
        DECLARE cur CURSOR LOCAL FOR
        SELECT ISCED_CODE, LANGUAGE_SKILL_ID FROM EWPCV_COOPCOND_SUBAR_LANSKIL WHERE COOPERATION_CONDITION_ID = @P_OLD_COOP_COND_ID;

        OPEN cur;
        FETCH NEXT FROM cur INTO @v_old_subarea_id, @v_language_skill_id;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @v_cc_subarlanskil_id = NEWID();
            EXECUTE dbo.CLONE_SUBJECT_AREA @v_old_subarea_id, @v_subarea_id OUTPUT;
            INSERT INTO EWPCV_COOPCOND_SUBAR_LANSKIL (ID, COOPERATION_CONDITION_ID, ISCED_CODE, LANGUAGE_SKILL_ID) VALUES (@v_cc_subarlanskil_id, @P_NEW_COOP_COND_ID, @v_subarea_id, @v_language_skill_id);
            FETCH NEXT FROM cur INTO @v_subarea_id, @v_language_skill_id;
        END
        CLOSE cur;
        DEALLOCATE cur;
    END
END;
GO

/*
  Dado un partner_id clona un partner y retorna el nuevo id
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_IIA_PARTNER'
) DROP PROCEDURE dbo.CLONE_IIA_PARTNER;
GO
CREATE PROCEDURE dbo.CLONE_IIA_PARTNER
    (@P_PARTNER_ID varchar(255), @P_NEW_PARTNER_ID varchar(255) OUTPUT)
AS
BEGIN
    DECLARE @v_inst_id varchar(255);
    DECLARE @v_ounit_id varchar(255);
    DECLARE @v_c_id varchar(255);
    DECLARE @v_id varchar(255);
    DECLARE @v_count integer;
    DECLARE cur CURSOR LOCAL FOR
    SELECT INSTITUTION_ID, ORGANIZATION_UNIT_ID
    FROM EWPCV_IIA_PARTNER
    WHERE ID = @P_PARTNER_ID;

    IF @P_PARTNER_ID IS NOT NULL
    BEGIN

        OPEN cur;
        FETCH NEXT FROM cur INTO @v_inst_id, @v_ounit_id;
        CLOSE cur;
        IF @v_inst_id IS NOT NULL
        BEGIN
            SET @P_NEW_PARTNER_ID = NEWID();
            INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID) VALUES (@P_NEW_PARTNER_ID, @v_inst_id, @v_ounit_id);

            SELECT @v_count = COUNT(1) FROM EWPCV_IIA_PARTNER_CONTACTS WHERE IIA_PARTNER_ID = @P_PARTNER_ID;
            IF @v_count > 0
            BEGIN
                DECLARE cur2 CURSOR LOCAL FOR
                SELECT CONTACTS_ID FROM EWPCV_IIA_PARTNER_CONTACTS WHERE IIA_PARTNER_ID = @P_PARTNER_ID;

                OPEN cur2;
                FETCH NEXT FROM cur2 INTO @v_c_id;

                WHILE @@FETCH_STATUS = 0
                BEGIN
                    EXECUTE dbo.CLONE_CONTACT @v_c_id, @v_id OUTPUT;
                    INSERT INTO EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (@P_NEW_PARTNER_ID, @v_id);
                    FETCH NEXT FROM cur2 INTO @v_c_id;
                END
                CLOSE cur2;
                DEALLOCATE cur2;
            END
        END
    END
END;
GO

/*
  Dado un IIA_ID y un IIA_APPROVAL_ID creará una copia completa de las condiciones de seguridad de un IIA para asociarlas a la version de terminación
  Esto implica generar una copia de Mobility_number, duration, coopcondeqflevel, coopcondsubarea, coopcondsubarlanskil y subjectAreas, pero no de los language_skill o mobility type
  Este metodo clona las condiciones de cooperacion pero sin asociarlas a ningun approval_id
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_COOP_COND'
) DROP PROCEDURE dbo.CLONE_COOP_COND;
GO
CREATE PROCEDURE dbo.CLONE_COOP_COND
    (@P_IIA_ID varchar(255), @P_IIA_APPROVAL_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT)
AS
BEGIN
    DECLARE @v_old_cc_id varchar(255);
    DECLARE @v_end_date datetime;
    DECLARE @v_start_date datetime;
    DECLARE @v_mobility_type_id varchar(255);
    DECLARE @v_other_info varchar(4000);
    DECLARE @v_blended integer;
    DECLARE @v_order_index integer;
    DECLARE @v_old_duration_id varchar(255);
    DECLARE @v_old_mob_number_id varchar(255);
    DECLARE @v_old_sending_partner_id varchar(255);
    DECLARE @v_old_receiving_partner_id varchar(255);
    DECLARE @v_duration_id varchar(255);
    DECLARE @v_mob_number_id varchar(255);
    DECLARE @v_sending_partner_id varchar(255);
    DECLARE @v_receiving_partner_id varchar(255);
    DECLARE @v_cc_id varchar(255);
    DECLARE @v_count integer;

    IF @P_IIA_ID IS NOT NULL AND @P_IIA_APPROVAL_ID IS NOT NULL
    BEGIN
        DECLARE cur CURSOR LOCAL FOR
        SELECT ID, END_DATE, START_DATE, DURATION_ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, OTHER_INFO, BLENDED, ORDER_INDEX
        FROM EWPCV_COOPERATION_CONDITION
        WHERE IIA_ID = @P_IIA_ID AND IIA_APPROVAL_ID = @P_IIA_APPROVAL_ID;

        OPEN cur;
        FETCH NEXT FROM cur INTO @v_old_cc_id, @v_end_date, @v_start_date, @v_old_duration_id, @v_old_mob_number_id, @v_mobility_type_id, @v_old_receiving_partner_id, @v_old_sending_partner_id, @v_other_info, @v_blended, @v_order_index;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @v_cc_id = NEWID();
            EXECUTE dbo.CLONE_DURATION @v_old_duration_id, @v_duration_id OUTPUT;
            EXECUTE dbo.CLONE_MOBILITY_NUMBER @v_old_mob_number_id, @v_mob_number_id OUTPUT;
            EXECUTE dbo.CLONE_IIA_PARTNER @v_old_sending_partner_id, @v_sending_partner_id OUTPUT;
            EXECUTE dbo.CLONE_IIA_PARTNER @v_old_receiving_partner_id, @v_receiving_partner_id OUTPUT;

            INSERT INTO EWPCV_COOPERATION_CONDITION (ID, END_DATE, START_DATE, DURATION_ID, MOBILITY_NUMBER_ID, MOBILITY_TYPE_ID, RECEIVING_PARTNER_ID, SENDING_PARTNER_ID, IIA_ID, OTHER_INFO, BLENDED, ORDER_INDEX)
            VALUES (@v_cc_id, @v_end_date, @v_start_date, @v_duration_id, @v_mob_number_id, @v_mobility_type_id, @v_receiving_partner_id, @v_sending_partner_id, @P_IIA_ID, @v_other_info, @v_blended, @v_order_index);

            EXECUTE dbo.CLONE_COOPCOND_EQFLEVEL @v_cc_id, @v_old_cc_id;
            EXECUTE dbo.CLONE_COOPCOND_SUBAREA @v_cc_id, @v_old_cc_id;
            EXECUTE dbo.CLONE_COOPCOND_SUBARLANSKIL @v_cc_id, @v_old_cc_id;
            FETCH NEXT FROM cur INTO @v_old_cc_id, @v_end_date, @v_start_date, @v_old_duration_id, @v_old_mob_number_id, @v_mobility_type_id, @v_old_receiving_partner_id, @v_old_sending_partner_id, @v_other_info, @v_blended, @v_order_index;
        END
        CLOSE cur;
        DEALLOCATE cur;
    END
END;
GO

/*
        Finalizar unilateralmente un IIA que ya haya alcanzado un mutuo acuerdo.
     */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'TERMINATE_IIA'
) DROP PROCEDURE dbo.TERMINATE_IIA;
GO
CREATE PROCEDURE  dbo.TERMINATE_IIA(@P_IIA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS

BEGIN
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @v_count_iia integer
		DECLARE @v_count_approval integer
		DECLARE @v_count_negotiation integer
		DECLARE @v_count_terminated integer
		DECLARE @v_internal_iia_id varchar(255)
		SET @return_value = 0

		IF @P_IIA_ID IS NULL
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha encontrado el IIA indicado';
				SET @return_value = -1
			END

		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 0;
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA LOCAL asociado al id que se ha informado';
				SET @return_value = -1
			END
		END

		IF @return_value = 0
		BEGIN
			SELECT @v_count_approval = COUNT(1) FROM dbo.EWPCV_IIA_APPROVALS WHERE LOCAL_IIA_ID = @P_IIA_ID
			IF @v_count_approval = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha alcanzado un acuerdo mutuo en el IIA. No se puede terminar.';
				SET @return_value = -1
			END
		END

        IF @return_value = 0
        BEGIN
            SELECT @v_count_negotiation = count(1) FROM dbo.EWPCV_COOPERATION_CONDITION WHERE IIA_ID = @P_IIA_ID AND IIA_APPROVAL_ID IS NULL
            IF @v_count_negotiation > 0
            BEGIN
                SET @P_ERROR_MESSAGE = 'No se puede terminar el IIA porque esta en negociación. Debes revertir al estado de mutuo acuerdo antes de terminarlo.';
                SET @return_value = -1
            END
        END

        DECLARE @v_iia_approval_id varchar(255);
        SELECT TOP 1 @v_iia_approval_id = ID FROM dbo.EWPCV_IIA_APPROVALS WHERE LOCAL_IIA_ID = @P_IIA_ID ORDER BY DATE_IIA_APPROVAL DESC
        SELECT @v_internal_iia_id = IIA_ID FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
        IF @return_value = 0
                BEGIN
                    SELECT @v_count_terminated = count(1) FROM dbo.EWPCV_COOPERATION_CONDITION WHERE IIA_ID = @v_internal_iia_id AND IIA_APPROVAL_ID = @v_iia_approval_id AND TERMINATED = 1
                    IF @v_count_terminated > 0
                    BEGIN
                        SET @P_ERROR_MESSAGE = 'El IIA ya ha sido terminado. No se puede terminar dos veces.'
                        SET @return_value = -2
                    END
                END

		IF @return_value = 0
		BEGIN
            EXECUTE dbo.CLONE_COOP_COND @v_internal_iia_id, @v_iia_approval_id, @P_ERROR_MESSAGE
            UPDATE dbo.EWPCV_COOPERATION_CONDITION SET TERMINATED = 1 WHERE IIA_ID = @v_internal_iia_id AND IIA_APPROVAL_ID IS NULL;

            INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
                VALUES (NEWID(), @P_IIA_ID, @P_IIA_ID, 0, 1)

			EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output
			EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
		END

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.TERMINATE_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO