

/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA INSTITUTION

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_INSTITUTION' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>
		
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="institution_name" type="languageItemListType" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="university_contact_details" minOccurs="0" >
					<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="httpListWithOptionalLang"  minOccurs="0" />
				<xs:element name="logo_url" type="HTTPS"  minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.VALIDA_INSTITUTION(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_INSTITUTION)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO
	
/*
	Borra una institución
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INSTITUTION'
) DROP PROCEDURE dbo.BORRA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.BORRA_INSTITUTION(@P_INSTITUTION_ID varchar(255)) AS
BEGIN
	DECLARE @v_factsheet_id varchar(255)
	DECLARE @v_inst_id_interop varchar(255)
	DECLARE @URL_ID varchar(255)
	DECLARE @CONTACT_ID varchar(255)

	SELECT 
		@v_inst_id_interop = ID,
		@v_factsheet_id = FACT_SHEET
		FROM EWPCV_INSTITUTION
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);

	UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE UPPER(INSTITUTION_ID) = @P_INSTITUTION_ID;     
	
	EXECUTE dbo.BORRA_INSTITUTION_NAMES @v_inst_id_interop
	
	--Borrado de URLs de factsheet y factsheet
	EXECUTE dbo.BORRA_INST_FACTSHEET_URL @v_inst_id_interop
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @v_factsheet_id
        
	--Borrado de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM EWPCV_CONTACT 
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE dbo.BORRA_CONTACT @CONTACT_ID
			FETCH NEXT FROM cur INTO @CONTACT_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
	--Borro la institucion
	DELETE FROM dbo.EWPCV_INSTITUTION WHERE ID = @v_inst_id_interop

END;
GO
	
/* 
	Elimina una INSTITUTION del sistema.
	Recibe como parametros: 
    El identificador (SCHAC) de la INSTITUTION
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_INSTITUTION'
) DROP PROCEDURE dbo.DELETE_INSTITUTION;
GO 
CREATE PROCEDURE dbo.DELETE_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer

		SET @return_value = 0
		--validacion existe institucion
		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION WHERE INSTITUTION_ID = @P_INSTITUTION_ID
		IF @v_count = 0 
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = 'La institución no existe'
			END 
			ELSE 
			BEGIN
				--validacion intitucion	sin factsheet
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
					AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución tiene un factsheet asociado, bórrelo antes de borrar la institución')
					END
				--validacion intitucion	sin ounits
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución tiene unidades organizativas dependientes, bórrelas antes de borrar la institución')
					END
					
				IF @return_value = 0
					BEGIN
						EXECUTE BORRA_INSTITUTION @P_INSTITUTION_ID
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_INSTITUTION_ID, 6, 2)
					END		
			END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_INSTITUTION'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END 
;
GO
	
/* 
	Inserta una INSTITUTION en el sistema
	Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
	Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/ 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_INSTITUTION'
) DROP PROCEDURE dbo.INSERT_INSTITUTION;
GO    
CREATE PROCEDURE dbo.INSERT_INSTITUTION(@P_INSTITUTION xml, @P_INSTITUTION_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
    DECLARE @v_count integer
	EXECUTE dbo.VALIDA_INSTITUTION @P_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
	SET @v_count = 0
	SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION WHERE INSTITUTION_ID = @P_INSTITUTION.value('(/institution/institution_id)[1]', 'varchar(255)')
	IF @v_count > 0
        BEGIN
            SET @return_value = -1
            SET @P_ERROR_MESSAGE = 'La institución ya existe en el sistema'
        END
    ELSE
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @v_prim_contact_id varchar(255)
			DECLARE @v_contact_id varchar(255)
			DECLARE @CONTACT_PERSON xml

			DECLARE @INSTITUTION_ID varchar(255)
			DECLARE @ABBREVIATION varchar(255)
			DECLARE @LOGO_URL varchar(255)
			DECLARE @INSTITUTION_NAME_LIST xml
			DECLARE @UNIVERSITY_CONTACT_DETAILS xml

			SELECT 
				@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
				@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
				@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
				@INSTITUTION_NAME_LIST = T.c.query('institution_name'),
				@UNIVERSITY_CONTACT_DETAILS = T.c.query('university_contact_details')
			FROM @P_INSTITUTION.nodes('institution') T(c)
			
			-- Insertamos el contacto principal
			EXECUTE dbo.INSERTA_CONTACT @UNIVERSITY_CONTACT_DETAILS, @INSTITUTION_ID, NULL, @v_prim_contact_id OUTPUT

			-- Insertamos la lista de contactos
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_INSTITUTION.nodes('institution/contact_details_list/contact_person') T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, NULL, @v_contact_id OUTPUT
					FETCH NEXT FROM cur INTO @CONTACT_PERSON
				END
			CLOSE cur
			DEALLOCATE cur
			
			-- Insertamos la INSTITUTION en EWPCV_INSTITUTION
			EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, @ABBREVIATION, @LOGO_URL, @INSTITUTION_NAME_LIST, @v_prim_contact_id, @P_INSTITUTION_ID OUTPUT

			-- Insertamos la lista de fact sheet urls
            EXECUTE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS @P_INSTITUTION, @P_INSTITUTION_ID

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @INSTITUTION_ID, 6, 0)

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_INSTITUTION'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	Inserta las urls de factsheet de la institución  y devuelve el identificador del factsheet generado o existente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION_FACTSHEET_URLS'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS;
GO 
CREATE PROCEDURE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS(@P_INSTITUTION xml, @P_INSTITUTION_ID varchar(255)) as
BEGIN
	DECLARE @v_lang_item_id VARCHAR(255)
	DECLARE @v_institution_id VARCHAR(255)
	DECLARE @v_factsheet_id VARCHAR(255)
	DECLARE @LANG VARCHAR(255)
	DECLARE @TEXT VARCHAR(255)
	DECLARE @FACTSHEET_URL xml
	
	SELECT  @TEXT = T.c.value('(.)[1]', 'varchar(255)') FROM @P_INSTITUTION.nodes('institution/factsheet_url_list/text') T(c)
	IF @TEXT IS NOT NULL
	BEGIN

		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.value('(@lang)[1]', 'varchar(255)'),
			T.c.value('(.)[1]', 'varchar(255)'),
			T.c.query('.')
		FROM @P_INSTITUTION.nodes('institution/factsheet_url_list/text') T(c)
		
		OPEN cur
		FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_lang_item_id = NULL

				SELECT @v_lang_item_id = ID
					FROM dbo.EWPCV_LANGUAGE_ITEM LAIT 
					INNER JOIN dbo.EWPCV_INST_FACT_SHEET_URL FSU ON FSU.URL_ID = LAIT.ID
					WHERE UPPER(LAIT.LANG) = UPPER(@LANG)
					AND UPPER(LAIT.TEXT) = UPPER(@TEXT)
					AND FSU.INSTITUTION_ID = @P_INSTITUTION_ID

				IF @v_lang_item_id IS NULL
					BEGIN
						execute dbo.INSERTA_LANGUAGE_ITEM @FACTSHEET_URL,  @v_lang_item_id OUTPUT
					INSERT INTO EWPCV_INST_FACT_SHEET_URL (URL_ID, INSTITUTION_ID) VALUES (@v_lang_item_id, @P_INSTITUTION_ID)
						SET @v_lang_item_id = NULL
					END

				FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
			END
		CLOSE cur
		DEALLOCATE cur
	END
	
END
;
GO


 /*  
 Actualiza una institucion
 */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_INSTITUTION'
) DROP PROCEDURE dbo.ACTUALIZA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION xml) AS
BEGIN
	DECLARE @v_prim_contact_id varchar(255)
	DECLARE @v_contact_pr_id varchar(255)
	DECLARE @v_cd_id varchar(255)
	DECLARE @v_factsheet_id varchar(255)
	DECLARE @v_institution_id varchar(255)
	DECLARE @v_contact_id varchar(255)
	
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @ABBREVIATION VARCHAR(255)
	DECLARE @LOGO_URL VARCHAR(255)
	DECLARE @INSTITUTION_NAME_LIST xml
	DECLARE @UNIVERSITY_CONTACT_DETAILS xml
	DECLARE @INSTITUTION_URL_LIST xml
	DECLARE @CONTACT_DETAILS_LIST xml
	DECLARE @FACTSHEET_URL_LIST xml
	DECLARE @CONTACT_PERSON xml

	DECLARE @ID varchar(255)

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME_LIST = T.c.query('institution_name'),
		@INSTITUTION_URL_LIST = T.c.query('institution_url_list'),
		@UNIVERSITY_CONTACT_DETAILS = T.c.query('university_contact_details'),
		@FACTSHEET_URL_LIST = T.c.query('factsheet_url_list'),
		@CONTACT_DETAILS_LIST = T.c.query('contact_details_list')
	FROM @P_INSTITUTION.nodes('institution') T(c)

	
	SELECT @v_institution_id = ID, @v_factsheet_id = FACT_SHEET
			FROM dbo.EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID)= UPPER(@P_INSTITUTION_ID)

	UPDATE dbo.EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE ID = @v_institution_id

	--borramos los nombres que tenia y los reemplazamos por los nuevos
    EXECUTE dbo.BORRA_INSTITUTION_NAMES @v_institution_id
    EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_institution_id, @INSTITUTION_NAME_LIST
	
	--borramos los contactos antiguos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) 
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_CONTACT @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

	-- Insertamos la nueva lista de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @CONTACT_DETAILS_LIST.nodes('contact_details_list/contact_person') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_PERSON
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, NULL, @v_contact_id OUTPUT
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
		END
	CLOSE cur
	DEALLOCATE cur
	
	--insertamos el nuevo contacto principal
	EXECUTE dbo.INSERTA_CONTACT @UNIVERSITY_CONTACT_DETAILS, @INSTITUTION_ID, NULL, @v_prim_contact_id OUTPUT
	
    --borramos las urls de factsheet que tenia y las reemplazamos por las nuevas
	EXECUTE dbo.BORRA_INST_FACTSHEET_URL @v_institution_id
	EXECUTE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS @P_INSTITUTION, @v_institution_id
        
    UPDATE dbo.EWPCV_INSTITUTION SET 
        PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id, ABBREVIATION = @ABBREVIATION, LOGO_URL = @LOGO_URL
        WHERE ID = @v_institution_id;
END 
;
GO


/* 
	Actualiza una INSTITUTION del sistema.
    Recibe como parametros: 
    El identificador (ID de interoperabilidad) de la INSTITUTION
	Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_INSTITUTION'
) DROP PROCEDURE dbo.UPDATE_INSTITUTION;
GO 
CREATE PROCEDURE dbo.UPDATE_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	
	DECLARE @v_count integer
	SET @return_value = 0;
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_INSTITUTION @P_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
			IF @return_value = 0 
			BEGIN
			    IF @P_INSTITUTION_ID = @P_INSTITUTION.value('(/institution/institution_id)[1]', 'varchar(255)')
			    BEGIN
                    BEGIN TRY
                        BEGIN TRANSACTION
                        EXECUTE dbo.ACTUALIZA_INSTITUTION @P_INSTITUTION_ID, @P_INSTITUTION

                        INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_INSTITUTION_ID, 6, 1)

                        COMMIT TRANSACTION
                    END TRY

                    BEGIN CATCH
                        THROW
                        SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_INSTITUTION'
                        SET @return_value = -1
                        ROLLBACK TRANSACTION
                    END CATCH
                END
                ELSE
                BEGIN
                    SET @P_ERROR_MESSAGE = 'El identificador de la institución no puede ser modificado'
                    SET @return_value = -1
                END
			END;
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La institución no existe'
		SET @return_value = -1
	END 
END
;
GO

/*
    Borra los nombres de la institution
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INSTITUTION_NAMES'
) DROP PROCEDURE dbo.BORRA_INSTITUTION_NAMES;
GO 
CREATE PROCEDURE dbo.BORRA_INSTITUTION_NAMES (@P_INST_ID_INTEROP varchar(255)) AS
BEGIN
	DECLARE @NAME_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT NAME_ID
			FROM dbo.EWPCV_INSTITUTION_NAME
			WHERE INSTITUTION_ID = @P_INST_ID_INTEROP

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            DELETE FROM dbo.EWPCV_INSTITUTION_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


