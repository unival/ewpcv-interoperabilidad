

/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA OUNIT

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_OUNIT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	
	<xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>
	
	<xs:simpleType name="HTTPS">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https://.+" />
        </xs:restriction>
    </xs:simpleType>
	
	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="ceroOneType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="organization_unit">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="organization_unit_code" type="notEmptyStringType" />
				<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="parent-ounit-id" type="xs:string" minOccurs="0" />
				<xs:element name="is-tree-structure" type="ceroOneType" minOccurs="1" />
				<xs:element name="ounit_contact_details" minOccurs="0" >
				<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="httpListWithOptionalLang"  minOccurs="0" />
				<xs:element name="logo_url" type="HTTPS"  minOccurs="0" />
				
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_OUNIT'
) DROP PROCEDURE dbo.VALIDA_OUNIT;
GO 
CREATE PROCEDURE dbo.VALIDA_OUNIT(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_OUNIT)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO

/*
	Borra una ounit 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_OUNIT'
) DROP PROCEDURE dbo.BORRA_OUNIT;
GO 
CREATE PROCEDURE dbo.BORRA_OUNIT(@P_OUNIT_ID varchar(255)) AS
BEGIN

	DECLARE @CONTACT_ID varchar(255)

	UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE INTERNAL_ID = @P_OUNIT_ID;
	
	EXECUTE dbo.BORRA_OUNIT_NAMES @P_OUNIT_ID;
	
	--Borrado de URLs de factsheet y factsheet
	EXECUTE dbo.BORRA_OUNIT_FACTSHEET_URL @P_OUNIT_ID;
        
	--Borrado de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT 
		WHERE ORGANIZATION_UNIT_ID = @P_OUNIT_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE dbo.BORRA_CONTACT @CONTACT_ID
			FETCH NEXT FROM cur INTO @CONTACT_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
	--Borro la ounit y su relacion con institution
	DELETE FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @P_OUNIT_ID;
	DELETE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE INTERNAL_ID = @P_OUNIT_ID;

END;
GO
	
/* Elimina una OUNIT del sistema.
	Recibe como parametros: 
	El identificador de la OUNIT
	El código schac de la intitución a la que pertenece la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_OUNIT'
) DROP PROCEDURE dbo.DELETE_OUNIT;
GO 
CREATE PROCEDURE dbo.DELETE_OUNIT(@P_OUNIT_ID varchar(255), @P_INSTITUTION_SCHAC varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @v_internal_ounit_id varchar(255);
		
		SELECT @v_internal_ounit_id = INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT ou 
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
		INNER JOIN dbo.EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
		WHERE ou.ID = @P_OUNIT_ID AND ei.INSTITUTION_ID = @P_INSTITUTION_SCHAC;

		SET @return_value = 0
		--validacion existe OUNIT
		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(INTERNAL_ID) = UPPER(@v_internal_ounit_id);
		IF @v_count = 0 
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
			END 
			ELSE 
			BEGIN
				--validacion ounit sin ounits hijas
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(PARENT_OUNIT_ID) = UPPER(@v_internal_ounit_id);
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = 'No se puede borrar una unidad organizativa padre sin antes borrar los hijos'
					END
					
				IF @return_value = 0
					BEGIN
						EXECUTE BORRA_OUNIT @v_internal_ounit_id
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_internal_ounit_id, 7, 2)
					END		
			END
		

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_OUNIT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END 
;
GO
	
	
 /*
	Persiste una ounit en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_COMPLETA'
) DROP PROCEDURE dbo.INSERTA_OUNIT_COMPLETA;
GO    
CREATE PROCEDURE dbo.INSERTA_OUNIT_COMPLETA(@P_OUNIT xml, @P_PRIM_CONT_DETAIL_ID varchar(255), @P_OUNIT_INTERNAL_ID varchar(255), @P_OUNIT_ID varchar(255) OUTPUT) as
BEGIN
	DECLARE @v_inst_id varchar(255)
	DECLARE @v_id_parent varchar(255)
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @ABBREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @INTERNAL_PARENT_OUNIT_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)
	DECLARE @OUNIT_NAME_LIST xml

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer'),
		@OUNIT_NAME_LIST = T.c.query('organization_unit_name')
	FROM @P_OUNIT.nodes('organization_unit') T(c)

	SET @INTERNAL_PARENT_OUNIT_ID = NULL;

	IF @PARENT_OUNIT_ID IS NOT NULL AND @IS_TREE_STRUCTURE = 1
	BEGIN
	    SELECT @INTERNAL_PARENT_OUNIT_ID = OU.INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT OU
	    INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON IOU.ORGANIZATION_UNITS_ID = OU.INTERNAL_ID
	    INNER JOIN dbo.EWPCV_INSTITUTION I ON I.ID = IOU.INSTITUTION_ID
        WHERE OU.ID = @PARENT_OUNIT_ID AND I.INSTITUTION_ID = @INSTITUTION_ID;
	END
	
	SELECT @v_inst_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);

	-- Buscamos le ID del parent
	SELECT @v_id_parent = ou.INTERNAL_ID
	FROM dbo.EWPCV_ORGANIZATION_UNIT ou
	INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
	INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.INSTITUTION_ID = iou.INSTITUTION_ID
	WHERE ou.ID = @PARENT_OUNIT_ID AND ins.INSTITUTION_ID = @INSTITUTION_ID;
	
	SET @P_OUNIT_ID = NEWID();

	INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE, ABBREVIATION, LOGO_URL, PRIMARY_CONTACT_DETAIL_ID, IS_TREE_STRUCTURE, PARENT_OUNIT_ID, INTERNAL_ID)
                                     VALUES (@P_OUNIT_ID, @OUNIT_CODE, @ABBREVIATION, @LOGO_URL, @P_PRIM_CONT_DETAIL_ID, @IS_TREE_STRUCTURE, @v_id_parent, @P_OUNIT_INTERNAL_ID);
	INSERT INTO EWPCV_INST_ORG_UNIT (ORGANIZATION_UNITS_ID, INSTITUTION_ID) VALUES (@P_OUNIT_INTERNAL_ID, @v_inst_id);                                     
		
	EXECUTE dbo.INSERTA_OUNIT_NAMES @P_OUNIT_INTERNAL_ID, @OUNIT_NAME_LIST;
END;
GO	
	
/* 
	Valida los datos introducidos en el xml
	La institucion indicada debe exisitir
	El ounit code no se puede repetir en la misma institucion
	No se pueden mezclar estructuras de representacion de unidades organizativas, o todas van sueltas o todas van en estructura de arbol
	La estructura de representacion de unidades organizativas solo debe tener un root si se representa como arbol
	El ounit code indicado como padre debe existir asociado a la institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_OUNIT'
) DROP PROCEDURE dbo.VALIDA_DATOS_OUNIT;
GO    
CREATE PROCEDURE dbo.VALIDA_DATOS_OUNIT(@P_OUNIT xml, @P_OUNIT_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	SET @return_value = 0;
	DECLARE @v_existing_ounit_code varchar(255) = 'v_existing_ounit_code'
	DECLARE @v_existing_parent varchar(255)
	DECLARE @v_count integer
	DECLARE @v_existing_tree_str integer
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)

	IF @P_ERROR_MESSAGE IS NULL 
		SET @P_ERROR_MESSAGE = '';
	
	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer')
	FROM @P_OUNIT.nodes('organization_unit') T(c)
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
	IF @v_count = 0
	BEGIN
		SET @return_value = -1;
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
			No existe ninguna institución con el código SCHAC ' + @INSTITUTION_ID;
	END
	ELSE
	BEGIN
		--Si es actualizacion no se puede setear padre a si mismo
		IF @P_OUNIT_ID IS NOT NULL AND @IS_TREE_STRUCTURE = 1  AND @PARENT_OUNIT_ID = @P_OUNIT_ID
		BEGIN
			SET @return_value = -1;
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
					No se puede establecer como padre de una unidad organizativa a si misma';
		END 
		ELSE 
		BEGIN
			--	si es actualizacion	si era root no se puede setear padre
			IF @P_OUNIT_ID IS NOT NULL
			BEGIN
				SELECT @v_existing_ounit_code = ORGANIZATION_UNIT_CODE, @v_existing_parent = PARENT_OUNIT_ID FROM EWPCV_ORGANIZATION_UNIT ou
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
				INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
				WHERE ou.INTERNAL_ID = @P_OUNIT_ID;
			END
		
			IF @P_OUNIT_ID IS NOT NULL AND @v_existing_parent IS NULL AND @PARENT_OUNIT_ID IS NOT NULL AND @IS_TREE_STRUCTURE = 1
			BEGIN 
				SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Esta actualizando una unidad organizativa declarada como root asignandole una unidad organizativa padre';
			END 
			ELSE IF @P_OUNIT_ID IS NOT NULL AND @v_existing_parent IS NOT NULL AND @PARENT_OUNIT_ID IS NULL AND @IS_TREE_STRUCTURE = 1
			BEGIN
				SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Esta tratando de convertir en root una unidad organizativa que no lo era, actualice el root en su lugar';
			END 
			ELSE
			BEGIN
				SELECT @v_count = COUNT(1)
				FROM dbo.EWPCV_ORGANIZATION_UNIT ou
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
				INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
				WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
				AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@OUNIT_CODE);
				
				--	si es actualizacion el ounit code que viene no este duplicado solo si no es el que ya tenia
				IF @v_count > 0 AND UPPER(@v_existing_ounit_code) != UPPER(@OUNIT_CODE)
				BEGIN 
					SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Ya existe una unidad organizativa con el código ' + @OUNIT_CODE + ' para la institución ' + @INSTITUTION_ID;
				END
				ELSE
				BEGIN
					SELECT @v_count = COUNT(DISTINCT(ou.IS_TREE_STRUCTURE))
					FROM dbo.EWPCV_ORGANIZATION_UNIT ou
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
					INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
					WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID);

					IF @v_count > 1
					BEGIN 
						SET @return_value = -1;
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
							Existen unidades organizativas con estructura de árbol y sin estructura de árbol para la institucion indicada, la BBDD necesita revisión.';
					END
					ELSE
					BEGIN
						SELECT @v_existing_tree_str = ou.IS_TREE_STRUCTURE
							FROM dbo.EWPCV_ORGANIZATION_UNIT ou
							INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
							INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
							WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
						IF @v_existing_tree_str != @IS_TREE_STRUCTURE
						BEGIN 
							SET @return_value = -1;
							SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
								Ya hay unidades organizativas introducidas para esta universidad con diferente estructura de representacion a la indicada: ' + @IS_TREE_STRUCTURE;
						END
						ELSE
						BEGIN
							IF @IS_TREE_STRUCTURE = 0 AND @PARENT_OUNIT_ID IS NOT NULL
							BEGIN
								SET @return_value = -1;
								SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
									La estructura de representacion de la unida organizativa no está como formato árbol. El atributo PARENT_OUNIT_ID debe ser nulo.';
							END
							ELSE IF @IS_TREE_STRUCTURE = 1 AND @PARENT_OUNIT_ID IS NULL
							BEGIN
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
								INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.INTERNAL_ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND ou.PARENT_OUNIT_ID IS NULL;
								--En caso de actualizacion no se valida este paso porque esta validado antes
								IF @v_count > 0 AND @P_OUNIT_ID IS NULL
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										Ya existe una unidad organizativa informada como "root"';
								END   
							END
							ELSE IF @IS_TREE_STRUCTURE = 1 AND @PARENT_OUNIT_ID IS NOT NULL
							BEGIN
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
								INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.INTERNAL_ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND ou.PARENT_OUNIT_ID IS NULL;
								IF @v_count = 0 
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										No existe ninguna unidad organizativa informada como root, inserte primero el "root".';
								END 
							
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT iou
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT ou ON iou.ORGANIZATION_UNITS_ID = ou.INTERNAL_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) 
								AND UPPER(iou.ORGANIZATION_UNITS_ID) = (SELECT INTERNAL_ID FROM EWPCV_ORGANIZATION_UNIT WHERE ID = @PARENT_OUNIT_ID);
								IF @v_count = 0 
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										La unidad organizativa informada como padre no existe.';
								END 
							END
						END
					END
				END
			END
		END
	END
END;
GO

	
/* Inserta una OUNIT en el sistema
	Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
	Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_OUNIT'
) DROP PROCEDURE dbo.INSERT_OUNIT;
GO    
CREATE PROCEDURE dbo.INSERT_OUNIT(@P_OUNIT xml, @P_OUNIT_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	EXECUTE dbo.VALIDA_OUNIT @P_OUNIT, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		
		DECLARE @v_prim_contact_id varchar(255)
		DECLARE @v_count integer
		DECLARE @v_contact_id varchar(255)
		DECLARE @v_factsheet_id varchar(255)
		DECLARE @INSTITUTION_ID varchar(255)
		DECLARE @OUNIT_CODE varchar(255)
		DECLARE @CONTACT_PERSON xml
		DECLARE @OUNIT_CONTACT_DETAILS xml

		SELECT 
			@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
			@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'), 
			@OUNIT_CONTACT_DETAILS = T.c.query('ounit_contact_details')
		FROM @P_OUNIT.nodes('organization_unit') T(c)
		
		
		EXECUTE dbo.VALIDA_DATOS_OUNIT @P_OUNIT, null, @P_ERROR_MESSAGE OUTPUT, @return_value OUTPUT
		
		IF @return_value = 0 
		BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @v_internal_id varchar(255)
			SET @v_internal_id = NEWID();
			
			-- Insertamos la OUNIT en OUNIT
			EXECUTE dbo.INSERTA_OUNIT_COMPLETA @P_OUNIT, null, @v_internal_id, @P_OUNIT_ID OUTPUT;
			
			-- Insertamos el contacto principal
			EXECUTE dbo.INSERTA_CONTACT @OUNIT_CONTACT_DETAILS, @INSTITUTION_ID, @OUNIT_CODE, @v_prim_contact_id OUTPUT;

			-- Insertamos la lista de contactos
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_OUNIT.nodes('organization_unit/contact_details_list/contact_person') T(c);
			OPEN cur;
			FETCH NEXT FROM cur INTO @CONTACT_PERSON;
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, @OUNIT_CODE, @v_contact_id OUTPUT;
					FETCH NEXT FROM cur INTO @CONTACT_PERSON;
				END
			CLOSE cur;
			DEALLOCATE cur;

			-- Insertamos la lista de fact sheet urls
			EXECUTE dbo.INSERTA_OUNIT_FACTSHEET_URLS @v_internal_id, @P_OUNIT ;
			
			-- Actualizamos la OUNIT en OUNIT
			UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id WHERE INTERNAL_ID = @v_internal_id ;

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_internal_id, 7, 0)

			COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_INSTITUTION'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH

	END
END
;
GO

/*
	Inserta las urls de factsheet de la ounit y devuelve el identificador del factsheet generado o existente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_FACTSHEET_URLS'
) DROP PROCEDURE dbo.INSERTA_OUNIT_FACTSHEET_URLS;
GO 
CREATE PROCEDURE dbo.INSERTA_OUNIT_FACTSHEET_URLS(@P_OUNIT_ID varchar(255), @P_OUNIT xml) as
BEGIN
	DECLARE @v_lang_item_id VARCHAR(255)
	DECLARE @v_institution_id VARCHAR(255)
	DECLARE @v_ounit_code VARCHAR(255)
	DECLARE @LANG VARCHAR(255)
	DECLARE @TEXT VARCHAR(255)
	DECLARE @FACTSHEET_URL xml

	SELECT 	@v_institution_id = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@v_ounit_code = T.c.value('(organization_unit_code)[1]', 'varchar(255)') 
	FROM @P_OUNIT.nodes('organization_unit') T(c);
	
	SELECT  @TEXT = T.c.value('(.)[1]', 'varchar(255)') FROM @P_OUNIT.nodes('organization_unit/factsheet_url_list/text') T(c)
	IF @TEXT IS NOT NULL
	BEGIN

		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.value('(@lang)[1]', 'varchar(255)'),
			T.c.value('(.)[1]', 'varchar(255)'),
			T.c.query('.')
		FROM @P_OUNIT.nodes('organization_unit/factsheet_url_list/text') T(c)
		
		OPEN cur
		FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_lang_item_id = NULL

				SELECT @v_lang_item_id = ID
					FROM dbo.EWPCV_LANGUAGE_ITEM LAIT 
					INNER JOIN dbo.EWPCV_OUNIT_FACT_SHEET_URL FSU ON FSU.URL_ID = LAIT.ID
					WHERE UPPER(LAIT.LANG) = UPPER(@LANG)
					AND UPPER(LAIT.TEXT) = UPPER(@TEXT)
					AND FSU.OUNIT_ID = @P_OUNIT_ID

				IF @v_lang_item_id IS NULL
					BEGIN
						execute dbo.INSERTA_LANGUAGE_ITEM @FACTSHEET_URL,  @v_lang_item_id OUTPUT
					INSERT INTO EWPCV_OUNIT_FACT_SHEET_URL (URL_ID, OUNIT_ID) VALUES (@v_lang_item_id, @P_OUNIT_ID)
						SET @v_lang_item_id = NULL
					END

				FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
			END
		CLOSE cur
		DEALLOCATE cur
	END
	
END
;
GO


 /*  
 Actualiza una ounit
 */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_OUNIT'
) DROP PROCEDURE dbo.ACTUALIZA_OUNIT;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_OUNIT(@P_OUNIT_ID varchar(255), @P_OUNIT xml) AS
BEGIN

	DECLARE @v_contact_id varchar(255)
	DECLARE @v_prim_contact_id varchar(255)
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @ABBREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @PARENT_OUNIT_INTERNAL_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)
	DECLARE @OUNIT_NAME_LIST xml
	DECLARE @OUNIT_CONTACT_DETAILS xml
	DECLARE @CONTACT_DETAILS_LIST xml
	DECLARE @CONTACT_PERSON xml


	DECLARE @ID varchar(255)
	
	
	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer'),
		@OUNIT_NAME_LIST = T.c.query('organization_unit_name'),
		@OUNIT_CONTACT_DETAILS = T.c.query('ounit_contact_details'),
		@CONTACT_DETAILS_LIST = T.c.query('contact_details_list')
	FROM @P_OUNIT.nodes('organization_unit') T(c)

	UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE INTERNAL_ID = @P_OUNIT_ID

	--borramos los nombres que tenia y los reemplazamos por los nuevos
    EXECUTE dbo.BORRA_OUNIT_NAMES @P_OUNIT_ID
    EXECUTE dbo.INSERTA_OUNIT_NAMES @P_OUNIT_ID, @OUNIT_NAME_LIST
	
	--borramos los contactos antiguos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT
		WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(@P_OUNIT_ID) 
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_CONTACT @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

	-- Insertamos la nueva lista de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @CONTACT_DETAILS_LIST.nodes('contact_details_list/contact_person') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_PERSON
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, @OUNIT_CODE, @v_contact_id OUTPUT
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
		END
	CLOSE cur
	DEALLOCATE cur
	
	--insertamos el nuevo contacto principal
	EXECUTE dbo.INSERTA_CONTACT @OUNIT_CONTACT_DETAILS, @INSTITUTION_ID, @OUNIT_CODE, @v_prim_contact_id OUTPUT
	
    --borramos las urls de factsheet que tenia y las reemplazamos por las nuevas
	EXECUTE dbo.BORRA_OUNIT_FACTSHEET_URL @P_OUNIT_ID
	--se borra el registro si es un dummy
	EXECUTE dbo.INSERTA_OUNIT_FACTSHEET_URLS @P_OUNIT_ID, @P_OUNIT

	SELECT @PARENT_OUNIT_INTERNAL_ID = INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = @PARENT_OUNIT_ID;
        
	UPDATE EWPCV_ORGANIZATION_UNIT SET 
	    PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id, ABBREVIATION = @ABBREVIATION, LOGO_URL = @LOGO_URL, ORGANIZATION_UNIT_CODE = @OUNIT_CODE, PARENT_OUNIT_ID = @PARENT_OUNIT_INTERNAL_ID
		WHERE INTERNAL_ID = @P_OUNIT_ID;
END 
;
GO

/* 
	Actualiza una OUNIT del sistema.
    Recibe como parametros: 
    El identificador de la Unidad organizativa
	Admite un xml de tipo Ounit con toda la informacion actualizada de la ounit, sus contactos y sus factsheet
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_OUNIT'
) DROP PROCEDURE dbo.UPDATE_OUNIT;
GO 
CREATE PROCEDURE dbo.UPDATE_OUNIT(@P_OUNIT_ID varchar(255), @P_OUNIT xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	
	DECLARE @v_count integer
	SET @return_value = 0;
	DECLARE @v_internal_ounit_id varchar(255)
		
	SELECT @v_internal_ounit_id = INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT ou 
	INNER JOIN dbo.EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
	INNER JOIN dbo.EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
	WHERE ou.ID = @P_OUNIT_ID AND ei.INSTITUTION_ID = (	SELECT T.c.value('(institution_id)[1]', 'varchar(255)')	FROM @P_OUNIT.nodes('organization_unit') T(c));
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(INTERNAL_ID) = UPPER(@v_internal_ounit_id)
	IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_OUNIT @P_OUNIT, @P_ERROR_MESSAGE output, @return_value output
			IF @return_value = 0 
			BEGIN
				
				EXECUTE dbo.VALIDA_DATOS_OUNIT @P_OUNIT, @v_internal_ounit_id, @P_ERROR_MESSAGE output, @return_value output
				IF @return_value = 0 
				BEGIN
					BEGIN TRY
						BEGIN TRANSACTION
						EXECUTE dbo.ACTUALIZA_OUNIT @v_internal_ounit_id, @P_OUNIT
						INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @v_internal_ounit_id, 7, 1)
						COMMIT TRANSACTION
					END TRY

					BEGIN CATCH
						THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_OUNIT'
						SET @return_value = -1
						ROLLBACK TRANSACTION
					END CATCH
				END 
			END;
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO

/*
    Borra los nombres de la OUNIT
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_OUNIT_NAMES'
) DROP PROCEDURE dbo.BORRA_OUNIT_NAMES;
GO 
CREATE PROCEDURE dbo.BORRA_OUNIT_NAMES (@P_OUNIT_ID varchar(255)) AS
BEGIN
	DECLARE @NAME_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT NAME_ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME
			WHERE ORGANIZATION_UNIT_ID = @P_OUNIT_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            DELETE FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
    Actualiza el campo IS_EXPOSED de forma recursiva
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OCULTAR_O_MOSTRAR_OUNIT'
) DROP PROCEDURE dbo.OCULTAR_O_MOSTRAR_OUNIT;
GO 
CREATE PROCEDURE dbo.OCULTAR_O_MOSTRAR_OUNIT (@P_OUNIT_ID varchar(255), @P_IS_EXPOSED integer) AS
BEGIN
	DECLARE @ID varchar(255)

	UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET IS_EXPOSED = @P_IS_EXPOSED WHERE INTERNAL_ID = @P_OUNIT_ID;

	INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, ELEMENT_TYPE, ACTION) VALUES (NEWID(), @P_OUNIT_ID, 7, 1)
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT
			WHERE PARENT_OUNIT_ID = @P_OUNIT_ID;
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE OCULTAR_O_MOSTRAR_OUNIT @ID, @P_IS_EXPOSED;
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO  

/* Oculta una ounit de manera que esta deja de estar expuesta. 
	En el caso de estar en estructura de árbol y ser una OUNIT padre, se ocultarán también todas las hijas.
	Recibe como parametros: 
	El identificador de la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OCULTAR_OUNIT'
) DROP PROCEDURE dbo.OCULTAR_OUNIT;
GO 
CREATE PROCEDURE dbo.OCULTAR_OUNIT (@P_OUNIT_ID varchar(255), @P_INSTITUTION_SCHAC varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	SET @return_value = 0;
	DECLARE @v_count integer
	DECLARE @v_internal_ounit_id varchar(255)
	
	SELECT @v_internal_ounit_id = INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT ou 
	INNER JOIN dbo.EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
	INNER JOIN dbo.EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
	WHERE ou.ID = @P_OUNIT_ID AND ei.INSTITUTION_ID = @P_INSTITUTION_SCHAC;
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(INTERNAL_ID) = UPPER(@v_internal_ounit_id)
	IF @v_count > 0 
		BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			EXECUTE OCULTAR_O_MOSTRAR_OUNIT @v_internal_ounit_id, 0;
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.OCULTAR_OUNIT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH			
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO  
 
 
 /* Oculta una ounit de manera que esta deja de estar expuesta. 
	En el caso de estar en estructura de árbol y ser una OUNIT padre, se ocultarán también todas las hijas.
	Recibe como parametros: 
	El identificador de la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'EXPONER_OUNIT'
) DROP PROCEDURE dbo.EXPONER_OUNIT;
GO 
CREATE PROCEDURE dbo.EXPONER_OUNIT (@P_OUNIT_ID varchar(255), @P_INSTITUTION_SCHAC varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	SET @return_value = 0;
	DECLARE @v_count integer
	DECLARE @v_internal_ounit_id varchar(255)
	
	SELECT @v_internal_ounit_id = INTERNAL_ID FROM dbo.EWPCV_ORGANIZATION_UNIT ou 
	INNER JOIN dbo.EWPCV_INST_ORG_UNIT inst ON ou.INTERNAL_ID = inst.ORGANIZATION_UNITS_ID 
	INNER JOIN dbo.EWPCV_INSTITUTION ei ON ei.ID = inst.INSTITUTION_ID 
	WHERE ou.ID = @P_OUNIT_ID AND ei.INSTITUTION_ID = @P_INSTITUTION_SCHAC;
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(INTERNAL_ID) = UPPER(@v_internal_ounit_id)
	IF @v_count > 0 
		BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			EXECUTE OCULTAR_O_MOSTRAR_OUNIT @v_internal_ounit_id, 1;
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.EXPONER_OUNIT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH			
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO  
