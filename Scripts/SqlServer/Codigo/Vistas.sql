
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIAS_PDF_VIEW'
) DROP VIEW dbo.IIAS_PDF_VIEW;
GO 
CREATE VIEW dbo.IIAS_PDF_VIEW AS SELECT
    I.IIA_ID                 AS INTERNAL_IIA_ID,
    I.ID                     AS IIA_ID,
    I.IIA_CODE               AS IIA_CODE,
    I.REMOTE_IIA_ID          AS REMOTE_IIA_ID,
    I.REMOTE_IIA_CODE        AS REMOTE_IIA_CODE,
    I.IS_REMOTE              AS IS_REMOTE,
    F.FILE_CONTENT           AS PDF,
    F.MIME_TYPE              AS MIME_TYPE
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_IIA_PARTNER FP
    ON I.FIRST_PARTNER_ID = FP.ID
INNER JOIN dbo.EWPCV_IIA_PARTNER SP
    ON I.SECOND_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_FILES F
    ON F.FILE_ID = I.PDF_ID AND CASE I.IS_REMOTE WHEN 0 THEN FP.INSTITUTION_ID ELSE SP.INSTITUTION_ID END = F.SCHAC
WHERE PDF_ID IS NOT NULL;
GO


IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_VIEW'
) DROP VIEW dbo.FACTSHEET_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_VIEW AS 
  SELECT 
	I.INSTITUTION_ID    				  	 	 	AS INSTITUTION_ID,
	I.ABBREVIATION	 					  	 	 	AS ABREVIATION,
	MAX(F.DECISION_WEEKS_LIMIT)			  	 	 	AS DECISION_WEEK_LIMIT,
	MAX(F.TOR_WEEKS_LIMIT) 					  	 	AS TOR_WEEK_LIMIT,
	FORMAT(MAX(F.NOMINATIONS_AUTUM_TERM), '--MM-dd' )  	AS NOMINATIONS_AUTUM_TERM,
	FORMAT(MAX(F.NOMINATIONS_SPRING_TERM), '--MM-dd' ) 	AS NOMINATIONS_SPRING_TERM,
	FORMAT(MAX(F.APPLICATION_AUTUM_TERM), '--MM-dd' ) 	AS APPLICATION_AUTUM_TERM,
	FORMAT(MAX(F.APPLICATION_SPRING_TERM), '--MM-dd' ) 	AS APPLICATION_SPRING_TERM,
	dbo.EXTRAE_EMAILS(MAX(F.CONTACT_DETAILS_ID))	  	AS APPLICATION_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(F.CONTACT_DETAILS_ID)) 	 	AS APPLICATION_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(F.CONTACT_DETAILS_ID)) AS APPLICATION_URLS,
	dbo.EXTRAE_EMAILS(MAX(HI.CONTACT_DETAIL_ID))	  	AS HOUSING_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(HI.CONTACT_DETAIL_ID)) 	 	AS HOUSING_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(HI.CONTACT_DETAIL_ID))	AS HOUSING_URLS,
	dbo.EXTRAE_EMAILS(MAX(VI.CONTACT_DETAIL_ID))	  	AS VISA_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(VI.CONTACT_DETAIL_ID)) 	  	AS VISA_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(VI.CONTACT_DETAIL_ID)) AS VISA_URLS,
	dbo.EXTRAE_EMAILS(MAX(II.CONTACT_DETAIL_ID))	  	AS INSURANCE_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(II.CONTACT_DETAIL_ID)) 	 	AS INSURANCE_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID))	AS INSURANCE_URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_FACT_SHEET F 
    ON I.FACT_SHEET = F.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS C 
    ON C.ID = F.CONTACT_DETAILS_ID
LEFT JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM HI
	ON IIT.INFORMATION_ID = HI.ID AND HI."TYPE" = 'HOUSING'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM VI
	ON IIT.INFORMATION_ID = VI.ID AND VI."TYPE" = 'VISA'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM II
	ON IIT.INFORMATION_ID = II.ID AND II."TYPE" = 'INSURANCE'
GROUP BY I.INSTITUTION_ID,I.ABBREVIATION
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_ADD_INFO_VIEW'
) DROP VIEW dbo.FACTSHEET_ADD_INFO_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_ADD_INFO_VIEW AS 
  SELECT 
	AI.ID										AS ADD_INFO_ID,
	I.INSTITUTION_ID 		 			        AS INSTITUTION_ID,
	AI."TYPE"		 					        AS INFO_TYPE,
	dbo.EXTRAE_EMAILS(AI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(AI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(AI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_INFORMATION_ITEM AI
	ON IIT.INFORMATION_ID = AI.ID
    AND AI."TYPE" NOT IN ('HOUSING','VISA','INSURANCE')
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_REQ_INFO_VIEW'
) DROP VIEW dbo.FACTSHEET_REQ_INFO_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_REQ_INFO_VIEW AS 
 SELECT 
	REI.ID										AS REQ_INFO_ID,
	I.INSTITUTION_ID 							 AS INSTITUTION_ID,
	REI."TYPE" 								     AS REQ_TYPE,
	REI.NAME 								    AS NAME,
	REI.DESCRIPTION 						     AS DESCRIPTION,
    dbo.EXTRAE_EMAILS(REI.CONTACT_DETAIL_ID)	    AS EMAIL,
	dbo.EXTRAE_TELEFONO(REI.CONTACT_DETAIL_ID)      AS PHONE,
	dbo.EXTRAE_URLS_CONTACTO(REI.CONTACT_DETAIL_ID) AS URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_INS_REQUIREMENTS IR
	ON IR.INSTITUTION_ID = I.ID 
INNER JOIN dbo.EWPCV_REQUIREMENTS_INFO REI
	ON IR.REQUIREMENT_ID = REI.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIAS_VIEW'
) DROP VIEW dbo.IIAS_VIEW;
GO 
CREATE VIEW dbo.IIAS_VIEW AS 
  SELECT
    I.IIA_ID                 AS PK_IIA_ID,
	I.ID                     AS IIA_ID,
	I.IIA_CODE               AS IIA_CODE,
	I.REMOTE_IIA_ID          AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE        AS REMOTE_IIA_CODE,
	I.START_DATE             AS IIA_START_DATE,
	I.END_DATE               AS IIA_END_DATE,
	I.MODIFY_DATE            AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE          AS IIA_APPROVAL_DATE,
	I.REMOTE_COP_COND_HASH   AS REMOTE_HASH,
	I.APPROVAL_COP_COND_HASH AS APPROVAL_HASH,
	I.IS_REMOTE			   AS IS_REMOTE,
	I.IS_AUTOGENERATED	   AS IS_AUTOGENERATED,
	dbo.EXTRAE_IS_TERMINATED(I.IIA_ID, I.REMOTE_IIA_ID) AS IS_TERMINATED,
	FP.INSTITUTION_ID                                                                                      AS FIRST_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(FP.INSTITUTION_ID)                                                      AS FIRST_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(FP.ORGANIZATION_UNIT_ID, FP.INSTITUTION_ID)                                    AS FIRST_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(FP.ORGANIZATION_UNIT_ID, FP.INSTITUTION_ID)                                   AS FIRST_OUNIT_NAMES,
	FP.SIGNING_DATE                                                                                        AS FIRST_SIGNING_DATE,
	FPSCP.FIRST_NAMES                                                                                      AS FIRST_SIGNER_NAME,
	FPSCP.LAST_NAME                                                                                        AS FIRST_SIGNER_LAST_NAME,
	FPSCP.BIRTH_DATE                                                                                       AS FIRST_SIGNER_BIRTH_DATE,
	FPSCP.GENDER                                                                                           AS FIRST_SIGNER_GENDER,
	FPSCP.COUNTRY_CODE                                                                                     AS FIRST_SIGNER_CITIZENSHIP,
	FPSC.CONTACT_ROLE                                                                       			   AS FIRST_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(FPSC.ID)                                                                   AS FIRST_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(FPSC.ID)	                                                           AS FIRST_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(FPSCD.ID)                                                                     AS FIRST_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(FPSCD.ID)                                                                            AS FIRST_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(FPSC.CONTACT_DETAILS_ID)                                                           AS FIRST_SIGNER_PHONES,
	dbo.EXTRAE_FAX(FPSC.CONTACT_DETAILS_ID)                                                           	   AS FIRST_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(FPSCD.STREET_ADDRESS)                                                         AS FIRST_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(FPSCD.STREET_ADDRESS)                                                               AS FIRST_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(FPSCD.MAILING_ADDRESS)                                                        AS FIRST_SIGNER_M_ADD_LINES,
	dbo.EXTRAE_ADDRESS(FPSCD.MAILING_ADDRESS)                                                              AS FIRST_SIGNER_MAILING_ADDR,
	FPSCFA.POSTAL_CODE                                                                                     AS FIRST_SIGNER_POSTAL_CODE,
	FPSCFA.LOCALITY                                                                                        AS FIRST_SIGNER_LOCALITY,
	FPSCFA.REGION                                                                                          AS FIRST_SIGNER_REGION,	
	FPSCFA.COUNTRY                                                                                         AS FIRST_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SECOND_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                      AS SECOND_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(SP.ORGANIZATION_UNIT_ID, SP.INSTITUTION_ID)                                    AS SECOND_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID, SP.INSTITUTION_ID)                                   AS SECOND_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SECOND_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SECOND_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SECOND_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SECOND_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SECOND_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SECOND_SIGNER_CITIZENSHIP,
	SPSC.CONTACT_ROLE                                                                       			   AS SECOND_SIGNER_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                   AS SECOND_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                           AS SECOND_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                     AS SECOND_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSCD.ID)                                                                            AS SECOND_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                           AS SECOND_SIGNER_PHONES,
	dbo.EXTRAE_FAX(SPSC.CONTACT_DETAILS_ID)                                                          	   AS SECOND_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                         AS SECOND_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                               AS SECOND_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.MAILING_ADDRESS)                                                        AS SECOND_SIGNER_M_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.MAILING_ADDRESS)                                                              AS SECOND_SIGNER_MAILING_ADDR,
	SPSCFA.POSTAL_CODE                                                                                     AS SECOND_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SECOND_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SECOND_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SECOND_SIGNER_COUNTRY,
	I.PDF_ID                                                                                               AS PDF_ID
FROM dbo.EWPCV_IIA I
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
	ON I.SECOND_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER FP
	ON I.FIRST_PARTNER_ID = FP.ID
LEFT JOIN dbo.EWPCV_CONTACT FPSC
    ON FP.SIGNER_PERSON_CONTACT_ID = FPSC.ID
LEFT JOIN dbo.EWPCV_PERSON FPSCP 
    ON FPSC.PERSON_ID = FPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS FPSCD 
    ON FPSC.CONTACT_DETAILS_ID = FPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS FPSCFA 
    ON FPSCD.STREET_ADDRESS = FPSCFA.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_CONTACTS_VIEW'
) DROP VIEW dbo.IIA_CONTACTS_VIEW;
GO 
CREATE VIEW IIA_CONTACTS_VIEW AS SELECT
	C.ID                                                                                                   AS CONTACT_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
    P.INSTITUTION_ID                                                                                       AS INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(P.INSTITUTION_ID)                                                       AS INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(P.ORGANIZATION_UNIT_ID, P.INSTITUTION_ID)                                      AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(P.ORGANIZATION_UNIT_ID, P.INSTITUTION_ID)                                     AS OUNIT_NAMES,
	PE.FIRST_NAMES                                                                                         AS CONTACT_NAME,
	PE.LAST_NAME                                                                                           AS LAST_NAME,
	PE.BIRTH_DATE                                                                                          AS BIRTH_DATE,
	PE.GENDER                                                                                              AS GENDER,
	PE.COUNTRY_CODE                                                                                        AS CITIZENSHIP,
	C.CONTACT_ROLE                                                                       			       AS CONTACT_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(C.ID)                                                                      AS CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(C.ID)	                                                               AS CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(CD.ID)                                                                        AS CONTACT_URLS,
	dbo.EXTRAE_EMAILS(CD.ID)                                                                               AS EMAILS,
	dbo.EXTRAE_TELEFONO(C.CONTACT_DETAILS_ID)                                                              AS PHONES,
	dbo.EXTRAE_FAX(C.CONTACT_DETAILS_ID)                                                                   AS FAXES,
	dbo.EXTRAE_ADDRESS_LINES(CD.STREET_ADDRESS)                                                            AS ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(CD.STREET_ADDRESS)                                                                  AS ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(CD.MAILING_ADDRESS)                                                           AS M_ADD_LINES,
	dbo.EXTRAE_ADDRESS(CD.MAILING_ADDRESS)                                                                 AS MAILING_ADDR,
	FA.POSTAL_CODE                                                                                         AS POSTAL_CODE,
	FA.LOCALITY                                                                                            AS LOCALITY,
	FA.REGION                                                                                              AS REGION,	
	FA.COUNTRY                                                                                             AS COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_IIA_PARTNER P
    ON (I.FIRST_PARTNER_ID = P.ID OR I.SECOND_PARTNER_ID = P.ID)
INNER JOIN EWPCV_IIA_PARTNER_CONTACTS PC
    ON P.ID = PC.IIA_PARTNER_ID
INNER JOIN EWPCV_CONTACT C
    ON PC.CONTACTS_ID = C.ID
LEFT JOIN EWPCV_PERSON PE 
    ON C.PERSON_ID = PE.ID
LEFT JOIN EWPCV_CONTACT_DETAILS CD 
    ON C.CONTACT_DETAILS_ID = CD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS FA 
    ON CD.STREET_ADDRESS = FA.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_CONDITIONS_VIEW'
) DROP VIEW dbo.IIA_COOP_CONDITIONS_VIEW;
GO 
CREATE VIEW dbo.IIA_COOP_CONDITIONS_VIEW AS 
  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	CC.ORDER_INDEX                                                                                         AS COOP_COND_ORDER_INDEX,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                            AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                       AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                       AS SUBJECT_AREAS,
	dbo.EXTRAE_S_AREA(CC.ID) 																			   AS SUBJECT_AREAS_LIST,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                      AS RECEIVING_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(RP.ORGANIZATION_UNIT_ID, RP.INSTITUTION_ID)                                    AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID, RP.INSTITUTION_ID)                                    AS RECEIVING_OUNIT_NAMES,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                      AS SENDING_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(SP.ORGANIZATION_UNIT_ID, SP.INSTITUTION_ID)                                    AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID, SP.INSTITUTION_ID)                                   AS SENDING_OUNIT_NAMES,
	CC.IIA_APPROVAL_ID                                                                                     AS APPROVAL_ID,
	CC.TERMINATED                                                                                          AS TERMINATED
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.IIA_ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_COND_CONTACTS_VIEW'
) DROP VIEW dbo.IIA_COOP_COND_CONTACTS_VIEW;
GO 
CREATE VIEW IIA_COOP_COND_CONTACTS_VIEW AS SELECT
	C.ID                                                                                                   AS CONTACT_ID,
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
    P.INSTITUTION_ID                                                                                       AS INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(P.INSTITUTION_ID)                                                           AS INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(P.ORGANIZATION_UNIT_ID, P.INSTITUTION_ID)                                          AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(P.ORGANIZATION_UNIT_ID, P.INSTITUTION_ID)                                         AS OUNIT_NAMES,
	PE.FIRST_NAMES                                                                                      AS CONTACT_NAME,
	PE.LAST_NAME                                                                                        AS LAST_NAME,
	PE.BIRTH_DATE                                                                                       AS BIRTH_DATE,
	PE.GENDER                                                                                           AS GENDER,
	PE.COUNTRY_CODE                                                                                     AS CITIZENSHIP,
	C.CONTACT_ROLE                                                                       			   AS CONTACT_ROLE,
	dbo.EXTRAE_NOMBRES_CONTACTO(C.ID)                                                                       AS CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(C.ID)	                                                               AS CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(CD.ID)                                                                         AS CONTACT_URLS,
	dbo.EXTRAE_EMAILS(CD.ID)                                                                                AS EMAILS,
	dbo.EXTRAE_TELEFONO(C.CONTACT_DETAILS_ID)                                                               AS PHONES,
	dbo.EXTRAE_FAX(C.CONTACT_DETAILS_ID)                                                               	   AS FAXES,
	dbo.EXTRAE_ADDRESS_LINES(CD.STREET_ADDRESS)                                                             AS ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(CD.STREET_ADDRESS)                                                                   AS ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(CD.MAILING_ADDRESS)                                                            AS M_ADD_LINES,
	dbo.EXTRAE_ADDRESS(CD.MAILING_ADDRESS)                                                                  AS MAILING_ADDR,
	FA.POSTAL_CODE                                                                                     AS POSTAL_CODE,
	FA.LOCALITY                                                                                        AS LOCALITY,
	FA.REGION                                                                                          AS REGION,	
	FA.COUNTRY                                                                                         AS COUNTRY
FROM EWPCV_IIA I
INNER JOIN EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.IIA_ID
INNER JOIN EWPCV_IIA_PARTNER P
    ON (CC.SENDING_PARTNER_ID = P.ID OR CC.RECEIVING_PARTNER_ID = P.ID)
INNER JOIN EWPCV_IIA_PARTNER_CONTACTS PC
    ON P.ID = PC.IIA_PARTNER_ID
INNER JOIN EWPCV_CONTACT C
    ON PC.CONTACTS_ID = C.ID
LEFT JOIN EWPCV_PERSON PE 
    ON C.PERSON_ID = PE.ID
LEFT JOIN EWPCV_CONTACT_DETAILS CD 
    ON C.CONTACT_DETAILS_ID = CD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS FA 
    ON CD.STREET_ADDRESS = FA.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'MOBILITY_VIEW'
) DROP VIEW dbo.MOBILITY_VIEW;
GO 
CREATE VIEW dbo.MOBILITY_VIEW AS 
 SELECT
    M.MOBILITY_ID                                                                                              AS INTERNAL_MOBILITY_ID,
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL_DEPARTURE                                                                                      AS EQF_LEVEL_DEPARTURE,
	M.EQF_LEVEL_NOMINATION                                                                                     AS EQF_LEVEL_NOMINATION,
	M.MODIFY_DATE																							   AS MODIFY_DATE,
	M.IIA_ID                                                                                                   AS IIA_ID,
	M.RECEIVING_IIA_ID                                                                                         AS RECEIVING_IIA_ID,
	SA.ISCED_CODE + '|:|' + SA.ISCED_CLARIFICATION                                                           AS SUBJECT_AREA,
	M.STATUS_OUTGOING                                                                                          AS MOBILITY_STATUS_OUTGOING,
	M.STATUS_INCOMING                                                                                          AS MOBILITY_STATUS_INCOMING,
	M.MOBILITY_COMMENT                                                                                         AS MOBILITY_COMMENT,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                         AS MOBILITY_TYPE,
	dbo.EXTRAE_NIVELES_IDIOMAS(M.MOBILITY_ID)                                                           AS LANGUAGE_SKILL,
	MP.GLOBAL_ID                                                                                               AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	dbo.EXTRAE_PHOTO_URL_LIST(STC.CONTACT_DETAILS_ID)                                                              AS STUDENT_PHOTO_URLS,
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID, M.RECEIVING_INSTITUTION_ID)                          AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID, M.RECEIVING_INSTITUTION_ID)                         AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID, M.SENDING_INSTITUTION_ID)                              AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID, M.SENDING_INSTITUTION_ID)                             AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SC.CONTACT_DETAILS_ID)                                                                    	   AS SENDER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SCD.MAILING_ADDRESS)                                                                  AS SENDER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.MAILING_ADDRESS)                                                                        AS SENDER_CONTACT_MAILING_ADDR,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_SEN_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SACD.MAILING_ADDRESS)                                                                 AS ADMV_SEN_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.MAILING_ADDRESS)                                                                       AS ADMV_SEN_CONTACT_MAILING_ADDR,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RC.CONTACT_DETAILS_ID)                                                                    	   AS RECEIVER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RCD.MAILING_ADDRESS)                                                                  AS RECEIVER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.MAILING_ADDRESS)                                                                        AS RECEIVER_CONTACT_MAILING_ADDR,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RAC.CONTACT_DETAILS_ID)                                                                    	   AS ADMV_REC_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RACD.MAILING_ADDRESS)                                                                 AS ADMV_REC_CONTACT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.MAILING_ADDRESS)                                                                       AS ADMV_REC_CONTACT_MAILING_ADDR,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY,
	dbo.EXTRAE_ACADEMIC_TERM(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_TERM,
	dbo.EXTRAE_ACADEMIC_YEAR(M.ACADEMIC_TERM_ID)                                                                   AS ACADEMIC_YEAR
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
LEFT JOIN EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;
GO

/*
	Obtiene la informacion de los learning agreements. Se empleará para identificar que learning agreements se han modificado.
	La informacion del estudiante en esta tabla solo estará presente cuando suponga un cambio sobre la informacion original que figura en la movilidad.
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LEARNING_AGREEMENT_VIEW'
) DROP VIEW dbo.LEARNING_AGREEMENT_VIEW;
GO 
CREATE view dbo.LEARNING_AGREEMENT_VIEW AS SELECT 
	M.ID                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION                        AS MOBILITY_REVISION,
	LA.ID                                      AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION             AS LA_REVISION,
	LA.STATUS                                  AS LA_STATUS,
	LA.CHANGES_ID							   AS LA_CHANGES_ID,
	dbo.EXTRAE_ISCED_GRADE(TOR.ID)                 AS GRADE_CONVERSION_TABLES,
	LA.COMMENT_REJECT						   AS COMMENT_REJECT,
	dbo.EXTRAE_FIRMAS(SS.ID)                       AS STUDENT_SIGN,
	SS.SIGNATURE                               AS STUDENT_SIGNATURE,
	dbo.EXTRAE_FIRMAS(SCS.ID)                      AS SENDER_SIGN,
	SCS.SIGNATURE                              AS SENDER_SIGNATURE,
	dbo.EXTRAE_FIRMAS(RCS.ID)                      AS RECEIVER_SIGN,
	RCS.SIGNATURE                              AS RECEIVER_SIGNATURE,
	LA.MODIFIED_DATE                           AS MODIFIED_DATE,
	MP.GLOBAL_ID                  			   AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                            AS STUDENT_NAME,
	STP.LAST_NAME                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                              AS STUDENT_LOCALITY,
	STFA.REGION                                AS STUDENT_REGION,	
	STFA.COUNTRY                               AS STUDENT_COUNTRY	
FROM EWPCV_MOBILITY M
INNER JOIN EWPCV_MOBILITY_LA MLA
    ON MLA.MOBILITY_ID = M.MOBILITY_ID
INNER JOIN EWPCV_LEARNING_AGREEMENT LA 
    ON MLA.LEARNING_AGREEMENT_ID = LA.ID AND MLA.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
INNER JOIN EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
LEFT JOIN EWPCV_SIGNATURE SS 
    ON LA.STUDENT_SIGN = SS.ID
LEFT JOIN EWPCV_SIGNATURE SCS 
    ON LA.SENDER_COORDINATOR_SIGN = SCS.ID
LEFT JOIN EWPCV_SIGNATURE RCS 
    ON LA.RECEIVER_COORDINATOR_SIGN = RCS.ID
LEFT JOIN EWPCV_CONTACT STC 
	ON LA.MODIFIED_STUDENT_CONTACT_ID = STC.ID
LEFT JOIN EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
LEFT JOIN EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
LEFT JOIN EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
LEFT JOIN EWPCV_CONTACT_DETAILS SCD
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
LEFT JOIN EWPCV_TOR TOR
    ON LA.TOR_ID = TOR.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_COMPONENTS_VIEW'
) DROP VIEW dbo.LA_COMPONENTS_VIEW;
GO 
/*
	Obtiene la informacion de los componentes asociados a los learning agreements.
*/
CREATE VIEW dbo.LA_COMPONENTS_VIEW AS WITH COMPONENTS AS (
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, RC.RECOGNIZED_LA_COMPONENT_ID as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_RECOGNIZED_LA_COMPONENT RC ON RC.LEARNING_AGREEMENT_ID = LA.ID AND RC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, VC.VIRTUAL_LA_COMPONENT_ID as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_VIRTUAL_LA_COMPONENT VC ON VC.LEARNING_AGREEMENT_ID = LA.ID AND VC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, BC.BLENDED_LA_COMPONENT_ID as BLENDED_LA_COMPONENT_ID, null as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_BLENDED_LA_COMPONENT BC ON BC.LEARNING_AGREEMENT_ID = LA.ID AND BC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
    UNION
    SELECT la.id, la.LEARNING_AGREEMENT_REVISION, null as STUDIED_LA_COMPONENT_ID, null as RECOGNIZED_LA_COMPONENT_ID, null as VIRTUAL_LA_COMPONENT_ID, null as BLENDED_LA_COMPONENT_ID, DC.DOCTORAL_LA_COMPONENTS_ID as DOCTORAL_LA_COMPONENTS_ID FROM EWPCV_LEARNING_AGREEMENT LA INNER JOIN EWPCV_DOCTORAL_LA_COMPONENT DC ON DC.LEARNING_AGREEMENT_ID = LA.ID AND DC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
)
SELECT
	C.ID                                            AS COMPONENT_ID,
	LA.ID                                           AS LA_ID,
	LA.LEARNING_AGREEMENT_REVISION                  AS LA_REVISION,
	C.LA_COMPONENT_TYPE                             AS COMPONENT_TYPE,
	C.TITLE                                         AS TITLE,
	C.SHORT_DESCRIPTION                             AS DESCRIPTION,
	dbo.EXTRAE_CREDITS(C.id)                            AS CREDITS,
	dbo.EXTRAE_ACADEMIC_TERM(ATR.ID)                    AS ACADEMIC_TERM,
	dbo.EXTRAE_NOMBRES_ACADEMIC_TERM(ATR.ID)            AS ACADEMIC_TERM_NAMES,
	concat(AY.START_YEAR , '|:|' , AY.END_YEAR)           AS ACADEMIC_YEAR,
	C.STATUS                                        AS COMPONENT_STATUS,
	C.REASON_CODE                                   AS REASON_CODE,
	C.REASON_TEXT                                   AS REASON_TEXT,
	C.RECOGNITION_CONDITIONS                        AS RECOGNITION_CONDITIONS,
	C.LOS_ID                                        AS LOS_ID,
	C.LOS_CODE                                      AS LOS_CODE,
	C.LOI_ID                                        AS LOI_ID,
	ATR.INSTITUTION_ID                              AS INSTITUTION_ID,
	dbo.EXTRAE_NOMBRES_INSTITUCION(LOS.INSTITUTION_ID)  AS RECEIVING_INSTITUTION_NAMES,
	dbo.EXTRAE_CODIGO_OUNIT(ATR.ORGANIZATION_UNIT_ID, ATR.INSTITUTION_ID)   AS OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(ATR.ORGANIZATION_UNIT_ID, ATR.INSTITUTION_ID)  AS RECEIVING_OUNIT_NAMES,
	dbo.EXTRAE_NOMBRES_LOS(LOS.ID)                      AS LOS_NAMES,
	dbo.EXTRAE_DESCRIPTIONS_LOS(LOS.ID)                 AS LOS_DESCRIPTIONS,
	dbo.EXTRAE_URLS_LOS(LOS.ID)                         AS LOS_URL,
    dbo.EXTRAE_LEVELS(LOI.ID)                           AS LOI_LEVELS,
	dbo.EXTRAE_GRAD_SCHEME_DESC(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_DESC,
	dbo.EXTRAE_GRAD_SCHEME_LABEL(LOI.GRADING_SCHEME_ID)	AS LOI_GRAD_SCHEME_LABEL,
	dbo.EXTRAE_RES_DIST(LOI.RESULT_DISTRIBUTION)        AS RESULT_DISTR,
	dbo.EXTRAE_GROUP_TYPE(LOI.ID)                       AS LOI_GROUP_TYPE,
	dbo.EXTRAE_GROUP(LOI.ID)                            AS LOI_GROUP,
	LOI.ENGAGEMENT_HOURS                            AS ENGAGEMENT_HOURS,
	LOI.LANGUAGE_OF_INSTRUCTION                     AS LANGUAGE_OF_INSTRUCTION,
    LOI.START_DATE                                  AS START_DATE,
    LOI.END_DATE                                    AS END_DATE,
    LOI.PERCENTAGE_LOWER                            AS PERCENTAGE_LOWER,
    LOI.PERCENTAGE_EQUAL                            AS PERCENTAGE_EQUAL,
    LOI.PERCENTAGE_HIGHER                           AS PERCENTAGE_HIGHER,
    LOI.RESULT_LABEL                                AS RESULT_LABEL,
    LOI.STATUS                                      AS LOI_STATUS,
    CONVERT(varchar(max),LOI.EXTENSION)             AS LOI_EXTENSION
FROM COMPONENTS LA
INNER JOIN EWPCV_LA_COMPONENT C
    ON C.ID = LA.STUDIED_LA_COMPONENT_ID
    OR C.ID = LA.RECOGNIZED_LA_COMPONENT_ID
    OR C.ID = LA.VIRTUAL_LA_COMPONENT_ID
    OR C.ID = LA.BLENDED_LA_COMPONENT_ID
    OR C.ID = LA.DOCTORAL_LA_COMPONENTS_ID
LEFT JOIN EWPCV_ACADEMIC_TERM ATR
    ON C.ACADEMIC_TERM_DISPLAY_NAME = ATR.ID
LEFT JOIN EWPCV_ACADEMIC_YEAR AY
    ON ATR.ACADEMIC_YEAR_ID= AY.ID
LEFT JOIN EWPCV_LOS LOS
    ON C.LOS_ID= LOS.ID
LEFT JOIN EWPCV_LOI LOI
    ON C.LOI_ID= LOI.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'DICTIONARIES_VIEW'
) DROP VIEW dbo.DICTIONARIES_VIEW;
GO 
CREATE VIEW dbo.DICTIONARIES_VIEW as 
  SELECT 
	ID 				AS ID,
	CODE 			AS CODE,
	DESCRIPTION 	AS DESCRIPTION,
	DICTIONARY_TYPE AS DICTIONARY_TYPE,
	CALL_YEAR 		AS DICTIONARY_YEAR
FROM dbo.EWPCV_DICTIONARY
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'EVENT_VIEW'
) DROP VIEW dbo.EVENT_VIEW;
GO 
CREATE VIEW dbo.EVENT_VIEW AS 
  SELECT 
ID					AS ID,
EVENT_DATE			AS EVENT_DATE,
TRIGGERING_HEI		AS TRIGGERING_HEI,
OWNER_HEI			AS OWNER_HEI,
EVENT_TYPE			AS EVENT_TYPE,
CHANGED_ELEMENT_ID	AS CHANGED_ELEMENT_ID,
ELEMENT_TYPE		AS ELEMENT_TYPE,
OBSERVATIONS		AS OBSERVATIONS,
STATUS				AS STATUS,
REQUEST_PARAMS		AS REQUEST_PARAMS,
CONVERT(varchar(max),MESSAGE)				AS MESSAGE,
X_REQUEST_ID		AS X_REQUEST_ID,
REQUEST_BODY 		AS REQUEST_BODY
FROM dbo.EWPCV_EVENT
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_IDENTIFIERS_VIEW'
) DROP VIEW dbo.INSTITUTION_IDENTIFIERS_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_IDENTIFIERS_VIEW AS 
  SELECT 
	SCHAC AS SCHAC,
	PREVIOUS_SCHAC AS PREVIOUS_SCHAC,
	PIC AS PIC,
	ERASMUS AS ERASMUS,
	EUC AS EUC,
	ERASMUS_CHARTER AS ERASMUS_CHARTER,
    INSTITUTION_NAMES AS INSTITUTION_NAMES,
    OTHER_IDS AS OTHER_IDS
FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'NOTIFICATIONS_VIEW'
) DROP VIEW dbo.NOTIFICATIONS_VIEW;
GO 
CREATE VIEW dbo.NOTIFICATIONS_VIEW AS SELECT 
	ID 				   	AS ID,
	CHANGED_ELEMENT_IDS	AS CHANGED_ELEMENT_ID,
	HEI_ID 				AS HEI_ID,
	NOTIFICATION_DATE 	AS NOTIFICATION_DATE,
	"TYPE" 				AS "TYPE",
	CNR_TYPE 			AS CNR_TYPE,
	RETRIES 			AS RETRIES,
	PROCESSING 			AS PROCESSING,
	OWNER_HEI 			AS OWNER_HEI,
	PROCESSING_DATE 	AS PROCESSING_DATE,
	SHOULD_RETRY 		AS SHOULD_RETRY,
	LAST_ERROR 			AS LAST_ERROR
FROM dbo.EWPCV_NOTIFICATION
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'UPDATE_REQUESTS_VIEW'
) DROP VIEW dbo.UPDATE_REQUESTS_VIEW;
GO 
CREATE VIEW dbo.UPDATE_REQUESTS_VIEW AS SELECT 
ID                   AS ID,
VERSION              AS VERSION,
SENDING_HEI_ID       AS SENDING_HEI_ID,
"TYPE"               AS "TYPE",
UPDATE_REQUEST_DATE  AS UPDATE_REQUEST_DATE,
IS_PROCESSING        AS IS_PROCESSING,
MOBILITY_TYPE        AS MOBILITY_TYPE,
UPDATE_INFORMATION   AS UPDATE_INFORMATION,
RETRIES              AS RETRIES,
PROCESSING_DATE      AS PROCESSING_DATE,
LEARNING_AGREEMENT_ID AS LA_ID,
LEARNING_AGREEMENT_REVISION AS LA_REVISION,
SHOULD_RETRY AS SHOULD_RETRY,
LAST_ERROR AS LAST_ERROR
FROM dbo.EWPCV_MOBILITY_UPDATE_REQUEST
;
GO

/*
	Obtiene la informacion de Institutucion y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_VIEW'
) DROP VIEW dbo.INSTITUTION_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_VIEW AS 
SELECT DISTINCT
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    ins.LOGO_URL                                       AS INSTITUTION_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID)             AS INSTITUTION_WEBSITE,
    dbo.EXTRAE_INST_URLS_FACTSHEET(ins.ID)             AS INSTITUTION_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)     AS STREET_ADDR_LINES,
    dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)           AS STREET_ADDRESS,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)    AS MAILING_ADDR_LINES,
    dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)          AS MAILING_ADDRESS
    
        /*
            Datos de la institution
        */
        FROM dbo.EWPCV_INSTITUTION ins
        LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ins.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address
;
GO

/*
	Obtiene la informacion de los diferentes contactos de las instituciones
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_CONTACTS_VIEW'
) DROP VIEW dbo.INSTITUTION_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                          AS CONTACT_ID,
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				   AS CONTACT_URL,
    cont.CONTACT_ROLE                                  AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				   AS CONTACT_NAME,
    person.FIRST_NAMES                                 AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                   AS CONTACT_LAST_NAME,
    person.GENDER                                      AS CONTACT_GENDER,
    dbo.EXTRAE_TELEFONO(condet.ID)                         AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                              AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                   AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                  AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                 AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)        AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)              AS STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS MAILING_ADDRESS
    
        
        FROM dbo.EWPCV_INSTITUTION ins
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON (cont.institution_id = ins.institution_id AND cont.organization_unit_id is null)
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ins.primary_contact_detail_id IS NULL OR condet.ID <> ins.primary_contact_detail_id )
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address						
        LEFT JOIN dbo.EWPCV_PERSON person ON person.id = cont.person_id
		
		/*
            Datos de la institution
        */
		LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
;
GO	

/*
	Obtiene la informacion de Unidad Organizativa y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_VIEW'
) DROP VIEW dbo.OUNIT_VIEW;
GO 
CREATE VIEW dbo.OUNIT_VIEW AS 
SELECT DISTINCT
	ounit.INTERNAL_ID 									 AS INTERNAL_ID,
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID									 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    dbo.EXTRAE_NOMBRES_OUNIT(ounit.ID, ins.INSTITUTION_ID)  AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID) 				 AS OUNIT_WEBSITE,
	dbo.EXTRAE_OUNIT_URLS_FACTSHEET(ounit.INTERNAL_ID)	 AS OUNIT_FACTSHEET_URL,
    CASE WHEN
    priflexadd_s.COUNTRY is not null THEN priflexadd_s.COUNTRY  
    ELSE priflexadd_m.COUNTRY 
    END AS COUNTRY,
    CASE WHEN 
    priflexadd_s.LOCALITY is not null THEN priflexadd_s.LOCALITY  
    ELSE priflexadd_m.LOCALITY
    END AS CITY,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)       AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)             AS STREET_ADDRESS,
    dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)      AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)            AS MAILING_ADDRESS,
	ounit.PARENT_OUNIT_ID								 AS PARENT_OUNIT_ID,
    ounit.IS_EXPOSED									 AS IS_EXPOSED 
    
        /*
            Datos de la organización
        */
        FROM dbo.EWPCV_ORGANIZATION_UNIT ounit
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.INTERNAL_ID
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id
		
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON ounit.INTERNAL_ID = iou.ORGANIZATION_UNITS_ID
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ounit.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
		LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_m ON priflexadd_m.id = pricondet.mailing_address
;
GO	

/*
	Obtiene la informacion de los diferentes contactos de las unidades organizativas 
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_CONTACTS_VIEW'
) DROP VIEW dbo.OUNIT_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.OUNIT_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                            AS CONTACT_ID,
	ounit.INTERNAL_ID									 AS INTERNAL_ID,
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID                     				 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    dbo.EXTRAE_NOMBRES_OUNIT(ounit.ID, ins.INSTITUTION_ID)  AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				 AS OUNIT_WEBSITE,
    cont.CONTACT_ROLE                                 	 AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				 AS CONTACT_NAME,
    person.FIRST_NAMES                                   AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                     AS CONTACT_LAST_NAME,
    person.GENDER                                        AS CONTACT_GENDER,
    dbo.EXTRAE_TELEFONO(condet.ID)                       AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                            AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                 AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                    AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                   AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)      AS CONT_STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)            AS CONTACT_STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS CONT_MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS CONTACT_MAILING__ADDRESS,
	ounit.IS_EXPOSED									 AS IS_EXPOSED
    
        
       FROM dbo.EWPCV_ORGANIZATION_UNIT ounit
	    /*
			Datos institucion
		*/
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.organization_units_id = ounit.INTERNAL_ID
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON iou.institution_id = ins.id
        
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON cont.organization_unit_id = ounit.id AND cont.institution_id = ins.institution_id
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ounit.primary_contact_detail_id IS NULL OR condet.ID <> ounit.primary_contact_detail_id ) 
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address
        LEFT JOIN dbo.EWPCV_PERSON person ON person.id = cont.person_id

         /*
            Datos de la institution
        */
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.INTERNAL_ID
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id
;
GO	

/*
	Vista con los datos de las asignaturas
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LOS_VIEW'
) DROP VIEW dbo.LOS_VIEW;
GO 
CREATE VIEW dbo.LOS_VIEW AS 
SELECT 
ID 												AS LOS_ID,
LOS_CODE 										AS LOS_CODE,
EQF_LEVEL 										AS LOS_EQF_LEVEL,
INSTITUTION_ID 									AS LOS_INSTITUTION,
ISCEDF 											AS LOS_ISCEDF,
dbo.EXTRAE_NOMBRES_OUNIT(ORGANIZATION_UNIT_ID, INSTITUTION_ID)  AS LOS_OUNIT,
SUBJECT_AREA 									AS LOS_SUBJECT_AREA,
TOP_LEVEL_PARENT 								AS TOP_LEVLE_PARENT,
"TYPE" 											AS LOS_TYPE,
dbo.EXTRAE_NOMBRES_LOS(ID) 						AS LOS_NAMES,
dbo.EXTRAE_DESCRIPTIONS_LOS(ID) 				AS LOS_DESCRIPTIONS,
dbo.EXTRAE_URLS_LOS(ID) 						AS LOS_URLS
FROM EWPCV_LOS
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'GRADING_SCHEME_VIEW'
) DROP VIEW dbo.GRADING_SCHEME_VIEW;
GO 
CREATE VIEW dbo.GRADING_SCHEME_VIEW AS SELECT  
    g.id                                AS GRADING_SCHEME_ID,
    dbo.EXTRAE_GRAD_SCHEME_DESC(g.id)       AS GRADING_SCHEME_DESC,
    dbo.EXTRAE_GRAD_SCHEME_LABEL(g.id)      AS GRADING_SCHEME_LABELS
FROM EWPCV_GRADING_SCHEME g
;
GO

/*
	Vista con los datos de agrupaciones GROUP_TYPES con sus GROUP asociados, GROUP_TYPES sin groups asociados y GROUPS sin asociar a GROUP TYPES si los hubiera
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'GROUPING_VIEW'
) DROP VIEW dbo.GROUPING_VIEW;
GO 
CREATE VIEW dbo.GROUPING_VIEW AS SELECT 
    gt.id                           AS GROUP_TYPE_ID,
    dbo.EXTRAE_LANGUAGE_ITEM(gt.title)    AS GROUP_TYPE_NAME,
    g.id                            AS GROUP_ID,
    dbo.EXTRAE_LANGUAGE_ITEM(g.title)         AS GROUP_NAME,
    g.sorting_key                   AS SORTING_KEY
FROM EWPCV_GROUP_TYPE gt 
FULL OUTER JOIN EWPCV_GROUP g ON gt.ID = g.group_type_id
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'TOR_ATTACHMENT_VIEW'
) DROP VIEW dbo.TOR_ATTACHMENT_VIEW;
GO 
CREATE view dbo.TOR_ATTACHMENT_VIEW AS WITH IDS AS (
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, null AS REPORT_ID, null AS LOI_ID, tor_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_TOR_ATTACHMENT tor_a ON tor_a.TOR_ID = tor.ID
    UNION 
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, re.ID AS REPORT_ID, null AS LOI_ID, report_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_REPORT re ON re.tor_id = tor.id INNER JOIN EWPCV_REPORT_ATT report_a ON report_a.REPORT_ID = re.ID
    UNION
    SELECT la.id AS LA_ID, la.tor_id AS TOR_ID, re.ID AS REPORT_ID, loi.ID AS LOI_ID, loi_a.ATTACHMENT_ID AS ATTACHMENT_ID FROM EWPCV_LEARNING_AGREEMENT la INNER JOIN EWPCV_TOR tor ON tor.id = la.tor_id INNER JOIN EWPCV_REPORT re ON re.tor_id = tor.id INNER JOIN EWPCV_LOI loi ON loi.report_id = re.id INNER JOIN EWPCV_LOI_ATT loi_a ON loi_a.LOI_ID = loi.ID
)SELECT 
	ids.LA_ID								        AS LA_ID, 
	ids.TOR_ID							            AS TOR_ID,
	ids.REPORT_ID								    AS REPORT_ID,
	ids.LOI_ID								        AS LOI_ID,
	att.ATTACH_TYPE					                AS ATTACH_TYPE,
	CONVERT(varchar(max),att.EXTENSION)	            AS ATTACH_EXTENSION,
	dbo.EXTRAE_ATTACH_TITLE(att.id)		                AS ATTACHMENT_TITLES,
	dbo.EXTRAE_ATTACH_DESC(att.id)		                AS ATTACHMENT_DESC,
    att_cont.LANG                                   AS ATTACHMENT_LANG,
    att_cont."CONTENT"        AS ATTACHMENT_CONTENT
    
	FROM IDS ids
    INNER JOIN EWPCV_ATTACHMENT att
	ON att.ID = ids.ATTACHMENT_ID
	INNER JOIN EWPCV_ATTACHMENT_CONTENT att_cont
	ON att_cont.ATTACHMENT_ID = att.ID
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'DIPLOMA_VIEW'
) DROP VIEW dbo.DIPLOMA_VIEW;
GO
CREATE view dbo.DIPLOMA_VIEW AS SELECT  
    loi.ID                                  AS LOI_ID,
    dip.ID                                  AS DIPLOMA_ID,
    dip.DIPLOMA_VERSION                     AS DIPLOMA_VERSION,
    dip.ISSUE_DATE                          AS ISSUE_DATE,
    dip.INTRODUCTION                        AS INTRODUCTION,
    dip.SIGNATURE                           AS SIGNATURE,
    dip_sec.TITLE                           AS DIPLOMA_SECTION_TITLE,
    dip_sec.SECTION_NUMBER                  AS DIPLOMA_SECTION_NUMBER,
    dip_sec."CONTENT"                       AS DIPLOMA_SECTION_CONTENT,
    dbo.EXTRAE_ADDIT_INFO(dip_sec.id)           AS ADDITIONAL_INFO,
    att.ATTACH_TYPE                         AS ATTACHMENT_TYPE,
    CONVERT(varchar(max),att.EXTENSION)                           AS EXTENSION
	
    FROM EWPCV_DIPLOMA dip
    INNER JOIN EWPCV_LOI loi ON dip.ID = loi.DIPLOMA_ID
    LEFT JOIN EWPCV_DIPLOMA_SECTION dip_sec ON dip_sec.DIPLOMA_ID = dip.ID
    LEFT JOIN EWPCV_DIPLOMA_SEC_ATTACH dip_sec_at ON dip_sec.ID = dip_sec_at.section_id
    LEFT JOIN EWPCV_ATTACHMENT att ON att.ID = dip_sec_at.attachment_id
    LEFT JOIN EWPCV_ATTACHMENT_CONTENT att_content ON att_content.ATTACHMENT_ID = att.ID
;
GO

/*
Cuenta el total de LAs en un año academico procedentes de cada institucion solo tiene en cuenta la ultima version de los LAs asi evitamos contar varias veces un mismo LA si tiene varias versiones ademas extrae el estado de la ultima version del LA
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_STATS_V_STATUS'
) DROP VIEW dbo.LA_STATS_V_STATUS;
GO
CREATE  VIEW dbo.LA_STATS_V_STATUS AS
SELECT
	SENDING_INSTITUTION_ID AS SENDING_INSTITUTION,
    RECEIVING_INSTITUTION_ID AS RECEIVING_INSTITUTION,
    START_YEAR,
    END_YEAR,
    SUM(TOTAL) AS TOTAL,
    SUM(LAST_PENDING) AS LAST_PENDING,
    SUM(LAST_APPROVED) AS LAST_APPROVED,
    SUM(LAST_REJECTED) AS LAST_REJECTED,
    0 AS APPROVAL_MODIFIED,
    0 AS APPROVAL_UNMODIFIED
    FROM (
    SELECT acy.start_year AS START_YEAR, acy.end_year AS END_YEAR ,la.status, count(1) AS TOTAL,
        CASE WHEN la.status = 0 THEN Count(1) ELSE 0 END AS LAST_PENDING,
        CASE WHEN la.status in(1,2) THEN Count(1) ELSE 0 END AS LAST_APPROVED,
        CASE WHEN la.status = 3 THEN Count(1) ELSE 0 END AS LAST_REJECTED,
        M.SENDING_INSTITUTION_ID,
        M.RECEIVING_INSTITUTION_ID
    FROM
        (Select ID AS LA_ID, max(LEARNING_AGREEMENT_REVISION) AS LA_REVISION from dbo.EWPCV_LEARNING_AGREEMENT GROUP BY ID) AS LAST_REVISION
        INNER JOIN dbo.EWPCV_LEARNING_AGREEMENT LA ON LA.ID = LA_ID AND LA.LEARNING_AGREEMENT_REVISION = LAST_REVISION.LA_REVISION
        INNER JOIN dbo.EWPCV_MOBILITY_LA MLA ON MLA.LEARNING_AGREEMENT_ID = LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = LAST_REVISION.LA_REVISION
        INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_TERM ACT ON M.ACADEMIC_TERM_ID = ACT.ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR ACY ON ACT.ACADEMIC_YEAR_ID = ACY.ID
        GROUP BY acy.start_year, acy.end_year, la.status, M.SENDING_INSTITUTION_ID, M.RECEIVING_INSTITUTION_ID
        ) AS GORUPED
    GROUP BY START_YEAR, END_YEAR, SENDING_INSTITUTION_ID, RECEIVING_INSTITUTION_ID;
GO
/*
Learning agreements modificados tras la aprobacion extrae datos agregados por academicYear y por institucion emisora
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_STATS_MODIFIED'
) DROP VIEW dbo.LA_STATS_MODIFIED;
GO
CREATE VIEW dbo.LA_STATS_MODIFIED AS
    SELECT
        M.SENDING_INSTITUTION_ID AS SENDING_INSTITUTION,
        M.RECEIVING_INSTITUTION_ID AS RECEIVING_INSTITUTION,
        START_YEAR,
        END_YEAR,
        0 AS TOTAL,
        0 AS LAST_PENDING,
        0 AS LAST_APPROVED,
        0 AS LAST_REJECTED,
        COUNT(1) AS APPROVAL_MODIFIED,
        0 AS APPROVAL_UNMODIFIED
    FROM
        (Select LA1.ID AS LA_ID, LA1.LEARNING_AGREEMENT_REVISION AS LA_REVISION from dbo.EWPCV_LEARNING_AGREEMENT LA1
            WHERE LA1.status = 1
            AND LA1.ID IN
                (SELECT LA2.ID FROM dbo.EWPCV_LEARNING_AGREEMENT LA2
                WHERE LA2.ID = LA1.ID
                AND LA2.LEARNING_AGREEMENT_REVISION > LA1.LEARNING_AGREEMENT_REVISION
                )) AS MODIFIED
        INNER JOIN dbo.EWPCV_MOBILITY_LA MLA ON MLA.LEARNING_AGREEMENT_ID = LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = LA_REVISION
        INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_TERM ACT ON M.ACADEMIC_TERM_ID = ACT.ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR ACY ON ACT.ACADEMIC_YEAR_ID = ACY.ID
        GROUP BY acy.start_year, acy.end_year , M.SENDING_INSTITUTION_ID, M.RECEIVING_INSTITUTION_ID
        ;
GO
/*
Learning agreements no modificados tras la aprobacion Extrae datos agregados por academicYear y por institucion emisora
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_STATS_UNMODIFIED'
) DROP VIEW dbo.LA_STATS_UNMODIFIED;
GO
CREATE VIEW dbo.LA_STATS_UNMODIFIED AS
    SELECT
    M.SENDING_INSTITUTION_ID AS SENDING_INSTITUTION,
    M.RECEIVING_INSTITUTION_ID AS RECEIVING_INSTITUTION,
    START_YEAR,
    END_YEAR,
    0 AS TOTAL,
    0 AS LAST_PENDING,
    0 AS LAST_APPROVED,
    0 AS LAST_REJECTED,
    0 AS APPROVAL_MODIFIED,
    Count(1) as APPROVAL_UNMODIFIED
    FROM
        (Select LA1.ID AS LA_ID, LA1.LEARNING_AGREEMENT_REVISION AS LA_REVISION from dbo.EWPCV_LEARNING_AGREEMENT LA1
            WHERE LA1.status = 1
            AND LA1.ID NOT IN
                (SELECT LA2.ID FROM dbo.EWPCV_LEARNING_AGREEMENT LA2
                WHERE LA2.ID = LA1.ID
                AND LA2.LEARNING_AGREEMENT_REVISION > LA1.LEARNING_AGREEMENT_REVISION
                )) AS UNMODIFIED
        INNER JOIN dbo.EWPCV_MOBILITY_LA MLA ON MLA.LEARNING_AGREEMENT_ID = LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = LA_REVISION
        INNER JOIN dbo.EWPCV_MOBILITY M ON MLA.MOBILITY_ID = M.MOBILITY_ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_TERM ACT ON M.ACADEMIC_TERM_ID = ACT.ID
        LEFT JOIN dbo.EWPCV_ACADEMIC_YEAR ACY ON ACT.ACADEMIC_YEAR_ID = ACY.ID
        GROUP BY acy.start_year, acy.end_year , M.SENDING_INSTITUTION_ID, M.RECEIVING_INSTITUTION_ID
        ;
GO
/*
Vista para las estadisticas de los learning agreements
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'LA_STATS'
) DROP VIEW dbo.LA_STATS;
GO
CREATE VIEW dbo.LA_STATS AS
SELECT
    SENDING_INSTITUTION,
    RECEIVING_INSTITUTION,
    START_YEAR,
    END_YEAR,
    SUM(TOTAL) AS TOTAL,
    SUM(LAST_PENDING) AS LAST_PENDING,
    SUM(LAST_APPROVED) AS LAST_APPROVED,
    SUM(LAST_REJECTED) AS LAST_REJECTED,
    SUM(APPROVAL_MODIFIED) AS APPROVAL_MODIFIED,
	SUM(APPROVAL_UNMODIFIED) AS APPROVAL_UNMODIFIED ,
    SUM(APPROVAL_MODIFIED) + SUM(APPROVAL_UNMODIFIED) AS SOME_VERSION_APPROVED
    FROM (
        SELECT * FROM dbo.LA_STATS_V_STATUS VS
        UNION SELECT * FROM dbo.LA_STATS_MODIFIED M
        UNION SELECT * FROM dbo.LA_STATS_UNMODIFIED NM ) AS GROUPED
GROUP BY SENDING_INSTITUTION, RECEIVING_INSTITUTION, START_YEAR, END_YEAR;
GO
/*
Vista para las estadisticas de los learning agreements
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUTGOING_LA_STATS'
) DROP VIEW dbo.OUTGOING_LA_STATS;
GO
CREATE VIEW dbo.OUTGOING_LA_STATS AS
SELECT
    SENDING_INSTITUTION,
    START_YEAR,
    END_YEAR,
    SUM(TOTAL) AS TOTAL,
    SUM(LAST_PENDING) AS LAST_PENDING,
    SUM(LAST_APPROVED) AS LAST_APPROVED,
    SUM(LAST_REJECTED) AS LAST_REJECTED,
    SUM(APPROVAL_MODIFIED) AS APPROVAL_MODIFIED,
    SUM(APPROVAL_UNMODIFIED) AS APPROVAL_UNMODIFIED,
    SUM(SOME_VERSION_APPROVED) AS SOME_VERSION_APPROVED
FROM dbo.LA_STATS
GROUP BY START_YEAR, END_YEAR , SENDING_INSTITUTION;
GO

/*
Vista donde se recojen las estadisticas ILA
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INCOMING_LA_STATS'
) DROP VIEW dbo.INCOMING_LA_STATS;
GO
CREATe VIEW dbo.INCOMING_LA_STATS AS
SELECT
    RECEIVING_INSTITUTION,
    START_YEAR,
    END_YEAR,
    SUM(TOTAL) AS TOTAL,
    SUM(LAST_PENDING) AS LAST_PENDING,
    SUM(LAST_APPROVED) AS LAST_APPROVED,
    SUM(LAST_REJECTED) AS LAST_REJECTED,
    SUM(APPROVAL_MODIFIED) AS APPROVAL_MODIFIED,
    SUM(APPROVAL_UNMODIFIED) AS APPROVAL_UNMODIFIED,
    SUM(SOME_VERSION_APPROVED) AS SOME_VERSION_APPROVED
FROM dbo.LA_STATS
GROUP BY START_YEAR, END_YEAR , RECEIVING_INSTITUTION;
GO

/*
Vista de las estadisticas Iia
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIAS_STATS'
) DROP VIEW dbo.IIAS_STATS;
GO
CREATE  VIEW dbo.IIAS_STATS AS
  SELECT
        SUM(FETCHABLES) AS FETCHABLES,
        SUM(PARTNER_APPROVED) AS PARTNER_APPROVED,
        SUM(LOCAL_APPROVED) AS LOCAL_APPROVED,
        SUM(BOTH_APPROVED) AS BOTH_APPROVED
FROM (
    SELECT
        REMOTE_IIA_ID,
        SUM(FETCHABLE) AS FETCHABLES,
        CASE WHEN SUM(PARTNER_APPROVED) = 1 AND SUM(LOCAL_APPROVED) = 0 THEN 1 ELSE 0 END AS PARTNER_APPROVED,
        CASE WHEN SUM(PARTNER_APPROVED) = 0 AND SUM(LOCAL_APPROVED) = 1 THEN 1 ELSE 0 END AS LOCAL_APPROVED,
        CASE WHEN SUM(PARTNER_APPROVED) = 1 AND SUM(LOCAL_APPROVED) = 1 THEN 1 ELSE 0 END AS BOTH_APPROVED
    FROM (
        SELECT
            REMOTE_IIA_ID,
            CASE WHEN IS_REMOTE = 0 AND IS_EXPOSED = 1 THEN 1 ELSE 0 END AS FETCHABLE,
            CASE WHEN IS_REMOTE = 0 AND APPROVAL_COP_COND_HASH = REMOTE_COP_COND_HASH THEN 1 ELSE 0 END AS PARTNER_APPROVED,
            CASE WHEN IS_REMOTE = 1 AND APPROVAL_COP_COND_HASH = REMOTE_COP_COND_HASH THEN 1 ELSE 0 END AS LOCAL_APPROVED
        FROM dbo.EWPCV_IIA
        )AS IIA_STATUS
	GROUP BY REMOTE_IIA_ID
    )
	AS GROUPED;
GO

/*
	Vista donde se puede obtener las IIAS que estan pendientes a tener snapshots
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIAS_SNAPSHOTS_PENDING_VIEW'
) DROP VIEW dbo.IIAS_SNAPSHOTS_PENDING_VIEW;
GO 	
CREATE VIEW dbo.IIAS_SNAPSHOTS_PENDING_VIEW
AS
SELECT
    LOCAL_IIA.ID AS LOCAL_ID,
    FP.INSTITUTION_ID AS LOCAL_HEI,
    LOCAL_IIA.remote_cop_cond_hash AS LOCAL_HASH,
    LOCAL_IIA.approval_cop_cond_hash AS LOCAL_APPROVAL_HASH,
    REMOTE_IIA.REMOTE_IIA_ID AS PARTNER_ID,
    SP.INSTITUTION_ID AS PARTNER_HEI,
    REMOTE_IIA.remote_cop_cond_hash AS PARTNER_HASH,
    REMOTE_IIA.approval_cop_cond_hash AS PARTNER_APPROVAL_HASH
FROM EWPCV_IIA LOCAL_IIA
LEFT JOIN dbo.EWPCV_IIA_PARTNER FP ON LOCAL_IIA.FIRST_PARTNER_ID = FP.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP ON LOCAL_IIA.SECOND_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_IIA REMOTE_IIA ON REMOTE_IIA.approval_date IS NOT NULL
    AND REMOTE_IIA.REMOTE_IIA_ID NOT IN (SELECT REMOTE_IIA_ID FROM dbo.EWPCV_IIA_APPROVALS)
    AND REMOTE_IIA.IS_REMOTE = 1
    AND LOCAL_IIA.REMOTE_IIA_ID = REMOTE_IIA.REMOTE_IIA_ID
WHERE LOCAL_IIA.approval_date IS NOT NULL
    AND REMOTE_IIA.approval_date IS NOT NULL
    AND LOCAL_IIA.REMOTE_IIA_ID NOT IN (SELECT REMOTE_IIA_ID FROM dbo.EWPCV_IIA_APPROVALS)
    AND LOCAL_IIA.IS_REMOTE = 0;
GO

/*
	Vista donde se puede obtener los datos de la tabla EWPCV_IIA_APPROVALS
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_APPROVALS_VIEW'
) DROP VIEW dbo.IIA_APPROVALS_VIEW;
GO
CREATE VIEW dbo.IIA_APPROVALS_VIEW
AS
SELECT
    IIA_APPROVAL.ID,
    IIA_APPROVAL.LOCAL_IIA_ID,
    IIA_APPROVAL.REMOTE_IIA_ID,
    IIA_APPROVAL.IIA_API_VERSION,
    IIA_APPROVAL.LOCAL_HASH,
    IIA_APPROVAL.REMOTE_HASH,
    IIA_APPROVAL.DATE_IIA_APPROVAL,
    IIA_APPROVAL.LOCAL_HASH_V7,
    IIA_APPROVAL.REMOTE_HASH_V7,
    IIA_APPROVAL.LOCAL_MESSAGE,
    IIA_APPROVAL.REMOTE_MESSAGE,
    IIA_APPROVAL.LOCAL_HASH_V6,
    IIA_APPROVAL.REMOTE_HASH_V6
FROM
    dbo.EWPCV_IIA_APPROVALS IIA_APPROVAL;
GO

/*
    Vista donde se puede obtener los elementos borrados por una institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'DELETED_ELEMENTS_VIEW'
) DROP VIEW dbo.DELETED_ELEMENTS_VIEW;
GO
CREATE VIEW dbo.DELETED_ELEMENTS_VIEW
AS
SELECT
    ID,
    ELEMENT_ID,
    ELEMENT_TYPE,
    OWNER_SCHAC,
    DELETION_DATE
FROM
    dbo.EWPCV_DELETED_ELEMENTS;
GO

/*
    Vista donde se almacenan los ficheros como los PDFs de los IIAS
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FILES_VIEW'
) DROP VIEW dbo.FILES_VIEW;
GO
CREATE VIEW dbo.FILES_VIEW
AS
SELECT
    ID,
    FILE_ID,
    FILE_CONTENT,
    SCHAC,
    MIME_TYPE
FROM
    dbo.EWPCV_FILES;
GO

/*
    Vista donde se almacenan los datos de las regresiones de los IIAS
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_REGRESSIONS_VIEW'
) DROP VIEW dbo.IIA_REGRESSIONS_VIEW;
GO
CREATE VIEW dbo.IIA_REGRESSIONS_VIEW
AS
SELECT
    ID,
    IIA_ID,
    IIA_HASH,
    OWNER_SCHAC,
    REGRESSION_DATE
FROM
    dbo.EWPCV_IIA_REGRESSIONS;
GO