/*
***************************************
******** Funciones Comunes ************
***************************************
*/
	/*
		Persiste una etiqueta multiidioma en el sistema independientemente de si este ya existe y retorna el UUID generado para el registro.
	*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LANGUAGE_ITEM'
) DROP PROCEDURE dbo.INSERTA_LANGUAGE_ITEM;
GO 
CREATE PROCEDURE  dbo.INSERTA_LANGUAGE_ITEM(@P_LANGUAGE_ITEM xml, @v_id varchar(255) OUTPUT) as
BEGIN
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255)
	SET @lang = @P_LANGUAGE_ITEM.value('(/text/@lang)[1]','varchar(255)')
	SET @text = @P_LANGUAGE_ITEM.value('(/text)[1]','varchar(255)')
	if (@lang is not null) OR (@text is not null)
	BEGIN
		SET @v_id = NEWID()  
		INSERT INTO dbo.EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT) 
			VALUES (@v_id, @lang, @text)
	END
END
;
GO


	/*
		Inserta un registro en la tabla de cnr para poder notificar cambios
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_NOTIFICATION'
) DROP PROCEDURE dbo.INSERTA_NOTIFICATION;
GO 
CREATE PROCEDURE  dbo.INSERTA_NOTIFICATION(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255)) as
BEGIN
	INSERT INTO dbo.EWPCV_NOTIFICATION(ID, CHANGED_ELEMENT_IDS, HEI_ID, NOTIFICATION_DATE, "TYPE", CNR_TYPE, PROCESSING, OWNER_HEI)
			VALUES (NEWID(), @P_ELEMENT_ID, LOWER(@P_NOTIFY_HEI), SYSDATETIME(), @P_TYPE, 1, 0, LOWER(@P_NOTIFIER_HEI) );
END 
;


GO


	/*
		Inserta datos de persona si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_PERSONA'
) DROP PROCEDURE dbo.INSERTA_PERSONA;
GO 
CREATE PROCEDURE  dbo.INSERTA_PERSONA(@P_PERSON xml, @return_value varchar(255) OUTPUT) as
BEGIN
DECLARE @GIVEN_NAME varchar(255)
DECLARE @FAMILY_NAME varchar(255)
DECLARE @CITIZENSHIP varchar(255)
DECLARE @BIRTH_DATE date
DECLARE @GENDER varchar(255)
DECLARE @v_id varchar(255)

	SELECT
		@GIVEN_NAME = T.c.value('given_name[1]','varchar(255)'),
		@FAMILY_NAME = T.c.value('family_name[1]','varchar(255)'),
		@CITIZENSHIP = T.c.value('citizenship[1]','varchar(255)'),
		@BIRTH_DATE = T.c.value('birth_date[1]','date'),
		@GENDER = T.c.value('gender[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*') T(c) 

	IF @GIVEN_NAME IS NOT NULL 
			OR @FAMILY_NAME IS NOT NULL 
			OR @BIRTH_DATE IS NOT NULL 
			OR @CITIZENSHIP IS NOT NULL 
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO EWPCV_PERSON (ID, FIRST_NAMES, LAST_NAME, COUNTRY_CODE, BIRTH_DATE, GENDER) 
				VALUES (@v_id, @GIVEN_NAME, @FAMILY_NAME, @CITIZENSHIP, @BIRTH_DATE, @GENDER)
			SET @return_value = @v_id
		END
END
;
GO

/*
		Inserta detalles de contacto.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_DETAILS'
) DROP PROCEDURE dbo.INSERTA_CONTACT_DETAILS;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_DETAILS(@P_CONTACT xml, @return_value varchar(255) OUTPUT) as
BEGIN
		DECLARE @v_id varchar(255)
		DECLARE @v_p_id varchar(255)
		DECLARE @v_fax_id varchar(255)
		DECLARE @v_a_id varchar(255)
		DECLARE @v_ma_id varchar(255)
		DECLARE @v_li_id varchar(255)
		DECLARE @PHONE XML
		DECLARE @FAX XML
		DECLARE @STREET_ADDRESS XML
		DECLARE @MAILING_ADDRESS XML
		DECLARE @CONTACT_URL XML
		DECLARE @EMAIL_LIST XML
		DECLARE @email varchar(255)
		DECLARE @item xml

		SELECT 
			 @FAX = T.c.query('fax'),
			 @PHONE = T.c.query('phone'),
			 @STREET_ADDRESS = T.c.query('street_address'),	
			 @MAILING_ADDRESS = T.c.query('mailing_address'),	
			 @EMAIL_LIST = T.c.query('email_list'),
			 @CONTACT_URL = T.c.query('contact_url')
		FROM  @P_CONTACT.nodes('*') T(c)

		EXECUTE dbo.INSERTA_TELEFONO @FAX, @v_fax_id OUTPUT
		EXECUTE dbo.INSERTA_TELEFONO @PHONE, @v_p_id OUTPUT
		EXECUTE dbo.INSERTA_FLEXIBLE_ADDRES @STREET_ADDRESS, @v_a_id OUTPUT
		EXECUTE dbo.INSERTA_FLEXIBLE_ADDRES @MAILING_ADDRESS, @v_ma_id OUTPUT

		IF @v_p_id IS NOT NULL OR @v_a_id IS NOT NULL OR @v_ma_id IS NOT NULL OR @CONTACT_URL IS NOT NULL OR @EMAIL_LIST IS NOT NULL
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT_DETAILS (ID, PHONE_NUMBER, FAX_NUMBER, STREET_ADDRESS, MAILING_ADDRESS) VALUES (@v_id, @v_p_id, @v_fax_id, @v_a_id, @v_ma_id)

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('*/email_list/email') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @EMAIL_LIST
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@email = T.c.value('(.)[1]','varchar(255)')
					FROM @EMAIL_LIST.nodes('/email') T(c) 
					INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (@v_id, @email);
					FETCH NEXT FROM cur INTO @EMAIL_LIST
				END
			CLOSE cur
			DEALLOCATE cur

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('*/contact_url/text') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @CONTACT_URL
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.query('.')
					FROM @CONTACT_URL.nodes('/text') T(c) 
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
					INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (@v_id, @v_li_id);
					FETCH NEXT FROM cur INTO @CONTACT_URL
				END
			CLOSE cur
			DEALLOCATE cur

		END
		SET @return_value = @v_id

END
;
GO

/*
	Inserta datos de contacto y de persona 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_PERSON'
) DROP PROCEDURE dbo.INSERTA_CONTACT_PERSON;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_PERSON(@P_PERSON xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value varchar(255) OUTPUT) as
BEGIN
DECLARE @GENDER varchar(255)

DECLARE @CONTACT xml
DECLARE @CONTACT_NAME xml
DECLARE @CONTACT_DESCRIPTION xml
DECLARE @INSTITUTION_ID varchar(255)
DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
DECLARE @CONTACT_ROLE varchar(255)
DECLARE @v_p_id varchar(255)
DECLARE @v_c_id varchar(255)
DECLARE @v_ounit_id varchar(255)
DECLARE @v_inst_id varchar(255)
DECLARE @v_id varchar(255)
DECLARE @v_li_id varchar(255)
DECLARE @item xml

	SELECT
		@CONTACT = T.c.query('.'),
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('contact_role[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*/*') T(c) 

	EXECUTE dbo.INSERTA_PERSONA @P_PERSON, @v_p_id OUTPUT
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @CONTACT, @v_c_id OUTPUT

	IF @v_p_id is not null or @v_c_id is not null or 
		@CONTACT_NAME is not null or @CONTACT_DESCRIPTION is not null 
	BEGIN

		IF @P_INSTITUTION_ID IS NOT NULL 
		BEGIN
			SET @v_inst_id = LOWER(@P_INSTITUTION_ID);
		END;
                
	
		IF @P_OUNIT_CODE IS NOT NULL 
		BEGIN 
			SELECT @v_ounit_id = tabO.ID
				FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.INTERNAL_ID
				INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
				WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
				AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
		END 

		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, @v_p_id);

		-- Lista de contact name
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_NAME.nodes('contact_name/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

		-- Lista de contact description
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

	END

	SET @return_value = @v_id

END
;
GO

/* 
Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_CONTACT_DETAILS'
) DROP PROCEDURE dbo.BORRA_CONTACT_DETAILS;
GO 
CREATE PROCEDURE  dbo.BORRA_CONTACT_DETAILS(@P_ID varchar(255)) AS 
BEGIN

	DECLARE @v_phone_id varchar(255)
	DECLARE @v_fax_id varchar(255)
	DECLARE @v_mailing_id varchar(255)
	DECLARE @v_street_id varchar(255)
	DECLARE @v_url_id varchar(255)


	SELECT @v_phone_id=PHONE_NUMBER, @v_fax_id = FAX_NUMBER,  @v_mailing_id=MAILING_ADDRESS, @v_street_id=STREET_ADDRESS
		FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DECLARE cur CURSOR LOCAL FOR
		SELECT URL_ID FROM dbo.EWPCV_CONTACT_URL WHERE CONTACT_DETAILS_ID=@P_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @v_url_id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_CONTACT_URL WHERE URL_ID = @v_url_id
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @v_url_id
		FETCH NEXT FROM cur INTO @v_url_id
	END
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = @P_ID

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DELETE FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @v_phone_id
	DELETE FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @v_fax_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_mailing_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_street_id

END
;
GO


/* 
	Persiste una lista de nombres de institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION_NAMES'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION_NAMES;
GO 
CREATE PROCEDURE  dbo.INSERTA_INSTITUTION_NAMES(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id varchar(255)
	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	DECLARE @v_count integer
	
	IF @P_INSTITUTION_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_INSTITUTION_NAMES.nodes('/institution_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe y esta asignado a la institucion
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')
			
			SELECT @v_count=count(1) FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_INSTITUTION_NAME ina on lait.ID = ina.NAME_ID
				WHERE UPPER(lait.text)=UPPER(@text) and ((@lang is null AND lait.lang is null) OR UPPER(lait.lang)=UPPER(@lang)) and ina.INSTITUTION_ID = @P_INSTITUTION_ID
			if @v_count = 0
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				INSERT INTO dbo.EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES(@v_lang_item_id, @P_INSTITUTION_ID)
				SET @v_count = 1
			END
		
			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;

GO

/*
		Persiste una lista de nombres de organizacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_NAMES'
) DROP PROCEDURE dbo.INSERTA_OUNIT_NAMES;
GO 
CREATE PROCEDURE  dbo.INSERTA_OUNIT_NAMES(@P_OUNIT_ID varchar(255), @P_ORGANIZATION_UNIT_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id varchar(255)
	DECLARE @v_count integer

	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	
	IF @P_ORGANIZATION_UNIT_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_ORGANIZATION_UNIT_NAMES.nodes('/organization_unit_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe y esta asignado a la OUNIT
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')

			SELECT @v_count=count(1) FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_ORGANIZATION_UNIT_NAME ona on lait.ID = ona.NAME_ID
				WHERE UPPER(lait.text)=UPPER(@text) and UPPER(lait.lang)=UPPER(@lang) and ona.ORGANIZATION_UNIT_ID = @P_OUNIT_ID

			if @v_count=0
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES(@v_lang_item_id, @P_OUNIT_ID)
				SET @v_count = 1
			END

			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;
GO

/* 
	Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_TELEFONO'
) DROP PROCEDURE dbo.INSERTA_TELEFONO;
GO 
CREATE PROCEDURE  dbo.INSERTA_TELEFONO(@P_PHONE xml, @v_id varchar(255) OUTPUT) as
BEGIN
	IF (@P_PHONE.value('(/*/e164)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/*/extension_number)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/*/other_format)[1]', 'varchar(255)') IS NOT NULL)
	BEGIN
		SET @v_id = NEWID()  
		INSERT INTO dbo.EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
						VALUES (@v_id, @P_PHONE.value('(/*/e164)[1]', 'varchar(255)'), @P_PHONE.value('(/*/extension_number)[1]', 'varchar(255)'), @P_PHONE.value('(/*/other_format)[1]', 'varchar(255)'))
	END 
END
;
GO


/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.INSERTA_INSTITUTION(@P_INSTITUTION_ID  VARCHAR(255), @P_ABREVIATION VARCHAR(255), @P_LOGO_URL VARCHAR(255),
		 @P_INSTITUTION_NAMES xml, @P_PRIMARY_CONTACT_DETAIL_ID VARCHAR(255), @return_value varchar(255) OUTPUT) AS

BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_count integer

	SELECT @v_count = count(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count = 0
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, PRIMARY_CONTACT_DETAIL_ID)
				VALUES (@v_id, LOWER(@P_INSTITUTION_ID), @P_ABREVIATION, @P_LOGO_URL, @P_PRIMARY_CONTACT_DETAIL_ID);
		END
	ELSE
		BEGIN
			SELECT @v_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			UPDATE  dbo.EWPCV_INSTITUTION SET  ABBREVIATION = @P_ABREVIATION, LOGO_URL = @P_LOGO_URL WHERE ID = @v_id;
		END
	
	EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_id, @P_INSTITUTION_NAMES

	SET @return_value = @v_id

END
;
GO


/*
	Borra un academic term
*/	
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_ACADEMIC_TERM'
) DROP PROCEDURE dbo.BORRA_ACADEMIC_TERM;
GO 
CREATE PROCEDURE  dbo.BORRA_ACADEMIC_TERM(@P_ID  varchar(255)) AS
BEGIN
	DECLARE @DISP_NAME_ID varchar(255)
	DECLARE @v_ay_id varchar(255)
	DECLARE @v_ay_count integer

	DECLARE cur CURSOR LOCAL FOR
		SELECT DISP_NAME_ID
			FROM dbo.EWPCV_ACADEMIC_TERM_NAME
			WHERE ACADEMIC_TERM_ID = @P_ID
	
	OPEN cur
	FETCH NEXT FROM cur INTO @DISP_NAME_ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_ACADEMIC_TERM_NAME WHERE DISP_NAME_ID = @DISP_NAME_ID
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DISP_NAME_ID
		FETCH NEXT FROM cur INTO @DISP_NAME_ID
	END 
	CLOSE cur
	DEALLOCATE cur

	SELECT @v_ay_id = ACADEMIC_YEAR_ID
			FROM dbo.EWPCV_ACADEMIC_TERM WHERE ID = @P_ID
	
	DELETE FROM dbo.EWPCV_ACADEMIC_TERM WHERE ID = @P_ID

	SELECT @v_ay_count = COUNT(1) FROM dbo.EWPCV_ACADEMIC_TERM WHERE ACADEMIC_YEAR_ID = @v_ay_id
	IF @v_ay_count = 0 
	BEGIN
		DELETE FROM dbo.EWPCV_ACADEMIC_YEAR WHERE ID = @v_ay_id
	END

END
;
GO


/*
	Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
*/	
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FLEXIBLE_ADDRES'
) DROP PROCEDURE dbo.INSERTA_FLEXIBLE_ADDRES;
GO 
CREATE PROCEDURE  dbo.INSERTA_FLEXIBLE_ADDRES(@P_ADDRESS xml, @return_value varchar(255) OUTPUT) as
BEGIN
DECLARE @building_number integer
DECLARE @building_name varchar(255)
DECLARE @street_name varchar(255)
DECLARE @unit varchar(255)
DECLARE @building_floor varchar(255)
DECLARE @post_office_box varchar(255)
DECLARE @postal_code varchar(255)
DECLARE @locality varchar(255)
DECLARE @region varchar(255)
DECLARE @country varchar(255)
DECLARE @list xml
DECLARE @item varchar(255)
DECLARE @ID varchar(255)

	SELECT
		@building_number = T.c.value('building_number[1]','varchar(255)'),
		@building_name = T.c.value('building_name[1]','varchar(255)'),
		@street_name = T.c.value('street_name[1]','varchar(255)'),
		@unit = T.c.value('unit[1]','varchar(255)'),
		@building_floor = T.c.value('building_floor[1]','varchar(255)'),
		@post_office_box = T.c.value('post_office_box[1]','varchar(255)'),
		@postal_code = T.c.value('postal_code[1]','varchar(255)'),
		@locality = T.c.value('locality[1]','varchar(255)'),
		@region = T.c.value('region[1]','varchar(255)'),
		@country = T.c.value('country[1]','varchar(255)')
	FROM  @P_ADDRESS.nodes('/*') T(c) 

	IF @postal_code is not null 
		or @locality is not null 
		or @region is not null 
		or @country is not null
		BEGIN 
			SET @ID = NEWID()
			-- INSERT
			INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (@ID, @building_number, @building_name, @country,
					@building_floor, @locality, @post_office_box, @postal_code,
					@region, @street_name, @unit )

			-- Lista recipient_name
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/recipient_name_list/recipient_name') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/recipient_name') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) 
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista address_line
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/address_line_list/address_line') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/address_line') T(c) 
					INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista delivery_point
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/delivery_point_code_list/delivery_point_code') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/delivery_point_code') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

		END 
		SET @return_value = @ID
END
;

GO


/*
	Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
	Devuelve el id de la ounit
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INST_OUNIT'
) DROP PROCEDURE dbo.INSERTA_INST_OUNIT;
GO 
CREATE PROCEDURE dbo.INSERTA_INST_OUNIT(@P_INSTITUTION xml, @return_value varchar(255) OUTPUT) as
BEGIN
	DECLARE @v_inst_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_ext_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @v_ou_code VARCHAR(255)

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@INSTITUTION_NAME = T.c.query('(institution_name)'),
		@v_ou_code = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_INSTITUTION.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, null, null, @INSTITUTION_NAME, NULL, @v_inst_id OUTPUT
	IF @v_ou_code IS NOT NULL 
	BEGIN
		EXECUTE dbo.INSERTA_OUNIT @P_INSTITUTION, @v_ou_id OUTPUT, @v_ext_ou_id OUTPUT
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @v_ou_id
		IF @v_count = 0
			INSERT INTO dbo.EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (@v_inst_id, @v_ou_id)

		SET @return_value = @v_ext_ou_id
	END 
END
;
GO


/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT'
) DROP PROCEDURE dbo.INSERTA_OUNIT;
GO 
CREATE PROCEDURE  dbo.INSERTA_OUNIT(@P_INSTITUTION xml, @internal_id varchar(255) OUTPUT, @external_id varchar(255) OUTPUT) as
BEGIN
	DECLARE @OUNIT_EXTERNAL_ID VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_CODE VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_NAME xml
	DECLARE @OUNIT_INTERNAL_ID varchar(255)

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_NAME = T.c.query('(organization_unit_name)')
	FROM @P_INSTITUTION.nodes('*') T(c)
	
	IF @ORGANIZATION_UNIT_CODE IS NOT NULL 
	BEGIN 
		SELECT @OUNIT_INTERNAL_ID = O.INTERNAL_ID, @OUNIT_EXTERNAL_ID = O.ID
				FROM EWPCV_ORGANIZATION_UNIT O
				INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.INTERNAL_ID = IO.ORGANIZATION_UNITS_ID
				INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
				WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)
				AND UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)

		IF @OUNIT_INTERNAL_ID is null
		BEGIN
			SET @OUNIT_EXTERNAL_ID = NEWID()
			SET @OUNIT_INTERNAL_ID = NEWID();
			INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE, INTERNAL_ID, IS_TREE_STRUCTURE) VALUES (@OUNIT_EXTERNAL_ID, @ORGANIZATION_UNIT_CODE, @OUNIT_INTERNAL_ID,0)
		END

		EXECUTE dbo.INSERTA_OUNIT_NAMES @OUNIT_INTERNAL_ID, @ORGANIZATION_UNIT_NAME
	END
	SET @internal_id = @OUNIT_INTERNAL_ID
	SET @external_id = @OUNIT_EXTERNAL_ID

END
;
GO


/*
	Inserta un nivel de idioma si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LANGUAGE_SKILL'
) DROP PROCEDURE dbo.INSERTA_LANGUAGE_SKILL;
GO 
CREATE PROCEDURE  dbo.INSERTA_LANGUAGE_SKILL(@P_LANGUAGE_SKILL xml, @return_value varchar(255) OUTPUT) as
BEGIN

	DECLARE @v_id VARCHAR(255)
	DECLARE @LANG VARCHAR(255) 
	DECLARE @CEFR_LEVEL VARCHAR(255)

		SELECT 
		@LANG = T.c.value('(language)[1]','varchar(255)'),
		@CEFR_LEVEL = T.c.value('(cefr_level)[1]','varchar(255)')
	FROM @P_LANGUAGE_SKILL.nodes('*') T(c)
	
	 IF @CEFR_LEVEL IS NULL 
		BEGIN
			SELECT @v_id = ID
			FROM dbo.EWPCV_LANGUAGE_SKILL
			WHERE UPPER("LANGUAGE") = UPPER(@LANG)
			AND CEFR_LEVEL is null
		END
	ELSE
		BEGIN
			SELECT @v_id = ID
			FROM dbo.EWPCV_LANGUAGE_SKILL
			WHERE UPPER("LANGUAGE") = UPPER(@LANG)
			AND  UPPER(CEFR_LEVEL) = UPPER(@CEFR_LEVEL)
		END

	IF @v_id is null and (@LANG is not null or @CEFR_LEVEL is not null)
	BEGIN
		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_LANGUAGE_SKILL (ID, "LANGUAGE", CEFR_LEVEL) VALUES (@v_id, @LANG, @CEFR_LEVEL)
	END

	SET @return_value = @v_id
	
END 
;
GO

/*
	Inserta un area de aprendizaje si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SUBJECT_AREA'
) DROP PROCEDURE dbo.INSERTA_SUBJECT_AREA;
GO 
CREATE PROCEDURE  dbo.INSERTA_SUBJECT_AREA(@P_SUBJECT_AREA xml, @return_value varchar(255) OUTPUT) as
BEGIN

	DECLARE @v_id varchar(255)
	DECLARE @ISCED_CODE VARCHAR(255) 
	DECLARE @ISCED_CLARIFICATION VARCHAR(255)

	SELECT 
		@ISCED_CODE = T.c.value('(isced_code)[1]','varchar(255)'),
		@ISCED_CLARIFICATION = T.c.value('(isced_clarification)[1]','varchar(255)')
	FROM @P_SUBJECT_AREA.nodes('subject_area') T(c)
	
	IF (@ISCED_CODE is not null) or (@ISCED_CLARIFICATION is not null)
	BEGIN
		SET @v_id =  NEWID()
		INSERT INTO dbo.EWPCV_SUBJECT_AREA (ID, ISCED_CODE, ISCED_CLARIFICATION) VALUES (@v_id, @ISCED_CODE, @ISCED_CLARIFICATION)
    END
		
	SET @return_value = @v_id
	
END 
;
GO

/* 
	Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_CONTACT'
) DROP PROCEDURE dbo.BORRA_CONTACT;
GO 
CREATE PROCEDURE  dbo.BORRA_CONTACT(@P_ID varchar(255)) AS
BEGIN
	DECLARE @v_person_id VARCHAR(255)
	DECLARE @v_details_id VARCHAR(255)
	DECLARE @NAME_ID varchar(255)
	DECLARE @DESCRIPTION_ID varchar(255)

	IF @P_ID IS NOT NULL
	BEGIN

		SELECT @v_person_id = PERSON_ID, @v_details_id = CONTACT_DETAILS_ID
			FROM dbo.EWPCV_CONTACT
			WHERE ID = @P_ID

		DECLARE cur CURSOR LOCAL FOR
			SELECT NAME_ID
			FROM dbo.EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @NAME_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_NAME WHERE NAME_ID = @NAME_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
				FETCH NEXT FROM cur INTO @NAME_ID
			END
		CLOSE cur
		DEALLOCATE cur


		DECLARE cur CURSOR LOCAL FOR
			SELECT DESCRIPTION_ID
			FROM dbo.EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
				FETCH NEXT FROM cur INTO @DESCRIPTION_ID
			END
		CLOSE cur
		DEALLOCATE cur

		DELETE FROM dbo.EWPCV_CONTACT WHERE ID = @P_ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @v_details_id
		DELETE FROM dbo.EWPCV_PERSON WHERE ID = @v_person_id

	END 

END 
;
GO

/*
	Inserta datos de contacto y retorna id de contact detail id
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT'
) DROP PROCEDURE dbo.INSERTA_CONTACT;
GO 
CREATE PROCEDURE dbo.INSERTA_CONTACT(@P_CONTACT xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_c_id varchar(255)

	DECLARE @v_ounit_id varchar(255)
	DECLARE @v_inst_id varchar(255)
	DECLARE @v_li_id varchar(255)

	DECLARE @CONTACT_NAME xml
	DECLARE @CONTACT_DESCRIPTION xml
	DECLARE @INSTITUTION_ID varchar(255) --deprecated
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255) --deprecated
	DECLARE @CONTACT_ROLE varchar(255)
	DECLARE @item xml

	SELECT 
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('(contact_role)[1]', 'varchar(255)')
	FROM @P_CONTACT.nodes('*') T(c)

	
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @P_CONTACT, @v_c_id OUTPUT

	IF @v_c_id IS NOT NULL AND @CONTACT_NAME IS NOT NULL AND  @CONTACT_DESCRIPTION IS NOT NULL  
		BEGIN
			
			IF @P_INSTITUTION_ID IS NOT NULL
				BEGIN
					SET @v_inst_id = LOWER(@P_INSTITUTION_ID);
				END;
						
			
			IF @P_OUNIT_CODE IS NOT NULL 
				BEGIN 
					SELECT @v_ounit_id = tabO.ID
						FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.INTERNAL_ID
						INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
						WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
						AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
				END 
			
			SET @v_id  = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, null)

			IF @CONTACT_NAME is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_NAME.nodes('contact_name/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

			IF @CONTACT_DESCRIPTION is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

		END 

	SET @return_value = @v_c_id

END
; 

GO


/*
	Borra la lista de inst_fact_sheet_url
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INST_FACTSHEET_URL'
) DROP PROCEDURE dbo.BORRA_INST_FACTSHEET_URL
GO 
CREATE PROCEDURE dbo.BORRA_INST_FACTSHEET_URL(@P_INSTITUTION_ID varchar(255)) AS
BEGIN
	DECLARE @URL_ID varchar(255)
	DECLARE cur CURSOR LOCAL FOR
	SELECT URL_ID
		FROM dbo.EWPCV_INST_FACT_SHEET_URL
		WHERE INSTITUTION_ID = @P_INSTITUTION_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @URL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_INST_FACT_SHEET_URL WHERE URL_ID = @URL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @URL_ID
			FETCH NEXT FROM cur INTO @URL_ID
		END
	CLOSE cur
	DEALLOCATE cur
END;
GO

/*
	Borra la lista de ounit_fact_sheet_url
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_OUNIT_FACTSHEET_URL'
) DROP PROCEDURE dbo.BORRA_OUNIT_FACTSHEET_URL
GO
CREATE PROCEDURE dbo.BORRA_OUNIT_FACTSHEET_URL(@P_OUNIT_ID varchar(255)) AS
BEGIN
	DECLARE @URL_ID varchar(255)
	DECLARE cur CURSOR LOCAL FOR
	SELECT URL_ID
		FROM dbo.EWPCV_OUNIT_FACT_SHEET_URL
		WHERE OUNIT_ID = @P_OUNIT_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @URL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_OUNIT_FACT_SHEET_URL WHERE URL_ID = @URL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @URL_ID
			FETCH NEXT FROM cur INTO @URL_ID
		END
	CLOSE cur
	DEALLOCATE cur
END;
GO

/*
		Inserta un archivo en la tabla de EWPCV_FILES
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FICHERO'
) DROP PROCEDURE dbo.INSERTA_FICHERO;
GO
CREATE PROCEDURE  dbo.INSERTA_FICHERO(@PDF varchar(max), @SCHAC varchar(255), @MIME_TYPE varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    DECLARE @v_id varchar(255)

    SET @v_id = NEWID()

	IF @MIME_TYPE IS NULL
		BEGIN
			SET @MIME_TYPE = 'application/pdf'
		END;

	INSERT INTO dbo.EWPCV_FILES (ID,FILE_ID,FILE_CONTENT,SCHAC,MIME_TYPE) VALUES (NEWID(), @v_id, CONVERT(VARBINARY(max), @PDF), LOWER(@SCHAC), @MIME_TYPE );

    SET @return_value = @v_id
END;
GO

/*
  Clona un languageItem devuelve el id del nuevo LanguageItem
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_LANGUAGE_ITEM'
) DROP PROCEDURE dbo.CLONE_LANGUAGE_ITEM;
GO
CREATE PROCEDURE dbo.CLONE_LANGUAGE_ITEM(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN

    IF @P_ID IS NOT NULL
    BEGIN
        SET @return_value = NEWID()
        INSERT INTO dbo.EWPCV_LANGUAGE_ITEM (ID, LANG, TEXT)
            SELECT @return_value, LANG, TEXT FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @P_ID
    END
END;
GO

/*
  Clona una persona con toddos sus objetos asociados y devuelve el id de la nueva persona
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_PERSON'
) DROP PROCEDURE dbo.CLONE_PERSON;
GO
CREATE PROCEDURE dbo.CLONE_PERSON(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    IF @P_ID IS NOT NULL
    BEGIN
        SET @return_value = NEWID()
        INSERT INTO dbo.EWPCV_PERSON (ID, BIRTH_DATE, COUNTRY_CODE, FIRST_NAMES, GENDER, LAST_NAME, PERSON_ID)
            SELECT @return_value, BIRTH_DATE, COUNTRY_CODE, FIRST_NAMES, GENDER, LAST_NAME, PERSON_ID
            FROM dbo.EWPCV_PERSON WHERE ID = @P_ID
    END
END;
GO

/*
  Clona un telefono con todos sus objetos asociados y devuelve el id del nuevo telefono
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_PHONE'
) DROP PROCEDURE dbo.CLONE_PHONE;
GO
CREATE PROCEDURE dbo.CLONE_PHONE(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    IF @P_ID IS NOT NULL
    BEGIN
        SET @return_value = NEWID()
        INSERT INTO dbo.EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT)
            SELECT @return_value, E164, EXTENSION_NUMBER, OTHER_FORMAT
            FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @P_ID
    END
END;
GO

/*
  Clona una direccion con todos sus objetos asociados y devuelve el id de la nueva direccion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_FLEXIBLE_ADDRESS'
) DROP PROCEDURE dbo.CLONE_FLEXIBLE_ADDRESS;
GO
CREATE PROCEDURE dbo.CLONE_FLEXIBLE_ADDRESS(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    DECLARE @v_line_count integer
    DECLARE @v_recipient_count integer
    DECLARE @v_delivery_point_count integer

    IF @P_ID IS NOT NULL
    BEGIN
        SET @return_value = NEWID()
        INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR", LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT)
            SELECT @return_value, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR", LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT
            FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @P_ID

        SELECT @v_line_count = COUNT(1) FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        IF @v_line_count > 0
        BEGIN
            INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID, ADDRESS_LINE)
                SELECT @return_value, ADDRESS_LINE
                FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        END

        SELECT @v_recipient_count = COUNT(1) FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        IF @v_recipient_count > 0
        BEGIN
            INSERT INTO dbo.EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME)
                SELECT @return_value, RECIPIENT_NAME
                FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        END

        SELECT @v_delivery_point_count = COUNT(1) FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        IF @v_delivery_point_count > 0
        BEGIN
            INSERT INTO dbo.EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE)
                SELECT @return_value, DELIVERY_POINT_CODE
                FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @P_ID
        END
    END
END;
GO

/*
  Clona un detalle de contacto con todos sus objetos asociados y devuelve el id del nuevo detalle de contacto
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_CONTACT_DETAILS'
) DROP PROCEDURE dbo.CLONE_CONTACT_DETAILS;
GO
CREATE PROCEDURE dbo.CLONE_CONTACT_DETAILS(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    DECLARE @v_old_phone_id varchar(255)
    DECLARE @v_old_fax_id varchar(255)
    DECLARE @v_old_mailing_id varchar(255)
    DECLARE @v_old_street_id varchar(255)
    DECLARE @v_phone_id varchar(255)
    DECLARE @v_fax_id varchar(255)
    DECLARE @v_mailing_id varchar(255)
    DECLARE @v_street_id varchar(255)
    DECLARE @v_mail_count integer
    DECLARE @v_photo_count integer
    DECLARE @v_url_count integer
    DECLARE @v_old_url_id varchar(255)
    DECLARE @v_language_item_id varchar(255)

    IF @P_ID IS NOT NULL
    BEGIN
        SET @return_value = NEWID()
        SELECT @v_old_phone_id = PHONE_NUMBER, @v_old_mailing_id = MAILING_ADDRESS, @v_old_street_id = STREET_ADDRESS, @v_old_fax_id = FAX_NUMBER
            FROM dbo.EWPCV_CONTACT_DETAILS
            WHERE ID = @P_ID

        EXECUTE dbo.CLONE_PHONE @v_old_phone_id, @v_phone_id OUTPUT
        EXECUTE dbo.CLONE_PHONE @v_old_fax_id, @v_fax_id OUTPUT
        EXECUTE dbo.CLONE_FLEXIBLE_ADDRESS @v_old_mailing_id, @v_mailing_id OUTPUT
        EXECUTE dbo.CLONE_FLEXIBLE_ADDRESS @v_old_street_id, @v_street_id OUTPUT

        INSERT INTO dbo.EWPCV_CONTACT_DETAILS (ID, FAX_NUMBER, PHONE_NUMBER, STREET_ADDRESS, MAILING_ADDRESS)
            VALUES (@return_value, @v_fax_id, @v_phone_id, @v_street_id, @v_mailing_id)

        SELECT @v_mail_count = COUNT(1) FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = @P_ID
        IF @v_mail_count > 0
        BEGIN
            INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL)
                SELECT @return_value, EMAIL
                FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL
                WHERE CONTACT_DETAILS_ID = @P_ID
        END

        SELECT @v_photo_count = COUNT(1) FROM dbo.EWPCV_PHOTO_URL WHERE CONTACT_DETAILS_ID = @P_ID
        IF @v_photo_count > 0
        BEGIN
            INSERT INTO dbo.EWPCV_PHOTO_URL (PHOTO_URL_ID, CONTACT_DETAILS_ID, "URL", PHOTO_SIZE, PHOTO_DATE, PHOTO_PUBLIC)
                SELECT NEWID(), @return_value, "URL", PHOTO_SIZE, PHOTO_DATE, PHOTO_PUBLIC
                FROM dbo.EWPCV_PHOTO_URL
                WHERE CONTACT_DETAILS_ID = @P_ID
        END

        SELECT @v_url_count = COUNT(1) FROM dbo.EWPCV_CONTACT_URL WHERE CONTACT_DETAILS_ID = @P_ID
        IF @v_url_count > 0
        BEGIN
            DECLARE cur CURSOR LOCAL FOR
            SELECT URL_ID
            FROM dbo.EWPCV_CONTACT_URL
            WHERE CONTACT_DETAILS_ID = @P_ID

            OPEN cur
            FETCH NEXT FROM cur INTO @v_old_url_id
            WHILE @@FETCH_STATUS = 0
            BEGIN
                EXECUTE dbo.CLONE_LANGUAGE_ITEM @v_old_url_id, @v_language_item_id OUTPUT
                INSERT INTO dbo.EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID)
                    VALUES (@return_value, @v_language_item_id)
                FETCH NEXT FROM cur INTO @v_old_url_id
            END
            CLOSE cur
            DEALLOCATE cur
        END
    END
END;
GO

/*
  Clona un contacto con todos sus objetos asociados y devuelve el id del nuevo contacto
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CLONE_CONTACT'
) DROP PROCEDURE dbo.CLONE_CONTACT;
GO
CREATE PROCEDURE dbo.CLONE_CONTACT(@P_ID varchar(255), @return_value varchar(255) OUTPUT) AS
BEGIN
    DECLARE @v_person_id varchar(255)
    DECLARE @v_contact_details_id varchar(255)
    DECLARE @v_inst_id varchar(255)
    DECLARE @v_ounit_id varchar(255)
    DECLARE @v_role varchar(255)
    DECLARE @v_names_count integer
    DECLARE @v_desc_count integer
    DECLARE @v_language_item_id varchar(255)
    DECLARE @v_old_name_id varchar(255)
    DECLARE @v_old_desc_id varchar(255)

    IF @P_ID IS NOT NULL
    BEGIN
        SELECT @v_inst_id = INSTITUTION_ID, @v_ounit_id = ORGANIZATION_UNIT_ID, @v_role = CONTACT_ROLE, @v_contact_details_id = CONTACT_DETAILS_ID, @v_person_id = PERSON_ID
            FROM dbo.EWPCV_CONTACT
            WHERE ID = @P_ID

        SET @return_value = NEWID()
        EXECUTE dbo.CLONE_PERSON @v_person_id, @v_person_id OUTPUT
        EXECUTE dbo.CLONE_CONTACT_DETAILS @v_contact_details_id, @v_contact_details_id OUTPUT

        INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID)
            VALUES (@return_value, @v_inst_id, @v_ounit_id, @v_role, @v_contact_details_id, @v_person_id)

        SELECT @v_names_count = COUNT(1) FROM dbo.EWPCV_CONTACT_NAME WHERE CONTACT_ID = @P_ID
        IF @v_names_count > 0
        BEGIN
            DECLARE cur CURSOR LOCAL FOR
            SELECT NAME_ID
            FROM dbo.EWPCV_CONTACT_NAME
            WHERE CONTACT_ID = @P_ID

            OPEN cur
            FETCH NEXT FROM cur INTO @v_old_name_id
            WHILE @@FETCH_STATUS = 0
            BEGIN
                EXECUTE dbo.CLONE_LANGUAGE_ITEM @v_old_name_id, @v_language_item_id OUTPUT
                INSERT INTO dbo.EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID)
                    VALUES (@return_value, @v_language_item_id)
                FETCH NEXT FROM cur INTO @v_old_name_id
            END
            CLOSE cur
            DEALLOCATE cur
        END

        SELECT @v_desc_count = COUNT(1) FROM dbo.EWPCV_CONTACT_DESCRIPTION WHERE CONTACT_ID = @P_ID
        IF @v_desc_count > 0
        BEGIN
            DECLARE cur CURSOR LOCAL FOR
            SELECT DESCRIPTION_ID
            FROM dbo.EWPCV_CONTACT_DESCRIPTION
            WHERE CONTACT_ID = @P_ID

            OPEN cur
            FETCH NEXT FROM cur INTO @v_old_desc_id
            WHILE @@FETCH_STATUS = 0
            BEGIN
                EXECUTE dbo.CLONE_LANGUAGE_ITEM @v_old_desc_id, @v_language_item_id OUTPUT
                INSERT INTO dbo.EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID)
                    VALUES (@return_value, @v_language_item_id)
                FETCH NEXT FROM cur INTO @v_old_desc_id
            END
            CLOSE cur
            DEALLOCATE cur
        END
    END
END;
GO