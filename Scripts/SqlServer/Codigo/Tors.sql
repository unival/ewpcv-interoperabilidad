
/******************/
/* GRADING_SCHEME */
/******************/


/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA GRADING_SCHEME

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_GRADING_SCHEME' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_GRADING_SCHEME;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_GRADING_SCHEME
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	
	<xs:element name="grading_scheme">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="label" type="languageItemListType" />
				<xs:element name="grad_description" type="languageItemListType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_GRADING_SCHEME'
) DROP PROCEDURE dbo.VALIDA_GRADING_SCHEME;
GO 
CREATE PROCEDURE dbo.VALIDA_GRADING_SCHEME(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_GRADING_SCHEME)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO








/*******************/
/* GROUP_TYPE_LIST */
/*******************/


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_GROUP_TYPE_LIST' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_GROUP_TYPE_LIST;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_GROUP_TYPE_LIST
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemType">
		<xs:sequence>
			<xs:element name="text">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="group_type_list">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="group_type" maxOccurs="unbounded">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="title" type="languageItemType" />
							<xs:element name="group_list">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="loi_group" maxOccurs="unbounded">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="title" type="languageItemType" />
													<xs:element name="sorting_key" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>							
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>
';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_GROUP_TYPE_LIST'
) DROP PROCEDURE dbo.VALIDA_GROUP_TYPE_LIST;
GO 
CREATE PROCEDURE dbo.VALIDA_GROUP_TYPE_LIST(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_GROUP_TYPE_LIST)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO







 
 
/******************/
/* GROUP_LIST */
/******************/


/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA GROUP_LIST

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_GROUP_LIST' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_GROUP_LIST;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_GROUP_LIST
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemType">
		<xs:sequence>
			<xs:element name="text">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="group_list">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="loi_group" maxOccurs="unbounded">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="title" type="languageItemType" />
							<xs:element name="sorting_key" type="xs:integer" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>							
	</xs:element>

</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_GROUP_LIST'
) DROP PROCEDURE dbo.VALIDA_GROUP_LIST;
GO 
CREATE PROCEDURE dbo.VALIDA_GROUP_LIST(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_GROUP_LIST)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO





-- FUNCIONES GENERALES BASICAS


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_GRADING_SCHEME'
) DROP PROCEDURE dbo.INSERT_GRADING_SCHEME;
GO 
CREATE PROCEDURE dbo.INSERT_GRADING_SCHEME(@P_GRADING_SCHEME XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_lang_item varchar(255)
	DECLARE @LANGITEM xml

	-- VALIDA_GRADING_SCHEME 
	EXECUTE dbo.VALIDA_GRADING_SCHEME @P_GRADING_SCHEME, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_GRADING_SCHEME(ID, [VERSION]) VALUES (@v_id, 0)

			-- Insertamos la lista de label
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_GRADING_SCHEME.nodes('grading_scheme/label/text')  T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @LANGITEM
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT @LANGITEM
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_lang_item output 
					INSERT INTO dbo.EWPCV_GRADING_SCHEME_LABEL(LABEL_ID, GRADING_SCHEME_ID) VALUES (@v_lang_item, @v_id) 
					FETCH NEXT FROM cur INTO @LANGITEM
				END
			CLOSE cur
			DEALLOCATE cur

			-- Insertamos la lista de description
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_GRADING_SCHEME.nodes('grading_scheme/description/text')  T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @LANGITEM
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT @LANGITEM
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_lang_item output 
					INSERT INTO dbo.EWPCV_GRADING_SCHEME_DESC(DESCRIPTION_ID, GRADING_SCHEME_ID) VALUES (@v_lang_item, @v_id) 
					FETCH NEXT FROM cur INTO @LANGITEM
				END
			CLOSE cur
			DEALLOCATE cur

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_GRADING_SCHEME'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END
;

GO





IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_GROUP_LIST'
) DROP PROCEDURE dbo.INSERT_GROUP_LIST;
GO 
CREATE PROCEDURE dbo.INSERT_GROUP_LIST(@P_GROUP_LIST XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_group_id varchar(255)
	DECLARE @v_lang_item varchar(255)
	DECLARE @TITLE xml
	DECLARE @SORTING_KEY integer

	-- VALIDA_GROUP_LIST 
	EXECUTE dbo.VALIDA_GROUP_LIST @P_GROUP_LIST, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			-- Insertamos la lista de grupos
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('title/text'),
				T.c.value('sorting_key[1]', 'integer')
			FROM @P_GROUP_LIST.nodes('group_list/loi_group')  T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @TITLE, @SORTING_KEY
			WHILE @@FETCH_STATUS = 0
				BEGIN
				SET @v_group_id = NEWID()
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TITLE, @v_lang_item output 
				INSERT INTO dbo.EWPCV_GROUP(ID, TITLE, SORTING_KEY) VALUES (@v_group_id, @v_lang_item, @SORTING_KEY)

				FETCH NEXT FROM cur INTO @TITLE, @SORTING_KEY
				END
			CLOSE cur
			DEALLOCATE cur

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_GROUP_LIST'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END
;

GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_GROUP_TYPE_LIST'
) DROP PROCEDURE dbo.INSERT_GROUP_TYPE_LIST;
GO 
CREATE PROCEDURE dbo.INSERT_GROUP_TYPE_LIST(@P_GROUP_TYPE_LIST XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_group_id varchar(255)
	DECLARE @v_group_type_id varchar(255)
	DECLARE @v_lang_item varchar(255)
	DECLARE @TITLE xml
	DECLARE @GROUP_LIST xml
	DECLARE @LG_TITLE xml
	DECLARE @SORTING_KEY integer

	-- VALIDA_GROUP_LIST 
	EXECUTE dbo.VALIDA_GROUP_TYPE_LIST @P_GROUP_TYPE_LIST, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			-- Lista tipo de grupo
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('title/text'),
				T.c.query('group_list')
			FROM @P_GROUP_TYPE_LIST.nodes('group_type_list/group_type')  T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @TITLE, @GROUP_LIST
			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				SET @v_group_type_id = NEWID()
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TITLE, @v_lang_item output 
				INSERT INTO dbo.EWPCV_GROUP_TYPE(ID, TITLE) VALUES (@v_group_type_id, @v_lang_item)

				-- Lista grupos loi
				DECLARE cur2 CURSOR LOCAL FOR
				SELECT 
					T.c.query('title/text'),
					T.c.value('sorting_key[1]', 'integer')
				FROM @GROUP_LIST.nodes('group_list/loi_group')  T(c)
				OPEN cur2

				FETCH NEXT FROM cur2 INTO @LG_TITLE, @SORTING_KEY
				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @v_group_id = NEWID()
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LG_TITLE, @v_lang_item output 
					INSERT INTO dbo.EWPCV_GROUP (ID, TITLE, SORTING_KEY, GROUP_TYPE_ID) 
						VALUES (@v_group_id, @v_lang_item, @SORTING_KEY, @v_group_type_id)
					
					FETCH NEXT FROM cur2 INTO @LG_TITLE, @SORTING_KEY
				END
				CLOSE cur2
				DEALLOCATE cur2
	
				FETCH NEXT FROM cur INTO  @TITLE, @GROUP_LIST
			END
			CLOSE cur
			DEALLOCATE cur

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_GROUP_TYPE_LIST'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END
;

GO



 
 


/******************/
/* TOR            */
/******************/
 
  
   /* 
    Validaciones un TOR
  */
 
 

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA TOR

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_TOR' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_TOR;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_TOR
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTP">
		<xs:restriction base="xs:anyURI">
			<xs:pattern value="https?://.+"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="HTTPS">
		<xs:restriction base="xs:anyURI">
			<xs:pattern value="https://.+" />
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="ceroOneType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>


	<xs:complexType name="blobContentListType">
		<xs:sequence>
			<xs:element name="blob_content" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="attachmentType">
		<xs:sequence>
			<xs:element name="attach_reference" type="xs:string" minOccurs="0" />
			<xs:element name="title" type="languageItemListType" />
			<xs:element name="att_type">
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:enumeration value="0"/>
						<xs:enumeration value="1"/>
						<xs:enumeration value="2"/>
						<xs:enumeration value="3"/>
						<xs:enumeration value="4"/>
						<xs:enumeration value="5"/>
						<xs:enumeration value="6"/>
						<xs:enumeration value="7"/>
						<xs:enumeration value="8"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="description" type="languageItemListType" minOccurs="0" />
			<xs:element name="content" type="blobContentListType" />
			<xs:element name="extension" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemType">
		<xs:sequence>
			<xs:element name="text">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="loiStatusType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/> <!-- PASSED -->
			<xs:enumeration value="1"/><!-- FAILED -->
			<xs:enumeration value="2"/><!-- IN-PROGRESS -->
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="educationLevelType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="sectionListType">
		<xs:sequence>
			<xs:element maxOccurs="unbounded" name="diploma_section">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="title" type="xs:string" minOccurs="0" />
						<xs:element name="content" type="xs:string" minOccurs="0" />
						<xs:element name="additional_inf_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="additional_inf" type="xs:string" />
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="section_attachment_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="attachment" type="attachmentType" />
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="section_number" type="xs:integer" />
						<xs:element name="section_list" type="sectionListType" minOccurs="0" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="tor">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="generated_date" type="xs:date" />
				<xs:element name="tor_report_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element maxOccurs="unbounded" name="tor_report">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="issue_date" type="xs:date" />
										<xs:element name="loi_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" name="loi">
														<xs:complexType>
															<xs:sequence minOccurs="0">
																<xs:element name="loi_id" type="xs:string" minOccurs="0"/>
																<xs:element name="los_id" type="xs:string" minOccurs="0"/>
																<xs:element name="los_code" type="xs:string" minOccurs="0"/>
																<xs:element name="start_date" type="xs:date" minOccurs="0"  />
																<xs:element name="end_date" type="xs:date" minOccurs="0"  />
																<xs:element name="status" type="loiStatusType" minOccurs="0" />
																<xs:element name="grading_scheme_id" type="xs:string" minOccurs="0"  />
																<xs:element name="result_label" type="xs:string" minOccurs="0"  />
																<xs:element name="percentage_lower" type="xs:integer" minOccurs="0"  />
																<xs:element name="percentage_equal" type="xs:integer" minOccurs="0"  />
																<xs:element name="percentage_higher" type="xs:integer" minOccurs="0"  />
																<xs:element name="result_distribution">
																	<xs:complexType>
																		<xs:sequence>
																			<xs:element name="dist_categories_list">
																				<xs:complexType>
																					<xs:sequence>
																						<xs:element maxOccurs="unbounded" name="dist_categories" minOccurs="0">
																							<xs:complexType>
																								<xs:sequence>
																									<xs:element name="label" type="languageItemType" minOccurs="0" />
																									<xs:element name="dist_cat_count" type="xs:integer" minOccurs="0" />
																								</xs:sequence>
																							</xs:complexType>
																						</xs:element>
																					</xs:sequence>
																				</xs:complexType>
																			</xs:element>
																			<xs:element name="description" type="languageItemListType" minOccurs="0" />
																		</xs:sequence>
																	</xs:complexType>
																</xs:element>
																<xs:element name="education_level_list">
																	<xs:complexType>
																		<xs:sequence>
																			<xs:element maxOccurs="unbounded" name="education_level">
																				<xs:complexType>
																					<xs:sequence>
																						<xs:element name="level_type" type="educationLevelType" />
																						<xs:element name="description" type="languageItemListType" />
																						<xs:element name="level_value" type="xs:string" />
																					</xs:sequence>
																				</xs:complexType>
																			</xs:element>
																		</xs:sequence>
																	</xs:complexType>
																</xs:element>
																<xs:element name="language_of_instruction" type="xs:string" minOccurs="0" />
																<xs:element name="engagement_hours" type="xs:integer" minOccurs="0" />
																<xs:element name="attachments_ref_list" minOccurs="0" >
																	<xs:complexType>
																		<xs:sequence>
																			<xs:element maxOccurs="unbounded" name="attachments_ref" type="xs:string" />
																		</xs:sequence>
																	</xs:complexType>
																</xs:element>
																<xs:element name="group_type_id" type="xs:string"  minOccurs="0" />
																<xs:element name="group_id" type="xs:string"  minOccurs="0" />
																<xs:element name="diploma" minOccurs="0" >
																	<xs:complexType>
																		<xs:sequence>
																			<xs:element name="version" type="xs:integer" />
																			<xs:element name="issue_date" type="xs:date" />
																			<xs:element name="introduction" type="xs:string" />
																			<xs:element name="signature" type="xs:string" minOccurs="0" />
																			<xs:element name="section_list" type="sectionListType" minOccurs="0">
																			</xs:element>
																		</xs:sequence>
																	</xs:complexType>
																</xs:element>
																<xs:element name="extension" type="xs:string" minOccurs="0" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="tor_attachment_list" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" name="attachment" type="attachmentType" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="mobility_attachment_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element minOccurs="0" maxOccurs="unbounded" name="attachment" type="attachmentType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="extension" type="xs:string" minOccurs="0" />
				<xs:element name="isced_table_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="isced_table" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="grade_frequency_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" name="grade_frequency">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="label" type="xs:string" />
																<xs:element name="percentage" type="xs:integer" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="isced_code" type="xs:string" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>			
		</xs:complexType>
	</xs:element>

</xs:schema>';

GO


-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_TOR'
) DROP PROCEDURE dbo.VALIDA_TOR;
GO 
CREATE PROCEDURE dbo.VALIDA_TOR(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_TOR)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO


-- VALIDAR DATOS
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_TOR'
) DROP PROCEDURE dbo.VALIDA_DATOS_TOR;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_TOR(@P_TOR xml, @P_LA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) output, @return_value integer output) AS
BEGIN
	DECLARE @TOR_REPORT_LIST xml

	SET @return_value = 0
	
	SELECT
		@TOR_REPORT_LIST = T.c.query('.')
	FROM @P_TOR.nodes('tor/tor_report_list')  T(c)

	IF @TOR_REPORT_LIST is not null
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_TOR_REPORT_LIST @TOR_REPORT_LIST, @P_LA_ID, @P_ERROR_MESSAGE output, @return_value output
	END

END
;
GO


-- Valida datos un objeto TOR_REPORT_LIST
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_TOR_REPORT_LIST'
) DROP PROCEDURE dbo.VALIDA_DATOS_TOR_REPORT_LIST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_TOR_REPORT_LIST(@P_TOR_REPORT_LIST xml, @P_LA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) output, @return_value integer output) AS
BEGIN
	DECLARE @TOR_REPORT xml
	DECLARE @TOR_ATTACHMENT_LIST xml
	DECLARE @LOI_LIST xml

	SET @return_value = 0

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @P_TOR_REPORT_LIST.nodes('tor_report_list/tor_report')  T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @TOR_REPORT
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			SELECT
				@TOR_ATTACHMENT_LIST = T.c.query('.')
			FROM @TOR_REPORT.nodes('tor_report/tor_attachment_list')  T(c)
			EXECUTE dbo.VALIDA_DATOS_TOR_ATTACHMENT_LIST @TOR_ATTACHMENT_LIST, @P_ERROR_MESSAGE output, @return_value output
	
			IF @return_value = 0 
			BEGIN
				SELECT
					@LOI_LIST = T.c.query('.')
				FROM @TOR_REPORT.nodes('tor_report/loi_list')  T(c)
				EXECUTE dbo.VALIDA_DATOS_LOI_LIST @LOI_LIST, @TOR_ATTACHMENT_LIST, @P_LA_ID, @P_ERROR_MESSAGE output, @return_value output
			END 

			FETCH NEXT FROM cur INTO @TOR_REPORT
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO

--Valida datos un objeto TOR_ATTACHMENT_LIST
--Comprobamos que no haya 2 referencias iguales */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_TOR_ATTACHMENT_LIST'
) DROP PROCEDURE dbo.VALIDA_DATOS_TOR_ATTACHMENT_LIST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_TOR_ATTACHMENT_LIST(@P_TOR_ATTACHMENT_LIST xml, @P_ERROR_MESSAGE varchar(4000) output, @return_value integer output) AS
BEGIN
	DECLARE @ATTACH_REFERENCE varchar(255)
	SET @return_value = 0
    
	DECLARE @texto_repeticiones varchar(4000)
	SET @texto_repeticiones = ''

	DECLARE @lista_attref TABLE(
		attref varchar(255))

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('attach_reference[1]', 'varchar(255)')
	FROM @P_TOR_ATTACHMENT_LIST.nodes('tor_attachment_list/attachment')  T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACH_REFERENCE
	WHILE @@FETCH_STATUS = 0
		BEGIN
		INSERT INTO @lista_attref (attref) VALUES (@ATTACH_REFERENCE)
		FETCH NEXT FROM cur INTO @ATTACH_REFERENCE
		END
	
	CLOSE cur
	DEALLOCATE cur
	
	SELECT @texto_repeticiones = @texto_repeticiones + ' Referencia ' + a.attref + ' repetida.' FROM 
		(SELECT COUNT(*) as repeticiones, attref  FROM @lista_attref GROUP BY attref) a
		WHERE a.repeticiones > 1

	IF @texto_repeticiones <> ''
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, @texto_repeticiones)
		SET @return_value = -1
	END 

END
;
GO


--Valida datos un objeto LOI_LIST
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LOI_LIST'
) DROP PROCEDURE dbo.VALIDA_DATOS_LOI_LIST;
GO 
CREATE PROCEDURE dbo.VALIDA_DATOS_LOI_LIST(@P_LOI_LIST XML, @P_TOR_ATTACHMENT_LIST XML, @P_LA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) output, @return_value integer output) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @i integer
	DECLARE @v_group_type_id varchar(255)
	DECLARE @v_group_id varchar(255)
	DECLARE @valida_datos_loi_tor_attachment_list_return integer

	DECLARE @LOI_ID varchar(255)
	DECLARE @LOS_ID varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @GROUP_TYPE_ID varchar(255)
	DECLARE @GROUP_ID varchar(255)
	DECLARE @ATTACHMENTS_REF_LIST xml
	DECLARE @STATUS varchar(255)
	DECLARE @EDUCATION_LEVEL xml

	SET @return_value = 0
	SET @i = 0

	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.value('loi_id[1]', 'varchar(255)'),
		T.c.value('los_id[1]', 'varchar(255)'),
		T.c.value('los_code[1]', 'varchar(255)'),
		T.c.value('group_type_id[1]', 'varchar(255)'),
		T.c.value('group_id[1]', 'varchar(255)'),
		T.c.value('status[1]', 'varchar(255)'),
		T.c.query('attachments_ref_list'),
		T.c.query('education_level')
	FROM @P_LOI_LIST.nodes('loi_list/loi')  T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @LOI_ID, @LOS_ID, @LOS_CODE, @GROUP_TYPE_ID, @GROUP_ID, @STATUS, @ATTACHMENTS_REF_LIST, @EDUCATION_LEVEL

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @i = @i + 1
			-- Comprobamos que exista un componente no reconocido con el loi id informado en el LA
			IF @LOI_ID is not null
			BEGIN
				 WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
				SELECT @v_count = COUNT(1) FROM COMPONENTS  
				WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(@P_LA_ID)
				AND LOWER(LOI_ID) = LOWER(@LOI_ID)	  
				
				IF @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - LOI_ID: el indentificador ', @LOI_ID, ' no está asociado a ningún componente del LA' )
					SET @return_value = -1
				END 
			END 

			-- Comprobamos que exista un componente no reconocido con el LOS_ID en el LA y con un LOI asociado
			IF @LOS_ID is not null
			BEGIN
				WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
				SELECT @v_count = COUNT(1) FROM COMPONENTS  
				WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(@P_LA_ID)
				AND LOI_ID IS NOT NULL
				AND LOWER(LOS_ID) = LOWER(@LOS_ID)
				
				IF @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - LOS_ID: el indentificador ', @LOS_ID, ' no está asociado a ningún componente del LA con LOI.' )
					SET @return_value = -1
				END 
			END 

			-- Comprobamos que exista un componente no reconocido con el LOS_CODE y con un LOI asociado
			IF @LOS_CODE is not null
			BEGIN
				WITH COMPONENTS AS (
					SELECT s.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_STUDIED_LA_COMPONENT s on s.studied_la_component_id = c.id
					UNION
					SELECT b.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_BLENDED_LA_COMPONENT b on b.blended_la_component_id = c.id
					UNION
					SELECT v.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_VIRTUAL_LA_COMPONENT v on v.virtual_la_component_id = c.id
					UNION
					SELECT d.LEARNING_AGREEMENT_ID, c.LOI_ID, c.LOS_ID, c.LOS_CODE FROM dbo.EWPCV_LA_COMPONENT c LEFT JOIN dbo.EWPCV_DOCTORAL_LA_COMPONENT d on d.doctoral_la_components_id = c.id
				)
				SELECT @v_count = COUNT(1) FROM COMPONENTS  
				WHERE LOWER(LEARNING_AGREEMENT_ID) = LOWER(@P_LA_ID)
				AND LOI_ID IS NOT NULL
				AND LOWER(LOS_CODE) = LOWER(@LOS_CODE)
				
				IF @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - LOS_CODE: el codigo ', @LOS_CODE, ' no está asociado a ningún componente del LA con LOI.' )
					SET @return_value = -1
				END
				ELSE
				BEGIN
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LOS WHERE LOWER(LOS_CODE) = LOWER(@LOS_CODE)
					IF @v_count = 0
					BEGIN
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - LOS_CODE: el codigo ', @LOS_CODE, ' no se corresponde con ningún LOS de la BBDD.' )
						SET @return_value = -1 
					END
				END 
			END 

			-- Comprobamos el id del Group Type
			IF @GROUP_TYPE_ID IS NOT NULL 
			BEGIN 
				SELECT  @v_count = COUNT(1) FROM dbo.EWPCV_GROUP_TYPE where LOWER(ID) = LOWER(@GROUP_TYPE_ID)
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - GROUP_TYPE_ID: el indentificador ', @GROUP_TYPE_ID, ' no se corresponde con ningún GROUP_TYPE de la BBDD.' )
					SET @return_value = -1 
				END
			END 

			IF @GROUP_ID IS NOT NULL 
			BEGIN 
				-- Comprobamos el id del Group
				SELECT  @v_count = COUNT(1) FROM dbo.EWPCV_GROUP WHERE ID = @GROUP_ID
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'LOI_LIST(', @i, ') - GROUP_ID: el indentificador ', @GROUP_ID, ' no se corresponde con ningún GROUP de la BBDD.' )
					SET @return_value = -1 				
				END 
			END 
			
			-- Comprobamos que la lista de attachment_refs coincida con las referencias de la lista de TOR_ATTACHMENT_LIST
			IF @ATTACHMENTS_REF_LIST is not null
			BEGIN
				IF @P_TOR_ATTACHMENT_LIST is null
				BEGIN
					SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'TOR_ATTACHMENT_LIST: No puede ir vacía la lista de attachment "TOR_ATTACHMENT_LIST" si hemos informado el campo ATTACHMENTS_REF_LIST del LOI con referencias.' )
					SET @return_value = -1
				END
				ELSE
				BEGIN
					EXECUTE dbo.VALIDA_DATOS_LOI_TOR_ATTACHMENT_LIST @P_TOR_ATTACHMENT_LIST, @ATTACHMENTS_REF_LIST, @valida_datos_loi_tor_attachment_list_return output
					IF @valida_datos_loi_tor_attachment_list_return < 0
					BEGIN
						SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'ATTACHMENTS_REF_LIST y TOR_ATTACHMENT_LIST: Alguna de las referencias informadas en el LOI ', @i, ' no existen en la lista TOR_ATTACHMENT_LIST.' )
						SET @return_value = -1
					END 
				END
			END
			
			-- Comprobamos el status
			-- *No requerido, validación implícita en el XSD*

			-- Comprobamos la lista de education_level
			-- *No requerido, validación implícita en el XSD*
					   
			FETCH NEXT FROM cur INTO @LOI_ID, @LOS_ID, @LOS_CODE, @GROUP_TYPE_ID, @GROUP_ID, @STATUS, @ATTACHMENTS_REF_LIST, @EDUCATION_LEVEL
		END 
	CLOSE cur
	DEALLOCATE cur

END 
;
GO


-- Valida datos de los adjutos de loi
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LOI_TOR_ATTACHMENT_LIST'
) DROP PROCEDURE dbo.VALIDA_DATOS_LOI_TOR_ATTACHMENT_LIST;
GO 
CREATE PROCEDURE VALIDA_DATOS_LOI_TOR_ATTACHMENT_LIST(@P_TOR_ATTACHMENT_LIST XML, @P_ATTACHMENTS_REF_LIST XML, @return_value integer output) AS
BEGIN
	DECLARE @REF varchar(255)
	SET @return_value = 0
	IF @P_TOR_ATTACHMENT_LIST is not null and @P_ATTACHMENTS_REF_LIST is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.value('.[1]','varchar(255)')
		FROM @P_ATTACHMENTS_REF_LIST.nodes('attachments_ref_list/attachments_ref') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @REF
		WHILE @@FETCH_STATUS = 0
			BEGIN
				IF dbo.EXISTE_TOR_ATTACHMENT(@REF, @P_TOR_ATTACHMENT_LIST) = -1
					SET @return_value = -1
				FETCH NEXT FROM cur INTO @REF
			END
		CLOSE cur
		DEALLOCATE cur
	END
END 
;
GO


-- Funciones auxiliares EXISTE_

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXISTE_TOR_ATTACHMENT'
) DROP FUNCTION dbo.EXISTE_TOR_ATTACHMENT;
GO 
CREATE FUNCTION dbo.EXISTE_TOR_ATTACHMENT(@P_REF_ATTACHMENT varchar(255),  @P_TOR_ATTACHMENT_LIST XML) RETURNS integer
BEGIN
	DECLARE @return_value integer
	DECLARE @v_count integer

	SELECT 
		@v_count = COUNT(*)
	FROM @P_TOR_ATTACHMENT_LIST.nodes('tor_attachment_list/attachment') T(c)
		WHERE LOWER(T.c.value('attach_reference[1]','varchar(255)')) = LOWER(@P_REF_ATTACHMENT)

	IF @v_count = 0 
		SET @return_value = -1
	ELSE
		SET @return_value = 0

	return @return_value
END
;
GO


IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXISTE_REF_ATTACHMENT'
) DROP FUNCTION dbo.EXISTE_REF_ATTACHMENT;
GO 
CREATE FUNCTION dbo.EXISTE_REF_ATTACHMENT(@P_REF_ATTACHMENT varchar(255), @P_ATTACHMENTS_REF_LIST XML) RETURNS integer AS
BEGIN
	/*
	@P_ATTACHMENTS_REF_LIST: Lista de adjuntos referenciados (solamente referencia e id)
		temp_attach_ref_list(
			temp_attach_ref(
				attach_reference varchar(255)
				attachment_id varchar(255)
			)
		)
	*/
	DECLARE @return_value integer
	DECLARE @v_count integer

	SELECT
		@v_count = COUNT(1)
	FROM @P_ATTACHMENTS_REF_LIST.nodes('temp_attach_ref_list/temp_attach_ref') T(c)
		WHERE LOWER(T.c.value('attach_reference[1]', 'varchar(255)')) = LOWER(@P_REF_ATTACHMENT)

	IF @v_count > 0	
		SET @return_value = 0
	ELSE 
		SET @return_value = -1
 
	RETURN @return_value
END 
;
GO



-- Funciones auxiliares obtener tor

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_TOR'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_TOR;
GO 
CREATE PROCEDURE dbo.OBTEN_NOTIFIER_HEI_TOR(@P_TOR_ID varchar(255), @P_HEI_TO_NOTIFY varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_la_id varchar(255)

	SELECT @v_la_id = LA.ID FROM dbo.EWPCV_LEARNING_AGREEMENT LA WHERE LOWER(LA.TOR_ID) = LOWER(@P_TOR_ID)
	EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR_INSERT @v_la_id, @P_HEI_TO_NOTIFY, @return_value output

END
;
GO


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_TOR_INSERT'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_TOR_INSERT;
GO 
CREATE PROCEDURE dbo.OBTEN_NOTIFIER_HEI_TOR_INSERT(@P_LA_ID varchar(255), @P_HEI_TO_NOTIFY varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_la_id varchar(255)
	DECLARE @v_r_hei varchar(255)
	DECLARE @v_s_hei varchar(255)
	DECLARE @v_la_revision integer

	SELECT @v_la_revision = MAX(LEARNING_AGREEMENT_REVISION) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE LOWER(ID) = LOWER(@P_LA_ID)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
    FROM dbo.EWPCV_MOBILITY M
      INNER JOIN dbo.EWPCV_MOBILITY_LA MLA
        ON MLA.MOBILITY_ID = M.MOBILITY_ID
    WHERE LOWER(MLA.LEARNING_AGREEMENT_ID) = LOWER(@P_LA_ID) AND MLA.LEARNING_AGREEMENT_REVISION = @v_la_revision

	IF LOWER(@v_s_hei) = LOWER(@P_HEI_TO_NOTIFY)
		SET @return_value = LOWER(@v_r_hei)
	ELSE
		SET @return_value = null
	
END
;
GO







-- FUNCIONES DE BORRADO DE DATOS



  /* Elimina un ToR del sistema.
    Recibe como parametro el identificador del ToR a eliminar.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_TOR'
) DROP PROCEDURE dbo.DELETE_TOR;
GO 
CREATE PROCEDURE dbo.DELETE_TOR(@P_TOR_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_total integer
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_mob_id varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios')
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_total = COUNT(1) FROM dbo.EWPCV_TOR WHERE ID = @P_TOR_ID

		IF @v_total = 0
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El ToR "' , @P_TOR_ID , '" no existe.')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR @P_TOR_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output

		IF @v_notifier_hei is null
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_mob_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)

			EXECUTE dbo.BORRA_TOR @P_TOR_ID

			EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_TOR_ID, @v_mob_id, 2, 2)

			COMMIT TRANSACTION;
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END
;
GO


  /* 
    Función para eliminar un TOR
*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_TOR'
) DROP PROCEDURE dbo.BORRA_TOR;
GO 
CREATE PROCEDURE dbo.BORRA_TOR(@P_TOR_ID varchar(255)) AS
  BEGIN

	EXECUTE dbo.BORRA_ISCED_TABLE_LIST @P_TOR_ID
    EXECUTE dbo.BORRA_REPORTS @P_TOR_ID 
    EXECUTE dbo.BORRA_MOBILITY_ATTACHMENT_LIST @P_TOR_ID
    
    UPDATE dbo.EWPCV_LEARNING_AGREEMENT SET TOR_ID = NULL WHERE LOWER(TOR_ID) = LOWER(@P_TOR_ID)
    
  	DELETE FROM dbo.EWPCV_TOR WHERE LOWER(ID) = LOWER(@P_TOR_ID)
  END
 ;
GO


  /*
    Borra los datos una lista de objetos ISCED_TABLE
  */
 

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_ISCED_TABLE_LIST'
) DROP PROCEDURE dbo.BORRA_ISCED_TABLE_LIST;
GO 
CREATE PROCEDURE dbo.BORRA_ISCED_TABLE_LIST(@P_TOR_ID varchar(255)) AS
BEGIN
	DECLARE @ID varchar(255)
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
      FROM dbo.EWPCV_ISCED_TABLE
      WHERE TOR_ID = @P_TOR_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_ISCED_TABLE @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_ISCED_TABLE WHERE TOR_ID = @P_TOR_ID

END
;
GO



  /*
    Borra los datos un objeto ISCED_TABLE
  
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_ISCED_TABLE'
) DROP PROCEDURE dbo.BORRA_ISCED_TABLE;
GO 
CREATE PROCEDURE dbo.BORRA_ISCED_TABLE(@P_ISCED_TABLE_ID varchar(255)) AS
BEGIN
	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
      FROM dbo.EWPCV_GRADE_FREQUENCY
      WHERE ISCED_TABLE_ID = @P_ISCED_TABLE_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_GRADE_FREQUENCY WHERE ID = @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

END;
GO



  /*
    Borra los datos un objeto report
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_REPORTS'
) DROP PROCEDURE dbo.BORRA_REPORTS;
GO 
CREATE PROCEDURE dbo.BORRA_REPORTS(@P_TOR_ID varchar(255)) AS
BEGIN
	DECLARE @REP_ID varchar(255) 
	DECLARE @LOI_ID varchar(255) 
	DECLARE @ATTACHMENT_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
      FROM dbo.EWPCV_REPORT
      WHERE TOR_ID = @P_TOR_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @REP_ID

	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Reinicia loi
			DECLARE cur2 CURSOR LOCAL FOR
			SELECT ID
			  FROM dbo.EWPCV_LOI
			  WHERE REPORT_ID = @REP_ID
			OPEN cur2
			FETCH NEXT FROM cur2 INTO @LOI_ID

			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.REINICIA_LOI @LOI_ID
					FETCH NEXT FROM cur2 INTO @LOI_ID
				END
			CLOSE cur2
			DEALLOCATE cur2

			-- Borra Attachments del report
			DECLARE cur2 CURSOR LOCAL FOR
			SELECT ATTACHMENT_ID 
				FROM dbo.EWPCV_REPORT_ATT 
				WHERE REPORT_ID = @REP_ID
			OPEN cur2
			FETCH NEXT FROM cur2 INTO @ATTACHMENT_ID

			WHILE @@FETCH_STATUS = 0
				BEGIN
					DELETE FROM dbo.EWPCV_REPORT_ATT WHERE ATTACHMENT_ID = @ATTACHMENT_ID
					EXECUTE dbo.BORRA_ATTACHMENT @ATTACHMENT_ID      
					FETCH NEXT FROM cur2 INTO @ATTACHMENT_ID
				END
			CLOSE cur2
			DEALLOCATE cur2
			
			DELETE FROM dbo.EWPCV_REPORT WHERE TOR_ID = @P_TOR_ID

			FETCH NEXT FROM cur INTO @REP_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


  /*
    Borra los datos un objeto loi
  */
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REINICIA_LOI'
) DROP PROCEDURE dbo.REINICIA_LOI;
GO 
CREATE PROCEDURE dbo.REINICIA_LOI(@P_LOI_ID varchar(255)) AS
 BEGIN
	DECLARE @GRADING_SCHEME_ID varchar(255)
	DECLARE @RESULT_DISTRIBUTION varchar(255)
	DECLARE @DIPLOMA_ID varchar(255)
	DECLARE @ATTACHMENT_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT GRADING_SCHEME_ID, RESULT_DISTRIBUTION, DIPLOMA_ID
      FROM dbo.EWPCV_LOI
      WHERE ID = @P_LOI_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @GRADING_SCHEME_ID, @RESULT_DISTRIBUTION, @DIPLOMA_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE dbo.EWPCV_LOI SET ENGAGEMENT_HOURS = NULL, LANGUAGE_OF_INSTRUCTION = NULL, REPORT_ID = NULL,
			GRADING_SCHEME_ID = NULL, RESULT_DISTRIBUTION = NULL, DIPLOMA_ID = NULL , PERCENTAGE_LOWER = NULL, PERCENTAGE_EQUAL = NULL, 
			PERCENTAGE_HIGHER = NULL, RESULT_LABEL = NULL, STATUS = NULL WHERE ID = @P_LOI_ID
    
			EXECUTE dbo.BORRA_RESULT_DISTRIBUTION @RESULT_DISTRIBUTION
			EXECUTE dbo.BORRA_DIPLOMA @DIPLOMA_ID
			EXECUTE dbo.BORRA_EDUCATION_LEVEL @P_LOI_ID
			DELETE FROM dbo.EWPCV_LOS_LOI WHERE LOI_ID = @P_LOI_ID

			DECLARE cur2 CURSOR LOCAL FOR
				SELECT ATTACHMENT_ID FROM dbo.EWPCV_LOI_ATT WHERE LOI_ID = @P_LOI_ID
			OPEN cur2
			FETCH NEXT FROM cur2 into @ATTACHMENT_ID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					DELETE FROM dbo.EWPCV_LOI_ATT WHERE ATTACHMENT_ID = @ATTACHMENT_ID
					EXECUTE dbo.BORRA_ATTACHMENT @ATTACHMENT_ID
					FETCH NEXT FROM cur2 into @ATTACHMENT_ID
				END
			CLOSE cur2
			DEALLOCATE cur2

			FETCH NEXT FROM cur INTO @GRADING_SCHEME_ID, @RESULT_DISTRIBUTION, @DIPLOMA_ID
		END 
		
	CLOSE cur
	DEALLOCATE cur
END
;

GO
  
  
  /*
    Borra los datos un objeto grading scheme
  */ 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_GRADING_SCHEME'
) DROP PROCEDURE dbo.BORRA_GRADING_SCHEME;
GO 
CREATE PROCEDURE dbo.BORRA_GRADING_SCHEME(@P_ID varchar(255)) AS
 BEGIN
	DECLARE @DESCRIPTION_ID varchar(255)
	DECLARE @LABEL_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT DESCRIPTION_ID 
      FROM dbo.EWPCV_GRADING_SCHEME_DESC 
      WHERE GRADING_SCHEME_ID = @P_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @DESCRIPTION_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
			FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		END 
	CLOSE cur
	DEALLOCATE cur



	DECLARE cur CURSOR LOCAL FOR
	SELECT LABEL_ID 
      FROM dbo.EWPCV_GRADING_SCHEME_LABEL 
      WHERE GRADING_SCHEME_ID = @P_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @LABEL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @LABEL_ID
			FETCH NEXT FROM cur INTO @LABEL_ID
		END 
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_GRADING_SCHEME WHERE ID = @P_ID

END
;
GO


  /*
    Borra los datos un objeto result distribution
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_RESULT_DISTRIBUTION'
) DROP PROCEDURE dbo.BORRA_RESULT_DISTRIBUTION;
GO 
CREATE PROCEDURE dbo.BORRA_RESULT_DISTRIBUTION(@P_ID varchar(255)) AS
BEGIN
	DECLARE @DESCRIPTION_ID varchar(255)
	DECLARE @ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT DESCRIPTION_ID 
      FROM dbo.EWPCV_DISTR_DESCRIPTION 
      WHERE RESULT_DISTRIBUTION_ID = @P_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @DESCRIPTION_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_DISTR_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
			FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		END 
	CLOSE cur
	DEALLOCATE cur

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID 
      FROM EWPCV_RESULT_DIST_CATEGORY 
      WHERE RESULT_DIST_ID = @P_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM EWPCV_RESULT_DIST_CATEGORY WHERE ID = @ID  
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @ID
			FETCH NEXT FROM cur INTO @ID
		END 
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_RESULT_DISTRIBUTION WHERE ID = @P_ID

END
;
GO



  /*
    Borra los datos un objeto diploma
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_DIPLOMA'
) DROP PROCEDURE dbo.BORRA_DIPLOMA;
GO 
CREATE PROCEDURE dbo.BORRA_DIPLOMA(@P_ID varchar(255)) AS
 BEGIN
	DECLARE @SECTION_ID varchar(255)
	DECLARE @ATTACHMENT_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_DIPLOMA_SECTION
		WHERE DIPLOMA_ID = @P_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @SECTION_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--attachments de la seccion
			DECLARE cur2 CURSOR LOCAL FOR
			SELECT ATTACHMENT_ID 
				FROM dbo.EWPCV_DIPLOMA_SEC_ATTACH
				WHERE SECTION_ID = @SECTION_ID
			OPEN cur2
			FETCH NEXT FROM cur2 INTO @ATTACHMENT_ID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					--borramos relacion
					DELETE FROM dbo.EWPCV_DIPLOMA_SEC_ATTACH WHERE ATTACHMENT_ID = @ATTACHMENT_ID
					--borramos objeto
					EXECUTE dbo.BORRA_ATTACHMENT @ATTACHMENT_ID
					FETCH NEXT FROM cur2 INTO @ATTACHMENT_ID	
				END 
			CLOSE cur2
			DEALLOCATE cur2
			FETCH NEXT FROM cur INTO @SECTION_ID
		END 
	CLOSE cur
	DEALLOCATE cur
	
	--Borramos las relaciones de todas las secciones del diploma 
	DELETE FROM dbo.EWPCV_DIPLOMA_SECTION_SECTION WHERE ID IN (SELECT ID FROM dbo.EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = @P_ID)
	--Borramos la informacion adicional de todas las secciones del diploma 
	DELETE FROM dbo.EWPCV_ADDITIONAL_INFO WHERE SECTION_ID IN (SELECT ID FROM dbo.EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = @P_ID)
	--Borramos todas las secciones del diploma 
	DELETE FROM dbo.EWPCV_DIPLOMA_SECTION WHERE DIPLOMA_ID = @P_ID
	--Borramos el diploma
	DELETE FROM dbo.EWPCV_DIPLOMA WHERE ID = @P_ID

END
;
GO




  /*
    Borra los datos de un attachment
*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_ATTACHMENT'
) DROP PROCEDURE dbo.BORRA_ATTACHMENT;
GO 
CREATE PROCEDURE dbo.BORRA_ATTACHMENT(@P_ATTACHMENT_ID varchar(255)) AS
BEGIN

	DECLARE @v_attach_desc_id varchar(255)
	DECLARE @v_attach_tit_id  varchar(255)

	DECLARE cur_d CURSOR LOCAL FOR 
	SELECT ID
		FROM dbo.EWPCV_ATTACHMENT_DESCRIPTION
		WHERE ATTACHMENT_ID = @P_ATTACHMENT_ID
	OPEN cur_d
	FETCH NEXT FROM cur_d INTO @v_attach_desc_id
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_ATTACHMENT_DESCRIPTION WHERE ID = @v_attach_desc_id
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @v_attach_desc_id
			FETCH NEXT FROM cur_d INTO @v_attach_desc_id
		END
	CLOSE cur_d
	DEALLOCATE cur_d

	DECLARE cur_t CURSOR LOCAL FOR 
	SELECT ID
		FROM dbo.EWPCV_ATTACHMENT_TITLE
		WHERE ATTACHMENT_ID = @P_ATTACHMENT_ID
	OPEN cur_t
	FETCH NEXT FROM cur_t INTO @v_attach_tit_id
	WHILE @@FETCH_STATUS = 0
		BEGIN
		DELETE FROM dbo.EWPCV_ATTACHMENT_TITLE WHERE ID = @v_attach_tit_id
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @v_attach_tit_id
		FETCH NEXT FROM cur_t INTO @v_attach_tit_id
	END
	CLOSE cur_t
	DEALLOCATE cur_t

	DELETE FROM dbo.EWPCV_TOR_ATTACHMENT WHERE ATTACHMENT_ID = @P_ATTACHMENT_ID
	DELETE FROM dbo.EWPCV_ATTACHMENT_CONTENT WHERE ATTACHMENT_ID = @P_ATTACHMENT_ID
	DELETE FROM dbo.EWPCV_ATTACHMENT WHERE ID = @P_ATTACHMENT_ID

END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_EDUCATION_LEVEL'
) DROP PROCEDURE dbo.BORRA_EDUCATION_LEVEL;
GO 
CREATE PROCEDURE dbo.BORRA_EDUCATION_LEVEL(@P_LOI_ID varchar(255)) AS
BEGIN
	DECLARE @v_level_desc_id varchar(255)
	DECLARE @LEVEL_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT LEVEL_ID FROM dbo.EWPCV_LEVELS WHERE LOI_ID = @P_LOI_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @LEVEL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_LEVELS WHERE LEVEL_ID = @LEVEL_ID
			SELECT @v_level_desc_id = LEVEL_DESCRIPTION_ID FROM EWPCV_LEVEL_DESCRIPTION WHERE LEVEL_ID = @LEVEL_ID
			IF @v_level_desc_id is not null
			BEGIN
				DELETE FROM EWPCV_LEVEL_DESCRIPTION WHERE LEVEL_DESCRIPTION_ID = @v_level_desc_id
				DELETE FROM EWPCV_LANGUAGE_ITEM WHERE ID = @v_level_desc_id
			END 
			FETCH NEXT FROM cur INTO @LEVEL_ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO





 /*
    Borra los datos de mobility
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_MOBILITY_ATTACHMENT_LIST'
) DROP PROCEDURE dbo.BORRA_MOBILITY_ATTACHMENT_LIST;
GO 
CREATE PROCEDURE dbo.BORRA_MOBILITY_ATTACHMENT_LIST(@P_TOR_ID varchar(255)) AS
BEGIN
  
	DECLARE @ATTACHMENT_ID varchar(255)
 
	DECLARE cur CURSOR LOCAL FOR
	SELECT ATTACHMENT_ID
      FROM dbo.EWPCV_TOR_ATTACHMENT
      WHERE TOR_ID = @P_TOR_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACHMENT_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_ATTACHMENT @ATTACHMENT_ID

			FETCH NEXT FROM cur INTO @ATTACHMENT_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO







 
 /*********************************/
  /* Inserta un ToR en el sistema.
    Admite un objeto de tipo TOR con toda la informacion del ToR.
    Admite un parámetro de salida con el identificador ToR en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
  

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_TOR'
) DROP PROCEDURE dbo.INSERT_TOR;
GO 
CREATE PROCEDURE dbo.INSERT_TOR(@P_TOR XML, @P_LA_ID varchar(255), @P_TOR_ID varchar(255) output, @P_ERROR_MESSAGE varchar(4000) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output) AS

BEGIN
	DECLARE @v_tor_id varchar(255)
	DECLARE @v_la_id varchar(255)
	DECLARE @v_existe_tor_id varchar(255)
	DECLARE @v_la_revision varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_valida integer
	DECLARE @v_mob_id varchar(255)

	DECLARE @TOR_REPORT_LIST xml
	DECLARE @MOBILITY_ATTACHMENT_LIST xml
	DECLARE @ISCED_TABLE_LIST xml


	SET @return_value = 0

	SELECT 
		@TOR_REPORT_LIST = T.c.query('tor_report_list'),
		@MOBILITY_ATTACHMENT_LIST = T.c.query('mobility_attachment_list'),
		@ISCED_TABLE_LIST = T.c.query('isced_table_list')
	FROM @P_TOR.nodes('tor') T(c)


	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios')
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_existe_tor_id = TOR_ID, @v_la_revision = LEARNING_AGREEMENT_REVISION 
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
				WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = (
					  SELECT MAX(LEARNING_AGREEMENT_REVISION)
					  FROM dbo.EWPCV_LEARNING_AGREEMENT 
					  WHERE ID = @P_LA_ID
					  )

		IF @v_la_id IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El Learning Agreement "' , @P_LA_ID ,'" no existe.')
			SET @return_value = -1
		END
	END 

	IF @return_value = 0
	BEGIN
		IF @v_existe_tor_id IS NOT NULL
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El Learning Agreement ', @P_LA_ID , ' ya tiene asociado el ToR ', @v_existe_tor_id ,', usa el método UPDATE_TOR para actualizarlo.')
			SET @return_value = -1
		END 
	END
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR_INSERT @v_la_id, @P_HEI_TO_NOTIFY, @v_notifier_hei output
		IF @v_notifier_hei is null
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora.')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_NUM_STUDIED_LOI @v_la_id, @v_la_revision, @P_ERROR_MESSAGE output, @v_valida output
		IF @v_valida < 0
			SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_NUM_LOI @TOR_REPORT_LIST, @v_la_id, @v_la_revision, @P_ERROR_MESSAGE output, @v_valida output
		IF @v_valida < 0
			SET @return_value = -1
	END
		   	  
	IF @return_value = 0
	BEGIN
		-- Validamos que vengan informados los campos obligatorios
		EXECUTE dbo.VALIDA_TOR @P_TOR, @P_ERROR_MESSAGE output, @return_value output
	END

	IF @return_value = 0
	BEGIN
		--Validamos la calidad del dato de los campos informados 
		EXECUTE dbo.VALIDA_DATOS_TOR @P_TOR, @P_LA_ID,  @P_ERROR_MESSAGE output, @return_value output
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			--Insertamos el TOR
			EXECUTE dbo.INSERTA_TOR @P_TOR, @P_LA_ID, @v_tor_id output
			-- Insertamos la lista de attachment propios de la movilidad
			EXECUTE dbo.INSERTA_MOBILITY_ATTACHMENT_LIST @MOBILITY_ATTACHMENT_LIST, @v_tor_id
			-- Insertamos la lista de isced_tables
			EXECUTE dbo.INSERTA_ISCED_TABLE_LIST @ISCED_TABLE_LIST, @v_tor_id

			SET @P_TOR_ID = @v_tor_id

			SET @v_mob_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)

			EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_TOR_ID, @v_mob_id, 2, 0)
		
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END

END 
;

GO


 
 
 
 
 
 
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_TOR'
) DROP PROCEDURE dbo.INSERTA_TOR;
GO 
CREATE PROCEDURE dbo.INSERTA_TOR(@P_TOR xml, @P_LA_ID varchar(255), @return_value VARCHAR(255) output) AS
BEGIN
	SET @return_value = NEWID()

	DECLARE @GENERATED_DATE date
	DECLARE @EXTENSION varbinary(max)
	DECLARE @TOR_REPORT_LIST xml

	SELECT 
		@GENERATED_DATE = CAST(T.c.value('generated_date[1]', 'varchar(255)') as date),
		@EXTENSION = T.c.value('extension[1]', 'varbinary(max)'),
		@TOR_REPORT_LIST = T.c.query('tor_report_list')
	FROM @P_TOR.nodes('tor') T(c)



	INSERT INTO dbo.EWPCV_TOR (ID, VERSION, GENERATED_DATE, EXTENSION)
		VALUES (@return_value, 0, @GENERATED_DATE, @EXTENSION)
	UPDATE dbo.EWPCV_LEARNING_AGREEMENT SET TOR_ID = @return_value WHERE ID = @P_LA_ID
	EXECUTE dbo.INSERTA_TOR_REPORT_LIST @TOR_REPORT_LIST, @return_value

END
;
GO


 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_TOR_REPORT_LIST'
) DROP PROCEDURE dbo.INSERTA_TOR_REPORT_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_TOR_REPORT_LIST(@P_TOR_REPORT_LIST xml, @P_TOR_ID varchar(255)) AS
BEGIN
	DECLARE @v_report_id varchar(255)
	DECLARE @v_temp_ref_list xml
	DECLARE @v_attachment_id varchar(255)

	DECLARE @TOR_REPORT xml
	DECLARE @ISSUE_DATE date
	DECLARE @LOI_LIST xml
	DECLARE @TOR_ATTACHMENT_LIST xml
	DECLARE @ATTACHMENT xml
	DECLARE @ATTACH_REFERENCE varchar(255)

	IF @P_TOR_REPORT_LIST is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.query('.')
		FROM @P_TOR_REPORT_LIST.nodes('tor_report_list/tor_report') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @TOR_REPORT
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SELECT 
					@ISSUE_DATE = CAST(T.c.value('issue_date[1]', 'varchar(255)') as date),
					@LOI_LIST = T.c.query('loi_list'),
					@TOR_ATTACHMENT_LIST = T.c.query('tor_attachment_list')
				FROM @TOR_REPORT.nodes('tor_report') T(c)

				EXECUTE dbo.INSERTA_TOR_REPORT @ISSUE_DATE, @P_TOR_ID, @v_report_id output
				EXECUTE dbo.INSERTA_LOI_LIST @LOI_LIST, @TOR_ATTACHMENT_LIST, @v_report_id, @v_temp_ref_list output

				IF @v_temp_ref_list is not null and @TOR_ATTACHMENT_LIST is not null
				BEGIN
					DECLARE cur2 CURSOR LOCAL FOR
					SELECT 
						T.c.value('attach_reference[1]', 'varchar(255)'),
						T.c.query('.')
					FROM @TOR_ATTACHMENT_LIST.nodes('tor_attachment_list/attachment') T(c)
					OPEN cur2
					FETCH NEXT FROM CUR2 INTO @ATTACH_REFERENCE, @ATTACHMENT
					WHILE @@FETCH_STATUS = 0
						BEGIN
							IF dbo.EXISTE_REF_ATTACHMENT(@ATTACH_REFERENCE, @v_temp_ref_list) < 0
							BEGIN
								EXECUTE dbo.INSERTA_ATTACHMENT @ATTACHMENT, @v_attachment_id output
								INSERT INTO dbo.EWPCV_REPORT_ATT(ATTACHMENT_ID, REPORT_ID) VALUES (@v_attachment_id, @v_report_id)
							END

							FETCH NEXT FROM cur2 INTO @ATTACH_REFERENCE, @ATTACHMENT
						END
					CLOSE cur2
					DEALLOCATE cur2
				END 

				FETCH NEXT FROM cur INTO @TOR_REPORT
			END
		CLOSE cur
		DEALLOCATE cur
	END
END 
;

GO


 


 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ATTACHMENT'
) DROP PROCEDURE dbo.INSERTA_ATTACHMENT;
GO 
CREATE PROCEDURE dbo.INSERTA_ATTACHMENT(@P_ATTACHMENT xml, @return_value varchar(255) output) AS 
BEGIN
	DECLARE @v_id VARCHAR(255)
	DECLARE @v_li_id VARCHAR(255)
	DECLARE @v_c_id VARCHAR(255)

	DECLARE @TITLE xml
	DECLARE @ATT_TYPE integer
	DECLARE @DESCRIPTION xml
	DECLARE @CONTENT xml
	DECLARE @EXTENSION varbinary(max)
	DECLARE @LANGITEM xml
	DECLARE @BLOBITEM xml
	DECLARE @LANG varchar(255)
	DECLARE @BLOB_CONTENT varbinary(max)

	SET @v_id = NEWID()

	SELECT
		@TITLE = T.c.query('title'),
		@ATT_TYPE = T.c.value('att_type[1]', 'integer'),
		@DESCRIPTION = T.c.query('description'),
		@CONTENT = T.c.query('content'),
		@EXTENSION = T.c.value('extension[1]', 'varbinary(max)')
	FROM @P_ATTACHMENT.nodes('attachment') T(c)

	-- Nuevo attachment
	INSERT INTO dbo.EWPCV_ATTACHMENT (ID, VERSION, ATTACH_TYPE, EXTENSION) 
		VALUES (@v_id, 0, @ATT_TYPE, @EXTENSION)
	SET @return_value = @v_id 
	
	-- Titulo multiidioma
	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @TITLE.nodes('title/text') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_li_id output
			INSERT INTO dbo.EWPCV_ATTACHMENT_TITLE (ATTACHMENT_ID , ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

	-- Descripcion multiidioma
	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_li_id output
			INSERT INTO dbo.EWPCV_ATTACHMENT_DESCRIPTION (ATTACHMENT_ID , ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

	--Contenido (blob)
	DECLARE cur CURSOR LOCAL FOR
	SELECT
		T.c.query('.')
	FROM @CONTENT.nodes('content/blob_content') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @BLOBITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @v_c_id = NEWID()

			SET @LANG = @BLOBITEM.value('(/blob_content/@lang)[1]','varchar(255)')
			SET @BLOB_CONTENT = @BLOBITEM.value('(/blob_content)[1]','varbinary(max)')
			INSERT INTO dbo.EWPCV_ATTACHMENT_CONTENT (ATTACHMENT_ID , ID, LANG, CONTENT) 
				VALUES (@v_id, @v_c_id, @LANG, @BLOB_CONTENT)

			FETCH NEXT FROM cur INTO @BLOBITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


 
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_TOR_REPORT'
) DROP PROCEDURE dbo.INSERTA_TOR_REPORT;
GO 
CREATE PROCEDURE dbo.INSERTA_TOR_REPORT(@P_ISSUE_DATE date, @P_TOR_ID varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_report_id varchar(255)
	SET @v_report_id = NEWID()

	INSERT INTO dbo.EWPCV_REPORT (ID, VERSION, ISSUE_DATE, TOR_ID) 
		VALUES (@v_report_id, 0, @P_ISSUE_DATE, @P_TOR_ID)

	SET @return_value = @v_report_id

END 
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOI_LIST'
) DROP PROCEDURE dbo.INSERTA_LOI_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_LOI_LIST(@P_LOI_LIST xml, @P_TOR_ATTACHMENT_LIST xml, @P_REPORT_ID varchar(255), @P_TEMP_REF_LIST xml output) AS
BEGIN
	DECLARE @LOI xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_LOI_LIST.nodes('loi_list/loi') T(c)
	OPEN cur
	FETCH NEXT FROM cur into @LOI
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LOI @LOI, @P_TOR_ATTACHMENT_LIST, @P_REPORT_ID, @P_TEMP_REF_LIST
			FETCH NEXT FROM cur into @LOI
		END
	CLOSE cur
	DEALLOCATE cur
	
END 
;
GO


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_LOI'
) DROP PROCEDURE dbo.INSERTA_LOI;
GO 
CREATE PROCEDURE dbo.INSERTA_LOI(@P_LOI xml, @P_TOR_ATTACHMENT_LIST xml, @P_REPORT_ID varchar(255), @P_TEMP_REF_LIST xml output) AS
BEGIN

	DECLARE @v_loi_id varchar(255)
	DECLARE @v_los_id varchar(255)
	DECLARE @v_los_code varchar(255)
	DECLARE @v_result_distribution_id varchar(255)
	DECLARE @v_diploma_id varchar(255)
	DECLARE @v_count_los_loi integer
	DECLARE @v_attachment_id varchar(255)
	DECLARE @temp_attach_ref xml
	DECLARE @tag_attachment_ref xml
	DECLARE @tag_attachment_id xml

	DECLARE @LOI_ID varchar(255)
	DECLARE @LOS_ID varchar(255)
	DECLARE @LOS_CODE varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @STATUS integer
	DECLARE @GRADING_SCHEME_ID  varchar(255)
	DECLARE @RESULT_LABEL varchar(255)
	DECLARE @PERCENTAGE_LOWER integer
	DECLARE @PERCENTAGE_EQUAL integer
	DECLARE @PERCENTAGE_HIGHER integer
	DECLARE @RESULT_DISTRIBUTION xml
	DECLARE @EDUCATION_LEVEL xml
	DECLARE @LANGUAGE_OF_INSTRUCTION varchar(255)
	DECLARE @ENGAGEMENT_HOURS integer
	DECLARE @ATTACHMENTS_REF_LIST xml
	DECLARE @GROUP_TYPE_ID varchar(255)
	DECLARE @GROUP_ID  varchar(255)
	DECLARE @DIPLOMA xml
	DECLARE @EXTENSION varbinary(max)
	DECLARE @ATTACHMENTS_REF varchar(255)
	DECLARE @ATTACHMENT xml
	DECLARE @ATTACH_REFERENCE varchar(255)

	SELECT 
		@LOI_ID = T.c.value('loi_id[1]', 'varchar(255)'),
		@LOS_ID = T.c.value('los_id[1]', 'varchar(255)'),
		@LOS_CODE = T.c.value('los_code[1]', 'varchar(255)'),
		@START_DATE = CAST(T.c.value('start_date[1]', 'varchar(255)') as date),
		@END_DATE = CAST(T.c.value('end_date[1]', 'varchar(255)') as date),
		@STATUS = T.c.value('status[1]', 'integer'),
		@GRADING_SCHEME_ID = T.c.value('grading_scheme_id[1]', 'varchar(255)'),
		@RESULT_LABEL = T.c.value('result_label[1]', 'varchar(255)'),
		@PERCENTAGE_LOWER = T.c.value('percentage_lower[1]', 'integer'),
		@PERCENTAGE_EQUAL = T.c.value('percentage_equal[1]', 'integer'),
		@PERCENTAGE_HIGHER = T.c.value('percentage_higher[1]', 'integer'),
		@RESULT_DISTRIBUTION = T.c.query('result_distribution'),
		@EDUCATION_LEVEL = T.c.query('education_level'),
		@LANGUAGE_OF_INSTRUCTION = T.c.value('language_of_instruction[1]', 'varchar(255)'),
		@ENGAGEMENT_HOURS = T.c.value('engagement_hours[1]', 'integer'),
		@ATTACHMENTS_REF_LIST = T.c.query('attachments_ref_list'),
		@GROUP_TYPE_ID = T.c.value('group_type_id[1]', 'varchar(255)'),
		@GROUP_ID = T.c.value('group_id[1]', 'varchar(255)'),
		@DIPLOMA = T.c.query('diploma'),
		@EXTENSION = T.c.value('extension[1]', 'varbinary(max)')
	FROM @P_LOI.nodes('loi') T(c)


	IF @LOI_ID is not null
		-- Obtener el LOS_ID a partir del LOI_ID
		SELECT @v_los_id = LOS_ID FROM dbo.EWPCV_LA_COMPONENT WHERE LOI_ID = @LOI_ID

	IF @LOI_ID is null and @LOS_CODE is not null
	BEGIN
		-- Obtener el LOI_ID y LOS_ID a partir del LOS_CODE
		SELECT @v_loi_id = LOI_ID FROM EWPCV_LA_COMPONENT WHERE LOS_CODE = @LOS_CODE 
		SELECT @v_los_id = LOS_ID FROM dbo.EWPCV_LA_COMPONENT WHERE LOS_CODE = @LOS_CODE

		-- Obtener el LOS_ID a partir del LOS_CODE de la tabla LOS
		IF @v_los_id is null
			SELECT @v_los_id = ID FROM EWPCV_LOS WHERE LOS_CODE = @LOS_CODE
	END 

	IF @LOI_ID is null and @LOS_CODE is null
	BEGIN
		-- Obtener el loi_id a partir del LOS_ID
		SELECT @v_loi_id = LOI_ID FROM dbo.EWPCV_LA_COMPONENT WHERE LOS_ID = @LOS_ID
	END

	EXECUTE dbo.INSERTA_RESULT_DISTRIBUTION @RESULT_DISTRIBUTION, @v_result_distribution_id output
	EXECUTE dbo.INSERTA_DIPLOMA @DIPLOMA, @v_diploma_id output

	UPDATE EWPCV_LOI SET 
		ENGAGEMENT_HOURS = @ENGAGEMENT_HOURS, 
		LANGUAGE_OF_INSTRUCTION = @LANGUAGE_OF_INSTRUCTION, 
		GRADING_SCHEME_ID = @GRADING_SCHEME_ID, 
		RESULT_DISTRIBUTION = @v_result_distribution_id, 
		START_DATE = @START_DATE, 
		END_DATE = @END_DATE, 
		PERCENTAGE_LOWER = @PERCENTAGE_LOWER, 
		PERCENTAGE_EQUAL = @PERCENTAGE_EQUAL, 
		PERCENTAGE_HIGHER = @PERCENTAGE_HIGHER, 
		RESULT_LABEL = @RESULT_LABEL, 
		STATUS = @STATUS, 
		DIPLOMA_ID = @v_diploma_id, 
		REPORT_ID = @P_REPORT_ID, 
		EXTENSION = @EXTENSION
		WHERE ID = @v_loi_id


		SELECT @v_count_los_loi = COUNT(1) FROM dbo.EWPCV_LOS_LOI WHERE LOI_ID = @v_loi_id AND LOS_ID = @v_los_id
		IF @v_count_los_loi = 0 
			INSERT INTO dbo.EWPCV_LOS_LOI(LOI_ID, LOS_ID) VALUES (@v_loi_id, @v_los_id)

		IF @GROUP_ID IS NOT NULL OR @GROUP_TYPE_ID IS NOT NULL 
		BEGIN
			INSERT INTO dbo.EWPCV_LOI_GROUPING(LOI_ID, GROUP_ID, GROUP_TYPE_ID)
				VALUES (@v_loi_id, @GROUP_ID, @GROUP_TYPE_ID)
		END 
		EXECUTE dbo.INSERTA_EDUCATION_LEVEL_LIST @EDUCATION_LEVEL, @v_loi_id
		

	-- Buscar para cada VALOR de attachments_ref_list/attachments_ref si éste es uno de los attach_reference de los attachment de @P_TOR_ATTACHMENT_LIST
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('.[1]', 'varchar(255)')
	FROM @ATTACHMENTS_REF_LIST.nodes('attachments_ref_list/attachments_ref') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACHMENTS_REF

	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			DECLARE cur2 CURSOR LOCAL FOR
			SELECT 
				T.c.query('.'),
				T.c.value('attach_reference[1]', 'varchar(255)')
			FROM @P_TOR_ATTACHMENT_LIST.nodes('tor_attachment_list/attachment') T(c)
			OPEN cur2
			FETCH NEXT FROM cur2 INTO @ATTACHMENT, @ATTACH_REFERENCE
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF LOWER(@ATTACH_REFERENCE) = LOWER(@ATTACHMENTS_REF)
				BEGIN
					IF dbo.EXISTE_REF_ATTACHMENT(@ATTACHMENTS_REF, @P_TEMP_REF_LIST)<0
					BEGIN
						-- No se ha usado la referencia anteriormente
						EXECUTE dbo.INSERTA_ATTACHMENT @ATTACHMENT, @v_attachment_id output
						INSERT INTO EWPCV_LOI_ATT(ATTACHMENT_ID, LOI_ID) VALUES (@v_attachment_id, @v_loi_id)
						
						-- Añadir a @P_TEMP_REF_LIST la pareja de valores referencia e id
						-- Equivalente a llamada INSERTA_LISTA_TEMP en version Oracle
						IF @P_TEMP_REF_LIST is null
						BEGIN
							SET @P_TEMP_REF_LIST = '<temp_attach_ref_list></temp_attach_ref_list>'
						END
						
						SET @temp_attach_ref = '<temp_attach_ref></temp_attach_ref>'
						SET @tag_attachment_ref = (SELECT @ATTACH_REFERENCE FOR XML PATH('attach_reference'))
						SET @tag_attachment_id = (SELECT @v_attachment_id FOR XML PATH('attachment_id'))

						SET @temp_attach_ref.modify('         
							insert sql:variable("@tag_attachment_ref")         
							as last into (/temp_attach_ref[1])         
							') 
						SET @temp_attach_ref.modify('         
							insert sql:variable("@tag_attachment_id")         
							as last into (/temp_attach_ref[1])         
							') 
						SET @P_TEMP_REF_LIST.modify('         
							insert sql:variable("@temp_attach_ref")         
							as last into (/temp_attach_ref_list[1])         
							') 

					END
					ELSE
					BEGIN
						-- Ya se ha usado la referencia
						-- Recuperar el id a partir de la referencia en el xml de parejas referencia-id
						SET @v_attachment_id = null

						SELECT
							T.c.value('attachment_id[1]', 'varchar(255)')
						FROM @P_TEMP_REF_LIST.nodes('temp_attach_ref_list/temp_attach_ref') T(c)
						WHERE LOWER(T.c.value('attach_reference[1]', 'varchar(255)')) = LOWER(@ATTACH_REFERENCE)

						IF @v_attachment_id is not null
						BEGIN
							INSERT INTO dbo.EWPCV_LOI_ATT(ATTACHMENT_ID, LOI_ID) VALUES (@v_attachment_id, @v_loi_id)
						END
					END
				END
				FETCH NEXT FROM cur2 INTO @ATTACHMENT, @ATTACH_REFERENCE
			END
			CLOSE cur2
			DEALLOCATE cur2

			FETCH NEXT FROM cur INTO @ATTACHMENTS_REF
		END 
	CLOSE cur
	DEALLOCATE cur

END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_RESULT_DISTRIBUTION'
) DROP PROCEDURE dbo.INSERTA_RESULT_DISTRIBUTION;
GO 
CREATE PROCEDURE dbo.INSERTA_RESULT_DISTRIBUTION(@P_RESULT_DISTRIBUTION xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_li_id varchar(255)

	DECLARE @DIST_CATEGORIES_LIST xml
	DECLARE @DESCRIPTION xml
	DECLARE @LANGITEM xml

	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_RESULT_DISTRIBUTION (ID, VERSION) VALUES (@v_id, 0)

	SELECT 
		@DIST_CATEGORIES_LIST = T.c.query('dist_categories_list'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_RESULT_DISTRIBUTION.nodes('result_distribution')  T(c)

	EXECUTE dbo.INSERTA_DIST_CATEGORIES_LIST @DIST_CATEGORIES_LIST, @v_id

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text')  T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_li_id output
			INSERT INTO dbo.EWPCV_DISTR_DESCRIPTION (RESULT_DISTRIBUTION_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @LANGITEM
		END 
	CLOSE cur
	DEALLOCATE cur

	SET @return_value = @v_id

END
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_DIST_CATEGORIES_LIST'
) DROP PROCEDURE dbo.INSERTA_DIST_CATEGORIES_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_DIST_CATEGORIES_LIST(@P_DIST_CATEGORIES_LIST xml, @P_RESULT_DIST_ID varchar(255)) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_li_id varchar(255)

	DECLARE @DIST_CATEGORIES xml
	DECLARE @LABEL xml
	DECLARE @DIST_CAT_COUNT integer

	IF @P_DIST_CATEGORIES_LIST is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.query('.')
		FROM @P_DIST_CATEGORIES_LIST.nodes('dist_categories_list/dist_categories') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @DIST_CATEGORIES
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			SELECT
				@LABEL = T.c.query('label/text'),
				@DIST_CAT_COUNT = T.c.value('dist_cat_count[1]', 'integer')
			FROM @DIST_CATEGORIES.nodes('dist_categories') T(c)

			SET @v_id = NEWID()
			IF @LABEL is not null
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LABEL, @v_li_id output
			ELSE
				SET @v_li_id = null

			INSERT INTO dbo.EWPCV_RESULT_DIST_CATEGORY (ID, VERSION, DISTRUBTION_COUNT, LABEL, RESULT_DIST_ID)
				VALUES(@v_id, 0, @DIST_CAT_COUNT, @v_li_id, @P_RESULT_DIST_ID)

			FETCH NEXT FROM cur INTO @DIST_CATEGORIES
		END 
		CLOSE cur
		DEALLOCATE cur

	END
END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_DIPLOMA'
) DROP PROCEDURE dbo.INSERTA_DIPLOMA;
GO 
CREATE PROCEDURE dbo.INSERTA_DIPLOMA(@P_DIPLOMA XML, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @version integer
	DECLARE @INTRODUCTION varbinary(max)
	DECLARE @ISSUE_DATE date
	DECLARE @SIGNATURE varbinary(max)
	DECLARE @SECTION_LIST xml

	SET @v_id = NEWID()

	SELECT 
		@version = T.c.value('version[1]', 'integer'),
		@INTRODUCTION = T.c.value('introduction[1]', 'varbinary(max)'),
		@ISSUE_DATE = CAST(T.c.value('issue_date[1]', 'varchar(255)') as date),
		@SIGNATURE = T.c.value('signature[1]', 'varbinary(max)'),
		@SECTION_LIST = T.c.query('section_list')
	FROM @P_DIPLOMA.nodes('diploma') T(c)

	INSERT INTO dbo.EWPCV_DIPLOMA (ID, VERSION, DIPLOMA_VERSION, ISSUE_DATE, INTRODUCTION, SIGNATURE)
		VALUES(@v_id, 0, @VERSION, @ISSUE_DATE, @INTRODUCTION, @SIGNATURE)

    EXECUTE dbo.INSERTA_DIPLOMA_SECTION_LIST @SECTION_LIST, @v_id, NULL, 0
	   	 
	SET @return_value = @v_id
END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_DIPLOMA_SECTION_LIST'
) DROP PROCEDURE dbo.INSERTA_DIPLOMA_SECTION_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_DIPLOMA_SECTION_LIST(@P_SECTION_LIST xml, @P_DIPLOMA_ID varchar(255), @P_SECTION_ID varchar(255), @P_RECURSIVIDAD integer) AS
BEGIN
	-- Numero maximo de llamadas recursivas a INSERTA_DIPLOMA_SECTION_LIST
	DECLARE @MAX_RECURSIVIDAD integer = 3

	DECLARE @v_s_id VARCHAR(255)
	DECLARE @v_RECURSIVIDAD integer

	DECLARE @DIPLOMA_SECTION xml
	DECLARE @TITLE varchar(255)
	DECLARE @CONTENT varbinary(max)
	DECLARE @SECTION_NUMBER int
	DECLARE @ADDITIONAL_INF_LIST xml
	DECLARE @SECTION_ATTACHMENT_LIST xml
	DECLARE @SECTION_LIST xml

	IF @P_RECURSIVIDAD is not null
		SET @v_RECURSIVIDAD = @P_RECURSIVIDAD + 1
	ELSE
		SET @v_RECURSIVIDAD = 1

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_SECTION_LIST.nodes('section_list/diploma_section') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @DIPLOMA_SECTION
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT 
				@TITLE = T.c.value('title[1]', 'varchar(255)'),
				@CONTENT = T.c.value('content[1]', 'varbinary(max)'),
				@SECTION_NUMBER = T.c.value('section_number[1]', 'integer'),
				@ADDITIONAL_INF_LIST = T.c.query('additional_inf_list'),
				@SECTION_ATTACHMENT_LIST = T.c.query('section_attachment_list'),
				@SECTION_LIST = T.c.query('section_list')
			FROM @DIPLOMA_SECTION.nodes('diploma_section') T(c)
			
			SET @v_s_id = NEWID()
			
			INSERT INTO dbo.EWPCV_DIPLOMA_SECTION (ID, VERSION, TITLE, CONTENT, SECTION_NUMBER, DIPLOMA_ID)
				VALUES (@v_s_id, 0, @TITLE, @CONTENT, @SECTION_NUMBER , @P_DIPLOMA_ID)

			EXECUTE dbo.INSERTA_ADDITIONAL_INF_LIST @ADDITIONAL_INF_LIST, @v_s_id
			EXECUTE dbo.INSERTA_SECTION_ATTACHMENT_LIST @SECTION_ATTACHMENT_LIST, @v_s_id		
			
			IF (@SECTION_LIST is not null and LEN(CONVERT(varchar(max), @SECTION_LIST)) > 0) AND @v_RECURSIVIDAD < @MAX_RECURSIVIDAD
				EXECUTE dbo.INSERTA_DIPLOMA_SECTION_LIST @SECTION_LIST, @P_DIPLOMA_ID, v_s_id, @v_RECURSIVIDAD
		
			FETCH NEXT FROM cur INTO @DIPLOMA_SECTION
		END
	CLOSE cur
	DEALLOCATE cur


END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ADDITIONAL_INF_LIST'
) DROP PROCEDURE dbo.INSERTA_ADDITIONAL_INF_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_ADDITIONAL_INF_LIST(@P_ADDITIONAL_INF_LIST xml, @P_DIPLOMA_SECTION_ID varchar(255)) AS
BEGIN
    DECLARE @v_id varchar(255)
	DECLARE @ADDITIONAL_INF varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('.[1]', 'varchar(255)')
	FROM @P_ADDITIONAL_INF_LIST.nodes('additional_inf_list/additional_inf') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ADDITIONAL_INF
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_ADDITIONAL_INFO (ID, VERSION, SECTION_ID, ADDITIONAL_INFO)
    			VALUES (@v_id, 0, @P_DIPLOMA_SECTION_ID, @ADDITIONAL_INF)

			FETCH NEXT FROM cur INTO @ADDITIONAL_INF
		END

	CLOSE cur
	DEALLOCATE cur 


END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_SECTION_ATTACHMENT_LIST'
) DROP PROCEDURE dbo.INSERTA_SECTION_ATTACHMENT_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_SECTION_ATTACHMENT_LIST(@P_SECTION_ATTACHMENT_LIST xml, @P_SECTION_ID varchar(255)) AS
BEGIN
	DECLARE @v_id varchar(255)

	DECLARE @ATTACHMENT xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_SECTION_ATTACHMENT_LIST.nodes('section_attachment_list/attachment') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACHMENT
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_ATTACHMENT @ATTACHMENT, @v_id output
			INSERT INTO dbo.EWPCV_DIPLOMA_SEC_ATTACH (ATTACHMENT_ID, SECTION_ID) VALUES (@v_id, @P_SECTION_ID)

			FETCH NEXT FROM cur INTO @ATTACHMENT
		END
	CLOSE cur
	DEALLOCATE cur

END 
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_EDUCATION_LEVEL_LIST'
) DROP PROCEDURE dbo.INSERTA_EDUCATION_LEVEL_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_EDUCATION_LEVEL_LIST(@P_EDUCATION_LEVEL_LIST xml, @P_LOI_ID varchar(255)) AS
BEGIN
	DECLARE @v_el_id varchar(255)
	DECLARE @EDUCATION_LEVEL xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_EDUCATION_LEVEL_LIST.nodes('education_level_list/education_level') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @EDUCATION_LEVEL
	WHILE @@FETCH_STATUS = 0
		BEGIN
			 EXECUTE dbo.INSERTA_EDUCATION_LEVEL @EDUCATION_LEVEL, @v_el_id output
			 INSERT INTO dbo.EWPCV_LEVELS (LOI_ID, LEVEL_ID) VALUES (@P_LOI_ID, @v_el_id)

			FETCH NEXT FROM cur INTO @EDUCATION_LEVEL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_EDUCATION_LEVEL'
) DROP PROCEDURE dbo.INSERTA_EDUCATION_LEVEL;
GO 
CREATE PROCEDURE dbo.INSERTA_EDUCATION_LEVEL(@P_EDUCATION_LEVEL xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @v_li_id varchar(255)
	DECLARE @LEVEL_TYPE varchar(255)
	DECLARE @LEVEL_VALUE varchar(255)
	DECLARE @DESCRIPTION xml
	DECLARE @LANGITEM xml

	SELECT 
		@LEVEL_TYPE = T.c.value('level_type[1]', 'varchar(255)'),
		@LEVEL_VALUE = T.c.value('level_value[1]', 'varchar(255)'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_EDUCATION_LEVEL.nodes('education_level') T(c)

	SET @v_id = NEWID()
	INSERT INTO dbo.EWPCV_LEVEL (ID, VERSION, LEVEL_TYPE, LEVEL_VALUE) 
		VALUES (@v_id, 0, @LEVEL_TYPE, @LEVEL_VALUE)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @LANGITEM
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @LANGITEM, @v_li_id output
			INSERT INTO dbo.EWPCV_LEVEL_DESCRIPTION (LEVEL_ID, LEVEL_DESCRIPTION_ID) VALUES (@v_id, @v_li_id)

			FETCH NEXT FROM cur INTO @LANGITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_ATTACHMENT_LIST'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_ATTACHMENT_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_MOBILITY_ATTACHMENT_LIST(@P_MOBILITY_ATTACHMENT_LIST xml, @P_TOR_ID varchar(255) output) AS

BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @ATTACHMENT xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_MOBILITY_ATTACHMENT_LIST.nodes('mobility_attachment_list/attachment') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACHMENT
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_ATTACHMENT @ATTACHMENT, @v_id output
			INSERT INTO dbo.EWPCV_TOR_ATTACHMENT(ATTACHMENT_ID, TOR_ID) VALUES (@v_id, @P_TOR_ID)

			FETCH NEXT FROM cur INTO @ATTACHMENT
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ISCED_TABLE_LIST'
) DROP PROCEDURE dbo.INSERTA_ISCED_TABLE_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_ISCED_TABLE_LIST(@P_ISCED_TABLE_LIST xml, @P_TOR_ID varchar(255) output) AS

BEGIN
	DECLARE @ISCED_TABLE xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_ISCED_TABLE_LIST.nodes('isced_table_list/isced_table') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @ISCED_TABLE
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_ISCED_TABLE @ISCED_TABLE, @P_TOR_ID

			FETCH NEXT FROM cur INTO @ISCED_TABLE
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ISCED_TABLE'
) DROP PROCEDURE dbo.INSERTA_ISCED_TABLE;
GO 
CREATE PROCEDURE dbo.INSERTA_ISCED_TABLE(@P_ISCED_TABLE xml, @P_TOR_ID varchar(255) output) AS

BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @ISCED_CODE varchar(255)
	DECLARE @GRADE_FREQUENCY_LIST xml
	
	SELECT 
		@ISCED_CODE = T.c.value('isced_code[1]', 'varchar(255)'),
		@GRADE_FREQUENCY_LIST = T.c.query('grade_frequency_list')
	FROM @P_ISCED_TABLE.nodes('isced_table') T(c)
	
	SET @v_id = NEWID()

    INSERT INTO dbo.EWPCV_ISCED_TABLE (ID, ISCED_CODE, TOR_ID)
		VALUES (@v_id, @ISCED_CODE, @P_TOR_ID)

    EXECUTE dbo.INSERTA_GRADE_FREQUENCY_LIST @GRADE_FREQUENCY_LIST, @v_id

END
;
GO





IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_GRADE_FREQUENCY_LIST'
) DROP PROCEDURE dbo.INSERTA_GRADE_FREQUENCY_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_GRADE_FREQUENCY_LIST(@P_GRADE_FREQUENCY_LIST xml, @P_ISCED_TABLE_ID varchar(255)) AS
BEGIN
	DECLARE @GRADE_FREQUENCY xml

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_GRADE_FREQUENCY_LIST.nodes('grade_frequency_list/grade_frequency') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @GRADE_FREQUENCY
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_GRADE_FREQUENCY @GRADE_FREQUENCY, @P_ISCED_TABLE_ID
			FETCH NEXT FROM cur INTO @GRADE_FREQUENCY
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_GRADE_FREQUENCY'
) DROP PROCEDURE dbo.INSERTA_GRADE_FREQUENCY;
GO 
CREATE PROCEDURE dbo.INSERTA_GRADE_FREQUENCY(@P_GRADE_FREQUENCY xml, @P_ISCED_TABLE_ID varchar(255)) AS
BEGIN
	DECLARE @v_id varchar(255)
	DECLARE @LABEL varchar(255)
	DECLARE @PERCENTAGE integer

	SET @v_id = NEWID()

	SELECT 
		@LABEL = T.c.value('label[1]', 'varchar(255)'),
		@PERCENTAGE = T.c.value('percentage[1]', 'integer')
	FROM @P_GRADE_FREQUENCY.nodes('grade_frequency') T(c) 

  	INSERT INTO dbo.EWPCV_GRADE_FREQUENCY (ID, LABEL, PERCENTAGE, ISCED_TABLE_ID)
		VALUES (@v_id, @LABEL, @PERCENTAGE, @P_ISCED_TABLE_ID)

END
;
GO



 
 

 
 
 
 
 
 
  /* Actualiza un TOR
    Requeire un id de TOR con el ToR a actualizar.
    Admite un objeto de tipo TOR con toda la informacion del ToR.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Retorna un codigo de ejecucion 0 ok <0 si error.
     
  */




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_TOR'
) DROP PROCEDURE dbo.UPDATE_TOR;
GO 
CREATE PROCEDURE dbo.UPDATE_TOR(@P_TOR_ID varchar(255), @P_TOR xml, @P_ERROR_MESSAGE VARCHAR(4000) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output) AS
 BEGIN
	DECLARE @v_total integer
	DECLARE @v_tor_id varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_la_id varchar(255)
	DECLARE @v_la_revision int
	DECLARE @v_valida integer
	DECLARE @v_mob_id varchar(255)
	
	DECLARE @TOR_REPORT_LIST xml

	SET @return_value = 0

	SELECT 
		@TOR_REPORT_LIST = T.c.query('tor_report_list')
	FROM @P_TOR.nodes('tor') T(c)

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios.')
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_total = COUNT(1) FROM dbo.EWPCV_TOR WHERE ID = @P_TOR_ID

		IF @v_total = 0
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El ToR "' , @P_TOR_ID , '" no existe.')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR @P_TOR_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output

		IF @v_notifier_hei is null
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = ID, @v_la_revision = LEARNING_AGREEMENT_REVISION FROM dbo.EWPCV_LEARNING_AGREEMENT 
		WHERE TOR_ID = @P_TOR_ID AND LEARNING_AGREEMENT_REVISION = (
			SELECT MAX(LEARNING_AGREEMENT_REVISION)
			FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE TOR_ID = @P_TOR_ID
			)

		EXECUTE dbo.VALIDA_NUM_STUDIED_LOI @v_la_id, @v_la_revision, @P_ERROR_MESSAGE output, @v_valida output
		IF @v_valida < 0
			SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_NUM_LOI @TOR_REPORT_LIST, @v_la_id, @v_la_revision, @P_ERROR_MESSAGE output, @v_valida output
		IF @v_valida < 0
			SET @return_value = -1
	END
	
	IF @return_value = 0
	BEGIN
		-- Validamos que vengan informados los campos obligatorios
		EXECUTE dbo.VALIDA_TOR @P_TOR, @P_ERROR_MESSAGE output, @return_value output
	END

	IF @return_value = 0
	BEGIN
		--Validamos la calidad del dato de los campos informados
		EXECUTE dbo.VALIDA_DATOS_TOR @P_TOR, @v_la_id, @P_ERROR_MESSAGE output, @return_value output
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			--Actualizamos el TOR
			EXECUTE dbo.ACTUALIZA_TOR @P_TOR_ID, @P_TOR

			SET @v_mob_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)

			EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_TOR_ID, @v_mob_id, 2, 1)
		
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
	
 END
 ;
GO





















  
  
/* VERSION SIMPLE TOR (BASIC) */



IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_SIMPLE_TOR' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_SIMPLE_TOR;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_SIMPLE_TOR
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="HTTP">
		<xs:restriction base="xs:anyURI">
			<xs:pattern value="https?://.+"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="HTTPS">
		<xs:restriction base="xs:anyURI">
			<xs:pattern value="https://.+" />
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="ceroOneType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>


	<xs:complexType name="blobContentListType">
		<xs:sequence>
			<xs:element name="blob_content" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="attachmentType">
		<xs:sequence>
			<xs:element name="attach_reference" type="xs:string" />
			<xs:element name="title" type="languageItemListType" />
			<xs:element name="att_type">
				<xs:simpleType>
					<xs:restriction base="xs:integer">
						<xs:enumeration value="0"/>
						<xs:enumeration value="1"/>
						<xs:enumeration value="2"/>
						<xs:enumeration value="3"/>
						<xs:enumeration value="4"/>
						<xs:enumeration value="5"/>
						<xs:enumeration value="6"/>
						<xs:enumeration value="7"/>
						<xs:enumeration value="8"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="description" type="languageItemListType" />
			<xs:element name="content" type="blobContentListType" />
			<xs:element name="extension" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemType">
		<xs:sequence>
			<xs:element name="text">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="loiStatusType">
		<xs:restriction base="xs:string">
			<xs:enumeration value="PASSED"/>
			<xs:enumeration value="FAILED"/>
			<xs:enumeration value="IN-PROGRESS"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="educationLevelType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="sectionListType">
		<xs:sequence>
			<xs:element maxOccurs="unbounded" name="diploma_section">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="title" type="languageItemType" />
						<xs:element name="content" type="xs:string" />
						<xs:element name="additional_inf_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="additional_inf" type="xs:string" />
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="section_attachment_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="attachment" type="attachmentType" />
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="section_number" type="xs:integer" />
						<xs:element name="section_list" type="sectionListType" minOccurs="0" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="simple_tor">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="generated_date" type="xs:date" />
				<xs:element name="simple_tor_report_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element maxOccurs="unbounded" name="simple_tor_report">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="issue_date" type="xs:date" />
										<xs:element name="tor_attachment_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" name="attachment" type="attachmentType" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="extension" type="xs:string" minOccurs="0" />
			</xs:sequence>			
		</xs:complexType>
	</xs:element>

</xs:schema>';
GO
;

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_SIMPLE_TOR'
) DROP PROCEDURE dbo.VALIDA_SIMPLE_TOR;
GO 
CREATE PROCEDURE dbo.VALIDA_SIMPLE_TOR(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_SIMPLE_TOR)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO
;

-- Renombrado de la funcion de validacion por coherencia con version Oracle

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_BASIC_TOR'
) DROP PROCEDURE dbo.VALIDA_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.VALIDA_BASIC_TOR(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
	EXECUTE dbo.VALIDA_SIMPLE_TOR @XML, @errMsg output, @errNum output
END
;
GO
;




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_BASIC_TOR_ATTACH'
) DROP PROCEDURE dbo.BORRA_BASIC_TOR_ATTACH;
GO 
CREATE PROCEDURE dbo.BORRA_BASIC_TOR_ATTACH(@P_TOR_ID varchar(255)) AS
BEGIN
	DECLARE @ATTACHMENT_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT rep_att.ATTACHMENT_ID 
        FROM dbo.EWPCV_REPORT repo 
        INNER JOIN dbo.EWPCV_REPORT_ATT rep_att ON repo.ID = rep_att.REPORT_ID 
        WHERE repo.TOR_ID = @P_TOR_ID 
	OPEN cur
	FETCH NEXT FROM cur INTO @ATTACHMENT_ID
	WHILE @@FETCH_STATUS = 0 
		BEGIN
			DELETE FROM dbo.EWPCV_REPORT_ATT WHERE ATTACHMENT_ID = @ATTACHMENT_ID
			EXECUTE dbo.BORRA_ATTACHMENT @ATTACHMENT_ID
			FETCH NEXT FROM cur INTO @ATTACHMENT_ID
		END
	CLOSE cur
	DELETE FROM dbo.EWPCV_REPORT WHERE TOR_ID = @P_TOR_ID
	DEALLOCATE cur
END
; 
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_BASIC_TOR'
) DROP PROCEDURE dbo.BORRA_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.BORRA_BASIC_TOR(@P_TOR_ID varchar(255)) AS
BEGIN
	EXECUTE dbo.BORRA_BASIC_TOR_ATTACH @P_TOR_ID
	UPDATE dbo.EWPCV_LEARNING_AGREEMENT SET TOR_ID = NULL WHERE LOWER(TOR_ID) = LOWER(@P_TOR_ID)
	DELETE FROM dbo.EWPCV_TOR WHERE LOWER(ID) = LOWER(@P_TOR_ID)
END
;
GO



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_BASIC_TOR_REPORT_LIST'
) DROP PROCEDURE dbo.INSERTA_BASIC_TOR_REPORT_LIST;
GO 
CREATE PROCEDURE dbo.INSERTA_BASIC_TOR_REPORT_LIST(@P_TOR_REPORT_LIST xml, @P_TOR_ID varchar(255)) AS
BEGIN 
	DECLARE @v_report_id varchar(255)
	DECLARE @v_attachment_id varchar(255)
	
	DECLARE @SIMPLE_TOR_REPORT xml
	DECLARE @ATTACHMENT xml
	DECLARE @ISSUE_DATE date

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_TOR_REPORT_LIST.nodes('simple_tor_report_list/simple_tor_report') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @SIMPLE_TOR_REPORT
	WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @ISSUE_DATE = CAST(@SIMPLE_TOR_REPORT.value('(/simple_tor_report/issue_date)[1]','varchar(255)') as date)
			
			EXECUTE dbo.INSERTA_TOR_REPORT @ISSUE_DATE, @P_TOR_ID, @v_report_id output

			DECLARE cur2 CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @SIMPLE_TOR_REPORT.nodes('simple_tor_report/tor_attachment_list/attachment') T(c)
			OPEN cur2
			FETCH NEXT FROM cur2 INTO @ATTACHMENT
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.INSERTA_ATTACHMENT @ATTACHMENT, @v_attachment_id output
					INSERT INTO dbo.EWPCV_REPORT_ATT(ATTACHMENT_ID, REPORT_ID) VALUES (@v_attachment_id, @v_report_id)

					FETCH NEXT FROM cur2 INTO @ATTACHMENT
				END 
			FETCH NEXT FROM cur INTO @SIMPLE_TOR_REPORT
			CLOSE cur2
			DEALLOCATE cur2
		END
	CLOSE cur
	DEALLOCATE cur

END 
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_BASIC_TOR'
) DROP PROCEDURE dbo.INSERTA_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.INSERTA_BASIC_TOR(@P_TOR xml, @P_LA_ID varchar(255), @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_tor_id varchar(255)
	DECLARE @GENERATED_DATE date
	DECLARE @SIMPLE_TOR_REPORT_LIST xml

	SELECT 
		@GENERATED_DATE = CAST(T.c.value('generated_date[1]', 'varchar(255)') as date),
		@SIMPLE_TOR_REPORT_LIST = T.c.query('simple_tor_report_list')
	FROM @P_TOR.nodes('simple_tor') T(c)

	SET @v_tor_id = NEWID()

    INSERT INTO dbo.EWPCV_TOR (ID, VERSION, GENERATED_DATE) VALUES (@v_tor_id, 0, @GENERATED_DATE)

    UPDATE dbo.EWPCV_LEARNING_AGREEMENT SET TOR_ID = @v_tor_id WHERE ID = @P_LA_ID

	EXECUTE dbo.INSERTA_BASIC_TOR_REPORT_LIST @SIMPLE_TOR_REPORT_LIST, @v_tor_id

    SET @return_value = @v_tor_id

END 
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_TOR'
) DROP PROCEDURE dbo.ACTUALIZA_TOR;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_TOR(@P_TOR_ID varchar(255), @P_TOR xml) AS
BEGIN
	DECLARE @GENERATED_DATE date
	DECLARE @EXTENSION varbinary(max)
	DECLARE @TOR_REPORT_LIST xml
	DECLARE @MOBILITY_ATTACHMENT_LIST xml
	DECLARE @ISCED_TABLE_LIST xml

	SELECT
	@GENERATED_DATE = CAST(T.c.value('generated_date[1]', 'varchar(255)') as date),
	@EXTENSION = T.c.value('extension[1]', 'varbinary(max)'),
	@TOR_REPORT_LIST = T.c.query('tor_report_list'),
	@MOBILITY_ATTACHMENT_LIST = T.c.query('mobility_attachment_list'),
	@ISCED_TABLE_LIST = T.c.query('isced_table_list')
	FROM @P_TOR.nodes('tor') T(c)

	UPDATE dbo.EWPCV_TOR SET VERSION = VERSION + 1, GENERATED_DATE = @GENERATED_DATE, EXTENSION = @EXTENSION WHERE ID = @P_TOR_ID

	EXECUTE dbo.BORRA_ISCED_TABLE_LIST @P_TOR_ID
	EXECUTE dbo.BORRA_REPORTS @P_TOR_ID
	EXECUTE dbo.BORRA_MOBILITY_ATTACHMENT_LIST @P_TOR_ID

	EXECUTE dbo.INSERTA_TOR_REPORT_LIST @TOR_REPORT_LIST, @P_TOR_ID
	EXECUTE dbo.INSERTA_MOBILITY_ATTACHMENT_LIST @MOBILITY_ATTACHMENT_LIST, @P_TOR_ID
	EXECUTE dbo.INSERTA_ISCED_TABLE_LIST @ISCED_TABLE_LIST, @P_TOR_ID

END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_BASIC_TOR'
) DROP PROCEDURE dbo.ACTUALIZA_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_BASIC_TOR(@P_TOR_ID varchar(255), @P_TOR_SIMPLE_OBJECT xml) AS
BEGIN  
	DECLARE @GENERATED_DATE date
	DECLARE @SIMPLE_TOR_REPORT_LIST xml

	SELECT 
		@GENERATED_DATE = CAST(T.c.value('generated_date[1]', 'varchar(255)') as date),
		@SIMPLE_TOR_REPORT_LIST = T.c.query('simple_tor_report_list')
	FROM @P_TOR_SIMPLE_OBJECT.nodes('simple_tor') T(c)	

    UPDATE dbo.EWPCV_TOR SET VERSION = VERSION + 1, GENERATED_DATE = @GENERATED_DATE
    WHERE ID = @P_TOR_ID

    EXECUTE dbo.BORRA_BASIC_TOR_ATTACH @P_TOR_ID

    EXECUTE dbo.INSERTA_BASIC_TOR_REPORT_LIST @SIMPLE_TOR_REPORT_LIST, @P_TOR_ID


END
;
GO


  /* Elimina un BASIC_TOR del sistema.
    Recibe como parametro el identificador del ToR a eliminar.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_BASIC_TOR'
) DROP PROCEDURE dbo.DELETE_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.DELETE_BASIC_TOR(@P_TOR_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output)AS

BEGIN
	DECLARE @v_count integer
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_mob_id varchar(255)

	SET @return_value = 0
	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios')
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_TOR WHERE ID = @P_TOR_ID
		IF @v_count = 0 
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT('El TOR ', @P_TOR_ID, ' no existe.')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN

		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR @P_TOR_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
			BEGIN
				SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora.')
				SET @return_value = -1
			END
			ELSE
			BEGIN
			    SET @v_mob_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)
				EXECUTE dbo.BORRA_BASIC_TOR @P_TOR_ID
				EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei
				INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
				    VALUES (NEWID(), @P_TOR_ID, @v_mob_id, 2, 2)
			END

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_BASIC_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH

	END
   
END 
;
GO



IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'OBTEN_MOBILITY_ID'
) DROP FUNCTION dbo.OBTEN_MOBILITY_ID;
GO 
CREATE FUNCTION dbo.OBTEN_MOBILITY_ID(@P_TOR_ID varchar(255)) RETURNS varchar(255) AS
BEGIN
	DECLARE @return_value varchar(255) 
	DECLARE @v_m_id varchar(255)

	SELECT @v_m_id = M.ID
		FROM dbo.EWPCV_LEARNING_AGREEMENT LA
		INNER JOIN dbo.EWPCV_MOBILITY_LA ML ON ML.LEARNING_AGREEMENT_ID = LA.ID
		INNER JOIN dbo.EWPCV_MOBILITY M ON M.MOBILITY_ID = ML.MOBILITY_ID
		WHERE LOWER(LA.TOR_ID) = LOWER(@P_TOR_ID)

	SET @return_value = @v_m_id

	RETURN @return_value
END
;
GO


  /* Inserta un BASIC_TOR en el sistema.
    Admite un objeto de tipo TOR_SIMPLE_OBJECT con toda la informacion del ToR.
    Admite un parámetro de salida con el identificador ToR en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificación o eliminación.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.


*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_BASIC_TOR'
) DROP PROCEDURE dbo.INSERT_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.INSERT_BASIC_TOR(@P_TOR_SIMPLE_OBJECT xml, @P_LA_ID varchar(255), @P_TOR_ID varchar(255) output, @P_ERROR_MESSAGE varchar(4000) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output) AS
BEGIN
	DECLARE @v_la_id varchar(255)
	DECLARE @v_existe_tor_id varchar(255)
	DECLARE @v_tor_id varchar(255)
	DECLARE @v_la_revision varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_mob_id_tor_id varchar(255)

	SET @return_value = 0
	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios')
		SET @return_value = -1
	END

	SELECT @v_la_id = ID, @v_existe_tor_id = TOR_ID, @v_la_revision = LEARNING_AGREEMENT_REVISION 
		FROM dbo.EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID AND LEARNING_AGREEMENT_REVISION = (
				  SELECT MAX(LEARNING_AGREEMENT_REVISION)
				  FROM dbo.EWPCV_LEARNING_AGREEMENT 
				  WHERE ID = @P_LA_ID
				  )

    IF @v_la_id IS NULL 
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El Learning Agreement "' , @P_LA_ID ,'" no existe.')
		SET @return_value = -1
	END

	IF @return_value = 0
	BEGIN
		IF @v_existe_tor_id IS NOT NULL
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El Learning Agreement ', @P_LA_ID , ' ya tiene asociado el ToR ', @v_existe_tor_id ,', usa el método UPDATE_TOR para actualizarlo.')
			SET @return_value = -1
		END 
	END

	IF @return_value = 0
	BEGIN
		EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR_INSERT @v_la_id, @P_HEI_TO_NOTIFY, @v_notifier_hei output
		IF @v_notifier_hei is null
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora')
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN	
		BEGIN TRY
			BEGIN TRANSACTION
			
			-- Validamos que vengan informados los campos obligatorios
			EXECUTE dbo.VALIDA_BASIC_TOR @P_TOR_SIMPLE_OBJECT, @P_ERROR_MESSAGE, @return_value output
			IF @return_value = 0
				EXECUTE dbo.INSERTA_BASIC_TOR @P_TOR_SIMPLE_OBJECT, @P_LA_ID, @v_tor_id output
			
			SET @P_TOR_ID = @v_tor_id

			SET @v_mob_id_tor_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)

			EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id_tor_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_TOR_ID, @v_mob_id_tor_id, 2, 0)

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_BASIC_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO



  /* Actualiza un BASIC_TOR en el sistema.
    Recibe como parametro el identificador del ToR a actualizar.
    Admite un objeto de tipo BASIC_TOR con la informacion actualizada.
    Admite un parámetro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.

*/
 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_BASIC_TOR'
) DROP PROCEDURE dbo.UPDATE_BASIC_TOR;
GO 
CREATE PROCEDURE dbo.UPDATE_BASIC_TOR(@P_TOR_ID varchar(255), @P_TOR_SIMPLE_OBJECT xml, @P_ERROR_MESSAGE VARCHAR(4000) output, @P_HEI_TO_NOTIFY varchar(255) output, @return_value integer output) AS
BEGIN
	DECLARE @v_total integer
	DECLARE @v_tor_id varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_mob_id_tor_id varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'No se ha indicado la institución a la que notificar los cambios.')
		SET @return_value = -1
	END

	SELECT @v_total = COUNT(1) FROM dbo.EWPCV_TOR WHERE ID = @P_TOR_ID

	IF @v_total = 0
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El ToR "' , @P_TOR_ID , '" no existe.')
		SET @return_value = -1
	END

	EXECUTE dbo.OBTEN_NOTIFIER_HEI_TOR @P_TOR_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output

	IF @v_notifier_hei is null
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La institución a notificar no coincide con la institución emisora.')
		SET @return_value = -1
	END
	
	IF @return_value = 0
	BEGIN
		-- Validamos que vengan informados los campos obligatorios
		EXECUTE dbo.VALIDA_BASIC_TOR @P_TOR_SIMPLE_OBJECT, @P_ERROR_MESSAGE output, @return_value
	END

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			
			--Actualizamos el TOR
			EXECUTE dbo.ACTUALIZA_BASIC_TOR @P_TOR_ID, @P_TOR_SIMPLE_OBJECT
		
			SET @v_mob_id_tor_id  = dbo.OBTEN_MOBILITY_ID(@P_TOR_ID)
			EXECUTE dbo.INSERTA_NOTIFICATION @v_mob_id_tor_id, 3, @P_HEI_TO_NOTIFY, @v_notifier_hei

			INSERT INTO dbo.EWPCV_EXPORT_ACTIONS (ID, ELEMENT_ID, NOTIFICATION_ELEMENT_ID, ELEMENT_TYPE, ACTION)
			    VALUES (NEWID(), @P_TOR_ID, @v_mob_id_tor_id, 2, 1)

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_BASIC_TOR'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
	
END
;
GO







-- Validacionea adicionales

 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_NUM_STUDIED_LOI'
) DROP PROCEDURE dbo.VALIDA_NUM_STUDIED_LOI;
GO 
CREATE PROCEDURE dbo.VALIDA_NUM_STUDIED_LOI(@P_LA_ID varchar(255), @P_LA_REVISION integer, @P_ERROR_MESSAGE varchar(4000) output, @return_value integer output) AS
BEGIN
	DECLARE @v_studied_la_loi_count integer
	DECLARE @v_studied_la_count integer
	
	SET @return_value = 0 

    -- Obtenemos el nÃºmero de componentes estudiados del LA
    SELECT @v_studied_la_count = COUNT(1) FROM dbo.EWPCV_STUDIED_LA_COMPONENT C
		INNER JOIN dbo.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.STUDIED_LA_COMPONENT_ID
		WHERE C.LEARNING_AGREEMENT_ID = @P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
    
     -- Obtenemos el nÃºmero de componentes estudiados del LA
    SELECT @v_studied_la_loi_count = COUNT(1) FROM dbo.EWPCV_STUDIED_LA_COMPONENT C
		INNER JOIN dbo.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.STUDIED_LA_COMPONENT_ID
		WHERE C.LEARNING_AGREEMENT_ID = @P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
		AND LAC.LOI_ID IS NOT NULL
	    
    IF @v_studied_la_count <> @v_studied_la_loi_count
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, ' Todos los componentes estudiados del LA deben tener un LOI.')
		SET @return_value = -1
	END 
END
;
GO




IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_NUM_LOI'
) DROP PROCEDURE dbo.VALIDA_NUM_LOI;
GO 
CREATE PROCEDURE dbo.VALIDA_NUM_LOI(@P_TOR_REPORT_LIST xml, @P_LA_ID varchar(255), @P_LA_REVISION integer, @P_ERROR_MESSAGE varchar(255) output, @return_value integer output) AS
BEGIN
	DECLARE @v_report_loi_count integer
	DECLARE @v_loi_message_error varchar(255)
	DECLARE @v_la_loi_count integer
	DECLARE @v_count integer
	DECLARE @v_loi_identifier varchar(255)
	DECLARE @i integer
	DECLARE @j integer

	DECLARE @TOR_REPORT xml
	DECLARE @LOI xml
	DECLARE @LOI_ID varchar(255)
	DECLARE @LOS_ID varchar(255)
	DECLARE @LOS_CODE varchar(255)

	SET @i = 0
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @P_TOR_REPORT_LIST.nodes('tor_report_list/tor_report') T(c)
	OPEN cur
	FETCH NEXT FROM cur INTO @TOR_REPORT
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @v_report_loi_count = 0
        SET @v_loi_message_error = null
		SET @i = @i + 1
		SET @j = 0
		;
		
		-- Obtenemos el numero de lois que estan asociados a componentes estudiados del LA
        WITH COMPONENTS AS (
          SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID AS LA_COMPONENT_ID 
            FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
            LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
			)

        SELECT @v_la_loi_count = COUNT(1) FROM COMPONENTS C
			INNER JOIN dbo.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.LA_COMPONENT_ID
			WHERE C.ID = @P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		-- Se recorre la lista de LOI del report para comprobar que todos los loi informados estan en el LA asociado
		DECLARE cur2 CURSOR LOCAL FOR
		SELECT 
			T.c.query('.')
		FROM @TOR_REPORT.nodes('tor_report/loi_list/loi') T(c) 
		OPEN cur2
		FETCH NEXT FROM cur2 INTO @LOI
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @j = @j + 1
				
				SET @LOI_ID = @LOI.value('(loi/loi_id)[1]','varchar(255)')
				SET @LOS_ID = @LOI.value('(loi/los_id)[1]','varchar(255)')
				SET @LOS_CODE = @LOI.value('(loi/los_code)[1]','varchar(255)');
				
				WITH COMPONENTS AS (
				  SELECT la.id, la.LEARNING_AGREEMENT_REVISION, SC.STUDIED_LA_COMPONENT_ID AS LA_COMPONENT_ID 
					FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
					LEFT JOIN EWPCV_STUDIED_LA_COMPONENT SC ON SC.LEARNING_AGREEMENT_ID = LA.ID AND SC.LEARNING_AGREEMENT_REVISION = LA.LEARNING_AGREEMENT_REVISION
				)

				SELECT @v_count = COUNT(1) FROM COMPONENTS C
					INNER JOIN dbo.EWPCV_LA_COMPONENT LAC ON LAC.ID = C.LA_COMPONENT_ID
					WHERE C.ID = @P_LA_ID AND C.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION
					AND (LAC.LOI_ID = @LOI_ID OR 
						LAC.LOS_CODE = @LOS_CODE OR 
						LAC.LOS_ID =  @LOS_ID)

				IF @v_count = 0
				BEGIN
					--
					IF @LOI_ID is not null
						SET @v_loi_identifier = CONCAT('LOI_ID:', @LOI_ID)
					ELSE
					
						IF @LOS_ID is not null
							SET @v_loi_identifier = CONCAT('LOS_ID:', @LOS_ID)
						ELSE
							SET @v_loi_identifier = CONCAT('LOS_CODE:', @LOS_CODE)	
					
					SET @v_loi_message_error = CONCAT(@v_loi_message_error, 'LOI_LIST[', null , '] : El LOI con ' , @v_loi_identifier, ' no existe en el acuerdo de aprendizaje.')
					--
				END
				
				ELSE
					SET @v_report_loi_count = @v_report_loi_count + 1
            
 				FETCH NEXT FROM cur2 INTO @LOI
			END
		CLOSE cur2
		DEALLOCATE cur2

        IF @v_loi_message_error IS NOT NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT (@P_ERROR_MESSAGE , ' TOR_REPORT_LIST(', @i, '): ', @v_loi_message_error)
			SET @return_value = -1
        END
		
        -- Si no coincide el nÃºmero de LOI del LA y los informados en el report
        IF @v_report_loi_count <> @v_la_loi_count
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT (@P_ERROR_MESSAGE , ' TOR_REPORT_LIST(', @i, '): No coincide el numero de LOI de componentes estudiados del acuerdo de aprendizaje y los LOI informados en el ToR.')
			SET @return_value = -1
  		END 

		IF @j = 0
		BEGIN
			SET @P_ERROR_MESSAGE = CONCAT (@P_ERROR_MESSAGE , ' TOR_REPORT_LIST(', @i, '): LOI_LIST: No puede ir vacia.')
			SET @return_value = -1
  		END 

		FETCH NEXT FROM cur INTO @TOR_REPORT

	END
	CLOSE cur
	DEALLOCATE cur

	IF @i = 0
	BEGIN
		SET @P_ERROR_MESSAGE = CONCAT (@P_ERROR_MESSAGE , ' - TOR_REPORT_LIST:  La lista de informes no puede ir vacia.')
		SET @return_value = -1
  	END 



END
;
GO
