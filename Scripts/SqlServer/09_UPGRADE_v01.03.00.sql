/*
	**********************************
	INICIO MODIFICACIONES SOBRE TABLAS 
	**********************************
*/
    --EWPCV_REQUIREMENTS_INFO
	ALTER TABLE EWP.dbo.EWPCV_REQUIREMENTS_INFO ADD VERSION bigint;
	ALTER TABLE EWP.dbo.EWPCV_REQUIREMENTS_INFO ADD  CONSTRAINT DEF_VER_REQ_INFO  DEFAULT ((0)) FOR [VERSION];
	GO
	UPDATE EWP.dbo.EWPCV_REQUIREMENTS_INFO SET VERSION = 0;

	--EWPCV_INSTITUTION
	ALTER TABLE EWP.dbo.EWPCV_INSTITUTION ADD PRIMARY_CONTACT_DETAIL_ID varchar(255);
	
	alter table EWP.dbo.EWPCV_INSTITUTION add constraint FK_PRIM_CONTACT_DETAIL_ID
	foreign key (PRIMARY_CONTACT_DETAIL_ID) references EWP.dbo.EWPCV_CONTACT_DETAILS;
		
	--EWPCV_ORGANIZATION_UNIT
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD PRIMARY_CONTACT_DETAIL_ID varchar(255);
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD IS_TREE_STRUCTURE bigint;
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD IS_EXPOSED bigint;
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD PARENT_OUNIT_ID varchar(255);
	
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD  CONSTRAINT DEF_TREE_STRUCTURE  DEFAULT ((0)) FOR [IS_TREE_STRUCTURE];
	ALTER TABLE EWP.dbo.EWPCV_ORGANIZATION_UNIT ADD  CONSTRAINT DEF_IS_EXPOSED  DEFAULT ((1)) FOR [IS_EXPOSED];	
	GO
	UPDATE EWP.dbo.EWPCV_ORGANIZATION_UNIT SET IS_TREE_STRUCTURE = 0;
	UPDATE EWP.dbo.EWPCV_ORGANIZATION_UNIT SET IS_EXPOSED = 1;
		
	alter table EWP.dbo.EWPCV_ORGANIZATION_UNIT add constraint FK_PRIM_CONT_DETAIL_ID
	foreign key (PRIMARY_CONTACT_DETAIL_ID) references EWP.dbo.EWPCV_CONTACT_DETAILS;
	
	alter table EWP.dbo.EWPCV_ORGANIZATION_UNIT add constraint FK_OUNIT_PARENT_ID
	foreign key (PARENT_OUNIT_ID) references EWP.dbo.EWPCV_ORGANIZATION_UNIT;
	

	DROP TABLE EWP.dbo.EWPCV_ORG_UNIT_ORG_UNIT;
	
	--EWPCV_MOBILITY_PARTICIPANT
	ALTER TABLE EWPCV_MOBILITY_PARTICIPANT ADD GLOBAL_ID varchar(255);
	
/*
	*********************************
	 FIN MODIFICACIONES SOBRE TABLAS 
	*********************************
*/

/*
	*************************************
	INICIO MODIFICACIONES SOBRE FUNCIONES
	*************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_FAX'
) DROP FUNCTION dbo.EXTRAE_FAX;
GO
CREATE FUNCTION dbo.EXTRAE_FAX(@P_CONTACT_DETAILS_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
select @return_value = stuff( (select '|;|' + F.E164 + '|:|' + F.EXTENSION_NUMBER + '|:|' + F.OTHER_FORMAT
				FROM dbo.EWPCV_CONTACT_DETAILS CD,
					dbo.EWPCV_PHONE_NUMBER F
				WHERE F.ID = CD.FAX_NUMBER
					AND CD.ID = @P_CONTACT_DETAILS_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

IF EXISTS (
Select * from sysobjects where type = 'FN' and category = 0 AND NAME = 'EXTRAE_URLS_FACTSHEET'
) DROP FUNCTION dbo.EXTRAE_URLS_FACTSHEET;
GO
CREATE FUNCTION dbo.EXTRAE_URLS_FACTSHEET(@P_FACTSHEET_ID uniqueidentifier) RETURNS varchar(max)
BEGIN
DECLARE @return_value varchar(max)
SELECT @return_value = stuff( (select '|;|' + LAIT.TEXT + '|:|' + LAIT."LANG"
			   FROM dbo.EWPCV_FACT_SHEET_URL FSU,
					dbo.EWPCV_LANGUAGE_ITEM LAIT
				WHERE FSU.URL_ID = LAIT.ID
					AND FSU.FACT_SHEET_ID = @P_FACTSHEET_ID
               for xml path ('')
              ), 1, 3, ''
            );
	RETURN @return_value
END
;
GO

/*
	*************************************
	FIN MODIFICACIONES SOBRE FUNCIONES
	*************************************
*/

/*
	***********************************
	 INICIO MODIFICACIONES SOBRE VISTAS 
	***********************************
*/

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'FACTSHEET_VIEW'
) DROP VIEW dbo.FACTSHEET_VIEW;
GO 
CREATE VIEW dbo.FACTSHEET_VIEW AS 
  SELECT 
	I.INSTITUTION_ID    				  	 	 	AS INSTITUTION_ID,
	I.ABBREVIATION	 					  	 	 	AS ABREVIATION,
	MAX(F.DECISION_WEEKS_LIMIT)			  	 	 	AS DECISION_WEEK_LIMIT,
	MAX(F.TOR_WEEKS_LIMIT) 					  	 	AS TOR_WEEK_LIMIT,
	FORMAT(MAX(F.NOMINATIONS_AUTUM_TERM), '--MM-dd' )  	AS NOMINATIONS_AUTUM_TERM,
	FORMAT(MAX(F.NOMINATIONS_SPRING_TERM), '--MM-dd' ) 	AS NOMINATIONS_SPRING_TERM,
	FORMAT(MAX(F.APPLICATION_AUTUM_TERM), '--MM-dd' ) 	AS APPLICATION_AUTUM_TERM,
	FORMAT(MAX(F.APPLICATION_SPRING_TERM), '--MM-dd' ) 	AS APPLICATION_SPRING_TERM,
	dbo.EXTRAE_EMAILS(MAX(F.CONTACT_DETAILS_ID))	  	AS APPLICATION_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(F.CONTACT_DETAILS_ID)) 	 	AS APPLICATION_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(F.CONTACT_DETAILS_ID)) AS APPLICATION_URLS,
	dbo.EXTRAE_EMAILS(MAX(HI.CONTACT_DETAIL_ID))	  	AS HOUSING_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(HI.CONTACT_DETAIL_ID)) 	 	AS HOUSING_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(HI.CONTACT_DETAIL_ID))	AS HOUSING_URLS,
	dbo.EXTRAE_EMAILS(MAX(VI.CONTACT_DETAIL_ID))	  	AS VISA_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(VI.CONTACT_DETAIL_ID)) 	  	AS VISA_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(VI.CONTACT_DETAIL_ID)) AS VISA_URLS,
	dbo.EXTRAE_EMAILS(MAX(II.CONTACT_DETAIL_ID))	  	AS INSURANCE_EMAIL,
	dbo.EXTRAE_TELEFONO(MAX(II.CONTACT_DETAIL_ID)) 	 	AS INSURANCE_PHONE,
	dbo.EXTRAE_URLS_CONTACTO(MAX(II.CONTACT_DETAIL_ID))	AS INSURANCE_URLS
FROM dbo.EWPCV_INSTITUTION I
INNER JOIN dbo.EWPCV_FACT_SHEET F 
    ON I.FACT_SHEET = F.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS C 
    ON C.ID = F.CONTACT_DETAILS_ID
LEFT JOIN dbo.EWPCV_INST_INF_ITEM IIT
	ON IIT.INSTITUTION_ID = I.ID 
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM HI
	ON IIT.INFORMATION_ID = HI.ID AND HI."TYPE" = 'HOUSING'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM VI
	ON IIT.INFORMATION_ID = VI.ID AND VI."TYPE" = 'VISA'
LEFT JOIN dbo.EWPCV_INFORMATION_ITEM II
	ON IIT.INFORMATION_ID = II.ID AND II."TYPE" = 'INSURANCE'
GROUP BY I.INSTITUTION_ID,I.ABBREVIATION;

GO
/*
	Obtiene la informacion de Institutucion y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_VIEW'
) DROP VIEW dbo.INSTITUTION_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_VIEW AS 
SELECT DISTINCT
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    ins.LOGO_URL                                       AS INSTITUTION_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID) 				   AS INSTITUTION_WEBSITE,
	dbo.EXTRAE_URLS_FACTSHEET(ins.FACT_SHEET) 			AS INSTITUTION_FACTSHEET_URL,
    priflexadd_s.COUNTRY                               AS COUNTRY,
    priflexadd_s.LOCALITY                              AS CITY,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)     AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)           AS STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)     AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)           AS MAILING_ADDRESS
    
        /*
            Datos de la institution
        */
        FROM dbo.EWPCV_INSTITUTION ins
        LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ins.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address;
GO

/*
	Obtiene la informacion de los diferentes contactos de las instituciones
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'INSTITUTION_CONTACTS_VIEW'
) DROP VIEW dbo.INSTITUTION_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.INSTITUTION_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                          AS CONTACT_ID,
    ins.INSTITUTION_ID                                 AS INSTITUTION_ID, 
    dbo.EXTRAE_NOMBRES_INSTITUCION(ins.INSTITUTION_ID)     AS INSTITUTION_NAME,
    ins.ABBREVIATION                                   AS INSTITUTION_ABBREVIATION,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				   AS CONTACT_URL,
    cont.CONTACT_ROLE                                  AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				   AS CONTACT_NAME,
    dbo.EXTRAE_TELEFONO(condet.ID)                         AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                              AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                   AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                  AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                 AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)        AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)              AS STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS MAILING_ADDRESS
    
        
        FROM dbo.EWPCV_INSTITUTION ins
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON cont.institution_id = ins.institution_id
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ins.primary_contact_detail_id IS NULL OR condet.ID <> ins.primary_contact_detail_id )
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address						
		
		/*
            Datos de la institution
        */
		LEFT JOIN dbo.EWPCV_INSTITUTION_NAME instname ON instname.institution_id = ins.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = instname.name_id;
GO	

/*
	Obtiene la informacion de Unidad Organizativa y de su contacto principal
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_VIEW'
) DROP VIEW dbo.OUNIT_VIEW;
GO 
CREATE VIEW dbo.OUNIT_VIEW AS 
SELECT DISTINCT
    ou.ID                                 					AS OUNIT_ID, 
	ins.INSTITUTION_ID                     					AS INSTITUTION_ID, 
	ou.ORGANIZATION_UNIT_CODE                               AS OUNIT_CODE, 
    dbo.EXTRAE_NOMBRES_OUNIT(ou.ID)    						AS OUNIT_NAME,
    ou.ABBREVIATION                                   		AS OUNIT_ABBREVIATION,
    ou.LOGO_URL                                       		AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(pricondet.ID) 				   	AS OUNIT_WEBSITE,
	dbo.EXTRAE_URLS_FACTSHEET(ou.FACT_SHEET) 				AS OUNIT_FACTSHEET_URL,
    priflexadd_s.COUNTRY                               		AS COUNTRY,
    priflexadd_s.LOCALITY                              		AS CITY,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.STREET_ADDRESS)    	AS STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.STREET_ADDRESS)           	AS STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(pricondet.MAILING_ADDRESS)     AS MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(pricondet.MAILING_ADDRESS)           AS MAILING_ADDRESS,
	ou.PARENT_OUNIT_ID                                 		AS PARENT_OUNIT_ID,
	ou.IS_EXPOSED											AS IS_EXPOSED
    
        /*
            Datos de la institution
        */
        FROM dbo.EWPCV_ORGANIZATION_UNIT ou
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ouname ON ouname.organization_unit_id = ou.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitem ON langitem.id = ouname.name_id
		
		/*
			Datos institucion
		*/
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.organization_units_id = ou.id
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON iou.institution_id = ins.id
        
        /*
            Datos del contacto principal
        */
        LEFT JOIN dbo.EWPCV_CONTACT_DETAILS pricondet ON ou.primary_contact_detail_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_CONTACT pricont ON pricont.contact_details_id = pricondet.ID
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS priflexadd_s ON priflexadd_s.id = pricondet.street_address;
GO

/*
	Obtiene la informacion de los diferentes contactos de las unidades organizativas 
*/	
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_CONTACTS_VIEW'
) DROP VIEW dbo.OUNIT_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.OUNIT_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                            AS CONTACT_ID,
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID                     				 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    dbo.EXTRAE_NOMBRES_OUNIT(ounit.ID)                   AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				 AS OUNIT_WEBSITE,
    cont.CONTACT_ROLE                                 	 AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				 AS CONTACT_NAME,
    dbo.EXTRAE_TELEFONO(condet.ID)                       AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                            AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                 AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                    AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                   AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)      AS CONT_STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)            AS CONTACT_STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS CONT_MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS CONTACT_MAILING__ADDRESS,
	ounit.IS_EXPOSED									 AS IS_EXPOSED
    
        
       FROM dbo.EWPCV_ORGANIZATION_UNIT ounit
	    /*
			Datos institucion
		*/
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.organization_units_id = ounit.id
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON iou.institution_id = ins.id
        
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON cont.organization_unit_id = ounit.id
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ounit.primary_contact_detail_id IS NULL OR condet.ID <> ounit.primary_contact_detail_id ) 
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address

         /*
            Datos de la institution
        */
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id;
GO	


IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'MOBILITY_VIEW'
) DROP VIEW dbo.MOBILITY_VIEW;
GO 
CREATE VIEW dbo.MOBILITY_VIEW AS 
  SELECT 
    M.ID                                                                                                       AS MOBILITY_ID,
	M.MOBILITY_REVISION																						   AS MOBILITY_REVISION,
	M.PLANNED_ARRIVAL_DATE                                                                                     AS PLANNED_ARRIVAL_DATE,
	M.ACTUAL_ARRIVAL_DATE                                                                                      AS ACTUAL_ARRIVAL_DATE,
	M.PLANNED_DEPARTURE_DATE                                                                                   AS PLANNED_DEPARTURE_DATE,
	M.ACTUAL_DEPARTURE_DATE                                                                                    AS ACTUAL_DEPARTURE_DATE, 
	M.EQF_LEVEL                                                                                                AS EQF_LEVEL,
	M.IIA_ID                                                                                                   AS IIA_ID,
	M.COOPERATION_CONDITION_ID                                                                                 AS COOPERATION_CONDITION_ID,
	SA.ISCED_CODE + '|:|' + SA.ISCED_CLARIFICATION                                                             AS SUBJECT_AREA,
	M.STATUS                                                                                                   AS MOBILITY_STATUS,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                           AS MOBILITY_TYPE,
	dbo.EXTRAE_NIVELES_IDIOMAS(M.ID,M.MOBILITY_REVISION)                                                           AS LANGUAGE_SKILL,
	MP.GLOBAL_ID                                                                                               AS STUDENT_GLOBAL_ID,
	STP.FIRST_NAMES                                                                                            AS STUDENT_NAME,
	STP.LAST_NAME                                                                                              AS STUDENT_LAST_NAME,
	STP.BIRTH_DATE                                                                                             AS STUDENT_BIRTH_DATE,
	STP.GENDER                                                                                                 AS STUDENT_GENDER,
	STP.COUNTRY_CODE                                                                                           AS STUDENT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(STC.ID)                                                                            AS STUDENT_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(STC.ID)	                                                                   AS STUDENT_CONTACT_DESCRIPTIONS,
	dbo.EXTRAE_URLS_CONTACTO(STCD.ID)                                                                              AS STUDENT_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(STCD.ID)                                                                                     AS STUDENT_EMAILS,
	dbo.EXTRAE_TELEFONO(STC.CONTACT_DETAILS_ID)                                                                    AS STUDENT_PHONE,
	dbo.EXTRAE_ADDRESS_LINES(STCD.STREET_ADDRESS)                                                                  AS STUDENT_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(STCD.STREET_ADDRESS)                                                                        AS STUDENT_ADDRESS,
	STFA.POSTAL_CODE                                                                                           AS STUDENT_POSTAL_CODE,
	STFA.LOCALITY                                                                                              AS STUDENT_LOCALITY,
	STFA.REGION                                                                                                AS STUDENT_REGION,	
	STFA.COUNTRY                                                                                               AS STUDENT_COUNTRY,	
	M.RECEIVING_INSTITUTION_ID                                                                                 AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.RECEIVING_INSTITUTION_ID)                                                     AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = M.RECEIVING_ORGANIZATION_UNIT_ID)   AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.RECEIVING_ORGANIZATION_UNIT_ID)                                                     AS RECEIVING_OUNIT_NAMES,
	M.SENDING_INSTITUTION_ID							                                                       AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(M.SENDING_INSTITUTION_ID)                                                       AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = M.SENDING_ORGANIZATION_UNIT_ID)     AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(M.SENDING_ORGANIZATION_UNIT_ID)                                                       AS SENDING_OUNIT_NAMES,
	SP.FIRST_NAMES                                                                                             AS SENDER_CONTACT_NAME,
	SP.LAST_NAME                                                                                               AS SENDER_CONTACT_LAST_NAME,
	SP.BIRTH_DATE                                                                                              AS SENDER_CONTACT_BIRTH_DATE,
	SP.GENDER                                                                                                  AS SENDER_CONTACT_GENDER,
	SP.COUNTRY_CODE                                                                                            AS SENDER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SC.ID)                                                                             AS SENDER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SC.ID)	                                                                   AS SENDER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SCD.ID)                                                                               AS SENDER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SCD.ID)                                                                                      AS SENDER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SC.CONTACT_DETAILS_ID)                                                                     AS SENDER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SCD.STREET_ADDRESS)                                                                   AS SENDER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.STREET_ADDRESS)                                                                         AS SENDER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SCD.MAILING_ADDRESS)                                                              AS SENDER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SCD.MAILING_ADDRESS)                                                                    AS SENDER_CONTACT_MAILING_ADDR,
	SFA.POSTAL_CODE                                                                                            AS SENDER_CONTACT_POSTAL_CODE,
	SFA.LOCALITY                                                                                               AS SENDER_CONTACT_LOCALITY,
	SFA.REGION                                                                                                 AS SENDER_CONTACT_REGION,	
	SFA.COUNTRY                                                                                                AS SENDER_CONTACT_COUNTRY,		
	SAP.FIRST_NAMES                                                                                            AS ADMV_SEN_CONTACT_NAME,
	SAP.LAST_NAME                                                                                              AS ADMV_SEN_CONTACT_LAST_NAME,
	SAP.BIRTH_DATE                                                                                             AS ADMV_SEN_CONTACT_BIRTH_DATE,
	SAP.GENDER                                                                                                 AS ADMV_SEN_CONTACT_GENDER,
	SAP.COUNTRY_CODE                                                                                           AS ADMV_SEN_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SAC.ID)                                                                            AS ADMV_SEN_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SAC.ID)	                                                                   AS ADMV_SEN_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SACD.ID)          	                                                                   AS ADMV_SEN_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(SACD.ID)                                                                                     AS ADMV_SEN_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(SAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_SEN_CONTACT_PHONES,
	dbo.EXTRAE_FAX(SAC.CONTACT_DETAILS_ID)                                                                     AS ADMV_SEN_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SACD.STREET_ADDRESS)                                                                  AS ADMV_SEN_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.STREET_ADDRESS)                                                                        AS ADMV_SEN_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SACD.MAILING_ADDRESS)                                                             AS ADMV_SEN_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(SACD.MAILING_ADDRESS)                                                                   AS ADMV_SEN_CONTACT_MAILING_ADDR,
	SAFA.POSTAL_CODE                                                                                           AS ADMV_SEN_CONTACT_POSTAL_CODE,
	SAFA.LOCALITY                                                                                              AS ADMV_SEN_CONTACT_LOCALITY,
	SAFA.REGION                                                                                                AS ADMV_SEN_CONTACT_REGION,	
	SAFA.COUNTRY                                                                                               AS ADMV_SEN_CONTACT_COUNTRY,	
	RP.FIRST_NAMES                                                                                             AS RECEIVER_CONTACT_NAME,
	RP.LAST_NAME                                                                                               AS RECEIVER_CONTACT_LAST_NAME,
	RP.BIRTH_DATE                                                                                              AS RECEIVER_CONTACT_BIRTH_DATE,
	RP.GENDER                                                                                                  AS RECEIVER_CONTACT_GENDER,
	RP.COUNTRY_CODE                                                                                            AS RECEIVER_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RC.ID)                                                                             AS RECEIVER_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RC.ID)	                                                                   AS RECEIVER_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RCD.ID)          	                                                                   AS RECEIVER_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RCD.ID)                                                                                      AS RECEIVER_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RC.CONTACT_DETAILS_ID)                                                                     AS RECEIVER_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RCD.STREET_ADDRESS)                                                                   AS RECEIVER_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.STREET_ADDRESS)                                                                         AS RECEIVER_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RCD.MAILING_ADDRESS)                                                                   AS RECEIVER_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RCD.MAILING_ADDRESS)                                                                    AS RECEIVER_CONTACT_MAILING_ADDR,
	RFA.POSTAL_CODE                                                                                            AS RECEIVER_CONTACT_POSTAL_CODE,
	RFA.LOCALITY                                                                                               AS RECEIVER_CONTACT_LOCALITY,
	RFA.REGION                                                                                                 AS RECEIVER_CONTACT_REGION,	
	RFA.COUNTRY                                                                                                AS RECEIVER_CONTACT_COUNTRY,
	RAP.FIRST_NAMES                                                                                            AS ADMV_REC_CONTACT_NAME,
	RAP.LAST_NAME                                                                                              AS ADMV_REC_CONTACT_LAST_NAME,
	RAP.BIRTH_DATE                                                                                             AS ADMV_REC_CONTACT_BIRTH_DATE,
	RAP.GENDER                                                                                                 AS ADMV_REC_CONTACT_GENDER,
	RAP.COUNTRY_CODE                                                                                           AS ADMV_REC_CONTACT_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RAC.ID)                                                                            AS ADMV_REC_CONT_CONT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RAC.ID)	                                                                   AS ADMV_REC_CONT_CONT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RACD.ID)          	                                                                   AS ADMV_REC_CONT_CONT_URLS,
	dbo.EXTRAE_EMAILS(RACD.ID)                                                                                     AS ADMV_REC_CONTACT_EMAILS,
	dbo.EXTRAE_TELEFONO(RAC.CONTACT_DETAILS_ID)                                                                    AS ADMV_REC_CONTACT_PHONES,
	dbo.EXTRAE_FAX(RAC.CONTACT_DETAILS_ID)                                                                AS ADMV_REC_CONTACT_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RACD.STREET_ADDRESS)                                                                  AS ADMV_REC_CONT_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.STREET_ADDRESS)                                                                        AS ADMV_REC_CONTACT_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RACD.MAILING_ADDRESS)                                                                  AS ADMV_REC_CONT_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RACD.MAILING_ADDRESS)                                                                    AS ADMV_REC_CONTACT_MAILING_ADDR,
	RAFA.POSTAL_CODE                                                                                           AS ADMV_REC_CONTACT_POSTAL_CODE,
	RAFA.LOCALITY                                                                                              AS ADMV_REC_CONTACT_LOCALITY,
	RAFA.REGION                                                                                                AS ADMV_REC_CONTACT_REGION,	
	RAFA.COUNTRY                                                                                               AS ADMV_REC_CONTACT_COUNTRY	
FROM dbo.EWPCV_MOBILITY M
INNER JOIN dbo.EWPCV_SUBJECT_AREA SA 
	ON M.ISCED_CODE = SA.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT 
	ON M.MOBILITY_TYPE_ID = MT.ID
INNER JOIN dbo.EWPCV_MOBILITY_PARTICIPANT MP 
	ON M.MOBILITY_PARTICIPANT_ID = MP.ID
INNER JOIN dbo.EWPCV_CONTACT STC 
	ON MP.CONTACT_ID = STC.ID
INNER JOIN dbo.EWPCV_PERSON STP 
	ON STC.PERSON_ID = STP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS STCD 
	ON STC.CONTACT_DETAILS_ID = STCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS STFA 
	ON STFA.ID = STCD.STREET_ADDRESS
INNER JOIN dbo.EWPCV_CONTACT SC 
	ON M.SENDER_CONTACT_ID = SC.ID
INNER JOIN dbo.EWPCV_PERSON SP 
	ON SC.PERSON_ID = SP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS SCD 
	ON SC.CONTACT_DETAILS_ID = SCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SFA 
	ON SFA.ID = SCD.STREET_ADDRESS
INNER JOIN dbo.EWPCV_CONTACT RC 
	ON M.RECEIVER_CONTACT_ID = RC.ID
INNER JOIN dbo.EWPCV_PERSON RP 
	ON RC.PERSON_ID = RP.ID
INNER JOIN dbo.EWPCV_CONTACT_DETAILS RCD 
	ON RC.CONTACT_DETAILS_ID = RCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RFA 
	ON RFA.ID = RCD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT SAC 
	ON M.SENDER_ADMV_CONTACT_ID = SAC.ID
LEFT JOIN dbo.EWPCV_PERSON SAP 
	ON SAC.PERSON_ID = SAP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SACD 
	ON SAC.CONTACT_DETAILS_ID = SACD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SAFA 
	ON SAFA.ID = SACD.STREET_ADDRESS
LEFT JOIN dbo.EWPCV_CONTACT RAC 
	ON M.RECEIVER_ADMV_CONTACT_ID = RAC.ID
LEFT JOIN dbo.EWPCV_PERSON RAP 
	ON RAC.PERSON_ID = RAP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RACD 
	ON RAC.CONTACT_DETAILS_ID = RACD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RAFA 
	ON RAFA.ID = RACD.STREET_ADDRESS
;
GO

IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'IIA_COOP_CONDITIONS_VIEW'
) DROP VIEW dbo.IIA_COOP_CONDITIONS_VIEW;
GO 
CREATE VIEW dbo.IIA_COOP_CONDITIONS_VIEW AS 
  SELECT 
	CC.ID                                                                                                  AS COOPERATION_CONDITION_ID,
	I.ID                                                                                                   AS IIA_ID,
	I.IIA_CODE                                                                                             AS IIA_CODE,
	I.REMOTE_IIA_ID                                                                                        AS REMOTE_IIA_ID,
	I.REMOTE_IIA_CODE                                                                                      AS REMOTE_IIA_CODE,
	I.START_DATE                                                                                           AS IIA_START_DATE,
	I.END_DATE                                                                                             AS IIA_END_DATE,
	I.MODIFY_DATE                                                                                          AS IIA_MODIFY_DATE,
	I.APPROVAL_DATE                                                                                        AS IIA_APPROVAL_DATE, 
	CC.START_DATE                                                                                          AS COOP_COND_START_DATE,
	CC.END_DATE                                                                                            AS COOP_COND_END_DATE,
	CC.BLENDED                                                                                             AS COOP_COND_BLENDED,
	dbo.EXTRAE_EQF_LEVEL(CC.ID)                                                                            AS COOP_COND_EQF_LEVEL,
	CAST(D.NUMBERDURATION as varchar) + '|:|' + CAST(D.UNIT as varchar)                                    AS COOP_COND_DURATION,
	N.NUMBERMOBILITY                                                                                       AS COOP_COND_PARTICIPANTS,
	CC.OTHER_INFO                                                                                          AS COOP_COND_OTHER_INFO,
	MT.MOBILITY_CATEGORY + '|:|' + MT.MOBILITY_GROUP                                                       AS MOBILITY_TYPE,
	dbo.EXTRAE_S_AREA_L_SKILL(CC.ID)                                                                       AS SUBJECT_AREAS,
	RP.INSTITUTION_ID                                                                                      AS RECEIVING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(RP.INSTITUTION_ID)                                                      AS RECEIVING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = RP.ORGANIZATION_UNIT_ID)    AS RECEIVING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(RP.ORGANIZATION_UNIT_ID)                                                      AS RECEIVING_OUNIT_NAMES,
	RP.SIGNING_DATE                                                                                        AS RECEIVER_SIGNING_DATE,
	RPSCP.FIRST_NAMES                                                                                      AS RECEIVER_SIGNER_NAME,
	RPSCP.LAST_NAME                                                                                        AS RECEIVER_SIGNER_LAST_NAME,
	RPSCP.BIRTH_DATE                                                                                       AS RECEIVER_SIGNER_BIRTH_DATE,
	RPSCP.GENDER                                                                                           AS RECEIVER_SIGNER_GENDER,
	RPSCP.COUNTRY_CODE                                                                                     AS RECEIVER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(RPSC.ID)                                                                   AS RECEIVER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(RPSC.ID)	                                                           AS RECEIVER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(RPSCD.ID)                                                                     AS RECEIVER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(RPSC.ID)                                                                             AS RECEIVER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(RPSC.CONTACT_DETAILS_ID)                                                           AS RECEIVER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(RPSC.CONTACT_DETAILS_ID)                                                           	   AS RECEIVER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.STREET_ADDRESS)                                                         AS RECEIVER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.STREET_ADDRESS)                                                               AS RECEIVER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(RPSCD.MAILING_ADDRESS)                                                        AS RECEIVER_SIGNER_M_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(RPSCD.MAILING_ADDRESS)                                                              AS RECEIVER_SIGNER_MAILING_ADDR,
	RPSCFA.POSTAL_CODE                                                                                     AS RECEIVER_SIGNER_POSTAL_CODE,
	RPSCFA.LOCALITY                                                                                        AS RECEIVER_SIGNER_LOCALITY,
	RPSCFA.REGION                                                                                          AS RECEIVER_SIGNER_REGION,	
	RPSCFA.COUNTRY                                                                                         AS RECEIVER_SIGNER_COUNTRY,
	SP.INSTITUTION_ID                                                                                      AS SENDING_INSTITUTION,
	dbo.EXTRAE_NOMBRES_INSTITUCION(SP.INSTITUTION_ID)                                                      AS SENDING_INSTITUTION_NAMES,
	(SELECT ORGANIZATION_UNIT_CODE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = SP.ORGANIZATION_UNIT_ID)    AS SENDING_OUNIT_CODE,
	dbo.EXTRAE_NOMBRES_OUNIT(SP.ORGANIZATION_UNIT_ID)                                                      AS SENDING_OUNIT_NAMES,
	SP.SIGNING_DATE                                                                                        AS SENDER_SIGNING_DATE,
	SPSCP.FIRST_NAMES                                                                                      AS SENDER_SIGNER_NAME,
	SPSCP.LAST_NAME                                                                                        AS SENDER_SIGNER_LAST_NAME,
	SPSCP.BIRTH_DATE                                                                                       AS SENDER_SIGNER_BIRTH_DATE,
	SPSCP.GENDER                                                                                           AS SENDER_SIGNER_GENDER,
	SPSCP.COUNTRY_CODE                                                                                     AS SENDER_SIGNER_CITIZENSHIP,
	dbo.EXTRAE_NOMBRES_CONTACTO(SPSC.ID)                                                                   AS SENDER_SIGNER_CONTACT_NAMES,
	dbo.EXTRAE_DESCRIPCIONES_CONTACTO(SPSC.ID)	                                                           AS SENDER_SIGNER_CONTACT_DESCS,
	dbo.EXTRAE_URLS_CONTACTO(SPSCD.ID)                                                                     AS SENDER_SIGNER_CONTACT_URLS,
	dbo.EXTRAE_EMAILS(SPSC.ID)                                                                             AS SENDER_SIGNER_EMAILS,
	dbo.EXTRAE_TELEFONO(SPSC.CONTACT_DETAILS_ID)                                                           AS SENDER_SIGNER_PHONES,
	dbo.EXTRAE_FAX(SPSC.CONTACT_DETAILS_ID)                                                          	   AS SENDER_SIGNER_FAXES,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.STREET_ADDRESS)                                                         AS SENDER_SIGNER_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.STREET_ADDRESS)                                                               AS SENDER_SIGNER_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(SPSCD.MAILING_ADDRESS)                                                        AS SENDER_SIGNER_M_ADDRESS_LINES,
	dbo.EXTRAE_ADDRESS(SPSCD.MAILING_ADDRESS)                                                              AS SENDER_SIGNER_MAILING_ADDR,
	SPSCFA.POSTAL_CODE                                                                                     AS SENDER_SIGNER_POSTAL_CODE,
	SPSCFA.LOCALITY                                                                                        AS SENDER_SIGNER_LOCALITY,
	SPSCFA.REGION                                                                                          AS SENDER_SIGNER_REGION,	
	SPSCFA.COUNTRY                                                                                         AS SENDER_SIGNER_COUNTRY
FROM dbo.EWPCV_IIA I
INNER JOIN dbo.EWPCV_COOPERATION_CONDITION CC
    ON CC.IIA_ID = I.ID
LEFT JOIN dbo.EWPCV_DURATION D
    ON CC.DURATION_ID = D.ID
LEFT JOIN dbo.EWPCV_MOBILITY_NUMBER N
    ON CC.MOBILITY_NUMBER_ID = N.ID
LEFT JOIN dbo.EWPCV_MOBILITY_TYPE MT
    ON CC.MOBILITY_TYPE_ID = MT.ID
LEFT JOIN dbo.EWPCV_IIA_PARTNER SP
    ON CC.SENDING_PARTNER_ID = SP.ID
LEFT JOIN dbo.EWPCV_CONTACT SPSC
    ON SP.SIGNER_PERSON_CONTACT_ID = SPSC.ID
LEFT JOIN dbo.EWPCV_PERSON SPSCP 
    ON SPSC.PERSON_ID = SPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS SPSCD 
    ON SPSC.CONTACT_DETAILS_ID = SPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS SPSCFA 
    ON SPSCD.STREET_ADDRESS = SPSCFA.ID 
LEFT JOIN dbo.EWPCV_IIA_PARTNER RP
    ON CC.RECEIVING_PARTNER_ID = RP.ID
LEFT JOIN dbo.EWPCV_CONTACT RPSC
    ON RP.SIGNER_PERSON_CONTACT_ID = RPSC.ID
LEFT JOIN dbo.EWPCV_PERSON RPSCP 
    ON RPSC.PERSON_ID = RPSCP.ID
LEFT JOIN dbo.EWPCV_CONTACT_DETAILS RPSCD 
    ON RPSC.CONTACT_DETAILS_ID = RPSCD.ID
LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS RPSCFA 
    ON RPSCD.STREET_ADDRESS = RPSCFA.ID;
GO
/*
	*********************************
	 FIN MODIFICACIONES SOBRE VISTAS 
	*********************************
*/

/*
	**********************************
	INICIO MODIFICACIONES SOBRE COMMON 
	**********************************
*/
/*
		Inserta detalles de contacto.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_DETAILS'
) DROP PROCEDURE dbo.INSERTA_CONTACT_DETAILS;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_DETAILS(@P_CONTACT xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
		DECLARE @v_id uniqueidentifier
		DECLARE @v_p_id uniqueidentifier
		DECLARE @v_fax_id uniqueidentifier
		DECLARE @v_a_id uniqueidentifier
		DECLARE @v_ma_id uniqueidentifier
		DECLARE @v_li_id uniqueidentifier
		DECLARE @PHONE XML
		DECLARE @FAX XML
		DECLARE @STREET_ADDRESS XML
		DECLARE @MAILING_ADDRESS XML
		DECLARE @CONTACT_URL XML
		DECLARE @EMAIL_LIST XML
		DECLARE @email varchar(255)
		DECLARE @item xml

		SELECT 
			 @FAX = T.c.query('fax'),
			 @PHONE = T.c.query('phone'),
			 @STREET_ADDRESS = T.c.query('street_address'),	
			 @MAILING_ADDRESS = T.c.query('mailing_address'),	
			 @EMAIL_LIST = T.c.query('email_list'),
			 @CONTACT_URL = T.c.query('contact_url')
		FROM  @P_CONTACT.nodes('*') T(c)

		EXECUTE dbo.INSERTA_TELEFONO @FAX, @v_fax_id OUTPUT
		EXECUTE dbo.INSERTA_TELEFONO @PHONE, @v_p_id OUTPUT
		EXECUTE dbo.INSERTA_FLEXIBLE_ADDRES @STREET_ADDRESS, @v_a_id OUTPUT
		EXECUTE dbo.INSERTA_FLEXIBLE_ADDRES @MAILING_ADDRESS, @v_ma_id OUTPUT

		IF @v_p_id IS NOT NULL OR @v_a_id IS NOT NULL OR @v_ma_id IS NOT NULL OR @CONTACT_URL IS NOT NULL OR @EMAIL_LIST IS NOT NULL
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT_DETAILS (ID, PHONE_NUMBER, FAX_NUMBER, STREET_ADDRESS, MAILING_ADDRESS) VALUES (@v_id, @v_p_id, @v_fax_id, @v_a_id, @v_ma_id)

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('*/email_list/email') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @EMAIL_LIST
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@email = T.c.value('(.)[1]','varchar(255)')
					FROM @EMAIL_LIST.nodes('/email') T(c) 
					INSERT INTO dbo.EWPCV_CONTACT_DETAILS_EMAIL (CONTACT_DETAILS_ID, EMAIL ) VALUES (@v_id, @email);
					FETCH NEXT FROM cur INTO @EMAIL_LIST
				END
			CLOSE cur
			DEALLOCATE cur

			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_CONTACT.nodes('*/contact_url/text') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @CONTACT_URL
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.query('.')
					FROM @CONTACT_URL.nodes('/text') T(c) 
					EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
					INSERT INTO EWPCV_CONTACT_URL (CONTACT_DETAILS_ID, URL_ID ) VALUES (@v_id, @v_li_id);
					FETCH NEXT FROM cur INTO @CONTACT_URL
				END
			CLOSE cur
			DEALLOCATE cur

		END
		SET @return_value = @v_id

END
;
GO

/*
	Inserta datos de contacto y de persona 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT_PERSON'
) DROP PROCEDURE dbo.INSERTA_CONTACT_PERSON;
GO 
CREATE PROCEDURE  dbo.INSERTA_CONTACT_PERSON(@P_PERSON xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value uniqueidentifier OUTPUT) as
BEGIN
DECLARE @GENDER varchar(255)

DECLARE @CONTACT xml
DECLARE @CONTACT_NAME xml
DECLARE @CONTACT_DESCRIPTION xml
DECLARE @INSTITUTION_ID varchar(255)
DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
DECLARE @CONTACT_ROLE varchar(255)
DECLARE @v_p_id uniqueidentifier
DECLARE @v_c_id uniqueidentifier
DECLARE @v_ounit_id uniqueidentifier
DECLARE @v_inst_id varchar(255)
DECLARE @v_id uniqueidentifier
DECLARE @v_li_id uniqueidentifier
DECLARE @item xml

	SELECT
		@CONTACT = T.c.query('.'),
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('contact_role[1]','varchar(255)')
	FROM  @P_PERSON.nodes('*/*') T(c) 

	EXECUTE dbo.INSERTA_PERSONA @P_PERSON, @v_p_id OUTPUT
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @CONTACT, @v_c_id OUTPUT

	IF @v_p_id is not null or @v_c_id is not null or 
		@CONTACT_NAME is not null or @CONTACT_DESCRIPTION is not null 
	BEGIN

		IF @P_INSTITUTION_ID IS NOT NULL AND @P_OUNIT_CODE IS NULL
		BEGIN
			SET @v_inst_id = @P_INSTITUTION_ID;
		END;
                
	
		IF @P_OUNIT_CODE IS NOT NULL 
		BEGIN 
			SELECT @v_ounit_id = tabO.ID
				FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.ID
				INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
				WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
				AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
		END 

		SET @v_id = NEWID()
		INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, @v_p_id);

		-- Lista de contact name
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_NAME.nodes('contact_name/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

		-- Lista de contact description
		DECLARE cur CURSOR LOCAL FOR
			SELECT T.c.query('.') FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c)
		OPEN cur
		FETCH NEXT FROM cur INTO @item
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
			INSERT INTO EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id);
			FETCH NEXT FROM cur INTO @item
		END
		CLOSE cur
		DEALLOCATE cur

	END

	SET @return_value = @v_id

END
;
GO

/* 
Borra el detalle de contacto de la tabla ewpcv_contact_details, asi como todos los registros relacionados (emails, telefono, urls, y direcciones) 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_CONTACT_DETAILS'
) DROP PROCEDURE dbo.BORRA_CONTACT_DETAILS;
GO 
CREATE PROCEDURE  dbo.BORRA_CONTACT_DETAILS(@P_ID uniqueidentifier) AS 
BEGIN

	DECLARE @v_phone_id uniqueidentifier
	DECLARE @v_fax_id uniqueidentifier
	DECLARE @v_mailing_id uniqueidentifier
	DECLARE @v_street_id uniqueidentifier
	DECLARE @v_url_id uniqueidentifier


	SELECT @v_phone_id=PHONE_NUMBER, @v_fax_id = FAX_NUMBER,  @v_mailing_id=MAILING_ADDRESS, @v_street_id=STREET_ADDRESS
		FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DECLARE cur CURSOR LOCAL FOR
		SELECT URL_ID FROM dbo.EWPCV_CONTACT_URL WHERE CONTACT_DETAILS_ID=@P_ID
	OPEN cur
	FETCH NEXT FROM cur INTO @v_url_id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM dbo.EWPCV_CONTACT_URL WHERE URL_ID = @v_url_id
		DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @v_url_id
		FETCH NEXT FROM cur INTO @v_url_id
	END
	CLOSE cur
	DEALLOCATE cur

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS_EMAIL WHERE CONTACT_DETAILS_ID = @P_ID

	DELETE FROM dbo.EWPCV_CONTACT_DETAILS WHERE ID = @P_ID

	DELETE FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @v_phone_id
	DELETE FROM dbo.EWPCV_PHONE_NUMBER WHERE ID = @v_fax_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_mailing_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_mailing_id

	DELETE FROM dbo.EWPCV_FLEXAD_DELIV_POINT_COD WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXAD_RECIPIENT_NAME WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS_LINE WHERE FLEXIBLE_ADDRESS_ID = @v_street_id
	DELETE FROM dbo.EWPCV_FLEXIBLE_ADDRESS WHERE ID = @v_street_id

END
;
GO

/* 
	Persiste una lista de nombres de institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION_NAMES'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION_NAMES;
GO 
CREATE PROCEDURE  dbo.INSERTA_INSTITUTION_NAMES(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id uniqueidentifier
	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	DECLARE @v_count integer
	
	IF @P_INSTITUTION_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_INSTITUTION_NAMES.nodes('/institution_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe y esta asignado a la institucion
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')
			
			SELECT @v_count=count(1) FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_INSTITUTION_NAME ina on lait.ID = ina.NAME_ID
				WHERE UPPER(lait.text)=UPPER(@text) and UPPER(lait.lang)=UPPER(@lang) and ina.INSTITUTION_ID = @P_INSTITUTION_ID
			if @v_count = 0
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				INSERT INTO dbo.EWPCV_INSTITUTION_NAME (NAME_ID, INSTITUTION_ID) VALUES(@v_lang_item_id, @P_INSTITUTION_ID)
				SET @v_count = 1
			END
		
			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;

GO

/*
		Persiste una lista de nombres de organizacion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_NAMES'
) DROP PROCEDURE dbo.INSERTA_OUNIT_NAMES;
GO 
CREATE PROCEDURE  dbo.INSERTA_OUNIT_NAMES(@P_OUNIT_ID uniqueidentifier, @P_ORGANIZATION_UNIT_NAMES xml) as
BEGIN
	DECLARE @v_lang_item_id uniqueidentifier
	DECLARE @v_count integer

	DECLARE @xml xml
	DECLARE @lang varchar(255)
	DECLARE @text varchar(255) 
	
	IF @P_ORGANIZATION_UNIT_NAMES is not null
	BEGIN
		DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM   @P_ORGANIZATION_UNIT_NAMES.nodes('/organization_unit_name/text') T(c) 
		
		OPEN cur
		FETCH NEXT FROM cur INTO @xml

		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Comprobar si el duo lang+texto ya existe y esta asignado a la OUNIT
			SET @lang = @xml.value('(/text/@lang)[1]','varchar(255)')
			SET @text = @xml.value('(/text)[1]','varchar(255)')

			SELECT @v_count=count(1) FROM dbo.EWPCV_LANGUAGE_ITEM lait
				 inner join dbo.EWPCV_ORGANIZATION_UNIT_NAME ona on lait.ID = ona.NAME_ID
				WHERE UPPER(lait.text)=UPPER(@text) and UPPER(lait.lang)=UPPER(@lang) and ona.ORGANIZATION_UNIT_ID = @P_OUNIT_ID

			if @v_count=0
			BEGIN
				EXECUTE dbo.INSERTA_LANGUAGE_ITEM @xml, @v_lang_item_id OUTPUT
				INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT_NAME (NAME_ID, ORGANIZATION_UNIT_ID) VALUES(@v_lang_item_id, @P_OUNIT_ID)
				SET @v_count = 1
			END

			FETCH NEXT FROM cur INTO @xml
		END
 
		CLOSE cur
		DEALLOCATE cur
		
	END
END
;
GO

/* 
	Inserta un telefono de contacto independietemente de si existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_TELEFONO'
) DROP PROCEDURE dbo.INSERTA_TELEFONO;
GO 
CREATE PROCEDURE  dbo.INSERTA_TELEFONO(@P_PHONE xml, @v_id uniqueidentifier OUTPUT) as
BEGIN
	IF (@P_PHONE.value('(/*/e164)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/*/extension_number)[1]', 'varchar(255)') IS NOT NULL) 
		OR (@P_PHONE.value('(/*/other_format)[1]', 'varchar(255)') IS NOT NULL)
	BEGIN
		SET @v_id = NEWID()  
		INSERT INTO dbo.EWPCV_PHONE_NUMBER (ID, E164, EXTENSION_NUMBER, OTHER_FORMAT) 
						VALUES (@v_id, @P_PHONE.value('(/*/e164)[1]', 'varchar(255)'), @P_PHONE.value('(/*/extension_number)[1]', 'varchar(255)'), @P_PHONE.value('(/*/other_format)[1]', 'varchar(255)'))
	END 
END
;
GO

/*
	Inserta una direccion independietemente de si existe ya en el sistema y devuelve el identificador generado.
*/	
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FLEXIBLE_ADDRES'
) DROP PROCEDURE dbo.INSERTA_FLEXIBLE_ADDRES;
GO 
CREATE PROCEDURE  dbo.INSERTA_FLEXIBLE_ADDRES(@P_ADDRESS xml, @return_value varchar(255) OUTPUT) as
BEGIN
DECLARE @building_number integer
DECLARE @building_name varchar(255)
DECLARE @street_name varchar(255)
DECLARE @unit varchar(255)
DECLARE @building_floor varchar(255)
DECLARE @post_office_box varchar(255)
DECLARE @postal_code varchar(255)
DECLARE @locality varchar(255)
DECLARE @region varchar(255)
DECLARE @country varchar(255)
DECLARE @list xml
DECLARE @item varchar(255)
DECLARE @ID uniqueidentifier

	SELECT
		@building_number = T.c.value('building_number[1]','varchar(255)'),
		@building_name = T.c.value('building_name[1]','varchar(255)'),
		@street_name = T.c.value('street_name[1]','varchar(255)'),
		@unit = T.c.value('unit[1]','varchar(255)'),
		@building_floor = T.c.value('building_floor[1]','varchar(255)'),
		@post_office_box = T.c.value('post_office_box[1]','varchar(255)'),
		@postal_code = T.c.value('postal_code[1]','varchar(255)'),
		@locality = T.c.value('locality[1]','varchar(255)'),
		@region = T.c.value('region[1]','varchar(255)'),
		@country = T.c.value('country[1]','varchar(255)')
	FROM  @P_ADDRESS.nodes('/*') T(c) 

	IF @postal_code is not null 
		or @locality is not null 
		or @region is not null 
		or @country is not null
		BEGIN 
			SET @ID = NEWID()
			-- INSERT
			INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS (ID, BUILDING_NAME, BUILDING_NUMBER, COUNTRY, "FLOOR",
					LOCALITY, POST_OFFICE_BOX, POSTAL_CODE, REGION, STREET_NAME, UNIT) 
					VALUES (@ID, @building_number, @building_name, @country,
					@building_floor, @locality, @post_office_box, @postal_code,
					@region, @street_name, @unit )

			-- Lista recipient_name
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/recipient_name_list/recipient_name') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/recipient_name') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_RECIPIENT_NAME (FLEXIBLE_ADDRESS_ID, RECIPIENT_NAME) 
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista address_line
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/address_line_list/address_line') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/address_line') T(c) 
					INSERT INTO dbo.EWPCV_FLEXIBLE_ADDRESS_LINE (FLEXIBLE_ADDRESS_ID,ADDRESS_LINE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

			-- Lista delivery_point
			DECLARE cur CURSOR LOCAL FOR
				SELECT T.c.query('.') FROM @P_ADDRESS.nodes('/*/delivery_point_code_list/delivery_point_code') T(c) 		
			OPEN cur
			FETCH NEXT FROM cur INTO @list
			WHILE @@FETCH_STATUS = 0
				BEGIN
					SELECT
						@item = T.c.value('.[1]','varchar(255)')
					FROM @list.nodes('/delivery_point_code') T(c) 
					INSERT INTO dbo.EWPCV_FLEXAD_DELIV_POINT_COD (FLEXIBLE_ADDRESS_ID, DELIVERY_POINT_CODE)
						VALUES (@ID, @item)
					FETCH NEXT FROM cur INTO @list
				END
			CLOSE cur
			DEALLOCATE cur

		END 
		SET @return_value = @ID
END
;

GO


/*
	Inserta la institucion y la organization unit y la relacion entre ellas si no existe ya en el sistema
	Devuelve el id de la ounit
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INST_OUNIT'
) DROP PROCEDURE dbo.INSERTA_INST_OUNIT;
GO 
CREATE PROCEDURE dbo.INSERTA_INST_OUNIT(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_inst_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @v_ou_code VARCHAR(255)

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@INSTITUTION_NAME = T.c.query('(institution_name)'),
		@v_ou_code = T.c.value('(organization_unit_code)[1]','varchar(255)')
	FROM @P_INSTITUTION.nodes('*') T(c)

	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, null, null, null, @INSTITUTION_NAME, NULL, @v_inst_id OUTPUT
	IF @v_ou_code IS NOT NULL 
	BEGIN
		EXECUTE dbo.INSERTA_OUNIT @P_INSTITUTION, @v_ou_id OUTPUT
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @v_ou_id
		IF @v_count = 0
			INSERT INTO dbo.EWPCV_INST_ORG_UNIT (INSTITUTION_ID, ORGANIZATION_UNITS_ID) VALUES (@v_inst_id, @v_ou_id)

		SET @return_value = @v_ou_id
	END 
END
;
GO

/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.INSERTA_INSTITUTION(@P_INSTITUTION_ID  VARCHAR(255), @P_ABREVIATION VARCHAR(255), @P_LOGO_URL VARCHAR(255),
		 @P_FACTSHEET_ID VARCHAR(255),  @P_INSTITUTION_NAMES xml, @P_PRIMARY_CONTACT_DETAIL_ID VARCHAR(255), @return_value uniqueidentifier OUTPUT) AS

BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_count integer

	SELECT @v_count = count(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count = 0
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_INSTITUTION (ID, INSTITUTION_ID, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID)
				VALUES (@v_id, @P_INSTITUTION_ID, @P_ABREVIATION, @P_LOGO_URL, @P_FACTSHEET_ID, @P_PRIMARY_CONTACT_DETAIL_ID); 
		END
	ELSE
		BEGIN
			SELECT @v_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
			UPDATE  dbo.EWPCV_INSTITUTION SET  ABBREVIATION = @P_ABREVIATION, LOGO_URL = @P_LOGO_URL, FACT_SHEET = @P_FACTSHEET_ID WHERE ID = @v_id; 
		END
	
	EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_id, @P_INSTITUTION_NAMES

	SET @return_value = @v_id

END
;
GO

/*
	Inserta datos de contacto y retorna id de contact detail id
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_CONTACT'
) DROP PROCEDURE dbo.INSERTA_CONTACT;
GO 
CREATE PROCEDURE dbo.INSERTA_CONTACT(@P_CONTACT xml, @P_INSTITUTION_ID varchar(255), @P_OUNIT_CODE varchar(255), @return_value uniqueidentifier OUTPUT) AS
BEGIN
	DECLARE @v_id uniqueidentifier
	DECLARE @v_c_id uniqueidentifier

	DECLARE @v_ounit_id uniqueidentifier
	DECLARE @v_inst_id varchar(255)
	DECLARE @v_li_id uniqueidentifier

	DECLARE @CONTACT_NAME xml
	DECLARE @CONTACT_DESCRIPTION xml
	DECLARE @INSTITUTION_ID varchar(255) --deprecated
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255) --deprecated
	DECLARE @CONTACT_ROLE varchar(255)
	DECLARE @item xml

	SELECT 
		@CONTACT_NAME = T.c.query('contact_name'),
		@CONTACT_DESCRIPTION = T.c.query('contact_description'),
		@CONTACT_ROLE = T.c.value('(contact_role)[1]', 'varchar(255)')
	FROM @P_CONTACT.nodes('*') T(c)

	
	EXECUTE dbo.INSERTA_CONTACT_DETAILS @P_CONTACT, @v_c_id OUTPUT

	IF @v_c_id IS NOT NULL AND @CONTACT_NAME IS NOT NULL AND  @CONTACT_DESCRIPTION IS NOT NULL  
		BEGIN
			
			IF @P_INSTITUTION_ID IS NOT NULL AND @P_OUNIT_CODE IS NULL
				BEGIN
					SET @v_inst_id = @P_INSTITUTION_ID;
				END;
						
			
			IF @P_OUNIT_CODE IS NOT NULL 
				BEGIN 
					SELECT @v_ounit_id = tabO.ID
						FROM dbo.EWPCV_ORGANIZATION_UNIT tabO 
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT tabIO ON tabIO.ORGANIZATION_UNITS_ID = tabO.ID
						INNER JOIN dbo.EWPCV_INSTITUTION tabI ON tabIO.INSTITUTION_ID = tabI.ID
						WHERE UPPER(tabI.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
						AND UPPER(tabO.ORGANIZATION_UNIT_CODE) = UPPER(@P_OUNIT_CODE)
				END 
			
			SET @v_id  = NEWID()
			INSERT INTO dbo.EWPCV_CONTACT (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, CONTACT_ROLE, CONTACT_DETAILS_ID, PERSON_ID) 
					VALUES (@v_id, @v_inst_id, @v_ounit_id, @CONTACT_ROLE, @v_c_id, null)

			IF @CONTACT_NAME is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_NAME.nodes('contact_name/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_NAME (CONTACT_ID, NAME_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

			IF @CONTACT_DESCRIPTION is not null 
				BEGIN
					DECLARE cur CURSOR LOCAL FOR
					SELECT 
						T.c.query('.')
					FROM @CONTACT_DESCRIPTION.nodes('contact_description/text') T(c) 
					OPEN cur
					FETCH NEXT FROM cur INTO @item
					WHILE @@FETCH_STATUS = 0
						BEGIN
							EXECUTE dbo.INSERTA_LANGUAGE_ITEM @item, @v_li_id OUTPUT
							INSERT INTO dbo.EWPCV_CONTACT_DESCRIPTION (CONTACT_ID, DESCRIPTION_ID) VALUES (@v_id, @v_li_id)
							FETCH NEXT FROM cur INTO @item
						END
					CLOSE cur
					DEALLOCATE cur
				END 

		END 

	SET @return_value = @v_c_id

END
; 

GO

/* 
	Borra el contacto de la tabla contact, asi como la persona asociada y los nombres y descripciones del contacto)
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_CONTACT'
) DROP PROCEDURE dbo.BORRA_CONTACT;
GO 
CREATE PROCEDURE  dbo.BORRA_CONTACT(@P_ID varchar(255)) AS
BEGIN
	DECLARE @v_person_id VARCHAR(255)
	DECLARE @v_details_id VARCHAR(255)
	DECLARE @NAME_ID uniqueidentifier
	DECLARE @DESCRIPTION_ID uniqueidentifier

	IF @P_ID IS NOT NULL
	BEGIN

		SELECT @v_person_id = PERSON_ID, @v_details_id = CONTACT_DETAILS_ID
			FROM dbo.EWPCV_CONTACT
			WHERE ID = @P_ID

		DECLARE cur CURSOR LOCAL FOR
			SELECT NAME_ID
			FROM dbo.EWPCV_CONTACT_NAME
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @NAME_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_NAME WHERE NAME_ID = @NAME_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
				FETCH NEXT FROM cur INTO @NAME_ID
			END
		CLOSE cur
		DEALLOCATE cur


		DECLARE cur CURSOR LOCAL FOR
			SELECT DESCRIPTION_ID
			FROM dbo.EWPCV_CONTACT_DESCRIPTION
			WHERE CONTACT_ID = @P_ID

		OPEN cur
		FETCH NEXT FROM cur INTO @DESCRIPTION_ID
		WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE FROM dbo.EWPCV_CONTACT_DESCRIPTION WHERE DESCRIPTION_ID = @DESCRIPTION_ID
				DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @DESCRIPTION_ID
				FETCH NEXT FROM cur INTO @DESCRIPTION_ID
			END
		CLOSE cur
		DEALLOCATE cur

		DELETE FROM dbo.EWPCV_CONTACT WHERE ID = @P_ID
		EXECUTE dbo.BORRA_CONTACT_DETAILS @v_details_id
		DELETE FROM dbo.EWPCV_PERSON WHERE ID = @v_person_id

	END 

END 
;
GO

/*
	Borra URLs de factsheet
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_FACTSHEET_URL'
) DROP PROCEDURE dbo.BORRA_FACTSHEET_URL
GO 
CREATE PROCEDURE dbo.BORRA_FACTSHEET_URL(@P_FACTSHEET_ID uniqueidentifier) AS
BEGIN
	DECLARE @URL_ID uniqueidentifier
	DECLARE cur CURSOR LOCAL FOR
	SELECT URL_ID
		FROM dbo.EWPCV_FACT_SHEET_URL
		WHERE FACT_SHEET_ID = @P_FACTSHEET_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @URL_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_FACT_SHEET_URL WHERE URL_ID = @URL_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @URL_ID
			FETCH NEXT FROM cur INTO @URL_ID
		END
	CLOSE cur
	DEALLOCATE cur
END;
GO

/*
	*********************************
	 FIN MODIFICACIONES SOBRE COMMON 
	*********************************
*/

/*
	**************************************
	 INICIO MODIFICACIONES SOBRE FACTSHEET 
	**************************************
*/


/*
	Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FACTSHEET'
) DROP PROCEDURE dbo.INSERTA_FACTSHEET;
GO 
CREATE PROCEDURE  dbo.INSERTA_FACTSHEET(@P_FACTSHEET xml, @P_INFORMATION_ITEM xml, @P_FACTSHEET_ID VARCHAR(255), @return_value varchar(255) OUTPUT) as

BEGIN
	DECLARE	@v_contact_id VARCHAR(255)
	DECLARE	@v_factsheet_id uniqueidentifier

	DECLARE @email varchar(255)
	DECLARE @phone xml
	DECLARE @url xml

	SELECT
		@email = T.c.value('email[1]','varchar(255)'),
		@phone = T.c.query('phone'),
		@url = T.c.query('url')
	FROM   @P_INFORMATION_ITEM.nodes('/application_info') T(c) 

	EXECUTE dbo.INSERTA_CONTACT_DETAIL @url, @email, @phone, @v_contact_id OUTPUT

	IF  @P_FACTSHEET_ID IS NULL
		BEGIN
			SET @v_factsheet_id = NEWID()
			INSERT INTO dbo.EWPCV_FACT_SHEET (ID, DECISION_WEEKS_LIMIT, TOR_WEEKS_LIMIT, NOMINATIONS_AUTUM_TERM, NOMINATIONS_SPRING_TERM, APPLICATION_AUTUM_TERM, APPLICATION_SPRING_TERM, CONTACT_DETAILS_ID)
						VALUES(@v_factsheet_id, 
						@P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
						@P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
						@P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
						@P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
						@v_contact_id)
		END;
	ELSE
		BEGIN
			SET @v_factsheet_id = @P_FACTSHEET_ID
			UPDATE dbo.EWPCV_FACT_SHEET SET DECISION_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/decision_week_limit)[1]', 'varchar(255)'),
                TOR_WEEKS_LIMIT = @P_FACTSHEET.value('(factsheet/tor_week_limit)[1]', 'varchar(255)'),
                NOMINATIONS_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/nominations_autum_term)[1]', 'date'),
                NOMINATIONS_SPRING_TERM = @P_FACTSHEET.value('(factsheet/nominations_spring_term)[1]', 'date'),
                APPLICATION_AUTUM_TERM = @P_FACTSHEET.value('(factsheet/application_autum_term)[1]', 'date'),
                APPLICATION_SPRING_TERM = @P_FACTSHEET.value('(factsheet/application_spring_term)[1]', 'date'),
                CONTACT_DETAILS_ID = @v_contact_id
                WHERE ID = @v_factsheet_id;
		END;
	SET @return_value = @v_factsheet_id

END
;
GO
		
/*
	Persiste una hoja de contactos asociada a una institucion en el sistema.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.INSERTA_FACTSHEET_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.INSERTA_FACTSHEET_INSTITUTION(@P_FACTSHEET xml, @P_FACTSHEET_ID VARCHAR(255)) as

BEGIN

	DECLARE @v_id uniqueidentifier
	DECLARE @v_inst_id uniqueidentifier
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @v_add_info_id uniqueidentifier

	-- FACTSHEET base
	DECLARE @FACTSHEET xml
	DECLARE @APPLICATION_INFO xml
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ABREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @INSTITUTION_NAME xml
	DECLARE @HOUSING_INFO xml
	DECLARE @VISA_INFO xml
	DECLARE @INSURANCE_INFO xml

	SELECT
		@FACTSHEET = T.c.query('factsheet'),
		@APPLICATION_INFO = T.c.query('application_info'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABREVIATION = T.c.value('(abreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME = T.c.query('institution_name'),
		@HOUSING_INFO = T.c.query('housing_info'),
		@VISA_INFO = T.c.query('visa_info'),
		@INSURANCE_INFO = T.c.query('insurance_info')
	FROM @P_FACTSHEET.nodes('/factsheet_institution') T(c) 

	EXECUTE dbo.INSERTA_FACTSHEET @FACTSHEET, @APPLICATION_INFO, @P_FACTSHEET_ID, @v_factsheet_id OUTPUT
	EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, @ABREVIATION, @LOGO_URL, @v_factsheet_id, @INSTITUTION_NAME, NULL, @v_inst_id OUTPUT

	EXECUTE dbo.INSERTA_INFORMATION_ITEM @HOUSING_INFO, 'HOUSING', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @VISA_INFO, 'VISA', @v_inst_id
	EXECUTE dbo.INSERTA_INFORMATION_ITEM @INSURANCE_INFO, 'INSURANCE', @v_inst_id


	-- LISTA INFORMACION ADICIONAL
	DECLARE @ADDITIONAL_INFO_LIST xml
	DECLARE @ADDITIONAL_INFO xml
	DECLARE @INFO_TYPE varchar(255)

	DECLARE curAddInfo CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_info_list/additional_info') T(c) 

	OPEN curAddInfo
	FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@INFO_TYPE = T.c.value('(info_type)[1]','varchar(255)'),
				@ADDITIONAL_INFO = T.c.query('information_item')
			FROM @ADDITIONAL_INFO_LIST.nodes('/additional_info') T(c) 
			EXECUTE dbo.INSERTA_INFORMATION_ITEM @ADDITIONAL_INFO, @INFO_TYPE, @v_inst_id
			FETCH NEXT FROM curAddInfo INTO @ADDITIONAL_INFO_LIST
		END

	CLOSE curAddInfo
	DEALLOCATE curAddInfo


	-- LISTA REQUERIMIENTOS ACCESIBILIDAD
	DECLARE @accessibility_requirement_list xml
	DECLARE @accreq_type varchar(255)
	DECLARE @accreq_name varchar(255)
	DECLARE @accreq_desc varchar(255)
	DECLARE @accreq_url xml
	DECLARE @accreq_email varchar(255)
	DECLARE @accreq_phone xml

	DECLARE curAccReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/accessibility_requirements_list/accessibility_requirement') T(c) 

	OPEN curAccReq
	FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list

	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT
				@accreq_type = T.c.value('(req_type)[1]','varchar(255)'),
				@accreq_name = T.c.value('(name)[1]','varchar(255)'),
				@accreq_desc = T.c.value('(description)[1]','varchar(255)'),
				@accreq_url = T.c.query('information_item/url'),
				@accreq_email = T.c.value('(information_item/email)[1]','varchar(255)'),
				@accreq_phone = T.c.query('information_item/phone')
			FROM @accessibility_requirement_list.nodes('/accessibility_requirement') T(c) 
			EXECUTE dbo.INSERTA_REQUIREMENT_INFO @accreq_type, @accreq_name, @accreq_desc, @accreq_url, @accreq_email, @accreq_phone, @v_inst_id
			FETCH NEXT FROM curAccReq INTO @accessibility_requirement_list
		END

	CLOSE curAccReq
	DEALLOCATE curAccReq


	-- LISTA REQUERIMIENTOS ADICIONALES
	DECLARE @aditional_requirement_list xml
	DECLARE @addreq_name varchar(255)
	DECLARE @addreq_desc varchar(255)
	DECLARE @addreq_url xml
	
	DECLARE curAddReq CURSOR FOR
	SELECT
		T.c.query('.')
	FROM @P_FACTSHEET.nodes('/factsheet_institution/additional_requirements_list/additional_requirement') T(c) 

	OPEN curAddReq
	FETCH NEXT FROM curAddReq INTO @aditional_requirement_list

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT
			@addreq_name = T.c.value('(name)[1]','varchar(255)'),
			@addreq_desc = T.c.value('(description)[1]','varchar(255)'),
			@addreq_url = T.c.query('url')
		FROM @aditional_requirement_list.nodes('/additional_requirement') T(c) 
		EXECUTE dbo.INSERTA_REQUIREMENT_INFO 'REQUIREMENT', @addreq_name, @addreq_desc, @addreq_url, null, null, @v_inst_id
		FETCH NEXT FROM curAddReq INTO @accessibility_requirement_list
		END

	CLOSE curAddReq
	DEALLOCATE curAddReq

END
;
GO

/* Inserta un factsheet en el sistema
	Admite un objeto de tipo FACTSHEET_INSTITUTION con toda la informacion relativa a la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_FACTSHEET'
) DROP PROCEDURE dbo.INSERT_FACTSHEET;
GO 
CREATE PROCEDURE dbo.INSERT_FACTSHEET(@P_FACTSHEET xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer
			DECLARE @INSTITUTION_ID varchar(255)
			DECLARE @FACTSHEET_ID varchar(255)
			
				--validacion existe la institucion indicada
				SET @INSTITUTION_ID = @P_FACTSHEET.value('(factsheet_institution/institution_id)[1]', 'varchar(255)')
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@INSTITUTION_ID); 
				
				IF  @v_count = 0 
				BEGIN
					SET @return_value = -1
					SET @P_ERROR_MESSAGE = 'La institución '+ @INSTITUTION_ID +' vinculada al factsheet no existe en el sistema.'
				END
				ELSE 
				BEGIN
					SELECT @v_count = COUNT(1) 
						FROM dbo.EWPCV_INSTITUTION INS 
						INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND FS.CONTACT_DETAILS_ID IS NOT NULL;
					
					IF  @v_count > 0 
					BEGIN
						SET @P_ERROR_MESSAGE = 'Ya existe un fact sheet para la institucion indicada, utilice el metodo update para actualizarla.'
						SET @return_value = -1
					END 
					ELSE
					BEGIN
						SELECT @FACTSHEET_ID = FACT_SHEET 
							FROM dbo.EWPCV_INSTITUTION INS
							WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
						EXECUTE dbo.INSERTA_FACTSHEET_INSTITUTION @P_FACTSHEET, @FACTSHEET_ID
						SET @return_value = 0 
					END
				END
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	Borra un factsheet asociado a una institucion no borra la institucion.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_FACTSHEET_INSTITUTION'
) DROP PROCEDURE dbo.BORRA_FACTSHEET_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.BORRA_FACTSHEET_INSTITUTION(@P_INSTITUTION_ID uniqueidentifier, @P_FACTSHEET_ID uniqueidentifier) as
BEGIN
	DECLARE @CONTACT_DETAIL_ID uniqueidentifier
	SELECT @CONTACT_DETAIL_ID = CONTACT_DETAILS_ID FROM dbo.EWPCV_FACT_SHEET WHERE ID = @P_FACTSHEET_ID;
	
	
	UPDATE dbo.EWPCV_FACT_SHEET 
		SET DECISION_WEEKS_LIMIT = null,
                TOR_WEEKS_LIMIT = null,
                NOMINATIONS_AUTUM_TERM = null,
                NOMINATIONS_SPRING_TERM = null,
                APPLICATION_AUTUM_TERM = null,
                APPLICATION_SPRING_TERM = null,
                CONTACT_DETAILS_ID = null
		WHERE ID = @P_FACTSHEET_ID;
	EXECUTE dbo.BORRA_CONTACT_DETAILS @CONTACT_DETAIL_ID
	EXECUTE dbo.BORRA_REQUIREMENTS_INFO @P_INSTITUTION_ID
	EXECUTE dbo.BORRA_INFORMATION_ITEMS @P_INSTITUTION_ID	
END;
GO

/* Elimina un FACTSHEET del sistema.
	Recibe como parametro el identificador de la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_FACTSHEET'
) DROP PROCEDURE dbo.DELETE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @ID uniqueidentifier
		DECLARE @FACT_SHEET uniqueidentifier

		SET @return_value = 0

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
		IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
				SET @return_value = -1
			END
		ELSE
			BEGIN
				SELECT @v_count = COUNT(1) 
					FROM dbo.EWPCV_INSTITUTION INS 
					INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
					SET @return_value = -1
				END 
			END

		IF @return_value = 0
		BEGIN
			EXECUTE dbo.BORRA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_FACTSHEET'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/* Actualiza un FACTSHEET del sistema.
	Recibe como parametro el identificador de la Institucion a actualizar
	Admite un objeto de tipo FACTSHEET_INSTITUTION con la informacion actualizada.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_FACTSHEET'
) DROP PROCEDURE dbo.UPDATE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.UPDATE_FACTSHEET( @P_INSTITUTION_ID VARCHAR(255), @P_FACTSHEET xml, 
 @P_ERROR_MESSAGE VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @P_FACTSHEET, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_count integer
			DECLARE @ID uniqueidentifier
			DECLARE @FACT_SHEET uniqueidentifier
			DECLARE @INSTITUTION_ID_XML varchar(255)

		
			SELECT @ID = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			
			IF  @ID IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
					SET @return_value = -1
				END
			ELSE
				BEGIN 
					--validacion existe la institucion indicada en el xml de entrada
					SET @INSTITUTION_ID_XML = @P_FACTSHEET.value('(factsheet_institution/institution_id)[1]', 'varchar(255)')
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@INSTITUTION_ID_XML); 
					IF  @v_count = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = 'La institución '+ @INSTITUTION_ID_XML +' vinculada al factsheet no existe en el sistema.'
					END
					ELSE
					BEGIN
						SELECT @v_count = COUNT(1) 
							FROM dbo.EWPCV_INSTITUTION INS 
							INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
							WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) AND FS.CONTACT_DETAILS_ID IS NOT NULL;
						IF @v_count = 0
						BEGIN
							SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
							SET @return_value = -1
						END 
					END
				END

			IF @return_value = 0
			BEGIN
				EXECUTE dbo.ACTUALIZA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET, @P_FACTSHEET ;
			END
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_FACTSHEET'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	**************************************
	  FIN MODIFICACIONES SOBRE FACTSHEET 
	**************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE IIAS 
	****************************************
*/

	IF EXISTS (SELECT * FROM sys.xml_schema_collections 
						WHERE name = 'EWPCV_XML_IIA' 
						AND schema_id = SCHEMA_ID('dbo'))
	   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;
	 
	CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
	AS '<?xml version="1.0" encoding="utf-8"?>
	<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

		<xs:simpleType name="emptyType">
			<xs:restriction base="xs:string">
				<xs:enumeration value=""/>
			</xs:restriction>
		</xs:simpleType>

		<xs:simpleType name="dateOrEmptyType">
			<xs:union memberTypes="xs:date emptyType"/>
		</xs:simpleType>  

		<xs:simpleType name="notEmptyStringType">
			<xs:restriction base="xs:string">
				<xs:minLength value="1"/>
			</xs:restriction>
		</xs:simpleType>

		<xs:complexType name="languageItemListType">
			<xs:sequence>
				<xs:element name="text" maxOccurs="unbounded">
					<xs:complexType>
						<xs:simpleContent>
							<xs:extension base="xs:string">
								<xs:attribute name="lang" type="xs:string" use="required" />
							</xs:extension>
						</xs:simpleContent>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="phoneNumberType">
			<xs:sequence>
				<xs:element name="e164" minOccurs="0">
					<xs:simpleType>
						<xs:restriction base="xs:string">
							<xs:pattern value="\+[0-9]{1,15}"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="extension_number" minOccurs="0" type="xs:string" />
				<xs:element name="other_format" minOccurs="0" type="xs:string" />
			</xs:sequence>
		</xs:complexType>

		<xs:simpleType name="genderType">
			<xs:restriction base="xs:integer">
				<xs:enumeration value="0"/>
				<xs:enumeration value="1"/>
				<xs:enumeration value="2"/>
				<xs:enumeration value="9"/>
			</xs:restriction>
		</xs:simpleType>

		<xs:complexType name="informationItemType">
			<xs:sequence>
				<xs:element name="email">
					<xs:simpleType>
						<xs:restriction base="xs:string"> 
							<xs:pattern value="[^@]+@[^\.]+\..+"/> 
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
				<xs:element name="phone" type="phoneNumberType" />
				<xs:element name="url" type="languageItemListType" />
			</xs:sequence>
		</xs:complexType>
		
		<xs:complexType name="flexibleAddressType">
			<xs:sequence>
				<xs:element name="recipient_name_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="address_line_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element type="xs:string" name="building_number" minOccurs="0"/>
				<xs:element type="xs:string" name="building_name" minOccurs="0"/>
				<xs:element type="xs:string" name="street_name" minOccurs="0"/>
				<xs:element type="xs:string" name="unit" minOccurs="0"/>
				<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
				<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
				<xs:element name="delivery_point_code_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element type="notEmptyStringType" name="postal_code"/>
				<xs:element type="xs:string" name="locality" minOccurs="0"/>
				<xs:element type="xs:string" name="region" minOccurs="0"/>
				<xs:element type="notEmptyStringType" name="country"/>
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="contactPersonType">
			<xs:sequence>
				<xs:element name="given_name" type="xs:string" minOccurs="0" />
				<xs:element name="family_name" type="xs:string" minOccurs="0" />
				<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="citizenship" type="xs:string" minOccurs="0" />
				<xs:element name="gender" type="genderType" minOccurs="0" />
				<xs:element name="contact">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
							<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
							<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
							<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
							<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
							<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
							<xs:element name="email_list" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
							<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="institutionType">
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="partnerType">
			<xs:sequence>
				<xs:element name="institution" type="institutionType" />
				<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
				<xs:element name="partner_contacts_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="languageSkillsType">
			<xs:sequence>
				<xs:element name="language" type="notEmptyStringType" />
				<xs:element name="cefr_level" type="notEmptyStringType" />
			</xs:sequence>
		</xs:complexType>

		<xs:complexType name="subjectAreaType">
			<xs:sequence>
				<xs:element name="isced_code" type="notEmptyStringType" />
				<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>

		<xs:element name="iia">
			<xs:complexType>
				<xs:sequence>
					<xs:element name="iia_code" type="notEmptyStringType" />
					<xs:element name="start_date" type="xs:date" minOccurs="0"/>
					<xs:element name="end_date" type="xs:date" minOccurs="0"/>
					<xs:element name="cooperation_condition_list">
						<xs:complexType>
							<xs:sequence>
								<xs:element name="cooperation_condition" maxOccurs="unbounded" >
									<xs:complexType>
										<xs:sequence>
											<xs:element name="start_date" type="xs:date" />
											<xs:element name="end_date" type="xs:date" />
											<xs:element name="eqf_level_list">
												<xs:complexType>
													<xs:sequence>
														<xs:element name="eqf_level" maxOccurs="unbounded" type="xs:integer" />
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="mobility_number" type="xs:integer" />
											<xs:element name="mobility_type">
												<xs:complexType>
													<xs:sequence>
														<xs:element name="mobility_category">
															<xs:simpleType>
																<xs:restriction base="xs:string">
																	<xs:enumeration value="Teaching"/>
																	<xs:enumeration value="Studies"/>
																	<xs:enumeration value="Training"/>
																</xs:restriction>
															</xs:simpleType>
														</xs:element>
														<xs:element name="mobility_group">
															<xs:simpleType>
																<xs:restriction base="xs:string">
																	<xs:enumeration value="Student"/>
																	<xs:enumeration value="Staff"/>
																</xs:restriction>
															</xs:simpleType>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="receiving_partner" type="partnerType" />
											<xs:element name="sending_partner" type="partnerType" />
											<xs:element name="subject_area_language_list">
												<xs:complexType>
													<xs:sequence>
														<xs:element name="subject_area_language" maxOccurs="unbounded">
															<xs:complexType>
																<xs:sequence>
																	<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																	<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
																</xs:sequence>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="cop_cond_duration" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element name="number_duration" nillable="true" minOccurs="0" type="xs:integer" />
														<xs:element name="unit" minOccurs="0">
															<xs:simpleType>
																<xs:restriction base="xs:integer">
																	<xs:enumeration value="0"/>
																	<xs:enumeration value="1"/>
																	<xs:enumeration value="2"/>
																	<xs:enumeration value="3"/>
																	<xs:enumeration value="4"/>
																</xs:restriction>
															</xs:simpleType>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="other_info" type="xs:string" />
											<xs:element name="blended" type="xs:boolean" />
										</xs:sequence>
									</xs:complexType>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="pdf" type="xs:string" minOccurs="0" />
				</xs:sequence>
			</xs:complexType>
		</xs:element>
	</xs:schema>';

	GO
/*
	Valida que el eqf level solo venga informado cuando mobility_category = Studies y mobility_group = Student
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_EQF_LEVEL'
) DROP PROCEDURE dbo.VALIDA_EQF_LEVEL;
GO 
CREATE PROCEDURE  dbo.VALIDA_EQF_LEVEL(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @v_m_category varchar(255)
	DECLARE @v_m_group varchar(255)
	DECLARE @v_eqf_level varchar(255)

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	
	SELECT 
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(mobility_type/mobility_category)[1]', 'varchar(255)'),
		T.c.value('(mobility_type/mobility_group)[1]', 'varchar(255)'),
		T.c.value('(eqf_level_list/eqf_level)[1]', 'varchar(255)')
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)
	
	OPEN cur
	FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_eqf_level
	WHILE @@FETCH_STATUS = 0
		BEGIN
			IF UPPER(@v_m_category) = 'STUDIES' AND UPPER(@v_m_group) = 'STUDENT' AND @v_eqf_level IS NULL
			BEGIN
				SET @return_value=-1
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				Se debe informar al menos un eqf_level cuando la condicion de cooperacion es de tipo Student-Studies'
			END
		FETCH NEXT FROM cur INTO  @v_m_category, @v_m_group, @v_eqf_level
		END;
	CLOSE cur
	DEALLOCATE cur
END
;
GO

/*
	Valida que existan los institution id y ounit id de los partners
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_IIA_PARTNER_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_IIA_PARTNER_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_IIA_PARTNER_INSTITUTION(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
DECLARE @COOPERATION_CONDITION_LIST xml
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_s_ounit_code varchar(255)
DECLARE @v_r_inst_id varchar(255)
DECLARE @v_r_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	SELECT 
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list')
	FROM @P_IIA.nodes('iia') T(c)
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(sending_partner/institution/institution_id)[1]', 'varchar(255)'),
		T.c.value('(sending_partner/institution/organization_unit_code)[1]', 'varchar(255)'),
		T.c.value('(receiving_partner/institution/institution_id)[1]', 'varchar(255)'),
		T.c.value('(receiving_partner/institution/organization_unit_code)[1]', 'varchar(255)')
		FROM @COOPERATION_CONDITION_LIST.nodes('cooperation_condition_list/cooperation_condition') T(c)
	
	OPEN cur
	FETCH NEXT FROM cur INTO  @v_s_inst_id, @v_s_ounit_code, @v_r_inst_id, @v_r_ounit_code
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--validacion de institution del sender
			SELECT @v_count_inst = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@v_s_inst_id); 
			IF  @v_count_inst = 0 
			BEGIN
				SET @return_value=-1
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				La institución '+ @v_s_inst_id +' vinculada a un sending partner no existe en el sistema.'
			END
			ELSE IF @v_s_ounit_code IS NOT NULL
			BEGIN
				--validacion de ounit del sender
				SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
					INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_s_inst_id)
					AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_s_ounit_code);
				IF  @v_count_ounit = 0 
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
					La unidad organizativa '+ @v_s_ounit_code +' vinculada a ' + @v_s_inst_id  + ' no existe en el sistema.'
				END
			END 
				
			--validacion de institution del receiver	
			SELECT @v_count_inst = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@v_r_inst_id); 
			IF  @v_count_inst = 0 
			BEGIN
				SET @return_value=-1
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
				La institución '+ @v_r_inst_id +' vinculada a un receiving partner no existe en el sistema.'
			END
			ELSE IF @v_r_ounit_code IS NOT NULL
			BEGIN
				--validacion de institution del receiver
				SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
					INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_r_inst_id)
					AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_r_ounit_code);
				IF  @v_count_ounit = 0 
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
					La unidad organizativa '+ @v_r_ounit_code +' vinculada a ' + @v_r_inst_id  + ' no existe en el sistema.'
				END
			END 
			FETCH NEXT FROM cur INTO  @v_s_inst_id, @v_s_ounit_code, @v_r_inst_id, @v_r_ounit_code
		END
	CLOSE cur
	DEALLOCATE cur
	
END 
;
GO

/*
	Valida la calidad de los datos de entrada al aprovisionamiento
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_IIA'
) DROP PROCEDURE dbo.VALIDA_DATOS_IIA;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_IIA(@P_IIA XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 

	EXECUTE dbo.VALIDA_EQF_LEVEL @P_IIA, @P_ERROR_MESSAGE output, @return_value output;
	
	IF @return_value = 0 
	BEGIN
		EXECUTE dbo.VALIDA_IIA_PARTNER_INSTITUTION @P_IIA, @P_ERROR_MESSAGE output, @return_value output;
	END 
END 
;
GO


	/* Inserta un IIA en el sistema
			Admite un objeto de tipo IIA con toda la informacion del Acuerdo Interinstitucional
			Admite un parametro de salida con el identificador del acuerdo interinstitucional en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
			Admite un parametro de salida con una descripcion del error en caso de error.
			Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	IF EXISTS (
	Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_IIA'
	) DROP PROCEDURE dbo.INSERT_IIA;
	GO 
	CREATE PROCEDURE dbo.INSERT_IIA(@P_IIA XML, @P_IIA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS

	BEGIN
		DECLARE @v_notifier_hei varchar(255)

		IF @P_HEI_TO_NOTIFY IS NULL
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END
		ELSE
		BEGIN
			EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
			--validacion, institution id y ounit code de los partners existen
			EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
			IF @return_value = 0
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

					EXECUTE dbo.INSERTA_IIA @P_IIA, @P_IIA_ID OUTPUT
					EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY, @v_notifier_hei output
					EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei

					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
					THROW
					SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_IIA'
					SET @return_value = -1
					ROLLBACK TRANSACTION
				END CATCH
			END
		END
	END
	;
	GO

	/*
		Inserta datos de contacto y de persona independientemente de si existe ya en el sistema
	*/
	IF EXISTS (
	Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_IIA_PARTNER'
	) DROP PROCEDURE dbo.INSERTA_IIA_PARTNER;
	GO 
	CREATE PROCEDURE  dbo.INSERTA_IIA_PARTNER(@P_IIA_PARTNER xml, @return_value uniqueidentifier output) AS
	BEGIN
		DECLARE @v_id uniqueidentifier
		DECLARE @v_c_id uniqueidentifier
		DECLARE @v_ounit_id uniqueidentifier
		DECLARE @INSTITUTION xml
		DECLARE @SIGNER_PERSON xml
		DECLARE @INSTITUTION_ID VARCHAR(255)
		DECLARE @SIGNING_DATE date


		SELECT 
			@INSTITUTION = T.c.query('institution'),
			@SIGNER_PERSON = T.c.query('signer_person'),
			@INSTITUTION_ID = T.c.value('(institution/institution_id)[1]', 'varchar(255)'),
			@SIGNING_DATE = T.c.value('(signing_date)[1]', 'date')
		FROM @P_IIA_PARTNER.nodes('*') T(c)

		EXECUTE dbo.INSERTA_INST_OUNIT @INSTITUTION, @v_ounit_id output
		EXECUTE dbo.INSERTA_CONTACT_PERSON @SIGNER_PERSON, null, null, @v_c_id output
		SET @v_id = NEWID()

		INSERT INTO EWPCV_IIA_PARTNER (ID, INSTITUTION_ID, ORGANIZATION_UNIT_ID, SIGNER_PERSON_CONTACT_ID, SIGNING_DATE) 
				VALUES (@v_id, @INSTITUTION_ID, @v_ounit_id, @v_c_id, @SIGNING_DATE)

		SET @return_value = @v_id
		
	END
	;
	GO

	/*
		Persiste la lista de contactos de un partner, si la lista está vacia no hace nada
	*/
	IF EXISTS (
	Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_PARTNER_CONTACTS'
	) DROP PROCEDURE dbo.INSERTA_PARTNER_CONTACTS;
	GO 
	CREATE PROCEDURE  dbo.INSERTA_PARTNER_CONTACTS(@P_PARTNER_ID uniqueidentifier, @P_PARTNER_CONTACTS xml) AS
	BEGIN
		DECLARE @partner_contact xml
		DECLARE @v_c_p_id uniqueidentifier

		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.query('.')
		FROM @P_PARTNER_CONTACTS.nodes('partner_contacts_list/partner_contact') T(c) 

		OPEN cur
		FETCH NEXT FROM cur INTO @partner_contact
		WHILE @@FETCH_STATUS = 0
			BEGIN
				EXECUTE dbo.INSERTA_CONTACT_PERSON @partner_contact, null, null, @v_c_p_id output
				INSERT INTO dbo.EWPCV_IIA_PARTNER_CONTACTS (IIA_PARTNER_ID, CONTACTS_ID) VALUES (@P_PARTNER_ID, @v_c_p_id)
				FETCH NEXT FROM cur INTO @partner_contact
			END
		CLOSE cur
		DEALLOCATE cur

	END
	;
	GO

	/* Actualiza un IIA del sistema.
		Recibe como parametro del Acuerdo Interinstitucional a actualizar
		Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
		Admite un parametro de salida con una descripcion del error en caso de error.
		Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
	IF EXISTS (
	Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_IIA'
	) DROP PROCEDURE dbo.UPDATE_IIA;
	GO 
	CREATE PROCEDURE dbo.UPDATE_IIA (@P_IIA_ID uniqueidentifier, @P_IIA xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT, 
		@P_HEI_TO_NOTIFY VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

	BEGIN
		EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		--validacion, institution id y ounit code de los partners existen
		EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
		
		IF @return_value=0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DECLARE @v_notifier_hei varchar(255)
				DECLARE @v_cod_retorno integer
				DECLARE @v_count integer

				IF @P_HEI_TO_NOTIFY IS NULL 
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
					SET @return_value = -1
				END

				IF @return_value = 0 
				BEGIN
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
					IF  @v_count = 0 
					BEGIN
						SET @P_ERROR_MESSAGE = 'El IIA no existe.'
						SET @return_value = -1
					END
				END

				IF @return_value = 0 
				BEGIN
					EXECUTE dbo.ACTUALIZA_IIA @P_IIA_ID, @P_IIA
					EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
					EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END 
				COMMIT TRANSACTION
			
			END TRY
			BEGIN CATCH
				THROW
				SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_IIA'
				SET @return_value = -1
				ROLLBACK TRANSACTION
			END CATCH
		END
	END
	;
	GO
/*
	****************************************
	  FIN MODIFICACIONES SOBRE IIAS
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/
/*
***************************************
******** Funciones Mobility ***********
***************************************
*/


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_SIGNATURE' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_SIGNATURE
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>
	<xs:element name="signature" type="signatureType" />
</xs:schema>'

GO

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_MOBILITY' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>


	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_id" type="notEmptyStringType" minOccurs="0"  />
				<xs:element name="cooperation_condition_id" type="notEmptyStringType" minOccurs="0"/>
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="eqf_level" type="xs:integer" />
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>			
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="status">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
							<xs:enumeration value="4"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'

GO


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LEARNING_AGREEMENT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="creditType">
		<xs:sequence>
			<xs:element name="scheme" type="notEmptyStringType" />
			<xs:element name="credit_value">
				<xs:simpleType>
					<xs:restriction base="xs:decimal">
						<xs:totalDigits value="5" />
						<xs:fractionDigits value="1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language_skill" maxOccurs="unbounded">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="language" type="notEmptyStringType" />
						<xs:element name="cefr_level" type="notEmptyStringType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="learning_agreement">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="mobility_id" type="notEmptyStringType" />
				<xs:element name="student_la" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="given_name" type="xs:string" />
							<xs:element name="family_name" type="xs:string" />
							<xs:element name="birth_date" type="xs:string" />
							<xs:element name="citizenship" type="xs:string" />
							<xs:element name="gender" type="genderType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="la_component_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="la_component" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="la_component_type">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_code" type="xs:string" minOccurs="0" />
										<xs:element name="title" type="notEmptyStringType" />
										<xs:element name="academic_term">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="academic_year" type="academicYearType" />
													<xs:element name="institution_id" type="notEmptyStringType" />
													<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
													<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="description" type="languageItemListType" minOccurs="0" />
													<xs:element name="term_number" type="xs:integer" />
													<xs:element name="total_terms" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="credit" type="creditType" />
										<xs:element name="recognition_conditions" type="xs:string" minOccurs="0" />
										<xs:element name="short_description" type="xs:string" minOccurs="0" />
										<xs:element name="status" type="xs:string" minOccurs="0" />
										<xs:element name="reason_code" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
													<xs:enumeration value="5"/>
												</xs:restriction>
											</xs:simpleType>													
										</xs:element>
										<xs:element name="reason_text" type="xs:string" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="student_signature" type="signatureType" />
				<xs:element name="sending_hei_signature" type="signatureType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'
;

GO

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_SIGNATURE'
) DROP PROCEDURE dbo.VALIDA_SIGNATURE;
GO 
CREATE PROCEDURE  dbo.VALIDA_SIGNATURE(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_SIGNATURE)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 


IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY'
) DROP PROCEDURE dbo.VALIDA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_MOBILITY)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;


GO 



IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.VALIDA_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE  dbo.VALIDA_LEARNING_AGREEMENT(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_LEARNING_AGREEMENT)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO 


/*
	Valida que el iias y la coop condition indicadas existan
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY_IIA'
) DROP PROCEDURE dbo.VALIDA_MOBILITY_IIA;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY_IIA(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	
	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	
	DECLARE @v_iia_id varchar(255)
	DECLARE @v_cc_id varchar(255)
	DECLARE @v_count integer

	SELECT 
		@v_iia_id = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@v_cc_id = T.c.value('(cooperation_condition_id)[1]', 'varchar(255)')
	FROM @P_MOBILITY_XML.nodes('mobility') T(c)
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE UPPER(ID) = UPPER(@v_iia_id);
	
	IF @v_count = 0 
	BEGIN 
		SET @return_value = -1;
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
		El acuerdo interistitucional indicado no existe en el sistema.'
	END 
	ELSE 
	BEGIN 
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_COOPERATION_CONDITION WHERE UPPER(ID) = UPPER(@v_cc_id) AND UPPER(IIA_ID) = UPPER(@v_iia_id);
		IF @v_count = 0 
		BEGIN 
			SET @return_value = -1;
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
			La condicion de cooperacion indicada no existe en el sistema o no está asociada al acuerdo interistitucional indicado.'
		END 
	END;
	
END
;
GO


 /*
	Valida que existan los institution id y ounit id de la mobility
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_MOBILITY_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_MOBILITY_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_MOBILITY_INSTITUTION(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_s_ounit_code varchar(255)
DECLARE @v_r_inst_id varchar(255)
DECLARE @v_r_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	SELECT 
		@v_s_inst_id = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@v_s_ounit_code = T.c.value('(sending_institution/organization_unit_code)[1]', 'varchar(255)'),
		@v_r_inst_id = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@v_r_ounit_code = T.c.value('(receiving_institution/organization_unit_code)[1]', 'varchar(255)')
		FROM @P_MOBILITY_XML.nodes('mobility') T(c)
	
	--validacion de institution del sender
	SELECT @v_count_inst = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@v_s_inst_id); 
	IF  @v_count_inst = 0 
	BEGIN
		SET @return_value=-1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
		La institución '+ @v_s_inst_id +' informada como sender institution no existe en el sistema.'
	END
	ELSE IF @v_s_ounit_code IS NOT NULL
	BEGIN
		--validacion de ounit del sender
		SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
			INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
			WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_s_inst_id)
			AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_s_ounit_code);
		IF  @v_count_ounit = 0 
		BEGIN
			SET @return_value=-1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
			La unidad organizativa '+ @v_s_ounit_code +' vinculada a ' + @v_s_inst_id  + ' no existe en el sistema.'
		END
	END 
		
	--validacion de institution del receiver	
	SELECT @v_count_inst = COUNT(1) FROM dbo.EWPCV_INSTITUTION_IDENTIFIERS WHERE UPPER(SCHAC) = UPPER(@v_r_inst_id); 
	IF  @v_count_inst = 0 
	BEGIN
		SET @return_value=-1
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
		La institución '+ @v_s_inst_id +' informada como receiver institution no existe en el sistema.'
	END
	ELSE IF @v_r_ounit_code IS NOT NULL
	BEGIN
		--validacion de institution del receiver
		SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
			INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
			WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_r_inst_id)
			AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_r_ounit_code);
		IF  @v_count_ounit = 0 
		BEGIN
			SET @return_value=-1
			SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
			La unidad organizativa '+ @v_r_ounit_code +' vinculada a ' + @v_s_inst_id  + ' no existe en el sistema.'
		END
	END 

END 
;
GO



/*
	Valida la calidad del dato de la entrada al aprovisionamiento de movilidades
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_MOBILITY'
) DROP PROCEDURE dbo.VALIDA_DATOS_MOBILITY;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_MOBILITY(@P_MOBILITY_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	
	EXECUTE dbo.VALIDA_MOBILITY_IIA @P_MOBILITY_XML, @P_ERROR_MESSAGE output, @return_value output
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_MOBILITY_INSTITUTION @P_MOBILITY_XML, @P_ERROR_MESSAGE output, @return_value output
	END 
	
END
;
GO
 
 
/*
	Valida que existan los institution id y ounit id de los academic term de los componentes del LA
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_ACADEMIC_TERM_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_ACADEMIC_TERM_INSTITUTION;
GO 
CREATE PROCEDURE  dbo.VALIDA_ACADEMIC_TERM_INSTITUTION(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
DECLARE @COMPONENTS_LIST xml
DECLARE @v_mobility_id uniqueidentifier
DECLARE @v_s_inst_id varchar(255)
DECLARE @v_r_inst_id varchar(255)

DECLARE @v_component_type integer
DECLARE @v_inst_id varchar(255)
DECLARE @v_ounit_code varchar(255)
DECLARE @v_count_inst integer
DECLARE @v_count_ounit integer

	SET @return_value=0
	IF @P_ERROR_MESSAGE IS NULL 
	BEGIN
	 SET @P_ERROR_MESSAGE = ''
	END
	
	--obtenemos datos del xml
	SELECT 
		@v_mobility_id =T.c.value('(mobility_id)[1]', 'uniqueidentifier'),
		@COMPONENTS_LIST = T.c.query('la_component_list')
	FROM @P_LA_XML.nodes('learning_agreement') T(c)
	
	--obtenemos sender y reciver de la ultima version de la movilidad
	SELECT @v_s_inst_id  = SENDING_INSTITUTION_ID, @v_r_inst_id = RECEIVING_INSTITUTION_ID 
	FROM EWPCV_MOBILITY 
	WHERE ID = @v_mobility_id AND MOBILITY_REVISION = (
		SELECT MAX(MOBILITY_REVISION) FROM EWPCV_MOBILITY 
		WHERE ID = @v_mobility_id)
	
	--cursor para extraer informacion de cada componente
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.value('(la_component_type)[1]', 'integer'),
		T.c.value('(academic_term/institution_id)[1]', 'varchar(255)'),
		T.c.value('(academic_term/organization_unit_code)[1]', 'varchar(255)')
		FROM @COMPONENTS_LIST.nodes('la_component_list/la_component') T(c)
	
	OPEN cur
	FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--componentes estudiados o virtuales
			IF @v_component_type = 0 OR @v_component_type = 5 
			BEGIN
				
				IF @v_inst_id != @v_r_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
					La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo estudiado/virtual no coincide con la institucion ' + @v_r_inst_id + ' de la entidad receptora'
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
						La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.'
					END
				END
			END
			--componentes reconocidos
			ELSE IF @v_component_type = 1
			BEGIN
				IF @v_inst_id != @v_s_inst_id
				BEGIN
					SET @return_value=-1
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
					La institucion ' + @v_inst_id + ' del componente del periodo academico de tipo reconocido no coincide con la institucion ' + @v_s_inst_id + ' de la entidad emisora'
				END
				ELSE IF @v_ounit_code is not null 
				BEGIN 
					SELECT @v_count_ounit = COUNT(1) FROM dbo.EWPCV_INSTITUTION INS
						INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
						INNER JOIN dbo.EWPCV_ORGANIZATION_UNIT OU ON IOU.ORGANIZATION_UNITS_ID = OU.ID
						WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@v_inst_id)
						AND UPPER(OU.ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
					IF  @v_count_ounit = 0 
					BEGIN
						SET @return_value=-1
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + '
						La unidad organizativa '+ @v_ounit_code +' vinculada a ' + @v_inst_id  + ' no existe en el sistema.'
					END
				END
			END
			FETCH NEXT FROM cur INTO  @v_component_type, @v_inst_id, @v_ounit_code
		END
	CLOSE cur
	DEALLOCATE cur

END 
;
GO

/*
	Valida la calidad del dato de la entrada al aprovisionamiento de learning agreements
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_LA'
) DROP PROCEDURE dbo.VALIDA_DATOS_LA;
GO 
CREATE PROCEDURE  dbo.VALIDA_DATOS_LA(@P_LA_XML XML, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as 
BEGIN 
	
	EXECUTE dbo.dbo.VALIDA_ACADEMIC_TERM_INSTITUTION @P_LA_XML, @P_ERROR_MESSAGE output, @return_value output
	
END
;
GO

/*
	Inserta el estudiante en la tabla de mobility participant si no existe ya en el sistema y devuelve el identificador generado.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY_PARTICIPANT'
) DROP PROCEDURE dbo.INSERTA_MOBILITY_PARTICIPANT;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY_PARTICIPANT(@P_STUDENT xml, @return_value varchar(255) output) AS
BEGIN
	DECLARE @v_s_id VARCHAR(255)
	DECLARE @v_c_id uniqueidentifier
	DECLARE @GLOBAL_ID varchar(255)
	DECLARE @CONTACT_PERSON xml

	SELECT 
		@GLOBAL_ID = T.c.value('(global_id)[1]','varchar(255)'),
		@CONTACT_PERSON = T.c.query('contact_person')
	FROM @P_STUDENT.nodes('student') T(c)

	SET @v_s_id = NEWID();				
	EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, null, null, @v_c_id output
	INSERT INTO EWPCV_MOBILITY_PARTICIPANT (ID, CONTACT_ID, GLOBAL_ID) VALUES (@v_s_id, @v_c_id, @GLOBAL_ID)


	SET @return_value = @v_s_id

END
;
GO

/*
	Persiste una movilidad en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_MOBILITY'
) DROP PROCEDURE dbo.INSERTA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.INSERTA_MOBILITY(@P_MOBILITY xml, @P_REVISION integer, @v_id uniqueidentifier output) AS
BEGIN
	DECLARE @v_s_ounit_id uniqueidentifier
	DECLARE @v_r_ounit_id uniqueidentifier
	DECLARE @v_student_id varchar(255)
	DECLARE @v_s_contact_id uniqueidentifier
	DECLARE @v_s_admv_contact_id uniqueidentifier
	DECLARE @v_r_contact_id uniqueidentifier
	DECLARE @v_r_admv_contact_id uniqueidentifier
	DECLARE @v_mobility_type_id uniqueidentifier
	DECLARE @v_lang_skill_id uniqueidentifier
	DECLARE @v_isced_code varchar(255)

	DECLARE @SENDING_INSTITUTION xml
	DECLARE @RECEIVING_INSTITUTION xml
	DECLARE @STUDENT xml
	DECLARE @SENDER_CONTACT xml
	DECLARE @SENDER_ADMV_CONTACT xml
	DECLARE @RECEIVER_CONTACT xml
	DECLARE @RECEIVER_ADMV_CONTACT xml
	DECLARE @SUBJECT_AREA xml
	DECLARE @MOBILITY_TYPE varchar(255)
	DECLARE @ACTUAL_ARRIVAL_DATE date
	DECLARE @ACTUAL_DEPATURE_DATE date
	DECLARE @COOPERATION_CONDITION_ID varchar(255)
	DECLARE @EQF_LEVEL integer
	DECLARE @IIA_ID varchar(255)
	DECLARE @PLANED_ARRIVAL_DATE date
	DECLARE @PLANED_DEPATURE_DATE date
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @STATUS integer
	DECLARE @STUDENT_LANGUAGE_SKILL_LIST xml
	DECLARE @STUDENT_LANGUAGE_SKILL xml

	SELECT 
		@SENDING_INSTITUTION = T.c.query('sending_institution'),
		@RECEIVING_INSTITUTION = T.c.query('receiving_institution'),
		@STUDENT = T.c.query('student'),
		@SENDER_CONTACT = T.c.query('sender_contact'),
		@SENDER_ADMV_CONTACT = T.c.query('sender_admv_contact'),
		@RECEIVER_CONTACT = T.c.query('receiver_contact'),
		@RECEIVER_ADMV_CONTACT = T.c.query('receiver_admv_contact'),
		@SUBJECT_AREA = T.c.query('subject_area'),
		@MOBILITY_TYPE = T.c.value('(mobility_type)[1]', 'varchar(255)'),
		@ACTUAL_ARRIVAL_DATE = T.c.value('(actual_arrival_date)[1]', 'date'),
		@ACTUAL_DEPATURE_DATE = T.c.value('(actual_depature_date)[1]', 'date'),
		@PLANED_ARRIVAL_DATE = T.c.value('(planed_arrival_date)[1]', 'date'),
		@PLANED_DEPATURE_DATE = T.c.value('(planed_depature_date)[1]', 'date'),
		@IIA_ID = T.c.value('(iia_id)[1]', 'varchar(255)'),
		@COOPERATION_CONDITION_ID = T.c.value('(cooperation_condition_id)[1]', 'varchar(255)'),
		@EQF_LEVEL = T.c.value('(eqf_level)[1]', 'integer'),
		@SENDING_INSTITUTION_ID = T.c.value('(sending_institution/institution_id)[1]', 'varchar(255)'),
		@RECEIVING_INSTITUTION_ID = T.c.value('(receiving_institution/institution_id)[1]', 'varchar(255)'),
		@STATUS = T.c.value('(status)[1]', 'integer'),
		@STUDENT_LANGUAGE_SKILL_LIST = T.c.query('student_language_skill_list')

	FROM @P_MOBILITY.nodes('mobility') T(c)

	EXECUTE dbo.INSERTA_INST_OUNIT @SENDING_INSTITUTION , @v_s_ounit_id output
	EXECUTE dbo.INSERTA_INST_OUNIT @RECEIVING_INSTITUTION , @v_r_ounit_id output
	EXECUTE dbo.INSERTA_MOBILITY_PARTICIPANT @STUDENT , @v_student_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_CONTACT , null, null, @v_s_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @SENDER_ADMV_CONTACT , null, null, @v_s_admv_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_CONTACT , null, null, @v_r_contact_id output
	EXECUTE dbo.INSERTA_CONTACT_PERSON @RECEIVER_ADMV_CONTACT , null, null, @v_r_admv_contact_id output

	EXECUTE dbo.INSERTA_SUBJECT_AREA @SUBJECT_AREA , @v_isced_code output
	EXECUTE dbo.INSERTA_MOBILITY_TYPE @MOBILITY_TYPE , @v_mobility_type_id output

	IF @v_id IS NULL 
	BEGIN 
		SET @v_id = NEWID()
	END

	INSERT INTO dbo.EWPCV_MOBILITY (ID,
			ACTUAL_ARRIVAL_DATE,
			ACTUAL_DEPARTURE_DATE,
			COOPERATION_CONDITION_ID,
			EQF_LEVEL,
			IIA_ID,
			ISCED_CODE,
			MOBILITY_PARTICIPANT_ID,
			MOBILITY_REVISION,
			PLANNED_ARRIVAL_DATE,
			PLANNED_DEPARTURE_DATE,
			RECEIVING_INSTITUTION_ID,
			RECEIVING_ORGANIZATION_UNIT_ID,
			SENDING_INSTITUTION_ID,
			SENDING_ORGANIZATION_UNIT_ID,
			STATUS,
			MOBILITY_TYPE_ID,
			SENDER_CONTACT_ID,
			SENDER_ADMV_CONTACT_ID,
			RECEIVER_CONTACT_ID,
			RECEIVER_ADMV_CONTACT_ID)
			VALUES (@v_id,
			@ACTUAL_ARRIVAL_DATE,
			@ACTUAL_DEPATURE_DATE,
			@COOPERATION_CONDITION_ID,
			@EQF_LEVEL,
			@IIA_ID,
			@v_isced_code,
			@v_student_id,
			(@P_REVISION +1),
			@PLANED_ARRIVAL_DATE,
			@PLANED_DEPATURE_DATE,
			@RECEIVING_INSTITUTION_ID,
			@v_r_ounit_id,
			@SENDING_INSTITUTION_ID,
			@v_s_ounit_id,
			@STATUS,
			@v_mobility_type_id,
			@v_s_contact_id,
			@v_s_admv_contact_id,
			@v_r_contact_id,
			@v_r_admv_contact_id)

	DECLARE cur CURSOR LOCAL FOR
		SELECT
			T.c.query('.')
		FROM @STUDENT_LANGUAGE_SKILL_LIST.nodes('student_language_skill_list/student_language_skill') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_SKILL @STUDENT_LANGUAGE_SKILL, @v_lang_skill_id output
			INSERT INTO dbo.EWPCV_MOBILITY_LANG_SKILL (MOBILITY_ID, MOBILITY_REVISION, LANGUAGE_SKILL_ID) VALUES (@v_id, (@P_REVISION + 1), @v_lang_skill_id)
			FETCH NEXT FROM cur INTO @STUDENT_LANGUAGE_SKILL
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
	Actualiza una movilidad 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_MOBILITY'
) DROP PROCEDURE dbo.ACTUALIZA_MOBILITY;
GO 
CREATE PROCEDURE  dbo.ACTUALIZA_MOBILITY(@P_OMOBILITY_ID uniqueidentifier, @P_MOBILITY xml) AS 
BEGIN

	DECLARE @v_m_revision VARCHAR(255);
	SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID;
	
	--insertamos la nueva revision de la mobility
	EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, @v_m_revision, @P_OMOBILITY_ID OUTPUT
	
	-- Actualizamos la tabla de relaciones EWPCV_MOBILITY_LA y vinculamos el LA a la nueva revisión de la movilidad
    UPDATE dbo.EWPCV_MOBILITY_LA SET MOBILITY_ID = @P_OMOBILITY_ID, MOBILITY_REVISION = (@v_m_revision + 1) WHERE MOBILITY_ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision;
END 
;
GO

/* Inserta una movilidad en el sistema, habitualmente se empleará para el proceso de nominaciones previo a los learning agreements.
	Admite un objeto de tipo MOBILITY con toda la informacion de la movilidad.
	Admite un parametro de salida con el identificador de la movilidad en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_MOBILITY'
) DROP PROCEDURE dbo.INSERT_MOBILITY;
GO 
CREATE PROCEDURE dbo.INSERT_MOBILITY(@P_MOBILITY xml, @P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) AS
BEGIN

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			EXECUTE dbo.INSERTA_MOBILITY @P_MOBILITY, -1, @P_OMOBILITY_ID OUTPUT
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO



/* Actualiza una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a actualizar
	Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_MOBILITY'
) DROP PROCEDURE dbo.UPDATE_MOBILITY;
GO 
CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID uniqueidentifier OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE
		BEGIN
			IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			BEGIN
				SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
                SET @return_value = -1
			END
 		END
	END 
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_sending_hei
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/* Inserta un learning agreement en el sistema, habitualmente la movilidad ya estará registrada por un proceso de nominacion previo.
	Admite un objeto de tipo LEARNING_AGREEMENT con toda la informacion de la movilidad y de los componentes asociados.
	El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
	Admite un parametro de salida con el identificador LEARNING_AGREEMENT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.INSERT_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.INSERT_LEARNING_AGREEMENT(@P_LA XML, @P_LA_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @SENDING_INSTITUTION_ID varchar(255)
	DECLARE @RECEIVING_INSTITUTION_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_m_revision integer
	DECLARE @v_la_id uniqueidentifier
	DECLARE @MOBILITY_ID varchar(255)

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output
	
	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
					FROM dbo.EWPCV_MOBILITY 
					WHERE ID = @MOBILITY_ID
		IF @v_m_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_la_id = LEARNING_AGREEMENT_ID 
					FROM dbo.EWPCV_MOBILITY_LA 
					WHERE MOBILITY_ID = @MOBILITY_ID
		IF @v_la_id IS NOT NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'Ya existe un Learning Agreement asociado a la mobilidad indicada, por favor actualice o elimine el learning agreement: ' + CAST(@v_la_id as varchar(255)) + '.'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		--validamos que los institution y ounit de los componentes sean correctos
		EXECUTE dbo.VALIDA_DATOS_LA @P_LA, @P_ERROR_MESSAGE output, @return_value output
	END
	
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

				EXECUTE dbo.INSERTA_LA_REVISION NULL, @P_LA, 0, @v_m_revision, @P_LA_ID output
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion receptora'
					SET @P_LA_ID = NULL
					SET @return_value = -1
					ROLLBACK TRANSACTION
				END 
				ELSE
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					COMMIT TRANSACTION
				END
		END TRY
		
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/* Actualiza un learning agreement del sistema. Esta operacion genera una nueva revision del LA que requerira ser firmada por las 3 partes.
	Recibe como parametro el identificador del learning agreement a actualizar.
	El objeto del estudiante se debe generar únicamente si ha habido modificaciones sobre los datos de la movilidad
	Admite un objeto de tipo LEARNING_AGREEMENT con la informacion actualizada de la movilidad y de los componentes asociados.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_LEARNING_AGREEMENT'
) DROP PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT;
GO 
CREATE PROCEDURE dbo.UPDATE_LEARNING_AGREEMENT(@P_LA_ID uniqueidentifier, @P_LA XML, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_la_id uniqueidentifier
	DECLARE @v_la_revision integer
	DECLARE @v_m_revision integer
	DECLARE @v_no_firmado integer
	DECLARE @MOBILITY_ID varchar(255)
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_la_revision_siguiente integer

	SELECT 
		@MOBILITY_ID = T.c.value('(mobility_id)[1]', 'uniqueidentifier')
	FROM @P_LA.nodes('learning_agreement') T(c)

	EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @P_LA, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END
	END
	   	 
	IF @return_value = 0
	BEGIN
		SELECT @v_la_revision = MAX(LEARNING_AGREEMENT_REVISION)
			FROM EWPCV_LEARNING_AGREEMENT 
			WHERE ID = @P_LA_ID
		IF @v_la_revision IS NULL  
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado no existe'
			SET @return_value = -1
		END
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_m_revision = MAX(MOBILITY_REVISION)
			FROM dbo.EWPCV_MOBILITY_LA
				WHERE MOBILITY_ID = @MOBILITY_ID
				AND LEARNING_AGREEMENT_ID = @P_LA_ID
				AND LEARNING_AGREEMENT_REVISION = @v_la_revision;
		IF @v_m_revision IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad indicada para el learning agreement no es correcta.'
			SET @return_value = -1
		END 
	END

	IF @return_value = 0
	BEGIN
		SELECT @v_no_firmado = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT WHERE ID = @P_LA_ID AND STATUS = 0
		IF @v_no_firmado > 0
		BEGIN
			SET @P_ERROR_MESSAGE = 'El learning agreement indicado tiene cambios pendientes de revisar, no se puede actualizar.'
			SET @return_value = -1
		END 
	END


	EXECUTE dbo.OBTEN_NOTIFIER_HEI_LA @P_LA_ID, @v_la_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
	IF @v_notifier_hei IS NULL
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se pueden actualizar acuerdos de aprendizaje para las cuales no se es el propietario. Unicamente se pueden actualizar acuerdos de aprendizaje de tipo outgoing.'
		SET @return_value = -1
	END
	
	IF @return_value = 0
	BEGIN
		--validamos que los institution y ounit de los componentes sean correctos
		EXECUTE dbo.VALIDA_DATOS_LA @P_LA, @P_ERROR_MESSAGE output, @return_value output
	END
	
	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			SET @v_la_revision_siguiente = @v_la_revision + 1
			EXECUTE dbo.INSERTA_LA_REVISION @P_LA_ID, @P_LA, @v_la_revision_siguiente, @v_m_revision, @v_la_id output
			EXECUTE dbo.INSERTA_NOTIFICATION @MOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_notifier_hei
					   			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_LEARNING_AGREEMENT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO


/*
	****************************************
	  FIN MODIFICACIONES SOBRE MOBILITY_LA 
	****************************************
*/

/*
	****************************************
	INICIO MODIFICACIONES SOBRE INSTITUTIONS 
	****************************************
*/



/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA INSTITUTION

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_INSTITUTION' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_INSTITUTION
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="institution">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="institution_name" type="languageItemListType" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="university_contact_details" minOccurs="0" >
					<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="languageItemListType"  minOccurs="0" />
				<xs:element name="logo_url" type="xs:string"  minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_INSTITUTION'
) DROP PROCEDURE dbo.VALIDA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.VALIDA_INSTITUTION(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_INSTITUTION)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO
	
/*
	Borra una institución
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INSTITUTION'
) DROP PROCEDURE dbo.BORRA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.BORRA_INSTITUTION(@P_INSTITUTION_ID varchar(255)) AS
BEGIN
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @v_inst_id_interop uniqueidentifier
	DECLARE @URL_ID uniqueidentifier
	DECLARE @CONTACT_ID uniqueidentifier

	SELECT 
		@v_inst_id_interop = ID,
		@v_factsheet_id = FACT_SHEET
		FROM EWPCV_INSTITUTION
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);

	UPDATE EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE UPPER(INSTITUTION_ID) = @P_INSTITUTION_ID;     
	
	EXECUTE dbo.BORRA_INSTITUTION_NAMES @v_inst_id_interop
	
	--Borrado de URLs de factsheet y factsheet
	EXECUTE dbo.BORRA_FACTSHEET_URL @v_factsheet_id
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @v_factsheet_id
        
	--Borrado de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM EWPCV_CONTACT 
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE dbo.BORRA_CONTACT @CONTACT_ID
			FETCH NEXT FROM cur INTO @CONTACT_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
	--Borro la institucion
	DELETE FROM dbo.EWPCV_INSTITUTION WHERE ID = @v_inst_id_interop

END;
GO
	
/* 
	Elimina una INSTITUTION del sistema.
	Recibe como parametros: 
    El identificador (SCHAC) de la INSTITUTION
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_INSTITUTION'
) DROP PROCEDURE dbo.DELETE_INSTITUTION;
GO 
CREATE PROCEDURE dbo.DELETE_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer

		SET @return_value = 0
		--validacion existe institucion
		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION WHERE INSTITUTION_ID = @P_INSTITUTION_ID
		IF @v_count = 0 
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = 'La institución no existe'
			END 
			ELSE 
			BEGIN
				--validacion intitucion	sin factsheet
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
					AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + 'La institución tiene un factsheet asociado, bórrelo antes de borrar la institución'
					END
				--validacion intitucion	sin ounits
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_INSTITUTION INS
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT IOU ON INS.ID = IOU.INSTITUTION_ID
					WHERE UPPER(INS.INSTITUTION_ID) = @P_INSTITUTION_ID
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE + 'La institución tiene unidades organizativas dependientes, bórrelas antes de borrar la institución'
					END
					
				IF @return_value = 0
					BEGIN
						EXECUTE BORRA_INSTITUTION @P_INSTITUTION_ID
					END		
			END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_INSTITUTION'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END 
;
GO
	
/* 
	Inserta una INSTITUTION en el sistema
	Admite un objeto de tipo INSTITUTION con toda la informacion de la Institution, sus contactos y sus factsheet
	Admite un parametro de salida con el identificador de la INSTITUTION en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/ 
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_INSTITUTION'
) DROP PROCEDURE dbo.INSERT_INSTITUTION;
GO    
CREATE PROCEDURE dbo.INSERT_INSTITUTION(@P_INSTITUTION xml, @P_INSTITUTION_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	EXECUTE dbo.VALIDA_INSTITUTION @P_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			DECLARE @v_prim_contact_id uniqueidentifier
			DECLARE @v_count integer
			DECLARE @v_contact_id uniqueidentifier
			DECLARE @v_factsheet_id uniqueidentifier
			DECLARE @CONTACT_PERSON xml

			DECLARE @INSTITUTION_ID varchar(255)
			DECLARE @ABBREVIATION varchar(255)
			DECLARE @LOGO_URL varchar(255)
			DECLARE @INSTITUTION_NAME_LIST xml
			DECLARE @UNIVERSITY_CONTACT_DETAILS xml

			SELECT 
				@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
				@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
				@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
				@INSTITUTION_NAME_LIST = T.c.query('institution_name'),
				@UNIVERSITY_CONTACT_DETAILS = T.c.query('university_contact_details')
			FROM @P_INSTITUTION.nodes('institution') T(c)
			
			-- Insertamos el contacto principal
			EXECUTE dbo.INSERTA_CONTACT @UNIVERSITY_CONTACT_DETAILS, @INSTITUTION_ID, NULL, @v_prim_contact_id OUTPUT

			-- Insertamos la lista de contactos
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_INSTITUTION.nodes('institution/contact_details_list/contact_person') T(c)
			OPEN cur
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, NULL, @v_contact_id OUTPUT
					FETCH NEXT FROM cur INTO @CONTACT_PERSON
				END
			CLOSE cur
			DEALLOCATE cur

			-- Insertamos la lista de fact sheet urls
			EXECUTE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS @P_INSTITUTION, @v_factsheet_id OUTPUT
			
			-- Insertamos la INSTITUTION en EWPCV_INSTITUTION
			EXECUTE dbo.INSERTA_INSTITUTION @INSTITUTION_ID, @ABBREVIATION, @LOGO_URL, @v_factsheet_id, @INSTITUTION_NAME_LIST, @v_prim_contact_id, @P_INSTITUTION_ID OUTPUT

			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_INSTITUTION'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	Inserta las urls de factsheet de la institución  y devuelve el identificador del factsheet generado o existente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_INSTITUTION_FACTSHEET_URLS'
) DROP PROCEDURE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS;
GO 
CREATE PROCEDURE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_lang_item_id VARCHAR(255)
	DECLARE @v_institution_id VARCHAR(255)
	DECLARE @v_factsheet_id VARCHAR(255)
	DECLARE @LANG VARCHAR(255)
	DECLARE @TEXT VARCHAR(255)
	DECLARE @FACTSHEET_URL xml

	SELECT 	@v_institution_id = T.c.value('(institution_id)[1]', 'varchar(255)') FROM @P_INSTITUTION.nodes('institution') T(c);
			
	SELECT @v_factsheet_id = FACT_SHEET FROM EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = @v_institution_id;
	
	SELECT  @TEXT = T.c.value('(.)[1]', 'varchar(255)') FROM @P_INSTITUTION.nodes('institution/factsheet_url_list/text') T(c)
	IF @TEXT IS NOT NULL
	BEGIN
		IF @v_factsheet_id IS NULL
		BEGIN
			SET @v_factsheet_id = NEWID()
			INSERT INTO EWPCV_FACT_SHEET (ID) VALUES (@v_factsheet_id)
		END

		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.value('(@lang)[1]', 'varchar(255)'),
			T.c.value('(.)[1]', 'varchar(255)'),
			T.c.query('.')
		FROM @P_INSTITUTION.nodes('institution/factsheet_url_list/text') T(c)
		
		OPEN cur
		FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_lang_item_id = NULL

				SELECT @v_lang_item_id = ID
					FROM dbo.EWPCV_LANGUAGE_ITEM LAIT 
					INNER JOIN dbo.EWPCV_FACT_SHEET_URL FSU ON FSU.URL_ID = LAIT.ID
					WHERE UPPER(LAIT.LANG) = UPPER(@LANG)
					AND UPPER(LAIT.TEXT) = UPPER(@TEXT)
					AND FSU.FACT_SHEET_ID = @v_factsheet_id

				IF @v_lang_item_id IS NULL
					BEGIN
						execute dbo.INSERTA_LANGUAGE_ITEM @FACTSHEET_URL,  @v_lang_item_id OUTPUT
					INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (@v_lang_item_id, @v_factsheet_id)
						SET @v_lang_item_id = NULL
					END

				FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
			END
		CLOSE cur
		DEALLOCATE cur
	END

	SET @return_value = @v_factsheet_id
	
END
;
GO


 /*  
 Actualiza una institucion
 */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_INSTITUTION'
) DROP PROCEDURE dbo.ACTUALIZA_INSTITUTION;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION xml) AS
BEGIN
	DECLARE @v_prim_contact_id uniqueidentifier
	DECLARE @v_contact_pr_id uniqueidentifier
	DECLARE @v_cd_id uniqueidentifier
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @v_institution_id uniqueidentifier
	DECLARE @v_contact_id uniqueidentifier
	
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @ABBREVIATION VARCHAR(255)
	DECLARE @LOGO_URL VARCHAR(255)
	DECLARE @INSTITUTION_NAME_LIST xml
	DECLARE @UNIVERSITY_CONTACT_DETAILS xml
	DECLARE @INSTITUTION_URL_LIST xml
	DECLARE @CONTACT_DETAILS_LIST xml
	DECLARE @FACTSHEET_URL_LIST xml
	DECLARE @CONTACT_PERSON xml

	DECLARE @ID uniqueidentifier

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@INSTITUTION_NAME_LIST = T.c.query('institution_name'),
		@INSTITUTION_URL_LIST = T.c.query('institution_url_list'),
		@UNIVERSITY_CONTACT_DETAILS = T.c.query('university_contact_details'),
		@FACTSHEET_URL_LIST = T.c.query('factsheet_url_list'),
		@CONTACT_DETAILS_LIST = T.c.query('contact_details_list')
	FROM @P_INSTITUTION.nodes('institution') T(c)

	
	SELECT @v_institution_id = ID, @v_factsheet_id = FACT_SHEET
			FROM dbo.EWPCV_INSTITUTION
			WHERE UPPER(INSTITUTION_ID)= UPPER(@P_INSTITUTION_ID)

	UPDATE dbo.EWPCV_INSTITUTION SET PRIMARY_CONTACT_DETAIL_ID = NULL WHERE ID = @v_institution_id

	--borramos los nombres que tenia y los reemplazamos por los nuevos
    EXECUTE dbo.BORRA_INSTITUTION_NAMES @v_institution_id
    EXECUTE dbo.INSERTA_INSTITUTION_NAMES @v_institution_id, @INSTITUTION_NAME_LIST
	
	--borramos los contactos antiguos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT
		WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) 
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_CONTACT @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

	-- Insertamos la nueva lista de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @CONTACT_DETAILS_LIST.nodes('contact_details_list/contact_person') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_PERSON
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, NULL, @v_contact_id OUTPUT
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
		END
	CLOSE cur
	DEALLOCATE cur
	
	--insertamos el nuevo contacto principal
	EXECUTE dbo.INSERTA_CONTACT @UNIVERSITY_CONTACT_DETAILS, @INSTITUTION_ID, NULL, @v_prim_contact_id OUTPUT
	
    --borramos las urls de factsheet que tenia y las reemplazamos por las nuevas
	EXECUTE dbo.BORRA_FACTSHEET_URL @v_factsheet_id    
	--se borra el registro si es un dummy
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @v_factsheet_id AND CONTACT_DETAILS_ID IS NULL;
	EXECUTE dbo.INSERTA_INSTITUTION_FACTSHEET_URLS @P_INSTITUTION, @v_factsheet_id OUTPUT
        
    UPDATE dbo.EWPCV_INSTITUTION SET 
        PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id, ABBREVIATION = @ABBREVIATION, LOGO_URL = @LOGO_URL, FACT_SHEET = @v_factsheet_id
        WHERE ID = @v_institution_id;
END 
;
GO


/* 
	Actualiza una INSTITUTION del sistema.
    Recibe como parametros: 
    El identificador (ID de interoperabilidad) de la INSTITUTION
	Admite un objeto de tipo INSTITUTION con toda la informacion actualizada de la Institution, sus contactos y sus factsheet
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_INSTITUTION'
) DROP PROCEDURE dbo.UPDATE_INSTITUTION;
GO 
CREATE PROCEDURE dbo.UPDATE_INSTITUTION(@P_INSTITUTION_ID varchar(255), @P_INSTITUTION xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	
	DECLARE @v_count integer
	SET @return_value = 0;
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID)
	IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_INSTITUTION @P_INSTITUTION, @P_ERROR_MESSAGE output, @return_value output
			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
					EXECUTE dbo.ACTUALIZA_INSTITUTION @P_INSTITUTION_ID, @P_INSTITUTION
					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH
					THROW
					SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_INSTITUTION'
					SET @return_value = -1
					ROLLBACK TRANSACTION
				END CATCH
			END;
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La institución no existe'
		SET @return_value = -1
	END 
END
;
GO

/*
    Borra los nombres de la institution
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_INSTITUTION_NAMES'
) DROP PROCEDURE dbo.BORRA_INSTITUTION_NAMES;
GO 
CREATE PROCEDURE dbo.BORRA_INSTITUTION_NAMES (@P_INST_ID_INTEROP uniqueidentifier) AS
BEGIN
	DECLARE @NAME_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT NAME_ID
			FROM dbo.EWPCV_INSTITUTION_NAME
			WHERE INSTITUTION_ID = @P_INST_ID_INTEROP

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            DELETE FROM dbo.EWPCV_INSTITUTION_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO



/*
	****************************************
	  FIN MODIFICACIONES SOBRE INSTITUTIONS 
	****************************************
*/

/*
	****************************************
	  INICIO MODIFICACIONES SOBRE OUNITS 
	****************************************
*/



/*

	VALIDACION VIA XSD

*/

-- GENERAR EL TIPO XML SCHEMA COLLECTION PARA OUNIT

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_OUNIT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_OUNIT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>
	
	<xs:simpleType name="ceroOneType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
						<xs:element name="fax" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:element name="organization_unit">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="institution_id" type="notEmptyStringType" />
				<xs:element name="organization_unit_code" type="notEmptyStringType" />
				<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
				<xs:element name="abbreviation" type="xs:string" minOccurs="0" />
				<xs:element name="parent-ounit-id" type="xs:string" minOccurs="0" />
				<xs:element name="is-tree-structure" type="ceroOneType" minOccurs="1" />
				<xs:element name="ounit_contact_details" minOccurs="0" >
				<xs:complexType>
						<xs:sequence>
							<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="mailing_address" type="flexibleAddressType" minOccurs="0" />
							<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="contact_details_list" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="contact_person" type="contactPersonType"  maxOccurs="unbounded" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="factsheet_url_list" type="languageItemListType"  minOccurs="0" />
				<xs:element name="logo_url" type="xs:string"  minOccurs="0" />
				
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';
GO

-- FUNCION DE VALIDACION
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_OUNIT'
) DROP PROCEDURE dbo.VALIDA_OUNIT;
GO 
CREATE PROCEDURE dbo.VALIDA_OUNIT(@XML xml, @errMsg varchar(255) output, @errNum integer output) as
BEGIN
DECLARE @TYPED_XML XML(dbo.EWPCV_XML_OUNIT)
DECLARE @UNTYPED_XML XML
SET @UNTYPED_XML = @XML
	BEGIN TRY
	  SELECT @TYPED_XML = @UNTYPED_XML
	  SET @errNum = 0
	END TRY
	BEGIN CATCH
	  SELECT @errMsg=ERROR_MESSAGE()
	  SET @errNum = -1
	END CATCH
END
;

GO

/*
	Borra una ounit 
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_OUNIT'
) DROP PROCEDURE dbo.BORRA_OUNIT;
GO 
CREATE PROCEDURE dbo.BORRA_OUNIT(@P_OUNIT_ID varchar(255)) AS
BEGIN

	DECLARE @v_ou_id uniqueidentifier
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @CONTACT_ID uniqueidentifier

	SELECT 
		@v_ou_id = ID,
		@v_factsheet_id = FACT_SHEET
		FROM dbo.EWPCV_ORGANIZATION_UNIT
		WHERE UPPER(ID) = UPPER(@P_OUNIT_ID);

	UPDATE EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE ID = @v_ou_id;     
	
	EXECUTE dbo.BORRA_OUNIT_NAMES @v_ou_id;
	
	--Borrado de URLs de factsheet y factsheet
	EXECUTE dbo.BORRA_FACTSHEET_URL @v_factsheet_id
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @v_factsheet_id
        
	--Borrado de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT 
		WHERE ORGANIZATION_UNIT_ID = @v_ou_id
	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE dbo.BORRA_CONTACT @CONTACT_ID
			FETCH NEXT FROM cur INTO @CONTACT_ID
		END
	CLOSE cur
	DEALLOCATE cur
	
	--Borro la ounit y su relacion con institution
	DELETE FROM dbo.EWPCV_INST_ORG_UNIT WHERE ORGANIZATION_UNITS_ID = @v_ou_id;
	DELETE FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE ID = @v_ou_id

END;
GO
	
/* Elimina una OUNIT del sistema.
	Recibe como parametros: 
	El identificador de la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_OUNIT'
) DROP PROCEDURE dbo.DELETE_OUNIT;
GO 
CREATE PROCEDURE dbo.DELETE_OUNIT(@P_OUNIT_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer

		SET @return_value = 0
		--validacion existe OUNIT
		SELECT @v_count = COUNT(*) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(ID) = UPPER(@P_OUNIT_ID);
		IF @v_count = 0 
			BEGIN
				SET @return_value = -1
				SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
			END 
			ELSE 
			BEGIN
				--validacion ounit sin ounits hijas
				SELECT @v_count = COUNT(*) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(PARENT_OUNIT_ID) = UPPER(@P_OUNIT_ID);
				IF @v_count > 0 
					BEGIN
						SET @return_value = -1
						SET @P_ERROR_MESSAGE = 'No se puede borrar una unidad organizativa padre sin antes borrar los hijos'
					END
					
				IF @return_value = 0
					BEGIN
						EXECUTE BORRA_OUNIT @P_OUNIT_ID
					END		
			END
		

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_OUNIT'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH

END 
;
GO
	
	
 /*
	Persiste una ounit en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_COMPLETA'
) DROP PROCEDURE dbo.INSERTA_OUNIT_COMPLETA;
GO    
CREATE PROCEDURE dbo.INSERTA_OUNIT_COMPLETA(@P_OUNIT xml, @P_FACTSHEET_ID uniqueidentifier, @P_PRIM_CONT_DETAIL_ID uniqueidentifier, @P_OUNIT_ID uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_inst_id varchar(255)
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @ABBREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)
	DECLARE @OUNIT_NAME_LIST xml

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer'),
		@OUNIT_NAME_LIST = T.c.query('organization_unit_name')
	FROM @P_OUNIT.nodes('organization_unit') T(c)
	
	SELECT @v_inst_id = ID FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
	
	SET @P_OUNIT_ID = NEWID();
	INSERT INTO EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE, ABBREVIATION, LOGO_URL, FACT_SHEET, PRIMARY_CONTACT_DETAIL_ID, IS_TREE_STRUCTURE, PARENT_OUNIT_ID) 
                                     VALUES (@P_OUNIT_ID, @OUNIT_CODE, @ABBREVIATION, @LOGO_URL, @P_FACTSHEET_ID, @P_PRIM_CONT_DETAIL_ID, @IS_TREE_STRUCTURE, @PARENT_OUNIT_ID); 
	INSERT INTO EWPCV_INST_ORG_UNIT (ORGANIZATION_UNITS_ID, INSTITUTION_ID) VALUES (@P_OUNIT_ID, @v_inst_id);                                     
		
	EXECUTE dbo.INSERTA_OUNIT_NAMES @P_OUNIT_ID, @OUNIT_NAME_LIST;
END;
GO	
	
/* 
	Valida los datos introducidos en el xml
	La institucion indicada debe exisitir
	El ounit code no se puede repetir en la misma institucion
	No se pueden mezclar estructuras de representacion de unidades organizativas, o todas van sueltas o todas van en estructura de arbol
	La estructura de representacion de unidades organizativas solo debe tener un root si se representa como arbol
	El ounit code indicado como padre debe existir asociado a la institucion
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'VALIDA_DATOS_OUNIT'
) DROP PROCEDURE dbo.VALIDA_DATOS_OUNIT;
GO    
CREATE PROCEDURE dbo.VALIDA_DATOS_OUNIT(@P_OUNIT xml, @P_OUNIT_ID UNIQUEIDENTIFIER, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	SET @return_value = 0;
	DECLARE @v_existing_ounit_code varchar(255) = 'v_existing_ounit_code'
	DECLARE @v_existing_parent uniqueidentifier
	DECLARE @v_count integer
	DECLARE @v_existing_tree_str integer
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)

	IF @P_ERROR_MESSAGE IS NULL 
		SET @P_ERROR_MESSAGE = '';
	
	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer')
	FROM @P_OUNIT.nodes('organization_unit') T(c)
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
	IF @v_count = 0
	BEGIN
		SET @return_value = -1;
		SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
			No existe ninguna institución con el código SCHAC ' + @INSTITUTION_ID;
	END
	ELSE
	BEGIN
		--Si es actualizacion no se puede setear padre a si mismo
		IF @P_OUNIT_ID IS NOT NULL AND @IS_TREE_STRUCTURE = 1  AND @PARENT_OUNIT_ID = @P_OUNIT_ID
		BEGIN
			SET @return_value = -1;
				SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
					No se puede establecer como padre de una unidad organizativa a si misma';
		END 
		ELSE 
		BEGIN
			--	si es actualizacion	si era root no se puede setear padre
			IF @P_OUNIT_ID IS NOT NULL
			BEGIN
				SELECT @v_existing_ounit_code = ORGANIZATION_UNIT_CODE, @v_existing_parent = PARENT_OUNIT_ID FROM EWPCV_ORGANIZATION_UNIT WHERE ID = @P_OUNIT_ID;
			END
		
			IF @P_OUNIT_ID IS NOT NULL AND @v_existing_parent IS NULL AND @PARENT_OUNIT_ID IS NOT NULL AND @IS_TREE_STRUCTURE = 1
			BEGIN 
				SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Esta actualizando una unidad organizativa declarada como root asignandole una unidad organizativa padre';
			END 
			ELSE IF @P_OUNIT_ID IS NOT NULL AND @v_existing_parent IS NOT NULL AND @PARENT_OUNIT_ID IS NULL AND @IS_TREE_STRUCTURE = 1
			BEGIN
				SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Esta tratando de convertir en root una unidad organizativa que no lo era, actualice el root en su lugar';
			END 
			ELSE
			BEGIN
				SELECT @v_count = COUNT(1)
				FROM dbo.EWPCV_ORGANIZATION_UNIT ou
				INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.ID
				INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
				WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
				AND UPPER(ou.ORGANIZATION_UNIT_CODE) = UPPER(@OUNIT_CODE);
				
				--	si es actualizacion el ounit code que viene no este duplicado solo si no es el que ya tenia
				IF @v_count > 0 AND UPPER(@v_existing_ounit_code) != UPPER(@OUNIT_CODE)
				BEGIN 
					SET @return_value = -1;
					SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
						Ya existe una unidad organizativa con el código ' + @OUNIT_CODE + ' para la institución ' + @INSTITUTION_ID;
				END
				ELSE
				BEGIN
					SELECT @v_count = COUNT(DISTINCT(ou.IS_TREE_STRUCTURE))
					FROM dbo.EWPCV_ORGANIZATION_UNIT ou
					INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.ID
					INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
					WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID);

					IF @v_count > 1
					BEGIN 
						SET @return_value = -1;
						SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
							Existen unidades organizativas con estructura de árbol y sin estructura de árbol para la institucion indicada, la BBDD necesita revisión.';
					END
					ELSE
					BEGIN
						SELECT @v_existing_tree_str = ou.IS_TREE_STRUCTURE
							FROM dbo.EWPCV_ORGANIZATION_UNIT ou
							INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.ORGANIZATION_UNITS_ID = ou.ID
							INNER JOIN dbo.EWPCV_INSTITUTION i ON i.ID = iou.INSTITUTION_ID
							WHERE UPPER(i.INSTITUTION_ID) = UPPER(@INSTITUTION_ID);
						IF @v_existing_tree_str != @IS_TREE_STRUCTURE
						BEGIN 
							SET @return_value = -1;
							SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
								Ya hay unidades organizativas introducidas para esta universidad con diferente estructura de representacion a la indicada: ' + @IS_TREE_STRUCTURE;
						END
						ELSE
						BEGIN
							IF @IS_TREE_STRUCTURE = 0 AND @PARENT_OUNIT_ID IS NOT NULL
							BEGIN
								SET @return_value = -1;
								SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
									La estructura de representacion de la unida organizativa no está como formato árbol. El atributo PARENT_OUNIT_ID debe ser nulo.';
							END
							ELSE IF @IS_TREE_STRUCTURE = 1 AND @PARENT_OUNIT_ID IS NULL
							BEGIN
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
								INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND ou.PARENT_OUNIT_ID IS NULL;
								--En caso de actualizacion no se valida este paso porque esta validado antes
								IF @v_count > 0 AND @P_OUNIT_ID IS NULL
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										Ya existe una unidad organizativa informada como "root"';
								END   
							END
							ELSE IF @IS_TREE_STRUCTURE = 1 AND @PARENT_OUNIT_ID IS NOT NULL
							BEGIN
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
								INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) AND ou.PARENT_OUNIT_ID IS NULL;
								IF @v_count = 0 
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										No existe ninguna unidad organizativa informada como root, inserte primero el "root".';
								END 
							
								SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INST_ORG_UNIT iou
								INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
								WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID) 
								AND UPPER(iou.ORGANIZATION_UNITS_ID) = UPPER(@PARENT_OUNIT_ID);
								IF @v_count = 0 
								BEGIN
									SET @return_value = -1;
									SET @P_ERROR_MESSAGE = @P_ERROR_MESSAGE +'
										La unidad organizativa informada como padre no existe.';
								END 
							END
						END
					END
				END
			END
		END
	END
END;
GO

	
/* Inserta una OUNIT en el sistema
	Admite un objeto de tipo OUNIT con toda la informacion de la Ounit, sus contactos y sus factsheet
	Admite un parametro de salida con el identificador de la OUNIT en el sistema de interoperabilidad que sera necesario almacenar para futuras operaciones de modificacion o eliminacion.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERT_OUNIT'
) DROP PROCEDURE dbo.INSERT_OUNIT;
GO    
CREATE PROCEDURE dbo.INSERT_OUNIT(@P_OUNIT xml, @P_OUNIT_ID uniqueidentifier OUTPUT, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	EXECUTE dbo.VALIDA_OUNIT @P_OUNIT, @P_ERROR_MESSAGE output, @return_value output
	IF @return_value=0
	BEGIN
		
		DECLARE @v_prim_contact_id uniqueidentifier
		DECLARE @v_count integer
		DECLARE @v_contact_id uniqueidentifier
		DECLARE @v_factsheet_id uniqueidentifier
		DECLARE @INSTITUTION_ID varchar(255)
		DECLARE @OUNIT_CODE varchar(255)
		DECLARE @CONTACT_PERSON xml
		DECLARE @OUNIT_CONTACT_DETAILS xml

		SELECT 
			@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
			@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'), 
			@OUNIT_CONTACT_DETAILS = T.c.query('ounit_contact_details')
		FROM @P_OUNIT.nodes('organization_unit') T(c)
		
		
		EXECUTE dbo.VALIDA_DATOS_OUNIT @P_OUNIT, null, @P_ERROR_MESSAGE OUTPUT, @return_value OUTPUT
		
		IF @return_value = 0 
		BEGIN TRY
		BEGIN TRANSACTION
		
			-- Insertamos la OUNIT en OUNIT
			EXECUTE dbo.INSERTA_OUNIT_COMPLETA @P_OUNIT, null, null, @P_OUNIT_ID OUTPUT;
			
			-- Insertamos el contacto principal
			EXECUTE dbo.INSERTA_CONTACT @OUNIT_CONTACT_DETAILS, @INSTITUTION_ID, @OUNIT_CODE, @v_prim_contact_id OUTPUT;

			-- Insertamos la lista de contactos
			DECLARE cur CURSOR LOCAL FOR
			SELECT 
				T.c.query('.')
			FROM @P_OUNIT.nodes('organization_unit/contact_details_list/contact_person') T(c);
			OPEN cur;
			FETCH NEXT FROM cur INTO @CONTACT_PERSON;
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, @OUNIT_CODE, @v_contact_id OUTPUT;
					FETCH NEXT FROM cur INTO @CONTACT_PERSON;
				END
			CLOSE cur;
			DEALLOCATE cur;

			-- Insertamos la lista de fact sheet urls
			EXECUTE dbo.INSERTA_OUNIT_FACTSHEET_URLS @P_OUNIT, @v_factsheet_id OUTPUT;
			
			-- Actualizamos la OUNIT en OUNIT
			UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id, FACT_SHEET = @v_factsheet_id WHERE ID = @P_OUNIT_ID ;

			COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.INSERT_INSTITUTION'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH

	END
END
;
GO

/*
	Inserta las urls de factsheet de la ounit y devuelve el identificador del factsheet generado o existente
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_OUNIT_FACTSHEET_URLS'
) DROP PROCEDURE dbo.INSERTA_OUNIT_FACTSHEET_URLS;
GO 
CREATE PROCEDURE dbo.INSERTA_OUNIT_FACTSHEET_URLS(@P_OUNIT xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_lang_item_id VARCHAR(255)
	DECLARE @v_institution_id VARCHAR(255)
	DECLARE @v_ounit_code VARCHAR(255)
	DECLARE @v_factsheet_id VARCHAR(255)
	DECLARE @LANG VARCHAR(255)
	DECLARE @TEXT VARCHAR(255)
	DECLARE @FACTSHEET_URL xml

	SELECT 	@v_institution_id = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@v_ounit_code = T.c.value('(organization_unit_code)[1]', 'varchar(255)') 
	FROM @P_OUNIT.nodes('organization_unit') T(c);
	
	SELECT @v_factsheet_id = ou.FACT_SHEET
		FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@v_institution_id)
		AND UPPER(ORGANIZATION_UNIT_CODE) = UPPER(@v_ounit_code);
	
	SELECT  @TEXT = T.c.value('(.)[1]', 'varchar(255)') FROM @P_OUNIT.nodes('organization_unit/factsheet_url_list/text') T(c)
	IF @TEXT IS NOT NULL
	BEGIN
		IF @v_factsheet_id IS NULL
		BEGIN
			SET @v_factsheet_id = NEWID()
			INSERT INTO EWPCV_FACT_SHEET (ID) VALUES (@v_factsheet_id)
		END

		DECLARE cur CURSOR LOCAL FOR
		SELECT 
			T.c.value('(@lang)[1]', 'varchar(255)'),
			T.c.value('(.)[1]', 'varchar(255)'),
			T.c.query('.')
		FROM @P_OUNIT.nodes('organization_unit/factsheet_url_list/text') T(c)
		
		OPEN cur
		FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @v_lang_item_id = NULL

				SELECT @v_lang_item_id = ID
					FROM dbo.EWPCV_LANGUAGE_ITEM LAIT 
					INNER JOIN dbo.EWPCV_FACT_SHEET_URL FSU ON FSU.URL_ID = LAIT.ID
					WHERE UPPER(LAIT.LANG) = UPPER(@LANG)
					AND UPPER(LAIT.TEXT) = UPPER(@TEXT)
					AND FSU.FACT_SHEET_ID = @v_factsheet_id

				IF @v_lang_item_id IS NULL
					BEGIN
						execute dbo.INSERTA_LANGUAGE_ITEM @FACTSHEET_URL,  @v_lang_item_id OUTPUT
					INSERT INTO EWPCV_FACT_SHEET_URL (URL_ID, FACT_SHEET_ID) VALUES (@v_lang_item_id, @v_factsheet_id)
						SET @v_lang_item_id = NULL
					END

				FETCH NEXT FROM cur INTO @LANG, @TEXT, @FACTSHEET_URL
			END
		CLOSE cur
		DEALLOCATE cur
	END

	SET @return_value = @v_factsheet_id
	
END
;
GO


 /*  
 Actualiza una ounit
 */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_OUNIT'
) DROP PROCEDURE dbo.ACTUALIZA_OUNIT;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_OUNIT(@P_OUNIT_ID varchar(255), @P_OUNIT xml) AS
BEGIN
	
	DECLARE @v_ounit_id uniqueidentifier
	DECLARE @v_factsheet_id uniqueidentifier
	DECLARE @v_contact_id uniqueidentifier
	DECLARE @v_prim_contact_id uniqueidentifier
	
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @OUNIT_CODE varchar(255)
	DECLARE @ABBREVIATION varchar(255)
	DECLARE @LOGO_URL varchar(255)
	DECLARE @PARENT_OUNIT_ID varchar(255)
	DECLARE @IS_TREE_STRUCTURE varchar(255)
	DECLARE @OUNIT_NAME_LIST xml
	DECLARE @OUNIT_CONTACT_DETAILS xml
	DECLARE @CONTACT_DETAILS_LIST xml
	DECLARE @CONTACT_PERSON xml


	DECLARE @ID uniqueidentifier
	
	
	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@OUNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@ABBREVIATION = T.c.value('(abbreviation)[1]', 'varchar(255)'),
		@LOGO_URL = T.c.value('(logo_url)[1]', 'varchar(255)'),
		@PARENT_OUNIT_ID = T.c.value('(parent-ounit-id)[1]', 'varchar(255)'),
		@IS_TREE_STRUCTURE = T.c.value('(is-tree-structure)[1]', 'integer'),
		@OUNIT_NAME_LIST = T.c.query('organization_unit_name'),
		@OUNIT_CONTACT_DETAILS = T.c.query('ounit_contact_details'),
		@CONTACT_DETAILS_LIST = T.c.query('contact_details_list')
	FROM @P_OUNIT.nodes('organization_unit') T(c)
	
	
	SELECT 
		@v_ounit_id = ou.ID,
		@v_factsheet_id = ou.FACT_SHEET 
		FROM dbo.EWPCV_ORGANIZATION_UNIT ou             
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON UPPER(ou.ID) = UPPER(iou.ORGANIZATION_UNITS_ID)
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON ins.ID = iou.INSTITUTION_ID
		WHERE UPPER(ins.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
		AND UPPER(ou.id) = UPPER(@P_OUNIT_ID);
		
	

	UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET PRIMARY_CONTACT_DETAIL_ID = NULL, FACT_SHEET = NULL WHERE ID = @v_ounit_id

	--borramos los nombres que tenia y los reemplazamos por los nuevos
    EXECUTE dbo.BORRA_OUNIT_NAMES @v_ounit_id
    EXECUTE dbo.INSERTA_OUNIT_NAMES @v_ounit_id, @OUNIT_NAME_LIST
	
	--borramos los contactos antiguos
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
		FROM dbo.EWPCV_CONTACT
		WHERE UPPER(ORGANIZATION_UNIT_ID) = UPPER(@v_ounit_id) 
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.BORRA_CONTACT @ID
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur

	-- Insertamos la nueva lista de contactos
	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @CONTACT_DETAILS_LIST.nodes('contact_details_list/contact_person') T(c) 

	OPEN cur
	FETCH NEXT FROM cur INTO @CONTACT_PERSON
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_CONTACT_PERSON @CONTACT_PERSON, @INSTITUTION_ID, @OUNIT_CODE, @v_contact_id OUTPUT
			FETCH NEXT FROM cur INTO @CONTACT_PERSON
		END
	CLOSE cur
	DEALLOCATE cur
	
	--insertamos el nuevo contacto principal
	EXECUTE dbo.INSERTA_CONTACT @OUNIT_CONTACT_DETAILS, @INSTITUTION_ID, @OUNIT_CODE, @v_prim_contact_id OUTPUT
	
    --borramos las urls de factsheet que tenia y las reemplazamos por las nuevas
	EXECUTE dbo.BORRA_FACTSHEET_URL @v_factsheet_id    
	--se borra el registro si es un dummy
	DELETE FROM dbo.EWPCV_FACT_SHEET WHERE ID = @v_factsheet_id AND CONTACT_DETAILS_ID IS NULL;
	EXECUTE dbo.INSERTA_OUNIT_FACTSHEET_URLS @P_OUNIT, @v_factsheet_id OUTPUT
        
	UPDATE EWPCV_ORGANIZATION_UNIT SET 
	    PRIMARY_CONTACT_DETAIL_ID = @v_prim_contact_id, FACT_SHEET = @v_factsheet_id, ABBREVIATION = @ABBREVIATION, LOGO_URL = @LOGO_URL, ORGANIZATION_UNIT_CODE = @OUNIT_CODE, PARENT_OUNIT_ID = @PARENT_OUNIT_ID
		WHERE ID = @P_OUNIT_ID;
END 
;
GO

/* 
	Actualiza una OUNIT del sistema.
    Recibe como parametros: 
    El identificador de la Unidad organizativa
	Admite un xml de tipo Ounit con toda la informacion actualizada de la ounit, sus contactos y sus factsheet
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_OUNIT'
) DROP PROCEDURE dbo.UPDATE_OUNIT;
GO 
CREATE PROCEDURE dbo.UPDATE_OUNIT(@P_OUNIT_ID varchar(255), @P_OUNIT xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	
	DECLARE @v_count integer
	SET @return_value = 0;
	
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(ID) = UPPER(@P_OUNIT_ID)
	IF @v_count > 0 
		BEGIN
			EXECUTE dbo.VALIDA_OUNIT @P_OUNIT, @P_ERROR_MESSAGE output, @return_value output
			IF @return_value = 0 
			BEGIN
				
				EXECUTE dbo.VALIDA_DATOS_OUNIT @P_OUNIT, @P_OUNIT_ID, @P_ERROR_MESSAGE output, @return_value output
				IF @return_value = 0 
				BEGIN
					BEGIN TRY
						BEGIN TRANSACTION
						EXECUTE dbo.ACTUALIZA_OUNIT @P_OUNIT_ID, @P_OUNIT
						COMMIT TRANSACTION
					END TRY

					BEGIN CATCH
						THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_OUNIT'
						SET @return_value = -1
						ROLLBACK TRANSACTION
					END CATCH
				END 
			END;
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO

/*
    Borra los nombres de la OUNIT
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_OUNIT_NAMES'
) DROP PROCEDURE dbo.BORRA_OUNIT_NAMES;
GO 
CREATE PROCEDURE dbo.BORRA_OUNIT_NAMES (@P_OUNIT_ID uniqueidentifier) AS
BEGIN
	DECLARE @NAME_ID uniqueidentifier

	DECLARE cur CURSOR LOCAL FOR
	SELECT NAME_ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME
			WHERE ORGANIZATION_UNIT_ID = @P_OUNIT_ID

	OPEN cur
	FETCH NEXT FROM cur INTO @NAME_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            DELETE FROM dbo.EWPCV_ORGANIZATION_UNIT_NAME WHERE NAME_ID = @NAME_ID
			DELETE FROM dbo.EWPCV_LANGUAGE_ITEM WHERE ID = @NAME_ID
			FETCH NEXT FROM cur INTO @NAME_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO

/*
    Actualiza el campo IS_EXPOSED de forma recursiva
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OCULTAR_O_MOSTRAR_OUNIT'
) DROP PROCEDURE dbo.OCULTAR_O_MOSTRAR_OUNIT;
GO 
CREATE PROCEDURE dbo.OCULTAR_O_MOSTRAR_OUNIT (@P_OUNIT_ID uniqueidentifier, @P_IS_EXPOSED integer) AS
BEGIN
	DECLARE @ID uniqueidentifier

	UPDATE dbo.EWPCV_ORGANIZATION_UNIT SET IS_EXPOSED = @P_IS_EXPOSED WHERE ID = @P_OUNIT_ID;
	
	DECLARE cur CURSOR LOCAL FOR
	SELECT ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT
			WHERE PARENT_OUNIT_ID = @P_OUNIT_ID;
	OPEN cur
	FETCH NEXT FROM cur INTO @ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
            EXECUTE OCULTAR_O_MOSTRAR_OUNIT @ID, @P_IS_EXPOSED;
			FETCH NEXT FROM cur INTO @ID
		END
	CLOSE cur
	DEALLOCATE cur
END
;
GO  

/* Oculta una ounit de manera que esta deja de estar expuesta. 
	En el caso de estar en estructura de árbol y ser una OUNIT padre, se ocultarán también todas las hijas.
	Recibe como parametros: 
	El identificador de la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OCULTAR_OUNIT'
) DROP PROCEDURE dbo.OCULTAR_OUNIT;
GO 
CREATE PROCEDURE dbo.OCULTAR_OUNIT (@P_OUNIT_ID uniqueidentifier,  @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	SET @return_value = 0;
	DECLARE @v_count integer
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(ID) = UPPER(@P_OUNIT_ID)
	IF @v_count > 0 
		BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			EXECUTE OCULTAR_O_MOSTRAR_OUNIT @P_OUNIT_ID, 0;
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.OCULTAR_OUNIT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH			
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO  
 
 
 /* Oculta una ounit de manera que esta deja de estar expuesta. 
	En el caso de estar en estructura de árbol y ser una OUNIT padre, se ocultarán también todas las hijas.
	Recibe como parametros: 
	El identificador de la OUNIT
	Un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'EXPONER_OUNIT'
) DROP PROCEDURE dbo.EXPONER_OUNIT;
GO 
CREATE PROCEDURE dbo.EXPONER_OUNIT (@P_OUNIT_ID uniqueidentifier,  @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS
BEGIN
	SET @return_value = 0;
	DECLARE @v_count integer
	SELECT @v_count = COUNT(1) FROM dbo.EWPCV_ORGANIZATION_UNIT WHERE UPPER(ID) = UPPER(@P_OUNIT_ID)
	IF @v_count > 0 
		BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
			EXECUTE OCULTAR_O_MOSTRAR_OUNIT @P_OUNIT_ID, 1;
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.EXPONER_OUNIT'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH			
		END
	ELSE
	BEGIN
		SET @P_ERROR_MESSAGE = 'La unidad organizativa no existe'
		SET @return_value = -1
	END 
END
;
GO  


/*
	****************************************
	  FIN MODIFICACIONES SOBRE OUNITS 
	****************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.03.00', GETDATE(), '09_UPGRADE_v01.03.00');