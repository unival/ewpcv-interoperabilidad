
-- IMPORTANTE: PARA EL CORRECTO FUNCIONAMIENTO HAY QUE REEMPLAZAR ESTAS VARIABLES:
-- UBICACI�N ARCHIVOS SQL SERVER -> BaseDatos -> C:\Trabajo\Proyectos\EWP\Scripts\SqlServer <- MODIFICAR ESTE VALOR
:setvar UbiSqlServer C:\Trabajo\Proyectos\EWP\Scripts\SqlServer
GO
PRINT ' - EJECUCION DEL SCRIPT 0_borrar_tablas';
GO
:r $(UbiSqlServer)\0_borrar_tablas_sqlserver.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 01_create_ewpcv_sqlserver.sql';
GO
:r $(UbiSqlServer)\01_create_ewpcv_sqlserver.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 02_upgrade.sql';
GO
:r $(UbiSqlServer)\02_upgrade.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 03_UPGRADE_v01.00.00.sql';
GO
:r $(UbiSqlServer)\03_UPGRADE_v01.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 04_Definicion_Aprovisionamiento.sql';
GO
:r $(UbiSqlServer)\04_Definicion_Aprovisionamiento.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 05_UPGRADE_v01.01.00.sql';
GO
:r $(UbiSqlServer)\05_UPGRADE_v01.01.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 06_Aprovisionamiento.sql';
GO
:r $(UbiSqlServer)\06_Aprovisionamiento.sql
GO
--
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 07_UPGRADE_v01.02.00.sql';
GO
:r $(UbiSqlServer)\07_UPGRADE_v01.02.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 08_UPGRADE_v01.02.01.sql';
GO
:r $(UbiSqlServer)\08_UPGRADE_v01.02.01.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 09_UPGRADE_v01.03.00.sql';
GO
:r $(UbiSqlServer)\09_UPGRADE_v01.03.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 10_UPGRADE_v01.03.01.sql';
GO
:r $(UbiSqlServer)\10_UPGRADE_v01.03.01.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 11_UPGRADE_v01.04.00.sql';
GO
:r $(UbiSqlServer)\11_UPGRADE_v01.04.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 12_UPGRADE_v01.04.01.sql';
GO
:r $(UbiSqlServer)\12_UPGRADE_v01.04.01.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 13_UPGRADE_v01.05.00.sql';
GO
:r $(UbiSqlServer)\13_UPGRADE_v01.05.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 14_UPGRADE_v01.06.00.sql';
GO
:r $(UbiSqlServer)\14_UPGRADE_v01.06.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 15_UPGRADE_v01.06.01.sql';
GO
:r $(UbiSqlServer)\15_UPGRADE_v01.06.01.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 16_UPGRADE_v01.06.02.sql';
GO
:r $(UbiSqlServer)\16_UPGRADE_v01.06.02.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 17_UPGRADE_v01.06.03.sql';
GO
:r $(UbiSqlServer)\17_UPGRADE_v01.06.03.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 18_UPGRADE_v01.08.00.sql';
GO
:r $(UbiSqlServer)\18_UPGRADE_v01.08.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 19_UPGRADE_v01.08.02.sql';
GO
:r $(UbiSqlServer)\19_UPGRADE_v01.08.02.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 20_UPGRADE_v01.08.03.sql';
GO
:r $(UbiSqlServer)\20_UPGRADE_v01.08.03.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 21_UPGRADE_v01.08.04.sql';
GO
:r $(UbiSqlServer)\21_UPGRADE_v01.08.04.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 22_UPGRADE_v01.08.05.sql';
GO
:r $(UbiSqlServer)\22_UPGRADE_v01.08.05.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 23_UPGRADE_FEATURE_EHU_MEJORA_vista_pdf_iias.sql';
GO
:r $(UbiSqlServer)\23_UPGRADE_FEATURE_EHU_MEJORA_vista_pdf_iias.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 23_UPGRADE_FEATURE_EHU_ERROR_condiciones_cooperacion.sql';
GO
:r $(UbiSqlServer)\23_UPGRADE_FEATURE_EHU_ERROR_condiciones_cooperacion.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 23_UPGRADE_FEATURE_EHU_ERROR_desboradamiento_de_campos.sql';
GO
:r $(UbiSqlServer)\23_UPGRADE_FEATURE_EHU_ERROR_desboradamiento_de_campos.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 24_UPGRADE_FEATURE_MEJORA_vista_institution_contacts.sql';
GO
:r $(UbiSqlServer)\24_UPGRADE_FEATURE_MEJORA_vista_institution_contacts.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 25_UPGRADE_FEATURE_MEJORA_vista_ounit_contacts.sql';
GO
:r $(UbiSqlServer)\25_UPGRADE_FEATURE_MEJORA_vista_ounit_contacts.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 26_UPGRADE_modificacion_pk_ounits.sql';
GO
:r $(UbiSqlServer)\26_UPGRADE_modificacion_pk_ounits.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 26_UPGRADE_FEATURE_EHU_ERROR_errores_interoperabilidad.sql';
GO
:r $(UbiSqlServer)\26_UPGRADE_FEATURE_EHU_ERROR_errores_interoperabilidad.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 27_UPGRADE_FEATURE_EHU_ERROR_nombres_intitucion_repetidos.sql';
GO
:r $(UbiSqlServer)\27_UPGRADE_FEATURE_EHU_ERROR_nombres_intitucion_repetidos.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 27_UPGRADE_FEATURE_MEJORA_fix_update_institution.sql';
GO
:r $(UbiSqlServer)\27_UPGRADE_FEATURE_MEJORA_fix_update_institution.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 28_UPGRADE_FEATURE_MEJORA_ordenacion_cooperation_condition.sql';
GO
:r $(UbiSqlServer)\28_UPGRADE_FEATURE_MEJORA_ordenacion_cooperation_condition.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 28_fix_insertar_pdf_iia.sql';
GO
:r $(UbiSqlServer)\28_fix_insertar_pdf_iia.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 29_actualizacion_ounit_ids.sql';
GO
:r $(UbiSqlServer)\29_actualizacion_ounit_ids.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 30_UPGRADE_FEATURE_Mejoras_auditoria_cnr.sql';
GO
:r $(UbiSqlServer)\30_UPGRADE_FEATURE_Mejoras_auditoria_cnr.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 31_UPGRADE_FEATURE_error_al_insertar_movilidades_us.sql';
GO
:r $(UbiSqlServer)\31_UPGRADE_FEATURE_error_al_insertar_movilidades_us.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 32_UPGRADE_FEATURE_MEJORA_permitir_las_sin_componentes.sql';
GO
:r $(UbiSqlServer)\32_UPGRADE_FEATURE_MEJORA_permitir_las_sin_componentes.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 33_OLA_STATS_v02.01.00.sql';
GO
:r $(UbiSqlServer)\33_OLA_STATS_v02.01.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 34_ILA_STATS_v02.01.00.sql';
GO
:r $(UbiSqlServer)\34_ILA_STATS_v02.01.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 35_Iia_STATS_v02.01.00.sql';
GO
:r $(UbiSqlServer)\35_Iia_STATS_v02.01.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 36_DICTIONARY_v02.01.00.sql';
GO
:r $(UbiSqlServer)\36_DICTIONARY_v02.01.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 37_IIA_v02.02.00.sql';
GO
:r $(UbiSqlServer)\37_IIA_v02.02.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 38_IIA_APPROVALS_v02.03.00.sql';
GO
:r $(UbiSqlServer)\38_IIA_APPROVALS_v02.03.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 39_CAPTURA_IIA_APPROVALS.sql';
GO
:r $(UbiSqlServer)\39_CAPTURA_IIA_APPROVALS.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 40_LONGITUD_CAMPOS_LAS.sql';
GO
:r $(UbiSqlServer)\40_LONGITUD_CAMPOS_LAS.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 40_IIA_RECOVER_SNAPSHOT_v03.00.00.sql';
GO
:r $(UbiSqlServer)\40_IIA_RECOVER_SNAPSHOT_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 41_DELETED_ELEMENTS_v03.00.00.sql';
GO
:r $(UbiSqlServer)\41_DELETED_ELEMENTS_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 42_FILE_API_v03.00.00.sql';
GO
:r $(UbiSqlServer)\42_FILE_API_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 43_UPDATE_IIA_HASH_V7_v03.00.00.sql';
GO
:r $(UbiSqlServer)\43_UPDATE_IIA_HASH_V7_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 44_INTEGRACION_IIAs_FILE_API_v03.00.00.sql';
GO
:r $(UbiSqlServer)\44_INTEGRACION_IIAs_FILE_API_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 45_IIA_TERMINATE_v03.00.00.sql';
GO
:r $(UbiSqlServer)\45_IIA_TERMINATE_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 46_IIA_REGRESSIONS_v03.00.00.sql';
GO
:r $(UbiSqlServer)\46_IIA_REGRESSIONS_v03.00.00.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 47_NOTIFICACIONES_NO_REINTENTABLES_v03.00.00.sql';
GO
:r $(UbiSqlServer)\47_NOTIFICACIONES_NO_REINTENTABLES_v03.00.00.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 48_UPDATE_IIA_HASH_V7_v03.00.01.sql';
GO
:r $(UbiSqlServer)\48_UPDATE_IIA_HASH_V7_v03.00.01.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 49_EXTRACCION_FACTSHEET_URL.sql';
GO
:r $(UbiSqlServer)\49_EXTRACCION_FACTSHEET_URL.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 50_CAMBIOS_IDS_SECUENCIALES.sql';
GO
:r $(UbiSqlServer)\50_CAMBIOS_IDS_SECUENCIALES.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 51_CAMBIOS_FECHAS_APPROVAL_DATE_A_TIMESTAMP.sql';
GO
:r $(UbiSqlServer)\51_CAMBIOS_FECHAS_APPROVAL_DATE_A_TIMESTAMP.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 52_EXPORT_ACTIONS.sql';
GO
:r $(UbiSqlServer)\52_EXPORT_ACTIONS.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 53_MANAGE_ERRORS.sql';
GO
:r $(UbiSqlServer)\53_MANAGE_ERRORS.sql
GO
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 54_QUARTZ_EWPSINC.sql';
GO
:r $(UbiSqlServer)\54_QUARTZ_EWPSINC.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 55_REGULARIZACION_ordenes_coop_cond_iias.SQL';
GO
:r $(UbiSqlServer)\55_REGULARIZACION_ordenes_coop_cond_iias.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT 56_ACTUALIZACION_MOBILITY_UPDATE_REQUEST.SQL';
GO
:r $(UbiSqlServer)\56_ACTUALIZACION_MOBILITY_UPDATE_REQUEST.sql
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Funciones.SQL';
GO
:r $(UbiSqlServer)\codigo\Funciones.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Common.SQL';
GO
:r $(UbiSqlServer)\codigo\Common.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Factsheet.SQL';
GO
:r $(UbiSqlServer)\codigo\Factsheet.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Iias.SQL';
GO
:r $(UbiSqlServer)\codigo\Iias.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Institutions.SQL';
GO
:r $(UbiSqlServer)\codigo\Institutions.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Los.SQL';
GO
:r $(UbiSqlServer)\codigo\Los.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/MobilityLa.SQL';
GO
:r $(UbiSqlServer)\codigo\MobilityLa.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Ounits.SQL';
GO
:r $(UbiSqlServer)\codigo\Ounits.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Tors.SQL';
GO
:r $(UbiSqlServer)\codigo\Tors.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Dictionary.SQL';
GO
:r $(UbiSqlServer)\codigo\Dictionary.SQL
GO
PRINT ' '
PRINT ' - EJECUCION DEL SCRIPT codigo/Vistas.SQL';
GO
:r $(UbiSqlServer)\codigo\Vistas.SQL
GO