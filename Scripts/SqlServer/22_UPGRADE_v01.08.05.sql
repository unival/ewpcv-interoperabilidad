
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_TOR'
) DROP PROCEDURE dbo.ACTUALIZA_TOR;
GO 
CREATE PROCEDURE dbo.ACTUALIZA_TOR(@P_TOR_ID varchar(255), @P_TOR xml) AS
BEGIN
	DECLARE @GENERATED_DATE date
	DECLARE @EXTENSION varbinary(max)
	DECLARE @TOR_REPORT_LIST xml
	DECLARE @MOBILITY_ATTACHMENT_LIST xml
	DECLARE @ISCED_TABLE_LIST xml

	SELECT
	@GENERATED_DATE = CAST(T.c.value('generated_date[1]', 'varchar(255)') as date),
	@EXTENSION = T.c.value('extension[1]', 'varbinary(max)'),
	@TOR_REPORT_LIST = T.c.query('tor_report_list'),
	@MOBILITY_ATTACHMENT_LIST = T.c.query('mobility_attachment_list'),
	@ISCED_TABLE_LIST = T.c.query('isced_table_list')
	FROM @P_TOR.nodes('tor') T(c)

	UPDATE dbo.EWPCV_TOR SET VERSION = VERSION + 1, GENERATED_DATE = @GENERATED_DATE, EXTENSION = @EXTENSION WHERE ID = @P_TOR_ID

	EXECUTE dbo.BORRA_ISCED_TABLE_LIST @P_TOR_ID
	EXECUTE dbo.BORRA_REPORTS @P_TOR_ID
	EXECUTE dbo.BORRA_MOBILITY_ATTACHMENT_LIST @P_TOR_ID

	EXECUTE dbo.INSERTA_TOR_REPORT_LIST @TOR_REPORT_LIST, @P_TOR_ID
	EXECUTE dbo.INSERTA_MOBILITY_ATTACHMENT_LIST @MOBILITY_ATTACHMENT_LIST, @P_TOR_ID
	EXECUTE dbo.INSERTA_ISCED_TABLE_LIST @ISCED_TABLE_LIST, @P_TOR_ID

END
;
GO
/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.05', GETDATE(), '22_UPGRADE_v01.08.05');



