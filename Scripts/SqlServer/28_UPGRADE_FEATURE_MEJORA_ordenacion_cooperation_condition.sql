
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/

	ALTER TABLE ewp.dbo.EWPCV_COOPERATION_CONDITION	ADD ORDER_INDEX bigint;
	GO

	CREATE TRIGGER TRIGGER_COOPERATION_CONDITION_ORDER_INDEX
	ON ewp.dbo.EWPCV_COOPERATION_CONDITION
	AFTER INSERT AS
	BEGIN
		DECLARE
		@cc_id varchar(250)
		
		SET NOCOUNT ON;
		
		SELECT @cc_id = INSERTED.[ID] FROM INSERTED
		UPDATE ewp.dbo.EWPCV_COOPERATION_CONDITION SET ORDER_INDEX = NEXT VALUE FOR dbo.SEC_EWPCV_COOPERATION_CONDITION
		WHERE ID = @cc_id;
	END;
	GO
	
	CREATE SEQUENCE dbo.SEC_EWPCV_COOPERATION_CONDITION  START WITH 1  INCREMENT BY 1 ;  

	BEGIN
		DECLARE @coop_cond_id varchar(255)
		DECLARE c_coop_cond CURSOR FOR   
		SELECT 
			ID 
		FROM 
			ewp.dbo.EWPCV_COOPERATION_CONDITION ecc
		  
		OPEN c_coop_cond  
		  
		FETCH NEXT FROM c_coop_cond INTO @coop_cond_id  
		  
		WHILE @@FETCH_STATUS = 0
			BEGIN
				UPDATE ewp.dbo.EWPCV_COOPERATION_CONDITION SET ORDER_INDEX = NEXT VALUE FOR dbo.SEC_EWPCV_COOPERATION_CONDITION
				WHERE ID = @coop_cond_id;

				FETCH NEXT FROM c_coop_cond INTO @coop_cond_id  
			END
		CLOSE c_coop_cond
		DEALLOCATE c_coop_cond  
	END;
	GO  
	

/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE FUNCIONES
    ***********************************
*/
/*
    **********************************
    INICIO MODIFICACIONES SOBRE VISTAS
    **********************************
*/

/*
    *********************************
     FIN MODIFICACIONES SOBRE VISTAS
    *********************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TYPES
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE TYPES
    ***********************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE PKG
    *************************************
*/

/*
    ***********************************
     FIN MODIFICACIONES SOBRE PKG
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE PERMISOS
    ***********************************
*/

/*
    ***********************************
     INICIO CREACION DE SINONINIMOS
    ***********************************
*/

/*
    ***********************************
     FIN CREACION DE SINONINIMOS
    ***********************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v02.00.00', GETDATE(), '28_UPGRADE_FEATURE_MEJORA_ordenacion_cooperation_condition');
