DECLARE @v_count integer
DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_inst_id  NVARCHAR(9)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'validator-hei01.developers.erasmuswithoutpaper.eu';


DECLARE @errMsg varchar(4000)
DECLARE @errNum integer
DECLARE @INSTITUTION_ID uniqueidentifier
DECLARE @OUNIT_ID uniqueidentifier
DECLARE @IIA_ID uniqueidentifier
DECLARE @NVARCHAR_IIA_ID NVARCHAR(37)
DECLARE @MOBILITY_ID uniqueidentifier
DECLARE @NVARCHAR_MOBILITY_ID NVARCHAR(37)
DECLARE @LA_ID uniqueidentifier


--Borrado de datos de tablas

DELETE FROM EWPCV_FACT_SHEET_URL ;
DELETE FROM EWPCV_INSTITUTION_NAME ;
DELETE FROM EWPCV_ORGANIZATION_UNIT_NAME ;
DELETE FROM EWPCV_INST_ORG_UNIT ;
DELETE FROM EWPCV_INST_INF_ITEM ;
DELETE FROM EWPCV_OU_INF_ITEM ;
DELETE FROM EWPCV_OU_REQUIREMENT_INFO ;
DELETE FROM EWPCV_INS_REQUIREMENTS ;
DELETE FROM EWPCV_INSTITUTION ;
DELETE FROM EWPCV_ORGANIZATION_UNIT ;
DELETE FROM EWPCV_FACT_SHEET ;
DELETE FROM EWPCV_REQUIREMENTS_INFO ;
DELETE FROM EWPCV_INFORMATION_ITEM ;
DELETE FROM EWPCV_MOBILITY_LANG_SKILL ;
DELETE FROM EWPCV_COOPCOND_SUBAR_LANSKIL ;
DELETE FROM EWPCV_LANGUAGE_SKILL ;
DELETE FROM EWPCV_COOPCOND_EQFLVL ;
DELETE FROM EWPCV_COOPERATION_CONDITION ;
DELETE FROM EWPCV_MOBILITY_NUMBER ;
DELETE FROM EWPCV_DURATION ;
DELETE FROM EWPCV_IIA ;
DELETE FROM EWPCV_IIA_PARTNER_CONTACTS ;
DELETE FROM EWPCV_IIA_PARTNER ;
DELETE FROM EWPCV_MOBILITY_LA ;
DELETE FROM EWPCV_MOBILITY ;
DELETE FROM EWPCV_MOBILITY_PARTICIPANT ;
DELETE FROM EWPCV_MOBILITY_TYPE ;
DELETE FROM EWPCV_SUBJECT_AREA ;
DELETE FROM EWPCV_STUDIED_LA_COMPONENT ;
DELETE FROM EWPCV_RECOGNIZED_LA_COMPONENT ;
DELETE FROM EWPCV_VIRTUAL_LA_COMPONENT ;
DELETE FROM EWPCV_BLENDED_LA_COMPONENT ;
DELETE FROM EWPCV_DOCTORAL_LA_COMPONENT ;
DELETE FROM EWPCV_LEARNING_AGREEMENT ;
DELETE FROM EWPCV_SIGNATURE ;
DELETE FROM EWPCV_CONTACT_NAME ;
DELETE FROM EWPCV_CONTACT_DESCRIPTION ;
DELETE FROM EWPCV_CONTACT ;
DELETE FROM EWPCV_PERSON ;
DELETE FROM EWPCV_CONTACT_URL;
DELETE FROM EWPCV_CONTACT_DETAILS_EMAIL ;
DELETE FROM EWPCV_PHOTO_URL;
DELETE FROM EWPCV_CONTACT_DETAILS ;
DELETE FROM EWPCV_PHONE_NUMBER ;
DELETE FROM EWPCV_FLEXIBLE_ADDRESS_LINE;
DELETE FROM EWPCV_FLEXAD_DELIV_POINT_COD ;
DELETE FROM EWPCV_FLEXAD_RECIPIENT_NAME ;
DELETE FROM EWPCV_FLEXIBLE_ADDRESS;
DELETE FROM EWPCV_COMPONENT_CREDITS ;
DELETE FROM EWPCV_LOI_CREDITS ;
DELETE FROM EWPCV_CREDIT ;
DELETE FROM EWPCV_DISTR_CATEGORIES ;
DELETE FROM EWPCV_RESULT_DIST_CATEGORY ;
DELETE FROM EWPCV_DISTR_DESCRIPTION ;
DELETE FROM EWPCV_LA_COMPONENT ;
DELETE FROM EWPCV_LOS_LOI ;
DELETE FROM EWPCV_LOI ;
DELETE FROM EWPCV_RESULT_DISTRIBUTION ;
DELETE FROM EWPCV_ACADEMIC_TERM_NAME ;
DELETE FROM EWPCV_ACADEMIC_TERM ;
DELETE FROM EWPCV_ACADEMIC_YEAR ;
DELETE FROM EWPCV_GRADING_SCHEME_DESC ;
DELETE FROM EWPCV_GRADING_SCHEME_LABEL ;
DELETE FROM EWPCV_GRADING_SCHEME ;
DELETE FROM EWPCV_LOS_DESCRIPTION ;
DELETE FROM EWPCV_LOS_LOS ;
DELETE FROM EWPCV_LOS_NAME ;
DELETE FROM EWPCV_LOS_URLS ;
DELETE FROM EWPCV_LOS ;
DELETE FROM EWPCV_MOBILITY_UPDATE_REQUEST ;
DELETE FROM EWPCV_NOTIFICATION ;
DELETE FROM EWPCV_LANGUAGE_ITEM ;
DELETE FROM EWPCV_EVENT;

--insercion de la institucion en la tabla de identificadores si no existe

SET @v_count = 0;

SELECT @v_count = count(1) FROM EWPCV_INSTITUTION_IDENTIFIERS WHERE SCHAC = @v_inst_id;

IF @v_count = 0 
BEGIN
	INSERT INTO EWPCV_INSTITUTION_IDENTIFIERS (SCHAC) VALUES (@v_inst_id);
END;

SET @v_count = 0;
SELECT @v_count = count(1) FROM EWPCV_INSTITUTION_IDENTIFIERS WHERE SCHAC = @v_receiving_inst_id;

IF @v_count = 0 
BEGIN
	INSERT INTO EWPCV_INSTITUTION_IDENTIFIERS (SCHAC) VALUES (@v_receiving_inst_id);
END;

--Insercion de institucion dummy
PRINT 'Insertamos institucion '

DECLARE @xml_institution xml 
set @xml_institution= '<institution>
	<institution_id>'+ @v_inst_id + N'</institution_id>
	<institution_name>
		<text lang="en">English university name</text>
		<text lang="es">Nombre de universidad en castellano</text>
	</institution_name>
	<abbreviation>Abreviation</abbreviation>
	<university_contact_details>
		<street_address>
			<recipient_name_list>
				<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
				<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
			</recipient_name_list>
			<address_line_list>								
				<address_line>Calle test</address_line>								
				<address_line>numero, piso</address_line>								
			</address_line_list>
			<building_number>1</building_number>
			<building_name>Nombre de edificio</building_name>
			<street_name>Nombre de calle</street_name>
			<unit>1</unit>
			<building_floor>Floor</building_floor>
			<post_office_box>PostOffice</post_office_box>
			<delivery_point_code_list>
				<delivery_point_code>DeliveryPointCode1</delivery_point_code>
				<delivery_point_code>DeliveryPointCode2</delivery_point_code>
			</delivery_point_code_list>
			<postal_code>00001</postal_code>
			<locality>Valencia</locality>
			<region>VLC</region>
			<country>ES</country>
		</street_address>
		<mailing_address>
			<recipient_name_list>
				<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
				<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
			</recipient_name_list>
			<address_line_list>								
				<address_line>Calle test</address_line>								
				<address_line>numero, piso</address_line>								
			</address_line_list>
			<building_number>1</building_number>
			<building_name>Nombre de edificio</building_name>
			<street_name>Nombre de calle</street_name>
			<unit>1</unit>
			<building_floor>Floor</building_floor>
			<post_office_box>PostOffice</post_office_box>
			<delivery_point_code_list>
				<delivery_point_code>DeliveryPointCode1</delivery_point_code>
				<delivery_point_code>DeliveryPointCode2</delivery_point_code>
			</delivery_point_code_list>
			<postal_code>46001</postal_code>
			<locality>Valencia</locality>
			<region>VLC</region>
			<country>ES</country>
		</mailing_address>
		<contact_url>
			<text lang="en">https://www.abc.com</text>
			<text lang="es">https://www.abc.es</text>
		</contact_url>
	</university_contact_details>
	<contact_details_list>
		<contact_person>
			<given_name>John Test</given_name>
			<family_name>Doe Test</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact1 name</text>
					<text lang="es">Nombre contacto1</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact1 description</text>
					<text lang="es">Descripción contacto1</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact1 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact1 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact1</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact1 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact1 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>Spain</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
		</contact_person>
		<contact_person>
			<given_name>John Test2</given_name>
			<family_name>Doe Test2</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact2 name</text>
					<text lang="es">Nombre contacto2</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact2 description</text>
					<text lang="es">Descripción contacto2</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact2 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact2 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact2</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact2 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact2 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>Spain</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
		</contact_person>
	</contact_details_list>
	<factsheet_url_list>
		<text lang="en">https://www.abc.com/en/factsheet</text>
		<text lang="es">https://www.abc.es/es/factsheet</text>
	</factsheet_url_list>
	<logo_url>https://www.abc.es/logo.png</logo_url>                  
</institution>'


SET @errMsg = '';
SET @errNum = 0;
EXECUTE dbo.INSERT_INSTITUTION @xml_institution, @INSTITUTION_ID output, @errMsg output, @errNum output

IF @errNum < 0 
BEGIN
	PRINT 'Error insertando institucion ' 
	PRINT 'Error: ' + @errMsg
END
ELSE
BEGIN
	PRINT 'insertada institucion '
	PRINT 'Insertamos ounit '

	DECLARE @xml_ounit xml 
	set @xml_ounit= '<organization_unit>
		<institution_id>'+ @v_inst_id + N'</institution_id>
		<organization_unit_code>ounit_code</organization_unit_code>
		<organization_unit_name>
			<text lang="en">English ounit name</text>
			<text lang="es">nombre de ounit en castellano</text>
		</organization_unit_name>
		<abbreviation>Abreviacion</abbreviation>
		<is-tree-structure>0</is-tree-structure>
		<ounit_contact_details>
			<street_address>
				<recipient_name_list>
					<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
					<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
				</recipient_name_list>
				<address_line_list>								
					<address_line>Calle test</address_line>								
					<address_line>numero, piso</address_line>								
				</address_line_list>
				<building_number>1</building_number>
				<building_name>Nombre de edificio</building_name>
				<street_name>Nombre de calle</street_name>
				<unit>1</unit>
				<building_floor>Floor</building_floor>
				<post_office_box>PostOffice</post_office_box>
				<delivery_point_code_list>
					<delivery_point_code>DeliveryPointCode1</delivery_point_code>
					<delivery_point_code>DeliveryPointCode2</delivery_point_code>
				</delivery_point_code_list>
				<postal_code>00001</postal_code>
				<locality>Valencia</locality>
				<region>VLC</region>
				<country>ES</country>
			</street_address>
			<mailing_address>
				<recipient_name_list>
					<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
					<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
				</recipient_name_list>
				<address_line_list>								
					<address_line>Calle test</address_line>								
					<address_line>numero, piso</address_line>								
				</address_line_list>
				<building_number>1</building_number>
				<building_name>Nombre de edificio</building_name>
				<street_name>Nombre de calle</street_name>
				<unit>1</unit>
				<building_floor>Floor</building_floor>
				<post_office_box>PostOffice</post_office_box>
				<delivery_point_code_list>
					<delivery_point_code>DeliveryPointCode1</delivery_point_code>
					<delivery_point_code>DeliveryPointCode2</delivery_point_code>
				</delivery_point_code_list>
				<postal_code>00001</postal_code>
				<locality>Valencia</locality>
				<region>VLC</region>
				<country>ES</country>
			</mailing_address>
			<contact_url>
				<text lang="en">https://www.abc.com</text>
				<text lang="es">https://www.abc.es</text>
			</contact_url>
		</ounit_contact_details>
		<contact_details_list>
			<contact_person>
				<given_name>John Test</given_name>
				<family_name>Doe Test</family_name>
				<birth_date>2000-01-01</birth_date>
				<citizenship>ES</citizenship>
				<gender>1</gender>
				<contact>
					<contact_name>
						<text lang="en">Contact1 name</text>
						<text lang="es">Nombre contacto1</text>
					</contact_name>
					<contact_description>
						<text lang="en">Contact1 description</text>
						<text lang="es">Descripción contacto1</text>
					</contact_description>
					<contact_url>
						<text lang="en">https://www.ewp.org/contacts</text>
						<text lang="es">https://www.ewp.org/contactos</text>
					</contact_url>
					<contact_role>Contact1 Role</contact_role>
					<email_list>
						<email>email1@domain.com</email>
						<email>email2@domain.com</email>
					</email_list>
					<street_address>
						<recipient_name_list>
							<recipient_name>Contact1 Street Address RecName</recipient_name>
							<recipient_name>Street Address RecName Contact1</recipient_name>
						</recipient_name_list>
						<address_line_list>								
							<address_line>Contact1 Address line1</address_line>								
							<address_line>Address line2</address_line>								
						</address_line_list>
						<building_number>1</building_number>
						<building_name>Contact1 Building name</building_name>
						<street_name>Calle test</street_name>
						<unit>1</unit>
						<building_floor>Floor</building_floor>
						<post_office_box>PostOffice</post_office_box>
						<delivery_point_code_list>
							<delivery_point_code>DeliveryPointCode1</delivery_point_code>
							<delivery_point_code>DeliveryPointCode2</delivery_point_code>
						</delivery_point_code_list>
						<postal_code>46001</postal_code>
						<locality>Valencia</locality>
						<region>VLC</region>
						<country>Spain</country>
					</street_address>
					<phone>
						<e164>+34111222333</e164>
						<extension_number>1234</extension_number>
						<other_format>555-555-555</other_format>
					</phone>
				</contact>
			</contact_person>
			<contact_person>
				<given_name>John Test2</given_name>
				<family_name>Doe Test2</family_name>
				<birth_date>2000-01-01</birth_date>
				<citizenship>ES</citizenship>
				<gender>1</gender>
				<contact>
					<contact_name>
						<text lang="en">Contact2 name</text>
						<text lang="es">Nombre contacto2</text>
					</contact_name>
					<contact_description>
						<text lang="en">Contact2 description</text>
						<text lang="es">Descripción contacto2</text>
					</contact_description>
					<contact_url>
						<text lang="en">https://www.ewp.org/contacts</text>
						<text lang="es">https://www.ewp.org/contactos</text>
					</contact_url>
					<contact_role>Contact2 Role</contact_role>
					<email_list>
						<email>email1@domain.com</email>
						<email>email2@domain.com</email>
					</email_list>
					<street_address>
						<recipient_name_list>
							<recipient_name>Contact2 Street Address RecName</recipient_name>
							<recipient_name>Street Address RecName Contact2</recipient_name>
						</recipient_name_list>
						<address_line_list>								
							<address_line>Contact2 Address line1</address_line>								
							<address_line>Address line2</address_line>								
						</address_line_list>
						<building_number>1</building_number>
						<building_name>Contact2 Building name</building_name>
						<street_name>Calle test</street_name>
						<unit>1</unit>
						<building_floor>Floor</building_floor>
						<post_office_box>PostOffice</post_office_box>
						<delivery_point_code_list>
							<delivery_point_code>DeliveryPointCode1</delivery_point_code>
							<delivery_point_code>DeliveryPointCode2</delivery_point_code>
						</delivery_point_code_list>
						<postal_code>46001</postal_code>
						<locality>Valencia</locality>
						<region>VLC</region>
						<country>Spain</country>
					</street_address>
					<phone>
						<e164>+34111222333</e164>
						<extension_number>1234</extension_number>
						<other_format>555-555-555</other_format>
					</phone>
				</contact>
			</contact_person>
		</contact_details_list>
		<factsheet_url_list>
			<text lang="en">https://www.abc.com/ounit/en/factsheet</text>
			<text lang="es">https://www.abc.es/ounit/es/factsheet</text>
		</factsheet_url_list>
		<logo_url>https://www.abc.es/logoounit.png</logo_url>                  
	</organization_unit>'

	SET @errMsg = '';
	SET @errNum = 0;
	EXECUTE dbo.INSERT_OUNIT @xml_ounit, @OUNIT_ID output, @errMsg output, @errNum output

	
	IF @errNum < 0 
	BEGIN
		PRINT 'Error insertando ounit ' 
		PRINT 'Error: ' + @errMsg
	END
	ELSE
	BEGIN
		PRINT 'insertada ounit '
		PRINT 'Insertamos factsheet '

		DECLARE @xml_factsheet xml 
		set @xml_factsheet= '<factsheet_institution>
			<institution_id>'+ @v_inst_id + N'</institution_id>
			<logo_url>https://www.abc.es/</logo_url>
			<abreviation>Abreviacion</abreviation>
			<factsheet>
				<decision_week_limit>5</decision_week_limit>
				<tor_week_limit>5</tor_week_limit>
				<nominations_autum_term>2000-01-01</nominations_autum_term>
				<nominations_spring_term>2000-01-01</nominations_spring_term>
				<application_autum_term>2000-01-01</application_autum_term>
				<application_spring_term>2000-01-01</application_spring_term>
			</factsheet>
			<application_info>
				<email>application.email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.application.com/</text>
					<text lang="es">https://www.application.es/</text>
				</url>
			</application_info>
			<housing_info>
				<email>housing.email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.housing.com/</text>
					<text lang="es">https://www.housing.es/</text>
				</url>
			</housing_info>
			<visa_info>
				<email>visa.email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.visa.com/</text>
					<text lang="es">https://www.visa.es/</text>
				</url>
			</visa_info>
			<insurance_info>
				<email>insurance.email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.insurance.com/</text>
					<text lang="es">https://www.insurance.es/</text>
				</url>
			</insurance_info>
			<additional_info_list>
				<additional_info>
					<info_type>Additional information</info_type>
					<information_item>
						<email>email@domain.com</email>
						<phone>
							<e164>+34111222333</e164>
							<extension_number>1234</extension_number>
							<other_format>(23) 456789</other_format>
						</phone>
						<url>
							<text lang="en">https://www.additionalinfo.com/</text>
							<text lang="es">https://www.additionalinfo.es/</text>
						</url>
					</information_item>
				</additional_info>
				<additional_info>
					<info_type>More additional information</info_type>
					<information_item>
						<email>email@domain.com</email>
						<phone>
							<e164>+34111222333</e164>
							<extension_number>78687</extension_number>
							<other_format>(55) 86778687u</other_format>
						</phone>
						<url>
							<text lang="en">https://www.moreadditionalinfo.com/</text>
							<text lang="es">https://www.moreadditionalinfo.es/</text>
						</url>
					</information_item>
				</additional_info>
			</additional_info_list>
			<accessibility_requirements_list>
				<accessibility_requirement>
					<req_type>infrastructure</req_type>
					<name>Infraestructure accessibility requirement</name>
					<description>Description of infraestructure accessibility requirement</description>
					<information_item>
						<email>email@domain.com</email>
						<phone>
							<e164>+34111222333</e164>
							<extension_number>1234</extension_number>
							<other_format>(23) 456789</other_format>
						</phone>
						<url>
							<text lang="en">https://www.InfraestructureAccessibilityRequirement.com/</text>
							<text lang="es">https://www.InfraestructureAccessibilityRequirement.es/</text>
						</url>
					</information_item>
				</accessibility_requirement>
				<accessibility_requirement>
					<req_type>service</req_type>
					<name>Service accessibility requirement</name>
					<description>Description of service accessibility requirement</description>
					<information_item>
						<email>email@domain.com</email>
						<phone>
							<e164>+4722222222</e164>
							<extension_number>1234</extension_number>
							<other_format>(23) 456789</other_format>
						</phone>
						<url>
							<text lang="en">http://www.ServiceAccessibilityRequirement.com/</text>
							<text lang="es">http://www.ServiceAccessibilityRequirement.es/</text>
						</url>
					</information_item>
				</accessibility_requirement>
			</accessibility_requirements_list>
			<additional_requirements_list>
				<additional_requirement>
					<name>Additional requirement</name>
					<description>Description of additional requirement</description>
					<url>
						<text lang="en">http://www.AdditionalRequirement.com/</text>
						<text lang="es">http://www.AdditionalRequirement.es/</text>
					</url>
				</additional_requirement>
				<additional_requirement>
					<name>Another additional requirement</name>
					<description>Description of another additional requirement</description>
					<url>
						<text lang="en">http://www.AnotherAdditionalRequirement.com/</text>
						<text lang="es">http://www.AnotherAdditionalRequirement.es/</text>
					</url>
				</additional_requirement>
			</additional_requirements_list>
		</factsheet_institution>'
		
		
		SET @errMsg = '';
		SET @errNum = 0;
		EXECUTE dbo.INSERT_FACTSHEET @xml_factsheet, @errMsg output, @errNum output
		
		IF @errNum < 0 
		BEGIN
			PRINT 'Error insertando factsheet ' 
			PRINT 'Error: ' + @errMsg
		END
		ELSE
		BEGIN
			PRINT 'insertado factsheet'
			PRINT 'Insertamos IIA '

			
			DECLARE @xml_iia xml 
			set @xml_iia= '<iia>
				<institution_id>'+ @v_inst_id + N'</institution_id>
				<start_date>2000-01-01</start_date>
				<end_date>2001-01-01</end_date>
				<cooperation_condition_list>
					<cooperation_condition>
						<start_date>2000-01-01</start_date>
						<end_date>2001-01-01</end_date>
						<eqf_level_list>
							<eqf_level>1</eqf_level>
							<eqf_level>2</eqf_level>
						</eqf_level_list>
						<mobility_number>1</mobility_number>
						<mobility_type>
							<mobility_category>Teaching</mobility_category>
							<mobility_group>Staff</mobility_group>
						</mobility_type>
						<receiving_partner>
							<institution>
								<institution_id>'+ @v_inst_id + N'</institution_id>
							</institution>
							<signing_date>2001-01-01</signing_date>
							<signer_person>
								<given_name>John Test</given_name>
								<family_name>Doe Test</family_name>
								<birth_date>2000-01-01</birth_date>
								<citizenship>ES</citizenship>
								<gender>1</gender>
								<contact>
									<contact_name>
										<text lang="en">Contact1 name</text>
										<text lang="es">Nombre contacto1</text>
									</contact_name>
									<contact_description>
										<text lang="en">Contact1 description</text>
										<text lang="es">Descripción contacto1</text>
									</contact_description>
									<contact_url>
										<text lang="en">https://www.ewp.org/contacts</text>
										<text lang="es">https://www.ewp.org/contactos</text>
									</contact_url>
									<contact_role>Contact1 Role</contact_role>
									<email_list>
										<email>email1@domain.com</email>
										<email>email2@domain.com</email>
									</email_list>
									<street_address>
										<recipient_name_list>
											<recipient_name>Contact1 Street Address RecName</recipient_name>
											<recipient_name>Street Address RecName Contact1</recipient_name>
										</recipient_name_list>
										<address_line_list>								
											<address_line>Contact1 Address line1</address_line>								
											<address_line>Address line2</address_line>								
										</address_line_list>
										<building_number>1</building_number>
										<building_name>Contact1 Building name</building_name>
										<street_name>Calle test</street_name>
										<unit>1</unit>
										<building_floor>Floor</building_floor>
										<post_office_box>PostOffice</post_office_box>
										<delivery_point_code_list>
											<delivery_point_code>DeliveryPointCode1</delivery_point_code>
											<delivery_point_code>DeliveryPointCode2</delivery_point_code>
										</delivery_point_code_list>
										<postal_code>46001</postal_code>
										<locality>Valencia</locality>
										<region>VLC</region>
										<country>Spain</country>
									</street_address>
									<phone>
										<e164>+34111222333</e164>
										<extension_number>1234</extension_number>
										<other_format>555-555-555</other_format>
									</phone>
								</contact>
							</signer_person>
							<partner_contacts_list>
								<partner_contact>
									<given_name>John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>Spain</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</receiving_partner>
						<sending_partner>
							<institution>
								<institution_id>'+ @v_receiving_inst_id + N'</institution_id>
							</institution>
							<signing_date>2000-01-01</signing_date>
							<signer_person>
								<given_name>John Test</given_name>
								<family_name>Doe Test</family_name>
								<birth_date>2000-01-01</birth_date>
								<citizenship>ES</citizenship>
								<gender>1</gender>
								<contact>
									<contact_name>
										<text lang="en">Contact1 name</text>
										<text lang="es">Nombre contacto1</text>
									</contact_name>
									<contact_description>
										<text lang="en">Contact1 description</text>
										<text lang="es">Descripción contacto1</text>
									</contact_description>
									<contact_url>
										<text lang="en">https://www.ewp.org/contacts</text>
										<text lang="es">https://www.ewp.org/contactos</text>
									</contact_url>
									<contact_role>Contact1 Role</contact_role>
									<email_list>
										<email>email1@domain.com</email>
										<email>email2@domain.com</email>
									</email_list>
									<street_address>
										<recipient_name_list>
											<recipient_name>Contact1 Street Address RecName</recipient_name>
											<recipient_name>Street Address RecName Contact1</recipient_name>
										</recipient_name_list>
										<address_line_list>								
											<address_line>Contact1 Address line1</address_line>								
											<address_line>Address line2</address_line>								
										</address_line_list>
										<building_number>1</building_number>
										<building_name>Contact1 Building name</building_name>
										<street_name>Calle test</street_name>
										<unit>1</unit>
										<building_floor>Floor</building_floor>
										<post_office_box>PostOffice</post_office_box>
										<delivery_point_code_list>
											<delivery_point_code>DeliveryPointCode1</delivery_point_code>
											<delivery_point_code>DeliveryPointCode2</delivery_point_code>
										</delivery_point_code_list>
										<postal_code>46001</postal_code>
										<locality>Valencia</locality>
										<region>VLC</region>
										<country>Spain</country>
									</street_address>
									<phone>
										<e164>+34111222333</e164>
										<extension_number>1234</extension_number>
										<other_format>555-555-555</other_format>
									</phone>
								</contact>
							</signer_person>
							<partner_contacts_list>
								<partner_contact>
									<given_name>John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>Spain</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</sending_partner>	
						<subject_area_language_list>
							<subject_area_language>
								<subject_area>
									<isced_code>06</isced_code>
									<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
								</subject_area>
								<language_skill>
									<language>EN</language>
									<cefr_level>B2</cefr_level>
								</language_skill>
							</subject_area_language>
						</subject_area_language_list>
						<cop_cond_duration>
							<number_duration>1</number_duration>
							<unit>1</unit>
						</cop_cond_duration>
						<other_info>Other Info</other_info>
						<blended>0</blended>
					</cooperation_condition>
					<cooperation_condition>
						<start_date>2000-01-01</start_date>
						<end_date>2001-01-01</end_date>
						<eqf_level_list>
							<eqf_level>1</eqf_level>
							<eqf_level>2</eqf_level>
						</eqf_level_list>
						<mobility_number>1</mobility_number>
						<mobility_type>
							<mobility_category>Studies</mobility_category>
							<mobility_group>Student</mobility_group>
						</mobility_type>
						<receiving_partner>
							<institution>
								<institution_id>'+ @v_receiving_inst_id + N'</institution_id>
							</institution>
							<signing_date>2001-01-01</signing_date>
							<signer_person>
								<given_name>John Test</given_name>
								<family_name>Doe Test</family_name>
								<birth_date>2000-01-01</birth_date>
								<citizenship>ES</citizenship>
								<gender>1</gender>
								<contact>
									<contact_name>
										<text lang="en">Contact1 name</text>
										<text lang="es">Nombre contacto1</text>
									</contact_name>
									<contact_description>
										<text lang="en">Contact1 description</text>
										<text lang="es">Descripción contacto1</text>
									</contact_description>
									<contact_url>
										<text lang="en">https://www.ewp.org/contacts</text>
										<text lang="es">https://www.ewp.org/contactos</text>
									</contact_url>
									<contact_role>Contact1 Role</contact_role>
									<email_list>
										<email>email1@domain.com</email>
										<email>email2@domain.com</email>
									</email_list>
									<street_address>
										<recipient_name_list>
											<recipient_name>Contact1 Street Address RecName</recipient_name>
											<recipient_name>Street Address RecName Contact1</recipient_name>
										</recipient_name_list>
										<address_line_list>								
											<address_line>Contact1 Address line1</address_line>								
											<address_line>Address line2</address_line>								
										</address_line_list>
										<building_number>1</building_number>
										<building_name>Contact1 Building name</building_name>
										<street_name>Calle test</street_name>
										<unit>1</unit>
										<building_floor>Floor</building_floor>
										<post_office_box>PostOffice</post_office_box>
										<delivery_point_code_list>
											<delivery_point_code>DeliveryPointCode1</delivery_point_code>
											<delivery_point_code>DeliveryPointCode2</delivery_point_code>
										</delivery_point_code_list>
										<postal_code>46001</postal_code>
										<locality>Valencia</locality>
										<region>VLC</region>
										<country>Spain</country>
									</street_address>
									<phone>
										<e164>+34111222333</e164>
										<extension_number>1234</extension_number>
										<other_format>555-555-555</other_format>
									</phone>
								</contact>
							</signer_person>
							<partner_contacts_list>
								<partner_contact>
									<given_name>John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>Spain</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</receiving_partner>
						<sending_partner>
							<institution>
								<institution_id>'+ @v_inst_id + N'</institution_id>
							</institution>
							<signing_date>2000-01-01</signing_date>
							<signer_person>
								<given_name>John Test</given_name>
								<family_name>Doe Test</family_name>
								<birth_date>2000-01-01</birth_date>
								<citizenship>ES</citizenship>
								<gender>1</gender>
								<contact>
									<contact_name>
										<text lang="en">Contact1 name</text>
										<text lang="es">Nombre contacto1</text>
									</contact_name>
									<contact_description>
										<text lang="en">Contact1 description</text>
										<text lang="es">Descripción contacto1</text>
									</contact_description>
									<contact_url>
										<text lang="en">https://www.ewp.org/contacts</text>
										<text lang="es">https://www.ewp.org/contactos</text>
									</contact_url>
									<contact_role>Contact1 Role</contact_role>
									<email_list>
										<email>email1@domain.com</email>
										<email>email2@domain.com</email>
									</email_list>
									<street_address>
										<recipient_name_list>
											<recipient_name>Contact1 Street Address RecName</recipient_name>
											<recipient_name>Street Address RecName Contact1</recipient_name>
										</recipient_name_list>
										<address_line_list>								
											<address_line>Contact1 Address line1</address_line>								
											<address_line>Address line2</address_line>								
										</address_line_list>
										<building_number>1</building_number>
										<building_name>Contact1 Building name</building_name>
										<street_name>Calle test</street_name>
										<unit>1</unit>
										<building_floor>Floor</building_floor>
										<post_office_box>PostOffice</post_office_box>
										<delivery_point_code_list>
											<delivery_point_code>DeliveryPointCode1</delivery_point_code>
											<delivery_point_code>DeliveryPointCode2</delivery_point_code>
										</delivery_point_code_list>
										<postal_code>46001</postal_code>
										<locality>Valencia</locality>
										<region>VLC</region>
										<country>Spain</country>
									</street_address>
									<phone>
										<e164>+34111222333</e164>
										<extension_number>1234</extension_number>
										<other_format>555-555-555</other_format>
									</phone>
								</contact>
							</signer_person>
							<partner_contacts_list>
								<partner_contact>
									<given_name>John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>Spain</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</sending_partner>	
						<subject_area_language_list>
							<subject_area_language>
								<subject_area>
									<isced_code>06</isced_code>
									<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
								</subject_area>
								<language_skill>
									<language>EN</language>
									<cefr_level>B2</cefr_level>
								</language_skill>
							</subject_area_language>
						</subject_area_language_list>
						<cop_cond_duration>
							<number_duration>1</number_duration>
							<unit>1</unit>
						</cop_cond_duration>
						<other_info>Other Info</other_info>
						<blended>0</blended>
					</cooperation_condition>
				</cooperation_condition_list>
				<pdf></pdf>
			</iia>'
			
			SET @errMsg = '';
			SET @errNum = 0;
			EXECUTE dbo.INSERT_IIA @xml_iia, @IIA_ID output, @errMsg output, @v_receiving_inst_id, @errNum output
		
			IF @errNum < 0 
			BEGIN
				PRINT 'Error insertando IIA ' 
				PRINT 'Error: ' + @errMsg
			END
			ELSE
			BEGIN
				PRINT 'insertado IIA'
				PRINT 'Insertamos Mobility'

				SET @NVARCHAR_IIA_ID = CONVERT(NVARCHAR(37),@IIA_ID);

				DECLARE @xml_mobility xml 
				set @xml_mobility= '<mobility>
					<sending_institution>
						<institution_id>'+@v_inst_id+N'</institution_id>
					</sending_institution>
					<receiving_institution>
						<institution_id>'+@v_receiving_inst_id+N'</institution_id>
					</receiving_institution>
					<planed_arrival_date>2021-01-01</planed_arrival_date>
					<planed_depature_date>2021-06-01</planed_depature_date>
					<iia_id>'+@NVARCHAR_IIA_ID+N'</iia_id>
					<student>
						<global_id>urn:schac:personalUniqueCode:int:esi:example.edu:xxxxxxxxxx</global_id>
						<contact_person>
							<given_name>John Test</given_name>
							<family_name>Doe Test</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact1 name</text>
									<text lang="es">Nombre contacto1</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact1 description</text>
									<text lang="es">Descripción contacto1</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Contact1 Role</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact1 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact1</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact1 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact1 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>Spain</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
						</contact_person>
						<photo_url_list>
							<photo_url>
								<url>https://imagenes.com/foto1</url>
								<photo_size>35x45</photo_size>
								<photo_date>2012-01-01</photo_date>
								<photo_public>0</photo_public>
							</photo_url>
							<photo_url>
								<url>https://imagenes.com/foto2</url>
								<photo_size>35x45</photo_size>
								<photo_date>2012-01-01</photo_date>
								<photo_public>1</photo_public>
							</photo_url>
						</photo_url_list>
					</student>
					<subject_area>
						<isced_code>06</isced_code>
						<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
					</subject_area>
					<student_language_skill_list>
						<student_language_skill>
							<language>EN</language>
							<cefr_level>B2</cefr_level>
						</student_language_skill>
					</student_language_skill_list>
					<sender_contact>
						<given_name>John Test</given_name>
							<family_name>Doe Test</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact1 name</text>
									<text lang="es">Nombre contacto1</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact1 description</text>
									<text lang="es">Descripción contacto1</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Contact1 Role</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact1 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact1</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact1 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact1 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>Spain</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
					</sender_contact>
					<sender_admv_contact>
						<given_name>John Test2</given_name>
							<family_name>Doe Test2</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact2 name</text>
									<text lang="es">Nombre contacto2</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact2 description</text>
									<text lang="es">Descripción contacto2</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Contact2 Role</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact2 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact2</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact2 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact2 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>Spain</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
					</sender_admv_contact>
					<receiver_contact>
						<given_name>John Test</given_name>
							<family_name>Doe Test</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact1 name</text>
									<text lang="es">Nombre contacto1</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact1 description</text>
									<text lang="es">Descripción contacto1</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Contact1 Role</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact1 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact1</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact1 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact1 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>Spain</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
					</receiver_contact>
					<receiver_admv_contact>
						<given_name>John Test2</given_name>
							<family_name>Doe Test2</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact2 name</text>
									<text lang="es">Nombre contacto2</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact2 description</text>
									<text lang="es">Descripción contacto2</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Contact2 Role</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact2 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact2</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact2 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact2 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>Spain</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
					</receiver_admv_contact>
					<mobility_type>Semester</mobility_type>
					<eqf_level_nomination>1</eqf_level_nomination>
					<eqf_level_departure>1</eqf_level_departure>
					<academic_term>
						<academic_year>2021/2021</academic_year>
						<institution_id>'+@v_inst_id+N'</institution_id>
						<start_date>2021-01-01</start_date>	
						<end_date>2021-06-01</end_date>	
						<description>
							<text lang="en">First Semester</text>
							<text lang="es">Primer Semestre</text>
						</description>
						<term_number>1</term_number>		
						<total_terms>2</total_terms>	
					</academic_term>
					<status_outgoing>1</status_outgoing>
				</mobility>'
				
				SET @errMsg = '';
				SET @errNum = 0;
				EXECUTE dbo.INSERT_MOBILITY @xml_mobility, @MOBILITY_ID  OUTPUT, @errMsg OUTPUT, @v_receiving_inst_id, @errNum OUTPUT
		
				IF @errNum < 0 
				BEGIN
					PRINT 'Error insertando Mobility ' 
					PRINT 'Error: ' + @errMsg
				END
				ELSE
				BEGIN
					PRINT 'insertado Mobility'
					PRINT 'Insertamos LA'
					SET @NVARCHAR_MOBILITY_ID = CONVERT(NVARCHAR(37),@MOBILITY_ID);
					
					DECLARE @xml xml
					SET @xml ='<learning_agreement>
						<mobility_id>'+@NVARCHAR_MOBILITY_ID+N'</mobility_id>
						<student_la>
							<given_name>John Test</given_name>
							<family_name>Doe Test</family_name>
							<birth_date>2001-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>	
						</student_la>
						<la_component_list>
							<la_component>
								<la_component_type>0</la_component_type>
								<los_code>LOSCODE</los_code>
								<title>Studied Component Title</title>
								<academic_term>
									<academic_year>2021/2021</academic_year>
									<institution_id>'+@v_receiving_inst_id+N'</institution_id>
									<start_date>2021-01-01</start_date>	
									<end_date>2021-06-01</end_date>	
									<description>
										<text lang="en">First Semester 2000</text>
										<text lang="es">Primer Semestre 2000</text>
									</description>
									<term_number>1</term_number>		
									<total_terms>2</total_terms>	
								</academic_term>
								<credit>
									<scheme>ECTS</scheme>
									<credit_value>60</credit_value>
								</credit>
							</la_component>
							<la_component>
								<la_component_type>1</la_component_type>
								<los_code>OtherLOSCODE</los_code>
								<title>Recognized Component Title</title>
								<academic_term>
									<academic_year>2021/2021</academic_year>
									<institution_id>'+@v_inst_id+N'</institution_id>
									<start_date>2021-01-01</start_date>	
									<end_date>2021-06-01</end_date>	
									<description>
										<text lang="en">First Semester 2000</text>
										<text lang="es">Primer Semestre 2000</text>
									</description>
									<term_number>1</term_number>		
									<total_terms>2</total_terms>	
								</academic_term>
								<credit>
									<scheme>ECTS</scheme>
									<credit_value>60</credit_value>
								</credit>
							</la_component>
						</la_component_list>
						<student_signature>
							<signer_name>John Test</signer_name>
							<signer_position>Doe Test</signer_position>	
							<signer_email>email@domain.com</signer_email>	
							<sign_date>2001-01-01T08:16:00</sign_date>		
							<signer_app>InHouse</signer_app>	
							<signature>STRING</signature>	
						</student_signature>
						<sending_hei_signature>
							<signer_name>John Test</signer_name>
							<signer_position>Doe Test</signer_position>	
							<signer_email>email@domain.com</signer_email>	
							<sign_date>2001-01-01T08:16:00</sign_date>		
							<signer_app>InHouse</signer_app>	
							<signature>STRING</signature>	
						</sending_hei_signature>
					</learning_agreement>'
					
					SET @errMsg = '';
					SET @errNum = 0;
					EXECUTE dbo.INSERT_LEARNING_AGREEMENT @xml, @LA_ID output, @errMsg output, @v_receiving_inst_id, @errNum output
		
					IF @errNum < 0 
					BEGIN
						PRINT 'Error insertando LA ' 
						PRINT 'Error: ' + @errMsg
					END
					ELSE
					BEGIN
						PRINT 'insertado LA'
						PRINT 'Fin'
					--LA
					END
				--Mobility
				END
			--IIA
			END
		--Factsheet
		END
	--ounit
	END
--institucion
END
 