
DECLARE @xmlApprove xml
SET @xmlApprove ='<req:omobility-las-update-request
    xmlns:req="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:la="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd"
    xsi:schemaLocation="
        https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd
        https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd
    "
>
    <req:sending-hei-id>UV.ES</req:sending-hei-id>
    <req:approve-proposal-v1>
        <req:omobility-id>C65B4D31-E979-0CBC-E053-1FC616ACBA85</req:omobility-id>
        <req:changes-proposal-id>C65B4D31-E97D-0CBC-E053-1FC616ACBA85-1</req:changes-proposal-id>
        <req:signature>
            <la:signer-name>Paweł Tomasz Kowalski</la:signer-name>
            <la:signer-position>Mobility coordinator</la:signer-position>
            <la:signer-email>pawel.kowalski@example.com</la:signer-email>
            <la:timestamp>2018-11-14T08:18:19+02:00</la:timestamp>
            <la:signer-app>USOS</la:signer-app>
        </req:signature>
    </req:approve-proposal-v1>
</req:omobility-las-update-request>
'

DECLARE @xmlComment xml
SET @xmlComment ='<req:omobility-las-update-request
    xmlns:req="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:la="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd"
    xsi:schemaLocation="
        https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd
        https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd
    "
><req:sending-hei-id>UV.ES</req:sending-hei-id>
    <req:comment-proposal-v1>
        <req:omobility-id>C65B4D31-E979-0CBC-E053-1FC616ACBA85</req:omobility-id>
        <req:changes-proposal-id>C65B4D31-E97D-0CBC-E053-1FC616ACBA85-0</req:changes-proposal-id>
        <req:comment>"Introductory calculus" is no longer conducted. We suggest replacing it with "Calculus I".</req:comment>
        <req:signature>
            <la:signer-name>Paweł Tomasz Kowalski</la:signer-name>
            <la:signer-position>Mobility coordinator</la:signer-position>
            <la:signer-email>pawel.kowalski@example.com</la:signer-email>
            <la:timestamp>2018-11-14T08:18:19+02:00</la:timestamp>
            <la:signer-app>USOS</la:signer-app>
        </req:signature>
    </req:comment-proposal-v1>
</req:omobility-las-update-request>
'



DECLARE @errMsg varchar(255)
DECLARE @errNum integer


--EXECUTE dbo.VALIDA_ACCEPT_PROPOSAL @xmlApprove, @errMsg output, @errNum output

--EXECUTE dbo.VALIDA_COMMENT_PROPOSAL @xmlComment, @errMsg output, @errNum output

EXECUTE dbo.ACCEPT_LEARNING_AGREEMENT_PROPOSAL '8ece1bb8-2cb1-43e5-a1c4-dc50d7d14780', 1, @xmlApprove, @errMsg output, @errNum output

--EXECUTE dbo.REJECT_LEARNING_AGREEMENT_PROPOSAL '8ece1bb8-2cb1-43e5-a1c4-dc50d7d14780', 0, @xmlComment, @errMsg output, @errNum output

PRINT @errMsg
PRINT @errNum