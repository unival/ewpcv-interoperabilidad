DECLARE  @v_inst_id  NVARCHAR(9)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';

DECLARE @xml xml 
set @xml= '<los>
	<eqf_level>1</eqf_level>
	<institution_id>'+@v_inst_id+N'</institution_id>
	<iscedf>8</iscedf>
	<los_code>LOS Code</los_code>
	<los_name_list>
		<text lang="en">Name</text>
		<text lang="es">Nombre</text>
	</los_name_list>
	<los_description_list>
		<text lang="en">Description</text>
		<text lang="es">Descripcion</text>
	</los_description_list>
	<los_url_list>
		<text lang="en">http://losurl.com</text>
		<text lang="es">http://losurl.es</text>
	</los_url_list>
	<subject_area>7</subject_area>
	<los_type>1</los_type>
</los>'


DECLARE @errMsg varchar(255)
DECLARE @errNum integer
DECLARE @LOS_ID varchar(255)
DECLARE @LOS_PARENT_ID varchar(255)

--EXECUTE dbo.VALIDA_LOS @xml, @errMsg output, @errNum output

EXECUTE dbo.INSERT_LOS @xml, NULL, @LOS_ID output, @errMsg output, @errNum output

--EXECUTE dbo.UPDATE_OUNIT '8BFAE0CB-6C46-433B-8B02-562C00938500', @xml, @errMsg output, @errNum output

--EXECUTE dbo.DELETE_OUNIT 'B83D02CE-64AC-4E17-88FA-B62F09515D1B', @errMsg output, @errNum output


PRINT @errMsg
PRINT @errNum
PRINT 'LOS_ID: ' 
PRINT @LOS_ID