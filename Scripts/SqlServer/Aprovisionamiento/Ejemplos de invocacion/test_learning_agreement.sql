
DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_inst_id  NVARCHAR(9)
DECLARE  @v_iia_id  NVARCHAR(37)
DECLARE  @v_mobility_id  NVARCHAR(37)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'uv.es';
SET @v_iia_id = 'C15FF0A8-F4A2-479B-A3B3-6324D039E24B';
SET @v_mobility_id = '6DCFC3AC-601F-4A30-BD62-7A51D4AB9F07';

DECLARE @xml xml
SET @xml ='<learning_agreement>
	<mobility_id>'+@v_mobility_id+N'</mobility_id>
	<student_la>
		<given_name>John Test</given_name>
		<family_name>Doe Test</family_name>
		<birth_date>2001-01-01</birth_date>
		<citizenship>ES</citizenship>
		<gender>1</gender>	
	</student_la>
	<la_component_list>
		<la_component>
			<la_component_type>0</la_component_type>
			<los_code>LOS CODE UV</los_code>
			<los_type>0</los_type>
			<title>Studied Component Title</title>
			<academic_term>
				<academic_year>2000/2001</academic_year>
				<institution_id>'+@v_receiving_inst_id+N'</institution_id>
				<start_date>2000-01-01</start_date>	
				<end_date>2001-06-01</end_date>	
				<description>
					<text lang="en">First Semester 2000</text>
					<text lang="es">Primer Semestre 2000</text>
				</description>
				<term_number>1</term_number>		
				<total_terms>2</total_terms>	
			</academic_term>
			<credit>
				<scheme>ECTS</scheme>
				<credit_value>60</credit_value>
			</credit>
		</la_component>
		<la_component>
			<la_component_type>1</la_component_type>
			<los_code>OtherLOSCODE</los_code>
			<title>Recognized Component Title</title>
			<academic_term>
				<academic_year>2000/2001</academic_year>
				<institution_id>'+@v_inst_id+N'</institution_id>
				<start_date>2000-01-01</start_date>	
				<end_date>2001-06-01</end_date>	
				<description>
					<text lang="en">First Semester 2000</text>
					<text lang="es">Primer Semestre 2000</text>
				</description>
				<term_number>1</term_number>		
				<total_terms>2</total_terms>	
			</academic_term>
			<credit>
				<scheme>ECTS</scheme>
				<credit_value>60</credit_value>
			</credit>
		</la_component>
	</la_component_list>
	<student_signature>
		<signer_name>John Test</signer_name>
		<signer_position>Doe Test</signer_position>	
		<signer_email>email@domain.com</signer_email>	
		<sign_date>2001-01-01T08:16:00</sign_date>		
		<signer_app>InHouse</signer_app>	
		<signature>STRING</signature>	
	</student_signature>
	<sending_hei_signature>
		<signer_name>John Test</signer_name>
		<signer_position>Doe Test</signer_position>	
		<signer_email>email@domain.com</signer_email>	
		<sign_date>2001-01-01T08:16:00</sign_date>		
		<signer_app>InHouse</signer_app>	
		<signature>STRING</signature>	
	</sending_hei_signature>
</learning_agreement>'



DECLARE @errMsg varchar(255)
DECLARE @errNum integer
DECLARE @LA_ID uniqueidentifier




--EXECUTE dbo.VALIDA_LEARNING_AGREEMENT @xml, @errMsg output, @errNum output

EXECUTE dbo.INSERT_LEARNING_AGREEMENT @xml, @LA_ID output, @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.UPDATE_LEARNING_AGREEMENT '410B32F9-7E92-4BE9-8023-CAB79ACF123A', @xml, @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.DELETE_LEARNING_AGREEMENT 'C4BE013E-F266-4E35-A14E-7E2F746BB703', @errMsg output, @v_receiving_inst_id, @errNum output


PRINT @errMsg
PRINT @errNum
PRINT ' LA ID: ' 
PRINT @LA_ID

