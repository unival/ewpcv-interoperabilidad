DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_inst_id  NVARCHAR(9)
DECLARE  @v_iia_id  NVARCHAR(37)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'uv.es';
SET @v_iia_id = '50AEA797-2596-45A0-9237-8419094117BF';

DECLARE @xml xml 
set @xml= '<mobility>
	<sending_institution>
		<institution_id>'+@v_inst_id+N'</institution_id>
	</sending_institution>
	<receiving_institution>
		<institution_id>'+@v_receiving_inst_id+N'</institution_id>
	</receiving_institution>
	<planed_arrival_date>2000-01-01</planed_arrival_date>
	<planed_depature_date>2000-01-01</planed_depature_date>
	
	<student>
		<global_id>urn:schac:personalUniqueCode:int:esi:example.edu:xxxxxxxxxx</global_id>
		<contact_person>
			<given_name>John Test</given_name>
			<family_name>Doe Test</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact1 name</text>
					<text lang="es">Nombre contacto1</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact1 description</text>
					<text lang="es">Descripción contacto1</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact1 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact1 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact1</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact1 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact1 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
		</contact_person>
		<photo_url_list>
			<photo_url>
				<url>https://imagenes.com/foto1</url>
				<photo_size>35x45</photo_size>
				<photo_date>2012-01-01</photo_date>
				<photo_public>0</photo_public>
			</photo_url>
			<photo_url>
				<url>https://imagenes.com/foto2</url>
				<photo_size>35x45</photo_size>
				<photo_date>2012-01-01</photo_date>
				<photo_public>1</photo_public>
			</photo_url>
		</photo_url_list>
	</student>
	<subject_area>
		<isced_code>06</isced_code>
		<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
	</subject_area>
	<student_language_skill_list>
		<student_language_skill>
			<language>EN</language>
			<cefr_level>B2</cefr_level>
		</student_language_skill>
	</student_language_skill_list>
	<sender_contact>
		<given_name>John Test</given_name>
			<family_name>Doe Test</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact1 name</text>
					<text lang="es">Nombre contacto1</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact1 description</text>
					<text lang="es">Descripción contacto1</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact1 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact1 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact1</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact1 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact1 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
	</sender_contact>
	<sender_admv_contact>
		<given_name>John Test2</given_name>
			<family_name>Doe Test2</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact2 name</text>
					<text lang="es">Nombre contacto2</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact2 description</text>
					<text lang="es">Descripción contacto2</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact2 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact2 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact2</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact2 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact2 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
	</sender_admv_contact>
	<receiver_contact>
		<given_name>John Test</given_name>
			<family_name>Doe Test</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact1 name</text>
					<text lang="es">Nombre contacto1</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact1 description</text>
					<text lang="es">Descripción contacto1</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact1 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact1 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact1</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact1 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact1 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
	</receiver_contact>
	<receiver_admv_contact>
		<given_name>John Test2</given_name>
			<family_name>Doe Test2</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact2 name</text>
					<text lang="es">Nombre contacto2</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact2 description</text>
					<text lang="es">Descripción contacto2</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact2 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact2 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact2</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact2 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact2 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
	</receiver_admv_contact>
	<mobility_type>Semester</mobility_type>
	<eqf_level_nomination>1</eqf_level_nomination>
	<eqf_level_departure>1</eqf_level_departure>
	<academic_term>
		<academic_year>2021/2022</academic_year>
		<institution_id>'+@v_inst_id+N'</institution_id>
		<start_date>2000-01-01</start_date>	
		<end_date>2000-01-01</end_date>	
		<description>
			<text lang="en">First Semester</text>
			<text lang="es">Primer Semestre</text>
		</description>
		<term_number>1</term_number>		
		<total_terms>2</total_terms>	
	</academic_term>
	<status_outgoing>1</status_outgoing>
</mobility>'

DECLARE @errMsg varchar(255)
DECLARE @errNum integer
declare @P_OMOBILITY_ID uniqueidentifier	


--EXECUTE dbo.VALIDA_MOBILITY @xml, @errMsg output, @errNum output

--EXECUTE dbo.VALIDA_DATOS_MOBILITY @xml, @errMsg OUTPUT, @errNum OUTPUT

--EXECUTE dbo.INSERT_MOBILITY @xml, @P_OMOBILITY_ID  OUTPUT, @errMsg OUTPUT, @v_receiving_inst_id, @errNum OUTPUT

--EXECUTE dbo.UPDATE_MOBILITY '53283BF1-8119-4EB1-9D24-0DC05A464C26', @xml, @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.DELETE_MOBILITY '35252B1B-72EB-4D34-BE8B-EC6D2D9EDA91',  @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.REJECT_NOMINATION 'd3bc8b44-9680-14ec-e053-1fc616acf2b8', 'las fechas estan mal', @errMsg output, 'uv.es', @errNum output

EXECUTE dbo.APPROVE_NOMINATION 'd3bc8b44-9680-14ec-e053-1fc616acf2b8', 'Todo en orden', @errMsg output, 'uv.es', @errNum output

PRINT @errMsg
PRINT @errNum
PRINT @P_OMOBILITY_ID

