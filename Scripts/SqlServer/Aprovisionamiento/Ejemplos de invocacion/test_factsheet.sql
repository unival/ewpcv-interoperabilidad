DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_inst_id  NVARCHAR(9)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'validator-hei01.developers.erasmuswithoutpaper.eu';

DECLARE @xml xml 
set @xml= '<factsheet_institution>
	<institution_id>'+@v_inst_id + N'</institution_id>
	<logo_url>https://www.abc.es/</logo_url>
	<abreviation>Abreviacion</abreviation>
	<factsheet>
		<decision_week_limit>5</decision_week_limit>
		<tor_week_limit>5</tor_week_limit>
		<nominations_autum_term>2000-01-01</nominations_autum_term>
		<nominations_spring_term>2000-01-01</nominations_spring_term>
		<application_autum_term>2000-01-01</application_autum_term>
		<application_spring_term>2000-01-01</application_spring_term>
	</factsheet>
	<application_info>
		<email>application.email@domain.com</email>
		<phone>
			<e164>+34111222333</e164>
			<extension_number>1234</extension_number>
			<other_format>(23) 456789</other_format>
		</phone>
		<url>
			<text lang="en">https://www.application.com/</text>
			<text lang="es">https://www.application.es/</text>
		</url>
	</application_info>
	<housing_info>
		<email>housing.email@domain.com</email>
		<phone>
			<e164>+34111222333</e164>
			<extension_number>1234</extension_number>
			<other_format>(23) 456789</other_format>
		</phone>
		<url>
			<text lang="en">https://www.housing.com/</text>
			<text lang="es">https://www.housing.es/</text>
		</url>
	</housing_info>
	<visa_info>
		<email>visa.email@domain.com</email>
		<phone>
			<e164>+34111222333</e164>
			<extension_number>1234</extension_number>
			<other_format>(23) 456789</other_format>
		</phone>
		<url>
			<text lang="en">https://www.visa.com/</text>
			<text lang="es">https://www.visa.es/</text>
		</url>
	</visa_info>
	<insurance_info>
		<email>insurance.email@domain.com</email>
		<phone>
			<e164>+34111222333</e164>
			<extension_number>1234</extension_number>
			<other_format>(23) 456789</other_format>
		</phone>
		<url>
			<text lang="en">https://www.insurance.com/</text>
			<text lang="es">https://www.insurance.es/</text>
		</url>
	</insurance_info>
	<additional_info_list>
		<additional_info>
			<info_type>Additional information</info_type>
			<information_item>
				<email>email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.additionalinfo.com/</text>
					<text lang="es">https://www.additionalinfo.es/</text>
				</url>
			</information_item>
		</additional_info>
		<additional_info>
			<info_type>More additional information</info_type>
			<information_item>
				<email>email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>78687</extension_number>
					<other_format>(55) 86778687u</other_format>
				</phone>
				<url>
					<text lang="en">https://www.moreadditionalinfo.com/</text>
					<text lang="es">https://www.moreadditionalinfo.es/</text>
				</url>
			</information_item>
		</additional_info>
	</additional_info_list>
	<accessibility_requirements_list>
		<accessibility_requirement>
			<req_type>infrastructure</req_type>
			<name>Infraestructure accessibility requirement</name>
			<description>Description of infraestructure accessibility requirement</description>
			<information_item>
				<email>email@domain.com</email>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">https://www.InfraestructureAccessibilityRequirement.com/</text>
					<text lang="es">https://www.InfraestructureAccessibilityRequirement.es/</text>
				</url>
			</information_item>
		</accessibility_requirement>
		<accessibility_requirement>
			<req_type>service</req_type>
			<name>Service accessibility requirement</name>
			<description>Description of service accessibility requirement</description>
			<information_item>
				<email>email@domain.com</email>
				<phone>
					<e164>+4722222222</e164>
					<extension_number>1234</extension_number>
					<other_format>(23) 456789</other_format>
				</phone>
				<url>
					<text lang="en">http://www.ServiceAccessibilityRequirement.com/</text>
					<text lang="es">http://www.ServiceAccessibilityRequirement.es/</text>
				</url>
			</information_item>
		</accessibility_requirement>
	</accessibility_requirements_list>
	<additional_requirements_list>
		<additional_requirement>
			<name>Additional requirement</name>
			<description>Description of additional requirement</description>
			<url>
				<text lang="en">http://www.AdditionalRequirement.com/</text>
				<text lang="es">http://www.AdditionalRequirement.es/</text>
			</url>
		</additional_requirement>
		<additional_requirement>
			<name>Another additional requirement</name>
			<description>Description of another additional requirement</description>
			<url>
				<text lang="en">http://www.AnotherAdditionalRequirement.com/</text>
				<text lang="es">http://www.AnotherAdditionalRequirement.es/</text>
			</url>
		</additional_requirement>
	</additional_requirements_list>
</factsheet_institution>'



DECLARE @errMsg varchar(255)
DECLARE @errNum integer

--EXECUTE dbo.VALIDA_FACTSHEET_INSTITUTION @xml, @errMsg output, @errNum output

--EXECUTE dbo.INSERT_FACTSHEET @xml, @errMsg output, @errNum output

--EXECUTE dbo.UPDATE_FACTSHEET 'UM.ES', @xml, @errMsg output, @errNum output

--EXECUTE dbo.DELETE_FACTSHEET 'UM.ES', @errMsg output, @errNum output


PRINT @errMsg
PRINT @errNum


