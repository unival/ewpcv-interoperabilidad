
DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_r_ounit_code  NVARCHAR(11)
DECLARE  @v_inst_id  NVARCHAR(9)
DECLARE  @v_ounit_code  NVARCHAR(10)
SET @v_inst_id = 'unican.es';
SET @v_ounit_code = 'ounit_code';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'validator-hei01.developers.erasmuswithoutpaper.eu';
SET @v_r_ounit_code = 'ounit_code2';

DECLARE @xml xml 
set @xml= '<iia>
				<iia_code>iia_code</iia_code>
				<start_date>2000-01-01</start_date>
				<end_date>2001-01-01</end_date>
				<first_partner>
					<institution>
						<institution_id>'+@v_inst_id + N'</institution_id>
						<organization_unit_code>'+@v_ounit_code + N'</organization_unit_code>
					</institution>
					<signing_date>2001-01-01</signing_date>
					<signer_person>
						<given_name>Perico</given_name>
						<family_name>de los palotes </family_name>
						<birth_date>2000-01-01</birth_date>
						<citizenship>ES</citizenship>
						<gender>1</gender>
						<contact>
							<contact_name>
								<text lang="en">Contact1 name</text>
								<text lang="es">Nombre contacto1</text>
							</contact_name>
							<contact_description>
								<text lang="en">Contact1 description</text>
								<text lang="es">Descripción contacto1</text>
							</contact_description>
							<contact_url>
								<text lang="en">https://www.ewp.org/contacts</text>
								<text lang="es">https://www.ewp.org/contactos</text>
							</contact_url>
							<contact_role>First Partner Singer</contact_role>
							<email_list>
								<email>email1@domain.com</email>
								<email>email2@domain.com</email>
							</email_list>
							<street_address>
								<recipient_name_list>
									<recipient_name>Contact1 Street Address RecName</recipient_name>
									<recipient_name>Street Address RecName Contact1</recipient_name>
								</recipient_name_list>
								<address_line_list>								
									<address_line>Contact1 Address line1</address_line>								
									<address_line>Address line2</address_line>								
								</address_line_list>
								<building_number>1</building_number>
								<building_name>Contact1 Building name</building_name>
								<street_name>Calle test</street_name>
								<unit>1</unit>
								<building_floor>Floor</building_floor>
								<post_office_box>PostOffice</post_office_box>
								<delivery_point_code_list>
									<delivery_point_code>DeliveryPointCode1</delivery_point_code>
									<delivery_point_code>DeliveryPointCode2</delivery_point_code>
								</delivery_point_code_list>
								<postal_code>46001</postal_code>
								<locality>Valencia</locality>
								<region>VLC</region>
								<country>ES</country>
							</street_address>
							<phone>
								<e164>+34111222333</e164>
								<extension_number>1234</extension_number>
								<other_format>555-555-555</other_format>
							</phone>
						</contact>
					</signer_person>
					<partner_contacts_list>
						<partner_contact>
							<given_name>FPC John Test2</given_name>
							<family_name>Doe Test2</family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact2 name</text>
									<text lang="es">Nombre contacto2</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact2 description</text>
									<text lang="es">Descripción contacto2</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>First Partner Contac</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact2 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact2</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact2 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact2 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>ES</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
						</partner_contact>
					</partner_contacts_list>
				</first_partner>
				<second_partner>
					<institution>
						<institution_id>'+@v_receiving_inst_id + N'</institution_id>
						<organization_unit_code>'+@v_r_ounit_code + N'</organization_unit_code>
					</institution>
					<signing_date>2001-01-01</signing_date>
					<signer_person>
						<given_name>SPS John</given_name>
						<family_name>Doe </family_name>
						<birth_date>2000-01-01</birth_date>
						<citizenship>ES</citizenship>
						<gender>1</gender>
						<contact>
							<contact_name>
								<text lang="en">Contact1 name</text>
								<text lang="es">Nombre contacto1</text>
							</contact_name>
							<contact_description>
								<text lang="en">Contact1 description</text>
								<text lang="es">Descripción contacto1</text>
							</contact_description>
							<contact_url>
								<text lang="en">https://www.ewp.org/contacts</text>
								<text lang="es">https://www.ewp.org/contactos</text>
							</contact_url>
							<contact_role>Second Partner Singer</contact_role>
							<email_list>
								<email>email1@domain.com</email>
								<email>email2@domain.com</email>
							</email_list>
							<street_address>
								<recipient_name_list>
									<recipient_name>Contact1 Street Address RecName</recipient_name>
									<recipient_name>Street Address RecName Contact1</recipient_name>
								</recipient_name_list>
								<address_line_list>								
									<address_line>Contact1 Address line1</address_line>								
									<address_line>Address line2</address_line>								
								</address_line_list>
								<building_number>1</building_number>
								<building_name>Contact1 Building name</building_name>
								<street_name>Calle test</street_name>
								<unit>1</unit>
								<building_floor>Floor</building_floor>
								<post_office_box>PostOffice</post_office_box>
								<delivery_point_code_list>
									<delivery_point_code>DeliveryPointCode1</delivery_point_code>
									<delivery_point_code>DeliveryPointCode2</delivery_point_code>
								</delivery_point_code_list>
								<postal_code>46001</postal_code>
								<locality>Valencia</locality>
								<region>VLC</region>
								<country>ES</country>
							</street_address>
							<phone>
								<e164>+34111222333</e164>
								<extension_number>1234</extension_number>
								<other_format>555-555-555</other_format>
							</phone>
						</contact>
					</signer_person>
					<partner_contacts_list>
						<partner_contact>
							<given_name>Maria </given_name>
							<family_name>Sarmiento </family_name>
							<birth_date>2000-01-01</birth_date>
							<citizenship>ES</citizenship>
							<gender>1</gender>
							<contact>
								<contact_name>
									<text lang="en">Contact2 name</text>
									<text lang="es">Nombre contacto2</text>
								</contact_name>
								<contact_description>
									<text lang="en">Contact2 description</text>
									<text lang="es">Descripción contacto2</text>
								</contact_description>
								<contact_url>
									<text lang="en">https://www.ewp.org/contacts</text>
									<text lang="es">https://www.ewp.org/contactos</text>
								</contact_url>
								<contact_role>Second Partner Contac</contact_role>
								<email_list>
									<email>email1@domain.com</email>
									<email>email2@domain.com</email>
								</email_list>
								<street_address>
									<recipient_name_list>
										<recipient_name>Contact2 Street Address RecName</recipient_name>
										<recipient_name>Street Address RecName Contact2</recipient_name>
									</recipient_name_list>
									<address_line_list>								
										<address_line>Contact2 Address line1</address_line>								
										<address_line>Address line2</address_line>								
									</address_line_list>
									<building_number>1</building_number>
									<building_name>Contact2 Building name</building_name>
									<street_name>Calle test</street_name>
									<unit>1</unit>
									<building_floor>Floor</building_floor>
									<post_office_box>PostOffice</post_office_box>
									<delivery_point_code_list>
										<delivery_point_code>DeliveryPointCode1</delivery_point_code>
										<delivery_point_code>DeliveryPointCode2</delivery_point_code>
									</delivery_point_code_list>
									<postal_code>46001</postal_code>
									<locality>Valencia</locality>
									<region>VLC</region>
									<country>ES</country>
								</street_address>
								<phone>
									<e164>+34111222333</e164>
									<extension_number>1234</extension_number>
									<other_format>555-555-555</other_format>
								</phone>
							</contact>
						</partner_contact>
					</partner_contacts_list>
				</second_partner>
				<cooperation_condition_list>
					<cooperation_condition>
						<start_date>2000-01-01</start_date>
						<end_date>2001-01-01</end_date>
						<eqf_level_list>
							<eqf_level>1</eqf_level>
							<eqf_level>2</eqf_level>
						</eqf_level_list>
						<mobility_number>1</mobility_number>
						<mobility_type>
							<mobility_category>Teaching</mobility_category>
							<mobility_group>Staff</mobility_group>
						</mobility_type>
						<receiving_partner>
							<institution>
								<institution_id>'+@v_inst_id + N'</institution_id>
								<organization_unit_code>'+@v_ounit_code + N'</organization_unit_code>
							</institution>
							<signing_date>2001-01-01</signing_date>
							<partner_contacts_list>
								<partner_contact>
									<given_name>RP1 John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Receiving Partner Contact1</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>ES</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</receiving_partner>
						<sending_partner>
							<institution>
								<institution_id>'+@v_receiving_inst_id + N'</institution_id>
								<organization_unit_code>'+@v_r_ounit_code + N'</organization_unit_code>
							</institution>
							<signing_date>2000-01-01</signing_date>
							<partner_contacts_list>
								<partner_contact>
									<given_name>SP1 John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Sending Partner Contact1</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>ES</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</sending_partner>	
						<subject_area_language_list>
							<subject_area_language>
								<subject_area>
									<isced_code>06</isced_code>
									<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
								</subject_area>
								<language_skill>
									<language>EN</language>
									<cefr_level>B2</cefr_level>
								</language_skill>
							</subject_area_language>
						</subject_area_language_list>
						<cop_cond_duration>
							<number_duration>1</number_duration>
							<unit>1</unit>
						</cop_cond_duration>
						<other_info>Other Info</other_info>
						<blended>0</blended>
					</cooperation_condition>
					<cooperation_condition>
						<start_date>2000-01-01</start_date>
						<end_date>2001-01-01</end_date>
						<eqf_level_list>
							<eqf_level>1</eqf_level>
							<eqf_level>2</eqf_level>
						</eqf_level_list>
						<mobility_number>1</mobility_number>
						<mobility_type>
							<mobility_category>Studies</mobility_category>
							<mobility_group>Student</mobility_group>
						</mobility_type>
						<receiving_partner>
							<institution>
								<institution_id>'+@v_receiving_inst_id + N'</institution_id>
								<organization_unit_code>'+@v_r_ounit_code + N'</organization_unit_code>
							</institution>
							<signing_date>2001-01-01</signing_date>
							<partner_contacts_list>
								<partner_contact>
									<given_name>RP2 John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Receiving Partner Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>ES</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</receiving_partner>
						<sending_partner>
							<institution>
								<institution_id>'+@v_inst_id + N'</institution_id>
								<organization_unit_code>'+@v_ounit_code + N'</organization_unit_code>
							</institution>
							<signing_date>2000-01-01</signing_date>
							<partner_contacts_list>
								<partner_contact>
									<given_name>SP2 John Test2</given_name>
									<family_name>Doe Test2</family_name>
									<birth_date>2000-01-01</birth_date>
									<citizenship>ES</citizenship>
									<gender>1</gender>
									<contact>
										<contact_name>
											<text lang="en">Contact2 name</text>
											<text lang="es">Nombre contacto2</text>
										</contact_name>
										<contact_description>
											<text lang="en">Contact2 description</text>
											<text lang="es">Descripción contacto2</text>
										</contact_description>
										<contact_url>
											<text lang="en">https://www.ewp.org/contacts</text>
											<text lang="es">https://www.ewp.org/contactos</text>
										</contact_url>
										<contact_role>Receiving Partner Contact2 Role</contact_role>
										<email_list>
											<email>email1@domain.com</email>
											<email>email2@domain.com</email>
										</email_list>
										<street_address>
											<recipient_name_list>
												<recipient_name>Contact2 Street Address RecName</recipient_name>
												<recipient_name>Street Address RecName Contact2</recipient_name>
											</recipient_name_list>
											<address_line_list>								
												<address_line>Contact2 Address line1</address_line>								
												<address_line>Address line2</address_line>								
											</address_line_list>
											<building_number>1</building_number>
											<building_name>Contact2 Building name</building_name>
											<street_name>Calle test</street_name>
											<unit>1</unit>
											<building_floor>Floor</building_floor>
											<post_office_box>PostOffice</post_office_box>
											<delivery_point_code_list>
												<delivery_point_code>DeliveryPointCode1</delivery_point_code>
												<delivery_point_code>DeliveryPointCode2</delivery_point_code>
											</delivery_point_code_list>
											<postal_code>46001</postal_code>
											<locality>Valencia</locality>
											<region>VLC</region>
											<country>ES</country>
										</street_address>
										<phone>
											<e164>+34111222333</e164>
											<extension_number>1234</extension_number>
											<other_format>555-555-555</other_format>
										</phone>
									</contact>
								</partner_contact>
							</partner_contacts_list>
						</sending_partner>	
						<subject_area_language_list>
							<subject_area_language>
								<language_skill>
									<language>EN</language>
									<cefr_level>B2</cefr_level>
								</language_skill>
							</subject_area_language>
						</subject_area_language_list>
						<subject_area_list>
							<subject_area>
								<isced_code>06</isced_code>
									<isced_clarification>Information and Communication Technologies (ICTs)</isced_clarification>
							</subject_area>
						</subject_area_list>
						<cop_cond_duration>
							<number_duration>1</number_duration>
							<unit>1</unit>
						</cop_cond_duration>
						<other_info>Other Info</other_info>
						<blended>0</blended>
					</cooperation_condition>
				</cooperation_condition_list>
				<pdf>Prueba</pdf>
				<file_mime_type>text/plain</file_mime_type>
			</iia>'


DECLARE @errMsg varchar(255)
DECLARE @errNum integer
DECLARE @IIA_ID uniqueidentifier

--EXECUTE dbo.VALIDA_IIA @xml, @errMsg output, @errNum output

--EXECUTE dbo.INSERT_IIA @xml, @IIA_ID output, @errMsg output, @v_receiving_inst_id, @errNum output

EXECUTE dbo.UPDATE_IIA 'EBAD39EE-E54F-41F5-A31E-2ADFEBF71534', @xml, @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.DELETE_IIA '95FC8343-7BA9-4595-B621-404DE64A1064', @errMsg output, @v_receiving_inst_id, @errNum output

--EXECUTE dbo.APPROVE_IIA 'b7e1adc6-cc7b-4a98-8487-10f2fad5b80a', @v_receiving_inst_id, @errMsg output, @errNum output

--EXECUTE dbo.PREPARE_SNAPSHOTS @errMsg output, @errNum output

--EXECUTE dbo.RECOVER_IIA_SNAPSHOT '36de7634-2dcb-4b78-ae31-3acf73717d3d', @errMsg output, @errNum output

--EXECUTE dbo.TERMINATE_IIA '36de7634-2dcb-4b78-ae31-3acf73717d3d', @errMsg output, @errNum output

PRINT @errMsg
PRINT @errNum
PRINT ' IIA ID: ' 
PRINT @IIA_ID
