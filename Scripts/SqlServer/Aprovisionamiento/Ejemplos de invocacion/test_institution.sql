DECLARE  @v_receiving_inst_id  NVARCHAR(50)
DECLARE  @v_inst_id  NVARCHAR(9)
SET @v_inst_id = 'unican.es';
--DECLARE  @v_inst_id  NVARCHAR(6)
--SET @v_inst_id = 'ucv.es';
SET @v_receiving_inst_id = 'validator-hei01.developers.erasmuswithoutpaper.eu';

DECLARE @xml xml 
set @xml= '<institution>
	<institution_id>'+@v_inst_id + N'</institution_id>
	<institution_name>
		<text lang="en">English university name</text>
		<text lang="es">Nombre de universidad en castellano</text>
	</institution_name>
	<abbreviation>Abreviation</abbreviation>
	<university_contact_details>
		<street_address>
			<recipient_name_list>
				<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
				<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
			</recipient_name_list>
			<address_line_list>								
				<address_line>Calle test</address_line>								
				<address_line>numero, piso</address_line>								
			</address_line_list>
			<building_number>1</building_number>
			<building_name>Nombre de edificio</building_name>
			<street_name>Nombre de calle</street_name>
			<unit>1</unit>
			<building_floor>Floor</building_floor>
			<post_office_box>PostOffice</post_office_box>
			<delivery_point_code_list>
				<delivery_point_code>DeliveryPointCode1</delivery_point_code>
				<delivery_point_code>DeliveryPointCode2</delivery_point_code>
			</delivery_point_code_list>
			<postal_code>00001</postal_code>
			<locality>Valencia</locality>
			<region>VLC</region>
			<country>ES</country>
		</street_address>
		<mailing_address>
			<recipient_name_list>
				<recipient_name>Direccion1 de contacto de la universidad</recipient_name>
				<recipient_name>Direccion2 de contacto de la universidad</recipient_name>
			</recipient_name_list>
			<address_line_list>								
				<address_line>Calle test</address_line>								
				<address_line>numero, piso</address_line>								
			</address_line_list>
			<building_number>1</building_number>
			<building_name>Nombre de edificio</building_name>
			<street_name>Nombre de calle</street_name>
			<unit>1</unit>
			<building_floor>Floor</building_floor>
			<post_office_box>PostOffice</post_office_box>
			<delivery_point_code_list>
				<delivery_point_code>DeliveryPointCode1</delivery_point_code>
				<delivery_point_code>DeliveryPointCode2</delivery_point_code>
			</delivery_point_code_list>
			<postal_code>46001</postal_code>
			<locality>Valencia</locality>
			<region>VLC</region>
			<country>ES</country>
		</mailing_address>
		<contact_url>
			<text lang="en">https://www.abc.com</text>
			<text lang="es">https://www.abc.es</text>
		</contact_url>
	</university_contact_details>
	<contact_details_list>
		<contact_person>
			<given_name>John Test</given_name>
			<family_name>Doe Test</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact1 name</text>
					<text lang="es">Nombre contacto1</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact1 description</text>
					<text lang="es">Descripción contacto1</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact1 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact1 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact1</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact1 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact1 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
		</contact_person>
		<contact_person>
			<given_name>John Test2</given_name>
			<family_name>Doe Test2</family_name>
			<birth_date>2000-01-01</birth_date>
			<citizenship>ES</citizenship>
			<gender>1</gender>
			<contact>
				<contact_name>
					<text lang="en">Contact2 name</text>
					<text lang="es">Nombre contacto2</text>
				</contact_name>
				<contact_description>
					<text lang="en">Contact2 description</text>
					<text lang="es">Descripción contacto2</text>
				</contact_description>
				<contact_url>
					<text lang="en">https://www.ewp.org/contacts</text>
					<text lang="es">https://www.ewp.org/contactos</text>
				</contact_url>
				<contact_role>Contact2 Role</contact_role>
				<email_list>
					<email>email1@domain.com</email>
					<email>email2@domain.com</email>
				</email_list>
				<street_address>
					<recipient_name_list>
						<recipient_name>Contact2 Street Address RecName</recipient_name>
						<recipient_name>Street Address RecName Contact2</recipient_name>
					</recipient_name_list>
					<address_line_list>								
						<address_line>Contact2 Address line1</address_line>								
						<address_line>Address line2</address_line>								
					</address_line_list>
					<building_number>1</building_number>
					<building_name>Contact2 Building name</building_name>
					<street_name>Calle test</street_name>
					<unit>1</unit>
					<building_floor>Floor</building_floor>
					<post_office_box>PostOffice</post_office_box>
					<delivery_point_code_list>
						<delivery_point_code>DeliveryPointCode1</delivery_point_code>
						<delivery_point_code>DeliveryPointCode2</delivery_point_code>
					</delivery_point_code_list>
					<postal_code>46001</postal_code>
					<locality>Valencia</locality>
					<region>VLC</region>
					<country>ES</country>
				</street_address>
				<phone>
					<e164>+34111222333</e164>
					<extension_number>1234</extension_number>
					<other_format>555-555-555</other_format>
				</phone>
			</contact>
		</contact_person>
	</contact_details_list>
	<factsheet_url_list>
		<text lang="en">https://www.abc.com/en/factsheet</text>
		<text lang="es">https://www.abc.es/es/factsheet</text>
	</factsheet_url_list>
	<logo_url>https://www.abc.es/logo.png</logo_url>                  
</institution>'



DECLARE @errMsg varchar(255)
DECLARE @errNum integer
DECLARE @INSTITUTION_ID uniqueidentifier


--EXECUTE dbo.VALIDA_INSTITUTION @xml, @errMsg output, @errNum output

EXECUTE dbo.INSERT_INSTITUTION @xml, @INSTITUTION_ID output, @errMsg output, @errNum output

--EXECUTE dbo.UPDATE_INSTITUTION 'LU.ES', @xml, @errMsg output, @errNum output

--EXECUTE dbo.DELETE_INSTITUTION 'LU.ES', @errMsg output, @errNum output


PRINT @errMsg
PRINT @errNum
PRINT ' INSTITUTION_ID: ' 
PRINT @INSTITUTION_ID


