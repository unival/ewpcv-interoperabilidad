DECLARE @xml xml ='<group_list>
	<loi_group>
		<title>
			<text lang="en">Title1</text>
		</title>
		<sorting_key>1</sorting_key>
	</loi_group>
	<loi_group>
		<title>
			<text lang="es">Titulo2</text>
		</title>
		<sorting_key>2</sorting_key>
	</loi_group>
</group_list>'

DECLARE @errMsg varchar(255)
DECLARE @errNum integer


EXECUTE dbo.VALIDA_GROUP_LIST @xml, @errMsg output, @errNum output

--EXECUTE dbo.INSERT_GROUP_LIST @xml, @errMsg output, @errNum output

--EXECUTE dbo.UPDATE_GROUP_LIST 

--EXECUTE dbo.DELETE_GROUP_LIST 

PRINT @errMsg
PRINT @errNum
