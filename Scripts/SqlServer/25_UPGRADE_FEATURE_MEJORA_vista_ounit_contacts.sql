
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'V' and category = 0 AND NAME = 'OUNIT_CONTACTS_VIEW'
) DROP VIEW dbo.OUNIT_CONTACTS_VIEW;
GO 
CREATE VIEW dbo.OUNIT_CONTACTS_VIEW AS 
SELECT DISTINCT
    condet.id                                            AS CONTACT_ID,
    ounit.ID                                             AS OUNIT_ID, 
	ins.INSTITUTION_ID                     				 AS INSTITUTION_ID,
    ounit.ORGANIZATION_UNIT_CODE                         AS OUNIT_CODE,
    dbo.EXTRAE_NOMBRES_OUNIT(ounit.ID)                   AS OUNIT_NAME,
    ounit.ABBREVIATION                                   AS OUNIT_ABBREVIATION,
    ounit.LOGO_URL                                       AS OUNIT_LOGO_URL,
    dbo.EXTRAE_URLS_CONTACTO(condet.ID) 				 AS OUNIT_WEBSITE,
    cont.CONTACT_ROLE                                 	 AS CONTACT_ROLE,
    dbo.EXTRAE_NOMBRES_CONTACTO(cont.ID)				 AS CONTACT_NAME,
    person.FIRST_NAMES                                   AS CONTACT_FIRST_NAMES,
    person.LAST_NAME                                     AS CONTACT_LAST_NAME,
    person.GENDER                                        AS CONTACT_GENDER,
    dbo.EXTRAE_TELEFONO(condet.ID)                       AS CONTACT_PHONE_NUMBER,
    dbo.EXTRAE_FAX(condet.ID)                            AS CONTACT_FAX_NUMBER,
    dbo.EXTRAE_EMAILS(condet.ID) 		                 AS CONTACT_MAIL,
    flexadd_s.COUNTRY                                    AS CONTACT_COUNTRY,
    flexadd_s.LOCALITY                                   AS CONTACT_CITY,
	dbo.EXTRAE_ADDRESS_LINES(condet.STREET_ADDRESS)      AS CONT_STREET_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.STREET_ADDRESS)            AS CONTACT_STREET_ADDRESS,
	dbo.EXTRAE_ADDRESS_LINES(condet.MAILING_ADDRESS)     AS CONT_MAILING_ADDR_LINES,
	dbo.EXTRAE_ADDRESS(condet.MAILING_ADDRESS)           AS CONTACT_MAILING__ADDRESS,
	ounit.IS_EXPOSED									 AS IS_EXPOSED
    
        
       FROM dbo.EWPCV_ORGANIZATION_UNIT ounit
	    /*
			Datos institucion
		*/
		INNER JOIN dbo.EWPCV_INST_ORG_UNIT iou ON iou.organization_units_id = ounit.id
		INNER JOIN dbo.EWPCV_INSTITUTION ins ON iou.institution_id = ins.id
        
        /*
            Datos de contactos
        */
        LEFT JOIN dbo.EWPCV_CONTACT cont ON cont.organization_unit_id = ounit.id
        INNER JOIN dbo.EWPCV_CONTACT_DETAILS condet ON cont.contact_details_id = condet.ID AND (ounit.primary_contact_detail_id IS NULL OR condet.ID <> ounit.primary_contact_detail_id ) 
        LEFT JOIN dbo.EWPCV_FLEXIBLE_ADDRESS flexadd_s ON flexadd_s.id = condet.street_address
        LEFT JOIN dbo.EWPCV_PERSON person ON person.id = cont.person_id

         /*
            Datos de la institution
        */
        LEFT JOIN dbo.EWPCV_ORGANIZATION_UNIT_NAME ounitname ON ounitname.organization_unit_id = ounit.id
        LEFT JOIN dbo.EWPCV_LANGUAGE_ITEM langitemi ON langitemi.id = ounitname.name_id
;
GO

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.08.04', GETDATE(), '25_UPGRADE_FEATURE_MEJORA_vista_ounit_contacts');


