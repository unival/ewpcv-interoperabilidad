
USE [EWP]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_CALENDARS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [CALENDAR_NAME] [VARCHAR] (200)  NOT NULL ,
  [CALENDAR] [IMAGE] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_CRON_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [CRON_EXPRESSION] [VARCHAR] (120)  NOT NULL ,
  [TIME_ZONE_ID] [VARCHAR] (80) 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_FIRED_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [ENTRY_ID] [VARCHAR] (95)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [INSTANCE_NAME] [VARCHAR] (200)  NOT NULL ,
  [FIRED_TIME] [BIGINT] NOT NULL ,
  [SCHED_TIME] [BIGINT] NOT NULL ,
  [PRIORITY] [INTEGER] NOT NULL ,
  [STATE] [VARCHAR] (16)  NOT NULL,
  [JOB_NAME] [VARCHAR] (200)  NULL ,
  [JOB_GROUP] [VARCHAR] (200)  NULL ,
  [IS_NONCONCURRENT] [VARCHAR] (1)  NULL ,
  [REQUESTS_RECOVERY] [VARCHAR] (1)  NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_PAUSED_TRIGGER_GRPS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_SCHEDULER_STATE] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [INSTANCE_NAME] [VARCHAR] (200)  NOT NULL ,
  [LAST_CHECKIN_TIME] [BIGINT] NOT NULL ,
  [CHECKIN_INTERVAL] [BIGINT] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_LOCKS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [LOCK_NAME] [VARCHAR] (40)  NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_JOB_DETAILS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [JOB_NAME] [VARCHAR] (200)  NOT NULL ,
  [JOB_GROUP] [VARCHAR] (200)  NOT NULL ,
  [DESCRIPTION] [VARCHAR] (250) NULL ,
  [JOB_CLASS_NAME] [VARCHAR] (250)  NOT NULL ,
  [IS_DURABLE] [VARCHAR] (1)  NOT NULL ,
  [IS_NONCONCURRENT] [VARCHAR] (1)  NOT NULL ,
  [IS_UPDATE_DATA] [VARCHAR] (1)  NOT NULL ,
  [REQUESTS_RECOVERY] [VARCHAR] (1)  NOT NULL ,
  [JOB_DATA] [IMAGE] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_SIMPLE_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [REPEAT_COUNT] [BIGINT] NOT NULL ,
  [REPEAT_INTERVAL] [BIGINT] NOT NULL ,
  [TIMES_TRIGGERED] [BIGINT] NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_SIMPROP_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [STR_PROP_1] [VARCHAR] (512) NULL,
  [STR_PROP_2] [VARCHAR] (512) NULL,
  [STR_PROP_3] [VARCHAR] (512) NULL,
  [INT_PROP_1] [INT] NULL,
  [INT_PROP_2] [INT] NULL,
  [LONG_PROP_1] [BIGINT] NULL,
  [LONG_PROP_2] [BIGINT] NULL,
  [DEC_PROP_1] [NUMERIC] (13,4) NULL,
  [DEC_PROP_2] [NUMERIC] (13,4) NULL,
  [BOOL_PROP_1] [VARCHAR] (1) NULL,
  [BOOL_PROP_2] [VARCHAR] (1) NULL,
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_BLOB_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [BLOB_DATA] [IMAGE] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EWPCV_QRTZ_TRIGGERS] (
  [SCHED_NAME] [VARCHAR] (120)  NOT NULL ,
  [TRIGGER_NAME] [VARCHAR] (200)  NOT NULL ,
  [TRIGGER_GROUP] [VARCHAR] (200)  NOT NULL ,
  [JOB_NAME] [VARCHAR] (200)  NOT NULL ,
  [JOB_GROUP] [VARCHAR] (200)  NOT NULL ,
  [DESCRIPTION] [VARCHAR] (250) NULL ,
  [NEXT_FIRE_TIME] [BIGINT] NULL ,
  [PREV_FIRE_TIME] [BIGINT] NULL ,
  [PRIORITY] [INTEGER] NULL ,
  [TRIGGER_STATE] [VARCHAR] (16)  NOT NULL ,
  [TRIGGER_TYPE] [VARCHAR] (8)  NOT NULL ,
  [START_TIME] [BIGINT] NOT NULL ,
  [END_TIME] [BIGINT] NULL ,
  [CALENDAR_NAME] [VARCHAR] (200)  NULL ,
  [MISFIRE_INSTR] [SMALLINT] NULL ,
  [JOB_DATA] [IMAGE] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_CALENDARS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_CALENDARS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [CALENDAR_NAME]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_CRON_TRIGGERS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_CRON_TRIGGERS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_FIRED_TRIGGERS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_FIRED_TRIGGERS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [ENTRY_ID]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_PAUSED_TRIGGER_GRPS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_PAUSED_TRIGGER_GRPS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [TRIGGER_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_SCHEDULER_STATE] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_SCHEDULER_STATE] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [INSTANCE_NAME]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_LOCKS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_LOCKS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [LOCK_NAME]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_JOB_DETAILS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_JOB_DETAILS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [JOB_NAME],
    [JOB_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_SIMPLE_TRIGGERS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_SIMPLE_TRIGGERS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_SIMPROP_TRIGGERS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_SIMPROP_TRIGGERS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_TRIGGERS] WITH NOCHECK ADD
  CONSTRAINT [PK_QRTZ_TRIGGERS] PRIMARY KEY  CLUSTERED
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  )  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_CRON_TRIGGERS] ADD
  CONSTRAINT [FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) REFERENCES [dbo].[EWPCV_QRTZ_TRIGGERS] (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_SIMPLE_TRIGGERS] ADD
  CONSTRAINT [FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) REFERENCES [dbo].[EWPCV_QRTZ_TRIGGERS] (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_SIMPROP_TRIGGERS] ADD
  CONSTRAINT [FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) REFERENCES [dbo].[EWPCV_QRTZ_TRIGGERS] (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_BLOB_TRIGGERS] ADD
  CONSTRAINT [FK_QRTZ_BLOB_TRIGGERS_QRTZ_TRIGGERS] FOREIGN KEY
  (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) REFERENCES [dbo].[EWPCV_QRTZ_TRIGGERS] (
    [SCHED_NAME],
    [TRIGGER_NAME],
    [TRIGGER_GROUP]
  ) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EWPCV_QRTZ_TRIGGERS] ADD
  CONSTRAINT [FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS] FOREIGN KEY
  (
    [SCHED_NAME],
    [JOB_NAME],
    [JOB_GROUP]
  ) REFERENCES [dbo].[EWPCV_QRTZ_JOB_DETAILS] (
    [SCHED_NAME],
    [JOB_NAME],
    [JOB_GROUP]
  )
GO

	/*
		Persiste una institucion en el sistema si no existe ya en el sistema y retorna el UUID generado para el registro.
*/

ALTER PROCEDURE  dbo.INSERTA_OUNIT(@P_INSTITUTION xml, @return_value uniqueidentifier OUTPUT) as
BEGIN
	DECLARE @v_id VARCHAR(255)
	DECLARE @v_ou_id VARCHAR(255)
	DECLARE @v_count VARCHAR(255)
	DECLARE @INSTITUTION_ID VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_CODE VARCHAR(255)
	DECLARE @ORGANIZATION_UNIT_NAME xml

	SELECT 
		@INSTITUTION_ID = T.c.value('(institution_id)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]','varchar(255)'),
		@ORGANIZATION_UNIT_NAME = T.c.query('(organization_unit_name)')
	FROM @P_INSTITUTION.nodes('*') T(c)
	
	IF @ORGANIZATION_UNIT_CODE IS NOT NULL 
	BEGIN 
		SELECT @v_id = O.ID
				FROM EWPCV_ORGANIZATION_UNIT O
				INNER JOIN EWPCV_INST_ORG_UNIT IO ON O.ID = IO.ORGANIZATION_UNITS_ID
				INNER JOIN EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
				WHERE UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)
				AND UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)

		IF @v_id is null 
		BEGIN
			SET @v_id = NEWID()
			INSERT INTO dbo.EWPCV_ORGANIZATION_UNIT (ID, ORGANIZATION_UNIT_CODE) VALUES (@v_id, @ORGANIZATION_UNIT_CODE)
		END

		EXECUTE dbo.INSERTA_OUNIT_NAMES @v_id, @ORGANIZATION_UNIT_NAME
	END
	SET @return_value = @v_id

END
;
GO

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_IIA' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>
	
	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" />
				<xs:element name="end_date" type="xs:date" />
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded" >
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="0" type="xs:integer" />
													<xs:element name="unit" minOccurs="0">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO

IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_MOBILITY' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_MOBILITY
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level" type="notEmptyStringType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>


	<xs:element name="mobility">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="sending_institution" type="institutionType" />
				<xs:element name="receiving_institution" type="institutionType" />
				<xs:element name="actual_arrival_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="actual_depature_date" type="dateOrEmptyType" minOccurs="0" />
				<xs:element name="planed_arrival_date" type="xs:date" />
				<xs:element name="planed_depature_date" type="xs:date" />
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="cooperation_condition_id" type="notEmptyStringType" />
				<xs:element name="student">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="global_id" type="notEmptyStringType" />
							<xs:element name="contact_person" type="contactPersonType" minOccurs="0" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="eqf_level" type="xs:integer" />
				<xs:element name="subject_area" type="subjectAreaType" />
				<xs:element name="student_language_skill_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="student_language_skill" type="languageSkillsType" maxOccurs="unbounded" />
						</xs:sequence>			
					</xs:complexType>
				</xs:element>
				<xs:element name="sender_contact" type="contactMobilityPersonType" />
				<xs:element name="sender_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="receiver_contact" type="contactMobilityPersonType" />
				<xs:element name="receiver_admv_contact" type="contactMobilityPersonType" minOccurs="0" />
				<xs:element name="mobility_type" type="xs:string" minOccurs="0" />
				<xs:element name="status">
					<xs:simpleType>
						<xs:restriction base="xs:integer">
							<xs:enumeration value="0"/>
							<xs:enumeration value="1"/>
							<xs:enumeration value="2"/>
							<xs:enumeration value="3"/>
							<xs:enumeration value="4"/>
						</xs:restriction>
					</xs:simpleType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'

GO


IF EXISTS (SELECT * FROM sys.xml_schema_collections 
                    WHERE name = 'EWPCV_XML_LEARNING_AGREEMENT' 
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT;
 
CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_LEARNING_AGREEMENT
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>  

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string"> 
						<xs:pattern value="[^@]+@[^\.]+\..+"/> 
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="urls" type="languageItemListType" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="creditType">
		<xs:sequence>
			<xs:element name="scheme" type="notEmptyStringType" />
			<xs:element name="credit_value">
				<xs:simpleType>
					<xs:restriction base="xs:decimal">
						<xs:totalDigits value="5" />
						<xs:fractionDigits value="1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element type="notEmptyStringType" name="country"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" type="xs:string" minOccurs="0" />
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactMobilityPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="notEmptyStringType" />
			<xs:element name="family_name" type="notEmptyStringType" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0"/>
			<xs:element name="citizenship" type="xs:string" minOccurs="0"/>
			<xs:element name="gender" type="genderType" minOccurs="0"/>
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email" type="xs:string"/>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="signatureType">
		<xs:sequence>
			<xs:element name="signer_name" type="notEmptyStringType" />
			<xs:element name="signer_position" type="notEmptyStringType" />
			<xs:element name="signer_email" type="notEmptyStringType" />
			<xs:element name="sign_date" type="xs:dateTime" />
			<xs:element name="signer_app" type="xs:string" minOccurs="0" />
			<xs:element name="signature" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language_skill" maxOccurs="unbounded">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="language" type="notEmptyStringType" />
						<xs:element name="cefr_level" type="notEmptyStringType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="academicYearType">
		<xs:restriction base="xs:string">
			<xs:pattern value="\d{4}[-/]\d{4}"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:element name="learning_agreement">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="mobility_id" type="notEmptyStringType" />
				<xs:element name="student_la" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="given_name" type="xs:string" />
							<xs:element name="family_name" type="xs:string" />
							<xs:element name="birth_date" type="xs:string" />
							<xs:element name="citizenship" type="xs:string" />
							<xs:element name="gender" type="genderType" />
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="la_component_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="la_component" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="la_component_type">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:element>
										<xs:element name="los_code" type="xs:string" minOccurs="0" />
										<xs:element name="title" type="notEmptyStringType" />
										<xs:element name="academic_term">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="academic_year" type="academicYearType" />
													<xs:element name="institution_id" type="notEmptyStringType" />
													<xs:element name="organization_unit_code" type="xs:string" minOccurs="0" />
													<xs:element name="start_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="end_date" type="dateOrEmptyType" minOccurs="0" />
													<xs:element name="description" type="languageItemListType" minOccurs="0" />
													<xs:element name="term_number" type="xs:integer" />
													<xs:element name="total_terms" type="xs:integer" />
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="credit" type="creditType" />
										<xs:element name="recognition_conditions" type="xs:string" minOccurs="0" />
										<xs:element name="short_description" type="xs:string" minOccurs="0" />
										<xs:element name="status" type="xs:string" minOccurs="0" />
										<xs:element name="reason_code" minOccurs="0">
											<xs:simpleType>
												<xs:restriction base="xs:integer">
													<xs:enumeration value="0"/>
													<xs:enumeration value="1"/>
													<xs:enumeration value="2"/>
													<xs:enumeration value="3"/>
													<xs:enumeration value="4"/>
													<xs:enumeration value="5"/>
												</xs:restriction>
											</xs:simpleType>													
										</xs:element>
										<xs:element name="reason_text" type="xs:string" minOccurs="0" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="student_signature" type="signatureType" />
				<xs:element name="sending_hei_signature" type="signatureType" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>


</xs:schema>'
;

GO
/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/

ALTER PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_LA(@P_LA_ID VARCHAR(255), @P_LA_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY M
			INNER JOIN dbo.EWPCV_MOBILITY_LA MLA 
			ON MLA.MOBILITY_ID = M.ID AND MLA.MOBILITY_REVISION = M.MOBILITY_REVISION
		WHERE MLA.LEARNING_AGREEMENT_ID = @P_LA_ID AND MLA.LEARNING_AGREEMENT_REVISION = @P_LA_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = UPPER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/*
	Obtiene el codigo schac de la institucion que genera la notificacion de un LA
*/

ALTER PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_MOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = UPPER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.02.00', GETDATE(), '07_UPGRADE_v01.02.00');