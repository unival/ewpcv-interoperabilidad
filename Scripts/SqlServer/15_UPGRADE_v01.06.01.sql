
/*
    **********************************
    INICIO MODIFICACIONES SOBRE TABLAS
    **********************************
*/  
	ALTER TABLE dbo.EWPCV_IIA ADD IS_EXPOSED INTEGER DEFAULT 1;
 
/*
    *********************************
     FIN MODIFICACIONES SOBRE TABLAS
    *********************************
*/
/*
    *************************************
    INICIO MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'NOTIFICA_ELEMENTO'
) DROP PROCEDURE dbo.NOTIFICA_ELEMENTO;
GO
CREATE PROCEDURE dbo.NOTIFICA_ELEMENTO(@P_ELEMENT_ID varchar(255), @P_TYPE integer, @P_NOTIFY_HEI varchar(255), @P_NOTIFIER_HEI varchar(255), @P_ERROR_MESSAGE VARCHAR(500) OUTPUT, @RETURN_VALUE INTEGER OUTPUT) AS
BEGIN

	DECLARE @v_is_exposed integer;
	DECLARE @v_count integer
	SET @v_count = 0; 
	SET @RETURN_VALUE = 0;
	

	IF @P_ELEMENT_ID IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'Dede indicar el identificador del elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
	IF @P_TYPE IS NULL
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar el tipo de elemento a notificar')
        SET @RETURN_VALUE = -1;
	END
    IF @P_NOTIFY_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion a notificar')
        SET @RETURN_VALUE = -1;
	END
    IF @P_NOTIFIER_HEI IS NULL 
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'Dede indicar la institucion que envia la notificacion')
        SET @RETURN_VALUE = -1;
	END

	IF @P_TYPE = 0 AND @RETURN_VALUE = 0 -- IIA
    BEGIN    
		SELECT @v_is_exposed = IS_EXPOSED FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
		SELECT @v_count = COUNT(1)  FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID) AND IS_REMOTE = 0;
        IF @v_count = 0  
		BEGIN
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'El IIA a notificar no existe o no es local')
            SET @RETURN_VALUE = -1;
        END 
	END
    ELSE IF @P_TYPE = 1 AND @RETURN_VALUE = 0 -- OMOBILITY
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE, 'La omobility a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 2 AND @RETURN_VALUE = 0--IMOBILITY
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY WHERE LOWER(ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'La imobility a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 3 AND @RETURN_VALUE = 0-- TOR
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.ID
        INNER JOIN EWPCV_LEARNING_AGREEMENT LA ON ML.LEARNING_AGREEMENT_ID = LA.ID 
        INNER JOIN EWPCV_TOR T ON LA.TOR_ID = T.ID
        WHERE LOWER(M.ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El TOR a notificar no existe')
            SET @RETURN_VALUE = -1;
        END
	END
    ELSE IF @P_TYPE = 4 AND @RETURN_VALUE = 0--IIA APPROVAL
	BEGIN    
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'No se permite la generacion de notificaciones de tipo IIA Approval utiliza el metodo de aprobacion de IIAs en su lugar')
		SET @RETURN_VALUE = -1;
	END
    ELSE IF @P_TYPE = 5 AND @RETURN_VALUE = 0 --LA
	BEGIN    
        SELECT @v_count = COUNT(1) FROM EWPCV_MOBILITY M
        INNER JOIN EWPCV_MOBILITY_LA ML ON ML.MOBILITY_ID = M.ID
        WHERE LOWER(M.ID) = LOWER(@P_ELEMENT_ID);
        IF @v_count = 0 
		BEGIN    
            SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El Learning Agreement a notificar aprobacion no existe')
            SET @RETURN_VALUE = -1;
        END
    END
	ELSE IF @RETURN_VALUE = 0
	BEGIN 
		SET @P_ERROR_MESSAGE = CONCAT(@P_ERROR_MESSAGE,'El tipo de elemento indicado no admite CNR')
        SET @RETURN_VALUE = -1;
	END 

	IF @RETURN_VALUE = 0
	BEGIN
		EXECUTE dbo.INSERTA_NOTIFICATION @P_ELEMENT_ID, @P_TYPE, @P_NOTIFY_HEI, @P_NOTIFIER_HEI
		IF @v_is_exposed IS NOT NULL AND @v_is_exposed = 0
		BEGIN
		UPDATE EWPCV_IIA SET IS_EXPOSED = 1
			WHERE ID = @P_ELEMENT_ID
		END 
	END
END
;
GO
	
/*
    *************************************
    FIN MODIFICACIONES SOBRE FUNCIONES
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE VISTAS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE VISTAS
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE COMMON
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE COMMON
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/
/* Elimina un FACTSHEET del sistema.
	Recibe como parametro el identificador de la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_FACTSHEET'
) DROP PROCEDURE dbo.DELETE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @ID varchar(255)
		DECLARE @FACT_SHEET varchar(255)

		SET @return_value = 0

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
		IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
				SET @return_value = -1
			END
		ELSE
			BEGIN
				SELECT @v_count = COUNT(1) 
					FROM dbo.EWPCV_INSTITUTION INS 
					INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
					SET @return_value = -1
				END 
			END

		IF @return_value = 0
		BEGIN
			SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			EXECUTE dbo.BORRA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_FACTSHEET'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO
/*
    *************************************
    FIN MODIFICACIONES SOBRE FACTSHEET
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE IIAS
    *************************************
*/
IF EXISTS (SELECT * FROM sys.xml_schema_collections
                    WHERE name = 'EWPCV_XML_IIA'
                    AND schema_id = SCHEMA_ID('dbo'))
   DROP XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA;

CREATE XML SCHEMA COLLECTION dbo.EWPCV_XML_IIA
AS '<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:simpleType name="emptyType">
		<xs:restriction base="xs:string">
			<xs:enumeration value=""/>
		</xs:restriction>
	</xs:simpleType>

	<xs:simpleType name="dateOrEmptyType">
		<xs:union memberTypes="xs:date emptyType"/>
	</xs:simpleType>

	<xs:simpleType name="notEmptyStringType">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
		</xs:restriction>
	</xs:simpleType>

	    <xs:simpleType name="HTTP">
        <xs:restriction base="xs:anyURI">
            <xs:pattern value="https?://.+"/>
        </xs:restriction>
    </xs:simpleType>

	<xs:complexType name="httpListWithOptionalLang">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="HTTP">
							<xs:attribute name="lang" type="xs:string" use="optional" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageItemListType">
		<xs:sequence>
			<xs:element name="text" maxOccurs="unbounded">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="lang" type="xs:string" use="required" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="phoneNumberType">
		<xs:sequence>
			<xs:element name="e164" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="\+[0-9]{1,15}"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="extension_number" minOccurs="0" type="xs:string" />
			<xs:element name="other_format" minOccurs="0" type="xs:string" />
		</xs:sequence>
	</xs:complexType>

	<xs:simpleType name="genderType">
		<xs:restriction base="xs:integer">
			<xs:enumeration value="0"/>
			<xs:enumeration value="1"/>
			<xs:enumeration value="2"/>
			<xs:enumeration value="9"/>
		</xs:restriction>
	</xs:simpleType>

	<xs:complexType name="informationItemType">
		<xs:sequence>
			<xs:element name="email">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[^@]+@[^\.]+\..+"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="phone" type="phoneNumberType" />
			<xs:element name="url" type="httpListWithOptionalLang" />
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="flexibleAddressType">
		<xs:sequence>
			<xs:element name="recipient_name_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="recipient_name" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="address_line_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="address_line" maxOccurs="4" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="xs:string" name="building_number" minOccurs="0"/>
			<xs:element type="xs:string" name="building_name" minOccurs="0"/>
			<xs:element type="xs:string" name="street_name" minOccurs="0"/>
			<xs:element type="xs:string" name="unit" minOccurs="0"/>
			<xs:element type="xs:string" name="building_floor" minOccurs="0"/>
			<xs:element type="xs:string" name="post_office_box" minOccurs="0"/>
			<xs:element name="delivery_point_code_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="delivery_point_code" maxOccurs="unbounded" minOccurs="0" type="xs:string" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element type="notEmptyStringType" name="postal_code"/>
			<xs:element type="xs:string" name="locality" minOccurs="0"/>
			<xs:element type="xs:string" name="region" minOccurs="0"/>
			<xs:element name="country">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="contactPersonType">
		<xs:sequence>
			<xs:element name="given_name" type="xs:string" minOccurs="0" />
			<xs:element name="family_name" type="xs:string" minOccurs="0" />
			<xs:element name="birth_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="citizenship" minOccurs="0">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[A-Z][A-Z]"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="gender" type="genderType" minOccurs="0" />
			<xs:element name="contact">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="contact_name" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_description" type="languageItemListType" minOccurs="0" />
						<xs:element name="contact_url" type="httpListWithOptionalLang" minOccurs="0" />
						<xs:element name="institution_id" type="xs:string" minOccurs="0"/>
						<xs:element name="contact_role" type="xs:string" minOccurs="0"/>
						<xs:element name="organization_unit_code" type="xs:string" minOccurs="0"/>
						<xs:element name="email_list" minOccurs="0">
							<xs:complexType>
								<xs:sequence>
									<xs:element maxOccurs="unbounded" name="email">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:pattern value="[^@]+@[^.]+\..+"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:element>
								</xs:sequence>
							</xs:complexType>
						</xs:element>
						<xs:element name="street_address" type="flexibleAddressType" minOccurs="0" />
						<xs:element name="phone" type="phoneNumberType" minOccurs="0"/>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="institutionType">
		<xs:sequence>
			<xs:element name="institution_id" type="notEmptyStringType" />
			<xs:element name="institution_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_name" type="languageItemListType" minOccurs="0" />
			<xs:element name="organization_unit_code" type="notEmptyStringType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="partnerType">
		<xs:sequence>
			<xs:element name="institution" type="institutionType" />
			<xs:element name="signing_date" type="dateOrEmptyType" minOccurs="0" />
			<xs:element name="signer_person" type="contactPersonType" minOccurs="0" />
			<xs:element name="partner_contacts_list" minOccurs="0">
				<xs:complexType>
					<xs:sequence>
						<xs:element name="partner_contact" maxOccurs="unbounded" minOccurs="0" type="contactPersonType" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="languageSkillsType">
		<xs:sequence>
			<xs:element name="language" type="notEmptyStringType" />
			<xs:element name="cefr_level">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="[ABC][12]|NS"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="subjectAreaType">
		<xs:sequence>
			<xs:element name="isced_code" type="notEmptyStringType" />
			<xs:element name="isced_clarification" type="xs:string" minOccurs="0" />
		</xs:sequence>
	</xs:complexType>

	<xs:element name="iia">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="iia_code" type="notEmptyStringType" />
				<xs:element name="start_date" type="xs:date" minOccurs="0"/>
				<xs:element name="end_date" type="xs:date" minOccurs="0"/>
				<xs:element name="cooperation_condition_list">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="cooperation_condition" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="start_date" type="xs:date" />
										<xs:element name="end_date" type="xs:date" />
										<xs:element name="eqf_level_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="eqf_level" maxOccurs="unbounded">
														<xs:simpleType>
													        <xs:restriction base="xs:integer">
																<xs:minInclusive value="1"/>
																<xs:maxInclusive value="8"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="mobility_number" type="xs:integer" />
										<xs:element name="mobility_type">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="mobility_category">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Teaching"/>
																<xs:enumeration value="Studies"/>
																<xs:enumeration value="Training"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
													<xs:element name="mobility_group">
														<xs:simpleType>
															<xs:restriction base="xs:string">
																<xs:enumeration value="Student"/>
																<xs:enumeration value="Staff"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="receiving_partner" type="partnerType" />
										<xs:element name="sending_partner" type="partnerType" />
										<xs:element name="subject_area_language_list">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area_language" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
																<xs:element name="subject_area"  minOccurs="0" type="subjectAreaType" />
																<xs:element name="language_skill" minOccurs="0"  type="languageSkillsType" />
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="subject_area_list"  minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="subject_area" minOccurs="0" maxOccurs="unbounded" type="subjectAreaType"/>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="cop_cond_duration" minOccurs="1">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="number_duration" nillable="true" minOccurs="1" type="xs:decimal" />
													<xs:element name="unit" minOccurs="1">
														<xs:simpleType>
															<xs:restriction base="xs:integer">
																<xs:enumeration value="0"/>
																<xs:enumeration value="1"/>
																<xs:enumeration value="2"/>
																<xs:enumeration value="3"/>
																<xs:enumeration value="4"/>
															</xs:restriction>
														</xs:simpleType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
										<xs:element name="other_info" type="xs:string" />
										<xs:element name="blended" type="xs:boolean" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="pdf" type="xs:string" minOccurs="0" />
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>';


GO
/* Elimina un FACTSHEET del sistema.
	Recibe como parametro el identificador de la institucion
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_FACTSHEET'
) DROP PROCEDURE dbo.DELETE_FACTSHEET;
GO 
CREATE PROCEDURE dbo.DELETE_FACTSHEET(@P_INSTITUTION_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @return_value integer OUTPUT) as
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @v_count integer
		DECLARE @ID varchar(255)
		DECLARE @FACT_SHEET varchar(255)

		SET @return_value = 0

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
		IF  @v_count = 0 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No figuran datos para el identificador de institución indicado.'
				SET @return_value = -1
			END
		ELSE
			BEGIN
				SELECT @v_count = COUNT(1) 
					FROM dbo.EWPCV_INSTITUTION INS 
					INNER JOIN EWPCV_FACT_SHEET FS ON INS.FACT_SHEET = FS.ID
					WHERE UPPER(INS.INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID) AND FS.CONTACT_DETAILS_ID IS NOT NULL;
				IF @v_count = 0
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion no tiene fact sheet insertelo antes de actualizarlo.'
					SET @return_value = -1
				END 
			END

		IF @return_value = 0
		BEGIN
			SELECT @ID = ID, @FACT_SHEET = FACT_SHEET FROM dbo.EWPCV_INSTITUTION WHERE UPPER(INSTITUTION_ID) = UPPER(@P_INSTITUTION_ID);
			EXECUTE dbo.BORRA_FACTSHEET_INSTITUTION @ID, @FACT_SHEET
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_FACTSHEET'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO

/* Actualiza un IIA del sistema.
	Recibe como parametro del Acuerdo Interinstitucional a actualizar
	Admite un objeto de tipo IIA con la informacion actualizada del Acuerdo Interinstitucional a actualizar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_IIA'
) DROP PROCEDURE dbo.UPDATE_IIA;
GO 
CREATE PROCEDURE dbo.UPDATE_IIA (@P_IIA_ID varchar(255), @P_IIA xml, @P_ERROR_MESSAGE varchar(4000) OUTPUT, 
	@P_HEI_TO_NOTIFY VARCHAR(255) OUTPUT, @return_value integer OUTPUT) as

BEGIN
	EXECUTE dbo.VALIDA_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	--validacion, institution id y ounit code de los partners existen
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_IIA @P_IIA, @P_ERROR_MESSAGE output, @return_value output
	END 
	IF @return_value=0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			DECLARE @v_notifier_hei varchar(255)
			DECLARE @v_cod_retorno integer
			DECLARE @v_count integer

			IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END

			IF @return_value = 0 
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID
				IF  @v_count = 0 
				BEGIN
					SET @P_ERROR_MESSAGE = 'El IIA no existe.'
					SET @return_value = -1
				END
			END
			
			IF @return_value = 0 
			BEGIN
				SELECT @v_count = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 1
				IF  @v_count > 0 
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden actualizar IIAs remotos.'
					SET @return_value = -1
				END
			END			

			IF @return_value = 0 
			BEGIN
				EXECUTE dbo.ACTUALIZA_IIA @P_IIA_ID, @P_IIA
				EXECUTE dbo.OBTEN_NOTIFIER_HEI @P_IIA_ID, @P_HEI_TO_NOTIFY , @v_notifier_hei output
				EXECUTE dbo.INSERTA_NOTIFICATION @P_IIA_ID, 0, @P_HEI_TO_NOTIFY, @v_notifier_hei
			END 
			COMMIT TRANSACTION
		
		END TRY
		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_IIA'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/*
	Actualiza un IIA 	
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'ACTUALIZA_IIA'
) DROP PROCEDURE dbo.ACTUALIZA_IIA;
GO 
CREATE PROCEDURE  dbo.ACTUALIZA_IIA(@P_IIA_ID varchar(255), @P_IIA xml) AS

BEGIN

	DECLARE @IIA_CODE varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @COOPERATION_CONDITION_LIST xml
	DECLARE @PDF varchar(255)
	DECLARE @v_is_exposed integer;

	SELECT 
		@IIA_CODE = T.c.value('(iia_code)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@COOPERATION_CONDITION_LIST = T.c.query('cooperation_condition_list'),
		@PDF = T.c.value('(pdf)[1]', 'varchar(255)')
	FROM @P_IIA.nodes('iia') T(c)

	SELECT @v_is_exposed = IS_EXPOSED FROM EWPCV_IIA WHERE LOWER(ID) = LOWER(@P_IIA_ID);
	
	IF @v_is_exposed = 0
		BEGIN
		UPDATE EWPCV_IIA SET START_DATE = @START_DATE, END_DATE = @END_DATE, IIA_CODE = @IIA_CODE, MODIFY_DATE = SYSDATETIME(), IS_EXPOSED = 1
			WHERE ID = @P_IIA_ID
		END 
	ELSE
		BEGIN
		UPDATE EWPCV_IIA SET START_DATE = @START_DATE, END_DATE = @END_DATE, IIA_CODE = @IIA_CODE, MODIFY_DATE = SYSDATETIME()
			WHERE ID = @P_IIA_ID
		END 

	EXECUTE dbo.BORRA_COOP_CONDITIONS @P_IIA_ID
	EXECUTE dbo.INSERTA_COOP_CONDITIONS @P_IIA_ID, @COOPERATION_CONDITION_LIST

	IF (@PDF='' OR @PDF is null)
	BEGIN
		UPDATE dbo.EWPCV_IIA SET PDF = null WHERE ID = @P_IIA_ID
	END
	ELSE BEGIN
		BEGIN TRY
			DECLARE @sql varchar(255)
			SET @sql = 'UPDATE dbo.EWPCV_IIA SET PDF = (SELECT * FROM OPENROWSET(BULK '''+@PDF+''', SINGLE_BLOB) AS BLOB) WHERE ID = '''+CAST(@P_IIA_ID AS varchar(255))+''''
			EXEC(@sql)
		END TRY
		BEGIN CATCH
			PRINT 'ERROR a intentar guardar PDF en dbo.EWPCV_IIA'
		END CATCH	
	END

END
;
GO

/*  
	Vincula un IIA propio con un IIA remoto. Esto es, seteamos los valores id del iia remoto, code del iia remoto 
	y hash de las cooperation condition del iia remoto en nuestro IIA propio.
	Recibe como parámetro el identificador propio y el identificador de la copia remota.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UNBIND_IIA'
) DROP PROCEDURE dbo.UNBIND_IIA;
GO 
CREATE PROCEDURE  dbo.UNBIND_IIA(@P_IIA_ID varchar(255), @P_ERROR_MESSAGE varchar(4000) OUTPUT, @return_value integer OUTPUT) AS

BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		DECLARE @v_count_iia integer
		SET @return_value = 0

		IF @P_IIA_ID IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado el id del IIA propio';
				SET @return_value = -1
			END
		
		IF @return_value = 0
		BEGIN
			SELECT @v_count_iia = COUNT(1) FROM dbo.EWPCV_IIA WHERE ID = @P_IIA_ID AND IS_REMOTE = 0;
			IF @v_count_iia = 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No existe ningún IIA LOCAL asociado al id que se ha informado';
				SET @return_value = -1
			END 
		END 

		IF @return_value = 0 
		BEGIN
				UPDATE EWPCV_IIA SET 
					REMOTE_IIA_ID = null,
					REMOTE_IIA_CODE = null
					WHERE ID = @P_IIA_ID
		END 
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UNBIND_IIA'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO
/*
    *************************************
    FIN MODIFICACIONES SOBRE IIAS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/


/*
    *************************************
    FIN MODIFICACIONES SOBRE INSTITUTION
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE LOS
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE LOS
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_MOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_MOB;
/*
	Obtiene el codigo schac de la institucion que genera la notificacion de una mobility outgoing
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_OMOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_OMOB;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_OMOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_r_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_s_hei)
		ELSE 
			SET @return_value = null

END
;
GO
/*
	Obtiene el codigo schac de la institucion que genera la notificacion de una mobility incoming
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'OBTEN_NOTIFIER_HEI_IMOB'
) DROP PROCEDURE dbo.OBTEN_NOTIFIER_HEI_IMOB;
GO 
CREATE PROCEDURE  dbo.OBTEN_NOTIFIER_HEI_IMOB(@P_MOB_ID VARCHAR(255), @P_MOBLITY_REVISION integer, @P_HEI_TO_NOTIFY VARCHAR(255), @return_value VARCHAR(255) output) AS
BEGIN
	DECLARE @v_s_hei VARCHAR(255)
	DECLARE @v_r_hei VARCHAR(255)

	SELECT @v_r_hei = RECEIVING_INSTITUTION_ID, @v_s_hei = SENDING_INSTITUTION_ID
		FROM dbo.EWPCV_MOBILITY 
		WHERE ID = @P_MOB_ID
			AND MOBILITY_REVISION = @P_MOBLITY_REVISION

		IF UPPER(@v_s_hei) = UPPER(@P_HEI_TO_NOTIFY) 
			SET @return_value = LOWER(@v_r_hei)
		ELSE 
			SET @return_value = null

END
;
GO

/* Actualiza una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a actualizar
	Admite un objeto de tipo MOBILITY con la informacion actualizada de la movilidad.
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.

*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_MOBILITY'
) DROP PROCEDURE dbo.UPDATE_MOBILITY;
GO 
CREATE PROCEDURE dbo.UPDATE_MOBILITY(@P_OMOBILITY_ID varchar(255) OUTPUT, @P_MOBILITY xml, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_sending_hei varchar(255)
	DECLARE @v_id varchar(255)
	DECLARE @v_es_outgoing integer
	DECLARE @STATUS_OUTGOING integer
	DECLARE @v_count integer
	DECLARE @v_la_count integer

	EXECUTE dbo.VALIDA_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output

	IF @return_value = 0
	BEGIN	
		IF @P_HEI_TO_NOTIFY IS NULL 
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
				SET @return_value = -1
			END
	END

	IF @return_value = 0
	BEGIN	

		SELECT @v_id=ID, @v_sending_hei=SENDING_INSTITUTION_ID
			FROM dbo.EWPCV_MOBILITY 
			WHERE ID = @P_OMOBILITY_ID
			ORDER BY MOBILITY_REVISION DESC

		IF @v_id is null
		BEGIN
			SET @P_ERROR_MESSAGE = 'No existe la mobilidad indicada'
			SET @return_value = -1			
		END
		ELSE IF UPPER(@v_sending_hei) = UPPER(@P_HEI_TO_NOTIFY) 		
		BEGIN
			SET @P_ERROR_MESSAGE =  'No se pueden actualizar movilidades para las cuales no se es el propietario. Unicamente se pueden actualizar movilidades de tipo outgoing.';
			SET @return_value = -1
		END
		ELSE 
		BEGIN
			SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
				INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
				WHERE MLA.MOBILITY_ID = @P_OMOBILITY_ID
					AND LA.TOR_ID IS NOT NULL;
			IF @v_count > 0
			BEGIN
				SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
				SET @return_value = -1
			END
		END 
	END 

	IF @return_value = 0
	BEGIN
		SET @v_es_outgoing = dbo.ES_OMOBILITY(@P_MOBILITY, @P_HEI_TO_NOTIFY)

		SELECT
			@STATUS_OUTGOING = T.c.value('(status_outgoing)[1]','integer')
		FROM @P_MOBILITY.nodes('/mobility') T(c) 

		EXECUTE dbo.VALIDA_STATUS @STATUS_OUTGOING, @v_es_outgoing, @P_ERROR_MESSAGE output, @return_value output
	END 
	
	IF @return_value = 0
	BEGIN
		EXECUTE dbo.VALIDA_DATOS_MOBILITY @P_MOBILITY, @P_ERROR_MESSAGE output, @return_value output
	END 

	IF @return_value = 0
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			EXECUTE dbo.ACTUALIZA_MOBILITY @P_OMOBILITY_ID, @P_MOBILITY
			
			SELECT @v_la_count = COUNT(1) FROM dbo.EWPCV_MOBILITY_LA WHERE LOWER(MOBILITY_ID) = LOWER(@P_OMOBILITY_ID);
			IF @v_la_count > 0 
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 5, @P_HEI_TO_NOTIFY, @v_sending_hei
				END
			ELSE 
				BEGIN
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_sending_hei
				END;
			
			COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			THROW
			SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_MOBILITY'
			SET @return_value = -1
			ROLLBACK TRANSACTION
		END CATCH
	END
END
;
GO

/* Elimina una movilidad del sistema.
	Recibe como parametro el identificador de la movilidad a eliminar
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'DELETE_MOBILITY'
) DROP PROCEDURE dbo.DELETE_MOBILITY;
GO 
CREATE PROCEDURE dbo.DELETE_MOBILITY(@P_OMOBILITY_ID varchar(255) OUTPUT, @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255), @return_value integer OUTPUT) AS
BEGIN
	DECLARE @v_count integer
	DECLARE @v_m_revision integer
	DECLARE @v_notifier_hei varchar(255)

	BEGIN TRY
		BEGIN TRANSACTION
		SET @return_value = 0
		IF @P_HEI_TO_NOTIFY IS NULL 
		BEGIN
			SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
			SET @return_value = -1
		END

		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count >0
			BEGIN
				SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
				EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
				IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se pueden borrar movilidades para las cuales no se es el propietario. Unicamente se pueden borrar movilidades de tipo outgoing.'
					SET @return_value = -1
				END
				ELSE 
				BEGIN
					SELECT @v_count = COUNT(1) FROM dbo.EWPCV_LEARNING_AGREEMENT LA 
					INNER JOIN  dbo.EWPCV_MOBILITY_LA MLA ON LA.ID = MLA.LEARNING_AGREEMENT_ID AND LA.LEARNING_AGREEMENT_REVISION = MLA.LEARNING_AGREEMENT_REVISION
					WHERE MLA.MOBILITY_ID = @P_OMOBILITY_ID
						AND LA.TOR_ID IS NOT NULL;
					IF @v_count > 0
					BEGIN
						SET @P_ERROR_MESSAGE = 'No se puede actualizar una movilidad que ya tengan un TOR'
						SET @return_value = -1
					END
				END
				IF @return_value = 0
				BEGIN
					EXECUTE dbo.BORRA_MOBILITY @P_OMOBILITY_ID
					EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
				END
			END
		ELSE
			BEGIN
				SET @P_ERROR_MESSAGE = 'La movilidad no existe'
				SET @return_value = -1
			END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		THROW
		SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.DELETE_MOBILITY'
		SET @return_value = -1
		ROLLBACK TRANSACTION
	END CATCH
END
;
GO
/* Cancela una movilidad
    Recibe como parametro el identificador de la movilidad
    Admite un parametro de salida con una descripcion del error en caso de error.
    Se debe indicar el codigo SCHAC de la institucion a notificar
    Retorna un codigo de ejecucion 0 ok <0 si error.
  */
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'CANCEL_MOBILITY'
) DROP PROCEDURE dbo.CANCEL_MOBILITY;
GO 
CREATE PROCEDURE dbo.CANCEL_MOBILITY(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) output, @P_HEI_TO_NOTIFY varchar(255), @return_value integer output)
  AS
   
  BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_resultado integer
	DECLARE @v_count integer
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = count(1) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		IF @v_count > 0
		BEGIN
		  SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
		  EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output
		  IF @v_notifier_hei IS NULL 
		  BEGIN
			SET @P_ERROR_MESSAGE = 'No se pueden cancelar movilidades para las cuales no se es el propietario. Unicamente se pueden cancelar movilidades de tipo outgoing.'
			SET @return_value = -1
		  END
	  
		  UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 0, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
    
		  EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
		END
		ELSE
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
  END
;
GO




/* Cambia el estado de la movilidad al estado LIVE, el estudiante sale de la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_DEPARTURE'
) DROP PROCEDURE dbo.STUDENT_DEPARTURE;
GO 
CREATE PROCEDURE dbo.STUDENT_DEPARTURE(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado LIVE de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado LIVE movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 1, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_DEPARTURE'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO



	/* Cambia el estado de la movilidad al estado RECOGNIZED, el estudiante vuelve a la universidad origen
	Recibe como parametro el identificador de la movilidad
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/
            
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'STUDENT_ARRIVAL'
) DROP PROCEDURE dbo.STUDENT_ARRIVAL;
GO 
CREATE PROCEDURE dbo.STUDENT_ARRIVAL(@P_OMOBILITY_ID varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_OMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei is null
				BEGIN
					SET @P_ERROR_MESSAGE = 'No se puede cambiar al estado RECOGNIZED de movilidades para las cuales no se es el propietario. Unicamente se pueden cambiar al estado RECOGNIZED movilidades de tipo outgoing.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_OUTGOING = 3, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 1, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.STUDENT_ARRIVAL'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO



	/* Cambia el estado de la movilidad al estado VERIFIED, la movilidad se ha aprobado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'APPROVE_NOMINATION'
) DROP PROCEDURE dbo.APPROVE_NOMINATION;
GO 
CREATE PROCEDURE dbo.APPROVE_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0
	
	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.APPROVE_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO




	/* Cambia el estado de la movilidad al estado REJECTED, la movilidad se ha rechazado por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'REJECT_NOMINATION'
) DROP PROCEDURE dbo.REJECT_NOMINATION;
GO 
CREATE PROCEDURE dbo.REJECT_NOMINATION(@P_OMOBILITY_ID varchar(255), @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION
						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 4, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME() WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.REJECT_NOMINATION'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;
GO




	/* Cambia las fechas actuales de la movilidad por parte de la universidad receptora
	Recibe como parametro el identificador de la movilidad
	Admite como parametro un comentario
	Admite un parametro de salida con una descripcion del error en caso de error.
	Se debe indicar el codigo SCHAC de la institucion a notificar
	Retorna un codigo de ejecucion 0 ok <0 si error.
	*/

IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'UPDATE_ACTUAL_DATES'
) DROP PROCEDURE dbo.UPDATE_ACTUAL_DATES;
GO 
CREATE PROCEDURE dbo.UPDATE_ACTUAL_DATES(@P_OMOBILITY_ID varchar(255), @P_ACTUAL_DEPARTURE date, @P_ACTUAL_ARRIVAL date, @P_MOBILITY_COMMENT varchar(255), @P_ERROR_MESSAGE varchar(255) OUTPUT, @P_HEI_TO_NOTIFY varchar(255) , @return_value integer OUTPUT) AS 
BEGIN
	DECLARE @v_notifier_hei varchar(255)
	DECLARE @v_count int
	DECLARE @v_m_revision varchar(255)

	SET @return_value = 0

	IF @P_HEI_TO_NOTIFY is null
	BEGIN
		SET @P_ERROR_MESSAGE = 'No se ha indicado la institucion a la que notificar los cambios'
		SET @return_value = -1
	END 
	ELSE
	BEGIN
		SELECT @v_count = COUNT(1) FROM dbo.EWPCV_MOBILITY where ID = @P_OMOBILITY_ID
		IF @v_count > 0 
		BEGIN
			SELECT @v_m_revision = MAX(MOBILITY_REVISION) FROM dbo.EWPCV_MOBILITY WHERE ID = @P_OMOBILITY_ID
			EXECUTE dbo.OBTEN_NOTIFIER_HEI_IMOB @P_OMOBILITY_ID, @v_m_revision, @P_HEI_TO_NOTIFY, @v_notifier_hei output

			IF @v_notifier_hei IS NULL
				BEGIN
					SET @P_ERROR_MESSAGE = 'La institucion a notificar no coincide con la institucion emisora de la movilidad. Unicamente se deben aceptar o rechazar movilidades de tipo INCOMING.'
					SET @return_value = -1
				END

			IF @return_value = 0 
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

						UPDATE dbo.EWPCV_MOBILITY SET STATUS_INCOMING = 6, MOBILITY_COMMENT = @P_MOBILITY_COMMENT, MODIFY_DATE = SYSDATETIME(), 
							ACTUAL_ARRIVAL_DATE = @P_ACTUAL_ARRIVAL, ACTUAL_DEPARTURE_DATE = @P_ACTUAL_DEPARTURE
							WHERE ID = @P_OMOBILITY_ID AND MOBILITY_REVISION = @v_m_revision
						EXECUTE dbo.INSERTA_NOTIFICATION @P_OMOBILITY_ID, 2, @P_HEI_TO_NOTIFY, @v_notifier_hei
						COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					THROW
						SET @P_ERROR_MESSAGE = 'Error al ejecutar dbo.UPDATE_ACTUAL_DATES'
						SET @return_value = -1
						ROLLBACK TRANSACTION
				END CATCH
			END
		END
		ELSE 
		BEGIN
			SET @P_ERROR_MESSAGE = 'La movilidad no existe.'
			SET @return_value = -1
		END
	END
END
;

GO

/*	
	Persiste un periodo academico en el sistema y retorna el UUID generado para el registro.
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'INSERTA_ACADEMIC_TERM'
) DROP PROCEDURE dbo.INSERTA_ACADEMIC_TERM;
GO 
CREATE PROCEDURE  dbo.INSERTA_ACADEMIC_TERM(@P_ACADEMIC_TERM xml, @v_id varchar(255) output) as
BEGIN
	DECLARE @v_a_id varchar(255)
	DECLARE @v_o_id varchar(255)
	DECLARE @v_li_id varchar(255)

	DECLARE @ACADEMIC_YEAR varchar(255)
	DECLARE @INSTITUTION_ID varchar(255)
	DECLARE @ORGANIZATION_UNIT_CODE varchar(255)
	DECLARE @TOTAL_TERMS varchar(255)
	DECLARE @TERM_NUMBER varchar(255)
	DECLARE @START_DATE date
	DECLARE @END_DATE date
	DECLARE @DESCRIPTION xml
	DECLARE @TEXT_ITEM xml
		
	SELECT 
		@ACADEMIC_YEAR = T.c.value('(academic_year)[1]', 'varchar(255)'),
		@INSTITUTION_ID = T.c.value('(institution_id)[1]', 'varchar(255)'),
		@ORGANIZATION_UNIT_CODE = T.c.value('(organization_unit_code)[1]', 'varchar(255)'),
		@TOTAL_TERMS = T.c.value('(total_terms)[1]', 'varchar(255)'),
		@TERM_NUMBER = T.c.value('(term_number)[1]', 'varchar(255)'),
		@START_DATE = T.c.value('(start_date)[1]', 'date'),
		@END_DATE = T.c.value('(end_date)[1]', 'date'),
		@DESCRIPTION = T.c.query('description')
	FROM @P_ACADEMIC_TERM.nodes('academic_term') T(c)

	SELECT @v_a_id = ID 
			FROM EWPCV_ACADEMIC_YEAR
			WHERE UPPER(END_YEAR) = UPPER(SUBSTRING(@ACADEMIC_YEAR, 1, 4))
			AND	UPPER(START_YEAR) =  UPPER(SUBSTRING(@ACADEMIC_YEAR, 6, 4))

	IF (@v_a_id is null)
	BEGIN
		SET @v_a_id = NEWID()
		INSERT INTO dbo.EWPCV_ACADEMIC_YEAR (ID, START_YEAR, END_YEAR)
			VALUES(@v_a_id, SUBSTRING(@ACADEMIC_YEAR, 1, 4), SUBSTRING(@ACADEMIC_YEAR, 6, 4))
	END 


	SELECT @v_o_id = O.ID
			FROM dbo.EWPCV_ORGANIZATION_UNIT O 
			INNER JOIN dbo.EWPCV_INST_ORG_UNIT IO ON IO.ORGANIZATION_UNITS_ID = O.ID
			INNER JOIN dbo.EWPCV_INSTITUTION I ON IO.INSTITUTION_ID = I.ID
			WHERE UPPER(I.INSTITUTION_ID) = UPPER(@INSTITUTION_ID)
			AND UPPER(O.ORGANIZATION_UNIT_CODE) = UPPER(@ORGANIZATION_UNIT_CODE)

	SET @v_id = NEWID()

	INSERT INTO EWPCV_ACADEMIC_TERM (ID, END_DATE, START_DATE, INSTITUTION_ID, ORGANIZATION_UNIT_ID, ACADEMIC_YEAR_ID, TOTAL_TERMS, TERM_NUMBER)
			VALUES (@v_id, @END_DATE, @START_DATE , LOWER(@INSTITUTION_ID) , @v_o_id, @v_a_id, @TOTAL_TERMS, @TERM_NUMBER)

	DECLARE cur CURSOR LOCAL FOR
	SELECT 
		T.c.query('.')
	FROM @DESCRIPTION.nodes('description/text') T(c)

	OPEN cur
	FETCH NEXT FROM cur INTO @TEXT_ITEM
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE dbo.INSERTA_LANGUAGE_ITEM @TEXT_ITEM , @v_li_id output
			INSERT INTO dbo.EWPCV_ACADEMIC_TERM_NAME (ACADEMIC_TERM_ID, DISP_NAME_ID) VALUES (@v_id, @v_li_id)
			FETCH NEXT FROM cur INTO @TEXT_ITEM
		END
	CLOSE cur
	DEALLOCATE cur

END
;

GO

/*
	Borra los language skill asociados a una revision de una mobilidad
*/
IF EXISTS (
Select * from sysobjects where type = 'P' and category = 0 AND NAME = 'BORRA_MOBILITY_LANSKILL'
) DROP PROCEDURE dbo.BORRA_MOBILITY_LANSKILL;
GO 
CREATE PROCEDURE  dbo.BORRA_MOBILITY_LANSKILL(@P_MOBILITY_ID varchar(255), @P_MOBILITY_REVISION integer) AS
BEGIN
	DECLARE @LANGUAGE_SKILL_ID varchar(255)

	DECLARE cur CURSOR LOCAL FOR
	SELECT LANGUAGE_SKILL_ID
			FROM dbo.EWPCV_MOBILITY_LANG_SKILL 
			WHERE MOBILITY_ID = @P_MOBILITY_ID
			AND MOBILITY_REVISION = @P_MOBILITY_REVISION

	OPEN cur
	FETCH NEXT FROM cur INTO @LANGUAGE_SKILL_ID
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM dbo.EWPCV_MOBILITY_LANG_SKILL WHERE LANGUAGE_SKILL_ID = @LANGUAGE_SKILL_ID
			FETCH NEXT FROM cur INTO @LANGUAGE_SKILL_ID
		END
	CLOSE cur
	DEALLOCATE cur

END
;
GO
/*
    *************************************
    FIN MODIFICACIONES SOBRE MOBILITYLA
    *************************************
*/

/*
    *************************************
    INICIO MODIFICACIONES SOBRE OUNIT
    *************************************
*/
/*
    *************************************
    FIN MODIFICACIONES SOBRE OUNIT
    *************************************
*/


/*
    *************************************
    INICIO MODIFICACIONES SOBRE TORS
    *************************************
*/

/*
    *************************************
    FIN MODIFICACIONES SOBRE TORS
    *************************************
*/

INSERT INTO EWP.dbo.EWPCV_SCRIPT_VERSIONS ( ID, EWPCV_VERSION, VERSION_DATE, SCRIPT) VALUES (NEXT VALUE FOR EWP.dbo.SEC_EWPCV_SCRIPT_VERSION, 'v01.06.01', GETDATE(), '15_UPGRADE_v01.06.01');



