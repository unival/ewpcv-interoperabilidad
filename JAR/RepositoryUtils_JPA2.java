package eus.ehu.service.ewp.api.iia.controller.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.criteria.*;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.RepositoryUtilsInterface;

@Component
public class RepositoryUtils implements RepositoryUtilsInterface {

  private RepositoryUtils() {
    // Empty constructor. Sonar rules
  }

  /**
   * Creates a Pageable
   * 
   * @param filtro
   * @param defaultOrder
   * @param aditionalOrder
   * @return
   */
  @Override
  public Pageable createPageable(final PageFilterDTO filtro, final String defaultOrder,
                                 final String... aditionalOrder) {
    final Sort.Direction dirOrdenacion = (filtro.isAscending()) ? Sort.Direction.ASC : Sort.Direction.DESC;

    // columnas por las que se ordenaran los registros
    final String[] colOrd = new String[aditionalOrder.length + 1];
    // el primer elemento es el orden establecido en el filtro o el por defecto
    colOrd[0] = defaultOrder;
    // columnas de ordenacion adicionales
    for (int i = 0; i < aditionalOrder.length; i++) {
      colOrd[i + 1] = aditionalOrder[i];
    }

    return PageRequest.of(filtro.getInitialNumber(), filtro.getPageTotal(), dirOrdenacion, colOrd);
  }


  @Override
  public <T> Predicate buildAndExecuteCriteriaBuilder(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final Map<String, Object> paramFilter, final Map<String, Object> datesFilter,
      final Map<String, Object> staticFilter) {

    final List<Predicate> predicates = new ArrayList<>();

    final Predicate predicateStrings =
        createCriteriaBuilderByParamFilters(criteriaBuilder, root, predicates, paramFilter);

    final Predicate predicateDates = createCriteriaBuilderByDatesFilter(criteriaBuilder, root, predicates, datesFilter);

    final Predicate predicateStaticFilter =
        createCriteriaBuilderByParamFilters(criteriaBuilder, root, predicates, staticFilter);

    return criteriaBuilder.and(predicateStrings, predicateDates, predicateStaticFilter);
  }

  /**
   * 
   * Create a Predicate
   * 
   * @param criteriaBuilder
   * @param value
   * @param root
   * @param key
   * @param <T>
   * @return
   */
  private static <T> Predicate buildJoinPredicateByKey(final CriteriaBuilder criteriaBuilder, final Object value,
      final Root<T> root, final String key) {
    final List<String> keys = new ArrayList<>(Arrays.asList(key.split("\\.")));
    Path<T> criteriaPath = null;
    if (keys.size() > 1) {
      Join<T, T> join = root.join(keys.get(0), JoinType.LEFT);
      keys.remove(0);
      while (keys.size() > 1) {
        join = join.join(keys.get(0), JoinType.LEFT);
        keys.remove(0);
      }
      criteriaPath = join.get(keys.get(0));
    } else {
      criteriaPath = root.get(keys.get(0));
    }
    if (Objects.nonNull(value)) {
      value.getClass();
      if (value instanceof String) {
        return getPredicateString(criteriaBuilder, value, criteriaPath);
      } else if (value instanceof List) {
        final List<Integer> list = new ArrayList<>();
        list.addAll((List) value);
        return criteriaPath.in(list);
      } else {
        return criteriaBuilder.equal(criteriaPath, value);
      }
    }
    return criteriaBuilder.isNull(criteriaPath);
  }

  private static <T> Predicate getPredicateString(final CriteriaBuilder criteriaBuilder, final Object value,
      final Path<T> criteriaPath) {
    final String normalizedValue = Normalizer.normalize(value.toString(), Normalizer.Form.NFD);
    return criteriaBuilder.like(
        criteriaBuilder.upper(criteriaBuilder.function("convert", String.class,
            new Expression[] {criteriaPath, criteriaBuilder.literal("US7ASCII")})),
        "%" + normalizedValue.toUpperCase().replaceAll("[^\\p{ASCII}]", "") + "%");
  }

  private static <T> Predicate getPredicateFechaFrom(final CriteriaBuilder criteriaBuilder, final Object value,
      final Root<T> root, final String key) {
    final String normalizedValue = Normalizer.normalize(value.toString(), Normalizer.Form.NFD);
    try {
      return criteriaBuilder.greaterThanOrEqualTo(root.get(key),
          new SimpleDateFormat("dd/MM/yy hh:mm:ss").parse(normalizedValue));
    } catch (final ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  private static <T> Predicate getPredicateFechaTo(final CriteriaBuilder criteriaBuilder, final Object value,
      final Root<T> root, final String key) {
    final String normalizedValue = Normalizer.normalize(value.toString(), Normalizer.Form.NFD);
    try {
      return criteriaBuilder.lessThanOrEqualTo(root.get(key),
          new SimpleDateFormat("dd/MM/yy hh:mm:ss").parse(normalizedValue));
    } catch (final ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Creates a Predicate by params filter.
   *
   * Example:
   *
   * final Map<String, Object> paramFilter = new HashMap<>(); This is the paramFilter parameter final Map<String,
   * Object> paramChildFilter = new HashMap<>(); This is the paramFilter child parameter paramChildFilter.put("iiaCode",
   * "IIA_L03"); paramChildFilter.put("iiaId", "C2DA1F6F-25E9-3EEC-E053-1FC616AC1E79"); paramFilter.put("OR",
   * paramDentroFilter);
   * 
   * @param criteriaBuilder
   * @param root
   * @param predicates
   * @param paramFilter
   * @param <T>
   * @return
   */
  @Override
  public <T> Predicate createCriteriaBuilderByParamFilters(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final List<Predicate> predicates, final Map<String, Object> paramFilter) {

    if (Objects.nonNull(paramFilter) && !paramFilter.isEmpty()) {
      String actionToDo;
      for (final Map.Entry<String, Object> entry : paramFilter.entrySet()) {
        final Object filterObj = entry.getValue();

        if (filterObj instanceof Map) {
          actionToDo = entry.getKey();
          createFilterLogicalOperations(criteriaBuilder, root, predicates, actionToDo, (Map<String, String>) filterObj);
        } else {
          predicates.add(buildJoinPredicateByKey(criteriaBuilder, filterObj, root, entry.getKey()));
        }
      }
      return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
    return criteriaBuilder.and();
  }

  /**
   * Creates a Predicate by dates filter.
   * 
   * Example:
   * 
   * final Map<String, Object> datesFilterFrom = new HashMap<>(); This is the datesFilter child FROM
   * datesFilterFrom.put("iiaStartDate", "07/07/21 00:00:00"); final Map<String, Object> datesFilterTo = new
   * HashMap<>(); This is the datesFilter child TO datesFilterTo.put("iiaEndDate", "10/12/22 00:00:00");
   *
   * final Map<String, Object> datesFilter = new HashMap<>(); This is the datesFilter parameter datesFilter.put("from",
   * datesFilterFrom); datesFilter.put("to", datesFilterTo);
   * 
   * @param criteriaBuilder
   * @param root
   * @param predicates
   * @param datesFilter
   * @param <T>
   * @return
   */
  @Override
  public <T> Predicate createCriteriaBuilderByDatesFilter(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final List<Predicate> predicates, final Map<String, Object> datesFilter) {

    if (Objects.nonNull(datesFilter) && !datesFilter.isEmpty()) {
      if (Objects.nonNull(datesFilter.get("from"))) {
        final Map<String, Object> datesFilterFrom = (Map<String, Object>) datesFilter.get("from");
        for (final Map.Entry<String, Object> entry : datesFilterFrom.entrySet()) {
          final Object filtroObj = entry.getValue();
          predicates.add(getPredicateFechaFrom(criteriaBuilder, filtroObj, root, entry.getKey()));
        }
      }
      if (Objects.nonNull(datesFilter.get("to"))) {
        final Map<String, Object> datesFilterTo = (Map<String, Object>) datesFilter.get("to");
        for (final Map.Entry<String, Object> entry : datesFilterTo.entrySet()) {
          final Object filtroObj = entry.getValue();
          predicates.add(getPredicateFechaTo(criteriaBuilder, filtroObj, root, entry.getKey()));
        }
      }
      return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
    return criteriaBuilder.and();
  }

  private static <T> void createFilterLogicalOperations(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final List<Predicate> predicates, final String actionToDo, final Map<String, String> filterObj) {

    final List<Predicate> predicatesAccion = new ArrayList<>();

    for (final Map.Entry<String, String> entry : filterObj.entrySet()) {
      predicatesAccion.add(buildJoinPredicateByKey(criteriaBuilder, entry.getValue(), root, entry.getKey()));
    }

    switch (actionToDo) {
      case "OR":
        predicates.add(criteriaBuilder.or(predicatesAccion.toArray(new Predicate[predicatesAccion.size()])));
        break;
      case "AND":
        predicates.add(criteriaBuilder.and(predicatesAccion.toArray(new Predicate[predicatesAccion.size()])));
        break;
      default:
        break;
    }
  }
}
