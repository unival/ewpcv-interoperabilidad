package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Grade frequency.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class GradeFrequency implements SQLData, Serializable {
  public static final String SQL_TYPE = "PKG_TORS.GRADE_FREQUENCY";
  public static final String SQL_LIST_TYPE = "PKG_TORS.GRADE_FREQUENCY_LIST";

  private static final long serialVersionUID = 5639211920661560430L;
  private String label;
  private String percentage;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setLabel(sqlInput.readString());
    setPercentage(sqlInput.readString());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getLabel());
    sqlOutput.writeString(getPercentage());
  }
}
