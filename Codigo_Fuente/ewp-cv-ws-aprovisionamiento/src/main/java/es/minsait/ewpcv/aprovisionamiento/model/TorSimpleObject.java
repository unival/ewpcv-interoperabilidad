package es.minsait.ewpcv.aprovisionamiento.model;


import java.io.Serializable;
import java.sql.*;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Tor simple object.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class TorSimpleObject implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.TOR_SIMPLE_OBJECT";
  private static final long serialVersionUID = -2949897024079554751L;
  private Date generatedDate;
  private List<SimpleTorReport> torReportList;


  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setGeneratedDate(sqlInput.readDate());
    setTorReportList(Util.listFromArray(sqlInput.readArray(), SimpleTorReport.class));

  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getGeneratedDate())) {
      sqlOutput.writeDate(new java.sql.Date(getGeneratedDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    Util.writeArrayFromList(sqlOutput, SimpleTorReport.SQL_LIST_TYPE, getTorReportList());
  }
}

