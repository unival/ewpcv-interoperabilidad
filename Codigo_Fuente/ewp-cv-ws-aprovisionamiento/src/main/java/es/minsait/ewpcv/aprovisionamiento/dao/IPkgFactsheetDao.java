package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.model.FactsheetInstitution;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The interface Pkg factsheet dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgFactsheetDao {

  RespuestaPkg insertFactsheet(FactsheetInstitution pFactsheet);

  RespuestaPkg updateFactsheet(String institutionId, FactsheetInstitution pFactsheet);

  RespuestaPkg deleteFactsheet(String institutionId);
}
