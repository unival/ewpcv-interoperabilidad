package es.minsait.ewpcv.aprovisionamiento.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgTorDao;
import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The type Pkg tor dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgTorDaoImpl implements IPkgTorDao {

  private static final String PKG = "PKG_TORS";

  @PersistenceContext
  EntityManager em;

  @Override
  public RespuestaPkg insertTor(final Tor pTor, final String pLaId, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_TOR(?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pTor);
        function.setString(3, pLaId);
        function.registerOutParameter(4, Types.VARCHAR);
        function.registerOutParameter(5, Types.VARCHAR);
        function.setString(6, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(4), function.getString(5)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg insertBasicTor(final TorSimpleObject pTor, final String pLaId, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (
          CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_BASIC_TOR(?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pTor);
        function.setString(3, pLaId);
        function.registerOutParameter(4, Types.VARCHAR);
        function.registerOutParameter(5, Types.VARCHAR);
        function.setString(6, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(4), function.getString(5)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateTor(final String pTorId, final Tor pTor, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_TOR(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, pTorId);
        function.setObject(3, pTor);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateBasicTor(final String pTorId, final TorSimpleObject pTor, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_TOR(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, pTorId);
        function.setObject(3, pTor);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteTor(final String pTorId, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_TOR(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, pTorId);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteBasicTor(final String pTorId, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_BASIC_TOR(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, pTorId);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg insertGroupTypeList(final List<GroupType> pGroupTypeList) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".INSERT_GROUP_TYPE_LIST(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pGroupTypeList);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg insertGroupList(final List<LoiGroup> pGroupList) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_GROUP_LIST(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pGroupList);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg insertGradingScheme(final GradingScheme pGradingScheme) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".INSERT_GRADING_SCHEME(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pGradingScheme);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }
}
