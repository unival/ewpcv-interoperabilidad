package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Duration class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class DurationClass implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_IIAS.DURATION";
  private static final long serialVersionUID = 1L;
  BigDecimal numberDuration;
  Integer unit;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setNumberDuration(sqlInput.readBigDecimal());
    setUnit(sqlInput.readInt());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeBigDecimal(getNumberDuration());
    sqlOutput.writeInt(getUnit());
  }
}
