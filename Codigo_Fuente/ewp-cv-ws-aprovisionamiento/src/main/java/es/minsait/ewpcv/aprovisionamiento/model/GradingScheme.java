package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Grading scheme.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class GradingScheme implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.GRADING_SCHEME";
  private static final long serialVersionUID = -5191742320298990527L;
  private List<LanguageItem> label;
  private List<LanguageItem> gradDescription;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setLabel(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setGradDescription(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getLabel());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getGradDescription());
  }
}
