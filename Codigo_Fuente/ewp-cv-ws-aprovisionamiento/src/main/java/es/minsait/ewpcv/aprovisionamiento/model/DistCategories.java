package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Dist categories.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class DistCategories implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.DIST_CATEGORIES";
  public static final String SQL_LIST_TYPE = "PKG_TORS.DIST_CATEGORIES_LIST";
  private static final long serialVersionUID = -2565301938447930848L;

  private LanguageItem label;
  private Integer distCatCount;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setLabel((LanguageItem) sqlInput.readObject());
    setDistCatCount(sqlInput.readInt());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeObject(getLabel());
    sqlOutput.writeInt(getDistCatCount());
  }
}
