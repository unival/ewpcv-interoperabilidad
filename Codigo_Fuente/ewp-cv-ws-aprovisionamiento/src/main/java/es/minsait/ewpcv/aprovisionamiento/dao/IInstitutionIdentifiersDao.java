package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionIdentifiersView;

/**
 * The interface Institution identifiers dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionIdentifiersDao {

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);

  InstitutionIdentifiersView findById(String id);

  List<InstitutionIdentifiersView> findAll();

  List<InstitutionIdentifiersView> findAllByErasmusCode(String erasmusCode);
}
