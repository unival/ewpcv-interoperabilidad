package es.minsait.ewpcv.aprovisionamiento.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IFunctionsUtilDao;

/**
 * The type Function utils dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class FunctionUtilsDaoImpl implements IFunctionsUtilDao {

  @PersistenceContext
  EntityManager em;

  @Override
  public String notificaElemento(final String elementId, final Integer type, final String heiToNotify,
      final String notifierHei) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<String> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call EWP.NOTIFICA_ELEMENTO(?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, elementId);
        function.setInt(3, type);
        function.setString(4, heiToNotify);
        function.setString(5, notifierHei);
        function.registerOutParameter(6, Types.VARCHAR);
        function.execute();
        result.set(function.getString(6));
      }
    });
    return result.get();
  }

  @Override
  public String resetMUpdateRequest(final String identifier) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<String> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call EWP.RESET_M_UPDATE_REQUEST(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, identifier);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(function.getString(3));
      }
    });
    return result.get();
  }

  @Override
  public String resetNotificacion(final String identifier) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<String> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call EWP.RESET_NOTIFICATION(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, identifier);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(function.getString(3));
      }
    });
    return result.get();
  }

  @Override
  public String insertaNotificacionEntrante(final String elementId, final Integer type, final String heiToNotify,
                                 final String notifierHei) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<String> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call EWP.INSERTA_NOTIFICATION_ENTRANTE(?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, elementId);
        function.setInt(3, type);
        function.setString(4, heiToNotify);
        function.setString(5, notifierHei);
        function.registerOutParameter(6, Types.VARCHAR);
        function.execute();
        result.set(function.getString(6));
      }
    });
    return result.get();
  }
}
