package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Iia coop conditions view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "IIA_COOP_CONDITIONS_VIEW")
@Data
public class IiaCoopConditionsView implements Serializable {

  private static final long serialVersionUID = 8052251614269012037L;

  @Id
  @Column(name = "COOPERATION_CONDITION_ID")
  String coopCondId;

  @Column(name = "IIA_ID")
  String iiaId;

  @Column(name = "IIA_CODE")
  String iiaCode;

  @Column(name = "REMOTE_IIA_ID")
  String remoteIiaId;

  @Column(name = "REMOTE_IIA_CODE")
  String remoteIiaCode;

  @Column(name = "IIA_START_DATE")
  Date iiaStartDate;

  @Column(name = "IIA_END_DATE")
  Date iiaEndDate;

  @Column(name = "IIA_MODIFY_DATE")
  Date iiaModifyDate;

  @Column(name = "IIA_APPROVAL_DATE")
  Date iiaApprovalDate;

  @Column(name = "COOP_COND_START_DATE")
  Date coopCondStartDate;

  @Column(name = "COOP_COND_END_DATE")
  Date coopCondEndDate;

  @Column(name = "COOP_COND_BLENDED")
  Boolean coopCondBlended;

  @Column(name = "COOP_COND_ORDER_INDEX")
  Integer coopCondOrderIndex;

  @Column(name = "COOP_COND_EQF_LEVEL")
  String coopCondEqfLevel;

  @Column(name = "COOP_COND_DURATION")
  String coopCondDuration;

  @Column(name = "COOP_COND_PARTICIPANTS")
  Integer coopCondParticipants;

  @Column(name = "COOP_COND_OTHER_INFO")
  String coopCondOtherInfo;

  @Column(name = "MOBILITY_TYPE")
  String mobilityType;

  @Column(name = "SUBJECT_AREAS")
  String subjectAreas;

  @Column(name = "SUBJECT_AREAS_LIST")
  String subjectAreasList;

  @Column(name = "RECEIVING_INSTITUTION")
  String receivingInstitution;

  @Column(name = "RECEIVING_INSTITUTION_NAMES")
  String receivingInstitutionNames;

  @Column(name = "RECEIVING_OUNIT_CODE")
  String receivingOunitCode;

  @Column(name = "RECEIVING_OUNIT_NAMES")
  String receivingOunitNames;

  @Column(name = "SENDING_INSTITUTION")
  String sendingInstitution;

  @Column(name = "SENDING_INSTITUTION_NAMES")
  String sendingInstitutionNames;

  @Column(name = "SENDING_OUNIT_CODE")
  String sendingOunitCode;

  @Column(name = "SENDING_OUNIT_NAMES")
  String sendingOunitNames;

  @Column(name = "APPROVAL_ID")
  String approvalId;

  @Column(name = "TERMINATED")
  String terminated;
}
