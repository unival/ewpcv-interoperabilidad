package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.model.Ounit;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The interface Pkg ounit dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgOunitDao {

  RespuestaPkg insertOunit(Ounit pOunit);

  RespuestaPkg updateOunit(String id, Ounit pOunit);

  RespuestaPkg deleteOunit(String id, String institutionSchac);

  RespuestaPkg ocultarOunit(String id, String institutionSchac);

  RespuestaPkg exponerOunit(String id, String institutionSchac);
}
