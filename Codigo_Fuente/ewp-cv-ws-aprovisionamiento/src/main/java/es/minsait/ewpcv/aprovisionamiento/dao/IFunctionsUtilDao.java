package es.minsait.ewpcv.aprovisionamiento.dao;

/**
 * The interface Functions util dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IFunctionsUtilDao {

    String notificaElemento(String elementId, Integer type, String heiToNotify, String notifierHei);

    String resetMUpdateRequest(String identifier);

    String resetNotificacion(String identifier);

    String insertaNotificacionEntrante(String elementId, Integer type, String heiToNotify, String notifierHei);
}
