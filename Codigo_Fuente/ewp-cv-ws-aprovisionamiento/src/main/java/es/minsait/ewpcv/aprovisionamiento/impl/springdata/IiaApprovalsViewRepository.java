package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.IiaApprovalsView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IiaApprovalsViewRepository
        extends JpaRepository<IiaApprovalsView, String>, JpaSpecificationExecutor<IiaApprovalsView> {
}
