package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Ounit contacts view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "OUNIT_CONTACTS_VIEW")
@Data
public class OunitContactsView implements Serializable {

  private static final long serialVersionUID = 344857425563878433L;

  @Id
  @Column(name = "CONTACT_ID")
  private String contactId;

  @Column(name = "OUNIT_ID")
  private String ounitId;

  @Column(name = "INTERNAL_ID")
  private String internalID;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "OUNIT_CODE")
  private String ounitCode;

  @Column(name = "OUNIT_NAME")
  private String ounitName;

  @Column(name = "OUNIT_ABBREVIATION")
  private String ounitAbbreviation;

  @Column(name = "OUNIT_LOGO_URL")
  private String ounitLogo;

  @Column(name = "CONTACT_ROLE")
  private String contactRole;

  @Column(name = "CONTACT_NAME")
  private String contactName;

  @Column(name = "CONTACT_FIRST_NAMES")
  private String contactFirstNames;

  @Column(name = "CONTACT_LAST_NAME")
  private String contactLastName;

  @Column(name = "CONTACT_GENDER")
  private String contactGender;

  @Column(name = "CONTACT_PHONE_NUMBER")
  private String contactPhoneNumber;

  @Column(name = "CONTACT_FAX_NUMBER")
  private String contactFaxNumber;

  @Column(name = "CONTACT_MAIL")
  private String contactMail;

  @Column(name = "CONTACT_COUNTRY")
  private String contactCountry;

  @Column(name = "CONTACT_CITY")
  private String contactCity;

  @Column(name = "CONT_STREET_ADDR_LINES")
  private String contactStreetAddressLines;

  @Column(name = "CONTACT_STREET_ADDRESS")
  private String contactStreetAddress;

  @Column(name = "CONT_MAILING_ADDR_LINES")
  private String contactMailingAddressLines;

  @Column(name = "CONTACT_MAILING_ADDRESS")
  private String contactMailingAddress;

  @Column(name = "IS_EXPOSED")
  private Boolean isExposed;
}
