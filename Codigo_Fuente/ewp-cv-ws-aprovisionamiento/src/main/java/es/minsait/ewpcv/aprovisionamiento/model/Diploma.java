package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.*;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Diploma.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Diploma implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.DIPLOMA";
  private static final long serialVersionUID = 5372697003784531772L;

  private Integer version;
  private Date issueDate;
  private byte[] introduction;
  private byte[] signature;
  private List<DiplomaSection> sectionList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setVersion(sqlInput.readInt());
    setIssueDate(sqlInput.readDate());
    setIntroduction(sqlInput.readBytes());
    setSignature(sqlInput.readBytes());
    setSectionList(Util.listFromArray(sqlInput.readArray(), DiplomaSection.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeInt(getVersion());
    if (Objects.nonNull(getIssueDate())) {
      sqlOutput.writeDate(new java.sql.Date(getIssueDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeBytes(getIntroduction());
    sqlOutput.writeBytes(getSignature());
    Util.writeArrayFromList(sqlOutput, DiplomaSection.SQL_LIST_TYPE, getSectionList());
  }
}
