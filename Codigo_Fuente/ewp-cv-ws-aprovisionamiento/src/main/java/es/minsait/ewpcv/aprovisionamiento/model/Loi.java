package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Loi.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Loi implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.LOI";
  public static final String SQL_LIST_TYPE = "PKG_TORS.LOI_LIST";
  public static final String SQL_LIST_ATTACH_REF = "PKG_TORS.ATTACHMENTS_REF_LIST";
  private static final long serialVersionUID = -3193344626424094490L;

  private String loiId;
  private String losId;
  private String losCode;
  private Date startDate;
  private Date endDate;
  private Integer status;
  private String gradingSchemeId;
  private String resultLabel;
  private Integer percentageLower;
  private Integer percentageEqual;
  private Integer percentageHigher;
  private ResultDistribution resultDistribution;
  private List<EducationLevel> educationLevel;
  private String languageOfInstruction;
  private Integer engagementHours;
  private List<String> attachmentRefList;
  private String groupTypeId;
  private String groupId;
  private Diploma diploma;
  private byte[] extension;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setLoiId(sqlInput.readString());
    setLosId(sqlInput.readString());
    setLosCode(sqlInput.readString());
    setStartDate(sqlInput.readDate());
    setEndDate(sqlInput.readDate());
    setStatus(sqlInput.readInt());
    setGradingSchemeId(sqlInput.readString());
    setResultLabel(sqlInput.readString());
    setPercentageLower(sqlInput.readInt());
    setPercentageEqual(sqlInput.readInt());
    setPercentageHigher(sqlInput.readInt());
    setResultDistribution((ResultDistribution) sqlInput.readObject());
    setEducationLevel(Util.listFromArray(sqlInput.readArray(), EducationLevel.class));
    setLanguageOfInstruction(sqlInput.readString());
    setEngagementHours(sqlInput.readInt());
    setAttachmentRefList(Util.listFromArray(sqlInput.readArray(), String.class));
    setGroupTypeId(sqlInput.readString());
    setGroupId(sqlInput.readString());
    setDiploma((Diploma) sqlInput.readObject());
    setExtension(sqlInput.readBytes());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getLoiId());
    sqlOutput.writeString(getLosId());
    sqlOutput.writeString(getLosCode());
    if (Objects.nonNull(getStartDate())) {
      sqlOutput.writeDate(new java.sql.Date(getStartDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getEndDate())) {
      sqlOutput.writeDate(new java.sql.Date(getEndDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeInt(getStatus());
    sqlOutput.writeString(getGradingSchemeId());
    sqlOutput.writeString(getResultLabel());
    sqlOutput.writeInt(getPercentageLower());
    sqlOutput.writeInt(getPercentageEqual());
    sqlOutput.writeInt(getPercentageHigher());
    sqlOutput.writeObject(getResultDistribution());
    Util.writeArrayFromList(sqlOutput, EducationLevel.SQL_LIST_TYPE, getEducationLevel());
    sqlOutput.writeString(getLanguageOfInstruction());
    sqlOutput.writeInt(getEngagementHours());
    Util.writeArrayFromList(sqlOutput, SQL_LIST_ATTACH_REF, getAttachmentRefList());
    sqlOutput.writeString(getGroupTypeId());
    sqlOutput.writeString(getGroupId());
    sqlOutput.writeObject(getDiploma());
    sqlOutput.writeBytes(getExtension());
  }
}
