package es.minsait.ewpcv.aprovisionamiento.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgFactsheetDao;
import es.minsait.ewpcv.aprovisionamiento.model.FactsheetInstitution;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The type Pkg factsheet dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgFactsheetDaoImpl implements IPkgFactsheetDao {

  private static final String PKG = "PKG_FACTSHEET";

  @PersistenceContext
  EntityManager em;

  @Override
  public RespuestaPkg insertFactsheet(final FactsheetInstitution pFactsheet) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_FACTSHEET(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pFactsheet);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateFactsheet(final String institutionId, final FactsheetInstitution pFactsheet) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_FACTSHEET(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, institutionId);
        function.setObject(3, pFactsheet);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteFactsheet(final String institutionId) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_FACTSHEET(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, institutionId);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }
}
