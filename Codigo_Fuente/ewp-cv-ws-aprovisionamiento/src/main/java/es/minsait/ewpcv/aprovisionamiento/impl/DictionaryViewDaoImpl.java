package es.minsait.ewpcv.aprovisionamiento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IDictionaryViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.DictionaryViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryType;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryView;

/**
 * The type Dictionary view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class DictionaryViewDaoImpl implements IDictionaryViewDao {

  @Autowired
  DictionaryViewRepository repository;
  @Autowired
  RepositoryUtilsInterface repoUtils;

  @Override
  public List<DictionaryView> findByDictionaryType(final DictionaryType dictionaryType) {
    return repository.findByDictionaryType(dictionaryType.getType());
  }

  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {

    final Pageable pageable = repoUtils.createPageable(filtro, "description");

    final Page<DictionaryView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
        .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

}
