package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.FactsheetReqInfoView;

/**
 * The interface Factsheet req info view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface FactsheetReqInfoViewRepository
    extends JpaRepository<FactsheetReqInfoView, String>, JpaSpecificationExecutor<FactsheetReqInfoView> {

  List<FactsheetReqInfoView> findByInstitutionId(String institutionId);
}
