package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.Objects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Contact person.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class ContactPerson implements SQLData, Serializable {

  public static final String SQL_TYPE = "CONTACT_PERSON";
  public static final String SQL_LIST_TYPE = "CONTACT_PERSON_LIST";
  private static final long serialVersionUID = 1L;
  String givenName;
  String familyName;
  Date birthDate;
  String citizenShip;
  Integer gender;
  Contact contact;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setGivenName(sqlInput.readString());
    setFamilyName(sqlInput.readString());
    setBirthDate(sqlInput.readDate());
    setCitizenShip(sqlInput.readString());
    setGender(sqlInput.readInt());
    setContact((Contact) sqlInput.readObject());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getGivenName());
    sqlOutput.writeString(getFamilyName());
    if (Objects.nonNull(getBirthDate())) {
      sqlOutput.writeDate(new java.sql.Date(getBirthDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeString(getCitizenShip());
    if (Objects.nonNull(getGender())) {
      sqlOutput.writeInt(getGender());
    } else {
      sqlOutput.writeInt(9);
    }
    sqlOutput.writeObject(getContact());
  }
}
