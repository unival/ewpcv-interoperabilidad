package es.minsait.ewpcv.aprovisionamiento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IInstitutionIdentifiersDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.InstitutionIdentifiersRepository;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionIdentifiersView;

/**
 * The type Institution identifiers dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class InstitutionIdentifiersDaoImpl implements IInstitutionIdentifiersDao {

  @Autowired
  InstitutionIdentifiersRepository institutionIdentifiersRepository;
  @Autowired
  RepositoryUtilsInterface repoUtils;


  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {
    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<InstitutionIdentifiersView> page =
        institutionIdentifiersRepository.findAll((root, query, criteriaBuilder) -> repoUtils
            .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

  @Override
  public InstitutionIdentifiersView findById(final String id) {
    return institutionIdentifiersRepository.findByIdIgnoreCase(id);
  }

  @Override
  public List<InstitutionIdentifiersView> findAll() {
    return institutionIdentifiersRepository.findAll();
  }

  @Override
  public List<InstitutionIdentifiersView> findAllByErasmusCode(String erasmusCode) {
    return institutionIdentifiersRepository.findAllByErasmusCode(erasmusCode);
  }
}
