package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

@Getter
@Setter
@NoArgsConstructor
public class Dictionary implements SQLData, Serializable {

    private static final long serialVersionUID = 1L;
    private static final String SQL_TYPE = "PKG_DICTIONARY.DICTIONARY_ENTRY";

    String code;
    String entryDescription;
    String dictionaryType;
    String callYear;

    @Override
    public String getSQLTypeName() throws SQLException {
        return SQL_TYPE;
    }

    @Override
    public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {

        setCode(sqlInput.readString());
        setEntryDescription(sqlInput.readString());
        setDictionaryType(sqlInput.readString());
        setCallYear(sqlInput.readString());
    }

    @Override
    public void writeSQL(final SQLOutput sqlOutput) throws SQLException {

        sqlOutput.writeString(getCode());
        sqlOutput.writeString(getEntryDescription());
        sqlOutput.writeString(getDictionaryType());
        sqlOutput.writeString(getCallYear());
    }

}
