package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Result distribution.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class ResultDistribution implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.RESULT_DISTRIBUTION";
  private List<DistCategories> distCategoriesList;
  private List<LanguageItem> description;
  private static final long serialVersionUID = 7432595764656406439L;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setDistCategoriesList(Util.listFromArray(sqlInput.readArray(), DistCategories.class));
    setDescription(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    Util.writeArrayFromList(sqlOutput, DistCategories.SQL_LIST_TYPE, getDistCategoriesList());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getDescription());
  }
}
