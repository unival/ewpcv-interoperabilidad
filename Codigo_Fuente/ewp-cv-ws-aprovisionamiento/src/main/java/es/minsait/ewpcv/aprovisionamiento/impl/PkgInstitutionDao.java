package es.minsait.ewpcv.aprovisionamiento.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgInstitutionDao;
import es.minsait.ewpcv.aprovisionamiento.model.Institution;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The type Pkg institution dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgInstitutionDao implements IPkgInstitutionDao {

  private static final String PKG = "PKG_INSTITUTION";

  @PersistenceContext
  EntityManager em;

  @Override
  public RespuestaPkg insertInstitution(final Institution pInstitution) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_INSTITUTION(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pInstitution);
        function.registerOutParameter(3, Types.VARCHAR);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateInstitution(final String id, final Institution pInstitution) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_INSTITUTION(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setObject(3, pInstitution);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteInstitution(final String id) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_INSTITUTION(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }
}
