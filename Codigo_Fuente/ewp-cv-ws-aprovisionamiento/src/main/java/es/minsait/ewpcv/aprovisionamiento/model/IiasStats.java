package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "IIAS_STATS")
@Data
public class IiasStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IiasStatsId id;

}
