package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.InstitutionView;

/**
 * The interface Institution view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface InstitutionViewRepository
    extends JpaRepository<InstitutionView, String>, JpaSpecificationExecutor<InstitutionView> {

  @Query(value = "SELECT iv FROM InstitutionView iv WHERE LOWER(iv.institutionId) = LOWER(:schac)")
  InstitutionView findBySchacCode(@Param("schac") String schac);

  List<InstitutionView> findAllByOrderByInstitutionNameAsc();
}
