package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Multilanguage blob.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class MultilanguageBlob implements SQLData, Serializable {

    public static final String SQL_TYPE = "MULTILANGUAGE_BLOB";
    public static final String SQL_LIST_TYPE = "MULTILANGUAGE_BLOB_LIST";

    private static final long serialVersionUID = 5991917915777407625L;
    private String lang;
    private byte[] blobContent;

    @Override
    public String getSQLTypeName() throws SQLException {
        return SQL_TYPE;
    }

    @Override
    public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
        setLang(sqlInput.readString());
        setBlobContent(sqlInput.readBytes());
    }

    @Override
    public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
        sqlOutput.writeString(getLang());
        sqlOutput.writeBytes(getBlobContent());
    }
}
