package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.model.Institution;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The interface Pkg institution dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgInstitutionDao {

  RespuestaPkg insertInstitution(Institution pInstitution);

  RespuestaPkg updateInstitution(String id, Institution pInstitution);

  RespuestaPkg deleteInstitution(String id);
}
