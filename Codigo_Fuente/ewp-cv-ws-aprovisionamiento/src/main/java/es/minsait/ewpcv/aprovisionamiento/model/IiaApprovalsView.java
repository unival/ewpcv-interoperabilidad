package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IIA_APPROVALS_VIEW")
@Data
public class IiaApprovalsView implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    String id;

    @Column(name = "LOCAL_IIA_ID")
    String localIiaId;

    @Column(name = "REMOTE_IIA_ID")
    String remoteIiaId;

    @Column(name = "IIA_API_VERSION")
    String iiaApiVersion;

    @Column(name = "LOCAL_HASH")
    String localHash;

    @Column(name = "REMOTE_HASH")
    String remoteHash;

    @Column(name = "DATE_IIA_APPROVAL")
    Date dateIiaApproval;

    @Column(name = "LOCAL_HASH_V7")
    String localHashV7;

    @Column(name = "REMOTE_HASH_V7")
    String remoteHashV7;

    @Column(name = "LOCAL_MESSAGE")
    byte[] localMessage;

    @Column(name = "REMOTE_MESSAGE")
    byte[] remoteMessage;
}
