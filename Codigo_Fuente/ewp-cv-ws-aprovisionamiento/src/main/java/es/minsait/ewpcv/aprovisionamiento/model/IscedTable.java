package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Isced table.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class IscedTable implements SQLData, Serializable {
  public static final String SQL_TYPE = "PKG_TORS.ISCED_TABLE";
  public static final String SQL_LIST_TYPE = "PKG_TORS.ISCED_TABLE_LIST";

  private static final long serialVersionUID = -687775884317075090L;
  private List<GradeFrequency> gradeFrequencyList;
  private String iscedCode;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setGradeFrequencyList(Util.listFromArray(sqlInput.readArray(), GradeFrequency.class));
    setIscedCode(sqlInput.readString());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    Util.writeArrayFromList(sqlOutput, GradeFrequency.SQL_LIST_TYPE, getGradeFrequencyList());
    sqlOutput.writeString(getIscedCode());
  }
}
