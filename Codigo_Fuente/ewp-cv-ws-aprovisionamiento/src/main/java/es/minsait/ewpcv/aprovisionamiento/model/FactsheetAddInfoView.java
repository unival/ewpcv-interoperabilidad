package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Factsheet add info view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "FACTSHEET_ADD_INFO_VIEW")
@Data
public class FactsheetAddInfoView implements Serializable {

  private static final long serialVersionUID = 4631503747751000090L;

  @Id
  @Column(name = "ADD_INFO_ID")
  String addInfoId;

  @Column(name = "INSTITUTION_ID")
  String institutionId;

  @Column(name = "INFO_TYPE")
  String infoType;

  @Column(name = "EMAIL")
  String email;

  @Column(name = "PHONE")
  String phone;

  @Column(name = "URLS")
  String urls;
}
