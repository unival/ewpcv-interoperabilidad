package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Language item.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class LanguageItem implements SQLData, Serializable {

  public static final String SQL_TYPE = "LANGUAGE_ITEM";
  public static final String SQL_LIST_TYPE = "LANGUAGE_ITEM_LIST";
  private static final long serialVersionUID = 1L;
  String lang;
  String text;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setLang(sqlInput.readString());
    setText(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getLang());
    sqlOutput.writeString(getText());
  }
}
