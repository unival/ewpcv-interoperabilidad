package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaApprovalsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.IiaApprovalsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.IiaApprovalsView;
import es.minsait.ewpcv.aprovisionamiento.model.IiasView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class IiaApprovalsViewDaoImpl implements IIiaApprovalsViewDao {

    @Autowired
    IiaApprovalsViewRepository repository;

    @Autowired
    RepositoryUtilsInterface repoUtils;

    @Override
    public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
                                            final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {

        final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

        final Page<IiaApprovalsView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
                .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);

        final Map<Object, Object> results = new HashMap<>();
        results.put("results", page.getContent());
        results.put("total", page.getTotalElements());

        return results;
    }
}
