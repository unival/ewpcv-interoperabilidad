package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.model.Iia;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The interface Pkg iias dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgIiasDao {

  RespuestaPkg insertIias(Iia pIias, String pHeiToNotify);

  RespuestaPkg updateIias(String id, Iia pIias, String pHeiToNotify);

  RespuestaPkg deleteIias(String id, String pHeiToNotify);

  RespuestaPkg bindIias(String ownId, String remoteId);

  RespuestaPkg unbindIias(final String ownId);

  RespuestaPkg approveIias(String internalId, String heiToNotify);

  RespuestaPkg prepareSnapshots();

  RespuestaPkg recoverIiaSnapshot(final String id);

  RespuestaPkg terminateIia(final String id, final String pHeiToNotify);
}
