package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Institution.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Institution implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_INSTITUTION.INSTITUTION";
  private static final long serialVersionUID = -3607022624884791399L;
  private String institutionID;
  private List<LanguageItem> institutionName;
  private String abbreviation;
  private Contact primaryContact;
  private List<ContactPerson> secondContactList;
  private List<LanguageItem> factSheetUrlList;
  private String logoUrl;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setInstitutionID(sqlInput.readString());
    setInstitutionName(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setAbbreviation(sqlInput.readString());
    setPrimaryContact((Contact) sqlInput.readObject());
    setSecondContactList(Util.listFromArray(sqlInput.readArray(), ContactPerson.class));
    setFactSheetUrlList(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setLogoUrl(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getInstitutionID());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getInstitutionName());
    sqlOutput.writeString(getAbbreviation());
    sqlOutput.writeObject(getPrimaryContact());
    Util.writeArrayFromList(sqlOutput, ContactPerson.SQL_LIST_TYPE, getSecondContactList());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getFactSheetUrlList());
    sqlOutput.writeString(getLogoUrl());
  }
}
