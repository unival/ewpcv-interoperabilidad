package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.LaComponentView;

/**
 * The interface La components view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LaComponentsViewRepository
    extends JpaRepository<LaComponentView, String>, JpaSpecificationExecutor<LaComponentView> {

  List<LaComponentView> findByLaIdAndLaRevision(String laId, Integer laRevision);
}
