package es.minsait.ewpcv.aprovisionamiento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IInstitutionContactsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.InstitutionContactsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionContactsView;

/**
 * The type Institution contacts view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class InstitutionContactsViewDaoImpl implements IInstitutionContactsViewDao {

  @Autowired
  InstitutionContactsViewRepository institutionContactsViewRepository;
  @Autowired
  RepositoryUtilsInterface repoUtils;


  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {
    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<InstitutionContactsView> page =
        institutionContactsViewRepository.findAll((root, query, criteriaBuilder) -> repoUtils
            .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

  @Override
  public List<InstitutionContactsView> findBySchacCode(final String schac) {
    return institutionContactsViewRepository.findBySchacCode(schac);
  }

  @Override
  public List<InstitutionContactsView> findAllByOrderByInstitutionNameAsc() {
    return institutionContactsViewRepository.findAllByOrderByInstitutionNameAsc();
  }
}
