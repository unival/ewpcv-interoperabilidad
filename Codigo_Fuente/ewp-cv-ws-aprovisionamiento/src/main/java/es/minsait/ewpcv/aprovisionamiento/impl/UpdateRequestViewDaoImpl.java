package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IUpdateRequestViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.UpdateRequestViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.UpdateRequestsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Update request view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class UpdateRequestViewDaoImpl implements IUpdateRequestViewDao {

    @Autowired
    UpdateRequestViewRepository repository;
    @Autowired
    RepositoryUtilsInterface repoUtils;

    @Override
    public Map<Object, Object> findByFilter(PageFilterDTO filtro, Map<String, Object> paramFilter, Map<String, Object> datesFilter, Map<String, Object> staticFilter, String defaultOrder) {
        final Pageable pageable = repoUtils.createPageable(filtro, "description");

        final Page<UpdateRequestsView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
                .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
        final Map<Object, Object> results = new HashMap<>();
        results.put("results", page.getContent());
        results.put("total", page.getTotalElements());

        return results;
    }
}
