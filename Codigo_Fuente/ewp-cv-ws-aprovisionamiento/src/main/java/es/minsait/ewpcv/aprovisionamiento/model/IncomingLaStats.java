package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "INCOMING_LA_STATS")
@Data
public class IncomingLaStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IncomingLaStatsId id;

}
