package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

import java.util.Map;

public interface IOutgoingLaStatsDao {
    Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
                                     final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);
}
