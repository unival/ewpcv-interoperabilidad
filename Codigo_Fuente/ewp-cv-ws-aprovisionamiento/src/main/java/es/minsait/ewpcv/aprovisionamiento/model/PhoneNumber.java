package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Phone number.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class PhoneNumber implements SQLData, Serializable {

  public static final String SQL_TYPE = "PHONE_NUMBER";
  public static final String SQL_LIST_TYPE = "PHONE_NUMBER_LIST";
  private static final long serialVersionUID = 1L;
  String e164;
  String extensionNumber;
  String otherFormat;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setE164(sqlInput.readString());
    setExtensionNumber(sqlInput.readString());
    setOtherFormat(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getE164());
    sqlOutput.writeString(getExtensionNumber());
    sqlOutput.writeString(getOtherFormat());
  }
}
