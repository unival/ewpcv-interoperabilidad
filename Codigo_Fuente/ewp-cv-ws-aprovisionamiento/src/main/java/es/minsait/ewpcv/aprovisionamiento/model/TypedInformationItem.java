package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Typed information item.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class TypedInformationItem implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_FACTSHEET.TYPED_INFORMATION_ITEM";
  public static final String SQL_LIST_TYPE = "PKG_FACTSHEET.INFORMATION_ITEM_LIST";
  private static final long serialVersionUID = 1L;
  String infoType;
  InformationItem informationItem;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setInfoType(sqlInput.readString());
    setInformationItem((InformationItem) sqlInput.readObject());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getInfoType());
    sqlOutput.writeObject(informationItem);
  }
}
