package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Group type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class GroupType implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.GROUP_TYPE";
  public static final String SQL_LIST_TYPE = "PKG_TORS.GROUP_TYPE_LIST";
  private static final long serialVersionUID = -5394310978306547993L;
  private LanguageItem title;
  private List<LoiGroup> groupList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setTitle((LanguageItem) sqlInput.readObject());
    setGroupList(Util.listFromArray(sqlInput.readArray(), LoiGroup.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeObject(getTitle());
    Util.writeArrayFromList(sqlOutput, LoiGroup.SQL_LIST_TYPE, getGroupList());
  }
}
