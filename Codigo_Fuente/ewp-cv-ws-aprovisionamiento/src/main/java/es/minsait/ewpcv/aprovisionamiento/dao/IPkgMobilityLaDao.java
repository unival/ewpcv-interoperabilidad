package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.Date;

import es.minsait.ewpcv.aprovisionamiento.model.LearningAgreement;
import es.minsait.ewpcv.aprovisionamiento.model.Mobility;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;
import es.minsait.ewpcv.aprovisionamiento.model.SignatureMobility;

/**
 * The interface Pkg mobility la dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgMobilityLaDao {

  RespuestaPkg insertLearningAgreement(LearningAgreement pLearningAgreement, String pHeiToNotify);

  RespuestaPkg updateLearningAgreement(String id, LearningAgreement pLearningAgreement, String pHeiToNotify);

  RespuestaPkg deleteLearningAgreement(String id, String pHeiToNotify);

  RespuestaPkg acceptLearningAgreement(String id, Integer pLaRevision, SignatureMobility signature,
      String pHeiToNotify);

  RespuestaPkg rejectLearningAgreement(String id, Integer pLaRevision, SignatureMobility signature, String pObservation,
      String pHeiToNotify);

  RespuestaPkg insertMobility(Mobility pMobilityLa, String pHeiToNotify);

  RespuestaPkg updateMobility(String id, Mobility pMobilityLa, String pHeiToNotify);

  RespuestaPkg deleteMobility(String id, String pHeiToNotify);

  RespuestaPkg cancelMobility(String id, String pHeiToNotify);

  RespuestaPkg studentDeparture(String id, String pHeiToNotify);

  RespuestaPkg studentArrival(String id, String pHeiToNotify);

  RespuestaPkg approveNomination(String id, String comment, String pHeiToNotify);

  RespuestaPkg rejectNomination(String id, String comment, String pHeiToNotify);

  RespuestaPkg updateActualDates(String id, Date actualDeparture, Date actualArrival, String comment,
      String pHeiToNotify);

  RespuestaPkg fixLaApprove(String id);

  RespuestaPkg fixLaReject(String id);
}
