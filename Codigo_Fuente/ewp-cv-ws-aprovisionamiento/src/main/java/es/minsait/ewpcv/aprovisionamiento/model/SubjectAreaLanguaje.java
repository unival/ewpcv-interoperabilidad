package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Subject area languaje.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class SubjectAreaLanguaje implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_IIAS.SUBJECT_AREA_LANGUAGE";
  public static final String SQL_LIST_TYPE = "PKG_IIAS.SUBJECT_AREA_LANGUAGE_LIST";
  private static final long serialVersionUID = 1L;
  SubjectArea subjectArea;
  LanguageSkill languageSkill;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setSubjectArea((SubjectArea) sqlInput.readObject());
    setLanguageSkill((LanguageSkill) sqlInput.readObject());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeObject(getSubjectArea());
    sqlOutput.writeObject(getLanguageSkill());
  }
}
