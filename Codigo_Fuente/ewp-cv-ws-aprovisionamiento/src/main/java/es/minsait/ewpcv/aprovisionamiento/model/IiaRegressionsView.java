package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Iias regressions view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "IIA_REGRESSIONS_VIEW")
@Data
public class IiaRegressionsView implements Serializable {

  private static final long serialVersionUID = 5905582402052332528L;

  @Id
  @Column(name = "ID")
  String id;

  @Column(name = "IIA_ID")
  String iiaId;

  @Column(name = "IIA_HASH")
  String iiaHash;

  @Column(name = "OWNER_SCHAC")
  String ownerSchac;

  @Column(name = "REGRESSION_DATE")
  Date regressionDate;

}
