package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.UpdateRequestsView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The interface Update request view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface UpdateRequestViewRepository extends JpaRepository<UpdateRequestsView, String>, JpaSpecificationExecutor<UpdateRequestsView> {
}
