package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Subject area.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class SubjectArea implements SQLData, Serializable {

  public static final String SQL_TYPE = "SUBJECT_AREA";
  public static final String SQL_LIST_TYPE = "SUBJECT_AREA_LIST";
  private static final long serialVersionUID = 1L;
  String iscedCode;
  String iscedClarification;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setIscedCode(sqlInput.readString());
    setIscedCode(sqlInput.readString());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getIscedCode());
    sqlOutput.writeString(getIscedClarification());
  }
}
