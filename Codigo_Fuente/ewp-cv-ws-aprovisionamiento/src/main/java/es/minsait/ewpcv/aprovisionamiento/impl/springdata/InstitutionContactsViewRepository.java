package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.InstitutionContactsView;

/**
 * The interface Institution contacts view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface InstitutionContactsViewRepository
    extends JpaRepository<InstitutionContactsView, String>, JpaSpecificationExecutor<InstitutionContactsView> {

  @Query(value = "SELECT icv FROM InstitutionContactsView icv WHERE LOWER(icv.institutionId) = LOWER(:schac)")
  List<InstitutionContactsView> findBySchacCode(@Param("schac") String schac);

  List<InstitutionContactsView> findAllByOrderByInstitutionNameAsc();
}
