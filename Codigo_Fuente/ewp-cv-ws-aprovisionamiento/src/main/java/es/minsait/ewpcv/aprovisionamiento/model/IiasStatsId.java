package es.minsait.ewpcv.aprovisionamiento.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class IiasStatsId implements Serializable {

    @Column(name = "FETCHABLES")
    private Integer fetchables;

    @Column(name = "PARTNER_APPROVED")
    private Integer partnerApproved;

    @Column(name = "LOCAL_APPROVED")
    private Integer localApproved;

    @Column(name = "BOTH_APPROVED")
    private Integer bothApproved;
}
