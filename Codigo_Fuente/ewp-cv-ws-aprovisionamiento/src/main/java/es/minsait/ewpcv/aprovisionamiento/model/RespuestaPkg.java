package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Respuesta pkg.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaPkg {
  Integer codRetorno;
  String id;
  String error;
}
