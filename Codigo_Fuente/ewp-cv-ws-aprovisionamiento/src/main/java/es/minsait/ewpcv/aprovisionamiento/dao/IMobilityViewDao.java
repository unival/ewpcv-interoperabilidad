package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.model.MobilityView;

/**
 * The interface Mobility view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IMobilityViewDao {

  List<String> findAllSchacCodeFromSendingInstitutionByReceivingInstitution(String receivingInstitution);

  List<String> findAllSubjectAreaIncludedInLaByReceivingInstitution(String receivingInstitution);

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);

  MobilityView findByMobilityIdAndMobilityRevision(String mobilityId, Integer mobilityRevision);

}
