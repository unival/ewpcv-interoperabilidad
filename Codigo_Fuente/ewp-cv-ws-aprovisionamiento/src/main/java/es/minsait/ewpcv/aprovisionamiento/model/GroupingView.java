package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Grouping view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "GROUPING_VIEW")
@Data
@IdClass(GroupingViewRelationId.class)
public class GroupingView implements Serializable {

    private static final long serialVersionUID = -208440167361224760L;

    @Id
    @Column(name = "GROUP_TYPE_ID")
    String groupTypeId;

    @Column(name = "GROUP_TYPE_NAME")
    String groupTypeName;

    @Id
    @Column(name = "GROUP_ID")
    String groupId;

    @Column(name = "GROUP_NAME")
    String groupName;

    @Column(name = "SORTING_KEY")
    String sortingKey;

}
