package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IIAS_SNAPSHOTS_PENDING_VIEW")
@Data
public class IiasSnapshotsPendingView implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IiasSnapshotsPendingViewId id;

}
