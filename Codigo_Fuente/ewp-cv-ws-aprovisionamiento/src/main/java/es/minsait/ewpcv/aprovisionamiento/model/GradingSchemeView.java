package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Grading scheme view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "GRADING_SCHEME_VIEW")
@Data
public class GradingSchemeView implements Serializable {

    private static final long serialVersionUID = 5315629428206165001L;

    @Id
    @Column(name = "GRADING_SCHEME_ID")
    String gradingSchemeId;

    @Column(name = "GRADING_SCHEME_DESC")
    String gradingSchemeDesc;

    @Column(name = "GRADING_SCHEME_LABELS")
    String gradingSchemeLabels;
}
