package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Tor attachment view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "TOR_ATTACHMENT_VIEW")
@Data
@IdClass(TorAttachmentViewRelationId.class)
public class TorAttachmentView implements Serializable {

    private static final long serialVersionUID = -6090687673403809697L;

    @Column(name ="ATTACH_EXTENSION")
    byte[] attachExtension;

    @Column(name ="ATTACHMENT_CONTENT")
    byte[] attachmentContent;

    @Column(name ="ATTACHMENT_DESC")
    String attachmentDesc;

    @Column(name ="ATTACHMENT_LANG")
    String attachmentLang;

    @Column(name ="ATTACHMENT_TITLES")
    String atachmentTitles;

    @Column(name ="ATTACH_TYPE")
    Integer attachType;

    @Id
    @Column(name ="LA_ID")
    String laId;

    @Id
    @Column(name ="LOI_ID")
    String loiId;

    @Id
    @Column(name ="REPORT_ID")
    String reportId;

    @Id
    @Column(name ="TOR_ID")
    String torId;
}
