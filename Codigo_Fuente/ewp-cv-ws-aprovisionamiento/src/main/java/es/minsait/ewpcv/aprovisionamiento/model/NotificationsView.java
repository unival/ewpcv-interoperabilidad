package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Notifications view.
 *
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "NOTIFICATIONS_VIEW")
@Data
public class NotificationsView implements Serializable {

   private static final long serialVersionUID = 153405203774148099L;

   @Id
   @Column(name = "ID")
   String id;

    @Column(name = "CHANGED_ELEMENT_ID")
    String changedElementId;

    @Column(name = "HEI_ID")
    String heiId;

    @Column(name = "NOTIFICATION_DATE")
    Date notificationDate;

    @Column(name = "TYPE")
    String type;

    @Column(name = "CNR_TYPE")
    String cnrType;

    @Column(name = "RETRIES")
    Integer retries;

    @Column(name = "PROCESSING")
    Boolean processing;

    @Column(name = "OWNER_HEI")
    String ownerHei;

    @Column(name = "PROCESSING_DATE")
    Date processingDate;

    @Column(name = "SHOULD_RETRY")
    Boolean shouldRetry;

    @Column(name = "LAST_ERROR")
    String lastError;
    
}
