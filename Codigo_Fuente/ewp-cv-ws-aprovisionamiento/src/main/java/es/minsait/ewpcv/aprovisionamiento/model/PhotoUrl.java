package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.Objects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Photo url.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class PhotoUrl implements SQLData, Serializable {

  private static final long serialVersionUID = 1L;
  private static final String SQL_TYPE = "PKG_MOBILITY_LA.PHOTO_URL";

  private String url;
  private String photoSize;
  private Date photoDate;
  private Integer photoPublic;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setUrl(sqlInput.readString());
    setPhotoSize(sqlInput.readString());
    setPhotoDate(sqlInput.readDate());
    setPhotoPublic(sqlInput.readInt());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getUrl());
    sqlOutput.writeString(getPhotoSize());
    if (Objects.nonNull(getPhotoDate())) {
      sqlOutput.writeDate(new java.sql.Date(getPhotoDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeInt(getPhotoPublic());
  }
}
