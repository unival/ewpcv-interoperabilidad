package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Files view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "FILES_VIEW")
@Data
public class FilesView implements Serializable {

  private static final long serialVersionUID = 5905582402052332528L;

  @Id
  @Column(name = "ID")
  String id;

  @Column(name = "FILE_ID")
  private String fileId;

  @Column(name = "SCHAC")
  private String schac;

  @Column(name = "FILE_CONTENT")
  private byte[] fileContent;

  @Column(name="MIME_TYPE")
  private String mimeType;

}
