package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Mobility view relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityViewRelationId implements Serializable {
  private static final long serialVersionUID = 2577709020108104195L;
  String mobilityId;
}
