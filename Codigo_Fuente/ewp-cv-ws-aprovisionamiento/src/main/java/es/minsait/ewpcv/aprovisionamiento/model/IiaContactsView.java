package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * The type Iia contacts view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "IIA_CONTACTS_VIEW")
@Data
public class IiaContactsView implements Serializable {

  private static final long serialVersionUID = 8052251614269012037L;

  @Id
  @Column(name = "CONTACT_ID")
  String contactId;

  @Column(name = "IIA_ID")
  String iiaId;

  @Column(name = "IIA_CODE")
  String iiaCode;

  @Column(name = "REMOTE_IIA_ID")
  String remoteIiaId;

  @Column(name = "REMOTE_IIA_CODE")
  String remoteIiaCode;

  @Column(name = "INSTITUTION")
  String institution;

  @Column(name = "INSTITUTION_NAMES")
  String institutionNames;

  @Column(name = "OUNIT_CODE")
  String ounitCode;

  @Column(name = "OUNIT_NAMES")
  String ounitNames;

  @Column(name = "CONTACT_NAME")
  String contactName;

  @Column(name = "LAST_NAME")
  String lastName;

  @Column(name = "BIRTH_DATE")
  Date birthDate;

  @Column(name = "GENDER")
  Integer gender;

  @Column(name = "CITIZENSHIP")
  String citizenship;

  @Column(name = "CONTACT_ROLE")
  String contactRole;

  @Column(name = "CONTACT_NAMES")
  String contactNames;

  @Column(name = "CONTACT_DESCS")
  String contactDescs;

  @Column(name = "CONTACT_URLS")
  String contactUrls;

  @Column(name = "EMAILS")
  String emails;

  @Column(name = "PHONES")
  String phones;

  @Column(name = "ADDRESS_LINES")
  String addressLines;

  @Column(name = "ADDRESS")
  String address;

  @Column(name = "FAXES")
  String faxes;

  @Column(name = "M_ADD_LINES")
  String MaddressLines;

  @Column(name = "MAILING_ADDR")
  String Maddress;

  @Column(name = "POSTAL_CODE")
  String PostalCode;

  @Column(name = "LOCALITY")
  String Locality;

  @Column(name = "REGION")
  String Region;

  @Column(name = "COUNTRY")
  String Country;

}
