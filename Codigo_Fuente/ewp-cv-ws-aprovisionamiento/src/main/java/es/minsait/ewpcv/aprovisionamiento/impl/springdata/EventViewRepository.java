package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.ElementTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import es.minsait.ewpcv.aprovisionamiento.model.EventView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The interface Event view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface EventViewRepository extends JpaRepository<EventView, Integer>, JpaSpecificationExecutor<EventView> {


    @Query(value= "SELECT observations FROM EventView WHERE elementType = :elementType " +
            " AND changedElementId = :changedElementId  AND status = 'KO' order by eventDate desc ")
    List<String> findObservations(@Param("changedElementId") String changedElementId, @Param("elementType") ElementTypes elementType);
}
