package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Attachment.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Attachment implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.ATTACHMENT";
  public static final String SQL_LIST_SECTION_TYPE = "PKG_TORS.SECTION_ATTACHMENT_LIST";
  public static final String SQL_LIST_TOR_TYPE = "PKG_TORS.TOR_ATTACHMENT_LIST";
  public static final String SQL_LIST_MOBILITY_TYPE = "PKG_TORS.MOBILITY_ATTACHMENT_LIST";
  private static final long serialVersionUID = -5912048696632233413L;
  private String attachReference;
  private List<LanguageItem> title;
  private String attType;
  private List<LanguageItem> description;
  private List<MultilanguageBlob> content;
  private byte[] extension;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setAttachReference(sqlInput.readString());
    setTitle(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setAttType(sqlInput.readString());
    setDescription(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setContent(Util.listFromArray(sqlInput.readArray(), MultilanguageBlob.class));
    setExtension(sqlInput.readBytes());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getAttachReference());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getTitle());
    sqlOutput.writeString(getAttType());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getDescription());
    Util.writeArrayFromList(sqlOutput, MultilanguageBlob.SQL_LIST_TYPE, getContent());
    sqlOutput.writeBytes(getExtension());
  }
}
