package es.minsait.ewpcv.aprovisionamiento.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type La component view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "LA_COMPONENTS_VIEW")
@Data
public class LaComponentView {

  @Id
  @Column(name = "COMPONENT_ID")
  String componentId;

  @Column(name = "LA_ID")
  String laId;

  @Column(name = "LA_REVISION")
  Integer laRevision;

  @Column(name = "COMPONENT_TYPE")
  Integer componentType;

  @Column(name = "TITLE")
  String title;

  @Column(name = "DESCRIPTION")
  String description;

  @Column(name = "CREDITS")
  String credits;

  @Column(name = "ACADEMIC_TERM")
  String academicTerm;

  @Column(name = "ACADEMIC_TERM_NAMES")
  String academicTermNames;

  @Column(name = "ACADEMIC_YEAR")
  String academicYear;

  @Column(name = "COMPONENT_STATUS")
  Integer status;

  @Column(name = "REASON_CODE")
  Integer reasonCode;

  @Column(name = "REASON_TEXT")
  String reasonText;

  @Column(name = "RECOGNITION_CONDITIONS")
  String recognitionConditions;

  @Column(name = "LOS_CODE")
  String losCode;

  @Column(name = "INSTITUTION_ID")
  String institutionId;

  @Column(name = "RECEIVING_INSTITUTION_NAMES")
  String receivingInstitutionNames;

  @Column(name = "OUNIT_CODE")
  String ounitCode;

  @Column(name = "RECEIVING_OUNIT_NAMES")
  String receivingOunitNames;

  @Column(name = "LOS_NAMES")
  String losNames;

  @Column(name = "LOS_DESCRIPTIONS")
  String losDescriptions;

  @Column(name = "LOS_URL")
  String losUrl;

  @Column(name = "ENGAGEMENT_HOURS")
  Integer engagementHours;

  @Column(name = "LANGUAGE_OF_INSTRUCTION")
  String languageOfInstitution;

  @Column(name = "LOI_EXTENSION")
  byte[] loiExtension;

  @Column(name = "LOI_GRAD_SCHEME_DESC")
  String loiGradChemeDesc;

  @Column(name = "LOI_GRAD_SCHEME_LABEL")
  String loiGradSchemeLabel;

  @Column(name = "LOI_GROUP")
  String loiGroup;

  @Column(name = "LOI_GROUP_TYPE")
  String loiGroupType;

  @Column(name = "LOI_ID")
  String loiId;

  @Column(name = "LOI_LEVELS")
  String loiLevels;

  @Column(name = "LOI_STATUS")
  String loiStatus;

  @Column(name = "PERCENTAGE_EQUAL")
  String percentageEqual;

  @Column(name = "PERCENTAGE_HIGHER")
  String percentageHigher;

  @Column(name = "PERCENTAGE_LOWER")
  String percentageLower;

  @Column(name = "START_DATE")
  Date startDate;

  @Column(name = "END_DATE")
  Date endDate;

  @Column(name = "RESULT_DISTR")
  String resultDistr;

  @Column(name = "RESULT_LABEL")
  String resultLabel;

  @Column(name = "LOS_ID")
  String losId;

}
