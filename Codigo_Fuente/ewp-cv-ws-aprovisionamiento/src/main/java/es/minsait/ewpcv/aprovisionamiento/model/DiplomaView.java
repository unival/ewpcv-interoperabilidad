package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Diploma view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "DIPLOMA_VIEW")
@Data
public class DiplomaView implements Serializable {

    private static final long serialVersionUID = 6514329617227741628L;

    @Id
    @Column(name="DIPLOMA_ID")
    String diplomaId;

    @Column(name="ADDITIONAL_INFO")
    String additionalInfo;

    @Column(name="ATTACHMENT_TYPE")
    Integer attachmentType;

    @Column(name="DIPLOMA_SECTION_CONTENT")
    byte[] diplomaSectionContent;

    @Column(name="DIPLOMA_SECTION_NUMBER")
    Integer diplomaSectionNumber;

    @Column(name="DIPLOMA_SECTION_TITLE")
    String diplomaSectionTitle;

    @Column(name="DIPLOMA_VERSION")
    Integer diplomaVersion;

    @Column(name="EXTENSION")
    byte[] extension;

    @Column(name="INTRODUCTION")
    String introduction;

    @Column(name="ISSUE_DATE")
    Date issueDate;

    @Column(name="LOI_ID")
    String loiId;

    @Column(name="SIGNATURE")
    String signature;
}
