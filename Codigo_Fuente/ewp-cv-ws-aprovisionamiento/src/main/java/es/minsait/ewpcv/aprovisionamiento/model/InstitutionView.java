package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Institution view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "INSTITUTION_VIEW")
@Data
public class InstitutionView implements Serializable {

  private static final long serialVersionUID = -6474354563566380704L;

  @Id
  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "INSTITUTION_NAME")
  private String institutionName;

  @Column(name = "INSTITUTION_ABBREVIATION")
  private String institutionAbbreviation;

  @Column(name = "INSTITUTION_LOGO_URL")
  private String institutionLogo;

  @Column(name = "INSTITUTION_WEBSITE")
  private String institutionWebsite;

  @Column(name = "INSTITUTION_FACTSHEET_URL")
  private String institutionFactsheetUrl;

  @Column(name = "COUNTRY")
  private String country;

  @Column(name = "CITY")
  private String ccity;

  @Column(name = "STREET_ADDR_LINES")
  private String streetAddressLines;

  @Column(name = "STREET_ADDRESS")
  private String streetAddress;

  @Column(name = "MAILING_ADDR_LINES")
  private String mailingAddressLines;

  @Column(name = "MAILING_ADDRESS")
  private String mailingAddress;
}
