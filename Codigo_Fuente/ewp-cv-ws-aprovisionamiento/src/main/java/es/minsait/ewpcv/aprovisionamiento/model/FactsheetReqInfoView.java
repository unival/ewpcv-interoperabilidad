package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Factsheet req info view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "FACTSHEET_REQ_INFO_VIEW")
@Data
public class FactsheetReqInfoView implements Serializable {

  private static final long serialVersionUID = -1034996421736932316L;

  @Id
  @Column(name = "REQ_INFO_ID")
  String reqInfoId;

  @Column(name = "INSTITUTION_ID")
  String institutionId;

  @Column(name = "REQ_TYPE")
  String reqType;

  @Column(name = "NAME")
  String name;

  @Column(name = "DESCRIPTION")
  String description;

  @Column(name = "EMAIL")
  String email;

  @Column(name = "PHONE")
  String phone;

  @Column(name = "URLS")
  String urls;
}
