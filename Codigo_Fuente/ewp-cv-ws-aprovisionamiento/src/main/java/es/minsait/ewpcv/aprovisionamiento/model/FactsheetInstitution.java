package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Factsheet institution.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class FactsheetInstitution implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_FACTSHEET.FACTSHEET_INSTITUTION";
  private static final long serialVersionUID = 1L;
  String institutionId;
  String abreviation;
  String logoUrl;
  List<LanguageItem> institutionName;
  Factsheet factsheet;
  InformationItem applicationInfo;
  InformationItem housingInfo;
  InformationItem visaInfo;
  InformationItem insuranceInfo;
  List<TypedInformationItem> additionalInfo;
  List<AccesibilityRequirementsInfo> acessibilityReqirements;
  List<RequirementsInfo> additionalRequirements;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setInstitutionId(sqlInput.readString());
    setAbreviation(sqlInput.readString());
    setLogoUrl(sqlInput.readString());
    setInstitutionName(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setFactsheet((Factsheet) sqlInput.readObject());
    setApplicationInfo((InformationItem) sqlInput.readObject());
    setHousingInfo((InformationItem) sqlInput.readObject());
    setVisaInfo((InformationItem) sqlInput.readObject());
    setInsuranceInfo((InformationItem) sqlInput.readObject());
    setAdditionalInfo(Util.listFromArray(sqlInput.readArray(), TypedInformationItem.class));
    setAcessibilityReqirements(Util.listFromArray(sqlInput.readArray(), AccesibilityRequirementsInfo.class));
    setAdditionalRequirements(Util.listFromArray(sqlInput.readArray(), RequirementsInfo.class));
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getInstitutionId());
    sqlOutput.writeString(getAbreviation());
    sqlOutput.writeString(getLogoUrl());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getInstitutionName());
    sqlOutput.writeObject(getFactsheet());
    sqlOutput.writeObject(getApplicationInfo());
    sqlOutput.writeObject(getHousingInfo());
    sqlOutput.writeObject(getVisaInfo());
    sqlOutput.writeObject(getInsuranceInfo());
    Util.writeArrayFromList(sqlOutput, TypedInformationItem.SQL_LIST_TYPE, getAdditionalInfo());
    Util.writeArrayFromList(sqlOutput, AccesibilityRequirementsInfo.SQL_LIST_TYPE, getAcessibilityReqirements());
    Util.writeArrayFromList(sqlOutput, RequirementsInfo.SQL_LIST_TYPE, getAdditionalRequirements());
  }
}
