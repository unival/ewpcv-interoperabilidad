package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.DeletedElementsView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import es.minsait.ewpcv.aprovisionamiento.model.DiplomaView;

/**
 * The interface DeletedElementsView view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
public interface DeletedElementsViewRepository
    extends JpaRepository<DeletedElementsView, Integer>, JpaSpecificationExecutor<DeletedElementsView> {
}
