package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

import java.util.Map;

/**
 * The interface Iia view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaPdfViewDao {

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
                                   final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);

}
