package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.model.ElementTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IEventViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.EventViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.EventView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * The type Event view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class EventViewDaoImpl implements IEventViewDao {

  @Autowired
  EventViewRepository repository;
  @Autowired
  RepositoryUtilsInterface repoUtils;

  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {

    final Pageable pageable = repoUtils.createPageable(filtro, "eventDate");

    final Page<EventView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
        .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public String findLastObservations(String changedElementId, ElementTypes elementType) {
    String query = "SELECT observations FROM EventView WHERE elementType = :elementType " +
            " AND changedElementId = :changedElementId  AND status = 'KO' order by eventDate desc ";
    Query q = entityManager.createQuery(query, String.class);
    q.setMaxResults(1);
    q.setParameter("changedElementId", changedElementId);
    q.setParameter("elementType", elementType);
    //List<String> observaciones = repository.findObservations(changedElementId, elementType);
    //obtenemos la primera observacion de la lista, la mas reciente o si no existe null
   // return (observaciones == null || observaciones.isEmpty()) ? null : observaciones.get(0);
    return (String) q.getSingleResult();
  }
}
