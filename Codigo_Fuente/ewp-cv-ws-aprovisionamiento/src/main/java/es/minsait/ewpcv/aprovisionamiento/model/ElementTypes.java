package es.minsait.ewpcv.aprovisionamiento.model;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Element types.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum ElementTypes {
  IIA(0), OMOBILITY(1), IMOBILITY(2), IMOBILITY_TOR(3), IIA_APPROVAL(4), LA(5);

  private int value;

  ElementTypes(int value) {
    this.value = value;
  }

  public static String[] names() {
    ElementTypes[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }
}
