package es.minsait.ewpcv.aprovisionamiento.model;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.sql.rowset.serial.SerialBlob;
import java.io.Serializable;
import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * The type Iia.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Iia implements SQLData, Serializable {

  private static final long serialVersionUID = 1L;
  private static final String SQL_TYPE = "PKG_IIAS.IIA";

  Date startDate;
  Date endDate;
  String iiaCode;
  Partner firstPartner;
  Partner secondPartner;
  List<CooperationCondition> cooperationConditionList;
  byte[] pdf;
  String fileMimeType;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setStartDate(sqlInput.readDate());
    setEndDate(sqlInput.readDate());
    setIiaCode(sqlInput.readString());
    setFirstPartner((Partner) sqlInput.readObject());
    setSecondPartner((Partner) sqlInput.readObject());
    setCooperationConditionList(Util.listFromArray(sqlInput.readArray(), CooperationCondition.class));
    setPdf(sqlInput.readBytes());
    setFileMimeType(sqlInput.readString());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getStartDate())) {
      sqlOutput.writeDate(new java.sql.Date(getStartDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getEndDate())) {
      sqlOutput.writeDate(new java.sql.Date(getEndDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeString(getIiaCode());
    sqlOutput.writeObject(getFirstPartner());
    sqlOutput.writeObject(getSecondPartner());
    Util.writeArrayFromList(sqlOutput, CooperationCondition.SQL_LIST_TYPE, getCooperationConditionList());
    Blob blob = null;
    if (getPdf() != null) {
      blob = new SerialBlob(getPdf());
    }
    sqlOutput.writeBlob(blob);
    sqlOutput.writeString(getFileMimeType());
  }
}
