package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.*;
import java.util.Date;
import java.util.Objects;

import javax.sql.rowset.serial.SerialBlob;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Signature mobility.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class SignatureMobility implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.SIGNATURE";
  private static final long serialVersionUID = 1L;
  String signerName;
  String signerPosition;
  String signerEmail;
  Date signDate;
  String signerApp;
  byte[] signature;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setSignerName(sqlInput.readString());
    setSignerPosition(sqlInput.readString());
    setSignerEmail(sqlInput.readString());
    setSignDate(sqlInput.readDate());
    setSignerApp(sqlInput.readString());
    setSignature(sqlInput.readBytes());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getSignerName());
    sqlOutput.writeString(getSignerPosition());
    sqlOutput.writeString(getSignerEmail());
    if (Objects.nonNull(getSignDate())) {
      sqlOutput.writeDate(new java.sql.Date(getSignDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeString(getSignerApp());
    Blob blob = null;
    if (getSignature() != null) {
      blob = new SerialBlob(getSignature());
    }
    sqlOutput.writeBlob(blob);
  }
}
