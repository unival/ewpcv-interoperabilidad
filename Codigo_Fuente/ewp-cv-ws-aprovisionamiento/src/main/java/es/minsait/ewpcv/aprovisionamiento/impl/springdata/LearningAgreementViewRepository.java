package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.LearningAgreementView;

/**
 * The interface Learning agreement view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LearningAgreementViewRepository
    extends JpaRepository<LearningAgreementView, String>, JpaSpecificationExecutor<LearningAgreementView> {

  List<LearningAgreementView> findByLaId(String laId);

  List<LearningAgreementView> findByMobilityIdAndMobilityRevision(String mobilityId, Integer mobilityRevision);

}
