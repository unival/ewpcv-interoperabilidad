package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Cooperation condition.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class CooperationCondition implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_IIAS.COOPERATION_CONDITION";
  public static final String SQL_LIST_TYPE = "PKG_IIAS.COOPERATION_CONDITION_LIST";
  public static final String SQL_LIST_EQF = "PKG_IIAS.EQF_LEVEL_LIST";
  private static final long serialVersionUID = 1L;
  Date startDate;
  Date endDate;
  List<Integer> eqfLevelList;
  Integer mobilityNumber;
  MobilityType mobilityType;
  Partner receivingPartner;
  Partner sendingPartner;
  List<SubjectAreaLanguaje> subjectAreaLanguajeList;
  List<SubjectArea> subjectAreaList;
  DurationClass duration;
  String otherInfo;
  Integer blended;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setStartDate(sqlInput.readDate());
    setEndDate(sqlInput.readDate());
    setEqfLevelList(Util.listFromArray(sqlInput.readArray(), Integer.class));
    setMobilityNumber(sqlInput.readInt());
    setMobilityType((MobilityType) sqlInput.readObject());
    setReceivingPartner((Partner) sqlInput.readObject());
    setSendingPartner((Partner) sqlInput.readObject());
    setSubjectAreaLanguajeList(Util.listFromArray(sqlInput.readArray(), SubjectAreaLanguaje.class));
    setSubjectAreaList(Util.listFromArray(sqlInput.readArray(), SubjectArea.class));
    setDuration((DurationClass) sqlInput.readObject());
    setOtherInfo(sqlInput.readString());
    setBlended(sqlInput.readInt());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getStartDate())) {
      sqlOutput.writeDate(new java.sql.Date(getStartDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getEndDate())) {
      sqlOutput.writeDate(new java.sql.Date(getEndDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }

    Util.writeArrayFromList(sqlOutput, SQL_LIST_EQF, getEqfLevelList());
    sqlOutput.writeInt(getMobilityNumber());
    sqlOutput.writeObject(getMobilityType());
    sqlOutput.writeObject(getReceivingPartner());
    sqlOutput.writeObject(getSendingPartner());
    Util.writeArrayFromList(sqlOutput, SubjectAreaLanguaje.SQL_LIST_TYPE, getSubjectAreaLanguajeList());
    Util.writeArrayFromList(sqlOutput, SubjectArea.SQL_LIST_TYPE, getSubjectAreaList());
    sqlOutput.writeObject(getDuration());
    sqlOutput.writeString(getOtherInfo());
    sqlOutput.writeInt(getBlended());
  }
}
