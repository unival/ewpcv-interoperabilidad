package es.minsait.ewpcv.aprovisionamiento.dao;

import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;


/**
 * The interface Repository utils interface.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface RepositoryUtilsInterface {

  /**
   * Creates a Pageable for retrieving results
   * 
   * @param filtro
   * @param defaultOrder
   * @param aditionalOrder
   * @return
   */
  Pageable createPageable(final PageFilterDTO filtro, final String defaultOrder, final String... aditionalOrder);


  /**
   * Dinamically creates an execute a query based on the collectiones received
   * 
   * @param criteriaBuilder
   * @param root
   * @param paramFilter
   * @param datesFilter
   * @param staticFilter
   * @param <T>
   * @return
   */
  <T> Predicate buildAndExecuteCriteriaBuilder(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final Map<String, Object> paramFilter, final Map<String, Object> datesFilter,
      final Map<String, Object> staticFilter);

  /**
   * Creates a Predicate by params filter.
   *
   * Example:
   *
   * final Map<String, Object> paramFilter = new HashMap<>(); This is the paramFilter parameter final Map<String,
   * Object> paramChildFilter = new HashMap<>(); This is the paramFilter child parameter paramChildFilter.put("iiaCode",
   * "IIA_L03"); paramChildFilter.put("iiaId", "C2DA1F6F-25E9-3EEC-E053-1FC616AC1E79"); paramFilter.put("OR",
   * paramDentroFilter);
   *
   * @param criteriaBuilder
   * @param root
   * @param predicates
   * @param paramFilter
   * @param <T>
   * @return
   */
  <T> Predicate createCriteriaBuilderByParamFilters(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final List<Predicate> predicates, final Map<String, Object> paramFilter);

  /**
   * Creates a Predicate by dates filter.
   *
   * Example:
   *
   * final Map<String, Object> datesFilterFrom = new HashMap<>(); This is the datesFilter child FROM
   * datesFilterFrom.put("iiaStartDate", "07/07/21 00:00:00"); final Map<String, Object> datesFilterTo = new
   * HashMap<>(); This is the datesFilter child TO datesFilterTo.put("iiaEndDate", "10/12/22 00:00:00");
   *
   * final Map<String, Object> datesFilter = new HashMap<>(); This is the datesFilter parameter datesFilter.put("from",
   * datesFilterFrom); datesFilter.put("to", datesFilterTo);
   *
   * @param criteriaBuilder
   * @param root
   * @param predicates
   * @param datesFilter
   * @param <T>
   * @return
   */
  <T> Predicate createCriteriaBuilderByDatesFilter(final CriteriaBuilder criteriaBuilder, final Root<T> root,
      final List<Predicate> predicates, final Map<String, Object> datesFilter);
}
