package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Diploma section.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class DiplomaSection implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.DIPLOMA_SECTION";
  public static final String SQL_LIST_TYPE = "PKG_TORS.SECTION_LIST";
  public static final String SQL_LIST_ADDIT_INF = "PKG_TORS.ADDITIONAL_INF_LIST";

  private static final long serialVersionUID = -8728798003890743548L;
  private String title;
  private byte[] content;
  private List<String> additionalInfList;
  private List<Attachment> sectionAttachmentList;
  private Integer sectionNumber;
  private List<DiplomaSection2> sectionList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setTitle(sqlInput.readString());
    setContent(sqlInput.readBytes());
    setAdditionalInfList(Util.listFromArray(sqlInput.readArray(), String.class));
    setSectionAttachmentList(Util.listFromArray(sqlInput.readArray(), Attachment.class));
    setSectionNumber(sqlInput.readInt());
    setSectionList(Util.listFromArray(sqlInput.readArray(), DiplomaSection2.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getTitle());
    sqlOutput.writeBytes(getContent());
    Util.writeArrayFromList(sqlOutput, SQL_LIST_ADDIT_INF, getAdditionalInfList());
    Util.writeArrayFromList(sqlOutput, Attachment.SQL_LIST_SECTION_TYPE, getSectionAttachmentList());
    sqlOutput.writeInt(getSectionNumber());
    Util.writeArrayFromList(sqlOutput, DiplomaSection2.SQL_LIST_TYPE, getSectionList());
  }
}
