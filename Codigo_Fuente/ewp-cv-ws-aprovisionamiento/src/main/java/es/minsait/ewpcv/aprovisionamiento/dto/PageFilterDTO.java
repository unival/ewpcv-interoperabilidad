package es.minsait.ewpcv.aprovisionamiento.dto;

import lombok.Data;

/**
 * The type Page filter dto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class PageFilterDTO {
  private int initialNumber;
  private int pageTotal;
  private boolean isAscending = true;
}

