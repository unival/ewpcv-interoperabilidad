package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.OutgoingLaStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OutgoingLaStatsRepository
        extends JpaRepository<OutgoingLaStats, String>, JpaSpecificationExecutor<OutgoingLaStats> {
}
