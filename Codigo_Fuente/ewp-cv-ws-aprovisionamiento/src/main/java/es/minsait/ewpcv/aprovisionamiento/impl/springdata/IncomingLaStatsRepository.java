package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.IncomingLaStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IncomingLaStatsRepository
        extends JpaRepository<IncomingLaStats, String>, JpaSpecificationExecutor<IncomingLaStats> {
}
