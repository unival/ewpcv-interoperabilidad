package es.minsait.ewpcv.aprovisionamiento.dao;

import es.minsait.ewpcv.aprovisionamiento.model.Dictionary;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

public interface IPkgDictionaryDao {

    RespuestaPkg insertDictionary(Dictionary pDictionary);

    RespuestaPkg updateDictionary(String id, Dictionary pDictionary);

    RespuestaPkg deleteDictionary(String id);
}
