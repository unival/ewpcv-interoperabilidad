package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Event view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EVENT_VIEW")
@Data
public class EventView implements Serializable {

  private static final long serialVersionUID = 7018128906302334869L;

  @Id
  @Column(name = "ID")
  String id;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "EVENT_DATE")
  Date eventDate;

  @Column(name = "TRIGGERING_HEI")
  String triggeringHei;

  @Column(name = "OWNER_HEI")
  String ownerHei;

  @Column(name = "EVENT_TYPE")
  String eventType;

  @Column(name = "CHANGED_ELEMENT_ID")
  String changedElementId;

  @Column(name = "ELEMENT_TYPE")
  ElementTypes elementType;

  @Column(name = "OBSERVATIONS")
  String observations;

  @Column(name = "REQUEST_PARAMS")
  String requestParams;

  @Column(name = "MESSAGE")
  byte[] message;

  @Column(name = "X_REQUEST_ID")
  String xRequestId;

  @Column(name = "STATUS")
  String status;

  @Column(name = "REQUEST_BODY")
  byte[] requestBody;


}
