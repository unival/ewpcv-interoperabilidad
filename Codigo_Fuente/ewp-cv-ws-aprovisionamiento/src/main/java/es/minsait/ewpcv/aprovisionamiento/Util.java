package es.minsait.ewpcv.aprovisionamiento;

import java.sql.Array;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import oracle.jdbc.driver.OracleConnection;
import oracle.sql.OracleSQLOutput;


/**
 * The type Util.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class Util {

  public static <T> List<T> listFromArray(Array array, Class<T> typeClass) throws SQLException {
    if (array == null) {
      return Collections.emptyList();
    }
    final Object[] objectArray = (Object[]) array.getArray();
    List<T> list = new ArrayList<>(objectArray.length);
    for (Object o : objectArray) {
      list.add(typeClass.cast(o));
    }
    return list;
  }

  public static <T> void writeArrayFromList(SQLOutput sqlOutput, String listType, List<T> list) throws SQLException {
    final OracleSQLOutput out = (OracleSQLOutput) sqlOutput;
    OracleConnection conn = (OracleConnection) out.getSTRUCT().getJavaSqlConnection();
    if (list == null) {
      list = Collections.emptyList();
    }
    final Array array = conn.createOracleArray(listType, list.toArray());
    out.writeArray(array);
  }

  public static void  writeNullPossibleNumber(final SQLOutput stream, final Integer intValue) throws SQLException {
    if (intValue == null) {
      stream.writeBigDecimal(null);
    } else {
      stream.writeInt(intValue);
    }
  }
}
