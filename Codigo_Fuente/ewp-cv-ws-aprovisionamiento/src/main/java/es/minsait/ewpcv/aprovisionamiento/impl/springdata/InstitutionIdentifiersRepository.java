package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.InstitutionIdentifiersView;

/**
 * The interface Institution identifiers repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface InstitutionIdentifiersRepository
    extends JpaRepository<InstitutionIdentifiersView, String>, JpaSpecificationExecutor<InstitutionIdentifiersView> {

  @Override
  List<InstitutionIdentifiersView> findAll();

  @Query("Select iii FROM InstitutionIdentifiersView iii WHERE LOWER(iii.schac) = LOWER(:id)")
  InstitutionIdentifiersView findByIdIgnoreCase(@Param("id") String id);

  @Query("SELECT inst FROM InstitutionIdentifiersView inst WHERE LOWER(inst.erasmus) = LOWER(:erasmusCode)")
  List<InstitutionIdentifiersView> findAllByErasmusCode(@Param("erasmusCode")String erasmusCode);

}
