package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Learning agreement.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class LearningAgreement implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.LEARNING_AGREEMENT";
  private static final long serialVersionUID = 1L;
  String mobilityId;
  StudentLa studentLa;
  List<LaComponent> laComponentList;
  SignatureMobility studentSignature;
  SignatureMobility sendingHeiSignature;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setMobilityId(sqlInput.readString());
    setStudentLa((StudentLa) sqlInput.readObject());
    setLaComponentList(Util.listFromArray(sqlInput.readArray(), LaComponent.class));
    setStudentSignature((SignatureMobility) sqlInput.readObject());
    setSendingHeiSignature((SignatureMobility) sqlInput.readObject());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getMobilityId());
    sqlOutput.writeObject(getStudentLa());
    Util.writeArrayFromList(sqlOutput, LaComponent.SQL_LIST_TYPE, getLaComponentList());
    sqlOutput.writeObject(getStudentSignature());
    sqlOutput.writeObject(getSendingHeiSignature());
  }
}
