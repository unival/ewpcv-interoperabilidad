package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.Objects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The type Factsheet.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Factsheet implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_FACTSHEET.FACTSHEET";
  private static final long serialVersionUID = 1L;
  Integer decissionWeekLimit;
  Integer torWeekLimit;
  Date nominationsAutumTerm;
  Date nominationsSpringTerm;
  Date applicationAutumTerm;
  Date applicationSpringTerm;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setDecissionWeekLimit(sqlInput.readInt());
    setTorWeekLimit(sqlInput.readInt());
    setNominationsAutumTerm(sqlInput.readDate());
    setNominationsSpringTerm(sqlInput.readDate());
    setApplicationAutumTerm(sqlInput.readDate());
    setApplicationSpringTerm(sqlInput.readDate());

  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeInt(getDecissionWeekLimit());
    sqlOutput.writeInt(getTorWeekLimit());
    if (Objects.nonNull(getNominationsAutumTerm())) {
      sqlOutput.writeDate(new java.sql.Date(getNominationsAutumTerm().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getNominationsSpringTerm())) {
      sqlOutput.writeDate(new java.sql.Date(getNominationsSpringTerm().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getApplicationAutumTerm())) {
      sqlOutput.writeDate(new java.sql.Date(getApplicationAutumTerm().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getApplicationSpringTerm())) {
      sqlOutput.writeDate(new java.sql.Date(getApplicationSpringTerm().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
  }
}
