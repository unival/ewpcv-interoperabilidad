package es.minsait.ewpcv.aprovisionamiento.impl;


import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgMobilityLaDao;
import es.minsait.ewpcv.aprovisionamiento.model.LearningAgreement;
import es.minsait.ewpcv.aprovisionamiento.model.Mobility;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;
import es.minsait.ewpcv.aprovisionamiento.model.SignatureMobility;

/**
 * The type Pkg mobility la dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgMobilityLaDaoImpl implements IPkgMobilityLaDao {

  private static final String PKG = "PKG_MOBILITY_LA";

  @PersistenceContext
  EntityManager em;

  @Override
  public RespuestaPkg insertLearningAgreement(final LearningAgreement pLearningAgreement, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".INSERT_LEARNING_AGREEMENT(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pLearningAgreement);
        function.registerOutParameter(3, Types.VARCHAR);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateLearningAgreement(final String id, final LearningAgreement pLearningAgreement,
      final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".UPDATE_LEARNING_AGREEMENT(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setObject(3, pLearningAgreement);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteLearningAgreement(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".DELETE_LEARNING_AGREEMENT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg acceptLearningAgreement(final String id, final Integer pLaRevision,
      final SignatureMobility signature, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".ACCEPT_LEARNING_AGREEMENT(?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setInt(3, pLaRevision);
        function.setObject(4, signature);
        function.registerOutParameter(5, Types.VARCHAR);
        function.setString(6, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(5)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg rejectLearningAgreement(final String id, final Integer pLaRevision,
      final SignatureMobility signature, final String pObservation, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".REJECT_LEARNING_AGREEMENT(?,?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setInt(3, pLaRevision);
        function.setObject(4, signature);
        function.setString(5, pObservation);
        function.registerOutParameter(6, Types.VARCHAR);
        function.setString(7, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(6)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg insertMobility(final Mobility pMobilityLa, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_MOBILITY(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pMobilityLa);
        function.registerOutParameter(3, Types.VARCHAR);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateMobility(final String id, final Mobility pMobilityLa, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_MOBILITY(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setObject(3, pMobilityLa);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteMobility(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_MOBILITY(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg cancelMobility(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".CANCEL_MOBILITY(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(3, "");
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg studentDeparture(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".STUDENT_DEPARTURE(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(3, "");
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg studentArrival(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".STUDENT_ARRIVAL(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(3, "");
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg approveNomination(final String id, final String comment, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (
          CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".APPROVE_NOMINATION(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setString(3, comment);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(4, "");
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg rejectNomination(final String id, final String comment, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".REJECT_NOMINATION(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setString(3, comment);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(4, "");
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateActualDates(final String id, final Date actualDeparture, final Date actualArrival,
      final String comment, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function =
          connection.prepareCall("{ ? = call " + PKG + ".UPDATE_ACTUAL_DATES(?,?,?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);

        final java.sql.Date actualDepartureSql =
            Objects.nonNull(actualDeparture) ? new java.sql.Date(actualDeparture.getTime()) : null;

        function.setDate(3, actualDepartureSql);

        final java.sql.Date actualArrivalSql =
            Objects.nonNull(actualArrival) ? new java.sql.Date(actualArrival.getTime()) : null;

        function.setDate(4, actualArrivalSql);
        function.setString(5, comment);
        function.registerOutParameter(6, Types.VARCHAR);
        function.setString(6, "");
        function.setString(7, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(6)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg fixLaApprove(final String id) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".FIX_LA_APPROVE(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(3, "");
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg fixLaReject(final String id) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".FIX_LA_REJECT(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(3, "");
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

}
