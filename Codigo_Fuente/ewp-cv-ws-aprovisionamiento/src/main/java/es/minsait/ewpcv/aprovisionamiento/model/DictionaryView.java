package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Dictionary view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "DICTIONARIES_VIEW")
@Data
public class DictionaryView implements Serializable {

  private static final long serialVersionUID = 7018128906302334869L;

  @Id
  @Column(name = "ID")
  String id;

  @Column(name = "CODE")
  String code;

  @Column(name = "DESCRIPTION")
  String description;

  @Column(name = "DICTIONARY_TYPE")
  String dictionaryType;

  @Column(name = "DICTIONARY_YEAR")
  String callYear;

}
