package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryType;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryView;

/**
 * The interface Dictionary view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IDictionaryViewDao {

  List<DictionaryView> findByDictionaryType(DictionaryType dictionaryType);

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);

}
