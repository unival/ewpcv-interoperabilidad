package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Tor.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Tor implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.TOR";
  private static final long serialVersionUID = -2949897024079554751L;
  private Date generatedDate;
  private List<TorReport> torReportList;
  private List<Attachment> mobilityAttachmentList;
  private byte[] extension;
  private List<IscedTable> iscedTableList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setGeneratedDate(sqlInput.readDate());
    setTorReportList(Util.listFromArray(sqlInput.readArray(), TorReport.class));
    setMobilityAttachmentList(Util.listFromArray(sqlInput.readArray(), Attachment.class));
    setExtension(sqlInput.readBytes());
    setIscedTableList(Util.listFromArray(sqlInput.readArray(), IscedTable.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getGeneratedDate())) {
      sqlOutput.writeDate(new java.sql.Date(getGeneratedDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    Util.writeArrayFromList(sqlOutput, TorReport.SQL_LIST_TYPE, getTorReportList());
    Util.writeArrayFromList(sqlOutput, Attachment.SQL_LIST_MOBILITY_TYPE, getMobilityAttachmentList());
    sqlOutput.writeBytes(getExtension());
    Util.writeArrayFromList(sqlOutput, IscedTable.SQL_LIST_TYPE, getIscedTableList());
  }
}
