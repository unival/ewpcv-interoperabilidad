package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.*;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type La component.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class LaComponent implements SQLData, Serializable {

  public static final int INT_DEFAULT_VALUE = 0;

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.LA_COMPONENT";
  public static final String SQL_LIST_TYPE = "PKG_MOBILITY_LA.LA_COMPONENT_LIST";
  private static final long serialVersionUID = 1L;
  Integer laComponentType;
  String losId;
  String losCode;
  Integer losType;
  String title;
  AcademicTerm academicTerm;
  Credit credit;
  String recognitionConditions;
  String shortDescription;
  Integer status;
  Integer reasonCode;
  String reasonText;
  Date startDate;
  Date endDate;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setLaComponentType(sqlInput.readInt());
    setLosId(sqlInput.readString());
    setLosCode(sqlInput.readString());
    setLosType(sqlInput.readInt());
    setTitle(sqlInput.readString());
    setAcademicTerm((AcademicTerm) sqlInput.readObject());
    setCredit((Credit) sqlInput.readObject());
    setRecognitionConditions(sqlInput.readString());
    setShortDescription(sqlInput.readString());
    setStatus(sqlInput.readInt());
    setReasonCode(sqlInput.readInt());
    setReasonText(sqlInput.readString());
    setStartDate(sqlInput.readDate());
    setEndDate(sqlInput.readDate());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    Util.writeNullPossibleNumber(sqlOutput, getLaComponentType());
    sqlOutput.writeString(getLosId());
    sqlOutput.writeString(getLosCode());
    Util.writeNullPossibleNumber(sqlOutput, getLosType());
    sqlOutput.writeString(getTitle());
    sqlOutput.writeObject(getAcademicTerm());
    sqlOutput.writeObject(getCredit());
    sqlOutput.writeString(getRecognitionConditions());
    sqlOutput.writeString(getShortDescription());
    Util.writeNullPossibleNumber(sqlOutput, getStatus());
    Util.writeNullPossibleNumber(sqlOutput, getReasonCode());
    sqlOutput.writeString(getReasonText());
    if (Objects.nonNull(getStartDate())) {
      sqlOutput.writeDate(new java.sql.Date(getStartDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getEndDate())) {
      sqlOutput.writeDate(new java.sql.Date(getEndDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
  }

}
