package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Academic term.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class AcademicTerm implements SQLData, Serializable {

  public static final String SQL_TYPE = "ACADEMIC_TERM";
  private static final long serialVersionUID = 1L;
  String academicYear;
  String institutionId;
  String organizationUnitCode;
  Date startDate;
  Date endDate;
  List<LanguageItem> description;
  Integer termNumber;
  Integer totalTerms;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setAcademicYear(sqlInput.readString());
    setInstitutionId(sqlInput.readString());
    setOrganizationUnitCode(sqlInput.readString());
    setStartDate(sqlInput.readDate());
    setEndDate(sqlInput.readDate());
    setDescription(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setTermNumber(sqlInput.readInt());
    setTotalTerms(sqlInput.readInt());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getAcademicYear());
    sqlOutput.writeString(getInstitutionId());
    sqlOutput.writeString(getOrganizationUnitCode());
    if (Objects.nonNull(getStartDate())) {
      sqlOutput.writeDate(new java.sql.Date(getStartDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getEndDate())) {
      sqlOutput.writeDate(new java.sql.Date(getEndDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getDescription());
    sqlOutput.writeInt(getTermNumber());
    sqlOutput.writeInt(getTotalTerms());
  }
}
