package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.*;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The type Simple tor report.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class SimpleTorReport implements SQLData, Serializable {
  public static final String SQL_TYPE = "PKG_TORS.SIMPLE_TOR_REPORT";
  public static final String SQL_LIST_TYPE = "PKG_TORS.SIMPLE_TOR_REPORT_LIST";
  private static final long serialVersionUID = -7712971241339292471L;
  private Date issueDate;
  private List<Attachment> torAttachmentList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setIssueDate(sqlInput.readDate());
    setTorAttachmentList(Util.listFromArray(sqlInput.readArray(), Attachment.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getIssueDate())) {
      sqlOutput.writeDate(new java.sql.Date(getIssueDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    Util.writeArrayFromList(sqlOutput, Attachment.SQL_LIST_TOR_TYPE, getTorAttachmentList());
  }
}
