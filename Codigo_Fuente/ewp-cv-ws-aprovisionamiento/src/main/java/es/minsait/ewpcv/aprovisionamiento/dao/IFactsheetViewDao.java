package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The interface Factsheet view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IFactsheetViewDao {

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);
}
