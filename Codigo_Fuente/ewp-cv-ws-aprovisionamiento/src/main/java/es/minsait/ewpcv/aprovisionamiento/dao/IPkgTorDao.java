package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The interface Pkg tor dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IPkgTorDao {

  RespuestaPkg insertTor(Tor pTor, String pLaId, String pHeiToNotify);

  RespuestaPkg insertBasicTor(TorSimpleObject pTor, String pLaId, String pHeiToNotify);

  RespuestaPkg updateTor(String pTorId, Tor pTor, String pHeiToNotify);

  RespuestaPkg updateBasicTor(String pTorId, TorSimpleObject pTor, String pHeiToNotify);

  RespuestaPkg deleteTor(String pTorId, String pHeiToNotify);

  RespuestaPkg deleteBasicTor(String pTorId, String pHeiToNotify);

  RespuestaPkg insertGroupTypeList(List<GroupType> pGroupTypeList);

  RespuestaPkg insertGroupList(List<LoiGroup> pGroupList);

  RespuestaPkg insertGradingScheme(GradingScheme pGradingScheme);
}
