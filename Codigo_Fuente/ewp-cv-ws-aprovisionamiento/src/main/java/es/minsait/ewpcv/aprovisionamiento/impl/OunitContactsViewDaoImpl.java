package es.minsait.ewpcv.aprovisionamiento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IOunitContactsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.OunitContactsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.OunitContactsView;

/**
 * The type Ounit contacts view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class OunitContactsViewDaoImpl implements IOunitContactsViewDao {

  @Autowired
  OunitContactsViewRepository ounitContactsViewRepository;
  @Autowired
  RepositoryUtilsInterface repoUtils;


  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {
    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<OunitContactsView> page = ounitContactsViewRepository.findAll((root, query, criteriaBuilder) -> repoUtils
        .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

  @Override
  public OunitContactsView findByOunitCodeAndInstitutionId(final String ounitCode, final String schac) {
    return ounitContactsViewRepository.findByOunitCodeAndInstitutionId(ounitCode, schac);
  }

  @Override
  public List<OunitContactsView> findByInstitutionId(final String schac) {
    return ounitContactsViewRepository.findByInstitutionId(schac);
  }

  @Override
  public List<OunitContactsView> findAllByOrderByOunitNameAsc() {
    return ounitContactsViewRepository.findAllByOrderByOunitNameAsc();
  }
}
