package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The type Iias view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "IIAS_PDF_VIEW")
@Data
public class IiasPdfView implements Serializable {

  private static final long serialVersionUID = 5905582402052332528L;

  @Id
  @Column(name = "INTERNAL_IIA_ID")
  String iiaId;

  @Column(name = "IIA_ID")
  String interopId;

  @Column(name = "IIA_CODE")
  String iiaCode;

  @Column(name = "REMOTE_IIA_ID")
  String remoteIiaId;

  @Column(name = "REMOTE_IIA_CODE")
  String remoteIiaCode;

  @Column(name = "IS_REMOTE")
  Boolean isRemote;

  @Column(name = "PDF")
  byte[] pdf;

  @Column(name = "MIME_TYPE")
  String mimeType;

}
