package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Ounit view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "OUNIT_VIEW")
@Data
public class OunitView implements Serializable {

  private static final long serialVersionUID = -1665374206169428933L;

  @Id
  @Column(name="INTERNAL_ID")
  private String internalId;

  @Column(name = "OUNIT_ID")
  private String ounitId;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "OUNIT_CODE")
  private String ounitCode;

  @Column(name = "OUNIT_NAME")
  private String ounitName;

  @Column(name = "OUNIT_ABBREVIATION")
  private String ounitAbbreviation;

  @Column(name = "OUNIT_LOGO_URL")
  private String ounitLogo;

  @Column(name = "OUNIT_WEBSITE")
  private String ounitWebsite;

  @Column(name = "OUNIT_FACTSHEET_URL")
  private String ounitFactsheetUrl;

  @Column(name = "COUNTRY")
  private String country;

  @Column(name = "CITY")
  private String ccity;

  @Column(name = "STREET_ADDR_LINES")
  private String streetAddressLines;

  @Column(name = "STREET_ADDRESS")
  private String streetAddress;

  @Column(name = "MAILING_ADDR_LINES")
  private String mailingAddressLines;

  @Column(name = "MAILING_ADDRESS")
  private String mailingAddress;

  @Column(name = "PARENT_OUNIT_ID")
  private String parentOunitId;

  @Column(name = "IS_EXPOSED")
  private Boolean isExposed;
}
