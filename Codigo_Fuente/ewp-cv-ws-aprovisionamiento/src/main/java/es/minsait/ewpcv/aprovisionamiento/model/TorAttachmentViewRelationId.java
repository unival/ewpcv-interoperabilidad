package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Tor attachment view relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class TorAttachmentViewRelationId implements Serializable {

    private static final long serialVersionUID = 1599959314895889653L;

    String laId;
    String loiId;
    String reportId;
    String torId;
}
