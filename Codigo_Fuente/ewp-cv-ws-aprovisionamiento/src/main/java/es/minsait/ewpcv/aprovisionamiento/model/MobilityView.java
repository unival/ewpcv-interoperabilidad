package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Mobility view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "MOBILITY_VIEW")
@Data
@IdClass(MobilityViewRelationId.class)
public class MobilityView implements Serializable {

  private static final long serialVersionUID = 1777896769064994907L;

  @Id
  @Column(name = "INTERNAL_MOBILITY_ID")
  String mobilityId;

  @Column(name = "MOBILITY_ID")
  String id;

  @Column(name = "MOBILITY_REVISION")
  Integer mobilityRevision;

  @Column(name = "RECEIVER_CONTACT_ADDRESS")
  String receiverContactAddress;

  @Column(name = "RECEIVER_CONTACT_MAILING_ADDR")
  String receiverContactMAddress;

  @Column(name = "RECEIVER_CONTACT_POSTAL_CODE")
  String receiverContactPostalCode;

  @Column(name = "RECEIVER_CONTACT_LOCALITY")
  String receiverContactLocality;

  @Column(name = "RECEIVER_CONTACT_REGION")
  String receiverContactRegion;

  @Column(name = "RECEIVER_CONTACT_COUNTRY")
  String receiverContactCountry;

  @Column(name = "ADMV_REC_CONTACT_NAME")
  String admvRecContactName;

  @Column(name = "ADMV_REC_CONTACT_LAST_NAME")
  String admvRecContactLastName;

  @Column(name = "ADMV_REC_CONTACT_BIRTH_DATE")
  Date admvRecContactBirthDate;

  @Column(name = "ADMV_REC_CONTACT_GENDER")
  Integer admvRecContactGender;

  @Column(name = "ADMV_REC_CONTACT_CITIZENSHIP")
  String admvRecContactCitizenship;

  @Column(name = "ADMV_REC_CONT_CONT_NAMES")
  String admvRecContContNames;

  @Column(name = "ADMV_REC_CONT_CONT_DESCS")
  String admvRecContContDescs;

  @Column(name = "ADMV_REC_CONT_CONT_URLS")
  String admvRecContContUrls;

  @Column(name = "ADMV_REC_CONTACT_EMAILS")
  String admvRecContactEmails;

  @Column(name = "ADMV_REC_CONTACT_PHONES")
  String admvRecContactPhones;

  @Column(name = "ADMV_REC_CONTACT_FAXES")
  String admvRecContactFaxes;

  @Column(name = "ADMV_REC_CONT_ADDR_LINES")
  String admvContAddrLines;

  @Column(name = "ADMV_REC_CONTACT_ADDRESS")
  String admvRecContactAddress;

  @Column(name = "ADMV_REC_CONTACT_M_ADDR_LINES")
  String admvContMAddrLines;

  @Column(name = "ADMV_REC_CONTACT_MAILING_ADDR")
  String admvRecContactMAddress;

  @Column(name = "ADMV_REC_CONTACT_POSTAL_CODE")
  String admvRecContactPostalCode;

  @Column(name = "ADMV_REC_CONTACT_LOCALITY")
  String admvRecContactLocality;

  @Column(name = "ADMV_REC_CONTACT_REGION")
  String admvRecContactRegion;

  @Column(name = "ADMV_REC_CONTACT_COUNTRY")
  String admvRecContactCountry;

  @Column(name = "PLANNED_ARRIVAL_DATE")
  Date plannedArrivalDate;

  @Column(name = "ACTUAL_ARRIVAL_DATE")
  Date actualArrivalData;

  @Column(name = "PLANNED_DEPARTURE_DATE")
  Date plannedDepartureDate;

  @Column(name = "ACTUAL_DEPARTURE_DATE")
  Date actualDepartureDate;

  @Column(name = "EQF_LEVEL_DEPARTURE")
  Integer eqfLevelDeparture;

  @Column(name = "EQF_LEVEL_NOMINATION")
  Integer eqfLevelNominarion;

  @Column(name = "IIA_ID")
  String iiaId;

  @Column(name = "RECEIVING_IIA_ID")
  String recevingIiaId;

  @Column(name = "SUBJECT_AREA")
  String subjectArea;

  @Column(name = "MOBILITY_STATUS_OUTGOING")
  Integer mobilityStatusOutgoing;

  @Column(name = "MOBILITY_STATUS_INCOMING")
  Integer mobilityStatusIncoming;

  @Column(name = "MOBILITY_COMMENT")
  String mobilityComment;

  @Column(name = "MOBILITY_TYPE")
  String mobilityType;

  @Column(name = "LANGUAGE_SKILL")
  String languageSkill;

  @Column(name = "STUDENT_GLOBAL_ID")
  String studentGlobalId;

  @Column(name = "STUDENT_NAME")
  String studentName;

  @Column(name = "STUDENT_LAST_NAME")
  String studentLastName;

  @Column(name = "STUDENT_BIRTH_DATE")
  Date studentBirthDate;

  @Column(name = "STUDENT_GENDER")
  Integer studentGender;

  @Column(name = "STUDENT_CITIZENSHIP")
  String studentCitizenship;

  @Column(name = "STUDENT_CONTACT_NAMES")
  String studentContactNames;

  @Column(name = "STUDENT_CONTACT_DESCRIPTIONS")
  String studentContactDescriptions;

  @Column(name = "STUDENT_CONTACT_URLS")
  String studentContactUrls;

  @Column(name = "STUDENT_EMAILS")
  String studentMails;

  @Column(name = "STUDENT_PHONE")
  String studentPhone;

  @Column(name = "STUDENT_ADDRESS_LINES")
  String studentAddressLines;

  @Column(name = "STUDENT_ADDRESS")
  String studentAddress;

  @Column(name = "STUDENT_POSTAL_CODE")
  String studentPostalCode;

  @Column(name = "STUDENT_LOCALITY")
  String studentLocality;

  @Column(name = "STUDENT_REGION")
  String studentRegion;

  @Column(name = "STUDENT_COUNTRY")
  String studentCountry;

  @Column(name = "STUDENT_PHOTO_URLS")
  String studentPhotoUrls;

  @Column(name = "RECEIVING_INSTITUTION")
  String receivingInstitution;

  @Column(name = "RECEIVING_INSTITUTION_NAMES")
  String receivingInstitutionNames;

  @Column(name = "RECEIVING_OUNIT_CODE")
  String receivingOunitCode;

  @Column(name = "RECEIVING_OUNIT_NAMES")
  String receivingOunitNames;

  @Column(name = "SENDING_INSTITUTION")
  String sendingInstitution;

  @Column(name = "SENDING_INSTITUTION_NAMES")
  String sendingInstitutionNames;

  @Column(name = "SENDING_OUNIT_CODE")
  String sendingOunitCode;

  @Column(name = "SENDING_OUNIT_NAMES")
  String sendingOunitNames;

  @Column(name = "SENDER_CONTACT_NAME")
  String senderContactName;

  @Column(name = "SENDER_CONTACT_LAST_NAME")
  String senderContactLastName;

  @Column(name = "SENDER_CONTACT_BIRTH_DATE")
  Date senderContactBirthDate;

  @Column(name = "SENDER_CONTACT_GENDER")
  Integer senderContactGender;

  @Column(name = "SENDER_CONTACT_CITIZENSHIP")
  String senderContactCitizenship;

  @Column(name = "SENDER_CONT_CONT_NAMES")
  String senderContContNames;

  @Column(name = "SENDER_CONT_CONT_DESCS")
  String senderConConDescs;

  @Column(name = "SENDER_CONT_CONT_URLS")
  String senderContContUrls;

  @Column(name = "SENDER_CONTACT_EMAILS")
  String senderContactEmails;

  @Column(name = "SENDER_CONTACT_PHONES")
  String senderContactPhones;

  @Column(name = "SENDER_CONT_ADDR_LINES")
  String senderContAddrLines;

  @Column(name = "SENDER_CONTACT_ADDRESS")
  String senderContactAddress;

  @Column(name = "SENDER_CONTACT_FAXES")
  String senderContactFaxes;

  @Column(name = "SENDER_CONT_M_ADDR_LINES")
  String senderContMAddrLines;

  @Column(name = "SENDER_CONTACT_MAILING_ADDR")
  String senderContactMAddress;

  @Column(name = "SENDER_CONTACT_POSTAL_CODE")
  String senderContactPostalCode;

  @Column(name = "SENDER_CONTACT_LOCALITY")
  String senderContactLocality;

  @Column(name = "SENDER_CONTACT_REGION")
  String senderContactRegion;

  @Column(name = "SENDER_CONTACT_COUNTRY")
  String senderContactCountry;

  @Column(name = "ADMV_SEN_CONTACT_NAME")
  String admvSenContactName;

  @Column(name = "ADMV_SEN_CONTACT_LAST_NAME")
  String admvSenContactLastName;

  @Column(name = "ADMV_SEN_CONTACT_BIRTH_DATE")
  Date admvSenContactBirthDate;

  @Column(name = "ADMV_SEN_CONTACT_GENDER")
  Integer admvSenContactGender;

  @Column(name = "ADMV_SEN_CONTACT_CITIZENSHIP")
  String admvSenContactCitizenship;

  @Column(name = "ADMV_SEN_CONT_CONT_NAMES")
  String admvSenContContNames;

  @Column(name = "ADMV_SEN_CONT_CONT_DESCS")
  String admvSenContContDescs;

  @Column(name = "ADMV_SEN_CONT_CONT_URLS")
  String admvSenContContUrls;

  @Column(name = "ADMV_SEN_CONTACT_EMAILS")
  String admvSenContactEmails;

  @Column(name = "ADMV_SEN_CONTACT_PHONES")
  String admvSenContactPhones;

  @Column(name = "ADMV_SEN_CONT_ADDR_LINES")
  String admvSenContAddrLines;

  @Column(name = "ADMV_SEN_CONTACT_ADDRESS")
  String admvSenContactAddress;

  @Column(name = "ADMV_SEN_CONTACT_FAXES")
  String admvSenContactFaxes;

  @Column(name = "ADMV_SEN_CONT_M_ADDR_LINES")
  String admvSenContMAddrLines;

  @Column(name = "ADMV_SEN_CONTACT_MAILING_ADDR")
  String admvSenContactMAddress;

  @Column(name = "ADMV_SEN_CONTACT_POSTAL_CODE")
  String admvSenContactPostalCode;

  @Column(name = "ADMV_SEN_CONTACT_LOCALITY")
  String admvSenContactLocality;

  @Column(name = "ADMV_SEN_CONTACT_REGION")
  String admvSenContactRegion;

  @Column(name = "ADMV_SEN_CONTACT_COUNTRY")
  String admvSenContactCountry;

  @Column(name = "RECEIVER_CONTACT_NAME")
  String receiverContactName;

  @Column(name = "RECEIVER_CONTACT_LAST_NAME")
  String receiverContactLastName;

  @Column(name = "RECEIVER_CONTACT_BIRTH_DATE")
  Date receiverContactBirthDate;

  @Column(name = "RECEIVER_CONTACT_GENDER")
  Integer receiverContactGender;

  @Column(name = "RECEIVER_CONTACT_CITIZENSHIP")
  String receiverContactCitizenship;

  @Column(name = "RECEIVER_CONT_CONT_NAMES")
  String receiverContContNames;

  @Column(name = "RECEIVER_CONT_CONT_DESCS")
  String receiverContContDescs;

  @Column(name = "RECEIVER_CONT_CONT_URLS")
  String receiverContContUrls;

  @Column(name = "RECEIVER_CONTACT_EMAILS")
  String receiverContactEmails;

  @Column(name = "RECEIVER_CONTACT_PHONES")
  String receiverContactPhones;

  @Column(name = "RECEIVER_CONT_ADDR_LINES")
  String receiverContAddrLines;

  @Column(name = "RECEIVER_CONTACT_FAXES")
  String receiverContactFaxes;

  @Column(name = "RECEIVER_CONT_M_ADDR_LINES")
  String receiverContMAddrLines;

  @Column(name = "ACADEMIC_TERM")
  String academicTerm;

  @Column(name = "ACADEMIC_YEAR")
  String academicYear;

  @Column(name = "MODIFY_DATE")
  Date modifyDate;
}
