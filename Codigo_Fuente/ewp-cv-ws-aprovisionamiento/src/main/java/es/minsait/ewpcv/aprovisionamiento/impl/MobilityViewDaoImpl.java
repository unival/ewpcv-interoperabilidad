package es.minsait.ewpcv.aprovisionamiento.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import es.minsait.ewpcv.aprovisionamiento.dao.IMobilityViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.MobilityViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.LearningAgreementView;
import es.minsait.ewpcv.aprovisionamiento.model.MobilityView;

/**
 * The type Mobility view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class MobilityViewDaoImpl implements IMobilityViewDao {

  MobilityViewRepository repository;

  RepositoryUtilsInterface repoUtils;

  @PersistenceContext
  EntityManager em;

  @Autowired
  public MobilityViewDaoImpl(final MobilityViewRepository repository, final RepositoryUtilsInterface repoUtils) {
    this.repository = repository;
    this.repoUtils = repoUtils;
  }

  @Override
  public List<String> findAllSchacCodeFromSendingInstitutionByReceivingInstitution(final String receivingInstitution) {
    return repository.findAllSchacCodeFromSendingInstitutionByReceivingInstitution(receivingInstitution);
  }

  @Override
  public List<String> findAllSubjectAreaIncludedInLaByReceivingInstitution(final String receivingInstitution) {
    return repository.findAllSubjectAreaIncludedInLaByReceivingInstitution(receivingInstitution);
  }

  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {
    final List<Predicate> predicates = new ArrayList<>();

    final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    final CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery();
    final Root<MobilityView> from = criteriaQuery.from(MobilityView.class);
    final Path<Object> path = from.get("mobilityId");
    final CriteriaQuery<Object> select = criteriaQuery.select(from);

    if (Objects.nonNull(paramFilter.get("mobilityStatus"))) {
      // Subquery 1 (MobilityId)
      final Subquery<LearningAgreementView> subquery1 = criteriaQuery.subquery(LearningAgreementView.class);
      final Root fromLearningAgreementVW = subquery1.from(LearningAgreementView.class);
      subquery1.select(fromLearningAgreementVW.get("mobilityId"));

      // Subquery 2 (Max LaRevision)
      final Subquery<LearningAgreementView> subquery2 = criteriaQuery.subquery(LearningAgreementView.class);
      final Root fromLearningAgreementVW2 = subquery2.from(LearningAgreementView.class);
      subquery2.select(criteriaBuilder.max(fromLearningAgreementVW2.get("laRevision")))
          .where(criteriaBuilder.equal(fromLearningAgreementVW.get("laId"), fromLearningAgreementVW2.get("laId")));

      // Composing Where with two subqueries
      final List<Integer> list = (List<Integer>) paramFilter.get("mobilityStatus");
      subquery1.where(criteriaBuilder.and(fromLearningAgreementVW.get("laStatus").in(list),
          fromLearningAgreementVW.get("laRevision").in(subquery2)));

      // Adding the subquery predicate
      predicates.add(criteriaBuilder.in(path).value(subquery1));
      paramFilter.remove("mobilityStatus");
    } else {
      // Subquery (MobilityId IN LearningAgreementView)
      final Subquery<LearningAgreementView> subquery = criteriaQuery.subquery(LearningAgreementView.class);
      final Root fromLearningAgreementVW = subquery.from(LearningAgreementView.class);
      subquery.select(fromLearningAgreementVW.get("mobilityId"));
      predicates.add(criteriaBuilder.in(path).value(subquery));
    }

    // Adding the paramFilter predicates
    repoUtils.createCriteriaBuilderByParamFilters(criteriaBuilder, from, predicates, paramFilter);
    // Adding the datesFilter predicates
    repoUtils.createCriteriaBuilderByDatesFilter(criteriaBuilder, from, predicates, datesFilter);
    // Adding the staticFilter predicates
    repoUtils.createCriteriaBuilderByParamFilters(criteriaBuilder, from, predicates, staticFilter);

    select.where(predicates.toArray(new Predicate[predicates.size()]));

    final TypedQuery<Object> typedQuery = em.createQuery(select);
    final List<Object> resultList = typedQuery.getResultList();

    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<Object> page = new PageImpl<Object>(resultList, pageable, resultList.size());

    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

  @Override
  public MobilityView findByMobilityIdAndMobilityRevision(final String mobilityId, final Integer mobilityRevision) {
    return repository.findByMobilityIdAndMobilityRevision(mobilityId, mobilityRevision);
  }
}
