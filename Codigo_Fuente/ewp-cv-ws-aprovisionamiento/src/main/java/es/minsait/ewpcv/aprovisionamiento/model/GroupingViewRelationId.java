package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Grouping view relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class GroupingViewRelationId implements Serializable {
    private static final long serialVersionUID = -4776342066992106832L;
    String groupTypeId;
    String groupId;
}
