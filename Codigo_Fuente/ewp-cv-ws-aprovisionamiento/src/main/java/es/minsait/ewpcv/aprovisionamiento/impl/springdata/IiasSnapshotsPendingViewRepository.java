package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import es.minsait.ewpcv.aprovisionamiento.model.IiasSnapshotsPendingView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IiasSnapshotsPendingViewRepository
        extends JpaRepository<IiasSnapshotsPendingView, String>, JpaSpecificationExecutor<IiasSnapshotsPendingView>{

}
