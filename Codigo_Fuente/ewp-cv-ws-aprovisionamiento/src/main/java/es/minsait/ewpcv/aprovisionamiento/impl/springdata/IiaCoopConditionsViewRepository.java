package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import es.minsait.ewpcv.aprovisionamiento.model.IiaCoopConditionsView;

/**
 * The interface Iia coop conditions view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IiaCoopConditionsViewRepository
    extends JpaRepository<IiaCoopConditionsView, String>, JpaSpecificationExecutor<IiaCoopConditionsView> {
}
