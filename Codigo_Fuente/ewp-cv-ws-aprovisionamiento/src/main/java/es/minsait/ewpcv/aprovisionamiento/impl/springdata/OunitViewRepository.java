package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.OunitView;

/**
 * The interface Ounit view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface OunitViewRepository extends JpaRepository<OunitView, String>, JpaSpecificationExecutor<OunitView> {

  @Query(
      value = "SELECT ov FROM OunitView ov WHERE LOWER(ov.ounitCode) = LOWER(:ounitCode) AND LOWER(ov.institutionId) = LOWER(:schac)")
  OunitView findByOunitCodeAndInstitutionId(@Param("ounitCode") String ounitCode, @Param("schac") String schac);

  @Query(value = "SELECT ov FROM OunitView ov WHERE LOWER(ov.institutionId) = LOWER(:schac) ORDER BY ov.ounitName ASC")
  List<OunitView> findByInstitutionId(@Param("schac") String schac);

  List<OunitView> findAllByOrderByOunitNameAsc();

  @Query(value="SELECT ov FROM OunitView ov WHERE LOWER(ov.internalId) = LOWER(:id)")
  OunitView findById(@Param("id") String id);

  @Query(value="SELECT ov FROM OunitView ov WHERE LOWER(ov.ounitId) = LOWER(:id) AND LOWER(ov.institutionId) = LOWER(:schac)")
  OunitView findByIdAndIntitutionSchac(@Param("id") String id, @Param("schac") String schac);
}
