package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Student.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Student implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.STUDENT";
  private static final long serialVersionUID = 1L;

  String globalId;
  ContactPerson contactPerson;
  private List<PhotoUrl> photoUrlList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s) throws SQLException {
    setGlobalId(sqlInput.readString());
    setContactPerson((ContactPerson) sqlInput.readObject());
    setPhotoUrlList(Util.listFromArray(sqlInput.readArray(), PhotoUrl.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getGlobalId());
    sqlOutput.writeObject(getContactPerson());
    Util.writeArrayFromList(sqlOutput, LanguageSkill.SQL_LIST_TYPE, getPhotoUrlList());
  }
}
