package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Learning agreement view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "LEARNING_AGREEMENT_VIEW")
@Data
@IdClass(LearningAgreementViewRelationId.class)
public class LearningAgreementView implements Serializable {

  private static final long serialVersionUID = 3846628280065626447L;

  @Id
  @Column(name = "LA_ID")
  String laId;

  @Id
  @Column(name = "LA_REVISION")
  Integer laRevision;

  @Column(name = "MOBILITY_ID")
  String mobilityId;

  @Column(name = "MOBILITY_REVISION")
  Integer mobilityRevision;

  @Column(name = "LA_STATUS")
  Integer laStatus;

  @Column(name = "LA_CHANGES_ID")
  String laChangesId;

  @Column(name = "COMMENT_REJECT")
  String commentRecject;

  @Column(name = "STUDENT_SIGN")
  String studentSign;

  @Column(name = "STUDENT_SIGNATURE")
  byte[] studentSignature;

  @Column(name = "SENDER_SIGN")
  String senderSign;

  @Column(name = "SENDER_SIGNATURE")
  byte[] senderSignature;

  @Column(name = "RECEIVER_SIGN")
  String receiverSign;

  @Column(name = "RECEIVER_SIGNATURE")
  byte[] receiverSignature;

  @Column(name = "MODIFIED_DATE")
  Date modifiedDate;

  @Column(name = "STUDENT_GLOBAL_ID")
  String studentGlobalId;

  @Column(name = "STUDENT_NAME")
  String studentName;

  @Column(name = "STUDENT_LAST_NAME")
  String studentLastName;

  @Column(name = "STUDENT_BIRTH_DATE")
  Date studentBirthDate;

  @Column(name = "STUDENT_GENDER")
  Integer studentGender;

  @Column(name = "STUDENT_CITIZENSHIP")
  String studentCitizenship;

  @Column(name = "STUDENT_CONTACT_NAMES")
  String studentContactNames;

  @Column(name = "STUDENT_CONTACT_DESCRIPTIONS")
  String studentContactDescriptions;

  @Column(name = "STUDENT_CONTACT_URLS")
  String studentContactUrls;

  @Column(name = "STUDENT_EMAILS")
  String studentEmails;

  @Column(name = "STUDENT_PHONE")
  String studentHome;

  @Column(name = "STUDENT_ADDRESS_LINES")
  String studentAddressLines;

  @Column(name = "STUDENT_ADDRESS")
  String studentAddress;

  @Column(name = "STUDENT_POSTAL_CODE")
  String studentPostalCode;

  @Column(name = "STUDENT_LOCALITY")
  String studentLocality;

  @Column(name = "STUDENT_REGION")
  String studentRegion;

  @Column(name = "STUDENT_COUNTRY")
  String studentCountry;

  @Column(name = "GRADE_CONVERSION_TABLES")
  String gradeConversionTables;

}
