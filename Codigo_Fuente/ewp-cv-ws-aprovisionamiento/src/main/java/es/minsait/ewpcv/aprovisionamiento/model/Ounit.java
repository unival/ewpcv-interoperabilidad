package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Ounit.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Ounit implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_OUNITS.OUNIT";
  private static final long serialVersionUID = 1L;
  private String heiId;
  private String ounitCode;
  private List<LanguageItem> ounitName;
  private String abbreviation;
  private Contact primaryContact;
  private List<ContactPerson> secondContactList;
  private List<LanguageItem> factSheetUrlList;
  private String logoUrl;
  private Boolean isTreeStructure;
  private String parentOunitId;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setHeiId(sqlInput.readString());
    setOunitCode(sqlInput.readString());
    setOunitName(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setAbbreviation(sqlInput.readString());
    setPrimaryContact((Contact) sqlInput.readObject());
    setSecondContactList(Util.listFromArray(sqlInput.readArray(), ContactPerson.class));
    setFactSheetUrlList(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setLogoUrl(sqlInput.readString());
    setIsTreeStructure(sqlInput.readBoolean());
    setParentOunitId(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getHeiId());
    sqlOutput.writeString(getOunitCode());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getOunitName());
    sqlOutput.writeString(getAbbreviation());
    sqlOutput.writeObject(getPrimaryContact());
    Util.writeArrayFromList(sqlOutput, ContactPerson.SQL_LIST_TYPE, getSecondContactList());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getFactSheetUrlList());
    sqlOutput.writeString(getLogoUrl());
    sqlOutput.writeBoolean(Objects.isNull(getIsTreeStructure()) ? Boolean.FALSE : getIsTreeStructure());
    sqlOutput.writeString(getParentOunitId());
  }
}
