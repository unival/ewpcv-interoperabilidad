package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * The type Update requests view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "UPDATE_REQUESTS_VIEW")
@Data
public class UpdateRequestsView implements Serializable {

   private static final long serialVersionUID = 153405203774148099L;

   @Id
   @Column(name = "ID")
   String id;
   
   @Column(name = "VERSION")
   Integer version;

   @Column(name = "LA_ID")
   String laId;

   @Column(name = "LA_REVISION")
   String laRevision;
   
   @Column(name = "SENDING_HEI_ID")
   String sendingHeiId;

   @Column(name = "TYPE")
   Integer type;

   @Column(name = "UPDATE_REQUEST_DATE")
   Date updateRequestDate;

   @Column(name = "IS_PROCESSING")
   boolean isProcessing;

   @Column(name = "MOBILITY_TYPE")
   Integer mobilityType;

   @Column(name = "UPDATE_INFORMATION")
   byte[] updateInformation;

   @Column(name = "RETRIES")
   Integer retries;

   @Column(name = "PROCESSING_DATE")
   Date processingDate;

   @Column(name = "SHOULD_RETRY")
   boolean shouldRetry;

   @Column(name = "LAST_ERROR")
   String lastError;
    
}
