package es.minsait.ewpcv.aprovisionamiento.impl;


import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgIiasDao;
import es.minsait.ewpcv.aprovisionamiento.model.Iia;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Pkg iias dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgIiasDaoImpl implements IPkgIiasDao {

  private static final String PKG = "PKG_IIAS";

  @PersistenceContext
  EntityManager em;


  @Override
  public RespuestaPkg insertIias(final Iia pIias, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_IIA(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pIias);
        function.registerOutParameter(3, Types.VARCHAR);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateIias(final String id, final Iia pIias, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_IIA(?,?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setObject(3, pIias);
        function.registerOutParameter(4, Types.VARCHAR);
        function.setString(5, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteIias(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_IIA(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg bindIias(final String ownId, final String remoteId) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".BIND_IIA(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, ownId);
        function.setString(3, remoteId);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg unbindIias(final String ownId) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UNBIND_IIA(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, ownId);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg approveIias(final String internalId, final String heiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".APPROVE_IIA(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, internalId);
        function.setString(3, heiToNotify);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg prepareSnapshots() {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".PREPARE_SNAPSHOTS(?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.registerOutParameter(2, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(2)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg recoverIiaSnapshot(final String id) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".RECOVER_IIA_SNAPSHOT(?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }


  @Override
  public RespuestaPkg terminateIia(final String id, final String pHeiToNotify) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".TERMINATE_IIA(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.registerOutParameter(3, Types.VARCHAR);
        function.setString(4, pHeiToNotify);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
      }
    });
    return result.get();
  }

}
