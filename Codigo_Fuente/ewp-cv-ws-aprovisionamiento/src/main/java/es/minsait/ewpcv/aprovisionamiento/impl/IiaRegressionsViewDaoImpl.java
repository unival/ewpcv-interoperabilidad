package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IIaRegressionsViewDao;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.IiaRegressionsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.IiaRegressionsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.minsait.ewpcv.aprovisionamiento.dao.IEventViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.EventViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.ElementTypes;
import es.minsait.ewpcv.aprovisionamiento.model.EventView;

/**
 * The type Event view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class IiaRegressionsViewDaoImpl implements IIaRegressionsViewDao {

  @Autowired
  IiaRegressionsViewRepository repository;
  @Autowired
  RepositoryUtilsInterface repoUtils;

  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {

    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<IiaRegressionsView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
        .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }

}
