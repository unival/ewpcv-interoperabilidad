package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.DeletedElementsViewDao;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.DeletedElementsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.DeletedElementsView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaContactsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dao.RepositoryUtilsInterface;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.IIiaContactsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.IiaContactsView;

/**
 * The type Iia contacts view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Repository
public class DeletedElementsViewDaoImpl implements DeletedElementsViewDao {

  @Autowired
  DeletedElementsViewRepository repository;
  @Autowired
  RepositoryUtilsInterface repoUtils;


  @Override
  public Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, final String defaultOrder) {
    final Pageable pageable = repoUtils.createPageable(filtro, defaultOrder);

    final Page<DeletedElementsView> page = repository.findAll((root, query, criteriaBuilder) -> repoUtils
        .buildAndExecuteCriteriaBuilder(criteriaBuilder, root, paramFilter, datesFilter, staticFilter), pageable);
    final Map<Object, Object> results = new HashMap<>();
    results.put("results", page.getContent());
    results.put("total", page.getTotalElements());

    return results;
  }
}
