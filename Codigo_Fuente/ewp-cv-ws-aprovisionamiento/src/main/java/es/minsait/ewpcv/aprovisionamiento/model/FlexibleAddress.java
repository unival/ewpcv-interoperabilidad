package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Flexible address.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class FlexibleAddress implements SQLData, Serializable {

  public static final String SQL_TYPE = "FLEXIBLE_ADDRESS";
  private static final long serialVersionUID = 1L;
  private static final String SQL_LIST_RECIPENT = "RECIPIENT_NAME_LIST";
  private static final String SQL_LIST_ADDRESS = "ADDRESS_LINE_LIST";
  private static final String SQL_LIST_DELIVERY = "DELIVERY_POINT_CODE_LIST";

  List<String> recipentNames;
  List<String> addressLines;
  String buildingNumber;
  String buildingName;
  String streetName;
  String unit;
  String floor;
  String postOfficeBox;
  List<String> deliveryPointCodes;
  String postalCode;
  String locality;
  String region;
  String country;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setRecipentNames(Util.listFromArray(sqlInput.readArray(), String.class));
    setAddressLines(Util.listFromArray(sqlInput.readArray(), String.class));
    setBuildingNumber(sqlInput.readString());
    setBuildingName(sqlInput.readString());
    setStreetName(sqlInput.readString());
    setUnit(sqlInput.readString());
    setFloor(sqlInput.readString());
    setPostOfficeBox(sqlInput.readString());
    setDeliveryPointCodes(Util.listFromArray(sqlInput.readArray(), String.class));
    setPostalCode(sqlInput.readString());
    setLocality(sqlInput.readString());
    setRegion(sqlInput.readString());
    setCountry(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    Util.writeArrayFromList(sqlOutput, SQL_LIST_RECIPENT, getRecipentNames());
    Util.writeArrayFromList(sqlOutput, SQL_LIST_ADDRESS, getAddressLines());
    sqlOutput.writeString(getBuildingNumber());
    sqlOutput.writeString(getBuildingName());
    sqlOutput.writeString(getStreetName());
    sqlOutput.writeString(getUnit());
    sqlOutput.writeString(getFloor());
    sqlOutput.writeString(getPostOfficeBox());
    Util.writeArrayFromList(sqlOutput, SQL_LIST_DELIVERY, getDeliveryPointCodes());
    sqlOutput.writeString(getPostalCode());
    sqlOutput.writeString(getLocality());
    sqlOutput.writeString(getRegion());
    sqlOutput.writeString(getCountry());
  }
}
