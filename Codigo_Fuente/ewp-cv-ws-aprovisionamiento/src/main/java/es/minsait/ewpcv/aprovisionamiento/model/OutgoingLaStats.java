package es.minsait.ewpcv.aprovisionamiento.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "OUTGOING_LA_STATS")
@Data
public class OutgoingLaStats implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private OutgoingLaStatsId id;
}
