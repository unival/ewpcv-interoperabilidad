package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.OunitContactsView;

/**
 * The interface Ounit contacts view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface OunitContactsViewRepository
    extends JpaRepository<OunitContactsView, String>, JpaSpecificationExecutor<OunitContactsView> {

  @Query(
      value = "SELECT ocv FROM OunitContactsView ocv WHERE LOWER(ocv.ounitCode) = LOWER(:ounitCode) AND LOWER(ocv.institutionId) = LOWER(:schac)")
  OunitContactsView findByOunitCodeAndInstitutionId(@Param("ounitCode") String ounitCode, @Param("schac") String schac);

  @Query(
      value = "SELECT ocv FROM OunitContactsView ocv WHERE LOWER(ocv.institutionId) = LOWER(:schac) ORDER BY ocv.ounitName ASC")
  List<OunitContactsView> findByInstitutionId(@Param("schac") String schac);

  List<OunitContactsView> findAllByOrderByOunitNameAsc();
}
