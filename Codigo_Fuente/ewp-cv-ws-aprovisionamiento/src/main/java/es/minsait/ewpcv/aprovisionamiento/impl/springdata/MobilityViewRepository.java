package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.MobilityView;

/**
 * The interface Mobility view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface MobilityViewRepository
    extends JpaRepository<MobilityView, String>, JpaSpecificationExecutor<MobilityView> {

  MobilityView findByMobilityIdAndMobilityRevision(String mobilityId, Integer mobilityRevision);

  @Query("SELECT DISTINCT(mv.sendingInstitution) "
      + " FROM MobilityView mv WHERE UPPER(mv.receivingInstitution) = UPPER(:receivingInstitution)")
  List<String> findAllSchacCodeFromSendingInstitutionByReceivingInstitution(
      @Param("receivingInstitution") String receivingInstitution);

  @Query("SELECT DISTINCT(mv.subjectArea) "
      + " FROM MobilityView mv WHERE UPPER(mv.receivingInstitution) = UPPER(:receivingInstitution) AND mv.mobilityId IN("
      + "SELECT lav.mobilityId FROM LearningAgreementView lav)")
  List<String> findAllSubjectAreaIncludedInLaByReceivingInstitution(
      @Param("receivingInstitution") String receivingInstitution);
}
