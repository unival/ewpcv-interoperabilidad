package es.minsait.ewpcv.aprovisionamiento.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OutgoingLaStatsId implements Serializable {

    @Column(name = "SENDING_INSTITUTION")
    String sendingInstitution;

    @Column(name = "START_YEAR")
    String startYear;

    @Column(name = "END_YEAR")
    String endYear;

    @Column(name = "TOTAL")
    Integer total;

    @Column(name = "LAST_PENDING")
    Integer lastPending;

    @Column(name = "LAST_APPROVED")
    Integer lastApproved;

    @Column(name = "LAST_REJECTED")
    Integer lastRejected;

    @Column(name = "APPROVAL_MODIFIED")
    Integer approvalModified;

    @Column(name = "APPROVAL_UNMODIFIED")
    Integer approvalUnmodified;

    @Column(name = "SOME_VERSION_APPROVED")
    Integer someVersionApproved;
}
