package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.Objects;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Student la.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class StudentLa implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.STUDENT_LA";
  private static final long serialVersionUID = 1L;
  String givenName;
  String familyName;
  Date birthName;
  String citizenShip;
  Integer gender;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setGivenName(sqlInput.readString());
    setFamilyName(sqlInput.readString());
    setBirthName(sqlInput.readDate());
    setCitizenShip(sqlInput.readString());
    setGender(sqlInput.readInt());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getGivenName());
    sqlOutput.writeString(getFamilyName());
    if (Objects.nonNull(getBirthName())) {
      sqlOutput.writeDate(new java.sql.Date(getBirthName().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeString(getCitizenShip());
    sqlOutput.writeInt(getGender());
  }
}
