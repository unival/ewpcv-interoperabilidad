package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.DictionaryView;

/**
 * The interface Dictionary view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface DictionaryViewRepository
    extends JpaRepository<DictionaryView, Integer>, JpaSpecificationExecutor<DictionaryView> {

  List<DictionaryView> findByDictionaryType(String dictionaryType);

}
