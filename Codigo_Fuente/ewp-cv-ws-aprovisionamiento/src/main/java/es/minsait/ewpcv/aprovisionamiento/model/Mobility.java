package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Mobility.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Mobility implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_MOBILITY_LA.MOBILITY";
  private static final long serialVersionUID = 1L;
  InstitutionBasic sendingInstitutionBasic;
  InstitutionBasic receivingInstitutionBasic;
  Date actualArrivalDate;
  Date actualDepatureDate;
  Date planedArrivalDate;
  Date planedDepatureDate;
  String iiaId;
  String receivingIiaId;
  String cooperationConditionId;
  Student student;
  SubjectArea subjectArea;
  private List<LanguageSkill> languageSkillList;
  ContactPerson senderContact;
  ContactPerson senderAdmvContact;
  ContactPerson receiverContact;
  ContactPerson receiverAdmvContact;
  String mobilityType;
  Integer eqfLevelNomination;
  Integer eqfLevelDeparture;
  AcademicTerm academicTerm;
  Integer statusOutgoing;


  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String s)
      throws SQLException {
    setSendingInstitutionBasic((InstitutionBasic) sqlInput.readObject());
    setReceivingInstitutionBasic((InstitutionBasic) sqlInput.readObject());
    setActualArrivalDate(sqlInput.readDate());
    setActualDepatureDate(sqlInput.readDate());
    setPlanedArrivalDate(sqlInput.readDate());
    setPlanedDepatureDate(sqlInput.readDate());
    setIiaId(sqlInput.readString());
    setReceivingIiaId(sqlInput.readString());
    setCooperationConditionId(sqlInput.readString());
    setStudent((Student) sqlInput.readObject());
    setSubjectArea((SubjectArea) sqlInput.readObject());
    setLanguageSkillList(Util.listFromArray(sqlInput.readArray(), LanguageSkill.class));
    setSenderContact((ContactPerson) sqlInput.readObject());
    setSenderAdmvContact((ContactPerson) sqlInput.readObject());
    setReceiverContact((ContactPerson) sqlInput.readObject());
    setReceiverAdmvContact((ContactPerson) sqlInput.readObject());
    setMobilityType(sqlInput.readString());
    setEqfLevelNomination(sqlInput.readInt());
    setEqfLevelDeparture(sqlInput.readInt());
    setAcademicTerm((AcademicTerm) sqlInput.readObject());
    setStatusOutgoing(sqlInput.readInt());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeObject(getSendingInstitutionBasic());
    sqlOutput.writeObject(getReceivingInstitutionBasic());
    if (Objects.nonNull(getActualArrivalDate())) {
      sqlOutput.writeDate(new java.sql.Date(getActualArrivalDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getActualDepatureDate())) {
      sqlOutput.writeDate(new java.sql.Date(getActualDepatureDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getPlanedArrivalDate())) {
      sqlOutput.writeDate(new java.sql.Date(getPlanedArrivalDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    if (Objects.nonNull(getPlanedDepatureDate())) {
      sqlOutput.writeDate(new java.sql.Date(getPlanedDepatureDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeString(getIiaId());
    sqlOutput.writeString(getReceivingIiaId());
    sqlOutput.writeString(getCooperationConditionId());
    sqlOutput.writeObject(getStudent());
    sqlOutput.writeObject(getSubjectArea());
    Util.writeArrayFromList(sqlOutput, LanguageSkill.SQL_LIST_TYPE, getLanguageSkillList());
    sqlOutput.writeObject(getSenderContact());
    sqlOutput.writeObject(getSenderAdmvContact());
    sqlOutput.writeObject(getReceiverContact());
    sqlOutput.writeObject(getReceiverAdmvContact());
    sqlOutput.writeString(getMobilityType());
    sqlOutput.writeInt(getEqfLevelNomination());
    sqlOutput.writeInt(getEqfLevelDeparture());
    sqlOutput.writeObject(getAcademicTerm());
    sqlOutput.writeInt(getStatusOutgoing());
  }
}
