package es.minsait.ewpcv.aprovisionamiento.dao;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.model.OunitView;

/**
 * The interface Ounit view dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOunitViewDao {

  Map<Object, Object> findByFilter(final PageFilterDTO filtro, final Map<String, Object> paramFilter,
      final Map<String, Object> datesFilter, final Map<String, Object> staticFilter, String defaultOrder);

  OunitView findByOunitCodeAndInstitutionId(String ounitCode, String schac);

  List<OunitView> findByInstitutionId(String schac);

  List<OunitView> findAllByOrderByOunitNameAsc();

  OunitView findById(String id);
}
