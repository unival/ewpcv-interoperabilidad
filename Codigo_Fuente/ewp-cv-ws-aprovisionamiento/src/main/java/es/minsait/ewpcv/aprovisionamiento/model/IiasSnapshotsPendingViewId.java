package es.minsait.ewpcv.aprovisionamiento.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class IiasSnapshotsPendingViewId implements Serializable {

    @Column(name = "LOCAL_ID")
    String localId;

    @Column(name = "LOCAL_HEI")
    String localHei;

    @Column(name = "LOCAL_HASH")
    String localHash;

    @Column(name = "LOCAL_APPROVAL_HASH")
    String localApprovalHash;

    @Column(name = "PARTNER_ID")
    String partnerId;

    @Column(name = "PARTNER_HEI")
    String partnerHei;

    @Column(name = "PARTNER_HASH")
    String partnerHash;

    @Column(name = "PARTNER_APPROVAL_HASH")
    String partnerApprovalHash;
}
