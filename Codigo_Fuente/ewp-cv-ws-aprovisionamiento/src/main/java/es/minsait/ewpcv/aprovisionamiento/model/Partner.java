package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Partner.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Partner implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_IIAS.PARTNER";
  private static final long serialVersionUID = 1L;
  InstitutionBasic institutionBasic;
  Date signingDate;
  ContactPerson contactPerson;
  List<ContactPerson> contactPersonList;


  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setInstitutionBasic((InstitutionBasic) sqlInput.readObject());
    setSigningDate(sqlInput.readDate());
    setContactPerson((ContactPerson) sqlInput.readObject());
    setContactPersonList(Util.listFromArray(sqlInput.readArray(), ContactPerson.class));
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeObject(getInstitutionBasic());
    if (Objects.nonNull(getSigningDate())) {
      sqlOutput.writeDate(new java.sql.Date(getSigningDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    sqlOutput.writeObject(getContactPerson());
    Util.writeArrayFromList(sqlOutput, ContactPerson.SQL_LIST_TYPE, getContactPersonList());
  }
}
