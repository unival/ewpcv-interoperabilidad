package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

/**
 * The type DeletedElement view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "DELETED_ELEMENTS_VIEW")
@Data
public class DeletedElementsView implements Serializable {

  private static final long serialVersionUID = 7018128906302334869L;

  @Id
  @Column(name = "ID")
  String id;
  @Column(name = "ELEMENT_ID")
  private String elementId;

  @Column(name = "ELEMENT_TYPE")
  private int elementType;

  @Column(name = "OWNER_SCHAC")
  private String ownerSchac;

  @Column(name = "DELETION_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date deletionDate;


}
