package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Institution basic.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class InstitutionBasic implements SQLData, Serializable {

  public static final String SQL_TYPE = "INSTITUTION";
  private static final long serialVersionUID = 1L;
  String institutionID;
  List<LanguageItem> institutionName;
  List<LanguageItem> organizationUnitName;
  String organizationUnitCode;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setInstitutionID(sqlInput.readString());
    setInstitutionName(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setOrganizationUnitName(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setOrganizationUnitCode(sqlInput.readString());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getInstitutionID());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getInstitutionName());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getOrganizationUnitName());
    sqlOutput.writeString(getOrganizationUnitCode());
  }
}
