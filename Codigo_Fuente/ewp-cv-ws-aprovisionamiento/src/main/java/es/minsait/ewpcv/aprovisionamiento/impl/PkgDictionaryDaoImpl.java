package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgDictionaryDao;
import es.minsait.ewpcv.aprovisionamiento.model.Dictionary;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

@Repository
//@Transactional
public class PkgDictionaryDaoImpl implements IPkgDictionaryDao {

    private static final String PKG = "PKG_DICTIONARY";

    @PersistenceContext
    EntityManager em;

    @Override
    public RespuestaPkg insertDictionary(final Dictionary pDictionary) {
        final Session s = em.unwrap(Session.class);

        final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
        s.doWork(connection -> {
            try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_DICTIONARY(?,?,?) }")) {
                function.registerOutParameter(1, Types.INTEGER);
                function.setObject(2, pDictionary);
                function.registerOutParameter(3, Types.VARCHAR);
                function.registerOutParameter(4, Types.VARCHAR);
                function.execute();
                result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
            }
        });
        return result.get();
    }

    @Override
    public RespuestaPkg updateDictionary(final String id, final Dictionary pDictionary) {
        final Session s = em.unwrap(Session.class);
        final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
        s.doWork(connection -> {
            try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_DICTIONARY(?,?,?) }")) {
                function.registerOutParameter(1, Types.INTEGER);
                function.setString(2, id);
                function.setObject(3, pDictionary);
                function.registerOutParameter(4, Types.VARCHAR);
                function.execute();
                result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
            }
        });
        return result.get();
    }

    @Override
    public RespuestaPkg deleteDictionary(final String id) {
        final Session s = em.unwrap(Session.class);
        final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
        s.doWork(connection -> {
            try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_DICTIONARY(?,?) }")) {
                function.registerOutParameter(1, Types.INTEGER);
                function.setString(2, id);
                function.registerOutParameter(3, Types.VARCHAR);
                function.execute();
                result.set(new RespuestaPkg(function.getInt(1), null, function.getString(3)));
            }
        });
        return result.get();
    }
}
