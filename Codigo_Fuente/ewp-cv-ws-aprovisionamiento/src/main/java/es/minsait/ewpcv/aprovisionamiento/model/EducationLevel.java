package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Education level.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class EducationLevel implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_TORS.EDUCATION_LEVEL";
  public static final String SQL_LIST_TYPE = "PKG_TORS.EDUCATION_LEVEL_LIST";
  private static final long serialVersionUID = 5270189825433569561L;

  private Integer levelType;
  private List<LanguageItem> description;
  private String levelValue;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setLevelType(sqlInput.readInt());
    setDescription(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setLevelValue(sqlInput.readString());
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeInt(getLevelType());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getDescription());
    sqlOutput.writeString(getLevelValue());
  }
}
