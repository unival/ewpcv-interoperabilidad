package es.minsait.ewpcv.aprovisionamiento.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.model.FactsheetAddInfoView;

/**
 * The interface Factsheet add info view repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface FactsheetAddInfoViewRepository
    extends JpaRepository<FactsheetAddInfoView, String>, JpaSpecificationExecutor<FactsheetAddInfoView> {

  List<FactsheetAddInfoView> findByInstitutionId(String institutionId);

}
