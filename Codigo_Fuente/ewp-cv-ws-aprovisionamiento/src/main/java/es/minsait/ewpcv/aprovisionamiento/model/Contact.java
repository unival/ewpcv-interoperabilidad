package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Contact.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class Contact implements SQLData, Serializable {

  public static final String SQL_TYPE = "CONTACT";
  private static final long serialVersionUID = 1L;
  private static final String SQL_LIST_EMAIL = "EMAIL_LIST";

  List<LanguageItem> contactNameList;
  List<LanguageItem> contactDescriptionList;
  List<LanguageItem> contactUrlList;
  String contactRole;
  String institutionId;
  String organizationUnitCode;
  List<String> emailList;
  FlexibleAddress streetAddress;
  FlexibleAddress mailingAddress;
  PhoneNumber phoneNumber;
  PhoneNumber faxNumber;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setContactNameList(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setContactDescriptionList(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setContactUrlList(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
    setContactRole(sqlInput.readString());
    setInstitutionId(sqlInput.readString());
    setOrganizationUnitCode(sqlInput.readString());
    setEmailList(Util.listFromArray(sqlInput.readArray(), String.class));
    setStreetAddress((FlexibleAddress) sqlInput.readObject());
    setMailingAddress((FlexibleAddress) sqlInput.readObject());
    setPhoneNumber((PhoneNumber) sqlInput.readObject());
    setFaxNumber((PhoneNumber) sqlInput.readObject());
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getContactNameList());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getContactDescriptionList());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getContactUrlList());
    sqlOutput.writeString(getContactRole());
    sqlOutput.writeString(getInstitutionId());
    sqlOutput.writeString(getOrganizationUnitCode());
    Util.writeArrayFromList(sqlOutput, SQL_LIST_EMAIL, getEmailList());
    sqlOutput.writeObject(getStreetAddress());
    sqlOutput.writeObject(getMailingAddress());
    sqlOutput.writeObject(getPhoneNumber());
    sqlOutput.writeObject(getFaxNumber());
  }
}
