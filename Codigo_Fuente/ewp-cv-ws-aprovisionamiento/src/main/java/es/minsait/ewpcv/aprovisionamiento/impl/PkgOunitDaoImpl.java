package es.minsait.ewpcv.aprovisionamiento.impl;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgOunitDao;
import es.minsait.ewpcv.aprovisionamiento.model.Ounit;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The type Pkg ounit dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
// TODO descomentar transaccionalidad para test
// @Transactional
public class PkgOunitDaoImpl implements IPkgOunitDao {

  private static final String PKG = "PKG_OUNITS";

  @PersistenceContext
  EntityManager em;

  @Override
  public RespuestaPkg insertOunit(final Ounit pOunit) {
    final Session s = em.unwrap(Session.class);

    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".INSERT_OUNIT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setObject(2, pOunit);
        function.registerOutParameter(3, Types.VARCHAR);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), function.getString(3), function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg updateOunit(final String id, final Ounit pOunit) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".UPDATE_OUNIT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setObject(3, pOunit);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg deleteOunit(final String id, final String institutionSchac) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".DELETE_OUNIT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setString(3, institutionSchac);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg ocultarOunit(final String id, final String institutionSchac) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".OCULTAR_OUNIT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setString(3, institutionSchac);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }

  @Override
  public RespuestaPkg exponerOunit(final String id, final String institutionSchac) {
    final Session s = em.unwrap(Session.class);
    final AtomicReference<RespuestaPkg> result = new AtomicReference<>();
    s.doWork(connection -> {
      try (CallableStatement function = connection.prepareCall("{ ? = call " + PKG + ".EXPONER_OUNIT(?,?,?) }")) {
        function.registerOutParameter(1, Types.INTEGER);
        function.setString(2, id);
        function.setString(3, institutionSchac);
        function.registerOutParameter(4, Types.VARCHAR);
        function.execute();
        result.set(new RespuestaPkg(function.getInt(1), null, function.getString(4)));
      }
    });
    return result.get();
  }
}
