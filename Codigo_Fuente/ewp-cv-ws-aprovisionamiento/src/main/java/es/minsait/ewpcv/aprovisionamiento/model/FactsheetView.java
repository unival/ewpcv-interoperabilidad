package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Factsheet view.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "FACTSHEET_VIEW")
@Data
public class FactsheetView implements Serializable {

  private static final long serialVersionUID = 1151989257372223389L;

  @Id
  @Column(name = "INSTITUTION_ID")
  String institutionId;

  @Column(name = "ABREVIATION")
  String abreviation;

  @Column(name = "DECISION_WEEK_LIMIT")
  Integer decisionWeekLimit;

  @Column(name = "TOR_WEEK_LIMIT")
  Integer torWeekLimit;

  @Column(name = "NOMINATIONS_AUTUM_TERM")
  String nominationsAutumnTerm;

  @Column(name = "NOMINATIONS_SPRING_TERM")
  String nominationsSpringTerm;

  @Column(name = "APPLICATION_AUTUM_TERM")
  String applicationAutumnTerm;

  @Column(name = "APPLICATION_SPRING_TERM")
  String applicationSpringTerm;

  @Column(name = "APPLICATION_EMAIL")
  String applicationEmail;

  @Column(name = "APPLICATION_PHONE")
  String applicationPhone;

  @Column(name = "APPLICATION_URLS")
  String applicationUrls;

  @Column(name = "HOUSING_EMAIL")
  String housingEmail;

  @Column(name = "HOUSING_PHONE")
  String housingPhone;

  @Column(name = "HOUSING_URLS")
  String housingUrls;

  @Column(name = "VISA_EMAIL")
  String visaEmail;

  @Column(name = "VISA_PHONE")
  String visaPhone;

  @Column(name = "VISA_URLS")
  String visaUrls;

  @Column(name = "INSURANCE_EMAIL")
  String insuranceEmail;

  @Column(name = "INSURANCE_PHONE")
  String insurancePhone;

  @Column(name = "INSURANCE_URLS")
  String insuranceUrls;

}
