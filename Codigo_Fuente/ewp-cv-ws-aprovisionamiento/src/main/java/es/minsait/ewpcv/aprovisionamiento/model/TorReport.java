package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The type Tor report.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class TorReport implements SQLData, Serializable {
  public static final String SQL_TYPE = "PKG_TORS.TOR_REPORT";
  public static final String SQL_LIST_TYPE = "PKG_TORS.TOR_REPORT_LIST";
  private static final long serialVersionUID = -7712971241339292471L;
  private Date issueDate;
  private List<Loi> loiList;
  private List<Attachment> torAttachmentList;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(final SQLInput sqlInput, final String typeName) throws SQLException {
    setIssueDate(sqlInput.readDate());
    setLoiList(Util.listFromArray(sqlInput.readArray(), Loi.class));
    setTorAttachmentList(Util.listFromArray(sqlInput.readArray(), Attachment.class));
  }

  @Override
  public void writeSQL(final SQLOutput sqlOutput) throws SQLException {
    if (Objects.nonNull(getIssueDate())) {
      sqlOutput.writeDate(new java.sql.Date(getIssueDate().getTime()));
    } else {
      sqlOutput.writeDate(null);
    }
    Util.writeArrayFromList(sqlOutput, Loi.SQL_LIST_TYPE, getLoiList());
    Util.writeArrayFromList(sqlOutput, Attachment.SQL_LIST_TOR_TYPE, getTorAttachmentList());
  }
}
