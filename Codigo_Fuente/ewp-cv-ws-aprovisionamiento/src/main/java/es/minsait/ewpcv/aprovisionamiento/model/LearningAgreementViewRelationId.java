package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Learning agreement view relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LearningAgreementViewRelationId implements Serializable {
  private static final long serialVersionUID = -3036405531343185850L;
  String laId;
  Integer laRevision;
}
