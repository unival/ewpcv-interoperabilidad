package es.minsait.ewpcv.aprovisionamiento.model;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Information item.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Getter
@Setter
@NoArgsConstructor
public class InformationItem implements SQLData, Serializable {

  public static final String SQL_TYPE = "PKG_FACTSHEET.INFORMATION_ITEM";
  private static final long serialVersionUID = 1L;
  String email;
  PhoneNumber phone;
  List<LanguageItem> urls;

  @Override
  public String getSQLTypeName() throws SQLException {
    return SQL_TYPE;
  }

  @Override
  public void readSQL(SQLInput sqlInput, String s) throws SQLException {
    setEmail(sqlInput.readString());
    setPhone((PhoneNumber) sqlInput.readObject());
    setUrls(Util.listFromArray(sqlInput.readArray(), LanguageItem.class));
  }

  @Override
  public void writeSQL(SQLOutput sqlOutput) throws SQLException {
    sqlOutput.writeString(getEmail());
    sqlOutput.writeObject(getPhone());
    Util.writeArrayFromList(sqlOutput, LanguageItem.SQL_LIST_TYPE, getUrls());
  }
}
