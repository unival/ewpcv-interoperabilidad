package es.minsait.ewpcv.aprovisionamiento.impl;

import org.assertj.core.util.Lists;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IMobilityViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type Mobility test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class MobilityTest {

  @Autowired
  IMobilityViewDao dao;

  @Test
  public void findByFilter() {
    final PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramsWithLogicalFilter = new HashMap<>();

    final Map<String, Object> paramFilter = new HashMap<>();
    // paramFilter.put("mobilityStatus", Arrays.asList("3"));

    final List<Integer> nums = Lists.newArrayList(0, 1);
    // paramsWithLogicalFilter.put("mobilityStatus", nums);


    // paramsWithLogicalFilter.put("OR", paramFilter);

    final Map<String, Object> staticFilter = new HashMap<>();
    staticFilter.put("receivingInstitution", "UV.ES");
    dao.findByFilter(filter, paramFilter, null, staticFilter, "mobilityId");
  }

  @Test
  public void getAllSchacCodeFromSendingInstitutionByReceivingInstitution() {
    dao.findAllSchacCodeFromSendingInstitutionByReceivingInstitution("UV.ES");
  }

  @Test
  public void findAllSubjectAreaByReceivingInstitution() {
    dao.findAllSubjectAreaIncludedInLaByReceivingInstitution("UV.ES");
  }


  @Test
  public void findByMobilityIdAndMobilityRevision() {
    dao.findByMobilityIdAndMobilityRevision("C4F40C34-F571-6FEC-E053-1FC616ACFAF5", 0);
  }
}
