package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.jdbc.SqlScriptsTestExecutionListener;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgMobilityLaDao;
import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The type Pkg mobility la test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, SqlScriptsTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgMobilityLaTest {

  @Autowired
  IPkgMobilityLaDao dao;

  @Before
  public void init() {

  }

  @Test
  public void insertLearningAgreementTest() throws IOException {

    final LearningAgreement learningAgreement = new LearningAgreement();

    learningAgreement.setMobilityId("C2D1D7B6-14C5-18E9-E053-1FC616ACEC7F");
    learningAgreement.setStudentLa(new StudentLa());
    learningAgreement.getStudentLa().setGender(1);
    learningAgreement.getStudentLa().setCitizenShip("CITIZEN");
    learningAgreement.getStudentLa().setFamilyName("familyName");
    learningAgreement.getStudentLa().setGivenName("givenName");
    learningAgreement.getStudentLa().setBirthName(new Date());
    final List<LaComponent> laComponentList = new ArrayList<>();
    final LaComponent laComponent = new LaComponent();
    laComponent.setLaComponentType(2);
    laComponent.setLosCode("code");
    laComponent.setTitle("title");
    laComponent.setAcademicTerm(new AcademicTerm());
    final List<LanguageItem> languageItemList = new ArrayList<>();
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setLang("lang");
    languageItem.setText("text");
    languageItemList.add(languageItem);
    laComponent.getAcademicTerm().setDescription(languageItemList);
    laComponent.getAcademicTerm().setTotalTerms(1);
    laComponent.getAcademicTerm().setTermNumber(2);
    laComponent.getAcademicTerm().setEndDate(new Date());
    laComponent.getAcademicTerm().setStartDate(new Date());
    laComponent.getAcademicTerm().setOrganizationUnitCode("code");
    laComponent.getAcademicTerm().setInstitutionId("id");
    laComponent.getAcademicTerm().setAcademicYear("year");
    laComponent.setCredit(new Credit());
    laComponent.getCredit().setCreditValue(12f);
    laComponent.getCredit().setScheme("Scheme");
    laComponent.setRecognitionConditions("conditions");
    laComponent.setShortDescription("description");
    laComponent.setStatus(1);
    laComponent.setReasonCode(1);
    laComponent.setReasonText("text");
    laComponentList.add(laComponent);
    learningAgreement.setLaComponentList(laComponentList);
    final SignatureMobility signatureMobility = new SignatureMobility();
    signatureMobility.setSignerName("Sergio");
    signatureMobility.setSignerPosition("position");
    signatureMobility.setSignerEmail("sdf@minsait.com");
    signatureMobility.setSignDate(new Date());
    signatureMobility.setSignerApp("signerApp");
    learningAgreement.setStudentSignature(signatureMobility);
    learningAgreement.setSendingHeiSignature(signatureMobility);

    final RespuestaPkg respuesta = dao.insertLearningAgreement(learningAgreement, "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    assertTrue(true);
  }

  @Test
  public void updateLearningAgreementTest() throws IOException {

    final LearningAgreement learningAgreement = new LearningAgreement();

    learningAgreement.setMobilityId("ID");
    learningAgreement.setStudentLa(new StudentLa());
    learningAgreement.getStudentLa().setGender(1);
    learningAgreement.getStudentLa().setCitizenShip("CITIZEN");
    learningAgreement.getStudentLa().setFamilyName("familyName");
    learningAgreement.getStudentLa().setGivenName("givenName");
    learningAgreement.getStudentLa().setBirthName(new Date());
    final List<LaComponent> laComponentList = new ArrayList<>();
    final LaComponent laComponent = new LaComponent();
    laComponent.setLaComponentType(2);
    laComponent.setLosCode("code");
    laComponent.setTitle("title");
    laComponent.setAcademicTerm(new AcademicTerm());
    final List<LanguageItem> languageItemList = new ArrayList<>();
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setLang("lang");
    languageItem.setText("text");
    languageItemList.add(languageItem);
    laComponent.getAcademicTerm().setDescription(languageItemList);
    laComponent.getAcademicTerm().setTotalTerms(1);
    laComponent.getAcademicTerm().setTermNumber(2);
    laComponent.getAcademicTerm().setEndDate(new Date());
    laComponent.getAcademicTerm().setStartDate(new Date());
    laComponent.getAcademicTerm().setOrganizationUnitCode("code");
    laComponent.getAcademicTerm().setInstitutionId("id");
    laComponent.getAcademicTerm().setAcademicYear("year");
    laComponent.setCredit(new Credit());
    laComponent.getCredit().setCreditValue(12f);
    laComponent.getCredit().setScheme("Scheme");
    laComponent.setRecognitionConditions("conditions");
    laComponent.setShortDescription("description");
    laComponent.setStatus(2);
    laComponent.setReasonCode(2);
    laComponent.setReasonText("text");
    laComponentList.add(laComponent);
    learningAgreement.setLaComponentList(laComponentList);
    final SignatureMobility signatureMobility = new SignatureMobility();
    signatureMobility.setSignerName("name");
    signatureMobility.setSignerPosition("position");
    signatureMobility.setSignerEmail("email");
    signatureMobility.setSignDate(new Date());
    signatureMobility.setSignerApp("signerApp");
    learningAgreement.setStudentSignature(signatureMobility);
    learningAgreement.setSendingHeiSignature(signatureMobility);

    final RespuestaPkg respuesta = dao.updateLearningAgreement("2", learningAgreement, "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void deleteLearningAgreementTest() {
    final RespuestaPkg respuesta = dao.deleteLearningAgreement("C329B032-662C-6F15-E053-1FC616AC9A04", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void acceptLearningAgreementTest() {
    final SignatureMobility signature = new SignatureMobility();

    signature.setSignerName("name");
    signature.setSignerPosition("position");
    signature.setSignerEmail("a@a");
    signature.setSignDate(new Date());
    signature.setSignerApp("app");

    final RespuestaPkg respuesta = dao.acceptLearningAgreement("hola", 1, signature, "UV.ES");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void rejectLearningAgreementTest() {
    final SignatureMobility signature = new SignatureMobility();

    signature.setSignerName("name");
    signature.setSignerPosition("position");
    signature.setSignerEmail("a@a");
    signature.setSignDate(new Date());
    signature.setSignerApp("app");

    final RespuestaPkg respuesta = dao.rejectLearningAgreement("hola", 1, signature, "observation", "UV.ES");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void insertMobilityLaTest() throws IOException {

    final Mobility mobility = new Mobility();

    mobility.setSendingInstitutionBasic(new InstitutionBasic());
    mobility.getSendingInstitutionBasic().setInstitutionID("ID34");
    final List<LanguageItem> institutionNameList = new ArrayList<>();
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setText("NAME TEXT");
    languageItem.setLang("NAME LANG");
    institutionNameList.add(languageItem);
    final List<LanguageItem> institutionNameList2 = new ArrayList<>();
    final LanguageItem languageItem2 = new LanguageItem();
    languageItem2.setText("TEXT");
    languageItem2.setLang("LANG");
    institutionNameList2.add(languageItem2);
    mobility.getSendingInstitutionBasic().setInstitutionName(institutionNameList);
    mobility.getSendingInstitutionBasic().setOrganizationUnitCode("CODE");
    mobility.getSendingInstitutionBasic().setOrganizationUnitName(institutionNameList);
    mobility.setReceivingInstitutionBasic(new InstitutionBasic());
    mobility.getReceivingInstitutionBasic().setOrganizationUnitName(institutionNameList2);
    mobility.getReceivingInstitutionBasic().setOrganizationUnitCode("ID");
    mobility.getReceivingInstitutionBasic().setInstitutionName(institutionNameList2);
    mobility.getReceivingInstitutionBasic().setInstitutionID("CODE");
    mobility.setActualArrivalDate(new Date());
    mobility.setActualDepatureDate(new Date());
    mobility.setPlanedArrivalDate(new Date());
    mobility.setPlanedDepatureDate(new Date());
    mobility.setIiaId("2");
    mobility.setReceivingIiaId("3");
    mobility.setCooperationConditionId("CONDITIONID");
    mobility.setStudent(new Student());
    mobility.getStudent().setContactPerson(new ContactPerson());
    mobility.getStudent().getContactPerson().setGivenName("GIVEN NAME");
    mobility.getStudent().getContactPerson().setFamilyName("FAMILY NAME");
    mobility.getStudent().getContactPerson().setCitizenShip("CITIZEN SHIP");
    mobility.getStudent().getContactPerson().setContact(new Contact());
    mobility.getStudent().getContactPerson().getContact().setContactUrlList(institutionNameList);
    mobility.getStudent().getContactPerson().getContact().setPhoneNumber(new PhoneNumber());
    mobility.getStudent().getContactPerson().getContact().getPhoneNumber().setE164("+34666666666");
    final FlexibleAddress flexibleAddress = new FlexibleAddress();
    final List<String> listString = new ArrayList<>();
    listString.add("string");
    flexibleAddress.setRecipentNames(listString);
    flexibleAddress.setAddressLines(listString);
    flexibleAddress.setBuildingNumber("BUILDING NUMBER");
    flexibleAddress.setBuildingName("BUILDING NAME");
    flexibleAddress.setStreetName("STREET NAME");
    flexibleAddress.setUnit("UNIT");
    flexibleAddress.setFloor("FLOOR");
    flexibleAddress.setPostOfficeBox("OFFICE BOX");
    flexibleAddress.setDeliveryPointCodes(listString);
    flexibleAddress.setPostalCode("POSTAL CODE");
    flexibleAddress.setLocality("LOCALITY");
    flexibleAddress.setRegion("REGION");
    flexibleAddress.setCountry("COUNTRY");
    mobility.getStudent().getContactPerson().getContact().setStreetAddress(flexibleAddress);
    mobility.getStudent().getContactPerson().getContact().setContactRole("CONTACT ROLE");
    mobility.getStudent().getContactPerson().getContact().setInstitutionId("ID");
    mobility.getStudent().getContactPerson().getContact().setOrganizationUnitCode("UNIT CODE");
    final List<String> emailList = new ArrayList<>();
    emailList.add("agf@minsait.com");
    mobility.getStudent().getContactPerson().getContact().setEmailList(emailList);
    mobility.getStudent().getContactPerson().getContact().setContactDescriptionList(institutionNameList);
    mobility.getStudent().getContactPerson().getContact().setContactNameList(institutionNameList);
    mobility.getStudent().getContactPerson().setBirthDate(new Date());
    mobility.getStudent().getContactPerson().setGender(1);
    mobility.getStudent().setGlobalId("GLOBAL ID");
    mobility.setEqfLevelDeparture(2);
    mobility.setEqfLevelNomination(3);
    final SubjectArea subjectArea = new SubjectArea();
    subjectArea.setIscedClarification("CLARIFICATION");
    subjectArea.setIscedCode("CODE");
    mobility.setSubjectArea(subjectArea);
    final List<LanguageSkill> languageSkillList = new ArrayList<>();
    final LanguageSkill languageSkill = new LanguageSkill();
    languageSkill.setCefrLevel("CEFR");
    languageSkill.setLanguage("LANGUAGE");
    languageSkillList.add(languageSkill);
    mobility.setLanguageSkillList(languageSkillList);
    final ContactPerson contactPerson = new ContactPerson();
    contactPerson.setGivenName("GIVEN NAME");
    contactPerson.setFamilyName("FAMILY NAME");
    contactPerson.setCitizenShip("CITIZEN SHIP");
    contactPerson.setContact(new Contact());
    contactPerson.getContact().setContactUrlList(institutionNameList);
    contactPerson.getContact().setPhoneNumber(new PhoneNumber());
    contactPerson.getContact().getPhoneNumber().setE164("+34666666666");
    final List<String> emailContactPerson = new ArrayList<>();
    emailContactPerson.add("b@b.com");
    contactPerson.getContact().setEmailList(emailContactPerson);
    contactPerson.setBirthDate(new Date());
    contactPerson.setGender(1);
    mobility.setSenderContact(contactPerson);
    mobility.setSenderAdmvContact(contactPerson);
    final ContactPerson contactPerson2 = new ContactPerson();
    contactPerson2.setGivenName("GIVEN");
    contactPerson2.setFamilyName("FAMILY");
    contactPerson2.setCitizenShip("CITIZEN");
    contactPerson2.setContact(new Contact());
    contactPerson2.getContact().setContactUrlList(institutionNameList);
    contactPerson2.getContact().setPhoneNumber(new PhoneNumber());
    contactPerson2.getContact().getPhoneNumber().setE164("+34666666677");
    final List<String> emailContactPerson2 = new ArrayList<>();
    emailContactPerson2.add("a@a.com");
    contactPerson2.getContact().setEmailList(emailContactPerson2);
    contactPerson2.setBirthDate(new Date());
    contactPerson2.setGender(1);
    mobility.setReceiverContact(contactPerson2);
    mobility.setReceiverAdmvContact(contactPerson2);
    mobility.setMobilityType("MOBILITY TYPE");
    mobility.setStatusOutgoing(1);

    final AcademicTerm academicTerm = new AcademicTerm();
    academicTerm.setAcademicYear("2020/2021");
    academicTerm.setTermNumber(1);
    academicTerm.setTotalTerms(2);

    mobility.setAcademicTerm(academicTerm);

    final RespuestaPkg respuesta = dao.insertMobility(mobility, "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    assertTrue(true);
  }

  @Test
  public void updateMobilityTest() throws IOException {

    final Mobility mobility = new Mobility();

    mobility.setSendingInstitutionBasic(new InstitutionBasic());
    mobility.getSendingInstitutionBasic().setInstitutionID("ID");
    final List<LanguageItem> institutionNameList = new ArrayList<>();
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setText("NAME TEXT");
    languageItem.setLang("NAME LANG");
    institutionNameList.add(languageItem);
    mobility.getSendingInstitutionBasic().setInstitutionName(institutionNameList);
    mobility.getSendingInstitutionBasic().setOrganizationUnitCode("CODE");
    mobility.getSendingInstitutionBasic().setOrganizationUnitName(institutionNameList);
    mobility.setReceivingInstitutionBasic(new InstitutionBasic());
    mobility.getReceivingInstitutionBasic().setOrganizationUnitName(institutionNameList);
    mobility.getReceivingInstitutionBasic().setOrganizationUnitCode("ID");
    mobility.getReceivingInstitutionBasic().setInstitutionName(institutionNameList);
    mobility.getReceivingInstitutionBasic().setInstitutionID("CODE");
    mobility.setActualArrivalDate(new Date());
    mobility.setActualDepatureDate(new Date());
    mobility.setPlanedArrivalDate(new Date());
    mobility.setPlanedDepatureDate(new Date());
    mobility.setIiaId("2");
    mobility.setReceivingIiaId("3");
    mobility.setCooperationConditionId("CONDITIONID");
    mobility.setStudent(new Student());
    mobility.getStudent().setContactPerson(new ContactPerson());
    mobility.getStudent().getContactPerson().setGivenName("GIVEN NAME");
    mobility.getStudent().getContactPerson().setFamilyName("FAMILY NAME");
    mobility.getStudent().getContactPerson().setCitizenShip("CITIZEN SHIP");
    mobility.getStudent().getContactPerson().setContact(new Contact());
    mobility.getStudent().getContactPerson().getContact().setContactUrlList(institutionNameList);
    mobility.getStudent().getContactPerson().getContact().setPhoneNumber(new PhoneNumber());
    mobility.getStudent().getContactPerson().getContact().getPhoneNumber().setE164("+34666666666");
    final FlexibleAddress flexibleAddress = new FlexibleAddress();
    final List<String> listString = new ArrayList<>();
    listString.add("string");
    flexibleAddress.setRecipentNames(listString);
    flexibleAddress.setAddressLines(listString);
    flexibleAddress.setBuildingNumber("BUILDING NUMBER");
    flexibleAddress.setBuildingName("BUILDING NAME");
    flexibleAddress.setStreetName("STREET NAME");
    flexibleAddress.setUnit("UNIT");
    flexibleAddress.setFloor("FLOOR");
    flexibleAddress.setPostOfficeBox("OFFICE BOX");
    flexibleAddress.setDeliveryPointCodes(listString);
    flexibleAddress.setPostalCode("POSTAL CODE");
    flexibleAddress.setLocality("LOCALITY");
    flexibleAddress.setRegion("REGION");
    flexibleAddress.setCountry("COUNTRY");
    mobility.getStudent().getContactPerson().getContact().setStreetAddress(flexibleAddress);
    mobility.getStudent().getContactPerson().getContact().setContactRole("CONTACT ROLE");
    mobility.getStudent().getContactPerson().getContact().setInstitutionId("ID");
    mobility.getStudent().getContactPerson().getContact().setOrganizationUnitCode("UNIT CODE");
    final List<String> emailList2 = new ArrayList<>();
    emailList2.add("b@b.com");
    mobility.getStudent().getContactPerson().getContact().setEmailList(emailList2);
    mobility.getStudent().getContactPerson().getContact().setContactDescriptionList(institutionNameList);
    mobility.getStudent().getContactPerson().getContact().setContactNameList(institutionNameList);
    mobility.getStudent().getContactPerson().setBirthDate(new Date());
    mobility.getStudent().getContactPerson().setGender(1);
    mobility.getStudent().setGlobalId("GLOBAL ID");
    mobility.setEqfLevelDeparture(2);
    mobility.setEqfLevelNomination(2);
    final SubjectArea subjectArea = new SubjectArea();
    subjectArea.setIscedClarification("CLARIFICATION");
    subjectArea.setIscedCode("CODE");
    mobility.setSubjectArea(subjectArea);
    final List<LanguageSkill> languageSkillList = new ArrayList<>();
    final LanguageSkill languageSkill = new LanguageSkill();
    languageSkill.setCefrLevel("CEFR");
    languageSkill.setLanguage("LANGUAGE");
    languageSkillList.add(languageSkill);
    mobility.setLanguageSkillList(languageSkillList);
    final ContactPerson contactPerson = new ContactPerson();
    contactPerson.setGivenName("GIVEN NAME");
    contactPerson.setFamilyName("FAMILY NAME");
    contactPerson.setCitizenShip("CITIZEN SHIP");
    contactPerson.setContact(new Contact());
    contactPerson.getContact().setContactUrlList(institutionNameList);
    contactPerson.getContact().setPhoneNumber(new PhoneNumber());
    contactPerson.getContact().getPhoneNumber().setE164("+34666666666");
    contactPerson.setBirthDate(new Date());
    contactPerson.setGender(1);
    mobility.setSenderContact(contactPerson);
    mobility.setSenderAdmvContact(contactPerson);
    mobility.setReceiverContact(contactPerson);
    mobility.setReceiverAdmvContact(contactPerson);
    mobility.setMobilityType("MOBILITY TYPE");
    mobility.setStatusOutgoing(3);

    final AcademicTerm academicTerm = new AcademicTerm();
    academicTerm.setAcademicYear("2020/2021");
    academicTerm.setTermNumber(1);
    academicTerm.setTotalTerms(2);

    mobility.setAcademicTerm(academicTerm);

    final List<PhotoUrl> photoUrls = new ArrayList<>();
    final PhotoUrl photoUrl = new PhotoUrl();
    photoUrl.setUrl("http://test.test/foto.jpg");
    mobility.getStudent().setPhotoUrlList(photoUrls);

    final RespuestaPkg respuesta = dao.updateMobility("1", mobility, "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());

    assertTrue(true);

  }

  @Test
  public void deleteMobilityLaTest() {
    final RespuestaPkg respuesta = dao.deleteMobility("C777B80C-51F8-34C5-E053-1FC616ACAE22", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void cancelMobilityLaTest() {
    final RespuestaPkg respuesta = dao.cancelMobility("C777B80C-51F8-34C5-E053-1FC616ACAE22", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void studentDepartureMobilityLaTest() {
    final RespuestaPkg respuesta = dao.studentDeparture("C777B80C-51F8-34C5-E053-1FC616ACAE22", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void studentArrivalMobilityLaTest() {
    final RespuestaPkg respuesta = dao.studentArrival("C777B80C-51F8-34C5-E053-1FC616ACAE22", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void approveNominationMobilityLaTest() {
    final RespuestaPkg respuesta = dao.approveNomination("C777B80C-51F8-34C5-E053-1FC616ACAE22", "", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void rejectNominationMobilityLaTest() {
    final RespuestaPkg respuesta =
        dao.rejectNomination("C777B80C-51F8-34C5-E053-1FC616ACAE22", "Movilidad rechazada", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void updateActualDatesMobilityLaTest() {
    final RespuestaPkg respuesta = dao.updateActualDates("C777B80C-51F8-34C5-E053-1FC616ACAE22", new Date(), new Date(),
        "Movilidad rechazada", "HEI");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void fixLaApproveTest() {
    final RespuestaPkg respuesta = dao.fixLaApprove("C777B80C-51F8-34C5-E053-1FC616ACAE22");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

  @Test
  public void fixLaRejectTest() {
    final RespuestaPkg respuesta = dao.fixLaReject("C777B80C-51F8-34C5-E053-1FC616ACAE22");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    assertTrue(true);
  }

}
