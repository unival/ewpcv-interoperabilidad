package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiasStatsDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class IiasStatsTest {

    @Autowired
    IIiasStatsDao dao;

    @Before
    public void init() {

    }

    @Test
    public void findByFilter() {
        final PageFilterDTO filter = new PageFilterDTO();
        filter.setPageTotal(5);

        final Map<String, Object> paramFilter = new HashMap<>();
        paramFilter.put("id.fetchables", "11");

        final Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "id.fetchables");
        assertNotNull(list);
    }
}
