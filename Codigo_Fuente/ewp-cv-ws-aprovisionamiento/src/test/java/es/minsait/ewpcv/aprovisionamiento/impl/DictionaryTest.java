package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IDictionaryViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryType;
import es.minsait.ewpcv.aprovisionamiento.model.DictionaryView;

/**
 * The type Dictionary test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class DictionaryTest {

  @Autowired
  IDictionaryViewDao dao;

  @Before
  public void init() {

  }

  @Test
  public void findByDictionaryTypeTest() {
    final List<DictionaryView> list = dao.findByDictionaryType(DictionaryType.EDUCATION_FIELDS);
    System.out.println("***list: " + list.size());
  }

  @Test
  public void findByFilter() {
    final PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    paramFilter.put("dictionaryType", DictionaryType.EDUCATION_LEVELS.toString());

    final Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "description");
    assertNotNull(list);
  }
}
