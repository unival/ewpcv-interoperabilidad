package es.minsait.ewpcv.aprovisionamiento.impl;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import es.minsait.ewpcv.aprovisionamiento.dao.IFunctionsUtilDao;
import es.minsait.ewpcv.aprovisionamiento.model.ElementTypes;

/**
 * The type Function utils test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class FunctionUtilsTest {
  @Autowired
  IFunctionsUtilDao dao;

  @Test
  public void testNotificaElemento() {
    final String response =
        dao.notificaElemento("01ef5d1d-1318-478e-b430-6a4f8734349d", ElementTypes.IIA.getValue(), "unican.es", "uv.es");
    System.out.println(response);
  }

  @Test
  public void testMUpdateRequest() {
    final String response = dao.resetMUpdateRequest("test");
    System.out.println(response);
  }

  @Test
  public void testResetNotificacion() {
    final String response = dao.resetNotificacion("test");
    System.out.println(response);
  }

  @Test
  public void testInsertaNotificacionEntrante() {
    final String response =
            dao.insertaNotificacionEntrante("01ef5d1d-1318-478e-b430-6a4f8734349d", ElementTypes.IIA.getValue(), "unican.es", "uv.es");
    System.out.println(response);
  }
}
