package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaCoopConditionsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type Iia coop conditions test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class IiaCoopConditionsTest {

  @Autowired
  IIiaCoopConditionsViewDao dao;

  @Test
  public void findByFilter() {
    PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    final Map<String, Object> paramDentroFilter = new HashMap<>();
    paramDentroFilter.put("coopCondId", "C2DA1F6F-26C9-3EEC-E053-1FC616AC1E79");
    paramFilter.put("OR", paramDentroFilter);

    final Map<String, Object> datesFilterFrom = new HashMap<>();
    datesFilterFrom.put("iiaStartDate", "07/07/21 00:00:00");
    final Map<String, Object> datesFilterTo = new HashMap<>();
    datesFilterTo.put("iiaEndDate", "10/12/22 00:00:00");


    final Map<String, Object> datesFilter = new HashMap<>();
    datesFilter.put("from", datesFilterFrom);
    datesFilter.put("to", datesFilterTo);

    Map<Object, Object> list = dao.findByFilter(filter, paramFilter, datesFilter, null, "coopCondId");
    assertNotNull(list);
  }
}
