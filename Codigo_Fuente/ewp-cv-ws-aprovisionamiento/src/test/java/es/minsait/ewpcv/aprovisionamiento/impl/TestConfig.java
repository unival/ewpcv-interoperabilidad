package es.minsait.ewpcv.aprovisionamiento.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


/**
 * The type Test config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
@ComponentScan(basePackages = {"es.minsait.ewpcv.aprovisionamiento"})
@EnableJpaRepositories(basePackages = {"es.minsait.ewpcv.aprovisionamiento.impl.springdata"})
@EnableTransactionManagement()
@PropertySource(value = "classpath:application-test.properties")
public class TestConfig {

  @Autowired
  private Environment env;

  /**
   * Entity manager factory local container entity manager factory bean.<
   *
   * @return the local container entity manager factory bean
   */
  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource());
    em.setPackagesToScan("es.minsait.ewpcv.aprovisionamiento");// Entities to load
    HibernateJpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendor);
    em.setJpaProperties(additionalProperties());

    return em;
  }

  /**
   * Data source data source.
   *
   * @return the data source
   */
  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(this.env.getProperty("spring.datasource.driver.class"));
    dataSource.setUrl(this.env.getProperty("spring.datasource.url"));
    dataSource.setUsername(this.env.getProperty("spring.datasource.username"));
    dataSource.setPassword(this.env.getProperty("spring.datasource.password"));

    return dataSource;
  }

  /**
   * Transaction manager platform transaction manager.
   *
   * @param emf the emf
   * @return the platform transaction manager
   */
  @Bean
  public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf);

    return transactionManager;
  }

  /**
   * Exception translation persistence exception translation post processor.
   *
   * @return the persistence exception translation post processor
   */
  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  private Properties additionalProperties() {
    Properties properties = new Properties();

    // Config properties hibernate
    properties.setProperty("hibernate.dialect", this.env.getProperty("spring.jpa.database-platform"));
    properties.setProperty("hibernate.hbm2ddl.auto", this.env.getProperty("spring.jpa.hibernate.ddl-auto"));
    properties.setProperty("hibernate.show_sql", this.env.getProperty("spring.jpa.show-sql"));
    properties.setProperty("hibernate.format_sql", this.env.getProperty("spring.jpa.properties.hibernate.format_sql"));
    // properties.setProperty("hibernate.physical_naming_strategy",
    // this.env.getProperty("spring.jpa.hibernate.naming.physical-strategy"));

    properties.setProperty("hibernate.connection.characterEncoding", "UTF-8");
    properties.setProperty("hibernate.connection.useUnicode", "true");
    properties.setProperty("hibernate.connection.charSet", "UTF-8");
    properties.setProperty("lazy", "false");

    return properties;
  }

}
