package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.ILaComponentsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type La components view test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class LaComponentsViewTest {

  @Autowired
  ILaComponentsViewDao dao;

  @Test
  public void findByFilter() {
    PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    paramFilter.put("laId", "C2D1D7B6-14C8-18E9-E053-1FC616ACEC7F");
    paramFilter.put("laRevision", "1");

    Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "laRevision");
    assertNotNull(list);
  }

  @Test
  @Ignore
  public void findByMobilityIdAndMobilityRevision() {
    dao.findByLaIdAndLaRevision("5a10eb2c-be18-4191-8746-5a1ed6a4415f", 1);
  }
}
