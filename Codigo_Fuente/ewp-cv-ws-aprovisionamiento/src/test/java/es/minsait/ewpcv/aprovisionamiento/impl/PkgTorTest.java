package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgTorDao;
import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The type Pkg tor test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgTorTest {

  @Autowired
  IPkgTorDao dao;

  @Test
  public void testInsertTor() {
    final Tor tor = new Tor();
    tor.setGeneratedDate(new Date());
    tor.setTorReportList(Arrays.asList(generateTorReport()));

    final RespuestaPkg respuesta = dao.insertTor(tor, "a5e536f7-a9bd-4906-9887-31a2f844d1ea", "uv.es");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  private TorReport generateTorReport() {
    final TorReport torReport = new TorReport();
    torReport.setIssueDate(new Date());
    torReport.setLoiList(Arrays.asList(generateLoi()));
    return torReport;
  }

  private Loi generateLoi() {
    final Loi loi = new Loi();
    loi.setStartDate(new Date());
    loi.setEndDate(new Date());
    loi.setStatus(0);
    loi.setPercentageLower(1);
    loi.setPercentageEqual(1);
    loi.setPercentageHigher(1);
    loi.setResultDistribution(generateResultDist());
    loi.setLanguageOfInstruction("LANGUAGE_OF_INSTRUCTION");
    loi.setEngagementHours(100);
    return loi;
  }

  private ResultDistribution generateResultDist() {
    final List<LanguageItem> languageItemList = new ArrayList<>();
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setLang("lang");
    languageItem.setText("text");
    languageItemList.add(languageItem);
    final ResultDistribution rd = new ResultDistribution();
    rd.setDescription(languageItemList);
    rd.setDistCategoriesList(Arrays.asList(generateDistCategories()));
    return rd;
  }

  private DistCategories generateDistCategories() {
    final LanguageItem languageItem = new LanguageItem();
    languageItem.setLang("lang");
    languageItem.setText("text");
    final DistCategories dc = new DistCategories();
    dc.setDistCatCount(1);
    dc.setLabel(languageItem);
    return dc;
  }
}
