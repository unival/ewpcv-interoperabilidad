package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IOunitViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.OunitContactsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.OunitViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.OunitContactsView;
import es.minsait.ewpcv.aprovisionamiento.model.OunitView;

/**
 * The type Ounit view test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class OunitViewTest {

  @Autowired
  OunitViewRepository repository;

  @Autowired
  IOunitViewDao dao;

  @Autowired
  OunitContactsViewRepository contactsRepository;

  @Before
  public void init() {}

  @Test
  public void findOunitViewByOunitCode() {
    final OunitView ounitView = repository.findByOunitCodeAndInstitutionId("ounit.uv", "uv.es");
    System.out.println("***InstitutionIdentifier: " + ounitView);
  }

  @Test
  public void findOunitViewDaoByOunitCode() {
    final PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    paramFilter.put("institutionId", "uv.es");

    final Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "institutionId");
    assertNotNull(list);
  }

  @Test
  public void findOunitViewByInstitutionId() {
    final List<OunitView> ounitViewList = repository.findByInstitutionId("uv.es");
    System.out.println("***InstitutionIdentifier: " + ounitViewList);
  }

  @Test
  public void findOunitContactViewByOunitCode() {
    final OunitContactsView ounitView = contactsRepository.findByOunitCodeAndInstitutionId("ounit.ROOT.uv", "uv.es");
    System.out.println("***InstitutionIdentifier: " + ounitView);
  }

  @Test
  public void findOunitContactViewByInstitutionId() {
    final List<OunitContactsView> ounitViewList = contactsRepository.findByInstitutionId("uv.es");
    System.out.println("***InstitutionIdentifier: " + ounitViewList);
  }

  @Test
  public void findAllOunitContactViewByOrder() {
    final List<OunitContactsView> ounitViewList = contactsRepository.findAllByOrderByOunitNameAsc();
    System.out.println("***InstitutionIdentifier: " + ounitViewList);
  }

  @Test
  public void findAllOunitViewByOrder() {
    final List<OunitView> ounitViewList = repository.findAllByOrderByOunitNameAsc();
    System.out.println("***InstitutionIdentifier: " + ounitViewList);
  }
}
