package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.dao.IInstitutionIdentifiersDao;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionIdentifiersView;

/**
 * The type Institution identifiers test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class InstitutionIdentifiersTest {

  @Autowired
  IInstitutionIdentifiersDao dao;

  @Before
  public void init() {}

  @Test
  public void findByDictionaryTypeTest() {
    InstitutionIdentifiersView institutionIdentifiersView = dao.findById("um.es");
    System.out.println("***InstitutionIdentifier: " + institutionIdentifiersView);
  }

  @Test
  public void findAll() {
    List<InstitutionIdentifiersView> institutionIdentifiersViewList = dao.findAll();
    System.out.println("***InstitutionIdentifier: " + institutionIdentifiersViewList);
  }
}
