package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.util.Assert;

import java.util.Arrays;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgOunitDao;
import es.minsait.ewpcv.aprovisionamiento.model.LanguageItem;
import es.minsait.ewpcv.aprovisionamiento.model.Ounit;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The type Pkg ounit test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgOunitTest {

  @Autowired
  IPkgOunitDao dao;

  @Test
  public void testInsertOunit() {
    final LanguageItem li = new LanguageItem();
    li.setLang("ES");
    li.setText("Ounit de prueba");

    final Ounit ounit = new Ounit();
    ounit.setHeiId("uv.es");
    ounit.setOunitCode("Code de prueba");
    ounit.setOunitName(Arrays.asList(li));
    ounit.setAbbreviation("Ounit Pru");
    ounit.setParentOunitId("CA4D674D-FDDB-763B-E053-1FC616AC0906");
    ounit.setIsTreeStructure(Boolean.TRUE);

    final RespuestaPkg respuesta = dao.insertOunit(ounit);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testUpdateOunit() {
    final LanguageItem li = new LanguageItem();
    li.setLang("ES");
    li.setText("Ounit de prueba");

    final Ounit ounit = new Ounit();
    ounit.setHeiId("uv.es");
    ounit.setOunitCode("ounit.child.B.uv");
    ounit.setOunitName(Arrays.asList(li));
    ounit.setAbbreviation("Ounit Pru");
    ounit.setParentOunitId("CA4D674D-FDDB-763B-E053-1FC616AC0906");
    ounit.setIsTreeStructure(Boolean.TRUE);

    final RespuestaPkg respuesta = dao.updateOunit("CA71D355-CC6B-1594-E053-1FC616ACA280", ounit);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    // Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testDeleteOunit() {
    final RespuestaPkg respuesta = dao.deleteOunit("CA71D355-CC6B-1594-E053-1FC616ACA280", "uv.es");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testOcultarOunit() {
    final RespuestaPkg respuesta = dao.ocultarOunit("CA4D674D-FDDB-763B-E053-1FC616AC0906", "uv.es");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testExponerOunit() {
    final RespuestaPkg respuesta = dao.exponerOunit("CA4D674D-FDDB-763B-E053-1FC616AC0906", "uv.es");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }
}
