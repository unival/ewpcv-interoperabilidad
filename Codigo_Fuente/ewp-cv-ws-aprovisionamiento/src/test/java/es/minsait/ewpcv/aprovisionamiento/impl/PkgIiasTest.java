package es.minsait.ewpcv.aprovisionamiento.impl;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.*;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgIiasDao;
import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The type Pkg iias test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgIiasTest {

  private static final String HEI_TO_NOTIFY = "UM.ES";
  @Autowired
  IPkgIiasDao dao;

  @Before
  public void init() {

  }

  private Iia generaIIA() {
    final Iia iia = new Iia();

    iia.setIiaCode("IIA_EWP");
    iia.setStartDate(new Date());
    iia.setEndDate(new Date());

    // Institucion envio
    final LanguageItem sendingInstitutionNameES = new LanguageItem();
    sendingInstitutionNameES.setLang("ES");
    sendingInstitutionNameES.setText("Universidad de Valencia");
    final LanguageItem sendingInstitutionNameEN = new LanguageItem();
    sendingInstitutionNameEN.setLang("EN");
    sendingInstitutionNameEN.setText("University of Valencia");

    final LanguageItem sendingOunitNameES = new LanguageItem();
    sendingOunitNameES.setLang("ES");
    sendingOunitNameES.setText("Departamento de prueba");

    final InstitutionBasic sendingInstitutionBasic = new InstitutionBasic();
    sendingInstitutionBasic.setInstitutionID("UV.ES");
    sendingInstitutionBasic.setInstitutionName(Arrays.asList(sendingInstitutionNameES, sendingInstitutionNameEN));
    // sendingInstitutionBasic.setOrganizationUnitCode("OunitCodeUV");
    // sendingInstitutionBasic.setOrganizationUnitName(Arrays.asList(sendingOunitNameES));

    // Institucion recepcion
    final LanguageItem receivingInstitutionNameES = new LanguageItem();
    receivingInstitutionNameES.setLang("ES");
    receivingInstitutionNameES.setText("Universidad de Murcia");

    final LanguageItem receivingOunitNameES = new LanguageItem();
    receivingOunitNameES.setLang("ES");
    receivingOunitNameES.setText("Departamento de prueba murcia");

    final InstitutionBasic receivingInstitutionBasic = new InstitutionBasic();
    receivingInstitutionBasic.setInstitutionID("UM.ES");
    receivingInstitutionBasic.setInstitutionName(Arrays.asList(receivingInstitutionNameES));
    // receivingInstitutionBasic.setOrganizationUnitCode("OunitCodeUM");
    // receivingInstitutionBasic.setOrganizationUnitName(Arrays.asList(receivingOunitNameES));

    // subject areas
    final SubjectArea sa = new SubjectArea();
    sa.setIscedCode("06");
    sa.setIscedClarification("Information and Communication Technologies (ICTs)");

    // language skills
    final LanguageSkill b2Espanol = new LanguageSkill();
    b2Espanol.setLanguage("ES");
    b2Espanol.setCefrLevel("B2");

    // vinculaciones subject area language skills
    final SubjectAreaLanguaje ictsB2Es = new SubjectAreaLanguaje();
    ictsB2Es.setLanguageSkill(b2Espanol);
    ictsB2Es.setSubjectArea(sa);

    // duraciones
    final DurationClass unMes = new DurationClass();
    unMes.setUnit(3);
    unMes.setNumberDuration(new BigDecimal(1.5));

    final DurationClass dosMeses = new DurationClass();
    dosMeses.setUnit(3);
    dosMeses.setNumberDuration(new BigDecimal(1.5));

    // Tipos de movilidad
    final MobilityType estudios = new MobilityType();
    estudios.setMobilityCategory("STUDIES");
    estudios.setMobilityGroup("STUDENT");

    // condiciones de cooperacion
    final List<CooperationCondition> cooperationConditions = new ArrayList<>();
    final CooperationCondition cooperationCondition1 = new CooperationCondition();
    cooperationCondition1.setStartDate(new Date());
    cooperationCondition1.setEndDate(new Date());
    cooperationCondition1.setEqfLevelList(Arrays.asList(1));
    cooperationCondition1.setMobilityNumber(1);
    cooperationCondition1.setMobilityType(estudios);
    cooperationCondition1.setDuration(unMes);
    cooperationCondition1.setSubjectAreaLanguajeList(Arrays.asList(ictsB2Es));
    cooperationCondition1.setBlended(0);

    final CooperationCondition cooperationCondition2 = new CooperationCondition();
    cooperationCondition2.setStartDate(new Date());
    cooperationCondition2.setEndDate(new Date());
    cooperationCondition2.setEqfLevelList(Arrays.asList(1));
    cooperationCondition2.setMobilityNumber(1);
    cooperationCondition2.setMobilityType(estudios);
    cooperationCondition2.setDuration(dosMeses);
    cooperationCondition2.setSubjectAreaLanguajeList(Arrays.asList(ictsB2Es));
    cooperationCondition2.setBlended(0);

    // ---Sending partner
    final Partner sendingPartner = new Partner();
    sendingPartner.setInstitutionBasic(sendingInstitutionBasic);
    sendingPartner.setSigningDate(new Date());

    final ContactPerson senderContactPerson = new ContactPerson();
    // datos personales del sending partner
    // senderContactPerson.setGender(0);
    senderContactPerson.setCitizenShip("ES");
    senderContactPerson.setBirthDate(new Date());
    senderContactPerson.setFamilyName("Coordinador Emisor");
    senderContactPerson.setGivenName("PEPE");

    // contacto del sending partner
    final Contact senderContact = new Contact();

    final LanguageItem urlSIC = new LanguageItem();
    urlSIC.setLang("ES");
    urlSIC.setText("https://www.uv.es/");

    final LanguageItem nameSIC = new LanguageItem();
    nameSIC.setLang("ES");
    nameSIC.setText("Contacto s i c");

    final LanguageItem descriptionsSIC = new LanguageItem();
    descriptionsSIC.setLang("ES");
    descriptionsSIC.setText("Contacto del coordinador de la institucion emisora");

    final FlexibleAddress flexibleAddresSIC = new FlexibleAddress();
    flexibleAddresSIC.setRecipentNames(Arrays.asList("institucion emisora"));
    flexibleAddresSIC.setAddressLines(Arrays.asList("Av Blasco Ibañez, 13"));
    flexibleAddresSIC.setCountry("ES");
    flexibleAddresSIC.setRegion("Comunidad Valenciana");
    flexibleAddresSIC.setLocality("Valencia");
    flexibleAddresSIC.setPostalCode("460010");

    final PhoneNumber phoneSIC = new PhoneNumber();
    phoneSIC.setE164("+34222222222");

    senderContact.setEmailList(Arrays.asList("sender_coordinator@email.com"));
    senderContact.setOrganizationUnitCode(sendingInstitutionBasic.getOrganizationUnitCode());
    senderContact.setInstitutionId(sendingInstitutionBasic.getInstitutionID());
    senderContact.setContactRole("coordinador");
    senderContact.setContactUrlList(Arrays.asList(urlSIC));
    senderContact.setContactDescriptionList(Arrays.asList(descriptionsSIC));
    senderContact.setContactNameList(Arrays.asList(nameSIC));
    senderContact.setStreetAddress(flexibleAddresSIC);
    senderContact.setPhoneNumber(phoneSIC);

    senderContactPerson.setContact(senderContact);
    sendingPartner.setContactPerson(senderContactPerson);

    // --- Receiving partner
    final Partner receivingPartner = new Partner();
    receivingPartner.setInstitutionBasic(receivingInstitutionBasic);
    // receivingPartner.setSigningDate(new Date());

    final ContactPerson receiverContactPerson = new ContactPerson();
    // datos personales del receiving partner
    // receiverContactPerson.setGender(0);
    receiverContactPerson.setCitizenShip("ES");
    receiverContactPerson.setBirthDate(new Date());
    receiverContactPerson.setFamilyName("Coordinador Receptor");
    receiverContactPerson.setGivenName("PACO");

    // contacto del receiving partner
    final Contact receiverContact = new Contact();

    final LanguageItem urlRIC = new LanguageItem();
    urlRIC.setLang("ES");
    urlRIC.setText("https://www.um.es/");

    final LanguageItem nameRIC = new LanguageItem();
    nameRIC.setLang("ES");
    nameRIC.setText("Contacto r i c");

    final LanguageItem descriptionsRIC = new LanguageItem();
    descriptionsRIC.setLang("ES");
    descriptionsRIC.setText("Contacto del coordinador de la institucion receptora");

    final FlexibleAddress flexibleAddresRIC = new FlexibleAddress();
    flexibleAddresRIC.setRecipentNames(Arrays.asList("institucion receptora"));
    flexibleAddresRIC.setAddressLines(Arrays.asList("Paseo del Teniente Flomesta, 3"));
    flexibleAddresRIC.setCountry("ES");
    flexibleAddresRIC.setRegion("Region de Murcia");
    flexibleAddresRIC.setLocality("Murcia");
    flexibleAddresRIC.setPostalCode("30003");

    final PhoneNumber phoneRIC = new PhoneNumber();
    phoneRIC.setE164("+34111111111");

    receiverContact.setEmailList(Arrays.asList("receiver_coordinator@email.com"));
    receiverContact.setOrganizationUnitCode(receivingInstitutionBasic.getOrganizationUnitCode());
    receiverContact.setInstitutionId(receivingInstitutionBasic.getInstitutionID());
    receiverContact.setContactRole("coordinador");
    receiverContact.setContactUrlList(Arrays.asList(urlRIC));
    receiverContact.setContactDescriptionList(Arrays.asList(descriptionsRIC));
    receiverContact.setContactNameList(Arrays.asList(nameRIC));
    receiverContact.setStreetAddress(flexibleAddresRIC);
    receiverContact.setPhoneNumber(phoneRIC);

    receiverContactPerson.setContact(receiverContact);
    receivingPartner.setContactPerson(receiverContactPerson);

    cooperationCondition1.setSendingPartner(sendingPartner);
    cooperationCondition1.setReceivingPartner(receivingPartner);
    cooperationCondition2.setSendingPartner(receivingPartner);
    cooperationCondition2.setReceivingPartner(sendingPartner);

    cooperationConditions.add(cooperationCondition1);
    // cooperationConditions.add(cooperationCondition2);
    iia.setCooperationConditionList(cooperationConditions);
    return iia;
  }


  @Test
  public void testInsertIias() {

    final Iia iia = generaIIA();

    final RespuestaPkg respuesta = dao.insertIias(iia, "ID2");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testUnbindIia() {

    final RespuestaPkg respuesta = dao.unbindIias("ID2");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  @Ignore
  public void testUpdateIias() {

    final Iia iia = generaIIA();
    iia.setIiaCode("IIA_EWP_MODIFICADO");
    iia.getCooperationConditionList().get(0).getDuration().setNumberDuration(new BigDecimal(1.3));
    iia.getCooperationConditionList().get(1).setMobilityNumber(50);
    final String iiaID = "C4646299-A184-37B4-E053-1FC616ACD85A";
    final RespuestaPkg respuesta = dao.updateIias(iiaID, iia, HEI_TO_NOTIFY);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    Assert.isNull(respuesta.getError(), "iiaID " + iiaID + " error: " + respuesta.getError());
  }

  @Test
  public void testDeleteIias() {
    final String iiaID = "C7C7910E-9F3F-6718-E053-1FC616ACABB8";
    final RespuestaPkg respuesta = dao.deleteIias(iiaID, HEI_TO_NOTIFY);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    Assert.isNull(respuesta.getError(), "iiaID " + iiaID + " error: " + respuesta.getError());
  }

  @Test
  public void testInsertUpdateDeleteIias() {

    final Iia iia = generaIIA();

    final RespuestaPkg respuestaInsercion = dao.insertIias(iia, null);
    final String iiaId = respuestaInsercion.getId();
    System.out.println("***INSERCION ");
    System.out.println("***retorno: " + respuestaInsercion.getCodRetorno());
    System.out.println("***Error: " + respuestaInsercion.getError());
    System.out.println("***ID: " + respuestaInsercion.getId());

    if (Objects.nonNull(iiaId)) {
      iia.setIiaCode("IIA_EWP_MODIFICADO");
      iia.getCooperationConditionList().get(0).getDuration().setNumberDuration(new BigDecimal(1.3));
      iia.getCooperationConditionList().get(1).setMobilityNumber(50);
      final RespuestaPkg respuestaActualizacion = dao.updateIias(iiaId, iia, HEI_TO_NOTIFY);
      System.out.println("***ACTUALIZACION ");
      System.out.println("***retorno: " + respuestaActualizacion.getCodRetorno());
      System.out.println("***Error: " + respuestaActualizacion.getError());
      System.out.println("***ID: " + respuestaActualizacion.getId());

      final RespuestaPkg respuestaBorrado = dao.deleteIias(iiaId, HEI_TO_NOTIFY);
      System.out.println("***BORRADO ");
      System.out.println("***retorno: " + respuestaBorrado.getCodRetorno());
      System.out.println("***Error: " + respuestaBorrado.getError());
      System.out.println("***ID: " + respuestaBorrado.getId());
    }
  }

  @Test
  public void testPrepareSnapshots() {
    final RespuestaPkg respuesta = dao.prepareSnapshots();
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testRecoverIiaSnapshot() {

    final RespuestaPkg respuesta = dao.recoverIiaSnapshot("f0a9e503-a25f-4519-b339-5334a31ef509");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

}
