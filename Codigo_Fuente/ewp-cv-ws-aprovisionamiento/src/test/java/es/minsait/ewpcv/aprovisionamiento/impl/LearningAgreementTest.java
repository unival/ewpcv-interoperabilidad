package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.ILearningAgreementViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type Learning agreement test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class LearningAgreementTest {

  @Autowired
  ILearningAgreementViewDao dao;

  @Test
  public void findByFilter() {
    PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    paramFilter.put("mobilityId", "C2D1D7B6-149E-18E9-E053-1FC616ACEC7F");
    paramFilter.put("mobilityRevision", "0");

    Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "mobilityId");
    assertNotNull(list);
  }

  @Test
  @Ignore
  public void findByLaId() {
    dao.findByLaId("C4F451BF-C621-70CC-E053-1FC616ACB616");
  }

  @Test
  public void findByMobilityIdAndMobilityRevision() {
    dao.findByMobilityIdAndMobilityRevision("C4F40C34-F571-6FEC-E053-1FC616ACFAF5", 0);
  }
}
