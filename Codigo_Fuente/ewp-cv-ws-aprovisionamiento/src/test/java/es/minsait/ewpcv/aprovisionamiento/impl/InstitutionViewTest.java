package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import es.minsait.ewpcv.aprovisionamiento.impl.springdata.InstitutionContactsViewRepository;
import es.minsait.ewpcv.aprovisionamiento.impl.springdata.InstitutionViewRepository;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionContactsView;
import es.minsait.ewpcv.aprovisionamiento.model.InstitutionView;


/**
 * The type Institution view test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class InstitutionViewTest {

  @Autowired
  InstitutionContactsViewRepository institutionContactsViewRepository;

  @Autowired
  InstitutionViewRepository institutionViewRepository;

  @Before
  public void init() {}

  @Test
  public void findByIdContacsVIew() {
    List<InstitutionContactsView> institutionContactsView = institutionContactsViewRepository.findBySchacCode("uv.es");
    Assert.assertNotNull(institutionContactsView);
  }

  @Test
  public void findAllInstitutionView() {
    List<InstitutionView> institutionViewList = institutionViewRepository.findAll();
    Assert.assertNotNull(institutionViewList);
  }

  @Test
  public void findAllInstitutionViewByOrder() {
    List<InstitutionView> institutionViewList = institutionViewRepository.findAllByOrderByInstitutionNameAsc();
    Assert.assertNotNull(institutionViewList);
  }

  @Test
  public void findInstitutionViewBySchacCode() {
    InstitutionView institutionView = institutionViewRepository.findBySchacCode("uv.es");
    Assert.assertNotNull(institutionView);
  }

  @Test
  public void findAllInstitutionContactsViewByOrder() {
    List<InstitutionContactsView> institutionViewList =
        institutionContactsViewRepository.findAllByOrderByInstitutionNameAsc();
    Assert.assertNotNull(institutionViewList);
  }
}
