package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.minsait.ewpcv.aprovisionamiento.impl.springdata.TorAttachmentViewRepository;

/**
 * The type Tor attachment view test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class TorAttachmentViewTest {

  @Autowired
  TorAttachmentViewRepository repository;

  @Test
  public void findAll() {
    repository.findAll();
  }
}
