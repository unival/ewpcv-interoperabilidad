package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.jdbc.SqlScriptsTestExecutionListener;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.Date;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgFactsheetDao;
import es.minsait.ewpcv.aprovisionamiento.model.*;

/**
 * The type Pkg factsheet test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, SqlScriptsTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgFactsheetTest {

  @Autowired
  IPkgFactsheetDao dao;

  @Before
  public void init() {

  }

  @Test
  @Ignore
  public void insertFactsheetTest() {

    FactsheetInstitution fi = new FactsheetInstitution();
    fi.setInstitutionId("UV.ES");
    fi.setAbreviation("Valencia");

    LanguageItem name = new LanguageItem();
    name.setText("Universidad de Valencia");
    name.setLang("ES");
    fi.setInstitutionName(Arrays.asList(name));

    Factsheet f = new Factsheet();
    f.setDecissionWeekLimit(3);
    f.setTorWeekLimit(3);
    f.setNominationsAutumTerm(new Date());
    f.setNominationsSpringTerm(new Date());
    f.setApplicationAutumTerm(new Date());
    f.setApplicationSpringTerm(new Date());
    fi.setFactsheet(f);

    InformationItem applicationInfo = new InformationItem();
    applicationInfo.setEmail("a@a.com");
    LanguageItem appUrl = new LanguageItem();
    appUrl.setText("a.com");
    appUrl.setLang("ES");
    applicationInfo.setUrls(Arrays.asList(appUrl));
    PhoneNumber phone = new PhoneNumber();
    phone.setE164("+34666666666");
    applicationInfo.setPhone(phone);
    fi.setApplicationInfo(applicationInfo);

    InformationItem housingInfo = new InformationItem();
    housingInfo.setEmail("b@b.com");
    LanguageItem housingUrl = new LanguageItem();
    housingUrl.setText("b.com");
    housingUrl.setLang("ES");
    housingInfo.setUrls(Arrays.asList(housingUrl));
    fi.setHousingInfo(housingInfo);

    InformationItem visaInfo = new InformationItem();
    visaInfo.setEmail("c@c.com");
    LanguageItem visaUrl = new LanguageItem();
    visaUrl.setText("c.com");
    visaUrl.setLang("ES");
    visaInfo.setUrls(Arrays.asList(visaUrl));
    fi.setVisaInfo(visaInfo);

    InformationItem insuranceInfo = new InformationItem();
    insuranceInfo.setEmail("d@d.com");
    LanguageItem insUrl = new LanguageItem();
    insUrl.setText("d.com");
    insUrl.setLang("ES");
    insuranceInfo.setUrls(Arrays.asList(insUrl));
    fi.setInsuranceInfo(insuranceInfo);

    TypedInformationItem additionalInfo = new TypedInformationItem();
    additionalInfo.setInfoType("adicional");
    InformationItem info = new InformationItem();
    info.setEmail("e@e.com");
    LanguageItem add1Url = new LanguageItem();
    add1Url.setText("e.com");
    add1Url.setLang("ES");
    info.setUrls(Arrays.asList(add1Url));
    PhoneNumber phoneInfo = new PhoneNumber();
    phoneInfo.setE164("+34777777777");
    info.setPhone(phoneInfo);
    additionalInfo.setInformationItem(info);

    TypedInformationItem additionalInfo2 = new TypedInformationItem();
    additionalInfo2.setInfoType("adicional2");
    InformationItem info2 = new InformationItem();
    info2.setEmail("f@f.com");
    LanguageItem add2Url = new LanguageItem();
    add2Url.setText("f.com");
    add2Url.setLang("ES");
    info2.setUrls(Arrays.asList(add2Url));
    PhoneNumber phoneInfo2 = new PhoneNumber();
    phoneInfo2.setE164("+34888888888");
    info2.setPhone(phoneInfo2);
    additionalInfo2.setInformationItem(info2);

    fi.setAdditionalInfo(Arrays.asList(additionalInfo, additionalInfo2));

    RespuestaPkg respuesta = dao.insertFactsheet(fi);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
  }

  @Test
  @Ignore
  public void deleteFactsheetTest() {
    RespuestaPkg respuesta = dao.deleteFactsheet("hola hola");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
  }
}
