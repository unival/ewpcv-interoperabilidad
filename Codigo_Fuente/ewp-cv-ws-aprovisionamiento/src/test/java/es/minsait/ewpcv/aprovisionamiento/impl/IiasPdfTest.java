package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaPdfViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

/**
 * The type Iias test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class IiasPdfTest {

  @Autowired
  IIiaPdfViewDao dao;

  @Before
  public void init() {
  }

  @Test
  public void findByFilter() {
    final PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    final Map<String, Object> paramDentroFilter = new HashMap<>();
    paramDentroFilter.put("iiaCode", "IIA_TEST_CODE");
    paramDentroFilter.put("iiaId", "dbe8021f-495f-6999-e053-1fc616ac4d81");
    paramFilter.put("OR", paramDentroFilter);


    final Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "iiaCode");
    assertNotNull(list);
  }
}
