package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgDictionaryDao;
import es.minsait.ewpcv.aprovisionamiento.model.Dictionary;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.util.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgDictionaryTest {

    @Autowired
    IPkgDictionaryDao dao;

    @Before
    public void init() {

    }

    private Dictionary generaDictionary() {
        final Dictionary dictionary = new Dictionary();
        dictionary.setCode("DICTIONARY_CODE");
        dictionary.setEntryDescription("DICTIONARY_DESCRIPTION");
        dictionary.setDictionaryType("DICTIONARY_TYPE");
        dictionary.setCallYear("2019");
        return dictionary;
    }

    @Test
    public void testInsertDictionary() {

        final Dictionary dictionary = generaDictionary();

        final RespuestaPkg respuesta = dao.insertDictionary(dictionary);
        System.out.println("***retorno: " + respuesta.getCodRetorno());
        System.out.println("***Error: " + respuesta.getError());
        System.out.println("***ID: " + respuesta.getId());
        Assert.isNull(respuesta.getError(), respuesta.getError());
    }

    @Test
    public void testUpdateDictionary() {

        final Dictionary dictionary = generaDictionary();

        dictionary.setCode("DICTIONARY_CODE_MODIFICADO");
        dictionary.setEntryDescription("DICTIONARY_DESCRIPTION_MODIFICADO");
        dictionary.setDictionaryType("DICTIONARY_TYPE_MODIFICADO");
        dictionary.setCallYear("2022");
        final String id = "10f3b6ea-9da4-a83a-e063-011a160a9792";

        final RespuestaPkg respuesta = dao.updateDictionary(id, dictionary);
        System.out.println("***retorno: " + respuesta.getCodRetorno());
        System.out.println("***Error: " + respuesta.getError());
        Assert.isNull(respuesta.getError(), "dictionaryID " + id + " error: " + respuesta.getError());
    }

    @Test
    public void testDeleteDictionary() {
        final String id = "10f3db8b-fd05-a9e6-e063-011a160a1ed7";
        final RespuestaPkg respuesta = dao.deleteDictionary(id);
        System.out.println("***retorno: " + respuesta.getCodRetorno());
        System.out.println("***Error: " + respuesta.getError());
        Assert.isNull(respuesta.getError(), "dictionaryID " + id + " error: " + respuesta.getError());
    }
}
