package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IEventViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type Event test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class EventTest {

  @Autowired
  IEventViewDao dao;

  @Before
  public void init() {

  }

  @Test
  public void findByFilter() {
    PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    paramFilter.put("triggeringHei", "UV.ES");

    Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "eventDate");
    assertNotNull(list);
  }
}
