package es.minsait.ewpcv.aprovisionamiento.impl;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;

/**
 * The type Iias test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class IiasTest {

  @Autowired
  IIiaViewDao dao;

  @Before
  public void init() {}

  @Test
  public void findByFilter() {
    final PageFilterDTO filter = new PageFilterDTO();
    filter.setPageTotal(5);

    final Map<String, Object> paramFilter = new HashMap<>();
    final Map<String, Object> paramDentroFilter = new HashMap<>();
    paramDentroFilter.put("iiaCode", "IIA_TEST_CODE");
    paramDentroFilter.put("iiaId", "ceec7e32-c6c6-2917-e053-1fc616acc04b");
    paramFilter.put("OR", paramDentroFilter);

    final Map<String, Object> datesFilterFrom = new HashMap<>();
    datesFilterFrom.put("iiaStartDate", "22/10/21 08:55:21");
    final Map<String, Object> datesFilterTo = new HashMap<>();
    datesFilterTo.put("iiaEndDate", "22/10/21 08:55:21");


    final Map<String, Object> datesFilter = new HashMap<>();
    datesFilter.put("from", datesFilterFrom);
    datesFilter.put("to", datesFilterTo);

    final Map<Object, Object> list = dao.findByFilter(filter, paramFilter, datesFilter, null, "iiaCode");
    assertNotNull(list);
  }
}
