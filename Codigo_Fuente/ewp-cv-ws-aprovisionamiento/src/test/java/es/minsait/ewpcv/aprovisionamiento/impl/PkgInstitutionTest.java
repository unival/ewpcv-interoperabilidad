package es.minsait.ewpcv.aprovisionamiento.impl;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.util.Assert;

import java.util.Arrays;

import es.minsait.ewpcv.aprovisionamiento.dao.IPkgInstitutionDao;
import es.minsait.ewpcv.aprovisionamiento.model.Institution;
import es.minsait.ewpcv.aprovisionamiento.model.LanguageItem;
import es.minsait.ewpcv.aprovisionamiento.model.RespuestaPkg;

/**
 * The type Pkg institution test.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class PkgInstitutionTest {

  @Autowired
  IPkgInstitutionDao dao;

  @Test
  public void testInsertInstitution() {
    final LanguageItem li = new LanguageItem();
    li.setLang("ES");
    li.setText("Institution de prueba santi");

    final Institution institution = new Institution();
    institution.setInstitutionID("santi.univ");
    institution.setInstitutionName(Arrays.asList(li));



    final RespuestaPkg respuesta = dao.insertInstitution(institution);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testUpdateInstitution() {
    final LanguageItem li = new LanguageItem();
    li.setLang("ES");
    li.setText("Institution de prueba santi");

    final Institution institution = new Institution();
    institution.setInstitutionID("santi.univ");
    institution.setInstitutionName(Arrays.asList(li));
    institution.setAbbreviation("Univ. Santi");



    final RespuestaPkg respuesta = dao.updateInstitution("santi.univ", institution);
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

  @Test
  public void testDeleteInstitution() {
    final RespuestaPkg respuesta = dao.deleteInstitution("santi.univ");
    System.out.println("***retorno: " + respuesta.getCodRetorno());
    System.out.println("***Error: " + respuesta.getError());
    System.out.println("***ID: " + respuesta.getId());
    Assert.isNull(respuesta.getError(), respuesta.getError());
  }

}
