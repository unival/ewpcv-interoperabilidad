package es.minsait.ewpcv.aprovisionamiento.impl;

import es.minsait.ewpcv.aprovisionamiento.dao.IIiaApprovalsViewDao;
import es.minsait.ewpcv.aprovisionamiento.dto.PageFilterDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@Ignore
public class IiaApprovalsViewTest {

    @Autowired
    IIiaApprovalsViewDao dao;

    @Before
    public void init() {

    }

    @Test
    public void findByFilter() {
        PageFilterDTO filter = new PageFilterDTO();
        filter.setPageTotal(5);

        final Map<String, Object> paramFilter = new HashMap<>();
        paramFilter.put("localIiaId", "l_iia_id_1");

        Map<Object, Object> list = dao.findByFilter(filter, paramFilter, null, null, "localIiaId");
        assertNotNull(list);
    }
}
