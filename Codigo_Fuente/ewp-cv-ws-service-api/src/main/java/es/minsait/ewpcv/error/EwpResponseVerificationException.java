package es.minsait.ewpcv.error;

/**
 * The type Ewp not found exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpResponseVerificationException extends Exception {

  public EwpResponseVerificationException(String message) {
    super(message);
  }

}
