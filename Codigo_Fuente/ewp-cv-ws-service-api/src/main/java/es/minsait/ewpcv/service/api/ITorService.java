package es.minsait.ewpcv.service.api;

import java.util.List;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpNotFoundException;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;

/**
 * The interface Tor service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface ITorService {

    List<String> obtenerMobilitiesIds(String receivingHeiId, List<String> sendingHeisIds, String modifiedSince,
                                      List<String> receivingHeiIdToFilter);

    MobilityMDTO obtenerMobilityWithTor(String mobilityId);

    List<ImobilityTorsGetResponse.Tor> findTorByOmobilityIdAndReceivingInstitutionId(String receivingHei,
                                                                                     List<String> omobilityId, List<String> heisCoveredByClient) throws EwpConverterException;

    boolean actualizarTor(ImobilityTorsGetResponse.Tor tor, String sendingHeiId);

    void guardarMobilityTorCopiaRemota(ImobilityTorsGetResponse.Tor remoto, String mobilityId, String sendingHeiId)
            throws EwpNotFoundException;
}
