package es.minsait.ewpcv.service.api;

import es.minsait.ewpcv.repository.model.config.AuditConfigMDTO;

/**
 * The interface Audit config service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IAuditConfigService {

  AuditConfigMDTO findByName(String name);

}
