package es.minsait.ewpcv.converter.api;


import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasOutgoingStatsResponse;

import java.util.List;

/**
 * Interface Statitics converter.
 *
 * @author (ilanciano@minsait.com)
 */
public interface IStatisticsOlaConverter {

    LasOutgoingStatsResponse.AcademicYearLaStats convertToStatsResponse(StatisticsOlaMDTO mdto);
    public List<LasOutgoingStatsResponse.AcademicYearLaStats> convertToStatsResponses(List<StatisticsOlaMDTO> mdtos);
}
