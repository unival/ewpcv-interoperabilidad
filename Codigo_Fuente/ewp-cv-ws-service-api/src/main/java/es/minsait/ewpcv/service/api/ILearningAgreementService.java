package es.minsait.ewpcv.service.api;

import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;

/**
 * The interface Learning agreement service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface ILearningAgreementService {

  void eliminarLA(String changedElementIds);

  boolean compararAcademicTerm(AcademicTermMDTO remoto, AcademicTermMDTO local);

  LearningAgreementMDTO buscarLAPorChangesId(String mobilityId, String changesId);

  boolean compararLearningAgreements(LearningAgreementMDTO item, LearningAgreementMDTO i);
}
