package es.minsait.ewpcv.error;

/**
 * The type Ewp converter exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpConverterException extends Exception {

  public EwpConverterException(String message) {
    super(message);
  }

}
