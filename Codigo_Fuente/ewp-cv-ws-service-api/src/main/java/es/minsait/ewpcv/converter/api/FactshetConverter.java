package es.minsait.ewpcv.converter.api;

import java.util.List;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;

/**
 * The interface Factshet converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface FactshetConverter {

  List<FactsheetResponse.Factsheet> convertToFactsheet(List<InstitutionMDTO> institudtions)
      throws EwpConverterException;

  List<InstitutionMDTO> convertToInstitutionMDTO(List<FactsheetResponse.Factsheet> factsheets);

  InstitutionMDTO convertToInstitutionMDTO(FactsheetResponse.Factsheet factsheets);
}
