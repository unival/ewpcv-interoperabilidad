package es.minsait.ewpcv.service.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;

/**
 * The interface Institution service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionService {

  void actualizarInstitutions(List<InstitutionsResponse.Hei> hei);

  List<InstitutionsResponse.Hei> obtenerInstitutionsInformation(List<String> heisIds);

  boolean isKnownHei(String heiId);


  void actualizarInstitutionsOunits(String heiId, List<OrganizationUnitMDTO> ounits);

  void addOunitToInstitution(OrganizationUnitMDTO ounit, String heiId);
}
