package es.minsait.ewpcv.converter.api;

import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasIncomingStatsResponse;

import java.util.List;


/**
 * Interface Statitics ILA converter.
 *
 * @author (ilanciano@minsait.com)
 */
public interface IStatisticsILAConverter {

    public LasIncomingStatsResponse.AcademicYearLaStats convertToStatsResponse(StatisticsILAMDTO mdto);

    List<LasIncomingStatsResponse.AcademicYearLaStats> convertToStatsResponses(List<StatisticsILAMDTO> mdtos);

}
