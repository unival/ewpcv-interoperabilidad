package es.minsait.ewpcv.service.api;

import java.util.List;

import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;

/**
 * The interface Iias approval service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiasApprovalService {

  List<IiasApprovalResponse.Approval> findIiasByRemoteIiaIdAndPartners(String aprovingHeiId, String ownerHeiId,
      List<String> iiaId);

}
