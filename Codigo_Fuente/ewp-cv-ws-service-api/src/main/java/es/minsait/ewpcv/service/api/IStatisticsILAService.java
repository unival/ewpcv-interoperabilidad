package es.minsait.ewpcv.service.api;

import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasIncomingStatsResponse;

import java.util.List;

/**
 * The interface StatisticsILA service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IStatisticsILAService {

    void obtainStatistics (String user);

     List<LasIncomingStatsResponse.AcademicYearLaStats> getStatisticsRest();
}
