package es.minsait.ewpcv.service.api;


import es.minsait.ewpcv.registryclient.HeiEntry;

/**
 * The interface Institution identifiers service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionIdentifiersService {

  boolean existInstitutionBySchac(final String schac);

  void saveOrUpdateInstitutionIdentifier(HeiEntry heiEntry);

}
