package es.minsait.ewpcv.error;

/**
 * The type Ewp not found exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpNotFoundException extends Exception {

  public EwpNotFoundException(String message) {
    super(message);
  }

}
