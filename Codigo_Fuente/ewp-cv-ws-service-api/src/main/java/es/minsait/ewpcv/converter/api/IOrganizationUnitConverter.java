package es.minsait.ewpcv.converter.api;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import eu.erasmuswithoutpaper.api.types.contact.Contact;

/**
 * The interface Organization unit converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOrganizationUnitConverter {

  List<OunitsResponse.Ounit> convertoToOunitList(List<OrganizationUnitMDTO> ounits,
      Map<String, List<ContactMDTO>> contactsMap);

  OunitsResponse.Ounit convertToOunit(OrganizationUnitMDTO mdto, List<ContactMDTO> contacts);

  List<OrganizationUnitMDTO> convertToOrganizationUnitMDTOList(final List<OunitsResponse.Ounit> ounits);

  OrganizationUnitMDTO convertToOrganizationUnitMDTO(OunitsResponse.Ounit ounit);

  List<ContactMDTO> convertToContactMDTOList(List<Contact> contacts, String organizationUnitId, String heiId);
}
