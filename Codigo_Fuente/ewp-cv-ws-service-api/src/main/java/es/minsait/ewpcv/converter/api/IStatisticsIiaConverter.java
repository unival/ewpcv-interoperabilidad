package es.minsait.ewpcv.converter.api;

import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasStatsResponse;

/**
 * Interface Statitics Iia converter.
 *
 * @author (ilanciano@minsait.com)
 */
public interface IStatisticsIiaConverter {

    IiasStatsResponse convertToStatsResponse(StatisticsIiaMDTO mdto);
}
