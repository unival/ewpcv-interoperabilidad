package es.minsait.ewpcv.service.api;

import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;

/**
 * The interface Event service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IEventService {

  void save(EventMDTO event);
}
