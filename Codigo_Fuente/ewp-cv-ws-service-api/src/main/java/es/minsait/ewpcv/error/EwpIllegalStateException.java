package es.minsait.ewpcv.error;

/**
 * The type Ewp illegal state exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpIllegalStateException extends Exception {

  public EwpIllegalStateException(String message) {
    super(message);
  }

}
