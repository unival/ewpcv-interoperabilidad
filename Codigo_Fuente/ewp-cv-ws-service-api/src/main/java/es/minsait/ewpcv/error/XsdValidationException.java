package es.minsait.ewpcv.error;

/**
 * The type XsdValidationException exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class XsdValidationException extends Exception{

      public XsdValidationException(String message) {
     super(message);
      }

}
