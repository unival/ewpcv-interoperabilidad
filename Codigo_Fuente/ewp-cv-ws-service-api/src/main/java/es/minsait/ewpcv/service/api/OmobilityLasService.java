package es.minsait.ewpcv.service.api;

import java.text.ParseException;
import java.util.List;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.organization.FactSheetMDTO;
import es.minsait.ewpcv.repository.model.organization.InformationItemMDTO;
import es.minsait.ewpcv.repository.model.organization.RequirementsInfoMDTO;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;

/**
 * The interface Omobility las service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface OmobilityLasService {

  List<LearningAgreement> findOMobilitiesByIdAndSendingInstitutionId(String sendingHeiId,
      final List<String> receivingHeiIdList, List<String> oMobilityIdList) throws EwpConverterException;

  List<String> findLocalOMobilitiesId(String sendingHeiId, String receivingHei, String modifiedSince)
      throws ParseException;

  List<String> getLaIdsByCriteria(String sendingHeiId, List<String> receivingHeiId, String receivingAcademicYearId,
      String globalId, String mobilityType, String modifiedSince);

  boolean isMatchSendingHeiIdAndOmobilityId(String sendingHeiId, String oMobilityId);

  Boolean insertarOmobilities(LearningAgreement remoto, String ewpInstance) throws EwpConverterException;

  Boolean actualizarOmobilities(LearningAgreement remoto, MobilityMDTO local, String ewpInstance)
      throws EwpConverterException;

  void eliminarMobilityByIdAndSendingHeiId(String id, final String sendingHeiId);

  MobilityMDTO findLastMobilityRevisionByIdAndSendingHeiId(final String id, final String sendingHeiId);

  String findLastMobilityRevisionReceiverHeiByIdAndSendingHei(String mobilityId, String sendingHeiId);

  boolean compararFactSheet(FactSheetMDTO remoto, FactSheetMDTO local);

  boolean compararAdditionalRequirementList(List<RequirementsInfoMDTO> remoto, List<RequirementsInfoMDTO> local);

  boolean compararInformationItemList(List<InformationItemMDTO> remoto, List<InformationItemMDTO> local);

  List<String> getIdsByCriteria(String sendingHeiId, List<String> receivingHeiId, String receivingAcademicYearId,
      String modifiedSince);

  List<StudentMobilityForStudies> findStudentOmobilityByIdAndSendingInstitutionIdAndLastRevision(String sendingHeiId,
      List<String> receivingHeiIdList, List<String> omobilityId) throws EwpConverterException;

  boolean actualizarOmobility(StudentMobilityForStudies mobilityForStudies, MobilityMDTO local, String ewpInstance) throws EwpConverterException;

  List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> findStudentImobilityByIdAndReceivingInstitutionId(
      String receivingHeiId, final List<String> sendingHeiIdList, List<String> omobilityId)
      throws EwpConverterException;

  boolean actualizarImobility(
      eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies mobilityForStudies, String ewpInstance);

  Boolean guardarOmobilityCopiaRemota(StudentMobilityForStudies remoto, String ewpInstance);

  List<String> findImobilitiesARefrescar(String sendingHei);

}
