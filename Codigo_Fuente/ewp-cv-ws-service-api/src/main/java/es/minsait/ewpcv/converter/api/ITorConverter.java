package es.minsait.ewpcv.converter.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;

/**
 * The interface Tor converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface ITorConverter {

  List<ImobilityTorsGetResponse.Tor> convertToImobilityTor(List<MobilityMDTO> mobilities);

  List<MobilityMDTO> convertToTorMDTOList(List<ImobilityTorsGetResponse.Tor> tors);
}
