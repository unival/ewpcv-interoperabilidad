package es.minsait.ewpcv.converter.api;

import es.minsait.ewpcv.registryclient.HeiEntry;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;

/**
 * The interface Institution identifiers converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionIdentifiersConverter {

  InstitutionIdentifiersMDTO convertToInstitutionIdentifiersMDTO(HeiEntry heiEntry);
}
