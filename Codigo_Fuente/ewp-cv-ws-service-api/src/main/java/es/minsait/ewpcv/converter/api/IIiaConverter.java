package es.minsait.ewpcv.converter.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;

/**
 * The interface Iia converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaConverter {

  List<IiasGetResponse.Iia> convertToIias(String hei_id, List<IiaMDTO> iiaList);

  List<IiaMDTO> convertToIiaMDTO(String hei_id, List<IiasGetResponse.Iia> iiaList, boolean sendPdf);
}
