package es.minsait.ewpcv.service.api;

import es.minsait.ewpcv.error.EwpIiaSnapshotException;
import es.minsait.ewpcv.error.EwpOperationNotAllowedException;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * The interface Iias service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiasService {

  List<String> getIiaIdsByCriteria(String heiId, List<String> partnerHeiId, List<String> receivingAcademicYearId,
      String modifiedSince);

  List<IiasGetResponse.Iia> getIiaListByCriteria(String heiId, List<String> partnerHeiId, List<String> iiaId);

  Boolean guardarIiaCopiaRemota(IiasGetResponse.Iia remoto, String ewpInstance);

  Boolean actualizarIiaCopiaRemota(IiasGetResponse.Iia iiaRemoto, IiaMDTO iiaCopiaRemota, String ewpInstance);

  boolean compararLanguageItems(LanguageItemMDTO remoto, LanguageItemMDTO local, boolean actualizarLocal);

  IiaMDTO buscaPorIiaRemotoAndIsRemote(String remoteIiaId, Boolean isRemote);

  List<String> buscaRemoteIdPorIsRemoteAndInstitutions(String sendingHei, String receivingPartner, Boolean isRemote);

  void eliminarIiaRemoto(String remoteIiaId);

  Boolean processRemoteIiaDeletion(String remoteIiaId, String ownerHeiId, String heiId, String ewpInstance, boolean iiaDeleteEnabled, EventMDTO event) throws EwpOperationNotAllowedException;

  IiaMDTO findByInteropId(String interopId);

  boolean compararHash(IiasGetResponse.Iia iia, String remoteIiaId);

  void actualizarApprovalCoopCondHash(String changedElementIds, String conditionsHash);

  IiaMDTO save(IiaMDTO iia);

  void updateIiaApprovalByInteropId(String interopId, String approvalConditionHash, Date approvalDate);

  boolean compararLanguageItemsList(List<LanguageItemMDTO> remoto, List<LanguageItemMDTO> local,
      Boolean actualizarLocal);

  boolean compararContacts(ContactMDTO remoto, ContactMDTO local, boolean actualizarLocal);

  boolean compararContactsList(List<ContactMDTO> remoto, List<ContactMDTO> local, List<ContactMDTO> elementosABorrar);

  boolean compararContactDetails(ContactDetailsMDTO remoto, ContactDetailsMDTO local, boolean actualziarLocal);

  boolean esContactDetailsVacio(final ContactDetailsMDTO contactDetails);

  void updateCoopCondHash(List<IiasGetResponse.Iia> iiaIdsList, String ewpInstance);

  boolean compararAdditionalRequeriments(List<RequirementsInfoMDTO> remoto, List<RequirementsInfoMDTO> local);

  boolean compararInformationItems(List<InformationItemMDTO> remoto, List<InformationItemMDTO> local,
                                   boolean actualizarLocal);

  boolean generarCopiaAutogeneradaIia(IiasGetResponse.Iia remoto, String ewpInstance,
      boolean autogenerateWithCandidates);

  boolean verificarHashRemoto(IiasGetResponse.Iia iia);

  @Transactional(readOnly = false)
  void takeSnapshot(String remoteIiaId, EventMDTO event, String iiaApiVersion, String ewpInstance, byte[] remoteMessage) throws EwpIiaSnapshotException, IOException;

  void takeSnapshotWithProvidedRemoteMessageInEvent(String remoteIiaId, EventMDTO event, String iiaApiVersion, String ewpInstance) throws EwpIiaSnapshotException, IOException;

  @Transactional(readOnly = false)
  String proccesRegression(IiasGetResponse.Iia remoto, String antiguoHashIiaRemoto);

  boolean verificarValidoParaAprovar(IiasGetResponse.Iia iia);
}
