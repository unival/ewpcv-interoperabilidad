package es.minsait.ewpcv.error;

/**
 * The type Ewp not found exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpIiaSnapshotException extends Exception {

  public EwpIiaSnapshotException(String message) {
    super(message);
  }

}
