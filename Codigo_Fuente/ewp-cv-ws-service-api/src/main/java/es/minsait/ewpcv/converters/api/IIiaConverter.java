package es.minsait.ewpcv.converters.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import eu.erasmuswithoutpaper.api.iias.endpoints.IiasGetResponse;


/**
 * The interface Iia converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaConverter {

  List<IiasGetResponse.Iia> convertToIias(String hei_id, List<IiaMDTO> iiaList);

  IiasGetResponse.Iia convertSingleIia(String hei_id, IiaMDTO iia);

}
