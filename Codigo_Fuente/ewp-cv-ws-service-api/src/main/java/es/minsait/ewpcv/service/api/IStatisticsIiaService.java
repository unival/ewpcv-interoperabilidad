package es.minsait.ewpcv.service.api;


import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasStatsResponse;

/**
 * The interface StatisticsIia service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IStatisticsIiaService {
    void obtainStatistics ();

    IiasStatsResponse getStatisticsRest();

}
