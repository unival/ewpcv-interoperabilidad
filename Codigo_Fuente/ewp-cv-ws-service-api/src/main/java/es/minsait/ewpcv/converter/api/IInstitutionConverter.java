package es.minsait.ewpcv.converter.api;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import eu.erasmuswithoutpaper.api.types.contact.Contact;

/**
 * The interface Institution converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionConverter {

  List<InstitutionsResponse.Hei> convertoToHeisList(List<InstitutionMDTO> institutions,
      List<InstitutionIdentifiersMDTO> institutionsIds, Map<String, List<ContactMDTO>> contacts);

  InstitutionsResponse.Hei convertToHei(InstitutionMDTO institution, InstitutionIdentifiersMDTO institutionId,
      List<ContactMDTO> contacts);

  InstitutionMDTO convertToInstitutionMDTO(InstitutionsResponse.Hei hei);

  List<ContactMDTO> convertToContactMDTOList(List<Contact> contact, String institutionId);
}
