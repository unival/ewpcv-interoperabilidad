package es.minsait.ewpcv.service.api;

import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;

/**
 * The interface FileApi service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

public interface IFileApiService {

  FileMDTO getFileByIdAndScach(String id, String schac);

  void insertFile(byte[] fileData, String fileId, String schac, String mimeType);

  boolean existsFile(String pdfFile, String heiId);
}
