package es.minsait.ewpcv.service.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;

/**
 * The interface Ounit service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOunitService {

  List<OunitsResponse.Ounit> obtenerOunits(String heiId, List<String> ounitsIds, List<String> ounitsCodes);

  List<OrganizationUnitMDTO> actualizarOrganizationUnits(final String heiId, final List<OunitsResponse.Ounit> remoto,
      String rootOunitId);

  boolean existsOUnit(String ounitId, String institutionSchac);
}
