package es.minsait.ewpcv.error;

/**
 * The type Ewp incorrect hash exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpOperationNotAllowedException extends Exception {
  private static final long serialVersionUID = 3574904948728010133L;


  public EwpOperationNotAllowedException(final String message) {
    super(message);
  }

}
