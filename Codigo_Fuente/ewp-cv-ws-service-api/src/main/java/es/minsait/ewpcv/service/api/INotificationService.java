package es.minsait.ewpcv.service.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;

/**
 * The interface Notification service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
public interface INotificationService {

  void saveNotificationIiaRefresh(String notifierHeiId, String iiaId, String ownerHeiId);

  void saveNotificationIiaNotify(String notifierHeiId, String iiaId, String ownerHeiId);

  void saveNotificationIiaApprovalRefresh(String notifierHeiId, String iiaId, String ownerHeiId);

  void saveNotificationIiaApprovalNotify(String notifierHeiId, String iiaId, String ownerHeiId);

  void saveNotificationOmobilityLasRefresh(String sendingHeiId, List<String> mobilityIdList, String ewpInstance);

  void setNotProcessingAndNotRepeteable(NotificationMDTO notificacion, String lastError);

  List<NotificationMDTO> reservarNotificacionesEnviar(NotificationTypes tipoNotificacion, int retriesDelay,
      int blockSize);

  List<NotificationMDTO> reservarNotificacionesRecibidas(int retriesDelay, int blockSize, NotificationTypes tipo);

  void liberarNotificaciones(NotificationMDTO notificacion, String lastError);

  void eliminarNotificacion(NotificationMDTO notificacion);

  void purgarNotificaciones(Long maxProcessingTime);

  void saveNotificationOmobilityRefresh(String sendingHeiId, String id, String instance);

  void saveNotificationImobilityRefresh(String receivingHeiId, String omobiityId, String ownerHeiId);

  void saveNotificationTorRefresh(String receivingHeiId, String omobiityId, String ownerHeiId);

  void saveNotificationOmobilityLA(String sendingHeiId, String omobiityId, String receivingHeiId);

  boolean existsPendingNotification(String changedElementIds, NotificationTypes tipoObjeto, String heiId);
}