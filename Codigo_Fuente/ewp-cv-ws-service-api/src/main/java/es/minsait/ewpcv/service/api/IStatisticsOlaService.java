package es.minsait.ewpcv.service.api;


import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasOutgoingStatsResponse;

import java.util.List;

/**
 * The interface StatisticsOla service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

public interface IStatisticsOlaService {

    void obtainStatistics (String user);

    List<LasOutgoingStatsResponse.AcademicYearLaStats> getStatisticsRest();

}
