package es.minsait.ewpcv.service.api;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.xml.bind.JAXBException;

import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestType;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateRequest;

/**
 * The interface Mobility update request service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IMobilityUpdateRequestService {


  @Transactional(readOnly = false)
  void updateRequestOutgoing(OmobilityLasUpdateRequest updateRequest, MobilityUpdateRequestType type) throws Exception;


  List<MobilityUpdateRequestMDTO> reservarMobilityUpdateRequest(UpdateRequestMobilityTypeEnum tipo, int maxRetries,
      Integer blockSize);

  void updateRequestIncoming(OmobilityLasUpdateRequest updateRequest, MobilityUpdateRequestType type) throws Exception;

  void deleteMobillityUpdateRequest(String id);

  void saveMobilityUpdateRequest(String sendingHeiId, byte[] requestUpdate, MobilityUpdateRequestType mobilityType);

  @Transactional
  void updateShouldRetryLastError(String id,String errorMessage);

  void changeStatusIsProcessingUpdateRequest(List<String> updateRequestList, boolean isProcessing);

  @Transactional
  void changeStatusIsNotProcessingAndRetriesUpdateRequest(List<String> updateRequestList);

  <T> T bytesToClassImplByClass(byte[] pFicheroXml, Class<?> claseJava) throws JAXBException;

  void purgarUpdateRequests(Long maxProcessingTime);
}
