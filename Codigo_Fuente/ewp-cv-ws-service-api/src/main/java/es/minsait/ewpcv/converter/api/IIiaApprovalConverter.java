package es.minsait.ewpcv.converter.api;

import java.util.List;

import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;

/**
 * The interface Iia approval converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaApprovalConverter {

  List<IiasApprovalResponse.Approval> convertToIiasApproval(List<IiaMDTO> iiaList, String hei_id);

}
