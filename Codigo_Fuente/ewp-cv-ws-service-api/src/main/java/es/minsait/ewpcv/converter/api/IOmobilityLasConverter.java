package es.minsait.ewpcv.converter.api;

import java.util.List;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;

/**
 * The interface Omobility las converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOmobilityLasConverter {

  List<LearningAgreement> convertToOmobilities(List<MobilityMDTO> omobilityList) throws EwpConverterException;

  MobilityMDTO convertToOmobilityMDTO(LearningAgreement learningAgreement, String laId, Integer maxRevision)
      throws EwpConverterException;

  List<StudentMobilityForStudies> convertToStudentMobilitiesForStudies(List<MobilityMDTO> mobilities);

  StudentMobilityForStudies convertToStudentMobilityForStudies(MobilityMDTO mdto);

  List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> convertToImobilityStudentMobilitiesForStudies(
      List<MobilityMDTO> mobilities);

  MobilityMDTO convertToOmobilityMDTO(
      eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies mobilityForStudies);

  MobilityMDTO convertToOmobilityMDTO(StudentMobilityForStudies mobilityForStudies);
}
