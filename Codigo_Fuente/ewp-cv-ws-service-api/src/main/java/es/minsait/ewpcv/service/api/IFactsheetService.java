package es.minsait.ewpcv.service.api;

import java.util.List;

import es.minsait.ewpcv.error.EwpConverterException;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;

/**
 * The interface Factsheet service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IFactsheetService {

  List<FactsheetResponse.Factsheet> obtenerFactsheets(List<String> heisIds) throws EwpConverterException;

  void actualizarFactsheets(FactsheetResponse.Factsheet factsheet);
}
