package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.IncommingLasUpdateRequestJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Incomming las update request job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class IncommingLasUpdateRequestJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailIncommingLasUpdateRequestJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(IncommingLasUpdateRequestJob.class);
    jobDetailFactory.setDescription("Batch para envio de solicitudes de actualizacion de Learning Agreement");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerIncommingLasUpdateRequestJob(
      @Qualifier("jobDetailIncommingLasUpdateRequestJob") final JobDetail jobDetail,
      @Value("${ewp.las.update.incoming.cron}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
