package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.config.ManifestConfig;
import eu.erasmuswithoutpaper.api.discovery.Manifest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Manifest controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/discovery", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class ManifestController {

  @Autowired
  ManifestConfig manifestConfig;

  @GetMapping
  public ResponseEntity<Manifest> getManifest() {
    log.debug("Inicio invocacion discovery/manifest");
    final Manifest m = manifestConfig.ManifestReader();
    log.debug("Respuesta discovery/manifest");
    return ResponseEntity.ok(m);
  }

}
