package es.minsait.ewpcv.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.common.HttpMethodEnum;

/**
 * The type Client request.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class ClientRequest implements Serializable {
  private String url;
  private String heiId;
  private HttpMethodEnum method;
  private Map<String, List<String>> params = new HashMap<>();
  private String xmlString;

  private boolean httpsecClient = false;
  private boolean httpsecServer = false;
  private boolean tlssec = false;

  private String apiName;

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public HttpMethodEnum getMethod() {
    return method;
  }

  public void setMethod(final HttpMethodEnum method) {
    this.method = method;
  }

  public String getHeiId() {
    return heiId;
  }

  public void setHeiId(final String heiId) {
    this.heiId = heiId;
  }

  public Map<String, List<String>> getParams() {
    return params;
  }

  public void setParams(final Map<String, List<String>> params) {
    this.params = params;
  }

  public boolean isHttpsecClient() {
    return httpsecClient;
  }

  public void setHttpsecClient(final boolean httpsecClient) {
    this.httpsecClient = httpsecClient;
  }

  public boolean isHttpsecServer() {
    return httpsecServer;
  }

  public void setHttpsecServer(final boolean httpsecServer) {
    this.httpsecServer = httpsecServer;
  }

  public String getXmlString() {
    return xmlString;
  }

  public void setXmlString(final String xmlString) {
    this.xmlString = xmlString;
  }

  public boolean isTlssec() {
    return tlssec;
  }

  public void setTlssec(boolean tlssec) {
    this.tlssec = tlssec;
  }

  public String getApiName() {
    return apiName;
  }

  public void setApiName(String apiName) {
    this.apiName = apiName;
  }
}

