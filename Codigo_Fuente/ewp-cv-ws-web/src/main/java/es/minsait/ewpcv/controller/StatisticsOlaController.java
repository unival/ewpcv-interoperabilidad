package es.minsait.ewpcv.controller;


import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IStatisticsOlaService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasOutgoingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * The StatisticsOla controller..
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@RestController
@RequestMapping(value = "/StatisticsOla", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class StatisticsOlaController {

    @Autowired
    GlobalProperties globalProperties;
    @Autowired
    private IStatisticsOlaService statisticsOlaService;
    @Autowired
    private Utils utils;
    @Autowired
    private IEventService eventService;

    @GetMapping()
    public ResponseEntity<LasOutgoingStatsResponse> getStatisticsOla(final HttpServletRequest request) {

        log.debug("Inicio invocacion GET StatisticsOla");

        final EventMDTO event = utils.initializeAuditableEvent(null, null, request, EwpEventConstants.STATISTICSOLA_GET,
                NotificationTypes.STATISTICS_OLA_NAMESPACE, null);

        final LasOutgoingStatsResponse respuesta = new LasOutgoingStatsResponse();

        if (Objects.nonNull(statisticsOlaService.getStatisticsRest())) {
            respuesta.getAcademicYearLaStats().addAll(statisticsOlaService.getStatisticsRest());
        }

        if (utils.getValue(EwpConstants.STATISTICSOLAUDIT)) {
            utils.setMessageToEventOK(event, respuesta);
        }

        eventService.save(event);

        log.debug("Fin invocacion GET StatisticsOla");
        return ResponseEntity.ok(respuesta);
    }



}
