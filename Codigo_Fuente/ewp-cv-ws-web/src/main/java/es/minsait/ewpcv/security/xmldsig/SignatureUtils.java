package es.minsait.ewpcv.security.xmldsig;

import com.google.common.base.Strings;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.crypto.*;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * The type Signature utils.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class SignatureUtils {

  public SignatureUtils() {

  }

  @SuppressWarnings({"rawtypes", "unchecked"})
  public String firma(final Document msg, final String rutaKeystore, final String keystorePassword, final String alias,
      final String aliasPassword, final String keystoreType, final String algoritmoFirma) throws Exception {

    // Create a DOM XMLSignatureFactory that will be used to
    // generate the enveloped signature.
    final XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

    // Create a Reference to the enveloped document (in this case,
    // you are signing the whole document, so a URI of "" signifies
    // that, and also specify the SHA1 digest algorithm and
    // the ENVELOPED Transform. Este es el elemento que queremos firmar, en nuestro caso el EMREX-ELMO entero
    final Reference ref = fac.newReference("", fac.newDigestMethod(DigestMethod.SHA1, null),
        Collections.singletonList(fac.newTransform(algoritmoFirma, (TransformParameterSpec) null)), null, null);

    // Create the SignedInfo.
    final SignedInfo si = fac.newSignedInfo(
        fac.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec) null),
        fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));

    // Load the KeyStore and get the signing key and certificate.
    final KeyStore ks = KeyStore.getInstance(keystoreType);
    ks.load(new FileInputStream(rutaKeystore), keystorePassword.toCharArray());
    final KeyStore.PrivateKeyEntry keyEntry =
        (KeyStore.PrivateKeyEntry) ks.getEntry(alias, new KeyStore.PasswordProtection(
            Strings.isNullOrEmpty(aliasPassword) ? "".toCharArray() : aliasPassword.toCharArray()));
    final X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

    // Create the KeyInfo containing the X509Data.
    final KeyInfoFactory kif = fac.getKeyInfoFactory();
    final List x509Content = new ArrayList();
    x509Content.add(cert.getSubjectX500Principal().getName());
    x509Content.add(cert);
    final X509Data xd = kif.newX509Data(x509Content);
    final KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));

    // Instantiate the document to be signed.
    final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    // final Document doc = dbf.newDocumentBuilder().parse(new FileInputStream("purchaseOrder.xml"));

    // Create a DOMSignContext and specify the RSA PrivateKey and
    // location of the resulting XMLSignature's parent element.
    final DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), msg.getDocumentElement());

    // Create the XMLSignature, but don't sign it yet.
    final XMLSignature signature = fac.newXMLSignature(si, ki);

    // Marshal, generate, and sign the enveloped signature.
    signature.sign(dsc);

    return new TransformUtils().document2String(msg);
  }

  public boolean validar(final Document msg) throws Exception {
    boolean valido = true;
    // Find Signature element
    final NodeList nl = msg.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
    if (nl.getLength() == 0) {
      throw new Exception("No se ha encontrado la firma");
    }
    // System.out.println("Firma encontrada");
    /* Create a DOM XMLSignatureFactory that will be used to unmarshal the document containing the XMLSignature */
    final XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
    // System.out.println("Factoria creada");
    /* Create a DOMValidateContext and specify a KeyValue KeySelector and document context */
    final DOMValidateContext valContext = new DOMValidateContext(new KeyValueKeySelector(), nl.item(0));
    // System.out.println("ValContext creado");
    /* Assignar elemento identificador de parte firmada */
    final Element elem =
        (Element) msg.getElementsByTagNameNS("http://schemas.xmlsoap.org/soap/envelope/", "Body").item(0);
    final Attr idAttr = elem.getAttributeNode("Id");
    elem.setIdAttributeNode(idAttr, true);
    valContext.setIdAttributeNS(elem, null, "Id");
    // System.out.println("Identificador encontrado");
    // unmarshal the XMLSignature
    final XMLSignature signature = fac.unmarshalXMLSignature(valContext);
    // System.out.println("Firma unmarshal");
    // Validate the XMLSignature (generated above)
    final boolean coreValidity = signature.validate(valContext);
    // System.out.println("Core validity: "+coreValidity);
    // Check core validation status
    if (coreValidity == false) {
      valido = false;
      final boolean sv = signature.getSignatureValue().validate(valContext);
      // check the validation status of each Reference
      final Iterator<?> i = signature.getSignedInfo().getReferences().iterator();
      for (int j = 0; i.hasNext(); j++) {
        final boolean refValid = ((Reference) i.next()).validate(valContext);
        if (!refValid) {
          // System.out.println("Error en referencia: ref[" + j + "] validity status: " + refValid);
          throw new Exception("Error en referencia: ref[" + j + "] validity status: " + refValid);
        }
      }
      System.out.println("Error CoreValidation");
      throw new Exception("Error CoreValidation");
    }
    return valido;
  }


  /**
   * KeySelector which retrieves the public key out of the KeyValue element and returns it. NOTE: If the key algorithm
   * doesn't match signature algorithm, then the public key will be ignored.
   */
  private static class KeyValueKeySelector extends KeySelector {

    @Override
    public KeySelectorResult select(final KeyInfo keyInfo, final KeySelector.Purpose purpose,
        final AlgorithmMethod method, final XMLCryptoContext context) throws KeySelectorException {

      if (keyInfo == null) {
        throw new KeySelectorException("Null KeyInfo object!");
      }
      final SignatureMethod sm = (SignatureMethod) method;
      final List<?> list = keyInfo.getContent();

      for (int i = 0; i < list.size(); i++) {
        final XMLStructure xmlStructure = (XMLStructure) list.get(i);
        if (xmlStructure instanceof X509Data) {
          PublicKey pk = null;
          final List<?> l = ((X509Data) xmlStructure).getContent();
          if (l.size() > 0) {
            final X509Certificate cert = (X509Certificate) l.get(0);
            pk = cert.getPublicKey();
            // System.out.println("PUBLIC KEY"+cert.getSerialNumber());
            if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
              return new SimpleKeySelectorResult(pk);
            }
          }
        }
        if (xmlStructure instanceof KeyValue) {
          PublicKey pk = null;
          try {
            pk = ((KeyValue) xmlStructure).getPublicKey();
          } catch (final KeyException ke) {
            throw new KeySelectorException(ke);
          }
          // make sure algorithm is compatible with method
          if (algEquals(sm.getAlgorithm(), pk.getAlgorithm())) {
            return new SimpleKeySelectorResult(pk);
          }
        }
      }
      throw new KeySelectorException("No KeyValue element found!");
    }

    // @@@FIXME: this should also work for key types other than DSA/RSA
    static boolean algEquals(final String algURI, final String algName) {
      if (algName.equalsIgnoreCase("DSA") && algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) {
        return true;
      } else if (algName.equalsIgnoreCase("RSA") && algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)) {
        return true;
      } else {
        return false;
      }
    }
  }

  private static class SimpleKeySelectorResult implements KeySelectorResult {

    private PublicKey pk;

    SimpleKeySelectorResult(final PublicKey pk) {
      this.pk = pk;
    }

    @Override
    public Key getKey() {
      return pk;
    }
  }
}
