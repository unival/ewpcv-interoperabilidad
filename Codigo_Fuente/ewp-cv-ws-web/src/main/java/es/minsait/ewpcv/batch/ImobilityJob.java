package es.minsait.ewpcv.batch;


import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.ImobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Imobility job.
 * Se encarga de procesar los cnr entrantes de tipo imobilities, es decir, las nominaciones de movilidades que enviamos nosotros y nos aprueban o rechazan los partners
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class ImobilityJob implements Job {


  @Value("${ewp.imobilities.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.iia.cnr.notification.block:50}")
  private int blockSize;

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private IEventService eventService;

  @Autowired
  private OmobilityLasService mobilityService;

  @Autowired
  private Utils utils;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.incomingmobility:true}")
  private boolean cnrImobility;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrImobility) {
      log.info("***No se ejecuta el proceso de refresco de Incoming Mobilities a partir de CNR por configuracion");
      return;
    }
    log.info("Comienza proceso de refresco de Incoming Mobilities a partir de CNR");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.IMOBILITY);

    log.info("Se van a actualizar " + list.size() + " Incoming Mobilities");

    for (final NotificationMDTO notification : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IMOBILITY, notification.getOwnerHeiId(),
          notification.getHeiId(), notification.getChangedElementIds(), EwpEventConstants.UPDATE);

      try {
        if(actualizarMobility(notification, event)) {
          // si la mobilidad se actualiza sin problemas borramos la notificacion
          notificationService.eliminarNotificacion(notification);
        }
      } catch (final Exception e) {
        final String error = Objects.nonNull(notification)
                ? "Error al procesar Imobility CNR, id mobility: " + notification.getChangedElementIds()
                : "Error al procesar Imobility CNR";
        log.error(error, getClass().getName(), e);
        notificationService.liberarNotificaciones(notification, e.getMessage());
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }

      eventService.save(event);

    }

    log.info("Termina proceso de refresco de Incoming Mobilities");
  }

  private boolean actualizarMobility(final NotificationMDTO notification, final EventMDTO event)
          throws EwpOperationNotFoundException {
    try {
      final ClientResponse response = invocarImobilityGet(notification, event);

      if (!utils.verifyResponse(notification, event, response, EwpEventConstants.CNR_PROCESSING_GET)) {
        return false;
      }

        final ImobilitiesGetResponse imobilitiesResponse = (ImobilitiesGetResponse) response.getResult();

        if (Objects.nonNull(imobilitiesResponse)
            && CollectionUtils.isNotEmpty(imobilitiesResponse.getSingleIncomingMobilityObject())) {
          // actualizamos la mobilidad
          final StudentMobilityForStudies mobilityForStudies =
                  imobilitiesResponse.getSingleIncomingMobilityObject().get(0);
          final boolean actualiza = mobilityService.actualizarImobility(mobilityForStudies, globalProperties.getInstance());
          event.setEventType(actualiza ? EwpEventConstants.UPDATE : EwpEventConstants.NO_ACTION);
        }

    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Incoming Mobilities", getClass().getName(), e);
      throw e;
    }
    return true;
  }

  private ClientResponse invocarImobilityGet(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.IMOBILITIES_GET_RECEIVING_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getImobilitiesHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.IMOBILITIES_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.IMOBILITIES_API_NAME);
    return client.sendRequest(requestIndex, ImobilitiesGetResponse.class, event);
  }

}
