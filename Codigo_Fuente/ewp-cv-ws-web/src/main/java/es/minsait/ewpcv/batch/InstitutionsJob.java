package es.minsait.ewpcv.batch;


import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IInstitutionService;
import es.minsait.ewpcv.service.api.IOunitService;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import lombok.extern.slf4j.Slf4j;


/**
 * The type Institutions job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class InstitutionsJob implements Job {

  @Autowired
  private IInstitutionService institutionService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private IOunitService ounitService;

  @Autowired
  private IEventService eventService;

  @Autowired
  private Utils utils;

  @Value("${batch.refrescar.institutions:true}")
  private boolean refrescarInstitutions;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    log.info("Comienza proceso de refresco de Institutions");
    if (!refrescarInstitutions) {
      log.info("***No se ejecuta el proceso de refresco de Institutions por configuracion");
      return;
    }

    List<HeiEntry> heis = registry.getInstitutionsHeiUrls();

    heis = utils.filterHeisByProperties(heis, globalProperties.getDefaultInstitutionsHeiIds());

    for (final HeiEntry hei : heis) {
      actualizarInstitution(hei);
    }

    log.info("Termina proceso de refresco de Institutions");
  }

  private void actualizarInstitution(final HeiEntry hei) {
    final EventMDTO event = utils.initializeJobEvent(NotificationTypes.INSTITUTIONS, hei.getId(),
            globalProperties.getInstance(),hei.getId(), EwpEventConstants.UPDATE);
    try {
      final ClientResponse response = invocarInstitutionsGet(hei, event);

      if (!utils.verifyResponse(null, event, response, EwpEventConstants.INSTITUTIONSGET_GET)) {
        // si la respuesta no es correcta no continuamos con el proceso, esta verificacion ya guarda el evento
        return;
      }

      final InstitutionsResponse institutionsResponse = (InstitutionsResponse) response.getResult();

      if (Objects.nonNull(institutionsResponse) && CollectionUtils.isNotEmpty(institutionsResponse.getHei())) {
        log.info("Comienza proceso de refresco de Institutions");
        institutionService.actualizarInstitutions(institutionsResponse.getHei());

        for (final InstitutionsResponse.Hei res : institutionsResponse.getHei()) {
          final String heiId = res.getHeiId();
          final HeiEntry ounitHeiEntry = getOunitHeiEntry(heiId);
          final List<OunitsResponse.Ounit> ounitsInstitution = new ArrayList<>();
          for (final String ounit : res.getOunitId()) {
            final EventMDTO eventOunits = utils.initializeJobEvent(NotificationTypes.ORGANIZATION_UNIT,
                    hei.getId(), globalProperties.getInstance(), ounit, EwpEventConstants.UPDATE);

            final ClientResponse responseOunit = invocarOunitGet(ounitHeiEntry, heiId, ounit, eventOunits);

            if (!utils.verifyResponse(null, eventOunits, responseOunit, EwpEventConstants.ORGANIZATIONUNITGET_GET)) {
              // si la respuesta no es correcta no continuamos con el proceso, esta verificacion ya guarda el evento
              continue;
            }
            final OunitsResponse ounitsResponse = (OunitsResponse) responseOunit.getResult();
            if (Objects.nonNull(ounitsResponse)) {
              ounitsInstitution.addAll(ounitsResponse.getOunit());
            }

            eventService.save(eventOunits);
          }

          log.info("Comienza proceso de refresco de OrganizationUnits");
          final List<OrganizationUnitMDTO> ounitsGuardadas =
              ounitService.actualizarOrganizationUnits(heiId, ounitsInstitution, res.getRootOunitId());
          log.info("Termina proceso de refresco de OrganizationUnits");
          if (CollectionUtils.isNotEmpty(ounitsGuardadas)) {
            institutionService.actualizarInstitutionsOunits(heiId, ounitsGuardadas);
          }
        }
        log.info("Termina proceso de refresco de OrganizationUnits");
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Institutions", getClass().getName(), e);
      event.setObservations(Strings.isNullOrEmpty(e.getMessage()) ? e.toString() : e.getMessage());
      event.setStatus(EwpEventConstants.KO);
    }

    eventService.save(event);
  }

  private ClientResponse invocarInstitutionsGet(final HeiEntry hei, final EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.INSTITUTIONS_GET_HEI_ID, Arrays.asList(hei.getId()));
    // Para pruebas con dos instancias locales
    // final Map<String, String> urls = new HashMap<>();
    // urls.put(EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, targetUri + "/institutions/get");
    // hei.setUrls(urls);
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.INSTITUTIONS_API_NAME);
    return client.sendRequest(requestIndex, InstitutionsResponse.class, event);
  }

  private ClientResponse invocarOunitGet(final HeiEntry ounitHeiEntry, final String heiId, final String ounit, final EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Collections.singletonList(heiId));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID, Collections.singletonList(ounit));
    final ClientRequest request = utils.formarPeticion(ounitHeiEntry, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY,
        params, HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    return client.sendRequest(request, OunitsResponse.class, event);
  }

  private HeiEntry getOunitHeiEntry(final String heiId) throws EwpOperationNotFoundException {

    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls(heiId.toLowerCase());
    if (Objects.isNull(hei) || Objects.isNull(hei.getUrls()) || hei.getUrls().isEmpty()
        || Strings.isNullOrEmpty(hei.getUrls().get(EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY))) {
      throw new EwpOperationNotFoundException(
          "No se ha encontrado " + EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY + " para el heiId " + heiId, 404);
    }
    return hei;
  }

}
