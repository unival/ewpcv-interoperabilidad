package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import eu.erasmuswithoutpaper.api.echo.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * The type Echo controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/echo", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class EchoController {

  @Autowired
  GlobalProperties globalProperties;
  @Autowired
  RegistryClient registryClient;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;
  @Autowired
  private Utils utils;

  @EwpAuthenticate
  @GetMapping()
  public ResponseEntity<Response> getEcho(@RequestParam(name = "echo", required = false) final List<String> echolist,
      @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET ECHO");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.ECHOECHO_GET,
        NotificationTypes.ECHO, null);

    final Collection<String> heisCovered = utils.heisCovered(request);

    final Response respuesta = new Response();
    respuesta.getHeiId().addAll(heisCovered);
    if (Objects.nonNull(echolist)) {
      respuesta.getEcho().addAll(echolist);
    }

    if (utils.getValue(EwpConstants.ECHOAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }

    eventService.save(event);

    log.debug("Fin invocacion GET ECHO");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping()
  public ResponseEntity<Response> postEcho(@RequestBody(required = false) final MultiValueMap<String, String> echolist,
      final HttpServletRequest request) {
    log.debug("Inicio invocacion POST ECHO");

    final EventMDTO event = utils.initializeAuditableEvent(null, echolist, request, EwpEventConstants.ECHOECHO_GET,
        NotificationTypes.ECHO, null);

    final Collection<String> heisCovered = utils.heisCovered(request);

    final Response respuesta = new Response();
    respuesta.getHeiId().addAll(heisCovered);
    if (Objects.nonNull(echolist) && !echolist.isEmpty()) {
      echolist.entrySet().forEach(e -> respuesta.getEcho().addAll(e.getValue()));
    }

    if (utils.getValue(EwpConstants.ECHOAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    log.debug("Fin invocacion POST ECHO");
    return ResponseEntity.ok(respuesta);
  }
}
