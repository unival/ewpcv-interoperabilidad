package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.OutgoingLasUpdateRequestJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Outgoing las update request job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class OutgoingLasUpdateRequestJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailOutgoingLasUpdateRequestJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(OutgoingLasUpdateRequestJob.class);
    jobDetailFactory
        .setDescription("Batch para el procesamiento de solicitudes de actualizacion de Learning Agreement");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerOutgoingLasUpdateRequestJob(
      @Qualifier("jobDetailOutgoingLasUpdateRequestJob") final JobDetail jobDetail,
      @Value("${ewp.las.update.outgoing.cron}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
