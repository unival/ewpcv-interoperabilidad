package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.error.EwpXMLDsigException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.security.xmldsig.XmlDsigUtils;
import es.minsait.ewpcv.service.api.*;
import eu.emrex.elmo.Elmo;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsIndexResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Tor controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/tor", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class TorController {

  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private ITorService torService;
  @Autowired
  private ILearningAgreementService laService;
  @Autowired
  private IMobilityUpdateRequestService mobilityUpdateRequestService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private Utils utils;
  @Autowired
  private XmlDsigUtils xmlDsigUtils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.omobility.ids}")
  private int maxOmobilityIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<ImobilityTorsGetResponse> getGet(
          @RequestParam(value = "receiving_hei_id", required = false) final String receivingHeiId,
          @RequestParam(value = "omobility_id", required = false) final List<String> omobilityId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /get Transcript of Records");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.TORGET_GET,
            NotificationTypes.IMOBILITY_TOR, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (Objects.isNull(receivingHeiId) || Objects.isNull(omobilityId) || omobilityId.isEmpty()) {
        throw new EwpWebApplicationException("Receiving_hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }

      utils.isHeisCoveredByTheHost(receivingHeiId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);


      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final ImobilityTorsGetResponse respuesta = new ImobilityTorsGetResponse();
    try {

      final List<ImobilityTorsGetResponse.Tor> tors = torService
              .findTorByOmobilityIdAndReceivingInstitutionId(receivingHeiId, omobilityId, utils.heisCovered(request));
      // firmamos los elementos EMREX-ELMO de los tors
      firmarElmos(tors);
      respuesta.getTor().addAll(tors);

      utils.setMessageToEventOK(event, respuesta);
      eventService.save(event);

      log.debug("Fin invocacion GET /get Transcript of Records");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException | IOException | EwpXMLDsigException | JAXBException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<ImobilityTorsGetResponse> postGet(@RequestBody final MultiValueMap<String, String> paramsList,
                                                          final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get Transcript of Records");

    final String receivingHeiId = paramsList.toSingleValueMap().get("receiving_hei_id");
    final List<String> omobilityId = paramsList.get("omobility_id");

    final EventMDTO event = utils.initializeAuditableEvent(null, paramsList, request, EwpEventConstants.TORGET_POST,
            NotificationTypes.IMOBILITY_TOR, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (Objects.isNull(receivingHeiId) || omobilityId.isEmpty()) {
        throw new EwpWebApplicationException("Receiving_hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }

      utils.isHeisCoveredByTheHost(receivingHeiId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final ImobilityTorsGetResponse respuesta = new ImobilityTorsGetResponse();
    try {
      final List<ImobilityTorsGetResponse.Tor> tors = torService
              .findTorByOmobilityIdAndReceivingInstitutionId(receivingHeiId, omobilityId, utils.heisCovered(request));
      // firmamos los elementos EMREX-ELMO de los tors
      firmarElmos(tors);
      respuesta.getTor().addAll(tors);
      log.debug("Fin invocacion POST /get Transcript of Records");

      utils.setMessageToEventOK(event, respuesta);
      eventService.save(event);

      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException | EwpXMLDsigException | IOException | JAXBException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @GetMapping(path = "/index")
  public ResponseEntity<ImobilityTorsIndexResponse> getIndex(
          @RequestParam(value = "receiving_hei_id", required = false) final String receivingHeiId,
          @RequestParam(value = "sending_hei_id", required = false) final List<String> sendingHeiId,
          @RequestParam(value = "modified_since", required = false) String modifiedSince,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /index Transcript of Records");

    final EventMDTO event = utils.initializeAuditableEvent(sendingHeiId, emapParam, request,
            EwpEventConstants.TORINDEX_GET, NotificationTypes.IMOBILITY_TOR, null);

    try {
      if (Objects.isNull(receivingHeiId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id is required", HttpStatus.BAD_REQUEST.value());
      }

      utils.isHeisCoveredByTheHost(receivingHeiId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
    if (Objects.nonNull(modifiedSince)) {
      modifiedSince = modifiedSince.replace(" ", "+");
    }

    final List<String> receivingHeiIdToFilter;
    final List<String> heisCovered = utils.heisCovered(request);
    if (Objects.nonNull(sendingHeiId)) {
      receivingHeiIdToFilter = heisCovered.stream().filter(sendingHeiId::contains).collect(Collectors.toList());
    } else {
      receivingHeiIdToFilter = heisCovered;
    }

    final List<String> mobilitiesIds =
            torService.obtenerMobilitiesIds(receivingHeiId, sendingHeiId, modifiedSince, receivingHeiIdToFilter);

    final ImobilityTorsIndexResponse respuesta = new ImobilityTorsIndexResponse();
    respuesta.getOmobilityId().addAll(mobilitiesIds);
    log.debug("Fin invocacion GET /index Transcript of Records");

    utils.setMessageToEventOK(event, respuesta);
    eventService.save(event);

    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/index")
  public ResponseEntity<ImobilityTorsIndexResponse> postIndex(@RequestBody final MultiValueMap<String, String> torIndex,
                                                              final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /index Transcript of Records");

    final String receivingHeiId = torIndex.toSingleValueMap().get("receiving_hei_id");
    final List<String> sendingHeiId = torIndex.get("sending_hei_id");
    String modifiedSince = torIndex.toSingleValueMap().get("modified_since");

    final EventMDTO event = utils.initializeAuditableEvent(sendingHeiId, torIndex, request,
            EwpEventConstants.TORINDEX_POST, NotificationTypes.IMOBILITY_TOR, null);

    try {
      if (Objects.isNull(receivingHeiId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id is required", HttpStatus.BAD_REQUEST.value());
      }

      utils.isHeisCoveredByTheHost(receivingHeiId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (Objects.nonNull(modifiedSince)) {
      modifiedSince = modifiedSince.replace(" ", "+");
    }

    final List<String> receivingHeiIdToFilter;
    final List<String> heisCovered = utils.heisCovered(request);
    if (Objects.nonNull(sendingHeiId)) {
      receivingHeiIdToFilter = heisCovered.stream().filter(sendingHeiId::contains).collect(Collectors.toList());
    } else {
      receivingHeiIdToFilter = heisCovered;
    }

    final List<String> laIdsList =
            torService.obtenerMobilitiesIds(receivingHeiId, sendingHeiId, modifiedSince, receivingHeiIdToFilter);

    final ImobilityTorsIndexResponse respuesta = new ImobilityTorsIndexResponse();
    respuesta.getOmobilityId().addAll(laIdsList);
    log.debug("FIN invocacion POST /index Transcript of Records");

    utils.setMessageToEventOK(event, respuesta);
    eventService.save(event);

    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                final HttpServletRequest request) {

    final String receivingHeiId = cnrlist.toSingleValueMap().get("receiving_hei_id");
    final List<String> omobilityId = cnrlist.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(receivingHeiId, cnrlist, request, EwpEventConstants.TORCNR_POST,
                    NotificationTypes.IMOBILITY_TOR, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);


    if (CollectionUtils.isEmpty(omobilityId) || Strings.isNullOrEmpty(receivingHeiId)) {
      final EwpWebApplicationException ex =
              new EwpWebApplicationException("Missing argumanets for notification.", HttpStatus.BAD_REQUEST.value());
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    for (final String id : omobilityId) {
      notificationService.saveNotificationTorRefresh(receivingHeiId, id, globalProperties.getInstance());
    }

    return ResponseEntity.ok().build();
  }

  private String getByteArrayFromObject(final Object body, final boolean formatedOutput) throws IOException {
    try {
      final String bodyBytes;
      final JAXBContext jaxbContext = JAXBContext.newInstance(body.getClass());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatedOutput);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        marshaller.marshal(body, baos);
        bodyBytes = baos.toString();
      }
      return bodyBytes;
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  private void firmarElmos(final List<ImobilityTorsGetResponse.Tor> tors)
          throws IOException, EwpXMLDsigException, JAXBException {

    // firmamos el elemento EMREX-ELMO de la respuesta
    for (final ImobilityTorsGetResponse.Tor tor : tors) {
      final String elmoXML = Objects.requireNonNull(getByteArrayFromObject(tor.getElmo(), true));
      String xmlFirmado = null;
      try {
        xmlFirmado = xmlDsigUtils.changeOutboundMessage(elmoXML);
        // convertimos el xml en objeto anotado
        final JAXBContext jaxbContext = JAXBContext.newInstance(Elmo.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        final JAXBElement<Elmo> res =
                unmarshaller.unmarshal(new StreamSource(new StringReader(xmlFirmado)), Elmo.class);
        tor.setElmo(res.getValue());
      } catch (final Exception e) {
        log.error("Se ha producido un error al firmar el EMREX ELMO la respuesta no estará firmada");
      }
    }
  }
}
