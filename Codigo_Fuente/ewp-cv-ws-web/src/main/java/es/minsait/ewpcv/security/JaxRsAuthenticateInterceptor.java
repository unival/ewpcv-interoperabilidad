package es.minsait.ewpcv.security;

import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.RegistryClient;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpSecWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Objects;

/**
 * The type Jax rs authenticate interceptor.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
public class JaxRsAuthenticateInterceptor implements HandlerInterceptor {

  GlobalProperties properties;

  RegistryClient registryClient;

  HttpSignature httpSignature;

  Utils utils;

  IEventService eventService;

  private void injectBeans(final ServletRequest request) {
    if (Objects.isNull(properties)) {
      final ServletContext servletContext = request.getServletContext();
      final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
      properties = webApplicationContext.getBean(GlobalProperties.class);
    }
    if (Objects.isNull(registryClient)) {
      final ServletContext servletContext = request.getServletContext();
      final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
      registryClient = webApplicationContext.getBean(RegistryClient.class);
    }
    if (Objects.isNull(httpSignature)) {
      final ServletContext servletContext = request.getServletContext();
      final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
      httpSignature = webApplicationContext.getBean(HttpSignature.class);
    }
    if (Objects.isNull(utils)) {
      final ServletContext servletContext = request.getServletContext();
      final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
      utils = webApplicationContext.getBean(Utils.class);
    }
    if (Objects.isNull(eventService)) {
      final ServletContext servletContext = request.getServletContext();
      final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
      eventService = webApplicationContext.getBean(IEventService.class);
    }
  }

  private String errorToString(final ErrorResponse errorResponse) {
    try {
      final JAXBContext jaxbContext = JAXBContext.newInstance(errorResponse.getClass());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      marshaller.marshal(errorResponse, baos);
      return baos.toString();
    } catch (final JAXBException e) {
      log.error("Se ha producido un error generando el mensaje de la respuesta del filtro de seguridad");
    }
    return StringUtils.EMPTY;
  }

  private AuthenticateMethodResponse verifyX509CertificateRequest(final HttpServletRequest request)
      throws EwpSecWebApplicationException {
    final X509Certificate[] certificates = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
    log.info("Verifying Client certificate");
    if (certificates == null && !properties.isAllowMissingClientCertificate()) {
      return AuthenticateMethodResponse.builder().withRequiredMethodInfoFulfilled(false)
          .withResponseCode(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    final X509Certificate certificate = registryClient.getCertificateKnownInEwpNetwork(certificates);
    if (certificate == null && !properties.isAllowMissingClientCertificate()) {
      return AuthenticateMethodResponse.builder()
          .withErrorMessage("None of the client certificates is valid in the EWP network")
          .withResponseCode(HttpServletResponse.SC_UNAUTHORIZED).build();
    }

    request.setAttribute("EwpRequestCertificate", certificate);
    return AuthenticateMethodResponse.builder().build();

  }


  @Override
  public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
    injectBeans(request);
    try {
      if (properties.isSecurityDisabled() || !isMethodEwpAuthenticateAnotated(handler)) {
        if (((HandlerMethod) handler).getBean() instanceof BasicErrorController) {
          throw new EwpSecWebApplicationException(HttpStatus.valueOf(response.getStatus()).getReasonPhrase(),
              response.getStatus());
        }
        return true;
      }

      final AuthenticateMethodResponse httpSignatureVerifyResponse = httpSignature.verifyHttpSignatureRequest(request);
      if (httpSignatureVerifyResponse.isRequiredMethodInfoFulfilled()) {
        if (!httpSignatureVerifyResponse.isVerifiedOk()) {
          throw new EwpSecWebApplicationException(httpSignatureVerifyResponse.errorMessage(),
              httpSignatureVerifyResponse.status(), EwpSecWebApplicationException.AuthMethod.HTTPSIG);
        }
        return true;
      } else {
        final AuthenticateMethodResponse tlsCertVerifyResponse = verifyX509CertificateRequest(request);
        if (tlsCertVerifyResponse.isRequiredMethodInfoFulfilled()) {
          if (!tlsCertVerifyResponse.isVerifiedOk()) {
            throw new EwpSecWebApplicationException(tlsCertVerifyResponse.errorMessage(),
                tlsCertVerifyResponse.status());
          }
          return true;
        } else {
          throw new EwpSecWebApplicationException(
              httpSignatureVerifyResponse.hasErrorMessage() ? httpSignatureVerifyResponse.errorMessage()
                  : "No authorization method found in the request",
              httpSignatureVerifyResponse.status());
        }
      }
    } catch (final EwpSecWebApplicationException exception) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(exception.getMessage());
      errorResponse.setDeveloperMessage(message);
      response.setStatus(exception.getStatus());
      response.setContentType("text/html; charset=utf-8");
      response.addHeader("X-EWP-Verification-Message", exception.getMessage());
      if (response.getStatus() == HttpServletResponse.SC_UNAUTHORIZED) {
        response.addHeader("WWW-Authenticate", "Signature realm=\"EWP\"");
        response.addHeader("Want-Digest", "SHA-256");
      }
      if (httpSignature.clientWantsSignedResponse(request)) {
        httpSignature.signResponse(request, response, errorResponse, true);
      }
      response.getWriter().write(errorToString(errorResponse));
      log.error("Error validando una petición entrante: " + exception);

      // Evento mínimo para auditar una petición entrante no válida
      final EventMDTO event = utils.initializeAuditableEvent(null, null, request, null,
              NotificationTypes.UNDEFINED, null);
      event.setObservations(exception.getMessage());
      event.setStatus(EwpEventConstants.KO);
      event.setMessage(errorToString(errorResponse).getBytes(StandardCharsets.UTF_8));
      eventService.save(event);

      return false;
    }
  }

  private boolean isMethodEwpAuthenticateAnotated(final Object handler) throws EwpSecWebApplicationException {
    final HandlerMethod hm;
    try {
      hm = (HandlerMethod) handler;
      final Method method = hm.getMethod();
      if (!method.isAnnotationPresent(EwpAuthenticate.class)) {
        return false;
      }
    } catch (final ClassCastException e) {
      throw new EwpSecWebApplicationException("The invoked method doesn't exist", 404);
    }
    return true;
  }

  @Override
  public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
                         final ModelAndView modelAndView) throws Exception {
    if (response.getStatus() == HttpServletResponse.SC_UNAUTHORIZED) {
      response.addHeader("WWW-Authenticate", "Signature realm=\"EWP\"");
      response.addHeader("Want-Digest", "SHA-256");
    }
  }

  @Override
  public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex)
      throws Exception {

  }
}
