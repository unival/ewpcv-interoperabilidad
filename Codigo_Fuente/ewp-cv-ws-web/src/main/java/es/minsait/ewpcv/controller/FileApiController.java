package es.minsait.ewpcv.controller;


import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IFileApiService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;


/**
 * The FileApi controller..
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@RestController
@RequestMapping(value = "/files", produces = {MediaType.APPLICATION_PDF_VALUE, "application/xml; charset=utf-8"})
@Slf4j
@Validated
public class FileApiController {

  @Autowired
  GlobalProperties globalProperties;
  @Autowired
  private IFileApiService iFileApiService;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;


  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<byte[]> getFile(@RequestParam(value = "file_id") final String file_id,
      final HttpServletRequest request) {

    log.debug("Inicio invocacion GET FileApi");

    final EventMDTO event = utils.initializeAuditableEvent(null, null, request, EwpEventConstants.FILEAPIGET_GET,
        NotificationTypes.FILE_API, file_id);

    try {
      if (Strings.isNullOrEmpty(file_id)) {
        throw new EwpWebApplicationException("file_id is required", HttpStatus.BAD_REQUEST.value());
      }

      FileMDTO file = iFileApiService.getFileByIdAndScach(file_id, globalProperties.getInstance());

      if(Objects.isNull(file)){
        ResponseEntity response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        auditOk(event, response);
        return response;
      }

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.parseMediaType(file.getMimeType()));
      String filename = "file."+MediaType.parseMediaType(file.getMimeType()).getSubtype();
      headers.setContentLength(file.getFileContent().length);
      headers.setContentDispositionFormData(filename, filename);
      ResponseEntity<byte[]> response = new ResponseEntity<>(file.getFileContent(), headers, HttpStatus.OK);

      auditOk(event, response);

      log.debug("Fin invocacion GET FileApi");

      return  response;
    } catch (EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.FILEAPIAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
        eventService.save(event);
      }

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_XML);

      byte[] bytes;
      try {
        bytes = utils.getByteArrayFromObject(errorResponse, false);
      } catch (final IOException e) {
        log.error(EwpEventConstants.ERROR_RESPONSE, e);
        bytes = ex.getMessage().getBytes();
      }

      log.debug("Fin invocacion GET FileApi");
      return new ResponseEntity(bytes, headers, HttpStatus.valueOf(ex.getStatus()));
    }
  }

  private void auditOk(EventMDTO event, ResponseEntity<byte[]> response) {
    if (utils.getValue(EwpConstants.FILEAPIAUDIT)) {
      utils.setMessageToEventOK(event, response);
      eventService.save(event);
    }
  }
}
