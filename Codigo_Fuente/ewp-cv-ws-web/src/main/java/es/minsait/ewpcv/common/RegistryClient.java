package es.minsait.ewpcv.common;

import static es.minsait.ewpcv.common.EwpConstants.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import es.minsait.ewpcv.registryclient.ApiSearchConditions;
import es.minsait.ewpcv.registryclient.ClientImpl;
import es.minsait.ewpcv.registryclient.ClientImplOptions;
import es.minsait.ewpcv.registryclient.DefaultCatalogueFetcher;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Registry client.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class RegistryClient {
  private es.minsait.ewpcv.registryclient.RegistryClient client;

  @Autowired
  private GlobalProperties properties;

  @Autowired
  EwpKeyStore keystoreController;

  @PostConstruct
  private void loadRegistryClient() {
    try {
      final ClientImplOptions options = new ClientImplOptions();
      options.setCatalogueFetcher(new DefaultCatalogueFetcher(properties.getRegistryUrl(), keystoreController.getTruststore()));
      options.setAutoRefreshing(properties.isRegistryAutoRefreshing());
      options.setTimeBetweenRetries(properties.getRegistryTimeBetweenRetries());
      options.setProxy(properties.getProxy());
      options.setProxyPort(properties.getProxyPort());
      client = new ClientImpl(options);

      client.refresh();
    } catch (final es.minsait.ewpcv.registryclient.RegistryClient.RefreshFailureException ex) {
      log.error("Can't refresh registry client", ex);
    }
  }

  public X509Certificate getCertificateKnownInEwpNetwork(final X509Certificate[] certificates) {
    if (certificates == null) {
      return null;
    }

    for (final X509Certificate certificate : certificates) {
      if (client.isCertificateKnown(certificate)) {
        return certificate;
      }
    }
    return null;
  }

  public Collection<String> getHeisCoveredByCertificate(final X509Certificate certificate) {
    if (certificate != null && client.isCertificateKnown(certificate)) {
      final Collection<String> heiIds = client.getHeisCoveredByCertificate(certificate);
      return heiIds;
    }

    return new ArrayList<>();
  }

  public List<HeiEntry> getDictionaryHeisWithUrls() {
    final List<HeiEntry> heis =
        getHeis(DICTIONARY_NAMESPACE, DICTIONARIES_API_NAME, DICTIONARY_VERSION);
    heis.stream().forEach(hei -> hei = getDictionaryHeiUrls(hei.getId()));
    return heis;
  }

  public HeiEntry getDictionaryHeiUrls(final String heiId) {
    return getHeiUrls(heiId, DICTIONARY_NAMESPACE, DICTIONARIES_API_NAME, DICTIONARY_VERSION);
  }

  public List<HeiEntry> getEwpDiscoveryWithUrls() {
    final List<HeiEntry> heis =
            getHeis(DISCOVERY_NAMESPACE, DISCOVERY_API_NAME, DISCOVERY_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getEwpInstanceHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getEwpDiscoveryHeiUrls(final String heiId) {
    return getHeiUrls(heiId, DISCOVERY_NAMESPACE, DISCOVERY_API_NAME, DISCOVERY_CLIENT_VERSION);
  }

  public List<HeiEntry> getEwpInstanceHeisWithUrls() {
    final List<HeiEntry> heis =
            getHeis(INSTITUTION_NAMESPACE, INSTITUTIONS_API_NAME, INSTITUTION_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getEwpInstanceHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getEwpInstanceHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INSTITUTION_NAMESPACE, INSTITUTIONS_API_NAME, INSTITUTION_CLIENT_VERSION);
  }

  public List<HeiEntry> getEwpOrganizationUnitHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(ORGANIZATION_UNIT_NAMESPACE, ORGANIZATIONAL_UNITS_API_NAME,
        ORGANIZATION_UNIT_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getEwpOrganizationUnitHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getEwpOrganizationUnitHeiUrls(final String heiId) {
    return getHeiUrls(heiId, ORGANIZATION_UNIT_NAMESPACE, ORGANIZATIONAL_UNITS_API_NAME,
        ORGANIZATION_UNIT_CLIENT_VERSION);
  }

  public List<HeiEntry> getCoursesReplicationHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(COURSE_REPLICATION_NAMESPACE, COURSE_REPLICATION_API_NAME,
        COURSE_REPLICATION_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getCoursesReplicationHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getCoursesReplicationHeiUrls(final String heiId) {
    return getHeiUrls(heiId, COURSE_REPLICATION_NAMESPACE, COURSE_REPLICATION_API_NAME,
        COURSE_REPLICATION_CLIENT_VERSION);
  }

  public List<HeiEntry> getCoursesHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(COURSES_NAMESPACE, COURSES_API_NAME, COURSES_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getCoursesHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getCoursesHeiUrls(final String heiId) {
    return getHeiUrls(heiId, COURSES_NAMESPACE, COURSES_API_NAME, COURSES_CLIENT_VERSION);
  }

  public List<HeiEntry> getIiaHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(IIAS_NAMESPACE, IIAS_API_NAME, IIAS_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getIiaHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getIiaHeiUrls(final String heiId) {
    return getHeiUrls(heiId, IIAS_NAMESPACE, IIAS_API_NAME, IIAS_CLIENT_VERSION);
  }

  public List<HeiEntry> getIiaCnrHeisWithUrls() {
    final List<HeiEntry> heis =
        getHeis(IIAS_CNR_NAMESPACE, IIA_CNR_API_NAME, IIA_CNR_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getIiaCnrHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getIiaCnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, IIAS_CNR_NAMESPACE, IIA_CNR_API_NAME, IIA_CNR_CLIENT_VERSION);
  }

  public List<HeiEntry> getOmobilitiesHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(OUTGOING_MOBILITIES_NAMESPACE, OMOBILITIES_API_NAME,
        OUTGOING_MOBILITIES_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getOmobilitiesHeiUrls(hei.getId()), hei));
    return heis;
  }

  public List<HeiEntry> getOmobilitiesLaHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(OUTGOING_MOBILITIES_LAS_NAMESPACE, OMOBILITY_LAS_API_NAME,
        OUTGOING_MOBILITIES_LAS_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getOmobilitiesLasHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getIiaApprovalCnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, IIAS_APPROVAL_CNR_NAMESPACE, IIA_APPROVAL_CNR_API_NAME, IIA_APPROVAL_CNR_CLIENT_VERSION);
  }

  public HeiEntry getIiaApprovalHeiUrls(final String heiId) {
    return getHeiUrls(heiId, IIAS_APPROVAL_NAMESPACE, IIAS_APPROVAL_API_NAME, IIA_APPROVAL_CLIENT_VERSION);
  }

  public HeiEntry getOmobilitiesHeiUrls(final String heiId) {
    return getHeiUrls(heiId, OUTGOING_MOBILITIES_NAMESPACE, OMOBILITIES_API_NAME, OUTGOING_MOBILITIES_CLIENT_VERSION);
  }

  public HeiEntry getOmobilitiesLasHeiUrls(final String heiId) {
    return getHeiUrls(heiId, OUTGOING_MOBILITIES_LAS_NAMESPACE, OMOBILITY_LAS_API_NAME,
        OUTGOING_MOBILITIES_LAS_VERSION);
  }

  public List<HeiEntry> getOmobilitiesCnrHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(OUTGOING_MOBILITIES_CNR_NAMESPACE, OMOBILITY_CNR_API_NAME,
        OUTGOING_MOBILITIES_CNR_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getOmobilitiesCnrHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getOmobilitiesCnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, OUTGOING_MOBILITIES_CNR_NAMESPACE, OMOBILITY_CNR_API_NAME,
        OUTGOING_MOBILITIES_CNR_CLIENT_VERSION);
  }

  public HeiEntry getOmobilitiesLACnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, OUTGOING_MOBILITIES_LA_CNR_NAMESPACE, OMOBILITY_LA_CNR_API_NAME,
        OUTGOING_MOBILITIES_LA_CNR_CLIENT_VERSION);
  }

  public List<HeiEntry> getImobilitiesHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(INCOMING_MOBILITIES_NAMESPACE, IMOBILITIES_API_NAME,
        INCOMING_MOBILITIES_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getImobilitiesHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getImobilitiesHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INCOMING_MOBILITIES_NAMESPACE, IMOBILITIES_API_NAME, INCOMING_MOBILITIES_CLIENT_VERSION);
  }

  public List<HeiEntry> getImobilitiesCnrHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(INCOMING_MOBILITIES_CNR_NAMESPACE, IMOBILITY_CNR_API_NAME,
        INCOMING_MOBILITY_CNR_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getImobilitiesCnrHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getImobilitiesCnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INCOMING_MOBILITIES_CNR_NAMESPACE, IMOBILITY_CNR_API_NAME,
        INCOMING_MOBILITY_CNR_CLIENT_VERSION);
  }

  public List<HeiEntry> getImobilityTorsCnrHeisWithUrls() {
    final List<HeiEntry> heis = getHeis(INCOMING_MOBILITIES_TORS_CNR_NAMESPACE, IMOBILITY_TOR_CNR_API_NAME,
        INCOMING_MOBILITY_TORS_CNR_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getImobilityTorsCnrHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getImobilityTorsCnrHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INCOMING_MOBILITIES_TORS_CNR_NAMESPACE, IMOBILITY_TOR_CNR_API_NAME,
        INCOMING_MOBILITY_TORS_CNR_CLIENT_VERSION);
  }

  public List<HeiEntry> getInstitutionsHeiUrls() {
    final List<HeiEntry> heis =
        getHeis(INSTITUTION_NAMESPACE, INSTITUTIONS_API_NAME, INSTITUTION_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getInstitutionsHeiUrls(hei.getId()), hei));
    return heis;
  }

  public HeiEntry getInstitutionsHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INSTITUTION_NAMESPACE, INSTITUTIONS_API_NAME, INSTITUTION_CLIENT_VERSION);
  }

  public List<HeiEntry> getEchoHeis() {
    List<HeiEntry> heis = getHeis(ECHO_NAMESPACE, ECHO_API_NAME, ECHO_CLIENT_VERSION);
    heis.stream().forEach(hei -> {
      HeiEntry heiEntry = getEchoHeiUrls(hei.getId());
      hei.setUrls(heiEntry.getUrls());
      hei.setClientTlsCert(heiEntry.isClientTlsCert());
      hei.setServerTlsCert(heiEntry.isServerTlsCert());
      hei.setClientHttpSignature(heiEntry.isClientHttpSignature());
      hei.setServerHttpSignature(heiEntry.isServerHttpSignature());
    });
    return heis;
  }

  public HeiEntry getEchoHeiUrls(final String heiId) {
    return getHeiUrls(heiId, ECHO_NAMESPACE, ECHO_API_NAME, ECHO_CLIENT_VERSION);
  }

  public List<HeiEntry> getFactsheetHeisWithUrls() {
    final List<HeiEntry> heis =
        getHeis(FACTSHEET_NAMESPACE, FACTSHEET_API_NAME, FACTSHEET_CLIENT_VERSION);
    heis.stream().forEach(hei -> updateHeiDetails(getFactsheetHeiUrls(hei.getId()), hei));
    return heis;
  }

  private void updateHeiDetails(HeiEntry hei, HeiEntry hei1) {
    HeiEntry heiEntry = hei;
    hei1.setUrls(heiEntry.getUrls());
    hei1.setClientTlsCert(heiEntry.isClientTlsCert());
    hei1.setServerTlsCert(heiEntry.isServerTlsCert());
    hei1.setClientHttpSignature(heiEntry.isClientHttpSignature());
    hei1.setServerHttpSignature(heiEntry.isServerHttpSignature());
  }

  public HeiEntry getFactsheetHeiUrls(final String heiId) {
    return getHeiUrls(heiId, FACTSHEET_NAMESPACE, FACTSHEET_API_NAME, FACTSHEET_CLIENT_VERSION);
  }

  public HeiEntry getTorHeiUrls(final String heiId) {
    return getHeiUrls(heiId, INCOMING_MOBILITIES_TORS_NAMESPACE, IMOBILITY_TORS_API_NAME,
        INCOMING_MOBILITIES_TORS_VERSION);
  }

  public HeiEntry getMonitoringHeiUrls(final String heiId) {
    return getHeiUrls(heiId, MONITORING_NAMESPACE, MONITORING_API_NAME, MONITORING_CLIENT_VERSION);
  }

  private HeiEntry getHeiUrls(final String heiId, final String namespace, final String name,
      final String version) {
    final ApiSearchConditions myConditions = new ApiSearchConditions();
    myConditions.setApiClassRequired(namespace, name, version);
    myConditions.setRequiredHei(heiId);
    final Element manifest = client.findApi(myConditions);

    HeiEntry hei = getUrlsFromManifestElement(manifest);
    hei.setId(heiId);
    return hei;
  }

  private List<HeiEntry> getHeis(final String namespace, final String name, final String version) {
    final ApiSearchConditions myConditions = new ApiSearchConditions();
    myConditions.setApiClassRequired(namespace, name, version);

    final Collection<es.minsait.ewpcv.registryclient.HeiEntry> list = client.findHeis(myConditions);

    final List<HeiEntry> heis;

    heis = list.stream().map(e -> new HeiEntry(e.getId(), e.getName())).collect(Collectors.toList());

    return heis;
  }

  private HeiEntry getUrlsFromManifestElement(final Element manifestElement) {
    HeiEntry hei = new HeiEntry();
    final Map<String, String> urlMap = new HashMap<>();
    if (Objects.nonNull(manifestElement)) {
      final NodeList childNodeList = manifestElement.getChildNodes();
      for (int i = 0; i < childNodeList.getLength(); i++) {
        final Node childNode = childNodeList.item(i);
        // para obtener las urls de los servicios
        if ("url".equalsIgnoreCase(childNode.getLocalName())) {
          urlMap.put("url", childNode.getFirstChild().getNodeValue());
        } else if ("index-url".equalsIgnoreCase(childNode.getLocalName())) {
          urlMap.put("index-url", childNode.getFirstChild().getNodeValue());
        } else if ("get-url".equalsIgnoreCase(childNode.getLocalName())) {
          urlMap.put("get-url", childNode.getFirstChild().getNodeValue());
        } else if ("update-url".equalsIgnoreCase(childNode.getLocalName())) {
          urlMap.put("update-url", childNode.getFirstChild().getNodeValue());
        }
        // para obtener los mecanismos de seguridad soportados
        // para obtener los mecanismos de seguridad soportados
        if ("http-security".equalsIgnoreCase(childNode.getLocalName())) {
          for (int j = 0; j < childNode.getChildNodes().getLength(); j++) {
            Node securityNode = childNode.getChildNodes().item(j);
            if ("client-auth-methods".equalsIgnoreCase(securityNode.getLocalName())) {
              for (int k = 0; k < securityNode.getChildNodes().getLength(); k++) {
                Node authMethodNode = securityNode.getChildNodes().item(k);
                if ("httpsig".equalsIgnoreCase(authMethodNode.getLocalName())) {
                  hei.setClientHttpSignature(true);
                } else if ("tlscert".equalsIgnoreCase(authMethodNode.getLocalName())) {
                  hei.setClientTlsCert(true);
                }
              }
            } else if ("server-auth-methods".equalsIgnoreCase(securityNode.getLocalName())) {
              for (int k = 0; k < securityNode.getChildNodes().getLength(); k++) {
                Node authMethodNode = securityNode.getChildNodes().item(k);
                if ("httpsig".equalsIgnoreCase(authMethodNode.getLocalName())) {
                  hei.setServerHttpSignature(true);
                } else if ("tlscert".equalsIgnoreCase(authMethodNode.getLocalName())) {
                  hei.setServerTlsCert(true);
                }
              }
            }
          }
        }
      }
    }
    hei.setUrls(urlMap);

    return hei;
  }

  public RSAPublicKey findRsaPublicKey(final String fingerprint) {
    return client.findRsaPublicKey(fingerprint);
  }

  public RSAPublicKey findClientRsaPublicKey(final String fingerprint) {
    final RSAPublicKey rsaPublicKey = client.findRsaPublicKey(fingerprint);
    return rsaPublicKey != null && client.isClientKeyKnown(rsaPublicKey) ? rsaPublicKey : null;
  }

  public Collection<String> getHeisCoveredByClientKey(final RSAPublicKey rsapk) {
    return client.getHeisCoveredByClientKey(rsapk);
  }

  public Collection<es.minsait.ewpcv.registryclient.HeiEntry> getAllHeis() {
    return client.getAllHeis();
  }

  public HeiEntry getFilesHeiUrls(final String heiId) {
    return getHeiUrls(heiId, FILES_NAMESPACE, FILE_API_NAME, FILES_CLIENT_VERSION);
  }

}
