package es.minsait.ewpcv.common;

/**
 * The type Ewp event constants.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
public class EwpEventConstants {
  public static String OK = "OK";
  public static String KO = "KO";

  public static String NOTIFY = "NOTIFY";
  public static String INSERT = "INSERT";
  public static String REMOVE = "REMOVE";
  public static String UPDATE = "UPDATE";
  public static String CNR_PROCESSING_INDEX = "CNR PROCESSING INDEX";
  public static String CNR_PROCESSING_GET = "CNR PROCESSING GET";
  public static String ACCEPT = "ACCEPT";
  public static String APPROVE = "APPROVE";
  public static String REJECT = "REJECT";
  public static String UPDATE_LA_PROCESSING = "UPDATE LA PROCESSING";
  public static String REFRESH_INSERT = "REFRESH-INSERT";
  public static String REFRESH_UPDATE = "REFRESH-UPDATE";
  public static String REFRESH_REMOVE = "REFRESH-REMOVE";
  public static String REFRESH_PROCESSING_GET = "REFRESH-PROCESSING GET";
  public static String NO_ACTION = "NO ACTION";

  public static final String FETCH_FILE = "FETCH-FILE";

  public static final String ECHOECHO_POST = "ECHOECHO-POST";
  public static final String ECHOECHO_GET = "ECHOECHO-GET";
  public static final String FACTSHEETGET_GET = "FACTSHEETGET-GET";
  public static final String FACTSHEETGET_POST = "FACTSHEETGET-POST";
  public static final String IIAAPPROVALGET_GET = "IIAAPPROVALGET-GET";
  public static final String IIAAPPROVALGET_POST = "IIAAPPROVALGET-POST";
  public static final String IIAAPPROVALCNR_POST = "IIAAPPROVALCNR-POST";
  public static final String IIAINDEX_GET = "IIAINDEX-GET";
  public static final String IIAINDEX_POST = "IIAINDEX-POST";
  public static final String IIAGET_GET = "IIAGET-GET";
  public static final String IIAGET_POST = "IIAGET-POST";
  public static final String IIACNR_POST = "IIACNR-POST";
  public static final String IMOBILITYGET_GET = "IMOBILITYGET-GET";
  public static final String IMOBILITYGET_POST = "IMOBILITYGET-POST";
  public static final String IMOBILITYCNR_POST = "IMOBILITYCNR-POST";
  public static final String INSTITUTIONSGET_GET = "INSTITUTIONSGET-GET";
  public static final String INSTITUTIONSGET_POST = "INSTITUTIONSGET-POST";
  public static final String OMOBILITYINDEX_POST = "OMOBILITYINDEX-POST";
  public static final String OMOBILITYCNR_POST = "OMOBILITYCNR-POST";
  public static final String OMOBILITYGET_GET = "OMOBILITYGET-GET";
  public static final String OMOBILITYGET_POST = "OMOBILITYGET-POST";
  public static final String OMOBILITYINDEX_GET = "OMOBILITYINDEX-GET";
  public static final String OMOBILITYLASGET_GET = "OMOBILITYLASGET-GET";
  public static final String OMOBILITYLASGET_POST = "OMOBILITYLASGET-POST";
  public static final String OMOBILITYLASINDEX_GET = "OMOBILITYLASINDEX-GET";
  public static final String OMOBILITYLASINDEX_POST = "OMOBILITYLASINDEX-POST";
  public static final String OMOBILITYLASUPDATE_POST = "OMOBILITYLASUPDATE-POST";
  public static final String OMOBILITYLASCNR_POST = "OMOBILITYLASCNR-POST";
  public static final String TORGET_POST = "TORGET_POST";
  public static final String TORGET_GET = "TORGET_GET";
  public static final String TORINDEX_POST = "TORINDEX_POST";
  public static final String TORINDEX_GET = "TORINDEX_GET";
  public static final String TORCNR_POST = "TORCNR_POST";
  public static final String ORGANIZATIONUNITGET_GET = "ORGANIZATIONUNITGET-GET";

  public static final String FILEAPIGET_GET = "FILEAPI-GET";
  public static final String ORGANIZATIONUNITGET_POST = "ORGANIZATIONUNITGET-POST";

  public static final String STATISTICSOLA_GET = "STATISTICSOLA-GET";
  public static final String STATISTICSILA_GET = "STATISTICSILA-GET";
  public static final String STATISTICSIIA_GET = "STATISTICSIIA-GET";

  public static String ERROR_RESPONSE = "Error al almacenar la respuesta";

  public static final String UNDEFINED = "UNDEFINED";
}
