package es.minsait.ewpcv.config;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;

import es.minsait.ewpcv.batch.TorJob;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Trigger listener.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
public class TriggerListener implements org.quartz.TriggerListener {

  @Override
  /**
   * 
   * return true si hay que vetar la ejecucion del job
   */
  public boolean vetoJobExecution(final Trigger trigger, final JobExecutionContext context) {
    if (!context.getJobDetail().getJobClass().isInstance(TorJob.class)) {
      log.debug(getClass().getName() + " - " + "Se veta le ejecucion del job: "
          + context.getJobDetail().getJobClass().getName());
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String getName() {
    return "VetoTriggerListener";
  }

  @Override
  public void triggerFired(final Trigger trigger, final JobExecutionContext context) {

  }

  @Override
  public void triggerMisfired(final Trigger trigger) {

  }

  @Override
  public void triggerComplete(final Trigger trigger, final JobExecutionContext context,
      final Trigger.CompletedExecutionInstruction triggerInstructionCode) {

  }
}
