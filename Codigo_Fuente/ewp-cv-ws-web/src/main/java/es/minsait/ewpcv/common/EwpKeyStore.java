package es.minsait.ewpcv.common;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.Objects;

/**
 * The type Ewp key store.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Controller
public class EwpKeyStore {
  private static final Logger logger = LoggerFactory.getLogger(EwpKeyStore.class);
  @Autowired
  GlobalProperties properties;

  private String fomattedCertificate;
  private KeyStore truststore;
  private KeyStore keystore;
  private boolean successfullyInitiated;

  private Key ownPrivateKey;
  private String fomattedRsaPublicKey;
  private String ownPublicKeyFingerprint;

  @Value("${ewp.keystore.location}")
  private String keystoreLocation;
  @Value("${ewp.keystore.password}")
  private String keystorePassword;
  @Value("${ewp.keystore.type}")
  private String keystoreType;
  @Value("${ewp.keystore.certificate.alias}")
  private String keystoreCertificateAlias;

  @Value("${ewp.truststore.location}")
  private String truststoreLocation;
  @Value("${ewp.truststore.password}")
  private String truststorePassword;
  @Value("${ewp.truststore.type}")
  private String truststoreType;



  @PostConstruct
  private void loadKeystore() {

    if (Objects.isNull(keystoreLocation) || Objects.isNull(truststorePassword) || Objects.isNull(keystoreLocation)
        || Objects.isNull(keystorePassword) || Objects.isNull(keystoreCertificateAlias)) {
      logger.error("Missing keystore/truststore propeties");
      return;
    }

    try {
      truststore = KeyStore.getInstance(properties.getTruststoreType());
      truststore.load(new FileInputStream(truststoreLocation), truststorePassword.toCharArray());

      keystore = KeyStore.getInstance(properties.getKeystoreType());
      keystore.load(new FileInputStream(keystoreLocation), keystorePassword.toCharArray());

      retrieveCertificate(keystore, keystoreCertificateAlias);

      retrievePrivateKey(keystore, keystoreCertificateAlias, keystorePassword);

      successfullyInitiated = true;
    } catch (IOException | NoSuchAlgorithmException | CertificateException | KeyStoreException
        | UnrecoverableKeyException ex) {
      logger.error("Load keystore error", ex);
    }
  }

  private void retrievePrivateKey(final KeyStore keystore, final String alias, final String pwd)
      throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
    ownPrivateKey = (PrivateKey) keystore.getKey(alias, pwd.toCharArray());
  }

  private void retrieveCertificate(final KeyStore keystore, final String alias)
      throws KeyStoreException, CertificateEncodingException {
    final Certificate certificate = keystore.getCertificate(alias);
    final byte[] publicKey = Base64.getEncoder().encode(certificate.getPublicKey().getEncoded());
    this.fomattedRsaPublicKey = new String(publicKey).replaceAll("(.{1,64})", "$1\n");

    this.ownPublicKeyFingerprint = DigestUtils.sha256Hex(certificate.getPublicKey().getEncoded());

    final byte[] cert = Base64.getEncoder().encode(certificate.getEncoded());
    this.fomattedCertificate = new String(cert).replaceAll("(.{1,64})", "$1\n");
  }

  public String getCertificate() {
    return this.fomattedCertificate;
  }

  public Key getOwnPrivateKey() {
    return this.ownPrivateKey;
  }

  public String getRsaPublicKey() {
    return this.fomattedRsaPublicKey;
  }

  public String getOwnPublicKeyFingerprint() {
    return ownPublicKeyFingerprint;
  }

  public KeyStore getKeystore() {
    return this.keystore;
  }

  public KeyStore getTruststore() {
    return this.truststore;
  }

  public boolean isSuccessfullyInitiated() {
    return successfullyInitiated;
  }
}
