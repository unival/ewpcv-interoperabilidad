package es.minsait.ewpcv.common;

/**
 * The type Ewp request constants.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpRequestConstants {

  public static final String RESPONSE_VERIFIED = "Response is verified ok";
  // Parametros Servicio DISCOVERY
  public static final String DISCOVERY_URL_KEY = "url";

  // Parametros Servicio ECHO
  public static final String ECHO_URL_KEY = "url";

  // Parametros Servicio IIAS
  public static final String IIAS_HEI_ID = "hei_id";
  public static final String IIAS_IIA_ID = "iia_id";
  public static final String IIAS_IIA_CODE = "iia_code";
  public static final String IIAS_SEND_PDF = "send_pdf";
  public static final String IIAS_PARTNER_HEI_ID = "partner_hei_id";
  public static final String IIAS_RECEIVING_ACADEMIC_YEAR_ID = "receiving_academic_year_id";
  public static final String IIAS_MODIFIED_SINCE = "modified_since";

  // Parametros Servicio IIAS CNR
  public static final String IIAS_CNR_NOTIFIER_HEI_ID = "notifier_hei_id";
  public static final String IIAS_CNR_IIA_ID = "iia_id";

  // Parametros Servicio IIAS APPROVAL
  public static final String IIAS_APPROVAL_APPROVING_HEI_ID = "approving_hei_id";
  public static final String IIAS_APPROVAL_OWNER_HEI_ID = "owner_hei_id";
  public static final String IIAS_APPROVAL_IIA_ID = "iia_id";
  public static final String IIAS_APPROVAL_SEND_PDF = "send_pfd";

  // Parametros Servicio IIAS APPROVAL CNR
  public static final String IIAS_APPROVAL_CNR_APPROVING_HEI_ID = "approving_hei_id";
  public static final String IIAS_APPROVAL_CNR_OWNER_HEI_ID = "owner_hei_id";
  public static final String IIAS_APPROVAL_CNR_IIA_ID = "iia_id";
  // Parametros Servicio Factsheets
  public static final String FACTSHEET_HEI_ID = "hei_id";
  public static final String FACTSHEET_GET_URL_KEY = "url";
  // claves para recuperar las URLS de los mapas
  public static final String IIAS_APPROVAL_CNR_URL_KEY = "url";
  public static final String OUTGOING_MOBILITY_CNR = "omobility-cnr";
  public static final String OUTGOING_MOBILITY_CNR_URL_KEY = "url";
  public static final String INCOMING_MOBILITY_CNR = "imobility-cnr";
  public static final String INCOMING_MOBILITY_CNR_URL_KEY = "url";
  public static final String INCOMING_MOBILITY_TOR_CNR = "imobility-tor-cnr";
  public static final String INCOMING_MOBILITY_TOR_CNR_URL_KEY = "url";
  public static final String OUTGOING_MOBILITY_LA_CNR = "omobility-la-cnr";
  public static final String OUTGOING_MOBILITY_LA_GET = "get-url";
  public static final String OUTGOING_MOBILITY_LA_INDEX = "index-url";
  public static final String OUTGOING_MOBILITY_LA_UPDATE = "update-url";
  public static final String OUTGOING_MOBILITY_GET_URL = "get-url";
  public static final String IIAS_APPROVAL_GET_URL_KEY = "url";
  public static final String IIAS_INDEX_URL_KEY = "index-url";
  public static final String IIAS_GET_URL_KEY = "get-url";
  public static final String IIAS_CNR_URL_KEY = "url";
  // Parametros servicio Institutiosn
  public static final String INSTITUTIONS_GET_URL_KEY = "url";
  public static final String INSTITUTIONS_GET_HEI_ID = "hei_id";
  // Parametros servicio OrganizationUnit
  public static final String ORGANIZATIONUNIT_GET_URL_KEY = "url";
  public static final String ORGANIZATIONUNIT_GET_HEI_ID = "hei_id";
  public static final String ORGANIZATIONUNIT_GET_OUNIT_ID = "ounit_id";
  public static final String ORGANIZATIONUNIT_GET_OUNIT_CODE = "ounit_code";
  // Parametros servicio mobilities
  public static final String IMOBILITIES_GET_URL_KEY = "get-url";
  public static final String IMOBILITIES_GET_RECEIVING_HEI_ID = "receiving_hei_id";
  public static final String IMOBILITIES_GET_OMOBILITY_ID = "omobility_id";
  public static final String IMOBILITIES_CNR_RECEIVING_HEI_ID = "receiving_hei_id";
  public static final String IMOBILITIES_CNR_OMOBILITY_ID = "omobility_id";
  public static final String OUTGOING_MOBILITY_GET_SENDING_HEI_ID = "sending_hei_id";
  public static final String OMOBILITIES_INDEX_SENDING_HEI_ID = "sending_hei_id";
  public static final String OMOBILITIES_INDEX_RECEIVING_HEI_ID = "receiving_hei_id";
  public static final String OMOBILITIES_INDEX_URL_KEY = "index-url";

  public static final String OUTGOING_MOBILITY_LA_CNR_URL_KEY = "url";
  // Parametros Servicio LAs
  public static String OMOBILITY_LA_SENDING_HEI_ID = "sending_hei_id";
  public static String OMOBILITY_LA_RECEIVING_HEI_ID = "receiving_hei_id";
  public static String OMOBILITY_LA_RECEIVING_ACADEMIC_YEAR = "receiving_academic_year_id";
  public static String OMOBILITY_LA_GLOBAL_ID = "global_id";
  public static String OMOBILITY_LA_MOBILITY_TYPE = "mobility_type";
  public static String OMOBILITY_LA_MODIFIED_SINCE = "modified_since";
  public static String OMOBILITY_LA_OMOBILITY_ID = "omobility_id";
  // Parametros Servicio LAs CNR
  public static String OMOBILITY_LA_CNR_SENDING_HEI_ID = "sending_hei_id";
  public static String OMOBILITY_LA_CNR_OMOBILITY_ID = "omobility_id";
  // Parametros Servicio TORs
  public static String TOR_GET_RECEIVING_HEI_ID = "receiving_hei_id";
  public static String TOR_GET_OMOBILITY_ID = "omobility_id";
  public static String TOR_GET_URL_KEY = "get-url";
  public static String TOR_INDEX_URL_KEY = "index-url";
  public static String TOR_INDEX_SENDING_HEI_ID = "sending_hei_id";
  public static String TOR_INDEX_MODIFIED_SINCE = "modified_since";
  //Parametros File APi
  public static String FILE_ID = "file_id";
  public static String FILE_API_GET_URL_KEY = "url";

  // Parametros Servicio Monitoring
  public static String MONITORING_URL_KEY = "url";
  public static String MONITORING_SERVER_HEI_ID = "server_hei_id";
  public static String MONITORING_API_NAME = "api_name";
  public static String MONITORING_ERROR_TYPE = "error_type";
  public static String MONITORING_API_URL = "api_url";
  public static String MONITORING_HTTP_CODE = "http_code";
  public static String MONITORING_SERVER_MESSAGE = "server_message";
  public static String MONITORING_CLIENT_MESSAGE = "client_message";


}
