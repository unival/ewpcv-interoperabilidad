package es.minsait.ewpcv.security;

import org.springframework.util.StreamUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Cached body servlet request.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
/*
 * Este wrapper se utiliza para cachear el body de la request y poder leerlo varias veces sin consumir el input stream
 * Adicionalmente sobreescribe el mapa de parametros de la request para poder modificarlo en los interceptores, ya que
 * en caso contrario el mapa es inmutable.
 */
public class CachedBodyServletRequest extends HttpServletRequestWrapper {

  Map<String, String[]> parameterMap;
  private byte[] cachedBody;

  public CachedBodyServletRequest(final HttpServletRequest request) throws IOException {
    super(request);
    final InputStream requestInputStream = request.getInputStream();
    this.cachedBody = StreamUtils.copyToByteArray(requestInputStream);

    this.parameterMap = new HashMap<>();
    for (final String key : request.getParameterMap().keySet()) {
      this.parameterMap.put(key, request.getParameterMap().get(key));
    }
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {
    return new CachedBodyInputStream(this.cachedBody);
  }

  @Override
  public BufferedReader getReader() throws IOException {
    final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.cachedBody);
    return new BufferedReader(new InputStreamReader(byteArrayInputStream));
  }

  @Override
  public Map<String, String[]> getParameterMap() {
    return parameterMap;
  }


  /**
   * The type Cached body input stream.
   *
   * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
   */
  public class CachedBodyInputStream extends ServletInputStream {

    private InputStream cachedBodyInputStream;

    public CachedBodyInputStream(final byte[] cachedBody) {
      this.cachedBodyInputStream = new ByteArrayInputStream(cachedBody);
    }

    @Override
    public boolean isFinished() {
      try {
        return cachedBodyInputStream.available() == 0;
      } catch (final IOException e) {
        return true;
      }
    }

    @Override
    public boolean isReady() {
      return true;
    }

    @Override
    public void setReadListener(final ReadListener listener) {

    }

    @Override
    public int read() throws IOException {
      return cachedBodyInputStream.read();
    }
  }
}
