package es.minsait.ewpcv.common;

import java.io.Serializable;
import java.util.List;

/**
 * The type Base client response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class BaseClientResponse implements Serializable {
  private int statusCode;
  private String mediaType;
  private String errorMessage;
  private String rawResponse;
  private long duration;

  private List<String> headers;
  private String httpsecMsg;
  private Boolean xsdComplies = true;

  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(final int statusCode) {
    this.statusCode = statusCode;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaType(final String mediaType) {
    this.mediaType = mediaType;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getRawResponse() {
    return this.rawResponse;
  }

  public void setRawResponse(final String raw) {
    this.rawResponse = raw;
  }

  public long getDuration() {
    return this.duration;
  }

  public void setDuration(final long duration) {
    this.duration = duration;
  }

  public List<String> getHeaders() {
    return this.headers;
  }

  public void setHeaders(final List<String> headers) {
    this.headers = headers;
  }

  public String getHttpsecMsg() {
    return httpsecMsg;
  }

  public void setHttpsecMsg(final String httpsecMsg) {
    this.httpsecMsg = httpsecMsg;
  }

  public Boolean getXsdComplies() {
    return xsdComplies;
  }

  public void setXsdComplies(Boolean xsdComplies) {
    this.xsdComplies = xsdComplies;
  }
}
