package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.FactsheetsUpdateJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;


/**
 * The type Factsheets update job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class FactsheetsUpdateJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailFactsheetsUpdateJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(FactsheetsUpdateJob.class);
    jobDetailFactory.setDescription("Batch para actulizar los Factsheets");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerFactsheetsUpdateJob(
      @Qualifier("jobDetailFactsheetsUpdateJob") final JobDetail jobDetail,
      @Value("${ewp.refresco.factsheets.cron}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
