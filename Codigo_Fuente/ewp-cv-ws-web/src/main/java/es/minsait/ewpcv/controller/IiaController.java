package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.iias7.cnr.IiaCnrResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasIndexResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Iia controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/iias", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class IiaController {

  @Autowired
  private IIiasService iiaService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.iia.ids}")
  private int maxIiaIds;

  @EwpAuthenticate
  @GetMapping(path = "/index")
  public ResponseEntity<IiasIndexResponse> getIndex(
          @RequestParam(value = "receiving_academic_year_id", required = false) final List<String> receivingAcademicYearId,
          @RequestParam(value = "modified_since", required = false) final List<String> modifiedSince,
          final HttpServletRequest request, @RequestParam(required = false) final MultiValueMap<String, String> emapParam) {
    log.debug("Inicio invocacion GET /index IIA");

    List<String> partnerHeiId = utils.heisCovered(request);

    final EventMDTO event = utils.initializeAuditableEvent(partnerHeiId.get(0), emapParam, request,
            EwpEventConstants.IIAINDEX_GET, NotificationTypes.IIA, null);

    String modifiedSinceString = null;
    try {
      doParamsValidationIndex(partnerHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IIAAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
    }

    final List<String> iiaIdsList = iiaService.getIiaIdsByCriteria(globalProperties.getInstance(), partnerHeiId,
            receivingAcademicYearId, modifiedSinceString);
    final IiasIndexResponse respuesta = new IiasIndexResponse();
    respuesta.getIiaId().addAll(iiaIdsList);

    if (utils.getValue(EwpConstants.IIAAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("Fin invocacion GET /index IIA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/index")
  public ResponseEntity<IiasIndexResponse> postIndex(@RequestBody(required = false) final MultiValueMap<String, String> emapParam,
                                                     final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /index IIA");

    // Extraemos los parámetros del form que ahora puede no venir
    final List<String> receivingAcademicYearId = emapParam != null ? emapParam.get("receiving_academic_year_id") : null;
    final List<String> modifiedSince =  emapParam != null ? emapParam.get("modified_since") : null;
    String modifiedSinceString = null;
    List<String> partnerHeiId = utils.heisCovered(request);

    final EventMDTO event = utils.initializeAuditableEvent(partnerHeiId.get(0), emapParam, request,
            EwpEventConstants.IIAINDEX_POST, NotificationTypes.IIA, null);

    try {
      doParamsValidationIndex(partnerHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IIAAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (Objects.nonNull(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
    }

    final List<String> iiaIdsList = iiaService.getIiaIdsByCriteria(globalProperties.getInstance(), partnerHeiId,
            receivingAcademicYearId, modifiedSinceString);
    final IiasIndexResponse respuesta = new IiasIndexResponse();
    respuesta.getIiaId().addAll(iiaIdsList);

    if (utils.getValue(EwpConstants.IIAAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    log.debug("Fin invocacion POST /index IIA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<IiasGetResponse> getGet(
          @RequestParam(value = "iia_id") final List<String> iiaId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /get IIA");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.IIAGET_GET,
            NotificationTypes.IIA, Objects.nonNull(iiaId) ? iiaId.toString() : null);

    try {
      if ((Objects.nonNull(iiaId) && iiaId.size() > maxIiaIds)) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IIAAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final IiasGetResponse respuesta = new IiasGetResponse();
    final List<IiasGetResponse.Iia> iiaList =
            iiaService.getIiaListByCriteria(globalProperties.getInstance(), utils.heisCovered(request), iiaId);
    iiaService.updateCoopCondHash(iiaList, globalProperties.getInstance());
    respuesta.getIia().addAll(iiaList);

    if (utils.getValue(EwpConstants.IIAAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("Fin invocacion GET /get IIA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<IiasGetResponse> postGet(@RequestBody final MultiValueMap<String, String> iiaGetList,
                                                 final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get IIA");

    // Extraemos los parámetros del form
    final List<String> iiaId = iiaGetList.get("iia_id");

    final EventMDTO event = utils.initializeAuditableEvent(null, iiaGetList, request, EwpEventConstants.IIAGET_POST,
            NotificationTypes.IIA, Objects.nonNull(iiaId) ? iiaId.toString() : null);

    try {
      if ((Objects.nonNull(iiaId) && iiaId.size() > maxIiaIds)) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IIAAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final IiasGetResponse respuesta = new IiasGetResponse();
    final List<IiasGetResponse.Iia> iiaList =
            iiaService.getIiaListByCriteria(globalProperties.getInstance(), utils.heisCovered(request), iiaId);
    iiaService.updateCoopCondHash(iiaList, globalProperties.getInstance());
    respuesta.getIia().addAll(iiaList);

    if (utils.getValue(EwpConstants.IIAAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    log.debug("Fin invocacion POST /get IIA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity<IiaCnrResponse> postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                                final HttpServletRequest request) {

    List<String> notifierHeiId = utils.heisCovered(request);
    final String iiaId = cnrlist.toSingleValueMap().get("iia_id");

    final EventMDTO event = utils.initializeAuditableEvent(notifierHeiId, cnrlist, request,
            EwpEventConstants.IIACNR_POST, NotificationTypes.IIA, Objects.nonNull(iiaId) ? iiaId.toString() : null);

    if (CollectionUtils.isEmpty(notifierHeiId) || notifierHeiId.size()>1 || Strings.isNullOrEmpty(iiaId)) {
      final EwpWebApplicationException ex =
              new EwpWebApplicationException("Missing argumanets for notification.", HttpStatus.BAD_REQUEST.value());
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IIAAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
    notificationService.saveNotificationIiaRefresh(notifierHeiId.get(0), iiaId, globalProperties.getInstance());

    eventService.save(event);
    return ResponseEntity.ok(new IiaCnrResponse());
  }

  private void doParamsValidationIndex(final List<String> partnerHeiId, final List<String> receivingAcademicYearId,
                                       final List<String> modifiedSince) throws EwpWebApplicationException {
    if (CollectionUtils.isNotEmpty(partnerHeiId) && partnerHeiId.size()>1){
      throw new EwpWebApplicationException("Can't determine hei_id from signature information", HttpStatus.BAD_REQUEST.value());
    }
    if (!CollectionUtils.isEmpty(receivingAcademicYearId)) {
      for (final String yearId : receivingAcademicYearId) {
        if (!utils.isValidFormatReceivingAcademicYear(yearId)) {
          throw new EwpWebApplicationException("Receiving_academic_year_id in incorrect format.",
                  HttpStatus.BAD_REQUEST.value());
        }
      }
    }
    if (CollectionUtils.isNotEmpty(modifiedSince) && modifiedSince.size() > 1) {
      throw new EwpWebApplicationException("Multiple modified_since parameters.", HttpStatus.BAD_REQUEST.value());
    }
  }


}
