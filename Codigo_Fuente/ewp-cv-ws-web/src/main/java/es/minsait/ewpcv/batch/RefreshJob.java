package es.minsait.ewpcv.batch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.service.api.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;

import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasIndexResponse;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.ImobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsIndexResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesIndexResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasIndexResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Refresh job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class RefreshJob implements Job {

  @Value("${ewp.refresh.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.refresh.block.size:50}")
  private int blockSize;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private OmobilityLasService omobilityLasService;

  @Autowired
  private IIiasService iiaService;

  @Autowired
  private ITorService torService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private Utils utils;

  @Autowired
  private INotificationService notificationService;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${ewp.iia.autogenerate:true}")
  private boolean shouldAutogenerate;

  @Value("${ewp.iia.autogenerate.with.candidates:false}")
  private boolean autogenerateWithCandidates;

  @Value("${batch.refrescar.iias:true}")
  private boolean refrescarIias;

  @Value("${batch.refrescar.incomingmobility:true}")
  private boolean refrescarIncomingMobility;

  @Value("${batch.refrescar.incomingmobilitytor:true}")
  private boolean refrescarIncomingMobilityTOR;

  @Value("${batch.refrescar.outgoingmobility:true}")
  private boolean refrescarOutgoingMobility;

  @Value("${batch.refrescar.outgoingmobilityla:true}")
  private boolean refrescarOutgoingMobilityLA;

  @Value("${ewp.refresco.modify.since.dias:1}")
  private int modifiedSinceDays;


  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    String modifiedSince = null;
    if (modifiedSinceDays > 0) {
      final String formatter = "yyyy-MM-dd'T'HH:mm:ssXXX";
      final DateFormat format = new SimpleDateFormat(formatter);

      final Calendar c = Calendar.getInstance();
      c.setTime(new Date());
      c.add(Calendar.DATE, -modifiedSinceDays);
      modifiedSince = format.format(c.getTime());
    }


    if (refrescarIias) {
      log.info("***Comienza proceso de refresco automatico de IIAs");
      List<HeiEntry> iiaHeis = registry.getIiaHeisWithUrls();
      // filtramos para solo procesar los indicados en la property si esta informada
      iiaHeis = utils.filterHeisByProperties(iiaHeis, globalProperties.getDefaultRefreshHeiIds());

      for (final HeiEntry hei : iiaHeis) {
        refrescarIIAS(hei, modifiedSince);
      }
    } else {
      log.info("***No se ejecuta el proceso de refresco automatico de IIAS por configuracion");
    }

    if (refrescarOutgoingMobility) {
      log.info("***Comienza proceso de refresco automatico de Omobilities");
      List<HeiEntry> omobilitiesHeis = registry.getOmobilitiesHeisWithUrls();
      omobilitiesHeis = utils.filterHeisByProperties(omobilitiesHeis, globalProperties.getDefaultRefreshHeiIds());

      for (final HeiEntry hei : omobilitiesHeis) {
        refrescarOmobilities(hei, modifiedSince);
      }
    } else {
      log.info("***No se ejecuta el proceso de refresco de omobilities por configuracion");
    }

    if (refrescarIncomingMobility) {
      log.info("***Comienza proceso de refresco automatico de Imobilities");
      List<HeiEntry> imobilitiesHeis = registry.getImobilitiesHeisWithUrls();
      imobilitiesHeis = utils.filterHeisByProperties(imobilitiesHeis, globalProperties.getDefaultRefreshHeiIds());

      for (final HeiEntry hei : imobilitiesHeis) {
        refrescarImobilities(hei, modifiedSince);
      }
    } else {
      log.info("***No se ejecuta el proceso de refresco de Imobilities por configuracion");
    }

    if (refrescarOutgoingMobilityLA) {
      log.info("***Comienza proceso de refresco automatico de LAs");
      List<HeiEntry> laHeis = registry.getOmobilitiesLaHeisWithUrls();
      laHeis = utils.filterHeisByProperties(laHeis, globalProperties.getDefaultRefreshHeiIds());

      for (final HeiEntry hei : laHeis) {
        refrescarLAs(hei, modifiedSince);
      }
    } else {
      log.info("***No se ejecuta el proceso de refresco de OLAs por configuracion");
    }

    if (refrescarIncomingMobilityTOR) {
      log.info("***Comienza proceso de refresco automatico de TORs");
      List<HeiEntry> torsHeis = registry.getImobilityTorsCnrHeisWithUrls();
      torsHeis = utils.filterHeisByProperties(torsHeis, globalProperties.getDefaultRefreshHeiIds());

      for (final HeiEntry hei : torsHeis) {
        refrescarTors(hei, modifiedSince);
      }
    } else {
      log.info("***No se ejecuta el proceso de refresco de Tor por configuracion");
    }

  }

  private void refrescarLAs(final HeiEntry hei, final String modifiedSince) {
    Boolean registraEvento;
    try {
      // insercion-actualizacion de registros nuevos-modificados
      ClientResponse response = invocarLAIndex(hei, modifiedSince);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final OmobilityLasIndexResponse indexResponse = (OmobilityLasIndexResponse) response.getResult();

        for (final String id : indexResponse.getOmobilityId()) {
          registraEvento = true;
          final EventMDTO event =
                  utils.initializeJobEvent(NotificationTypes.LA, hei.getId(), globalProperties.getInstance(), id, null);
          event.setStatus(EwpEventConstants.OK);

          final MobilityMDTO learnAgrLocal = omobilityLasService.findLastMobilityRevisionByIdAndSendingHeiId(id, hei.getId());
          response = invocarLAGet(hei, id, event);

          if (!utils.verifyResponse(null,event,response, EwpEventConstants.REFRESH_PROCESSING_GET)) {
            continue;
          }
          final OmobilityLasGetResponse getResponse = (OmobilityLasGetResponse) response.getResult();
          if (Objects.isNull(learnAgrLocal)) {
            for (final LearningAgreement la : getResponse.getLa()) {
              registraEvento = omobilityLasService.insertarOmobilities(la, globalProperties.getInstance());
              event.setEventType(registraEvento ? EwpEventConstants.REFRESH_INSERT : EwpEventConstants.NO_ACTION);
            }
          } else {
            for (final LearningAgreement la : getResponse.getLa()) {
              registraEvento =
                      omobilityLasService.actualizarOmobilities(la, learnAgrLocal, globalProperties.getInstance());
              event.setEventType(registraEvento ? EwpEventConstants.REFRESH_UPDATE : EwpEventConstants.NO_ACTION);
            }
          }
          if (registraEvento) {
            eventService.save(event);
          }
        }

      }
      // eliminacion de registros borrados
      response = invocarLAIndex(hei, null);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final OmobilityLasIndexResponse indexResponse = (OmobilityLasIndexResponse) response.getResult();
        final List<String> localLas =
                omobilityLasService.findLocalOMobilitiesId(hei.getId(), globalProperties.getInstance(), modifiedSince);
        localLas.removeAll(indexResponse.getOmobilityId());
        for (final String id : localLas) {
          final EventMDTO event = utils.initializeJobEvent(NotificationTypes.LA, hei.getId(),
                  globalProperties.getInstance(), id, EwpEventConstants.REFRESH_REMOVE);
          event.setStatus(EwpEventConstants.OK);
          omobilityLasService.eliminarMobilityByIdAndSendingHeiId(id, globalProperties.getInstance());
          eventService.save(event);
        }
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de LAs", getClass().getName(), e);
    }
  }

  private void refrescarIIAS(final HeiEntry hei, final String modifiedSince) {
    Boolean registraEvento;

    try {
      // insercion-actualizacion de registrs nuevos-modificados
      ClientResponse response = invocarIIAIndex(hei, modifiedSince);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final IiasIndexResponse indexResponse = (IiasIndexResponse) response.getResult();

        for (final String id : indexResponse.getIiaId()) {
          registraEvento = true;
          final EventMDTO event =
                  utils.initializeJobEvent(NotificationTypes.IIA, hei.getId(), globalProperties.getInstance(), id, null);
          event.setStatus(EwpEventConstants.OK);

          final IiaMDTO iiaCopiaRemota = iiaService.buscaPorIiaRemotoAndIsRemote(id, Boolean.TRUE);
          response = invocarIIAGet(hei, id, event);

          if (!utils.verifyResponse(null, event, response, EwpEventConstants.REFRESH_PROCESSING_GET)) {
            continue;
          }
          final IiasGetResponse getResponse = (IiasGetResponse) response.getResult();
          if (Objects.isNull(getResponse) || CollectionUtils.isEmpty(getResponse.getIia())) {
            event.setEventType(EwpEventConstants.REFRESH_PROCESSING_GET);
            event.setObservations("respuesta vacia");
            continue;
          }
          final IiasGetResponse.Iia remoto = getResponse.getIia().get(0);
          if (Objects.isNull(iiaCopiaRemota)) {
            registraEvento = iiaService.guardarIiaCopiaRemota(remoto, globalProperties.getInstance());
            event.setEventType(registraEvento ? EwpEventConstants.REFRESH_INSERT : EwpEventConstants.NO_ACTION);
          } else {
            registraEvento =
                    iiaService.actualizarIiaCopiaRemota(remoto, iiaCopiaRemota, globalProperties.getInstance());
            //event.setEventType(registraEvento ? EwpEventConstants.REFRESH_UPDATE : EwpEventConstants.NO_ACTION);
            if(registraEvento){
              boolean isResponseVerified = utils.verifyResponse(null, event, response,  EwpEventConstants.REFRESH_UPDATE);
              if (isResponseVerified) {
                event.setEventType(EwpEventConstants.REFRESH_UPDATE);
                event.setObservations("respuesta verificada correctamente");
              } else {
                event.setEventType(EwpEventConstants.REFRESH_UPDATE);
                event.setObservations("respuesta vacia");
              }
            }else{
              event.setEventType(EwpEventConstants.NO_ACTION);
            }
          }
          // Si se han hecho cambios o insercion y no se bindea automaticamente entonces procedemos a generar la copia
          // autogenerada
          if (registraEvento && !bindeaCopiaPropia(remoto, event) && shouldAutogenerate) {
            if (iiaService.generarCopiaAutogeneradaIia(remoto, globalProperties.getInstance(),
                    autogenerateWithCandidates)) {
              event.setObservations("autogeneracion de copia local vinculada");
            }
          }
          if (registraEvento) {
            eventService.save(event);
          }
        }
      }
      // eliminacion de registros borrados
      response = invocarIIAIndex(hei, null);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final IiasIndexResponse indexResponse = (IiasIndexResponse) response.getResult();
        final List<String> localLas =
                iiaService.buscaRemoteIdPorIsRemoteAndInstitutions(hei.getId(), globalProperties.getInstance(), Boolean.TRUE);
        localLas.removeAll(indexResponse.getIiaId());
        for (final String id : localLas) {
          final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IIA, hei.getId(),
                  globalProperties.getInstance(), id, EwpEventConstants.REFRESH_REMOVE);
          event.setStatus(EwpEventConstants.OK);
          iiaService.eliminarIiaRemoto(id);
          eventService.save(event);
        }
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de IIAs", getClass().getName(), e);
    }
  }

  private void refrescarOmobilities(final HeiEntry hei, final String modifiedSince) {
    Boolean registraEvento;
    try {
      // insercion-actualizacion de registrs nuevos-modificados
      ClientResponse response = invocarOmobilitiesIndex(hei, modifiedSince);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final OmobilitiesIndexResponse indexResponse = (OmobilitiesIndexResponse) response.getResult();

        for (final String id : indexResponse.getOmobilityId()) {
          registraEvento = true;
          final EventMDTO event = utils.initializeJobEvent(NotificationTypes.OMOBILITY, hei.getId(),
                  globalProperties.getInstance(), id, null);
          event.setStatus(EwpEventConstants.OK);

          final MobilityMDTO local = omobilityLasService.findLastMobilityRevisionByIdAndSendingHeiId(id, hei.getId());
          response = invocarOmobilitiesGet(hei, id, event);

          if (!utils.verifyResponse(null,event,response, EwpEventConstants.REFRESH_PROCESSING_GET)) {
            continue;
          }
          final OmobilitiesGetResponse getResponse = (OmobilitiesGetResponse) response.getResult();
          if (Objects.isNull(getResponse) || CollectionUtils.isEmpty(getResponse.getSingleMobilityObject())) {
            event.setEventType(EwpEventConstants.REFRESH_PROCESSING_GET);
            event.setObservations("respuesta vacia");
            continue;
          }
          final StudentMobilityForStudies remoto = getResponse.getSingleMobilityObject().get(0);
          if (Objects.isNull(local)) {
            registraEvento = omobilityLasService.guardarOmobilityCopiaRemota(remoto, globalProperties.getInstance());
            event.setEventType(registraEvento ? EwpEventConstants.REFRESH_INSERT : EwpEventConstants.NO_ACTION);
          } else {
            registraEvento = omobilityLasService.actualizarOmobility(remoto, local, globalProperties.getInstance());
            event.setEventType(registraEvento ? EwpEventConstants.REFRESH_UPDATE : EwpEventConstants.NO_ACTION);
          }

          if (registraEvento) {
            eventService.save(event);
          }
        }
      }
      // eliminacion de registros borrados
      response = invocarOmobilitiesIndex(hei, null);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final OmobilitiesIndexResponse indexResponse = (OmobilitiesIndexResponse) response.getResult();
        final List<String> localOmobilities =
                omobilityLasService.findLocalOMobilitiesId(hei.getId(), globalProperties.getInstance(), null);
        localOmobilities.removeAll(indexResponse.getOmobilityId());
        for (final String id : localOmobilities) {
          final EventMDTO event = utils.initializeJobEvent(NotificationTypes.OMOBILITY, hei.getId(),
                  globalProperties.getInstance(), id, EwpEventConstants.REFRESH_REMOVE);
          event.setStatus(EwpEventConstants.OK);
          omobilityLasService.eliminarMobilityByIdAndSendingHeiId(id, globalProperties.getInstance());
          eventService.save(event);
        }
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Omobilities", getClass().getName(), e);
    }
  }

  private void refrescarImobilities(final HeiEntry hei, final String modifiedSince) {
    Boolean registraEvento;

    try {
      // Buscar en BBDD todas nuestras outgoin mobilities (enviadas por nosotros)
      // (que no tengan outgoing status cancelled o recognized, incomming status approved, y no tengan las dos actual
      // dates informadas)

      final List<String> imobilitesIds = omobilityLasService.findImobilitiesARefrescar(globalProperties.getInstance());

      for (final String mobilityId : imobilitesIds) {
        registraEvento = true;
        final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IMOBILITY, hei.getId(),
                globalProperties.getInstance(), mobilityId, null);
        event.setStatus(EwpEventConstants.OK);

        final ClientResponse response = invocarImobilitiesGet(hei, mobilityId, event);

        if (!utils.verifyResponse(null,event,response, EwpEventConstants.REFRESH_PROCESSING_GET)) {
          continue;
        }
        final ImobilitiesGetResponse getResponse = (ImobilitiesGetResponse) response.getResult();
        if (Objects.isNull(getResponse) || CollectionUtils.isEmpty(getResponse.getSingleIncomingMobilityObject())) {
          event.setEventType(EwpEventConstants.REFRESH_PROCESSING_GET);
          event.setObservations("respuesta vacia");
          continue;
        }
        final eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies remoto =
                getResponse.getSingleIncomingMobilityObject().get(0);

        if (Objects.nonNull(remoto)) {
          registraEvento = omobilityLasService.actualizarImobility(remoto, globalProperties.getInstance());
          event.setEventType(registraEvento ? EwpEventConstants.REFRESH_UPDATE : EwpEventConstants.NO_ACTION);
        }

        if (registraEvento) {
          eventService.save(event);
        }
      }

    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Imobilities", getClass().getName(), e);
    }
  }

  private void refrescarTors(final HeiEntry hei, final String modifiedSince) {
    Boolean registraEvento;
    try {
      // insercion-actualizacion de registrs nuevos-modificados
      ClientResponse response = invocarTorIndex(hei, modifiedSince);

      if (response.getStatusCode() == HttpStatus.SC_OK
              && EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        final ImobilityTorsIndexResponse indexResponse = (ImobilityTorsIndexResponse) response.getResult();

        for (final String id : indexResponse.getOmobilityId()) {
          registraEvento = true;
          final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IMOBILITY_TOR, hei.getId(),
                  globalProperties.getInstance(), id, null);
          event.setStatus(EwpEventConstants.OK);

          if (Objects.isNull(omobilityLasService.findLastMobilityRevisionByIdAndSendingHeiId(id, globalProperties.getInstance()))) {
            log.info(
                    "se ha tratado de actualizar un Tor para una mobility que no existe en el sistema, lo ignoramos");
            continue;
          }

          final MobilityMDTO mobilityCopiaRemota = torService.obtenerMobilityWithTor(id);
          response = invocarTorGet(hei, id, event);

          if (!utils.verifyResponse(null,event,response, EwpEventConstants.REFRESH_PROCESSING_GET)) {
            continue;
          }
          final ImobilityTorsGetResponse getResponse = (ImobilityTorsGetResponse) response.getResult();
          if (Objects.isNull(getResponse) || CollectionUtils.isEmpty(getResponse.getTor())) {
            event.setEventType(EwpEventConstants.REFRESH_PROCESSING_GET);
            event.setObservations("respuesta vacia");
            continue;
          }
          final ImobilityTorsGetResponse.Tor remoto = getResponse.getTor().get(0);
          if (Objects.isNull(mobilityCopiaRemota)) {
            torService.guardarMobilityTorCopiaRemota(remoto, id, globalProperties.getInstance());
            event.setEventType(EwpEventConstants.REFRESH_INSERT);
          } else {
            registraEvento = torService.actualizarTor(remoto, globalProperties.getInstance());
            event.setEventType(registraEvento ? EwpEventConstants.REFRESH_UPDATE : EwpEventConstants.NO_ACTION);
          }

          if (registraEvento) {
            eventService.save(event);
          }
        }
      }

      /*
       * // eliminacion de registros borrados response = invocarTorIndex(hei, null);
       *
       * if (response.getStatusCode() == HttpStatus.SC_OK &&
       * EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) { final
       * ImobilityTorsIndexResponse indexResponse = (ImobilityTorsIndexResponse) response.getResult(); final
       * List<String> localTorsMobilities = torService.findLocalMobilitiesWithTorId(hei.getId(),
       * globalProperties.getInstance()); localTorsMobilities.removeAll(indexResponse.getOmobilityId()); for (final
       * String id : localTorsMobilities) { final EventMDTO event = new EventMDTO(); event.setEventDate(new Date());
       * event.setOwnerHei(hei.getId()); event.setTriggeringHei(globalProperties.getInstance());
       * event.setChangedElementIds(id); event.setElementType(NotificationTypes.IMOBILITY_TOR);
       * event.setEventType(EwpEventConstants.REFRESH_REMOVE); event.setStatus(EwpEventConstants.OK);
       * torService.eliminarTorByMobilityId(id); eventService.save(event); } }
       */

    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de TORs", getClass().getName(), e);
    }
  }

  /**
   * Vincula la copia local del iias si la remota que vuelca esta asociada a uno existente.
   *
   * @param remoto
   * @param event
   * @return
   */
  private boolean bindeaCopiaPropia(final IiasGetResponse.Iia remoto, final EventMDTO event) {
    boolean bindeado = false;
    // obtenemos lel identificador de nuestro iias que nos envian
    final Optional<IiasGetResponse.Iia.Partner> copiaLocalEnRemoto = remoto.getPartner().stream()
            .filter(partner -> partner.getHeiId().equalsIgnoreCase(globalProperties.getInstance())).findAny();
    // si viene informado es que ellos lo tienen vinculado
    if (copiaLocalEnRemoto.isPresent() && StringUtils.isNotEmpty(copiaLocalEnRemoto.get().getIiaId())) {

      final String iiaId = copiaLocalEnRemoto.get().getIiaId();
      final IiaMDTO iiaLocal = iiaService.findByInteropId(iiaId);
      // obtenemos los datos de su iia
      final Optional<IiasGetResponse.Iia.Partner> partnerRemoto = remoto.getPartner().stream()
              .filter(partner -> !partner.getHeiId().equals(globalProperties.getInstance())).findFirst();
      // si viene informado lo añadimos al nuestro para vincularlo
      if (partnerRemoto.isPresent()) {
        iiaLocal.setIsRemote(Boolean.FALSE);
        iiaLocal.setRemoteIiaId(partnerRemoto.get().getIiaId());
        iiaLocal.setRemoteIiaCode(partnerRemoto.get().getIiaCode());
        iiaService.save(iiaLocal);
        event.setObservations("vinculacion de copias automatica");
        bindeado = true;
      }
    }
    return bindeado;
  }

  private ClientResponse invocarLAGet(final HeiEntry hei, final String elementId, EventMDTO event)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.OMOBILITY_LA_OMOBILITY_ID, Arrays.asList(elementId));
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_GET, params, HttpMethodEnum.POST,
                    EwpConstants.OMOBILITY_LAS_API_NAME);
    return client.sendRequest(requestIndex, OmobilityLasGetResponse.class, event);
  }

  private ClientResponse invocarLAIndex(final HeiEntry hei, final String modifiedSince)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.OMOBILITY_LA_RECEIVING_HEI_ID, Arrays.asList(globalProperties.getInstance()));
    if (Objects.nonNull(modifiedSince)) {
      params.put(EwpRequestConstants.OMOBILITY_LA_MODIFIED_SINCE, Arrays.asList(modifiedSince));
    }
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_INDEX, params, HttpMethodEnum.POST,
                    EwpConstants.OMOBILITY_LAS_API_NAME);
    return client.sendRequest(requestIndex, OmobilityLasIndexResponse.class, null);
  }

  private ClientResponse invocarIIAGet(final HeiEntry hei, final String elementId, EventMDTO event)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList(elementId));
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.IIAS_GET_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.IIAS_API_NAME);
    return client.sendRequest(requestIndex, IiasGetResponse.class, event);
  }

  private ClientResponse invocarIIAIndex(final HeiEntry hei, final String modifiedSince)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.IIAS_PARTNER_HEI_ID, Arrays.asList(globalProperties.getInstance()));
    if (Objects.nonNull(modifiedSince)) {
      params.put(EwpRequestConstants.IIAS_MODIFIED_SINCE, Arrays.asList(modifiedSince));
    }
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.IIAS_INDEX_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.IIAS_API_NAME);
    return client.sendRequest(requestIndex, IiasIndexResponse.class, null);
  }

  private ClientResponse invocarOmobilitiesIndex(final HeiEntry hei, final String modifiedSince)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.IMOBILITIES_GET_RECEIVING_HEI_ID, Arrays.asList(globalProperties.getInstance()));
    if (Objects.nonNull(modifiedSince)) {
      params.put(EwpRequestConstants.OMOBILITY_LA_MODIFIED_SINCE, Arrays.asList(modifiedSince));
    }
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.OMOBILITIES_INDEX_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.OMOBILITIES_API_NAME);
    return client.sendRequest(requestIndex, OmobilitiesIndexResponse.class, null);
  }

  private ClientResponse invocarOmobilitiesGet(final HeiEntry hei, final String elementId, EventMDTO event)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList(elementId));
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_GET_URL, params, HttpMethodEnum.POST,
                    EwpConstants.OMOBILITIES_API_NAME);
    return client.sendRequest(requestIndex, OmobilitiesGetResponse.class, event);
  }

  private ClientResponse invocarImobilitiesGet(final HeiEntry hei, final String elementId, EventMDTO event)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IMOBILITIES_GET_RECEIVING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList(elementId));
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.IMOBILITIES_GET_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.IMOBILITIES_API_NAME);
    return client.sendRequest(requestIndex, ImobilitiesGetResponse.class, event);
  }

  private ClientResponse invocarTorIndex(final HeiEntry hei, final String modifiedSince)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.TOR_GET_RECEIVING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.TOR_INDEX_SENDING_HEI_ID, Arrays.asList(globalProperties.getInstance()));
    if (Objects.nonNull(modifiedSince)) {
      params.put(EwpRequestConstants.TOR_INDEX_MODIFIED_SINCE, Arrays.asList(modifiedSince));
    }
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.TOR_INDEX_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.IMOBILITY_TORS_API_NAME);
    return client.sendRequest(requestIndex, ImobilityTorsIndexResponse.class, null);
  }

  private ClientResponse invocarTorGet(final HeiEntry hei, final String elementId, EventMDTO event)
          throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.TOR_GET_RECEIVING_HEI_ID, Arrays.asList(hei.getId()));
    params.put(EwpRequestConstants.TOR_GET_OMOBILITY_ID, Arrays.asList(elementId));
    final ClientRequest requestIndex =
            utils.formarPeticion(hei, EwpRequestConstants.TOR_GET_URL_KEY, params, HttpMethodEnum.POST,
                    EwpConstants.IMOBILITY_TORS_API_NAME);
    return client.sendRequest(requestIndex, ImobilityTorsGetResponse.class, event);
  }

}
