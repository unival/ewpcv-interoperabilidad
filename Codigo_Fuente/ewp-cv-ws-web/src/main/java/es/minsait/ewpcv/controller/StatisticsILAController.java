package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IStatisticsILAService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasIncomingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * The StatisticsILA controller.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@RestController
@RequestMapping(value = "/StatisticsIla", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class StatisticsILAController {

        @Autowired
        GlobalProperties globalProperties;
        @Autowired
        private IStatisticsILAService statisticsILAService;
        @Autowired
        private Utils utils;
        @Autowired
        private IEventService eventService;

        @GetMapping()
        public ResponseEntity<LasIncomingStatsResponse> getStatisticsOla(final HttpServletRequest request) {

            log.debug("Inicio invocacion GET StatisticsOla");

            final EventMDTO event = utils.initializeAuditableEvent(null, null, request, EwpEventConstants.STATISTICSILA_GET,
                    NotificationTypes.STATISTICS_ILA_NAMESPACE, null);

            final LasIncomingStatsResponse respuesta = new LasIncomingStatsResponse();

            if (Objects.nonNull(statisticsILAService.getStatisticsRest())) {
                respuesta.getAcademicYearLaStats().addAll(statisticsILAService.getStatisticsRest());
            }

            if (utils.getValue(EwpConstants.STATISTICSILAUDIT)) {
                utils.setMessageToEventOK(event, respuesta);
            }

            eventService.save(event);

            log.debug("Fin invocacion GET StatisticsOla");
            return ResponseEntity.ok(respuesta);
        }



    }
