package es.minsait.ewpcv.client;

import es.minsait.ewpcv.common.BaseClientResponse;

import java.io.Serializable;

/**
 * The type Client response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class ClientResponse extends BaseClientResponse implements Serializable {
  private Object result;
  private String decompressedBody;

  public Object getResult() {
    return result;
  }

  public void setResult(final Object result) {
    this.result = result;
  }

  public String getDecompressedBody() {
    return decompressedBody;
  }

  public void setDecompressedBody(String decompressedBody) {
    this.decompressedBody = decompressedBody;
  }
}
