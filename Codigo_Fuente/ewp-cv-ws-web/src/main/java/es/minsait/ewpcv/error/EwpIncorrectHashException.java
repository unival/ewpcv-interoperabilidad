package es.minsait.ewpcv.error;

/**
 * The type Ewp incorrect hash exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpIncorrectHashException extends Exception {
  private static final long serialVersionUID = 3574904948728010133L;
  int status;

  public EwpIncorrectHashException(final String message, final int status) {
    super(message);
    this.status = status;
  }

  public int getStatus() {
    return status;
  }
}
