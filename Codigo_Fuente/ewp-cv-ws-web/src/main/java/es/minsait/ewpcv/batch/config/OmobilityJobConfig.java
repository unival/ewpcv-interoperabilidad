package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.OmobilityJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Omobility job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class OmobilityJobConfig {

  @Bean
  public JobDetailFactoryBean jobDetailOmobility() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(OmobilityJob.class);
    jobDetailFactory.setDescription("Batch para refresco de omobilities");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerOmobility(@Qualifier("jobDetailOmobility") final JobDetail jobDetail,
      @Value("${ewp.omobilities.refresh}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
