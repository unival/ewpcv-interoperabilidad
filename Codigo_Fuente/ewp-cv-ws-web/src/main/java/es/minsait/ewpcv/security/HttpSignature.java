package es.minsait.ewpcv.security;

import es.minsait.ewpcv.common.EwpKeyStore;
import es.minsait.ewpcv.common.RegistryClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;
import org.tomitribe.auth.signatures.Base64;
import org.tomitribe.auth.signatures.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The type Http signature.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
public class HttpSignature {

  private static final Logger logger = LoggerFactory.getLogger(HttpSignature.class);

  @Autowired
  EwpKeyStore keystoreController;

  @Autowired
  RegistryClient registryClient;

  public boolean clientWantsSignedResponse(final ServerHttpRequest request) {
    // Check if client wants signed response and that the header is correct
    if (request.getHeaders().containsKey("accept-signature")) {
      final List<String> signaturesAccepted = new ArrayList<>();
      request.getHeaders().get("accept-signature").forEach(m -> signaturesAccepted.addAll(Arrays.asList(m.split(","))));
      final boolean anyMatch = signaturesAccepted.stream().anyMatch(m -> "rsa-sha256".equalsIgnoreCase(m));
      logger.debug("!!!!!!!!Client Wants Signed Response: " + anyMatch + " for X-Request-Id: "
          + request.getHeaders().get("X-Request-Id"));
      return anyMatch;
    }
    return false;
  }

  public boolean clientWantsSignedResponse(final HttpServletRequest request) {
    // Check if client wants signed response and that the header is correct
    if (Objects.nonNull(request.getHeader("accept-signature"))) {
      final List<String> signaturesAccepted = Arrays.asList(request.getHeader("accept-signature").split(","));
      final boolean anyMatch = signaturesAccepted.stream().anyMatch(m -> "rsa-sha256".equalsIgnoreCase(m));
      logger.debug("!!!!!!!!Client Wants Signed Error Response: " + anyMatch + " for X-Request-Id: "
          + request.getHeader("X-Request-Id"));
      return anyMatch;
    }
    return false;
  }

  public void signResponse(final HttpServletRequest request, final HttpServletResponse response, final Object body,
      final boolean formatedOutput) {
    try {
      logger.debug("!!!!!!!!Firma Respuesta Error!!!!!!!!");
      final String requestID = request.getHeader("X-Request-Id");
      final String requestAuthorization = request.getHeader("Authorization");
      Signature reqSig = null;
      if (requestAuthorization != null) {
        reqSig = Signature.fromString(requestAuthorization);
      }

      final Date today = new Date();
      final SimpleDateFormat rfc2616DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
      rfc2616DateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      final String stringToday = rfc2616DateFormat.format(today);

      final byte[] bodyBytes = getByteArrayFromObject(body, formatedOutput);
      final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bodyBytes);
      final String digestHeader = "SHA-256=" + new String(Base64.encodeBase64(digest));

      final List<String> headerNames = new ArrayList<>();
      final Map<String, String> headers = new HashMap<>();
      headers.put("Original-Date", stringToday);
      logger.debug("    !!!!Original date: " + stringToday);
      headers.put("Digest", digestHeader);
      logger.debug("    !!!!Digest calculado: " + digestHeader);
      logger.debug("    !!!!a partir de: " + new String(bodyBytes));

      if (requestID != null) {
        headers.put("X-Request-Id", requestID);
        logger.debug("    !!!!X-Request-Id: " + requestID);
      }
      if (reqSig != null) {
        headers.put("X-Request-Signature", reqSig.getSignature());
        logger.debug("    !!!!X-Request-Signature: " + reqSig.getSignature());
      }

      // Update the other header lists as well
      headers.keySet().forEach((header) -> {
        final String headerValue = headers.get(header);
        headerNames.add(header);
        response.addHeader(header, headerValue);
      });

      //if present, add the www-authenticate header
      if (response.getHeader("WWW-Authenticate") != null) {
        headerNames.add("WWW-Authenticate");
        headers.put("WWW-Authenticate", response.getHeader("WWW-Authenticate"));
      }
      //if present, add the want-digest header
      if (response.getHeader("Want-Digest") != null) {
        headerNames.add("Want-Digest");
        headers.put("Want-Digest", response.getHeader("Want-Digest"));
      }

      final Signature signature =
          new Signature(keystoreController.getOwnPublicKeyFingerprint(), "rsa-sha256", null, headerNames);
      final Key key = keystoreController.getOwnPrivateKey();

      final Signer signer = new Signer(key, signature);
      final Signature signed;
      signed = signer.sign("", "", headers);

      response.addHeader("Signature", signed.toString().replace("Signature ", ""));
      logger.debug("    !!!!XSignature recibido: " + signed.toString().replace("Signature ", ""));
    } catch (IOException | NoSuchAlgorithmException e) {
      logger.error("Can't sign response", e);
    }
  }

  public void signResponse(final ServerHttpRequest request, final ServerHttpResponse response, final Object body,
      final boolean formatedOutput) {
    try {
      logger.debug("!!!!!!!!Firma Respuesta!!!!!!!!");
      final HttpHeaders requestHeaders = request.getHeaders();
      final String requestID = requestHeaders.getFirst("X-Request-Id");
      final String requestAuthorization = requestHeaders.getFirst("Authorization");
      Signature reqSig = null;
      if (requestAuthorization != null) {
        reqSig = Signature.fromString(requestAuthorization);
      }

      final Date today = new Date();
      final SimpleDateFormat rfc2616DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
      rfc2616DateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      final String stringToday = rfc2616DateFormat.format(today);

      final byte[] bodyBytes = getByteArrayFromObject(body, formatedOutput);
      final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bodyBytes);
      final String digestHeader = "SHA-256=" + new String(Base64.encodeBase64(digest));

      final List<String> headerNames = new ArrayList<>();
      final Map<String, String> headers = new HashMap<>();

      headers.put("Original-Date", stringToday);
      logger.debug("    !!!!Original date: " + stringToday);
      headers.put("Digest", digestHeader);
      logger.debug("    !!!!Digest calculado: " + digestHeader);
      logger.debug("    !!!!a partir de: " + new String(bodyBytes));

      if (requestID != null) {
        headers.put("X-Request-Id", requestID);
        logger.debug("    !!!!X-Request-Id: " + requestID);
      }
      if (reqSig != null) {
        headers.put("X-Request-Signature", reqSig.getSignature());
        logger.debug("    !!!!X-Request-Signature: " + reqSig.getSignature());
      }

      // Update the other header lists as well
      headers.keySet().forEach((header) -> {
        final String headerValue = headers.get(header);
        headerNames.add(header);
        response.getHeaders().add(header, headerValue);
      });

      final Signature signature =
          new Signature(keystoreController.getOwnPublicKeyFingerprint(), "rsa-sha256", null, headerNames);
      final Key key = keystoreController.getOwnPrivateKey();

      final Signer signer = new Signer(key, signature);
      final Signature signed;
      signed = signer.sign("", "", headers);

      response.getHeaders().add("Signature", signed.toString().replace("Signature ", ""));
      logger.debug("    !!!!XSignature: " + signed.toString().replace("Signature ", ""));
    } catch (IOException | NoSuchAlgorithmException e) {
      logger.error("Can't sign response", e);
    }
  }

  public void signRequest(final String method, final URI uri, final HttpHeaders headers, final String requestId) {
    signRequest(method, uri, headers, "", requestId);
  }

  public void signRequest(final String method, final URI uri, final HttpHeaders requestHeaders, final String formData,
      final String requestId) {
    try {
      logger.debug("!!!!!!!!Firma Peticion!!!!!!!!");
      final Map<String, String> headers = new HashMap<>();

      headers.put("X-Request-Id", requestId);
      logger.debug("    !!!!X-Request-Id recibido: " + requestId);

      final Date today = new Date();
      final SimpleDateFormat rfc2616DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
      rfc2616DateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      final String stringToday = rfc2616DateFormat.format(today);
      headers.put("Original-Date", stringToday);
      logger.debug("    !!!!Original date: " + stringToday);

      final String host = (-1 == uri.getPort()) || (8080 == uri.getPort()) || (443 == uri.getPort()) ? uri.getHost() : uri.getHost() + ":" + uri.getPort();
      headers.put("host", host);
      logger.debug("    !!!!Host: " + host);

      final byte[] bodyBytes = formData.getBytes();
      final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bodyBytes);
      final String digestHeader = "SHA-256=" + new String(Base64.encodeBase64(digest));
      headers.put("Digest", digestHeader);
      logger.debug("    !!!!Digest calculado: " + digestHeader);
      logger.debug("    !!!!a partir de: " + new String(bodyBytes));

      final List<String> headerNames = new ArrayList<>();
      headerNames.add("(request-target)");
      headers.entrySet().forEach((header) -> {
        headerNames.add(header.getKey());
        requestHeaders.add(header.getKey(), header.getValue());
      });

      final Signature signature =
          new Signature(keystoreController.getOwnPublicKeyFingerprint(), "rsa-sha256", null, headerNames);
      final Key key = keystoreController.getOwnPrivateKey();

      final Signer signer = new Signer(key, signature);
      final Signature signed;
      signed = signer.sign(method, getFullRelativePath(uri.getPath(), uri.getQuery()), headers);

      requestHeaders.add("Authorization", signed.toString());
      logger.debug("    !!!!Authorization: " + signed.toString());

      // Want signed response
      requestHeaders.add("Accept-Signature", "rsa-sha256");
    } catch (IOException | NoSuchAlgorithmException e) {
      logger.error("Can't sign response", e);
    }
  }

  public AuthenticateMethodResponse verifyHttpSignatureRequest(final HttpServletRequest request) {

    logger.info("!!!!!!!!Verificacion Firma Peticion!!!!!!!!");
    logger.debug("    !!!!Request URI: " + request.getRequestURI());
    if (StringUtils.isNotEmpty(request.getQueryString())) {
      logger.debug("    !!!!Request Parameters: " + request.getQueryString());
    }
    final List<String> reqHeaders = Collections.list(request.getHeaderNames());
    final UriComponents uriComponents = ServletUriComponentsBuilder.fromContextPath(request).build();

    final String authorization = request.getHeader("authorization");

    if (authorization == null || !authorization.toLowerCase().startsWith("signature")) {
      return AuthenticateMethodResponse.builder().withRequiredMethodInfoFulfilled(false)
          .withResponseCode(HttpStatus.SC_UNAUTHORIZED).build();
    }
    final Signature signature = Signature.fromString(authorization);
    logger.debug("    !!!!Signature: " + signature);
    if (signature.getAlgorithm() != Algorithm.RSA_SHA256) {
      return AuthenticateMethodResponse.builder().withRequiredMethodInfoFulfilled(false)
          .withErrorMessage("Only signature algorithm rsa-sha256 is supported.")
          .withResponseCode(HttpStatus.SC_UNAUTHORIZED).build();
    }

    final Map<String, String> headers = new HashMap<>();
    reqHeaders.forEach((entry) -> {
      headers.put(entry.toLowerCase(), String.join(", ", request.getHeader(entry)));
    });
    final Optional<AuthenticateMethodResponse> authenticateMethodResponse = checkRequiredSignedHeaders(signature,
            "(request-target)", "host", "date|original-date", "digest", "x-request-id");
    if (authenticateMethodResponse.isPresent()) {
      return authenticateMethodResponse.get();
    }

    logger.debug("    !!!!Host: " + headers.get("host"));
    if (headers.containsKey("host") && !headers.get("host").equals(uriComponents.getHost()) &&
            !headers.get("host").equals(uriComponents.getHost() + ":" + uriComponents.getPort())) {
      return AuthenticateMethodResponse.builder().withErrorMessage("Host does not match")
              .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    }

    logger.debug("    !!!!X-Request-Id: " + headers.get("x-request-id"));
    if (headers.containsKey("x-request-id")
            && !headers.get("x-request-id").matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")) {
      return AuthenticateMethodResponse.builder().withErrorMessage("Authentication with non-canonical X-Request-ID")
              .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    }

    logger.debug("    !!!!Date: " + headers.get("date"));
    logger.debug("    !!!!Original-Date: " + headers.get("original-date"));
    if ((headers.containsKey("date") && !isDateWithinTimeThreshold(headers.get("date")))
        || (headers.containsKey("original-date") && !isDateWithinTimeThreshold(headers.get("original-date")))) {
      return AuthenticateMethodResponse.builder().withErrorMessage(
          "The date cannot be parsed or the date does not match your server clock within a certain threshold of timeDate.")
          .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    }

    try {
      logger.debug("    !!!!Digest: " + headers.get("digest"));
      if (headers.containsKey("digest")) {
        //si el digest emplea el algoritmo SHA debemos enviar una badRequest indicando que el algoritmo esta deprecated
        if (headers.get("digest").startsWith("SHA=")) {
          return AuthenticateMethodResponse.builder().withErrorMessage("Digest algorithm SHA is deprecated")
                  .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
        }

        // registra en log el mensaje de digest calculado a partir de la cadena
        final List<String> digestsCalculated = DigestsUtils.getAllDigests(request);

        String requestDigest = null;
        if (reqHeaders.contains("digest")) {
          requestDigest = Arrays.stream(request.getHeader("digest").split(","))
              .filter(d -> "SHA-256=".equalsIgnoreCase(d.substring(0, 8))).findFirst().orElse(null);
        }
        if (Objects.nonNull(requestDigest) && !digestsCalculated.contains(requestDigest.substring(8))) {
          return AuthenticateMethodResponse.builder()
              .withErrorMessage(
                  "Digest mismatch! calculated: " + digestsCalculated + ", header: " + request.getHeader("digest"))
              .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
        }
      }

      final String fingerprint = signature.getKeyId();
      final RSAPublicKey publicKey = registryClient.findClientRsaPublicKey(fingerprint);
      logger.debug("    !!!!SignatureKey de peticion: " + fingerprint);
      logger.debug("    !!!!PublicKey de registry: " + publicKey);
      if (publicKey == null) {
        return AuthenticateMethodResponse.builder().withErrorMessage("Key not found for fingerprint: " + fingerprint)
            .withResponseCode(HttpStatus.SC_FORBIDDEN).build();
      }

      final Verifier verifier = new Verifier(publicKey, signature);

      final boolean verifies = verifier.verify(request.getMethod().toLowerCase(), getFullRelativePath(request.getRequestURI(), request.getQueryString()), headers);

      if (!verifies) {
        logger.warn("Signature verification: " + verifies);
        return AuthenticateMethodResponse.builder().withErrorMessage("Signature verification: " + verifies)
            .withResponseCode(HttpStatus.SC_UNAUTHORIZED).build();
      } else {
        logger.debug("    !!!!Signature verification: " + verifies);
      }

      request.setAttribute("EwpRequestRSAPublicKey", publicKey);
    } catch (final NoSuchAlgorithmException e) {
      logger.warn("No such algorithm", e);
      return AuthenticateMethodResponse.builder().withErrorMessage("No such algorithm")
          .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    } catch (final IOException e) {
      logger.warn("Error reading", e);
      return AuthenticateMethodResponse.builder().withErrorMessage(e.getMessage())
          .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    } catch (final SignatureException e) {
      logger.warn("Signature error", e);
      return AuthenticateMethodResponse.builder().withErrorMessage(e.getMessage())
          .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    } catch (final MissingRequiredHeaderException e) {
      logger.warn("Signature error, missing header", e);
      return AuthenticateMethodResponse.builder().withErrorMessage(e.getMessage())
              .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
    }

    return AuthenticateMethodResponse.builder().build();
  }

  public String verifyHttpSignatureResponse(final String method, final URI requestUri,
      final HttpHeaders responseHeaders, final byte[] raw, final String requestID) {
    logger.debug("!!!!!!!!Validacion Firma Respuesta!!!!!!!!");
    logger.debug("    !!!!X-Request-Id: " + responseHeaders.getFirst("X-Request-Id"));
    if (!requestID.equals(responseHeaders.getFirst("X-Request-Id"))) {
      return "Header X-Request-Id does not match the id sent in the request";
    }

    if (!responseHeaders.containsKey("signature")) {
      return "Missing Signature header in response";
    }
    final String signatureHeader = (String) responseHeaders.getFirst("signature");
    logger.debug("    !!!!Signature: " + responseHeaders.getFirst("signature"));

    final Signature signature = Signature.fromString(signatureHeader);
    if (signature.getAlgorithm() != Algorithm.RSA_SHA256) {
      return "Only signature algorithm rsa-sha256 is supported.";
    }

    final Optional<AuthenticateMethodResponse> authenticateMethodResponse =
        checkRequiredSignedHeaders(signature, "date|original-date", "digest", "x-request-id", "x-request-signature");
    if (authenticateMethodResponse.isPresent()) {
      return authenticateMethodResponse.get().errorMessage();
    }

    final Map<String, String> headers = new HashMap<>();
    responseHeaders.keySet().forEach((hkey) -> {
      headers.put(hkey.toLowerCase(), (String) responseHeaders.getFirst(hkey));
    });

    logger.debug("    !!!!Date: " + headers.get("date"));
    logger.debug("    !!!!Original-Date: " + headers.get("original-date"));
    if ((headers.containsKey("date") && !isDateWithinTimeThreshold(headers.get("date")))
        || (headers.containsKey("original-date") && !isDateWithinTimeThreshold(headers.get("original-date")))) {
      return "The date cannot be parsed or the date does not match your server clock within a certain threshold of timeDate.";
    }

    try {
      logger.debug("    !!!!Digest: " + headers.get("digest"));
      if (headers.containsKey("digest")) {
        //si el digest emplea el algoritmo SHA debemos enviar una badRequest indicando que el algoritmo esta deprecated
        if (responseHeaders.get("digest").stream().anyMatch(d -> d.startsWith("SHA="))) {
          return "Digest algorithm SHA is deprecated";
        }

        final byte[] digest;
        try {
          digest = MessageDigest.getInstance("SHA-256").digest(raw);
        } catch (final NoSuchAlgorithmException e) {
          logger.warn("No such algorithm", e);
          return "No such algorithm";
        }
        final String digestCalculated = "SHA-256=" + new String(Base64.encodeBase64(digest));
        logger.debug("    !!!!Digest calculado: " + digestCalculated);
        logger.debug("    !!!!a partir de: " + new String(raw));
        String responseDigest = null;
        if (responseHeaders.containsKey("digest")) {
          responseDigest = responseHeaders.get("digest").stream()
              .filter(d -> "SHA-256=".equalsIgnoreCase(d.substring(0, 8))).findFirst().orElse(null);
        }
        if (Objects.nonNull(responseDigest) && !digestCalculated.substring(8).equals(responseDigest.substring(8))) {
          return "Digest mismatch! calculated (body length: " + raw.length + "): " + digestCalculated
              + ", header: " + responseHeaders.getFirst("digest");
        }
      }

      final String fingerprint = signature.getKeyId();
      final RSAPublicKey publicKey = registryClient.findRsaPublicKey(fingerprint);
      logger.debug("    !!!!SignatureKey de peticion: " + fingerprint);
      logger.debug("    !!!!PublicKey de registry: " + publicKey);
      if (publicKey == null) {
        return "Key not found for fingerprint: " + fingerprint;
      }

      final Verifier verifier = new Verifier(publicKey, signature);

      final boolean verifies = verifier.verify(method.toLowerCase(), getFullRelativePath(requestUri.getPath(), requestUri.getQuery()), headers);

      if (!verifies) {
        logger.warn("Signature verification: " + verifies);
        return "Signature verification: " + verifies;
      } else {
        logger.debug("    !!!!Signature verification: " + verifies);
      }
    } catch (final NoSuchAlgorithmException e) {
      logger.warn("No such algorithm", e);
      return "No such algorithm: " + e.getMessage();
    } catch (final IOException e) {
      logger.warn("Error reading", e);
      return e.getMessage();
    } catch (final SignatureException e) {
      logger.warn("Signature error", e);
      return e.getMessage();
    } catch (final MissingRequiredHeaderException e) {
      logger.warn("Signature error, missing header", e);
      return e.getMessage();
    }
    return null;
  }

  private Optional<AuthenticateMethodResponse> checkRequiredSignedHeaders(final Signature signature,
      final String... headers) {
    return Arrays.stream(headers).map(header -> {
      if (Arrays.stream(header.split("\\|")).noneMatch(h -> signature.getHeaders().contains(h))) {
        return AuthenticateMethodResponse.builder().withRequiredMethodInfoFulfilled(false)
            .withErrorMessage("Missing required signed header '" + header + "'")
            .withResponseCode(HttpStatus.SC_BAD_REQUEST).build();
      } else {
        return null;
      }
    }).filter(Objects::nonNull).findFirst();
  }


  private byte[] getByteArrayFromObject(final Object body, final boolean formatedOutput) throws IOException {
    try {
      // al firmar la respuesta de file api, el body es un array de bytes
      if (body instanceof byte[]) {
        return (byte[]) body;
      }
      final byte[] bodyBytes;
      final JAXBContext jaxbContext = JAXBContext.newInstance(body.getClass());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatedOutput);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        marshaller.marshal(body, baos);
        bodyBytes = baos.toByteArray();
      }
      return bodyBytes;
    } catch (final PropertyException ex) {
      logger.error("Property error", ex);
    } catch (final JAXBException ex) {
      logger.error("Jaxb error", ex);
    }
    return new byte[0];
  }

  private boolean isDateWithinTimeThreshold(final String dateString) {
    final Date today = new Date();
    try {
      final SimpleDateFormat rfc2616DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
      rfc2616DateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      final Date requestDate = rfc2616DateFormat.parse(dateString);
      // Check that time diff is less than five minutes
      return Math.abs(today.getTime() - requestDate.getTime()) <= 5 * 60 * 1000;
    } catch (final ParseException e) {
      logger.warn("Can't parse date: " + dateString, e);
    }
    return false;
  }

  /*
   * Return the full relative path of a URL. In particular, for the "(request-target)" header value.
   * See: https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#section-2.3
   * See: https://datatracker.ietf.org/doc/html/rfc7540#section-8.1.2.3
   * See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages#http_requests
   */
  private String getFullRelativePath(String path, String queryParams) {
    return (path == null ? "" : path) + (queryParams == null ? "" : "?" + queryParams);
  }
}
