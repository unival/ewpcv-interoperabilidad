package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.InstitutionsJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Institutions job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class InstitutionsJobConfig {

  @Bean
  public JobDetailFactoryBean jobDetailInstitutions() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(InstitutionsJob.class);
    jobDetailFactory.setDescription("Batch para refresco de intitutions");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerInstitutions(@Qualifier("jobDetailInstitutions") final JobDetail jobDetail,
      @Value("${ewp.institutions.refresh}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
