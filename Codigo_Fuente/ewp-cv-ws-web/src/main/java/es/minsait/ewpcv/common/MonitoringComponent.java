package es.minsait.ewpcv.common;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.RestClient;
import eu.erasmuswithoutpaper.api.architecture.Empty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

/**
 * Componente encargado de invocar al api de monitorizacion de acuerdo a la especificacion de EWP
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
public class MonitoringComponent {

  @Autowired
  private RestClient client;
  @Autowired
  private RegistryClient registry;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private Utils utils;


    /**
     * Comprueba que se cumplen los requisitos para realizar la invocacion e invoca al api de monitorizacion
     *
     * @param monitoringParams parametros necesarios para invocar al api de monitorizacion
     */
  public void invokeMonitoring(Map<String, List<String>> monitoringParams) {
    log.info("Invocando al api de monitorizacion");
    if (!shouldInvoke(monitoringParams)) return;

    final HeiEntry hei = registry.getMonitoringHeiUrls(globalProperties.getMonitoringSchac().toLowerCase());
    final ClientRequest request;
    try {
      request = utils.formarPeticion(hei, EwpRequestConstants.MONITORING_URL_KEY, monitoringParams, HttpMethodEnum.POST,
              EwpConstants.MONITORING_API_NAME);
      client.sendRequest(request, Empty.class, null);
    } catch (Exception e) {
      log.error("Se ha producido un error invocando al api de monitorizacion", e);
    }
  }

  /**
   * Crea los parametros necesarios para invocar al api de monitorizacion
   *
   * @param serverHeiId identifica la institucion que ha provocado el fallo y por tanto la llamada al api de monitorizacion
   * @param apiName nombre del api que ha provocado el fallo
   * @param errorType tipo de error que ha provocado el fallo, se empleaba en la v1 del api de monitorizacion pero lo mantenemos para la parametrizacion de que notificamos y que no
   * @param apiUrl url del api que se ha invocado y ha provocado el fallo
   * @param httpCode codigo http de la respuesta recibida que ha desencadenado la llamada al api de monitorizacion
   * @param serverMessage mensaje de error recibido en la respuesta que ha desencadenado la llamada al api de monitorizacion
   * @param clientMessage mensaje de error que hemos creado nosotros para notificar al api de monitorizacion, se emplea cuando no obtenemos respuesta o es valida pero no cumple algun criterio de negocio
   * @return
   */
  public Map<String, List<String>> createMonitoringRequestParams(String serverHeiId, String apiName, ErrorTypeEnum errorType, String apiUrl, String httpCode, String serverMessage, String clientMessage) {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.MONITORING_SERVER_HEI_ID, Arrays.asList(serverHeiId));
    params.put(EwpRequestConstants.MONITORING_API_NAME, Arrays.asList(apiName));
    params.put(EwpRequestConstants.MONITORING_ERROR_TYPE, Arrays.asList(errorType.name()));
    params.put(EwpRequestConstants.MONITORING_API_URL, Arrays.asList(apiUrl));
    params.put(EwpRequestConstants.MONITORING_HTTP_CODE, Arrays.asList(httpCode));
    params.put(EwpRequestConstants.MONITORING_SERVER_MESSAGE, Arrays.asList(serverMessage));
    params.put(EwpRequestConstants.MONITORING_CLIENT_MESSAGE, Arrays.asList(clientMessage));
    return params;
  }

  /**
   * Prepara los parametros necesarios para invocar al api de monitorizacion esta modalidad nutre el mapa de parametros como paso por referencia
   *
   * @param monitoringParams Mapa de parametros a nutrir
   * @param clientRequest Datos de la peticion de donde se extraen datos necesarios para el api de monitorizacion
   * @param errorType tipo de error que ha provocado el fallo, se empleaba en la v1 del api de monitorizacion pero lo mantenemos para la parametrizacion de que notificamos y que no
   * @param responseCode codigo http de la respuesta recibida que ha desencadenado la llamada al api de monitorizacion
   * @param serverMessage mensaje de error recibido en la respuesta que ha desencadenado la llamada al api de monitorizacion
   * @param clientMessage mensaje de error que hemos creado nosotros para notificar al api de monitorizacion, se emplea cuando no obtenemos respuesta o es valida pero no cumple algun criterio de negocio
   */
  public void prepareMonitoringParams(Map<String, List<String>> monitoringParams, ClientRequest clientRequest,
                                      ErrorTypeEnum errorType, Integer responseCode, String serverMessage, String clientMessage,  boolean isSslException) {
    monitoringParams.put("isSslException", Arrays.asList(String.valueOf(isSslException)));
    monitoringParams.put(EwpRequestConstants.MONITORING_SERVER_HEI_ID, Arrays.asList(clientRequest.getHeiId()));
    monitoringParams.put(EwpRequestConstants.MONITORING_API_NAME, Arrays.asList(clientRequest.getApiName()));
    monitoringParams.put(EwpRequestConstants.MONITORING_ERROR_TYPE, Arrays.asList(errorType.name()));
    monitoringParams.put(EwpRequestConstants.MONITORING_API_URL, Arrays.asList(clientRequest.getUrl()));
    if (Objects.nonNull(responseCode)) {
      monitoringParams.put(EwpRequestConstants.MONITORING_HTTP_CODE, Arrays.asList(String.valueOf(responseCode)));
    }
    monitoringParams.put(EwpRequestConstants.MONITORING_SERVER_MESSAGE, Arrays.asList(serverMessage));
    monitoringParams.put(EwpRequestConstants.MONITORING_CLIENT_MESSAGE, Arrays.asList(clientMessage));

  }

  /**
   * Comprueba si se debe invocar al api de monitorizacion
   * las condiciones para invocar al api de monitorizacion son:
   * - que se hayan recibido parametros
   * - obligatoriedad de prametros, serverHeiId, apiName,
   * - que api que ha originado el error no sea monitorizacion, registry, discovery o echo
   * - que la paarametrizacion de la aplicacion indique que se debe invocar al api de monitorizacion para el tipo de error recibido
   *
   * @param monitoringParams parametros a analizar para decidir si se invoca o no al api de monitorizacion
   * @return true si se debe invocar al api de monitorizacion, false en caso contrario
   */
  private boolean shouldInvoke(Map<String, List<String>> monitoringParams) {
    //- que se hayan recibido parametros
    if (monitoringParams.isEmpty()) {
      log.info("No se invoca al api de monitorizacion porque no se han recibido parametros");
      return false;
    }

    //- obligatoriedad de prametros, serverHeiId, apiName,
    if (CollectionUtils.isEmpty(monitoringParams.get(EwpRequestConstants.MONITORING_SERVER_HEI_ID))) {
      log.info("No se invoca al api de monitorizacion porque no se ha recibido el heiId del servidor");
      return false;
    }
    String apiName = monitoringParams.get(EwpRequestConstants.MONITORING_API_NAME).stream().findFirst().orElse(null);
    if (Objects.isNull(apiName)) {
      log.info("No se invoca al api de monitorizacion porque no se ha recibido el nombre del api que ha originado el error");
      return false;
    }

    //- que api que ha originado el error no sea monitorizacion, registry, discovery o echo
    switch (apiName) {
      case EwpConstants.MONITORING_API_NAME:
        log.info("No se invoca al api de monitorizacion para errores producidos por el api de monitorizacion");
        return false;
      case EwpConstants.REGISTRY_API_NAME:
        log.info("No se invoca al api de monitorizacion para errores producidos por el api de registro");
        return false;
      case EwpConstants.DISCOVERY_API_NAME:
        log.info("No se invoca al api de monitorizacion para errores producidos por el api de discovery");
        return false;
      case EwpConstants.ECHO_API_NAME:
        log.info("No se invoca al api de monitorizacion para errores producidos por el api de echo");
        return false;
      default:
        break;
    }

    boolean isSslException = Boolean.parseBoolean(Optional.ofNullable(monitoringParams.get("isSslException")).orElse(Collections.singletonList("false")).stream().findFirst().orElse("false"));
    if (isSslException) {
      log.info("No se invoca al api de monitorizacion para excepciones SSL");
      return false;
    }
    //- que la paarametrizacion de la aplicacion indique que se debe invocar al api de monitorizacion para el tipo de error recibido
    if(!Strings.isNullOrEmpty(globalProperties.getMonitoringAvoidNotifyErrorTypes())){
      String errorType = monitoringParams.get(EwpRequestConstants.MONITORING_ERROR_TYPE).stream().findFirst().orElse(null);
      if(globalProperties.getMonitoringAvoidNotifyErrorTypes().contains(errorType)){
        log.info("No se invoca al api de monitorizacion porque la parametrizacion de la aplicacion indica que no se debe invocar para el tipo de error recibido: {} ", errorType);
        return false;
      }
    }

    return true;
  }

}
