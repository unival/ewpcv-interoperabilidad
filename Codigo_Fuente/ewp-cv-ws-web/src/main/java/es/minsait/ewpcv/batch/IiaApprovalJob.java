package es.minsait.ewpcv.batch;

import java.util.*;

import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpResponseVerificationException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.iia.IiaApprovalMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;
import lombok.extern.slf4j.Slf4j;
/**
 * The type Iia approval job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class IiaApprovalJob implements Job {

  @Value("${ewp.iia.cnr.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.iia.cnr.notification.block:50}")
  private int blockSize;

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private IEventService eventService;

  @Autowired
  private IIiasService iiaService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private Utils utils;

  @Autowired
  private GlobalProperties globalProperties;

  @Value("${ewp.target.uri:}")
  private String targetUri;
  @Value("${batch.cnr.approval:true}")
  private boolean cnrApproval;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrApproval) {
      log.info("***No se ejecuta el proceso de actualizacion de IIA Approval a partir de CNR por configuracion");
      return;
    }

    log.info("***Comienza proceso de actualizacion de IIA Approval a partir de CNR");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.IIA_APPROVAL);
    log.info("Se van a actualizar " + list.size() + " IIA-Approval");
    for (final NotificationMDTO notification : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IIA_APPROVAL, notification.getOwnerHeiId(),
          notification.getHeiId(), notification.getChangedElementIds(), EwpEventConstants.APPROVE);

      final String iiaId = notification.getChangedElementIds();
      final IiaMDTO iia = iiaService.findByInteropId(iiaId);

      try {
        // llamamos a iia get para tomar la snapshot posteriormente, si falla cortamos flujo y no se invoca al approval para evitar aprobaciones sin snapshot
        byte[] iiaRaw = invocarIiaGet(notification.getHeiId(), notification.getOwnerHeiId(), iia.getRemoteIiaId());
        // llamamos a iia approval
        final ClientResponse response = invocarIiaApproval(notification, event);
        // si la respuesta es un error o no se puede verificar vamos con la siguiente
        if (!utils.verifyResponse(notification, event, response, EwpEventConstants.APPROVE)) {
          continue;
        }
        final IiasApprovalResponse responseApproval = (IiasApprovalResponse) response.getResult();
        // si la respuesta es correcta y no esta vacia actualizamos la iia
        if (Objects.nonNull(responseApproval) && CollectionUtils.isNotEmpty(responseApproval.getApproval())) {
          iiaService.updateIiaApprovalByInteropId(iia.getInteropId(), responseApproval.getApproval().get(0).getIiaHash(), new Date());
          try {
            iiaService.takeSnapshot(iia.getRemoteIiaId(), event, EwpConstants.IIAS_CLIENT_VERSION, globalProperties.getInstance(), iiaRaw);
          }catch (Exception e){
            log.error("Error al tomar snapshot de IIA remoto: " + iia.getRemoteIiaId(), e);
          }
        } else {
          event.setObservations("respuesta vacia");
//          No me queda claro que tengamos que eliminar la aprobacion en este caso, de momento solo ignoramos más info en la issue:
//          https://github.com/erasmus-without-paper/ewp-specs-api-iias/issues/160
//          iiaService.updateIiaApprovalById(iiaId, null, new Date());
        }
        notificationService.eliminarNotificacion(notification);
      } catch (final Exception e) {
        final String error = Objects.nonNull(notification)
            ? "Error al procesar IIA Approval CNR, id iia: " + notification.getChangedElementIds()
            : "Error al procesar IIA Approval CNR";
        log.error(error, getClass().getName(), e);
        notificationService.liberarNotificaciones(notification, e.getMessage());
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }
      eventService.save(event);
    }
  }


  private ClientResponse invocarIiaApproval(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_APPROVAL_APPROVING_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IIAS_APPROVAL_OWNER_HEI_ID, Arrays.asList(notificacion.getOwnerHeiId()));
    params.put(EwpRequestConstants.IIAS_APPROVAL_IIA_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getIiaApprovalHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.IIAS_APPROVAL_API_NAME);
    return client.sendRequest(requestIndex, IiasApprovalResponse.class, event);
  }

  private byte[] invocarIiaGet(final String heiId, final String ownerHeiId, final String iiaId) throws EwpOperationNotFoundException, EwpResponseVerificationException {
    EventMDTO event = null;
    try {
      event = utils.initializeJobEvent(NotificationTypes.IIA, heiId, ownerHeiId, iiaId, EwpEventConstants.IIAGET_POST);
      final Map<String, List<String>> params = new HashMap<>();
      params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(heiId));
      params.put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList(iiaId));
      final HeiEntry hei = registry.getIiaHeiUrls(heiId.toLowerCase());
      final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.IIAS_GET_URL_KEY, params, HttpMethodEnum.POST, EwpConstants.IIAS_API_NAME);
      ClientResponse response = client.sendRequest(request, IiasGetResponse.class, event);
      if (!utils.verifyResponse(null, event, response, EwpEventConstants.IIAGET_POST)) {
        throw new EwpResponseVerificationException("Error en la respuesta del IIA GET");
      }
      event.setObservations("IIA GET realizado para snapshot");
      eventService.save(event);
      return response.getRawResponse().getBytes();
    } catch (EwpResponseVerificationException er) {
      //la verificacion ya persiste el evento aqui no hace falta
      log.error("Error verificando la respuesta del IIA GET para el IIA ID remoto: " + iiaId + "error " + event.getObservations());
      throw er;
    } catch (Exception e) {
      log.error("Error realizando IIA GET IIA ID remoto: " + iiaId, e);
      if (event != null) {
        event.setStatus(EwpEventConstants.KO);
        event.setObservations("Error realizando IIA GET para el IIA ID remoto: " + iiaId + ": " + e.getMessage());
        eventService.save(event);
      }
      throw e;
    }
  }


}
