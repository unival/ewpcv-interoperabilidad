package es.minsait.ewpcv.controller;

import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import eu.erasmuswithoutpaper.api.architecture.Empty;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Echo controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/refresh")
@Slf4j
@Validated
public class RefreshController {

  @Resource(name = "jobDetailRefreshJob")
  JobDetail jobDetail;
  @Autowired
  SchedulerFactoryBean schedulerFactoryBean;

  @PostMapping()
  public ResponseEntity<Empty> triggerRefreshJob() {

    try {
      schedulerFactoryBean.getScheduler().triggerJob(jobDetail.getKey());
    } catch (Exception e) {
      log.error("Error al lanzar el job", e);
      return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return ResponseEntity.ok().build();
  }

}
