package es.minsait.ewpcv.batch;

import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;

import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpIncorrectHashException;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.architecture.Empty;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import lombok.extern.slf4j.Slf4j;


/**
 * The type Notificaciones job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class NotificacionesJob implements Job {

  public static final String SENDING_HEI_ID = "sending_hei_id";
  public static final String OMOBILITY_ID = "omobility_id";
  public static final String TEXTO_LOG = "Se van a notificar ";
  public static final String MAP_URL_KEY = "url";
  private static final String COMIENZA_PROCESO_DE_NOTIFICACION_DE = "***Comienza proceso de Notificacion de ";
  private static final String NO_SE_EJECUTA_POR_CONFIGURACION =
          "***Por configuracion no se ejecuta el proceso de Notificacion de ";


  @Autowired
  private INotificationService notificationService;

  @Autowired
  private IIiasService iiasService;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private RestClient client;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private Utils utils;

  @Value("${batch.notificacion.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.iia.cnr.notification.block:100}")
  private int blockSize;

  @Value("${batch.notification.notificar.iias:false}")
  private boolean notificarIias;

  @Value("${batch.notification.notificar.approval:false}")
  private boolean notificarIiasApproval;

  @Value("${batch.notification.notificar.incomingmobility:false}")
  private boolean notificarIncomingMobility;

  @Value("${batch.notification.notificar.incomingmobilitytor:false}")
  private boolean notificarIncomingMobilityTOR;

  @Value("${batch.notification.notificar.outgoingmobility:false}")
  private boolean notificarOutgoingMobility;

  @Value("${batch.notification.notificar.outgoingmobilityla:false}")
  private boolean notificarOutgoingMobilityLA;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    log.info("Inicio de tarea de carga notificaciones pendientes ");

    if (notificarIiasApproval) {
      notificarIiasApproval();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "IIA-APPROVAL");
    }

    if (notificarIias) {
      notificarIias();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "IIA");
    }

    if (notificarIncomingMobility) {
      notificarIncomingMobility();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "INCOMING-MOBILITY");
    }

    if (notificarIncomingMobilityTOR) {
      notificarIncomingMobilityTOR();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "INCOMING-MOBILITY-TOR");
    }

    if (notificarOutgoingMobility) {
      notificarOutgoingMobility();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "OUTGOING-MOBILITY");
    }

    if (notificarOutgoingMobilityLA) {
      notificarOutgoingMobilityLA();
    } else {
      log.info(NO_SE_EJECUTA_POR_CONFIGURACION + "MOBILITY-LA");
    }
  }

  private void notificarIias() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "IIA");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.IIA, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " IIAS");

    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> heiIds = new ArrayList<>();
        final List<String> iiaIds = new ArrayList<>();
        heiIds.add(notificacion.getOwnerHeiId());
        iiaIds.add(notificacion.getChangedElementIds());
        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.IIA);
        try {
          final Map<String, List<String>> params = new HashMap<>();
          params.put(EwpRequestConstants.IIAS_CNR_IIA_ID, iiaIds);
          final HeiEntry hei = getUrls(EwpConstants.IIA_CNR_API_NAME, notificacion.getHeiId().toLowerCase());

          final ClientRequest request =
              utils.formarPeticion(hei, EwpRequestConstants.IIAS_CNR_URL_KEY, params, HttpMethodEnum.POST,
                  EwpConstants.IIA_CNR_API_NAME);
          procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar IIA-CNR, id IIA: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion IIA-CNR";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          notificacion.setLastError(e.getMessage());
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private void notificarIncomingMobility() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "INCOMING-MOBILITY");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.IMOBILITY, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " INCOMING-MOBILITY");


    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> heiIds = new ArrayList<>();
        final List<String> omobilitiesIds = new ArrayList<>();
        heiIds.add(notificacion.getOwnerHeiId());
        omobilitiesIds.add(notificacion.getChangedElementIds());

        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.IMOBILITY);
        try {
          // llamamos al cnr
          final Map<String, List<String>> params = new HashMap<>();
          params.put(EwpRequestConstants.IMOBILITIES_CNR_RECEIVING_HEI_ID, heiIds);
          params.put(EwpRequestConstants.IMOBILITIES_CNR_OMOBILITY_ID, omobilitiesIds);
          final HeiEntry hei =
              getUrls(EwpConstants.IMOBILITY_CNR_API_NAME, notificacion.getHeiId().toLowerCase());
          final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.INCOMING_MOBILITY_CNR_URL_KEY, params,
              HttpMethodEnum.POST, EwpConstants.IMOBILITY_CNR_API_NAME);
          procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar INCOMING-MOBILITY, id Mobility: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion INCOMING-MOBILITY";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private void notificarIncomingMobilityTOR() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "INCOMING-MOBILITY-TOR");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.IMOBILITY_TOR, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " INCOMING-MOBILITY-TOR");


    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> heiIds = new ArrayList<>();
        final List<String> omobilitiesIds = new ArrayList<>();
        heiIds.add(notificacion.getOwnerHeiId());
        omobilitiesIds.add(notificacion.getChangedElementIds());
        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.IMOBILITY_TOR);
        try {
          // llamamos al cnr
          final Map<String, List<String>> params = new HashMap<>();
          params.put("receiving_hei_id", heiIds); // nuestro id
          params.put(OMOBILITY_ID, omobilitiesIds);
          final HeiEntry hei =
              getUrls(EwpConstants.IMOBILITY_TOR_CNR_API_NAME, notificacion.getHeiId().toLowerCase());
          final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.INCOMING_MOBILITY_TOR_CNR_URL_KEY, params,
              HttpMethodEnum.POST, EwpConstants.IMOBILITY_TOR_CNR_API_NAME);
          procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar INCOMING-MOBILITY-TOR, id Mobility: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion INCOMING-MOBILITY-TOR";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private void notificarOutgoingMobility() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "OUTGOING-MOBILITY");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.OMOBILITY, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " OUTGOING-MOBILITY");


    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> heiIds = new ArrayList<>();
        final List<String> omobilitiesIds = new ArrayList<>();
        heiIds.add(notificacion.getOwnerHeiId());
        omobilitiesIds.add(notificacion.getChangedElementIds());
        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.OMOBILITY);
        try {
          // llamamos al cnr
          final Map<String, List<String>> params = new HashMap<>();
          params.put(SENDING_HEI_ID, heiIds);
          params.put(OMOBILITY_ID, omobilitiesIds);
          final HeiEntry hei =
              getUrls(EwpConstants.OMOBILITY_CNR_API_NAME, notificacion.getHeiId().toLowerCase());
          final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_CNR_URL_KEY, params,
              HttpMethodEnum.POST, EwpConstants.OMOBILITY_CNR_API_NAME);
          procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar OUTGOING-MOBILITY, id Mobility: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion OUTGOING-MOBILITY";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private void notificarOutgoingMobilityLA() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "MOBILITY-LA");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.LA, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " MOBILITY-LA");

    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> heiIds = new ArrayList<>();
        final List<String> omobilitiesIds = new ArrayList<>();
        heiIds.add(notificacion.getOwnerHeiId());
        omobilitiesIds.add(notificacion.getChangedElementIds());
        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.LA);
        try {
          // llamamos al cnr
          final Map<String, List<String>> params = new HashMap<>();
          params.put(SENDING_HEI_ID, heiIds);
          params.put(OMOBILITY_ID, omobilitiesIds);
          final HeiEntry hei =
              getUrls(EwpConstants.OMOBILITY_LA_CNR_API_NAME, notificacion.getHeiId().toLowerCase());
          final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_CNR_URL_KEY, params,
              HttpMethodEnum.POST, EwpConstants.OMOBILITY_LA_CNR_API_NAME);
          procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar LA, id Mobility: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion LA";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private void notificarIiasApproval() {
    log.info(COMIENZA_PROCESO_DE_NOTIFICACION_DE + "IIA-APPROVAL");

    // obtenemos la lista de instituciones a excluir de la validacion
    final boolean noValidar = StringUtils.isBlank(globalProperties.getHashValidationHeis());
    final boolean validarTodas = EwpConstants.ALL_HASH_VALIDATION.equals(globalProperties.getHashValidationHeis());
    List<String> validationExcludedHeis = null;
    if (!noValidar && !validarTodas) {
      validationExcludedHeis = Arrays.asList(globalProperties.getHashValidationHeis().replace(" ", "").split(","));
    }

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesEnviar(NotificationTypes.IIA_APPROVAL, retriesDelay, blockSize);
    log.info(TEXTO_LOG + list.size() + " IIA-APPROVAL");

    for (final NotificationMDTO notificacion : list) {
      if (globalProperties.getInstance().equalsIgnoreCase(notificacion.getHeiId())) {
        //No notificamos a nosotros mismos para evitar problemas de tratamiento
        continue;
      }
      try {
        final List<String> approving = new ArrayList<>();
        final List<String> notifier = new ArrayList<>();
        final List<String> iiaIds = new ArrayList<>();
        approving.add(notificacion.getOwnerHeiId());
        iiaIds.add(notificacion.getChangedElementIds());
        notifier.add(notificacion.getHeiId());
        final EventMDTO event = generaEventMDTO(notificacion, NotificationTypes.IIA_APPROVAL);
        try {
          final IiasGetResponse iiaGetResponse = (IiasGetResponse) invocarIiaGet(notificacion, event).getResult();
          utils.setMessageToEventOK(event, iiaGetResponse);
          if (Objects.nonNull(iiaGetResponse) && CollectionUtils.isNotEmpty(iiaGetResponse.getIia())) {

            Optional<IiasGetResponse.Iia.Partner> myRemotePartner = iiaGetResponse.getIia().get(0).getPartner().stream().filter(
                    p -> p.getHeiId().equals(globalProperties.getInstance())
            ).findFirst();
            // Validamos que el iia de nuestro partner esté completamente vinculado a nuestra copia local del iia
            if (!myRemotePartner.isPresent() || myRemotePartner.get().getIiaId() == null || iiasService.findByInteropId(myRemotePartner.get().getIiaId()) == null){
              throw new EwpIncorrectHashException(
                      "El iia Remoto no esta bien vinculado tu iia",
                      500);
            }

            // realizamos la validacion del hash y las condiciones de cooperacion contestadas si hay que validar todas o
            // no esta en la lista de exclusiones
            if (!noValidar && (validarTodas || (CollectionUtils.isNotEmpty(validationExcludedHeis)
                    && !validationExcludedHeis.contains(notificacion.getHeiId())))) {
              if (!iiasService.verificarHashRemoto(iiaGetResponse.getIia().get(0))) {
                throw new EwpIncorrectHashException(
                        "Ha fallado la verificacion del hash recibido, no coincide con el calculado a partir de las condiciones de cooperacion",
                        500);
              }
            }

            //comprobamos que en el Iia que vamos a validar no queden parámetros notYetDefined o iscedCode que no cumplen
            //los requisitos de Iias v7
            if (!iiasService.verificarValidoParaAprovar(iiaGetResponse.getIia().get(0))){
              throw new EwpIncorrectHashException(
                      "No se pueden aprobar Iias que todavía contienen valores obligatorios sin definir para v7",
                      500);
            }

            // comparamos el hash que nos llega en la respuesta del get
            // con el hash que habiamos almacenado en el iiaGet previo
            final boolean hashCoinciden =
                    iiasService.compararHash(iiaGetResponse.getIia().get(0), notificacion.getChangedElementIds());
            if (!hashCoinciden) {
              throw new EwpIncorrectHashException(
                      "Ha fallado la comparación de los hashes, no son iguales y no se puede notificar al iias-approval-cnr",
                      500);
            }
            // actulizamos el approval cooperation conditions hash
            iiasService.actualizarApprovalCoopCondHash(notificacion.getChangedElementIds(),
                    iiaGetResponse.getIia().get(0).getIiaHash());
            // llamamos al iia_cnr
            final Map<String, List<String>> params = new HashMap<>();
            params.put("approving_hei_id", approving);
            params.put("owner_hei_id", notifier);
            params.put("iia_id", iiaIds);
            final HeiEntry hei =
                getUrls(EwpConstants.IIA_APPROVAL_CNR_API_NAME, notificacion.getHeiId().toLowerCase());
            final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_CNR_URL_KEY,
                params, HttpMethodEnum.POST, EwpConstants.IIA_APPROVAL_CNR_API_NAME);
            procesarRespuesta(client.sendRequest(request, Empty.class, event), notificacion, event);
            notificationService.eliminarNotificacion(notificacion);
          } else {
            event.setObservations("sin respuesta del servicio");
            notificationService.liberarNotificaciones(notificacion, "sin respuesta del servicio");
          }

        } catch (final EwpIncorrectHashException e) {
          log.debug(e.getMessage());
          notificationService.setNotProcessingAndNotRepeteable(notificacion, e.getMessage());
          event.setObservations(e.getMessage());
          event.setStatus(EwpEventConstants.KO);
          if (globalProperties.isNotificationRefreshEnabled() &&
                  !notificationService.existsPendingNotification(notificacion.getChangedElementIds(),NotificationTypes.IIA,notificacion.getHeiId())) {
            notificationService.saveNotificationIiaRefresh(notificacion.getHeiId(),notificacion.getChangedElementIds(),notificacion.getOwnerHeiId());
          }
        } catch (final Exception e) {
          final String error = Objects.nonNull(notificacion)
                  ? "Error al notificar IIA APPROVAL, id IIA: " + notificacion.getChangedElementIds()
                  : "Error al notificar actualizacion IIA APPROVAL";
          log.error(error, getClass().getName(), e);
          event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
          event.setStatus(EwpEventConstants.KO);
          procesarError(notificacion);
        }
        eventService.save(event);
      } catch (final Exception e) {
        final String error =
                Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                        : "Error al procesar la notificacion ";
        log.error(error, getClass().getName(), e);
      }
    }
  }

  private HeiEntry getUrls(final String apiName, final String heiId) {
    HeiEntry hei = new HeiEntry();
    hei.setId(heiId);
    switch (apiName) {
      case EwpConstants.IIAS_API_NAME:
        hei = registry.getIiaHeiUrls(heiId);
        break;
      case EwpConstants.IIA_CNR_API_NAME:
        hei = registry.getIiaCnrHeiUrls(heiId);
        break;
      case EwpConstants.IIA_APPROVAL_CNR_API_NAME:
        hei = registry.getIiaApprovalCnrHeiUrls(heiId);
        break;
      case EwpConstants.OMOBILITY_CNR_API_NAME:
        hei = registry.getOmobilitiesCnrHeiUrls(heiId);
        break;
      case EwpConstants.OMOBILITY_LA_CNR_API_NAME:
        hei = registry.getOmobilitiesLACnrHeiUrls(heiId);
        break;
      case EwpConstants.IMOBILITY_CNR_API_NAME:
        hei = registry.getImobilitiesCnrHeiUrls(heiId);
        break;
      case EwpConstants.IMOBILITY_TOR_CNR_API_NAME:
        hei = registry.getImobilityTorsCnrHeiUrls(heiId);
        break;
      default:
        break;
    }
    // Para pruebas con dos instancias locales
    //final Map<String, String> urls = new HashMap<>();
    //switch (apiName) {
    //  case EwpConstants.IIA_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.IIAS_CNR_URL_KEY, targetUri + "/iias/cnr");
    //    break;
    //  case EwpConstants.IIA_APPROVAL_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.IIAS_APPROVAL_CNR_URL_KEY, targetUri + "/iias/approval/cnr");
    //    break;
    //  case EwpConstants.OMOBILITY_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.OUTGOING_MOBILITY_CNR_URL_KEY, targetUri + "/outgoingmobilities/cnr");
    //    break;
    //  case EwpConstants.OMOBILITY_LA_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.OUTGOING_MOBILITY_LA_CNR_URL_KEY, targetUri + "/omobilities/cnr");
    //    break;
    //  case EwpConstants.IMOBILITY_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.INCOMING_MOBILITY_CNR_URL_KEY, targetUri + "/incomingmobilities/cnr");
    //    break;
    //  case EwpConstants.IMOBILITY_TOR_CNR_API_NAME:
    //    urls.put(EwpRequestConstants.INCOMING_MOBILITY_TOR_CNR_URL_KEY, targetUri + "/tor/cnr");
    //    break;
    //}
    // urls.put(EwpRequestConstants.OUTGOING_MOBILITY_CNR, targetUri + "/outgoingmobilities/cnr");
    // urls.put(EwpRequestConstants.OUTGOING_MOBILITY_LA_CNR, targetUri + "/omobilities/cnr");
    // urls.put(EwpRequestConstants.INCOMING_MOBILITY_CNR, targetUri + "/incomingmobilities/cnr");
    // urls.put(EwpRequestConstants.INCOMING_MOBILITY_TOR_CNR, targetUri + "/tor/cnr");
    // urls.put(EwpRequestConstants.IIAS_GET_URL_KEY, targetUri + "/iias/get");
    // hei.setUrls(urls);
    return hei;
  }

  private void procesarError(final NotificationMDTO notification) {
    final ClientResponse response = new ClientResponse();
    response.setStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    procesarRespuesta(response, notification, null);
  }

  private void procesarRespuesta(final ClientResponse response, final NotificationMDTO notificacion,
                                 final EventMDTO event) {
    if(Objects.nonNull(response.getRawResponse())){
      event.setMessage(response.getRawResponse().getBytes());
    }
    if (response.getStatusCode() == HttpServletResponse.SC_OK) {
      notificationService.eliminarNotificacion(notificacion);
      utils.setMessageToEventOK(event, response.getResult());
    } else {
      notificationService.liberarNotificaciones(notificacion, response.getRawResponse());
      if (Objects.nonNull(event)) {
        event.setObservations(response.getRawResponse());
        event.setStatus(EwpEventConstants.KO);
      }
    }
  }

  private ClientResponse invocarIiaGet(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put("hei_id", Arrays.asList(notificacion.getHeiId()));
    params.put("iia_id", Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = getUrls(EwpConstants.IIAS_API_NAME, notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.IIAS_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.IIAS_API_NAME);
    return client.sendRequest(requestIndex, IiasGetResponse.class, event);
  }

  private EventMDTO generaEventMDTO(final NotificationMDTO notificacion, final NotificationTypes elementType) {
    return utils.initializeJobEvent( elementType,notificacion.getHeiId(),notificacion.getOwnerHeiId(),
            notificacion.getChangedElementIds(),EwpEventConstants.NOTIFY);
  }

}
