package es.minsait.ewpcv.common;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.tomitribe.auth.signatures.Signature;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.*;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;

/**
 * The type Utils.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class Utils {

  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private RegistryClient registryClient;
  @Autowired
  private IAuditConfigService auditConfigService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private IEventService eventService;
  @Autowired
  private MonitoringComponent monitoringComponent;


  /**
   * retorna una lista con las instituciones cubiertas por el host que pueden consultar los datos. tambien comprueba que
   * la institucion solicitada se encuentre cubierta por el host invocante
   * 
   * @param receivingHeiId
   * @param request
   * @return
   */
  public List<String> filterByPerssions(final List<String> receivingHeiId, final HttpServletRequest request) {
    List<String> receivingHeiIdToFilter = new ArrayList<>();
    final List<String> heisCovered = heisCovered(request);
    if (CollectionUtils.isNotEmpty(receivingHeiId)) {
      receivingHeiIdToFilter = heisCovered.stream().filter(receivingHeiId::contains).collect(Collectors.toList());
    } else {
      receivingHeiIdToFilter.addAll(heisCovered);
    }
    return receivingHeiIdToFilter;
  }


  public List<String> heisCovered(final HttpServletRequest request) {
    final String authorization = request.getHeader("authorization");

    if (Objects.isNull(authorization)) {
      final X509Certificate[] certificates =
          (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
      final X509Certificate certificate = registryClient.getCertificateKnownInEwpNetwork(certificates);

      return registryClient.getHeisCoveredByCertificate(certificate).stream().collect(Collectors.toList());
    }
    final Signature signature = Signature.fromString(authorization);
    final String fingerprint = signature.getKeyId();
    final RSAPublicKey rsaPublicKey = registryClient.findRsaPublicKey(fingerprint);

    return Objects.nonNull(rsaPublicKey)
        ? registryClient.getHeisCoveredByClientKey(rsaPublicKey).stream().collect(Collectors.toList())
        : new ArrayList<>();
  }

  public <T> T bytesToClassImplByClass(final byte[] pFicheroXml, final Class<?> claseJava) throws JAXBException {
    final JAXBContext context = JAXBContext.newInstance(claseJava);
    final Unmarshaller un = context.createUnmarshaller();

    final String ficheroXml = new String(pFicheroXml, Charset.forName("UTF-8"));
    final StringBuffer ficheroXmlSB = new StringBuffer(ficheroXml);
    final T res = (T) un.unmarshal(new StreamSource(new StringReader(ficheroXmlSB.toString())));
    return res;
  }

  public byte[] getByteArrayFromObject(final Object body, final boolean formatedOutput) throws IOException {
    try {
      if(Objects.isNull(body)){
        return new byte[0];
      }

      // comprobamos si el body de la respuesta es un array de bytes,
      // si lo es no hace falta hacer nada mas
      if (body instanceof ResponseEntity) {
        ResponseEntity response = (ResponseEntity) body;

        if (response.getBody() instanceof byte[]) {
          return (byte[]) response.getBody();
        }
      }

      if (body instanceof ClientResponse) {
        return ((ClientResponse) body).getRawResponse().getBytes();
      }

      final byte[] bodyBytes;
      final JAXBContext jaxbContext = JAXBContext.newInstance(body.getClass());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatedOutput);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        marshaller.marshal(body, baos);
        bodyBytes = baos.toByteArray();
      }
      return bodyBytes;
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
    return new byte[0];
  }

  public void isHeisCoveredByTheHost(@RequestParam("hei_id") final String heiId) throws EwpWebApplicationException {
    if (Strings.isNullOrEmpty(heiId) || !heiId.equalsIgnoreCase(globalProperties.getInstance())) {
      throw new EwpWebApplicationException("Hei_id not covered by this instance.", HttpStatus.BAD_REQUEST.value());
    }
  }

  public boolean isValidFormatReceivingAcademicYear(final String receivingAcademicYear) {
    return receivingAcademicYear.matches("[0-9]{4}/[0-9]{4}");
  }

  public EventMDTO initializeAuditableEvent(final Object triggeringHeiId, final MultiValueMap<String, String> emapParam,
      final HttpServletRequest request, final String eventType, final NotificationTypes type,
      final String changedElement) {

    final Collection<String> heisCovered;
    if (Objects.nonNull(triggeringHeiId)) {
      if (triggeringHeiId instanceof Collection) {
        heisCovered = (Collection<String>) triggeringHeiId;
      } else {
        heisCovered = Arrays.asList((String) triggeringHeiId);
      }
    } else {
      heisCovered = heisCovered(request);
    }

    final EventMDTO event = new EventMDTO();

    event.setEventDate(new Date());
    event
        .setTriggeringHei(Objects.nonNull(heisCovered) ? heisCovered.toString() : request.getHeader(EwpConstants.HOST));
    event.setOwnerHei(globalProperties.getInstance());
    event.setEventType(eventType);
    event.setElementType(type);
    event.setStatus(EwpEventConstants.OK);
    event.setRequestParams(Objects.nonNull(emapParam) ? emapParam.toString() : request.getQueryString());
    event.setXRequestId(request.getHeader(EwpConstants.X_REQUEST_ID));
    try {
      if ("POST".equalsIgnoreCase(request.getMethod()) && !Strings.isNullOrEmpty(request.getRequestURI())
          && request.getRequestURI().contains("/omobilities/update")) {
        event.setRequestBody(request.getReader().lines().collect(Collectors.joining(System.lineSeparator()))
            .concat(System.lineSeparator()).getBytes());
      } else {
        // obtenemos las headers de la request
        StringBuilder sb = new StringBuilder();
        final Enumeration<String> headerNames = request.getHeaderNames();
        sb.append("##### REQUEST HEADERS #####\n");
        while (headerNames.hasMoreElements()) {
          final String headerName = headerNames.nextElement();
          sb.append(headerName);
          sb.append(": ");
          sb.append(request.getHeader(headerName));
          sb.append("\n");
        }
        sb.append("\n");
        sb.append("##### REQUEST PARAMS #####\n");
        sb.append(Objects.nonNull(emapParam) ? emapParam.toString() : "");

        event.setRequestBody(sb.toString().getBytes());
      }
    } catch (IOException e) {
      log.error("Error reading request body", e);
    }
    if (Objects.nonNull(changedElement)) {
      event.setChangedElementIds(changedElement);
    }
    return event;
  }


  public EventMDTO initializeJobEvent(final NotificationTypes elementType, final String ownerHei,
      final String triggeringHei, final String changedElementId, final String eventType) {
    final EventMDTO event = new EventMDTO();

    event.setEventDate(new Date());
    event.setTriggeringHei(triggeringHei);
    event.setOwnerHei(ownerHei);
    event.setEventType(eventType);
    event.setElementType(elementType);
    event.setChangedElementIds(changedElementId);
    event.setStatus(EwpEventConstants.OK);

    return event;
  }

  public void setMessageToEventKO(final EventMDTO event, final ErrorResponse errorResponse,
      final String observationError) {
    try {
      event.setMessage(getByteArrayFromObject(errorResponse, false));
    } catch (final IOException e) {
      log.error(EwpEventConstants.ERROR_RESPONSE, e);
    }
    event.setObservations(observationError);
    event.setStatus(EwpEventConstants.KO);
  }

  public void setMessageToEventOK(final EventMDTO event, final Object respuesta) {
    try {
      event.setMessage(getByteArrayFromObject(respuesta, false));
    } catch (final IOException e) {
      log.error(EwpEventConstants.ERROR_RESPONSE, e);
    }
  }

  public boolean getValue(final String value) {
    boolean valorRespuesta = false;
    try {
      final String aux = auditConfigService.findByName(value).getValue();

      if (Objects.nonNull(aux) && aux.equals("true")) {
        valorRespuesta = true;
      }
    } catch (final Exception e) {
      log.error("Error al recoger: " + value + " de la tabla EWPCV_AUDIT_CONFIG", e);
    }

    return valorRespuesta;
  }


  /**
   * Filtra la lista de heis por las que se encuentran en la lista recibida por parametro Ademas excluye nuestra
   * instancia
   *
   * @param collectionToFilter
   * @return
   */
  public List<HeiEntry> filterHeisByProperties(List<HeiEntry> collectionToFilter, String heisToFilter) {
    if (!StringUtils.isBlank(heisToFilter)) {
      log.info("***filtrado por propiedad unicamente refrescamos las siguientes instituciones: " + heisToFilter);
      final List<String> defaultHeiIds = Arrays.asList(heisToFilter.replace(" ", "").split(","));

      collectionToFilter = collectionToFilter.stream().filter(h -> defaultHeiIds.contains(h.getId().toLowerCase()))
          .collect(Collectors.toList());
    }

    // excluimos nuestra instancia
    collectionToFilter = collectionToFilter.stream()
        .filter(h -> !globalProperties.getInstance().equalsIgnoreCase(h.getId())).collect(Collectors.toList());
    return collectionToFilter;
  }

  /**
   * Forma una peticion a partir de un hei, una urlKey y un map de parametros
   *
   * @param hei
   * @param urlKey
   * @param params
   * @param method
   * @param apiName
   * @return
   * @throws EwpOperationNotFoundException
   */
  public ClientRequest formarPeticion(final HeiEntry hei, final String urlKey, final Map<String, List<String>> params,
      final HttpMethodEnum method, String apiName) throws EwpOperationNotFoundException {

    if (Objects.isNull(hei) || Objects.isNull(hei.getUrls()) || hei.getUrls().isEmpty()
        || Strings.isNullOrEmpty(hei.getUrls().get(urlKey))) {
      // primero notificamos a monitorizacion
      monitoringComponent.invokeMonitoring(monitoringComponent.createMonitoringRequestParams(hei.getId(), apiName, ErrorTypeEnum.API_LOOKUP_ERROR, null, null,null,
          "Didn't foud a valid url"));
      throw new EwpOperationNotFoundException("No se ha encontrado " + urlKey + " para el heiId " + hei.getId(), 404);
    }

    final ClientRequest request = new ClientRequest();
    request.setUrl(hei.getUrls().get(urlKey));
    request.setHeiId(hei.getId());
    request.setMethod(method);
    request.setParams(params);
    request.setHttpsecClient(hei.isClientHttpSignature());
    request.setHttpsecServer(hei.isServerHttpSignature());
    request.setTlssec(hei.isClientTlsCert());
    request.setApiName(apiName);

    return request;
  }


  /**
   * verifica la respuesta de una peticion y actualiza y persiste el evento asociado a la misma libera las
   * notificaciones asociadas al evento
   *
   * @param notificacion
   * @param event
   * @param response
   * @param eventType
   * @return
   */
  public boolean verifyResponse(final NotificationMDTO notificacion, final EventMDTO event,
      final ClientResponse response, final String eventType) {
    String observaciones = null;
    boolean errorResponse = false;
    boolean errorValidacion = false;

    if (response.getStatusCode() != org.apache.http.HttpStatus.SC_OK) {
      observaciones = response.getRawResponse();
      errorResponse = true;
    }
    //si no cumple con el xsd no verificamos la respuesta
    if(!response.getXsdComplies()){
      observaciones = response.getErrorMessage();
      event.setMessage(response.getRawResponse().getBytes());
      errorValidacion = true;
    } else if (globalProperties.isVerifyResponses()) {
      if (Objects.nonNull(response.getHttpsecMsg())
              && !EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
        observaciones = response.getHttpsecMsg();
        errorValidacion = true;
      }
    }
    if (errorResponse || errorValidacion) {
      event.setEventType(eventType);
      event.setObservations(observaciones);
      event.setStatus(EwpEventConstants.KO);
      try {
        event.setMessage(getByteArrayFromObject(response, false));
      } catch (final IOException e) {
        log.error(EwpEventConstants.ERROR_RESPONSE, e);
      }
      eventService.save(event);
      if(Objects.nonNull(notificacion)){
        notificationService.liberarNotificaciones(notificacion, observaciones);
      }
      return false;
    }

    setMessageToEventOK(event, response.getResult());
    return true;
  }
   /**
   * Recibe un mensaje de error y lo acorta a 4000 caracteres de ser necesario
   *
   * @param errorMessage Mensaje de error que se validará para no exceder los 4000 caracteres.
   * @return Mensaje de error en String.
   */
  public String truncateToMaxVarchar(final String errorMessage) {
    if (Objects.nonNull(errorMessage) && errorMessage.length() > 4000) {
      return errorMessage.substring(0, 4000);
    }
    return errorMessage;
  }
}
