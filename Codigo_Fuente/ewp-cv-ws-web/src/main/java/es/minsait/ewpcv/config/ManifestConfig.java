package es.minsait.ewpcv.config;


import eu.erasmuswithoutpaper.api.discovery.Manifest;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * The type Manifest config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class ManifestConfig {

  @Value("${manifestFile}")
  private String manifestFile;

  public Manifest ManifestReader() {
    final Jaxb2Marshaller manifestMarshaller = new Jaxb2Marshaller();
    manifestMarshaller.setClassesToBeBound(Manifest.class);

    final StaxEventItemReader<Manifest> xmlFileReader = new StaxEventItemReader<Manifest>();
    xmlFileReader.setName("ManifestReader");
    xmlFileReader.setResource(new PathResource(manifestFile));
    xmlFileReader.setFragmentRootElementName("manifest");
    xmlFileReader.setUnmarshaller(manifestMarshaller);

    try {
      xmlFileReader.open(new ExecutionContext());
      return xmlFileReader.read();
    } catch (final UnexpectedInputException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return new Manifest();
  }
}
