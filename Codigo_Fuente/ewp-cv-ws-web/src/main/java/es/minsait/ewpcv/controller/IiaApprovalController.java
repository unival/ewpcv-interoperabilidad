package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.batch.IiaCnrJob;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.error.EwpResponseVerificationException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.*;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;
import eu.erasmuswithoutpaper.api.iias7.approval.cnr.IiaApprovalCnrResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static es.minsait.ewpcv.model.notificacion.QEvent.event;

/**
 * The type Iia approval controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/iias/approval", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class IiaApprovalController {

  @Autowired
  private IIiasService iiaService;
  @Autowired
  private IIiasApprovalService iiasApprovalService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private IEventService eventService;
  @Autowired
  private RestClient client;
  @Autowired
  private RegistryClient registry;

  @Autowired
  private Utils utils;

  @Value("${max.iia.ids}")
  private int maxIiaIds;


  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<IiasApprovalResponse> getGet(
          @RequestParam(value = "iia_id", required = false) final List<String> iiaId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) throws EwpWebApplicationException {

    List<String> approvingHeiId = utils.heisCovered(request);
    String ownerHeiId = globalProperties.getInstance();

    log.debug("Inicio invocacion GET /get IIAAproval");
    final EventMDTO event =
            utils.initializeAuditableEvent(approvingHeiId.get(0), emapParam, request, EwpEventConstants.IIAAPPROVALGET_GET,
                    NotificationTypes.IIA_APPROVAL, Objects.nonNull(iiaId) ? iiaId.toString() : null);

    try {
      isParamOk(approvingHeiId, iiaId);

      if (iiaId.size() > maxIiaIds) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      return processException(ex, event);
    }

    final IiasApprovalResponse respuesta = new IiasApprovalResponse();
    respuesta.getApproval()
            .addAll(iiasApprovalService.findIiasByRemoteIiaIdAndPartners(approvingHeiId.get(0), globalProperties.getInstance(), iiaId));

    for (String iia : iiaId) {
      try {
        byte[] remoteIIARaw = invocarIiaGet(ownerHeiId,approvingHeiId.get(0), iia);
        iiaService.takeSnapshot(iia, event,EwpConstants.IIAS_CLIENT_VERSION, globalProperties.getInstance(), remoteIIARaw);
      } catch (Exception e) {
        log.error("error en el catch", e);
        event.setStatus("KO");
        event.setObservations("There was a problem taking the snapshot for the IIA, wont serve response");
        eventService.save(event);
        throw new EwpWebApplicationException("There was a problem taking the snapshot for the IIA, wont serve response", HttpStatus.INTERNAL_SERVER_ERROR.value());
      }
    }

    if (utils.getValue(EwpConstants.IIAAPPROVALAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    log.debug("Fin invocacion GET /get IIAApproval");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<IiasApprovalResponse> postGet(@RequestBody final MultiValueMap<String, String> iiaGetList,
                                                      final HttpServletRequest request) throws EwpWebApplicationException {
    log.debug("Inicio invocacion POST /get IIA approval");

    // Extraemos los parámetros del form
    List<String> approvingHeiId = utils.heisCovered(request);
    String ownerHeiId = globalProperties.getInstance();
    final List<String> iiaId = iiaGetList.get("iia_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(approvingHeiId, iiaGetList, request, EwpEventConstants.IIAAPPROVALGET_POST,
                    NotificationTypes.IIA_APPROVAL, Objects.nonNull(iiaId) ? iiaId.toString() : null);

    try {
      isParamOk(approvingHeiId, iiaId);

      if (Objects.nonNull(iiaId) && iiaId.size() > maxIiaIds) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      return processException(ex, event);
    }

    final IiasApprovalResponse respuesta = new IiasApprovalResponse();
    respuesta.getApproval()
            .addAll(iiasApprovalService.findIiasByRemoteIiaIdAndPartners(approvingHeiId.get(0), globalProperties.getInstance(), iiaId));

    for (String iia : iiaId) {
      try {
        byte[] remoteIIARaw = invocarIiaGet(ownerHeiId ,approvingHeiId.get(0), iia);
        iiaService.takeSnapshot(iia, event, EwpConstants.IIAS_CLIENT_VERSION, globalProperties.getInstance(), remoteIIARaw);
      } catch (Exception e) {
        log.error("error en el catch", e);
        event.setStatus("KO");
        event.setObservations("There was a problem taking the snapshot for the IIA, wont serve response");
        eventService.save(event);
        throw new EwpWebApplicationException("There was a problem taking the snapshot for the IIA, wont serve response", HttpStatus.INTERNAL_SERVER_ERROR.value());
      }
    }

    if (utils.getValue(EwpConstants.IIAAPPROVALAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    log.debug("Fin invocacion POST /get IIAApproval");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                final HttpServletRequest request) {

    List<String> approvingHeiId = utils.heisCovered(request);
    String ownerHeiId = globalProperties.getInstance();
    final String iiaId = cnrlist.toSingleValueMap().get("iia_id");

    final EventMDTO event = utils.initializeAuditableEvent(approvingHeiId, cnrlist, request,
            EwpEventConstants.IIAAPPROVALCNR_POST, NotificationTypes.IIA_APPROVAL, iiaId);

    try {
      isParamOk(approvingHeiId, Arrays.asList(iiaId));

    } catch (final EwpWebApplicationException ex) {
      return processException(ex, event);
    }

    notificationService.saveNotificationIiaApprovalRefresh(approvingHeiId.get(0), iiaId, globalProperties.getInstance());

    eventService.save(event);
    return ResponseEntity.ok(new IiaApprovalCnrResponse());
  }

  private void isParamOk(final List<String> partnerHei, final List<String> iiaId)
          throws EwpWebApplicationException {
    if (Objects.isNull(iiaId)) {
      throw new EwpWebApplicationException("iia_id is required",
              HttpStatus.BAD_REQUEST.value());
    }

    if (CollectionUtils.isEmpty(partnerHei) || partnerHei.size() > 1) {
      throw new EwpWebApplicationException("Can't determine hei_id from signature information",
              HttpStatus.BAD_REQUEST.value());
    }
  }

  private ResponseEntity processException(EwpWebApplicationException ex, EventMDTO event) {
    final ErrorResponse errorResponse = new ErrorResponse();
    final MultilineString message = new MultilineString();
    message.setValue(ex.getMessage());
    errorResponse.setDeveloperMessage(message);

    if (utils.getValue(EwpConstants.IIAAPPROVALAUDIT)) {
      utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
    }
    eventService.save(event);

    return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
  }

  private byte[] invocarIiaGet(final String ownerHeiId,final String approvingHeiId, String iiaRemoteId) throws EwpOperationNotFoundException, EwpResponseVerificationException {
    EventMDTO iiaGetEvent = null;
    try {
      final Map<String, List<String>> params = new HashMap<>();
      params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(approvingHeiId));
      params.put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList(iiaRemoteId));
      final HeiEntry hei = registry.getIiaHeiUrls(approvingHeiId.toLowerCase());
      final ClientRequest request =
              utils.formarPeticion(hei, EwpRequestConstants.IIAS_GET_URL_KEY, params, HttpMethodEnum.POST,
                      EwpConstants.IIAS_API_NAME);
      iiaGetEvent=utils.initializeJobEvent(NotificationTypes.IIA,approvingHeiId,ownerHeiId,iiaRemoteId,EwpEventConstants.IIAGET_POST);
      ClientResponse response = client.sendRequest(request, IiasGetResponse.class, iiaGetEvent);
      if (!utils.verifyResponse(null, iiaGetEvent, response, EwpEventConstants.IIAGET_POST)) {
        throw new EwpResponseVerificationException("Error en la respuesta del IIA GET");
      }
      iiaGetEvent.setObservations("IIA GET realizado para snapshot");
      eventService.save(iiaGetEvent);
      return response.getRawResponse().getBytes();
    }  catch (EwpResponseVerificationException er) {
      //la verificacion ya persiste el evento aqui no hace falta
      log.error("Error verificando la respuesta del IIA GET para el IIA ID remoto: " + iiaRemoteId + "error " + iiaGetEvent.getObservations());
      throw er;
    } catch (Exception e) {
      log.error("Error realizando IIA GET IIA ID remoto: " + iiaRemoteId, e);
      if (iiaGetEvent != null) {
        iiaGetEvent.setStatus(EwpEventConstants.KO);
        iiaGetEvent.setObservations("Error realizando IIA GET para el IIA ID remoto: " + iiaRemoteId + ": " + e.getMessage());
        eventService.save(iiaGetEvent);
      }
      throw e;
    }
  }

}
