package es.minsait.ewpcv.security;

import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.RegistryClient;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Slf4j
@Component
public class ContentTypeCheckInterceptor implements HandlerInterceptor {


    HttpSignature httpSignature;
    Utils utils;

    IEventService eventService;

    private void injectBeans(final ServletRequest request) {
        if (Objects.isNull(httpSignature)) {
            final ServletContext servletContext = request.getServletContext();
            final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            httpSignature = webApplicationContext.getBean(HttpSignature.class);
        }
        if (Objects.isNull(utils)) {
            final ServletContext servletContext = request.getServletContext();
            final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            utils = webApplicationContext.getBean(Utils.class);
        }
        if (Objects.isNull(eventService)) {
            final ServletContext servletContext = request.getServletContext();
            final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            eventService = webApplicationContext.getBean(IEventService.class);
        }

    }
    /**
     * Interceptor para manejar peticiones que vienen sin contenido y por tanto sin la cabecera content-type.
     * El atributo consumes de las anotaciones de restController no detecta si la peticion viene sin contenido ni cabecera asi que provoca fallos en el manejo de las peticiones.
     *
     * Las peticiones POST al servicio UPDATE de los LAs tienen que ser xmls el resto tienen que ser form-urlencoded
     * Algunos clientes no envian la cabecera content-type cuando no hay contenido y el servidor no puede procesar la peticion.
     *
     * @param request current HTTP request
     * @param response current HTTP response
     * @param handler chosen handler to execute, for type and/or instance evaluation
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        injectBeans(request);

        if (Objects.isNull(request)) {
            return false;
        }

        // si no es un post lo gestionamos con las anotaciones
        if (!HttpMethod.POST.matches(request.getMethod())) {
            return true;
        }

        String contentType = request.getHeader("Content-Type");
        //si es para el servicio de update de los LAS
        if (request.getRequestURI().contains("/omobilities/update")) {
            //si viene sin body o con un body que no sea un xml y es invocacion a /omobilities/update rechazamos la peticion porque no se puede procesar
            if ((StringUtils.isEmpty(contentType) || contentType.contains(MediaType.TEXT_XML_VALUE) || contentType.contains(MediaType.APPLICATION_XML_VALUE)) && request.getContentLength() == 0) {
                unnaceptableRequestProcessing(request, response,  "Request for update learning agreement service must have a body with content-type xml");
                return false;
            }
            return true;
        }
        else {
            //Para el resto de peticiones post tiene que ser form-urlencoded y admitimos que venga sin contenido ni content type
            if ((StringUtils.isEmpty(contentType) || !contentType.contains(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
                    && request.getContentLength() > 0) {
                unnaceptableRequestProcessing(request, response,  "The request with body must have content-type application/x-www-form-urlencoded");
                return false;
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }


    private void unnaceptableRequestProcessing(HttpServletRequest request, HttpServletResponse response, String messageText) throws IOException {
        response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        log.error("Error validando content type de una petición entrante: " + messageText);
        final ErrorResponse errorResponse = new ErrorResponse();
        final MultilineString message = new MultilineString();
        message.setValue(messageText);
        errorResponse.setDeveloperMessage(message);
        if (httpSignature.clientWantsSignedResponse(request)) {
            httpSignature.signResponse(request, response, errorResponse, true);
        }
        response.getWriter().write(errorToString(errorResponse));
        // Evento mínimo para auditar una petición entrante no válida
        final EventMDTO event = utils.initializeAuditableEvent(null, null, request, null,
                NotificationTypes.UNDEFINED, null);
        event.setObservations(messageText);
        event.setStatus(EwpEventConstants.KO);
        event.setMessage(errorToString(errorResponse).getBytes(StandardCharsets.UTF_8));
        eventService.save(event);
    }

    private String errorToString(final ErrorResponse errorResponse) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(errorResponse.getClass());
            final Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            marshaller.marshal(errorResponse, baos);
            return baos.toString();
        } catch (final JAXBException e) {
            log.error("Se ha producido un error generando el mensaje de la respuesta del filtro de seguridad");
        }
        return StringUtils.EMPTY;
    }
}
