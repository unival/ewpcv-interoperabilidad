package es.minsait.ewpcv.config;

import com.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;


/**
 * The type Swagger config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
@EnableSwagger2
@Profile({"dev", "desa", "loc"})
public class SwaggerConfig {
  @Autowired
  private Environment env;


  @Bean
  public Docket apiDocket() {

    final Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(documentsApiInfo()).select()
            .apis(RequestHandlerSelectors.basePackage("es.minsait.ewpcv.controller")).paths(documentsPaths()).build();

    return docket;

  }


  /**
   * Config paths.
   *
   * @return the predicate
   */
  private Predicate<String> documentsPaths() {
    return regex("/.*");
  }

  /**
   * Api info
   *
   * @return ApiInfo
   */
  private ApiInfo documentsApiInfo() {
    return new ApiInfoBuilder().title("Rest Services EWPCV")
            .description("Servicios REST del modulo de interoperabilidad EWPCV")
            // .license("Apache License Version 2.0")
            .version(getProperty("application-version"))
            // .contact(new Contact("", "", ""))
            .build();
  }

  private String getProperty(final String pPropertyKey) {
    return env.getProperty(pPropertyKey);
  }

}
