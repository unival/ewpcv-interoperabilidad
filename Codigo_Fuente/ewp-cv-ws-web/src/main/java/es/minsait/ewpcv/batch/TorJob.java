package es.minsait.ewpcv.batch;


import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;

import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.ITorService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Tor job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class TorJob implements Job {


  @Value("${ewp.tor.cnr.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.tor.cnr.notification.block:50}")
  private int blockSize;

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private IEventService eventService;

  @Autowired
  private ITorService torService;

  @Autowired
  private OmobilityLasService mobilityService;

  @Autowired
  private Utils utils;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.incomingmobilitytor:true}")
  private boolean cnrTor;


  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrTor) {
      log.info("***No se ejecuta el proceso de refresco de TORs a partir de CNR por configuracion");
      return;
    }
    log.info("Comienza el proceso de refresco de TORs a partir de CNR");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.IMOBILITY_TOR);

    log.info("Se van a actualizar " + list.size() + " TORs");

    for (final NotificationMDTO notification : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IMOBILITY_TOR, notification.getOwnerHeiId(),
              notification.getHeiId(), notification.getChangedElementIds(), null);

      try {
        if (actualizarTor(notification, event)) {
          // si el TOR se actualiza sin problemas borramos la notificacion
          notificationService.eliminarNotificacion(notification);
        }
      } catch (final Exception e) {
        final String error = Objects.nonNull(notification)
                ? "Error al procesar TOR CNR, id mobility: " + notification.getChangedElementIds()
                : "Error al procesar TOR CNR";
        log.error(error, getClass().getName(), e);
        notificationService.liberarNotificaciones(notification, e.getMessage());
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }
      eventService.save(event);
    }

    log.info("Termina proceso de refresco de TORs");
  }

  private boolean actualizarTor(final NotificationMDTO notification, final EventMDTO event)
          throws EwpOperationNotFoundException {
    try {
      if (Objects.isNull(mobilityService.findLastMobilityRevisionByIdAndSendingHeiId(notification.getChangedElementIds(), globalProperties.getInstance()))) {
        log.info(
                "se ha tratado de actualizar un Tor para una mobility que no existe en el sistema, lo ignoramos");
        return Boolean.FALSE;
      }
      final ClientResponse response = invocarTorGet(notification, event);

      if (!utils.verifyResponse(notification, event, response, EwpEventConstants.CNR_PROCESSING_GET)) {
        return false;
      }

      final ImobilityTorsGetResponse torGetResponse = (ImobilityTorsGetResponse) response.getResult();

      if (Objects.nonNull(torGetResponse) && CollectionUtils.isNotEmpty(torGetResponse.getTor())) {
        // actualizamos el TOR
        final ImobilityTorsGetResponse.Tor tor = torGetResponse.getTor().get(0);
        final boolean actualizado = torService.actualizarTor(tor, globalProperties.getInstance());
        event.setEventType(actualizado ? EwpEventConstants.UPDATE : EwpEventConstants.INSERT);
        return true;
      } else {
        event.setEventType(EwpEventConstants.NO_ACTION);
        return true;
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de TORs", getClass().getName(), e);
      throw e;
    }
  }

  private ClientResponse invocarTorGet(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.TOR_GET_RECEIVING_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.TOR_GET_OMOBILITY_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getTorHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.TOR_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.IMOBILITY_TORS_API_NAME);
    return client.sendRequest(requestIndex, ImobilityTorsGetResponse.class, event);
  }

}
