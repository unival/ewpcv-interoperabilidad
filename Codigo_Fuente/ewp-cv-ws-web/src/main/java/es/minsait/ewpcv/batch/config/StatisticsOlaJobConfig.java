package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.StatisticsOlaJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type StatisticsOla job config.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Configuration
public class StatisticsOlaJobConfig {

    @Bean
    public JobDetailFactoryBean jobDetailStatisticsOla() {
        final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(StatisticsOlaJob.class);
        jobDetailFactory.setDescription("Batch para refresco de StatisticsOla");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }

    @Bean
    public CronTriggerFactoryBean triggerStatisticsOla(@Qualifier("jobDetailStatisticsOla") final JobDetail jobDetail,
                                                       @Value("${ewp.StatisticsOla.refresh}") final String cron) {
        final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
        triggerFactory.setCronExpression(cron);
        triggerFactory.setJobDetail(jobDetail);
        return triggerFactory;
    }


}
