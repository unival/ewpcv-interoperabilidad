package es.minsait.ewpcv.batch.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import es.minsait.ewpcv.batch.RefreshJob;

/**
 * The type Refresh job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class RefreshJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailRefreshJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(RefreshJob.class);
    jobDetailFactory.setDescription("Batch para carga de actualizaciones de IIAS y LAs sin necesidad de CNR");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }
}
