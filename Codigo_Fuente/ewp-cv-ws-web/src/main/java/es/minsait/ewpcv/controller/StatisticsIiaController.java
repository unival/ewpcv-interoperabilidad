package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IStatisticsIiaService;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;


/**
 * The StatisticsIia controller.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@RestController
@RequestMapping(value = "/StatisticsIia", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class StatisticsIiaController {

    @Autowired
    GlobalProperties globalProperties;
    @Autowired
    private IStatisticsIiaService statisticsIiaService;
    @Autowired
    private Utils utils;
    @Autowired
    private IEventService eventService;

    @GetMapping()
    public ResponseEntity<IiasStatsResponse> getStatisticsIia(final HttpServletRequest request) {

        log.debug("Inicio invocacion GET StatisticsIia");

        final EventMDTO event = utils.initializeAuditableEvent(null, null, request, EwpEventConstants.STATISTICSIIA_GET,
                NotificationTypes.STATISTICS_IIA_NAMESPACE, null);

        IiasStatsResponse respuesta = new IiasStatsResponse();


        if (Objects.nonNull(statisticsIiaService.getStatisticsRest())) {

            respuesta=statisticsIiaService.getStatisticsRest();
        }

        if (utils.getValue(EwpConstants.STATISTICSIIAUDIT)) {
            utils.setMessageToEventOK(event, respuesta);
        }

        eventService.save(event);

        log.debug("Fin invocacion GET StatisticsIia");
        return ResponseEntity.ok(respuesta);
    }


}
