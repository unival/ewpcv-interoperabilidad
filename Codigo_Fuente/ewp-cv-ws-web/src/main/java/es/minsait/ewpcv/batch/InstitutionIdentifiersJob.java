package es.minsait.ewpcv.batch;

import es.minsait.ewpcv.common.RegistryClient;
import es.minsait.ewpcv.registryclient.HeiEntry;
import es.minsait.ewpcv.service.api.IInstitutionIdentifiersService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Institution identifiers job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class InstitutionIdentifiersJob implements Job {

  @Autowired
  RegistryClient registryClient;
  @Autowired
  private IInstitutionIdentifiersService institutionIdentifiersService;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    log.info("***Comienza el proceso de refresco de identificadores de instituciones");
    final List<HeiEntry> list = registryClient.getAllHeis().stream().collect(Collectors.toList());
    list.stream().forEach(heiEntry -> institutionIdentifiersService.saveOrUpdateInstitutionIdentifier(heiEntry));
  }
}
