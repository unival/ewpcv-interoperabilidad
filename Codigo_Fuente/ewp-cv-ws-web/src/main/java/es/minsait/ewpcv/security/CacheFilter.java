package es.minsait.ewpcv.security;

import com.google.common.base.Strings;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * The type Cache filter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
/*
 * Este filtro se utiliza para cachear el body de la request y poder leerlo varias veces sin consumir el input stream
 * Adicionalmente para los POST elimina la lista de parametros equivalente a los queryParams(propios de un GET) que se
 * deben ignorar en estos casos
 */
@Component
public class CacheFilter implements Filter {

  private FilterConfig filterConfig = null;

  @Override
  public void doFilter(ServletRequest request, final ServletResponse response, final FilterChain chain)
          throws IOException, ServletException {

    final HttpServletRequest httpRequest = (HttpServletRequest) request;

    final CachedBodyServletRequest requestWrapper = new CachedBodyServletRequest(httpRequest);
    // en caso de post tenemos que quitar los parametros propios del get
    if ("POST".equalsIgnoreCase(httpRequest.getMethod()) && !Strings.isNullOrEmpty(httpRequest.getQueryString())) {
      eliminarQueryParamsEnPost(requestWrapper);
    }
    request = requestWrapper;
    chain.doFilter(request, response);
  }

  @Override
  public void init(final FilterConfig filterConfiguration) throws ServletException {
    this.filterConfig = filterConfiguration;
  }

  @Override
  public void destroy() {
    this.filterConfig = null;
  }

  private void eliminarQueryParamsEnPost(final CachedBodyServletRequest request) {

    final Map<String, List<String>> queryParams = new HashMap<>();
    final List<String> queryString = Arrays.asList(request.getQueryString().split("&"));
    queryString.forEach(qs -> {
      final String[] keyValue = qs.split("=");
      if (queryParams.containsKey(keyValue[0])) {
        queryParams.get(keyValue[0]).add(keyValue[1]);
      } else {
        final List<String> value = new ArrayList<>();
        value.add(keyValue[1]);
        queryParams.put(keyValue[0], value);
      }
    });

    for (final String key : queryParams.keySet()) {
      final List<String> param = new ArrayList<String>(Arrays.asList(request.getParameterMap().get(key)));
      param.removeAll(queryParams.get(key));
      if (param.isEmpty()) {
        request.getParameterMap().remove(key);
      } else {
        request.getParameterMap().put(key, param.toArray(new String[0]));
      }
    }
    request.getParameterMap();
  }
}
