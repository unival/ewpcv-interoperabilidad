package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.LearningAgreementCnrJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Learning agreement cnr job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class LearningAgreementCnrJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailLearningAgreementCnrJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(LearningAgreementCnrJob.class);
    jobDetailFactory.setDescription("Batch para solicitud de Learning Agreements en respuesta de CNR");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerLearningAgreementCnrJob(
      @Qualifier("jobDetailLearningAgreementCnrJob") final JobDetail jobDetail,
      @Value("${ewp.la.cnr.refresco.cron}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
