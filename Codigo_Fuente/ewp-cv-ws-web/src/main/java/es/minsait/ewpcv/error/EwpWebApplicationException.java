package es.minsait.ewpcv.error;

/**
 * The type Ewp web application exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpWebApplicationException extends Exception {
  int status;

  public EwpWebApplicationException(String message, int status) {
    super(message);
    this.status = status;
  }

  public int getStatus() {
    return status;
  }
}
