package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IInstitutionService;
import es.minsait.ewpcv.service.api.IOunitService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * The type Organization unit controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/ounits", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class OrganizationUnitController {

  @Autowired
  private IInstitutionService institutionService;
  @Autowired
  private IOunitService ounitService;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.ounits.ids}")
  private int maxIds;
  @Value("${max.ounits.codes}")
  private int maxCodes;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<OunitsResponse> getGet(
          @RequestParam(value = "hei_id", required = true) final List<String> heiId,
          @RequestParam(value = "ounit_id", required = false) final List<String> ounitsIds,
          @RequestParam(value = "ounit_code", required = false) final List<String> ounitsCodes,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request)
          throws IOException {

    log.debug("Inicio invocacion GET /get Organization Unit");

    final ResponseEntity<OunitsResponse> respuesta =
            getResponse(heiId, ounitsIds, ounitsCodes, EwpEventConstants.ORGANIZATIONUNITGET_GET, emapParam, request);

    log.debug("Fin invocacion GET /get Organization Unit");
    return respuesta;
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<OunitsResponse> postGet(@RequestBody final MultiValueMap<String, String> params,
                                                @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request)
          throws IOException {
    log.debug("Inicio invocacion POST /get Organization Unit");

    // Extraemos los parámetros del form
    final List<String> heiId = params.get("hei_id");
    final List<String> ounitsIds = params.get("ounit_id");
    final List<String> ounitsCodes = params.get("ounit_code");

    final ResponseEntity<OunitsResponse> respuesta =
            getResponse(heiId, ounitsIds, ounitsCodes, EwpEventConstants.ORGANIZATIONUNITGET_POST, emapParam, request);

    log.debug("Fin invocacion POST /get Organization Unit");
    return respuesta;
  }

  private ResponseEntity<OunitsResponse> getResponse(final List<String> heiId, final List<String> ounitsIds,
                                                     final List<String> ounitsCodes, final String eventType, final MultiValueMap<String, String> emapParam,
                                                     final HttpServletRequest request) throws IOException {

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, eventType,
            NotificationTypes.ORGANIZATION_UNIT, Objects.nonNull(ounitsIds) ? ounitsIds.toString() : null);

    final ResponseEntity errorResponse = comprobarRequestParams(heiId, ounitsIds, ounitsCodes, event);

    if (errorResponse != null) {
      return errorResponse;
    }

    final List<OunitsResponse.Ounit> reponseOunits = ounitService.obtenerOunits(heiId.get(0), ounitsIds, ounitsCodes);

    final OunitsResponse respuesta = new OunitsResponse();
    if (reponseOunits != null) {
      respuesta.getOunit().addAll(reponseOunits);
    }

    if (utils.getValue(EwpConstants.ORGANIZATIONUNITAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);

    return ResponseEntity.ok(respuesta);
  }

  private ResponseEntity comprobarRequestParams(final List<String> heiId, final List<String> ounitsIds,
                                                final List<String> ounitsCodes, final EventMDTO event) throws IOException {
    try {
      if (Objects.isNull(heiId)) {
        throw new EwpWebApplicationException("hei_id is required.", HttpStatus.BAD_REQUEST.value());
      } else if (heiId.size() > 1) {
        throw new EwpWebApplicationException("just one hei_id is allowed at a time.", HttpStatus.BAD_REQUEST.value());
      }

      // comprobamos si la institucion sobre la que nos piden informacion de sus
      // ounits es nuestra o la tenemos en bbdd
      isKnownHei(heiId.get(0));

      // solo aceptamos o la lista de ids o la de codes, pero no las dos
      if (Objects.nonNull(ounitsIds) && Objects.nonNull(ounitsCodes)) {
        throw new EwpWebApplicationException(
                "Both ounit_id and ounit_code have been provided. Try to use only one of them.",
                HttpStatus.BAD_REQUEST.value());
      } else if (Objects.nonNull(ounitsIds) && ounitsIds.size() > maxIds) {
        throw new EwpWebApplicationException("Max number of Ounit id's has been exceeded.",
                HttpStatus.BAD_REQUEST.value());
      } else if (Objects.nonNull(ounitsCodes) && ounitsCodes.size() > maxCodes) {
        throw new EwpWebApplicationException("Max number of Ounit code's has been exceeded.",
                HttpStatus.BAD_REQUEST.value());
      } else if (Objects.isNull(ounitsIds) && Objects.isNull(ounitsCodes)) {
        throw new EwpWebApplicationException("ounit_id or ounit_code are required.", HttpStatus.BAD_REQUEST.value());
      }

    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.ORGANIZATIONUNITAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
    return null;
  }

  private void isKnownHei(@RequestParam("hei_id") final String heiId) throws EwpWebApplicationException {
    if (Strings.isNullOrEmpty(heiId) || !institutionService.isKnownHei(heiId)) {
      throw new EwpWebApplicationException("heid_id not covered nor known by this instance.",
              HttpStatus.BAD_REQUEST.value());
    }
  }
}
