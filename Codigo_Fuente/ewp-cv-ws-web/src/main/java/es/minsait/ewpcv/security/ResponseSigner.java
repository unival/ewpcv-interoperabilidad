package es.minsait.ewpcv.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * The type Response signer.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@ControllerAdvice
public class ResponseSigner implements ResponseBodyAdvice<Object> {

  @Autowired
  HttpSignature httpSignature;

  @Override
  public boolean supports(final MethodParameter returnType, final Class<? extends HttpMessageConverter<?>> converterType) {
    return true;
  }

  @Override
  public Object beforeBodyWrite(final Object body, final MethodParameter returnType, final MediaType selectedContentType,
                                final Class<? extends HttpMessageConverter<?>> selectedConverterType, final ServerHttpRequest request,
                                final ServerHttpResponse response) {
    request.getHeaders();
    if (httpSignature.clientWantsSignedResponse(request)) {
      httpSignature.signResponse(request, response, body, false);
    }

    return body;
  }

}
