package es.minsait.ewpcv.batch;


import es.minsait.ewpcv.service.api.IStatisticsIiaService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@DisallowConcurrentExecution
public class StatisticsIiaJob implements Job {

    @Autowired
    private IStatisticsIiaService statisticsIiaService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext)
            throws JobExecutionException {

        log.info("Comienza proceso de recogida de estadisticas IIA");

        statisticsIiaService.obtainStatistics();

        log.info("Termina proceso de recogida de estadisticas IIA");

    }
}
