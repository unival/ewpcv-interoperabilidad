package es.minsait.ewpcv.batch;

import java.util.*;

import es.minsait.ewpcv.error.EwpOperationNotAllowedException;
import eu.erasmuswithoutpaper.api.architecture.Empty;
import es.minsait.ewpcv.repository.model.notificacion.CnrType;
import eu.erasmuswithoutpaper.api.iias7.endpoints.MobilitySpecification;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;

/**
 * The type Iia cnr job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class IiaCnrJob implements Job {

  @Value("${ewp.iia.cnr.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.iia.cnr.notification.block:50}")
  private int blockSize;

  @Value("${ewp.iia.autogenerate:true}")
  private boolean shouldAutogenerate;

  @Value("${ewp.iia.autogenerate.with.candidates:false}")
  private boolean autogenerateWithCandidates;

  @Value("${ewp.iia.get.pdfs:false}")
  private boolean workWithPDFs;

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private IEventService eventService;

  @Autowired
  private IIiasService iiaService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private Utils utils;

  @Autowired
  private OnDemandFetchService onDemandFetchService;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.iias:true}")
  private boolean cnrIias;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrIias) {
      log.info("***No se ejecuta el proceso de actualizacion de IIA a partir de CNR por configuracion");
      return;
    }
    log.info("***Comienza proceso de actualizacion de IIA a partir de CNR");

    final List<NotificationMDTO> notificaciones =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.IIA);

    log.info("Se van a actualizar " + notificaciones.size() + " IIAS");
    for (final NotificationMDTO notificacion : notificaciones) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IIA, notificacion.getOwnerHeiId(),
          notificacion.getHeiId(), notificacion.getChangedElementIds(), null);

      try {
        ClientResponse response = invocarIiaGet(notificacion, event);
        if (!utils.verifyResponse(notificacion, event, response, EwpEventConstants.CNR_PROCESSING_GET)) {
          continue;
        }

        final IiasGetResponse remotoResponse = (IiasGetResponse) response.getResult();
        if (Objects.isNull(remotoResponse) || CollectionUtils.isEmpty(remotoResponse.getIia())) {
          event.setEventType(EwpEventConstants.REMOVE);
          log.debug("***Comienza proceso de eliminación de IIA a partir de CNR");
          if(iiaService.processRemoteIiaDeletion(notificacion.getChangedElementIds(), notificacion.getHeiId(), notificacion.getHeiId(), globalProperties.getInstance(), globalProperties.isIiaDeleteEnabled(), event)){
            notifyIIAApproval(notificacion);
          }
        } else {
          final IiasGetResponse.Iia remoto = remotoResponse.getIia().get(0);// Corregir

          //######### INICIO COMPROBACION HEI Y OUNITS#######
            IiasGetResponse.Iia.Partner partner = remoto.getPartner().stream().filter( p -> !p.getHeiId().equals(globalProperties.getInstance())).findFirst().orElse(null);
            if(partner != null){
              onDemandFetchService.fetchInstitutionIfNotExists(partner.getHeiId());
            }

            IiasGetResponse.Iia.CooperationConditions coopConditions = remoto.getCooperationConditions();
            List<MobilitySpecification> specs = new ArrayList<>();
            specs.addAll(coopConditions.getStudentStudiesMobilitySpec());
            specs.addAll(coopConditions.getStaffTeacherMobilitySpec());
            specs.addAll(coopConditions.getStaffTrainingMobilitySpec());
            specs.addAll(coopConditions.getStudentTraineeshipMobilitySpec());
            //obtenemos las ounits del otro partner
            List<String> idsOunits = obtenerIdsOunits(specs);

            idsOunits.forEach(id -> {
              onDemandFetchService.fetchOUnitIfNotExists(id, partner.getHeiId());
            });
            //######### FIN COMPROBACION HEI Y OUNITS#######

            //si esta activada la propiedad para buscar los PDFs y el iia tiene el identificador del fichero
            if(workWithPDFs && !Strings.isNullOrEmpty(remoto.getPdfFile())){
              onDemandFetchService.fetchFile(notificacion.getHeiId(), remoto.getPdfFile());
            }

            final IiaMDTO iiaCopiaRemota =
                    iiaService.buscaPorIiaRemotoAndIsRemote(notificacion.getChangedElementIds(), Boolean.TRUE);
            boolean hayCambios = true;
          String antiguoHashIiaRemoto = null;
          if (Objects.isNull(iiaCopiaRemota)) {
              hayCambios = iiaService.guardarIiaCopiaRemota(remoto, globalProperties.getInstance());
              event.setEventType(hayCambios ? EwpEventConstants.INSERT : EwpEventConstants.NO_ACTION);
            } else {
              antiguoHashIiaRemoto = iiaCopiaRemota.getRemoteCoopCondHash();
              hayCambios = iiaService.actualizarIiaCopiaRemota(remoto, iiaCopiaRemota, globalProperties.getInstance());
              event.setEventType(hayCambios ? EwpEventConstants.UPDATE : EwpEventConstants.NO_ACTION);
            }

          // Si no se bindea automaticamente entonces procedemos a generar la copia autogenerada
          if (hayCambios && !bindeaCopiaPropia(remoto, event) && shouldAutogenerate) {
            if (iiaService.generarCopiaAutogeneradaIia(remoto, globalProperties.getInstance(),
                autogenerateWithCandidates)) {
              event.setObservations("autogeneracion de copia local vinculada");
            }
          }

          iiaService.takeSnapshotWithProvidedRemoteMessageInEvent(notificacion.getChangedElementIds(), event, EwpConstants.IIAS_CLIENT_VERSION, globalProperties.getInstance());

          final String regresionMessage = iiaService.proccesRegression(remoto, antiguoHashIiaRemoto);
            if (StringUtils.isNotEmpty(regresionMessage)) {
                event.setObservations(Strings.isNullOrEmpty(event.getObservations()) ? regresionMessage : event.getObservations() + ", " + regresionMessage);
            }
        }
        notificationService.eliminarNotificacion(notificacion);
      }catch (final EwpOperationNotAllowedException e) {
        String error =!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString();
        log.error(error);
        event.setStatus(EwpEventConstants.KO);
        notificationService.setNotProcessingAndNotRepeteable(notificacion, error);
        event.setObservations(error);
      } catch (final Exception e) {
        final String error = Objects.nonNull(notificacion)
            ? "Error al procesar actualizacion IIA-CNR, id iia: " + notificacion.getChangedElementIds()
            : "Error al procesar actualizacion IIA-CNR";
        log.error(error, getClass().getName(), e);
        notificationService.liberarNotificaciones(notificacion, e.getMessage());
        event.setStatus(EwpEventConstants.KO);
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
      }
      eventService.save(event);
    }
  }

  private void notifyIIAApproval(NotificationMDTO notificacion) throws EwpOperationNotFoundException {
    final EventMDTO event = utils.initializeJobEvent(NotificationTypes.IIA_APPROVAL, notificacion.getHeiId(),
            notificacion.getOwnerHeiId(), notificacion.getChangedElementIds(), EwpEventConstants.NOTIFY);
    event.setObservations("Revocación de la aprobacion del IIA borrado");
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_APPROVAL_APPROVING_HEI_ID, Arrays.asList(notificacion.getOwnerHeiId()));
    params.put(EwpRequestConstants.IIAS_APPROVAL_OWNER_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IIAS_APPROVAL_IIA_ID, Arrays.asList(notificacion.getChangedElementIds()));
    params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getIiaApprovalCnrHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_CNR_URL_KEY,
            params, HttpMethodEnum.POST, EwpConstants.IIA_APPROVAL_CNR_API_NAME);
    ClientResponse response = client.sendRequest(request, Empty.class, event);

    if (utils.verifyResponse(notificacion, event, response, EwpEventConstants.APPROVE)){
      eventService.save(event);
    }
  }


  /**
   * Vincula la copia local del iias si la remota que vuelca esta asociada a uno existente.
   *
   * @param remoto
   * @param event
   * @return
   */
  private boolean bindeaCopiaPropia(final IiasGetResponse.Iia remoto, final EventMDTO event) {
    boolean bindeado = false;
    // obtenemos lel identificador de nuestro iias que nos envian
    final Optional<IiasGetResponse.Iia.Partner> copiaLocalEnRemoto = remoto.getPartner().stream()
        .filter(partner -> partner.getHeiId().equalsIgnoreCase(globalProperties.getInstance())).findAny();
    // si viene informado es que ellos lo tienen vinculado
    if (copiaLocalEnRemoto.isPresent() && StringUtils.isNotEmpty(copiaLocalEnRemoto.get().getIiaId())) {

      final String iiaId = copiaLocalEnRemoto.get().getIiaId();
      final IiaMDTO iiaLocal = iiaService.findByInteropId(iiaId);
      // obtenemos los datos de su iia
      final Optional<IiasGetResponse.Iia.Partner> partnerRemoto = remoto.getPartner().stream()
          .filter(partner -> !partner.getHeiId().equals(globalProperties.getInstance())).findFirst();
      // si viene informado lo añadimos al nuestro para vincularlo
      if (partnerRemoto.isPresent()) {
        if (!(iiaLocal.getRemoteIiaId() != null && iiaLocal.getRemoteIiaId().equals(partnerRemoto.get().getIiaId()))) {
          iiaLocal.setIsRemote(Boolean.FALSE);
          iiaLocal.setRemoteIiaId(partnerRemoto.get().getIiaId());
          iiaLocal.setRemoteIiaCode(partnerRemoto.get().getIiaCode());
          iiaService.save(iiaLocal);
          event.setObservations("vinculacion de copias automatica");
          notificarCopiaBindeada(iiaLocal);
        }
        bindeado = true;
      }
    }
    return bindeado;
  }


  private ClientResponse invocarIiaGet(final NotificationMDTO notificacion, EventMDTO event) throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getIiaHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.IIAS_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.IIAS_API_NAME);
    return client.sendRequest(requestIndex, IiasGetResponse.class, event);
  }


  private List<String> obtenerIdsOunits(List<MobilitySpecification> specs) {
    List<String> ounitsids = new ArrayList<>();

    for(MobilitySpecification mobSpec : specs){
      String ounitId ="";
      if(mobSpec.getSendingHeiId().equals(globalProperties.getInstance())){
        ounitId = mobSpec.getReceivingOunitId();
      }else{
        ounitId = mobSpec.getSendingOunitId();
      }

      if (!ounitsids.contains(ounitId) && !Strings.isNullOrEmpty(ounitId)) {
        ounitsids.add(ounitId);
      }
    }

    return ounitsids;
  }

  private void notificarCopiaBindeada(IiaMDTO iia){
    log.info("Comienza el proceso de notificación de vinculación automática.");
    NotificationMDTO notificacion = crearNotificacion(iia);

    try {
      final List<String> heiIds = Arrays.asList(notificacion.getOwnerHeiId());
      final List<String> iiaIds = Arrays.asList(notificacion.getChangedElementIds());

      EventMDTO evento = generaEventMDTO(notificacion, NotificationTypes.IIA);

      try{
        final Map<String, List<String>> params = new HashMap<>();
        params.put(EwpRequestConstants.IIAS_CNR_NOTIFIER_HEI_ID, heiIds);
        params.put(EwpRequestConstants.IIAS_CNR_IIA_ID, iiaIds);
        HeiEntry hei = registry.getIiaCnrHeiUrls(notificacion.getHeiId().toLowerCase());

        final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.IIAS_CNR_URL_KEY, params,
                HttpMethodEnum.POST, EwpConstants.IIA_CNR_API_NAME);
        procesarRespuesta(client.sendRequest(request, Empty.class, evento), notificacion, evento);
      }catch (final Exception e){
        final String error = Objects.nonNull(notificacion)
                ? "Error al notificar IIA bindeado, id IIA: " + notificacion.getChangedElementIds()
                : "Error al notificar IIA bindeado";
        log.error(error, getClass().getName(), e);
        evento.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage(): e.toString());
        evento.setStatus(EwpEventConstants.KO);
      }
      evento.setObservations("Notificación de vinculación automática de iias");
      eventService.save(evento);
    }catch (final Exception e){
      final String error =
              Objects.nonNull(notificacion) ? "Error al procesar la notificacion: " + notificacion.getId()
                      : "Error al procesar la notificacion ";
      log.error(error, getClass().getName(), e);
    }
    log.info("Fin del proceso de notificación de vinculación automática.");
  }

  private NotificationMDTO crearNotificacion(IiaMDTO iia) {
    NotificationMDTO notificacion = new NotificationMDTO();
    notificacion.setChangedElementIds(iia.getInteropId());
    notificacion.setOwnerHeiId(globalProperties.getInstance());
    String heiId = iia.getFirstPartner().getInstitutionId().equals(globalProperties.getInstance()) ?
            iia.getSecondPartner().getInstitutionId() : iia.getFirstPartner().getInstitutionId();
    notificacion.setHeiId(heiId);
    notificacion.setCnrType(CnrType.NOTIFY);
    notificacion.setType(NotificationTypes.IIA);
    notificacion.setNotificationDate(new Date());
    return notificacion;
  }

  private EventMDTO generaEventMDTO(NotificationMDTO notificacion, NotificationTypes elementType) {
    return utils.initializeJobEvent( elementType,notificacion.getHeiId(),notificacion.getOwnerHeiId(),
            notificacion.getChangedElementIds(),EwpEventConstants.NOTIFY);
  }

  private void procesarRespuesta(final ClientResponse response, final NotificationMDTO notificacion,
                                 final EventMDTO evento) {
    if (response.getStatusCode() == HttpServletResponse.SC_OK) {
      utils.setMessageToEventOK(evento, response.getResult());
    }else {
      if (Objects.nonNull(evento)){
        evento.setObservations(response.getRawResponse());
        evento.setStatus(EwpEventConstants.KO);
      }
    }

  }

}
