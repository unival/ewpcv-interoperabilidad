package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.InstitutionIdentifiersJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Institution identifiers job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class InstitutionIdentifiersJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailInstitutionIdentifiers() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(InstitutionIdentifiersJob.class);
    jobDetailFactory.setDescription("Batch para refresco de codigos de institucion dede el registro");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerInstitutionIdentifiers(
      @Qualifier("jobDetailInstitutionIdentifiers") final JobDetail jobDetail,
      @Value("${ewp.institution.identifiers.refresh}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
