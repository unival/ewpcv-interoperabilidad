package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.omobilities.cnr.OmobilityCnrResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesIndexResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The type Omobility controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/outgoingmobilities", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class OmobilityController {

  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private OmobilityLasService omobilityService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.omobility.ids}")
  private int maxOmobilityIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<OmobilitiesGetResponse> getGet(
          @RequestParam(value = "sending_hei_id", required = false) final List<String> sendingHeiId,
          @RequestParam(value = "omobility_id", required = false) final List<String> omobilityId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /get Omobilities");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.OMOBILITYGET_GET,
            NotificationTypes.OMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (CollectionUtils.isEmpty(sendingHeiId) || Objects.isNull(omobilityId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (sendingHeiId.size() > 1) {
        throw new EwpWebApplicationException("Hei_id parameter is not repeteable.", HttpStatus.BAD_REQUEST.value());
      }
      utils.isHeisCoveredByTheHost(sendingHeiId.get(0));
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final OmobilitiesGetResponse respuesta = new OmobilitiesGetResponse();
    try {
      final List<StudentMobilityForStudies> omobilities =
              omobilityService.findStudentOmobilityByIdAndSendingInstitutionIdAndLastRevision(sendingHeiId.get(0),
                      utils.heisCovered(request), omobilityId);
      respuesta.getSingleMobilityObject().addAll(omobilities);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion GET /get Omobilities");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<OmobilitiesGetResponse> postGet(@RequestBody final MultiValueMap<String, String> laGetList,
                                                        final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get Omobilities");

    final List<String> sendingHeiId = laGetList.get("sending_hei_id");
    final List<String> omobilityId = laGetList.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(null, laGetList, request, EwpEventConstants.OMOBILITYGET_POST,
                    NotificationTypes.OMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (CollectionUtils.isEmpty(sendingHeiId) || Objects.isNull(omobilityId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (sendingHeiId.size() > 1) {
        throw new EwpWebApplicationException("Hei_id parameter is not repeteable.", HttpStatus.BAD_REQUEST.value());
      }
      utils.isHeisCoveredByTheHost(sendingHeiId.get(0));
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final OmobilitiesGetResponse respuesta = new OmobilitiesGetResponse();
    try {
      final List<StudentMobilityForStudies> omobilities =
              omobilityService.findStudentOmobilityByIdAndSendingInstitutionIdAndLastRevision(sendingHeiId.get(0),
                      utils.heisCovered(request), omobilityId);
      respuesta.getSingleMobilityObject().addAll(omobilities);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /get Omobilities");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @GetMapping(path = "/index")
  public ResponseEntity<OmobilitiesIndexResponse> getIndex(
          @RequestParam(value = "sending_hei_id", required = false) final List<String> sendingHeiId,
          @RequestParam(value = "receiving_hei_id", required = false) final List<String> receivingHeiId,
          @RequestParam(value = "receiving_academic_year_id", required = false) final List<String> receivingAcademicYearId,
          @RequestParam(value = "modified_since", required = false) final List<String> modifiedSince,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /index Outgoing mobilities");

    final EventMDTO event = utils.initializeAuditableEvent(receivingHeiId, emapParam, request,
            EwpEventConstants.OMOBILITYINDEX_GET, NotificationTypes.OMOBILITY, null);

    String modifiedSinceString = null;
    try {
      doParamsValidationIndex(sendingHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      dtf.parse(modifiedSinceString);
    }

    final List<String> receivingHeiIdToFilter = filterByPerssions(receivingHeiId, request);
    if (CollectionUtils.isEmpty(receivingHeiIdToFilter)) {
      return ResponseEntity.ok(new OmobilitiesIndexResponse());
    }

    final List<String> mobilitiesIds = omobilityService.getIdsByCriteria(sendingHeiId.get(0), receivingHeiIdToFilter,
            CollectionUtils.isNotEmpty(receivingAcademicYearId) ? receivingAcademicYearId.get(0) : null,
            modifiedSinceString);

    final OmobilitiesIndexResponse respuesta = new OmobilitiesIndexResponse();
    respuesta.getOmobilityId().addAll(mobilitiesIds);

    if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("Fin invocacion GET /index Outgoing mobilities");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/index")
  public ResponseEntity<OmobilitiesIndexResponse> postIndex(@RequestBody final MultiValueMap<String, String> paramMap,
                                                            final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /index Outgoing mobilities");


    String modifiedSinceString = null;
    final List<String> sendingHeiId = paramMap.get("sending_hei_id");
    final List<String> receivingHeiId = paramMap.get("receiving_hei_id");
    final List<String> receivingAcademicYearId = paramMap.get("receiving_academic_year_id");
    final List<String> modifiedSince = paramMap.get("modified_since");

    final EventMDTO event = utils.initializeAuditableEvent(receivingHeiId, paramMap, request,
            EwpEventConstants.OMOBILITYINDEX_POST, NotificationTypes.OMOBILITY, null);

    try {
      doParamsValidationIndex(sendingHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      dtf.parse(modifiedSinceString);
    }

    final List<String> receivingHeiIdToFilter = filterByPerssions(receivingHeiId, request);
    if (CollectionUtils.isEmpty(receivingHeiIdToFilter)) {
      return ResponseEntity.ok(new OmobilitiesIndexResponse());
    }

    final List<String> laIdsList = omobilityService.getIdsByCriteria(sendingHeiId.get(0), receivingHeiIdToFilter,
            CollectionUtils.isNotEmpty(receivingAcademicYearId) ? receivingAcademicYearId.get(0) : null,
            modifiedSinceString);

    final OmobilitiesIndexResponse respuesta = new OmobilitiesIndexResponse();
    respuesta.getOmobilityId().addAll(laIdsList);

    if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("FIN invocacion POST /index Outgoing mobilities");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity<OmobilityCnrResponse> postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                                      final HttpServletRequest request) {

    final String sendingHeiId = cnrlist.toSingleValueMap().get("sending_hei_id");
    final List<String> omobilityId = cnrlist.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(sendingHeiId, cnrlist, request, EwpEventConstants.OMOBILITYCNR_POST,
                    NotificationTypes.OMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    if (CollectionUtils.isEmpty(omobilityId) || Strings.isNullOrEmpty(sendingHeiId)) {
      final EwpWebApplicationException ex =
              new EwpWebApplicationException("Missing argumanets for notification.", HttpStatus.BAD_REQUEST.value());
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    for (final String id : omobilityId) {
      notificationService.saveNotificationOmobilityRefresh(sendingHeiId, id, globalProperties.getInstance());
    }

    eventService.save(event);
    return ResponseEntity.ok(new OmobilityCnrResponse());
  }

  private void doParamsValidationIndex(final List<String> sendingHeiId, final List<String> receivingAcademicYearId,
                                       final List<String> modifiedSince) throws EwpWebApplicationException {
    if (Objects.isNull(sendingHeiId)) {
      throw new EwpWebApplicationException("Sending_Hei_id is required", HttpStatus.BAD_REQUEST.value());
    }
    if (sendingHeiId.size() > 1) {
      throw new EwpWebApplicationException("Sending_Hei_id repeated.", HttpStatus.BAD_REQUEST.value());
    }
    utils.isHeisCoveredByTheHost(sendingHeiId.get(0));
    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      if (modifiedSince.size() > 1) {
        throw new EwpWebApplicationException("Multiple modified_since parameters.", HttpStatus.BAD_REQUEST.value());
      }
      if (!isValidFormatModifiedSince(modifiedSince.get(0))) {
        throw new EwpWebApplicationException("Wrong format of modified_since parameter.",
                HttpStatus.BAD_REQUEST.value());
      }
    }
    if (!CollectionUtils.isEmpty(receivingAcademicYearId)) {
      for (final String yearId : receivingAcademicYearId) {
        if (!utils.isValidFormatReceivingAcademicYear(yearId)) {
          throw new EwpWebApplicationException("Receiving_academic_year_id in incorrect format.",
                  HttpStatus.BAD_REQUEST.value());
        }
      }
    }
  }

  private boolean isValidFormatModifiedSince(final String modifiedSince) {
    return modifiedSince.matches(
            "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\\.[0-9]+)?|(24:00:00(\\.0+)?))(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?");
  }

  private List<String> filterByPerssions(final List<String> receivingHeiId, final HttpServletRequest request) {
    List<String> receivingHeiIdToFilter = new ArrayList<>();
    final List<String> heisCovered = utils.heisCovered(request);
    if (CollectionUtils.isNotEmpty(receivingHeiId)) {
      receivingHeiIdToFilter = heisCovered.stream().filter(receivingHeiId::contains).collect(Collectors.toList());
    } else {
      receivingHeiIdToFilter.addAll(heisCovered);
    }
    return receivingHeiIdToFilter;
  }
}
