package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.imobilities.cnr.ImobilityCnrResponse;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.ImobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * The type Imobility controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/incomingmobilities", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class ImobilityController {

  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private OmobilityLasService omobilityService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.omobility.ids}")
  private int maxOmobilityIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<ImobilitiesGetResponse> getGet(
          @RequestParam(value = "receiving_hei_id", required = false) final List<String> receivingHeiId,
          @RequestParam(value = "omobility_id", required = false) final List<String> omobilityId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /get Imobilities");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.IMOBILITYGET_GET,
            NotificationTypes.IMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (CollectionUtils.isEmpty(receivingHeiId) || Objects.isNull(omobilityId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (receivingHeiId.size() > 1) {
        throw new EwpWebApplicationException("Hei_id parameter is not repeteable.", HttpStatus.BAD_REQUEST.value());
      }
      utils.isHeisCoveredByTheHost(receivingHeiId.get(0));
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final ImobilitiesGetResponse respuesta = new ImobilitiesGetResponse();
    try {
      final List<StudentMobilityForStudies> omobilities =
              omobilityService.findStudentImobilityByIdAndReceivingInstitutionId(receivingHeiId.get(0),
                      utils.heisCovered(request), omobilityId);
      respuesta.getSingleIncomingMobilityObject().addAll(omobilities);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion GET /get Imobilities");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<ImobilitiesGetResponse> postGet(@RequestBody final MultiValueMap<String, String> laGetList,
                                                        final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get Imobilities");
    final List<String> receivingHeiId = laGetList.get("receiving_hei_id");
    final List<String> omobilityId = laGetList.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(null, laGetList, request, EwpEventConstants.IMOBILITYGET_POST,
                    NotificationTypes.IMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      if (CollectionUtils.isEmpty(receivingHeiId) || Objects.isNull(omobilityId)) {
        throw new EwpWebApplicationException("Receiving_Hei_id and omobility_id are required",
                HttpStatus.BAD_REQUEST.value());
      }
      if (receivingHeiId.size() > 1) {
        throw new EwpWebApplicationException("Hei_id parameter is not repeteable.", HttpStatus.BAD_REQUEST.value());
      }
      utils.isHeisCoveredByTheHost(receivingHeiId.get(0));
      if (omobilityId.size() > maxOmobilityIds) {
        throw new EwpWebApplicationException("Max number of Imobility id's has exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final ImobilitiesGetResponse respuesta = new ImobilitiesGetResponse();
    try {
      final List<StudentMobilityForStudies> omobilities =
              omobilityService.findStudentImobilityByIdAndReceivingInstitutionId(receivingHeiId.get(0),
                      utils.heisCovered(request), omobilityId);
      respuesta.getSingleIncomingMobilityObject().addAll(omobilities);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /get Imobilities");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity<ImobilityCnrResponse> postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                                      final HttpServletRequest request) {

    final String receivingHeiId = cnrlist.toSingleValueMap().get("receiving_hei_id");
    final List<String> omobilityId = cnrlist.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(receivingHeiId, cnrlist, request, EwpEventConstants.IMOBILITYCNR_POST,
                    NotificationTypes.IMOBILITY, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);


    if (CollectionUtils.isEmpty(omobilityId) || Strings.isNullOrEmpty(receivingHeiId)) {
      final EwpWebApplicationException ex =
              new EwpWebApplicationException("Missing arguments for notification.", HttpStatus.BAD_REQUEST.value());
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.IMOBILITYAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    for (final String id : omobilityId) {
      notificationService.saveNotificationImobilityRefresh(receivingHeiId, id, globalProperties.getInstance());
    }

    eventService.save(event);
    return ResponseEntity.ok(new ImobilityCnrResponse());
  }

}
