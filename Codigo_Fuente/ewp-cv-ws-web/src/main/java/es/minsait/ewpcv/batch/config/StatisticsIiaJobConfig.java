package es.minsait.ewpcv.batch.config;

import es.minsait.ewpcv.batch.StatisticsIiaJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type StatisticsIia job config.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Configuration
public class StatisticsIiaJobConfig {

    @Bean
    public JobDetailFactoryBean jobDetailStatisticsIia() {
        final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(StatisticsIiaJob.class);
        jobDetailFactory.setDescription("Batch para refresco de StatisticsIia");
        jobDetailFactory.setDurability(true);
        return jobDetailFactory;
    }

    @Bean
    public CronTriggerFactoryBean triggerStatisticsIia(@Qualifier("jobDetailStatisticsIia") final JobDetail jobDetail,
                                                       @Value("${ewp.StatisticsIia.refresh}") final String cron) {
        final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
        triggerFactory.setCronExpression(cron);
        triggerFactory.setJobDetail(jobDetail);
        return triggerFactory;
    }


}
