package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IInstitutionService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * The type Institutions controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/institutions", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class InstitutionsController {

  @Autowired
  private IInstitutionService institutionService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;
  @Autowired
  private Utils utils;

  @Value("${max.institutions.ids}")
  private int maxIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<InstitutionsResponse> getGet(
          @RequestParam(value = "hei_id", required = true) final List<String> heisIds,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {

    log.debug("Inicio invocacion GET /get Institutions");

    final ResponseEntity<InstitutionsResponse> respuesta =
            getResponse(heisIds, EwpEventConstants.INSTITUTIONSGET_GET, emapParam, request);

    log.debug("Fin invocacion GET Institutions");
    return respuesta;
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<InstitutionsResponse> postGet(@RequestBody final MultiValueMap<String, String> params,
                                                      final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get Institutions");

    // Extraemos los parámetros del form
    final List<String> heisIds = params.get("hei_id");

    final ResponseEntity<InstitutionsResponse> respuesta =
            getResponse(heisIds, EwpEventConstants.INSTITUTIONSGET_POST, params, request);

    log.debug("Fin invocacion POST Institutions");
    return respuesta;
  }

  private ResponseEntity<InstitutionsResponse> getResponse(final List<String> heisIds, final String eventType,
                                                           final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, eventType,
            NotificationTypes.INSTITUTIONS, Objects.nonNull(heisIds) ? heisIds.toString() : null);

    try {
      if (CollectionUtils.isEmpty(heisIds)) {
        throw new EwpWebApplicationException("hei_id is required.", HttpStatus.BAD_REQUEST.value());
      } else if (heisIds.size() > maxIds) {
        throw new EwpWebApplicationException("Max number of HEI id's has been exceeded.",
                HttpStatus.BAD_REQUEST.value());
      }

    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.INSTITUTIONSAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final List<InstitutionsResponse.Hei> responseHeis = institutionService.obtenerInstitutionsInformation(heisIds);

    final InstitutionsResponse respuesta = new InstitutionsResponse();
    if (responseHeis != null) {
      respuesta.getHei().addAll(responseHeis);
    }

    if (utils.getValue(EwpConstants.INSTITUTIONSAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    return ResponseEntity.ok(respuesta);
  }

}
