package es.minsait.ewpcv.controller;

import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.Utils;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IFactsheetService;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * The type Factsheet controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/factsheet", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class FactsheetController {

  @Autowired
  private IFactsheetService factsheetService;
  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;
  @Autowired
  private Utils utils;


  @Value("${max.hei.ids}")
  private int maxHeisIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<FactsheetResponse> getGet(@RequestParam("hei_id") final List<String> heisIds,
                                                  @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request)
          throws IOException {
    log.debug("Inicio invocacion GET /get Facsheet");

    final EventMDTO event = utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.FACTSHEETGET_GET,
            NotificationTypes.FACTSHEET, Objects.nonNull(heisIds) ? heisIds.toString() : null);

    try {
      if (heisIds.size() > maxHeisIds) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    try {
      final FactsheetResponse respuesta = new FactsheetResponse();
      respuesta.getFactsheet().addAll(factsheetService.obtenerFactsheets(heisIds));

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);

      log.debug("Fin invocacion GET / get factsheet");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<FactsheetResponse> postGet(@RequestBody final MultiValueMap<String, String> heiGetList,
                                                   final HttpServletRequest request) throws IOException {
    log.debug("Inicio invocacion POST /get factsheet");
    // Extraemos los parámetros del form
    final List<String> heisIds = heiGetList.get("hei_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(null, heiGetList, request, EwpEventConstants.FACTSHEETGET_POST,
                    NotificationTypes.FACTSHEET, Objects.nonNull(heisIds) ? heisIds.toString() : null);

    try {
      if (CollectionUtils.isEmpty(heisIds)) {
        throw new EwpWebApplicationException("Incorrect parameter.", HttpStatus.BAD_REQUEST.value());
      }
      if (heisIds.size() > maxHeisIds) {
        throw new EwpWebApplicationException("Max number of IIA id's has exceeded.", HttpStatus.BAD_REQUEST.value());
      }
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    try {
      final FactsheetResponse respuesta = new FactsheetResponse();
      respuesta.getFactsheet().addAll(factsheetService.obtenerFactsheets(heisIds));

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);

      log.debug("Fin invocacion GET / get factsheet");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.FACTSHEETAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);

      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

}
