package es.minsait.ewpcv.batch;


import java.util.*;

import es.minsait.ewpcv.error.EwpConverterException;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Omobility job.
 *
 * Se encarga de procesar los cnr entrantes de tipo omobilities, es decir, las nominaciones de movilidades que recibimos nosotros y que debemos aprobar o rechazar
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class OmobilityJob implements Job {

  @Autowired
  private INotificationService notificationService;

  @Value("${ewp.omobilities.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.iia.cnr.notification.block:50}")
  private int blockSize;

  @Autowired
  private IEventService eventService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private OmobilityLasService mobilityService;

  @Autowired
  private Utils utils;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.outgoingmobility:true}")
  private boolean cnrOmobility;


  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrOmobility) {
      log.info("***No se ejecuta el proceso de refresco de Outgoing Mobilities a partir de CNR por configuracion");
      return;
    }

    log.info("***Comienza el proceso de refresco de Outgoing Mobilities a partir de CNR");

    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.OMOBILITY);

    log.info("Se van a actualizar " + list.size() + " Outgoing Mobilities");

    for (final NotificationMDTO notification : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.OMOBILITY, notification.getOwnerHeiId(),
          notification.getHeiId(), notification.getChangedElementIds(), null);

      try {
        if(actualizarMobility(notification, event)){
          // si no se produce un error eliminamos la notificacion
          notificationService.eliminarNotificacion(notification);
        }

      } catch (final Exception e) {
        final String error = Objects.nonNull(notification)
                ? "Error al procesar Omobility CNR, id mobility: " + notification.getChangedElementIds()
                : "Error al procesar Omobility CNR";
        log.error(error, getClass().getName(), e);
        notificationService.liberarNotificaciones(notification, e.getMessage());
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }
      eventService.save(event);
    }
    log.info("Termina proceso de refresco de Outgoing Mobilities");

  }

  private boolean actualizarMobility(final NotificationMDTO notification, final EventMDTO event)
          throws EwpOperationNotFoundException,EwpConverterException {

    try {
      final ClientResponse response = invocarOmobilityGet(notification, event);

      if (!utils.verifyResponse(notification, event, response, EwpEventConstants.CNR_PROCESSING_GET)) {
        return false;
      }

      final OmobilitiesGetResponse omobilitiesGetResponse = (OmobilitiesGetResponse) response.getResult();

      if (Objects.nonNull(omobilitiesGetResponse)
          && CollectionUtils.isNotEmpty(omobilitiesGetResponse.getSingleMobilityObject())) {
        // actualizamos la mobilidad
        final StudentMobilityForStudies mobilityForStudies = omobilitiesGetResponse.getSingleMobilityObject().get(0);

        final MobilityMDTO local = mobilityService.findLastMobilityRevisionByIdAndSendingHeiId(notification.getChangedElementIds(), notification.getHeiId());
        if (Objects.isNull(local)) {
          final boolean actualizado =
              mobilityService.guardarOmobilityCopiaRemota(mobilityForStudies, globalProperties.getInstance());
          event.setEventType(actualizado ? EwpEventConstants.INSERT : EwpEventConstants.NO_ACTION);
        } else {
          final boolean actualizado =
              mobilityService.actualizarOmobility(mobilityForStudies, local, globalProperties.getInstance());
          event.setEventType(actualizado ? EwpEventConstants.UPDATE : EwpEventConstants.NO_ACTION);
        }
      } else {
        event.setEventType(EwpEventConstants.NO_ACTION);
      }
    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Omobilities", getClass().getName(), e);
      throw e;
    }
    return true;
  }

  private ClientResponse invocarOmobilityGet(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getOmobilitiesHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_GET_URL, params, HttpMethodEnum.POST,
            EwpConstants.IMOBILITIES_API_NAME);
    return client.sendRequest(requestIndex, OmobilitiesGetResponse.class, event);
  }
}
