package es.minsait.ewpcv.batch;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.common.Utils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;

import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.EwpEventConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.common.RegistryClient;
import es.minsait.ewpcv.error.EwpIllegalStateException;
import es.minsait.ewpcv.error.EwpNotFoundException;
import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestType;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IMobilityUpdateRequestService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import es.minsait.ewpcv.service.api.INotificationService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Outgoing las update request job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class OutgoingLasUpdateRequestJob implements Job {

  @Autowired
  private IMobilityUpdateRequestService mobilityUpdateRequestService;

  @Autowired
  private OmobilityLasService mobilityService;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private RestClient client;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private INotificationService notificationService;

  @Value("${ewp.la.update.block:50}")
  private int blockSize;

  @Value("${ewp.la.update.max.retries:1}")
  private int maxRetries;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.outgoingmobilityla:true}")
  private boolean cnrLeanringAgreement;

  @Autowired
  private Utils utils;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrLeanringAgreement) {
      log.info("***No se ejecuta el proceso de actualizacion de solicitudes de actualizacion de LAs por configuracion");
      return;
    }

    final List<String> idsReintentar = new ArrayList<>();
    log.info("***Comienza el proceso de actualizacion de solicitudes de actualizacion de LAs");
    final List<MobilityUpdateRequestMDTO> mobilityUpdateRequestList = mobilityUpdateRequestService
            .reservarMobilityUpdateRequest(UpdateRequestMobilityTypeEnum.OUTGOING, maxRetries, blockSize);
    log.info("Se van a procesar " + mobilityUpdateRequestList.size()
            + " solicitudes de actualizacion de actualizacion de LAs");
    for (final MobilityUpdateRequestMDTO req : mobilityUpdateRequestList) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.LA, globalProperties.getInstance(),
          req.getSendingHeiId(), null, null);
      try {
        final OmobilityLasUpdateRequest updateRequest = mobilityUpdateRequestService
                .bytesToClassImplByClass(req.getUpdateInformation(), OmobilityLasUpdateRequest.class);
        final String type =
                MobilityUpdateRequestType.APPROVE_PROPOSAL_V1.equals(req.getType()) ? EwpEventConstants.ACCEPT
                        : EwpEventConstants.REJECT;
        final String mobilityId =
                EwpEventConstants.ACCEPT.equals(type) ? updateRequest.getApproveProposalV1().getOmobilityId()
                        : updateRequest.getCommentProposalV1().getOmobilityId();
        event.setEventType(type);
        event.setChangedElementIds(mobilityId);
        event.setObservations(
                EwpEventConstants.ACCEPT.equals(type) ? null : updateRequest.getCommentProposalV1().getComment());
        event.setMessage(req.getUpdateInformation());

        mobilityUpdateRequestService.updateRequestOutgoing(updateRequest, req.getType());
        mobilityUpdateRequestService.deleteMobillityUpdateRequest(req.getId());


        // obtenemos el identificador de la institucion receptora
        final String receiverHei = mobilityService.findLastMobilityRevisionReceiverHeiByIdAndSendingHei(mobilityId, globalProperties.getInstance());

        // si estamos procesando una aceptacion creamos una notificacion para que nos puedan consultar
        if (EwpEventConstants.ACCEPT.equals(type)) {
          notificationService.saveNotificationOmobilityLA(req.getSendingHeiId(), mobilityId, receiverHei);
        }

        event.setTriggeringHei(Objects.nonNull(receiverHei) ? receiverHei : req.getSendingHeiId());
      } catch (EwpIllegalStateException | EwpNotFoundException e) {
        event.setObservations(e.getMessage());
        event.setStatus(EwpEventConstants.KO);
        mobilityUpdateRequestService.deleteMobillityUpdateRequest(req.getId());
      } catch (final Exception e) {
        final String error = "Error al procesar el UPDATE LA. ID actualizacion: " + req.getId();
        log.error(error, e);
        idsReintentar.add(req.getId());
        if (Objects.isNull(event.getEventType())) {
          event.setEventType(EwpEventConstants.UPDATE_LA_PROCESSING);
        }
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }
      eventService.save(event);
    }

    mobilityUpdateRequestService.changeStatusIsNotProcessingAndRetriesUpdateRequest(idsReintentar);
  }

}