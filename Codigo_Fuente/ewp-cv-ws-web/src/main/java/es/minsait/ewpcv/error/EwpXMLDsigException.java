package es.minsait.ewpcv.error;

/**
 * The type Ewp xml dsig exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpXMLDsigException extends Exception {
  private static final long serialVersionUID = 5884078183631161517L;

  public EwpXMLDsigException(final String message) {
    super(message);
  }
}
