package es.minsait.ewpcv.client;

import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;

import com.google.common.base.Charsets;
import es.minsait.ewpcv.error.XsdValidationException;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import com.google.common.base.Strings;

import es.minsait.ewpcv.common.EwpConstants;
import es.minsait.ewpcv.common.EwpKeyStore;
import es.minsait.ewpcv.common.EwpRequestConstants;
import es.minsait.ewpcv.common.GlobalProperties;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.security.HttpSignature;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import es.minsait.ewpcv.common.*;
import org.xml.sax.SAXException;

/**
 * The type Rest client.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
public class RestClient {

  @Autowired
  GlobalProperties properties;

  @Autowired
  EwpKeyStore keystoreController;

  @Autowired
  HttpSignature httpSignature;

  @Autowired
  RegistryClient registryClient;

  @Value("${ewp.request.timeout.read:3000}")
  int readTimeout;
  @Value("${ewp.request.timeout.connect:3000}")
  int connectTimeout;
  @Value("${ewp.request.timeout.connectionRequest:3000}")
  int connectionRequestTimeout;

  @Autowired
  MonitoringComponent monitoringComponent;

  private static SSLContext initSecurityContext(final KeyStore keyStore, final KeyStore trustStore, final String pwd)
      throws NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException, UnrecoverableKeyException,
      KeyManagementException {
    final KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    kmf.init(keyStore, pwd.toCharArray());
    final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    tmf.init(trustStore);

    final SSLContext context = SSLContext.getInstance("TLS");
    context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    return context;
  }

  private RestTemplate createClient(ClientRequest clientRequest) {
    try {
      //Establecemos las configuraciones de comunicación, dada la especificacion no podemos delegar la compresión al cliente
      RequestConfig config = RequestConfig.custom()
              .setConnectTimeout(connectTimeout)
              .setConnectionRequestTimeout(connectionRequestTimeout)
              .setSocketTimeout(readTimeout)
              .setContentCompressionEnabled(false)
              .build();

      HttpClientBuilder httpClientBuilder  = HttpClientBuilder
              .create()
              .setDefaultRequestConfig(config);

      // configuracion de contexto ssl
      if (keystoreController.isSuccessfullyInitiated()) {
        final SSLContext sslContext =
            initSecurityContext(clientRequest.isTlssec() ? keystoreController.getKeystore() : null,
            keystoreController.getTruststore(), properties.getKeystorePassword());
        final SSLConnectionSocketFactory socketFactory =
            new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        httpClientBuilder.setSSLSocketFactory(socketFactory).setRedirectStrategy(new LaxRedirectStrategy());
      }

      // configuracion de proxy si se ha definido
      if (!Strings.isNullOrEmpty(properties.getProxy())) {
        httpClientBuilder.setProxy(new HttpHost(properties.getProxy(), properties.getProxyPort()));
      }

      HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build());


      RestTemplate restTemplate = new RestTemplate(requestFactory);
      StringHttpMessageConverter converter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
      converter.setWriteAcceptCharset(false);
      restTemplate.getMessageConverters().add(0, converter);
      return restTemplate;

    } catch (NoSuchAlgorithmException | KeyStoreException | NoSuchProviderException | UnrecoverableKeyException
        | KeyManagementException ex) {
      log.error("Cant't create HTTP client.", ex);
    }
    return null;
  }

  public ClientResponse sendRequest(final ClientRequest clientRequest, final Class responseClass,
                                    final EventMDTO event) {
    RestTemplate restTemplate = createClient(clientRequest);
    final ClientResponse clientResponse = new ClientResponse();
    final String requestID = UUID.randomUUID().toString();
    final Map<String, List<String>> monitoringParams = new HashMap<>();

    try {
      URI uri = new URL(clientRequest.getUrl()).toURI();
      final Instant start = Instant.now();
      final ResponseEntity<byte[]> response = invoke(clientRequest, restTemplate, requestID, event);
      processResponse(clientRequest, responseClass, clientResponse, requestID, response, start, monitoringParams, uri);
    } catch (HttpClientErrorException | HttpServerErrorException e) {
      handleNetworkError(clientRequest, clientResponse, e, monitoringParams);
    }catch (MalformedURLException e){
      handleMalformedUrl(clientRequest, clientResponse, e, monitoringParams);
    }catch (final JAXBException | SAXException e) {
      log.error("La respuesta recibida no cumple con el xsd especificado", e);
      clientResponse.setXsdComplies(false);
      handleInvalidResponse(clientRequest, clientResponse, monitoringParams, null);
    } catch (final XsdValidationException e) {
      log.error("La respuesta recibida no cumple con el xsd especificado", e);
      clientResponse.setXsdComplies(false);
      handleInvalidResponse(clientRequest, clientResponse, monitoringParams, e.getMessage());
    }catch (final Exception e) {
      handleNetworkError(clientRequest, clientResponse, e, monitoringParams);
    }

    log.debug("********* Respuesta de servicio a cliente *********");
    log.debug("   ****** STATUS: " + clientResponse.getStatusCode());
    log.debug("   ****** ERROR: " + clientResponse.getErrorMessage());
    log.debug("   ****** HEADERS: " + clientResponse.getHeaders());
    log.debug("   ****** BODY " + clientResponse.getRawResponse());
    log.debug("   ****** DECOMPRESSED BODY " + clientResponse.getDecompressedBody());

    if (!monitoringParams.isEmpty()) {
      monitoringComponent.invokeMonitoring(monitoringParams);
    }
    return clientResponse;
  }

  /**
   * realiza la invocacion al servicio a partir de la informacion de la peticion recibida como parametro
   *
   * @param clientRequest
   * @param restTemplate
   * @param requestID
   * @return
   * @throws URISyntaxException
   * @throws MalformedURLException
   */
  private ResponseEntity<byte[]> invoke(ClientRequest clientRequest, RestTemplate restTemplate, String requestID, EventMDTO event)
          throws URISyntaxException, MalformedURLException {
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(clientRequest.getUrl());
    URI uri = new URL(clientRequest.getUrl()).toURI();
    final ResponseEntity<byte[]> response;
    final Map<String, List<String>> params = clientRequest.getParams();

    final HttpHeaders headers = new HttpHeaders();
    log.debug("********* Invocacion de servicio mediante cliente *********");
    switch (clientRequest.getMethod()) {
      case POST:
        final HttpEntity<String> postEntity;
        if (Objects.nonNull(clientRequest.getXmlString())) {

          headers.setContentType(MediaType.TEXT_XML);
          if (clientRequest.isHttpsecClient()) {
            httpSignature.signRequest(HttpMethod.POST.name(), uri, headers, clientRequest.getXmlString(), requestID);
          }

          postEntity = new HttpEntity<>(clientRequest.getXmlString(), headers);
        } else {
          final String paramsData = paramsData2String(params);
          headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, StandardCharsets.UTF_8));
          if (clientRequest.isHttpsecClient()) {
            httpSignature.signRequest(HttpMethod.POST.name(), uri, headers, paramsData, requestID);
          }

          postEntity = new HttpEntity<>(paramsData, headers);
        }

        setEventRequestBody(event, postEntity, clientRequest);

        log.debug("   ****** METODO: " + HttpMethod.POST);
        log.debug("   ****** URI: " + uri.toString());
        log.debug("   ****** ENTIDAD " + postEntity.toString());
        response = restTemplate.exchange(uri, HttpMethod.POST, postEntity, byte[].class);

        break;
      case PUT:
        uri = uriBuilder.build().toUri();
        log.debug("   ****** PUT: " + HttpMethod.PUT);
        log.debug("   ****** URI: " + uri.toString());
        headers.add("accept-signature", "rsa-sha256");
        final HttpEntity<String> entity = new HttpEntity<>(headers);
        setEventRequestBody(event, entity, clientRequest);response = restTemplate.exchange(uri, HttpMethod.PUT, entity, byte[].class);
        break;
      default:
        if (Objects.nonNull(clientRequest.getHeiId())) {
          params.put("hei_id", Arrays.asList(clientRequest.getHeiId()));
        }
        params.entrySet().forEach(e -> uriBuilder.queryParam(e.getKey(),
                e.getValue().stream().map(Object::toString).collect(Collectors.joining(","))));
        uri = uriBuilder.build().toUri();

        if (clientRequest.isHttpsecClient()) {
          httpSignature.signRequest("get", uri, headers, requestID);
        }
        final HttpEntity<String> getEntity = new HttpEntity<>(headers);
        setEventRequestBody(event, getEntity, clientRequest);log.debug("   ****** METODO: " + HttpMethod.GET);
        log.debug("   ****** URI: " + uri.toString());
        log.debug("   ****** ENTIDAD " + getEntity.toString());
        response = restTemplate.exchange(uri, HttpMethod.GET, getEntity, byte[].class);
        break;
    }
    return response;
  }

  /**
   * Procesa la respuesta del servicio almacenando la informacion relevante en el objeto ClientResponse
   *
   * @param clientRequest
   * @param responseClass
   * @param clientResponse
   * @param requestID
   * @param response
   * @param start
   * @throws JAXBException
   */
  private void processResponse(ClientRequest clientRequest, Class responseClass, ClientResponse clientResponse,
                               String requestID, ResponseEntity<byte[]> response, Instant start, Map<String, List<String>> monitoringParams, URI uri)
          throws Exception {
    clientResponse.setDuration(ChronoUnit.MILLIS.between(start, Instant.now()));

    clientResponse.setStatusCode(response.getStatusCodeValue());
    clientResponse.setMediaType(response.getHeaders().getFirst("Content-Type"));

    clientResponse.setHeaders(response.getHeaders().entrySet().stream()
            .map(es -> es.getKey() + ": " + es.getValue().stream().map(Object::toString).collect(Collectors.joining(", ")))
            .collect(Collectors.toList()));

    String rawResponse = "";
    String charsetName = getCharsetname(response);
    if (response.getStatusCodeValue() == HttpServletResponse.SC_OK && Objects.nonNull(response.getBody())) {
      rawResponse = new String(response.getBody(), charsetName);
      clientResponse.setRawResponse(rawResponse);
      //si el contenido viene comprimido en gzip lo descomprimimos
      if(response.getHeaders().entrySet().stream().anyMatch((e) -> e.getKey().equalsIgnoreCase("Content-Encoding") && e.getValue().contains("gzip")) && GzipCompressor.isCompressed(response.getBody())){
        clientResponse.setDecompressedBody(GzipCompressor.decompress(response.getBody(), charsetName));
        clientResponse.setResult(getAsObject(clientResponse.getDecompressedBody(), responseClass, clientRequest.getApiName()));
      }
      else {
        clientResponse.setResult(getAsObject(rawResponse, responseClass, clientRequest.getApiName()));
      }
    } else if (response.hasBody()) {
      rawResponse = new String(response.getBody(), charsetName);;
      clientResponse.setRawResponse(rawResponse);
      String developperMessage = getDevelopperMessage(clientResponse, rawResponse);
      // preparamos la llamada a monitorizacion del error recibido.
      monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.SERVER_ERROR,
              response.getStatusCodeValue(), developperMessage, null, false);
    }

    if (properties.isVerifyResponses() && clientRequest.isHttpsecServer()) {
      final String errorMessage = httpSignature.verifyHttpSignatureResponse(clientRequest.getMethod().name(),
              uri, response.getHeaders(), response.getBody(), requestID);
      if (errorMessage != null) {
        clientResponse.setHttpsecMsg(errorMessage);
        // preparamos la llamada a monitorizacion del error recibido.
        monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.INVALID_RESPONSE,
                response.getStatusCodeValue(), null, errorMessage, false);
      } else {
        clientResponse.setHttpsecMsg(EwpRequestConstants.RESPONSE_VERIFIED);
      }
    }
  }

  private String getCharsetname(ResponseEntity<byte[]> response) {
    String charsetName = response.getHeaders().get("Content-Type").stream().filter((e) -> e.contains("charset=")).findFirst().orElse("charset=UTF-8");
    //debemos eliminar el charset= para que funcione el descompresor y plantemaos tambien el caso de que venga mas informacion tras ese charset por lo que nos quedamos con lo que hay despues del = y antes del ;
    int startPosition = charsetName.indexOf("charset=") + 8;
    int endPosition = charsetName.indexOf(";", startPosition) == -1 ? charsetName.length() : charsetName.indexOf(";", startPosition);
    charsetName = charsetName.substring(startPosition, endPosition).trim();
    return charsetName;
  }

  /**
   * manejo de excepciones de red durante la invocacion de la API se realiza una invocacion al api de monitorizacion
   * para notificar el error
   *
   * @param clientRequest
   * @param clientResponse
   * @param e
   */
  private void handleNetworkError(ClientRequest clientRequest, ClientResponse clientResponse, HttpStatusCodeException e,
                                  Map<String, List<String>> monitoringParams) {
    String developperMessage = getDevelopperMessage(clientResponse, e.getResponseBodyAsString());
    // preparamos la llamada a monitorizacion del error recibido.
    monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.SERVER_ERROR, e.getRawStatusCode(),
            Objects.nonNull(developperMessage) ? developperMessage : e.getMessage(), null, false);

    // continuamos con la ejecucion habitual
    clientResponse.setStatusCode(e.getRawStatusCode());
    clientResponse.setRawResponse(e.getResponseBodyAsString());
    clientResponse.setHeaders(e.getResponseHeaders().entrySet().stream()
            .map(es -> es.getKey() + ": " + es.getValue().stream().map(Object::toString).collect(Collectors.joining(", ")))
            .collect(Collectors.toList()));
  }

  /**
   * manejo de excepciones de red durante la invocacion de la API se realiza una invocacion al api de monitorizacion
   * para notificar el error
   *
   * @param clientRequest
   * @param clientResponse
   * @param e
   */
  private void handleNetworkError(ClientRequest clientRequest, ClientResponse clientResponse, Exception e,
                                  Map<String, List<String>> monitoringParams) {
    // preparamos la llamada a monitorizacion del error recibido.
    boolean isSslException = e instanceof SSLException || (e.getCause() != null && e.getCause() instanceof SSLException);
    monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.NETWORK_ERROR, null, null, e.getMessage(), isSslException);

    // continuamos con la ejecucion habitual
    clientResponse.setStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    clientResponse.setErrorMessage(e.getMessage());
    clientResponse.setRawResponse(e.getMessage());
  }

  /**
   * manejo de excepciones de red durante la invocacion de la API se realiza una invocacion al api de monitorizacion
   * para notificar el error
   * @param clientRequest
   * @param clientResponse
   * @param monitoringParams
   * @param errorMessage
   */
  private void handleInvalidResponse(ClientRequest clientRequest, ClientResponse clientResponse,
                                  Map<String, List<String>> monitoringParams, String errorMessage) {
    String developperMessage = Strings.isNullOrEmpty(errorMessage) ? "The received response does not match the expected format" : errorMessage;
    // preparamos la llamada a monitorizacion del error recibido.
    monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.INVALID_RESPONSE, clientResponse.getStatusCode(), null, developperMessage, false);

    // continuamos con la ejecucion habitual
    clientResponse.setStatusCode(HttpServletResponse.SC_NOT_ACCEPTABLE);
    clientResponse.setErrorMessage(developperMessage);
  }

  /**
   * manejo de excepciones de red al invocar al endpoint la url esta mal y se invoca al api de monitorizacion
   * para notificar el error
   *
   * @param clientRequest
   * @param clientResponse
   * @param e
   */
  private void handleMalformedUrl(ClientRequest clientRequest,  ClientResponse clientResponse, MalformedURLException e, Map<String, List<String>> monitoringParams) {

    // preparamos la llamada a monitorizacion del error recibido.
    monitoringComponent.prepareMonitoringParams(monitoringParams, clientRequest, ErrorTypeEnum.SERVER_ERROR, HttpStatus.NOT_FOUND.value(), null, e.getMessage(), false);

    // continuamos con la ejecucion habitual
    clientResponse.setStatusCode(HttpStatus.NOT_FOUND.value());
    clientResponse.setRawResponse(e.getMessage());
  }

  private void setEventRequestBody(EventMDTO event, HttpEntity<String> entity, ClientRequest clientRequest) {
    if (event != null) {

      event.setRequestParams(clientRequest.getParams() != null ? clientRequest.getParams().toString() : null);

      StringBuilder sb = new StringBuilder();

      sb.append("##### REQUEST HEADERS #####\n");
      entity.getHeaders().forEach((k, v) -> {
        sb.append(k).append(": ").append(v);
        sb.append("\n");

        if (k.equalsIgnoreCase(EwpConstants.X_REQUEST_ID)) {
          event.setXRequestId(v.get(0));
        }
      });
      sb.append("\n");
      sb.append("##### REQUEST BODY #####\n");
      sb.append(entity.getBody());
      event.setRequestBody(sb.toString().getBytes());
    }
  }

  protected String paramsData2String(final Map<String, List<String>> form) {
    final StringBuilder sb = new StringBuilder();

    try {
      for (final Map.Entry<String, List<String>> e : form.entrySet()) {
        for (final String value : e.getValue()) {
          if (sb.length() > 0) {
            sb.append('&');
          }
          sb.append(e.getKey());
          if (value != null) {
            sb.append('=');
            sb.append(URLEncoder.encode(value, Charsets.UTF_8.name()));
          }
        }
      }
    } catch (final Exception e) {
      log.error("failed to convert form", e);
    }

    return sb.toString();
  }


  private <T> T getAsObject(final String body, final Class responseClass, String apiName) throws JAXBException, SAXException, XsdValidationException{
    // hemos invocado al api de ficheros, no valida ni deserializa
    if(apiName.equals(EwpConstants.FILE_API_NAME)){
      return (T) body.getBytes();
    }

    final JAXBContext jaxbContext = JAXBContext.newInstance(responseClass);
    final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    SaxParserValidationEventHandler errorHandler = new SaxParserValidationEventHandler();
    if(properties.isValidateResponses()) {
      unmarshaller.setEventHandler(errorHandler);
      //añadimos validacion contra el schema indicado por el namespace de la respuesta
      final SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "file");
      sf.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "file");

      //Validamos contra el fichero ewp-connector-api.xsd que se encuentra en el classpath
      final URL schemaFile = this.getClass().getResource("/xsd/ewp-connector-api.xsd");

      final Schema schema = sf.newSchema(schemaFile);
      unmarshaller.setSchema(schema);
    }

    final JAXBElement<T> res =
              (JAXBElement<T>) unmarshaller.unmarshal(new StreamSource(new StringReader(body)), responseClass);

    if (!Strings.isNullOrEmpty(errorHandler.getErrorMessages())) {
      throw new XsdValidationException("The received response does not match the expected format: \n" + errorHandler.getErrorMessages());
    }
    return res.getValue();
  }

  private <T> T getAsObjectWithoutValidation(final String body, final Class responseClass) throws JAXBException, SAXException, XsdValidationException{
    final JAXBContext jaxbContext = JAXBContext.newInstance(responseClass);
    final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    SaxParserValidationEventHandler errorHandler = new SaxParserValidationEventHandler();

    final JAXBElement<T> res =
            (JAXBElement<T>) unmarshaller.unmarshal(new StreamSource(new StringReader(body)), responseClass);

    return res.getValue();
  }


  private String getDevelopperMessage(ClientResponse clientResponse, String rawResponse) {
    String developperMessage = null;
    try {
      final ErrorResponse error = getAsObjectWithoutValidation(rawResponse, ErrorResponse.class);
      developperMessage = error.getDeveloperMessage().getValue();
      clientResponse.setErrorMessage(developperMessage);
    } catch (final Exception e) {
      clientResponse.setErrorMessage(rawResponse);
    }
    return developperMessage;
  }
}
