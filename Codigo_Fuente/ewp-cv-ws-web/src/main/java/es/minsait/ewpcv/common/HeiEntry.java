package es.minsait.ewpcv.common;

import java.util.Map;

/**
 * The type Hei entry.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class HeiEntry {
  private String id;
  private String name;
  private Map<String, String> urls;
  private boolean clientTlsCert;
  private boolean serverTlsCert;
  private boolean clientHttpSignature;
  private boolean serverHttpSignature;

  public HeiEntry() {}

  public HeiEntry(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Map<String, String> getUrls() {
    return this.urls;
  }

  public void setUrls(Map<String, String> urls) {
    this.urls = urls;
  }

  public boolean isClientTlsCert() {
    return clientTlsCert;
  }

  public void setClientTlsCert(boolean clientTlsCert) {
    this.clientTlsCert = clientTlsCert;
  }

  public boolean isServerTlsCert() {
    return serverTlsCert;
  }

  public void setServerTlsCert(boolean serverTlsCert) {
    this.serverTlsCert = serverTlsCert;
  }

  public boolean isClientHttpSignature() {
    return clientHttpSignature;
  }

  public void setClientHttpSignature(boolean clientHttpSignature) {
    this.clientHttpSignature = clientHttpSignature;
  }

  public boolean isServerHttpSignature() {
    return serverHttpSignature;
  }

  public void setServerHttpSignature(boolean serverHttpSignature) {
    this.serverHttpSignature = serverHttpSignature;
  }

  @Override
  public String toString() {
    return "HeiEntry{" + "\nid='" + id + '\'' + ",\n name='" + name + '\'' + ",\n urls=" + urls + ",\n clientTlsCert="
        + clientTlsCert + ",\n serverTlsCert=" + serverTlsCert + ",\n clientHttpSignature=" + clientHttpSignature
        + ",\n serverHttpSignature=" + serverHttpSignature + '}';
  }
}
