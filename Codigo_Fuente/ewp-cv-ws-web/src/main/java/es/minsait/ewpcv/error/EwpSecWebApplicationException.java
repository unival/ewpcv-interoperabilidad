package es.minsait.ewpcv.error;

/**
 * The type Ewp sec web application exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpSecWebApplicationException extends Exception {
  int status;
  private AuthMethod authMethod;

  public EwpSecWebApplicationException(final String message, final int status) {
    this(message, status, AuthMethod.TLSCERT);
  }

  public EwpSecWebApplicationException(final String message, final int status, final AuthMethod authMethod) {
    super(message);
    this.status = status;
    this.authMethod = authMethod;
  }

  public AuthMethod getAuthMethod() {
    return authMethod;
  }

  public int getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return "EwpSecWebApplicationException{" +
            "status=" + status +
            ", authMethod=" + authMethod +
            ", message=" + getMessage() +
            '}';
  }

  /**
   * The enum Auth method.
   *
   * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
   */
  public static enum AuthMethod {
    TLSCERT, HTTPSIG
  }
}
