package es.minsait.ewpcv.security;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.tomitribe.auth.signatures.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Digests utils.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class DigestsUtils {

  private static final Logger logger = LoggerFactory.getLogger(DigestsUtils.class);

  /**
   * Obtiene una lista con todos los digests posibles si se trata de una invocacion post en formato urlEncoded o una
   * lista con un unico digest a partir del body si es un get
   * 
   * @param request
   * @return
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  public static List<String> getAllDigests(final HttpServletRequest request)
      throws IOException, NoSuchAlgorithmException {
    final List<String> digests;
    Charset charset = getCharset(request);
    final String contentType = request.getHeader("Content-Type");
    if (!Strings.isNullOrEmpty(contentType) && contentType.contains(MediaType.APPLICATION_FORM_URLENCODED_VALUE)) {
      digests = getAllFormDigests(request.getParameterMap(), request.getQueryString(), charset);
    }
    else {
      digests = Arrays.asList(calculateDigest(readBodyBytes(request, charset)));
    }
    return digests;
  }


  /**
   * Obtiene el charset a partir de la cabecera content-type
   * @param request peticion http recibida
   * @return charset recibido en la cabecera content-type o el charset por defecto si no viene informado
   */
  private static Charset getCharset(HttpServletRequest request) {
    String charsetName = Charsets.UTF_8.name(); // conjunto de caracteres predeterminado
    String contentType = request.getHeader(HttpHeaders.CONTENT_TYPE);
    // extraemos el charset del header si viene informado
    if (contentType != null && contentType.contains("charset=")) {
      String[] parts = contentType.split("charset=");
      if (parts.length > 1) {
        charsetName = parts[1].trim();
      }
    }
    // instanciamos el charset a partir de los leido o del default
    Charset charset = Charset.forName(charsetName);
    return charset;
  }

  /**
   * Lee el body empleando el charset recibido en la cabecera content type
   * Este metodo lee el mensaje recibido tal cual nos llega sin realizar ninguna modificación.
   * Ademas este metodo lee el body entero aunque acabe en salto de linea
   * 
   * @param request
   * @param charset
   * @return
   * @throws IOException
   */
  private static byte[] readBodyBytes(HttpServletRequest request, Charset charset) throws IOException {
    StringBuilder sb = new StringBuilder();
    BufferedReader reader = request.getReader();
    CharBuffer buffer = CharBuffer.allocate(4096);
    try {
      // lee un bloque de 4096 caracteres
      while (reader.read(buffer) != -1) {
        buffer.flip();
        // decodifica uno a uno el bloque de caracteres leido
        while (buffer.hasRemaining()) {
          char c = buffer.get();
          // concatena el caracter leido al stringbuilder independientemente del charset origen lo lee tal cual llega ya
          // sea \n o \r o \r\n
          sb.append(c);
        }
        buffer.clear();
      }
    } finally {
      reader.close();
    }
    return sb.toString().getBytes(charset);
  }

  /**
   * Obtiene todos los digests posibles a partir de la entrada recibida. Para ello obtiene todas las permutaciones de la
   * entrada del formUrlEncoded y calcula el digest de cada una de ellas.
   * 
   * @param form mapa de parametros recobidos en el formurlecoded
   * @param queryString query string recibido, si combinan metodos get y post se debe contemplar solo los del post para
   *        el calculo del digest
   * @param charset charset recibido en la cabecera content-type
   * @return lista de todos los digests posibles ante esa entrada
   */
  public static List<String> getAllFormDigests(final Map<String, String[]> form, final String queryString, Charset charset)
      throws NoSuchAlgorithmException {
    final List<String> digests = new ArrayList<>();
    final List<String> possibleRequests = DigestsUtils.paramsDataPermutations2String(
        DigestsUtils.possiblePermutations(new ArrayList<>(form.entrySet())), charset);
    if (CollectionUtils.isNotEmpty(possibleRequests)) {
      for (final String request : possibleRequests) {
        digests.add(calculateDigest(request.getBytes()));
      }
    } else {
      digests.add(calculateDigest("".getBytes()));
    }
    return digests;
  }

  /**
   * Calcula el digest de una request recibida por parametro empleando el algoritmo SHA-256
   * 
   * @param bodyBytes
   * @return
   * @throws NoSuchAlgorithmException
   */
  static String calculateDigest(final byte[] bodyBytes) throws NoSuchAlgorithmException {
    final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bodyBytes);
    final String stringDigest = new String(Base64.encodeBase64(digest));
    logger.debug("    !!!!Digest calculado: " + stringDigest);
    logger.debug("    !!!!a partir de: " + new String(bodyBytes));
    return stringDigest;
  }

  /**
   * Obtiene las cadenas en formato getString de todas las posibles permutaciones de un formulario recibido en formato
   * formUrlEncoded
   *
   */
  public static List<String> paramsDataPermutations2String(final List<Map<String, String[]>> formPermutations,
      final Charset charset) {
    final List<String> possibleRequests = new ArrayList<>();
    for (final Map<String, String[]> e : formPermutations) {
      //por si alguien calcula el digest antes de la codificacion de caracteres calculamos la permutacion con los valores codificados y sin codificar
      possibleRequests.add(DigestsUtils.paramsData2String(e, null));
      possibleRequests.add(DigestsUtils.paramsData2String(e, charset));
    }
    return possibleRequests;
  }

  /**
   * Convierte un mapa de parametros de un formulario en una cadena de caracteres de tipo get para calcular el digest
   * 
   * @param form mapa de parametros recobidos en el formurlecoded
   * @param charset charset recibido en la cabecera content-type
   * @return parametros concatenados en formato queryString por ejemplo
   *         sending_hei_id=UPV.ES&omobility_id=C7255E55-8457-5552-E053-0A032A9E4986&hei_id=UV.ES
   */
  public static String paramsData2String(final Map<String, String[]> form, final Charset charset) {
    final StringBuilder sb = new StringBuilder();
    String retorno;

    try {
      for (final Map.Entry<String, String[]> e : form.entrySet()) {
        for (final String value : e.getValue()) {
          if (sb.length() > 0) {
            sb.append('&');
          }
          sb.append(e.getKey());
          if (value != null) {
            sb.append('=');
            sb.append(Objects.nonNull(charset) ? URLEncoder.encode(value, charset.name()) : value);
          }
        }
      }
    } catch (final Exception e) {
      logger.error("failed to convert form", e);
    }
    retorno = sb.toString();

    return retorno;
  }


  /**
   * Obtiene todas las permutaciones posibles de un mapa de tipo Map<String,String[]>
   *
   * @param list lista de entradas del mapa
   * @return lista de mapas con todas las permutaciones posibles
   */
  public static List<Map<String, String[]>> possiblePermutations(final List<Map.Entry<String, String[]>> list) {
    // check if the list is non-null and non-empty
    if (list == null || list.size() == 0) {
      return Collections.emptyList();
    }
    return IntStream.range(0, list.size())
        // represent each list element as a list of permutation maps
        .mapToObj(i -> IntStream.range(0, list.size())
            // key - element position, value - element itself
            .mapToObj(j -> Collections.singletonMap(list.get(j).getKey(), list.get(j).getValue()))
            // Stream<List<Map<Integer,E>>>
            .collect(Collectors.toList()))
        // reduce a stream of lists to a single list
        .reduce((list1, list2) -> list1.stream().flatMap(map1 -> list2.stream()
            // filter out those keys that are already present
            .filter(map2 -> map2.keySet().stream().noneMatch(map1::containsKey))
            // concatenate entries of two maps, order matters
            .map(map2 -> new LinkedHashMap<String, String[]>() {
              private static final long serialVersionUID = -9167328389091344759L;
              {
                putAll(map1);
                putAll(map2);
              }
            }))
            // list of combinations
            .collect(Collectors.toList()))
        // otherwise an empty collection
        .orElse(Collections.emptyList());
  }

}
