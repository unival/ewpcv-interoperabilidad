package es.minsait.ewpcv.common;

/**
 * The type Ewp constants.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpConstants {
  // NAMESPACES DE LAS APIS DEL REGISTRO
  public static final String DISCOVERY_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-discovery/blob/stable-v6/manifest-entry.xsd";
  public static final String ECHO_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-echo/blob/stable-v2/manifest-entry.xsd";
  public static final String INSTITUTION_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-institutions/blob/stable-v2/manifest-entry.xsd";
  public static final String ORGANIZATION_UNIT_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/blob/stable-v2/manifest-entry.xsd";
  public static final String COURSE_REPLICATION_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-course-replication/blob/stable-v1/manifest-entry.xsd";
  public static final String COURSES_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-courses/blob/stable-v1/manifest-entry.xsd";
  public static final String IIAS_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/manifest-entry.xsd";
  public static final String IIAS_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iia-cnr/blob/stable-v3/manifest-entry.xsd";
  public static final String OUTGOING_MOBILITIES_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-omobilities/blob/stable-v1/manifest-entry.xsd";
  public static final String OUTGOING_MOBILITIES_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-cnr/blob/stable-v1/manifest-entry.xsd";
  public static final String INCOMING_MOBILITIES_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-imobilities/blob/stable-v1/manifest-entry.xsd";
  public static final String INCOMING_MOBILITIES_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-cnr/blob/stable-v1/manifest-entry.xsd";
  public static final String INCOMING_MOBILITIES_TORS_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v1/manifest-entry.xsd";
  public static final String INCOMING_MOBILITIES_TORS_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tor-cnr/blob/stable-v1/manifest-entry.xsd";
  public static final String OUTGOING_MOBILITIES_LA_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-la-cnr/blob/stable-v1/manifest-entry.xsd";
  public static final String IIAS_APPROVAL_CNR_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iia-approval-cnr/blob/stable-v2/manifest-entry.xsd";
  public static final String IIAS_APPROVAL_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/blob/stable-v2/manifest-entry.xsd";
  public static final String OUTGOING_MOBILITIES_LAS_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/manifest-entry.xsd";
  public static final String DICTIONARY_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-mt-dictionaries/blob/stable-v1/manifest-entry.xsd";
  public static final String FACTSHEET_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/blob/stable-v1/manifest-entry.xsd";
  public static final String MONITORING_NAMESPACE =
      "https://github.com/erasmus-without-paper/ewp-specs-api-monitoring/blob/stable-v1/manifest-entry.xsd";

  public static final String STATISTICS_OLA_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/stats-response.xsd";

  public static final String STATISTICS_IIA_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v6/endpoints/stats-response.xsd";

  public static final String FILES_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-file/blob/stable-v1/manifest-entry.xsd";

  // VERSIONES DE LAS APIS DEL REGISTRO
  public static final String ECHO_VERSION = "2.0.0";
  public static final String INSTITUTION_VERSION = "2.0.0";
  public static final String DISCOVERY_VERSION = "4.1.0";
  public static final String ORGANIZATION_UNIT_VERSION = "2.0.0";
  public static final String COURSES_VERSION = "0.7.0";
  public static final String COURSE_REPLICATION_VERSION = "1.0.0";
  public static final String OUTGOING_MOBILITIES_VERSION = "0.12.1";
  //public static final String OUTGOING_MOBILITIES_LAS_VERSION = "1.0.0";
  public static final String OUTGOING_MOBILITY_CNR_VERSION = "0.4.0";
  public static final String INCOMING_MOBILITIES_VERSION = "0.1.0";
  public static final String INCOMING_MOBILITY_CNR_VERSION = "0.1.0";
  //public static final String INCOMING_MOBILITIES_TORS_VERSION = "0.7.0";
  public static final String INCOMING_MOBILITY_TORS_CNR_VERSION = "0.1.0";
  public static final String IIAS_VERSION = "7.0.0";
  public static final String IIA_CNR_VERSION = "3.0.0";
  public static final String DICTIONARY_VERSION = "0.1.0";

  public static final String ECHO_CLIENT_VERSION = "2.0.0";
  public static final String INSTITUTION_CLIENT_VERSION = "2.0.0";
  public static final String ORGANIZATION_UNIT_CLIENT_VERSION = "2.0.0";
  public static final String COURSES_CLIENT_VERSION = "0.7.0";
  public static final String COURSE_REPLICATION_CLIENT_VERSION = "1.0.0";
  public static final String OUTGOING_MOBILITIES_CLIENT_VERSION = "0.11.0";
  public static final String OUTGOING_MOBILITIES_CNR_CLIENT_VERSION = "0.4.0";
  public static final String INCOMING_MOBILITIES_CLIENT_VERSION = "0.3.0";
  public static final String INCOMING_MOBILITY_CNR_CLIENT_VERSION = "0.1.0";
  public static final String INCOMING_MOBILITIES_TORS_VERSION = "1.0.0";
  public static final String INCOMING_MOBILITY_TORS_CNR_CLIENT_VERSION = "0.1.0";
  public static final String IIAS_CLIENT_VERSION = "7.0.0";
  public static final String IIA_CNR_CLIENT_VERSION = "3.0.0";
  public static final String IIA_APPROVAL_CNR_CLIENT_VERSION = "2.0.0";
  public static final String IIA_APPROVAL_CLIENT_VERSION = "2.0.0";
  public static final String OUTGOING_MOBILITIES_LAS_VERSION = "1.1.0";
  public static final String OUTGOING_MOBILITIES_LA_CNR_CLIENT_VERSION = "0.1.0";
  public static final String FACTSHEET_CLIENT_VERSION = "0.1.0";
  public static final String MONITORING_CLIENT_VERSION = "0.1.0";
  public static final String DISCOVERY_CLIENT_VERSION = "6.0.0";
  public static final String FILES_CLIENT_VERSION = "1.0.0";

  // NOMBRES DE LAS APIS EN EL REGISTRO
  public static final String DICTIONARIES_API_NAME = "mt-dictionaries";
  public static final String INSTITUTIONS_API_NAME = "institutions";
  public static final String ORGANIZATIONAL_UNITS_API_NAME = "organizational-units";
  public static final String COURSE_REPLICATION_API_NAME = "simple-course-replication";
  public static final String COURSES_API_NAME = "courses";
  public static final String IIAS_API_NAME = "iias";
  public static final String IIA_CNR_API_NAME = "iia-cnr";
  public static final String OMOBILITIES_API_NAME = "omobilities";
  public static final String OMOBILITY_LAS_API_NAME = "omobility-las";
  public static final String IIA_APPROVAL_CNR_API_NAME = "iia-approval-cnr";
  public static final String IIAS_APPROVAL_API_NAME = "iias-approval";
  public static final String OMOBILITY_CNR_API_NAME = "omobility-cnr";
  public static final String OMOBILITY_LA_CNR_API_NAME = "omobility-la-cnr";
  public static final String IMOBILITIES_API_NAME = "imobilities";
  public static final String IMOBILITY_CNR_API_NAME = "imobility-cnr";
  public static final String IMOBILITY_TOR_CNR_API_NAME = "imobility-tor-cnr";
  public static final String ECHO_API_NAME = "echo";
  public static final String FACTSHEET_API_NAME = "factsheet";
  public static final String IMOBILITY_TORS_API_NAME = "imobility-tors";
  public static final String MONITORING_API_NAME = "monitoring";
  public static final String REGISTRY_API_NAME = "registry";
  public static final String DISCOVERY_API_NAME = "discovery";

  public static final String FILE_API_NAME = "file";

  public static final String DICTIONARY_PARAM_NAME = "dictionary";
  public static final String DICTIONARY_CALL_YEAR_PARAM_NAME = "call_year";

  public static final String X_REQUEST_ID = "x-request-id";
  public static final String HOST = "host";
  public static final String RSA_PUBLIC_KEY = "EwpRequestRSAPublicKey";
  public static final String REQUEST_CERTIFICATE = "EwpRequestCertificate";

  public static final String ECHOAUDIT = "ewp.echo.audit";
  public static final String FACTSHEETAUDIT = "ewp.factsheet.audit";
  public static final String IIAAPPROVALAUDIT = "ewp.iiaApproval.audit";
  public static final String IIAAUDIT = "ewp.iia.audit";
  public static final String IMOBILITYAUDIT = "ewp.iMobility.audit";
  public static final String INSTITUTIONSAUDIT = "ewp.institutions.audit";
  public static final String OMOBILITYAUDIT = "ewp.oMobility.audit";
  public static final String OMOBILITYLASAUDIT = "ewp.oMobilityLas.audit";
  public static final String ORGANIZATIONUNITAUDIT = "ewp.organizationUnit.audit";
  public static final String TORAUDIT = "ewp.tor.audit";
  public static final String FILEAPIAUDIT = "ewp.fileApi.audit";


  public static final String STATISTICSIIAUDIT = "ewp.statisticsIia.audit";
  public static final String STATISTICSILAUDIT = "ewp.statisticsILA.audit";
  public static final String STATISTICSOLAUDIT = "ewp.statisticsOLA.audit";
  public static final String ALL_HASH_VALIDATION = "validateAll";

  public static final Class BYTE_CLASS = byte.class;
}
