package es.minsait.ewpcv.batch;

import es.minsait.ewpcv.service.api.IMobilityUpdateRequestService;
import es.minsait.ewpcv.service.api.INotificationService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The type Purga notificaciones solicitudes job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class PurgaNotificacionesSolicitudesJob implements Job {

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private IMobilityUpdateRequestService requestService;

  @Value("${ewp.notificaciones.refresh.maxprocessingtime}")
  private Long maxProcessingTime;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    log.info("***Comienza el proceso de refresco de las notificaciones y las solicitudes");

    // obtenemos las notificaciones que lleven mas de {maxProcessingTime}
    // minutos procesandose y ponemos el campo PROCESSING a 0
    notificationService.purgarNotificaciones(maxProcessingTime);

    // obtenemos las peticiones de actualizacion que lleven mas de
    // {maxProcessingTime} minutos procesandose y ponemos el campo IS_PROCESSING
    // a 0
    requestService.purgarUpdateRequests(maxProcessingTime);
  }
}
