package es.minsait.ewpcv.security.xmldsig;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * The type Transform utils.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class TransformUtils {

  /**
   * Convierte un String a Document
   *
   *
   * @param xml Cadena XML
   * @return Documento XML
   *
   */
  public Document string2Document(final String xml) throws Exception {
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true);
    final DocumentBuilder builder;
    try {
      builder = factory.newDocumentBuilder();
      final Document doc = builder.parse(new InputSource(new StringReader(xml)));
      return doc;
    } catch (final Exception e) {
      throw new Exception("Error al generar Documento: " + e.getMessage());
    }
  }

  /**
   * Convierte un Document a String
   *
   *
   * @param doc Document XML
   * @return String XML
   *
   */
  public String document2String(final Document doc) throws Exception {
    try {
      final DOMSource domSource = new DOMSource(doc);
      final StringWriter writer = new StringWriter();
      final StreamResult result = new StreamResult(writer);
      final TransformerFactory tf = TransformerFactory.newInstance();
      final Transformer transformer = tf.newTransformer();
      transformer.transform(domSource, result);
      return writer.toString();
    } catch (final TransformerException e) {
      throw new Exception("Error al generar Cadena" + e.getMessage());
    }
  }
}
