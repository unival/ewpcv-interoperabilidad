package es.minsait.ewpcv.error;

/**
 * The type Ewp operation not found exception.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpOperationNotFoundException extends Exception {
  int status;

  public EwpOperationNotFoundException(String message, int status) {
    super(message);
    this.status = status;
  }

  public int getStatus() {
    return status;
  }
}
