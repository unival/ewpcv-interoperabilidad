package es.minsait.ewpcv.common;

public enum ErrorTypeEnum {
  // When there is any error with looking up the API from the catalogue. For example the HEI does not implement an API
  // that it is expected to, like the API for fetching an LA when a CNR notification was received.
  API_LOOKUP_ERROR,
  // - For network errors other than timeout, like DNS failures or SSL certificate errors.,
  NETWORK_ERROR,
  // - When client decided the server is taking too long to respond.
  TIMEOUT,
  // - When server returned any 4xx or 5xx HTTP code.
  SERVER_ERROR,
  // - When the client could not verify the server's response, for example because of some incorrect values returned by
  // the server.;
  INVALID_RESPONSE;
}
