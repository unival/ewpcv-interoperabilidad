package es.minsait.ewpcv.common;


import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import es.minsait.ewpcv.service.api.*;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class OnDemandFetchService {

  @Autowired
  GlobalProperties globalProperties;

  @Autowired
  private IInstitutionService institutionService;
  @Autowired
  private IOunitService ounitService;
  @Autowired
  private RegistryClient registry;
  @Autowired
  private RestClient client;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IFileApiService fileApiService;

  /**
   * Obtenemos la informacion sobre la unidad organizativa y la insertamos
   * @param ounitId
   * @param heiId
   * @throws EwpOperationNotFoundException
   */
  public void fetchOUnitIfNotExists(String ounitId, String heiId) {

    final EventMDTO event = utils.initializeJobEvent(NotificationTypes.ORGANIZATION_UNIT, heiId, globalProperties.getInstance(), ounitId,
        EwpEventConstants.INSERT);

    try {
      // si no tenemos la institucion no podemos obtener la unidad organizativa
      if (!institutionService.isKnownHei(heiId)) {
        log.debug("No existe la institucion " + heiId + " en el sistema, no podemos obtener la unidad organizativa");
        event.setStatus(EwpEventConstants.KO);
        event.setObservations(
            "No existe la institucion " + heiId + " en el sistema, no podemos obtener la unidad organizativa");
        eventService.save(event);
        return;
      }

      if (!ounitService.existsOUnit(ounitId, heiId) && !Strings.isNullOrEmpty(ounitId)) {
        log.debug("La unidad organizariva " + ounitId + " no existe en BBDD, intentamos obtenerla");
        final ClientResponse response = invocarOunitGet(getOunitHeiEntry(heiId), heiId, ounitId, event);

        if (utils.verifyResponse(null, event, response, EwpEventConstants.INSERT)) {
          final OunitsResponse ounitsResponse = (OunitsResponse) response.getResult();

          if (Objects.nonNull(ounitsResponse)) {
            // en principio solo nos tiene que llegar una ounit y al no tenerla en local este metodo la inserta
            List<OrganizationUnitMDTO> orgUnits =
                ounitService.actualizarOrganizationUnits(heiId, ounitsResponse.getOunit(), null);

            // una vez insertada se la asignamos a la institucion
            institutionService.addOunitToInstitution(orgUnits.get(0), heiId);
          }
        } else {
          log.error(
              "Error en la respuesta, status: " + response.getStatusCode() + " ,error: " + response.getErrorMessage());
        }

        eventService.save(event);
      } else {
        log.debug("La Ounit " + ounitId + " ya existe en el sistema");
      }
    } catch (Exception e) {
      log.error("Error al obtener la unidad organizativa " + ounitId + " de la institucion " + heiId, e);
      event.setStatus(EwpEventConstants.KO);
      event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
      eventService.save(event);
    }
  }

  /**
   * Busca la institucion y si no la tenemos obtenemos la institucion y la insertamos
   * @param heiId: Identificador de la institucion
   * @throws EwpOperationNotFoundException
   */
  public void fetchInstitutionIfNotExists(String heiId) {

    final EventMDTO event =
        utils.initializeJobEvent(NotificationTypes.INSTITUTIONS, heiId, globalProperties.getInstance(), heiId, EwpEventConstants.INSERT);

    try {
      List<HeiEntry> heis = registry.getInstitutionsHeiUrls();
      HeiEntry hei = utils.filterHeisByProperties(heis, heiId).get(0);

      // Para pruebas con dos instancias locales
      // hei.getUrls().put(EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, targetUri + "/institutions/get");

      if (!institutionService.isKnownHei(heiId)) {
        log.debug("La institucion " + heiId + " no existe en BBDD, intentamos obtenerla");
        final ClientResponse response = invocarInstitutionsGet(hei, event);

        if (utils.verifyResponse(null, event, response, EwpEventConstants.INSERT)) {
          final InstitutionsResponse institutionsResponse = (InstitutionsResponse) response.getResult();

          if (Objects.nonNull(institutionsResponse) && Objects.nonNull(institutionsResponse.getHei())) {
            log.info("Insertamos la instucion");
            institutionService.actualizarInstitutions(institutionsResponse.getHei());
          }

          eventService.save(event);
        } else {
          log.error(
              "Error en la respuesta, status: " + response.getStatusCode() + " ,error: " + response.getErrorMessage());
        }
      } else {
        log.debug("La institucion " + heiId + " ya existe en el sistema");
      }
    } catch (Exception e) {
      log.error("Error al obtener la institucion " + heiId, e);
      event.setStatus(EwpEventConstants.KO);
      event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
      eventService.save(event);
    }
  }

  /**
   * Obtenemos el fichero invocando al api de ficheros del partner
   * @param heiId
   * @param fileId
   */
  public void fetchFile(String heiId, String fileId){

    if(Strings.isNullOrEmpty(heiId) || Strings.isNullOrEmpty(fileId)) {
      log.error("No se han recibido los datos necesarios para obtener el fichero");
      return;
    }
    EventMDTO event = utils.initializeJobEvent( NotificationTypes.FILE_API, heiId, globalProperties.getInstance(),
            fileId,EwpEventConstants.FETCH_FILE);
    try {
      //buscar si ya tenemos el fichero
      if (!fileApiService.existsFile(fileId, heiId)) {
        //invocamos al get del api de ficheros
        ClientResponse fileResponse = invocarFileGet(heiId, fileId, event);
        if (!utils.verifyResponse(null, event, fileResponse, EwpEventConstants.FETCH_FILE)) {
            return;
        }
        utils.setMessageToEventOK(event, fileResponse);
        eventService.save(event);
        //insertamos el fichero
        fileApiService.insertFile((byte[]) fileResponse.getResult(), fileId, heiId, MediaType.APPLICATION_PDF_VALUE);

      }
    }catch (Exception e) {
      log.error("Error al obtener la el fichero  " + fileId + " de la institucion: " +heiId, e);
      event.setStatus(EwpEventConstants.KO);
      event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
      eventService.save(event);
    }
  }

  private ClientResponse invocarFileGet(final String heiId, String fileId, EventMDTO event) throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.FILE_ID, Arrays.asList(fileId));
    final HeiEntry hei = registry.getFilesHeiUrls(heiId);
    final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.FILE_API_GET_URL_KEY, params, HttpMethodEnum.GET,
            EwpConstants.FILE_API_NAME);
    return client.sendRequest(request, EwpConstants.BYTE_CLASS, event);
  }

  private ClientResponse invocarInstitutionsGet(final HeiEntry hei, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.INSTITUTIONS_GET_HEI_ID, Arrays.asList(hei.getId()));
    final ClientRequest request = utils.formarPeticion(hei, EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.INSTITUTIONS_API_NAME);
    return client.sendRequest(request, InstitutionsResponse.class, event);
  }

  private ClientResponse invocarOunitGet(final HeiEntry ounitHeiEntry, final String heiId, final String ounit,
      EventMDTO event) throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Collections.singletonList(heiId));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID, Collections.singletonList(ounit));
    final ClientRequest request = utils.formarPeticion(ounitHeiEntry, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY,
        params, HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    return client.sendRequest(request, OunitsResponse.class, event);
  }

  private HeiEntry getOunitHeiEntry(final String heiId) throws EwpOperationNotFoundException {

    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls(heiId.toLowerCase());

    // Para pruebas con dos instancias locales
    // hei.getUrls().put(EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, targetUri + "/ounits/get");

    if (Objects.isNull(hei) || Objects.isNull(hei.getUrls()) || hei.getUrls().isEmpty()
        || Strings.isNullOrEmpty(hei.getUrls().get(EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY))) {
      throw new EwpOperationNotFoundException(
              "No se ha encontrado " + EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY + " para el heiId " + heiId, 404);
    }
    return hei;
  }
}
