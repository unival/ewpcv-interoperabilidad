package es.minsait.ewpcv.batch;

import java.util.*;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.iias.endpoints.IiasGetResponse;
import eu.erasmuswithoutpaper.api.iias.endpoints.MobilitySpecification;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.MobilityInstitution;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasGetResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Learning agreement cnr job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class LearningAgreementCnrJob implements Job {

  @Value("${ewp.la.cnr.retries.delay:1}")
  private int retriesDelay;

  @Value("${ewp.la.cnr.notification.block:50}")
  private int blockSize;

  @Autowired
  private INotificationService notificationService;

  @Autowired
  private OmobilityLasService omobilityLasService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private Utils utils;

  @Value("${ewp.target.uri:}")
  private String targetUri;
  @Autowired
  private GlobalProperties globalProperties;

  @Value("${batch.cnr.outgoingmobilityla:true}")
  private boolean cnrLeanringAgreement;

  @Autowired
  private OnDemandFetchService onDemandFetchService;


  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrLeanringAgreement) {
      log.info("***No se ejecuta el proceso de actualizacion de LA a partir de CNR por configuracion");
      return;
    }

    log.info("***Comienza proceso de actualizacion de LA a partir de CNR");
    final List<NotificationMDTO> list =
        notificationService.reservarNotificacionesRecibidas(retriesDelay, blockSize, NotificationTypes.LA);
    log.info("Se van a actualizar " + list.size() + " LA-CNR");
    for (final NotificationMDTO notification : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.LA, notification.getOwnerHeiId(),
          notification.getHeiId(), notification.getChangedElementIds(), EwpEventConstants.UPDATE_LA_PROCESSING);

      try {
        // buscamos a ver si
        // la tenemos, si no es asi es una nueva
        final MobilityMDTO learnAgrLocal =
                omobilityLasService.findLastMobilityRevisionByIdAndSendingHeiId(notification.getChangedElementIds(), notification.getHeiId());

        ClientResponse response = invocarLAGet(notification, event);
        if (!utils.verifyResponse(notification, event, response, EwpEventConstants.CNR_PROCESSING_GET)) {
          continue;
        }
        final OmobilityLasGetResponse getResponse = (OmobilityLasGetResponse) response.getResult();

        //comprobamos si tenemos las ounits, si no las tenemos las obtenemos
        if(Objects.nonNull(getResponse.getLa())){
          for (final LearningAgreement la : getResponse.getLa()) {
            //obtenemos el partner
            MobilityInstitution partner = la.getSendingHei().getHeiId().equals(globalProperties.getInstance())?
                    la.getReceivingHei() : la.getSendingHei();

            //obtenemos la institucion si no la tenemos
            onDemandFetchService.fetchInstitutionIfNotExists(partner.getHeiId());

            //obtenemos la ounit si no la tenemos
            onDemandFetchService.fetchOUnitIfNotExists(partner.getOunitId(), partner.getHeiId());
          }
        }

        if (Objects.isNull(learnAgrLocal)) {
          for (final LearningAgreement la : getResponse.getLa()) {
            final boolean actualiza = omobilityLasService.insertarOmobilities(la, globalProperties.getInstance());
            event.setEventType(actualiza ? EwpEventConstants.INSERT : EwpEventConstants.NO_ACTION);
          }
        } else {
          for (final LearningAgreement la : getResponse.getLa()) {
            final boolean actualiza =
                    omobilityLasService.actualizarOmobilities(la, learnAgrLocal, globalProperties.getInstance());
            event.setEventType(actualiza ? EwpEventConstants.UPDATE : EwpEventConstants.NO_ACTION);
          }
        }

        // eventosService.insertaEvento(notification, tipoEvento)
        notificationService.eliminarNotificacion(notification);
      } catch (final Exception e) {
        final String error = Objects.nonNull(notification)
                ? "Error al procesar actualizacion LA-CNR, id Mobility: " + notification.getChangedElementIds()
                : "Error al procesar actualizacion LA-CNR";
        log.error(error, getClass().getName(), e);
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
        notificationService.liberarNotificaciones(notification, e.getMessage());
      }
      eventService.save(event);
    }

  }

  private ClientResponse invocarLAGet(final NotificationMDTO notificacion, EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList(notificacion.getHeiId()));
    params.put(EwpRequestConstants.OMOBILITY_LA_OMOBILITY_ID, Arrays.asList(notificacion.getChangedElementIds()));
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls(notificacion.getHeiId().toLowerCase());
    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_GET, params, HttpMethodEnum.POST,
            EwpConstants.OMOBILITY_LAS_API_NAME);
    return client.sendRequest(requestIndex, OmobilityLasGetResponse.class, event);
  }
}
