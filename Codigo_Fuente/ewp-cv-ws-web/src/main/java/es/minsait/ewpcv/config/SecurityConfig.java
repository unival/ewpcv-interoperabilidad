package es.minsait.ewpcv.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * The type Security config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    // HTTP Basic authentication
    http.httpBasic().and().authorizeRequests().antMatchers("/**").permitAll().and().csrf().disable().formLogin()
            .disable();
  }

}
