package es.minsait.ewpcv.security;


import javax.servlet.http.HttpServletResponse;

/**
 * The type Authenticate method response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class AuthenticateMethodResponse {
  private final boolean requiredMethodInfoFulfilled;
  private final String errorMessage;
  private final int status;

  private AuthenticateMethodResponse(final boolean requiredMethodInfoFulfilled, final String errorMessage, final int status) {
    this.requiredMethodInfoFulfilled = requiredMethodInfoFulfilled;
    this.errorMessage = errorMessage;
    this.status = status;
  }

  public static AuthenticateMethodResponseBuilder builder() {
    return new AuthenticateMethodResponseBuilder();
  }

  public boolean isRequiredMethodInfoFulfilled() {
    return requiredMethodInfoFulfilled;
  }

  public boolean isVerifiedOk() {
    return status == HttpServletResponse.SC_OK;
  }

  public int status() {
    return status;
  }

  public boolean hasErrorMessage() {
    return errorMessage != null;
  }

  public String errorMessage() {
    return errorMessage;
  }

  /**
   * The type Authenticate method response builder.
   *
   * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
   */
  public static class AuthenticateMethodResponseBuilder {
    private boolean requiredMethodInfoFulfilled = true;
    private String errorMessage;
    private int status = HttpServletResponse.SC_OK;

    public AuthenticateMethodResponseBuilder withRequiredMethodInfoFulfilled(final boolean methodIsValid) {
      this.requiredMethodInfoFulfilled = methodIsValid;
      return this;
    }

    public AuthenticateMethodResponseBuilder withErrorMessage(final String errorMessage) {
      this.errorMessage = errorMessage;
      return this;
    }

    public AuthenticateMethodResponseBuilder withResponseCode(final int status) {
      this.status = status;
      return this;
    }

    public AuthenticateMethodResponse build() {
      return new AuthenticateMethodResponse(requiredMethodInfoFulfilled, errorMessage, status);
    }
  }
}
