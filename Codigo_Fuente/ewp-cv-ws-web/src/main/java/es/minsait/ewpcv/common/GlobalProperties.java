package es.minsait.ewpcv.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Global properties.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
@Data
public class GlobalProperties {
  @Value("${ewp.instance}")
  String instance;

  @Value("${ewp.name}")
  String name;

  @Value("${ewp.host.name}")
  String hostName;

  @Value("${ewp.base.uri}")
  String baseUri;

  @Value("${ewp.keystore.location}")
  String keyStoreLocation;

  @Value("${ewp.keystore.password}")
  String keystorePassword;

  @Value("${ewp.keystore.type}")
  String keystoreType;

  @Value("${ewp.keystore.certificate.alias}")
  String keystoreAlias;

  @Value("${ewp.truststore.location}")
  String truststoreLocation;

  @Value("${ewp.truststore.password}")
  String truststorePassword;

  @Value("${ewp.truststore.type}")
  String truststoreType;

  @Value("${ewp.allow.missing.client.certificate:false}")
  boolean allowMissingClientCertificate;

  @Value("${ewp.security.disabled:false}")
  boolean securityDisabled;

  @Value("${ewp.api.institutions.max.ids:1}")
  int institutionsMaxIds;

  @Value("${ewp.api.ounits.max.ids:1}")
  int ounitsMaxIds;

  @Value("${ewp.registry.url}")
  String registryUrl;

  @Value("${ewp.registry.auto.refreshing:false}")
  boolean registryAutoRefreshing;

  @Value("${ewp.registry.time.between.retries:180000}")
  long registryTimeBetweenRetries;

  @Value("${ewp.proxy:}")
  String proxy;

  @Value("${ewp.proxyPort:0}")
  int proxyPort;
  @Value("${ewp.verify.responses:true}")
  boolean verifyResponses;
  @Value("${ewp.institutions.defaultHeiIds:}")
  String defaultInstitutionsHeiIds;
  @Value("${ewp.refresh.defaultHeiIds:}")
  String defaultRefreshHeiIds;

  @Value("${ewp.iia.hashValidationHeis:}")
  String hashValidationHeis;

  @Value("${ewp.monitoring.schac.code:stats.erasmuswithoutpaper.eu}")
  String monitoringSchac;

  @Value("${ewp.monitoring.avoid.notify.error.types:}")
  String monitoringAvoidNotifyErrorTypes;

  @Value("${ewp.validate.responses:true}")
  boolean validateResponses;

  @Value("${ewp.iia.delete.enabled:true}")
  private boolean iiaDeleteEnabled;

  @Value("${ewp.autorefresh.iia.onhashfailure:false}")
  private boolean notificationRefreshEnabled;



  public String getInstance() {
    return this.instance.toLowerCase();
  }
}
