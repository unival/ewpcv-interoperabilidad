package es.minsait.ewpcv.common;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

public class SaxParserValidationEventHandler implements ValidationEventHandler {

    private StringBuilder errorMessages = new StringBuilder();

    @Override
    public boolean handleEvent(ValidationEvent event) {
        errorMessages.append(event.getMessage())
                .append("\n");

        // Si la severidad es FATAL_ERROR o ERROR, el unmarshalling se detiene inmediatamente.
        // Si devuelve false, el unmarshalling se detendrá inmediatamente.
        // Si devuelve true, el unmarshalling intentará continuar.
        return true;
    }

    public String getErrorMessages() {
        return errorMessages.toString();
    }
}