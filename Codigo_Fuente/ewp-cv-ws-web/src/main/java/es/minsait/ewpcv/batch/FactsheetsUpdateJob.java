package es.minsait.ewpcv.batch;


import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IFactsheetService;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * The type Factsheets update job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class FactsheetsUpdateJob implements Job {

  @Autowired
  private GlobalProperties properties;

  @Autowired
  private IFactsheetService factsheetService;

  @Autowired
  private RestClient client;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private Utils utils;

  @Autowired
  private GlobalProperties globalProperties;

  @Value("${batch.refrescar.factsheet:true}")
  private boolean refrescarFactsheet;

  @Autowired
  private OnDemandFetchService onDemandFetchService;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    log.info("Comienza proceso de refresco de Factsheets");
    if (!refrescarFactsheet) {
      log.info("no se ejecuta el proceso de refresco de Factsheet por configuracion");
      return;
    }

    List<HeiEntry> iiaHeis = registry.getFactsheetHeisWithUrls();

    iiaHeis = utils.filterHeisByProperties(iiaHeis, properties.getDefaultInstitutionsHeiIds());

    for (final HeiEntry hei : iiaHeis) {
      actualizarFactsheet(hei);
    }

    log.info("Termina proceso de refresco de Factsheets");
  }

  private void actualizarFactsheet(final HeiEntry hei) {
    final EventMDTO event = utils.initializeJobEvent(NotificationTypes.FACTSHEET, hei.getId(),
            globalProperties.getInstance(),hei.getId(), EwpEventConstants.UPDATE);


    try {
      final ClientResponse response = invocarFactsheetGet(hei, event);
      if (!utils.verifyResponse(null, event, response, EwpEventConstants.FACTSHEETGET_GET)) {
        // si la respuesta no es correcta no continuamos con el proceso, esta verificacion ya guarda el evento
        return;
      }

      // a partir del resultado de la consulta actualizamos los factsheets de
      // las instituciones en base de datos
      final List<FactsheetResponse.Factsheet> factsheet = ((FactsheetResponse) response.getResult()).getFactsheet();

      if (Objects.nonNull(factsheet) && !factsheet.isEmpty()) {
        //obtenemos la institucion si no la tenemos
        onDemandFetchService.fetchInstitutionIfNotExists(factsheet.get(0).getHeiId());

        factsheetService.actualizarFactsheets(factsheet.get(0));
        // guardamos en el evento el id del hei al que pertenece el factsheet
        event.setChangedElementIds(factsheet.get(0).getHeiId());
      }

    } catch (final Exception e) {
      log.error("Error en el proceso de refresco automatico de Factsheets ", getClass().getName(), e);
      event.setObservations(e.getMessage());
      event.setStatus(EwpEventConstants.KO);
    }

    eventService.save(event);
  }

  private ClientResponse invocarFactsheetGet(final HeiEntry hei, final EventMDTO event)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.FACTSHEET_HEI_ID, Arrays.asList(hei.getId()));

    final ClientRequest requestIndex =
        utils.formarPeticion(hei, EwpRequestConstants.FACTSHEET_GET_URL_KEY, params, HttpMethodEnum.POST,
            EwpConstants.FACTSHEET_API_NAME);

    return client.sendRequest(requestIndex, FactsheetResponse.class, event);
  }



}
