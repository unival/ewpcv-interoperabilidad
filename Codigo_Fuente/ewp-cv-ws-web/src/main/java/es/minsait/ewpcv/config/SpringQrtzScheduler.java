package es.minsait.ewpcv.config;

import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;

/**
 * The type Spring qrtz scheduler.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class SpringQrtzScheduler {

  @Bean
  public JobFactory jobFactory(final ApplicationContext applicationContext) {
    final AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
    jobFactory.setApplicationContext(applicationContext);
    return jobFactory;
  }

  @Bean(name = "EWPScheduler")
  public SchedulerFactoryBean scheduler(final @Qualifier("quartzDataSource") DataSource quartzDataSource, final JobFactory jobFactory,
                                        final List<Trigger> triggers, @Value("${quartz.driver}") final String driver) throws Exception {
    final SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
    schedulerFactory.setOverwriteExistingJobs(true);
    schedulerFactory.setDataSource(quartzDataSource);

    final Properties properties = new Properties();

    properties.setProperty("org.quartz.scheduler.instanceName", "EWPScheduler");
    properties.setProperty("org.quartz.scheduler.instanceId", "AUTO");
    properties.setProperty("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
    properties.setProperty("org.quartz.jobStore.driverDelegateClass", driver);
    properties.setProperty("org.quartz.jobStore.useProperties", "true");
    properties.setProperty("org.quartz.jobStore.misfireThreshold", "60000");
    properties.setProperty("org.quartz.jobStore.tablePrefix", "ewpcv_qrtz_");
    properties.setProperty("org.quartz.jobStore.isClustered", "true");
    properties.setProperty("org.quartz.plugin.shutdownHook.class", "org.quartz.plugins.management.ShutdownHookPlugin");
    properties.setProperty("org.quartz.plugin.shutdownHook.cleanShutdown", "TRUE");

    schedulerFactory.setQuartzProperties(properties);
    schedulerFactory.setJobFactory(jobFactory);
    schedulerFactory.setTriggers(triggers.toArray(new Trigger[triggers.size()]));

    // schedulerFactory.setGlobalTriggerListeners(new TriggerListener());

    schedulerFactory.afterPropertiesSet();

    return schedulerFactory;
  }

}
