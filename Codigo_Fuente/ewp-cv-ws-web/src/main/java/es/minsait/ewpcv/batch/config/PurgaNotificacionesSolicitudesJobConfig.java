package es.minsait.ewpcv.batch.config;


import es.minsait.ewpcv.batch.PurgaNotificacionesSolicitudesJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * The type Purga notificaciones solicitudes job config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Configuration
public class PurgaNotificacionesSolicitudesJobConfig {


  @Bean
  public JobDetailFactoryBean jobDetailPurgaNotificacionesSolicitudesJob() {
    final JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    jobDetailFactory.setJobClass(PurgaNotificacionesSolicitudesJob.class);
    jobDetailFactory.setDescription(
        "Batch para la reactivacion de notificaciones que no se han procesado correctamente y han quedado en un estado inconsistente");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  @Bean
  public CronTriggerFactoryBean triggerPurgaNotificacionesSolicitudesJob(
      @Qualifier("jobDetailPurgaNotificacionesSolicitudesJob") final JobDetail jobDetail,
      @Value("${ewp.notificaciones.refresh.cron}") final String cron) {
    final CronTriggerFactoryBean triggerFactory = new CronTriggerFactoryBean();
    triggerFactory.setCronExpression(cron);
    triggerFactory.setJobDetail(jobDetail);
    return triggerFactory;
  }
}
