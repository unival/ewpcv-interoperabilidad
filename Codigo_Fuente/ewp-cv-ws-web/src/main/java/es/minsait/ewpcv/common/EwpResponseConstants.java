package es.minsait.ewpcv.common;

/**
 * The type Ewp response constants.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EwpResponseConstants {

  public static final String LAS_UPDATE_RESPONSE_SUCCESS = "The update request was received with success.";
}
