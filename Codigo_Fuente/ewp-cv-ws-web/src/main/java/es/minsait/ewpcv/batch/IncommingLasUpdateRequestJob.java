package es.minsait.ewpcv.batch;

import java.util.*;

import org.apache.http.HttpStatus;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpIllegalStateException;
import es.minsait.ewpcv.error.EwpNotFoundException;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestType;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IMobilityUpdateRequestService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateRequest;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Incomming las update request job.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Component
@DisallowConcurrentExecution
public class IncommingLasUpdateRequestJob implements Job {

  @Autowired
  private IMobilityUpdateRequestService mobilityUpdateRequestService;

  @Autowired
  private OmobilityLasService mobilityService;

  @Autowired
  private RegistryClient registry;

  @Autowired
  private IEventService eventService;

  @Autowired
  private RestClient client;

  @Autowired
  private GlobalProperties globalProperties;

  @Autowired
  private Utils utils;

  @Value("${ewp.la.update.block:50}")
  private int blockSize;

  @Value("${ewp.la.update.max.retries:1}")
  private int maxRetries;

  @Value("${ewp.target.uri:}")
  private String targetUri;

  @Value("${batch.cnr.outgoingmobilityla:true}")
  private boolean cnrLeanringAgreement;

  @Override
  public void execute(final JobExecutionContext context) throws JobExecutionException {
    if (!cnrLeanringAgreement) {
      log.info("***No se ejecuta el proceso de notificacion de solicitudes de actualizacion de LAs por configuracion");
      return;
    }
    log.info("***Comienza el proceso de notificacion de solicitudes de actualizacion de LAs");
    final List<MobilityUpdateRequestMDTO> list = mobilityUpdateRequestService
        .reservarMobilityUpdateRequest(UpdateRequestMobilityTypeEnum.INCOMING, maxRetries, blockSize);
    final List<String> idsReintentar = new ArrayList<>();
    log.info("Se van a notificar " + list.size() + " solicitudes de actualizacion de LAs");

    for (final MobilityUpdateRequestMDTO m : list) {
      final EventMDTO event = utils.initializeJobEvent(NotificationTypes.LA, m.getSendingHeiId(),
          globalProperties.getInstance(), null, null);

      try {
        final OmobilityLasUpdateRequest updateRequest = mobilityUpdateRequestService
                .bytesToClassImplByClass(m.getUpdateInformation(), OmobilityLasUpdateRequest.class);
        final String type = MobilityUpdateRequestType.APPROVE_PROPOSAL_V1.equals(m.getType()) ? EwpEventConstants.ACCEPT
                : EwpEventConstants.REJECT;
        event.setEventType(type);
        event.setChangedElementIds(
                EwpEventConstants.ACCEPT.equals(type) ? updateRequest.getApproveProposalV1().getOmobilityId()
                        : updateRequest.getCommentProposalV1().getOmobilityId());
        event.setObservations(
                EwpEventConstants.ACCEPT.equals(type) ? null : updateRequest.getCommentProposalV1().getComment());


        final ClientResponse response = invocarLasUpdate(m, event);
        // esto solo setea el campo message, por lo que lo hacemos aunque este ko
        utils.setMessageToEventOK(event, response.getResult());
        if(Objects.nonNull(response.getRawResponse())){
          event.setMessage(response.getRawResponse().getBytes());
        }
        if (response.getStatusCode() != HttpStatus.SC_OK) {
          event.setObservations(response.getRawResponse());
          event.setStatus(EwpEventConstants.KO);
          idsReintentar.add(m.getId());
          if (m.getRetries()>=1 && (response.getStatusCode() == HttpStatus.SC_CONFLICT || response.getStatusCode() == HttpStatus.SC_BAD_REQUEST)) {
            // Si el error es 409 o 400, se elimina la solicitud, en principio deberiamos solo eliminar los errores 409, pero algunos proveedores
            // devuelven 400 en lugar de 409
            String error = utils.truncateToMaxVarchar(response.getErrorMessage());
            mobilityUpdateRequestService.updateShouldRetryLastError(m.getId(),error);
          }
        } else if (globalProperties.isVerifyResponses() && !EwpRequestConstants.RESPONSE_VERIFIED.equalsIgnoreCase(response.getHttpsecMsg())) {
          event.setObservations(response.getHttpsecMsg());
          event.setStatus(EwpEventConstants.KO);
          idsReintentar.add(m.getId());
        } else {
          mobilityUpdateRequestService.updateRequestIncoming(updateRequest, m.getType());
          mobilityUpdateRequestService.deleteMobillityUpdateRequest(m.getId());
        }

      } catch (EwpIllegalStateException | EwpNotFoundException e) {
        event.setObservations(e.getMessage());
        event.setStatus(EwpEventConstants.KO);
        mobilityUpdateRequestService.deleteMobillityUpdateRequest(m.getId());
      } catch (final Exception e) {
        final String error = "Error al notificar el UPDATE LA. ID actualizacion: " + m.getId();
        log.error(error, getClass().getName(), e);
        idsReintentar.add(m.getId());
        if (Objects.isNull(event.getEventType())) {
          event.setEventType(EwpEventConstants.UPDATE_LA_PROCESSING);
        }
        event.setObservations(!Strings.isNullOrEmpty(e.getMessage()) ? e.getMessage() : e.toString());
        event.setStatus(EwpEventConstants.KO);
      }
      eventService.save(event);
    }
    mobilityUpdateRequestService.changeStatusIsNotProcessingAndRetriesUpdateRequest(idsReintentar);
  }

  private ClientResponse invocarLasUpdate(final MobilityUpdateRequestMDTO mobilityUpdateRequest, EventMDTO eventMDTO)
      throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls(mobilityUpdateRequest.getSendingHeiId().toLowerCase());
    // Para pruebas con dos instancias locales
    // final Map<String, String> urls = new HashMap<>();
    // urls.put(EwpRequestConstants.OUTGOING_MOBILITY_LA_UPDATE, targetUri + "/omobilities/update");

    final ClientRequest requestUpdate = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_UPDATE,
        params, HttpMethodEnum.POST, EwpConstants.OMOBILITY_LAS_API_NAME);
    requestUpdate.setXmlString(new String(mobilityUpdateRequest.getUpdateInformation()) + System.lineSeparator());
    return client.sendRequest(requestUpdate, OmobilityLasUpdateResponse.class, eventMDTO);
  }

}
