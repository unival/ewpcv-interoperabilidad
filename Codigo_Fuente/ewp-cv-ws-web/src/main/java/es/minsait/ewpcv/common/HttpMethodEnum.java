package es.minsait.ewpcv.common;

/**
 * The enum Http method enum.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum HttpMethodEnum {
  GET, POST, PUT
}
