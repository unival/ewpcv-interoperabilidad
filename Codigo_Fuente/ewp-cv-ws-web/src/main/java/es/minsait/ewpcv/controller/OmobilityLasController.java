package es.minsait.ewpcv.controller;

import com.google.common.base.Strings;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpWebApplicationException;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestType;
import es.minsait.ewpcv.security.EwpAuthenticate;
import es.minsait.ewpcv.service.api.*;
import eu.erasmuswithoutpaper.api.architecture.ErrorResponse;
import eu.erasmuswithoutpaper.api.architecture.MultilineString;
import eu.erasmuswithoutpaper.api.architecture.MultilineStringWithOptionalLang;
import eu.erasmuswithoutpaper.api.omobilities.las.cnr.OmobilityLaCnrResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

/**
 * The type Omobility las controller.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@RestController
@RequestMapping(value = "/omobilities", produces = "application/xml; charset=utf-8")
@Slf4j
@Validated
public class OmobilityLasController {

  @Autowired
  private GlobalProperties globalProperties;
  @Autowired
  private OmobilityLasService omobilityService;
  @Autowired
  private ILearningAgreementService laService;
  @Autowired
  private IInstitutionIdentifiersService institutionIdentifiersService;
  @Autowired
  private IMobilityUpdateRequestService mobilityUpdateRequestService;
  @Autowired
  private INotificationService notificationService;
  @Autowired
  private Utils utils;
  @Autowired
  private IEventService eventService;
  @Autowired
  private IAuditConfigService auditConfigService;

  @Value("${max.omobility.ids}")
  private int maxOmobilityIds;

  @EwpAuthenticate
  @GetMapping(path = "/get")
  public ResponseEntity<OmobilityLasGetResponse> getGet(
          @RequestParam(value = "sending_hei_id", required = false) final List<String> sendingHeiId,
          @RequestParam(value = "omobility_id", required = false) final List<String> omobilityId,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /get LA");

    final EventMDTO event =
            utils.initializeAuditableEvent(null, emapParam, request, EwpEventConstants.OMOBILITYLASGET_GET,
                    NotificationTypes.OMOBILITY_LAS, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);


    try {
      doParamsValidationGet(sendingHeiId, omobilityId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final OmobilityLasGetResponse respuesta = new OmobilityLasGetResponse();
    try {
      final List<LearningAgreement> learningAgreementList = omobilityService
              .findOMobilitiesByIdAndSendingInstitutionId(sendingHeiId.get(0), omobilityId, utils.heisCovered(request));
      respuesta.getLa().addAll(learningAgreementList);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion GET /get LA");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/get")
  public ResponseEntity<OmobilityLasGetResponse> postGet(@RequestBody final MultiValueMap<String, String> laGetList,
                                                         final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /get LA");

    final List<String> sendingHeiId = laGetList.get("sending_hei_id");
    final List<String> omobilityId = laGetList.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(null, laGetList, request, EwpEventConstants.OMOBILITYLASGET_POST,
                    NotificationTypes.OMOBILITY_LAS, Objects.nonNull(omobilityId) ? omobilityId.toString() : null);

    try {
      doParamsValidationGet(sendingHeiId, omobilityId);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    final OmobilityLasGetResponse respuesta = new OmobilityLasGetResponse();
    try {
      final List<LearningAgreement> learningAgreementList = omobilityService
              .findOMobilitiesByIdAndSendingInstitutionId(sendingHeiId.get(0), omobilityId, utils.heisCovered(request));
      respuesta.getLa().addAll(learningAgreementList);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /get LA");
      return ResponseEntity.ok(respuesta);
    } catch (final EwpConverterException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.BAD_REQUEST);
    }
  }

  @EwpAuthenticate
  @GetMapping(path = "/index")
  public ResponseEntity<OmobilityLasIndexResponse> getIndex(
          @RequestParam(value = "sending_hei_id", required = false) final List<String> sendingHeiId,
          @RequestParam(value = "receiving_hei_id", required = false) final List<String> receivingHeiId,
          @RequestParam(value = "receiving_academic_year_id", required = false) final List<String> receivingAcademicYearId,
          @RequestParam(value = "global_id", required = false) final String globalId,
          @RequestParam(value = "mobility_type", required = false) final String mobilityType,
          @RequestParam(value = "modified_since", required = false) final List<String> modifiedSince,
          @RequestParam final MultiValueMap<String, String> emapParam, final HttpServletRequest request) {
    log.debug("Inicio invocacion GET /index LA");

    final EventMDTO event = utils.initializeAuditableEvent(receivingHeiId, emapParam, request,
            EwpEventConstants.OMOBILITYLASINDEX_GET, NotificationTypes.OMOBILITY_LAS, null);

    String modifiedSinceString = null;
    try {
      doParamsValidationIndex(sendingHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      dtf.parse(modifiedSinceString);
    }

    final List<String> receivingHeiIdToFilter = utils.filterByPerssions(receivingHeiId, request);
    if (CollectionUtils.isEmpty(receivingHeiIdToFilter)) {
      return ResponseEntity.ok(new OmobilityLasIndexResponse());
    }

    final List<String> laIdsList = omobilityService.getLaIdsByCriteria(sendingHeiId.get(0), receivingHeiIdToFilter,
            CollectionUtils.isNotEmpty(receivingAcademicYearId) ? receivingAcademicYearId.get(0) : null, globalId,
            mobilityType, modifiedSinceString);

    final OmobilityLasIndexResponse respuesta = new OmobilityLasIndexResponse();
    respuesta.getOmobilityId().addAll(laIdsList);

    if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("Fin invocacion GET /index LA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/index")
  public ResponseEntity<OmobilityLasIndexResponse> postIndex(
          @RequestBody final MultiValueMap<String, String> laIndexList, final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /index LA");

    String modifiedSinceString = null;
    final List<String> sendingHeiId = laIndexList.get("sending_hei_id");
    final List<String> receivingHeiId = laIndexList.get("receiving_hei_id");
    final List<String> receivingAcademicYearId = laIndexList.get("receiving_academic_year_id");
    final String globalId = laIndexList.toSingleValueMap().get("global_id");
    final String mobilityType = laIndexList.toSingleValueMap().get("mobility_type");
    final List<String> modifiedSince = laIndexList.get("modified_since");

    final EventMDTO event = utils.initializeAuditableEvent(receivingHeiId, laIndexList, request,
            EwpEventConstants.OMOBILITYLASINDEX_POST, NotificationTypes.OMOBILITY_LAS, null);

    try {
      doParamsValidationIndex(sendingHeiId, receivingAcademicYearId, modifiedSince);
    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }

    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      modifiedSinceString = modifiedSince.get(0).replace(" ", "+");
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      dtf.parse(modifiedSinceString);
    }

    final List<String> receivingHeiIdToFilter = utils.filterByPerssions(receivingHeiId, request);
    if (CollectionUtils.isEmpty(receivingHeiIdToFilter)) {
      return ResponseEntity.ok(new OmobilityLasIndexResponse());
    }

    final List<String> laIdsList = omobilityService.getLaIdsByCriteria(sendingHeiId.get(0), receivingHeiIdToFilter,
            CollectionUtils.isNotEmpty(receivingAcademicYearId) ? receivingAcademicYearId.get(0) : null, globalId,
            mobilityType, modifiedSinceString);

    final OmobilityLasIndexResponse respuesta = new OmobilityLasIndexResponse();
    respuesta.getOmobilityId().addAll(laIdsList);

    if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
      utils.setMessageToEventOK(event, respuesta);
    }
    eventService.save(event);
    log.debug("Fin invocacion POST /index LA");
    return ResponseEntity.ok(respuesta);
  }

  @EwpAuthenticate
  @PostMapping(path = "/update")
  public ResponseEntity<OmobilityLasUpdateResponse> update(@RequestBody final byte[] requestUpdate,
                                                           final HttpServletRequest request) {
    log.debug("Inicio invocacion POST /update LA");

    final EventMDTO event = utils.initializeAuditableEvent(null, null, request,
            EwpEventConstants.OMOBILITYLASUPDATE_POST, NotificationTypes.OMOBILITY_LAS, null);
    event.setRequestParams(new String(requestUpdate));

    OmobilityLasUpdateRequest updateRequest = null;
    try {
      updateRequest = utils.bytesToClassImplByClass(requestUpdate, OmobilityLasUpdateRequest.class);

      final String sendingHeiId = updateRequest.getSendingHeiId();
      final String omobilityId = getOmobilityId(updateRequest);
      final Signature signature = getSignature(updateRequest);


      final MobilityUpdateRequestType mobilityType = getUpdateRequestType(updateRequest);
      final String changesId = getChangesId(updateRequest);

      if (Objects.isNull(sendingHeiId) || Objects.isNull(omobilityId) || Objects.isNull(signature)) {
        throw new EwpWebApplicationException("Sending_Hei_id, Omobility_id and Signature are required",
                HttpStatus.BAD_REQUEST.value());
      }

      event.setChangedElementIds(omobilityId);
      event.setTriggeringHei(sendingHeiId);

      if (!omobilityService.isMatchSendingHeiIdAndOmobilityId(sendingHeiId, omobilityId)) {
        throw new EwpWebApplicationException("Sending-Hei-Id doesn´t match with omobility-Id.",
                HttpStatus.BAD_REQUEST.value());
      }

      final MobilityMDTO mobility = omobilityService.findLastMobilityRevisionByIdAndSendingHeiId(omobilityId, sendingHeiId);
      if (!utils.heisCovered(request).contains(mobility.getReceivingInstitutionId())) {
        throw new EwpWebApplicationException("The invoking host doesen't cover the receiving hei",
                HttpStatus.BAD_REQUEST.value());
      }

      final LearningAgreementMDTO la = laService.buscarLAPorChangesId(omobilityId, changesId);
      if (Objects.isNull(la)) {
        throw new EwpWebApplicationException( "Didn't find any LA with the received changesId",
						HttpStatus.CONFLICT.value() );
      }else if (LearningAgreementStatus.DISCARDED.equals(la.getStatus())) {
        throw new EwpWebApplicationException("Cannot process approval or rejection for a discarded proposal",
                HttpStatus.BAD_REQUEST.value());
    } else if (!LearningAgreementStatus.CHANGES_PROPOSED.equals(la.getStatus())) {
        throw new EwpWebApplicationException("The changes_id are already approved or rejected",
                HttpStatus.BAD_REQUEST.value());
      }
      mobilityUpdateRequestService.saveMobilityUpdateRequest(sendingHeiId, requestUpdate, mobilityType);

      final OmobilityLasUpdateResponse respuesta = new OmobilityLasUpdateResponse();

      final MultilineStringWithOptionalLang multilineStringWithOptionalLang = new MultilineStringWithOptionalLang();
      multilineStringWithOptionalLang.setValue(EwpResponseConstants.LAS_UPDATE_RESPONSE_SUCCESS);
      respuesta.getSuccessUserMessage().add(multilineStringWithOptionalLang);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventOK(event, respuesta);
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /update LA");
      return ResponseEntity.ok(respuesta);

    } catch (final JAXBException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /update LA con errores");
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()));

    } catch (final EwpWebApplicationException ex) {
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      log.debug("Fin invocacion POST /update LA con errores");
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
  }

  @EwpAuthenticate
  @PostMapping(path = "/cnr")
  public ResponseEntity<OmobilityLaCnrResponse> postCnr(@RequestBody final MultiValueMap<String, String> cnrlist,
                                                        final HttpServletRequest request) {

    final String sendingHeiId = cnrlist.toSingleValueMap().get("sending_hei_id");
    final List<String> mobilityIdList = cnrlist.get("omobility_id");

    final EventMDTO event =
            utils.initializeAuditableEvent(sendingHeiId, cnrlist, request, EwpEventConstants.OMOBILITYLASCNR_POST,
                    NotificationTypes.OMOBILITY_LAS, Objects.nonNull(mobilityIdList) ? mobilityIdList.toString() : null);

    if (Strings.isNullOrEmpty(sendingHeiId) || CollectionUtils.isEmpty(mobilityIdList)) {
      final EwpWebApplicationException ex =
              new EwpWebApplicationException("Missing argumanets for notification.", HttpStatus.BAD_REQUEST.value());
      final ErrorResponse errorResponse = new ErrorResponse();
      final MultilineString message = new MultilineString();
      message.setValue(ex.getMessage());
      errorResponse.setDeveloperMessage(message);

      if (utils.getValue(EwpConstants.OMOBILITYLASAUDIT)) {
        utils.setMessageToEventKO(event, errorResponse, ex.getMessage());
      }
      eventService.save(event);
      return new ResponseEntity(errorResponse, HttpStatus.valueOf(ex.getStatus()));
    }
    notificationService.saveNotificationOmobilityLasRefresh(sendingHeiId, mobilityIdList,
            globalProperties.getInstance());

    eventService.save(event);
    return ResponseEntity.ok(new OmobilityLaCnrResponse());
  }


  private void doParamsValidationGet(final List<String> sendingHeiId, final List<String> omobilityId)
          throws EwpWebApplicationException {
    if (Objects.isNull(sendingHeiId) || Objects.isNull(omobilityId)) {
      throw new EwpWebApplicationException("Sending_Hei_id and omobility_id are required",
              HttpStatus.BAD_REQUEST.value());
    }
    if (sendingHeiId.size() > NumberUtils.INTEGER_ONE) {
      throw new EwpWebApplicationException("Sending_Hei_id repeated.", HttpStatus.BAD_REQUEST.value());
    }
    utils.isHeisCoveredByTheHost(sendingHeiId.get(0));
    if (omobilityId.size() > maxOmobilityIds) {
      throw new EwpWebApplicationException("Max number of Omobility id's has exceeded.",
              HttpStatus.BAD_REQUEST.value());
    }
    if (!institutionIdentifiersService.existInstitutionBySchac(sendingHeiId.get(0))) {
      throw new EwpWebApplicationException("Unknown sending_hei_id.", HttpStatus.BAD_REQUEST.value());
    }
  }

  private void doParamsValidationIndex(final List<String> sendingHeiId, final List<String> receivingAcademicYearId,
                                       final List<String> modifiedSince) throws EwpWebApplicationException {
    if (Objects.isNull(sendingHeiId)) {
      throw new EwpWebApplicationException("Sending_Hei_id is required", HttpStatus.BAD_REQUEST.value());
    }
    if (sendingHeiId.size() > 1) {
      throw new EwpWebApplicationException("Sending_Hei_id repeated.", HttpStatus.BAD_REQUEST.value());
    }
    utils.isHeisCoveredByTheHost(sendingHeiId.get(0));
    if (CollectionUtils.isNotEmpty(modifiedSince)) {
      if (modifiedSince.size() > 1) {
        throw new EwpWebApplicationException("Multiple modified_since parameters.", HttpStatus.BAD_REQUEST.value());
      }
      if (!isValidFormatModifiedSince(modifiedSince.get(0))) {
        throw new EwpWebApplicationException("Wrong format of modified_since parameter.",
                HttpStatus.BAD_REQUEST.value());
      }
    }
    if (!CollectionUtils.isEmpty(receivingAcademicYearId)) {
      for (final String yearId : receivingAcademicYearId) {
        if (!utils.isValidFormatReceivingAcademicYear(yearId)) {
          throw new EwpWebApplicationException("Receiving_academic_year_id in incorrect format.",
                  HttpStatus.BAD_REQUEST.value());
        }
      }
    }
  }

  private boolean isValidFormatModifiedSince(final String modifiedSince) {
    return modifiedSince.matches(
            "-?([1-9][0-9]{3,}|0[0-9]{3})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\\.[0-9]+)?|(24:00:00(\\.0+)?))(Z|(\\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?");
  }

  private String getOmobilityId(final OmobilityLasUpdateRequest updateRequest) {
    if (Objects.nonNull(updateRequest.getApproveProposalV1())) {
      return updateRequest.getApproveProposalV1().getOmobilityId();
    } else {
      return Objects.nonNull(updateRequest.getCommentProposalV1())
              ? updateRequest.getCommentProposalV1().getOmobilityId()
              : null;
    }
  }

  private Signature getSignature(final OmobilityLasUpdateRequest updateRequest) {
    if (Objects.nonNull(updateRequest.getApproveProposalV1())) {
      return updateRequest.getApproveProposalV1().getSignature();
    } else {
      return Objects.nonNull(updateRequest.getCommentProposalV1()) ? updateRequest.getCommentProposalV1().getSignature()
              : null;
    }
  }

  private String getChangesId(final OmobilityLasUpdateRequest updateRequest) {
    if (Objects.nonNull(updateRequest.getApproveProposalV1())) {
      return updateRequest.getApproveProposalV1().getChangesProposalId();
    } else {
      return Objects.nonNull(updateRequest.getCommentProposalV1())
              ? updateRequest.getCommentProposalV1().getChangesProposalId()
              : null;
    }
  }

  private MobilityUpdateRequestType getUpdateRequestType(final OmobilityLasUpdateRequest updateRequest) {
    if (Objects.nonNull(updateRequest.getApproveProposalV1())) {
      return MobilityUpdateRequestType.APPROVE_PROPOSAL_V1;
    } else {
      return MobilityUpdateRequestType.COMMENT_PROPOSAL_V1;
    }
  }


}
