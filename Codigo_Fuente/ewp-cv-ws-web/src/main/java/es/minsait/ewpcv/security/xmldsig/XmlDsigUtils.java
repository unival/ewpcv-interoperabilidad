package es.minsait.ewpcv.security.xmldsig;

import es.minsait.ewpcv.error.EwpXMLDsigException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import java.util.Properties;

/**
 * The type Xml dsig utils.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
public class XmlDsigUtils {

  private static final Logger LOG = Logger.getLogger(XmlDsigUtils.class.getName());

  private Properties properties;

  public void setProperties(final Properties properties) {
    this.properties = properties;
  }

  public Properties getProperties() {
    return this.properties;
  }

  @Value("${ewp.xmldsig.keystore.location}")
  private String rutaKeystore;

  @Value("${ewp.xmldsig.keystore.password}")
  private String keystorePassword;

  @Value("${ewp.xmldsig.keystore.certificate.alias}")
  private String keyAlias;

  @Value("${ewp.xmldsig.keystore.certificate.aliaspass}")
  private String aliasPassword;

  @Value("${ewp.xmldsig.keystore.type}")
  private String keystoreType;

  @Value("${ewp.xmldsig.algoritmofirma}")
  private String algoritmoFirma;

  public String changeOutboundMessage(final String currentEnvelope) throws EwpXMLDsigException {

    String result = "";

    final TransformUtils transformer = new TransformUtils();
    final SignatureUtils sigutil = new SignatureUtils();

    Document doc = null;
    try {
      doc = transformer.string2Document(currentEnvelope);
    } catch (final Exception e) {
      LOG.error("Error al generar el documento ", e);
    }
    try {
      LOG.debug("Generando la firma");
      sigutil.firma(doc, rutaKeystore, keystorePassword, keyAlias, aliasPassword, keystoreType, algoritmoFirma);
      LOG.debug("Generando la cadena");
      result = transformer.document2String(doc);
    } catch (final Exception e) {
      LOG.error("Error al generar la firma", e);
      throw new EwpXMLDsigException("Error al generar la firma de la peticion");
    }

    return result;
  }
}
