import com.google.common.base.Strings;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.collections.CollectionUtils;

import es.minsait.ewpcv.client.ClientRequest;
import es.minsait.ewpcv.client.ClientResponse;
import es.minsait.ewpcv.client.RestClient;
import es.minsait.ewpcv.common.*;
import es.minsait.ewpcv.converter.api.IOrganizationUnitConverter;
import es.minsait.ewpcv.converters.impl.OrganizationUnitConverter;
import es.minsait.ewpcv.error.EwpOperationNotFoundException;
import es.minsait.ewpcv.repository.impl.OrganizationUnitDAO;
import es.minsait.ewpcv.security.HttpSignature;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.service.impl.OrganizationUnitServiceImpl;
import eu.erasmuswithoutpaper.api.discovery.Manifest;
import eu.erasmuswithoutpaper.api.echo.Response;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasIndexResponse;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.ImobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.OmobilitiesIndexResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasIndexResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateResponse;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("/application-test.properties")
@ContextConfiguration(classes = {GlobalProperties.class, RestClient.class, EwpKeyStore.class, RegistryClient.class,
    HttpSignature.class, MonitoringComponent.class, Utils.class})
@Ignore
public class ServicesTest {

  @MockBean
  IAuditConfigService auditConfigService;

  @MockBean
  INotificationService notificationService;

  @MockBean
  IEventService eventService;

  @Autowired
  RestClient cliente;
  @Autowired
  GlobalProperties globalProperties;

  @Autowired
  RegistryClient registry;

  @Autowired
  Utils utils;


  /***************************
   * REGISTRY
   ***************************/
  @Test
  public void testRegistry() {
    String heiId = "brussels.uni-foundation.eu";
    List heis;

    System.out.println("************* EWP Instance *************");
    System.out.println("EWP Instance: ");
    final HeiEntry instance = registry.getEwpInstanceHeiUrls(heiId);
    System.out.println(instance);

    System.out.println("EWP Instances : ");
    final List instanceHeis = utils.filterHeisByProperties(registry.getEwpInstanceHeisWithUrls(), heiId);
    System.out.println(instanceHeis.get(0));

    System.out.println("************* Echo *************");
    System.out.println("Echo: ");
    final HeiEntry echo = registry.getEchoHeiUrls(heiId);
    System.out.println(echo);

    System.out.println("Echo heis: ");
    final List echoHeis = registry.getEchoHeis();
    heis = utils.filterHeisByProperties(echoHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Factsheet *************");
    System.out.println("Factsheet: ");
    final HeiEntry factsheet = registry.getFactsheetHeiUrls(heiId);
    System.out.println(factsheet);

    System.out.println("Factsheet heis: ");
    final List factsheetHeis = registry.getFactsheetHeisWithUrls();
    heis = utils.filterHeisByProperties(factsheetHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Institution *************");
    System.out.println("Institution: ");
    final HeiEntry institutions = registry.getInstitutionsHeiUrls(heiId);
    System.out.println(institutions);

    System.out.println("Institution heis: ");
    final List institutionHeis = registry.getInstitutionsHeiUrls();
    heis = utils.filterHeisByProperties(institutionHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Ounits *************");
    System.out.println("Ounits: ");
    final HeiEntry ounit = registry.getEwpOrganizationUnitHeiUrls(heiId);
    System.out.println(ounit);

    System.out.println("Ounits heis: ");
    final List ounitHeis = registry.getEwpOrganizationUnitHeisWithUrls();
    heis = utils.filterHeisByProperties(ounitHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Courses *************");
    System.out.println("courses: ");
    final HeiEntry courses = registry.getCoursesHeiUrls(heiId);
    System.out.println(courses);

    System.out.println("Courses heis: ");
    final List courseHeis = registry.getCoursesHeisWithUrls();
    heis = utils.filterHeisByProperties(courseHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Courses Replication *************");
    System.out.println("courses replication: ");
    final HeiEntry coursesRep = registry.getCoursesReplicationHeiUrls(heiId);
    System.out.println(coursesRep);

    System.out.println("Courses Replication heis: ");
    final List courseRepHeis = registry.getCoursesReplicationHeisWithUrls();
    heis = utils.filterHeisByProperties(courseRepHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Omobilities *************");
    System.out.println("Omobilities: ");
    final HeiEntry omobilities = registry.getOmobilitiesHeiUrls(heiId);
    System.out.println(omobilities);

    System.out.println("Omobilities CNR: ");
    final HeiEntry omobilitiesCnr = registry.getOmobilitiesCnrHeiUrls(heiId);
    System.out.println(omobilitiesCnr);

    System.out.println("Omobilities Heis: ");
    final List omobilitiesHeis = registry.getOmobilitiesHeisWithUrls();
    heis = utils.filterHeisByProperties(omobilitiesHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("Omobilities CNR Heis: ");
    final List omobilitiesCnrHeis = registry.getOmobilitiesCnrHeisWithUrls();
    heis = utils.filterHeisByProperties(omobilitiesCnrHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Imobilities *************");
    System.out.println("Imobilities: ");
    final HeiEntry imobilities = registry.getImobilitiesHeiUrls(heiId);
    System.out.println(imobilities);

    System.out.println("Imobilities CNR: ");
    final HeiEntry imobilitiesCnr = registry.getImobilitiesHeiUrls(heiId);
    System.out.println(imobilitiesCnr);

    System.out.println("Imobilities Heis: ");
    final List imobilitiesHeis = registry.getImobilitiesHeisWithUrls();
    heis = utils.filterHeisByProperties(imobilitiesHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("Imobilities CNR Heis: ");
    final List imobilitiesCnrHeis = registry.getImobilitiesCnrHeisWithUrls();
    heis = utils.filterHeisByProperties(imobilitiesCnrHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* Omobilities LAs *************");
    System.out.println("OLA: ");
    final HeiEntry ola = registry.getOmobilitiesLasHeiUrls(heiId);
    System.out.println(ola);

    System.out.println("OLA CNR: ");
    final HeiEntry olaCnr = registry.getOmobilitiesLACnrHeiUrls(heiId);
    System.out.println(olaCnr);

    System.out.println("OLA Heis: ");
    final List olaHeis = registry.getOmobilitiesLaHeisWithUrls();
    heis = utils.filterHeisByProperties(olaHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* IIAs *************");
    System.out.println("IIA: ");
    final HeiEntry iia = registry.getIiaHeiUrls(heiId);
    System.out.println(iia);

    System.out.println("IIA CNR: ");
    final HeiEntry iiaCnr = registry.getIiaCnrHeiUrls(heiId);
    System.out.println(iiaCnr);

    System.out.println("IIA Heis: ");
    final List iiaHeis = registry.getIiaHeisWithUrls();
    heis = utils.filterHeisByProperties(iiaHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("IIA CNR Heis: ");
    final List iiaCnrHeis = registry.getIiaCnrHeisWithUrls();
    heis = utils.filterHeisByProperties(iiaCnrHeis, heiId);
    System.out.println(CollectionUtils.isNotEmpty(heis) ? heis.get(0) :"no se encuentra");

    System.out.println("************* IIAs Approval *************");
    System.out.println("IIA approval: ");
    final HeiEntry iiaAproval = registry.getIiaApprovalHeiUrls(heiId);
    System.out.println(iiaAproval);

    System.out.println("IIA CNR: ");
    final HeiEntry iiaAprovalCnr = registry.getIiaApprovalCnrHeiUrls(heiId);
    System.out.println(iiaAprovalCnr);

  }

  /***************************
   * DISCOVERY
   ***************************/
  @Test
  public void testDiscovery() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpDiscoveryHeiUrls("uv.es");
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.DISCOVERY_URL_KEY, new HashMap<>(), HttpMethodEnum.GET,
            EwpConstants.DISCOVERY_API_NAME);
    final ClientResponse response = cliente.sendRequest(req, Manifest.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }


  /***************************
   * ECHO
   ***************************/
  @Test
  public void testGetEcho() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEchoHeiUrls("demo.muni.cz");
    Map<String, List<String>> params = new HashMap<>();
    params.put("echo", Arrays.asList("a", "b", "a"));
    params.put("echo2", Arrays.asList("c", "d"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ECHO_URL_KEY, params, HttpMethodEnum.GET,
        EwpConstants.ECHO_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/echo");
    final ClientResponse response = cliente.sendRequest(req, Response.class, null);
    System.out.println("--print--" + response.getResult().toString());
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostEcho() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEchoHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put("echo", Arrays.asList("a", "b", "a"));
    params.put("echo2", Arrays.asList("c", "d"));
    params.put("aa", Arrays.asList("e", "f"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ECHO_URL_KEY, params, HttpMethodEnum.POST,
        EwpConstants.ECHO_API_NAME);;
    req.setUrl(globalProperties.getBaseUri() + "/echo");
    final ClientResponse response = cliente.sendRequest(req, Response.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }



  /***************************
   * IIAS
   ***************************/
  @Test
  public void testGetIiaIndex() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    // params().put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList("ewp18-bo.staging.moveon4.com"));
    // params().put(EwpRequestConstants.IIAS_PARTNER_HEI_ID, Arrays.asList("uv.es"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IIAS_INDEX_URL_KEY, params,
        HttpMethodEnum.GET, EwpConstants.IIAS_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/iias/index");

    final ClientResponse response = cliente.sendRequest(req, IiasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostIiaIndex() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaHeiUrls("ewp18-bo.staging.moveon4.com");

    final ClientRequest req = new ClientRequest();
    req.setUrl(hei.getUrls().get(EwpRequestConstants.IIAS_INDEX_URL_KEY));
    req.setMethod(HttpMethodEnum.POST);
    req.getParams().put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList("uv.es"));
    req.getParams().put(EwpRequestConstants.IIAS_RECEIVING_ACADEMIC_YEAR_ID, Arrays.asList("2010%2F2011"));
    req.setHttpsecClient(hei.isClientHttpSignature());
    req.setHttpsecServer(hei.isClientHttpSignature());
    req.setTlssec(hei.isClientTlsCert());
    final ClientResponse response = cliente.sendRequest(req, IiasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testGetIiaGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaHeiUrls("usal.es");

    final ClientRequest req = new ClientRequest();
     req.setUrl(globalProperties.getBaseUri() + "/iias/get");
    req.setUrl(hei.getUrls().get(EwpRequestConstants.IIAS_GET_URL_KEY));
//    req.setUrl("https://localhost/iias/get");

    req.setMethod(HttpMethodEnum.POST);
    req.getParams().put(EwpRequestConstants.IIAS_HEI_ID, Arrays.asList("uv.es"));
    req.getParams().put(EwpRequestConstants.IIAS_IIA_ID, Arrays.asList("7d564370-d92d-483b-8eb3-da624cc77536"));
    req.setHttpsecClient(hei.isClientHttpSignature());
    req.setHttpsecServer(hei.isClientHttpSignature());
    req.setTlssec(hei.isClientTlsCert());
    final ClientResponse response = cliente.sendRequest(req, IiasGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPOSTIiaCnr() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaHeiUrls("usal.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IIAS_CNR_NOTIFIER_HEI_ID, Arrays.asList("UV.ES"));
    params.put(EwpRequestConstants.IIAS_CNR_IIA_ID, Arrays.asList("CC94E024-FC7E-4DF6-E053-1FC616AC138D"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IIAS_CNR_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.IIA_CNR_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/iias/cnr");

    final ClientResponse response = cliente.sendRequest(req, IiasGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  /***************************
   * IIA APPROVAL
   ***************************/
  @Test
  public void testGetIiaApprovalGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaApprovalHeiUrls("um.es");
    Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.IIAS_APPROVAL_APPROVING_HEI_ID, Arrays.asList("um.es"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_OWNER_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_IIA_ID, Arrays.asList("c3ecc9df-3645-6530-e053-1fc616acc2c8"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_GET_URL_KEY, params,
        HttpMethodEnum.GET, EwpConstants.IIAS_APPROVAL_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/iias/approval/get");

    final ClientResponse response = cliente.sendRequest(req, IiasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostIiaApprovalGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaApprovalHeiUrls("um.es");
    Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.IIAS_APPROVAL_APPROVING_HEI_ID, Arrays.asList("um.es"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_OWNER_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_IIA_ID, Arrays.asList("c3ecc9df-3645-6530-e053-1fc616acc2c8"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.IIAS_APPROVAL_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/iias/approval/get");

    final ClientResponse response = cliente.sendRequest(req, IiasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostIiaApprovalCnr() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getIiaApprovalHeiUrls("um.es");
    Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.IIAS_APPROVAL_APPROVING_HEI_ID, Arrays.asList("UM.ES"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_OWNER_HEI_ID, Arrays.asList("UV.ES"));
    params.put(EwpRequestConstants.IIAS_APPROVAL_IIA_ID, Arrays.asList("C4288536-85C1-3A78-E053-1FC616AC8B51"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IIAS_APPROVAL_CNR_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.IIA_APPROVAL_CNR_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/iias/approval/cnr");

    final ClientResponse response = cliente.sendRequest(req, IiasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  /***************************
   * FACTSHEET
   ***************************/
  @Test
  public void testGetFactsheet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getFactsheetHeiUrls("uv.es");
    Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.FACTSHEET_HEI_ID, Arrays.asList("UV.ES"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.FACTSHEET_GET_URL_KEY, params,
        HttpMethodEnum.GET, EwpConstants.FACTSHEET_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/factsheet/get");

    final ClientResponse response = cliente.sendRequest(req, FactsheetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostFactsheet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getFactsheetHeiUrls("uv.es");
    Map<String, List<String>> params = new HashMap<>();

    params.put(EwpRequestConstants.FACTSHEET_HEI_ID, Arrays.asList("UV.ES"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.FACTSHEET_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.FACTSHEET_API_NAME);;
    // req.setUrl(globalProperties.getBaseUri() + "/factsheet/get");
    final ClientResponse response = cliente.sendRequest(req, FactsheetResponse.class, null);

    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  /***************************
   * MOBILITY LA
   ***************************/

  @Test
  public void testGetMobilityIndex() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls("usal.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList("uv.es"));
    // params.put("receiving_hei_id", Arrays.asList("usal.es"));
    // params.put("modified_since", Arrays.asList("2019-04-02T15:19:21+01:00"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_INDEX, params,
        HttpMethodEnum.GET, EwpConstants.OMOBILITY_LAS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/omobilities/index");

    final ClientResponse response = cliente.sendRequest(req, OmobilityLasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostMobilityIndex() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls("usal.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList("uv.es"));
    // params.put("receiving_hei_id", Arrays.asList("usal.es"));
    // params.put("modified_since", Arrays.asList("2019-04-02T15:19:21+01:00"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_INDEX, params,
        HttpMethodEnum.POST, EwpConstants.OMOBILITY_LAS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/omobilities/index");

    final ClientResponse response = cliente.sendRequest(req, OmobilityLasIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }


  @Test
  public void testGetMobilityLaGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls("usal.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.OMOBILITY_LA_OMOBILITY_ID, Arrays.asList("CC970086-D949-54B4-E053-1FC616AC7854"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_GET, params,
        HttpMethodEnum.GET, EwpConstants.OMOBILITY_LAS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/omobilities/get");

    final ClientResponse response = cliente.sendRequest(req, OmobilityLasGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testPostMobilityLaGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls("usal.es");
    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OMOBILITY_LA_SENDING_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.OMOBILITY_LA_OMOBILITY_ID, Arrays.asList("CC970086-D949-54B4-E053-1FC616AC7854"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_LA_GET, params,
        HttpMethodEnum.POST, EwpConstants.OMOBILITY_LAS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/omobilities/get");

    final ClientResponse response = cliente.sendRequest(req, OmobilityLasGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testUpdateLa() throws DatatypeConfigurationException, IOException {
    final HeiEntry hei = registry.getOmobilitiesLasHeiUrls("uv.es");
    final ClientRequest req = new ClientRequest();
    // req.setUrl(globalProperties.getBaseUri() + "/omobilities/update");
    req.setUrl(hei.getUrls().get(EwpRequestConstants.OUTGOING_MOBILITY_LA_UPDATE));

    // req.setUrl("https://webgesdes.uv.es/uvEWPWeb/omobilities/update");
    // final OmobilityLasUpdateRequest request = new OmobilityLasUpdateRequest();
    // final ApproveProposalV1 a = new ApproveProposalV1();
    // final Signature s = new Signature();
    // request.setSendingHeiId("uv.es");
    // a.setOmobilityId("CD389E0D-494D-AA2A-E053-0E029C93416C");
    // a.setChangesProposalId("Student_Test_Global_id");
    // s.setSignerName("Paweł Tomasz Kowalski");
    // s.setSignerPosition("Mobility coordinator");
    // s.setSignerEmail("pawel.kowalski@example.com");
    // s.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar("2000-01-01T00:00:00.000Z"));
    // s.setSignerApp("USOS");
    // a.setSignature(s);
    // request.setApproveProposalV1(a);

    final String xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
            + "<omobility-las-update-request xmlns=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd\" xmlns:ns2=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd\">"
            + "<sending-hei-id>uv.es</sending-hei-id>" + "<approve-proposal-v1>"
            + "<omobility-id>CD389E0D-494D-AA2A-E053-0E029C93416C</omobility-id>"
            + "<changes-proposal-id>Student_Test_Global_id</changes-proposal-id>" + "<signature>"
            + "<ns2:signer-name>Paweł Tomasz Kowalski</ns2:signer-name>"
            + "<ns2:signer-position>Mobility coordinator</ns2:signer-position>"
            + "<ns2:signer-email>pawel.kowalski@example.com</ns2:signer-email>"
            + "<ns2:timestamp>2000-01-01T00:00:00.000Z</ns2:timestamp>" + "<ns2:signer-app>USOS</ns2:signer-app>"
            + "</signature>" + "</approve-proposal-v1>" + "</omobility-las-update-request>";
    // final String xmlString = getStringFromObject(request, false);
    req.setXmlString(xmlString);
    req.setMethod(HttpMethodEnum.POST);
    req.setHttpsecClient(hei.isClientHttpSignature());
    req.setHttpsecServer(hei.isClientHttpSignature());
    req.setTlssec(hei.isClientTlsCert());
    req.setApiName(EwpConstants.OMOBILITY_LAS_API_NAME);
    final ClientResponse response = cliente.sendRequest(req, OmobilityLasUpdateResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  /***************************
   * OUTGOING MOBILITIES
   ***************************/

  @Test
  public void testGetOutgoingMobilityGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList("CC970086-D949-54B4-E053-1FC616AC7854"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OUTGOING_MOBILITY_GET_URL, params,
        HttpMethodEnum.GET, EwpConstants.OMOBILITIES_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/outgoingmobilities/get");

    final ClientResponse response = cliente.sendRequest(req, OmobilitiesGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testGetOutgoingMobilityIndex() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getOmobilitiesHeiUrls("ipiaget.org");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList("uv.es"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.OMOBILITIES_INDEX_URL_KEY, params,
        HttpMethodEnum.GET, EwpConstants.OMOBILITIES_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/outgoingmobilities/index");
    req.setUrl(hei.getUrls().get(EwpRequestConstants.OMOBILITIES_INDEX_URL_KEY));
    req.setMethod(HttpMethodEnum.POST);
    req.getParams().put(EwpRequestConstants.OUTGOING_MOBILITY_GET_SENDING_HEI_ID, Arrays.asList("ipiaget.org"));
    req.setHttpsecClient(hei.isClientHttpSignature());
    req.setHttpsecServer(hei.isClientHttpSignature());
    req.setTlssec(hei.isClientTlsCert());
    final ClientResponse response = cliente.sendRequest(req, OmobilitiesIndexResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  /***************************
   * INCOMING MOBILITIES
   ***************************/

  @Test
  public void testGetIncomingMobilityGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getImobilitiesHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.IMOBILITIES_CNR_RECEIVING_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.IMOBILITIES_GET_OMOBILITY_ID, Arrays.asList("CC970086-D949-54B4-E053-1FC616AC7854"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.IMOBILITIES_GET_URL_KEY, params,
        HttpMethodEnum.GET, EwpConstants.IMOBILITIES_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/incomingmobilities/get");

    final ClientResponse response = cliente.sendRequest(req, ImobilitiesGetResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }


  /***************************
   * INSTITUTIONS
   ***************************/
  @Test
  public void testInstitutionsGet() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getInstitutionsHeiUrls("ewp18-bo.staging.moveon4.com");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.INSTITUTIONS_GET_HEI_ID, Arrays.asList("ewp18-bo.staging.moveon4.com"));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.INSTITUTIONS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/institutions/get");

    final ClientResponse response = cliente.sendRequest(req, InstitutionsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testInstitutionsRegistry() throws EwpOperationNotFoundException {
    final List<HeiEntry> heis = registry.getInstitutionsHeiUrls();

    String heiId = "uv.es";
    HeiEntry hei = heis.stream().filter(h -> h.getId().equalsIgnoreCase(heiId)).findFirst().get();


    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.INSTITUTIONS_GET_HEI_ID, Arrays.asList(hei.getId()));

    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.INSTITUTIONS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/institutions/get");

    final ClientResponse response = cliente.sendRequest(req, InstitutionsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());

  }

  @Test
  public void testVariosRegistry() {
    final RSAPublicKey rsaPublicKey =
            registry.findRsaPublicKey("52aa73e6c55b11a3be79637e57d766fd9f95622031e4e3f6ae8bfa629b8b8645");
    registry.getHeisCoveredByClientKey(rsaPublicKey).stream().collect(Collectors.toList());
  }



  private String getStringFromObject(final Object body, final boolean formatedOutput) throws IOException {
    try {
      final String string;
      final JAXBContext jaxbContext = JAXBContext.newInstance(body.getClass());
      final Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatedOutput);
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        marshaller.marshal(body, baos);
        string = baos.toString();
      }
      return string;
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Test
  public void testInstitutionsJobRefresh() {
    final List<HeiEntry> heis = registry.getInstitutionsHeiUrls();
    final List<HeiEntry> heiFilter =
            heis.stream().filter(h -> h.getId().equalsIgnoreCase("massey.edu.si")).collect(Collectors.toList());

    try {
      final ClientResponse response = invocarInstitutionsGet(heiFilter.get(0));
      System.out.println("asdf");
    } catch (final EwpOperationNotFoundException e) {
      e.printStackTrace();
    }


  }


  private ClientResponse invocarInstitutionsGet(final HeiEntry hei) throws EwpOperationNotFoundException {
    final Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.INSTITUTIONS_GET_HEI_ID, Arrays.asList(hei.getId()));
    final ClientRequest requestIndex =
            formarPeticion(hei, EwpRequestConstants.INSTITUTIONS_GET_URL_KEY, params, HttpMethodEnum.POST);
    return cliente.sendRequest(requestIndex, InstitutionsResponse.class, null);
  }


  private ClientRequest formarPeticion(final HeiEntry hei, final String urlKey, final Map<String, List<String>> params,
                                       final HttpMethodEnum method) throws EwpOperationNotFoundException {
    final String url;

    final Map<String, String> urls = hei.getUrls();

    url = urls.get(urlKey);
    if (Strings.isNullOrEmpty(url)) {
      throw new EwpOperationNotFoundException("No se ha encontrado " + urlKey + " para el heiId " + hei.getId(), 404);
    }

    final ClientRequest request = new ClientRequest();
    request.setUrl(url);
    request.setHeiId(hei.getId());
    request.setMethod(method);
    request.setParams(params);
    request.setHttpsecClient(hei.isClientHttpSignature());
    request.setHttpsecServer(hei.isClientHttpSignature());
    request.setTlssec(hei.isClientTlsCert());

    return request;
  }


  /***************************
   * ORGANIZATION UNITS
   ***************************/
  @Test
  public void testOunitsGetValidOunitId() {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");
    final ClientRequest req = new ClientRequest();
    req.setUrl(globalProperties.getBaseUri() + "/ounits/get");
    //req.setUrl(hei.getUrls().get(EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY));
    req.setMethod(HttpMethodEnum.GET);
    req.getParams().put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("muni.cz"));
    req.getParams().put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID,
            Arrays.asList("muni-331"));
    req.setHttpsecClient(hei.isClientHttpSignature());
    req.setHttpsecServer(hei.isClientHttpSignature());
    req.setTlssec(hei.isClientTlsCert());
    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testOunitsGetInvalidOunitId() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID,
            Arrays.asList("CA352E8D-28AF-2F11-E053-1FC616AC3D07"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/ounits/get");

    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testOunitsGetValidOunitCode() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_CODE, Arrays.asList("ounit.child.uv"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/ounits/get");

    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testOunitsGetInvalidOunitCode() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");
    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_CODE, Arrays.asList("ounit.ROOT1.uv"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/ounits/get");

    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }

  @Test
  public void testOunitsGetAllParams() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("uv.es"));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID,
            Arrays.asList("CA352E8D-28AF-2F11-E053-1FC616AC3D06"));
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_CODE, Arrays.asList("ounit.ROOT.uv"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/ounits/get");


    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatusCode());
  }

  @Test
  public void testOunitsGetEmptyParams() throws EwpOperationNotFoundException {
    final HeiEntry hei = registry.getEwpOrganizationUnitHeiUrls("uv.es");

    Map<String, List<String>> params = new HashMap<>();
    params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_HEI_ID, Arrays.asList("uv.es"));
    // params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_ID,
    // Arrays.asList("CA352E8D-28AF-2F11-E053-1FC616AC3D06"));
    // params.put(EwpRequestConstants.ORGANIZATIONUNIT_GET_OUNIT_CODE, Arrays.asList("ounit.ROOT.uv"));
    final ClientRequest req = utils.formarPeticion(hei, EwpRequestConstants.ORGANIZATIONUNIT_GET_URL_KEY, params,
        HttpMethodEnum.POST, EwpConstants.ORGANIZATIONAL_UNITS_API_NAME);
    // req.setUrl(globalProperties.getBaseUri() + "/ounits/get");

    final ClientResponse response = cliente.sendRequest(req, OunitsResponse.class, null);
    Assert.assertEquals(HttpServletResponse.SC_BAD_REQUEST, response.getStatusCode());
  }

  @Test
  public void testFileGet() throws EwpOperationNotFoundException {

    Map<String, List<String>> params = new HashMap<>();
    params.put("file_id", Arrays.asList("10f7490b-dc88-0ab1-e063-011a160ac53f"));
    final ClientRequest request = new ClientRequest();
    request.setUrl("https://localhost/files/get");
    request.setHeiId("uv.es");
    request.setMethod(HttpMethodEnum.GET);
    request.setParams(params);
    request.setHttpsecClient(true);
    request.setHttpsecServer(true);
    request.setTlssec(false);
    request.setApiName("FILE");
    final ClientResponse response = cliente.sendRequest(request, null, null);
    Assert.assertEquals(HttpServletResponse.SC_OK, response.getStatusCode());
  }
}