package es.minsait.ewpcv.security;

import es.minsait.ewpcv.common.EwpRequestConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tomitribe.auth.signatures.Base64;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * Test para verificar el correcto funcionamiento de la clase DigestsUtils
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class DigestsUtilsTest {


  HashMap<String, String[]> paramMap;

  String queryString;
  String encodedQueryString;
  String emptyStringDigest;
  String queryStringDigest;
  String encodedQueryStringDigest;

  String xmlUnix ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
          + "<req:omobility-las-update-request xmlns:req=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:la=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd\" xsi:schemaLocation=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd\">\n"
          + "  <req:sending-hei-id>uv.es</req:sending-hei-id>\n" + "  <req:comment-proposal-v1>\n"
          + "    <req:omobility-id>ce9eb07c-3670-135c-e053-24029c93cd9a</req:omobility-id>\n"
          + "    <req:changes-proposal-id>ce9eb07c-3674-135c-e053-24029c93cd9a</req:changes-proposal-id>\n"
          + "    <req:comment>Rechazo...</req:comment>\n" + "    <req:signature>\n"
          + "      <la:signer-name>Manuel Martín</la:signer-name>\n"
          + "      <la:signer-position>Receiving Responsible</la:signer-position>\n"
          + "      <la:signer-email>manumohedano@usal.es</la:signer-email>\n"
          + "      <la:timestamp>2021-10-18T14:21:23+02:00</la:timestamp>\n"
          + "      <la:signer-app>IRIS</la:signer-app>\n" + "    </req:signature>\n" + "  </req:comment-proposal-v1>\n"
          + "</req:omobility-las-update-request>\n";

  String xmlWindows ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
          + "<req:omobility-las-update-request xmlns:req=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:la=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd\" xsi:schemaLocation=\"https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd https://raw.githubusercontent.com/erasmus-without-paper/ewp-specs-api-omobility-las/stable-v1/endpoints/update-request.xsd\">\r\n"
          + "  <req:sending-hei-id>uv.es</req:sending-hei-id>\n" + "  <req:comment-proposal-v1>\r\n"
          + "    <req:omobility-id>ce9eb07c-3670-135c-e053-24029c93cd9a</req:omobility-id>\r\n"
          + "    <req:changes-proposal-id>ce9eb07c-3674-135c-e053-24029c93cd9a</req:changes-proposal-id>\r\n"
          + "    <req:comment>Rechazo...</req:comment>\r\n" + "    <req:signature>\r\n"
          + "      <la:signer-name>Manuel Martín</la:signer-name>\r\n"
          + "      <la:signer-position>Receiving Responsible</la:signer-position>\r\n"
          + "      <la:signer-email>manumohedano@usal.es</la:signer-email>\r\n"
          + "      <la:timestamp>2021-10-18T14:21:23+02:00</la:timestamp>\r\n"
          + "      <la:signer-app>IRIS</la:signer-app>\r\n" + "    </req:signature>\r\n" + "  </req:comment-proposal-v1>\r\n"
          + "</req:omobility-las-update-request>\r\n";

  byte[] xmlUnixBytes = xmlUnix.getBytes();
  byte[] xmlWindowsBytes = xmlWindows.getBytes();
  String xmlUnixDigest;
  String xmlWindowsDigest;

  byte[] xmlUnixBytesIso = new String(xmlUnix.getBytes(StandardCharsets.ISO_8859_1)).getBytes(StandardCharsets.ISO_8859_1);
  byte[] xmlWindowsBytesIso =new String(xmlWindows.getBytes(StandardCharsets.ISO_8859_1)).getBytes(StandardCharsets.ISO_8859_1);
  String xmlUnixDigestIso;
  String xmlWindowsDigestIso;

  String calculateDigest(final byte[] bodyBytes) throws NoSuchAlgorithmException {
    final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bodyBytes);
    final String stringDigest = new String(Base64.encodeBase64(digest));
    return stringDigest;
  }

  HttpServletRequest request;

  @Before
  public void setUp() throws NoSuchAlgorithmException, UnsupportedEncodingException {
    paramMap  = new HashMap<String, String[]>() {
      {
        put(EwpRequestConstants.IIAS_HEI_ID, new String[] {"demo.usos.edu.pl"});
        put(EwpRequestConstants.IIAS_PARTNER_HEI_ID, new String[] {"uv.es"});
        put(EwpRequestConstants.IIAS_RECEIVING_ACADEMIC_YEAR_ID, new String[] {"2010/2011", "2011/2012"});
        put(EwpRequestConstants.IIAS_MODIFIED_SINCE, new String[] {"2004-02-12T15:19:21+01:00"});
      }
    };

    emptyStringDigest = calculateDigest(StringUtils.EMPTY.getBytes());
    queryString = DigestsUtils.paramsData2String(paramMap, null);
    queryStringDigest = calculateDigest(queryString.getBytes());
    encodedQueryString = DigestsUtils.paramsData2String(paramMap, StandardCharsets.UTF_8);
    encodedQueryStringDigest = calculateDigest(encodedQueryString.getBytes());

    xmlUnixDigest = calculateDigest(xmlUnixBytes);
    xmlWindowsDigest = calculateDigest(xmlWindowsBytes);
    xmlUnixDigestIso = calculateDigest(xmlUnixBytesIso);
    xmlWindowsDigestIso = calculateDigest(xmlWindowsBytesIso);
  }

  @Test
  public void testGetAllDigestsApplicationFormUrlEncodedEmpty() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    when(request.getParameterMap()).thenReturn(Collections.emptyMap());
    when(request.getQueryString()).thenReturn("");
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(emptyStringDigest));
  }

  @Test
  public void testGetAllDigestsApplicationFormUrlEncodedWithQueryString() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    when(request.getParameterMap()).thenReturn(Collections.emptyMap());
    when(request.getQueryString()).thenReturn(queryString);
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(emptyStringDigest));
  }

  @Test
  public void testGetAllDigestsApplicationFormUrlEncodedWithParamMap() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    when(request.getParameterMap()).thenReturn(paramMap);
    when(request.getQueryString()).thenReturn(StringUtils.EMPTY);
    List<String> result = DigestsUtils.getAllDigests(request);
    System.out.println("se comparan los digest calculados con "+ queryStringDigest +" calculado a partir de: " + queryString);
    Assert.assertTrue(result.contains(queryStringDigest));
  }

  @Test
  public void testGetAllDigestsApplicationFormUrlEncodedWithEncodedParamMap() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    when(request.getParameterMap()).thenReturn(paramMap);
    when(request.getQueryString()).thenReturn(StringUtils.EMPTY);
    List<String> result = DigestsUtils.getAllDigests(request);
    System.out.println("se comparan los digest calculados con "+ encodedQueryStringDigest +" calculado a partir de: " + encodedQueryString);
    Assert.assertTrue(result.contains(encodedQueryStringDigest));
  }


  @Test
  public void testGetAllDigestsApplicationFormUrlEncodedWithParamMapAndQueryString() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
    when(request.getParameterMap()).thenReturn(paramMap);
    when(request.getQueryString()).thenReturn(queryString);
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(queryStringDigest));
  }

  @Test
  public void testGetAllDigestsTextXmlWithUnixSeparator() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE);
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlUnixBytes))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlUnixDigest));
  }

  @Test
  public void testGetAllDigestsTextXmlWithUnixSeparatorUTF8() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE + "; charset=UTF-8");
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlUnixBytes))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlUnixDigest));
  }

  @Test
  public void testGetAllDigestsTextXmlWithUnixSeparatorIso() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE + "; charset=ISO-8859-1");
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlUnixBytesIso))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlUnixDigestIso));
  }

  @Test
  public void testGetAllDigestsTextXmlWithWindowsSeparator() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE);
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlWindowsBytes))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlWindowsDigest));
  }

  @Test
  public void testGetAllDigestsTextXmlWithWindowsSeparatorUTF8() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE + "; charset=UTF-8");
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlWindowsBytes))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlWindowsDigest));
  }

  @Test
  public void testGetAllDigestsTextXmlWithWindowsSeparatorIso() throws Exception {
    request = mock(HttpServletRequest.class);
    when(request.getHeader("Content-Type")).thenReturn(MediaType.TEXT_XML_VALUE + "; charset=ISO-8859-1");
    when(request.getReader()).thenReturn(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmlWindowsBytesIso))));
    List<String> result = DigestsUtils.getAllDigests(request);
    Assert.assertTrue(result.contains(xmlWindowsDigestIso));
  }

}

