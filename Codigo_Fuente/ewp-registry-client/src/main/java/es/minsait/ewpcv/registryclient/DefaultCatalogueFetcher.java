package es.minsait.ewpcv.registryclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Objects;

/**
 * A {@link CatalogueFetcher} which retrieves its <code>&lt;catalogue&gt;</code> response directly from the
 * <a href='https://registry.erasmuswithoutpaper.eu/'>Registry Service</a>.
 *
 * <p>
 * This default implementation will be used by the {@link ClientImpl} unless a custom implementation will be set via
 * {@link ClientImplOptions#setCatalogueFetcher(CatalogueFetcher)} method.
 * </p>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 * @since 1.0.0
 */
public class DefaultCatalogueFetcher implements CatalogueFetcher {

  private static final Logger logger = LoggerFactory.getLogger(DefaultCatalogueFetcher.class);
  private final String registryDomain;

  final KeyStore truststore;

  /**
   * Initialize with the default (official) Registry Service ( <code>registry.erasmuswithoutpaper.eu</code>).
   */
  public DefaultCatalogueFetcher() {
    this.registryDomain = "https://dev-registry.erasmuswithoutpaper.eu/catalogue-v1.xml";
    this.truststore = null;
  }

  /**
   * Allows you to use an alternate installation of the Registry Service.
   *
   * <p>
   * In particular, during the development you might want to use <code>dev-registry.erasmuswithoutpaper.eu</code>.
   * </p>
   *
   * @param customRegistryDomain domain name at which an alternate Registry Service installation has been set up.
   * @since 1.1.0
   */
  public DefaultCatalogueFetcher(final String customRegistryDomain, final KeyStore truststore) {
    this.registryDomain = customRegistryDomain;
    this.truststore = truststore;
  }

  private static byte[] readEntireStream(final InputStream is) throws IOException {
    final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    int nread;
    final byte[] data = new byte[16384];
    while ((nread = is.read(data, 0, data.length)) != -1) {
      buffer.write(data, 0, nread);
    }
    buffer.flush();
    return buffer.toByteArray();
  }

  @Override
  public RegistryResponse fetchCatalogue(String previousETag) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    URL url = new URL(this.registryDomain);
    logger.debug("Opening HTTPS connection to {}", url);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

    if (url.getProtocol().equals("https") && truststore != null) {
      TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      tmf.init(truststore);
      final SSLContext context = SSLContext.getInstance("TLS");
      context.init(null, tmf.getTrustManagers(), null);
      ((HttpsURLConnection) conn).setSSLSocketFactory(context.getSocketFactory());
    }

    conn.setRequestMethod("GET");
    conn.setAllowUserInteraction(false);
    conn.setRequestProperty("If-None-Match", previousETag);
    conn.setConnectTimeout(10 * 1000); // 10 sec, establish a connection
    conn.setReadTimeout(60 * 1000); // 60 sec, read whole
    conn.connect();

    final int status = conn.getResponseCode();
    logger.debug("Registry API responded with HTTP {}", status);

    /* Adjust the value of "Expires" for the difference in server and client times. */

    final long clientTimeNow = System.currentTimeMillis();
    final long serverTimeNow = conn.getHeaderFieldDate("Date", clientTimeNow);
    final long difference = serverTimeNow - clientTimeNow;
    if (Math.abs(difference) > 60000) {
      logger.debug("Difference in server-client time is " + difference + "ms");
    }
    final long serverTimeExpires = conn.getHeaderFieldDate("Expires", clientTimeNow + 300000);
    final Date expires = new Date(clientTimeNow + (serverTimeExpires - serverTimeNow));
    logger.debug("Effective expiry time: " + expires);

    switch (status) {
      case 200:
        String newETag = conn.getHeaderField("ETag");
        InputStream is = conn.getInputStream();
        byte[] content = readEntireStream(is);
        is.close();
        logger.debug("Read {} bytes with ETag {}", content.length, newETag);
        return new Http200RegistryResponse(content, newETag, expires);
      case 304:
        return new Http304RegistryResponse(expires);
      default:
        throw new IOException("Unexpected Registry API response status: " + status);
    }
  }
}
