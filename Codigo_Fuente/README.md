# Interoperabilidad Erasmus Without Papers (EWP)

## Gestión de contribuciones externas

### Envío de propuestas de nueva funcionalidad

Aunque la decisión final de aceptar o evolucionar nuevas funcionalidades propuestas será tomada siempre por las
universidades que comparten la gobernanza de este proyecto, cualquier entidad externa o usuario podrá realizar
propuestas de nueva funcionalidad siempre que estén convenientemente descritas y argumentadas.

El procedimiento para remitir las propuesta se gestionará a través de la página pública del proyecto en Bitbucket.
Accediendo al apartado issues, se insertará una nueva incidencia marcada con la etiqueta “proposal” y con todos los
detalles necesarios documentados en el campo descripción.

Una vez recibida la propuesta, será discutida en Bitbucket con la participación de los representantes de las
universidades miembro antes de que su alcance e implicaciones queden totalmente definidas. Finalmente, esta propuesta se
aceptará o denegará.

### Implementación de nuevas funcionalidades

Si se acepta, esta nueva funcionalidad deberá analizarse y ser dividida en el conjunto total de historias de usuario
necesarias para su implementación.

La creación, mantenimiento y evolución de estas historias de usuario se realizará en el backlog que las universidades
miembro comparten en JIRA.

Cada historia de usuario deberá pasar, igual que todas las actuales, por una fase de testeo funcional antes de ser
aprobada. Esto quiere decir que el código necesario para que esta historia de usuario se pueda implementar deberá ser
proporcionado por la persona que la ha propuesto o, en su defecto, por la empresa contratada para su implementación.

La implementación de la nueva funcionalidad deberá realizarse por la persona o empresa que contribuye y el código deberá
subirse al repositorio para ser validado.

El mecanismo de entrega de las modificaciones de código asociadas a cada historia de usuario será el de pull request,
siendo este el mecanismo habitual en los proyectos abiertos. El pull request se enviará a una rama nueva que tendrá el
ID de la issue interna en JIRA si se conoce o una breve descripción de la funcionalidad si es algo no recogido en la
gestión actual del proyecto. El nombre de la rama deberá seguir la estructura "feature/<id de jira>" o "
feature/<resumen funcionalidad>".

CHANGELOG
================
Changelog v 01.03.01

	Inclusion del mecanismo de seguridad TLS como server ya que de acuerdo al validador deberia incluirse
	Correccion de los endpoints de los servicios de instituciones y ounits que les faltaba un /get
	
	Correcciones sobre las apis de echo, institutions y ounits, derivadas del validador de usos.pl
	
	Modificacion de la vista de cooperation conditions para incluir el rol de los contactos que se muestran
	Correccion en la vista de cooperation conditions para mostrar los emails correctamente 
	Creacion de la vista de contactos de condiciones de cooperacion para mostrar el resto de contactos asociados a cada partner de una condicion de cooperacion a parte de los signer
	
	Correccion en aprovisionamiento de Mobilities permite no informar el IIA Id.
	Correccion en Aprovisionamiento factsheet la actualizacion de un factsheet no machaca el nombre de la institucion si no se informa el campo.
	
	Ampliacion del campo OunitNames en la funcion de extraccion de nombres para permitir almacentar muchos nombres o nombres muy largos en este campo
	
	Ampliacion de la envolvente java de aprovisionamiento para permitir invocar a los nuevos pkg de institution y ounits

Changelog v 01.04

	Modificaciones sobre APIs
		Filtrado por permisos(hots) en las invocaciones a IIAS, Omobilitie, Imobilities, LAs
	
	Nuevas APIs 
		Omobilities
		Imobilities
		OmobilitiesCNR
		ImobilitiesCNR

	Nuevos procesos programados
		Batch de procesamiiento de omobilities CNR 
		Batch de procesamiiento de imobilities CNR 
		Batch de refresco nocturno para omobilities (igual que iias y las): refresca las mobilities que nos envian pero que pueden no haber notificado
			Buscar en registro todas las heis con este api
			para cada una 
				Consultas index con sending hei (su schac) y receiving hei (tu schac) y modifiedSince (ultimo dia)
					Para cada una de las omobilities que te da este index haces un get
						si es nueva la insertas.
						si ya la tienes comparas y actualizas si procede.
				Consultas index	con sending hei (su schac) y receiving hei (tu schac) y sin modified since
					comparas las que tu tienes con ellos y las que te devuelven, las que tu tengas y ellos no te devuelvan se han borrado (hay que borrarlas)
		
		Batch de refresco nocturno de imobilities: refresca las mobilities que enviamos pero que no nos han contestado
			Buscar en BBDD todas nuestras outgoin mobilities (enviadas por nosotros) (que no tengan outgoing status cancelled o recognized, incomming status approved, y no tengan las dos actual dates informadas)
			para cada una
				consultas imobility get a la receiving hei
				con el resultado actualizamos los campos incoming status, comment y actual dates.
			
			
		
	Cambios en BBDD
		Modificacion de la tabla mobility, 
			nuevos campos: comentario, equflevel at nomination, equflevel at departure, outgoing status, incomming status, academic Term Id. Modified_Since
			Campos eliminados: status, eqflevel
		Nueva tabla Photos relacionda con contactDetails
		
		Modificaciones PKGS:
			MobilityLA
				Cambio en el objeto mobility, ahora admite los nuevos campos de la tabla: academic term (objeto complejo) outgoingStatus (reemplaza Status) eqflevel departure y eqflevel nomination (reemplazan eqflevel)
				Inserta mobility ahora genera cnr 
				Nuevas funciones para gestion de nominaciones
					Para el sender: cancel_mobility, student_departure, student_arrival
					Para el receiver: approve_nomination, reject_nomination, update_actual_dates
		Modificaciones Vistas:
			Institutions y ounits:
				Extraen el country y el city de la mailing addres si el street addres no tiene informados estos campos
			Mobility
				Muestra los nuevos campos de status, comments eqflevel y academic term.
			Factsheet
				Se incluyen los identificadores del registro en las vistas de additional_info y required_info para facilitar la integracion con hibernate

Changelog v 01.04.01

	Correccion del converter de IIAs approval, se evnia el remote id en lugar del id.
	Se evita un problema al recibir IIAs con un formato equivocado de año academico.
	Correccion del calculo de digests entrantes del metodo update request. se incluye un salto de linea al final del body leido para que coincidan los digests.
	Correccion en el mapeo del campo planned arrival date cuando recibimos un LA con start year moth en lugar de start date.
	Correccion del campo global id en la vista de LA, sacaba el id de la tabla en lugar del id europeo
	Correccion, admitir contactos sin telefono en un la
	Generacion de UUIDs en minuscula para Oracle
	Correcciones al recoger el endpoint de los cnr de omobility e imobility
	Cambios en aprovisionamiento para almacenar en minuscula codigos schac
	Modificaciones para convertir cadenas a minusculas en lugar de a mayusculas.
	Correcciones de LA, el identificador de propuesta de cambios se mapea con el valor del identificador del estudiante
	Correcciones en aprovisionamiento SQL server. Se corrigen las vistas de condiciones de cooperacion y movilidades para evitar problemas al convertir datos numericos a cadenas de caracteres.
	Correcciones detectadas tras pruebas con USAL
	-Las respuestas a los cnr ahora son xml vacios en lugar de respuestas vacias
	-Se convierte en opcional el campo Subject Area en el Recomended Language Skill de las Condiciones de cooperacion de los IIAS
	-Se modifica la version del CNR de los OLAs para que apunte a una version real 1.0.1
	-Capacidad de recibir LAs con la fecha en formato year moth en lugar de dates.

Changelog v 01.05

	Auditoría de eventos en llamadas recibidas. Se emplea configuracion por BBDD
		Auditoria de TORs -> falta en controller
		
	Validacion de datos en la entrada de aprovisionamiento para adecuarlo a las restricciones de XSD establecidas en EWP
		
	Modificacion de Aprovisionamiento LA para admitir LOS_ID y LOS_TYPE. Se genera un LOI en caso de informar dichos campos.
		Nuevos campos en la_component: 
			START_DATE
			END_DATE
			LOS_ID
			LOS_TYPE
			
	Modificacion del volcado de LAs desde otras universidades para generar los LOIs y los LOS si estos vienen informados.
		Si viene LOI_ID -> se genera y vincula al componente
		Si viene LOS_ID -> Se genera (si no existia) y vincula al componente
		Si viene LOS_CODE y existe -> se vincula al componente
		
	Modificacion de IIAs para permitir asociar SubjectAreas directamente a condiciones de cooperacion sin necesidad de estar vinculadas a una Language Skill.
		Nuevo campo en CoopConditions
			SubjectAreaList
		Modificacion de duracion de condiciones de cooperacion ahora admiten 2 decimales.
		
	Aprovisionamiento de LOS
		Insert
		Delete
		Update
		Cambiar padre
		
	Nueva API TORs
		INDEX
		GET
		CNR
		Batch CNR
		Batch Refresco
		*los batch no borran
	
	Nuevo Aprovisionamiento TORs
		Insertar Grading Scheme
		Insertar Gouping
		
		Insert
		Delete
		Update
		
		Insert_simplificado
		Delete_simplificado
		Update_simplificado

Changelog v 01.06

	Autogeneración de IIAs local en base a remoto cuando nuevo entrante.
	Script refresco de certificados
	Manual de purga de eventos
	Pruebas estres
	Sustitucion de Uniqueidentifier por Varchar en SqlServer.

Changelog v 01.06.01

    Correcciones en el volcado de datos del diccionario que duplicaba registros.
    Control de desbordamiento en errores y extracciones
    Correccion actualizacion y borrado de IIAS propios
    Correccion en el borrado de factsheet (sql server)
    Modificacion Unbind IIA, eliminada restriccion de aprobacion.
    Correccion actualizacion Ounits
    Correcciones Validador
    Modificacion Auditoria, trigering hei, changed element id
    Control de errores varios
    Control Duracion obligatoria en coop conditions
    Correccion insercion de años academicos en movilidades (inicio y fin estaban al reves)
    Parametrizar Autogeneracion ewp.iia.autogenerate=true
    Validacion del hash de las condiciones de cooperacion previo a la aprobacion. gestionado con la property ewp.iia.hashValidationHeis (noValidate)
    Un fallo de comprobacion de hash en la aprobacion de un iias pone max reintentos.
    Correcciones en ImobilitiesAPI, notificaba los cnr con sending_hei en lugar de receiving_hei
    Correccion del aprovisionamiento de Imobilities no se procesaba correctamente
    Se corrige el borrado de mobilities, podia provocar un fallo al tratar de borrar omobilities con language skills utilizados en iias.

Changelog v 01.06.02

    Correcciones en dos funciones de extraccion de datos para vistas
    Corrección de excepcion al purgar LAUpdateRequests cuando no habia nada que purgar
    Correccion en el refresco nocturno de Imobilities
    Correccion en la autogeneracion de IIAs bajo ciertas condiciones se generaba copia cuando no debia
    Añadido el campo component_id a la vista de componentes de LA
    Correccion, al notificar Omobilities u OutgoingLAs en bloques solo funcionaba correctamente la primera notificacion del bloque. El resto duplicaban uno de los valores a notificar
    Correccion, intensificado el control de errores en el proceso de notificacion, en ciertas circunstancias un registro mal insertado en la tabla de notificaciones rompia el flujo de notificacion de los demas elementos a notificar.
    Correccion en la generacion de Hashes de condiciones de cooperacion de los IIAS para que coincidan con el hash que calcula la universidad partner previo a su validacion.
    Correccion en la comparacion de iias, no volcaba bien las actualizaciones de eqflevel de las coop conditions

Changelog v 01.06.03

    Corrección en la comparación y volcado de listados como direcciones, fotos, y contactos.
    Revisión del flujo de sucesivas propuestas de cambios sobre un mismo LA y correcciones en el volcado este tipo de escenarios. [EWP-126] Consultas a PL - Varias modificaciones sobre un mismo LA - Jira (atlassian.net)
    Correcciones en actualizaciones de IIAS ante CNR.
    Corrección en aprovisionamiento oracle, notificaciones sobre imobilities no estaban bien (solo en versión Oracle). [EWP-124] Error en la aprobación y rechazo de Mobility - Jira (atlassian.net)
    Volcado de contactos de olas tras volcado de mobility. Se volcaban inicialmente a nulo y luego cuando volcaba el LA no se actualizaban.
    Correcciones de respuestas ante iias_approval cuando no están aprobados. Se devolvía el identificador del IIAs sin el hash, lo cual era una respuesta a medias que no estaba bien, si no está aprobado no se devuelve nada.
    Corrección en la vista de Factsheet, no recogía bien la información de visa, insurance y housing cuando el tipo de elemento estaba indicado en minúscula.
    Inclusión de campos La_id y LA_version a la tabla y vista de mobility update request. De esta forma facilitamos identificar a que LA se refiere cada registro.
    Corrección en la formación de años académicos de las condiciones de cooperación de los IIAS [EWP-123] Error en los años académicos de un IIA - Jira (atlassian.net)
    Modificación de envolvente java de aprovisionamiento para compatibilizarla con jpa 2.x Se externaliza la utilidad de repositorio que deberá ser incluida como componente en el proyecto que quiera integrar dicha librería.

Changelog v 01.07.00

	Inclusión de la vista MobilityUpdateRequest y mecanismos de acceso a ella en la envolvente de aprovisionamiento java.
	Revisión de los tipos de evento que se guardan en la tabla de eventos, a veces se guardaba un update y no se había hecho nada.
	Correcciones en la comparacion de IIAs, se tenía en cuenta la fecha de modificación (un campo calculado) para la comparacion y siempre se consideraba que había cambio
	Correcciones en CNR y refresco nocturno. Se comprueba que nuestra institución participe en el objeto notificado. Antes si nos llegaba un CNR o el index/get del refresco noctuno nos daba objetos que no eran nuestros, los volcabamos igualmente.
	Parametrización de procesos asíncronos. Se incluyen properties para desactivar el procesamiento de CNR por tipo de objeto. Asímismo se incluyen properties para deshabilitar el proceso de refresco nocturno de ciertos objetos.
	Parametrización de la fecha pivote del refresco nocturno para cargar objetos mas antiguos, además posibilita una carga total de objetos pendientes 

Changelog v 01.08.00

	1. Inclusión los partners a nivel de IIA 
	
	Modificado TIPO_IIA
	Modificada funcion VALIDA_IIA y funcion VALIDA_DATOS_IIA
	Modificada la funcion INSERTA_IIA
	Modificada la funcion ACTUALIZA_IIA
	Modificada la funcion BORRA_IIA_PARTNER
	Modificada la funcion BORRA_IIA
	Modificado INSERTA_IIA_PARTNER
	
	Modificado xsd del iia
	Modificado el metodo VALIDA_IIA_PARTNER_INSTITUTION 
	Creado el metodo VALIDA_PARTNER_INSTITUTION_OUNIT
	Modificado el metodo INSERTA_IIA 
	Modificada la funcion BORRA_IIA_PARTNER
	Modificada la funcion BORRA_IIA
	Modificado INSERTA_IIA_PARTNER
	
	
	Modificada la entidad IIA 
	Modificado el DTO IiaMdto
	Modificado el Mapper IiaMdtoMapper
	Modificada la clase iiaConverter common direccion BBDD-->WS
	Modificada la clase iiaConverter common direccion WS-->BBDD
	Modificada la clase IiaServiceImpl, compara IIA
	
	Modificada actualizacion de Institutions para excluirse a si mismo
	Corregida actualizacion de OUNITS, borraba las ounits ya existentes.
	Corregida actualizacion de IIAs, fecha modificacion fechas a nivel de IIAS

Changelog v 01.08.03

    En aprovisionamiento, se limita el uso de los objetos (nodos xml en sqlserver) SUBJECT_AREA_LANGUAGE_LIST y SUBJECT_AREA_LIST de las condiciones de cooperación de los IIAs, ahora si queremos emplear el primero es obligatorio informar al menos un idioma, en caso de no querer informar el idioma deberemos emplear el segundo.
    Se han corregido algunas erratas del contrato de integración de BBDD respecto a la obligatoriedad de los campos mencionados en el punto anterior.
    Se modifica la versión de lobgback-core de la 1.1.11 a la 1.2.9 que se empaquetaba en el war entregado. Debido al bug de log4j (realmete no cumplimos con las condiciones que permiten explotar el bug, pero se ha modificado la versión para que no se pueda explotar el bug en caso de que las cumpliésemos)

Changelog v 01.08.04

    Se ha corregido la gestion de errores al auditar eventos para que un fallo de auditoria no interfiera en el funionamiento habitual de la aplicacion. Se producia al ser invocados por un host que cubria muchas instituciones y desbordaba el campo de la tabla de eventos.
    Se ha corregido el script de instituciones de sqlserver que cuando se producia determinado error no informaba bien el mensaje de error. Concretamente al borrar una institucion con factsheet.
    Se ha hecho opcional el listado de subjectAreaLanguageSkill de las condiciones de cooperacion de los IIAs en sqlserver, hasta ahora obligaorio pero admitia vacio.
    Se ha formateado la fecha de las firmas de los acuerdos de aprendizaje de aceurdo al ISO8601 'YYYY-MM-DD"T"HH24:MI:SS'
	
