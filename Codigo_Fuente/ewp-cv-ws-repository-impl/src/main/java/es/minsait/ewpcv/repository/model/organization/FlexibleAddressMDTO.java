
package es.minsait.ewpcv.repository.model.organization;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Flexible address mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class FlexibleAddressMDTO extends BaseMDTO {

  private static final long serialVersionUID = 5892729770366613691L;

  String id;

  private Integer version;

  private List<String> recipientName;

  private List<String> addressLine;

  private String buildingNumber;

  private String buildingName;

  private String streetName;

  private String unit;

  private String floor;

  private String postOfficeBox;

  private List<String> deliveryPointCode;

  private String postalCode;

  private String locality;

  private String region;

  private String country;

}
