package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;

/**
 * The interface Mobility update request dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IMobilityUpdateRequestDAO {

  List<MobilityUpdateRequestMDTO> findAllNotProcessingByMobilityType(UpdateRequestMobilityTypeEnum mobilityType,
      int maxRetries, int blockSize);

  void changeStatusIsProcessingUpdateRequest(List<String> updateRequestList, boolean isProcessing);

  void deleteMobillityUpdateRequest(String id);

  void saveMobilityUpdateRequest(MobilityUpdateRequestMDTO mobilityUpdateRequestMDTO);

  void updateShouldRetryLastError(String id,String errorMessage);

  void changeStatusIsNotProcessingAndRetriesUpdateRequest(List<String> updateRequestList);

  List<MobilityUpdateRequestMDTO> obtenerRequestProcesandoseByTime(Long maxProcessingTime);
}
