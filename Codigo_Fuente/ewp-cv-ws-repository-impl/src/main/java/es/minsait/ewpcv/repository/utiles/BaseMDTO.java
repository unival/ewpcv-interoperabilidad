/**
 * Copyright (c) 2019 Generalitat Valenciana - Todos los derechos reservados.
 */
package es.minsait.ewpcv.repository.utiles;

import java.io.Serializable;

/**
 * The type Base mdto.
 *
 * @author jmgarciav at Indra
 */
public abstract class BaseMDTO implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

}
