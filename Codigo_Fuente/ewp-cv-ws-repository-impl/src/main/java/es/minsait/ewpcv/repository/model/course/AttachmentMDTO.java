package es.minsait.ewpcv.repository.model.course;

import java.util.List;

import es.minsait.ewpcv.model.course.AttachmentType;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Attachment mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class AttachmentMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;

  String id;
  private Integer version;
  private AttachmentType type;
  private byte[] extension;
  private List<AttachmentContentMDTO> contents;
  private List<LanguageItemMDTO> title;
  private List<LanguageItemMDTO> description;
}
