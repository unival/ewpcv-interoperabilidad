package es.minsait.ewpcv.mapper;

import es.minsait.ewpcv.model.fileApi.File;
import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface FileApi mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface FileApiMDTOMapper extends IModelDTOMapperCircular<File, FileMDTO> {
}
