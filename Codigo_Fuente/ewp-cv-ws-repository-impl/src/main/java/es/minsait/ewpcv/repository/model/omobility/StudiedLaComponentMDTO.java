
package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Studied la component mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class StudiedLaComponentMDTO extends BaseMDTO {

  private static final long serialVersionUID = 7481181138156394677L;

  private String learningAgreementId;

  private String studiedLaComponentId;

  private int learningAgreementRevision;

}
