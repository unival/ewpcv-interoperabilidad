
package es.minsait.ewpcv.repository.model.organization;

import java.util.Date;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;


/**
 * The type Photo url mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class PhotoUrlMDTO extends BaseMDTO {

  private static final long serialVersionUID = 6553888528555814641L;

  String id;

  private String url;

  private String size;

  private Date date;

  private boolean photoPublic;
}
