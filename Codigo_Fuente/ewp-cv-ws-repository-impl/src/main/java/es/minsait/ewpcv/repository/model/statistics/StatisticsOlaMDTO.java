package es.minsait.ewpcv.repository.model.statistics;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

/**
 * The type StatisticsOlaMDTO mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class StatisticsOlaMDTO extends BaseMDTO {
    String Id;
    private Integer version;
    private String academicYearStart;
    private String academicYearEnd;
    private Integer totalOutgoingNumber;
    private Integer approvalUnModif;
    private Integer approvalModif;
    private Integer lastAppr;
    private Integer lastRej;
    private Integer lastWait;
    private String sendingInstitution;
    private Date dumpDate;

}
