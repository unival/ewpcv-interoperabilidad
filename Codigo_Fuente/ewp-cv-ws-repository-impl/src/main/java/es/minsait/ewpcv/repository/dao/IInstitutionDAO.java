package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;

/**
 * The interface Institution dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionDAO {

  int getTotalInstitutionById(String instId);

  InstitutionMDTO getInstitutionById(String heiId);

  List<InstitutionMDTO> findAllByHeiId(List<String> heiId);

  InstitutionMDTO saveOrUpdate(InstitutionMDTO institution);

  InstitutionMDTO saveOrUpdateFactsheets(InstitutionMDTO institution);

  boolean isKnownHei(String heiId);
}
