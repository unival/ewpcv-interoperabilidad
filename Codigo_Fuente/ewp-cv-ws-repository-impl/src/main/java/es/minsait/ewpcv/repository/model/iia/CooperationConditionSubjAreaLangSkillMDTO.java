package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.annotation.Nullable;

/**
 * The type Cooperation condition subj area lang skill mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class CooperationConditionSubjAreaLangSkillMDTO extends BaseMDTO implements Comparable<CooperationConditionSubjAreaLangSkillMDTO> {

  private static final long serialVersionUID = -138148132106292009L;

  private String id;

  @Nullable
  private SubjectAreaMDTO subjectArea;

  private LanguageSkillMDTO languageSkill;

  private int orderIndexLang;

  @Override
  public int compareTo(CooperationConditionSubjAreaLangSkillMDTO other) {
    //comparmos los isced_codes
    //si iguales comparamos los languages
    if (this.getSubjectArea() != null && other.getSubjectArea() != null) {
      Integer iscedCode1 = Integer.parseInt(this.getSubjectArea().getIscedCode());
      Integer iscedCode2 = Integer.parseInt(other.getSubjectArea().getIscedCode());
      return iscedCode1.compareTo(iscedCode2);
    } else {
      return this.getLanguageSkill().compareTo(other.getLanguageSkill());
    }
  }
}
