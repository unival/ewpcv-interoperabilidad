package es.minsait.ewpcv.repository.model.course;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Diploma section mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class DiplomaSectionMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;

  String id;
  private Integer version;
  private String title;
  private byte[] content;
  private Integer number;
  private List<DiplomaAdditionalInfoMDTO> additionalInfo;
  private List<AttachmentMDTO> attachments;
  private List<DiplomaSectionMDTO> sections;

}
