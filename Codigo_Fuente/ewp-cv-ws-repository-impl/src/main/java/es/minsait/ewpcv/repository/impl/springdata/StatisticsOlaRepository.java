package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.omobility.OmobilityStats;
import es.minsait.ewpcv.model.omobility.StatisticsOla;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * The interface StatisticsOlaRepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface StatisticsOlaRepository extends JpaRepository<StatisticsOla, String> {

    @Query("Select statOla FROM StatisticsOla statOla WHERE statOla.dumpDate IN (select max(dumpDate) from StatisticsOla group by academicYearStart,academicYearEnd)")
    List<StatisticsOla> findMostRecentStats();
}
