package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;

import es.minsait.ewpcv.model.iia.MobilityType;

/**
 * The interface Mobility type repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface MobilityTypeRepository extends JpaRepository<MobilityType, String> {
  MobilityType findByMobilityCategoryAndMobilityGroup(String mobilityCategory, String mobilityGroup);
}
