
package es.minsait.ewpcv.repository.model.notificacion;

import java.util.Date;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Notification mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class NotificationMDTO extends BaseMDTO {

  private static final long serialVersionUID = -8025596437214914915L;

  String id;

  private Integer version;

  private NotificationTypes type;

  private String heiId;

  private String changedElementIds;

  private Date notificationDate;

  private CnrType cnrType;

  private Integer retries;

  private Boolean processing;

  private String ownerHeiId;

  private Date processingDate;

  private Boolean shouldRetry;

  private String lastError;
}
