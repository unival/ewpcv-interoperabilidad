
package es.minsait.ewpcv.repository.model.organization;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Organization unit mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class OrganizationUnitMDTO extends BaseMDTO {

  private static final long serialVersionUID = 5768491520265373734L;

  String id;

  private Integer version;

  private String organizationUnitCode;

  private List<InformationItemMDTO> informationItems;

  private List<LanguageItemMDTO> name;

  private String abbreviation;

  private String parentOUnitId;

  private FactSheetMDTO factSheet;

  private String logoUrl;

  private List<RequirementsInfoMDTO> additionalRequirements;

  private Boolean isTreeStructure;

  private Boolean isExposed;

  private ContactDetailsMDTO primaryContactDetails;

  private String internalId;

  private List<LanguageItemMDTO> factsheetUrls;
}
