package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;

import es.minsait.ewpcv.model.config.AuditConfig;

/**
 * The interface Audit config repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface AuditConfigRepository extends JpaRepository<AuditConfig, String> {

  AuditConfig findByName(String name);

}
