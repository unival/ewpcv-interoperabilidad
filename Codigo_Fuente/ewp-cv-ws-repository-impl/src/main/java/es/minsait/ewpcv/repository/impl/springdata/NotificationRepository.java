package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.model.notificacion.CnrType;
import es.minsait.ewpcv.model.notificacion.Notification;
import es.minsait.ewpcv.model.notificacion.NotificationTypes;


/**
 * The interface Notification repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
public interface NotificationRepository extends JpaRepository<Notification, String> {

  @Query("SELECT DISTINCT(n.changedElementIds) FROM Notification n WHERE n.cnrType = :cnrType "
      + " AND n.type = :notificationType AND n.processing = false AND n.shouldRetry = true "
      + " AND (n.processingDate is null OR CURRENT_TIMESTAMP > (n.processingDate + (n.retries * :delayTime)/24)) "
      + " AND n.ownerHeiId IS NOT NULL AND n.heiId IS NOT NULL AND n.changedElementIds IS NOT NULL")
  List<String> findDistinctPendingNotificationCahngedElements(@Param(value = "cnrType") CnrType cnrType,
      @Param(value = "notificationType") NotificationTypes notificationType, @Param(value = "delayTime") int delayTime,
      Pageable page);

  @Query("SELECT DISTINCT(n.changedElementIds) " + " FROM Notification n " + " WHERE n.cnrType = :cnrType "
          + " AND n.type = :notificationType AND n.processing = false AND n.shouldRetry = true"
          + " AND (n.processingDate is null OR CURRENT_TIMESTAMP > (n.processingDate + (n.retries * :delayTime)/24))"
          + " AND n.changedElementIds NOT IN ( " +
          "SELECT distinct(i.remoteIiaId) FROM Notification n2, Iia i " +
          " WHERE n2.changedElementIds = i.interopId " +
          " AND n2.cnrType = es.minsait.ewpcv.model.notificacion.CnrType.NOTIFY " +
          " AND n2.type = es.minsait.ewpcv.model.notificacion.NotificationTypes.IIA" +
          " AND i.isRemote = false "+
          " AND i.remoteIiaId IS NOT NULL)"
  )
  List<String> findIiasApprovalNotificationWhitoutIiaNotificationPending(@Param(value = "cnrType") CnrType cnrType,
                                                                         @Param(value = "notificationType") NotificationTypes notificationType, @Param(value = "delayTime") int delayTime,
                                                                         Pageable page);

  @Modifying
  @Query("UPDATE Notification SET processing = true, processingDate = :processingDate "
      + " WHERE changedElementIds IN (:ids) AND type = :tipoObjeto AND cnrType = :tipoNotificacion")
  void setProcessing(@Param(value = "ids") List<String> ids,
      @Param(value = "tipoNotificacion") CnrType tipoNotificacion,
      @Param(value = "tipoObjeto") NotificationTypes tipoObjeto, @Param(value = "processingDate") Date processingDate);

  @Modifying
  @Query("UPDATE Notification SET processing = false, retries = (retries + 1), lastError = :lastError "
      + " WHERE changedElementIds = :id AND type = :tipoObjeto AND cnrType = :tipoNotificacion")
  void setNotProcessing(@Param(value = "id") String id, @Param(value = "tipoNotificacion") CnrType tipoNotificacion,
      @Param(value = "tipoObjeto") NotificationTypes tipoObjeto, @Param(value = "lastError") String lastError);

  @Modifying
  @Query("UPDATE Notification SET processing = false, retries = (retries +1), lastError = :lastError, shouldRetry = false "
      + " WHERE changedElementIds = :id AND type = :tipoObjeto AND cnrType = :tipoNotificacion")
  void setNotProcessingAndNotRepeteable(@Param(value = "id") String id,
                        @Param(value = "tipoNotificacion") CnrType tipoNotificacion,
                        @Param(value = "tipoObjeto") NotificationTypes tipoObjeto,
                        @Param(value = "lastError") String lastError);

  @Modifying
  @Query("DELETE FROM Notification n " + "WHERE n.heiId = :heiId " + "AND n.ownerHeiId = :ownerId "
      + "AND n.changedElementIds = :changedElementId " + "AND n.type = :type " + "AND n.cnrType = :cnrType "
      + "AND n.notificationDate <= :date")
  void eliminarNotificaciones(@Param(value = "heiId") String heiId, @Param(value = "ownerId") String ownerHeiId,
      @Param(value = "changedElementId") String changedElementId, @Param(value = "type") NotificationTypes type,
      @Param(value = "cnrType") CnrType cnrType, @Param(value = "date") Date notificationDate);

  @Query("SELECT n " + " FROM Notification n "
      + " WHERE n.changedElementIds = :changedElementId AND n.type = :tipoObjeto AND n.cnrType = :tipoNotificacion AND n.shouldRetry = true")
  List<Notification> findByChangedElementIdTypeAndNotificationType(
      @Param(value = "changedElementId") String changedElementId,
      @Param(value = "tipoObjeto") NotificationTypes tipoObjeto,
      @Param(value = "tipoNotificacion") CnrType tipoNotificacion, Pageable page);

  @Query("SELECT n from Notification n WHERE n.processing = true")
  List<Notification> obtenerNotificacionesProcesandose();

  @Query("SELECT COUNT(n) FROM Notification n WHERE n.changedElementIds = :changedElementIds AND n.type = :tipoObjeto AND n.heiId = :heiId")
  long existsPendingNotification(@Param("changedElementIds") String changedElementIds, @Param(value = "tipoObjeto") NotificationTypes tipoObjeto, @Param(value = "heiId") String heiId);

  @Modifying
  @Query("UPDATE Notification SET shouldRetry = false, lastError = 'Notificacion mal formada' " +
          " WHERE changedElementIds is null or heiId is null or ownerHeiId is null or cnrType is null or type is null " +
          " and shouldRetry = true")
  void marcaNotificacionesErroneasNoReintentables();
}
