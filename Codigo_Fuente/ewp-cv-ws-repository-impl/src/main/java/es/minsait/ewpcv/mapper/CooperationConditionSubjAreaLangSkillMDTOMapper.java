package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.iia.CooperationConditionSubjAreaLangSkill;
import es.minsait.ewpcv.repository.model.iia.CooperationConditionSubjAreaLangSkillMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Cooperation condition subj area lang skill mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface CooperationConditionSubjAreaLangSkillMDTOMapper
    extends IModelDTOMapperCircular<CooperationConditionSubjAreaLangSkill, CooperationConditionSubjAreaLangSkillMDTO> {
}
