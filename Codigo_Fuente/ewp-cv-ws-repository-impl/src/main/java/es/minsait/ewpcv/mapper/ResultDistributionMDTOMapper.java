package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.ResultDistribution;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Result distribution mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface ResultDistributionMDTOMapper
    extends IModelDTOMapperCircular<ResultDistribution, ResultDistributionMDTO> {
}
