package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.iia.IiaApproval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

import es.minsait.ewpcv.model.iia.Iia;
import es.minsait.ewpcv.model.iia.IiaPartner;

/**
 * The interface Iia repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IiaRepository extends JpaRepository<Iia, String> {

  @Query("SELECT iia FROM Iia iia join iia.cooperationConditions coop WHERE iia.remoteIiaId in (:remoteIiaId) "
      + "AND coop.receivingPartner.institutionId in (:partners) AND coop.sendingPartner.institutionId in (:partners) AND iia.isRemote = true AND iia.approvalCopCondHash IS NOT NULL")
  List<Iia> findApprovedIiasByRemoteIiaIdAndPartners(@Param("remoteIiaId") List<String> remoteIiaId,
      @Param("partners") List<String> partners);

  Iia findByRemoteIiaIdAndIsRemote(String remoteIiaId, Boolean isRemote);

  Integer countByRemoteIiaIdAndIsRemote(String remoteIiaId, Boolean isRemote);

  @Query("SELECT DISTINCT(i.remoteIiaId) FROM Iia i "
      + " WHERE i.isRemote = :isRemote "
      + "AND ((i.firstPartner.institutionId = :sendingHei AND i.secondPartner.institutionId = :receivingHei)"
      + "OR (i.firstPartner.institutionId = :receivingHei AND i.secondPartner.institutionId = :sendingHei))")
  List<String> findRemoteIiaIdByIsRemoteAndPartnersInstitution(@Param("sendingHei") String sendingHei,
      @Param("receivingHei") String receivingHei, @Param("isRemote") Boolean isRemote);


  @Modifying
  @Query("UPDATE Iia SET remoteIiaId=NULL WHERE interopId=:interopId")
  void desvincularIiaByInteropId(@Param("interopId") String interopId);

  @Modifying
  @Query("UPDATE Iia set approvalCopCondHash = :condHash, approvalDate = :date "
      + "WHERE remoteIiaId = :remoteIiaId AND isRemote = true")
  void actualizarApprovalCoopCondHash(@Param("remoteIiaId") String remoteIiaId,
      @Param("condHash") String conditionsHash, @Param("date") Date date);

  @Modifying
  @Query("UPDATE Iia set approvalCopCondHash = :approvalConditionHash, approvalDate = :approvalDate "
      + "WHERE interopId = :interopId")
  void updateIiaApprovalByInteropId(@Param("interopId") String interopId, @Param("approvalConditionHash") String approvalConditionHash,
      @Param("approvalDate") Date approvalDate);

  @Modifying
  @Query("UPDATE Iia set remoteCoopCondHash = :conditionsHash " + "WHERE interopId = :interopId")
  void actualizaCoopCondHashByInteropId(@Param("interopId") String interopId, @Param("conditionsHash") String conditionsHash);

  @Query("Select iia FROM Iia iia  WHERE LOWER(iia.interopId) = LOWER(:interopId)")
  Iia findByInteropIdIgnoreCase(@Param("interopId") String interopId);

  @Query("Select count(1) FROM Iia iia " + " JOIN iia.cooperationConditions coopCond "
      + "WHERE iia.remoteIiaId IS NULL " + "AND iia.isRemote = false "
      + "AND LOWER(coopCond.receivingPartner.institutionId) IN (:partners) "
      + "AND LOWER(coopCond.sendingPartner.institutionId) IN (:partners)")
  int contarNoVinculadosPorInstituciones(@Param("partners") Set<String> partners);

  @Query("SELECT iia.approvalDate FROM Iia iia WHERE iia.remoteIiaId = :remoteIiaId AND iia.isRemote = true")
  Date obtenerApprovalDatePorRemoteIiaIdAndIsRemote1(@Param("remoteIiaId") String remoteIiaId);
}
