package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.LaComponent;
import es.minsait.ewpcv.repository.model.omobility.LaComponentMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface La component mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {LearningOpportunityInstanceMDTOMapper.class})
public interface LaComponentMDTOMapper extends IModelDTOMapperCircular<LaComponent, LaComponentMDTO> {
}
