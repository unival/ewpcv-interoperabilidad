package es.minsait.ewpcv.repository.model.statistics;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Version;
import java.util.Date;

/**
 * The type StatisticsILA mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class StatisticsILAMDTO extends BaseMDTO {

    String Id;
    private Integer version;
    private String receivingInstitution;
    private String academicYearStart;
    private String academicYearEnd;
    private Integer totalNumber;
    private Integer lastWait;
    private Integer lastAppr;
    private Integer lastRej;
    private Integer approvalModif;
    private Integer approvalUnModif;
    private Integer sameVersionAppr;
    private Date dumpDate;
}
