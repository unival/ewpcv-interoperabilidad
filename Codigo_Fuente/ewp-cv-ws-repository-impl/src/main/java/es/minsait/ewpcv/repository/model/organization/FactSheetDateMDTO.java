
package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.model.organization.FactSheetDateType;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Fact sheet date mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class FactSheetDateMDTO extends BaseMDTO {

  private FactSheetMDTO facthsheet;
  private AcademicTermMDTO academicTerm;
  private FactSheetDateType type;
}
