package es.minsait.ewpcv.repository.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.minsait.ewpcv.repository.model.iia.CooperationConditionMDTO;
import es.minsait.ewpcv.repository.model.iia.IiaApprovalMDTO;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.model.iia.IiaPartnerMDTO;

/**
 * The interface Iia dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaDAO {

  List<String> findRemoteIiaIdByIsRemoteAndPartnersInstitution(String sendingHei, String receivingPartner, Boolean isRemote);

  List<String> getIiaIdsByCriteria(Map<Object, Object> queryParams);

  List<IiaMDTO> getIiaListByCriteria(Map<Object, Object> queryParams);

  IiaMDTO findIiaByRemoteIiaIdAndIsRemote(String remoteIiaId, Boolean isRemote);

  Integer countIiaByRemoteIiaIdAndIsRemote(String remoteIiaId, Boolean isRemote);

  void removeIiaByRemoteIiaId(String remoteIiaId);

  void removeIiaByInteropId(String interopId, String hei);

  void desvincularIiaByInteropId(String interopId);

  IiaMDTO save(IiaMDTO iia);

  IiaMDTO findByInteropId(String interopId);

  IiaApprovalMDTO findLastIiaApprovalByIiaRemoteIiaId(String iiaRemoteIiaId);

  void actualizarApprovalCoopCondHash(String remoteIiaId, String conditionsHash);

  void updateIiaApprovalByInteropId(String interopId, String approvalConditionHash, Date approvalDate);

  void actualizaCoopCondHashByInteropId(String interopId, String conditionsHash);

  int contarNoVinculadosPorInstituciones(Set<String> partners);

  Date obtenerApprovalDatePorRemoteIiaIdAndIsRemote1(String remoteIiaId);

  IiaApprovalMDTO saveIiaApproval(IiaApprovalMDTO iiaApprovalMDTO);

  void updateCoopCondById(final String coopCondId, final String approvalId);

  IiaApprovalMDTO findLastIiaApprovalByLocalIia(String localIia);

  boolean RemoteIiaIsAproved(String iiaRemoteIiaId);
}
