package es.minsait.ewpcv.mapper;

import es.minsait.ewpcv.model.iia.IiaStats;
import es.minsait.ewpcv.repository.model.iia.IiaStatsMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface OmobilityIiaStatsMDTOMapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface IiaStatsMDTOMapper extends IModelDTOMapperCircular<IiaStats, IiaStatsMDTO> {

}
