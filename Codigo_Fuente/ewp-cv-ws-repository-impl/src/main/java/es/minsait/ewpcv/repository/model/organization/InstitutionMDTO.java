
package es.minsait.ewpcv.repository.model.organization;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Institution mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class InstitutionMDTO extends BaseMDTO {

  private static final long serialVersionUID = 1690324621657608949L;

  String id;

  private String institutionId;

  private Integer version;

  private List<InformationItemMDTO> informationItems;

  private List<LanguageItemMDTO> name;

  private String abbreviation;

  private List<OrganizationUnitMDTO> organizationUnits;

  private String logoUrl;

  private FactSheetMDTO factSheet;

  private List<RequirementsInfoMDTO> additionalRequirements;

  private ContactDetailsMDTO primaryContactDetails;

  private List<LanguageItemMDTO> factsheetUrls;
}
