package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.minsait.ewpcv.mapper.AuditConfigMDTOMapper;
import es.minsait.ewpcv.repository.dao.IAuditConfigDAO;
import es.minsait.ewpcv.repository.impl.springdata.AuditConfigRepository;
import es.minsait.ewpcv.repository.model.config.AuditConfigMDTO;

/**
 * The type Audit config dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class AuditConfigDAO implements IAuditConfigDAO {

  @Autowired
  private AuditConfigMDTOMapper auditConfigMDTOMapper;

  @Autowired
  private AuditConfigRepository auditConfigRepository;


  @Override
  public AuditConfigMDTO findByName(final String name) {
    return auditConfigMDTOMapper.entityToDto(auditConfigRepository.findByName(name));
  }
}
