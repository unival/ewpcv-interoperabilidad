package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.omobility.StatisticsILA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


/**
 * The interface StatisticsILARepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface StatisticsILARepository extends JpaRepository<StatisticsILA, String> {

    @Query("Select statILA FROM StatisticsILA statILA WHERE statILA.dumpDate IN (select max(dumpDate) from StatisticsILA group by academicYearStart,academicYearEnd)")
    List<StatisticsILA> findMostRecentStatsILA();
}
