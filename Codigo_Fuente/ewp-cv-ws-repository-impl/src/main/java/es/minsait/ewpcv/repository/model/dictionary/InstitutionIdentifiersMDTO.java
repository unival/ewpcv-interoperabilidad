package es.minsait.ewpcv.repository.model.dictionary;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Institution identifiers mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class InstitutionIdentifiersMDTO extends BaseMDTO {

  private String schac;
  private String previousSchac;
  private String pic;
  private String erasmus;
  private String euc;
  private String erasmusCharter;
  private String institutionNames;
  private String otherIds;
}
