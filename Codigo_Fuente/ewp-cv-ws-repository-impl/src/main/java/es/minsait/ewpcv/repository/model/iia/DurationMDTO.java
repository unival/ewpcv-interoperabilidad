
package es.minsait.ewpcv.repository.model.iia;

import java.math.BigDecimal;

import es.minsait.ewpcv.model.iia.DurationUnitVariants;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Duration mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class DurationMDTO extends BaseMDTO {

  private static final long serialVersionUID = -5109016997660359312L;

  String id;

  private Integer version;

  private DurationUnitVariants unit;

  private BigDecimal numberduration;

}
