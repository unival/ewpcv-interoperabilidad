package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.minsait.ewpcv.mapper.EventMDTOMapper;
import es.minsait.ewpcv.repository.dao.IEventDAO;
import es.minsait.ewpcv.repository.impl.springdata.EventRepository;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Event dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class EventDAO implements IEventDAO {

  private EventMDTOMapper eventMDTOMapper;
  private EventRepository eventRepository;

  @Autowired
  public EventDAO(EventMDTOMapper eventMDTOMapper, EventRepository eventRepository) {
    this.eventMDTOMapper = eventMDTOMapper;
    this.eventRepository = eventRepository;
  }

  @Override
  public void saveEvent(EventMDTO eventMDTO) {
    eventRepository.save(eventMDTOMapper.dtoToEntity(eventMDTO));
  }
}
