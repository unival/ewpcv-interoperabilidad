package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.dictionary.InstitutionIdentifiers;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Institution identifiers mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface InstitutionIdentifiersMDTOMapper
    extends IModelDTOMapperCircular<InstitutionIdentifiers, InstitutionIdentifiersMDTO> {
}
