package es.minsait.ewpcv.repository.model.course;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Grade frequency mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class GradeFrequencyMDTO implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  String id;
  private String label;
  private Long percentage;

}
