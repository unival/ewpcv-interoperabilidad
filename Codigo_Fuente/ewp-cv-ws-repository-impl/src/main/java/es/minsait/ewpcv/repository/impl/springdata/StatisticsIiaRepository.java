package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.iia.StatisticsIia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * The interface StatisticsIiaRepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface StatisticsIiaRepository extends JpaRepository<StatisticsIia, String> {

    @Query("Select statIia FROM StatisticsIia statIia WHERE statIia.dumpDate = (Select MAX(i.dumpDate) FROM StatisticsIia i)")
    StatisticsIia findMostRecentStats();

}
