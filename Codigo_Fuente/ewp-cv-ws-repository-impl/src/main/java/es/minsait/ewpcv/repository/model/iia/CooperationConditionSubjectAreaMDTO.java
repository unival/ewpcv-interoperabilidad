package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Cooperation condition subject area mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class CooperationConditionSubjectAreaMDTO extends BaseMDTO {
  private static final long serialVersionUID = 5595012582362156952L;

  private String id;

  private SubjectAreaMDTO subjectArea;

  private int orderIndexSubAr;
}
