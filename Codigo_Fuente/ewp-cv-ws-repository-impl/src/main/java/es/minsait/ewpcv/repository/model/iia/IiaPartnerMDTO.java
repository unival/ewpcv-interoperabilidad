
package es.minsait.ewpcv.repository.model.iia;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Iia partner mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class IiaPartnerMDTO extends BaseMDTO {

  private static final long serialVersionUID = -4977241475565278583L;

  String id;

  private Integer version;

  private String institutionId;

  private String organizationUnitId;

  private ContactMDTO signerPersonId;

  private Date signingDate;

  private List<ContactMDTO> contacts;

}
