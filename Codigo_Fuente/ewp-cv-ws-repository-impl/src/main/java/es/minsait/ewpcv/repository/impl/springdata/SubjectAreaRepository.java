package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;

import es.minsait.ewpcv.model.omobility.SubjectArea;

/**
 * The interface Subject area repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface SubjectAreaRepository extends JpaRepository<SubjectArea, String> {

  SubjectArea findByIscedCode(String iscedCode);
}
