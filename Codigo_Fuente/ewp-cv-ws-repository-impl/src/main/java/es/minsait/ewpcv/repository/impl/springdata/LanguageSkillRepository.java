package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;

import es.minsait.ewpcv.model.omobility.LanguageSkill;

import java.util.List;

/**
 * The interface Language skill repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LanguageSkillRepository extends JpaRepository<LanguageSkill, String> {

  List<LanguageSkill> findByLanguageAndCefrLevel(String language, String cefrLevel);
}
