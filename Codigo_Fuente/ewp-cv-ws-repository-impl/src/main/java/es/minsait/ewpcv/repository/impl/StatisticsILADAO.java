package es.minsait.ewpcv.repository.impl;



import es.minsait.ewpcv.mapper.OmobilityILAStatsMDTOMapper;
import es.minsait.ewpcv.mapper.StatisticsILAMDTOMapper;
import es.minsait.ewpcv.repository.dao.IStatisticsILADAO;
import es.minsait.ewpcv.repository.impl.springdata.OmobilityILAStatsRepository;
import es.minsait.ewpcv.repository.impl.springdata.StatisticsILARepository;
import es.minsait.ewpcv.repository.model.omobility.OmobilityILAStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type StatisticsILA dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Repository
@Slf4j
public class StatisticsILADAO implements IStatisticsILADAO {

    @Autowired
    private StatisticsILARepository statisticsILARepository;
    @Autowired
    private OmobilityILAStatsRepository omobilityILAStatsRepository;
    @Autowired
    private StatisticsILAMDTOMapper statisticsILAMDTOMapper;
    @Autowired
    private OmobilityILAStatsMDTOMapper omobilityILAStatsMDTOMapper;

    @Override
    public void saveStatsILA(StatisticsILAMDTO statsILA){
        statisticsILARepository.save(statisticsILAMDTOMapper.dtoToEntity(statsILA));
    }

    @Override
    public List<OmobilityILAStatsMDTO> getStatsOfOmobilitiTable() {
        return omobilityILAStatsMDTOMapper.entitiesToDtos(omobilityILAStatsRepository.findGreaterThan2021());
    }

    @Override
    public List<StatisticsILAMDTO> getStatisticsRest() {
        return statisticsILAMDTOMapper.entitiesToDtos(statisticsILARepository.findMostRecentStatsILA());
    }

}
