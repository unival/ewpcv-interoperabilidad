package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.PhotoUrl;
import es.minsait.ewpcv.repository.model.organization.PhotoUrlMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Photo url mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface PhotoUrlMDTOMapper extends IModelDTOMapperCircular<PhotoUrl, PhotoUrlMDTO> {
}
