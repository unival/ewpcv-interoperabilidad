
package es.minsait.ewpcv.repository.model.omobility;

import java.math.BigInteger;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Result distribution category mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class ResultDistributionCategoryMDTO extends BaseMDTO {

  private static final long serialVersionUID = 6760997414117302695L;

  String id;

  private Integer version;

  private String label;

  private BigInteger count;

}
