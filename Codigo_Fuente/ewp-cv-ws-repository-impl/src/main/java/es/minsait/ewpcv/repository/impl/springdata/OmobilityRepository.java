package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.omobility.Mobility;
import es.minsait.ewpcv.model.omobility.MobilityStatus;

/**
 * The interface Omobility repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface OmobilityRepository extends JpaRepository<Mobility, String> {

  @Query("SELECT m FROM Mobility m WHERE LOWER(m.sendingInstitutionId) = LOWER(:sendingHeiId) AND LOWER(m.receivingInstitutionId) IN(:receivingHeiIdList) AND m.id IN(:omobilityIdList) "
          + "AND m.mobilityRevision = (SELECT MAX(mob.mobilityRevision) FROM Mobility mob WHERE mob.id IN(:omobilityIdList))")
  List<Mobility> findOMobilitiesByIdAndSendingInstitutionId(@Param(value = "sendingHeiId") String sendingHeiId,
      @Param(value = "omobilityIdList") List<String> omobilityIdList,
      @Param(value = "receivingHeiIdList") List<String> receivingHeiIdList);

  @Query("SELECT COUNT(m) FROM Mobility m WHERE LOWER(m.sendingInstitutionId) = LOWER(:sendingHeiId) AND m.id=:omobilityId")
  int countOMobilitiesByIdAndSendingInstitutionId(@Param(value = "sendingHeiId") String sendingHeiId,
      @Param(value = "omobilityId") String omobilityId);

  @Query("SELECT mob FROM Mobility mob WHERE LOWER(mob.id) = LOWER(:id) AND LOWER(mob.sendingInstitutionId) = LOWER(:sendingHeiId) AND mob.mobilityRevision in"
          + "(SELECT max(mobi.mobilityRevision) FROM Mobility mobi WHERE LOWER(mobi.id) = LOWER(:id) AND LOWER(mobi.sendingInstitutionId) = LOWER(:sendingHeiId))")
  Mobility findLastRevisionByIdAndSendingInstitutionId(@Param("id") String id, @Param("sendingHeiId") String sendingHeiId);

  @Modifying
  @Query("DELETE FROM Mobility mob WHERE LOWER(mob.id) = LOWER(:id) AND LOWER(mob.sendingInstitutionId) = LOWER(:sendingHeiId)")
  void deleteByIdAndSendingHeiId(@Param("id") String mobilityId, @Param("sendingHeiId") String sendingHeiId);

  @Query("SELECT mob.receivingInstitutionId FROM Mobility mob WHERE LOWER(mob.id) = LOWER(:id) AND LOWER(mob.sendingInstitutionId) = LOWER(:sendingHeiId) AND mob.mobilityRevision in"
          + "(SELECT max(mobi.mobilityRevision) FROM Mobility mobi where LOWER(mobi.id) = LOWER(:id) AND LOWER(mobi.sendingInstitutionId) = LOWER(:sendingHeiId))")
  String findLastMobilityRevisionReceiverHeiByIdAndSendingHei(@Param("id") String id, @Param("sendingHeiId") String sendingHeiId);

  @Query("SELECT DISTINCT(m.id) " + " FROM Mobility m " + " JOIN m.learningAgreement l"
      + " WHERE LOWER(m.sendingInstitutionId) = LOWER(:sendingHeiId) AND LOWER(m.receivingInstitutionId) = LOWER(:receivingHeiId) "
      + "AND l.id IS NOT NULL")
  List<String> findOMobilitiesIdBySendingHeiAndReceivingHei(@Param(value = "sendingHeiId") String sendingHeiId,
      @Param(value = "receivingHeiId") String receivingHeiId);

  @Query("SELECT mob FROM Mobility mob WHERE LOWER(mob.sendingInstitutionId) = LOWER(:sendingHeiId) AND LOWER(mob.receivingInstitutionId) IN(:receivingHeiIdList) "
      + "AND LOWER(mob.id) IN (:omobilityId) "
      + "AND mob.mobilityRevision in (SELECT max(mobilityRevision) FROM Mobility where id =mob.id)")
  List<Mobility> obtenerMobilities(@Param(value = "sendingHeiId") String sendingHeiId,
      @Param(value = "receivingHeiIdList") List<String> receivingHeiIdList,
      @Param(value = "omobilityId") List<String> omobilityId);

  @Query("SELECT mob FROM Mobility mob WHERE LOWER(mob.receivingInstitutionId) = LOWER(:receivingHeiId) AND LOWER(mob.sendingInstitutionId) IN(:sendingHeiIdList) AND LOWER(mob.remoteMobilityId) IN (:omobilityId) "
      + "AND mob.mobilityRevision in (SELECT max(mobilityRevision) FROM Mobility where id = mob.remoteMobilityId)")
  List<Mobility> obtenerMobilitiesByReceivingIdAndRemoteIdAndLastRevision(
      @Param(value = "receivingHeiId") String receivingHeiId,
      @Param(value = "sendingHeiIdList") List<String> sendingHeiIdList,
      @Param(value = "omobilityId") List<String> omobilityId);

  @Query("SELECT DISTINCT mob.id FROM Mobility mob WHERE LOWER(mob.sendingInstitutionId) = LOWER(:sendingHei) "
      + " AND mob.actualArrivalDate is null AND mob.actualDepartureDate is null "
      + " AND mob.statusOutgoing <> :statusOutgoingCancelled AND mob.statusOutgoing <> :statusOutgoingRecognized AND "
      + " (mob.statusIncomming is null OR mob.statusIncomming <> :statusIncommingVerified )")
  List<String> findImobilitiesARefrescar(@Param(value = "sendingHei") String sendingHei,
      @Param(value = "statusOutgoingCancelled") MobilityStatus statusOutgoingCancelled,
      @Param(value = "statusOutgoingRecognized") MobilityStatus statusOutgoingRecognized,
      @Param(value = "statusIncommingVerified") MobilityStatus statusIncommingVerified);

}
