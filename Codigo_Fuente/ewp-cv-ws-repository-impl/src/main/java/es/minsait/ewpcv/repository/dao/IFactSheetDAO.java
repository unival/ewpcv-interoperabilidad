package es.minsait.ewpcv.repository.dao;

import es.minsait.ewpcv.repository.model.organization.FactSheetMDTO;

/**
 * The interface Fact sheet dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IFactSheetDAO {

  FactSheetMDTO getFactSheetByHeiId(final String heiId);
}
