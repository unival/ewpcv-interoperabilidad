package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.*;
import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.dao.IDeletedElementsDAO;
import es.minsait.ewpcv.model.iia.Iia;
import es.minsait.ewpcv.model.iia.IiaApproval;
import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.model.omobility.LanguageSkill;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.impl.springdata.*;
import es.minsait.ewpcv.repository.model.iia.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Iia dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class IiaDAO extends QueryDslRepositorySupport implements IIiaDAO {

  private IiaMDTOMapper iiaMDTOMapper;
  private IiaRepository iiaRepository;
  private LanguageSkillRepository languageSkillRepository;
  private MobilityTypeRepository mobilityTypeRepository;
  private MobilityTypeMDTOMapper mobilityTypeMapper;
  private IiaApprovalMDTOMapper iiaApprovalMapper;
  private IiaApprovalRepository iiaApprovalRepository;
  private CooperationConditionRepository cooperationConditionRepository;
  private IDeletedElementsDAO deletedElementsDAO;

  @Autowired
  public IiaDAO(final IiaMDTOMapper iiaMDTOMapper, final IiaRepository iiaRepository,
      final LanguageSkillRepository languageSkillRepository,
      final IiaPartnerMDTOMapper iiaPartnerMapper, final MobilityTypeRepository mobilityTypeRepository,
      final MobilityTypeMDTOMapper mobilityTypeMapper, final IiaApprovalMDTOMapper iiaApprovalMapper,
      final IiaApprovalRepository iiaApprovalRepository,
      final CooperationConditionRepository cooperationConditionRepository, final IDeletedElementsDAO deletedElementsDAO) {
    super(Iia.class);
    this.iiaMDTOMapper = iiaMDTOMapper;
    this.iiaRepository = iiaRepository;
    this.languageSkillRepository = languageSkillRepository;
    this.mobilityTypeRepository = mobilityTypeRepository;
    this.mobilityTypeMapper = mobilityTypeMapper;
    this.iiaApprovalMapper = iiaApprovalMapper;
    this.iiaApprovalRepository = iiaApprovalRepository;
    this.cooperationConditionRepository = cooperationConditionRepository;
    this.deletedElementsDAO = deletedElementsDAO;
  }

  @Override
  public List<String> getIiaIdsByCriteria(final Map<Object, Object> queryParams) {

    final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

    final CriteriaQuery<String> cq = cb.createQuery(String.class);
    final Root<Iia> root = cq.from(Iia.class);

    cq.distinct(true);
    cq.select(root.get("interopId"));

    final Join<Object, Object> joinCoopCond = root.join("cooperationConditions");
    final Join<Object, Object> joinReceivingPartner = joinCoopCond.join("receivingPartner");
    final Join<Object, Object> joinSendingPartner = joinCoopCond.join("sendingPartner");

    final List<Predicate> predicates =
        crearPredicate(cb, root, joinReceivingPartner, joinSendingPartner, joinCoopCond, queryParams);

    cq.where(predicates.toArray(new Predicate[predicates.size()]));
    return getEntityManager().createQuery(cq).getResultList();
  }

  @Override
  public List<IiaMDTO> getIiaListByCriteria(final Map<Object, Object> queryParams) {
    final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

    final CriteriaQuery<Iia> cq = cb.createQuery(Iia.class);
    final Root<Iia> root = cq.from(Iia.class);
    cq.select(root);

    final Join<Object, Object> joinCoopCond = root.join("cooperationConditions");
    final Join<Object, Object> joinReceivingPartner = joinCoopCond.join("receivingPartner");
    final Join<Object, Object> joinSendingPartner = joinCoopCond.join("sendingPartner");

    final List<Predicate> predicates =
        crearPredicate(cb, root, joinReceivingPartner, joinSendingPartner, joinCoopCond, queryParams);


    cq.where(predicates.toArray(new Predicate[predicates.size()]));
    final List<Iia> iiaList =
        getEntityManager().createQuery(cq).getResultList().stream().distinct().collect(Collectors.toList());
    return iiaMDTOMapper.entitiesToDtos(iiaList);
  }

  @Override
  public IiaMDTO findIiaByRemoteIiaIdAndIsRemote(final String remoteIiaId, final Boolean isRemote) {
    return iiaMDTOMapper.entityToDto(iiaRepository.findByRemoteIiaIdAndIsRemote(remoteIiaId, isRemote));
  }

  @Override
  public Integer countIiaByRemoteIiaIdAndIsRemote(final String remoteIiaId, final Boolean isRemote) {
    return iiaRepository.countByRemoteIiaIdAndIsRemote(remoteIiaId, isRemote);
  }

  @Override
  public void removeIiaByRemoteIiaId(final String remoteIiaId) {
    final Iia deletedIia = iiaRepository.findByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.TRUE);
    if (Objects.nonNull(deletedIia)) {
      iiaRepository.delete(deletedIia.getIiaId());
    }
  }

  @Override
  public void removeIiaByInteropId(final String interopId, final String hei) {
    final Iia deletedIia = iiaRepository.findByInteropIdIgnoreCase(interopId);
    if (Objects.nonNull(deletedIia)) {
      iiaRepository.delete(deletedIia.getIiaId());
      deletedElementsDAO.save(deletedIia.getIsRemote() ? deletedIia.getRemoteIiaId() : deletedIia.getInteropId(), NotificationTypes.IIA, hei);
    }
  }

  @Override
  public void desvincularIiaByInteropId(final String interopId) {
    iiaRepository.desvincularIiaByInteropId(interopId);
  }

  @Override
  public List<String> findRemoteIiaIdByIsRemoteAndPartnersInstitution(final String sendingHei, final String receivingPartner,
      final Boolean isRemote) {
    return iiaRepository.findRemoteIiaIdByIsRemoteAndPartnersInstitution(sendingHei, receivingPartner, isRemote);
  }

  private List<Predicate> crearPredicate(final CriteriaBuilder cb, final Root<Iia> root,
      final Join<Object, Object> joinReceivingPartner, final Join<Object, Object> joinSendingPartner,
      final Join<Object, Object> joinCoopCond, final Map<Object, Object> queryParams) {

    final List<Predicate> predicates = new ArrayList<>();

    final List<String> partnerHeiIdList = (List<String>) queryParams.get("partnerHeiId");
    partnerHeiIdList.replaceAll(String::toLowerCase);

    final Predicate and1 = cb.equal(cb.lower(joinReceivingPartner.get("institutionId")),
        ((String) queryParams.get("heiId")).toLowerCase());
    final Predicate and2 = cb.lower(joinSendingPartner.get("institutionId")).in(partnerHeiIdList);

    final Predicate and3 = cb.lower(joinReceivingPartner.get("institutionId")).in(partnerHeiIdList);
    final Predicate and4 =
        cb.equal(cb.lower(joinSendingPartner.get("institutionId")), ((String) queryParams.get("heiId")).toLowerCase());

    final Predicate principalAnd1 = cb.and(and1, and2);
    final Predicate principalAnd2 = cb.and(and3, and4);

    final Predicate mainOr = cb.or(principalAnd1, principalAnd2);
    predicates.add(cb.and(cb.isFalse(root.get("isRemote")), cb.isTrue(root.get("isExposed")), mainOr));

    if (Objects.nonNull(queryParams.get("receivingAcademicYearId"))) {
      final Stack<Predicate> predicatesAcademicyear = new Stack<>();
      final List<String> receivingAcademicYearIdList = (List<String>) queryParams.get("receivingAcademicYearId");
      for (final String academicYearId : receivingAcademicYearIdList) {
        final Predicate academicYearPredicate1 =
            cb.lessThanOrEqualTo(cb.function("YEAR", Double.class, joinCoopCond.get("startDate")), Double.parseDouble(academicYearId.split("/")[0]));
        final Predicate academicYearPredicate2 =
            cb.greaterThanOrEqualTo(cb.function("YEAR", Double.class, joinCoopCond.get("endDate")), Double.parseDouble(academicYearId.split("/")[1]));
        final Predicate predicatePrincipal = cb.and(academicYearPredicate1, academicYearPredicate2);
        predicatesAcademicyear.push(predicatePrincipal);
      }
      Predicate or = cb.or(predicatesAcademicyear.pop());
      while (!predicatesAcademicyear.empty()) {
        or = cb.or(or, predicatesAcademicyear.pop());
      }
      predicates.add(or);
    }
    if (Objects.nonNull(queryParams.get("modifiedSince"))) {
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      final ZonedDateTime zdt = ZonedDateTime.parse((String) queryParams.get("modifiedSince"), dtf);
      final Date dateTimeFormated = Date.from(zdt.toInstant());

      final Path<Date> dateModifiedSincePath = root.get("modifyDate");
      predicates.add(cb.greaterThanOrEqualTo(dateModifiedSincePath, dateTimeFormated));
    }
    if (Objects.nonNull(queryParams.get("iiaId"))) {
      final Stack<Predicate> predicatesIiaId = new Stack<>();
      final List<String> iiaIdList = (List<String>) queryParams.get("iiaId");
      for (final String iiaId : iiaIdList) {
        final Predicate iiaIIdPredicate = cb.equal(cb.lower(root.get("interopId")), iiaId.toLowerCase());
        predicatesIiaId.push(iiaIIdPredicate);
      }
      Predicate or = cb.or(predicatesIiaId.pop());
      while (!predicatesIiaId.empty()) {
        or = cb.or(or, predicatesIiaId.pop());
      }
      predicates.add(or);
    }
    if (Objects.nonNull(queryParams.get("iiaCode"))) {
      final Stack<Predicate> predicatesIiaCode = new Stack<>();
      final List<String> iiaCodeList = (List<String>) queryParams.get("iiaCode");
      for (final String iiaCode : iiaCodeList) {
        final Predicate iiaCodePredicate = cb.equal(cb.lower(root.get("iiaCode")), iiaCode.toLowerCase());
        predicatesIiaCode.push(iiaCodePredicate);
      }
      Predicate or = cb.or(predicatesIiaCode.pop());
      while (!predicatesIiaCode.empty()) {
        or = cb.or(or, predicatesIiaCode.pop());
      }
      predicates.add(or);
    }
    return predicates;
  }

  @Override
  public void actualizarApprovalCoopCondHash(final String remoteIiaId, final String conditionsHash) {
    iiaRepository.actualizarApprovalCoopCondHash(remoteIiaId, conditionsHash, new Date());
  }

  @Override
  public void updateIiaApprovalByInteropId(final String interopId, final String approvalConditionHash, final Date approvalDate) {
    iiaRepository.updateIiaApprovalByInteropId(interopId, approvalConditionHash, approvalDate);
  }

  @Override
  public void actualizaCoopCondHashByInteropId(final String interopId, final String conditionsHash) {
    iiaRepository.actualizaCoopCondHashByInteropId(interopId, conditionsHash);
  }

  @Override
  public int contarNoVinculadosPorInstituciones(final Set<String> partners) {
    return iiaRepository.contarNoVinculadosPorInstituciones(partners);
  }

  @Override
  public IiaMDTO save(final IiaMDTO iia) {

    final Iia iiaEntity = iiaMDTOMapper.dtoToEntity(iia);

    final List<MobilityTypeMDTO> mobilityTypes = iia.getCooperationConditions().stream()
        .map(CooperationConditionMDTO::getMobilityType).distinct().collect(Collectors.toList());
    guradarMobilityTypes(mobilityTypes);

    iiaEntity.getCooperationConditions().stream().forEach(coopCond -> {
      // Buscamos y si existe lo seteamos
      final MobilityType mt = mobilityTypeRepository.findByMobilityCategoryAndMobilityGroup(
          coopCond.getMobilityType().getMobilityCategory(), coopCond.getMobilityType().getMobilityGroup());
      if (Objects.nonNull(mt)) {
        coopCond.setMobilityType(mt);
      }
      coopCond.getLanguageSkill().stream().forEach(langSkill -> {
        // Buscamos y si no existe lo creamos
        final List<LanguageSkill> ls =
            Objects.nonNull(langSkill.getLanguageSkill()) ? languageSkillRepository.findByLanguageAndCefrLevel(
                langSkill.getLanguageSkill().getLanguage(), langSkill.getLanguageSkill().getCefrLevel()) : null;
        if (Objects.nonNull(ls) && !ls.isEmpty()) {
          langSkill.setLanguageSkill(ls.get(0));
        } else {
          langSkill.setLanguageSkill(languageSkillRepository.save(langSkill.getLanguageSkill()));
        }
      });
    });
    iiaEntity.setModifyDate(new Date());
    return iiaMDTOMapper.entityToDto(iiaRepository.save(iiaEntity));
  }

  private void guradarMobilityTypes(final List<MobilityTypeMDTO> mobilityTypes) {
    mobilityTypes.stream().forEach(mt -> {
      final MobilityType searched = mobilityTypeRepository
          .findByMobilityCategoryAndMobilityGroup(mt.getMobilityCategory(), mt.getMobilityGroup());
      if (Objects.isNull(searched)) {
        mobilityTypeRepository.save(mobilityTypeMapper.dtoToEntity(mt));
      }
    });
  }


  @Override
  public IiaMDTO findByInteropId(final String interopId) {
    return iiaMDTOMapper.entityToDto(iiaRepository.findByInteropIdIgnoreCase(interopId));
  }

  @Override
  public IiaApprovalMDTO findLastIiaApprovalByIiaRemoteIiaId(String iiaRemoteIiaId) {
    List<IiaApproval> lastIiaApprovalByIiaRemoteIiaId = iiaApprovalRepository.findLastIiaApprovalByIiaRemoteIiaId(iiaRemoteIiaId);
    return !CollectionUtils.isEmpty(lastIiaApprovalByIiaRemoteIiaId) ? iiaApprovalMapper.entityToDto(lastIiaApprovalByIiaRemoteIiaId.get(0)) : null;
  }

  @Override
  public Date obtenerApprovalDatePorRemoteIiaIdAndIsRemote1(final String remoteIiaId) {
    return iiaRepository.obtenerApprovalDatePorRemoteIiaIdAndIsRemote1(remoteIiaId);
  }

  @Override
  public IiaApprovalMDTO saveIiaApproval(IiaApprovalMDTO iiaApprovalMDTO) {
    final IiaApproval iiaApprovalEntity = iiaApprovalMapper.dtoToEntity(iiaApprovalMDTO);
    IiaApproval iiaApprovalSaved = iiaApprovalRepository.saveAndFlush(iiaApprovalEntity);
    return iiaApprovalMapper.entityToDto(iiaApprovalSaved);
  }

  @Override
  public void updateCoopCondById(final String coopCondId, final String approvalId) {
    cooperationConditionRepository.updateCoopCondById(coopCondId, approvalId);
  }

  @Override
  public IiaApprovalMDTO findLastIiaApprovalByLocalIia(String localIia) {
    return iiaApprovalMapper.entityToDto(iiaApprovalRepository.findFirst1ByLocalIiaOrderByDateIiaApprovalDesc(localIia));
  }

  @Override
  public boolean RemoteIiaIsAproved(String iiaRemoteIiaId) {
    return iiaApprovalRepository.existsByRemoteIia(iiaRemoteIiaId);
  }
}
