package es.minsait.ewpcv.mapper;

import es.minsait.ewpcv.model.iia.StatisticsIia;
import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface StatisticsIia mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface StatisticsIiaMDTOMapper extends IModelDTOMapperCircular<StatisticsIia, StatisticsIiaMDTO> {
}
