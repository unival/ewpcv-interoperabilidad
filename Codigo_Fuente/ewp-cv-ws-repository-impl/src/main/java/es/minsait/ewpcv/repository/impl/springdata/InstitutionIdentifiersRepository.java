package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.dictionary.InstitutionIdentifiers;

/**
 * The interface Institution identifiers repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface InstitutionIdentifiersRepository extends JpaRepository<InstitutionIdentifiers, String> {

  @Query("SELECT inst.id FROM InstitutionIdentifiers inst")
  List<String> findAllHeiSCHACCode();

  @Query("SELECT inst FROM InstitutionIdentifiers inst WHERE LOWER(inst.schac) in (:heisIds)")
  List<InstitutionIdentifiers> findAllBySCHAC(@Param("heisIds") List<String> heisIds);

  @Query("SELECT COUNT(1) FROM InstitutionIdentifiers inst WHERE LOWER(inst.schac) = LOWER(:schac)")
  int countBySchac(@Param("schac") String schac);

}
