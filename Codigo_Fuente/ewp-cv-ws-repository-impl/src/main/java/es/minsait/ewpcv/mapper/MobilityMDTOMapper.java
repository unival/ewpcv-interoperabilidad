package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.Mobility;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Mobility mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {LearningAgreementMDTOMapper.class})
public interface MobilityMDTOMapper extends IModelDTOMapperCircular<Mobility, MobilityMDTO> {
}
