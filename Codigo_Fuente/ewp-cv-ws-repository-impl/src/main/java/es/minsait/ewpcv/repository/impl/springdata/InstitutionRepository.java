package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.organization.Institution;

/**
 * The interface Institution repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface InstitutionRepository extends JpaRepository<Institution, String> {

  @Query("SELECT COUNT(inst) FROM Institution inst WHERE LOWER(inst.institutionId) = LOWER(:instId)")
  int getTotalInstitutionById(@Param(value = "instId") String instId);

  @Query("SELECT inst FROM Institution inst WHERE LOWER(inst.institutionId) = LOWER(:heiId)")
  Institution findIntitutionByHeiId(@Param(value = "heiId") String heiId);

  @Query("SELECT inst FROM Institution inst WHERE LOWER(inst.institutionId) in (:heiIds)")
  List<Institution> findAllByHeiId(@Param("heiIds") List<String> heiIds);
}
