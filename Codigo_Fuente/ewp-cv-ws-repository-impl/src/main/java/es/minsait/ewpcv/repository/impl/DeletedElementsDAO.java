package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.model.iia.DeletedElements;
import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.dao.IDeletedElementsDAO;
import es.minsait.ewpcv.repository.impl.springdata.DeletedElementsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
@Slf4j
public class DeletedElementsDAO implements IDeletedElementsDAO {

    private DeletedElementsRepository deletedElementsRepository;

    @Autowired
    public DeletedElementsDAO(final DeletedElementsRepository deletedElementsRepository) {
        this.deletedElementsRepository = deletedElementsRepository;
    }

    @Override
    public void save(String elementId, NotificationTypes elementType, String ownerSchac) {
        DeletedElements deleted = new DeletedElements();
        deleted.setElementId(elementId);
        deleted.setElementType(elementType);
        deleted.setOwnerSchac(ownerSchac);
        deleted.setDeletionDate(new Date());
        deletedElementsRepository.save(deleted);
    }

}
