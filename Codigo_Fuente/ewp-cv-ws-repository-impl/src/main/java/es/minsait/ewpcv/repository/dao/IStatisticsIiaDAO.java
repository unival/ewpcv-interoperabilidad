package es.minsait.ewpcv.repository.dao;


import es.minsait.ewpcv.repository.model.iia.IiaStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;

import java.util.List;

/**
 * The interface StatisticsIia dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IStatisticsIiaDAO {

    void saveStatsIia(StatisticsIiaMDTO statsIia);

    List<IiaStatsMDTO> getOmobilityIiaStats();

    StatisticsIiaMDTO getStatisticsRest();
}
