package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.AcademicYear;
import es.minsait.ewpcv.repository.model.course.AcademicYearMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Academic year mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface AcademicYearMDTOMapper extends IModelDTOMapperCircular<AcademicYear, AcademicYearMDTO> {
}
