package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.ResultDistributionCategory;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionCategoryMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Result distribution category mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface ResultDistributionCategoryMDTOMapper
    extends IModelDTOMapperCircular<ResultDistributionCategory, ResultDistributionCategoryMDTO> {
}
