package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.FlexibleAddress;
import es.minsait.ewpcv.repository.model.organization.FlexibleAddressMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Flexible address mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface FlexibleAddressMDTOMapper extends IModelDTOMapperCircular<FlexibleAddress, FlexibleAddressMDTO> {
}
