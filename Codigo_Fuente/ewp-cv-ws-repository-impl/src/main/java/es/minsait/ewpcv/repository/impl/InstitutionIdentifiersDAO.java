package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import es.minsait.ewpcv.mapper.InstitutionIdentifiersMDTOMapper;
import es.minsait.ewpcv.repository.dao.IInstitutionIdentifiersDAO;
import es.minsait.ewpcv.repository.impl.springdata.InstitutionIdentifiersRepository;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Institution identifiers dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class InstitutionIdentifiersDAO implements IInstitutionIdentifiersDAO {

  private InstitutionIdentifiersMDTOMapper institutionIdentifiersMDTOMapper;
  private InstitutionIdentifiersRepository institutionIdentifiersRepository;

  @Autowired
  public InstitutionIdentifiersDAO(final InstitutionIdentifiersMDTOMapper institutionIdentifiersMDTOMapper,
      final InstitutionIdentifiersRepository institutionIdentifiersRepository) {
    this.institutionIdentifiersMDTOMapper = institutionIdentifiersMDTOMapper;
    this.institutionIdentifiersRepository = institutionIdentifiersRepository;
  }

  @Override
  public boolean existInstitutionBySchac(final String schac) {
    return this.institutionIdentifiersRepository.countBySchac(schac) > 0;
  }

  @Override
  public void saveOrUpdateInstitutionIdentifier(final InstitutionIdentifiersMDTO institutionIdentifiersMDTO) {
    institutionIdentifiersRepository.save(institutionIdentifiersMDTOMapper.dtoToEntity(institutionIdentifiersMDTO));
  }

  @Override
  public List<InstitutionIdentifiersMDTO> findAllBySCHAC(final List<String> heisIds) {
    heisIds.replaceAll(String::toLowerCase);
    return institutionIdentifiersMDTOMapper.entitiesToDtos(institutionIdentifiersRepository.findAllBySCHAC(heisIds));
  }
}
