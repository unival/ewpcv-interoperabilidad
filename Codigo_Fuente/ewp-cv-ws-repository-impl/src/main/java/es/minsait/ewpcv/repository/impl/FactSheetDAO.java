package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.minsait.ewpcv.mapper.FactSheetMDTOMapper;
import es.minsait.ewpcv.repository.dao.IFactSheetDAO;
import es.minsait.ewpcv.repository.impl.springdata.FactsheetRepository;
import es.minsait.ewpcv.repository.model.organization.FactSheetMDTO;

/**
 * The type Fact sheet dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class FactSheetDAO implements IFactSheetDAO {

  @Autowired
  private FactSheetMDTOMapper factSheetMDTOMapper;

  @Autowired
  private FactsheetRepository factsheetRepository;

  @Override
  public FactSheetMDTO getFactSheetByHeiId(final String heiId) {
    return factSheetMDTOMapper.entityToDto(factsheetRepository.obtenerFactsheetByHeiId(heiId));
  }
}
