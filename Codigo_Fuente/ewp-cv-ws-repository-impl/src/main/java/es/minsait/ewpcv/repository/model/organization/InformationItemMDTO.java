
package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Information item mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class InformationItemMDTO extends BaseMDTO {

  private static final long serialVersionUID = 4502519936753455562L;

  String id;

  private String type;

  private Integer version;

  private ContactDetailsMDTO contactDetails;

}
