package es.minsait.ewpcv.repository.model.course;

import java.util.Arrays;

/**
 * The enum Learning opportunity specification type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LearningOpportunitySpecificationType {
  COURSE, CLASS, MODULE, DEGREE_PROGRAMME;

  public static String displayName(final LearningOpportunitySpecificationType losType) {
    String displayName;
    switch (losType) {
      case DEGREE_PROGRAMME:
        displayName = "Degree Programme";
        break;
      default:
        displayName = losType.toString();
        displayName = displayName.charAt(0) + displayName.substring(1).toLowerCase();
        break;
    }
    return displayName;
  }

  public static String abbreviation(final LearningOpportunitySpecificationType losType) {
    final String abbreviation;
    switch (losType) {
      case COURSE:
        abbreviation = "CR";
        break;
      case CLASS:
        abbreviation = "CLS";
        break;
      case MODULE:
        abbreviation = "MOD";
        break;
      case DEGREE_PROGRAMME:
        abbreviation = "DEP";
        break;
      default:
        abbreviation = "";
    }
    return abbreviation;
  }

  public static String loiAbbreviation(final LearningOpportunitySpecificationType losType) {
    return abbreviation(losType) + "I";
  }

  public static LearningOpportunitySpecificationType getTypeByAbreviation(final String abreviation) {
    return Arrays.stream(values()).filter(type -> abbreviation(type).equals(abreviation.toUpperCase())).findFirst()
        .orElse(null);
  }
}
