package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.LearningAgreement;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Learning agreement mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {LaComponentMDTOMapper.class, TranscriptOfRecordMDTOMapper.class})
public interface LearningAgreementMDTOMapper extends IModelDTOMapperCircular<LearningAgreement, LearningAgreementMDTO> {
}
