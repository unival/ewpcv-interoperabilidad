package es.minsait.ewpcv.repository.model.course;

import java.util.List;

import es.minsait.ewpcv.model.course.LevelType;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Level mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LevelMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;


  String id;
  private Integer version;
  private LevelType type;
  private String value;
  private List<LanguageItemMDTO> description;

}
