package es.minsait.ewpcv.repository.dao;

import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.model.iia.IiaRegressionsMDTO;

public interface IIiaRegressionsDAO {

    IiaRegressionsMDTO saveRegression(IiaRegressionsMDTO regression);
}
