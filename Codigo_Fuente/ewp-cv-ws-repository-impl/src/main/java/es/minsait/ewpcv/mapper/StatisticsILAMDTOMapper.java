package es.minsait.ewpcv.mapper;


import es.minsait.ewpcv.model.omobility.StatisticsILA;
import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface StatisticsILA mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface StatisticsILAMDTOMapper extends IModelDTOMapperCircular<StatisticsILA, StatisticsILAMDTO> {
}
