package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.course.TranscriptOfRecordMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;

/**
 * The interface Tor dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface ITorDAO {

    List<String> obtenerMobilitiesWithTorIds(String receivingHei, List<String> sendingHeisIds, String modifiedSince,
                                             List<String> receivingHeiIdToFilter);

    List<MobilityMDTO> findTorByOmobilityIdAndReceivingInstitutionId(String receivingHei, List<String> omobilityId,
                                                                     List<String> heisCoveredByClient);

    MobilityMDTO obtenerMobilityWithTor(String omobilityId);

    List<String> findLocalMobilitiesWithTorId(String receivingHei, String sendingHei);

    List<TranscriptOfRecordMDTO> findTorsByMobilityId(String mobilityId);

    void eliminarTor(TranscriptOfRecordMDTO tor);
}
