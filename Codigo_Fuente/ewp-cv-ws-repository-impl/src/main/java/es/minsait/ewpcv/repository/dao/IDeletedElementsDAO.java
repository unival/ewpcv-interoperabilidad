package es.minsait.ewpcv.repository.dao;

import es.minsait.ewpcv.model.notificacion.NotificationTypes;

public interface IDeletedElementsDAO {

    void save(String elementId, NotificationTypes elementType, String ownerSchac);

}
