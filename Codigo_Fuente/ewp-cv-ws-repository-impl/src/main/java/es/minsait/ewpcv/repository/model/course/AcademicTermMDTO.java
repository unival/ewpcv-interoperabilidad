
package es.minsait.ewpcv.repository.model.course;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Academic term mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class AcademicTermMDTO extends BaseMDTO {

  String id;

  private String version;

  private String institutionId;

  private String organizationUnitId;

  private AcademicYearMDTO academicYear;

  private List<LanguageItemMDTO> dispName;

  private Date startDate;

  private Date endDate;

  private BigInteger totalTerms;

  private BigInteger termNumber;

}
