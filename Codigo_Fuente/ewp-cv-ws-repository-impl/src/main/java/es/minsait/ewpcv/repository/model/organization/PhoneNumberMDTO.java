
package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;


/**
 * The type Phone number mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class PhoneNumberMDTO extends BaseMDTO {

  private static final long serialVersionUID = 6553888528555814641L;

  String id;

  private Integer version;

  private String e164;

  private String extensionNumber;

  private String otherFormat;
}
