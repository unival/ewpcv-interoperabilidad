package es.minsait.ewpcv.repository.impl;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.mapper.LearningAgreementMDTOMapper;
import es.minsait.ewpcv.mapper.LearningOpportunityInstanceMDTOMapper;
import es.minsait.ewpcv.mapper.LearningOpportunitySpecificationMDTOMapper;
import es.minsait.ewpcv.repository.dao.ILearningAgreementDAO;
import es.minsait.ewpcv.repository.impl.springdata.LearningAgreementRepository;
import es.minsait.ewpcv.repository.impl.springdata.LearningOpportunityInstanceRepository;
import es.minsait.ewpcv.repository.impl.springdata.LearningOpportunitySpecificationRepository;
import es.minsait.ewpcv.repository.model.course.LearningOpportunityInstanceMDTO;
import es.minsait.ewpcv.repository.model.course.LearningOpportunitySpecificationMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Learning agreement dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class LearningAgreementDAO implements ILearningAgreementDAO {

  private LearningAgreementMDTOMapper learningAgreementMDTOMapper;
  private LearningAgreementRepository learningAgreementRepository;
  private LearningOpportunitySpecificationRepository losRepository;
  private LearningOpportunitySpecificationMDTOMapper losMapper;
  private LearningOpportunityInstanceRepository loiRepository;
  private LearningOpportunityInstanceMDTOMapper loiMapper;

  @Autowired
  public LearningAgreementDAO(final LearningAgreementMDTOMapper learningAgreementMDTOMapper,
      final LearningAgreementRepository learningAgreementRepository,
      final LearningOpportunitySpecificationRepository losRepository,
      final LearningOpportunitySpecificationMDTOMapper losMapper,
      final LearningOpportunityInstanceRepository loiRepository,
      final LearningOpportunityInstanceMDTOMapper loiMapper) {
    this.learningAgreementMDTOMapper = learningAgreementMDTOMapper;
    this.learningAgreementRepository = learningAgreementRepository;
    this.losRepository = losRepository;
    this.losMapper = losMapper;
    this.loiRepository = loiRepository;
    this.loiMapper = loiMapper;
  }

  @Override
  public List<LearningAgreementMDTO> findAllLearningAgreementRevisionsById(final String id) {
    return learningAgreementMDTOMapper
        .entitiesToDtos(learningAgreementRepository.findAllLearningAgreementRevisionsById(id));
  }

  @Override
  public LearningAgreementMDTO findLearningAgreementByChangesId(final String mobilityId, final String changesId) {
    return learningAgreementMDTOMapper
        .entityToDto(learningAgreementRepository.findLearningAgreementByChangesId(mobilityId, changesId));
  }

  @Override
  public LearningAgreementMDTO findLearningAgreementByIdAndRevision(final String id, final Integer revision) {
    return learningAgreementMDTOMapper
        .entityToDto(learningAgreementRepository.findLearningAgreementByIdAndRevision(id, revision));
  }

  @Override
  public Boolean hasFirstVersion(final String id) {
    return learningAgreementRepository.countFirstVersion(id) > 0;
  }

  @Override
  public void saveLearningAgreement(final LearningAgreementMDTO learningAgreementMDTO) {
    learningAgreementRepository.save(learningAgreementMDTOMapper.dtoToEntity(learningAgreementMDTO));
  }

  @Override
  public Boolean existeLos(final String losId) {
    return Objects.nonNull(losRepository.findByIdIgnoreCase(losId));
  }

  @Override
  public LearningOpportunitySpecificationMDTO findLosByIdOrLosCode(final String losId, final String losCode,
      final String institutionId) {
    // primero buscamos por id
    if (!Strings.isNullOrEmpty(losId)) {
      return losMapper.entityToDto(losRepository.findByIdIgnoreCase(losId));
    } else {
      // si no tiene id buscamos por losCode
      if (!Strings.isNullOrEmpty(losCode)) {
        return losMapper.entityToDto(losRepository.findLosByLosCode(losCode, institutionId));
      } else {
        return null;
      }
    }
  }

  @Override
  public LearningOpportunityInstanceMDTO findLoiById(final String loiId) {
    return loiMapper.entityToDto(loiRepository.findByIdIgnoreCase(loiId));
  }

  @Override
  public Integer obtenerLoiVersion(final String loiId) {
    return loiRepository.obtenerLoiVersion(loiId);
  }

  @Override
  public Integer obtenerLosVersion(final String losId) {
    return losRepository.obtenerLosVersion(losId);
  }

  @Override
  public String findLosByLosCodeAndInstitution(final String losCode, final String heiId) {
    return losRepository.findLosIdByLosCodeAndInstitution(losCode, heiId);
  }

}
