package es.minsait.ewpcv.mapper;

import es.minsait.ewpcv.model.omobility.OmobilityILAStats;
import es.minsait.ewpcv.repository.model.omobility.OmobilityILAStatsMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface OmobilityILAStats mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface OmobilityILAStatsMDTOMapper extends IModelDTOMapperCircular<OmobilityILAStats, OmobilityILAStatsMDTO> {
}