package es.minsait.ewpcv.repository.dao;

import java.util.List;
import java.util.Map;

import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;

/**
 * The interface Omobility dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOmobilityDAO {

  List<MobilityMDTO> findOMobilitiesByIdAndSendingInstitutionId(String sendingHeiId, List<String> omobilityIdList,
      List<String> receivingHeiIdList);

  int countOMobilitiesByIdAndSendingInstitutionId(String sendingHeiId, String oMobilityId);

  List<String> getLaIdsByCriteria(Map<Object, Object> queryParams);

  MobilityMDTO save(MobilityMDTO mobilityMDTO);

  MobilityMDTO findLastRevisionByIdAndSendingInstitution(final String id, final String sendingHeiId);

  void eliminarMobilityByIdAndSendingHeiId(String id, String sendingHeiId);

  String findLastMobilityRevisionReceiverHeiByIdAndSendingHei(String id, final String sendingHeiId);

  List<String> findLocalOMobilitiesId(String sendingHeiId, String receivingHei);

  List<String> getIdsByCriteria(Map<Object, Object> queryParams);

  List<MobilityMDTO> obtenerMobilites(String sendingHeiId, List<String> receivingHeiIdList, List<String> omobilityId);

  List<MobilityMDTO> obtenerMobilitesByReceivingAndHeiRemoteIdAndLastRevision(String receivingHeiId,
      final List<String> sendingHeiIdList, List<String> omobilityId);

  List<String> findImobilitiesARefrescar(String sendingHei);
}
