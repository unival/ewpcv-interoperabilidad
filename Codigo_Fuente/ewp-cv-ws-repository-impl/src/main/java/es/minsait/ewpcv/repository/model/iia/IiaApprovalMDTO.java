
package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import java.util.Date;

/**
 * The type Iia approval mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class IiaApprovalMDTO extends BaseMDTO {

  private static final long serialVersionUID = 7235122653559858439L;

  private String id;
  private String localIia;
  private String remoteIia;
  private String iiaApiVersion;
  private String localHash;
  private String remoteHash;
  private Date dateIiaApproval;
  private String localHashv7;
  private String remoteHashv7;

  private byte[] localMessage;
  private byte[] remoteMessage;
}
