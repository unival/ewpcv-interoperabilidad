
package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Recognized la component mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class RecognizedLaComponentMDTO extends BaseMDTO {

  private static final long serialVersionUID = 4905529338509956464L;

  private String learningAgreementId;

  private String recognizedLaComponentId;

  private int learningAgreementRevision;
}
