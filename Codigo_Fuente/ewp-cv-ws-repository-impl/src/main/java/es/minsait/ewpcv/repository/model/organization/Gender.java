
package es.minsait.ewpcv.repository.model.organization;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Gender.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum Gender {
  NOT_KNOWN(BigInteger.ZERO), MALE(BigInteger.ONE), FEMALE(BigInteger.valueOf(2)), NOT_APPLICABLE(
      BigInteger.valueOf(9));

  private final BigInteger value;

  Gender(BigInteger v) {
    value = v;
  }

  public static String[] names() {
    Gender[] genders = values();
    return Arrays.stream(genders).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[genders.length]);
  }

  public static Gender getGenderByValue(BigInteger value) {
    for (Gender g : values()) {
      if (g.value.equals(value)) {
        return g;
      }
    }

    return null;
  }

  public BigInteger value() {
    return value;
  }
}
