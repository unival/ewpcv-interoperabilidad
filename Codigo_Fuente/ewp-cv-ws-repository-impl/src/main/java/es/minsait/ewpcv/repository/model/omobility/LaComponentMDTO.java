package es.minsait.ewpcv.repository.model.omobility;

import java.util.List;

import es.minsait.ewpcv.model.omobility.LaComponentReasonCode;
import es.minsait.ewpcv.model.omobility.LaComponentType;
import es.minsait.ewpcv.model.omobility.LearningAgreementComponentStatus;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.course.CreditMDTO;
import es.minsait.ewpcv.repository.model.course.LearningOpportunityInstanceMDTO;
import es.minsait.ewpcv.repository.model.course.LearningOpportunitySpecificationMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type La component mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LaComponentMDTO extends BaseMDTO {

  private static final long serialVersionUID = -6873182811117180847L;

  private String id;

  private Integer version;

  private AcademicTermMDTO academicTermDisplayName;

  private LearningOpportunityInstanceMDTO loiId;

  private String losCode;

  private LearningOpportunitySpecificationMDTO losId;

  private LearningAgreementComponentStatus status;

  private String title;

  private LaComponentReasonCode reasonCode;

  private String reasonText;

  private LaComponentType laComponentType;

  private String recognitionConditions;

  private String shortDescription;

  private List<CreditMDTO> credits;
}
