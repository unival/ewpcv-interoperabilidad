package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.persistence.criteria.*;

import es.minsait.ewpcv.mapper.MobilityMDTOMapper;
import es.minsait.ewpcv.mapper.TranscriptOfRecordMDTOMapper;
import es.minsait.ewpcv.model.omobility.Mobility;
import es.minsait.ewpcv.repository.dao.ITorDAO;
import es.minsait.ewpcv.repository.impl.springdata.TorRepository;
import es.minsait.ewpcv.repository.model.course.TranscriptOfRecordMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Tor dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class TorDAO extends QueryDslRepositorySupport implements ITorDAO {

  private TranscriptOfRecordMDTOMapper torMapper;
  private MobilityMDTOMapper mobilityMapper;
  private TorRepository torRepository;

  @Autowired
  public TorDAO(final TranscriptOfRecordMDTOMapper torMapper, final TorRepository torRepository,
      final MobilityMDTOMapper mobilityMapper) {
    super(TorDAO.class);
    this.torMapper = torMapper;
    this.torRepository = torRepository;
    this.mobilityMapper = mobilityMapper;
  }

  /**
   * Devuelve la lista de ids de las mobilidades que tienen ToRs asociados
   * 
   * @return lista de ids
   */
  @Override
  public List<String> obtenerMobilitiesWithTorIds(final String receivingHei, final List<String> sendingHeisIds,
      final String modifiedSince, final List<String> receivingHeiIdToFilter) {

    final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    final CriteriaQuery<String> cq = cb.createQuery(String.class);
    final Root<Mobility> root = cq.from(Mobility.class);
    cq.distinct(true).select(root.get("id"));
    final Join<Object, Object> joinLearningAgreement = root.join("learningAgreement");

    final List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.equal(cb.lower(root.get("receivingInstitutionId")), (receivingHei).toLowerCase()));

    if (Objects.nonNull(modifiedSince)) {
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      final ZonedDateTime zdt = ZonedDateTime.parse(modifiedSince, dtf);
      final Date dateTimeFormated = Date.from(zdt.toInstant());

      final Path<Date> dateModifiedSincePath = root.get("modifyDate");
      predicates.add(cb.greaterThanOrEqualTo(dateModifiedSincePath, dateTimeFormated));
    }

    if (Objects.nonNull(sendingHeisIds)) {
      if (!sendingHeisIds.isEmpty()) {
        final Stack<Predicate> predicatesReceivingHeiId = new Stack<>();
        for (final String sendingHeiId : sendingHeisIds) {
          final Predicate iiaCodePredicate =
              cb.equal(cb.lower(root.get("sendingInstitutionId")), sendingHeiId.toLowerCase());
          predicatesReceivingHeiId.push(iiaCodePredicate);
        }
        Predicate or = cb.or(predicatesReceivingHeiId.pop());
        while (!predicatesReceivingHeiId.empty()) {
          or = cb.or(or, predicatesReceivingHeiId.pop());
        }
        predicates.add(or);
      }
    }

    if (Objects.nonNull(receivingHeiIdToFilter)) {
      if (!receivingHeiIdToFilter.isEmpty()) {
        final Stack<Predicate> predicatesReceivingHeiId = new Stack<>();
        for (final String filterHeiId : receivingHeiIdToFilter) {
          final Predicate iiaCodePredicate =
              cb.equal(cb.lower(root.get("sendingInstitutionId")), filterHeiId.toLowerCase());
          predicatesReceivingHeiId.push(iiaCodePredicate);
        }
        Predicate or = cb.or(predicatesReceivingHeiId.pop());
        while (!predicatesReceivingHeiId.empty()) {
          or = cb.or(or, predicatesReceivingHeiId.pop());
        }
        predicates.add(or);
      }
    }

    cq.where(predicates.toArray(new Predicate[predicates.size()]));

    return getEntityManager().createQuery(cq).getResultList();
  }

  @Override
  public List<MobilityMDTO> findTorByOmobilityIdAndReceivingInstitutionId(final String receivingHei,
      final List<String> omobilityId, final List<String> heisCoveredByClient) {
    heisCoveredByClient.replaceAll(String::toLowerCase);

    return mobilityMapper.entitiesToDtos(
        torRepository.findTorByOmobilityIdAndReceivingInstitutionId(receivingHei, omobilityId, heisCoveredByClient));
  }

  @Override
  public MobilityMDTO obtenerMobilityWithTor(final String omobilityId) {
    return mobilityMapper.entityToDto(torRepository.findMobilityWithTor(omobilityId));
  }

  @Override
  public List<String> findLocalMobilitiesWithTorId(final String receivingHei, final String sendingHei) {
    return torRepository.findLocalMobilitiesWithTorId(receivingHei, sendingHei);
  }

  @Override
  public List<TranscriptOfRecordMDTO> findTorsByMobilityId(final String mobilityId) {
    return torMapper.entitiesToDtos(torRepository.findTorsByMobilityId(mobilityId));
  }

  @Override
  public void eliminarTor(final TranscriptOfRecordMDTO tor) {
    torRepository.delete(torMapper.dtoToEntity(tor));
  }
}
