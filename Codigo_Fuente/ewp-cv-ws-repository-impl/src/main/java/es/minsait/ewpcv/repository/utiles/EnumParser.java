package es.minsait.ewpcv.repository.utiles;

import es.minsait.ewpcv.repository.model.notificacion.CnrType;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;

/**
 * The type Enum parser.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EnumParser {

  public static es.minsait.ewpcv.model.notificacion.NotificationTypes modelToEntity(
      NotificationTypes notificationType) {
    es.minsait.ewpcv.model.notificacion.NotificationTypes entityNotificationType;
    switch (notificationType) {
      case IIA:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.IIA;
        break;
      case OMOBILITY:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.OMOBILITY;
        break;
      case IMOBILITY:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.IMOBILITY;
        break;
      case IMOBILITY_TOR:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.IMOBILITY_TOR;
        break;
      case IIA_APPROVAL:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.IIA_APPROVAL;
        break;
      case LA:
        entityNotificationType = es.minsait.ewpcv.model.notificacion.NotificationTypes.LA;
        break;
      default:
        throw new IllegalArgumentException("Unexpected enum constant: " + notificationType);
    }
    return entityNotificationType;
  }

  public static NotificationTypes entityToModel(
      es.minsait.ewpcv.model.notificacion.NotificationTypes notificationType) {
    es.minsait.ewpcv.repository.model.notificacion.NotificationTypes modelNotificationType;
    switch (notificationType) {
      case IIA:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.IIA;
        break;
      case OMOBILITY:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.OMOBILITY;
        break;
      case IMOBILITY:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.IMOBILITY;
        break;
      case IMOBILITY_TOR:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.IMOBILITY_TOR;
        break;
      case IIA_APPROVAL:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.IIA_APPROVAL;
        break;
      case LA:
        modelNotificationType = es.minsait.ewpcv.repository.model.notificacion.NotificationTypes.LA;
        break;
      default:
        throw new IllegalArgumentException("Unexpected enum constant: " + notificationType);
    }
    return modelNotificationType;
  }

  public static es.minsait.ewpcv.model.notificacion.CnrType modelToEntity(CnrType cnrType) {
    es.minsait.ewpcv.model.notificacion.CnrType entityCnrType;
    switch (cnrType) {
      case REFRESH:
        entityCnrType = es.minsait.ewpcv.model.notificacion.CnrType.REFRESH;
        break;
      case NOTIFY:
        entityCnrType = es.minsait.ewpcv.model.notificacion.CnrType.NOTIFY;
        break;
      default:
        throw new IllegalArgumentException("Unexpected enum constant: " + cnrType);
    }
    return entityCnrType;
  }

  public static CnrType modelToEntity(es.minsait.ewpcv.model.notificacion.CnrType cnrType) {
    es.minsait.ewpcv.repository.model.notificacion.CnrType modelCnrType;
    switch (cnrType) {
      case REFRESH:
        modelCnrType = es.minsait.ewpcv.repository.model.notificacion.CnrType.REFRESH;
        break;
      case NOTIFY:
        modelCnrType = es.minsait.ewpcv.repository.model.notificacion.CnrType.NOTIFY;
        break;
      default:
        throw new IllegalArgumentException("Unexpected enum constant: " + cnrType);
    }
    return modelCnrType;
  }
}
