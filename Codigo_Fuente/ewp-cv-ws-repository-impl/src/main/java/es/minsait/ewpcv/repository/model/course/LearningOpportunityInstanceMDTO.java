
package es.minsait.ewpcv.repository.model.course;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.model.course.LoiStatus;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Learning opportunity instance mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LearningOpportunityInstanceMDTO extends BaseMDTO {

  private static final long serialVersionUID = -5836434697967495706L;

  String id;

  private Integer version;

  private GradingSchemeMDTO gradingScheme;

  private ResultDistributionMDTO resultDistribution;

  private String languageOfInstruction;

  private BigDecimal engagementHours;

  private List<LevelMDTO> levels;

  private LoiGroupingMDTO loiGrouping;

  private Date startDate;

  private Date endDate;

  private Long percentageLower;

  private Long percentageEqual;

  private Long percentageHigher;

  private String resultLabel;

  private LoiStatus status;

  private byte[] extension;

  private List<AttachmentMDTO> attachments;

  private DiplomaMDTO diploma;

  private LearningOpportunitySpecificationMDTO learningOpportunitySpecification;

}
