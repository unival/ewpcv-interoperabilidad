package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.LearningOpportunitySpecification;
import es.minsait.ewpcv.repository.model.course.LearningOpportunitySpecificationMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Learning opportunity specification mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface LearningOpportunitySpecificationMDTOMapper
    extends IModelDTOMapperCircular<LearningOpportunitySpecification, LearningOpportunitySpecificationMDTO> {
}
