package es.minsait.ewpcv.repository.model.statistics;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.persistence.Column;
import java.util.Date;

/**
 * The type StatisticsIiaMDTO mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class StatisticsIiaMDTO extends BaseMDTO {

    String Id;
    private Integer version;
    private Integer fetchable;
    private Integer localUnappr_partnerAppr;
    private Integer localAppr_partnerUnappr;
    private Integer bothApproved;
    private Date dumpDate;

}
