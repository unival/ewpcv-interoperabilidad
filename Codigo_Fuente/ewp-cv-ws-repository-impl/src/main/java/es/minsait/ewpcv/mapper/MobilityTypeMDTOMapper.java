package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.repository.model.iia.MobilityTypeMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Mobility type mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface MobilityTypeMDTOMapper extends IModelDTOMapperCircular<MobilityType, MobilityTypeMDTO> {
}
