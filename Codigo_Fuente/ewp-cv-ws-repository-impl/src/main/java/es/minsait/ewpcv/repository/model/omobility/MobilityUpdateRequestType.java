package es.minsait.ewpcv.repository.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Mobility update request type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum MobilityUpdateRequestType {
  APPROVE_PROPOSAL_V1, COMMENT_PROPOSAL_V1;

  public static String[] names() {
    MobilityUpdateRequestType[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }
}
