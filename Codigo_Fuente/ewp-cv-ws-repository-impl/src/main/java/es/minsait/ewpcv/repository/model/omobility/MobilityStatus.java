
package es.minsait.ewpcv.repository.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Mobility status.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum MobilityStatus {
  CANCELLED("cancelled"), LIVE("live"), NOMINATION("nomination"), RECOGNIZED("recognized"), REJECTED("rejected");

  private final String value;

  MobilityStatus(String v) {
    value = v;
  }

  public static String[] names() {
    MobilityStatus[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public String value() {
    return value;
  }
}
