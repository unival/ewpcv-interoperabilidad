package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.FileApiMDTOMapper;
import es.minsait.ewpcv.repository.dao.IFileApiDAO;
import es.minsait.ewpcv.repository.impl.springdata.FileApiRepository;
import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The type StatisticsOla dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Repository
@Slf4j
public class FileApiDAO implements IFileApiDAO {

  @Autowired
  private FileApiRepository fileApiRepository;
  @Autowired
  private FileApiMDTOMapper fileMdtoMapper;

  @Override
  public FileMDTO getFileByIdAndSchac(String id, String schac) {
    return fileMdtoMapper.entityToDto(fileApiRepository.findByFileIdAndSchac(id, schac));
  }

  @Override
  public void insertFile(FileMDTO file) {
    fileApiRepository.save(fileMdtoMapper.dtoToEntity(file));
  }

  @Override
  public boolean existsFile(String fileId, String heiId) {
    return fileApiRepository.findByFileIdAndSchac(fileId, heiId) != null;
  }
}
