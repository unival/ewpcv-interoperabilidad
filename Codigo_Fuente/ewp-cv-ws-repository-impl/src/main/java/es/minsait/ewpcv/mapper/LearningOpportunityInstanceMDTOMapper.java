package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.LearningOpportunityInstance;
import es.minsait.ewpcv.repository.model.course.LearningOpportunityInstanceMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Learning opportunity instance mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {LoiGroupingMDTOMapper.class})
public interface LearningOpportunityInstanceMDTOMapper
    extends IModelDTOMapperCircular<LearningOpportunityInstance, LearningOpportunityInstanceMDTO> {
}
