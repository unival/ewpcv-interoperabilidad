package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.PhoneNumber;
import es.minsait.ewpcv.repository.model.organization.PhoneNumberMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Phone number mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface PhoneNumberMDTOMapper extends IModelDTOMapperCircular<PhoneNumber, PhoneNumberMDTO> {
}
