
package es.minsait.ewpcv.repository.model.organization;

import java.util.Date;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Person mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class PersonMDTO extends BaseMDTO {

  private static final long serialVersionUID = -519412941152310271L;

  String id;

  private Integer version;

  private Date birthDate;

  private String countryCode;

  private String firstNames;

  private Gender gender;

  private String lastName;

}
