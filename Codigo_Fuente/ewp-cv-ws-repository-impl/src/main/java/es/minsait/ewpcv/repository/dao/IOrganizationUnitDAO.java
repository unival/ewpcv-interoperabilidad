package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;

/**
 * The interface Organization unit dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IOrganizationUnitDAO {
  List<OrganizationUnitMDTO> findAllByHeiIdAndOunitIdExpuestas(String heiId, List<String> ounitsIds);

  List<OrganizationUnitMDTO> findAllByHeiId(String heiId);

  List<OrganizationUnitMDTO> findAllByHeiIdAndOunitCodeExpuestas(String heiId, List<String> ounitCodes);

  OrganizationUnitMDTO saveOrUpdate(OrganizationUnitMDTO mdto);

  boolean existsOunit(String ounitId, String institutionSchac);

  OrganizationUnitMDTO findByIdAndIntitutionSchac(String ounitId, String institutionSchac);

  OrganizationUnitMDTO findByInternalId(String internalId);
}
