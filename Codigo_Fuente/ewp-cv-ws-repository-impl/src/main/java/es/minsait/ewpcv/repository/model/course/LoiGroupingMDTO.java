package es.minsait.ewpcv.repository.model.course;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Loi grouping mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LoiGroupingMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;

  private LearningOpportunityInstanceMDTO loi;
  private GroupMDTO group;
  private GroupTypeMDTO groupType;
}
