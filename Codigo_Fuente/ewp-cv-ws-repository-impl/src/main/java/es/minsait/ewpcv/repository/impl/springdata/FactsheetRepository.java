package es.minsait.ewpcv.repository.impl.springdata;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import es.minsait.ewpcv.model.organization.FactSheet;

/**
 * The interface Factsheet repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface FactsheetRepository extends JpaRepository<FactSheet, String> {

  @Query("SELECT i.factSheet FROM Institution i WHERE LOWER(i.institutionId) = LOWER(:heiId)")
  FactSheet obtenerFactsheetByHeiId(@Param("heiId") String heiId);
}
