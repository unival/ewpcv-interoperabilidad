package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.ContactMDTO;

/**
 * The interface Contact dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IContactDAO {

  List<ContactMDTO> findInstitutionsContacts(List<String> heisIds);

  List<ContactMDTO> findOrganizationUnitContactsByOunitId(List<String> ounitIds, String heiId);

  ContactMDTO saveOrUpdate(ContactMDTO contact);

  void delete(ContactMDTO contact);
}
