package es.minsait.ewpcv.repository.dao;


import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;

/**
 * The interface FileApi dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IFileApiDAO {

    FileMDTO getFileByIdAndSchac(String id, String schac);

    void insertFile(FileMDTO file);

    boolean existsFile(String fileId, String heiId);
}
