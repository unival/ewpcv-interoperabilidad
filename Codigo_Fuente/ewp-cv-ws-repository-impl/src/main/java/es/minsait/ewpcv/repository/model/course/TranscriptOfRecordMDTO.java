package es.minsait.ewpcv.repository.model.course;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Transcript of record mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class TranscriptOfRecordMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;

  String id;
  private Integer version;
  private Date generatedDate;
  private byte[] extension;
  private List<ReportMDTO> report;
  private List<AttachmentMDTO> attachments;
  private List<IscedTableMDTO> iscedTable;
}
