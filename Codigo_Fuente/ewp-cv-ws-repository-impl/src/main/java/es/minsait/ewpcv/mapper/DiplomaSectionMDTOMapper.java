package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.DiplomaSection;
import es.minsait.ewpcv.repository.model.course.DiplomaSectionMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Diploma section mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface DiplomaSectionMDTOMapper extends IModelDTOMapperCircular<DiplomaSection, DiplomaSectionMDTO> {
}
