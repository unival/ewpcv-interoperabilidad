package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.config.AuditConfig;
import es.minsait.ewpcv.repository.model.config.AuditConfigMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Audit config mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface AuditConfigMDTOMapper extends IModelDTOMapperCircular<AuditConfig, AuditConfigMDTO> {
}
