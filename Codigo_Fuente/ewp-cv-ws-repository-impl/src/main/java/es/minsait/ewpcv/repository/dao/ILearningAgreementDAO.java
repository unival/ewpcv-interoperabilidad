package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.course.LearningOpportunityInstanceMDTO;
import es.minsait.ewpcv.repository.model.course.LearningOpportunitySpecificationMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;

/**
 * The interface Learning agreement dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface ILearningAgreementDAO {

  List<LearningAgreementMDTO> findAllLearningAgreementRevisionsById(String id);

  LearningAgreementMDTO findLearningAgreementByChangesId(String mobilityId, String changesId);

  LearningAgreementMDTO findLearningAgreementByIdAndRevision(String id, Integer revision);

  Boolean hasFirstVersion(String id);

  void saveLearningAgreement(LearningAgreementMDTO learningAgreementMDTO);

  Boolean existeLos(String losId);

  LearningOpportunitySpecificationMDTO findLosByIdOrLosCode(String losId, String losCode, String institutionId);

  LearningOpportunityInstanceMDTO findLoiById(String loiId);

  Integer obtenerLoiVersion(String loiId);

  Integer obtenerLosVersion(String value);

  String findLosByLosCodeAndInstitution(String losCode, String heiId);
}
