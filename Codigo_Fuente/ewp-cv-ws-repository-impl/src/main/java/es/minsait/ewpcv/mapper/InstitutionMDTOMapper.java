package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.Institution;
import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Institution mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(
    uses = {LanguageItemMDTOMapper.class, FactSheetMDTOMapper.class, InformationItemMDTOMapper.class,
        OrganizationUnitMDTOMapper.class, RequirementsInfoMDTOMapper.class, ContactDetailsMDTOMapper.class},
    componentModel = "spring")
public interface InstitutionMDTOMapper extends IModelDTOMapperCircular<Institution, InstitutionMDTO> {
}
