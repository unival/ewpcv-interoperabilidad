package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.omobility.OmobilityStats;
import es.minsait.ewpcv.model.omobility.StatisticsOla;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * The interface OmobilityStatsRepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface OmobilityStatsRepository extends JpaRepository<OmobilityStats, String> {

    @Query("Select statOmo FROM OmobilityStats statOmo WHERE statOmo.academicYearStart >='2021'")
    List<OmobilityStats> findGreaterThan2021();

}
