package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.iia.DeletedElements;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeletedElementsRepository extends JpaRepository<DeletedElements, Integer> {
}
