
package es.minsait.ewpcv.repository.model.omobility;

import java.util.Date;

import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Mobility update request mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityUpdateRequestMDTO extends BaseMDTO {

  private static final long serialVersionUID = 481865848786068620L;

  String id;

  private Integer version;

  private MobilityUpdateRequestType type;

  private String sendingHeiId;

  private byte[] updateInformation;

  private Date updateRequestDate;

  private boolean isProcessing;

  private UpdateRequestMobilityTypeEnum mobilityType;

  private Integer retries;

  private Date processingDate;

  private boolean shouldRetry;

  private String lastError;
}
