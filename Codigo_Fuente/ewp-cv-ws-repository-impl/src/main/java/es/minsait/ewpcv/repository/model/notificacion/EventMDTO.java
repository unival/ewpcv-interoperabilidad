package es.minsait.ewpcv.repository.model.notificacion;

import java.util.Date;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;


/**
 * The type Event mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class EventMDTO extends BaseMDTO {

  private static final long serialVersionUID = 8130079757486819172L;

  String id;
  private Date eventDate;
  private String triggeringHei;
  private String ownerHei;
  private String eventType;
  private String changedElementIds;
  private NotificationTypes elementType;
  private String observations;
  private String status;
  private String requestParams;
  private byte[] message;
  private String xRequestId;
  private byte[] requestBody;

}
