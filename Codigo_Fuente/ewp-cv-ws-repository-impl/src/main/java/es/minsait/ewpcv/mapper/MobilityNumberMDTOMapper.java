package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.iia.MobilityNumber;
import es.minsait.ewpcv.repository.model.iia.MobilityNumberMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Mobility number mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface MobilityNumberMDTOMapper extends IModelDTOMapperCircular<MobilityNumber, MobilityNumberMDTO> {
}
