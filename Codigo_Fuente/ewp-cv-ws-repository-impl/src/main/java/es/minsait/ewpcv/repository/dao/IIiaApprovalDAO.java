package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.iia.IiaMDTO;

/**
 * The interface Iia approval dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IIiaApprovalDAO {

  List<IiaMDTO> findApprovedIiasByRemoteIiaIdAndPartners(String approvingHeiId, String ownerHeiId, List<String> iiasId);

}
