
package es.minsait.ewpcv.repository.model.course;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Learning opportunity specification mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LearningOpportunitySpecificationMDTO extends BaseMDTO {

  private static final long serialVersionUID = -668908213887360185L;

  String id;

  private Integer version;

  private String institutionId;

  private String organizationUnitId;

  private String losCode;

  private LearningOpportunitySpecificationType type;

  private List<LanguageItemMDTO> name;

  private List<LanguageItemMDTO> url;

  private byte eqfLevel;

  private String iscedf;

  private String subjectArea;

  private List<LanguageItemMDTO> description;

  private List<LearningOpportunitySpecificationMDTO> learningOpportunitySpecifications;

  private boolean topLevelParent;

  private byte[] extension;

}
