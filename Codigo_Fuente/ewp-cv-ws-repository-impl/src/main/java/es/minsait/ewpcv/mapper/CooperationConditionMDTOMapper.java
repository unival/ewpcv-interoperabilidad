package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.iia.CooperationCondition;
import es.minsait.ewpcv.repository.model.iia.CooperationConditionMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Cooperation condition mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface CooperationConditionMDTOMapper
    extends IModelDTOMapperCircular<CooperationCondition, CooperationConditionMDTO> {
}
