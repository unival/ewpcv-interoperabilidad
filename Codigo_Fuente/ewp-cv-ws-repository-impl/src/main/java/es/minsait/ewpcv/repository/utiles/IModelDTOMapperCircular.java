package es.minsait.ewpcv.repository.utiles;

import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Mapper base para transformaciones de DTO/Entity
 *
 * @param <E> the type parameter
 * @param <D> the type parameter
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com) *
 */
public interface IModelDTOMapperCircular<E extends Serializable, D extends BaseMDTO> {
  static final Logger logMapeos =
      LoggerFactory.getLogger(es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular.class);

  int LLAMADA_ACTUAL = 2;
  int LLAMADA_PADRE = 3;
  int LLAMADA_PADRE_AUTOGENERADO = 4;
  int LLAMADA_ABUELO_AUTOGENERADO = 5;

  @BeforeMapping
  default void inicializarMapaInstancias() {
    StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
    if (esInicioMapeo(stackTraceElements)) {
      logMapeos.trace("Reseteamos instancias ya visitadas");
      CycleAvoidingMappingContext.resetKnownInstances();
    } else {
      logMapeos.trace("NO reseteamos instancias ya visitadas");
      CycleAvoidingMappingContext.getKnownInstances();
    }
  }

  default boolean esInicioMapeo(StackTraceElement[] stackTraceElements) {
    // si no proviene de un mapeo, o es un metodo autogenerado que no proviene
    // de un mapeo entonces es inicio de ciclo
    // en la segunda condicion debemos mirar los indices 4 y 5 porque las
    // colecciones generan un metodo de mapeo que no contiene la cadena
    return !provieneMapeoPrevio(stackTraceElements, LLAMADA_PADRE) || (esMapeoAutogenerado(stackTraceElements)
        && !(provieneMapeoPrevio(stackTraceElements, LLAMADA_PADRE_AUTOGENERADO)
            || provieneMapeoPrevio(stackTraceElements, LLAMADA_ABUELO_AUTOGENERADO)));
  }

  /**
   * Comprobamos que 4 elemento de la pila de llamadas sea un metodo de mapeo para determinar si el mapeo actual viene
   * de un mapeo previo La pila de llamadas será getStackTrace -> inicializarMapaInstancias -> mapeo actual -> metodo
   * invocante
   * 
   * @param stackTraceElements
   * @return
   */
  default boolean provieneMapeoPrevio(StackTraceElement[] stackTraceElements, int indice) {
    return stackTraceElements.length >= (indice - 1)
        && (stackTraceElements[indice].getMethodName().toUpperCase().contains("ENTITYTODTO")
            || stackTraceElements[indice].getMethodName().toUpperCase().contains("DTOTOENTITY"));
  }

  /**
   * Hemos detectado que en los metodos autogenerados el stackTrace apunta a la anotacion @generaded del encabezado de
   * la clase Verifica en base a la traza que no haya dos metodos de diferentes lineas seguidos
   *
   * @param stackTraceElements
   * @return
   */
  default boolean esMapeoAutogenerado(StackTraceElement[] stackTraceElements) {
    boolean numElementosAdecuado = stackTraceElements.length >= LLAMADA_ACTUAL;
    boolean mismaClase =
        stackTraceElements[LLAMADA_ACTUAL].getClassName().equals(stackTraceElements[LLAMADA_PADRE].getClassName());
    boolean mismoMetodo =
        stackTraceElements[LLAMADA_ACTUAL].getMethodName().equals(stackTraceElements[LLAMADA_PADRE].getMethodName());
    boolean mismaLinea =
        stackTraceElements[LLAMADA_ACTUAL].getLineNumber() == stackTraceElements[LLAMADA_PADRE].getLineNumber();
    return numElementosAdecuado && mismaClase && mismoMetodo && !mismaLinea;
  }

  @SuppressWarnings("unchecked")
  @BeforeMapping
  default <T> T getMappedInstance(Object source, @TargetType Class<T> targetType) {

    T instance = (T) CycleAvoidingMappingContext.getKnownInstances().get(source);

    if (logMapeos.isTraceEnabled()) {
      logMapeos.trace("Me preguntan por objeto " + source + " de tipo " + targetType.getClass().getCanonicalName()
          + " y hemos encontrado " + instance, 1000, 1);
    }


    return instance;
  }

  @BeforeMapping
  default void storeMappedInstance(Object source, @MappingTarget Object target) {
    CycleAvoidingMappingContext.getKnownInstances().put(source, target);
  }

  D entityToDto(E entity);

  E dtoToEntity(D dto);

  default List<D> entitiesToDtos(List<E> entities) {
    return Optional.ofNullable(entities).map(Collection::stream).orElse(Stream.empty()).map(e -> entityToDto(e))
        .collect(Collectors.toList());
  }

  List<E> dtosToentities(List<D> dtos);

  void updateEntityFromDto(D sourceDTO, @MappingTarget E entidadH);

  void updateDtoFromEntity(E source, @MappingTarget D entidadDTO);

  default Optional<D> optionalEntityToDto(Optional<E> optEntity) {
    if (optEntity.isPresent()) {
      return Optional.ofNullable(entityToDto(optEntity.get()));
    } else {
      return Optional.empty();
    }
  }

  default Page<D> entityPageToDtoPage(Pageable page, Page<E> entityPage) {
    List<D> dtos = entitiesToDtos(entityPage.getContent());
    return new PageImpl<>(dtos, page, entityPage.getTotalElements());
  }

  default List<D> entityPageToDtoList(Page<E> entityPage) {
    return entitiesToDtos(entityPage.getContent());
  }

}
