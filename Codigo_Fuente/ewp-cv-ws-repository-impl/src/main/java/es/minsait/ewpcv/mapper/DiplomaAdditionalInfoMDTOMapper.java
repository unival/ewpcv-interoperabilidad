package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.DiplomaAdditionalInfo;
import es.minsait.ewpcv.repository.model.course.DiplomaAdditionalInfoMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Diploma additional info mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface DiplomaAdditionalInfoMDTOMapper
    extends IModelDTOMapperCircular<DiplomaAdditionalInfo, DiplomaAdditionalInfoMDTO> {
}
