package es.minsait.ewpcv.repository.model.course;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Diploma mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class DiplomaMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;

  String id;
  private Integer version;
  private Integer diplomaVersion;
  private Date issueDate;
  private byte[] introduction;
  private byte[] signature;
  private List<DiplomaSectionMDTO> section;
}
