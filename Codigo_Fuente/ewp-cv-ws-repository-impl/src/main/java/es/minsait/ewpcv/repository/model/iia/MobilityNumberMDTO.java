
package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.model.iia.MobilityNumberVariants;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Mobility number mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityNumberMDTO extends BaseMDTO {

  String id;

  private Integer version;

  private MobilityNumberVariants variant;

  private int numbermobility;

}
