package es.minsait.ewpcv.mapper;


import es.minsait.ewpcv.model.iia.Iia;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface Iia mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {IiaPartnerMDTOMapper.class})
public interface IiaMDTOMapper extends IModelDTOMapperCircular<Iia, IiaMDTO> {

}

