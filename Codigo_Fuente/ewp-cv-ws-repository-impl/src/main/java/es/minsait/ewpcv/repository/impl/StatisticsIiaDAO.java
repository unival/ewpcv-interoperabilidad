package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.IiaStatsMDTOMapperImpl;
import es.minsait.ewpcv.mapper.StatisticsIiaMDTOMapper;
import es.minsait.ewpcv.repository.dao.IStatisticsIiaDAO;
import es.minsait.ewpcv.repository.impl.springdata.IiaStatsRepository;
import es.minsait.ewpcv.repository.impl.springdata.StatisticsIiaRepository;
import es.minsait.ewpcv.repository.model.iia.IiaStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The type Statistics Iia dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@Repository
@Slf4j
public class StatisticsIiaDAO implements IStatisticsIiaDAO {


    @Autowired
    private StatisticsIiaRepository statisticsIiaRepository;

    @Autowired
    private StatisticsIiaMDTOMapper statisticsIiaMDTOMapper;

    @Autowired
    private IiaStatsMDTOMapperImpl iiaStatsMDTOMapperImpl;

    @Autowired
    private IiaStatsRepository iiaStatsRepository;


    @Override
    public void saveStatsIia(StatisticsIiaMDTO statsIia) {

        statisticsIiaRepository.save(statisticsIiaMDTOMapper.dtoToEntity(statsIia));

    }

    @Override
    public List<IiaStatsMDTO> getOmobilityIiaStats() {
        return iiaStatsMDTOMapperImpl.entitiesToDtos(iiaStatsRepository.findAll());
    }

    @Override
    public StatisticsIiaMDTO getStatisticsRest() {
        StatisticsIiaMDTO mdto = statisticsIiaMDTOMapper.entityToDto(statisticsIiaRepository.findMostRecentStats());
        return mdto;

    }





}
