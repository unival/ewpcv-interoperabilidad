package es.minsait.ewpcv.repository.model.course;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Report mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class ReportMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3028009532203741470L;


  String id;
  private Integer version;
  private Date issueDate;
  private List<LearningOpportunityInstanceMDTO> lois;
  private List<AttachmentMDTO> attachments;

}
