
package es.minsait.ewpcv.repository.model.omobility;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.model.omobility.MobilityStatus;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.model.organization.MobilityParticipantMDTO;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Mobility mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityMDTO extends BaseMDTO {

  private static final long serialVersionUID = 3106919255412421957L;

  private String mobilityId; //pk de la tabla

  private String id; //id expuesto a la red EWP

  private Integer version;

  private Date actualArrivalDate;

  private Date actualDepartureDate;

  private String cooperationConditionId;

  private String iiaId;

  private String receivingIiaId;

  private SubjectAreaMDTO iscedCode;

  private MobilityParticipantMDTO mobilityParticipantId;

  private int mobilityRevision;

  private Date plannedArrivalDate;

  private Date plannedDepartureDate;

  private String receivingInstitutionId;

  private String receivingOrganizationUnitId;

  private String sendingInstitutionId;

  private String sendingOrganizationUnitId;

  private List<LearningAgreementMDTO> learningAgreement;

  private MobilityType mobilityType;

  private List<LanguageSkillMDTO> languageSkill;

  private ContactMDTO senderContactPerson;

  private ContactMDTO senderAdmvContactPerson;

  private ContactMDTO receiverContactPerson;

  private ContactMDTO receiverAdmvContactPerson;

  private String remoteMobilityId;

  private String comment;

  private Long eqfLevelNomination;

  private Long eqfLevelDeparture;

  private AcademicTermMDTO academicTerm;

  private MobilityStatus statusOutgoing;

  private MobilityStatus statusIncomming;

  private Date modifyDate;

}
