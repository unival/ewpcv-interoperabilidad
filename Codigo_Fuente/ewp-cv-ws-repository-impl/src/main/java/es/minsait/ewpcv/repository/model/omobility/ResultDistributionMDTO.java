
package es.minsait.ewpcv.repository.model.omobility;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Result distribution mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class ResultDistributionMDTO extends BaseMDTO {

  private static final long serialVersionUID = -7838737137111536546L;

  String id;

  private Integer version;

  private List<ResultDistributionCategoryMDTO> resultDistributionCategory;

  private List<LanguageItemMDTO> description;

}
