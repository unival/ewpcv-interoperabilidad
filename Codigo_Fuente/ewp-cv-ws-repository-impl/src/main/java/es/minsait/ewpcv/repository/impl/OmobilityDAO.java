package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.model.course.LearningOpportunityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.persistence.criteria.*;

import es.minsait.ewpcv.mapper.MobilityMDTOMapper;
import es.minsait.ewpcv.model.course.LearningOpportunitySpecification;
import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.model.omobility.*;
import es.minsait.ewpcv.repository.dao.IOmobilityDAO;
import es.minsait.ewpcv.repository.impl.springdata.*;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Omobility dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class OmobilityDAO extends QueryDslRepositorySupport implements IOmobilityDAO {

  private MobilityMDTOMapper omobilityMDTOMapper;
  private OmobilityRepository omobilityRepository;
  private LanguageSkillRepository languageSkillRepository;
  private MobilityTypeRepository mobilityTypeRepository;
  private LearningOpportunitySpecificationRepository losRepository;
  private LearningOpportunityInstanceRepository loiRepository;

  @Autowired
  public OmobilityDAO(final MobilityMDTOMapper omobilityMDTOMapper, final OmobilityRepository omobilityRepository,
      final LanguageSkillRepository languageSkillRepository, final MobilityTypeRepository mobilityTypeRepository,
      final LearningOpportunitySpecificationRepository losRepository,
      final LearningOpportunityInstanceRepository loiRepository) {
    super(OmobilityDAO.class);
    this.omobilityMDTOMapper = omobilityMDTOMapper;
    this.omobilityRepository = omobilityRepository;
    this.languageSkillRepository = languageSkillRepository;
    this.mobilityTypeRepository = mobilityTypeRepository;
    this.losRepository = losRepository;
    this.loiRepository = loiRepository;
  }

  @Override
  public List<MobilityMDTO> findOMobilitiesByIdAndSendingInstitutionId(final String sendingHeiId,
      final List<String> omobilityIdList, final List<String> receivingHeiIdList) {
    receivingHeiIdList.replaceAll(String::toLowerCase);
    return omobilityMDTOMapper.entitiesToDtos(omobilityRepository
        .findOMobilitiesByIdAndSendingInstitutionId(sendingHeiId, omobilityIdList, receivingHeiIdList));
  }

  @Override
  public int countOMobilitiesByIdAndSendingInstitutionId(final String sendingHeiId, final String oMobilityId) {
    return omobilityRepository.countOMobilitiesByIdAndSendingInstitutionId(sendingHeiId, oMobilityId);
  }

  @Override
  public List<String> getLaIdsByCriteria(final Map<Object, Object> queryParams) {

    final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    final CriteriaQuery<String> cq = cb.createQuery(String.class);
    final Root<Mobility> root = cq.from(Mobility.class);
    cq.distinct(true).select(root.get("id"));

    final Join<Object, Object> joinMobilityParticipant = root.join("mobilityParticipantId");

    final Join<Object, Object> joinMobilityType = root.join("mobilityType", JoinType.LEFT);

    final Join<Object, Object> joinLearningAgreement = root.join("learningAgreement");

    final List<Predicate> predicates =
        crearPredicate(cb, root, joinMobilityParticipant, joinMobilityType, joinLearningAgreement, queryParams);

    cq.where(predicates.toArray(new Predicate[predicates.size()]));

    return getEntityManager().createQuery(cq).getResultList();
  }

  private List<Predicate> crearPredicate(final CriteriaBuilder cb, final Root<Mobility> root,
      final Join<Object, Object> joinMobilityParticipant, final Join<Object, Object> joinMobilityType,
      final Join<Object, Object> joinLearningAgreement, final Map<Object, Object> queryParams) {
    final List<Predicate> predicates = new ArrayList<>();

    predicates.add(
        cb.equal(cb.lower(root.get("sendingInstitutionId")), ((String) queryParams.get("sendingHeiId")).toLowerCase()));

    if (Objects.nonNull(queryParams.get("receivingHeiId"))) {
      final List<String> receivingHeiIdList = (List<String>) queryParams.get("receivingHeiId");
      if (!receivingHeiIdList.isEmpty()) {
        final Stack<Predicate> predicatesReceivingHeiId = new Stack<>();
        for (final String receivingHeiId : receivingHeiIdList) {
          final Predicate iiaCodePredicate =
              cb.equal(cb.lower(root.get("receivingInstitutionId")), receivingHeiId.toLowerCase());
          predicatesReceivingHeiId.push(iiaCodePredicate);
        }
        Predicate or = cb.or(predicatesReceivingHeiId.pop());
        while (!predicatesReceivingHeiId.empty()) {
          or = cb.or(or, predicatesReceivingHeiId.pop());
        }
        predicates.add(or);
      }
    }

    // TODO Revisar
    if (Objects.nonNull(queryParams.get("receivingAcademicYearId"))) {
      final String year = (String) queryParams.get("receivingAcademicYearId");
      final String fromYear =
          isSouthEmisphere(year) ? Integer.valueOf((Integer.parseInt(year.substring(2, 4)) - 1)).toString()
              : year.substring(2, 4);
      final Date fechaDesde = createDateFromString("01/06/".concat(fromYear));
      final Date fechaHasta = createDateFromString("01/09/".concat(year.substring(7, 9)));

      final Path<Date> datePlanedArrivalDatePath = root.get("plannedArrivalDate");
      final Path<Date> datePlanedDepartureDatePath = root.get("plannedDepartureDate");
      final Path<Date> dateActualArrivalDatePath = root.get("actualArrivalDate");
      final Path<Date> dateActualDepartureDatePath = root.get("actualDepartureDate");

      final Predicate planedPredicate1 = cb.greaterThanOrEqualTo(datePlanedArrivalDatePath, fechaDesde);
      final Predicate planedPredicate2 = cb.lessThanOrEqualTo(datePlanedDepartureDatePath, fechaHasta);
      final Predicate planedPredicatePrincipal = cb.and(planedPredicate1, planedPredicate2);

      final Predicate actualPredicate1 = cb.greaterThanOrEqualTo(dateActualArrivalDatePath, fechaDesde);
      final Predicate actualPredicate2 = cb.lessThanOrEqualTo(dateActualDepartureDatePath, fechaHasta);
      final Predicate actualPredicatePrincipal = cb.and(actualPredicate1, actualPredicate2);

      final Predicate or = cb.or(planedPredicatePrincipal, actualPredicatePrincipal);
      predicates.add(or);
    }

    // TODO no sé si globalId de estudiante
    if (Objects.nonNull(queryParams.get("globalId"))) {
      predicates.add(cb.equal(joinMobilityParticipant.get("id"), queryParams.get("globalId")));
    }

    if (Objects.nonNull(queryParams.get("mobilityType"))) {
      final String mobilityType = (String) queryParams.get("mobilityType");
      predicates.add(cb.equal(cb.lower(joinMobilityType.get("mobilityCategory")), mobilityType.toLowerCase()));
    }

    if (Objects.nonNull(queryParams.get("modifiedSince"))) {
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      final ZonedDateTime zdt = ZonedDateTime.parse((String) queryParams.get("modifiedSince"), dtf);
      final Date dateTimeFormated = Date.from(zdt.toInstant());

      final Path<Date> dateModifiedSincePath = joinLearningAgreement.get("modifiedDate");
      predicates.add(cb.greaterThanOrEqualTo(dateModifiedSincePath, dateTimeFormated));
    }

    return predicates;
  }

  private boolean isSouthEmisphere(final String year) {
    return year.substring(2, 4).equals(year.substring(7, 9));
  }

  private Date createDateFromString(final String yearString) {
    try {
      final StringBuilder sb = new StringBuilder();
      sb.append(yearString).append(" 00:00:00");
      return new SimpleDateFormat("dd/MM/yy HH:mm:ss").parse(sb.toString());
    } catch (final ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public MobilityMDTO save(final MobilityMDTO mobilityMDTO) {
    final Mobility entity = omobilityMDTOMapper.dtoToEntity(mobilityMDTO);
    if (!CollectionUtils.isEmpty(entity.getLanguageSkill())) {
      final List<LanguageSkill> listaSesion = new ArrayList<>();
      entity.getLanguageSkill().stream().forEach(l -> {
        final List<LanguageSkill> lang =
                languageSkillRepository.findByLanguageAndCefrLevel(l.getLanguage(), l.getCefrLevel());
        if (Objects.nonNull(lang) && !lang.isEmpty()) {
          listaSesion.add(lang.get(0));
        } else {
          listaSesion.add(languageSkillRepository.save(l));
        }
      });
      entity.setLanguageSkill(listaSesion);
    }

    if (Objects.nonNull(entity.getMobilityType())) {
      final MobilityType mobilityType = mobilityTypeRepository.findByMobilityCategoryAndMobilityGroup(
          entity.getMobilityType().getMobilityCategory(), entity.getMobilityType().getMobilityGroup());
      if (Objects.nonNull(mobilityType)) {
        entity.setMobilityType(mobilityType);
      }
    }

    guardarLosActualizandoEntidad(entity);

    return omobilityMDTOMapper.entityToDto(omobilityRepository.save(entity));
  }

  private void guardarLosActualizandoEntidad(final Mobility entity) {
    // sacamos los componentes de la mobilidad
    final LearningAgreement ultimaRevision = CollectionUtils.isEmpty(entity.getLearningAgreement()) ? null
        : entity.getLearningAgreement().stream()
            .max(Comparator.comparing(LearningAgreement::getLearningAgreementRevision)).orElse(null);

    if (Objects.nonNull(ultimaRevision)) {
      final List<LaComponent> components = new ArrayList<>();
      if (!CollectionUtils.isEmpty(ultimaRevision.getStudiedLaComponents())) {
        components.addAll(ultimaRevision.getStudiedLaComponents());
      }
      if (!CollectionUtils.isEmpty(ultimaRevision.getRecognizedLaComponents())) {
        components.addAll(ultimaRevision.getRecognizedLaComponents());
      }
      if (!CollectionUtils.isEmpty(ultimaRevision.getBlendedLaComponents())) {
        components.addAll(ultimaRevision.getBlendedLaComponents());
      }
      if (!CollectionUtils.isEmpty(ultimaRevision.getDoctoralLaComponents())) {
        components.addAll(ultimaRevision.getDoctoralLaComponents());
      }
      if (!CollectionUtils.isEmpty(ultimaRevision.getVirtualLaComponents())) {
        components.addAll(ultimaRevision.getVirtualLaComponents());
      }
      for (final LaComponent component : components) {
        if (Objects.nonNull(component.getLosId())) {
          // guardamos el LOS y se lo asignamos al LOI
          final LearningOpportunitySpecification los = losRepository.save(component.getLosId());
          component.setLosId(los);
          if (Objects.nonNull(component.getLoiId())) {
            final LearningOpportunityInstance loi = loiRepository.findByIdIgnoreCase(component.getLoiId().getId());
            if(Objects.nonNull(loi)){
              loi.setLearningOpportunitySpecification(los);
              component.setLoiId(loi);
              //component.getLoiId().setLearningOpportunitySpecification(los);
            }
          }
        }
      }
    }
  }

  @Override
  public MobilityMDTO findLastRevisionByIdAndSendingInstitution(final String id, final String sendingHeiId) {
    return omobilityMDTOMapper.entityToDto(omobilityRepository.findLastRevisionByIdAndSendingInstitutionId(id, sendingHeiId));
  }

  @Override
  public void eliminarMobilityByIdAndSendingHeiId(final String id, final String sendingHeiId) {
    omobilityRepository.deleteByIdAndSendingHeiId(id, sendingHeiId);
  }

  @Override
  public String findLastMobilityRevisionReceiverHeiByIdAndSendingHei(final String id, final String sendingHeiId) {
    return omobilityRepository.findLastMobilityRevisionReceiverHeiByIdAndSendingHei(id, sendingHeiId);
  }

  @Override
  public List<String> findLocalOMobilitiesId(final String sendingHeiId, final String receivingHei) {
    return omobilityRepository.findOMobilitiesIdBySendingHeiAndReceivingHei(sendingHeiId, receivingHei);
  }

  @Override
  public List<String> getIdsByCriteria(final Map<Object, Object> queryParams) {

    final CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    final CriteriaQuery<String> cq = cb.createQuery(String.class);
    final Root<Mobility> root = cq.from(Mobility.class);
    cq.distinct(true).select(root.get("id"));

    final List<Predicate> predicates = crearPredicate(cb, root, queryParams);

    cq.where(predicates.toArray(new Predicate[predicates.size()]));

    return getEntityManager().createQuery(cq).getResultList();
  }

  @Override
  public List<MobilityMDTO> obtenerMobilites(final String sendingHeiId, final List<String> receivingHeiIdList,
      final List<String> omobilityId) {
    omobilityId.replaceAll(String::toLowerCase);
    receivingHeiIdList.replaceAll(String::toLowerCase);
    return omobilityMDTOMapper
        .entitiesToDtos(omobilityRepository.obtenerMobilities(sendingHeiId, receivingHeiIdList, omobilityId));
  }

  @Override
  public List<MobilityMDTO> obtenerMobilitesByReceivingAndHeiRemoteIdAndLastRevision(final String receivingHeiId,
      final List<String> sendingHeiIdList, final List<String> omobilityId) {
    omobilityId.replaceAll(String::toLowerCase);
    sendingHeiIdList.replaceAll(String::toLowerCase);
    return omobilityMDTOMapper.entitiesToDtos(omobilityRepository
        .obtenerMobilitiesByReceivingIdAndRemoteIdAndLastRevision(receivingHeiId, sendingHeiIdList, omobilityId));
  }

  private List<Predicate> crearPredicate(final CriteriaBuilder cb, final Root<Mobility> root,
      final Map<Object, Object> queryParams) {
    final List<Predicate> predicates = new ArrayList<>();

    predicates.add(
        cb.equal(cb.lower(root.get("sendingInstitutionId")), ((String) queryParams.get("sendingHeiId")).toLowerCase()));

    if (Objects.nonNull(queryParams.get("receivingHeiId"))) {
      final Stack<Predicate> predicatesReceivingHeiId = new Stack<>();
      final List<String> receivingHeiIdList = (List<String>) queryParams.get("receivingHeiId");
      for (final String receivingHeiId : receivingHeiIdList) {
        final Predicate iiaCodePredicate =
            cb.equal(cb.lower(root.get("receivingInstitutionId")), receivingHeiId.toLowerCase());
        predicatesReceivingHeiId.push(iiaCodePredicate);
      }
      Predicate or = cb.or(predicatesReceivingHeiId.pop());
      while (!predicatesReceivingHeiId.empty()) {
        or = cb.or(or, predicatesReceivingHeiId.pop());
      }
      predicates.add(or);
    }

    // TODO Revisar
    if (Objects.nonNull(queryParams.get("receivingAcademicYearId"))) {
      final String year = (String) queryParams.get("receivingAcademicYearId");
      final String fromYear =
          isSouthEmisphere(year) ? Integer.valueOf((Integer.parseInt(year.substring(2, 4)) - 1)).toString()
              : year.substring(2, 4);
      final Date fechaDesde = createDateFromString("01/06/".concat(fromYear));
      final Date fechaHasta = createDateFromString("01/09/".concat(year.substring(7, 9)));

      final Path<Date> datePlanedArrivalDatePath = root.get("plannedArrivalDate");
      final Path<Date> datePlanedDepartureDatePath = root.get("plannedDepartureDate");
      final Path<Date> dateActualArrivalDatePath = root.get("actualArrivalDate");
      final Path<Date> dateActualDepartureDatePath = root.get("actualDepartureDate");

      final Predicate planedPredicate1 = cb.greaterThanOrEqualTo(datePlanedArrivalDatePath, fechaDesde);
      final Predicate planedPredicate2 = cb.lessThanOrEqualTo(datePlanedDepartureDatePath, fechaHasta);
      final Predicate planedPredicatePrincipal = cb.and(planedPredicate1, planedPredicate2);

      final Predicate actualPredicate1 = cb.greaterThanOrEqualTo(dateActualArrivalDatePath, fechaDesde);
      final Predicate actualPredicate2 = cb.lessThanOrEqualTo(dateActualDepartureDatePath, fechaHasta);
      final Predicate actualPredicatePrincipal = cb.and(actualPredicate1, actualPredicate2);

      final Predicate or = cb.or(planedPredicatePrincipal, actualPredicatePrincipal);
      predicates.add(or);
    }

    if (Objects.nonNull(queryParams.get("modifiedSince"))) {
      final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
      final ZonedDateTime zdt = ZonedDateTime.parse((String) queryParams.get("modifiedSince"), dtf);
      final Date dateTimeFormated = Date.from(zdt.toInstant());

      final Predicate datePredicate = cb.greaterThanOrEqualTo(root.get("modifyDate"), dateTimeFormated);
      predicates.add(datePredicate);
    }

    return predicates;
  }

  @Override
  public List<String> findImobilitiesARefrescar(final String sendingHei) {
    return omobilityRepository.findImobilitiesARefrescar(sendingHei, MobilityStatus.CANCELLED,
        MobilityStatus.RECOGNIZED, MobilityStatus.VERIFIED);
  }
}
