package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import es.minsait.ewpcv.model.course.LearningOpportunityInstance;

/**
 * The interface Learning opportunity instance repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LearningOpportunityInstanceRepository extends JpaRepository<LearningOpportunityInstance, String> {

  @Query("Select loi FROM LearningOpportunityInstance loi WHERE LOWER(loi.id) = LOWER(:id)")
  LearningOpportunityInstance findByIdIgnoreCase(@Param("id") String id);

  @Query(value = "SELECT loi.version FROM LearningOpportunityInstance loi WHERE loi.id = :losId")
  Integer obtenerLoiVersion(@Param("losId") String loiId);
}
