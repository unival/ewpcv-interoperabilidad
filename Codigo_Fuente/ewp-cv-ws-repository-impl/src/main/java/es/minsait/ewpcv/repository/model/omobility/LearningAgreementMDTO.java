
package es.minsait.ewpcv.repository.model.omobility;


import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.repository.model.course.TranscriptOfRecordMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Learning agreement mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LearningAgreementMDTO extends BaseMDTO {

  private static final long serialVersionUID = 2271878625934960247L;

  private String id;

  private Integer version;

  private int learningAgreementRevision;

  private SignatureMDTO studentSign;

  private SignatureMDTO senderCoordinatorSign;

  private SignatureMDTO reciverCoordinatorSign;

  private Date modifiedDate;

  private byte[] pdf;

  private LearningAgreementStatus status;

  private List<LaComponentMDTO> studiedLaComponents;

  private List<LaComponentMDTO> recognizedLaComponents;

  private List<LaComponentMDTO> virtualLaComponents;

  private List<LaComponentMDTO> blendedLaComponents;

  private List<LaComponentMDTO> doctoralLaComponents;

  private String commentReject;

  private ContactMDTO modifiedStudentContactId;

  private String changesId;

  private TranscriptOfRecordMDTO transcriptOfRecord;

}
