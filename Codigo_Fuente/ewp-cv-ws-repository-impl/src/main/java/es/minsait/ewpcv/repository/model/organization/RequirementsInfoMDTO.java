
package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Requirements info mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class RequirementsInfoMDTO extends BaseMDTO {

  private static final long serialVersionUID = 4502519936753455562L;

  String id;
  private Integer version;
  private String type;
  private String name;
  private String descripcion;
  private ContactDetailsMDTO contactDetails;

}
