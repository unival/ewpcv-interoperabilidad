package es.minsait.ewpcv.repository.dao;

import es.minsait.ewpcv.repository.model.omobility.OmobilityStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;

import java.util.List;

/**
 * The interface StatisticsOla dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IStatisticsOlaDAO {
    void saveStatsOla(StatisticsOlaMDTO statsOla);
    List<OmobilityStatsMDTO> getStatsOfOmobilitiTable();
    List<StatisticsOlaMDTO> getStatisticsRest();
}
