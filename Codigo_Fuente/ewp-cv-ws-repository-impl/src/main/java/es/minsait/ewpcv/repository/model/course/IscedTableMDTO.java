package es.minsait.ewpcv.repository.model.course;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * The type Isced table mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class IscedTableMDTO implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;


  String id;
  private Integer version;
  private String iscedCode;
  private List<GradeFrequencyMDTO> frequencies;

}
