package es.minsait.ewpcv.repository.utiles;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.UUIDGenerator;

import java.io.Serializable;
import java.util.Objects;

import es.minsait.ewpcv.model.omobility.LearningAgreementRelationId;

/**
 * The type Use existing id or generate uuid generator.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class UseExistingIdOrGenerateUuidGenerator extends UUIDGenerator {

  @Override
  public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
    Serializable id = session.getEntityPersister(null, object).getClassMetadata().getIdentifier(object, session);
    if (Objects.isNull(id))
      return super.generate(session, object);
    if (id instanceof String)
      return id;
    if (id instanceof LearningAgreementRelationId)
      return ((LearningAgreementRelationId) id).getId();

    return super.generate(session, object);
  }
}
