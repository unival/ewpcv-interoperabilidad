package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.Contact;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Contact mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(uses = {PersonMDTOMapper.class, LanguageItemMDTOMapper.class, ContactDetailsMDTOMapper.class},
    componentModel = "spring")
public interface ContactMDTOMapper extends IModelDTOMapperCircular<Contact, ContactMDTO> {
}
