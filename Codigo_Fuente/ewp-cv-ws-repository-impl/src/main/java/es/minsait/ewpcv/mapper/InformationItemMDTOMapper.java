package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.InformationItem;
import es.minsait.ewpcv.repository.model.organization.InformationItemMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Information item mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(uses = {ContactDetailsMDTOMapper.class}, componentModel = "spring")
public interface InformationItemMDTOMapper extends IModelDTOMapperCircular<InformationItem, InformationItemMDTO> {
}
