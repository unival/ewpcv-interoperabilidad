package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.LanguageSkill;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Language skill mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface LanguageSkillMDTOMapper extends IModelDTOMapperCircular<LanguageSkill, LanguageSkillMDTO> {
}
