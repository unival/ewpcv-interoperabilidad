package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.model.omobility.MobilityUpdateRequest;
import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;

/**
 * The interface Mobility update request repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface MobilityUpdateRequestRepository extends JpaRepository<MobilityUpdateRequest, String> {

  @Query("SELECT u FROM MobilityUpdateRequest u WHERE u.shouldRetry = true AND u.isProcessing = false AND u.mobilityType = :mobilityType AND u.retries <= :maxRetries ")
  List<MobilityUpdateRequest> findAllNotProcessingByMobilityType(
      @Param("mobilityType") UpdateRequestMobilityTypeEnum mobilityType, @Param("maxRetries") Integer maxRetries,
      Pageable page);

  @Modifying
  @Query("UPDATE MobilityUpdateRequest SET isProcessing = :isProcessing, processingDate = :processingDate"
      + " WHERE id IN (:ids)")
  void setProcessing(@Param(value = "ids") List<String> ids, @Param(value = "isProcessing") boolean isProcessing,
      @Param(value = "processingDate") Date processingDate);

  @Modifying
  @Query("UPDATE MobilityUpdateRequest SET shouldRetry = false, lastError= :lastError " + " WHERE id = :id")
  void updateShouldRetryLastError(@Param(value = "id") String id, @Param(value = "lastError") String lastError);

  @Modifying
  @Query("UPDATE MobilityUpdateRequest SET isProcessing = false, retries = retries +1 " + " WHERE id IN (:ids)")
  void setProcessingFalseIncrementRetries(@Param(value = "ids") List<String> updateRequestList);

  @Query("SELECT req from MobilityUpdateRequest req WHERE req.isProcessing = true")
  List<MobilityUpdateRequest> obtenerRequestProcesandose();
}
