package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.iia.IiaStats;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface OmobilityIiaStatsRepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IiaStatsRepository extends JpaRepository<IiaStats, String> {

}
