package es.minsait.ewpcv.repository.model.course;

import java.util.List;

import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Grading scheme mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class GradingSchemeMDTO extends BaseMDTO {

  private static final long serialVersionUID = 7293846753795473140L;

  String id;

  private Integer version;

  private List<LanguageItemMDTO> label;

  private List<LanguageItemMDTO> description;

}
