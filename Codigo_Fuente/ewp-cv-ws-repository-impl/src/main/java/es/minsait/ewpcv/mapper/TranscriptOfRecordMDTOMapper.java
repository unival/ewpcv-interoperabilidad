package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.TranscriptOfRecord;
import es.minsait.ewpcv.repository.model.course.TranscriptOfRecordMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Transcript of record mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {ReportMDTOMapper.class})
public interface TranscriptOfRecordMDTOMapper
    extends IModelDTOMapperCircular<TranscriptOfRecord, TranscriptOfRecordMDTO> {
}
