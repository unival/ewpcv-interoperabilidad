package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.MobilityUpdateRequest;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Mobility update request mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface MobilityUpdateRequestMDTOMapper
    extends IModelDTOMapperCircular<MobilityUpdateRequest, MobilityUpdateRequestMDTO> {
}
