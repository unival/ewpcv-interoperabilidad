package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.ContactDetails;
import es.minsait.ewpcv.repository.model.organization.ContactDetailsMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Contact details mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface ContactDetailsMDTOMapper extends IModelDTOMapperCircular<ContactDetails, ContactDetailsMDTO> {
}
