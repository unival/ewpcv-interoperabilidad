package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import es.minsait.ewpcv.mapper.ContactMDTOMapper;
import es.minsait.ewpcv.repository.dao.IContactDAO;
import es.minsait.ewpcv.repository.impl.springdata.IContactRepository;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;

/**
 * The type Contact dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class ContactDAO implements IContactDAO {

  @Autowired
  private ContactMDTOMapper contactMDTOMapper;

  @Autowired
  private IContactRepository contactRepository;

  @Override
  public List<ContactMDTO> findInstitutionsContacts(final List<String> heisIds) {
    heisIds.replaceAll(String::toLowerCase);
    return contactMDTOMapper.entitiesToDtos(contactRepository.findInstitutionsContacts(heisIds));
  }

  @Override
  public List<ContactMDTO> findOrganizationUnitContactsByOunitId(final List<String> ounitIds, final String heiId) {
    ounitIds.replaceAll(String::toLowerCase);
    return contactMDTOMapper.entitiesToDtos(contactRepository.findOrganizationUnitContactsByOunitId(ounitIds, heiId));
  }

  @Override
  public ContactMDTO saveOrUpdate(final ContactMDTO contact) {
    return contactMDTOMapper.entityToDto(contactRepository.save(contactMDTOMapper.dtoToEntity(contact)));
  }

  @Override
  public void delete(final ContactMDTO contact) {
    contactRepository.delete(contactMDTOMapper.dtoToEntity(contact));
  }
}
