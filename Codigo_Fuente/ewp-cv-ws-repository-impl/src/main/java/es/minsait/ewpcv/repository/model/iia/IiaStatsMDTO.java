package es.minsait.ewpcv.repository.model.iia;


import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type OmobilityIiaStatsMDTO mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class IiaStatsMDTO extends BaseMDTO {

    private Integer fetchable;
    private Integer localUnappr_partnerAppr;
    private Integer localAppr_partnerUnappr;
    private Integer bothApproved;

}
