package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.Attachment;
import es.minsait.ewpcv.repository.model.course.AttachmentMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Attachment mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface AttachmentMDTOMapper extends IModelDTOMapperCircular<Attachment, AttachmentMDTO> {
}
