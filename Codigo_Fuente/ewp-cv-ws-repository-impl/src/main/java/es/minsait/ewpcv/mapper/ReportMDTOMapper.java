package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.Report;
import es.minsait.ewpcv.repository.model.course.ReportMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Report mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {LearningOpportunityInstanceMDTOMapper.class})
public interface ReportMDTOMapper extends IModelDTOMapperCircular<Report, ReportMDTO> {
}
