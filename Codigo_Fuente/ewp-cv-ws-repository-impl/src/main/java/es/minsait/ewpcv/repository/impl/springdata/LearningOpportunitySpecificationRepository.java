package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import es.minsait.ewpcv.model.course.LearningOpportunitySpecification;

/**
 * The interface Learning opportunity specification repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LearningOpportunitySpecificationRepository
    extends JpaRepository<LearningOpportunitySpecification, String> {

  @Query(
      value = "SELECT los FROM LearningOpportunitySpecification los WHERE los.losCode = :losCode AND LOWER(los.institutionId) = LOWER(:institutionId)")
  LearningOpportunitySpecification findLosByLosCode(@Param(value = "losCode") String losCode,
      @Param(value = "institutionId") String institutionId);

  @Query("Select los FROM LearningOpportunitySpecification los  WHERE LOWER(los.id) = LOWER(:id)")
  LearningOpportunitySpecification findByIdIgnoreCase(@Param("id") String id);

  @Query(value = "SELECT los.version FROM LearningOpportunitySpecification los WHERE los.id = :losId")
  Integer obtenerLosVersion(@Param("losId") String losId);

  @Query("Select los.id FROM LearningOpportunitySpecification los WHERE los.losCode = :losCode AND LOWER(los.institutionId) = LOWER(:heiId)")
  String findLosIdByLosCodeAndInstitution(@Param("losCode") String losCode, @Param("heiId") String heiId);
}
