
package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Mobility type mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityTypeMDTO extends BaseMDTO {

  private static final long serialVersionUID = -2969183507992967894L;

  String id;

  private Integer version;

  private String mobilityGroup;

  private String mobilityCategory;

}
