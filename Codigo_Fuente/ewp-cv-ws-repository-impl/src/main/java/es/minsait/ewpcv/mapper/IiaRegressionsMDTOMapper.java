package es.minsait.ewpcv.mapper;

import es.minsait.ewpcv.model.iia.IiaRegressions;
import es.minsait.ewpcv.repository.model.iia.IiaRegressionsMDTO;
import org.mapstruct.Mapper;

import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface IiaRegressions mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface IiaRegressionsMDTOMapper extends IModelDTOMapperCircular<IiaRegressions, IiaRegressionsMDTO> {
}
