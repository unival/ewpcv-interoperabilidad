package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import java.util.Date;
import lombok.Data;

@Data
public class IiaRegressionsMDTO extends BaseMDTO {

    private static final long serialVersionUID = -4977241475565278583L;

    private String id;

    private String iiaId;

    private String iiaHash;

    private String ownerSchac;

    private Date regressionDate;
}
