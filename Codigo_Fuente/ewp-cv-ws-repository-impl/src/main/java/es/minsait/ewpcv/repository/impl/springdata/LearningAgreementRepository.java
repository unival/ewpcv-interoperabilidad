package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.omobility.LearningAgreement;

/**
 * The interface Learning agreement repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface LearningAgreementRepository extends JpaRepository<LearningAgreement, String> {

  @Query("SELECT la FROM LearningAgreement la WHERE la.id = :id AND la.learningAgreementRevision = :revision")
  LearningAgreement findLearningAgreementByIdAndRevision(@Param("id") String id, @Param("revision") Integer revision);

  @Query("SELECT la FROM LearningAgreement la WHERE la.id = :id")
  List<LearningAgreement> findAllLearningAgreementRevisionsById(@Param("id") String id);

  @Query("SELECT la FROM Mobility m JOIN m.learningAgreement la " + " WHERE m.id = :mobilityId"
      + " AND la.changesId = :changesId")
  LearningAgreement findLearningAgreementByChangesId(@Param("mobilityId") String mobilityId,
      @Param("changesId") String changesId);

  @Query("SELECT COUNT(1) FROM LearningAgreement la WHERE la.id = :id AND la.status = 1")
  Integer countFirstVersion(@Param("id") String id);

}
