
package es.minsait.ewpcv.repository.model.organization;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Fact sheet mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class FactSheetMDTO extends BaseMDTO {

  private static final long serialVersionUID = 5532576881365871572L;

  String id;

  private Integer version;

  private ContactDetailsMDTO contactDetails;

  private Integer decisionWeeksLimit;

  private Integer torWeeksLimit;

  private Date nominationsAutumnTerm;

  private Date nominationsSpringTerm;

  private Date applicationAutumnTerm;

  private Date applicationSpringTerm;

}
