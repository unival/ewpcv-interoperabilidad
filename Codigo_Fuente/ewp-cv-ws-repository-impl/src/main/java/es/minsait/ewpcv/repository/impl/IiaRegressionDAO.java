package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.IiaRegressionsMDTOMapper;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import es.minsait.ewpcv.repository.impl.springdata.IiaRegressionsRepository;
import es.minsait.ewpcv.repository.model.iia.IiaRegressionsMDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

import es.minsait.ewpcv.model.iia.DeletedElements;
import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.dao.IDeletedElementsDAO;
import es.minsait.ewpcv.repository.impl.springdata.DeletedElementsRepository;
import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class IiaRegressionDAO implements IIiaRegressionsDAO {

    private IiaRegressionsRepository repository;

    private IiaRegressionsMDTOMapper mapper;


    @Autowired
    public IiaRegressionDAO(final IiaRegressionsRepository repository, IiaRegressionsMDTOMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public IiaRegressionsMDTO saveRegression(IiaRegressionsMDTO regression) {
        return mapper.entityToDto(repository.save(mapper.dtoToEntity(regression)));
    }
}
