package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.OrganizationUnit;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Organization unit mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(uses = {LanguageItemMDTOMapper.class, FactSheetMDTOMapper.class, InformationItemMDTOMapper.class,
    RequirementsInfoMDTOMapper.class, ContactDetailsMDTOMapper.class}, componentModel = "spring")
public interface OrganizationUnitMDTOMapper extends IModelDTOMapperCircular<OrganizationUnit, OrganizationUnitMDTO> {
}
