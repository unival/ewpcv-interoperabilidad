
package es.minsait.ewpcv.repository.model.course;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Academic year mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class AcademicYearMDTO extends BaseMDTO {

  String id;

  private Integer version;

  private String startYear;

  private String endYear;

}
