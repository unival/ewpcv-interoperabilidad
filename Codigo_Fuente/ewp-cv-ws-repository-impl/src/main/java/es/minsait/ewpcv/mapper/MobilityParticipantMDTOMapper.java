package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.MobilityParticipant;
import es.minsait.ewpcv.repository.model.organization.MobilityParticipantMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Mobility participant mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface MobilityParticipantMDTOMapper
    extends IModelDTOMapperCircular<MobilityParticipant, MobilityParticipantMDTO> {
}
