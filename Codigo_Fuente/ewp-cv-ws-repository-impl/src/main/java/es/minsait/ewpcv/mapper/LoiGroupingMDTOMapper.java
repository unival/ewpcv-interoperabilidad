package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import es.minsait.ewpcv.model.course.LoiGrouping;
import es.minsait.ewpcv.repository.model.course.LoiGroupingMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Loi grouping mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface LoiGroupingMDTOMapper extends IModelDTOMapperCircular<LoiGrouping, LoiGroupingMDTO> {

  @Override
  @Mapping(target = "loi", ignore = true)
  LoiGroupingMDTO entityToDto(LoiGrouping entity);

  @Override
  @Mapping(target = "loi", ignore = true)
  LoiGrouping dtoToEntity(LoiGroupingMDTO entity);
}
