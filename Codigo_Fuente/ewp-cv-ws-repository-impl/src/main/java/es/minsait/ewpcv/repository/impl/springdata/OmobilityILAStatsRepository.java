package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.omobility.OmobilityILAStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * The interface OmobilityILAStatsRepository repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface OmobilityILAStatsRepository extends JpaRepository<OmobilityILAStats, String> {

    @Query("Select statOmo FROM OmobilityILAStats statOmo WHERE statOmo.academicYearStart >='2021'")
    List<OmobilityILAStats> findGreaterThan2021();




}
