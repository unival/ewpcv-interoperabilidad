
package es.minsait.ewpcv.repository.model.course;

import java.math.BigDecimal;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Credit mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class CreditMDTO extends BaseMDTO {

  private static final long serialVersionUID = 5878082187961451526L;

  String id;

  private Integer version;

  private String scheme;

  private String level;

  private BigDecimal value;

}
