
package es.minsait.ewpcv.repository.model.iia;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Duration unit variants.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum DurationUnitVariants {
  HOURS, DAYS, WEEKS, MONTHS, YEARS;

  public static String[] names() {
    DurationUnitVariants[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }
}
