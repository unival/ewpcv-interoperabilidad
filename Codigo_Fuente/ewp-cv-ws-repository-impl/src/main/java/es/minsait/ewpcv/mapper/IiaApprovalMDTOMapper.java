package es.minsait.ewpcv.mapper;


import es.minsait.ewpcv.model.iia.IiaApproval;
import es.minsait.ewpcv.repository.model.iia.IiaApprovalMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;

/**
 * The interface Iia Approval mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring", uses = {IiaMDTOMapper.class})
public interface IiaApprovalMDTOMapper extends IModelDTOMapperCircular<IiaApproval, IiaApprovalMDTO> {

}

