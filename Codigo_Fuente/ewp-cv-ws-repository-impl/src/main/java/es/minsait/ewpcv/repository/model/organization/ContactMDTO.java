
package es.minsait.ewpcv.repository.model.organization;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Contact mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactMDTO extends BaseMDTO {

  private static final long serialVersionUID = 1890235560838437199L;

  String id;

  private Integer version;

  private String institutionId;

  private String organizationUnitId;

  private String role;

  private PersonMDTO person;

  private List<LanguageItemMDTO> name;

  private List<LanguageItemMDTO> description;

  private ContactDetailsMDTO contactDetails;

  private String intitutionId;

}
