package es.minsait.ewpcv.repository.utiles;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * The type Cycle avoiding mapping context.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class CycleAvoidingMappingContext {

  private static final ThreadLocal<Map<Object, Object>> CURRENT = new ThreadLocal<Map<Object, Object>>();

  public static void resetKnownInstances() {
    CURRENT.set(new IdentityHashMap<Object, Object>());
  }


  public static Map<Object, Object> getKnownInstances() {
    // si no hay operacion la anado ahora sin datos
    Map<Object, Object> knownInstances = new IdentityHashMap<Object, Object>();
    if (CURRENT.get() != null) {
      knownInstances = CURRENT.get();
    }
    return knownInstances;
  }
}
