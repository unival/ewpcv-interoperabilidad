package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.minsait.ewpcv.mapper.MobilityUpdateRequestMDTOMapper;
import es.minsait.ewpcv.model.omobility.MobilityUpdateRequest;
import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.dao.IMobilityUpdateRequestDAO;
import es.minsait.ewpcv.repository.impl.springdata.MobilityUpdateRequestRepository;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Mobility update request dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class MobilityUpdateRequestDAO implements IMobilityUpdateRequestDAO {

  private MobilityUpdateRequestMDTOMapper mobilityUpdateRequestMDTOMapper;
  private MobilityUpdateRequestRepository mobilityUpdateRequestRepository;

  @Autowired
  public MobilityUpdateRequestDAO(final MobilityUpdateRequestMDTOMapper mobilityUpdateRequestMDTOMapper,
      final MobilityUpdateRequestRepository mobilityUpdateRequestRepository) {
    this.mobilityUpdateRequestMDTOMapper = mobilityUpdateRequestMDTOMapper;
    this.mobilityUpdateRequestRepository = mobilityUpdateRequestRepository;
  }

  @Override
  public List<MobilityUpdateRequestMDTO> findAllNotProcessingByMobilityType(
      final UpdateRequestMobilityTypeEnum mobilityType, final int maxRetries, final int blockSize) {
    final Pageable p = new PageRequest(0, blockSize);
    return mobilityUpdateRequestMDTOMapper.entitiesToDtos(
        mobilityUpdateRequestRepository.findAllNotProcessingByMobilityType(mobilityType, maxRetries, p));
  }

  @Override
  public void changeStatusIsProcessingUpdateRequest(final List<String> updateRequestList, final boolean isProcessing) {
    mobilityUpdateRequestRepository.setProcessing(updateRequestList, isProcessing, new Date());
  }

  @Override
  public void deleteMobillityUpdateRequest(final String id) {
    mobilityUpdateRequestRepository.delete(id);
  }

  @Override
  public void saveMobilityUpdateRequest(final MobilityUpdateRequestMDTO mobilityUpdateRequestMDTO) {
    mobilityUpdateRequestRepository.save(mobilityUpdateRequestMDTOMapper.dtoToEntity(mobilityUpdateRequestMDTO));
  }

  @Override
  public void updateShouldRetryLastError(String id, String errorMessage) {
    mobilityUpdateRequestRepository.updateShouldRetryLastError(id, errorMessage);
  }

  @Override
  public void changeStatusIsNotProcessingAndRetriesUpdateRequest(final List<String> updateRequestList) {
    mobilityUpdateRequestRepository.setProcessingFalseIncrementRetries(updateRequestList);
  }

  @Override
  public List<MobilityUpdateRequestMDTO> obtenerRequestProcesandoseByTime(final Long maxProcessingTime) {
    final List<MobilityUpdateRequest> entities = mobilityUpdateRequestRepository.obtenerRequestProcesandose();
    if (CollectionUtils.isEmpty(entities)) {
      return new ArrayList<>();
    }
    final List<MobilityUpdateRequest> entitiesByDate = entities.stream()
        .filter(n -> (new Date().getTime() - n.getProcessingDate().getTime()) >= maxProcessingTime * 60000)
        .collect(Collectors.toList());
    return mobilityUpdateRequestMDTOMapper.entitiesToDtos(entitiesByDate);
  }
}
