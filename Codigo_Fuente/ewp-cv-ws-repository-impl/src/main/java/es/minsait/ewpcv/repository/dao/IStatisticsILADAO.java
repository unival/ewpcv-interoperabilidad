package es.minsait.ewpcv.repository.dao;


import es.minsait.ewpcv.repository.model.omobility.OmobilityILAStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;

import java.util.List;

/**
 * The interface StatisticsILA dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface IStatisticsILADAO {

    void saveStatsILA(StatisticsILAMDTO statsILA);

    List<OmobilityILAStatsMDTO> getStatsOfOmobilitiTable();
     List<StatisticsILAMDTO> getStatisticsRest();

}
