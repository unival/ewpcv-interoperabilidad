package es.minsait.ewpcv.repository.config;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

/**
 * The type Ewp naming strategy.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class EWPNamingStrategy implements PhysicalNamingStrategy {

  private static final String APP_NAME = "EWPCV";

  // Tablas esquema COMUN
  private static final String TC_PAISES = "TC_PAISES";
  private static final String TC_PROVINCIAS = "TC_PROVINCIAS";
  private static final String TC_LOCALIDADES = "TC_LOCALIDADES";
  private static final String COMARCAS = "COMARCAS";

  private List<String> listaTablasExternas = Arrays.asList(TC_PROVINCIAS, TC_LOCALIDADES, COMARCAS, TC_PAISES);

  @Override
  public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return name;
  }

  @Override
  public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return name;
  }

  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    if (listaTablasExternas.contains(name.getText())) {
      return name;
    } else {
      return convertToSnakeCase(name, Boolean.TRUE);
    }
  }

  @Override
  public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return convertToSnakeCase(name, Boolean.TRUE);
  }

  @Override
  public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return convertToSnakeCase(name, Boolean.FALSE);
  }

  @Nullable
  private Identifier convertToSnakeCase(final Identifier identifier, boolean addPrefix) {
    String newName = (addPrefix) ? APP_NAME.concat("_") : "";
    final String regex = "([a-z])([A-Z])";
    final String replacement = "$1_$2";
    if (identifier != null) {
      newName = newName.concat(identifier.getText().replaceAll(regex, replacement).toUpperCase());
    }

    return Identifier.toIdentifier(newName);
  }

}
