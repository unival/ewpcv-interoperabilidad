package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.organization.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The interface Contact repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IContactRepository extends JpaRepository<Contact, String> {

  @Query("SELECT cont FROM Contact cont WHERE LOWER(cont.institutionId) IN (:heisIds) AND cont.organizationUnitId IS NULL")
  List<Contact> findInstitutionsContacts(@Param("heisIds") List<String> heisIds);

  // La tabla Contact contiene una referencia a la ID Externa de la OUnit, que puede no ser única,
  // por lo que para recuperar los contactos de una OUnit necesitamos también saber la Institución,
  // ya que el par HEI ID + ID OUNIT EXTERNA sí es único.
  @Query("SELECT cont FROM Contact cont WHERE LOWER(cont.organizationUnitId) in (:ounitIds) AND cont.institutionId = :heiId")
  List<Contact> findOrganizationUnitContactsByOunitId(@Param("ounitIds") List<String> ounitIds, @Param("heiId") String heiId);
}
