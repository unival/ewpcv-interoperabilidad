package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.GradingScheme;
import es.minsait.ewpcv.repository.model.course.GradingSchemeMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Grading scheme mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface GradingSchemeMDTOMapper extends IModelDTOMapperCircular<GradingScheme, GradingSchemeMDTO> {
}
