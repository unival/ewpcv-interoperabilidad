package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Language item mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageItemMDTO extends BaseMDTO {

  private static final long serialVersionUID = 4988944441642222514L;

  String id;

  private Integer version;

  private String text;

  private String lang;


}
