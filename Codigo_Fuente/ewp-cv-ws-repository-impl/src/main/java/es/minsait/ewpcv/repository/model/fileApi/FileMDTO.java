package es.minsait.ewpcv.repository.model.fileApi;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;


/**
 * The type FileApi mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class FileMDTO extends BaseMDTO {

     private String id;
     private String fileId;
     private byte[] fileContent;
     private String schac;
     private String mimeType;

}
