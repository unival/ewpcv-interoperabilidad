package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.minsait.ewpcv.mapper.IiaMDTOMapper;
import es.minsait.ewpcv.model.iia.Iia;
import es.minsait.ewpcv.repository.dao.IIiaApprovalDAO;
import es.minsait.ewpcv.repository.impl.springdata.IiaRepository;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import lombok.extern.slf4j.Slf4j;


/**
 * The type Iia approval dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class IiaApprovalDAO extends QueryDslRepositorySupport implements IIiaApprovalDAO {

  private IiaMDTOMapper iiaMDTOMapper;
  private IiaRepository iiaRepository;

  @Autowired
  public IiaApprovalDAO(final IiaMDTOMapper iiaMDTOMapper, final IiaRepository iiaRepository) {
    super(Iia.class);
    this.iiaMDTOMapper = iiaMDTOMapper;
    this.iiaRepository = iiaRepository;
  }

  @Override
  public List<IiaMDTO> findApprovedIiasByRemoteIiaIdAndPartners(final String approvingHeiId, final String ownerHeiId,
                                                                final List<String> iiasId) {
    final List<String> partners = new ArrayList<>();
    partners.add(approvingHeiId);
    partners.add(ownerHeiId);
    final List<Iia> iiaList = iiaRepository.findApprovedIiasByRemoteIiaIdAndPartners(iiasId, partners).stream().distinct()
        .collect(Collectors.toList());
    return iiaMDTOMapper.entitiesToDtos(iiaList);
  }
}
