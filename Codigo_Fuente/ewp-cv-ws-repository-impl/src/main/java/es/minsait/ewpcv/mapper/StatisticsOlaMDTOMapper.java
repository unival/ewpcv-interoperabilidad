package es.minsait.ewpcv.mapper;
import es.minsait.ewpcv.model.omobility.StatisticsOla;
import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;
/**
 * The interface StatisticsOLA mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface StatisticsOlaMDTOMapper extends IModelDTOMapperCircular<StatisticsOla, StatisticsOlaMDTO> {
}

