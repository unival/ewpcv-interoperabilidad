package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.SubjectArea;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Subject area mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface SubjectAreaMDTOMapper extends IModelDTOMapperCircular<SubjectArea, SubjectAreaMDTO> {
}
