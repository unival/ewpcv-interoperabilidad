package es.minsait.ewpcv.repository.model.organization;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Mobility participant mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class MobilityParticipantMDTO extends BaseMDTO {

  private static final long serialVersionUID = 7251752586215452424L;

  String id;

  private Integer version;

  private ContactMDTO contactDetails;

  private String globalId;

}
