package es.minsait.ewpcv.repository.dao;

import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;

/**
 * The interface Event dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IEventDAO {

  void saveEvent(EventMDTO eventMDTO);

}
