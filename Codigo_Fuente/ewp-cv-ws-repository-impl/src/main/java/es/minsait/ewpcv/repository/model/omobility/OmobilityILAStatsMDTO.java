package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * The type OmobilityILAStatsMDTO mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class OmobilityILAStatsMDTO extends BaseMDTO {


    private String receivingInstitution;
    private String academicYearStart;
    private String academicYearEnd;
    private Integer totalNumber;
    private Integer lastWait;
    private Integer lastAppr;
    private Integer lastRej;
    private Integer approvalModif;
    private Integer approvalUnModif;
    private Integer sameVersionAppr;

}
