package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.FactSheet;
import es.minsait.ewpcv.repository.model.organization.FactSheetMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Fact sheet mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(uses = {ContactDetailsMDTOMapper.class, LanguageItemMDTOMapper.class}, componentModel = "spring")
public interface FactSheetMDTOMapper extends IModelDTOMapperCircular<FactSheet, FactSheetMDTO> {
}
