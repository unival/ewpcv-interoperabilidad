package es.minsait.ewpcv.repository.model.config;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Audit config mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditConfigMDTO extends BaseMDTO {
  private static final long serialVersionUID = 4441607632610662763L;

  private String name;

  private String value;
}
