package es.minsait.ewpcv.repository.model.organization;

import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Contact details mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactDetailsMDTO extends BaseMDTO {

  private static final long serialVersionUID = -7453313327957268749L;
  String id;

  private Integer version;

  private List<String> email;

  private List<LanguageItemMDTO> url;

  private PhoneNumberMDTO phoneNumber;

  private PhoneNumberMDTO faxNumber;

  private FlexibleAddressMDTO streetAddress;

  private FlexibleAddressMDTO mailingAddress;

  private List<PhotoUrlMDTO> photoUrls;

}
