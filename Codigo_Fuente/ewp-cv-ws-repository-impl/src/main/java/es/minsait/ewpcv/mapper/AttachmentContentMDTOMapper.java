package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.course.AttachmentContent;
import es.minsait.ewpcv.repository.model.course.AttachmentContentMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Attachment content mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface AttachmentContentMDTOMapper extends IModelDTOMapperCircular<AttachmentContent, AttachmentContentMDTO> {
}
