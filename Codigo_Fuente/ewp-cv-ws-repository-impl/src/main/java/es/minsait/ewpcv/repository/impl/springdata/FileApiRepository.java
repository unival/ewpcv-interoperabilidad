package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.fileApi.File;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * The interface FileApi repository.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
public interface FileApiRepository extends JpaRepository<File, String> {

    /**
     * Find by file id and schac file.
     *
     * @param fileId the file id
     * @param schac  the schac
     * @return the file
     */
    File findByFileIdAndSchac(String fileId, String schac);

}
