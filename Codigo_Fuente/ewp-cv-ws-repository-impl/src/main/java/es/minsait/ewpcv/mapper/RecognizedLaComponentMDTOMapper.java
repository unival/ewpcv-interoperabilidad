package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.RecognizedLaComponent;
import es.minsait.ewpcv.repository.model.omobility.RecognizedLaComponentMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Recognized la component mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface RecognizedLaComponentMDTOMapper
    extends IModelDTOMapperCircular<RecognizedLaComponent, RecognizedLaComponentMDTO> {
}
