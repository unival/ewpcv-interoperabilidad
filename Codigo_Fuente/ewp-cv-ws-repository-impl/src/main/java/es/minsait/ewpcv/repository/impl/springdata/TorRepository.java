package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.course.TranscriptOfRecord;
import es.minsait.ewpcv.model.omobility.Mobility;

/**
 * The interface Tor repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface TorRepository extends JpaRepository<TranscriptOfRecord, String> {

  @Query(
      value = "SELECT mob FROM Mobility mob LEFT JOIN mob.learningAgreement learn WHERE learn.transcriptOfRecord is not null"
          + " AND LOWER(mob.receivingInstitutionId) = LOWER(:receivingHei) AND mob.id in (:omobilityId) "
          + " AND LOWER(mob.sendingInstitutionId) in (:heisCoveredByClient) "
          + " AND mob.mobilityRevision in (SELECT max(mobilityRevision) FROM Mobility where id = mob.id)")
  List<Mobility> findTorByOmobilityIdAndReceivingInstitutionId(@Param(value = "receivingHei") String receivingHei,
      @Param(value = "omobilityId") List<String> omobilityId,
      @Param(value = "heisCoveredByClient") List<String> heisCoveredByClient);

  @Query(
      value = "SELECT mob FROM Mobility mob LEFT JOIN mob.learningAgreement learn WHERE learn.transcriptOfRecord is not null AND LOWER(mob.id) = LOWER(:omobilityId)"
          + " AND mob.mobilityRevision in (SELECT max(mobilityRevision) FROM Mobility where id = mob.id)")
  Mobility findMobilityWithTor(@Param(value = "omobilityId") String omobilityId);

  @Query(
      value = "SELECT mob FROM Mobility mob LEFT JOIN mob.learningAgreement learn WHERE learn.transcriptOfRecord is not null AND LOWER(mob.id) = LOWER(:omobilityId) "
          + " AND LOWER(mob.receivingInstitutionId) = LOWER(:receivingHei) AND LOWER(mob.sendingInstitutionId) = LOWER(:sendingHei)  ")
  List<String> findLocalMobilitiesWithTorId(@Param(value = "receivingHei") String receivingHei,
      @Param(value = "sendingHei") String sendingHei);

  @Query(value = "SELECT learn.transcriptOfRecord FROM Mobility mob LEFT JOIN mob.learningAgreement learn"
      + " WHERE learn.transcriptOfRecord is not null AND LOWER(mob.id) = LOWER(:omobilityId)")
  List<TranscriptOfRecord> findTorsByMobilityId(String mobilityId);
}

