package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Subject area mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class SubjectAreaMDTO extends BaseMDTO {

  private static final long serialVersionUID = 6675850468155873519L;

  private String id;
  private String iscedCode;

  private String iscedClarification;
}
