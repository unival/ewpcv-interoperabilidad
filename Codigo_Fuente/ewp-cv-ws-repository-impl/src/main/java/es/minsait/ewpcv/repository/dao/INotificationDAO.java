package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.notificacion.CnrType;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;

/**
 * The interface Notification dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
public interface INotificationDAO {

  void saveNotification(NotificationMDTO notificationMDTO);

  List<String> findDistinctNotificationsByNotificationTypeAndCnrType(CnrType cnrType,
      NotificationTypes notificationType, int retriesDelay, int blockSize);

  List<String> findIiasApprovalNotificationWhitoutIiaNotificationPending(CnrType cnrType, int retriesDelay, int blockSize);

  void reservarNotificaciones(List<String> changedElementsIds, CnrType tipoNotificacion, NotificationTypes tipoObjeto);

  void eliminarNotificacion(NotificationMDTO notification);

  void liberarNotificaciones(String changedElementIds, CnrType cnrType, NotificationTypes type, String lastError);

  void notificacionNoReintentable(String changedElementIds, CnrType cnrType, NotificationTypes type, String lastError);

  NotificationMDTO findLastNotification(String changedElementId, CnrType tipoNotificacion,
      NotificationTypes tipoObjeto);

  List<NotificationMDTO> obtenerNotificacionesProcesandoseByTime(Long maxProcessingTime);

  boolean existsPendingNotification(String changedElementIds, NotificationTypes tipoObjeto, String heiId);

  void marcaNotificacionesErroneasNoReintentables();
}
