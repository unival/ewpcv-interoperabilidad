package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;

import es.minsait.ewpcv.model.organization.MobilityParticipant;


/**
 * The interface Mobility participant repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface MobilityParticipantRepository extends JpaRepository<MobilityParticipant, String> {
}
