package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.iia.CooperationCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The interface Iia Approval repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface CooperationConditionRepository extends JpaRepository<CooperationCondition, String> {
    @Modifying
    @Query("UPDATE CooperationCondition set iiaApproval = :approvalId WHERE id = :coopCondId")
    void updateCoopCondById(@Param("coopCondId") String coopCondId, @Param("approvalId") String approvalId);
}
