package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * The type OmobilityStatsMDTO mdto.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Data
public class OmobilityStatsMDTO extends BaseMDTO {

    private String academicYearStart;
    private String academicYearEnd;
    private Integer totalOutgoingNumber;
    private Integer approvalUnModif;
    private Integer approvalModif;
    private Integer lastAppr;
    private Integer lastRej;
    private Integer lastWait;
    private String sendingInstitution;

}
