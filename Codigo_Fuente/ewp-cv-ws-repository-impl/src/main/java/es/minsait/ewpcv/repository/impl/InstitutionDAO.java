package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.InstitutionMDTOMapper;
import es.minsait.ewpcv.model.organization.Institution;
import es.minsait.ewpcv.model.organization.OrganizationUnit;
import es.minsait.ewpcv.repository.dao.IInstitutionDAO;
import es.minsait.ewpcv.repository.impl.springdata.InstitutionRepository;
import es.minsait.ewpcv.repository.impl.springdata.OrganizationUnitRepository;
import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The type Institution dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class InstitutionDAO implements IInstitutionDAO {

  @Autowired
  private InstitutionMDTOMapper institutionMDTOMapper;

  @Autowired
  private InstitutionRepository institutionRepository;

  @Autowired
  private OrganizationUnitRepository organizationUnitRepository;

  @Override
  public int getTotalInstitutionById(final String instId) {
    return institutionRepository.getTotalInstitutionById(instId);
  }

  @Override
  public InstitutionMDTO getInstitutionById(final String heiId) {
    return institutionMDTOMapper.entityToDto(institutionRepository.findIntitutionByHeiId(heiId));
  }

  @Override
  public InstitutionMDTO saveOrUpdate(final InstitutionMDTO institution) {
    final Institution inst = institutionMDTOMapper.dtoToEntity(institution);
    final List<OrganizationUnit> organizationUnits = new ArrayList<>();
    if (!CollectionUtils.isEmpty(institution.getOrganizationUnits())) {
      institution.getOrganizationUnits().forEach(o ->{
              if(Objects.nonNull(o)) {
                organizationUnits.add(organizationUnitRepository.findByInternalIdIgnoreCase(o.getInternalId()));
              }
      });
      inst.setOrganizationUnits(organizationUnits);
    }
    return institutionMDTOMapper.entityToDto(institutionRepository.save(inst));
  }

  @Override
  public InstitutionMDTO saveOrUpdateFactsheets(final InstitutionMDTO institution) {
    final Institution inst = institutionMDTOMapper.dtoToEntity(institution);
    final Institution instBBDD = Objects.nonNull(institution.getId()) ? institutionRepository.getOne(institution.getId()) : new Institution();
    instBBDD.setInstitutionId(institution.getInstitutionId());

    /** FACTSHEET **/
    instBBDD.setFactSheet(inst.getFactSheet());
    instBBDD.getFactSheet().setContactDetails(inst.getFactSheet().getContactDetails());


    /** INFORMATION ITEMS **/
    instBBDD.getInformationItems().clear();
    instBBDD.getInformationItems().addAll(inst.getInformationItems());


    /** ADDITIONAL REQUIREMENTS **/
    instBBDD.getAdditionalRequirements().clear();
    instBBDD.getAdditionalRequirements()

        .addAll(inst.getAdditionalRequirements());

    return institutionMDTOMapper.entityToDto(institutionRepository.save(instBBDD));
  }

  @Override
  public List<InstitutionMDTO> findAllByHeiId(final List<String> heiId) {
    heiId.replaceAll(String::toLowerCase);
    return institutionMDTOMapper.entitiesToDtos(institutionRepository.findAllByHeiId(heiId));
  }

  @Override
  public boolean isKnownHei(final String heiId) {
    return Objects.nonNull(institutionRepository.findIntitutionByHeiId(heiId));
  }

}
