package es.minsait.ewpcv.repository.model.omobility;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Language skill mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LanguageSkillMDTO extends BaseMDTO implements Comparable<LanguageSkillMDTO> {

  private static final long serialVersionUID = 7905440048702220395L;

  private String id;

  private String language;

  private String cefrLevel;

  @Override
  public int compareTo(LanguageSkillMDTO other) {
    if (this.getLanguage().equals(other.getLanguage())) {
      return this.getCefrLevel().compareTo(other.getCefrLevel());
    } else {
      //los isced_code son iguales pero los languages no son iguales
      return this.getLanguage().compareTo(other.getLanguage());
    }
  }
}
