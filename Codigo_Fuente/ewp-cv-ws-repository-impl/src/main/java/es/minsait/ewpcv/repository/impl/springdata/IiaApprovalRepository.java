package es.minsait.ewpcv.repository.impl.springdata;


import es.minsait.ewpcv.model.iia.IiaApproval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * The interface Iia Approval repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IiaApprovalRepository extends JpaRepository<IiaApproval, Long> {
    IiaApproval findFirst1ByLocalIiaOrderByDateIiaApprovalDesc(String localIia);

    @Query("SELECT ia" +
            " FROM IiaApproval ia" +
            " WHERE :iiaRemoteIiaId = ia.remoteIia" +
            " ORDER BY ia.dateIiaApproval DESC")
    List<IiaApproval> findLastIiaApprovalByIiaRemoteIiaId(@Param("iiaRemoteIiaId") String iiaRemoteIiaId);

    boolean existsByRemoteIia(String iiaRemoteIiaId);
}
