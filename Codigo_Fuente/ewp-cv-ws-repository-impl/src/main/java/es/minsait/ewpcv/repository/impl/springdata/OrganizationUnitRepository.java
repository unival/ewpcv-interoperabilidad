package es.minsait.ewpcv.repository.impl.springdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import es.minsait.ewpcv.model.organization.OrganizationUnit;

/**
 * The interface Organization unit repository.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface OrganizationUnitRepository extends JpaRepository<OrganizationUnit, String> {


  @Query("SELECT ounit FROM Institution inst inner join inst.organizationUnits as ounit"
      + " WHERE LOWER(inst.institutionId) = LOWER(:heiId) AND LOWER(ounit.id) in (:ounitsIds) "
      + " AND ounit.isExposed = 1")
  List<OrganizationUnit> findAllByHeiIdAndOunitIdExpuestas(@Param("heiId") String heiId,
      @Param("ounitsIds") List<String> ounitsIds);

  @Query("SELECT ounit FROM Institution inst right join inst.organizationUnits as ounit "
      + "WHERE LOWER(inst.institutionId) = LOWER(:heiId) AND LOWER(ounit.organizationUnitCode) in (:ounitCodes) "
      + " AND ounit.isExposed = 1")
  List<OrganizationUnit> findAllByHeiIdAndOunitCodeExpuestas(@Param("heiId") String heiId,
      @Param("ounitCodes") List<String> ounitCodes);

  @Query("SELECT ounit FROM Institution inst inner join inst.organizationUnits as ounit"
      + " WHERE LOWER(inst.institutionId) = LOWER(:heiId)")
  List<OrganizationUnit> findAllByHeiId(@Param("heiId") String heiId);


  @Query("Select ounit FROM OrganizationUnit ounit  WHERE LOWER(ounit.internalId) = LOWER(:internalId)")
  OrganizationUnit findByInternalIdIgnoreCase(@Param("internalId") String internalId);

  @Query("SELECT ounit FROM Institution inst inner join inst.organizationUnits as ounit"
          + " WHERE LOWER(inst.institutionId) = LOWER(:schac) AND LOWER(ounit.id) = LOWER(:interopId)")
  OrganizationUnit findByInteropIdAndIntitutionSchac(@Param("interopId") String interopId, @Param("schac") String schac);

  @Query("SELECT COUNT(1) FROM Institution inst inner join inst.organizationUnits as ounit"
          + " WHERE LOWER(inst.institutionId) = LOWER(:schac) AND LOWER(ounit.id) = LOWER(:interopId)")
  Long countByInteropIdAndIntitutionSchac(@Param("interopId") String interopId, @Param("schac") String schac);
}
