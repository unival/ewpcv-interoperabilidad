package es.minsait.ewpcv.mapper;
import es.minsait.ewpcv.model.omobility.OmobilityStats;
import es.minsait.ewpcv.repository.model.omobility.OmobilityStatsMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;
import org.mapstruct.Mapper;
/**
 * The interface OmobilityStats mdto mapper.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface OmobilityStatsMDTOMapper  extends IModelDTOMapperCircular<OmobilityStats, OmobilityStatsMDTO> {
}