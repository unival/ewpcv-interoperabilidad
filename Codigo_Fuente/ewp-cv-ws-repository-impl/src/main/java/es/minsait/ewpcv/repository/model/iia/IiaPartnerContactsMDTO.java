package es.minsait.ewpcv.repository.model.iia;

import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Iia partner contacts mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class IiaPartnerContactsMDTO extends BaseMDTO {

  private static final long serialVersionUID = -6157055537656112724L;

  String iiaId;
  ContactMDTO contactMDTO;

}
