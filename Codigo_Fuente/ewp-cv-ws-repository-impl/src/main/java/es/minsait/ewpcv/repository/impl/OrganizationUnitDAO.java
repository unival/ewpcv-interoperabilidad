package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.mapper.OrganizationUnitMDTOMapper;
import es.minsait.ewpcv.model.organization.OrganizationUnit;
import es.minsait.ewpcv.repository.dao.IOrganizationUnitDAO;
import es.minsait.ewpcv.repository.impl.springdata.OrganizationUnitRepository;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;

/**
 * The type Organization unit dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Repository
public class OrganizationUnitDAO implements IOrganizationUnitDAO {

  @Autowired
  private OrganizationUnitMDTOMapper organizationUnitMDTOMapper;

  @Autowired
  private OrganizationUnitRepository organizationUnitRepository;

  @Override
  public List<OrganizationUnitMDTO> findAllByHeiIdAndOunitIdExpuestas(final String heiId,
      final List<String> ounitsIds) {
    ounitsIds.replaceAll(String::toLowerCase);
    return organizationUnitMDTOMapper
        .entitiesToDtos(organizationUnitRepository.findAllByHeiIdAndOunitIdExpuestas(heiId, ounitsIds));
  }

  @Override
  public List<OrganizationUnitMDTO> findAllByHeiId(final String heiId) {
    return organizationUnitMDTOMapper.entitiesToDtos(organizationUnitRepository.findAllByHeiId(heiId));
  }

  @Override
  public List<OrganizationUnitMDTO> findAllByHeiIdAndOunitCodeExpuestas(final String heiId,
      final List<String> ounitCodes) {
    ounitCodes.replaceAll(String::toLowerCase);
    return organizationUnitMDTOMapper
        .entitiesToDtos(organizationUnitRepository.findAllByHeiIdAndOunitCodeExpuestas(heiId, ounitCodes));
  }

  @Override
  public OrganizationUnitMDTO saveOrUpdate(final OrganizationUnitMDTO mdto) {
    final OrganizationUnit entity = organizationUnitMDTOMapper.dtoToEntity(mdto);
    return organizationUnitMDTOMapper.entityToDto(organizationUnitRepository.save(entity));
  }

  @Override
  public boolean existsOunit(String ounitId, String institutionSchac) {
    Long count = organizationUnitRepository.countByInteropIdAndIntitutionSchac(ounitId, institutionSchac);
    return Objects.nonNull(count) && count > 0;
  }

  @Override
  public OrganizationUnitMDTO findByIdAndIntitutionSchac(String ounitId, String institutionSchac) {
    return organizationUnitMDTOMapper.entityToDto(organizationUnitRepository.findByInteropIdAndIntitutionSchac(ounitId, institutionSchac));
  }

  @Override
  public OrganizationUnitMDTO findByInternalId(String internalId) {
    return organizationUnitMDTOMapper.entityToDto(organizationUnitRepository.findByInternalIdIgnoreCase(internalId));
  }
}
