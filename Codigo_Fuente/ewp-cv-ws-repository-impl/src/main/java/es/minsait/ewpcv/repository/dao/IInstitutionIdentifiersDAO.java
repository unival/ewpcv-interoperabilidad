package es.minsait.ewpcv.repository.dao;

import java.util.List;

import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;

/**
 * The interface Institution identifiers dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public interface IInstitutionIdentifiersDAO {

  boolean existInstitutionBySchac(final String schac);

  void saveOrUpdateInstitutionIdentifier(InstitutionIdentifiersMDTO institutionIdentifiersMDTO);

  List<InstitutionIdentifiersMDTO> findAllBySCHAC(List<String> heisIds);
}
