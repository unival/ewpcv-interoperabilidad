
package es.minsait.ewpcv.repository.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Learning agreement component status.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LearningAgreementComponentStatus {
  INSERTED( "inserted" ), DELETED( "deleted" );

  private String value;

  LearningAgreementComponentStatus( String value ) {
    this.value = value;
  }

  public static String[] names() {
    LearningAgreementComponentStatus[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public static LearningAgreementComponentStatus fromValue( String value ) {
    LearningAgreementComponentStatus[] codes = values();
    return Arrays.stream( codes ).filter( s -> s.value().equalsIgnoreCase( value ) ).findFirst().orElse( null );
  }

	public String value() {
    return value;
  }
}
