package es.minsait.ewpcv.repository.model.omobility;

import java.util.Date;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Signature mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class SignatureMDTO extends BaseMDTO {

  private static final long serialVersionUID = -8062447477911977262L;

  String id;

  private Integer version;

  private String signerName;

  private String signerPosition;

  private String signerEmail;

  private Date timestamp;

  private String signerApp;

  private byte[] signature;
}
