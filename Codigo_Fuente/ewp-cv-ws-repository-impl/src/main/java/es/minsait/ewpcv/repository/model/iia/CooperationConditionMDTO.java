
package es.minsait.ewpcv.repository.model.iia;

import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.utiles.BaseMDTO;
import lombok.Data;

/**
 * The type Cooperation condition mdto.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class CooperationConditionMDTO extends BaseMDTO {

  private static final long serialVersionUID = 7235122653559858438L;

  String id;

  private Integer version;

  private Date endDate;

  private List<Byte> eqfLevel;

  private Date startDate;

  private DurationMDTO duration;

  private MobilityNumberMDTO mobilityNumber;

  private MobilityTypeMDTO mobilityType;

  private IiaPartnerMDTO receivingPartner;

  private IiaPartnerMDTO sendingPartner;

  private String otherInfo;

  private Boolean blended;

  private Boolean terminated;

  private List<CooperationConditionSubjAreaLangSkillMDTO> languageSkill;

  private List<CooperationConditionSubjectAreaMDTO> subjectAreaList;

  private String iiaApproval;

  private int orderIndex;

}
