package es.minsait.ewpcv.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.minsait.ewpcv.mapper.NotificationMDTOMapper;
import es.minsait.ewpcv.model.notificacion.Notification;
import es.minsait.ewpcv.repository.dao.INotificationDAO;
import es.minsait.ewpcv.repository.impl.springdata.NotificationRepository;
import es.minsait.ewpcv.repository.model.notificacion.CnrType;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.repository.utiles.EnumParser;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Notification dao.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Repository
@Slf4j
public class NotificationDAO implements INotificationDAO {

  private NotificationMDTOMapper notificationMDTOMapper;
  private NotificationRepository notificationRepository;

  @Autowired
  public NotificationDAO(final NotificationMDTOMapper notificationMDTOMapper,
      final NotificationRepository notificationRepository) {
    this.notificationMDTOMapper = notificationMDTOMapper;
    this.notificationRepository = notificationRepository;
  }


  @Override
  public void saveNotification(final NotificationMDTO notificationMDTO) {
    notificationMDTO.setShouldRetry(Boolean.TRUE);
    notificationRepository.save(notificationMDTOMapper.dtoToEntity(notificationMDTO));
  }

  @Override
  public List<String> findDistinctNotificationsByNotificationTypeAndCnrType(final CnrType cnrType,
      final NotificationTypes notificationType, final int retriesDelay, final int blockSize) {
    final Pageable p = new PageRequest(0, blockSize);
    return notificationRepository.findDistinctPendingNotificationCahngedElements(EnumParser.modelToEntity(cnrType),
        EnumParser.modelToEntity(notificationType), retriesDelay, p);
  }

  @Override
  public List<String> findIiasApprovalNotificationWhitoutIiaNotificationPending(final CnrType cnrType,
      final int retriesDelay, final int blockSize) {
    final Pageable p = new PageRequest(0, blockSize);
    return notificationRepository.findIiasApprovalNotificationWhitoutIiaNotificationPending(
        EnumParser.modelToEntity(cnrType), EnumParser.modelToEntity(NotificationTypes.IIA_APPROVAL), retriesDelay, p);
  }

  @Override
  public void reservarNotificaciones(final List<String> changedElementsIds, final CnrType tipoNotificacion,
      final NotificationTypes tipoObjeto) {
    notificationRepository.setProcessing(changedElementsIds,
        es.minsait.ewpcv.model.notificacion.CnrType.valueOf(tipoNotificacion.name()),
        es.minsait.ewpcv.model.notificacion.NotificationTypes.valueOf(tipoObjeto.name()), new Date());
  }

  @Override
  public void eliminarNotificacion(final NotificationMDTO notificacion) {
    notificationRepository.eliminarNotificaciones(notificacion.getHeiId(), notificacion.getOwnerHeiId(),
        notificacion.getChangedElementIds(), EnumParser.modelToEntity(notificacion.getType()),
        EnumParser.modelToEntity(notificacion.getCnrType()), notificacion.getNotificationDate());
  }

  @Override
  public void liberarNotificaciones(final String changedElementIds, final CnrType cnrType,
      final NotificationTypes type, String lastError) {
    //tenemos que limitar el last Error para que quepa en la columna varchar2(4000 byte)
    if (lastError != null && lastError.length() > 4000) {
      lastError = lastError.substring(0, 3999);
    }

    notificationRepository.setNotProcessing(changedElementIds,
        es.minsait.ewpcv.model.notificacion.CnrType.valueOf(cnrType.name()),
        es.minsait.ewpcv.model.notificacion.NotificationTypes.valueOf(type.name())
        , lastError);
  }

  @Override
  public void notificacionNoReintentable(final String changedElementIds, final CnrType cnrType,
                                         final NotificationTypes type, String lastError) {
    notificationRepository.setNotProcessingAndNotRepeteable(changedElementIds,
        es.minsait.ewpcv.model.notificacion.CnrType.valueOf(cnrType.name()),
        es.minsait.ewpcv.model.notificacion.NotificationTypes.valueOf(type.name())
        , lastError);
  }

  @Override
  public NotificationMDTO findLastNotification(final String changedElementId, final CnrType tipoNotificacion,
      final NotificationTypes tipoObjeto) {
    final Pageable p = new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "notificationDate"));
    final List<Notification> list = notificationRepository.findByChangedElementIdTypeAndNotificationType(
        changedElementId, es.minsait.ewpcv.model.notificacion.NotificationTypes.valueOf(tipoObjeto.name()),
        es.minsait.ewpcv.model.notificacion.CnrType.valueOf(tipoNotificacion.name()), p);
    return CollectionUtils.isEmpty(list) ? null : notificationMDTOMapper.entityToDto(list.get(0));
  }

  @Override
  public List<NotificationMDTO> obtenerNotificacionesProcesandoseByTime(final Long maxProcessingTime) {
    final List<Notification> entities = notificationRepository.obtenerNotificacionesProcesandose();
    final List<Notification> entitiesByDate = entities.stream()
        .filter(n -> (new Date().getTime() - n.getProcessingDate().getTime()) >= maxProcessingTime * 60000)
        .collect(Collectors.toList());
    return notificationMDTOMapper.entitiesToDtos(entitiesByDate);
  }
  @Override
  public boolean existsPendingNotification(String changedElementIds, NotificationTypes tipoObjeto, String heiId) {
    return notificationRepository.existsPendingNotification(changedElementIds, es.minsait.ewpcv.model.notificacion.NotificationTypes.valueOf(tipoObjeto.name()), heiId) > 0;
  }

  @Override
  public void marcaNotificacionesErroneasNoReintentables() {
    notificationRepository.marcaNotificacionesErroneasNoReintentables();
  }
}
