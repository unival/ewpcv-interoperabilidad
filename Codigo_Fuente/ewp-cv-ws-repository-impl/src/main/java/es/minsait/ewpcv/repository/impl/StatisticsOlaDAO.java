package es.minsait.ewpcv.repository.impl;

import es.minsait.ewpcv.mapper.OmobilityStatsMDTOMapper;
import es.minsait.ewpcv.mapper.StatisticsOlaMDTOMapper;
import es.minsait.ewpcv.repository.dao.IStatisticsOlaDAO;
import es.minsait.ewpcv.repository.impl.springdata.LearningAgreementRepository;
import es.minsait.ewpcv.repository.impl.springdata.OmobilityStatsRepository;
import es.minsait.ewpcv.repository.impl.springdata.StatisticsOlaRepository;
import es.minsait.ewpcv.repository.model.omobility.OmobilityStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * The type StatisticsOla dao.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Repository
@Slf4j
public class StatisticsOlaDAO implements IStatisticsOlaDAO {
    @Autowired
    private StatisticsOlaRepository statisticsOlaRepository;
    @Autowired
    private OmobilityStatsRepository omobilityStatsRepository;
    @Autowired
    private StatisticsOlaMDTOMapper statisticsOlaMDTOMapper;
    @Autowired
    private OmobilityStatsMDTOMapper omobilityStatsMDTOMapper;
    @Override
    public void saveStatsOla(StatisticsOlaMDTO statsOla){
        statisticsOlaRepository.save(statisticsOlaMDTOMapper.dtoToEntity(statsOla));
    }
    @Override
    public List<OmobilityStatsMDTO> getStatsOfOmobilitiTable() {
        return omobilityStatsMDTOMapper.entitiesToDtos(omobilityStatsRepository.findGreaterThan2021());
    }

    @Override
    public List<StatisticsOlaMDTO> getStatisticsRest() {

        return statisticsOlaMDTOMapper.entitiesToDtos(statisticsOlaRepository.findMostRecentStats());

    }


}
