package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.omobility.StudiedLaComponent;
import es.minsait.ewpcv.repository.model.omobility.StudiedLaComponentMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Studied la component mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface StudiedLaComponentMDTOMapper
    extends IModelDTOMapperCircular<StudiedLaComponent, StudiedLaComponentMDTO> {
}
