package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.iia.IiaPartner;
import es.minsait.ewpcv.repository.model.iia.IiaPartnerMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Iia partner mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(componentModel = "spring")
public interface IiaPartnerMDTOMapper extends IModelDTOMapperCircular<IiaPartner, IiaPartnerMDTO> {
}
