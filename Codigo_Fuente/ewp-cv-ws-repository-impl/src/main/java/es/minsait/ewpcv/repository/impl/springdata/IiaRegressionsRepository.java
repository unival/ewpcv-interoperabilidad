package es.minsait.ewpcv.repository.impl.springdata;

import es.minsait.ewpcv.model.iia.IiaRegressions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IiaRegressionsRepository extends JpaRepository<IiaRegressions, Integer> {
}
