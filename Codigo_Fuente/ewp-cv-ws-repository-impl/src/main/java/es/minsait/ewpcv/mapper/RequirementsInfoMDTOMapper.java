package es.minsait.ewpcv.mapper;

import org.mapstruct.Mapper;

import es.minsait.ewpcv.model.organization.RequirementsInfo;
import es.minsait.ewpcv.repository.model.organization.RequirementsInfoMDTO;
import es.minsait.ewpcv.repository.utiles.IModelDTOMapperCircular;

/**
 * The interface Requirements info mdto mapper.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Mapper(uses = {ContactDetailsMDTOMapper.class}, componentModel = "spring")
public interface RequirementsInfoMDTOMapper extends IModelDTOMapperCircular<RequirementsInfo, RequirementsInfoMDTO> {
}
