package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.impl.IiaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;

/**
 * The type Iia converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
public class IiaConverter implements IIiaConverter {

  @Autowired
  private IIiaDAO iiiaDAO;

  @Override
  public List<IiasGetResponse.Iia> convertToIias(String hei_id, List<IiaMDTO> iiaList) {
    return iiaList.stream().map((IiaMDTO iia) -> IiaConverterCommon.convertSingleIia(hei_id, iia, iiiaDAO))
        .collect(Collectors.toList());
  }

  @Override
  public List<IiaMDTO> convertToIiaMDTO(String hei_id, List<IiasGetResponse.Iia> iiaList, boolean sendPdf) {
    return iiaList.stream().map((IiasGetResponse.Iia iia) -> IiaConverterCommon.convertSingleIia(hei_id, iia))
        .collect(Collectors.toList());
  }
}
