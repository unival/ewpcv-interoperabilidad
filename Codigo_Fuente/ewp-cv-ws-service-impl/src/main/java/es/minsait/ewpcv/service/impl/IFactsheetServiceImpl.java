package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.FactshetConverter;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.repository.impl.InstitutionDAO;
import es.minsait.ewpcv.repository.model.organization.InstitutionMDTO;
import es.minsait.ewpcv.service.api.IFactsheetService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse.Factsheet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The type Factsheet service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class IFactsheetServiceImpl implements IFactsheetService {

  private InstitutionDAO institutionDAO;
  private FactshetConverter converter;
  private OmobilityLasService omobilityLasService;

  @Autowired
  public IFactsheetServiceImpl(final InstitutionDAO institutionDAO, final FactshetConverter converter,
      final OmobilityLasService omobilityLasService) {
    this.institutionDAO = institutionDAO;
    this.converter = converter;
    this.omobilityLasService = omobilityLasService;
  }

  @Override
  public List<Factsheet> obtenerFactsheets(final List<String> heisIds) throws EwpConverterException {
    final List<InstitutionMDTO> institutions = new ArrayList<>();

    final List<Factsheet> resultado = new ArrayList<>();

    for (final String heiId : heisIds) {
      final InstitutionMDTO inst = institutionDAO.getInstitutionById(heiId);
      if (Objects.nonNull(inst)) {
        institutions.add(inst);
      }
    }

    resultado.addAll(converter.convertToFactsheet(institutions));
    return resultado;
  }

  @Override
  @Transactional(readOnly = false)
  public void actualizarFactsheets(final Factsheet factsheet) {

    final InstitutionMDTO local = institutionDAO.getInstitutionById(factsheet.getHeiId());
    try {
      final InstitutionMDTO remoto = converter.convertToInstitutionMDTO(factsheet);
      if (Objects.nonNull(local)) {
        if (hayActualizacionDatos(local, remoto)) {
          // actualizamos el factsheet
          institutionDAO.saveOrUpdateFactsheets(local);
        } else {
          log.info("No hay nada que actualizar");
        }
      } else {
        // la institucion no existe en nuestro sistema
        log.info(" !!!!!! La institucion {} no existe en base de datos", factsheet.getHeiId());
        institutionDAO.saveOrUpdateFactsheets(remoto);
      }
    }catch (NullPointerException e){
      log.error("El factsheet recibido no cumple con la especificación de EWP ",e);
    }
  }

  private boolean hayActualizacionDatos(final InstitutionMDTO local, final InstitutionMDTO remoto) {

    return compararInstitutionsFactsheet(remoto, local);
  }

  /**
   * Compara la informacion de los factsheets de dos institutions y actualiza la copia local
   * 
   * @param remoto InstitutionMDTO remoto
   * @param local InstitutionMDTO local
   * @return true si hay diferencias entre los objetos
   */
  private boolean compararInstitutionsFactsheet(final InstitutionMDTO remoto, final InstitutionMDTO local) {
    Boolean hayCambio = Boolean.FALSE;

    log.trace("Comparamos Factsheet");
    if (Objects.nonNull(local.getFactSheet())) {
      if (Objects.nonNull(remoto.getFactSheet())
          && omobilityLasService.compararFactSheet(remoto.getFactSheet(), local.getFactSheet())) {
        log.trace("Ha cambiado el Factsheet");
        hayCambio = Boolean.TRUE;
      }
    } else if (Objects.nonNull(remoto.getFactSheet())) {
      log.trace("No existe Factseheet en local pero si en remoto, actualizamos el local");
      local.setFactSheet(remoto.getFactSheet());
      hayCambio = Boolean.TRUE;
    }

    log.trace("Comparamos InformationItems");
    if (Objects.nonNull(local.getInformationItems())) {
      if (omobilityLasService.compararInformationItemList(remoto.getInformationItems(), local.getInformationItems())) {
        log.trace("Ha cambiado la propiedad InformationItems");
        hayCambio = Boolean.TRUE;
      }
    } else if (Objects.nonNull(remoto.getInformationItems())) {
      log.trace(" No existen InformationItems en local pero si en remoto,  actualizamos local ");
      local.getInformationItems().addAll(remoto.getInformationItems());
      hayCambio = Boolean.TRUE;
    }


    log.trace("Comparamos AdditionalRequirements");
    if (Objects.nonNull(local.getAdditionalRequirements())) {
      if (omobilityLasService.compararAdditionalRequirementList(remoto.getAdditionalRequirements(),
          local.getAdditionalRequirements())) {
        log.trace("Ha cambiado la propiedad AdditionalRequirements");
        hayCambio = Boolean.TRUE;
      }
    } else if (Objects.nonNull(remoto.getAdditionalRequirements())) {
      log.trace(" No  existen AdditionalRequirements en local pero si en remoto, actualizamos  local ");
      local.setAdditionalRequirements(remoto.getAdditionalRequirements());
      hayCambio = Boolean.TRUE;
    }


    return hayCambio;
  }
}
