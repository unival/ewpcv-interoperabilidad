package es.minsait.ewpcv.service.impl;




import es.minsait.ewpcv.converter.api.IStatisticsILAConverter;
import es.minsait.ewpcv.repository.dao.IStatisticsILADAO;
import es.minsait.ewpcv.repository.model.omobility.OmobilityILAStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;
import es.minsait.ewpcv.service.api.IStatisticsILAService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasIncomingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The interface StatisticsILA service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Service
@Slf4j
@Transactional
public class StatisticsILAServiceImpl implements IStatisticsILAService {

    @Autowired
    private IStatisticsILADAO statisticsILADAO;
    @Autowired
    private IStatisticsILAConverter iStatisticsILAConverter;

    @Override
    public void obtainStatistics(String user) {
        List<OmobilityILAStatsMDTO> omoMdtos = statisticsILADAO.getStatsOfOmobilitiTable();



        for(OmobilityILAStatsMDTO omoMdto : omoMdtos ){
            if(omoMdto.getReceivingInstitution().equalsIgnoreCase(user)) {

                StatisticsILAMDTO statILA = new StatisticsILAMDTO();

                statILA.setAcademicYearStart(omoMdto.getAcademicYearStart());
                statILA.setAcademicYearEnd(omoMdto.getAcademicYearEnd());
                statILA.setReceivingInstitution(omoMdto.getReceivingInstitution());
                statILA.setTotalNumber(omoMdto.getTotalNumber());
                statILA.setLastWait(omoMdto.getLastWait());
                statILA.setLastAppr(omoMdto.getLastAppr());
                statILA.setLastRej(omoMdto.getLastRej());
                statILA.setApprovalModif(omoMdto.getApprovalModif());
                statILA.setApprovalUnModif(omoMdto.getApprovalUnModif());
                statILA.setSameVersionAppr(omoMdto.getSameVersionAppr());
                statILA.setDumpDate(new Date());

                statisticsILADAO.saveStatsILA(statILA);
            }
        }

    }
    @Override
    public List<LasIncomingStatsResponse.AcademicYearLaStats> getStatisticsRest() {
        return iStatisticsILAConverter.convertToStatsResponses(statisticsILADAO.getStatisticsRest()) ;
    }

}
