package es.minsait.ewpcv.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.minsait.ewpcv.converter.api.IInstitutionIdentifiersConverter;
import es.minsait.ewpcv.registryclient.HeiEntry;
import es.minsait.ewpcv.repository.dao.IInstitutionIdentifiersDAO;
import es.minsait.ewpcv.service.api.IInstitutionIdentifiersService;

/**
 * The type Institution identifiers service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class InstitutionIdentifiersServiceImpl implements IInstitutionIdentifiersService {

  private IInstitutionIdentifiersDAO institutionIdentifiersDAO;
  private IInstitutionIdentifiersConverter institutionIdentifiersConverter;

  @Autowired
  public InstitutionIdentifiersServiceImpl(final IInstitutionIdentifiersDAO institutionIdentifiersDAO,
      final IInstitutionIdentifiersConverter institutionIdentifiersConverter) {
    this.institutionIdentifiersDAO = institutionIdentifiersDAO;
    this.institutionIdentifiersConverter = institutionIdentifiersConverter;
  }

  @Override
  public boolean existInstitutionBySchac(final String schac) {
    return this.institutionIdentifiersDAO.existInstitutionBySchac(schac);
  }

  @Override
  public void saveOrUpdateInstitutionIdentifier(final HeiEntry heiEntry) {
    this.institutionIdentifiersDAO.saveOrUpdateInstitutionIdentifier(
        institutionIdentifiersConverter.convertToInstitutionIdentifiersMDTO(heiEntry));
  }

}
