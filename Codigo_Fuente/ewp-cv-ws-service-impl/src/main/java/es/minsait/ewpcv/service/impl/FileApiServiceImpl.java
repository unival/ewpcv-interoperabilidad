package es.minsait.ewpcv.service.impl;


import es.minsait.ewpcv.repository.impl.FileApiDAO;
import es.minsait.ewpcv.repository.model.fileApi.FileMDTO;
import es.minsait.ewpcv.service.api.IFileApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * The interface FileApi service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)loc
 */

@Service
@Slf4j
@Transactional
public class FileApiServiceImpl implements IFileApiService {

  @Autowired
  private FileApiDAO fileApiDAO;


  @Override
  public FileMDTO getFileByIdAndScach(String id, String schac) {
    return fileApiDAO.getFileByIdAndSchac(id, schac);
  }

  @Override
  public void insertFile(byte[] fileData, String fileId, String schac, String mimeType) {
    FileMDTO file = new FileMDTO();
    file.setSchac(schac.toLowerCase());
    file.setFileId(fileId);
    file.setFileContent(fileData);
    file.setMimeType(mimeType);
    fileApiDAO.insertFile(file);
  }

  @Override
  public boolean existsFile(String fileId, String schac) {
    return fileApiDAO.existsFile(fileId, schac.toLowerCase());
  }
}
