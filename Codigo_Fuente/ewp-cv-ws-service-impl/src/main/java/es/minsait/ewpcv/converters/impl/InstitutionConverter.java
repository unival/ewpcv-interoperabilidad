package es.minsait.ewpcv.converters.impl;

import com.google.common.base.Strings;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import es.minsait.ewpcv.converter.api.IInstitutionConverter;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import eu.erasmuswithoutpaper.api.types.contact.Contact;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Institution converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class InstitutionConverter implements IInstitutionConverter {

  @Autowired
  private InstitutionIdentifiersConverter institutionIdentifiersConverter;

  @Override
  public List<InstitutionsResponse.Hei> convertoToHeisList(final List<InstitutionMDTO> institutions,
      final List<InstitutionIdentifiersMDTO> institutionsIds, final Map<String, List<ContactMDTO>> contactsMap) {
    final List<InstitutionsResponse.Hei> res = new ArrayList<>();

    for (final InstitutionMDTO inst : institutions) {
      if (Objects.nonNull(inst)) {
        res.add(convertToHei(inst, obtenerIdentificadorBySCHAC(institutionsIds, inst.getInstitutionId()),
            contactsMap.get(inst.getInstitutionId())));
      }
    }

    return res;
  }

  @Override
  public InstitutionsResponse.Hei convertToHei(final InstitutionMDTO institution,
      final InstitutionIdentifiersMDTO institutionId, final List<ContactMDTO> contacts) {
    final InstitutionsResponse.Hei res = new InstitutionsResponse.Hei();

    res.setHeiId(institution.getInstitutionId());

    /** OTHER IDs **/
    if (Objects.nonNull(institutionId)) {
      res.getOtherId()
          .addAll(institutionIdentifiersConverter.convertOtherIdsStringToOtherHeiId(institutionId));
    }

    /** NAMES **/
    final List<StringWithOptionalLang> names = new ArrayList<>();
    institution.getName().forEach(name -> {
      names.add(CommonsConverter.convertToStringWithOptionalLang(name));
    });
    res.getName().addAll(names);

    res.setAbbreviation(institution.getAbbreviation());
    res.setLogoUrl(institution.getLogoUrl());


    if (Objects.nonNull(institution.getPrimaryContactDetails())) {
      res.setStreetAddress(
          CommonsConverter.convertToFlexibleAddress(institution.getPrimaryContactDetails().getStreetAddress()));
      res.setMailingAddress(
          CommonsConverter.convertToFlexibleAddress(institution.getPrimaryContactDetails().getMailingAddress()));

      /** WEBSITE URL */
      final List<HTTPWithOptionalLang> websites = new ArrayList<>();

      for (final LanguageItemMDTO lang : institution.getPrimaryContactDetails().getUrl()) {
        websites.add(CommonsConverter.convertLanguageItemMDTOToHttpWithOptionalLang(lang));
      }
      res.getWebsiteUrl().addAll(websites);
    }

    if (Objects.nonNull(institution.getOrganizationUnits())) {
      /** rootOUnitId **/
      if (institution.getOrganizationUnits().stream().anyMatch(OrganizationUnitMDTO::getIsTreeStructure)) {
        final List<OrganizationUnitMDTO> rootOrganizationUnit = institution.getOrganizationUnits().stream()
            .filter(ou -> ou.getIsTreeStructure() && ou.getIsExposed() && Strings.isNullOrEmpty(ou.getParentOUnitId()))
            .collect(Collectors.toList());

        if (rootOrganizationUnit.size() == 1) {
          res.setRootOunitId(rootOrganizationUnit.get(0).getId());
        }
      }

      /** OUnits **/
      final Set<String> orgUnitsIds = new TreeSet<>(); // A set not to add the root unit twice

      if (institution.getOrganizationUnits().stream().anyMatch(OrganizationUnitMDTO::getIsTreeStructure)) {
        if (Objects.nonNull(res.getRootOunitId())) {
          orgUnitsIds.add(res.getRootOunitId());
        }

        institution.getOrganizationUnits().stream()
            .filter(ou -> Objects.nonNull(ou) && ou.getIsExposed() && ou.getIsTreeStructure())
            .forEach(oUnit -> orgUnitsIds.add(oUnit.getId()));
      } else {
        institution.getOrganizationUnits().stream()
            .filter(ou -> Objects.nonNull(ou) && ou.getIsExposed())
            .forEach(oUnit -> orgUnitsIds.add(oUnit.getId()));
      }

      res.getOunitId().addAll(orgUnitsIds);
    }

    /** FACTSHEET URLs **/
    if (Objects.nonNull(institution.getFactSheet())) {
      final List<HTTPWithOptionalLang> httpList = new ArrayList<>();
      for (final LanguageItemMDTO lang : institution.getFactsheetUrls()) {
        httpList.add(CommonsConverter.convertLanguageItemMDTOToHttpWithOptionalLang(lang));
      }
      res.getMobilityFactsheetUrl().addAll(httpList);
    }

    /** CONTACTS **/
    if (CollectionUtils.isNotEmpty(contacts)) {
      final List<Contact> contactList = new ArrayList<>();
      for (final ContactMDTO contact : contacts) {
        contactList.add(CommonsConverter.convertToContact(contact));
      }
      res.getContact().addAll(contactList);
    }
    return res;
  }

  private InstitutionIdentifiersMDTO obtenerIdentificadorBySCHAC(final List<InstitutionIdentifiersMDTO> ids,
      final String schac) {
    final Optional opt = ids.stream().filter(id -> id.getSchac().equalsIgnoreCase(schac)).findFirst();
    return opt.isPresent() ? (InstitutionIdentifiersMDTO) opt.get() : null;
  }

  @Override
  public InstitutionMDTO convertToInstitutionMDTO(final InstitutionsResponse.Hei hei) {
    final InstitutionMDTO dto = new InstitutionMDTO();

    dto.setInstitutionId(hei.getHeiId());
    dto.setAbbreviation(hei.getAbbreviation());
    dto.setLogoUrl(hei.getLogoUrl());

    final ContactDetailsMDTO primaryContactDetails = new ContactDetailsMDTO();
    if (Objects.nonNull(hei.getMailingAddress())) {
      primaryContactDetails.setMailingAddress(CommonsConverter.convertToFlexibleAddressMDTO(hei.getMailingAddress()));
    }

    if (Objects.nonNull(hei.getStreetAddress())) {
      primaryContactDetails.setStreetAddress(CommonsConverter.convertToFlexibleAddressMDTO(hei.getStreetAddress()));
    }
    dto.setPrimaryContactDetails(primaryContactDetails);

    /** NAMES **/
    final List<LanguageItemMDTO> names = new ArrayList<>();
    for (final StringWithOptionalLang strOptionalLang : hei.getName()) {
      names.add(CommonsConverter.convertToLanguageItem(strOptionalLang));
    }
    dto.setName(names);

    /** WEBSITE URL */
    final List<LanguageItemMDTO> urls = new ArrayList<>();
    for (final HTTPWithOptionalLang httpOpt : hei.getWebsiteUrl()) {
      urls.add(CommonsConverter.convertToLanguageItemMDTO(httpOpt));
    }
    dto.getPrimaryContactDetails().setUrl(urls);

    /* MOBILITY FACTSHEET URL */
    if (!CollectionUtils.isEmpty(hei.getMobilityFactsheetUrl())) {
      final List<LanguageItemMDTO> fUrls = new ArrayList<>();
      for (final HTTPWithOptionalLang httpOpt : hei.getMobilityFactsheetUrl()) {
        fUrls.add(CommonsConverter.convertToLanguageItemMDTO(httpOpt));
      }

      dto.setFactsheetUrls(fUrls);
    }

    return dto;
  }

  @Override
  public List<ContactMDTO> convertToContactMDTOList(final List<Contact> contacts, final String institutionId) {
    final List<ContactMDTO> res = new ArrayList<>();

    for (final Contact contact : contacts) {
      final ContactMDTO mdto = CommonsConverter.convertToContactMDTO(contact);
      mdto.setInstitutionId(institutionId);
      res.add(mdto);
    }

    return res;
  }
}
