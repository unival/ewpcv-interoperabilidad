package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.model.organization.LanguageItem;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.course.AcademicYearMDTO;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.SignatureMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.MultilineStringWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasGetResponse;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.MobilityInstitution;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.Signature;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.Student;
import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;
import eu.erasmuswithoutpaper.api.types.contact.Contact;
import eu.erasmuswithoutpaper.api.types.phonenumber.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.security.Init;
import org.apache.xml.security.c14n.Canonicalizer;

import javax.xml.bind.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.google.common.base.Strings;

/**
 * The type Commons converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
public class CommonsConverter {
  public static final String IIAS_GET_RESPONSE_NAMESPACE =
      "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v6/endpoints/get-response.xsd";

  public static Contact convertToContact(final ContactMDTO contact) {

    Contact converted = null;
    if ( Objects.nonNull( contact ) && Objects.nonNull( contact.getName() ) && !contact.getName().isEmpty() ) {
        converted = new Contact();
        converted.getContactName()
					.addAll( contact.getName().stream().map( CommonsConverter::convertToStringWithOptionalLang )
							.filter( Objects::nonNull ).collect( Collectors.toList() ) );
        if ( converted.getContactName().isEmpty() ) {
          return null;
        }

        if ( Objects.nonNull( contact.getPerson() ) ) {
          if ( Objects.nonNull( contact.getPerson().getFirstNames() ) ) {
            final StringWithOptionalLang personGivenName = new StringWithOptionalLang();
            personGivenName.setValue(contact.getPerson().getFirstNames());
            converted.getPersonGivenNames().add(personGivenName);
          }
          if (Objects.nonNull(contact.getPerson().getLastName())) {
            final StringWithOptionalLang personFamilyName = new StringWithOptionalLang();
            personFamilyName.setValue(contact.getPerson().getLastName());
            converted.getPersonFamilyName().add(personFamilyName);
          }
          if ( Objects.nonNull(contact.getPerson().getGender())) {
            converted.setPersonGender(contact.getPerson().getGender().value());
          }
        }

        if (Objects.nonNull(contact.getRole())) {
            final MultilineStringWithOptionalLang roleDescription = new MultilineStringWithOptionalLang();
            roleDescription.setValue(contact.getRole());
            converted.getRoleDescription().add(roleDescription);
        }

        if (Objects.nonNull(contact.getContactDetails())) {
            converted.getPhoneNumber().add(
                    CommonsConverter.convertToPhoneNumber(contact.getContactDetails().getPhoneNumber()));
            converted.getFaxNumber()
                    .add(CommonsConverter.convertToPhoneNumber(contact.getContactDetails().getFaxNumber()));
            converted.getEmail().addAll(contact.getContactDetails().getEmail());
            converted.setStreetAddress(
                    CommonsConverter.convertToFlexibleAddress(contact.getContactDetails().getStreetAddress()));
            converted.setMailingAddress(
                    CommonsConverter.convertToFlexibleAddress(contact.getContactDetails().getMailingAddress()));
        }
    }
    return converted;
  }

  public static PhoneNumber convertToPhoneNumber(final PhoneNumberMDTO phoneNumber) {
    PhoneNumber converted = null;
    if (Objects.nonNull(phoneNumber) && (Objects.nonNull(phoneNumber.getE164()) || Objects.nonNull(phoneNumber.getExtensionNumber())
                    || Objects.nonNull(phoneNumber.getOtherFormat()))) {
        converted = new PhoneNumber();
        converted.setE164(StringUtils.replace(phoneNumber.getE164(), " ", ""));
        converted.setExt(StringUtils.replace(phoneNumber.getExtensionNumber(), "", ""));
        converted.setOtherFormat(StringUtils.replace(phoneNumber.getOtherFormat(), "", ""));
    }
    return converted;
  }

  public static PhoneNumberMDTO convertToPhoneNumberMDTO(final PhoneNumber phoneNumber) {
    PhoneNumberMDTO converted = null;
    if (Objects.nonNull(phoneNumber)) {
      converted = new PhoneNumberMDTO();
      converted.setE164(phoneNumber.getE164());
      converted.setExtensionNumber(phoneNumber.getExt() != null ? phoneNumber.getExt().toString() : null);
      converted.setOtherFormat(phoneNumber.getOtherFormat());
    }
    return converted;
  }

  public static StringWithOptionalLang convertToStringWithOptionalLang(final LanguageItemMDTO languageItem) {
    StringWithOptionalLang converted = null;
    if (Objects.nonNull(languageItem) && StringUtils.isNotBlank(languageItem.getText())) {
      converted = new StringWithOptionalLang();
      converted.setValue(languageItem.getText());
      converted.setLang(languageItem.getLang());
    }
    return converted;
  }

  /**
   * Convierte el mdto al objeto xml. Hay preferencia sobre el formato avanzado al addressLine, en el caso de que el
   * formato avanzado esté completo y también esté informado el addressLine.
   *
   * @param streetAddress Dirección a convertir
   * @return Dirección convertida
   */
  public static FlexibleAddress convertToFlexibleAddress(final FlexibleAddressMDTO streetAddress) {
    FlexibleAddress converted = null;
    if (Objects.nonNull(streetAddress)) {
      converted = new FlexibleAddress();
      converted.getRecipientName().addAll(streetAddress.getRecipientName());
      converted.setBuildingNumber(streetAddress.getBuildingNumber());
      converted.setBuildingName(streetAddress.getBuildingName());
      converted.setStreetName(streetAddress.getStreetName());
      converted.setUnit(streetAddress.getUnit());
      converted.setFloor(streetAddress.getFloor());
      converted.setPostOfficeBox(streetAddress.getPostOfficeBox());
      converted.getDeliveryPointCode().addAll(streetAddress.getDeliveryPointCode());
      /*
       * Si no tiene ni el campo streetName ni el campo deliveryPointCode del formato avanzado de dirección, se devuelve
       * el addressLine, dejando los campos del formato avanzado a null. Hay preferencia por el formato avanzado. Mirar
       * en caso de duda el xsd de ewp-specs-types-address
       */
      if (StringUtils.isBlank(converted.getStreetName()) && CollectionUtils.isEmpty(converted.getDeliveryPointCode())) {
        converted.getAddressLine().addAll(streetAddress.getAddressLine());
        blankFlexibleAddressAdvancedFormat(converted);
      }

      converted.setPostalCode(streetAddress.getPostalCode());
      converted.setLocality(streetAddress.getLocality());
      converted.setRegion(streetAddress.getRegion());
      converted.setCountry(streetAddress.getCountry());
    }
    return converted;
  }

  private static void blankFlexibleAddressAdvancedFormat(final FlexibleAddress flexibleAddress) {
    flexibleAddress.setBuildingNumber(null);
    flexibleAddress.setBuildingName(null);
    flexibleAddress.setStreetName(null);
    flexibleAddress.setUnit(null);
    flexibleAddress.setFloor(null);
    flexibleAddress.setPostOfficeBox(null);
    flexibleAddress.getDeliveryPointCode().clear();
  }

  public static XMLGregorianCalendar convertToGregorianCalendarWithTime(final Date date) {
    final String formatter = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    final DateFormat format = new SimpleDateFormat(formatter);
    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
    } catch (final DatatypeConfigurationException e) {
      log.error("Parsing date to XMLGregorianCalendar error");
    }
    return null;
  }

  public static XMLGregorianCalendar convertToGregorianCalendar(final Date date) {
    final String formatter = "yyyy-MM-dd";
    final DateFormat format = new SimpleDateFormat(formatter);

    if (Objects.isNull(date)) {
      return null;
    }

    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
    } catch (final DatatypeConfigurationException e) {
      log.error("Parsing date to XMLGregorianCalendar error");
    }
    return null;
  }

  public static XMLGregorianCalendar convertToGregorianCalendarYearMonth(final Date date) throws EwpConverterException {
    final String formatter = "yyyy-MM";
    final DateFormat format = new SimpleDateFormat(formatter);

    if (Objects.isNull(date)) {
      return null;
    }

    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
    } catch (final DatatypeConfigurationException e) {
      log.error("Parsing date to XMLGregorianCalendar error");
      throw new EwpConverterException("Parsing date to XMLGregorianCalendar error");
    }
  }

  public static XMLGregorianCalendar convertToGregorianCalendarMonthDay(final Date date) throws EwpConverterException {
    final String formatter = "--MM-dd";
    final DateFormat format = new SimpleDateFormat(formatter);
    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
    } catch (final DatatypeConfigurationException e) {
      log.error("Parsing date to XMLGregorianCalendar error");
      throw new EwpConverterException("Parsing date to XMLGregorianCalendar error");
    }
  }

  public static String generateCooperationConditionsHash(final IiasGetResponse.Iia.CooperationConditions cc) {
    Init.init();
    String digestHeader = StringUtils.EMPTY;
    try {
      final byte[] bodyBytes = coopConditionClassToBytes(cc, "UTF-8");

      final Canonicalizer canon = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
      final byte[] canonXmlBytes = canon.canonicalize(bodyBytes);

      final byte[] digest = MessageDigest.getInstance("SHA-256").digest(canonXmlBytes);

      digestHeader = DatatypeConverter.printHexBinary(digest).toLowerCase(Locale.ENGLISH);

    } catch (final Exception e) {
      log.error(e.getMessage());
      e.printStackTrace();
    }
    return digestHeader;
  }

  private static byte[] coopConditionClassToBytes(final Object clase, final String charset) throws JAXBException {
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    final JAXBContext jaxbContext = JAXBContext.newInstance(clase.getClass());
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
    jaxbMarshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, charset);
    jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    final QName qName = new QName(IIAS_GET_RESPONSE_NAMESPACE, "cooperation-conditions");
    final JAXBElement<Object> root = new JAXBElement<>(qName, (Class<Object>) clase.getClass(), clase);
    jaxbMarshaller.marshal(root, baos);
    return baos.toByteArray();
  }

  public static Student toStudent(final MobilityParticipantMDTO mobilityParticipantMDTO) {
    final Student converted = new Student();
    converted.setGivenNames(mobilityParticipantMDTO.getContactDetails().getPerson().getFirstNames());
    converted.setFamilyName(mobilityParticipantMDTO.getContactDetails().getPerson().getLastName());
    converted.setGlobalId(mobilityParticipantMDTO.getGlobalId());
    converted.setBirthDate(CommonsConverter
        .convertToGregorianCalendar(mobilityParticipantMDTO.getContactDetails().getPerson().getBirthDate()));
    converted.setCitizenship(mobilityParticipantMDTO.getContactDetails().getPerson().getCountryCode());
    converted.setGender(mobilityParticipantMDTO.getContactDetails().getPerson().getGender().value());
    if (CollectionUtils.isNotEmpty(mobilityParticipantMDTO.getContactDetails().getContactDetails().getEmail())) {
      converted.setEmail(mobilityParticipantMDTO.getContactDetails().getContactDetails().getEmail().get(0));
    }

    return converted;
  }

  public static Student toStudent(final ContactMDTO mobilityParticipantMDTO) {
    Student converted = null;
    if (Objects.nonNull(mobilityParticipantMDTO)) {
      converted = new Student();
      converted.setGivenNames(mobilityParticipantMDTO.getPerson().getFirstNames());
      converted.setFamilyName(mobilityParticipantMDTO.getPerson().getLastName());
      converted.setGlobalId(mobilityParticipantMDTO.getId());
      converted.setBirthDate(
          CommonsConverter.convertToGregorianCalendar(mobilityParticipantMDTO.getPerson().getBirthDate()));
      converted.setCitizenship(mobilityParticipantMDTO.getPerson().getCountryCode());
      converted.setGender(mobilityParticipantMDTO.getPerson().getGender().value());
      if (Objects.nonNull(mobilityParticipantMDTO.getContactDetails()) && CollectionUtils.isNotEmpty(mobilityParticipantMDTO.getContactDetails().getEmail())) {
        converted.setEmail(mobilityParticipantMDTO.getContactDetails().getEmail().get(0));
      }
    }
    return converted;
  }

  public static ContactMDTO convertToContactMDTO(final Contact contact) {
    ContactMDTO converted = null;
    //si nos llega un contancto sin datos devolvemos un null
    if(Objects.isNull(contact) || (CollectionUtils.isEmpty(contact.getPersonGivenNames()) &&
            CollectionUtils.isNotEmpty(contact.getPersonFamilyName()) &&
            Objects.isNull(contact.getPersonGender()))){
      return null;
    }else{
      converted = new ContactMDTO();
      if (Objects.nonNull(contact.getPersonGivenNames()) && !contact.getPersonGivenNames().isEmpty()
              && Objects.nonNull(contact.getPersonFamilyName())) {
        final PersonMDTO person = new PersonMDTO();
        // TODO: Pueden haber mas de un nombre/apellido dto es un string
        person.setFirstNames(
                CollectionUtils.isNotEmpty(contact.getPersonGivenNames()) ? contact.getPersonGivenNames().get(0).getValue()
                        : null);
        person.setLastName(
                CollectionUtils.isNotEmpty(contact.getPersonFamilyName()) ? contact.getPersonFamilyName().get(0).getValue()
                        : null);
        person.setGender(Gender.getGenderByValue(contact.getPersonGender()));
        converted.setPerson(person);
      }
      // Pueden haber mas de una descripcion de rol pero en el dto es un
      // string
      if (Objects.nonNull(contact.getRoleDescription()) && !contact.getRoleDescription().isEmpty()) {
        converted.setRole(contact.getRoleDescription().get(0).getValue());
      }
      converted.setName(
              contact.getContactName().stream().map(CommonsConverter::convertToLanguageItem).collect(Collectors.toList()));
      final ContactDetailsMDTO contactDetails = convertToContactDetailsMDTO(contact);
      converted.setContactDetails(contactDetails);
      return converted;
    }
  }

  private static ContactDetailsMDTO convertToContactDetailsMDTO(final Contact contact) {
    // Ahora mismo solo cogemos el primero de todos los numeros
    final ContactDetailsMDTO contactDetails = new ContactDetailsMDTO();
    contactDetails.setPhoneNumber(CommonsConverter.convertToPhoneNumberMDTO(contact.getPhoneNumber()));
    contactDetails.setMailingAddress(Objects.nonNull(contact.getMailingAddress())
        ? CommonsConverter.convertToFlexibleAddressMDTO(contact.getMailingAddress())
        : null);
    contactDetails.setStreetAddress(Objects.nonNull(contact.getStreetAddress())
        ? CommonsConverter.convertToFlexibleAddressMDTO(contact.getStreetAddress())
        : null);
    contactDetails.setEmail(contact.getEmail());

    // Podemos tener varios numeros de telefono pero solo hay uno en el MDTO
    contactDetails.setPhoneNumber(
        CollectionUtils.isNotEmpty(contact.getPhoneNumber()) ? convertToPhoneNumberMDTO(contact.getPhoneNumber())
            : null);
    contactDetails.setFaxNumber(
        CollectionUtils.isNotEmpty(contact.getFaxNumber()) ? convertToPhoneNumberMDTO(contact.getFaxNumber()) : null);
    return contactDetails;
  }

  public static Date convertToDate(final XMLGregorianCalendar signingDate) {
    return Objects.nonNull(signingDate) ? signingDate.toGregorianCalendar().getTime() : null;
  }

  public static LanguageItemMDTO convertToLanguageItem(final StringWithOptionalLang string) {
    LanguageItemMDTO converted = null;
    if (Objects.nonNull(string)) {
      converted = new LanguageItemMDTO();
      converted.setText(string.getValue());
      converted.setLang(string.getLang());
    }
    return converted;
  }

  private static PhoneNumberMDTO convertToPhoneNumberMDTO(final List<PhoneNumber> phoneNumber) {
    if (!CollectionUtils.isEmpty(phoneNumber) && Objects.nonNull(phoneNumber.get(0))) {
      final PhoneNumberMDTO number = new PhoneNumberMDTO();
      final PhoneNumber p = phoneNumber.get(0);
      number.setE164(p.getE164());
      number.setExtensionNumber(Objects.nonNull(p.getExt()) ? p.getExt().toString() : null);
      number.setOtherFormat(p.getOtherFormat());
      return number;
    }
    return null;
  }

  public static FlexibleAddressMDTO convertToFlexibleAddressMDTO(final FlexibleAddress mailingAddress) {
    final FlexibleAddressMDTO converted = new FlexibleAddressMDTO();
    converted.setBuildingNumber(mailingAddress.getBuildingNumber());
    converted.setBuildingName(mailingAddress.getBuildingName());
    converted.setStreetName(mailingAddress.getStreetName());
    converted.setUnit(mailingAddress.getUnit());
    converted.setFloor(mailingAddress.getFloor());
    converted.setPostOfficeBox(mailingAddress.getPostOfficeBox());
    converted.setDeliveryPointCode(
        mailingAddress.getDeliveryPointCode().stream().map(Object::toString).collect(Collectors.toList()));
    converted.setAddressLine(mailingAddress.getAddressLine());
    converted.setLocality(mailingAddress.getLocality());
    converted.setPostalCode(mailingAddress.getPostalCode());
    converted.setRecipientName(mailingAddress.getRecipientName());
    converted.setRegion(mailingAddress.getRegion());
    converted.setCountry(mailingAddress.getCountry());
    return converted;
  }

  public static MobilityParticipantMDTO convertStudentIntoMobilityParticipantMDTO(final Student student) {
    final MobilityParticipantMDTO converted = new MobilityParticipantMDTO();
    final ContactMDTO contact = new ContactMDTO();
    final PersonMDTO person = new PersonMDTO();

    if (Objects.nonNull(student.getBirthDate())) {
      person.setBirthDate(student.getBirthDate().toGregorianCalendar().getTime());
    }
    person.setLastName(student.getFamilyName());
    person.setFirstNames(student.getGivenNames());
    person.setCountryCode(student.getCitizenship());
    person.setGender(Gender.getGenderByValue(student.getGender()));

    final ContactDetailsMDTO cd = new ContactDetailsMDTO();
    cd.setEmail(new ArrayList<>());
    cd.getEmail().add(student.getEmail());

    contact.setPerson(person);
    contact.setContactDetails(cd);
    converted.setContactDetails(contact);
    converted.setGlobalId(student.getGlobalId());

    return converted;
  }

  public static List<LanguageSkillMDTO> convertToLanguageSkillsMDTO(
      final List<LearningAgreement.StudentLanguageSkill> languageSkill) {
    final List<LanguageSkillMDTO> res = new ArrayList<>();

    for (final LearningAgreement.StudentLanguageSkill studentLanguageSkill : languageSkill) {
      final LanguageSkillMDTO langSkill = new LanguageSkillMDTO();

      langSkill.setLanguage(studentLanguageSkill.getLanguage());
      langSkill.setCefrLevel(studentLanguageSkill.getCefrLevel());

      res.add(langSkill);
    }

    return res;
  }

  public static OrganizationUnitMDTO convertMovilityInstitutionIntoOrgUnit(final MobilityInstitution sendingHei) {
    final OrganizationUnitMDTO oUnit = new OrganizationUnitMDTO();

    final List<LanguageItemMDTO> langItems = new ArrayList<>();
    final LanguageItemMDTO langItem = new LanguageItemMDTO();
    langItem.setText(sendingHei.getOunitName());
    langItem.setLang(LanguageItem.SPANISH);
    langItems.add(langItem);

    oUnit.setId(sendingHei.getOunitId());
    oUnit.setName(langItems);

    return oUnit;
  }

  public static ContactMDTO convertContactPersonToContactMDTO(final MobilityInstitution.ContactPerson contactPerson) {
    final ContactMDTO converted = new ContactMDTO();

    final PersonMDTO person = new PersonMDTO();

    person.setLastName(contactPerson.getFamilyName());
    person.setFirstNames(contactPerson.getGivenNames());
    converted.setPerson(person);

    final ContactDetailsMDTO cd = new ContactDetailsMDTO();
    cd.setEmail(new ArrayList<>());
    cd.getEmail().add(contactPerson.getEmail());
    cd.setPhoneNumber(convertToPhoneNumberMDTO(Arrays.asList(contactPerson.getPhoneNumber())));

    converted.setContactDetails(cd);

    return converted;
  }

  public static SignatureMDTO convertSignatureToSignatureMDTO(final Signature signature) {
    final SignatureMDTO signatureMDTO = new SignatureMDTO();

    signatureMDTO.setSignerApp(signature.getSignerApp());
    signatureMDTO.setSignerEmail(signature.getSignerEmail());
    signatureMDTO.setSignerName(signature.getSignerName());
    signatureMDTO.setSignerPosition(signature.getSignerPosition());
    signatureMDTO.setTimestamp(convertToDate(signature.getTimestamp()));

    return signatureMDTO;
  }

  public static HTTPWithOptionalLang convertLanguageItemMDTOToHttpWithOptionalLang(
      final LanguageItemMDTO languageItemMDTO) {
    final HTTPWithOptionalLang http = new HTTPWithOptionalLang();

    http.setValue(languageItemMDTO.getText());
    http.setLang(languageItemMDTO.getLang());

    return http;
  }

  public static LanguageItemMDTO convertToLanguageItemMDTO(final HTTPWithOptionalLang httpOpt) {
    final LanguageItemMDTO res = new LanguageItemMDTO();

    res.setText(httpOpt.getValue());
    res.setLang(httpOpt.getLang());

    return res;
  }

  public static String convertToAcademicTermId(final AcademicTermMDTO academicTerm) {
    final StringBuilder str = new StringBuilder();

    str.append(convertToAcademicYearId(academicTerm.getAcademicYear()));
    str.append("-");
    str.append(academicTerm.getTermNumber());
    str.append("/");
    str.append(academicTerm.getTotalTerms());

    return str.toString();
  }

  public static String convertToAcademicYearId(final AcademicYearMDTO academicYear) {
    final StringBuilder str = new StringBuilder();

    str.append(academicYear.getStartYear());
    str.append("/");
    str.append(academicYear.getEndYear());

    return str.toString();
  }

  public static AcademicTermMDTO convertToAcademicTermMDTO(final String academicTerm) {
    // el academic term nos llega como un String con el siguiente formato:
    // AcademicYearId-TermNumber/NumberOfTerms
    // donde AcademicYearId = año inicio / año fin, por ejemplo: 2008/2009-1/1

    final AcademicTermMDTO mdto = new AcademicTermMDTO();
    final String[] split = academicTerm.split("-");
    mdto.setAcademicYear(convertToAcademicYearMDTO(split[0]));
    final String[] terms = split[1].split("/");
    mdto.setTermNumber(new BigInteger(terms[0]));
    mdto.setTotalTerms(new BigInteger(terms[1]));
    return mdto;
  }

  public static AcademicYearMDTO convertToAcademicYearMDTO(final String years) {
    final AcademicYearMDTO res = new AcademicYearMDTO();
    final String[] anyos = years.split("/");
    res.setStartYear(anyos[0]);
    res.setEndYear(anyos[1]);
    return res;
  }
}
