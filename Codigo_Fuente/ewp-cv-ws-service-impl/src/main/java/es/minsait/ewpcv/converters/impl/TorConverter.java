package es.minsait.ewpcv.converters.impl;


import es.minsait.ewpcv.converter.api.ITorConverter;
import es.minsait.ewpcv.model.course.AttachmentType;
import es.minsait.ewpcv.model.course.LevelType;
import es.minsait.ewpcv.model.course.LoiStatus;
import es.minsait.ewpcv.repository.impl.LearningAgreementDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.*;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.model.organization.PersonMDTO;
import eu.emrex.elmo.*;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;
import eu.europa.cedefop.europass.europass.v2.CountryCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * The type Tor converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class TorConverter implements ITorConverter {

  @Autowired
  private LearningAgreementDAO laDao;

  @Override
  public List<ImobilityTorsGetResponse.Tor> convertToImobilityTor(final List<MobilityMDTO> mobilities) {
    final List<ImobilityTorsGetResponse.Tor> res = new ArrayList<>();

    if (Objects.nonNull(mobilities) && !mobilities.isEmpty()) {
      for (final MobilityMDTO mobility : mobilities) {
        res.add(convertToSingleImobilityTor(mobility));
      }
    }

    return res;
  }

  /**
   * Convierte un MobilityMDTO en ImobilityTorsGetResponse.Tor
   *
   * @param mobility
   * @return ImobilityTorsGetResponse.Tor
   */
  private ImobilityTorsGetResponse.Tor convertToSingleImobilityTor(final MobilityMDTO mobility) {
    final TranscriptOfRecordMDTO mdto = obtenerTorFromMobility(mobility);
    final ImobilityTorsGetResponse.Tor tor = new ImobilityTorsGetResponse.Tor();

    if (Objects.nonNull(mdto)) {
      tor.setOmobilityId(mobility.getId());

      /** Conversion Table **/
      final ImobilityTorsGetResponse.Tor.GradeConversionTable gradeTable =
          new ImobilityTorsGetResponse.Tor.GradeConversionTable();

      gradeTable.getIscedTable().addAll(convertToIscedTableList(mdto.getIscedTable()));
      tor.setGradeConversionTable(gradeTable);

      /***********************/
      /***** INICIO ELMO *****/
      /***********************/
      final Elmo elmo = new Elmo();

      /** Generated date **/
      elmo.setGeneratedDate(CommonsConverter.convertToGregorianCalendarWithTime(mdto.getGeneratedDate()));

      /** Learner **/
      // cogemos la ultima revision del LA que tenga cambios en el alumno, si no la hay usamos el mobilityParticipant
      final LearningAgreementMDTO lastLaRevisionWithChanges = mobility.getLearningAgreement().stream()
          .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision)).orElse(null);

      if (Objects.nonNull(lastLaRevisionWithChanges)
          && Objects.nonNull(lastLaRevisionWithChanges.getModifiedStudentContactId())) {
        elmo.setLearner(personToLearner(lastLaRevisionWithChanges.getModifiedStudentContactId().getPerson()));
      } else {
        elmo.setLearner(personToLearner(mobility.getMobilityParticipantId().getContactDetails().getPerson()));
      }

      /** Reports **/
      final List<Elmo.Report> elmoReports = new ArrayList<>();
      for (final ReportMDTO report : mdto.getReport()) {
        elmoReports.add(convertToElmoReport(mobility, report));
      }
      elmo.getReport().addAll(elmoReports);

      /** Attachments **/
      elmo.getAttachment().addAll(convertToElmoAttachmentList(mdto.getAttachments()));

      /** Extension **/
      elmo.setExtension(convertToCustomExtension(mdto.getExtension()));


      /** Groups **/
      // tenemos que coger los grupos asociados a los Lois
      final Groups groups = new Groups();
      groups.getGroupType().addAll(convertToGroupTypeList(obtenerGrupTypesFromLois(lastLaRevisionWithChanges)));

      elmo.setGroups(groups);

      tor.setElmo(elmo);
      /***********************/
      /******* FIN ELMO ******/
      /***********************/
    }

    return tor;
  }

  private List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable> convertToIscedTableList(
      final List<IscedTableMDTO> iscedTables) {
    final List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable> res = new ArrayList<>();

    for (final IscedTableMDTO isced : iscedTables) {
      final ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable table =
          new ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable();
      table.setIscedCode(isced.getIscedCode());
      table.getGradeFrequency().addAll(convertToGradeFrequencyList(isced.getFrequencies()));
      res.add(table);
    }

    return res;
  }

  private List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency> convertToGradeFrequencyList(
      final List<GradeFrequencyMDTO> frequencies) {
    final List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency> res = new ArrayList<>();

    for (final GradeFrequencyMDTO gradeFreq : frequencies) {
      final ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency grade =
          new ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency();
      grade.setLabel(gradeFreq.getLabel());
      grade.setPercentage(new BigDecimal(gradeFreq.getPercentage()));
      res.add(grade);
    }

    return res;
  }

  /**
   * Convierte un ReportMDTO en Elmo.Report
   *
   * @param mobility
   * @param report
   * @return Elmo.Report
   */
  private Elmo.Report convertToElmoReport(final MobilityMDTO mobility, final ReportMDTO report) {
    final Elmo.Report elmoReport = new Elmo.Report();

    /** Issuer **/
    final Elmo.Report.Issuer issuer = new Elmo.Report.Issuer();
    final Elmo.Report.Issuer.Identifier id = new Elmo.Report.Issuer.Identifier();
    id.setValue(mobility.getReceivingInstitutionId());
    id.setType("schac");
    issuer.getIdentifier().add(id);
    elmoReport.setIssuer(issuer);

    /** Learning Opportunity Specification **/
    final List<LearningOpportunitySpecification> losList = new ArrayList<>();

    // obtenemos los LOS y LOIs a partir de los componentes de la ultima revision de LA de la mobilidad
    // mobility -> LA (ultima revision) -> components -> LOS, LOIs

    final LearningAgreementMDTO lastRevisionLa = mobility.getLearningAgreement().stream()
        .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision)).orElse(null);

    final List<LearningOpportunitySpecificationMDTO> lista = obtenerListaLos(lastRevisionLa);

    final Map<String, LaComponentMDTO> mapLosComponent = obtenerComponentsFromLos(lastRevisionLa);

    for (final LearningOpportunitySpecificationMDTO los : lista) {
      losList.add(convertoToLos(los, mapLosComponent.get(los.getId())));
    }

    elmoReport.getLearningOpportunitySpecification().addAll(losList);

    /** Issue Date **/
    elmoReport.setIssueDate(CommonsConverter.convertToGregorianCalendarWithTime(report.getIssueDate()));

    /** Grading Scheme **/
    // Listado con los identificadores de los gradingSchemes usados en el report
    List<GradingSchemeMDTO> gradingSchemeMDTOS =
        report.getLois().stream().map(LearningOpportunityInstanceMDTO::getGradingScheme).collect(Collectors.toList());

    // eliminamos grading schemes repetidos
    gradingSchemeMDTOS = gradingSchemeMDTOS.stream().filter(Objects::nonNull)
        .collect(collectingAndThen(
            toCollection(() -> new TreeSet<GradingSchemeMDTO>(Comparator.comparing(GradingSchemeMDTO::getId))),
            ArrayList::new));

    elmoReport.getGradingScheme().addAll(convertToGradingScheme(gradingSchemeMDTOS));

    /** Attachments **/
    // Lista con los attachments a los que hara referencia los LOIs asociados a los LOS asociados al report
    final List<Attachment> reportAtt = new ArrayList<>();
    for (final LearningOpportunityInstanceMDTO loi : report.getLois()) {
      reportAtt.addAll(convertToElmoAttachmentList(loi.getAttachments()));
    }
    // tambien pude ser que el report tenga attachments propios que no esten referenciados en los LOIs
    reportAtt.addAll(convertToElmoAttachmentList(report.getAttachments()));

    elmoReport.getAttachment().addAll(reportAtt);

    return elmoReport;
  }

  private Map<String, LaComponentMDTO> obtenerComponentsFromLos(final LearningAgreementMDTO lastRevisionLa) {
    // obtenemos todos los components del LA
    final List<LaComponentMDTO> componentMDTOS = new ArrayList<>();
    componentMDTOS.addAll(lastRevisionLa.getRecognizedLaComponents());
    componentMDTOS.addAll(lastRevisionLa.getBlendedLaComponents());
    componentMDTOS.addAll(lastRevisionLa.getDoctoralLaComponents());
    componentMDTOS.addAll(lastRevisionLa.getStudiedLaComponents());
    componentMDTOS.addAll(lastRevisionLa.getVirtualLaComponents());

    final Map<String, LaComponentMDTO> res = new HashMap<>();

    for (final LaComponentMDTO component : componentMDTOS) {
      if (Objects.nonNull(component.getLosId()) && Objects.nonNull(component.getLosId().getId())) {
        res.put(component.getLosId().getId(), component);
      }
    }
    return res;
  }

  /**
   * Convierte un LearningOpportunityInstanceMDTO en LearningOpportunitySpecification
   *
   * @param dto
   * @param component
   * @return LearningOpportunitySpecification
   */
  private LearningOpportunitySpecification convertoToLos(final LearningOpportunitySpecificationMDTO dto,
      final LaComponentMDTO component) {
    final LearningOpportunitySpecification los = new LearningOpportunitySpecification();

    // usamos como id nuestro id en bbdd
    final LearningOpportunitySpecification.Identifier losId = new LearningOpportunitySpecification.Identifier();
    losId.setType("ewp-los-id");
    losId.setValue(dto.getId());
    los.getIdentifier().addAll(Arrays.asList(losId));

    // names
    los.getTitle().addAll(convertToTokenWithOptionalLangList(dto.getName()));

    // type
    if (Objects.nonNull(dto.getType())) {
      los.setType(dto.getType().name());
    }

    // subject area
    los.setSubjectArea(dto.getSubjectArea());

    // isceed code
    los.setIscedCode(dto.getIscedf());

    // url
    if (Objects.nonNull(dto.getUrl()) && !dto.getUrl().isEmpty()) {
      los.setUrl(dto.getUrl().get(0).getText());
    }

    // description
    los.getDescription().addAll(convertToPlainMultLineString(dto.getDescription()));

    // description html
    los.getDescriptionHtml().addAll(convertToSimpleHTMStringList(dto.getDescription()));

    // LOIs
    // el elemento specifies solo puede tener un LOI por lo que cogemos el primero aunque
    // en la lista de LOIs solo tendría que haber uno
    los.setSpecifies(convertToSpecifies(component));


    // extension
    los.setExtension(convertToCustomExtension(dto.getExtension()));

    return los;
  }

  /**
   * Convierte un learningOpportunityInstanceMDTO en un LearningOpportunitySpecification.Specifies
   *
   * @param component LaComponent del que sacamos el loi
   * @return LearningOpportunitySpecification.Specifies
   */
  private LearningOpportunitySpecification.Specifies convertToSpecifies(final LaComponentMDTO component) {
    final LearningOpportunitySpecification.Specifies specifies = new LearningOpportunitySpecification.Specifies();

    final LearningOpportunityInstanceMDTO loiDTO = component.getLoiId();

    if (Objects.nonNull(loiDTO)) {

      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance loi =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance();

      /** Identificadores **/
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier id =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier();
      id.setType("ewp-loi-id");
      id.setValue(loiDTO.getId());
      loi.getIdentifier().addAll(Arrays.asList(id));

      /** start date **/
      loi.setStart(CommonsConverter.convertToGregorianCalendar(loiDTO.getStartDate()));

      /** end date **/
      loi.setDate(CommonsConverter.convertToGregorianCalendar(loiDTO.getEndDate()));

      /** academic term **/
      loi.setAcademicTerm(convertToAcademicTerm(component.getAcademicTermDisplayName()));

      /** status **/
      if (Objects.nonNull(loiDTO.getStatus())) {
        loi.setStatus(loiDTO.getStatus().value());
      }

      /** grading scheme local **/
      if (Objects.nonNull(loiDTO.getGradingScheme())) {
        loi.setGradingSchemeLocalId(loiDTO.getGradingScheme().getId());
      }

      /** result label **/
      loi.setResultLabel(loiDTO.getResultLabel());

      /** shortened grading **/
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading shortenedGrading =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading();
      shortenedGrading.setPercentageEqual(
          Objects.nonNull(loiDTO.getPercentageEqual()) ? new BigDecimal(loiDTO.getPercentageEqual()) : null);
      shortenedGrading.setPercentageHigher(
          Objects.nonNull(loiDTO.getPercentageHigher()) ? new BigDecimal(loiDTO.getPercentageHigher()) : null);
      shortenedGrading.setPercentageLower(
          Objects.nonNull(loiDTO.getPercentageLower()) ? new BigDecimal(loiDTO.getPercentageLower()) : null);
      loi.setShortenedGrading(shortenedGrading);

      /** Result distribution **/
      if (Objects.nonNull(loiDTO.getResultDistribution())) {
        final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution resultDistribution =
            new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution();

        resultDistribution.getDescription()
            .addAll(convertToPlainMultLineString(loiDTO.getResultDistribution().getDescription()));
        resultDistribution.getCategory()
            .addAll(convertToCatrgoryList(loiDTO.getResultDistribution().getResultDistributionCategory()));
        loi.setResultDistribution(resultDistribution);
      }

      /** credits **/
      loi.getCredit().addAll(convertToCreditList(component.getCredits()));

      /** level **/
      loi.getLevel().addAll(convertToLevelList(loiDTO.getLevels()));

      /** language of instruction **/
      loi.setLanguageOfInstruction(loiDTO.getLanguageOfInstruction());

      /** engagementHous **/
      loi.setEngagementHours(loiDTO.getEngagementHours());

      /** attachments **/
      loi.setAttachments(convertToAttachment(loiDTO.getAttachments()));

      /** grouping **/ // LOIGROUPING -> groupTypes
      if (Objects.nonNull(loiDTO.getLoiGrouping())) {
        final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping grouping =
            new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping();

        if (Objects.nonNull(loiDTO.getLoiGrouping().getGroupType())) {
          grouping.setTyperef(loiDTO.getLoiGrouping().getGroupType().getId());
          if (Objects.nonNull(loiDTO.getLoiGrouping().getGroup())) {
            grouping.setIdref(loiDTO.getLoiGrouping().getGroup().getId());
          }
        }
        loi.setGrouping(grouping);
      }

      /** extension **/
      loi.setExtension(convertToCustomExtension(loiDTO.getExtension()));

      /** Diploma suplement **/
      loi.setDiplomaSupplement(convertToDiplomaSuplement(loiDTO.getDiploma()));

      /** LOI **/
      specifies.setLearningOpportunityInstance(loi);
    }

    return specifies;
  }

  private List<Groups.GroupType> convertToGroupTypeList(final List<GroupTypeMDTO> groupTypes) {
    final List<Groups.GroupType> res = new ArrayList<>();

    for (final GroupTypeMDTO groupType : groupTypes) {
      final Groups.GroupType type = new Groups.GroupType();
      type.setTitle(convertToTokenWithOptionalLang(groupType.getTitle()));
      type.getGroup().addAll(convertToGroupList(groupType.getGroups()));
      type.setId(groupType.getId());
      res.add(type);
    }
    return res;
  }

  private List<Groups.GroupType.Group> convertToGroupList(final List<GroupMDTO> groups) {
    final List<Groups.GroupType.Group> res = new ArrayList<>();

    for (final GroupMDTO group : groups) {
      final Groups.GroupType.Group g = new Groups.GroupType.Group();
      g.setTitle(convertToTokenWithOptionalLang(group.getTitle()));
      g.setSortingKey(group.getSortingKey());
      g.setId(group.getId());
      res.add(g);
    }

    return res;
  }

  private List<GroupTypeMDTO> obtenerGrupTypesFromLois(final LearningAgreementMDTO learningAgreement) {
    final List<GroupTypeMDTO> aux = new ArrayList<>();

    final List<LearningOpportunityInstanceMDTO> lois = new ArrayList<>();
    for (final LearningOpportunitySpecificationMDTO los : obtenerListaLos(learningAgreement)) {
      lois.addAll(obtenerListaLois(los.getId(), learningAgreement));
    }
    lois.stream().filter(loi -> Objects.nonNull(loi.getLoiGrouping()))
        .forEach(loi -> aux.add(loi.getLoiGrouping().getGroupType()));

    // nos aseguramos que no tenemos grupos repetidos
    return aux.stream().filter(Objects::nonNull).collect(collectingAndThen(
        toCollection(() -> new TreeSet<GroupTypeMDTO>(Comparator.comparing(GroupTypeMDTO::getId))), ArrayList::new));
  }

  private Elmo.Learner personToLearner(final PersonMDTO person) {
    final Elmo.Learner learner = new Elmo.Learner();
    learner.setBday(CommonsConverter.convertToGregorianCalendar(person.getBirthDate()));
    learner.setBirthName(person.getFirstNames() + " " + person.getLastName());
    learner.setGivenNames(person.getFirstNames());
    learner.setFamilyName(person.getLastName());
    if (Objects.nonNull(person.getCountryCode())) {
      learner.setCitizenship(CountryCode.valueOf(person.getCountryCode().toUpperCase()));
    }
    if (Objects.nonNull(person.getGender())) {
      learner.setGender(person.getGender().value());
    }
    return learner;
  }

  private TranscriptOfRecordMDTO obtenerTorFromMobility(final MobilityMDTO mobility) {
    if (Objects.nonNull(mobility.getLearningAgreement()) && !mobility.getLearningAgreement().isEmpty()) {
      // cogemos la ultima revision de los LA y devolvemos su ToR
      final Optional<LearningAgreementMDTO> opt = mobility.getLearningAgreement().stream()
          .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision));

      return opt.map(LearningAgreementMDTO::getTranscriptOfRecord).orElse(null);
    } else {
      log.info("La mobilidad no tiene Learning Agreements");
      return null;
    }
  }

  private Map<String, List<LearningOpportunityInstanceMDTO>> obtenerLoisFromLos(
      final LearningAgreementMDTO learningAgreementMDTO, final List<LearningOpportunitySpecificationMDTO> lista) {
    final Map<String, List<LearningOpportunityInstanceMDTO>> resMap = new HashMap<>();

    for (final LearningOpportunitySpecificationMDTO los : lista) {
      resMap.put(los.getId(), obtenerListaLois(los.getId(), learningAgreementMDTO));
    }

    return resMap;
  }


  private List<Elmo.Report.GradingScheme> convertToGradingScheme(final List<GradingSchemeMDTO> gradingSchemeMDTOS) {
    final List<Elmo.Report.GradingScheme> res = new ArrayList<>();

    for (final GradingSchemeMDTO grading : gradingSchemeMDTOS) {
      final Elmo.Report.GradingScheme elmoGrading = new Elmo.Report.GradingScheme();
      elmoGrading.setLocalId(grading.getId());
      elmoGrading.getDescription().addAll(convertToPlainMultLineString(grading.getDescription()));
      res.add(elmoGrading);
    }

    return res;
  }

  private List<LearningOpportunitySpecificationMDTO> obtenerListaLos(
      final LearningAgreementMDTO learningAgreementMDTO) {
    final List<LearningOpportunitySpecificationMDTO> los = new ArrayList<>();

    if (Objects.nonNull(learningAgreementMDTO)) {
      if (Objects.nonNull(learningAgreementMDTO.getStudiedLaComponents())
          && !learningAgreementMDTO.getStudiedLaComponents().isEmpty()) {
        los.addAll(learningAgreementMDTO.getStudiedLaComponents().stream().map(LaComponentMDTO::getLosId)
            .collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getBlendedLaComponents())
          && !learningAgreementMDTO.getBlendedLaComponents().isEmpty()) {
        los.addAll(learningAgreementMDTO.getBlendedLaComponents().stream().map(LaComponentMDTO::getLosId)
            .collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getRecognizedLaComponents())
          && !learningAgreementMDTO.getRecognizedLaComponents().isEmpty()) {
        los.addAll(learningAgreementMDTO.getRecognizedLaComponents().stream().map(LaComponentMDTO::getLosId)
            .collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getDoctoralLaComponents())
          && !learningAgreementMDTO.getDoctoralLaComponents().isEmpty()) {
        los.addAll(learningAgreementMDTO.getDoctoralLaComponents().stream().map(LaComponentMDTO::getLosId)
            .collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getVirtualLaComponents())
          && !learningAgreementMDTO.getVirtualLaComponents().isEmpty()) {
        los.addAll(learningAgreementMDTO.getVirtualLaComponents().stream().map(LaComponentMDTO::getLosId)
            .collect(Collectors.toList()));
      }
    }

    // eliminamos los LOS repetidos
    return los.stream().filter(Objects::nonNull)
        .collect(collectingAndThen(toCollection(() -> new TreeSet<LearningOpportunitySpecificationMDTO>(
            Comparator.comparing(LearningOpportunitySpecificationMDTO::getId))), ArrayList::new));

  }

  /**
   * Obtiene la lista de LOIs asocidados a un LOS
   *
   * @param learningAgreementMDTO
   * @return List<LearningOpportunityInstanceMDTO>
   */
  private List<LearningOpportunityInstanceMDTO> obtenerListaLois(final String losId,
      final LearningAgreementMDTO learningAgreementMDTO) {
    final List<LearningOpportunityInstanceMDTO> lois = new ArrayList<>();

    if (Objects.nonNull(learningAgreementMDTO)) {
      if (Objects.nonNull(learningAgreementMDTO.getStudiedLaComponents())
          && !learningAgreementMDTO.getStudiedLaComponents().isEmpty()) {
        lois.addAll(
            learningAgreementMDTO.getStudiedLaComponents().stream().filter(c -> isLosAsignedToLaComponent(c, losId))
                .map(LaComponentMDTO::getLoiId).filter(Objects::nonNull).collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getBlendedLaComponents())
          && !learningAgreementMDTO.getBlendedLaComponents().isEmpty()) {
        lois.addAll(
            learningAgreementMDTO.getBlendedLaComponents().stream().filter(c -> isLosAsignedToLaComponent(c, losId))
                .map(LaComponentMDTO::getLoiId).filter(Objects::nonNull).collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getRecognizedLaComponents())
          && !learningAgreementMDTO.getRecognizedLaComponents().isEmpty()) {
        lois.addAll(
            learningAgreementMDTO.getRecognizedLaComponents().stream().filter(c -> isLosAsignedToLaComponent(c, losId))
                .map(LaComponentMDTO::getLoiId).filter(Objects::nonNull).collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getDoctoralLaComponents())
          && !learningAgreementMDTO.getDoctoralLaComponents().isEmpty()) {
        lois.addAll(
            learningAgreementMDTO.getDoctoralLaComponents().stream().filter(c -> isLosAsignedToLaComponent(c, losId))
                .map(LaComponentMDTO::getLoiId).filter(Objects::nonNull).collect(Collectors.toList()));
      }

      if (Objects.nonNull(learningAgreementMDTO.getVirtualLaComponents())
          && !learningAgreementMDTO.getVirtualLaComponents().isEmpty()) {
        lois.addAll(learningAgreementMDTO.getVirtualLaComponents().stream().map(LaComponentMDTO::getLoiId)
            .filter(Objects::nonNull).collect(Collectors.toList()));
      }
    }

    // eliminamos los lois repetidos
    return lois.stream().filter(Objects::nonNull)
        .collect(collectingAndThen(toCollection(() -> new TreeSet<LearningOpportunityInstanceMDTO>(
            Comparator.comparing(LearningOpportunityInstanceMDTO::getId))), ArrayList::new));
  }

  private boolean isLosAsignedToLaComponent(final LaComponentMDTO component, final String losId) {
    if (Objects.nonNull(component.getLosId())) {
      return component.getLosId().getId().equalsIgnoreCase(losId);
    } else {
      return Boolean.FALSE;
    }
  }

  private XmlExtensionContent convertToCustomExtension(final byte[] blob) {
    final XmlExtensionContent extensionContent = new XmlExtensionContent();

    extensionContent.setExtensionContent(Objects.nonNull(blob) ? new String(base64EncodeIfNot(blob)) : null);

    return extensionContent;
  }

  private DiplomaSupplement convertToDiplomaSuplement(final DiplomaMDTO diploma) {
    final DiplomaSupplement res = new DiplomaSupplement();

    if (Objects.nonNull(diploma)) {
      res.setIssueDate(CommonsConverter.convertToGregorianCalendar(diploma.getIssueDate()));
      res.setVersion(diploma.getDiplomaVersion().toString());
      res.setIntroduction(diploma.getIntroduction());
      res.setSignature(diploma.getSignature());
      res.getSection().addAll(convertToDiplomaSectionList(diploma.getSection()));
    }

    return res;
  }

  private List<DiplomaSupplementSection> convertToDiplomaSectionList(final List<DiplomaSectionMDTO> section) {
    final List<DiplomaSupplementSection> res = new ArrayList<>();

    for (final DiplomaSectionMDTO diplomaSection : section) {
      final DiplomaSupplementSection s = new DiplomaSupplementSection();
      final DiplomaSupplementSection.Content content = new DiplomaSupplementSection.Content();
      content.setType("text/plain");
      if (Objects.nonNull(diplomaSection.getContent())) {
        content.setValue(new String(base64EncodeIfNot(diplomaSection.getContent())));
      }
      s.setContent(content);
      s.setNumber(Objects.nonNull(diplomaSection.getNumber()) ? diplomaSection.getNumber().toString() : "");
      s.setTitle(diplomaSection.getTitle());

      if (Objects.nonNull(diplomaSection.getSections()) && !diplomaSection.getSections().isEmpty()) {
        s.getSection().addAll(convertToDiplomaSectionList(diplomaSection.getSections()));
      }

      if (Objects.nonNull(diplomaSection.getAdditionalInfo()) && !diplomaSection.getAdditionalInfo().isEmpty()) {
        s.getAdditionalInformation().addAll(diplomaSection.getAdditionalInfo().stream()
            .map(DiplomaAdditionalInfoMDTO::getAdditionalInfo).collect(Collectors.toList()));
      }

      if (Objects.nonNull(diplomaSection.getAttachments()) && !diplomaSection.getAttachments().isEmpty()) {
        s.getAttachment().addAll(convertToElmoAttachmentList(diplomaSection.getAttachments()));
      }

      res.add(s);
    }

    return res;
  }

  private LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments convertToAttachment(
      final List<AttachmentMDTO> attachments) {
    final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments res =
        new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments();

    res.getRef().addAll(attachments.stream().map(AttachmentMDTO::getId).collect(Collectors.toList()));

    return res;
  }

  private List<Attachment> convertToElmoAttachmentList(final List<AttachmentMDTO> attachments) {
    final List<Attachment> res = new ArrayList<>();

    for (final AttachmentMDTO attachment : attachments) {
      final Attachment a = new Attachment();
      a.setType(attachment.getType().value());
      a.setExtension(convertToCustomExtension(attachment.getExtension()));
      a.getDescription().addAll(convertToPlainMultLineString(attachment.getDescription()));
      a.getTitle().addAll(convertToTokenWithOptionalLangList(attachment.getTitle()));
      a.getContent().addAll(convertContentToTokenWithOptionalLangList(attachment.getContents()));

      final Attachment.Identifier id = new Attachment.Identifier();
      id.setType("internal");
      id.setValue(attachment.getId());
      a.getIdentifier().add(id);

      res.add(a);
    }

    return res;
  }

  private List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level> convertToLevelList(
      final List<LevelMDTO> levels) {
    final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level> res = new ArrayList<>();

    for (final LevelMDTO level : levels) {
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level l =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level();
      l.setType(level.getType().value());
      l.setValue(level.getValue());
      l.setDescription(convertToPlainMultLine(level.getDescription().get(0)));
      res.add(l);
    }

    return res;
  }

  private List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit> convertToCreditList(
      final List<CreditMDTO> credits) {
    final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit> res = new ArrayList<>();

    for (final CreditMDTO credit : credits) {
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit c =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit();

      c.setLevel(credit.getLevel());
      c.setScheme(credit.getScheme());
      c.setValue(credit.getValue());
      res.add(c);
    }

    return res;
  }

  private List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category> convertToCatrgoryList(
      final List<ResultDistributionCategoryMDTO> resultDistributionCategory) {

    final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category> res =
        new ArrayList<>();

    for (final ResultDistributionCategoryMDTO cat : resultDistributionCategory) {
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category c =
          new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category();

      c.setCount(cat.getCount());
      c.setLabel(cat.getLabel());
      res.add(c);
    }

    return res;
  }

  private List<PlaintextMultilineStringWithOptionalLang> convertToPlainMultLineString(
      final List<LanguageItemMDTO> languageItems) {
    final List<PlaintextMultilineStringWithOptionalLang> res = new ArrayList<>();

    for (final LanguageItemMDTO lang : languageItems) {
      res.add(convertToPlainMultLine(lang));
    }

    return res;
  }

  private PlaintextMultilineStringWithOptionalLang convertToPlainMultLine(final LanguageItemMDTO languageItem) {
    final PlaintextMultilineStringWithOptionalLang res = new PlaintextMultilineStringWithOptionalLang();
    res.setLang(languageItem.getLang());
    res.setValue(languageItem.getText());
    return res;
  }

  private List<TokenWithOptionalLang> convertToTokenWithOptionalLangList(final List<LanguageItemMDTO> names) {
    final List<TokenWithOptionalLang> res = new ArrayList<>();

    for (final LanguageItemMDTO lang : names) {
      res.add(convertToTokenWithOptionalLang(lang));
    }

    return res;
  }

  private TokenWithOptionalLang convertToTokenWithOptionalLang(final LanguageItemMDTO lang) {
    final TokenWithOptionalLang token = new TokenWithOptionalLang();
    token.setLang(lang.getLang());
    token.setValue(lang.getText());
    return token;
  }

  private List<TokenWithOptionalLang> convertContentToTokenWithOptionalLangList(
      final List<AttachmentContentMDTO> names) {
    final List<TokenWithOptionalLang> res = new ArrayList<>();

    for (final AttachmentContentMDTO lang : names) {
      final TokenWithOptionalLang token = new TokenWithOptionalLang();
      token.setLang(lang.getLanguage());
      token.setValue(new String(base64EncodeIfNot(lang.getContent())));
      res.add(token);
    }

    return res;
  }

  private List<SimpleHtmlStringWithOptionalLang> convertToSimpleHTMStringList(final List<LanguageItemMDTO> names) {
    final List<SimpleHtmlStringWithOptionalLang> res = new ArrayList<>();

    for (final LanguageItemMDTO lang : names) {
      final SimpleHtmlStringWithOptionalLang token = new SimpleHtmlStringWithOptionalLang();
      token.setLang(lang.getLang());
      token.setValue(lang.getText());
      res.add(token);
    }

    return res;
  }

  private LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm convertToAcademicTerm(
      final AcademicTermMDTO academicTerm) {
    final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm res =
        new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm();

    res.setStart(CommonsConverter.convertToGregorianCalendar(academicTerm.getStartDate()));
    res.setEnd(CommonsConverter.convertToGregorianCalendar(academicTerm.getEndDate()));
    res.getTitle().addAll(convertToTokenWithOptionalLangList(academicTerm.getDispName()));

    return res;
  }

  @Override
  public List<MobilityMDTO> convertToTorMDTOList(final List<ImobilityTorsGetResponse.Tor> tors) {
    final List<MobilityMDTO> res = new ArrayList<>();
    for (final ImobilityTorsGetResponse.Tor tor : tors) {
      res.add(convertToTorMDTO(tor));
    }
    return res;
  }

  private MobilityMDTO convertToTorMDTO(final ImobilityTorsGetResponse.Tor tor) {
    final MobilityMDTO mdto = new MobilityMDTO();

    // Para asignar el TOR a la mobilidad hay que hacerlo a traves del Learning Agreement
    final TranscriptOfRecordMDTO torMdto = new TranscriptOfRecordMDTO();
    final LearningAgreementMDTO la = new LearningAgreementMDTO();
    la.setTranscriptOfRecord(torMdto);
    mdto.setLearningAgreement(Collections.singletonList(la));

    /** Isced table **/
    torMdto.setIscedTable(convertToIscedTableMDTOList(tor.getGradeConversionTable().getIscedTable()));

    /** Generated Date **/
    torMdto.setGeneratedDate(CommonsConverter.convertToDate(tor.getElmo().getGeneratedDate()));

    /** Extension **/
    if (Objects.nonNull(tor.getElmo().getExtension())
        && Objects.nonNull(tor.getElmo().getExtension().getExtensionContent())) {
      torMdto.setExtension(base64DecodeIfSo(tor.getElmo().getExtension().getExtensionContent().getBytes()));
    }

    /** Attachments **/
    torMdto.setAttachments(convertToAttachmentMDTOList(tor.getElmo().getAttachment()));

    /** EMREX-ELMO **/
    torMdto.setReport(convertToReportMDTO(tor.getElmo().getReport(), tor.getElmo().getGroups().getGroupType()));

    return mdto;
  }

  private List<ReportMDTO> convertToReportMDTO(final List<Elmo.Report> reports,
      final List<Groups.GroupType> groupTypes) {
    final List<ReportMDTO> res = new ArrayList<>();

    for (final Elmo.Report elmoReport : reports) {
      final ReportMDTO reportMdto = new ReportMDTO();

      /** Issue Date **/
      reportMdto.setIssueDate(CommonsConverter.convertToDate(elmoReport.getIssueDate()));

      /** Lois **/
      // Los lois se deben asociar a los components de los LA
      for (final LearningOpportunitySpecification los : elmoReport.getLearningOpportunitySpecification()) {
        reportMdto.setLois(
            convertToLoiMDTOList(los.getSpecifies().getLearningOpportunityInstance(), elmoReport.getGradingScheme(),
                groupTypes, elmoReport.getAttachment(), los, getIssuerSchac(elmoReport.getIssuer())));
      }

      /** Attachments **/
      // el report tendra como attachments aquellos que no esten referenciados en ningun LOI
      reportMdto.setAttachments(obtenerReportAttachmentsMDTO(elmoReport));

      res.add(reportMdto);
    }

    return res;
  }

  private List<AttachmentMDTO> obtenerReportAttachmentsMDTO(final Elmo.Report elmoReport) {
    final List<AttachmentMDTO> res = new ArrayList<>();

    // obtenemos los attachments de los lois
    final List<String> loisAttachmentsIds = new ArrayList<>();
    for (final LearningOpportunitySpecification los : elmoReport.getLearningOpportunitySpecification()) {
      if (Objects.nonNull(los.getSpecifies().getLearningOpportunityInstance().getAttachments())) {
        loisAttachmentsIds.addAll(los.getSpecifies().getLearningOpportunityInstance().getAttachments().getRef());
      }
    }

    for (final Attachment reportAtt : elmoReport.getAttachment()) {
      final Attachment.Identifier id = obtenerInternalAttachmentId(reportAtt);
      if (Objects.nonNull(id) && !loisAttachmentsIds.contains(id.getValue())) {
        res.addAll(convertToAttachmentMDTOList(Collections.singletonList(reportAtt)));
      }
    }

    return res;
  }

  private List<LearningOpportunityInstanceMDTO> convertToLoiMDTOList(
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance learningOpportunityInstance,
      final List<Elmo.Report.GradingScheme> gradingSchemes, final List<Groups.GroupType> groupTypes,
      final List<Attachment> attachments, final LearningOpportunitySpecification los, final String issuerSchac) {
    final LearningOpportunityInstanceMDTO res = new LearningOpportunityInstanceMDTO();

    /** ID **/
    final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier id =
        obtenerInternalLOIid(learningOpportunityInstance.getIdentifier());
    if (Objects.nonNull(id)) {
      res.setId(id.getValue());
      // buscamos la version del LOI por el id
      res.setVersion(laDao.obtenerLoiVersion(id.getValue()));
    }

    /** Grading Scheme **/
    res.setGradingScheme(
        convertToGradingSchemeMDTO(learningOpportunityInstance.getGradingSchemeLocalId(), gradingSchemes));

    /** Result Distribution **/
    if (Objects.nonNull(learningOpportunityInstance.getResultDistribution())) {
      res.setResultDistribution(convertToResultDistributionMDTO(learningOpportunityInstance.getResultDistribution()));
    }

    /** Language of Instruction **/
    res.setLanguageOfInstruction(learningOpportunityInstance.getLanguageOfInstruction());

    /** Engagement Hours */
    res.setEngagementHours(learningOpportunityInstance.getEngagementHours());

    /** Levels */
    if (Objects.nonNull(learningOpportunityInstance.getLevel())) {
      res.setLevels(convertToLevelMDTOList(learningOpportunityInstance.getLevel()));
    }

    /** Group Types **/
    if (Objects.nonNull(learningOpportunityInstance.getGrouping()) && Objects.nonNull(groupTypes)) {
      res.setLoiGrouping(convertToLoiGroupingMDTOList(learningOpportunityInstance.getGrouping(), groupTypes));
    }

    /** Start Date **/
    res.setStartDate(CommonsConverter.convertToDate(learningOpportunityInstance.getStart()));

    /** End Date **/
    res.setEndDate(CommonsConverter.convertToDate(learningOpportunityInstance.getDate()));

    if (Objects.nonNull(learningOpportunityInstance.getShortenedGrading())) {
      if (Objects.nonNull(learningOpportunityInstance.getShortenedGrading().getPercentageLower())) {
        /** Percentage Lower **/
        res.setPercentageLower(learningOpportunityInstance.getShortenedGrading().getPercentageLower().longValue());
      }

      if (Objects.nonNull(learningOpportunityInstance.getShortenedGrading().getPercentageEqual())) {
        /** Percentage Equal **/
        res.setPercentageEqual(learningOpportunityInstance.getShortenedGrading().getPercentageEqual().longValue());
      }

      if (Objects.nonNull(learningOpportunityInstance.getShortenedGrading().getPercentageHigher())) {
        /** Percentage Higher **/
        res.setPercentageHigher(learningOpportunityInstance.getShortenedGrading().getPercentageHigher().longValue());
      }
    }

    /** Result label **/
    res.setResultLabel(learningOpportunityInstance.getResultLabel());

    /** Status **/
    if (Objects.nonNull(learningOpportunityInstance.getStatus())) {
      res.setStatus(LoiStatus.valueOf(learningOpportunityInstance.getStatus().toUpperCase()));
    }

    /** Extension **/
    if (Objects.nonNull(learningOpportunityInstance.getExtension())
        && Objects.nonNull(learningOpportunityInstance.getExtension().getExtensionContent())) {
      res.setExtension(base64DecodeIfSo(learningOpportunityInstance.getExtension().getExtensionContent().getBytes()));
    }

    /** Attachment **/
    res.setAttachments(obtenerLoiAttachmentsMDTO(attachments, learningOpportunityInstance.getAttachments()));

    /** Diploma **/
    if (Objects.nonNull(learningOpportunityInstance.getDiplomaSupplement())) {
      res.setDiploma(convertToDiplomaMDTO(learningOpportunityInstance.getDiplomaSupplement()));
    }

    /** LOS **/
    res.setLearningOpportunitySpecification(convertToLosMDTO(los, issuerSchac));

    return Collections.singletonList(res);
  }

  private LearningOpportunitySpecificationMDTO convertToLosMDTO(final LearningOpportunitySpecification los,
      final String issuerSchac) {
    final LearningOpportunitySpecificationMDTO res = new LearningOpportunitySpecificationMDTO();


    final LearningOpportunitySpecification.Identifier id = obtenerInternalLOSid(los.getIdentifier());

    if (Objects.nonNull(id)) {
      res.setId(id.getValue());
      res.setVersion(laDao.obtenerLosVersion(id.getValue()));
    }

    res.setName(convertToLanguageItemListFromToken(los.getTitle()));
    final LanguageItemMDTO url = new LanguageItemMDTO();
    url.setText(los.getUrl());
    url.setLang("en");
    res.setUrl(Collections.singletonList(url));
    res.setIscedf(los.getIscedCode());
    if (Objects.nonNull(los.getType())) {
      res.setType(LearningOpportunitySpecificationType.valueOf(los.getType().toUpperCase()));
    }
    res.setSubjectArea(los.getSubjectArea());
    res.setDescription(convertToLanguageItemList(los.getDescription()));
    if (Objects.nonNull(los.getExtension()) && Objects.nonNull(los.getExtension().getExtensionContent())) {
      res.setExtension(base64DecodeIfSo(los.getExtension().getExtensionContent().getBytes()));
    }
    res.setInstitutionId(issuerSchac);

    return res;
  }

  private List<IscedTableMDTO> convertToIscedTableMDTOList(
      final List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable> iscedTables) {
    final List<IscedTableMDTO> res = new ArrayList<>();

    for (final ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable isced : iscedTables) {
      final IscedTableMDTO table = new IscedTableMDTO();
      table.setIscedCode(isced.getIscedCode());
      table.setFrequencies(convertToGradeFrequencyMDTOList(isced.getGradeFrequency()));
      res.add(table);
    }

    return res;
  }

  private List<GradeFrequencyMDTO> convertToGradeFrequencyMDTOList(
      final List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency> frequencies) {
    final List<GradeFrequencyMDTO> res = new ArrayList<>();

    for (final ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency gradeFreq : frequencies) {
      final GradeFrequencyMDTO grade = new GradeFrequencyMDTO();
      grade.setLabel(gradeFreq.getLabel());
      grade.setPercentage(gradeFreq.getPercentage().longValue());
      res.add(grade);
    }

    return res;
  }

  private List<AttachmentMDTO> convertToAttachmentMDTOList(final List<Attachment> attachments) {
    final List<AttachmentMDTO> res = new ArrayList<>();

    for (final Attachment attachment : attachments) {
      final AttachmentMDTO mdto = new AttachmentMDTO();
      if (Objects.nonNull(attachment.getType())) {
        mdto.setType(AttachmentType.valueOf(attachment.getType().replace(" ", "_").toUpperCase()));
      }
      if (Objects.nonNull(attachment.getExtension())
          && Objects.nonNull(attachment.getExtension().getExtensionContent())) {
        mdto.setExtension(base64DecodeIfSo(attachment.getExtension().getExtensionContent().getBytes()));
      }
      mdto.setDescription(convertToLanguageItemList(attachment.getDescription()));
      mdto.setTitle(convertToLanguageItemListFromToken(attachment.getTitle()));
      mdto.setContents(convertContentAttachmentContentMDTOs(attachment.getContent()));

      final Attachment.Identifier id = obtenerInternalAttachmentId(attachment);
      mdto.setId(Objects.nonNull(id) ? id.getValue() : null);

      res.add(mdto);
    }

    return res;
  }

  private Attachment.Identifier obtenerInternalAttachmentId(final Attachment attachment) {
    return attachment.getIdentifier().stream().filter(a -> a.getType().equals("internal")).findFirst().orElse(null);
  }

  private LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier obtenerInternalLOIid(
      final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier> identifier) {
    return identifier.stream().filter(a -> a.getType().equals("ewp-loi-id")).findFirst().orElse(null);
  }

  private LearningOpportunitySpecification.Identifier obtenerInternalLOSid(
      final List<LearningOpportunitySpecification.Identifier> identifier) {
    return identifier.stream().filter(a -> "ewp-los-id".equals(a.getType())).findFirst()
        .orElse(identifier.stream().filter(a -> "local".equals(a.getType())).findFirst().orElse(null));
  }

  private String getIssuerSchac(final Elmo.Report.Issuer issuer) {
    final Optional<Elmo.Report.Issuer.Identifier> id =
        issuer.getIdentifier().stream().filter(i -> "schac".equals(i.getType())).findFirst();
    return id.isPresent() ? id.get().getValue() : null;
  }

  private List<LanguageItemMDTO> convertToLanguageItemList(
      final List<PlaintextMultilineStringWithOptionalLang> multilineStrings) {
    if (Objects.isNull(multilineStrings)) {
      return null;
    }

    final List<LanguageItemMDTO> res = new ArrayList<>();

    for (final PlaintextMultilineStringWithOptionalLang multiline : multilineStrings) {
      res.add(convertFromPlainText(multiline));
    }
    return res;
  }

  private LanguageItemMDTO convertFromPlainText(final PlaintextMultilineStringWithOptionalLang multiline) {
    final LanguageItemMDTO mdto = new LanguageItemMDTO();
    mdto.setText(multiline.getValue());
    mdto.setLang(multiline.getLang());
    return mdto;
  }

  private List<LanguageItemMDTO> convertToLanguageItemListFromToken(
      final List<TokenWithOptionalLang> multilineStrings) {
    if (Objects.isNull(multilineStrings)) {
      return null;
    }

    final List<LanguageItemMDTO> res = new ArrayList<>();

    for (final TokenWithOptionalLang token : multilineStrings) {
      res.add(convertToLanguageItemFromToken(token));
    }
    return res;
  }

  private LanguageItemMDTO convertToLanguageItemFromToken(final TokenWithOptionalLang multiline) {
    final LanguageItemMDTO mdto = new LanguageItemMDTO();
    mdto.setText(multiline.getValue());
    mdto.setLang(multiline.getLang());
    return mdto;
  }

  private List<AttachmentContentMDTO> convertContentAttachmentContentMDTOs(final List<TokenWithOptionalLang> tokens) {
    if (Objects.isNull(tokens)) {
      return null;
    }

    final List<AttachmentContentMDTO> res = new ArrayList<>();

    for (final TokenWithOptionalLang token : tokens) {
      if (Objects.nonNull(token)) {
        final AttachmentContentMDTO content = new AttachmentContentMDTO();
        content.setLanguage(token.getLang());
        content.setContent(base64DecodeIfSo(token.getValue().getBytes()));
        res.add(content);
      }
    }

    return res;
  }

  private AcademicTermMDTO convertToAcademicTermMDTO(
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm academicTerm) {
    final AcademicTermMDTO res = new AcademicTermMDTO();

    res.setStartDate(CommonsConverter.convertToDate(academicTerm.getStart()));
    res.setEndDate(CommonsConverter.convertToDate(academicTerm.getEnd()));
    res.setDispName(convertToLanguageItemListFromToken(academicTerm.getTitle()));

    return res;
  }

  private List<CreditMDTO> convertToCreditMDTOList(
      final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit> credits) {
    final List<CreditMDTO> res = new ArrayList<>();

    for (final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit credit : credits) {
      final CreditMDTO dto = new CreditMDTO();
      dto.setLevel(credit.getLevel());
      dto.setScheme(credit.getScheme());
      dto.setValue(credit.getValue());
      res.add(dto);
    }

    return res;
  }

  private GradingSchemeMDTO convertToGradingSchemeMDTO(final String gradingSchemeId,
      final List<Elmo.Report.GradingScheme> gradingSchemes) {

    final Elmo.Report.GradingScheme scheme = gradingSchemes.stream()
        .filter(gradingScheme -> gradingScheme.getLocalId().equals(gradingSchemeId)).findFirst().orElse(null);

    GradingSchemeMDTO res = null;
    if (Objects.nonNull(scheme)) {
      res = new GradingSchemeMDTO();
      res.setDescription(convertToLanguageItemList(scheme.getDescription()));
      // res.setLabel(convertToLanguageItemList(scheme.get));
      // res.setId(scheme.getLocalId());
      // TODO: Terminar esto, el campo lable no sabemos que es
    }

    return res;
  }


  private ResultDistributionMDTO convertToResultDistributionMDTO(
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution resultDistribution) {
    final ResultDistributionMDTO res = new ResultDistributionMDTO();
    res.setDescription(convertToLanguageItemList(resultDistribution.getDescription()));
    res.setResultDistributionCategory(convertToResultDistributionCategoryList(resultDistribution.getCategory()));
    return res;
  }

  private List<ResultDistributionCategoryMDTO> convertToResultDistributionCategoryList(
      final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category> categories) {
    final List<ResultDistributionCategoryMDTO> res = new ArrayList<>();

    for (final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category cat : categories) {
      final ResultDistributionCategoryMDTO dist = new ResultDistributionCategoryMDTO();
      dist.setCount(cat.getCount());
      dist.setLabel(cat.getLabel());
      res.add(dist);
    }

    return res;
  }

  private List<LevelMDTO> convertToLevelMDTOList(
      final List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level> levels) {
    final List<LevelMDTO> res = new ArrayList<>();

    for (final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level level : levels) {
      final LevelMDTO mdto = new LevelMDTO();
      if (Objects.nonNull(level.getType())) {
        mdto.setType(LevelType.valueOf(level.getType().toUpperCase()));
      }
      mdto.setValue(level.getValue());
      if (Objects.nonNull(level.getDescription())) {
        mdto.setDescription(convertToLanguageItemList(Collections.singletonList(level.getDescription())));
      }
      res.add(mdto);
    }

    return res;
  }

  private LoiGroupingMDTO convertToLoiGroupingMDTOList(
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping grouping,
      final List<Groups.GroupType> groupTypes) {

    final LoiGroupingMDTO res = new LoiGroupingMDTO();
    final GroupTypeMDTO mdto = new GroupTypeMDTO();

    // obtenemos el grouptype a partir del typeref
    final Groups.GroupType type =
        groupTypes.stream().filter(g -> g.getId().equals(grouping.getTyperef())).findFirst().orElse(null);

    if (Objects.nonNull(type)) {
      mdto.setTitle(convertToLanguageItemFromToken(type.getTitle()));

      // obtenermos el group a partir del idref
      final Groups.GroupType.Group group =
          type.getGroup().stream().filter(g -> g.getId().equals(grouping.getIdref())).findFirst().orElse(null);

      if (Objects.nonNull(group)) {
        final GroupMDTO groupMDTO = new GroupMDTO();
        groupMDTO.setSortingKey(group.getSortingKey());
        groupMDTO.setTitle(convertToLanguageItemFromToken(group.getTitle()));
        mdto.setGroups(Collections.singletonList(groupMDTO));
        res.setGroup(groupMDTO);
      }

      res.setGroupType(mdto);

    }

    return res;
  }

  private List<AttachmentMDTO> obtenerLoiAttachmentsMDTO(final List<Attachment> attachments,
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments attachmentsRef) {
    final List<Attachment> loisAttachments =
        attachments.stream().filter(a -> isLoiAttachment(a, attachmentsRef)).collect(Collectors.toList());
    return convertToAttachmentMDTOList(loisAttachments);
  }

  /**
   * Este metodo nos indica si el identificador de un Attachment esta incluido en una lista de Ids.
   * 
   * @param attachment el attachment
   * @param refs lista de referencias a ids de attachemnts
   * @return true si el id del attachment esta presente en la lista
   */
  private boolean isLoiAttachment(final Attachment attachment,
      final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments refs) {

    if (!attachment.getIdentifier().isEmpty()) {
      // intentamos obtener el identificador interno del attachment, si no existe cogemos el primero
      final Attachment.Identifier idLocal = attachment.getIdentifier().stream()
          .filter(id -> id.getType().equals("internal")).findFirst().orElse(attachment.getIdentifier().get(0));

      return refs.getRef().contains(idLocal.getValue());
    }

    return Boolean.TRUE;
  }

  private DiplomaMDTO convertToDiplomaMDTO(final DiplomaSupplement diploma) {
    final DiplomaMDTO res = new DiplomaMDTO();

    if (Objects.nonNull(diploma)) {
      res.setIssueDate(CommonsConverter.convertToDate(diploma.getIssueDate()));
      if (Objects.nonNull(diploma.getVersion())) {
        res.setDiplomaVersion(Integer.parseInt(diploma.getVersion()));
      }
      if (Objects.nonNull(diploma.getIntroduction())) {
        res.setIntroduction(base64DecodeIfSo((byte[]) diploma.getIntroduction()));
      }
      res.setSection(convertToDiplomaSectionMDTOList(diploma.getSection()));
    }

    return res;
  }

  private List<DiplomaSectionMDTO> convertToDiplomaSectionMDTOList(final List<DiplomaSupplementSection> section) {
    final List<DiplomaSectionMDTO> res = new ArrayList<>();

    for (final DiplomaSupplementSection diplomaSection : section) {

      final DiplomaSectionMDTO s = new DiplomaSectionMDTO();
      if (Objects.nonNull(diplomaSection.getContent()) && Objects.nonNull(diplomaSection.getContent().getValue())) {
        s.setContent(base64DecodeIfSo(diplomaSection.getContent().getValue().getBytes()));
      }
      if (Objects.nonNull(diplomaSection.getNumber())) {
        s.setNumber(Integer.parseInt(diplomaSection.getNumber()));
      }
      s.setTitle(diplomaSection.getTitle());
      if (Objects.nonNull(diplomaSection.getSection()) && !diplomaSection.getSection().isEmpty()) {
        s.setSections(convertToDiplomaSectionMDTOList(diplomaSection.getSection()));
      }

      if (Objects.nonNull(diplomaSection.getAdditionalInformation())
          && !diplomaSection.getAdditionalInformation().isEmpty()) {
        final List<DiplomaAdditionalInfoMDTO> additionalInfoMDTOS = new ArrayList<>();
        for (final String info : diplomaSection.getAdditionalInformation()) {
          final DiplomaAdditionalInfoMDTO dto = new DiplomaAdditionalInfoMDTO();
          dto.setAdditionalInfo(info);
          additionalInfoMDTOS.add(dto);
        }
      }

      if (Objects.nonNull(diplomaSection.getAttachment()) && !diplomaSection.getAttachment().isEmpty()) {
        s.setAttachments(convertToAttachmentMDTOList(diplomaSection.getAttachment()));
      }

      res.add(s);
    }

    return res;
  }

  private byte[] base64EncodeIfNot(final byte[] input) {
    try {
      Base64.getDecoder().decode(input);
      return input;
    } catch (final Exception e) {
      // si falla es que no estaba codificado y hay que codificarlo
      return Base64.getEncoder().encode(input);
    }
  }

  private byte[] base64DecodeIfSo(final byte[] input) {
    try {
      return Base64.getDecoder().decode(input);
    } catch (final Exception e) {
      // si falla es que no estaba codificado
      return input;
    }
  }
}
