package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converters.impl.IiaHashGenerator;
import es.minsait.ewpcv.error.EwpOperationNotAllowedException;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import org.apache.commons.collections.CollectionUtils;
import com.google.common.base.Strings;
import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.converters.impl.IiaConverterCommon;
import es.minsait.ewpcv.error.EwpIiaSnapshotException;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.model.iia.*;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import es.minsait.ewpcv.service.api.IEventService;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.INotificationService;
import es.minsait.ewpcv.utils.Utils;
import eu.erasmuswithoutpaper.api.iias7.endpoints.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The type Iias service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class IIiasServiceImpl implements IIiasService {

  private static final String HA_CAMBIADO_LA_PROPIEDAD = "Ha cambiado la propiedad {}";
  private final IIiaDAO iiaDAO;
  private final IIiaConverter iiaConverter;

  private final IEventService eventService;
  private final INotificationService notificationService;

  private final IIiaRegressionsDAO iiaRegressionsDAO;

  @Autowired
  public IIiasServiceImpl(final IIiaDAO iiaDAO, final IIiaConverter iiaConverter, IEventService eventService, INotificationService notificationService, IIiaRegressionsDAO iiaRegressionsDAO) {
    this.iiaDAO = iiaDAO;
    this.iiaConverter = iiaConverter;
    this.eventService = eventService;
    this.notificationService = notificationService;
    this.iiaRegressionsDAO = iiaRegressionsDAO;
  }

  @Override
  public List<String> getIiaIdsByCriteria(final String heiId, final List<String> partnerHeiId,
      final List<String> receivingAcademicYearId, final String modifiedSince) {

    final Map<Object, Object> queryParams = new HashMap<>();

    this.addParam(queryParams, heiId, "heiId");
    this.addParam(queryParams, partnerHeiId, "partnerHeiId");
    this.addParam(queryParams, receivingAcademicYearId, "receivingAcademicYearId");
    this.addParam(queryParams, modifiedSince, "modifiedSince");

    return iiaDAO.getIiaIdsByCriteria(queryParams);
  }

  @Override
  public List<IiasGetResponse.Iia> getIiaListByCriteria(final String heiId, final List<String> partnerHeiId,
      final List<String> iiaId) {

    //dividimos la lista de heis en bloques de 1000 e invocamos al DAO para cada bloque
    final int MAX_LIST_SIZE = 1000;
    Collection<List<String>> partitionedList = IntStream.range(0, partnerHeiId.size())
            .boxed()
            .collect(Collectors.groupingBy(partition -> (partition / MAX_LIST_SIZE),
                    Collectors.mapping(partnerHeiId::get, Collectors.toList())))
            .values();

    List<IiaMDTO> iias = new ArrayList<>();

    for(List<String> heiIds : partitionedList) {
      final Map<Object, Object> queryParams = new HashMap<>();
      this.addParam(queryParams, heiId, "heiId");
      this.addParam(queryParams, heiIds, "partnerHeiId");
      this.addParam(queryParams, iiaId, "iiaId");

      //buscamos los iias que cumplen los criterios
      List<IiaMDTO> filteredIias = iiaDAO.getIiaListByCriteria(queryParams);

      //los anadimos a la lista de iias que devolveremos
      iias.addAll(filteredIias);

      //para cada condicion de cooperacion ordenamos la lista de subject area language skill
      for (IiaMDTO iia : filteredIias) {
        for (CooperationConditionMDTO cc : iia.getCooperationConditions()) {
          cc.getLanguageSkill().sort(null);
        }
      }
    }
    return iiaConverter.convertToIias(heiId, iias);
  }

  /**
   * Comprueba que nuestra institucion participe en alguna de las condiciones de cooperacion del iia.
   * 
   * @param iia
   * @param ewpInstance
   * @return
   */
  private boolean participoEnIia(final IiasGetResponse.Iia iia, final String ewpInstance) {
    if (Objects.isNull(ewpInstance) || Objects.isNull(iia) || Objects.isNull(iia.getCooperationConditions())
        || (CollectionUtils.isEmpty(iia.getCooperationConditions().getStaffTeacherMobilitySpec())
            && CollectionUtils.isEmpty(iia.getCooperationConditions().getStaffTrainingMobilitySpec())
            && CollectionUtils.isEmpty(iia.getCooperationConditions().getStudentStudiesMobilitySpec())
            && CollectionUtils.isEmpty(iia.getCooperationConditions().getStudentTraineeshipMobilitySpec()))) {
      return Boolean.FALSE;
    }

    final boolean inStaffTeacherMobilitySpec =
        CollectionUtils.isNotEmpty(iia.getCooperationConditions().getStaffTeacherMobilitySpec())
            && iia.getCooperationConditions().getStaffTeacherMobilitySpec().stream()
                .anyMatch(c -> ewpInstance.equalsIgnoreCase(c.getReceivingHeiId())
                    || ewpInstance.equalsIgnoreCase(c.getSendingHeiId()));
    final boolean inStaffTrainingMobilitySpec =
        CollectionUtils.isNotEmpty(iia.getCooperationConditions().getStaffTrainingMobilitySpec())
            && iia.getCooperationConditions().getStaffTrainingMobilitySpec().stream()
                .anyMatch(c -> ewpInstance.equalsIgnoreCase(c.getReceivingHeiId())
                    || ewpInstance.equalsIgnoreCase(c.getSendingHeiId()));
    final boolean inStudentStudiesMobilitySpec =
        CollectionUtils.isNotEmpty(iia.getCooperationConditions().getStudentStudiesMobilitySpec())
            && iia.getCooperationConditions().getStudentStudiesMobilitySpec().stream()
                .anyMatch(c -> ewpInstance.equalsIgnoreCase(c.getReceivingHeiId())
                    || ewpInstance.equalsIgnoreCase(c.getSendingHeiId()));
    final boolean inStudentTraineeshipMobilitySpec =
        CollectionUtils.isNotEmpty(iia.getCooperationConditions().getStudentTraineeshipMobilitySpec())
            && iia.getCooperationConditions().getStudentTraineeshipMobilitySpec().stream()
                .anyMatch(c -> ewpInstance.equalsIgnoreCase(c.getReceivingHeiId())
                    || ewpInstance.equalsIgnoreCase(c.getSendingHeiId()));

    return inStaffTeacherMobilitySpec || inStaffTrainingMobilitySpec || inStudentStudiesMobilitySpec
        || inStudentTraineeshipMobilitySpec;
  }

  @Override
  @Transactional(readOnly = false)
  public Boolean guardarIiaCopiaRemota(final IiasGetResponse.Iia iiaRemoto, final String ewpInstance) {
    if (!participoEnIia(iiaRemoto, ewpInstance)) {
      log.info(" No participamos en el IIA, no lo volcamos");
      return Boolean.FALSE;
    }
    log.info(" No hay copia local, convertimos y persistimos la copia del Iia remoto en local");
    final IiaMDTO iiaCopiaRemota = IiaConverterCommon.convertSingleIia(ewpInstance, iiaRemoto);
    iiaCopiaRemota.setIsRemote(Boolean.TRUE);
    iiaCopiaRemota.setIsAutogenerated(Boolean.FALSE);
    iiaCopiaRemota.setIsExposed(Boolean.TRUE);
    iiaDAO.save(iiaCopiaRemota);
    return Boolean.TRUE;
  }

  @Override
  @Transactional(readOnly = false)
  public Boolean actualizarIiaCopiaRemota(final IiasGetResponse.Iia iiaRemoto, final IiaMDTO iiaCopiaRemota,
      final String ewpInstance) {
    if (!participoEnIia(iiaRemoto, ewpInstance)) {
      log.info(" No participamos en el IIA, no lo volcamos");
      return Boolean.FALSE;
    }
    final boolean hayCambio =
        compararLocalConRemotoAndModificar(IiaConverterCommon.convertSingleIia(ewpInstance, iiaRemoto), iiaCopiaRemota);
    iiaCopiaRemota.setIsRemote(Boolean.TRUE);
    iiaCopiaRemota.setIsAutogenerated(Boolean.FALSE);
    iiaCopiaRemota.setIsExposed(Boolean.TRUE);
    iiaCopiaRemota.setModifyDate(new Date());
    if (hayCambio) {
      log.info(" Actualizamos la copia local del Iia remoto");
      iiaDAO.save(iiaCopiaRemota);
    }
    return hayCambio;
  }

  /**
   * Compara dos IiaMDTO y actualiza la copia local
   * No compara approval hash ni approval date porque son ambito de otro api
   *
   * @param remoto IiaMDTO remoto
   * @param local IiaMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLocalConRemotoAndModificar(final IiaMDTO remoto, final IiaMDTO local) {

    Boolean hayCambio = false;

    log.trace(" Inicio de la comparación de los Iias ");

    if (!Objects.equals(remoto.getPdfId(), local.getPdfId())) {
      local.setPdfId(remoto.getPdfId());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "pdf");
      hayCambio = true;
    }

    if (!Objects.equals(remoto.getRemoteCoopCondHash(), local.getRemoteCoopCondHash())) {
      local.setRemoteCoopCondHash(remoto.getRemoteCoopCondHash());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "remoteCoopCondHash");
      hayCambio = true;
    }


    //comparamos los partners del IIA
    if (Objects.nonNull(remoto.getFirstPartner()) && compararPartners(remoto.getFirstPartner(), local.getFirstPartner())) {
      local.setFirstPartner(remoto.getFirstPartner());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "firstPartner");
      hayCambio = true;
    }

    if (Objects.nonNull(remoto.getSecondPartner()) && compararPartners(remoto.getSecondPartner(), local.getSecondPartner())) {
      local.setSecondPartner(remoto.getSecondPartner());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "secondPartner");
      hayCambio = true;
    }

    final List<CooperationConditionMDTO> localCoopConditions =
            CollectionUtils.isNotEmpty(local.getCooperationConditions()) ? local.getCooperationConditions()
                    : new ArrayList<>();
    if (compararCooperationConditionsList(remoto.getCooperationConditions(), localCoopConditions)) {
      local.setCooperationConditions(localCoopConditions);
      hayCambio = true;
    }


    if (!Objects.equals(remoto.getRemoteIiaCode(), local.getRemoteIiaCode())) {
      local.setRemoteIiaCode(remoto.getRemoteIiaCode());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "remoteIiaCode");
      hayCambio = true;
    }

    if (!Objects.equals(remoto.getIiaCode(), local.getIiaCode())) {
      local.setIiaCode(remoto.getIiaCode());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "iiaCode");
      hayCambio = true;
    }

    log.trace(" Fin de la comparación de los Iias");
    return hayCambio;
  }

  /**
   * Compara dos IiaPartnerMDTO, no actualiza la copia local
   *
   * @param remoto IiaPartnerMDTO remota
   * @param local IiaPartnerMDTO local
   * @return true si se detectan diferencias
   */
  public boolean compararPartners(final IiaPartnerMDTO remoto, final IiaPartnerMDTO local) {

    if (Objects.nonNull(remoto) && Objects.isNull(local)) {
      log.trace("Llega un partner nuevo que no teniamos");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getInstitutionId(), local.getInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "IiaPartnerMDTO.institutionId");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "IiaPartnerMDTO.organizationUnit");
      return Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getSigningDate(), local.getSigningDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "IiaPartnerMDTO.signingDate");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getSignerPersonId())) {
      final boolean compararContacts = compararContacts(remoto.getSignerPersonId(),
              Objects.nonNull(local.getSignerPersonId()) ? local.getSignerPersonId() : new ContactMDTO(), Boolean.FALSE);
      if (compararContacts) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getContacts()) && !remoto.getContacts().isEmpty()) {
      final boolean compararContacts = compararContactsList(remoto.getContacts(),
          Objects.nonNull(local.getContacts()) ? local.getContacts() : new ArrayList<>(), new ArrayList<>());

      if (compararContacts) {
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de ContactMDTO
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @param elementosABorrar listado en el que se anaden los contacts que hay que borrar, este listado se actualiza al
   *        finalizar el metodo, esta parametro no puede ser nulo y debe ser un listado vacio
   * @return true si hay diferencias entre los listados, en el listado elementos
   */
  @Override
  public boolean compararContactsList(final List<ContactMDTO> remoto, final List<ContactMDTO> local,
      List<ContactMDTO> elementosABorrar) {
    if (Objects.isNull(elementosABorrar)) {
      elementosABorrar = new ArrayList<>();
    }
    if (CollectionUtils.isNotEmpty(local)) {
      elementosABorrar.addAll(
          local.stream().filter(i -> !compararContactsWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<ContactMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
          ? remoto.stream().filter(i -> !compararContactsWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
          : new ArrayList<>();
    }

    log.trace("Se van a eliminar los siguientes ContactMDTO {}", elementosABorrar);
    local.removeAll(elementosABorrar);
    log.trace("Se van a incluir los siguientes ContactMDTO {}", incluir);
    local.addAll(incluir);

    return !(elementosABorrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un ContactMDTO esta en una lista de ContactMDTO
   *
   * @param item ContactMDTO
   * @param lista listado de ContactMDTO
   * @param esItemLocal indica si el item es un ContactMDTO local.
   * @return true si el ContactMDTO esta presente en la lista
   */
  private boolean compararContactsWithList(final ContactMDTO item, final List<ContactMDTO> lista,
      final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream()
          .anyMatch(i -> !compararContacts(i, Objects.nonNull(item) ? item : new ContactMDTO(), Boolean.FALSE));
    } else {
      return lista.stream()
          .anyMatch(i -> !compararContacts(item, Objects.nonNull(i) ? i : new ContactMDTO(), Boolean.FALSE));
    }
  }

  /**
   * Compara dos ContactMDTO
   * 
   * @param remoto ContactMDTO remoto
   * @param local ContactMDTO local
   * @param actualizarLocal indica si hay que actualizar la copia local al detectar cambios
   * @return true si hay cambios entre los dos ContactMDTO
   */
  @Override
  public boolean compararContacts(final ContactMDTO remoto, ContactMDTO local, final boolean actualizarLocal) {
    boolean hayCambios = Boolean.FALSE;

    if (Objects.isNull(remoto) && Objects.nonNull(local)) {
      log.trace("Se ha eliminado un contacto");
      if (actualizarLocal) {
        local = null;
      }
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getInstitutionId(), local.getInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ContactMDTO.institutionId");
      if (actualizarLocal) {
        local.setInstitutionId(remoto.getInstitutionId());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ContactMDTO.organizationUnitId");
      if (actualizarLocal) {
        local.setOrganizationUnitId(remoto.getOrganizationUnitId());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getRole(), local.getRole())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ContactMDTO.role");
      if (actualizarLocal) {
        local.setRole(remoto.getRole());
      }
      hayCambios = Boolean.TRUE;
    }

    if (Objects.isNull(local.getPerson()) && Objects.nonNull(remoto.getPerson())) {
      if (actualizarLocal) {
        local.setPerson(remoto.getPerson());
      }
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Person");
      hayCambios = Boolean.TRUE;
    } else if (Objects.nonNull(remoto.getPerson())) {
      final boolean compareRes = compararPerson(remoto.getPerson(), local.getPerson(), actualizarLocal);
      hayCambios = hayCambios || compareRes;
    }

    if (Objects.nonNull(remoto.getDescription()) && !remoto.getDescription().isEmpty()) {
      final List<LanguageItemMDTO> localDescription =
          CollectionUtils.isNotEmpty(local.getDescription()) ? local.getDescription() : new ArrayList<>();
      final boolean compareRes = compararLanguageItemsList(remoto.getDescription(), localDescription, actualizarLocal);
      if (compareRes && actualizarLocal) {
        local.setDescription(localDescription);
      }
      hayCambios = hayCambios || compareRes;
    }

    if (Objects.nonNull(remoto.getName())) {
      final List<LanguageItemMDTO> localName =
          CollectionUtils.isNotEmpty(local.getName()) ? local.getName() : new ArrayList<>();
      final boolean compareRes = compararLanguageItemsList(remoto.getName(), localName, actualizarLocal);
      if (compareRes && actualizarLocal) {
        local.setName(localName);
      }
      hayCambios = hayCambios || compareRes;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      final ContactDetailsMDTO localContactDetails =
          Objects.nonNull(local.getContactDetails()) ? local.getContactDetails() : new ContactDetailsMDTO();
      final boolean compareRes =
          compararContactDetails(remoto.getContactDetails(), localContactDetails, actualizarLocal);
      if (compareRes && actualizarLocal) {
        local.setContactDetails(localContactDetails);
      }
      hayCambios = hayCambios || compareRes;
    }

    return hayCambios;
  }

  /**
   * Compara dos ContactDetailsMDTO
   * 
   * @param remoto ContactDetailsMDTO remoto
   * @param local ContactDetailsMDTO local
   * @param actualizarLocal
   * @return true si hay diferencias entre la copia local y la remota
   */
  @Override
  public boolean compararContactDetails(final ContactDetailsMDTO remoto, final ContactDetailsMDTO local,
      final boolean actualizarLocal) {

    boolean hayCambios = Boolean.FALSE;

    if (Objects.nonNull(remoto) && CollectionUtils.isNotEmpty(remoto.getEmail())) {
      if (compararListOfStrings(remoto.getEmail(),
              CollectionUtils.isNotEmpty(local.getEmail()) ? local.getEmail() : new ArrayList<>())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Email");
        if (actualizarLocal) {
          local.setEmail(remoto.getEmail());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto) && Objects.nonNull(remoto.getFaxNumber())) {
      if (compararPhoneNumber(remoto.getFaxNumber(), local.getFaxNumber())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FaxNumber");
        if (actualizarLocal) {
          local.setFaxNumber(remoto.getFaxNumber());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto) && compararPhoneNumber(remoto.getPhoneNumber(), local.getPhoneNumber())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PhoneNumber");
      if (actualizarLocal) {
        local.setPhoneNumber(remoto.getPhoneNumber());
      }
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto) && Objects.nonNull(remoto.getMailingAddress())) {
      if (compararFlexibleAdress(remoto.getMailingAddress(), local.getMailingAddress())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "MailAddress");
        if (actualizarLocal) {
          local.setMailingAddress(remoto.getMailingAddress());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto) && Objects.nonNull(remoto.getUrl())) {
      if (compararLanguageItemsList(remoto.getUrl(), local.getUrl(), Boolean.FALSE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Url");
        if (actualizarLocal) {
          local.setUrl(remoto.getUrl());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto) && Objects.nonNull(remoto.getStreetAddress())) {
      if (compararFlexibleAdress(remoto.getStreetAddress(), local.getStreetAddress())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StreetAddress");
        if (actualizarLocal) {
          local.setStreetAddress(remoto.getStreetAddress());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto) && Objects.nonNull(remoto.getPhotoUrls())) {
      if (compararListOfPhotoURL(remoto.getPhotoUrls(),
          Objects.nonNull(local.getPhotoUrls()) ? local.getPhotoUrls() : new ArrayList<>())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PhotoUrls");
        if (actualizarLocal) {
          local.setPhotoUrls(remoto.getPhotoUrls());
        }
        hayCambios = Boolean.TRUE;
      }
    }

    return hayCambios;
  }

  /**
   * Compara dos listas de PhotoUrlMDTO actualizando la lista local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre ambos listados
   */
  private boolean compararListOfPhotoURL(final List<PhotoUrlMDTO> remoto, final List<PhotoUrlMDTO> local) {
    // todos los elementos de la lista local que no estan en la remota
    final List<PhotoUrlMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararPhotoUrlWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<PhotoUrlMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararPhotoUrlWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de PhotosURL");
    log.trace("Se van a borrar los siguientes  PhotosURL: {}", borrar);
    local.removeAll(borrar);
    log.trace("Se van a incluir los siguientes  PhotosURL: {}", incluir);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un PhotoUrlMDTO esta en una lista de PhotoUrlMDTO
   *
   * @param item PhotoUrlMDTO
   * @param lista listado de PhotoUrlMDTO
   * @param esItemLocal indica si el item es un PhotoUrlMDTO local.
   * @return true si el PhotoUrlMDTO esta presente en la lista
   */
  private boolean compararPhotoUrlWithList(final PhotoUrlMDTO item, final List<PhotoUrlMDTO> lista,
      final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararPhotoUrl(i, item, Boolean.FALSE));
    } else {
      return lista.stream().anyMatch(i -> !compararPhotoUrl(item, i, Boolean.FALSE));
    }
  }

  /**
   * Compara dos PhotoUrlMDTO
   * 
   * @param remoto PhotoUrlMDTO remoto
   * @param local PhotoUrlMDTO local
   * @param actualizaLocal indica si hay que actualizar la copia local al detectar cambios
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararPhotoUrl(final PhotoUrlMDTO remoto, final PhotoUrlMDTO local, final boolean actualizaLocal) {
    boolean hayCambios = Boolean.FALSE;

    if (Utils.compararFechaYDias(remoto.getDate(), local.getDate())) {
      log.trace("Ha cambiado la propiedad {}", "PhotoURL - Date");
      if (actualizaLocal) {
        local.setDate(remoto.getDate());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSize(), local.getSize())) {
      // log.trace("Ha cambiado la propiedad {}", "PhotoURL - Size");
      if (actualizaLocal) {
        local.setSize(remoto.getSize());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getUrl(), local.getUrl())) {
      // log.trace("Ha cambiado la propiedad {}", "PhotoURL - URL");
      if (actualizaLocal) {
        local.setUrl(remoto.getUrl());
      }
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  @Override
  public boolean esContactDetailsVacio(final ContactDetailsMDTO contactDetails) {
    return Objects.nonNull(contactDetails) && Objects.isNull(contactDetails.getEmail())
            && Objects.isNull(contactDetails.getPhoneNumber()) && Objects.isNull(contactDetails.getFaxNumber())
            && Objects.isNull(contactDetails.getStreetAddress()) && Objects.isNull(contactDetails.getMailingAddress())
            && CollectionUtils.isEmpty(contactDetails.getUrl());
  }


  @Override
  @Transactional(readOnly = false)
  public void updateCoopCondHash(final List<IiasGetResponse.Iia> iiaIdsList, final String ewpInstance) {
    iiaIdsList.forEach(i -> {
      final String id = obtenId(i, ewpInstance);
      if (!Strings.isNullOrEmpty(id)) {
        iiaDAO.actualizaCoopCondHashByInteropId(id, i.getIiaHash());
      }
    });
  }


  /**
   * Compara dos listados de RequirementsInfoMDTO actualizando el listado local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  @Override
  public boolean compararAdditionalRequeriments(final List<RequirementsInfoMDTO> remoto,
                                                final List<RequirementsInfoMDTO> local) {
    log.trace("Comparando RequirementsInfo");
    final List<RequirementsInfoMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararAdditionalRequerimentsWithList(i, remoto, Boolean.FALSE))
              .collect(Collectors.toList()));
    }
    final List<RequirementsInfoMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto) ? remoto.stream()
              .filter(i -> !compararAdditionalRequerimentsWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }
    log.trace("se van a borrar los siguientes RequirementsInfo {}", borrar.toString());
    log.trace("se van a insertar los siguientes RequirementsInfo {}", incluir.toString());
    local.addAll(incluir);
    local.removeAll(borrar);
    return !(incluir.isEmpty() && borrar.isEmpty());
  }

  /**
   * Comprueba si un RequirementsInfoMDTO esta en una lista de RequirementsInfoMDTO
   *
   * @param item RequirementsInfoMDTO
   * @param lista listado de RequirementsInfoMDTO
   * @param esItemLocal indica si el item es un RequirementsInfoMDTO local.
   * @return true si el RequirementsInfoMDTO esta presente en la lista
   */
  private boolean compararAdditionalRequerimentsWithList(final RequirementsInfoMDTO item,
                                                         final List<RequirementsInfoMDTO> lista, final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararRequirementsInfo(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararRequirementsInfo(item, i));
    }
  }

  /**
   * Compara dos RequirementsInfoMDTO, sin actualizar la copia local
   * 
   * @param remoto RequirementsInfoMDTO remoto
   * @param local RequirementsInfoMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararRequirementsInfo(final RequirementsInfoMDTO remoto, final RequirementsInfoMDTO local) {

    if (!Objects.equals(remoto.getName(), local.getName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "RequirementsInfo.name");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getDescripcion(), local.getDescripcion())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "RequirementsInfo.descripcion");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getType(), local.getType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "RequirementsInfo.type");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      final boolean compareRes =
          compararContactDetails(remoto.getContactDetails(), local.getContactDetails(), Boolean.FALSE);

      if (compareRes) {
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de InformationItemMDTO actualizando el listado local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  @Override
  public boolean compararInformationItems(final List<InformationItemMDTO> remoto, final List<InformationItemMDTO> local,
                                          final boolean actualizarLocal) {
    log.trace("Comparando InformationItems");
    final List<InformationItemMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararInformationItemWithList(i, remoto, Boolean.FALSE))
              .collect(Collectors.toList()));
    }
    final List<InformationItemMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararInformationItemWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }
    log.trace("se van a borrar los siguientes InformationItems {}", borrar.toString());
    log.trace("se van a insertar los siguientes InformationItems {}", incluir.toString());
    local.addAll(incluir);
    local.removeAll(borrar);
    return !(incluir.isEmpty() && borrar.isEmpty());
  }


  /**
   * Genera una copia de un IIA que se vinculara al que recibimos. Solo generamos copia local cuando nos llega un IIA no
   * vinculado y no tenemos ninguno local sin vincular con las mismas instituciones involucradas
   * 
   * @param remoto
   * @param ewpInstance
   * @return
   */
  @Override
  @Transactional(readOnly = false)
  public boolean generarCopiaAutogeneradaIia(final IiasGetResponse.Iia remoto, final String ewpInstance,
      final boolean autogenerateWithCandidates) {

    // buscamos si ya esta asociado a alguna copia local en cuyo caso no hacemos nada
    final Optional<IiasGetResponse.Iia.Partner> partnerRemoto =
        remoto.getPartner().stream().filter(partner -> !partner.getHeiId().equalsIgnoreCase(ewpInstance)).findAny();
    if (partnerRemoto.isPresent() && Objects.nonNull(partnerRemoto.get().getIiaId())
        && iiaDAO.countIiaByRemoteIiaIdAndIsRemote(partnerRemoto.get().getIiaId(), Boolean.FALSE) > 0) {
      return false;
    }

    final Set<String> partners =
        remoto.getPartner().stream().map(p -> p.getHeiId().toLowerCase()).collect(Collectors.toSet());
    final int candidatos = iiaDAO.contarNoVinculadosPorInstituciones(partners);
    // si no hay copias locales que sean candidatas de ser la copia generamos y vinculamos automaticamente
    if (candidatos == 0 || autogenerateWithCandidates) {
      final IiaMDTO iiaCopiaAutogenerada = IiaConverterCommon.convertSingleIia(ewpInstance, remoto);
      iiaCopiaAutogenerada.setIsAutogenerated(Boolean.TRUE);
      iiaCopiaAutogenerada.setIsRemote(Boolean.FALSE);
      iiaCopiaAutogenerada.setIsExposed(Boolean.FALSE);
      iiaDAO.save(iiaCopiaAutogenerada);
      return true;
    }
    return false;
  }

  /**
   * Calcula el hash de las condiciones de cooperacion de un IIA recibido como respuesta de una llamada get
   * 
   * @param iia
   * @return
   */
  @Override
  public boolean verificarHashRemoto(final IiasGetResponse.Iia iia) {
    try{
      final String hashCalculado = IiaHashGenerator.generateIiaHash(
              IiaHashGenerator.getTextToHash(
                      IiaConverterCommon.iiaClassToBytes(iia, "UTF-8"), IiaHashGenerator.getXslt7()));
      return hashCalculado.equalsIgnoreCase(iia.getIiaHash());
    }catch (final Exception e){
      log.error(e.getMessage());
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Comprueba que el iia es válido para aprobar en versión 7 con los cambios de obligatoriedad de campos en base a la
   * transformación via xslt para el cálculo del hash
   *
   * @param iia
   * @return
   */
  @Override
  public boolean verificarValidoParaAprovar(IiasGetResponse.Iia iia) {
    try{
      return IiaHashGenerator.validForApprove(IiaConverterCommon.iiaClassToBytes(iia, "UTF-8"));
    }catch (final Exception e){
      log.error(e.getMessage());
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Comprueba si un InformationItemMDTO esta en una lista de InformationItemMDTO
   *
   * @param item InformationItemMDTO
   * @param lista listado de InformationItemMDTO
   * @param esItemLocal indica si el item es un InformationItemMDTO local.
   * @return true si el InformationItemMDTO esta presente en la lista
   */
  private boolean compararInformationItemWithList(final InformationItemMDTO item, final List<InformationItemMDTO> lista,
                                                  final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararInformationItem(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararInformationItem(item, i));
    }
  }

  /**
   * Compara dos InformationItemMDTO, sin actualizar la copia local
   *
   * @param remoto InformationItemMDTO remoto
   * @param local InformationItemMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararInformationItem(final InformationItemMDTO remoto, final InformationItemMDTO local) {

    if (!Objects.equals(remoto.getType(), local.getType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "RequirementsInfo.type");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      return compararContactDetails(remoto.getContactDetails(), local.getContactDetails(), Boolean.FALSE);
    }

    return Boolean.FALSE;
  }

  private String obtenId(final IiasGetResponse.Iia iia, final String ewpInstance) {
    final Optional<IiasGetResponse.Iia.Partner> partner =
        iia.getPartner().stream().filter(p -> ewpInstance.equalsIgnoreCase(p.getHeiId())).findFirst();
    if (partner.isPresent()) {
      return partner.get().getIiaId();
    } else {
      return null;
    }
  }

  /**
   * Compara dos FlexibleAddressMDTO
   * 
   * @param remoto FlexibleAddressMDTO remoto
   * @param local FlexibleAddressMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararFlexibleAdress(final FlexibleAddressMDTO remoto, final FlexibleAddressMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    if (Objects.isNull(local)) {
      return Boolean.TRUE;
    }

    List<String> addressLineListaLocal = local.getAddressLine();
    if (Objects.nonNull(addressLineListaLocal)) {
      addressLineListaLocal.removeIf(Objects::isNull);
      if (addressLineListaLocal.isEmpty()) {
        local.setRecipientName(null);
      }
    }

    List<String> addressLineListaRemoto = remoto.getAddressLine();
    if (Objects.nonNull(addressLineListaRemoto)) {
      addressLineListaRemoto.removeIf(Objects::isNull);
      if (addressLineListaRemoto.isEmpty()) {
        remoto.setRecipientName(null);
      }
    }

    final boolean compareRes = compararListOfStrings(remoto.getAddressLine(),
            CollectionUtils.isNotEmpty(local.getAddressLine()) ? local.getAddressLine() : new ArrayList<>());
    hayCambios = hayCambios || compareRes;

    List<String> deliveryPointCodeListaLocal = local.getDeliveryPointCode();
    if (Objects.nonNull(deliveryPointCodeListaLocal)) {
      deliveryPointCodeListaLocal.removeIf(Objects::isNull);
      if (deliveryPointCodeListaLocal.isEmpty()) {
        local.setRecipientName(null);
      }
    }

    List<String> deliveryPointCodeListaRemoto = remoto.getDeliveryPointCode();
    if (Objects.nonNull(deliveryPointCodeListaRemoto)) {
      deliveryPointCodeListaRemoto.removeIf(Objects::isNull);
      if (deliveryPointCodeListaRemoto.isEmpty()) {
        remoto.setRecipientName(null);
      }
    }

    final boolean compareRes2 = compararListOfStrings(remoto.getDeliveryPointCode(),
            CollectionUtils.isNotEmpty(local.getDeliveryPointCode()) ? local.getDeliveryPointCode() : new ArrayList<>());
    hayCambios = hayCambios || compareRes2;

    List<String> recipientNameListaLocal = local.getRecipientName();
    if (Objects.nonNull(recipientNameListaLocal)) {
      recipientNameListaLocal.removeIf(Objects::isNull);
      if (recipientNameListaLocal.isEmpty()) {
        local.setRecipientName(null);
      }
    }

    List<String> recipientNameListaRemoto = remoto.getRecipientName();
    if (Objects.nonNull(recipientNameListaRemoto)) {
      recipientNameListaRemoto.removeIf(Objects::isNull);
      if (recipientNameListaRemoto.isEmpty()) {
        remoto.setRecipientName(null);
      }
    }

    final boolean compareRes3 = compararListOfStrings(remoto.getRecipientName(),
            CollectionUtils.isNotEmpty(local.getRecipientName()) ? local.getRecipientName() : new ArrayList<>());
    hayCambios = hayCambios || compareRes3;

    if (!Objects.equals(remoto.getBuildingName(), local.getBuildingName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.buildingName");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getBuildingNumber(), local.getBuildingNumber())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.buildingNumber");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getCountry(), local.getCountry())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.buildingCountry");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getFloor(), local.getFloor())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.Floor");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLocality(), local.getLocality())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.locality");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getPostalCode(), local.getPostalCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.postalCode");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getPostOfficeBox(), local.getPostOfficeBox())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.postOfficeBox");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getRegion(), local.getRegion())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.region");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getStreetName(), local.getStreetName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.streetName");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getUnit(), local.getUnit())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "FlexibleAddresMDTO.unit");
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  /**
   * Compara dos listados de Strings actualizando el listado local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  public boolean compararListOfStrings(final List<String> remoto, final List<String> local) {

    final List<String> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararStringWithList(i, remoto)).collect(Collectors.toList()));
    }

    final List<String> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
          ? remoto.stream().filter(i -> !compararStringWithList(i, local)).collect(Collectors.toList())
          : new ArrayList<>();
    }

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  private boolean compararStringWithList(final String string, final List<String> stringList) {
    if (Objects.isNull(stringList)) {
      return false;
    }
    return stringList.stream().anyMatch(i -> string.equals(i));
  }


  /**
   * Compara dos listas de LanguageItemMDTO
   *
   * @param remoto listado remoto
   * @param local listado local
   * @param actualizarLocal indica si hay que modificar la lista local al encontrar diferencias
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  @Override
  public boolean compararLanguageItemsList(final List<LanguageItemMDTO> remoto, List<LanguageItemMDTO> local,
      final Boolean actualizarLocal) {

    log.trace("Comparando languageItems");

    final List<LanguageItemMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLanguageItemWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<LanguageItemMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      final List<LanguageItemMDTO> finalLocal = local;
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararLanguageItemWithList(i, finalLocal, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("se van a borrar los siguientes LanguageItems {}", borrar);
    log.trace("se van a insertar los siguientes LanguageItems {}", incluir);

    if (actualizarLocal) {
      if (Objects.isNull(local)) {
        local = new ArrayList<>();
      }
      local.addAll(incluir);
      local.removeAll(borrar);
    }

    return !(incluir.isEmpty() && borrar.isEmpty());
  }

  /**
   * Comprueba si un LanguageItemMDTO esta en una lista de LanguageItemMDTO
   *
   * @param item LanguageItemMDTO
   * @param lista listado de LanguageItemMDTO
   * @param esItemLocal indica si el item es un LanguageItemMDTO local.
   * @return true si el LanguageItemMDTO esta presente en la lista
   */
  private boolean compararLanguageItemWithList(final LanguageItemMDTO item, final List<LanguageItemMDTO> lista,
      final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararLanguageItems(i, item, Boolean.FALSE));
    } else {
      return lista.stream().anyMatch(i -> !compararLanguageItems(item, i, Boolean.FALSE));
    }
  }

  /**
   * Compara dos PhoneNumberMDTO, actualizando la copia local
   * 
   * @param remoto PhoneNumberMDTO remoto
   * @param local PhoneNumberMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararPhoneNumber(final PhoneNumberMDTO remoto, final PhoneNumberMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    if (Objects.isNull(remoto) && Objects.nonNull(local)) {
      return Boolean.FALSE;
    }

    if (Objects.nonNull(remoto) && Objects.isNull(local)) {
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto) && !Objects.equals(remoto.getE164(), local.getE164())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PhoneNumberMDTO.E164");
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto) && !Objects.equals(remoto.getExtensionNumber(), local.getExtensionNumber())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PhoneNumberMDTO.extensionNumber");
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto) && !Objects.equals(remoto.getOtherFormat(), local.getOtherFormat())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PhoneNumberMDTO.format");
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  /**
   * Compara dos LanguageItemMDTO
   *
   * @param remoto LanguageItemMDTO remoto
   * @param local LanguageItemMDTO local
   * @param actualizarLocal indica si hay que modificar el LanguageItemMDTO local al encontrar diferencias
   * @return true si hay diferencias entre el local y el remoto
   */
  @Override
  public boolean compararLanguageItems(final LanguageItemMDTO remoto, final LanguageItemMDTO local,
      final boolean actualizarLocal) {
    boolean hayCambios = Boolean.FALSE;

    if (!Objects.equals(remoto.getLang(), local.getLang())) {
      // log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LanguajeItem.lang");
      if (actualizarLocal) {
        local.setLang(remoto.getLang());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getText(), local.getText())) {
      // log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LanguageItem.text");
      if (actualizarLocal) {
        local.setText(remoto.getText());
      }
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  /**
   * Compara dos PersonMDTO
   * 
   * @param remoto PersonMDTO remoto
   * @param local PersonMDTO local
   * @param actualizaLocal indica si hay que actualizar la copia local al detectar cambios
   * @return true si hay diferencias entre los PersonMDTO
   */
  public boolean compararPerson(final PersonMDTO remoto, final PersonMDTO local, final boolean actualizaLocal) {
    boolean hayCambios = Boolean.FALSE;

    if (Objects.nonNull(remoto) && Objects.isNull(local)) {
      hayCambios = Boolean.FALSE;
    }

    if (Utils.compararFechaYDias(remoto.getBirthDate(), local.getBirthDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PersonMDTO.birdthDate");
      if (actualizaLocal) {
        local.setBirthDate(remoto.getBirthDate());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getCountryCode(), local.getCountryCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PersonMDTO.countryCode");
      if (actualizaLocal) {
        local.setCountryCode(remoto.getCountryCode());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getFirstNames(), local.getFirstNames())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PersonMDTO.firstNames");
      if (actualizaLocal) {
        local.setFirstNames(remoto.getFirstNames());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getGender(), local.getGender())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PersonMDTO.gender");
      if (actualizaLocal) {
        local.setGender(remoto.getGender());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLastName(), local.getLastName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PersonMDTO.lastName");
      if (actualizaLocal) {
        local.setLastName(remoto.getLastName());
      }
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  /**
   * Compara dos listas de CooperationConditionMDTO
   * 
   * @param remoto lista remota
   * @param local lista local
   * @return true si se hay diferencias entre las listas
   */
  public Boolean compararCooperationConditionsList(final List<CooperationConditionMDTO> remoto,
      final List<CooperationConditionMDTO> local) {

    final List<CooperationConditionMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararCooperationConditionWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<CooperationConditionMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto) ? remoto.stream()
          .filter(i -> !compararCooperationConditionWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
          : new ArrayList<>();
    }

    log.trace("Se van a eliminar las siguientes CooperationConditionsMDTO {}", borrar);
    local.removeAll(borrar);
    log.trace("Se van a incluir las siguientes CooperationConditionsMDTO {}", incluir);
    local.addAll(incluir);

    return CollectionUtils.isNotEmpty(borrar) || CollectionUtils.isNotEmpty(incluir);
  }

  /**
   * Comprueba si un CooperationConditionMDTO esta en una lista de CooperationConditionMDTO
   *
   * @param item CooperationConditionMDTO
   * @param lista listado de CooperationConditionMDTO
   * @param esItemLocal indica si el item es un CooperationConditionMDTO local.
   * @return true si el CooperationConditionMDTO esta presente en la lista
   */
  private boolean compararCooperationConditionWithList(final CooperationConditionMDTO item,
      final List<CooperationConditionMDTO> lista, final boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararCooperationConditions(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararCooperationConditions(item, i));
    }
  }

  /**
   * Compara dos CooperationConditionMDTO, no actualiza la copia local
   * 
   * @param remoto CooperationConditionMDTO remota
   * @param local CooperationConditionMDTO local
   * @return true si se detectan diferencias
   */
  public boolean compararCooperationConditions(final CooperationConditionMDTO remoto,
      final CooperationConditionMDTO local) {

    log.trace(" Comparamos las CoopearationConditions ");


    if (Utils.compararFechaYDias(remoto.getStartDate(), local.getStartDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.startDate");
      return Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getEndDate(), local.getEndDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.endDate");
      return Boolean.TRUE;
    }

    if ((CollectionUtils.isEmpty(remoto.getEqfLevel()) && CollectionUtils.isNotEmpty(local.getEqfLevel()))
        || (CollectionUtils.isNotEmpty(remoto.getEqfLevel()) && CollectionUtils.isEmpty(local.getEqfLevel()))
        || (CollectionUtils.isNotEmpty(remoto.getEqfLevel()) && CollectionUtils.isNotEmpty(local.getEqfLevel())
            && !CollectionUtils.isEqualCollection(remoto.getEqfLevel(), local.getEqfLevel()))) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.eqfLevel");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOtherInfo(), local.getOtherInfo())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.otherInfo");
      return Boolean.TRUE;
    }

    final DurationMDTO durationRemoto = Objects.nonNull(remoto.getDuration()) ? remoto.getDuration() : new DurationMDTO();
    final DurationMDTO durationLocal = Objects.nonNull(local.getDuration()) ? local.getDuration() : new DurationMDTO();

    if (!Utils.nullSafeCompareBigDecimal(durationRemoto.getNumberduration(), durationLocal.getNumberduration())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.DurationMDTO.numberDuration");
      return Boolean.TRUE;
    }

    if (!Objects.equals(durationRemoto.getUnit(), durationLocal.getUnit())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.DurationMDTO.unit");
      return Boolean.TRUE;
    }

    final MobilityNumberMDTO numberRemoto = Objects.nonNull(remoto.getMobilityNumber()) ? remoto.getMobilityNumber() : new MobilityNumberMDTO();
    final MobilityNumberMDTO numberLocal = Objects.nonNull(local.getMobilityNumber()) ? local.getMobilityNumber() : new MobilityNumberMDTO();

    if (!Objects.equals(numberRemoto.getNumbermobility(), numberLocal.getNumbermobility())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.MobilityNumberMDTO.numberMobility");
      return Boolean.TRUE;
    }

    if (!Objects.equals(numberRemoto.getVariant(), numberLocal.getVariant())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.MobilityNumberMDTO.variant");
      return Boolean.TRUE;
    }

    final MobilityTypeMDTO typeRemoto = Objects.nonNull(remoto.getMobilityType()) ? remoto.getMobilityType() : new MobilityTypeMDTO();
    final MobilityTypeMDTO typeLocal = Objects.nonNull(local.getMobilityType()) ? local.getMobilityType() : new MobilityTypeMDTO();

    if (!Utils.nullSafeCompareStringIgnoreCase(typeRemoto.getMobilityCategory(), typeLocal.getMobilityCategory())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.MobilityTypeMDTO.mobilityCategory");
      return Boolean.TRUE;
    }

    if (!Utils.nullSafeCompareStringIgnoreCase(typeRemoto.getMobilityGroup(), typeLocal.getMobilityGroup())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.MobilityTypeMDTO.mobilityGroup");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getReceivingPartner())) {
      final boolean compararPartners = compararPartners(remoto.getReceivingPartner(), local.getReceivingPartner());

      if (compararPartners) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getSendingPartner())) {
      final boolean compararPartners = compararPartners(remoto.getSendingPartner(), local.getSendingPartner());

      if (compararPartners) {
        return Boolean.TRUE;
      }
    }

    if (!Objects.equals(remoto.getBlended(), local.getBlended())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.blended");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getLanguageSkill())) {
      final boolean compararLanguageSkill = compararLanguageSkillList(remoto.getLanguageSkill(),
          Objects.nonNull(local.getLanguageSkill()) ? local.getLanguageSkill() : new ArrayList<>());

      if (compararLanguageSkill) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getSubjectAreaList())) {
      final boolean compararSubjectArea = compararSubjectAreaList(remoto.getSubjectAreaList(),
          Objects.nonNull(local.getSubjectAreaList()) ? local.getSubjectAreaList() : new ArrayList<>());

      if (compararSubjectArea) {
        return Boolean.TRUE;
      }
    }

    if (!Objects.equals(remoto.getTerminated(), local.getTerminated())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.terminated");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrderIndex(), local.getOrderIndex())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.orderIndex");
        return Boolean.TRUE;
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de CooperationConditionSubjAreaLangSkillMDTO, no actualiza la lista local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararLanguageSkillList(final List<CooperationConditionSubjAreaLangSkillMDTO> remoto,
      final List<CooperationConditionSubjAreaLangSkillMDTO> local) {

    final List<CooperationConditionSubjAreaLangSkillMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLanguageSkillWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<CooperationConditionSubjAreaLangSkillMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLanguageSkillWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un CooperationConditionSubjAreaLangSkillMDTO esta en una lista de
   * CooperationConditionSubjAreaLangSkillMDTO
   *
   * @param item CooperationConditionSubjAreaLangSkillMDTO
   * @param lista listado de CooperationConditionSubjAreaLangSkillMDTO
   * @param esItemLocal indica si el item es un CooperationConditionSubjAreaLangSkillMDTO local.
   * @return true si el CooperationConditionSubjAreaLangSkillMDTO esta presente en la lista
   */
  private boolean compararLanguageSkillWithList(final CooperationConditionSubjAreaLangSkillMDTO item,
      final List<CooperationConditionSubjAreaLangSkillMDTO> lista, final Boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararLanguageSkills(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararLanguageSkills(item, i));
    }
  }

  /**
   * Compara dos CooperationConditionSubjAreaLangSkillMDTO, no actualiza la copia local
   * 
   * @param remoto CooperationConditionSubjAreaLangSkillMDTO remoto
   * @param local CooperationConditionSubjAreaLangSkillMDTO local
   * @return true si hay diferencias entre los dos objetos
   */
  public boolean compararLanguageSkills(final CooperationConditionSubjAreaLangSkillMDTO remoto,
      final CooperationConditionSubjAreaLangSkillMDTO local) {

    if (!Objects.equals(remoto.getOrderIndexLang(), local.getOrderIndexLang())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionSubjAreaLangSkillMDTO.orderIndexLang");
      return Boolean.TRUE;
    }

    final LanguageSkillMDTO skillRemoto = Objects.nonNull(remoto.getLanguageSkill()) ? remoto.getLanguageSkill() : new LanguageSkillMDTO();
    final LanguageSkillMDTO skillLocal = Objects.nonNull(local.getLanguageSkill()) ? local.getLanguageSkill() : new LanguageSkillMDTO();

    if (!Objects.equals(skillRemoto.getLanguage(), skillLocal.getLanguage())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionMDTO.LamguajeSkillMDTO.numberMovility");
      return Boolean.TRUE;
    }

    if (!Objects.equals(skillRemoto.getCefrLevel(), skillLocal.getCefrLevel())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD,
              "CooperationConditionSubjAreaLangSkillMDTO.LamguajeSkillMDTO.iscedClarification");
      return Boolean.TRUE;
    }

    final SubjectAreaMDTO areaRemoto =
            Objects.nonNull(remoto.getSubjectArea()) ? remoto.getSubjectArea() : new SubjectAreaMDTO();
    final SubjectAreaMDTO areaLocal =
            Objects.nonNull(local.getSubjectArea()) ? local.getSubjectArea() : new SubjectAreaMDTO();

    if (!Objects.equals(areaRemoto.getIscedClarification(), areaLocal.getIscedClarification())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD,
              "CooperationConditionSubjAreaLangSkillMDTO.SubjectAreaMDTO.iscedClarification");
      return Boolean.TRUE;
    }

    if (!Objects.equals(areaRemoto.getIscedCode(), areaLocal.getIscedCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionSubjAreaLangSkillMDTO.SubjectAreaMDTO.iscedCode");
      return Boolean.TRUE;
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de compararSubjectAreaList, no actualiza la lista local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararSubjectAreaList(final List<CooperationConditionSubjectAreaMDTO> remoto,
      final List<CooperationConditionSubjectAreaMDTO> local) {

    final List<CooperationConditionSubjectAreaMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararCoopCondSA(i.getSubjectArea(), remoto, Boolean.FALSE, i.getOrderIndexSubAr()))
          .collect(Collectors.toList()));
    }

    final List<CooperationConditionSubjectAreaMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto) ? remoto.stream()
          .filter(i -> !compararCoopCondSA(i.getSubjectArea(), local, Boolean.TRUE, i.getOrderIndexSubAr())).collect(Collectors.toList())
          : new ArrayList<>();
    }


    return !(borrar.isEmpty() && incluir.isEmpty());
  }


  /**
   * Compara dos SubjectAreaMDTO, no actualiza la copia local
   *
   * @param item item
   * @param lista lista
   * @param esItemLocal esItemLocal
   * @return true si hay diferencias entre los dos objetos
   */
  private boolean compararCoopCondSA(final SubjectAreaMDTO item, final List<CooperationConditionSubjectAreaMDTO> lista,
      final Boolean esItemLocal, final Integer orderIndexSubAr) {

    if (esItemLocal) {
      for (CooperationConditionSubjectAreaMDTO i : lista) {
        if (!compararSubjectArea(i.getSubjectArea(), item)
                || (i.getOrderIndexSubAr() != orderIndexSubAr)) {
          log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionSubjectAreaMDTO.orderIndexSubAr");
          return Boolean.TRUE;
        }
      }
    } else {
      for (CooperationConditionSubjectAreaMDTO i : lista) {
        if (!compararSubjectArea(item, i.getSubjectArea())
                || (i.getOrderIndexSubAr() != orderIndexSubAr)) {
          log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CooperationConditionSubjectAreaMDTO.orderIndexSubAr");
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos SubjectAreaMDTO, no actualiza la copia local
   *
   * @param remoto SubjectAreaMDTO remoto
   * @param local SubjectAreaMDTO local
   * @return true si hay diferencias entre los dos objetos
   */
  public boolean compararSubjectArea(final SubjectAreaMDTO remoto, final SubjectAreaMDTO local) {

    if (Objects.nonNull(remoto)) {
      if (Objects.nonNull(remoto.getIscedClarification())) {
        if (!remoto.getIscedClarification().equals(local.getIscedClarification())) {
          log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SubjectAreaMDTO.iscedClarification");
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }

  @Override
  public IiaMDTO buscaPorIiaRemotoAndIsRemote(final String remoteIiaId, final Boolean isRemote) {
    return iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, isRemote);
  }

  @Override
  public List<String> buscaRemoteIdPorIsRemoteAndInstitutions(final String sendingHei, final String receivingPartner,
      final Boolean isRemote) {
    return iiaDAO.findRemoteIiaIdByIsRemoteAndPartnersInstitution(sendingHei, receivingPartner, isRemote);
  }

  @Override
  @Transactional(readOnly = false)
  public void eliminarIiaRemoto(final String remoteIiaId) {
    iiaDAO.removeIiaByRemoteIiaId(remoteIiaId);
  }

  /**
   * Elimina un iia remoto
   * Ademas en funcion de la parametrizacion de la aplicacion se eliminara el iia local o se desvinculara, en cualquiera de los casos se emitiara una notificacion
   * @param remoteIiaId
   * @param partnerHei
   * @param heiId
   * @param ewpInstance
   * @param iiaDeleteEnabled
   * @return devuelve false si no se aprueba la eliminación del iia remoto
   */
  @Override
  @Transactional(readOnly = false)
  public Boolean processRemoteIiaDeletion(final String remoteIiaId, final String partnerHei, final String heiId, String ewpInstance, boolean iiaDeleteEnabled, EventMDTO event) throws EwpOperationNotAllowedException {
    StringBuilder sb = new StringBuilder();
    Boolean shouldApprove = false;
    IiaMDTO iiaRemoto = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.TRUE);

    if (Objects.isNull(iiaRemoto)) {
      return shouldApprove;
    }
    if (!iiaRemoto.getFirstPartner().getInstitutionId().equals(partnerHei) &&  !iiaRemoto.getSecondPartner().getInstitutionId().equals(partnerHei)) {
      throw new EwpOperationNotAllowedException("No se puede eliminar un iia remoto si no somos parte del iia");
    }
    if (remoteIiaIsAproved(remoteIiaId)) {
      throw new EwpOperationNotAllowedException("No se puede eliminar un iia remoto si esta aprobado");
    }

    iiaDAO.removeIiaByInteropId(iiaRemoto.getInteropId(), partnerHei);
    sb.append("Iia remoto eliminado: ").append(iiaRemoto.getRemoteIiaId());

    IiaMDTO iiaLocal = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.FALSE);
    if(Objects.nonNull(iiaLocal)){
      if (iiaDeleteEnabled) {
        iiaDAO.removeIiaByInteropId(iiaLocal.getInteropId(), ewpInstance);
        sb.append(", Iia local eliminado: ").append(iiaLocal.getInteropId());
      } else {
        iiaDAO.desvincularIiaByInteropId(iiaLocal.getInteropId());
          sb.append(", Iia local desvinculado: ").append(iiaLocal.getInteropId());
      }
      //si era un autogenerado que todavia no habiamos comunicado no tenemos porque notificarlo
      if(iiaLocal.getIsExposed()) {
        notificationService.saveNotificationIiaNotify(partnerHei, iiaLocal.getInteropId(), ewpInstance);
      }else {
        sb.append(", No se notifica el cambio del iia local porque no estaba expuesto");
      }
    }else{
      sb.append(", No hay iia local asociado");
    }

    if(!Strings.isNullOrEmpty(iiaRemoto.getApprovalCopCondHash())){
      shouldApprove = true;
      sb.append(", Notificación de aprobación enviada para revocar aprobación");
    }

    event.setObservations(sb.toString());
    return shouldApprove;
  }

  private void addParam(final Map<Object, Object> queryParams, final Object paramValue, final String paramKey) {
    if (Objects.nonNull(paramValue)) {
      queryParams.put(paramKey, paramValue);
    }
  }

  @Override
  @Transactional(readOnly = false)
  public IiaMDTO save(final IiaMDTO iia) {
    return iiaDAO.save(iia);
  }

  @Override
  @Transactional
  public void updateIiaApprovalByInteropId(final String interopId, final String approvalConditionHash, final Date approvalDate) {
    iiaDAO.updateIiaApprovalByInteropId(interopId, approvalConditionHash, approvalDate);
  }

  @Override
  public IiaMDTO findByInteropId(final String interopId) {
    return iiaDAO.findByInteropId(interopId); // local
  }
  @Override
  public boolean compararHash(final IiasGetResponse.Iia iia, final String remoteIiaId) {
    final String coopCondHas = iia.getIiaHash();

    final IiaMDTO iiaLocal = buscaPorIiaRemotoAndIsRemote(remoteIiaId, Boolean.TRUE);

    return Objects.equals(coopCondHas, iiaLocal.getRemoteCoopCondHash());
  }

  @Override
  @Transactional(readOnly = false)
  public void actualizarApprovalCoopCondHash(final String remoteIiaId, final String conditionsHash) {
    iiaDAO.actualizarApprovalCoopCondHash(remoteIiaId, conditionsHash);
  }

  private IiaApprovalMDTO checkAndSaveApproval(IiaMDTO iia, IiaMDTO iiaRemote, String iiaApiVersion, byte[] remoteMessage, byte[] localMessage) throws EwpIiaSnapshotException {

    if(Objects.isNull(iia) || Objects.isNull(iiaRemote)){
      throw new EwpIiaSnapshotException("No se puede asociar la aprobacion porque no se encuentran los iias");
    }

    if(!estanAprobadasLocalAndRemote(iia, iiaRemote)) {
      return null;
    }

    IiaApprovalMDTO iiaApproval = findLastIiaApprovalByLocalIia(iia.getInteropId());

    //SI ES UNO QUE YA ESTA NO SE DUPLICA
    if(Objects.nonNull(iiaApproval) &&
            (iiaApproval.getLocalHash().equals(iia.getApprovalCopCondHash()) ||
             iiaApproval.getRemoteHash().equals(iiaRemote.getApprovalCopCondHash()))){
      return null;
    }

    IiaApprovalMDTO iiaApprovalMDTO = new IiaApprovalMDTO();

    iiaApprovalMDTO.setLocalIia(iia.getInteropId());
    iiaApprovalMDTO.setRemoteIia(iia.getRemoteIiaId());
    iiaApprovalMDTO.setIiaApiVersion(iiaApiVersion);
    iiaApprovalMDTO.setLocalHash(iia.getApprovalCopCondHash());
    iiaApprovalMDTO.setRemoteHash(iiaRemote.getApprovalCopCondHash());
    iiaApprovalMDTO.setDateIiaApproval(iia.getApprovalDate());
    iiaApprovalMDTO.setLocalMessage(localMessage);
    iiaApprovalMDTO.setRemoteMessage(remoteMessage);
      try {
          iiaApprovalMDTO.setLocalHashv7(IiaHashGenerator.generateIiaHash(IiaHashGenerator.getTextToHash(localMessage, IiaHashGenerator.getXslt7())));
          iiaApprovalMDTO.setRemoteHashv7(IiaHashGenerator.generateIiaHash(IiaHashGenerator.getTextToHash(remoteMessage, IiaHashGenerator.getXslt7())));
      }catch (IOException e){
          e.printStackTrace();
      }
    return saveIiaApproval(iiaApprovalMDTO);

  }

  private static boolean estanAprobadasLocalAndRemote(IiaMDTO iia, IiaMDTO iiaRemote) {
    return Objects.nonNull(iia) && Objects.nonNull(iia.getApprovalDate()) && Objects.nonNull(iia.getApprovalCopCondHash()) &&
            Objects.nonNull(iiaRemote) && Objects.nonNull(iiaRemote.getApprovalDate()) && Objects.nonNull(iiaRemote.getApprovalCopCondHash()) &&
            iia.getApprovalCopCondHash().equals(iia.getRemoteCoopCondHash()) &&
            iiaRemote.getApprovalCopCondHash().equals(iiaRemote.getRemoteCoopCondHash());
  }

  private void bindCoopConditionsToApproval(IiaMDTO iia, IiaApprovalMDTO iiaApprovalMDTO) throws EwpIiaSnapshotException {
    if(Objects.isNull(iiaApprovalMDTO)){
      throw new EwpIiaSnapshotException("No se puede asociar las condiciones de cooperacion a la aprobacion porque no existe la aprobacion");
    }

    if(Objects.isNull(iia) || CollectionUtils.isEmpty(iia.getCooperationConditions())){
      throw new EwpIiaSnapshotException("No se puede asociar las condiciones de cooperacion a la aprobacion porque no se encuentran condiciones de cooperacion");
    }

    for(CooperationConditionMDTO cooperationCondition : iia.getCooperationConditions()){
      if(Objects.isNull(cooperationCondition.getIiaApproval())){
        updateCoopCondById(cooperationCondition.getId(), iiaApprovalMDTO.getId());
      }
    }
  }


  public IiaApprovalMDTO saveIiaApproval(final IiaApprovalMDTO iiaApprovalMDTO) {
    return iiaDAO.saveIiaApproval(iiaApprovalMDTO);
  }

  private void updateCoopCondById(final String iiaId, final String approvalId) {
    iiaDAO.updateCoopCondById(iiaId, approvalId);
  }

  @Transactional(readOnly = false)
  public IiaApprovalMDTO findLastIiaApprovalByLocalIia(String localIia) {
    return iiaDAO.findLastIiaApprovalByLocalIia(localIia);
  }

  @Override
  @Transactional(readOnly = false)
  public void takeSnapshot(String remoteIiaId, EventMDTO event, String iiaApiVersion, String ewpInstance, byte[] remoteMessage) throws EwpIiaSnapshotException, IOException {
    try {
      final IiaMDTO iiaLocal = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.FALSE);
      final IiaMDTO iiaRemote = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.TRUE);

      if(Objects.isNull(iiaLocal) || Objects.isNull(iiaRemote)){
        return;
      }

      byte[] localMessage = getIiaResponseBytesAndUpdateHash(ewpInstance, iiaLocal);

      IiaApprovalMDTO iiaApprovalMDTO = checkAndSaveApproval(iiaLocal, iiaRemote, iiaApiVersion, remoteMessage, localMessage);
      if (Objects.nonNull(iiaApprovalMDTO)) {
        bindCoopConditionsToApproval(iiaLocal, iiaApprovalMDTO);
        event.setObservations("Snapshot taken");
      }

    }catch (Exception e) {
      log.error("Error taking snapshot for remote iia id: " + remoteIiaId + ": ", e);
      event.setStatus("KO");
      event.setObservations("Error taking snapshot for remote iia id: " + remoteIiaId + ": " + e.getMessage());
      eventService.save(event);
      throw e;
    }
  }

  @Override
  @Transactional(readOnly = false)
  public void takeSnapshotWithProvidedRemoteMessageInEvent(String remoteIiaId, EventMDTO event, String iiaApiVersion, String ewpInstance) throws EwpIiaSnapshotException, IOException{
    try {
      final IiaMDTO iiaLocal = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.FALSE);
      final IiaMDTO iiaRemote = iiaDAO.findIiaByRemoteIiaIdAndIsRemote(remoteIiaId, Boolean.TRUE);

      if(Objects.isNull(iiaLocal) || Objects.isNull(iiaRemote)){
        return;
      }

      byte[] localMessage = getIiaResponseBytesAndUpdateHash(ewpInstance, iiaLocal);

      IiaApprovalMDTO iiaApprovalMDTO = checkAndSaveApproval(iiaLocal, iiaRemote, iiaApiVersion, event.getMessage(), localMessage);
      if (Objects.nonNull(iiaApprovalMDTO)) {
        bindCoopConditionsToApproval(iiaLocal, iiaApprovalMDTO);
        event.setObservations("Snapshot taken");
      }

    }catch (Exception e) {
      log.error("Error taking snapshot for remote iia id: " + remoteIiaId + ": ", e);
      event.setStatus("KO");
      event.setObservations("Error taking snapshot for remote iia id: " + remoteIiaId + ": " + e.getMessage());
      eventService.save(event);
      throw e;
    }
  }


  private byte[] getIiaResponseBytesAndUpdateHash(String heiId, IiaMDTO iia) throws IOException {

    //importante: ordenamos la lista de subject area language skill para que coincidan con los de la respuesta que damos en el get
    for (CooperationConditionMDTO cc : iia.getCooperationConditions()) {
      cc.getLanguageSkill().sort(null);
    }

    final IiasGetResponse respuesta = new IiasGetResponse();
    respuesta.getIia().addAll(iiaConverter.convertToIias(heiId, Collections.singletonList(iia)));
    log.debug("Updating hash when taking snapshot for local iia: " + iia.getInteropId());
    updateCoopCondHash(respuesta.getIia(), heiId);
    iia.setRemoteCoopCondHash(respuesta.getIia().get(0).getIiaHash());
    byte[] message = Utils.getByteArrayFromObject(respuesta, false);
    log.debug("IIA : " + new String(message));
    return message;
  }

  private byte[] getRemoteIiaResponseBytesAndUpdateHash(String heiId, IiaMDTO iia, String localIiaId) throws IOException {
    final IiasGetResponse respuesta = new IiasGetResponse();
    respuesta.getIia().addAll(iiaConverter.convertToIias(heiId, Collections.singletonList(iia)));
    // cambiamos el orden de firstPartner y secondPartner para que coincida con el orden de la respuesta
    List<IiasGetResponse.Iia.Partner> reordenada = new ArrayList<>();
    reordenada.add(respuesta.getIia().get(0).getPartner().get(1));
    reordenada.add(respuesta.getIia().get(0).getPartner().get(0));
    respuesta.getIia().get(0).getPartner().clear();
    respuesta.getIia().get(0).getPartner().addAll(reordenada);
    //cambiamos el id por el del registro que representa el iia local porque nos viene mapeado con el que no es.
    respuesta.getIia().get(0).getPartner().get(1).setIiaId(localIiaId);

    //como ahora el orden del id afecta al hash ya no podemos hacer el truco de darle la vuelta despues de convertirlo y tenemos que recalcularlo
    iia.setRemoteCoopCondHash(IiaHashGenerator.generateIiaHash(
            IiaHashGenerator.getTextToHash(Utils.getByteArrayFromObject(respuesta, false), IiaHashGenerator.getXslt7())));
    return Utils.getByteArrayFromObject(respuesta, false);
  }

  /**
   * Procesa la regresion de un iia remoto
   * detecta si el partner ha revertido a una snapshot, si es asi, revertimos el nuestro a dicha snapshot y tambien revertimos la aprobacion que le habiamos hecho.
   * @param remoto
   * @param antiguoHashIiaRemoto
   */
  @Override
  @Transactional(readOnly = false)
  public String proccesRegression(IiasGetResponse.Iia remoto, String antiguoHashIiaRemoto) {
    //si no habia copia remota no hay nada a lo que regresar
    if(StringUtils.isEmpty(antiguoHashIiaRemoto)){
      return null;
    }

    //el propietario del iia siempre es el primer partner por lo que en el remoto el primer partner tendra el schac del socio y su iiaId.
    String remoteIiaId = remoto.getPartner().get(0).getIiaId();

    IiaMDTO iiaLocal = buscaPorIiaRemotoAndIsRemote(remoteIiaId, Boolean.FALSE);
    if(Objects.isNull(iiaLocal)){
        return null;
    }

    IiaMDTO iiaRemote = buscaPorIiaRemotoAndIsRemote(remoteIiaId, Boolean.TRUE);
    IiaApprovalMDTO ultimaSnapshot = findLastIiaApprovalByIiaRemoteIiaId(iiaLocal.getRemoteIiaId());

    if(detectRegression(remoto.getIiaHash(), ultimaSnapshot, antiguoHashIiaRemoto) && Objects.nonNull(iiaLocal)){
      registerRegression(remoteIiaId, remoto.getIiaHash(), remoto.getPartner().get(0).getHeiId());
      boolean reverted = recoverIiaSnapshot(iiaLocal, ultimaSnapshot);

      if(detectApprovalRegression(iiaRemote, ultimaSnapshot)){
        //hemos detectado que el partner ha revertido a la ultima aprobacion por lo que actualizamos para volver a la ultima aprobacion
        //esto lo que hace es eliminar la aprobacion emitida durante la negociacion tras la snapshot
        iiaDAO.updateIiaApprovalByInteropId(iiaRemote.getInteropId(), ultimaSnapshot.getRemoteHash(), ultimaSnapshot.getDateIiaApproval());
        notificationService.saveNotificationIiaApprovalNotify(remoto.getPartner().get(0).getHeiId(), iiaLocal.getRemoteIiaId(), remoto.getPartner().get(1).getHeiId());
        return reverted ? "Reverted to snapshot and remote approval reverted" : "Remote approval reverted";
      }
      return reverted ? "Reverted to snapshot" : "Regression detected but no action taken";
    }
    return null;
  }

  /**
   * Detecta si ya habiamos aprobado el iia remoto despues de la ultima snapshot para revertir dicha aprobacion si es necesario
   * Para ello verifica si el hash de aprobacion del registro remoto (ultima aprobacion emitida) no coincide con el ultimo hash de aprobacion
   * @param iiaRemote registro del iia remoto que teniamos almacenado (ya actualizado tras procesar el cnr)
   * @param ultimaSnapshot ultima snapshot tomada del iia
   * @return true si se detecta que se debe revertir la aprobacion
   */
  private boolean detectApprovalRegression(IiaMDTO iiaRemote, IiaApprovalMDTO ultimaSnapshot) {
    return !Objects.equals(ultimaSnapshot.getRemoteHash(), iiaRemote.getApprovalCopCondHash());
  }

  /**
   * Detecta si el iia remoto ha revertido a la ultima aprobacion
   * Para ello verificamos que haya una aprobacion a la que volver, que el hash que acabamos de recibir es distinto del ultimo guardado y que el hash que acabamos de recibir es igual al ultimo hash aprobado
   * @param hashIiaRemotoRecibido hash del iia remoto recibido en la llamada realizada en el momento
   * @param ultimaSnapshot ultima snapshot tomada del iia
   * @param antiguoHashIiaRemoto hash del iia remoto que teniamos antes de procesar el cnr
   * @return true si se detecta que el partner ha revertido a la ultima aprobacion
   */
  private boolean detectRegression(String hashIiaRemotoRecibido, IiaApprovalMDTO ultimaSnapshot, String antiguoHashIiaRemoto) {
    return Objects.nonNull(ultimaSnapshot) && !Objects.equals(antiguoHashIiaRemoto, hashIiaRemotoRecibido) && Objects.equals(ultimaSnapshot.getRemoteHash(), hashIiaRemotoRecibido);
  }

  /**
   * Revierte nuestro IIA local a la ultima snapshot.
   * Se lanza cuando se detecta que el partner ha revertido a una snapshot
   * Si no habiamos hecho cambios en las condiciones de cooperacion no se recupera la snapshot y se devuelve false
   * @param iiaARecuperar
   * @param iiaApprovalMDTO
   * @return true si se ha recuperado la snapshot false si no se ha recuperado porque no habiamos hecho cambios
   */
  private boolean recoverIiaSnapshot(IiaMDTO iiaARecuperar, IiaApprovalMDTO iiaApprovalMDTO) {
    if(iiaARecuperar.getCooperationConditions().stream().noneMatch(cc-> cc.getIiaApproval() == null)){
      return false;
    }

    // borramos las ccs que no estan asociadas a una aprobacion
    iiaARecuperar.getCooperationConditions().removeIf(cc-> cc.getIiaApproval() == null);
    // cambiamos el hash que enviamos para que sea el hash de la snapshot recuperada
    iiaARecuperar.setRemoteCoopCondHash(iiaApprovalMDTO.getLocalHash());
    iiaARecuperar.setModifyDate(new Date());
    iiaDAO.save(iiaARecuperar);
    registerRegression(iiaARecuperar.getInteropId(), iiaARecuperar.getRemoteCoopCondHash(), iiaARecuperar.getFirstPartner().getInstitutionId());
    notificationService.saveNotificationIiaNotify(iiaARecuperar.getSecondPartner().getInstitutionId(), iiaARecuperar.getInteropId(), iiaARecuperar.getFirstPartner().getInstitutionId());
    return true;
  }

  /**
   * busca la ultima snapshot tomada para un iia dado su remote iia id
   * @param iiaRemoteIiaId
   * @return
   */
  private IiaApprovalMDTO findLastIiaApprovalByIiaRemoteIiaId(String iiaRemoteIiaId) {
    return iiaDAO.findLastIiaApprovalByIiaRemoteIiaId(iiaRemoteIiaId);
  }

  private boolean remoteIiaIsAproved(String iiaRemoteIiaId) {
    return iiaDAO.RemoteIiaIsAproved(iiaRemoteIiaId);
  }

  private void registerRegression(String iiaId, String iiaHash, String schac){
    IiaRegressionsMDTO regression = new IiaRegressionsMDTO();
    regression.setIiaId(iiaId);
    regression.setIiaHash(iiaHash);
    regression.setOwnerSchac(schac);
    regression.setRegressionDate(new Date());
    iiaRegressionsDAO.saveRegression(regression);
  }
}
