package es.minsait.ewpcv.service.impl;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.minsait.ewpcv.repository.dao.INotificationDAO;
import es.minsait.ewpcv.repository.model.notificacion.CnrType;
import es.minsait.ewpcv.repository.model.notificacion.NotificationMDTO;
import es.minsait.ewpcv.repository.model.notificacion.NotificationTypes;
import es.minsait.ewpcv.service.api.INotificationService;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Notification service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Service
@Transactional
@Slf4j
public class NotificationServiceImpl implements INotificationService {

  private INotificationDAO notificationDAO;


  @Autowired
  public NotificationServiceImpl(final INotificationDAO notificationDAO) {
    this.notificationDAO = notificationDAO;
  }

  @Override
  public void saveNotificationIiaRefresh(final String notifierHeiId, final String iiaId, final String ownerHeiId) {
    final NotificationMDTO notification =
        createNotificationMDTO(NotificationTypes.IIA, CnrType.REFRESH, notifierHeiId, iiaId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationIiaNotify(final String notifierHeiId, final String iiaId, final String ownerHeiId) {
    final NotificationMDTO notification =
            createNotificationMDTO(NotificationTypes.IIA, CnrType.NOTIFY, notifierHeiId, iiaId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationIiaApprovalRefresh(final String notifierHeiId, final String iiaId,
      final String ownerHeiId) {
    final NotificationMDTO notification =
        createNotificationMDTO(NotificationTypes.IIA_APPROVAL, CnrType.REFRESH, notifierHeiId, iiaId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationIiaApprovalNotify(final String notifierHeiId, final String iiaId,
                                                 final String ownerHeiId) {
    final NotificationMDTO notification =
            createNotificationMDTO(NotificationTypes.IIA_APPROVAL, CnrType.NOTIFY, notifierHeiId, iiaId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationOmobilityLasRefresh(final String sendingHeiId, final List<String> mobilityIdList,
      final String ewpInstance) {
    mobilityIdList.stream().forEach(mobilityId -> notificationDAO.saveNotification(
        createNotificationMDTO(NotificationTypes.LA, CnrType.REFRESH, sendingHeiId, mobilityId, ewpInstance)));
  }

  @Override
  public List<NotificationMDTO> reservarNotificacionesRecibidas(final int retriesDelay, final int blockSize,
      final NotificationTypes tipoObjeto) {
    final List<String> distinctIds = notificationDAO
        .findDistinctNotificationsByNotificationTypeAndCnrType(CnrType.REFRESH, tipoObjeto, retriesDelay, blockSize);
    if (CollectionUtils.isNotEmpty(distinctIds)) {
      notificationDAO.reservarNotificaciones(distinctIds, CnrType.REFRESH, tipoObjeto);
    }
    final List<NotificationMDTO> notificaciones = new ArrayList<>();
    for (final String id : distinctIds) {
      notificaciones.add(notificationDAO.findLastNotification(id, CnrType.REFRESH, tipoObjeto));
    }
    return notificaciones;
  }

  @Override
  public void liberarNotificaciones(final NotificationMDTO notificacion, String lastError) {
    notificationDAO.liberarNotificaciones(notificacion.getChangedElementIds(), notificacion.getCnrType(),
        notificacion.getType(), lastError);
  }

  @Override
  public void setNotProcessingAndNotRepeteable(final NotificationMDTO notificacion, final String lastError) {
    notificationDAO.notificacionNoReintentable(notificacion.getChangedElementIds(),
        notificacion.getCnrType(), notificacion.getType(), lastError);
  }

  @Override
  public List<NotificationMDTO> reservarNotificacionesEnviar(final NotificationTypes tipoObjeto, final int retriesDelay,
      final int blockSize) {
    final List<NotificationMDTO> notificaciones = new ArrayList<>();
    List<String> distinctIds = new ArrayList<>();

    // si vamos a notificar IIA_APPROVALs, debemos notificar solo los que no tengan notificaciones pendientes de enviar
    if (tipoObjeto.equals(NotificationTypes.IIA_APPROVAL)) {
      distinctIds =
          notificationDAO.findIiasApprovalNotificationWhitoutIiaNotificationPending(CnrType.NOTIFY, retriesDelay, blockSize);
    } else {
      distinctIds = notificationDAO.findDistinctNotificationsByNotificationTypeAndCnrType(CnrType.NOTIFY, tipoObjeto,
          retriesDelay, blockSize);
    }

    notificationDAO.reservarNotificaciones(distinctIds, CnrType.NOTIFY, tipoObjeto);
    for (final String id : distinctIds) {
      notificaciones.add(notificationDAO.findLastNotification(id, CnrType.NOTIFY, tipoObjeto));
    }
    return notificaciones;
  }

  private NotificationMDTO createNotificationMDTO(final NotificationTypes tipoObjeto, final CnrType cnrType,
      final String notifierHeiId, final String elementId, final String ownerHeiId) {
    final NotificationMDTO notification = new NotificationMDTO();
    notification.setType(tipoObjeto);
    notification.setHeiId(notifierHeiId);
    notification.setChangedElementIds(elementId);
    notification.setNotificationDate(new Date());
    notification.setCnrType(cnrType);
    notification.setRetries(0);
    notification.setProcessing(false);
    notification.setOwnerHeiId(ownerHeiId);
    return notification;
  }

  @Override
  public void eliminarNotificacion(final NotificationMDTO notificacion) {
    notificationDAO.eliminarNotificacion(notificacion);
  }

  @Override
  public void purgarNotificaciones(final Long maxProcessingTime) {
    final List<NotificationMDTO> notificaciones =
        notificationDAO.obtenerNotificacionesProcesandoseByTime(maxProcessingTime);

    log.debug("Se van a purgar {} notificaciones", notificaciones.size());

    for (final NotificationMDTO notificacion : notificaciones) {
      log.debug("Se verifican las notifiaciones");
      notificacion.setProcessing(Boolean.FALSE);
      notificationDAO.saveNotification(notificacion);
    }
    notificationDAO.marcaNotificacionesErroneasNoReintentables();
  }

  @Override
  public void saveNotificationOmobilityRefresh(final String sendingHeiId, final String id, final String instance) {
    final NotificationMDTO notification =
        createNotificationMDTO(NotificationTypes.OMOBILITY, CnrType.REFRESH, sendingHeiId, id, instance);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationImobilityRefresh(final String receivingHeiId, final String omobiityId,
      final String ownerHeiId) {
    final NotificationMDTO notification =
        createNotificationMDTO(NotificationTypes.IMOBILITY, CnrType.REFRESH, receivingHeiId, omobiityId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationTorRefresh(final String receivingHeiId, final String omobiityId,
      final String ownerHeiId) {
    final NotificationMDTO notification = createNotificationMDTO(NotificationTypes.IMOBILITY_TOR, CnrType.REFRESH,
        receivingHeiId, omobiityId, ownerHeiId);
    notificationDAO.saveNotification(notification);
  }

  @Override
  public void saveNotificationOmobilityLA(String sendingHeiId, String omobiityId, String receivingHeiId) {
    final NotificationMDTO notification =
        createNotificationMDTO(NotificationTypes.LA, CnrType.NOTIFY, receivingHeiId, omobiityId, sendingHeiId);
    notificationDAO.saveNotification(notification);
  }
  @Override
  public boolean existsPendingNotification(String changedElementIds, NotificationTypes tipoObjeto, String heiId){
    return notificationDAO.existsPendingNotification(changedElementIds , tipoObjeto, heiId);
  }
}
