package es.minsait.ewpcv.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.minsait.ewpcv.repository.dao.IAuditConfigDAO;
import es.minsait.ewpcv.repository.model.config.AuditConfigMDTO;
import es.minsait.ewpcv.service.api.IAuditConfigService;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Audit config service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional
@Slf4j
public class AuditConfigServiceImpl implements IAuditConfigService {

  @Autowired
  private IAuditConfigDAO iAuditConfigDAO;

  @Override
  public AuditConfigMDTO findByName(final String name) {
    return iAuditConfigDAO.findByName(name);
  }
}
