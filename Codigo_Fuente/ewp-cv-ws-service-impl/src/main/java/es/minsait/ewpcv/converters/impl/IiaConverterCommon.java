package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.model.iia.DurationUnitVariants;
import es.minsait.ewpcv.model.iia.MobilityNumberVariants;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.model.iia.*;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * The type Iia converter common.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
public class IiaConverterCommon {

  private static final String IIAS_GET_RESPONSE_NAMESPACE =
          "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd";

  private static final int MOB_PER_YEAR_NOT_DEFINED = 1;
  private static final String LANGUAGE_NOT_DEFINED = "en";
  private static final String CERF_LEVEL_NOT_DEFINED = "B2";
  private static final String ISCED_CODE_NOT_DEFINED = "1111";

  public static byte[] iiaClassToBytes(final IiasGetResponse.Iia iia, final String charset) {
    try{
      IiasGetResponse iiasGetResponse = new IiasGetResponse();
      iiasGetResponse.getIia().add(iia);
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      final JAXBContext jaxbContext = JAXBContext.newInstance(iiasGetResponse.getClass());
      final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, charset);
      jaxbMarshaller.marshal(iiasGetResponse, baos);
      return baos.toByteArray();
    }catch (final Exception e){
      log.error(e.getMessage());
      e.printStackTrace();
      return null;
    }
  }

  public static IiasGetResponse.Iia convertSingleIia(final String hei_id, final IiaMDTO iia, final IIiaDAO iIiaDAO) {
    final IiasGetResponse.Iia converted = new IiasGetResponse.Iia();

    converted.getPartner().add(convertToPartner(iia, iia.getFirstPartner(), hei_id));
    converted.getPartner().add(convertToPartner(iia, iia.getSecondPartner(), hei_id));

    final Comparator<? super IiasGetResponse.Iia.Partner> heiIdComparator =
            (Comparator<IiasGetResponse.Iia.Partner>) (lhs, rhs) -> {
              if (rhs.getHeiId().equals(hei_id)) {
                return 1;
              }
              if (lhs.getHeiId().equals(hei_id)) {
                return -1;
              }
              return 0;
            };
    converted.getPartner().sort(heiIdComparator);

    IiaApprovalMDTO lastApproval = iIiaDAO.findLastIiaApprovalByLocalIia(iia.getInteropId());

    converted.setCooperationConditions(convertToCooperationConditions(iia.getCooperationConditions(), Boolean.TRUE, Objects.nonNull(lastApproval) ? lastApproval.getId() : null));
    if (Objects.nonNull(iia.getApprovalDate()) &&
            Objects.nonNull(iia.getRemoteIiaId()) &&
            Objects.nonNull(iIiaDAO.obtenerApprovalDatePorRemoteIiaIdAndIsRemote1(iia.getRemoteIiaId()))){
        converted.setInEffect(true);
    }

    converted.setPdfFile(iia.getPdfId());

    try{
      converted.setIiaHash(IiaHashGenerator.generateIiaHash(
              IiaHashGenerator.getTextToHash(
                      iiaClassToBytes(converted, "UTF-8"), IiaHashGenerator.getXslt7())));
    }catch (final Exception e){
      log.error(e.getMessage());
      e.printStackTrace();
    }

    return converted;
  }

  public static IiasGetResponse.Iia.CooperationConditions convertToCooperationConditions(
      final List<CooperationConditionMDTO> cooperationConditions, final boolean withContacts, String lastApprovalId) {

    //solo servimos las condiciones de cooperacion que no estan asociadas a ningun approval o las que esten asociadas al último approval
    //primero miramos si hay alguna negociacion en curso, sin approval en cuyo caso solo servimos esas
    if (cooperationConditions.stream().anyMatch(cc -> Objects.isNull(cc.getIiaApproval()))) {
      cooperationConditions.removeIf(cc -> Objects.nonNull(cc.getIiaApproval()));
    }else{
      //si hay approval, eliminamos las condiciones de cooperacion que no esten asociadas al ultimo approval
      cooperationConditions.removeIf(cc -> Objects.nonNull(cc.getIiaApproval()) && !cc.getIiaApproval().equals(lastApprovalId));
    }

    final Map<String, List<CooperationConditionMDTO>> ccMap = cooperationConditions.stream()
        .collect(Collectors.groupingBy(cc -> cc.getMobilityType().getMobilityGroup().toLowerCase() + "-"
            + cc.getMobilityType().getMobilityCategory().toLowerCase()));

    final IiasGetResponse.Iia.CooperationConditions converted = new IiasGetResponse.Iia.CooperationConditions();

    if (ccMap.containsKey("staff-teaching")) {
      converted.getStaffTeacherMobilitySpec().addAll(ccMap.get("staff-teaching").stream()
          .map(cc -> convertToStaffTeacherMobilitySpec(cc, withContacts)).collect(Collectors.toList()));
    }
    if (ccMap.containsKey("staff-training")) {
      converted.getStaffTrainingMobilitySpec().addAll(ccMap.get("staff-training").stream()
          .map(cc -> convertToStaffTrainingMobilitySpec(cc, withContacts)).collect(Collectors.toList()));
    }
    if (ccMap.containsKey("student-studies")) {
      converted.getStudentStudiesMobilitySpec().addAll(ccMap.get("student-studies").stream()
          .map(cc -> convertToStudentStudiesMobilitySpec(cc, withContacts)).collect(Collectors.toList()));
    }
    if (ccMap.containsKey("student-training")) {
      converted.getStudentTraineeshipMobilitySpec().addAll(ccMap.get("student-training").stream()
              .map(cc -> convertToStudentTraineeshipMobilitySpec(cc, withContacts)).collect(Collectors.toList()));
    }
    if (!CollectionUtils.isEmpty(cooperationConditions)) {
      Boolean terminated = cooperationConditions.get(0).getTerminated();
      converted.setTerminatedAsAWhole((Objects.nonNull(terminated) && terminated) ? terminated : null);
    }

    return converted;
  }

  private static IiasGetResponse.Iia.Partner convertToPartner(final IiaMDTO iia, final IiaPartnerMDTO partner,
      final String hei_id) {
    IiasGetResponse.Iia.Partner converted = null;
    if (Objects.nonNull(partner)) {
      converted = new IiasGetResponse.Iia.Partner();
      converted.setHeiId(partner.getInstitutionId());
      converted.setOunitId(partner.getOrganizationUnitId());

      converted
              .setIiaCode(hei_id.equalsIgnoreCase(partner.getInstitutionId()) ? iia.getIiaCode() : iia.getRemoteIiaCode());

      converted.setIiaId(hei_id.equalsIgnoreCase(partner.getInstitutionId()) ? iia.getInteropId() : iia.getRemoteIiaId());

      converted.getContact()
              .addAll(partner.getContacts().stream().map(CommonsConverter::convertToContact).collect(Collectors.toList()));

      converted.setSigningContact(CommonsConverter.convertToContact(partner.getSignerPersonId()));
      if (Objects.nonNull(partner.getSigningDate())) {
        converted.setSigningDate(CommonsConverter.convertToGregorianCalendar(partner.getSigningDate()));
      }
    }
    return converted;
  }

  private static StaffTeacherMobilitySpec convertToStaffTeacherMobilitySpec(final CooperationConditionMDTO cc,
      final boolean withContacts) {
    final StaffTeacherMobilitySpec conv = new StaffTeacherMobilitySpec();
    addToStaffMobilitySpecification(conv, cc, withContacts);
    if(conv.getRecommendedLanguageSkill().isEmpty()){
      conv.getRecommendedLanguageSkill().add(languageSkillNotDefined());
    }else{
      for (RecommendedLanguageSkill rls: conv.getRecommendedLanguageSkill()) {
        if(rls.getLanguage() == null || rls.getCefrLevel() == null){
          rls.setLanguage(LANGUAGE_NOT_DEFINED);
          rls.setCefrLevel(CERF_LEVEL_NOT_DEFINED);
          rls.setNotYetDefined(true);
        }
      }
    }
    return conv;
  }

  private static StaffTrainingMobilitySpec convertToStaffTrainingMobilitySpec(final CooperationConditionMDTO cc,
      final boolean withContacts) {
    final StaffTrainingMobilitySpec conv = new StaffTrainingMobilitySpec();
    addToStaffMobilitySpecification(conv, cc, withContacts);
    return conv;
  }

  private static StudentStudiesMobilitySpec convertToStudentStudiesMobilitySpec(final CooperationConditionMDTO cc,
      final boolean withContacts) {
    final StudentStudiesMobilitySpec conv = new StudentStudiesMobilitySpec();
    addToStudentMobilitySpecification(conv, cc, withContacts);
    conv.setBlended(Objects.nonNull(cc.getBlended()) ? cc.getBlended() : false);
    conv.getEqfLevel().addAll(cc.getEqfLevel());
    if(conv.getRecommendedLanguageSkill().isEmpty()){
      conv.getRecommendedLanguageSkill().add(languageSkillNotDefined());
    }else{
      for (RecommendedLanguageSkill rls: conv.getRecommendedLanguageSkill()) {
        if(rls.getLanguage() == null || rls.getCefrLevel() == null){
          rls.setLanguage(LANGUAGE_NOT_DEFINED);
          rls.setCefrLevel(CERF_LEVEL_NOT_DEFINED);
          rls.setNotYetDefined(true);
        }
      }
    }
    return conv;
  }

  private static StudentTraineeshipMobilitySpec convertToStudentTraineeshipMobilitySpec(
      final CooperationConditionMDTO cc, final boolean withContacts) {
    final StudentTraineeshipMobilitySpec conv = new StudentTraineeshipMobilitySpec();
    addToStudentMobilitySpecification(conv, cc, withContacts);
    conv.setBlended(Objects.nonNull(cc.getBlended()) ? cc.getBlended() : false);
    conv.getEqfLevel().addAll(cc.getEqfLevel());
    return conv;
  }

  private static RecommendedLanguageSkill languageSkillNotDefined(){
    RecommendedLanguageSkill recommendedLanguageSkill = new RecommendedLanguageSkill();
    recommendedLanguageSkill.setNotYetDefined(true);
    recommendedLanguageSkill.setLanguage(LANGUAGE_NOT_DEFINED);
    recommendedLanguageSkill.setCefrLevel(CERF_LEVEL_NOT_DEFINED);
    return recommendedLanguageSkill;
  }

  private static void addToMobilitySpecification(final MobilitySpecification conv, final CooperationConditionMDTO cc,
      final boolean withContacts) {

    conv.setReceivingHeiId(cc.getReceivingPartner().getInstitutionId());
    if (Objects.nonNull(cc.getReceivingPartner().getOrganizationUnitId())) {
      conv.setReceivingOunitId(cc.getReceivingPartner().getOrganizationUnitId());
    }

    if (withContacts) {
      conv.getReceivingContact().addAll(cc.getReceivingPartner().getContacts().stream()
          .map(CommonsConverter::convertToContact).collect(Collectors.toList()));
      conv.getSendingContact().addAll(cc.getSendingPartner().getContacts().stream()
          .map(CommonsConverter::convertToContact).collect(Collectors.toList()));
    }

    conv.setSendingHeiId(cc.getSendingPartner().getInstitutionId());

    if (Objects.nonNull(cc.getSendingPartner().getOrganizationUnitId())) {
      conv.setSendingOunitId(cc.getSendingPartner().getOrganizationUnitId());
    }

    List<String> academicYears = extractYears(cc.getStartDate(), cc.getEndDate());
    conv.setReceivingFirstAcademicYearId(academicYears.get(0));
    conv.setReceivingLastAcademicYearId(academicYears.get(academicYears.size()-1));
    MobilitySpecification.MobilitiesPerYear mobilitiesPerYear = new MobilitySpecification.MobilitiesPerYear();
    if (cc.getMobilityNumber() != null){
      mobilitiesPerYear.setValue(BigInteger.valueOf(cc.getMobilityNumber().getNumbermobility()));
    }else{
      mobilitiesPerYear.setValue(BigInteger.valueOf(MOB_PER_YEAR_NOT_DEFINED));
      mobilitiesPerYear.setNotYetDefined(true);
    }
    conv.setMobilitiesPerYear(mobilitiesPerYear);
    conv.getRecommendedLanguageSkill().addAll(toRecommendedLanguageSkill(cc));
    conv.getSubjectArea().addAll(toSubjectAreaList(cc));
    conv.setOtherInfoTerms(cc.getOtherInfo());

  }

  private static List<RecommendedLanguageSkill> toRecommendedLanguageSkill(
      final CooperationConditionMDTO cc) {

    cc.getLanguageSkill().sort(Comparator.comparing(CooperationConditionSubjAreaLangSkillMDTO::getOrderIndexLang));

    final List<RecommendedLanguageSkill> converted = new ArrayList<>();
    for (final CooperationConditionSubjAreaLangSkillMDTO languageSkillMDTO : cc.getLanguageSkill()) {
      final RecommendedLanguageSkill recommendedLanguageSkill =
          new RecommendedLanguageSkill();
      if (Objects.nonNull(languageSkillMDTO.getLanguageSkill())) {
        recommendedLanguageSkill.setCefrLevel(languageSkillMDTO.getLanguageSkill().getCefrLevel());
        recommendedLanguageSkill.setLanguage(languageSkillMDTO.getLanguageSkill().getLanguage());
      }
      recommendedLanguageSkill.setSubjectArea(toSubjectArea(languageSkillMDTO.getSubjectArea()));
      converted.add(recommendedLanguageSkill);
    }
    return converted;
  }

  private static List<SubjectArea> toSubjectAreaList(final CooperationConditionMDTO cc) {

    cc.getSubjectAreaList().sort(Comparator.comparing(CooperationConditionSubjectAreaMDTO::getOrderIndexSubAr));

    final List<SubjectArea> converted = new ArrayList<>();

    for (final CooperationConditionSubjectAreaMDTO cCSubjectAreaMDTO : cc.getSubjectAreaList()) {

      final SubjectArea recommendedSA = new SubjectArea();
      if (cCSubjectAreaMDTO.getSubjectArea() != null) {
        recommendedSA.setIscedClarification(cCSubjectAreaMDTO.getSubjectArea().getIscedClarification());
        SubjectArea.IscedFCode iscedFCode = new SubjectArea.IscedFCode();
          if (cCSubjectAreaMDTO.getSubjectArea().getIscedCode().length() < 4) {
            iscedFCode.setValue(ISCED_CODE_NOT_DEFINED);
            iscedFCode.setV6Value(cCSubjectAreaMDTO.getSubjectArea().getIscedCode());
          } else {
              iscedFCode.setValue(cCSubjectAreaMDTO.getSubjectArea().getIscedCode());
          }
          recommendedSA.setIscedFCode(iscedFCode);
      }

      converted.add(recommendedSA);
    }

    return converted;
  }

  private static SubjectArea toSubjectArea(final SubjectAreaMDTO subjectAreaMDTO) {
    SubjectArea converted = null;
    if (Objects.nonNull(subjectAreaMDTO)) {
      converted = new SubjectArea();
      converted.setIscedClarification(subjectAreaMDTO.getIscedClarification());
      SubjectArea.IscedFCode iscedFCode = new SubjectArea.IscedFCode();
      if (subjectAreaMDTO.getIscedCode().length() < 4) {
        iscedFCode.setValue(ISCED_CODE_NOT_DEFINED);
        iscedFCode.setV6Value(subjectAreaMDTO.getIscedCode());
      } else {
        iscedFCode.setValue(subjectAreaMDTO.getIscedCode());
      }
      converted.setIscedFCode(iscedFCode);
    }
    return converted;
  }

  private static List<String> extractYears(final Date startDate, final Date endDate) {
    final List<String> years = new ArrayList<>();
    final int startMonth =
        Instant.ofEpochMilli(startDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    final int endMonth =
        Instant.ofEpochMilli(endDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getMonthValue();
    int startYear = startMonth >= Month.SEPTEMBER.getValue()
        ? Instant.ofEpochMilli(startDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getYear()
        : Instant.ofEpochMilli(startDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() - 1;
    final int endYear = endMonth <= Month.AUGUST.getValue()
        ? Instant.ofEpochMilli(endDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getYear()
        : Instant.ofEpochMilli(endDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() + 1;
    while (startYear < endYear) {
      years.add(startYear + "/" + (startYear + 1));
      startYear++;
    }
    return years;
  }

  private static void addToStudentMobilitySpecification(final StudentMobilitySpecification conv,
      final CooperationConditionMDTO cc, final boolean withContacts) {
    addToMobilitySpecification(conv, cc, withContacts);
    if(Objects.nonNull(cc.getDuration()) && Objects.nonNull(cc.getDuration().getNumberduration())) {
      conv.setTotalMonthsPerYear(new BigDecimal(cc.getDuration().getNumberduration().toBigInteger()));
    }
  }

  private static void addToStaffMobilitySpecification(final StaffMobilitySpecification conv,
      final CooperationConditionMDTO cc, final boolean withContacts) {
    addToMobilitySpecification(conv, cc, withContacts);
    if(Objects.nonNull(cc.getDuration()) && Objects.nonNull(cc.getDuration().getNumberduration())) {
      conv.setTotalDaysPerYear(new BigDecimal(cc.getDuration().getNumberduration().toBigInteger()));
    }
  }

  public static IiaMDTO convertSingleIia(final String hei_id, final IiasGetResponse.Iia iia) {

    final IiaMDTO converted = new IiaMDTO();
    converted.setPdfId(iia.getPdfFile());

    for (final IiasGetResponse.Iia.Partner partner : iia.getPartner()) {
      final IiaPartnerMDTO convertedPartner = convertToPartnerMDTO(partner);
      if (hei_id.equalsIgnoreCase(partner.getHeiId())) {
        converted.setIiaCode(partner.getIiaCode());
        converted.setFirstPartner(convertedPartner);
      } else {
        converted.setRemoteIiaId(partner.getIiaId());
        converted.setRemoteIiaCode(partner.getIiaCode());
        converted.setSecondPartner(convertedPartner);
      }
    }

    boolean terminated = Objects.nonNull(iia.getCooperationConditions().isTerminatedAsAWhole()) ? iia.getCooperationConditions().isTerminatedAsAWhole() : false;
    final List<CooperationConditionMDTO> cooperationConditions = new ArrayList<>();

    cooperationConditions.addAll(studentMobilitySpecToCooCondMDTO(
            iia.getCooperationConditions().getStudentStudiesMobilitySpec(), "STUDIES", terminated));

    cooperationConditions.addAll(studentMobilitySpecToCooCondMDTO(
            iia.getCooperationConditions().getStudentTraineeshipMobilitySpec(), "TRAINING", terminated));

    cooperationConditions.addAll(staffMobilitySpecToCooperationConditionMDTO(
            iia.getCooperationConditions().getStaffTeacherMobilitySpec(), terminated));

    cooperationConditions.addAll(staffTrainingMobilitySpecToCooperationConditionMDTO(
            iia.getCooperationConditions().getStaffTrainingMobilitySpec(), terminated));

    AtomicInteger order = new AtomicInteger(1);
    cooperationConditions.forEach(cc -> cc.setOrderIndex(order.getAndIncrement()));

    converted.setCooperationConditions(cooperationConditions);

    converted.setEndDate(cooperationConditions.stream().max(Comparator.comparing(CooperationConditionMDTO::getEndDate)).get().getEndDate());
    converted.setStartDate(cooperationConditions.stream().min(Comparator.comparing(CooperationConditionMDTO::getStartDate)).get().getStartDate());

    converted.setModifyDate(new Date());

    converted.setRemoteCoopCondHash(iia.getIiaHash());

    return converted;
  }

  private static List<CooperationConditionMDTO> staffTrainingMobilitySpecToCooperationConditionMDTO(
          final List<StaffTrainingMobilitySpec> staffTeacherMobilitySpec, final boolean terminated) {

    final List<CooperationConditionMDTO> cooperationConditions = new ArrayList<>();
    for (final StaffTrainingMobilitySpec spec : staffTeacherMobilitySpec) {
      final CooperationConditionMDTO c =
              mobilitySpecToCooperationConditionMDTO(spec, "TRAINING", "STAFF");

      final DurationMDTO duration = new DurationMDTO();
      duration.setNumberduration(spec.getTotalDaysPerYear());
      duration.setUnit(DurationUnitVariants.DAYS);
      c.setDuration(duration);
      c.setTerminated(terminated);
      cooperationConditions.add(c);
    }

    return cooperationConditions;
  }

  private static List<CooperationConditionMDTO> staffMobilitySpecToCooperationConditionMDTO(
          final List<StaffTeacherMobilitySpec> staffTeacherMobilitySpec, final boolean terminated) {

    final List<CooperationConditionMDTO> cooperationConditions = new ArrayList<>();
    for (final StaffTeacherMobilitySpec spec : staffTeacherMobilitySpec) {
      final CooperationConditionMDTO c =
              mobilitySpecToCooperationConditionMDTO(spec, "TEACHING", "STAFF");

      final DurationMDTO duration = new DurationMDTO();
      duration.setNumberduration(spec.getTotalDaysPerYear());
      duration.setUnit(DurationUnitVariants.DAYS);
      c.setDuration(duration);
      c.setTerminated(terminated);
      cooperationConditions.add(c);
    }

    return cooperationConditions;
  }

  private static List<CooperationConditionMDTO> studentMobilitySpecToCooCondMDTO(
          final List<? extends StudentMobilitySpecification> studentStudiesMobilitySpecs,
          final String mobilityCategory, final boolean terminated) {

    final List<CooperationConditionMDTO> cooperationConditions = new ArrayList<>();
    for (final StudentMobilitySpecification spec : studentStudiesMobilitySpecs) {
      final CooperationConditionMDTO c =
              mobilitySpecToCooperationConditionMDTO(spec, mobilityCategory, "STUDENT");
      final DurationMDTO duration = new DurationMDTO();
      duration.setNumberduration(spec.getTotalMonthsPerYear());
      duration.setUnit(DurationUnitVariants.MONTHS);
      c.setDuration(duration);
      c.setBlended(spec.isBlended());
      c.setEqfLevel(spec.getEqfLevel());
      c.setTerminated(terminated);
      cooperationConditions.add(c);
    }

    return cooperationConditions;
  }

  private static CooperationConditionMDTO mobilitySpecToCooperationConditionMDTO(final MobilitySpecification spec, final String mobilityCategory,
                                                                                 final String mobilityGroup) {
    final CooperationConditionMDTO c = new CooperationConditionMDTO();
    c.setReceivingPartner(convertToCopCondPartnerMDTO(spec.getReceivingHeiId(), spec.getReceivingOunitId(), spec.getReceivingContact()));
    c.setSendingPartner(convertToCopCondPartnerMDTO(spec.getSendingHeiId(), spec.getSendingOunitId(), spec.getSendingContact()));
    if(!Boolean.TRUE.equals(spec.getMobilitiesPerYear().isNotYetDefined())){
      final MobilityNumberMDTO mobilityNumber = new MobilityNumberMDTO();
      mobilityNumber.setNumbermobility(spec.getMobilitiesPerYear().getValue().intValue());
      mobilityNumber.setVariant(MobilityNumberVariants.TOTAL);
      c.setMobilityNumber(mobilityNumber);
    }
    c.setOtherInfo(spec.getOtherInfoTerms());
    c.setLanguageSkill(new ArrayList<>());
    if(!spec.getRecommendedLanguageSkill().isEmpty() && !Boolean.TRUE.equals(spec.getRecommendedLanguageSkill().get(0).isNotYetDefined())){
      c.getLanguageSkill().addAll(toRecommendedLanguageSkillMDTO(spec.getRecommendedLanguageSkill()));
    }
    c.setSubjectAreaList(new ArrayList<>());
    c.getSubjectAreaList().addAll(toSubjectAreaListMDTO(spec.getSubjectArea()));

    final int minStartYear = Integer.parseInt(spec.getReceivingFirstAcademicYearId().split("/")[0]);
    final int maxEndYear = Integer.parseInt(spec.getReceivingLastAcademicYearId().split("/")[1]);
    final Calendar cal = Calendar.getInstance();

    cal.set(Calendar.YEAR, minStartYear);
    cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    c.setStartDate(cal.getTime());

    cal.set(Calendar.YEAR, maxEndYear);
    cal.set(Calendar.MONTH, Calendar.AUGUST);
    cal.set(Calendar.DAY_OF_MONTH, 31);
    c.setEndDate(cal.getTime());

    final MobilityTypeMDTO type = new MobilityTypeMDTO();
    type.setMobilityCategory(mobilityCategory);
    type.setMobilityGroup(mobilityGroup);
    c.setMobilityType(type);
    return c;
  }

  private static IiaPartnerMDTO convertToPartnerMDTO(final IiasGetResponse.Iia.Partner partner) {

    final IiaPartnerMDTO p = new IiaPartnerMDTO();
    p.setInstitutionId(partner.getHeiId());
    p.setOrganizationUnitId(partner.getOunitId());

    p.setContacts(new ArrayList<>());
    p.getContacts()
            .addAll(partner.getContact().stream().map(CommonsConverter::convertToContactMDTO).collect(Collectors.toList()));

    p.setSignerPersonId(CommonsConverter.convertToContactMDTO(partner.getSigningContact()));

    if (Objects.nonNull(partner.getSigningDate())) {
      p.setSigningDate(CommonsConverter.convertToDate(partner.getSigningDate()));
    }

    return p;
  }

  private static IiaPartnerMDTO convertToCopCondPartnerMDTO(final String partnerSchac, final String partnerOunit, final List<eu.erasmuswithoutpaper.api.types.contact.Contact> contacts) {

    final IiaPartnerMDTO p = new IiaPartnerMDTO();
    p.setInstitutionId(partnerSchac);
    p.setOrganizationUnitId(partnerOunit);

    p.setContacts(new ArrayList<>());
    p.getContacts().addAll(contacts.stream().map(CommonsConverter::convertToContactMDTO).collect(Collectors.toList()));

    return p;
  }

  private static List<CooperationConditionSubjAreaLangSkillMDTO> toRecommendedLanguageSkillMDTO(
          final List<RecommendedLanguageSkill> recommendedLanguageSkill) {

    final List<CooperationConditionSubjAreaLangSkillMDTO> converted = new ArrayList<>();

    for (final RecommendedLanguageSkill skill : recommendedLanguageSkill) {
      final CooperationConditionSubjAreaLangSkillMDTO langSkillMDTO = new CooperationConditionSubjAreaLangSkillMDTO();

      final LanguageSkillMDTO langSkill = new LanguageSkillMDTO();
      langSkill.setLanguage(skill.getLanguage());
      langSkill.setCefrLevel(skill.getCefrLevel());

      langSkillMDTO.setLanguageSkill(langSkill);
      langSkillMDTO.setSubjectArea(toSubjectAreaMDTO(skill.getSubjectArea()));

      converted.add(langSkillMDTO);
    }
    AtomicInteger orderLang = new AtomicInteger(1);
    converted.forEach(cc -> cc.setOrderIndexLang(orderLang.getAndIncrement()));
    return converted;
  }

  private static SubjectAreaMDTO toSubjectAreaMDTO(final SubjectArea subjectArea) {
    SubjectAreaMDTO converted = null;
    if (Objects.nonNull(subjectArea)) {
      converted = new SubjectAreaMDTO();
      converted.setIscedClarification(subjectArea.getIscedClarification());
      converted.setIscedCode(subjectArea.getIscedFCode().getValue());
    }
    return converted;
  }

  private static List<CooperationConditionSubjectAreaMDTO> toSubjectAreaListMDTO(final List<SubjectArea> subjectArea) {
    final List<CooperationConditionSubjectAreaMDTO> converted = new ArrayList<>();

    /*
     * for (final CooperationConditionSubjectAreaMDTO cCSubjectAreaMDTO : cc.getSubjectAreaList()) {
     *
     * final SubjectArea recommendedSA = new SubjectArea(); if (cCSubjectAreaMDTO.getSubjectArea() != null) {
     * recommendedSA.setIscedClarification(cCSubjectAreaMDTO.getSubjectArea().getIscedClarification());
     * recommendedSA.setIscedFCode(cCSubjectAreaMDTO.getSubjectArea().getIscedCode()); }
     */

    for (final SubjectArea area : subjectArea) {
      final CooperationConditionSubjectAreaMDTO ccSubjectArea = new CooperationConditionSubjectAreaMDTO();

      final SubjectAreaMDTO element = new SubjectAreaMDTO();
      element.setIscedClarification(area.getIscedClarification());
      element.setIscedCode(area.getIscedFCode().getValue());
      
      ccSubjectArea.setSubjectArea(element);
      converted.add(ccSubjectArea);
    }

    AtomicInteger orderSubAr = new AtomicInteger(1);
    converted.forEach(cc -> cc.setOrderIndexSubAr(orderSubAr.getAndIncrement()));

    return converted;
  }

}
