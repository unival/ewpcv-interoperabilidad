package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Optional;

import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.MobilityInstitution;

/**
 * The type Mobility institution converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class MobilityInstitutionConverter {

  public static MobilityInstitution fromMobilitytoSendingHei(final MobilityMDTO mobilityMDTO, final OrganizationUnitMDTO ounit) {
    final MobilityInstitution converted = new MobilityInstitution();
    converted.setHeiId(mobilityMDTO.getSendingInstitutionId());
    if (Objects.nonNull(mobilityMDTO.getSendingOrganizationUnitId())) {
      final Optional<LanguageItemMDTO> ounitName =
         ounit.getName().stream().findFirst();
      converted.setOunitName(ounitName.isPresent() ? ounitName.get().getText() : StringUtils.EMPTY);
      converted.setOunitId(mobilityMDTO.getSendingOrganizationUnitId());
    }
    if (Objects.nonNull(mobilityMDTO.getSenderContactPerson())
        && Objects.nonNull(mobilityMDTO.getSenderContactPerson().getPerson())) {
      converted.setContactPerson(toContactPerson(mobilityMDTO.getSenderContactPerson()));
    }
    return converted;
  }

  public static MobilityInstitution fromMobilitytoReceivingHei(final MobilityMDTO mobilityMDTO, final OrganizationUnitMDTO ounit) {
    final MobilityInstitution converted = new MobilityInstitution();
    converted.setHeiId(mobilityMDTO.getReceivingInstitutionId());
    if (Objects.nonNull(mobilityMDTO.getReceivingOrganizationUnitId())) {
      final Optional<LanguageItemMDTO> ounitName =
          ounit.getName().stream().findFirst();
      converted.setOunitName(ounitName.isPresent() ? ounitName.get().getText() : StringUtils.EMPTY);
      converted.setOunitId(mobilityMDTO.getReceivingOrganizationUnitId());
    }
    if (Objects.nonNull(mobilityMDTO.getReceiverContactPerson())) {
      converted.setContactPerson(toContactPerson(mobilityMDTO.getReceiverContactPerson()));
    }
    return converted;
  }

  private static MobilityInstitution.ContactPerson toContactPerson(final ContactMDTO contactMDTO) {
    final MobilityInstitution.ContactPerson converted = new MobilityInstitution.ContactPerson();
    converted.setGivenNames(contactMDTO.getPerson().getFirstNames());
    converted.setFamilyName(contactMDTO.getPerson().getLastName());
    if (Objects.nonNull(contactMDTO.getContactDetails())) {
      converted.setEmail(contactMDTO.getContactDetails().getEmail().stream().findFirst().orElse(null));
      converted.setPhoneNumber(CommonsConverter.convertToPhoneNumber(contactMDTO.getContactDetails().getPhoneNumber()));
    }
    return converted;
  }

}
