package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converters.impl.OrganizationUnitConverter;
import es.minsait.ewpcv.repository.dao.IContactDAO;
import es.minsait.ewpcv.repository.dao.IOrganizationUnitDAO;
import es.minsait.ewpcv.repository.model.organization.ContactDetailsMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.repository.model.organization.OrganizationUnitMDTO;
import es.minsait.ewpcv.service.api.IOunitService;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import eu.erasmuswithoutpaper.api.types.contact.Contact;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Organization unit service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Slf4j
@Service
@Transactional(readOnly = true)
public class OrganizationUnitServiceImpl implements IOunitService {

  private static final String HA_CAMBIADO_LA_PROPIEDAD = "Ha cambiado la propiedad {}";

  private static final String MOBILITY_FACTSHEETS_URL = "Mobility Factsheets URL";

  @Autowired
  private IOrganizationUnitDAO ounitDao;

  @Autowired
  private OrganizationUnitConverter ounitConverter;

  @Autowired
  private IContactDAO contactDAO;

  @Autowired
  private IIiasServiceImpl iiasService;

  @Autowired
  private ApplicationContext applicationContext;


  @Override
  public List<OunitsResponse.Ounit> obtenerOunits(final String heiId, List<String> ounitsIds,
                                                  final List<String> ounitsCodes) {

    final List<OrganizationUnitMDTO> ounits;
    final List<ContactMDTO> contacts;

    if (Objects.nonNull(ounitsIds)) {
      ounits = ounitDao.findAllByHeiIdAndOunitIdExpuestas(heiId, ounitsIds);
      contacts = contactDAO.findOrganizationUnitContactsByOunitId(ounitsIds, heiId);
    } else {
      ounits = ounitDao.findAllByHeiIdAndOunitCodeExpuestas(heiId, ounitsCodes);
      ounitsIds = ounits.stream().map(OrganizationUnitMDTO::getId).collect(Collectors.toList());
      contacts = contactDAO.findOrganizationUnitContactsByOunitId(ounitsIds, heiId);
    }
    if (CollectionUtils.isNotEmpty(contacts)) {
      //eliminamos el contacto principal de la lista ya que se mapea en la institucion
      contacts.removeIf(c -> CollectionUtils.isNotEmpty(ounits) && Objects.nonNull(c.getContactDetails()) &&
              ounits.stream().anyMatch(
                      o -> Objects.nonNull(o.getPrimaryContactDetails()) && o.getPrimaryContactDetails().getId().equalsIgnoreCase(c.getContactDetails().getId()))
      );
    }

    final Map<String, List<ContactMDTO>> contactsMap = new HashMap<>();
    for (final ContactMDTO c : contacts) {
      List<ContactMDTO> contactList = contactsMap.get(c.getOrganizationUnitId());

      if (Objects.isNull(contactList)) {
        contactList = new ArrayList<>();
      }

      contactList.add(c);
      contactsMap.put(c.getOrganizationUnitId(), contactList);
    }

    List<OunitsResponse.Ounit> ounitsRes = new ArrayList<>();

    if (Objects.nonNull(ounits)) {
      ounitsRes = ounitConverter.convertoToOunitList(ounits, contactsMap);
    }

    return ounitsRes;
  }


  @Override
  public List<OrganizationUnitMDTO> actualizarOrganizationUnits(final String heiId,
                                                                final List<OunitsResponse.Ounit> ounits, final String rootOunitId) {

    final List<OrganizationUnitMDTO> ounitsFinales = new ArrayList<>();

    final List<OrganizationUnitMDTO> local = ounitDao.findAllByHeiId(heiId);

    final List<OrganizationUnitMDTO> remoto = ounitConverter.convertToOrganizationUnitMDTOList(ounits);

    // Marcamos que no tiene ninguna estructura de árbol, luego si se llegan a
    // ordernar, cambia el atributo a true
    remoto.forEach(r -> r.setIsTreeStructure(Boolean.FALSE));

    // Todas las ounits remotas están expuestas
    remoto.forEach(r -> r.setIsExposed(Boolean.TRUE));

    // Buscamos la ounits que están en local pero no en remoto, por lo que o no
    // están expuestas o se han borrado
    final List<OrganizationUnitMDTO> ounitsNoExpuestas = new ArrayList<>();
    for (final OrganizationUnitMDTO l : local) {
      if (remoto.stream().noneMatch(r -> r.getId().equalsIgnoreCase(l.getId()))) {
        ounitsNoExpuestas.add(l);
      }
    }

    // Eliminamos las ounitsNoExpuestas de la lista local
    ounitsNoExpuestas.forEach(o -> local.removeIf(l -> o.getId().equalsIgnoreCase(l.getId())));

    final List<OrganizationUnitMDTO> ounitsOrdenadas = new ArrayList<>();
    final List<OrganizationUnitMDTO> ounitsExpuestas;

    // Si está informado el rootOunitId, quiere decir que siguen la estructura
    // en árbol, por lo que se ordenan para su inserción/actualización.
    // Si no está informado, da igual el orden de inserción/actualización
    if (Objects.nonNull(rootOunitId)) {
      ounitsExpuestas = orderOunits(remoto);
    } else {
      ounitsExpuestas = remoto;
    }

    // Asociamos las claves externa e interna de las unidades ya persistidas
    // Aunque la clave interna es un Long, la clave de la unidad madre sigue siendo una cadena.
    Map<String, String> idsOUnits = local.stream()
            .collect(Collectors.toMap(OrganizationUnitMDTO::getId, ou -> ou.getInternalId().toString()));

    /* Actualizamos las OrganizationUnits */
    log.trace("Comienza el proceso de comparacion de OrganizationUnits");
    // no tenemos las organization units en bbdd, guardamos lo que nos llega
    if (CollectionUtils.isEmpty(local)) {
      ounitsFinales.addAll(ounitsExpuestas);
    } else {
      final boolean hayCambios = hayCambiosOunits(ounitsExpuestas, local, idsOUnits);

      if (Objects.nonNull(rootOunitId)) {
        ounitsOrdenadas.addAll(orderOunits(local));
      } else {
        ounitsOrdenadas.addAll(local);
      }

      if (hayCambios) {
        // actualizamos la informacion de la ounits
        ounitsFinales.addAll(ounitsOrdenadas);
      }

      ounitsNoExpuestas.forEach(o -> {
        o.setIsExposed(Boolean.FALSE);
        ounitsFinales.add(o);
      });
    }

    final List<OrganizationUnitMDTO> ounitsResultado = new ArrayList<>();

    //actualizamos solo las ounits que han cambiado o que se han dejado de exponer
    for (OrganizationUnitMDTO ounitsFinale : ounitsFinales) {
      // Antes de guardar actualizamos la posible clave externa de la unidad madre
      if (ounitsFinale.getParentOUnitId() != null) {
        if (idsOUnits.containsKey(ounitsFinale.getParentOUnitId())) {
          ounitsFinale.setParentOUnitId(idsOUnits.get(ounitsFinale.getParentOUnitId()));
        } else if (!idsOUnits.containsValue(ounitsFinale.getParentOUnitId())) {
          log.warn("Parent OUnit ID desconocido: {}", ounitsFinale.getParentOUnitId());
          ounitsFinale.setParentOUnitId(null);
        }
      }

      if (ounitsFinale.getParentOUnitId() != null && idsOUnits.containsKey(ounitsFinale.getParentOUnitId())) {
        ounitsFinale.setParentOUnitId(idsOUnits.get(ounitsFinale.getParentOUnitId()));
      }
      OrganizationUnitMDTO saved = saveOrUpdateOunit(ounitsFinale);
      if (saved != null) {
        ounitsResultado.add(saved);
        // Se añade la clave interna al mapa para contemplar las recién creadas
        idsOUnits.put(saved.getId(), saved.getInternalId().toString());
      }
      log.debug("Se ha actualizado la información de la ounit con id {}", ounitsFinale.getId());
    }
    //actualizamos los contactos de todas las ounits que recibimos
    ounitsExpuestas.forEach(o -> {
      final OunitsResponse.Ounit response =
              ounits.stream().filter(resp -> o.getId().equalsIgnoreCase(resp.getOunitId())).findFirst().orElse(null);

      if (Objects.nonNull(response)) {
        actualizarContactosOunit(response.getContact(), o, ounitsOrdenadas.stream().filter(localOunit -> localOunit.getId().equalsIgnoreCase(o.getId())).findFirst().orElse(null), heiId)
        ;
      }
    });

    return ounitsResultado;
  }

  private OrganizationUnitMDTO saveOrUpdateOunit(final OrganizationUnitMDTO ounit) {
    try {
      final OrganizationUnitServiceImpl otherBean = (OrganizationUnitServiceImpl) applicationContext.getBean("organizationUnitServiceImpl");
      return otherBean.transactionalSaveOrUpdateOunit(ounit);
    } catch (final Exception e) {
      log.error("se ha producido un error al guardar o actualizar la ounit, se captura para evitar problemas.", e);
      return null;
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public OrganizationUnitMDTO transactionalSaveOrUpdateOunit(final OrganizationUnitMDTO ounit) {
    return ounitDao.saveOrUpdate(ounit);
  }

  private void actualizarContactosOunit(final List<Contact> contactos, final OrganizationUnitMDTO ounit, final OrganizationUnitMDTO localOunit, final String heiId) {

    final List<ContactMDTO> localContacts =
            contactDAO.findOrganizationUnitContactsByOunitId(Arrays.asList(ounit.getId()), heiId);
    if (CollectionUtils.isNotEmpty(localContacts) && Objects.nonNull(localOunit)) {
      //eliminamos el contacto principal de la lista ya que se mapea en la institucion
      localContacts.removeIf(c -> Objects.nonNull(localOunit.getPrimaryContactDetails()) && Objects.nonNull(c.getContactDetails()) &&
              c.getContactDetails().getId().equalsIgnoreCase(localOunit.getPrimaryContactDetails().getId())
      );
    }

    final List<ContactMDTO> remoteContacts;

    if (!CollectionUtils.isEmpty(contactos)) {
      remoteContacts = ounitConverter.convertToContactMDTOList(contactos, ounit.getId(), heiId);

      /* Actualizamos los Contacts de la organization unit */
      log.trace("Comienza el proceso de comparacion de Contacts para la organizationUnit");
      if (CollectionUtils.isEmpty(localContacts)) {
        // no tenemos ningun contacto de la organizationUnit, guardamos los que
        // nos llegan
        remoteContacts.forEach(cont -> saveOrUpdateContact(cont));
      } else {
        final List<ContactMDTO> contactosBorrar = new ArrayList<>();
        final boolean hayCambios = hayCambiosContacts(remoteContacts, localContacts, contactosBorrar);

        // borramos los contactos relacionados con la organizationUnit que se
        // hayan
        // borrado en remoto
        contactosBorrar.forEach(c -> deleteContact(c));

        if (hayCambios) {
          // actualizamos la informacion de los contactos
          localContacts.forEach(cont -> saveOrUpdateContact(cont));
        }
      }
    }
  }

  private ContactMDTO saveOrUpdateContact(final ContactMDTO contact) {
    try {
      final OrganizationUnitServiceImpl otherBean = (OrganizationUnitServiceImpl) applicationContext.getBean("organizationUnitServiceImpl");
      return otherBean.transactionalSaveOrUpdateContact(contact);
    } catch (final Exception e) {
      log.error("se ha producido un error al guardar o actualizar el contacto de la ounit, se captura para evitar problemas.", e);
      return null;
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public ContactMDTO transactionalSaveOrUpdateContact(final ContactMDTO contact) {
    return contactDAO.saveOrUpdate(contact);
  }

  private void deleteContact(final ContactMDTO contact) {
    try {
      final OrganizationUnitServiceImpl otherBean = (OrganizationUnitServiceImpl) applicationContext.getBean("organizationUnitServiceImpl");
      otherBean.transactionalDeleteContact(contact);
    } catch (final Exception e) {
      log.error("se ha producido un error al eliminar el contacto de la ounit, se captura para evitar problemas.", e);
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void transactionalDeleteContact(final ContactMDTO contact) {
    contactDAO.delete(contact);
  }


  private List<OrganizationUnitMDTO> orderOunits(final List<OrganizationUnitMDTO> remoto) {
    final Map<String, String> remotoParentsMap = new TreeMap<>();

    for (final OrganizationUnitMDTO ounit : remoto) {
      remotoParentsMap.put(ounit.getId(), ounit.getParentOUnitId());
    }

    final List<String> remotoOunits = new ArrayList<>(remotoParentsMap.keySet());

    final List<String> sortOunitsId = new ArrayList<>(remotoOunits.size());

    // Busca el rootOunitId, lo añade a la lista ordenada de ounits y lo elimina
    // de la lista de remotos
    for (final Map.Entry<String, String> entry : remotoParentsMap.entrySet()) {
      if (Objects.isNull(entry.getValue())) {
        sortOunitsId.add(entry.getKey());
        remotoOunits.remove(entry.getKey());
      }
    }

    // Busca el id del padre de la ounit, e inserta el id la ounit justo después
    // del padre
    for (final String ounit : remotoOunits) {
      final String parent = remotoParentsMap.get(ounit);
      if (Objects.isNull(parent)) {
        sortOunitsId.add(ounit);
      } else {
        int posParent = sortOunitsId.indexOf(parent);
        if (posParent == -1 && remotoParentsMap.containsKey(parent) && !sortOunitsId.contains(parent)) {
          sortOunitsId.add(parent);
          posParent = sortOunitsId.indexOf(parent);
        }

        if (!sortOunitsId.contains(ounit)) {
          sortOunitsId.add(posParent + 1, ounit);
        }
      }
    }

    final List<OrganizationUnitMDTO> listaOrdenada = new ArrayList<>();

    for (final String ounitId : sortOunitsId) {
      final OrganizationUnitMDTO org =
              remoto.stream().filter(r -> r.getId().equalsIgnoreCase(ounitId)).findFirst().orElse(null);
      if (Objects.nonNull(org)) {
        org.setIsTreeStructure(Boolean.TRUE);
        listaOrdenada.add(org);
      }
    }

    return listaOrdenada;
  }


  private boolean hayCambiosOunits(final List<OrganizationUnitMDTO> remoto, final List<OrganizationUnitMDTO> local, Map<String, String> idsOUnits) {
    return !compararOunitsList(remoto, local, idsOUnits);
  }

  private boolean compararOunitsList(final List<OrganizationUnitMDTO> remoto, final List<OrganizationUnitMDTO> local, Map<String, String> idsOUnits) {

    final List<OrganizationUnitMDTO> incluir;
    List<OrganizationUnitMDTO> modificado = new ArrayList<>();

    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      modificado =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream()
                      .filter(r -> local.stream().filter(l -> l.getId().equalsIgnoreCase(r.getId()))
                              .anyMatch(l -> !compararOunits(r, l, Boolean.TRUE, idsOUnits)))
                      .collect(Collectors.toList())
                      : new ArrayList<>();

      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(r -> local.stream().noneMatch(l -> r.getId().equalsIgnoreCase(l.getId())))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.info("Se van a incluir los siguientes OrganizationUnitMDTO {}", incluir);
    local.addAll(incluir);

    return incluir.isEmpty() && modificado.isEmpty();
  }


  private boolean compararOunits(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                 final boolean actualizarLocal, final Map<String, String> idsOUnits) {

    boolean esIgual = Boolean.TRUE;

    if (!comparaIsExposed(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaIsTreeStructure(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaParentOUnitId(remoto, local, actualizarLocal, idsOUnits)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaOrganizationUnitCode(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaAbbreviation(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaLogoUrl(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaPrimaryContactDetails(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaName(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaPrimaryContactDetailUrl(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    if (!comparaMobilityFactsheetUrl(remoto, local, actualizarLocal)) {
      esIgual = Boolean.FALSE;
    }

    return esIgual;

  }

  private boolean comparaLogoUrl(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                 final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.nonNull(remoto.getLogoUrl()) && !remoto.getLogoUrl().equals(local.getLogoUrl())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LogoUrl");
      if (actualizarLocal) {
        local.setLogoUrl(remoto.getLogoUrl());
      }
      esIgual = Boolean.FALSE;
    }
    return esIgual;
  }

  private boolean comparaAbbreviation(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                      final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.nonNull(remoto.getAbbreviation()) && !remoto.getAbbreviation().equals(local.getAbbreviation())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Abbreviation");
      if (actualizarLocal) {
        local.setAbbreviation(remoto.getAbbreviation());
      }
      esIgual = Boolean.FALSE;
    }
    return esIgual;
  }

  private boolean comparaIsTreeStructure(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                         final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (!remoto.getIsTreeStructure().equals(local.getIsTreeStructure())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "isTreeStructure");
      if (actualizarLocal) {
        local.setIsTreeStructure(remoto.getIsTreeStructure());
      }
      esIgual = Boolean.FALSE;
    }
    return esIgual;
  }

  private boolean comparaIsExposed(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                   final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (!Objects.equals(remoto.getIsExposed(), local.getIsExposed())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "isExposed");
      if (actualizarLocal) {
        local.setIsExposed(remoto.getIsExposed());
      }
      esIgual = Boolean.FALSE;
    }
    return esIgual;
  }

  private boolean comparaMobilityFactsheetUrl(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local, final boolean actualizarLocal) {
    List<LanguageItemMDTO> localUrls = local.getFactsheetUrls();
    List<LanguageItemMDTO> remoteUrls = remoto.getFactsheetUrls();

    if (localUrls == null || !iiasService.compararLanguageItemsList(remoteUrls, localUrls, actualizarLocal)) {
      local.setFactsheetUrls(remoteUrls != null ? new ArrayList<>(remoteUrls) : new ArrayList<>());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, MOBILITY_FACTSHEETS_URL);
      return false;
    }
    return true;
  }

  private boolean comparaPrimaryContactDetailUrl(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                                 final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.nonNull(local.getPrimaryContactDetails()) && Objects.isNull(local.getPrimaryContactDetails().getUrl())
            && Objects.nonNull(remoto.getPrimaryContactDetails())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizatioUnitURL");
      if (actualizarLocal) {
        local.setPrimaryContactDetails(new ContactDetailsMDTO());
        local.getPrimaryContactDetails().setUrl(remoto.getPrimaryContactDetails().getUrl());
      }
      esIgual = Boolean.FALSE;
    } else {
      if (Objects.nonNull(remoto.getPrimaryContactDetails())
              && Objects.nonNull(remoto.getPrimaryContactDetails().getUrl()) && iiasService.compararLanguageItemsList(
              remoto.getPrimaryContactDetails().getUrl(), local.getPrimaryContactDetails().getUrl(), actualizarLocal)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizatioUnitURL");
        local.getPrimaryContactDetails().setUrl(remoto.getPrimaryContactDetails().getUrl());
        esIgual = Boolean.FALSE;
      }
    }
    return esIgual;
  }

  private boolean comparaName(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                              final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.isNull(local.getName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Name");
      local.setName(remoto.getName());
      esIgual = Boolean.FALSE;
    } else {
      if (Objects.nonNull(remoto.getName())
              && iiasService.compararLanguageItemsList(remoto.getName(), local.getName(), actualizarLocal)) {
        local.setName(remoto.getName());
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Name");
        esIgual = Boolean.FALSE;
      }
    }
    return esIgual;
  }

  private boolean comparaPrimaryContactDetails(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                               final boolean actualizarLocal) {
    final ContactDetailsMDTO localContactDetails =
            Objects.nonNull(local.getPrimaryContactDetails()) ? local.getPrimaryContactDetails() : new ContactDetailsMDTO();
    if (iiasService.compararContactDetails(remoto.getPrimaryContactDetails(), localContactDetails, Boolean.TRUE)) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PrimaryContactDetails");
      if (actualizarLocal) {
        local.setPrimaryContactDetails(localContactDetails);
      }
      return Boolean.FALSE;
    }

    return Boolean.TRUE;
  }

  private boolean comparaOrganizationUnitCode(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                              final boolean actualizarLocal) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.nonNull(remoto.getOrganizationUnitCode())) {
      if (!remoto.getOrganizationUnitCode().equals(local.getOrganizationUnitCode())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitCode");
        if (actualizarLocal) {
          local.setOrganizationUnitCode(remoto.getOrganizationUnitCode());
        }
        esIgual = Boolean.FALSE;
      }
    } else {
      if (Objects.nonNull(local.getOrganizationUnitCode())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitCode");
        if (actualizarLocal) {
          local.setOrganizationUnitCode(null);
        }
        esIgual = Boolean.FALSE;
      }
    }
    return esIgual;
  }

  private boolean comparaParentOUnitId(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
                                       final boolean actualizarLocal, final Map<String, String> idsOUnits) {
    boolean esIgual = Boolean.TRUE;
    if (Objects.nonNull(remoto.getParentOUnitId())) {
      final String remoteParentOUnitInternalId = idsOUnits.get(remoto.getParentOUnitId());
      if (remoteParentOUnitInternalId == null || !remoteParentOUnitInternalId.equals(local.getParentOUnitId())) {
        // La unidad madre puede venir dada pero no tenerla en la BD
        // Más adelante al guardar estará en BD y podremos deducir su clave interna
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ParentOUnitId");
        if (actualizarLocal) {
          local.setParentOUnitId(remoteParentOUnitInternalId == null ? remoto.getParentOUnitId() : remoteParentOUnitInternalId);
        }
        esIgual = Boolean.FALSE;
      }
    } else {
      if (Objects.nonNull(local.getParentOUnitId())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ParentOUnitId");
        if (actualizarLocal) {
          local.setParentOUnitId(null);
        }
        esIgual = Boolean.FALSE;
      }
    }
    return esIgual;
  }

  private boolean hayCambiosContacts(final List<ContactMDTO> remotoContacts, final List<ContactMDTO> localContacts,
                                     final List<ContactMDTO> elementosABorrar) {
    return iiasService.compararContactsList(remotoContacts, localContacts, elementosABorrar);
  }

  @Override
  public boolean existsOUnit(String ounitId, String institutionSchac) {
    return ounitDao.existsOunit(ounitId, institutionSchac);
  }
}
