package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.repository.dao.IOrganizationUnitDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import es.minsait.ewpcv.converter.api.IOrganizationUnitConverter;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.ounits.OunitsResponse;
import eu.erasmuswithoutpaper.api.types.contact.Contact;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Organization unit converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class OrganizationUnitConverter implements IOrganizationUnitConverter {

  @Autowired
  private IOrganizationUnitDAO ounitDao;

  @Override
  public List<OunitsResponse.Ounit> convertoToOunitList(final List<OrganizationUnitMDTO> ounits,
      final Map<String, List<ContactMDTO>> contactsMap) {
    final List<OunitsResponse.Ounit> res = new ArrayList<>();

    for (final OrganizationUnitMDTO mdto : ounits) {
      res.add(convertToOunit(mdto, contactsMap.get(mdto.getId())));
    }

    return res;
  }

  @Override
  public OunitsResponse.Ounit convertToOunit(final OrganizationUnitMDTO mdto, final List<ContactMDTO> contacts) {

    final OunitsResponse.Ounit res = new OunitsResponse.Ounit();

    res.setOunitId(mdto.getId());

    res.setOunitCode(mdto.getOrganizationUnitCode());

    res.setAbbreviation(mdto.getAbbreviation());
    res.setLogoUrl(mdto.getLogoUrl());

    if (Objects.nonNull(mdto.getPrimaryContactDetails())) {
      res.setStreetAddress(
          CommonsConverter.convertToFlexibleAddress(mdto.getPrimaryContactDetails().getStreetAddress()));
      res.setMailingAddress(
          CommonsConverter.convertToFlexibleAddress(mdto.getPrimaryContactDetails().getMailingAddress()));


      /** WEBSITE URL */
      final List<HTTPWithOptionalLang> websites = new ArrayList<>();

      for (final LanguageItemMDTO lang : mdto.getPrimaryContactDetails().getUrl()) {
        websites.add(CommonsConverter.convertLanguageItemMDTOToHttpWithOptionalLang(lang));
      }
      res.getWebsiteUrl().addAll(websites);
    }

    /** Factsheet URLs **/
    if (Objects.nonNull(mdto.getFactSheet())) {
      final List<HTTPWithOptionalLang> httpOptional = new ArrayList<>();

      for (final LanguageItemMDTO lang : mdto.getFactsheetUrls()) {
        httpOptional.add(CommonsConverter.convertLanguageItemMDTOToHttpWithOptionalLang(lang));
      }

      res.getMobilityFactsheetUrl().addAll(httpOptional);
    }

    /** Name **/
    if (Objects.nonNull(mdto.getName())) {
      final List<StringWithOptionalLang> names = new ArrayList<>();

      for (final LanguageItemMDTO lang : mdto.getName()) {
        names.add(CommonsConverter.convertToStringWithOptionalLang(lang));
      }

      res.getName().addAll(names);
    }

    /** CONTACTS **/
    final List<Contact> contactList = new ArrayList<>();
    if (Objects.nonNull(contacts)) {
      for (final ContactMDTO contact : contacts) {
        contactList.add(CommonsConverter.convertToContact(contact));
      }
      res.getContact().addAll(contactList);
    }

    // The MDTO contains the internal ID of the parent unit (if available),
    // but we want to return  in the API response the external ID instead.
    if (StringUtils.isNotEmpty(mdto.getParentOUnitId())) {
      OrganizationUnitMDTO parentUnit = ounitDao.findByInternalId(mdto.getParentOUnitId());
      res.setParentOunitId(parentUnit.getId());
    }

    return res;
  }

  @Override
  public List<OrganizationUnitMDTO> convertToOrganizationUnitMDTOList(final List<OunitsResponse.Ounit> ounits) {
    final List<OrganizationUnitMDTO> lista = new ArrayList<>();
    for (final OunitsResponse.Ounit ounit : ounits) {
      lista.add(convertToOrganizationUnitMDTO(ounit));
    }
    return lista;
  }

  @Override
  public OrganizationUnitMDTO convertToOrganizationUnitMDTO(final OunitsResponse.Ounit ounit) {
    final OrganizationUnitMDTO dto = new OrganizationUnitMDTO();

    dto.setId(ounit.getOunitId());
    dto.setOrganizationUnitCode(ounit.getOunitCode());
    dto.setAbbreviation(ounit.getAbbreviation());
    dto.setLogoUrl(ounit.getLogoUrl());

    // When converting from an API response, we cannot convert here the external ID into the internal ID,
    // because it is possible that we are converting a NEW unit, and therefore we have no internal database ID yet.
    // Thus, this is handled in OrganizationUnitService.actualizarOrganizationUnits() instead.
    dto.setParentOUnitId(ounit.getParentOunitId());

    final ContactDetailsMDTO primaryContactDetails = new ContactDetailsMDTO();
    if (Objects.nonNull(ounit.getMailingAddress())) {
      primaryContactDetails.setMailingAddress(CommonsConverter.convertToFlexibleAddressMDTO(ounit.getMailingAddress()));
    }

    if (Objects.nonNull(ounit.getStreetAddress())) {
      primaryContactDetails.setStreetAddress(CommonsConverter.convertToFlexibleAddressMDTO(ounit.getStreetAddress()));
    }
    dto.setPrimaryContactDetails(primaryContactDetails);

    /** NAMES **/
    final List<LanguageItemMDTO> names = new ArrayList<>();
    for (final StringWithOptionalLang strOptionalLang : ounit.getName()) {
      names.add(CommonsConverter.convertToLanguageItem(strOptionalLang));
    }
    dto.setName(names);

    /** WEBSITE URL */
    final List<LanguageItemMDTO> urls = new ArrayList<>();
    for (final HTTPWithOptionalLang httpOpt : ounit.getWebsiteUrl()) {
      urls.add(CommonsConverter.convertToLanguageItemMDTO(httpOpt));
    }
    dto.getPrimaryContactDetails().setUrl(urls);

    /* MOBILITY FACTSHEET URL */
    if (!CollectionUtils.isEmpty(ounit.getMobilityFactsheetUrl())) {
      final List<LanguageItemMDTO> fUrls = new ArrayList<>();
      for (final HTTPWithOptionalLang httpOpt : ounit.getMobilityFactsheetUrl()) {
        fUrls.add(CommonsConverter.convertToLanguageItemMDTO(httpOpt));
      }

      dto.setFactsheetUrls(fUrls);
    }

    if (esContactDetailsVacio(primaryContactDetails)) {
      dto.setPrimaryContactDetails(null);
    }

    return dto;

  }

  @Override
  public List<ContactMDTO> convertToContactMDTOList(final List<Contact> contacts, final String organizationUnitId,
      final String heiId) {
    final List<ContactMDTO> res = new ArrayList<>();

    for (final Contact contact : contacts) {
      final ContactMDTO mdto = CommonsConverter.convertToContactMDTO(contact);
      mdto.setOrganizationUnitId(organizationUnitId);
      mdto.setInstitutionId(heiId);
      res.add(mdto);
    }

    return res;
  }

  private boolean esContactDetailsVacio(final ContactDetailsMDTO contactDetails) {

    return Objects.nonNull(contactDetails) && Objects.isNull(contactDetails.getEmail())
        && Objects.isNull(contactDetails.getPhoneNumber()) && Objects.isNull(contactDetails.getFaxNumber())
        && Objects.isNull(contactDetails.getStreetAddress()) && Objects.isNull(contactDetails.getMailingAddress())
        && CollectionUtils.isEmpty(contactDetails.getUrl());
  }
}
