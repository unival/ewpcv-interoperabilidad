package es.minsait.ewpcv.converters.impl;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;

import org.w3c.dom.Document;

import javax.xml.bind.DatatypeConverter;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import javax.xml.transform.stream.StreamSource;

import java.io.*;

import java.nio.charset.StandardCharsets;

import java.security.MessageDigest;

import java.util.Locale;

@Slf4j

public class IiaHashGenerator {

    public static byte[] getXslt6() throws IOException{

        return readFromInputStream(IiaHashGenerator.class.getResourceAsStream("/xslt/transform_version_6.xsl"));

    }

    public static byte[] getXslt7() throws IOException{

        return readFromInputStream(IiaHashGenerator.class.getResourceAsStream("/xslt/transform_version_7.xsl"));

    }

    public static byte[] getTextToHash(byte[] iia, byte[] xslt) {
        String result = new String(transform(iia, xslt));

        if (StringUtils.isNotEmpty(result) && result.contains("<text-to-hash>")){
            result = result.split("<text-to-hash>")[1].split("</text-to-hash>")[0].trim();
        }

        return result.getBytes(StandardCharsets.UTF_8);

    }

    public static boolean validForApprove(byte[] iia) throws IOException {
        String result = new String(transform(iia, getXslt7()));
        if (StringUtils.isNotEmpty(result) && result.contains("<valid-for-approval>")){
            result = result.split("<valid-for-approval>")[1].split("</valid-for-approval>")[0].trim();
        }
        return !result.equalsIgnoreCase("false");
    }

    public static String generateIiaHash(byte[] textToHash){

        String digestHeader = StringUtils.EMPTY;

        try{

            final byte[] digest = MessageDigest.getInstance("SHA-256").digest(textToHash);

            digestHeader = DatatypeConverter.printHexBinary(digest).toLowerCase(Locale.ENGLISH);

        }catch (final Exception e){

            log.error(e.getMessage());

            e.printStackTrace();

        }

        return digestHeader;

    }

    private static byte[] readFromInputStream(InputStream inputStream) throws IOException{
        StringBuilder stringBuilder = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))){
            String line;
            while ((line = br.readLine()) != null){
                stringBuilder.append(line).append("\n");
            }
        }
        stringBuilder.delete(stringBuilder.length()-1, stringBuilder.length());
        return stringBuilder.toString().getBytes();
    }

    private static String transform(byte[] iia, byte[] xslt){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        try{
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new ByteArrayInputStream(iia));
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(new StreamSource(new ByteArrayInputStream(xslt)));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            transformer.transform(new DOMSource(document), new StreamResult(output));
            return output.toString();

        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

}

