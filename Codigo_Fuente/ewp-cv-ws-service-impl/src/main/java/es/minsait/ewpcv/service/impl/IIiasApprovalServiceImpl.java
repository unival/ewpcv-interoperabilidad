package es.minsait.ewpcv.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import es.minsait.ewpcv.converter.api.IIiaApprovalConverter;
import es.minsait.ewpcv.repository.dao.IIiaApprovalDAO;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import es.minsait.ewpcv.service.api.IIiasApprovalService;
import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;


/**
 * The type Iias approval service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(readOnly = true)
public class IIiasApprovalServiceImpl implements IIiasApprovalService {

  private IIiaApprovalDAO iiaApprovalDAO;
  private IIiaApprovalConverter iiaApprovalConverter;

  @Autowired
  public IIiasApprovalServiceImpl(final IIiaApprovalDAO iiaDAO, final IIiaApprovalConverter iiaConverter) {
    this.iiaApprovalDAO = iiaDAO;
    this.iiaApprovalConverter = iiaConverter;
  }

  @Override
  public List<IiasApprovalResponse.Approval> findIiasByRemoteIiaIdAndPartners(final String aprovingHeiId, final String ownerHeiId,
                                                                              final List<String> iiasId) {
    final List<IiaMDTO> iias = iiaApprovalDAO.findApprovedIiasByRemoteIiaIdAndPartners(aprovingHeiId, ownerHeiId, iiasId);
    return iiaApprovalConverter.convertToIiasApproval(iias, aprovingHeiId);
  }

}
