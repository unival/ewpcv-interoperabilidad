package es.minsait.ewpcv.service.impl;

import com.google.common.base.Strings;
import es.minsait.ewpcv.repository.dao.IEventDAO;
import es.minsait.ewpcv.repository.model.notificacion.EventMDTO;
import es.minsait.ewpcv.service.api.IEventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * The type Event service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Slf4j
public class EventServiceImpl implements IEventService {


  private IEventDAO eventDAO;
  private final ApplicationContext applicationContext;

  @Autowired
  public EventServiceImpl(final IEventDAO eventDAO, final ApplicationContext applicationContext) {
    this.eventDAO = eventDAO;
    this.applicationContext = applicationContext;
  }

  @Override
  public void save(final EventMDTO event) {
    try {
      final EventServiceImpl otherBean = (EventServiceImpl) applicationContext.getBean("eventServiceImpl");
      otherBean.transactionalSave(event);
    } catch (final Exception e) {
      log.error("se ha producido un error al registrar el evento, se captura para evitar problemas.", e);
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void transactionalSave(final EventMDTO event) {
    try {
      if (Strings.isNullOrEmpty(event.getChangedElementIds())) {
        event.setChangedElementIds("UNDEFINED");
      }

      if (Strings.isNullOrEmpty(event.getEventType())) {
        event.setEventType("UNDEFINED");
      }

      if (Objects.nonNull(event.getOwnerHei()) && event.getOwnerHei().length() > 255) {
        event.setOwnerHei(event.getOwnerHei().substring(0, 251) + "...");
      }
      if (Objects.nonNull(event.getTriggeringHei()) && event.getTriggeringHei().length() > 255) {
        event.setTriggeringHei(event.getTriggeringHei().substring(0, 251) + "...");
      }

      if (Objects.nonNull(event.getObservations()) && event.getObservations().length() > 4000) {
        event.setObservations(event.getObservations().substring(0, 3996) + "...");
      }

      if (Objects.nonNull(event.getRequestParams()) && event.getRequestParams().length() > 4000) {
        event.setRequestParams(event.getRequestParams().substring(0, 3996) + "...");
      }

      eventDAO.saveEvent(event);
    } catch (final Exception e) {
      log.error("se ha producido un error al registrar el evento, se captura para evitar problemas.", e);
    }
  }

}
