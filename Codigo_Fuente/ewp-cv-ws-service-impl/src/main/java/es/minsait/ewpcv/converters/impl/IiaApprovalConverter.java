package es.minsait.ewpcv.converters.impl;


import org.apache.xml.security.Init;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import es.minsait.ewpcv.converter.api.IIiaApprovalConverter;
import es.minsait.ewpcv.repository.model.iia.IiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.approval.IiasApprovalResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Iia approval converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class IiaApprovalConverter implements IIiaApprovalConverter {

  @Override
  public List<IiasApprovalResponse.Approval> convertToIiasApproval(final List<IiaMDTO> iiaList, final String heiId) {

    final List<IiasApprovalResponse.Approval> approvals = new ArrayList<>();
    Init.init();

    for (final IiaMDTO iia : iiaList) {
      final IiasApprovalResponse.Approval approval = new IiasApprovalResponse.Approval();

      approval.setIiaId(iia.getRemoteIiaId());
      approval.setIiaHash(iia.getApprovalCopCondHash());
      approval.setPdfFile(iia.getPdfId());
      approvals.add(approval);
    }

    return approvals;
  }


}
