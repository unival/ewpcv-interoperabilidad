package es.minsait.ewpcv.converters.impl;

import com.google.common.base.Strings;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.model.omobility.LaComponentReasonCode;
import es.minsait.ewpcv.model.omobility.LaComponentType;
import es.minsait.ewpcv.model.omobility.LearningAgreementComponentStatus;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.repository.dao.ILearningAgreementDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.LaComponentMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.SignatureMDTO;
import es.minsait.ewpcv.repository.model.organization.ContactMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.*;
import eu.erasmuswithoutpaper.api.types.academicterm.TermId;

/**
 * The type Component converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
public class ComponentConverter {

  @Autowired
  private ILearningAgreementDAO learningAgreementDAO;


  public ListOfComponents toFirstVersion(final LearningAgreementMDTO learningAgreementMDTO, final String receivingHei,
      final String sendingHei) throws EwpConverterException {
    final ListOfComponents converted = new ListOfComponents();

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getStudiedLaComponents())) {
      converted.setComponentsStudied(
          toComponentStudied(learningAgreementMDTO.getStudiedLaComponents(), Boolean.TRUE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getRecognizedLaComponents())) {
      converted.setComponentsRecognized(
          toComponentRecognized(learningAgreementMDTO.getRecognizedLaComponents(), Boolean.TRUE, sendingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getVirtualLaComponents())) {
      converted.setVirtualComponents(
          toVirtualComponents(learningAgreementMDTO.getVirtualLaComponents(), Boolean.TRUE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getBlendedLaComponents())) {
      converted.setBlendedMobilityComponents(
          toBlendedMobilityComponents(learningAgreementMDTO.getBlendedLaComponents(), Boolean.TRUE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getDoctoralLaComponents())) {
      converted.setShortTermDoctoralComponents(
          toShortTermDoctoralComponents(learningAgreementMDTO.getDoctoralLaComponents(), Boolean.TRUE, receivingHei));
    }

    if (Objects.nonNull(learningAgreementMDTO.getStudentSign())) {
      converted.setStudentSignature(toSignature(learningAgreementMDTO.getStudentSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getSenderCoordinatorSign())) {
      converted.setSendingHeiSignature(toSignature(learningAgreementMDTO.getSenderCoordinatorSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getReciverCoordinatorSign())) {
      converted.setReceivingHeiSignature(toSignature(learningAgreementMDTO.getReciverCoordinatorSign()));
    }
    return converted;
  }

  public ListOfComponents toApprovedChanges(final LearningAgreementMDTO learningAgreementMDTO,
      final String receivingHei, final String sendingHei) throws EwpConverterException {
    final ListOfComponents converted = new ListOfComponents();
    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getStudiedLaComponents())) {
      converted.setComponentsStudied(
          toComponentStudied(learningAgreementMDTO.getStudiedLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getRecognizedLaComponents())) {
      converted.setComponentsRecognized(
          toComponentRecognized(learningAgreementMDTO.getRecognizedLaComponents(), Boolean.FALSE, sendingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getVirtualLaComponents())) {
      converted.setVirtualComponents(
          toVirtualComponents(learningAgreementMDTO.getVirtualLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getBlendedLaComponents())) {
      converted.setBlendedMobilityComponents(
          toBlendedMobilityComponents(learningAgreementMDTO.getBlendedLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getDoctoralLaComponents())) {
      converted.setShortTermDoctoralComponents(
          toShortTermDoctoralComponents(learningAgreementMDTO.getDoctoralLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (Objects.nonNull(learningAgreementMDTO.getStudentSign())) {
      converted.setStudentSignature(toSignature(learningAgreementMDTO.getStudentSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getSenderCoordinatorSign())) {
      converted.setSendingHeiSignature(toSignature(learningAgreementMDTO.getSenderCoordinatorSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getReciverCoordinatorSign())) {
      converted.setReceivingHeiSignature(toSignature(learningAgreementMDTO.getReciverCoordinatorSign()));
    }
    return converted;
  }

  public LearningAgreement.ChangesProposal toChangesProposed(final LearningAgreementMDTO learningAgreementMDTO,
      final ContactMDTO modifiedStudentContactMDTO, final String changesProposalId, final String receivingHei,
      final String sendingHei) throws EwpConverterException {
    final LearningAgreement.ChangesProposal converted = new LearningAgreement.ChangesProposal();
    if (Objects.nonNull(modifiedStudentContactMDTO)) {
      converted.setStudent(CommonsConverter.toStudent(modifiedStudentContactMDTO));
    }
    converted.setId(changesProposalId);
    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getStudiedLaComponents())) {
      converted.setComponentsStudied(
          toComponentStudied(learningAgreementMDTO.getStudiedLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getRecognizedLaComponents())) {
      converted.setComponentsRecognized(
          toComponentRecognized(learningAgreementMDTO.getRecognizedLaComponents(), Boolean.FALSE, sendingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getVirtualLaComponents())) {
      converted.setVirtualComponents(
          toVirtualComponents(learningAgreementMDTO.getVirtualLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getBlendedLaComponents())) {
      converted.setBlendedMobilityComponents(
          toBlendedMobilityComponents(learningAgreementMDTO.getBlendedLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (CollectionUtils.isNotEmpty(learningAgreementMDTO.getDoctoralLaComponents())) {
      converted.setShortTermDoctoralComponents(
          toShortTermDoctoralComponents(learningAgreementMDTO.getDoctoralLaComponents(), Boolean.FALSE, receivingHei));
    }

    if (Objects.nonNull(learningAgreementMDTO.getStudentSign())) {
      converted.setStudentSignature(toSignature(learningAgreementMDTO.getStudentSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getSenderCoordinatorSign())) {
      converted.setSendingHeiSignature(toSignature(learningAgreementMDTO.getSenderCoordinatorSign()));
    }
    if (Objects.nonNull(learningAgreementMDTO.getReciverCoordinatorSign())) {
      converted.setReceivingHeiSignature(toSignature(learningAgreementMDTO.getReciverCoordinatorSign()));
    }
    return converted;
  }

  private ComponentList toComponentStudied(final List<LaComponentMDTO> studiedLaComponentsList,
      final boolean isFirstVersion, final String receivingHei) {
    final ComponentList componentList = new ComponentList();
    final List<Component> convertedList = new ArrayList();
    for (final LaComponentMDTO studiedLaComponentMDTO : studiedLaComponentsList) {
      final Component component = new Component();

      String losId = null;
      if (Objects.isNull(studiedLaComponentMDTO.getLosId()) && Objects.nonNull(studiedLaComponentMDTO.getLosCode())) {
        losId = learningAgreementDAO.findLosByLosCodeAndInstitution(studiedLaComponentMDTO.getLosCode(), receivingHei);
      }
      component.setLosId(losId);
      component.setLosCode(studiedLaComponentMDTO.getLosCode());
      component.setTitle(studiedLaComponentMDTO.getTitle());
      component.setLoiId(
          Objects.isNull(studiedLaComponentMDTO.getLoiId()) ? null : studiedLaComponentMDTO.getLoiId().getId());
      component.setTermId(toTermId(studiedLaComponentMDTO.getAcademicTermDisplayName()));
      component.getCredit().addAll(toCreditList(studiedLaComponentMDTO.getCredits()));
      if (!isFirstVersion && Objects.nonNull(studiedLaComponentMDTO.getStatus())) {
        component.setStatus(studiedLaComponentMDTO.getStatus().value());
        component.setReasonCode(Objects.nonNull(studiedLaComponentMDTO.getReasonCode()) ? studiedLaComponentMDTO.getReasonCode().value() : null);
        component.setReasonText(studiedLaComponentMDTO.getReasonText());
      }
      convertedList.add(component);
    }
    componentList.getComponent().addAll(convertedList);

    return componentList;

  }

  private ComponentList toComponentRecognized(final List<LaComponentMDTO> recognizedLaComponentsList,
      final boolean isFirstVersion, final String sendingHei) {
    final ComponentList componentList = new ComponentList();
    final List<Component> convertedList = new ArrayList();
    for (final LaComponentMDTO recognizedLaComponentMDTO : recognizedLaComponentsList) {
      final Component component = new Component();
      String losId = null;
      if (Objects.isNull(recognizedLaComponentMDTO.getLosId())
          && Objects.nonNull(recognizedLaComponentMDTO.getLosCode())) {
        losId = learningAgreementDAO.findLosByLosCodeAndInstitution(recognizedLaComponentMDTO.getLosCode(), sendingHei);
      }
      component.setLosId(losId);
      component.setLosCode(recognizedLaComponentMDTO.getLosCode());
      component.setTitle(recognizedLaComponentMDTO.getTitle());
      component.setLoiId(
          Objects.isNull(recognizedLaComponentMDTO.getLoiId()) ? null : recognizedLaComponentMDTO.getLoiId().getId());
      component.setTermId(toTermId(recognizedLaComponentMDTO.getAcademicTermDisplayName()));
      component.getCredit().addAll(toCreditList(recognizedLaComponentMDTO.getCredits()));
      component.setRecognitionConditions(recognizedLaComponentMDTO.getRecognitionConditions());
      if (!isFirstVersion && Objects.nonNull(recognizedLaComponentMDTO.getStatus())) {
        component.setStatus(recognizedLaComponentMDTO.getStatus().value());
        component.setReasonCode(Objects.nonNull(recognizedLaComponentMDTO.getReasonCode()) ? recognizedLaComponentMDTO.getReasonCode().value() : null);
        component.setReasonText(recognizedLaComponentMDTO.getReasonText());
      }
      convertedList.add(component);
    }
    componentList.getComponent().addAll(convertedList);

    return componentList;
  }

  private ComponentList toVirtualComponents(final List<LaComponentMDTO> virtualLaComponentsList,
      final boolean isFirstVersion, final String receivingHei) {
    final ComponentList componentList = new ComponentList();
    final List<Component> convertedList = new ArrayList();
    for (final LaComponentMDTO virtualLaComponentMDTO : virtualLaComponentsList) {
      final Component component = new Component();
      String losId = null;
      if (Objects.isNull(virtualLaComponentMDTO.getLosId()) && Objects.nonNull(virtualLaComponentMDTO.getLosCode())) {
        losId = learningAgreementDAO.findLosByLosCodeAndInstitution(virtualLaComponentMDTO.getLosCode(), receivingHei);
      }
      component.setLosId(losId);
      component
          .setLosCode(Objects.isNull(virtualLaComponentMDTO.getLosCode()) ? null : virtualLaComponentMDTO.getLosCode());
      component.setTitle(virtualLaComponentMDTO.getTitle());
      component.setLoiId(
          Objects.isNull(virtualLaComponentMDTO.getLoiId()) ? null : virtualLaComponentMDTO.getLoiId().getId());
      component.setTermId(toTermId(virtualLaComponentMDTO.getAcademicTermDisplayName()));
      component.getCredit().addAll(toCreditList(virtualLaComponentMDTO.getCredits()));
      component.setRecognitionConditions(virtualLaComponentMDTO.getRecognitionConditions());
      if (!isFirstVersion && Objects.nonNull(virtualLaComponentMDTO.getStatus())) {
        component.setStatus(virtualLaComponentMDTO.getStatus().value());
        component.setReasonCode(Objects.nonNull(virtualLaComponentMDTO.getReasonCode()) ? virtualLaComponentMDTO.getReasonCode().value() : null);
        component.setReasonText(virtualLaComponentMDTO.getReasonText());
      }
      convertedList.add(component);
    }
    componentList.getComponent().addAll(convertedList);

    return componentList;
  }

  private ComponentList toBlendedMobilityComponents(final List<LaComponentMDTO> blendedLaComponentsList,
      final boolean isFirstVersion, final String receivingHei) {
    final ComponentList componentList = new ComponentList();
    final List<Component> convertedList = new ArrayList();
    for (final LaComponentMDTO blendedLaComponentMDTO : blendedLaComponentsList) {
      final Component component = new Component();
      String losId = null;
      if (Objects.isNull(blendedLaComponentMDTO.getLosId()) && Objects.nonNull(blendedLaComponentMDTO.getLosCode())) {
        losId = learningAgreementDAO.findLosByLosCodeAndInstitution(blendedLaComponentMDTO.getLosCode(), receivingHei);
      }
      component.setLosId(losId);
      component.setLosCode(blendedLaComponentMDTO.getLosCode());
      component.setTitle(blendedLaComponentMDTO.getTitle());
      component.setLoiId(
          Objects.isNull(blendedLaComponentMDTO.getLoiId()) ? null : blendedLaComponentMDTO.getLoiId().getId());
      component.setTermId(toTermId(blendedLaComponentMDTO.getAcademicTermDisplayName()));
      component.getCredit().addAll(toCreditList(blendedLaComponentMDTO.getCredits()));
      component.setRecognitionConditions(blendedLaComponentMDTO.getRecognitionConditions());
      if (!isFirstVersion && Objects.nonNull(blendedLaComponentMDTO.getStatus())) {
        component.setStatus(blendedLaComponentMDTO.getStatus().value());
        component.setReasonCode(Objects.nonNull(blendedLaComponentMDTO.getReasonCode()) ? blendedLaComponentMDTO.getReasonCode().value() : null);
        component.setReasonText(blendedLaComponentMDTO.getReasonText());
      }
      convertedList.add(component);
    }
    componentList.getComponent().addAll(convertedList);

    return componentList;
  }

  private ComponentList toShortTermDoctoralComponents(final List<LaComponentMDTO> doctoralLaComponentsList,
      final boolean isFirstVersion, final String receivingHei) {
    final ComponentList componentList = new ComponentList();
    final List<Component> convertedList = new ArrayList();
    for (final LaComponentMDTO doctoralLaComponentMDTO : doctoralLaComponentsList) {
      final Component component = new Component();
      String losId = null;
      if (Objects.isNull(doctoralLaComponentMDTO.getLosId()) && Objects.nonNull(doctoralLaComponentMDTO.getLosCode())) {
        losId = learningAgreementDAO.findLosByLosCodeAndInstitution(doctoralLaComponentMDTO.getLosCode(), receivingHei);
      }
      component.setLosId(losId);
      component.setLosCode(doctoralLaComponentMDTO.getLosCode());
      component.setTitle(doctoralLaComponentMDTO.getTitle());
      component.setLoiId(
          Objects.isNull(doctoralLaComponentMDTO.getLoiId()) ? null : doctoralLaComponentMDTO.getLoiId().getId());
      component.setTermId(toTermId(doctoralLaComponentMDTO.getAcademicTermDisplayName()));
      component.getCredit().addAll(toCreditList(doctoralLaComponentMDTO.getCredits()));
      component.setRecognitionConditions(doctoralLaComponentMDTO.getRecognitionConditions());
      if (!isFirstVersion && Objects.nonNull(doctoralLaComponentMDTO.getStatus())) {
        component.setStatus(doctoralLaComponentMDTO.getStatus().value());
        component.setReasonCode(Objects.nonNull(doctoralLaComponentMDTO.getReasonCode()) ? doctoralLaComponentMDTO.getReasonCode().value() : null);
        component.setReasonText(doctoralLaComponentMDTO.getReasonText());
      }
      convertedList.add(component);
    }
    componentList.getComponent().addAll(convertedList);

    return componentList;
  }

  private TermId toTermId(final AcademicTermMDTO academicTermMDTO) {
    if (Objects.nonNull(academicTermMDTO)) {
      final TermId term = new TermId();
      term.setTermNumber(academicTermMDTO.getTermNumber());
      term.setTotalTerms(academicTermMDTO.getTotalTerms());
      return term;
    } else {
      return null;
    }
  }

  private Signature toSignature(final SignatureMDTO signatureMDTO) throws EwpConverterException {
    final Signature signature = new Signature();
    signature.setSignerApp(signatureMDTO.getSignerApp());
    signature.setSignerEmail(signatureMDTO.getSignerEmail());
    signature.setSignerName(signatureMDTO.getSignerName());
    signature.setSignerPosition(signatureMDTO.getSignerPosition());
    signature.setTimestamp(CommonsConverter.convertToGregorianCalendarWithTime(signatureMDTO.getTimestamp()));
    return signature;
  }

  private List<Component.Credit> toCreditList(final List<CreditMDTO> credits) {
    final List<Component.Credit> creditList = new ArrayList<>();
    if (Objects.nonNull(credits) && !credits.isEmpty()) {
      for (final CreditMDTO creditMDTO : credits) {
        if (Objects.nonNull(creditMDTO)) {
          final Component.Credit credit = new Component.Credit();
          credit.setScheme(creditMDTO.getScheme());
          credit.setValue(creditMDTO.getValue());
          creditList.add(credit);
        }
      }
    }
    return creditList;
  }

  private LaComponentMDTO toComponentMDTO(final Component component, final String institutionId) {
    LaComponentMDTO dto = null;
    if (Objects.nonNull(component)) {
      dto = new LaComponentMDTO();
      dto.setTitle(component.getTitle());
      dto.setLosCode(component.getLosCode());
      dto.setShortDescription(component.getShortDescription());
      dto.setRecognitionConditions(component.getRecognitionConditions());
      dto.setAcademicTermDisplayName(toAcademicTermMDTO(component.getTermId()));
      dto.setCredits(toCreditsMDTO(component.getCredit()));
      dto.setRecognitionConditions(component.getRecognitionConditions());
      dto.setLosId(toLearningOpportunitySpecificationMDTO(component, institutionId));
      dto.setLoiId(toLearningOpportunityInstanceMDTO(component, dto.getAcademicTermDisplayName(), dto.getLosId()));
    }
    return dto;
  }

  private LearningOpportunityInstanceMDTO toLearningOpportunityInstanceMDTO(final Component component,
      final AcademicTermMDTO academicTerm, final LearningOpportunitySpecificationMDTO los) {
    if (!Strings.isNullOrEmpty(component.getLoiId())) {
      // buscamos el LOI en bbdd
      LearningOpportunityInstanceMDTO loi = learningAgreementDAO.findLoiById(component.getLoiId());

      if (Objects.isNull(loi)) {
        // si el loi no existe lo creamos usando los datos del component
        loi = new LearningOpportunityInstanceMDTO();
        loi.setId(component.getLoiId());
        loi.setStartDate(academicTerm.getStartDate());
        loi.setEndDate(academicTerm.getEndDate());

        if (Objects.nonNull(los)) {
          loi.setLearningOpportunitySpecification(los);
        }
      }
      return loi;
    }
    return null;
  }

  private LearningOpportunitySpecificationMDTO toLearningOpportunitySpecificationMDTO(final Component component,
      final String institutionId) {
    // buscamos el los por el id o, si no tiene id, por el los code
    // dependiendo del tipo de componente la institucion asociada al los es diferente ->
    // para recognized es sendig, para el resto receiving
    LearningOpportunitySpecificationMDTO res =
        learningAgreementDAO.findLosByIdOrLosCode(component.getLosId(), component.getLosCode(), institutionId);

    if (Objects.isNull(res) && !Strings.isNullOrEmpty(component.getLosId())) {
      // no hemos encontrado el LOS en bbdd, creamos un nuevo LOS
      res = new LearningOpportunitySpecificationMDTO();
      res.setId(component.getLosId());
      res.setLosCode(component.getLosCode());
      res.setType(findLosTypeByLosId(component.getLosId()));
      res.setInstitutionId(institutionId);
    }

    return res;
  }

  private LearningOpportunitySpecificationType findLosTypeByLosId(final String losId) {
    final String typePrefix = losId.split("/")[0];
    return LearningOpportunitySpecificationType.getTypeByAbreviation(typePrefix);
  }

  private List<LaComponentMDTO> toComponentMDTOList(final ComponentList componentStudied, final LaComponentType type,
      final boolean isFirstVersion, final String sendingHeiId, final String receivingHeiId) {
    final List<LaComponentMDTO> convertedList = new ArrayList<>();

    for (final Component component : componentStudied.getComponent()) {
      // Dependiendo del tipo, pasamos el receivingHei o el sendingHei cuando convertimos el component en MDTO
      final String heiId = type.equals(LaComponentType.RECOGNIZED_COMPONENT) ? sendingHeiId : receivingHeiId;

      final LaComponentMDTO dto = toComponentMDTO(component, heiId);
      if (Objects.nonNull(dto)) {
        dto.setLaComponentType(type);
        if (!isFirstVersion && Objects.nonNull(component.getStatus())) {
          dto.setStatus(LearningAgreementComponentStatus.valueOf(component.getStatus().toUpperCase()));
          if (LaComponentType.STUDIED_COMPONENT.compareTo(type) == 0) {
            dto.setReasonCode(LaComponentReasonCode.fromValue(component.getReasonCode()));
            dto.setReasonText(component.getReasonText());
          }
        }
        convertedList.add(dto);
      }
    }
    return convertedList;
  }

  private List<CreditMDTO> toCreditsMDTO(final List<Component.Credit> credits) {
    final List<CreditMDTO> creditList = new ArrayList();
    for (final Component.Credit credit : credits) {
      final CreditMDTO creditMDTO = new CreditMDTO();
      creditMDTO.setScheme(credit.getScheme());
      creditMDTO.setValue(credit.getValue());
      creditList.add(creditMDTO);
    }
    return creditList;
  }

  private AcademicTermMDTO toAcademicTermMDTO(final TermId termId) {
    final AcademicTermMDTO term = new AcademicTermMDTO();

    if (Objects.nonNull(termId)) {
      term.setTermNumber(termId.getTermNumber());
      term.setTotalTerms(termId.getTotalTerms());
    }
    return term;
  }

  public LearningAgreementMDTO convertToLearningAgreementMDTO(final LearningAgreement.ChangesProposal changes,
      final String id, final String sendingHeiId, final String receivingHeiId) throws EwpConverterException {
    LearningAgreementMDTO mdto = null;
    if (Objects.nonNull(changes)) {
      mdto = convertToLearningAgreementMDTO(changes, LearningAgreementStatus.CHANGES_PROPOSED,
              id, sendingHeiId, receivingHeiId);
      mdto.setChangesId(changes.getId());
    }
    return mdto;
  }

  public LearningAgreementMDTO convertToLearningAgreementMDTO(final ListOfComponents components,
      final LearningAgreementStatus status, final String id, final String sendingHeiId, final String receivingHeiId)
      throws EwpConverterException {
    if (Objects.nonNull(components)) {
      final LearningAgreementMDTO mdto = new LearningAgreementMDTO();
      mdto.setStatus(status);
      mdto.setId(id);
      mdto.setModifiedDate(new Date());

      if (Objects.nonNull(components.getComponentsStudied())) {
        mdto.setStudiedLaComponents(
            toComponentMDTOList(components.getComponentsStudied(), LaComponentType.STUDIED_COMPONENT,
                LearningAgreementStatus.FIRST_VERSION.compareTo(status) == 0, sendingHeiId, receivingHeiId));
      }

      if (Objects.nonNull(components.getComponentsRecognized())) {
        mdto.setRecognizedLaComponents(
            toComponentMDTOList(components.getComponentsRecognized(), LaComponentType.RECOGNIZED_COMPONENT,
                LearningAgreementStatus.FIRST_VERSION.compareTo(status) == 0, sendingHeiId, receivingHeiId));
      }

      if (Objects.nonNull(components.getVirtualComponents())) {
        mdto.setVirtualLaComponents(
            toComponentMDTOList(components.getVirtualComponents(), LaComponentType.VIRTUAL_COMPONENT,
                LearningAgreementStatus.FIRST_VERSION.compareTo(status) == 0, sendingHeiId, receivingHeiId));
      }

      if (Objects.nonNull(components.getBlendedMobilityComponents())) {
        mdto.setBlendedLaComponents(
            toComponentMDTOList(components.getBlendedMobilityComponents(), LaComponentType.BLENDED_COMPONENT,
                LearningAgreementStatus.FIRST_VERSION.compareTo(status) == 0, sendingHeiId, receivingHeiId));
      }

      if (Objects.nonNull(components.getShortTermDoctoralComponents())) {
        mdto.setDoctoralLaComponents(toComponentMDTOList(components.getShortTermDoctoralComponents(),
            LaComponentType.SHORT_TER_DOCTORAL_COMPONENT, LearningAgreementStatus.FIRST_VERSION.compareTo(status) == 0,
            sendingHeiId, receivingHeiId));
      }

      if (Objects.nonNull(components.getStudentSignature())) {
        mdto.setStudentSign(toSignatureMDTO(components.getStudentSignature()));
      }

      if (Objects.nonNull(components.getSendingHeiSignature())) {
        mdto.setSenderCoordinatorSign(toSignatureMDTO(components.getSendingHeiSignature()));
      }

      if (Objects.nonNull(components.getReceivingHeiSignature())) {
        mdto.setReciverCoordinatorSign(toSignatureMDTO(components.getReceivingHeiSignature()));
      }
      return mdto;
    }
    return null;
  }

  private SignatureMDTO toSignatureMDTO(final Signature signature) throws EwpConverterException {
    final SignatureMDTO signatureMDTO = new SignatureMDTO();
    signatureMDTO.setSignerApp(signature.getSignerApp());
    signatureMDTO.setSignerEmail(signature.getSignerEmail());
    signatureMDTO.setSignerName(signature.getSignerName());
    signatureMDTO.setSignerPosition(signature.getSignerPosition());
    signatureMDTO.setTimestamp(signature.getTimestamp().toGregorianCalendar().getTime());
    return signatureMDTO;
  }

  private List<LaComponentMDTO> componentStudiedToComponentMDTO(final ComponentList componentStudied,
      final boolean isFirstVersion) {
    final List<LaComponentMDTO> convertedList = new ArrayList<>();

    for (final Component component : componentStudied.getComponent()) {
      final LaComponentMDTO dto = new LaComponentMDTO();

      dto.setTitle(component.getTitle());
      dto.setLosCode(component.getLosCode());
      dto.setShortDescription(component.getShortDescription());
      dto.setRecognitionConditions(component.getRecognitionConditions());
      dto.setAcademicTermDisplayName(toAcademicTermMDTO(component.getTermId()));
      dto.setCredits(toCreditsMDTO(component.getCredit()));
      dto.setRecognitionConditions(component.getRecognitionConditions());

      // TODO: Faltaría por añadir los LosId y LoiId

      if (!isFirstVersion && Objects.nonNull(component.getStatus())) {
        dto.setStatus(LearningAgreementComponentStatus.valueOf(component.getStatus().toUpperCase()));
      }
      convertedList.add(dto);
    }
    return convertedList;
  }

}
