package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.converter.api.IStatisticsILAConverter;
import es.minsait.ewpcv.repository.model.statistics.StatisticsILAMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasIncomingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Statitics ILA converter.
 *
 * @author (ilanciano@minsait.com)
 */
@Component
@Slf4j
public class StatisticsILAConverter implements IStatisticsILAConverter {

    @Override
    public LasIncomingStatsResponse.AcademicYearLaStats convertToStatsResponse(StatisticsILAMDTO mdto) {
        LasIncomingStatsResponse.AcademicYearLaStats responseItem = new LasIncomingStatsResponse.AcademicYearLaStats();

        responseItem.setReceivingAcademicYearId(mdto.getAcademicYearStart()+"/"+mdto.getAcademicYearEnd());
        responseItem.setLaIncomingTotal(BigInteger.valueOf(mdto.getTotalNumber()));
        responseItem.setLaIncomingSomeVersionApproved(BigInteger.valueOf(mdto.getSameVersionAppr()));
        responseItem.setLaIncomingLatestVersionApproved(BigInteger.valueOf(mdto.getLastAppr()));
        responseItem.setLaIncomingLatestVersionRejected(BigInteger.valueOf(mdto.getLastRej()));
        responseItem.setLaIncomingLatestVersionAwaiting(BigInteger.valueOf(mdto.getLastWait()));

        return responseItem;
    }

    @Override
    public List<LasIncomingStatsResponse.AcademicYearLaStats> convertToStatsResponses(List<StatisticsILAMDTO> mdtos) {
        final List<LasIncomingStatsResponse.AcademicYearLaStats> respuesta = new ArrayList<>();

        for (final StatisticsILAMDTO mdto : mdtos) {
            respuesta.add(convertToStatsResponse(mdto));
        }
        return respuesta;
    }


}
