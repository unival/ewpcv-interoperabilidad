package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IStatisticsIiaConverter;
import es.minsait.ewpcv.repository.dao.IStatisticsIiaDAO;
import es.minsait.ewpcv.repository.model.iia.IiaStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;
import es.minsait.ewpcv.service.api.IStatisticsIiaService;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * The interface StatisticsIia service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */

@Service
@Slf4j
@Transactional
public class StatisticsIiaServiceImpl implements IStatisticsIiaService {

    @Autowired
    private IStatisticsIiaDAO statisticsIiaDAO;
    @Autowired
    private IStatisticsIiaConverter iStatisticsIiaConverter;

    @Override
    public void obtainStatistics() {

        List<IiaStatsMDTO> omoMdtos = statisticsIiaDAO.getOmobilityIiaStats();

        for (IiaStatsMDTO omoMdto : omoMdtos) {
            StatisticsIiaMDTO statIia = new StatisticsIiaMDTO();

            statIia.setFetchable(omoMdto.getFetchable());
            statIia.setBothApproved(omoMdto.getBothApproved());
            statIia.setLocalUnappr_partnerAppr(omoMdto.getLocalUnappr_partnerAppr());
            statIia.setLocalAppr_partnerUnappr(omoMdto.getLocalAppr_partnerUnappr());

            statIia.setDumpDate(new Date());

            statisticsIiaDAO.saveStatsIia(statIia);
        }
    }
    @Override
    public IiasStatsResponse getStatisticsRest() {
        return iStatisticsIiaConverter.convertToStatsResponse(statisticsIiaDAO.getStatisticsRest());
    }
}
