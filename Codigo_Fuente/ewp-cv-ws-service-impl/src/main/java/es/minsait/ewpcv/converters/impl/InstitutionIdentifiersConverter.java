package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.converter.api.IInstitutionIdentifiersConverter;
import es.minsait.ewpcv.registryclient.HeiEntry;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import eu.erasmuswithoutpaper.api.registry.OtherHeiId;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * The type Institution identifiers converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class InstitutionIdentifiersConverter implements IInstitutionIdentifiersConverter {

  private static final String OTHER_ID_SPLIT_CODE_SEPARATOR = "\\|;\\|";
  private static final String OTHER_ID_SPLIT_VALUE_SEPARATOR = "\\|:\\|";

  @Override
  public InstitutionIdentifiersMDTO convertToInstitutionIdentifiersMDTO(final HeiEntry heiEntry) {
    final InstitutionIdentifiersMDTO heiMdto = new InstitutionIdentifiersMDTO();
    final Map<String, List<String>> ids = new HashMap<>();
    for (final Map.Entry<String, List<String>> entry : heiEntry.getOtherIds().entrySet()) {
      ids.put(entry.getKey(), entry.getValue());
    }
    heiMdto.setSchac(heiEntry.getId());
    heiMdto.setPreviousSchac(convertIdListToString(ids.get("previous-schac")));

    heiMdto.setPic(convertIdListToString(ids.get("pic")));
    heiMdto.setErasmus(convertIdListToString(ids.get("erasmus")));
    heiMdto.setEuc(convertIdListToString(ids.get("euc")));
    heiMdto.setErasmusCharter(convertIdListToString(ids.get("erasmus-charter")));

    heiMdto.setInstitutionNames(convertNamesToStirng(heiEntry.getAllNames()));

    ids.remove("previous-schac");
    ids.remove("pic");
    ids.remove("erasmus");
    ids.remove("euc");
    ids.remove("erasmus-charter");
    heiMdto.setOtherIds(convertOtherIdsListToOtherIdsStirng(ids));
    return heiMdto;
  }

  private String convertOtherIdsListToOtherIdsStirng(final Map<String, List<String>> otherIds) {
    if (otherIds.isEmpty()) {
      return null;
    }
    final StringJoiner sjComa = new StringJoiner("|;|");

    for (final Map.Entry<String, List<String>> entry : otherIds.entrySet()) {
      final StringJoiner sjDot = new StringJoiner("|:|");
      sjDot.add(entry.getKey()).add(entry.getValue().get(0));
      sjComa.add(sjDot.toString());
    }

    return sjComa.toString();
  }

  private String convertIdListToString(final List<String> otherIds) {
    return CollectionUtils.isNotEmpty(otherIds) ? String.join("|;|", otherIds) : null;
  }

  private String convertNamesToStirng(final Map<String, String> names) {

    final StringJoiner sjComa = new StringJoiner("|;|");
    for (final String key : names.keySet()) {
      final StringJoiner sjDot = new StringJoiner("|:|");
      sjDot.add(names.get(key)).add(key);
      sjComa.add(sjDot.toString());
    }
    return sjComa.toString();
  }

  public List<OtherHeiId> convertOtherIdsStringToOtherHeiId(final InstitutionIdentifiersMDTO institutionId) {
    final List<OtherHeiId> res = new ArrayList<>();

    if (StringUtils.isNotEmpty(institutionId.getPreviousSchac())) {
      res.add(buildOtherHeiId("previous-schac", institutionId.getPreviousSchac()));
    }
    if (StringUtils.isNotEmpty(institutionId.getPic())) {
      res.add(buildOtherHeiId("pic", institutionId.getPic()));
    }
    if (StringUtils.isNotEmpty(institutionId.getErasmus())) {
      res.add(buildOtherHeiId("erasmus", institutionId.getErasmus()));
    }
    if (StringUtils.isNotEmpty(institutionId.getEuc())) {
      res.add(buildOtherHeiId("euc", institutionId.getEuc()));
    }
    if (StringUtils.isNotEmpty(institutionId.getErasmusCharter())) {
      res.add(buildOtherHeiId("erasmus-charter", institutionId.getErasmusCharter()));
    }

    if (StringUtils.isNotEmpty(institutionId.getOtherIds())) {
      final String[] otherIdsStrings = institutionId.getOtherIds().split(OTHER_ID_SPLIT_CODE_SEPARATOR);

      for (final String idString : otherIdsStrings) {
        final OtherHeiId id = new OtherHeiId();
        final String[] codeValue = idString.split(OTHER_ID_SPLIT_VALUE_SEPARATOR);
        if (codeValue.length > 0) {
          id.setType(codeValue[0]);
          id.setValue(codeValue[1]);
          res.add(id);
        }
      }
    }

    return res;
  }

  private OtherHeiId buildOtherHeiId(String type, String value) {
    final OtherHeiId otherHeiId = new OtherHeiId();
    otherHeiId.setType(type);
    otherHeiId.setValue(value);
    return otherHeiId;
  }
}
