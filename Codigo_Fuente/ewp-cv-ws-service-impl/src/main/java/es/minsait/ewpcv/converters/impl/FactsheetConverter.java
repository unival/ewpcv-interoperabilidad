package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.converter.api.FactshetConverter;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.model.organization.AdditionalRequirementType;
import es.minsait.ewpcv.model.organization.InformationItemType;
import es.minsait.ewpcv.model.organization.LanguageItem;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.factsheet.CalendarEntry;
import eu.erasmuswithoutpaper.api.factsheet.FactsheetResponse;
import eu.erasmuswithoutpaper.api.factsheet.InformationEntry;
import eu.erasmuswithoutpaper.api.types.phonenumber.PhoneNumber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nullable;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Factsheet converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class FactsheetConverter implements FactshetConverter {

  @Override
  public List<FactsheetResponse.Factsheet> convertToFactsheet(final List<InstitutionMDTO> institutions)
      throws EwpConverterException {
    final List<FactsheetResponse.Factsheet> resultado = new ArrayList<>();

    if (!CollectionUtils.isEmpty(institutions)) {
      for (final InstitutionMDTO hei : institutions) {
        final FactSheetMDTO factsheet = hei.getFactSheet();
        if (Objects.nonNull(factsheet) && Objects.nonNull(factsheet.getTorWeeksLimit())) {
          final FactsheetResponse.Factsheet f = convertSingleFactsheet(hei, factsheet);
          resultado.add(f);
        }
      }
    }

    return resultado;
  }

  private FactsheetResponse.Factsheet convertSingleFactsheet(final InstitutionMDTO hei, final FactSheetMDTO factsheet)
      throws EwpConverterException {
    final FactsheetResponse.Factsheet f = new FactsheetResponse.Factsheet();
    f.setHeiId(hei.getInstitutionId());

    /** CALENDARIO - STUDENT APPLICATIONS **/

    final FactsheetResponse.Factsheet.Calendar calendar = new FactsheetResponse.Factsheet.Calendar();

    // Student applications
    final CalendarEntry studentApplications = new CalendarEntry();
    studentApplications
        .setSpringTerm(CommonsConverter.convertToGregorianCalendarMonthDay(factsheet.getApplicationSpringTerm()));
    studentApplications
        .setAutumnTerm(CommonsConverter.convertToGregorianCalendarMonthDay(factsheet.getApplicationAutumnTerm()));
    calendar.setStudentApplications(studentApplications);

    /** CALENDARIO - STUDENT NOMINATIONS **/

    // Student nominations
    final CalendarEntry studentNominations = new CalendarEntry();
    studentNominations
        .setSpringTerm(CommonsConverter.convertToGregorianCalendarMonthDay(factsheet.getNominationsSpringTerm()));
    studentNominations
        .setAutumnTerm(CommonsConverter.convertToGregorianCalendarMonthDay(factsheet.getNominationsAutumnTerm()));
    calendar.setStudentNominations(studentNominations);

    f.setCalendar(calendar);
    /** APPLICATION INFO **/
    f.setApplicationInfo(crearInformationEntry(factsheet.getContactDetails()));

    /** WEEKS LIMIT **/
    f.setDecisionWeeksLimit(BigInteger.valueOf(factsheet.getDecisionWeeksLimit()));
    f.setTorWeeksLimit(BigInteger.valueOf(factsheet.getTorWeeksLimit()));

    /** ACCESSIBILITY **/
    for (final RequirementsInfoMDTO accesibilityItem : hei.getAdditionalRequirements()) {
      if (accesibilityItem.getType().equalsIgnoreCase(AdditionalRequirementType.SERVICE.name())
              || accesibilityItem.getType().equalsIgnoreCase(AdditionalRequirementType.INFRASTRUCTURE.name())) {
        final FactsheetResponse.Factsheet.Accessibility accessibility = new FactsheetResponse.Factsheet.Accessibility();
        accessibility
            .setType(Objects.nonNull(accesibilityItem.getType()) ? accesibilityItem.getType().toLowerCase() : null);
        accessibility.setName(accesibilityItem.getName());
        accessibility.setDescription(accesibilityItem.getDescripcion());
        accessibility.setInformation(crearInformationEntry(accesibilityItem.getContactDetails()));
        f.getAccessibility().add(accessibility);
      }
    }

    if (Objects.nonNull(hei.getInformationItems())) {
      /** HOUSING INFO **/
      final InformationItemMDTO housingItem =
          getInformationItemByType(InformationItemType.HOUSING, hei.getInformationItems());
      if (Objects.nonNull(housingItem)) {
        f.setHousingInfo(crearInformationEntry(housingItem.getContactDetails()));
      }

      /** VISA INFO **/
      final InformationItemMDTO visaItem =
          getInformationItemByType(InformationItemType.VISA, hei.getInformationItems());
      if (Objects.nonNull(visaItem)) {
        f.setVisaInfo(crearInformationEntry(visaItem.getContactDetails()));
      }

      /** INSURANCE INFO **/
      final InformationItemMDTO insuranceItem =
          getInformationItemByType(InformationItemType.INSURANCE, hei.getInformationItems());
      if (Objects.nonNull(insuranceItem)) {
        f.setInsuranceInfo(crearInformationEntry(insuranceItem.getContactDetails()));
      }

      /** ADDITIONAL INFO **/
      // Anadimos la informacion adicional que sean necesarios
      final List<InformationItemMDTO> additionalInfos = getAdditionalInfos(hei.getInformationItems());
      if (!additionalInfos.isEmpty()) {
        for (final InformationItemMDTO ident : additionalInfos) {
          final FactsheetResponse.Factsheet.AdditionalInfo additionalInfo =
              new FactsheetResponse.Factsheet.AdditionalInfo();
          additionalInfo.setType(ident.getType());
          additionalInfo.setInfo(crearInformationEntry(ident.getContactDetails()));
          f.getAdditionalInfo().add(additionalInfo);
        }
      }
    }

    /** ADDITIONAL REQUIREMENTS **/
    // anadimos los 'aditional-requirement' que sean necesarios

    for (final RequirementsInfoMDTO requirement : hei.getAdditionalRequirements()) {
      if (!requirement.getType().equalsIgnoreCase(AdditionalRequirementType.SERVICE.name())
          && !requirement.getType().equalsIgnoreCase(AdditionalRequirementType.INFRASTRUCTURE.name())) {

        final FactsheetResponse.Factsheet.AdditionalRequirement aditionalReq =
            new FactsheetResponse.Factsheet.AdditionalRequirement();
        aditionalReq.setName(requirement.getName());
        aditionalReq.setDetails(requirement.getDescripcion());

        for (final LanguageItemMDTO url : requirement.getContactDetails().getUrl()) {
          final HTTPWithOptionalLang website = new HTTPWithOptionalLang();
          website.setLang(url.getLang());
          website.setValue(url.getText());
          aditionalReq.getInformationWebsite().add(website);
        }

        f.getAdditionalRequirement().add(aditionalReq);
      }
    }
    return f;
  }


  private InformationEntry crearInformationEntry(final ContactDetailsMDTO contactDetails) {
    return crearInformationEntry(contactDetails.getEmail().isEmpty() ? null : contactDetails.getEmail().get(0),
        contactDetails.getPhoneNumber(),
        contactDetails.getUrl());
  }

  private InformationEntry crearInformationEntry(final String email, final @Nullable PhoneNumberMDTO telefono,
      final List<LanguageItemMDTO> urlItems) {
    final InformationEntry infoEntry = new InformationEntry();
    infoEntry.setEmail(email);
    if (telefono != null) {
      final PhoneNumber housePhoneNumber = new PhoneNumber();
      housePhoneNumber.setE164(telefono.getE164());
      housePhoneNumber.setExt(telefono.getExtensionNumber());
      housePhoneNumber.setOtherFormat(telefono.getOtherFormat());
      infoEntry.setPhoneNumber(housePhoneNumber);
    }
    for (final LanguageItemMDTO item : urlItems) {
      final HTTPWithOptionalLang url = new HTTPWithOptionalLang();
      url.setLang(item.getLang());
      url.setValue(item.getText());
      infoEntry.getWebsiteUrl().add(url);
    }
    return infoEntry;
  }

  private InformationItemMDTO getInformationItemByType(final InformationItemType type,
      final List<InformationItemMDTO> informations) {
    final Optional<InformationItemMDTO> res;
    res = informations.stream().filter(item -> item.getType().equalsIgnoreCase(type.name())).findFirst();

    return res.isPresent() ? res.get() : null;
  }

  private List<InformationItemMDTO> getAdditionalInfos(final List<InformationItemMDTO> identifications) {
    final List<InformationItemMDTO> res;
    final List<String> values = Arrays.asList(InformationItemType.types());
    res = identifications.stream().filter(item -> !values.contains(item.getType())).collect(Collectors.toList());

    return res;
  }

  private RequirementsInfoMDTO getRequirementInfoType(final String type,
      final List<RequirementsInfoMDTO> identifications) {
    final Optional<RequirementsInfoMDTO> res;
    res = identifications.stream().filter(item -> item.getType().equalsIgnoreCase(type)).findFirst();

    return res.isPresent() ? res.get() : null;
  }

  @Override
  public InstitutionMDTO convertToInstitutionMDTO(final FactsheetResponse.Factsheet factsheet) {

    final InstitutionMDTO institution = convertSingleFactsheetToMDTO(factsheet);

    return institution;
  }

  @Override
  public List<InstitutionMDTO> convertToInstitutionMDTO(final List<FactsheetResponse.Factsheet> factsheets) {

    final List<InstitutionMDTO> res = new ArrayList<>();

    for (final FactsheetResponse.Factsheet factsheet : factsheets) {
      res.add(convertToInstitutionMDTO(factsheet));
    }

    return res;
  }

  private InstitutionMDTO convertSingleFactsheetToMDTO(final FactsheetResponse.Factsheet factsheet) {
    final InstitutionMDTO res = new InstitutionMDTO();
    res.setFactSheet(convertToFacsheetMDTO(factsheet));
    res.setInstitutionId(factsheet.getHeiId());
    res.setInformationItems(convertInformationItems(factsheet));
    res.setAdditionalRequirements(convertAdditionalRequirements(factsheet));
    return res;
  }

  private FactSheetMDTO convertToFacsheetMDTO(final FactsheetResponse.Factsheet factsheet) {
    final FactSheetMDTO mdto = new FactSheetMDTO();

    /** WEEK LIMITS **/
    mdto.setDecisionWeeksLimit(factsheet.getDecisionWeeksLimit().intValue());
    mdto.setTorWeeksLimit(factsheet.getTorWeeksLimit().intValue());

    /** TERMS **/
    final Date aplicationAutumnTerm =
        CommonsConverter.convertToDate(factsheet.getCalendar().getStudentApplications().getAutumnTerm());
    mdto.setApplicationAutumnTerm(aplicationAutumnTerm);

    final Date aplicationSpringTerm =
        CommonsConverter.convertToDate(factsheet.getCalendar().getStudentApplications().getSpringTerm());
    mdto.setApplicationSpringTerm(aplicationSpringTerm);

    final Date nominationsAutumnTerm =
        CommonsConverter.convertToDate(factsheet.getCalendar().getStudentNominations().getAutumnTerm());
    mdto.setNominationsAutumnTerm(nominationsAutumnTerm);

    final Date nominationsSpringTerm =
        CommonsConverter.convertToDate(factsheet.getCalendar().getStudentNominations().getSpringTerm());
    mdto.setNominationsSpringTerm(nominationsSpringTerm);

    /** APPLICATION INFO **/
    final ContactDetailsMDTO contactDetails = new ContactDetailsMDTO();
    contactDetails
        .setPhoneNumber(CommonsConverter.convertToPhoneNumberMDTO(factsheet.getApplicationInfo().getPhoneNumber()));
    contactDetails.setEmail(Arrays.asList(factsheet.getApplicationInfo().getEmail()));
    contactDetails.setUrl(getLanguageItemMDTOS(factsheet.getApplicationInfo().getWebsiteUrl()));

    mdto.setContactDetails(contactDetails);

    return mdto;
  }

  private List<InformationItemMDTO> convertInformationItems(final FactsheetResponse.Factsheet factsheet) {
    final List<InformationItemMDTO> res = new ArrayList<>();

    for (final FactsheetResponse.Factsheet.AdditionalInfo info : factsheet.getAdditionalInfo()) {
      final InformationItemMDTO dto = new InformationItemMDTO();

      dto.setType(info.getType());
      dto.setContactDetails(crearContactDetails(info.getInfo()));
      res.add(dto);
    }

    /** VISA INFO **/
    res.add(converttInformatioEntry(factsheet.getVisaInfo(), InformationItemType.VISA));
    /** INSURANCE INFO **/
    res.add(converttInformatioEntry(factsheet.getInsuranceInfo(), InformationItemType.INSURANCE));
    /** HOUSING INFO **/
    res.add(converttInformatioEntry(factsheet.getHousingInfo(), InformationItemType.HOUSING));

    return res;
  }

  private InformationItemMDTO converttInformatioEntry(final InformationEntry info, final InformationItemType type) {
    final InformationItemMDTO dto = new InformationItemMDTO();

    dto.setType(type.name());
    dto.setContactDetails(crearContactDetails(info));

    return dto;
  }

  private List<RequirementsInfoMDTO> convertAdditionalRequirements(final FactsheetResponse.Factsheet factsheet) {
    final List<RequirementsInfoMDTO> res = new ArrayList<>();

    for (final FactsheetResponse.Factsheet.AdditionalRequirement req : factsheet.getAdditionalRequirement()) {
      final RequirementsInfoMDTO dto = new RequirementsInfoMDTO();

      dto.setDescripcion(req.getDetails());
      dto.setName(req.getName());
      dto.setType(AdditionalRequirementType.REQUIREMENT.name());
      dto.setContactDetails(crearContactDetails(req));

      res.add(dto);
    }

    for (final FactsheetResponse.Factsheet.Accessibility acces : factsheet.getAccessibility()) {
      final RequirementsInfoMDTO dto = new RequirementsInfoMDTO();

      dto.setDescripcion(acces.getDescription());
      dto.setName(acces.getName());
      dto.setType(acces.getType());
      dto.setContactDetails(crearContactDetails(acces.getInformation()));

      res.add(dto);
    }

    return res;
  }

  private List<LanguageItemMDTO> getLanguageItemMDTOS(final List<HTTPWithOptionalLang> info) {
    final List<LanguageItemMDTO> urls = new ArrayList<>();

    for (final HTTPWithOptionalLang web : info) {
      final LanguageItemMDTO lang = new LanguageItemMDTO();
      lang.setLang(web.getLang());
      lang.setText(web.getValue());
      urls.add(lang);
    }
    return urls;
  }

  private ContactDetailsMDTO crearContactDetails(final InformationEntry info) {
    final ContactDetailsMDTO contactDetails = new ContactDetailsMDTO();

    contactDetails.setEmail(Arrays.asList(info.getEmail()));
    contactDetails.setPhoneNumber(CommonsConverter.convertToPhoneNumberMDTO(info.getPhoneNumber()));

    contactDetails.setUrl(getLanguageItemMDTOS(info.getWebsiteUrl()));

    return contactDetails;
  }

  private ContactDetailsMDTO crearContactDetails(final FactsheetResponse.Factsheet.AdditionalRequirement info) {
    final ContactDetailsMDTO contactDetails = new ContactDetailsMDTO();
    contactDetails.setUrl(getLanguageItemMDTOS(info.getInformationWebsite()));
    return contactDetails;
  }


}
