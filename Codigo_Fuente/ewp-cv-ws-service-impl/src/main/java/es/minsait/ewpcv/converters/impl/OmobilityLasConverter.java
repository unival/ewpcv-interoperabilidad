package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.repository.impl.OrganizationUnitDAO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import es.minsait.ewpcv.converter.api.IOmobilityLasConverter;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.model.omobility.MobilityStatus;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import eu.erasmuswithoutpaper.api.architecture.Empty;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.imobilities.endpoints.NominationStatus;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Omobility las converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Component
@Slf4j
public class OmobilityLasConverter implements IOmobilityLasConverter {

  @Autowired
  private ComponentConverter componentConverter;

  @Autowired
  private OrganizationUnitDAO organizationUnitDAO;

  @Override
  public List<LearningAgreement> convertToOmobilities(final List<MobilityMDTO> omobilityList)
      throws EwpConverterException {
    final List<LearningAgreement> convertedList = new ArrayList<>();
    for (final MobilityMDTO mobilityMDTO : omobilityList) {
      OrganizationUnitMDTO sendingOunitMDTO = organizationUnitDAO.findByIdAndIntitutionSchac(mobilityMDTO.getSendingOrganizationUnitId(),
              mobilityMDTO.getSendingInstitutionId());
      OrganizationUnitMDTO receivingOunitMDTO = organizationUnitDAO.findByIdAndIntitutionSchac(mobilityMDTO.getReceivingOrganizationUnitId(),
              mobilityMDTO.getReceivingInstitutionId());
      convertedList.add(this.convertToSingleOMobility(mobilityMDTO, sendingOunitMDTO, receivingOunitMDTO));
    }
    return convertedList;
  }

  private LearningAgreement convertToSingleOMobility(final MobilityMDTO mobilityMDTO,
                                                     OrganizationUnitMDTO sendingOunitMDTO,
                                                     OrganizationUnitMDTO receivingOunitMDTO) throws EwpConverterException {
    final LearningAgreement converted = new LearningAgreement();

    Collections.sort(mobilityMDTO.getLearningAgreement(),
        Comparator.comparingInt(LearningAgreementMDTO::getLearningAgreementRevision));

    final List<LearningAgreementMDTO> firstVersion = mobilityMDTO.getLearningAgreement().stream()
        .filter(la -> la.getStatus().equals(LearningAgreementStatus.FIRST_VERSION)).collect(Collectors.toList());

    final Optional<LearningAgreementMDTO> changesProposed = mobilityMDTO.getLearningAgreement().stream()
        .filter(la -> la.getStatus().equals(LearningAgreementStatus.CHANGES_PROPOSED)).collect(Collectors.toList())
        .stream().distinct().max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision));

    final Optional<LearningAgreementMDTO> approvedChanges = mobilityMDTO.getLearningAgreement().stream()
        .filter(la -> la.getStatus().equals(LearningAgreementStatus.APROVED)).collect(Collectors.toList()).stream()
        .distinct().max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision));

    converted.setOmobilityId(mobilityMDTO.getId());
    converted.setSendingHei(MobilityInstitutionConverter.fromMobilitytoSendingHei(mobilityMDTO, sendingOunitMDTO));
    converted.setReceivingHei(MobilityInstitutionConverter.fromMobilitytoReceivingHei(mobilityMDTO, receivingOunitMDTO));
    if (Objects.nonNull(mobilityMDTO.getAcademicTerm())) {
      converted.setReceivingAcademicYearId(
          CommonsConverter.convertToAcademicYearId(mobilityMDTO.getAcademicTerm().getAcademicYear()));
    }
    if (Objects.nonNull(mobilityMDTO.getMobilityParticipantId().getContactDetails())) {
      converted.setStudent(CommonsConverter.toStudent(mobilityMDTO.getMobilityParticipantId()));
    }

    converted.setStartDate(CommonsConverter.convertToGregorianCalendar(mobilityMDTO.getPlannedArrivalDate()));
    converted.setEndDate(CommonsConverter.convertToGregorianCalendar(mobilityMDTO.getPlannedDepartureDate()));
    converted.setEqfLevelStudiedAtDeparture(mobilityMDTO.getEqfLevelDeparture().byteValue());
    converted.setIscedFCode(mobilityMDTO.getIscedCode().getIscedCode());
    converted.setIscedClarification(mobilityMDTO.getIscedCode().getIscedClarification());
    if (CollectionUtils.isNotEmpty(mobilityMDTO.getLanguageSkill())) {
      converted.setStudentLanguageSkill(toStudentLanguageSkill(mobilityMDTO.getLanguageSkill().get(0)));
    }

    if (!firstVersion.isEmpty()) {
      converted.setFirstVersion(componentConverter.toFirstVersion(firstVersion.get(0),
          mobilityMDTO.getReceivingInstitutionId(), mobilityMDTO.getSendingInstitutionId()));
    }
    if (approvedChanges.isPresent()) {
      converted.setApprovedChanges(componentConverter.toApprovedChanges(approvedChanges.get(),
          mobilityMDTO.getReceivingInstitutionId(), mobilityMDTO.getSendingInstitutionId()));
    }
    if (changesProposed.isPresent()) {
      converted.setChangesProposal(componentConverter.toChangesProposed(changesProposed.get(),
          changesProposed.get().getModifiedStudentContactId(), changesProposed.get().getChangesId(),
          mobilityMDTO.getReceivingInstitutionId(), mobilityMDTO.getSendingInstitutionId()));
    }

    return converted;
  }

  private String convertToReceivingAcademicYear(final Date planedArrivalDate, final Date planedDepartureDate) {
    final Calendar calendar = Calendar.getInstance();
    calendar.setTime(planedArrivalDate);
    String startYear = String.valueOf(calendar.get(Calendar.YEAR));
    calendar.setTime(planedDepartureDate);
    final String endYear = String.valueOf(calendar.get(Calendar.YEAR));
    startYear = Objects.equals(startYear, endYear) ? Integer.toString((Integer.parseInt(startYear) - 1)) : startYear;
    return startYear.concat("/").concat(endYear);
  }

  private LearningAgreement.StudentLanguageSkill toStudentLanguageSkill(final LanguageSkillMDTO languageSkillMDTO) {
    final LearningAgreement.StudentLanguageSkill converted = new LearningAgreement.StudentLanguageSkill();
    converted.setCefrLevel(languageSkillMDTO.getCefrLevel());
    converted.setLanguage(languageSkillMDTO.getLanguage());
    return converted;
  }

  @Override
  public MobilityMDTO convertToOmobilityMDTO(final LearningAgreement la, final String laId, final Integer maxRevision)
      throws EwpConverterException {

    final MobilityMDTO converted = new MobilityMDTO();

    converted.setId(la.getOmobilityId());
    converted.setEqfLevelDeparture((long) la.getEqfLevelStudiedAtDeparture());
    converted.setSendingInstitutionId(la.getSendingHei().getHeiId());
    converted.setReceivingInstitutionId(la.getReceivingHei().getHeiId());
    converted.setModifyDate(new Date());

    // Ponemos esto ya que es el único estado posible en el que
    // podemos recibir cnr
    converted.setStatusOutgoing(MobilityStatus.LIVE);

    if (Objects.nonNull(la.getEndDate())) {
      converted.setPlannedDepartureDate(la.getEndDate().toGregorianCalendar().getTime());
    } else if (Objects.nonNull(la.getEndYearMonth())) {
      converted.setPlannedDepartureDate(la.getEndYearMonth().toGregorianCalendar().getTime());
    }

    if (Objects.nonNull(la.getStartDate())) {
      converted.setPlannedArrivalDate(la.getStartDate().toGregorianCalendar().getTime());
    } else if (Objects.nonNull(la.getStartYearMonth())) {
      converted.setPlannedArrivalDate(la.getStartYearMonth().toGregorianCalendar().getTime());
    }

    final SubjectAreaMDTO subjectArea = new SubjectAreaMDTO();
    subjectArea.setIscedCode(la.getIscedFCode());
    subjectArea.setIscedClarification(la.getIscedClarification());
    converted.setIscedCode(subjectArea);

    final MobilityParticipantMDTO participantMDTO =
        CommonsConverter.convertStudentIntoMobilityParticipantMDTO(la.getStudent());
    converted.setMobilityParticipantId(participantMDTO);

    final List<LearningAgreement.StudentLanguageSkill> aux = new ArrayList<>();
    aux.add(la.getStudentLanguageSkill());
    converted.setLanguageSkill(CommonsConverter.convertToLanguageSkillsMDTO(aux));

    //final OrganizationUnitMDTO sendingOrgUnit =
    //    CommonsConverter.convertMovilityInstitutionIntoOrgUnit(la.getSendingHei());
    converted.setSendingOrganizationUnitId(la.getSendingHei().getOunitId());

    //final OrganizationUnitMDTO receivingOrgUnit =
    //    CommonsConverter.convertMovilityInstitutionIntoOrgUnit(la.getReceivingHei());
    converted.setReceivingOrganizationUnitId(la.getReceivingHei().getOunitId());

    // Convertimos los LA
    // TODO:esto de poner el uuid si no nos viene está bien??
    final String la_id = Objects.nonNull(laId) ? laId : UUID.randomUUID().toString();
    Integer revision = Objects.nonNull(maxRevision) ? maxRevision : -1;
    final List<LearningAgreementMDTO> learningAgreements = new ArrayList<>();
    final LearningAgreementMDTO changesProposal = componentConverter.convertToLearningAgreementMDTO(
        la.getChangesProposal(), la_id, la.getSendingHei().getHeiId(), la.getReceivingHei().getHeiId());
    if (Objects.nonNull(changesProposal)) {
      changesProposal.setLearningAgreementRevision(++revision);
      learningAgreements.add(changesProposal);
    }

    final LearningAgreementMDTO approvedChanges =
        componentConverter.convertToLearningAgreementMDTO(la.getApprovedChanges(), LearningAgreementStatus.APROVED,
            la_id, la.getSendingHei().getHeiId(), la.getReceivingHei().getHeiId());

    if (Objects.nonNull(approvedChanges)) {
      approvedChanges.setLearningAgreementRevision(++revision);
      learningAgreements.add(approvedChanges);
    }

    final LearningAgreementMDTO firstVersion = componentConverter.convertToLearningAgreementMDTO(la.getFirstVersion(),
        LearningAgreementStatus.FIRST_VERSION, la_id, la.getSendingHei().getHeiId(), la.getReceivingHei().getHeiId());
    if (Objects.nonNull(firstVersion)) {
      firstVersion.setLearningAgreementRevision(++revision);
      learningAgreements.add(firstVersion);
    }
    converted.setLearningAgreement(learningAgreements);

    // Convertimos los contacts
    converted.setSenderContactPerson(
        CommonsConverter.convertContactPersonToContactMDTO(la.getSendingHei().getContactPerson()));

    converted.setReceiverContactPerson(
        CommonsConverter.convertContactPersonToContactMDTO(la.getReceivingHei().getContactPerson()));

    // TODO: Esto se puede sacar de algún lado??
    // converted.setSenderAdmvContactPerson(null);
    // converted.setReceiverAdmvContactPerson(null);

    return converted;
  }

  @Override
  public List<StudentMobilityForStudies> convertToStudentMobilitiesForStudies(final List<MobilityMDTO> mobilities) {
    final List<StudentMobilityForStudies> res = new ArrayList<>();

    for (final MobilityMDTO mdto : mobilities) {
      res.add(convertToStudentMobilityForStudies(mdto));
    }

    return res;
  }

  @Override
  public StudentMobilityForStudies convertToStudentMobilityForStudies(final MobilityMDTO mdto) {
    final StudentMobilityForStudies studentMov = new StudentMobilityForStudies();

    studentMov.setOmobilityId(mdto.getId());

    /** Sending hei **/
    final StudentMobilityForStudies.SendingHei sendingHei = new StudentMobilityForStudies.SendingHei();
    sendingHei.setHeiId(mdto.getSendingInstitutionId());
    if (Objects.nonNull(mdto.getSendingOrganizationUnitId())) {
      sendingHei.setOunitId(mdto.getSendingOrganizationUnitId());
    }
    sendingHei.setIiaId(mdto.getIiaId());
    studentMov.setSendingHei(sendingHei);

    /** Receiving hei **/
    final StudentMobilityForStudies.ReceivingHei receivingHei = new StudentMobilityForStudies.ReceivingHei();
    receivingHei.setHeiId(mdto.getReceivingInstitutionId());
    receivingHei.setIiaId(mdto.getReceivingIiaId());
    if (Objects.nonNull(mdto.getReceivingOrganizationUnitId())) {
      receivingHei.setOunitId(mdto.getReceivingOrganizationUnitId());
    }
    studentMov.setReceivingHei(receivingHei);

    /** Academic terms **/
    if (Objects.nonNull(mdto.getAcademicTerm()) && Objects.nonNull(mdto.getAcademicTerm().getTermNumber())
        && Objects.nonNull(mdto.getAcademicTerm().getTotalTerms())) {
      studentMov.setSendingAcademicTermEwpId(CommonsConverter.convertToAcademicTermId(mdto.getAcademicTerm()));
    } else {
      studentMov.setNonStandardMobilityPeriod(new Empty());
    }

    if (Objects.nonNull(mdto.getAcademicTerm())) {
      studentMov.setReceivingAcademicYearId(
          CommonsConverter.convertToAcademicYearId(mdto.getAcademicTerm().getAcademicYear()));
    }

    final StudentMobilityForStudies.Student student = new StudentMobilityForStudies.Student();

    /** Student - Given names **/
    final StringWithOptionalLang nameString = new StringWithOptionalLang();
    nameString.setValue(mdto.getMobilityParticipantId().getContactDetails().getPerson().getFirstNames());
    student.getGivenNames().add(nameString);

    /** Student - Family names **/
    final StringWithOptionalLang familyString = new StringWithOptionalLang();
    familyString.setValue(mdto.getMobilityParticipantId().getContactDetails().getPerson().getLastName());
    student.getFamilyName().add(familyString);

    /** Student - Global Id **/
    student.setGlobalId(mdto.getMobilityParticipantId().getGlobalId());

    /** Student - Birth Date **/
    student.setBirthDate(CommonsConverter
        .convertToGregorianCalendar(mdto.getMobilityParticipantId().getContactDetails().getPerson().getBirthDate()));

    /** Student - Citizenship **/
    student.setCitizenship(mdto.getMobilityParticipantId().getContactDetails().getPerson().getCountryCode());

    /** Student - Gender **/
    student.setGender(mdto.getMobilityParticipantId().getContactDetails().getPerson().getGender().value());

    /** Student - Email **/
    if (!mdto.getMobilityParticipantId().getContactDetails().getContactDetails().getEmail().isEmpty()) {
      student.setEmail(mdto.getMobilityParticipantId().getContactDetails().getContactDetails().getEmail().get(0));
    }

    /** Student - PhotoUrls **/
    final List<StudentMobilityForStudies.Student.PhotoUrl> photoUrls = new ArrayList<>();

    for (final PhotoUrlMDTO photo : mdto.getMobilityParticipantId().getContactDetails().getContactDetails()
        .getPhotoUrls()) {
      final StudentMobilityForStudies.Student.PhotoUrl photoUrl = new StudentMobilityForStudies.Student.PhotoUrl();
      photoUrl.setDate(CommonsConverter.convertToGregorianCalendar(photo.getDate()));
      photoUrl.setPublic(photo.isPhotoPublic());
      photoUrl.setSizePx(photo.getSize());
      photoUrl.setValue(photo.getUrl());

      photoUrls.add(photoUrl);
    }

    student.getPhotoUrl().addAll(photoUrls);

    /** Student - MailingAdress **/
    student.setMailingAddress(CommonsConverter.convertToFlexibleAddress(
        mdto.getMobilityParticipantId().getContactDetails().getContactDetails().getMailingAddress()));

    /** Student - StreetAddress **/
    student.setStreetAddress(CommonsConverter.convertToFlexibleAddress(
        mdto.getMobilityParticipantId().getContactDetails().getContactDetails().getStreetAddress()));

    /** Student - PhoneNumber **/
    student.getPhoneNumber().addAll(Arrays.asList(CommonsConverter.convertToPhoneNumber(
        mdto.getMobilityParticipantId().getContactDetails().getContactDetails().getPhoneNumber())));

    studentMov.setStudent(student);

    /** Status **/
    if (Objects.nonNull(mdto.getStatusOutgoing())) {
      studentMov.setStatus(
          eu.erasmuswithoutpaper.api.omobilities.endpoints.MobilityStatus.fromValue(mdto.getStatusOutgoing().value()));
    }

    /** Planned Dates **/
    studentMov.setPlannedArrivalDate(CommonsConverter.convertToGregorianCalendar(mdto.getPlannedArrivalDate()));
    studentMov.setPlannedDepartureDate(CommonsConverter.convertToGregorianCalendar(mdto.getPlannedDepartureDate()));

    /** Eqf Levels **/
    if (Objects.nonNull(mdto.getEqfLevelDeparture())) {
      studentMov.setEqfLevelStudiedAtDeparture(mdto.getEqfLevelDeparture().byteValue());
    }

    if (Objects.nonNull(mdto.getEqfLevelNomination())) {
      studentMov.setEqfLevelStudiedAtNomination(mdto.getEqfLevelNomination().byteValue());
    }

    /** Isced Code **/
    studentMov.setNomineeIscedFCode(mdto.getIscedCode().getIscedCode());

    /** Langage Skills **/
    final List<StudentMobilityForStudies.NomineeLanguageSkill> langSkills = new ArrayList<>();

    for (final LanguageSkillMDTO langSkill : mdto.getLanguageSkill()) {
      final StudentMobilityForStudies.NomineeLanguageSkill nomineeLanguageSkill =
          new StudentMobilityForStudies.NomineeLanguageSkill();
      nomineeLanguageSkill.setCefrLevel(langSkill.getCefrLevel());
      nomineeLanguageSkill.setLanguage(langSkill.getLanguage());
      langSkills.add(nomineeLanguageSkill);
    }

    studentMov.getNomineeLanguageSkill().addAll(langSkills);

    return studentMov;
  }

  @Override
  public List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> convertToImobilityStudentMobilitiesForStudies(
      final List<MobilityMDTO> mobilities) {
    final List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> res = new ArrayList<>();

    for (final MobilityMDTO mdto : mobilities) {
      res.add(convertToImobilityStudentMobilityForStudies(mdto));
    }

    return res;
  }

  private eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies convertToImobilityStudentMobilityForStudies(
      final MobilityMDTO mdto) {
    final eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies studentMov =
        new eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies();

    studentMov.setOmobilityId(mdto.getId());

    studentMov.setStatus(
        Objects.nonNull(mdto.getStatusIncomming()) ? NominationStatus.fromValue(mdto.getStatusIncomming().value())
            : null);

    studentMov.setComment(mdto.getComment());


    if (Objects.nonNull(mdto.getActualArrivalDate())) {
      studentMov.setActualArrivalDate(CommonsConverter.convertToGregorianCalendar(mdto.getActualArrivalDate()));
    }

    if (Objects.nonNull(mdto.getActualDepartureDate())) {
      studentMov.setActualDepartureDate(CommonsConverter.convertToGregorianCalendar(mdto.getActualDepartureDate()));
    }

    return studentMov;
  }

  @Override
  public MobilityMDTO convertToOmobilityMDTO(
      final eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies mobilityForStudies) {
    final MobilityMDTO mdto = new MobilityMDTO();

    mdto.setId(mobilityForStudies.getOmobilityId());

    /** Actual arribal & departure dates **/
    mdto.setActualArrivalDate(CommonsConverter.convertToDate(mobilityForStudies.getActualArrivalDate()));
    mdto.setActualDepartureDate(CommonsConverter.convertToDate(mobilityForStudies.getActualDepartureDate()));

    mdto.setStatusIncomming(MobilityStatus.statusFromValue(mobilityForStudies.getStatus().value().toLowerCase()));

    mdto.setComment(mobilityForStudies.getComment());

    return mdto;
  }


  @Override
  public MobilityMDTO convertToOmobilityMDTO(final StudentMobilityForStudies mobilityForStudies) {
    final MobilityMDTO mdto = new MobilityMDTO();
    mdto.setId(mobilityForStudies.getOmobilityId());
    mdto.setRemoteMobilityId(mobilityForStudies.getOmobilityId());
    mdto.setMobilityRevision(0);

    /** Sending hei, iia, ounit **/
    final StudentMobilityForStudies.SendingHei sendingHei = mobilityForStudies.getSendingHei();
    mdto.setSendingInstitutionId(sendingHei.getHeiId());
    mdto.setSendingOrganizationUnitId(sendingHei.getOunitId());
    mdto.setIiaId(sendingHei.getIiaId());

    /** Receiving hei, iia, ounit **/
    final StudentMobilityForStudies.ReceivingHei receivingHei = mobilityForStudies.getReceivingHei();
    mdto.setReceivingInstitutionId(receivingHei.getHeiId());
    mdto.setReceivingIiaId(receivingHei.getIiaId());
    mdto.setReceivingOrganizationUnitId(receivingHei.getOunitId());

    /** Academic terms **/
    if (Objects.nonNull(mobilityForStudies.getSendingAcademicTermEwpId())) {
      mdto.setAcademicTerm(
          CommonsConverter.convertToAcademicTermMDTO(mobilityForStudies.getSendingAcademicTermEwpId()));
    }

    final MobilityParticipantMDTO participant = new MobilityParticipantMDTO();
    final ContactMDTO contact = new ContactMDTO();
    final ContactDetailsMDTO contactDetails = new ContactDetailsMDTO();
    contact.setContactDetails(contactDetails);
    participant.setContactDetails(contact);
    mdto.setMobilityParticipantId(participant);

    /** Participant - Global Id **/
    participant.setGlobalId(mobilityForStudies.getStudent().getGlobalId());

    /** Participant - Birth Date **/
    final PersonMDTO person = new PersonMDTO();
    participant.getContactDetails().setPerson(person);
    person.setBirthDate(CommonsConverter.convertToDate(mobilityForStudies.getStudent().getBirthDate()));

    /** Participant - Name **/
    // cogemos el nombre en ingles o si no hay el primero que venga
    if (CollectionUtils.isNotEmpty(mobilityForStudies.getStudent().getGivenNames())) {
      person.setFirstNames(mobilityForStudies.getStudent().getGivenNames().stream()
          .filter(n -> Objects.nonNull(n.getLang()) && n.getLang().equalsIgnoreCase("EN"))
          .map(StringWithOptionalLang::getValue).findFirst()
          .orElse(mobilityForStudies.getStudent().getGivenNames().get(0).getValue()));
    }

    /** Participant - Family Name **/
    // cogemos el apellido en ingles o si no hay el primero que venga
    if (CollectionUtils.isNotEmpty(mobilityForStudies.getStudent().getFamilyName())) {
      person.setLastName(mobilityForStudies.getStudent().getFamilyName().stream()
          .filter(n -> Objects.nonNull(n.getLang()) && n.getLang().equalsIgnoreCase("EN"))
          .map(StringWithOptionalLang::getValue).findFirst()
          .orElse(mobilityForStudies.getStudent().getFamilyName().get(0).getValue()));
    }

    /** Participant - Citizenship **/
    person.setCountryCode(mobilityForStudies.getStudent().getCitizenship());

    /** Participant - Gender **/
    person.setGender(Gender.getGenderByValue(mobilityForStudies.getStudent().getGender()));

    /** Participant - Email **/
    contactDetails.setEmail(Arrays.asList(mobilityForStudies.getStudent().getEmail()));

    /** Student - PhotoUrls **/
    final List<PhotoUrlMDTO> photoUrls = new ArrayList<>();

    for (final StudentMobilityForStudies.Student.PhotoUrl photo : mobilityForStudies.getStudent().getPhotoUrl()) {
      final PhotoUrlMDTO photoUrl = new PhotoUrlMDTO();
      photoUrl.setDate(CommonsConverter.convertToDate(photo.getDate()));
      photoUrl.setPhotoPublic(photo.isPublic());
      photoUrl.setSize(photo.getSizePx());
      photoUrl.setUrl(photo.getValue());

      photoUrls.add(photoUrl);
    }

    participant.getContactDetails().getContactDetails().setPhotoUrls(photoUrls);

    /** Student - MailingAdress **/
    if (Objects.nonNull(mobilityForStudies.getStudent().getMailingAddress())) {
      contactDetails.setMailingAddress(
          CommonsConverter.convertToFlexibleAddressMDTO(mobilityForStudies.getStudent().getMailingAddress()));
    }

    /** Student - StreetAddress **/
    if (Objects.nonNull(mobilityForStudies.getStudent().getStreetAddress())) {
      contactDetails.setStreetAddress(
          CommonsConverter.convertToFlexibleAddressMDTO(mobilityForStudies.getStudent().getStreetAddress()));
    }

    /** Student - PhoneNumber **/
    if (CollectionUtils.isNotEmpty(mobilityForStudies.getStudent().getPhoneNumber())) {
      contactDetails.setPhoneNumber(
          CommonsConverter.convertToPhoneNumberMDTO(mobilityForStudies.getStudent().getPhoneNumber().get(0)));
    }

    /** Status **/
    if (Objects.nonNull(mobilityForStudies.getStatus())) {
      mdto.setStatusOutgoing(MobilityStatus.valueOf(mobilityForStudies.getStatus().value().toUpperCase()));
    }

    /** Planned Dates **/
    mdto.setPlannedArrivalDate(CommonsConverter.convertToDate(mobilityForStudies.getPlannedArrivalDate()));
    mdto.setPlannedDepartureDate(CommonsConverter.convertToDate(mobilityForStudies.getPlannedDepartureDate()));

    /** Eqf Levels **/
    if (Objects.nonNull(mobilityForStudies.getEqfLevelStudiedAtDeparture())) {
      mdto.setEqfLevelDeparture(mobilityForStudies.getEqfLevelStudiedAtDeparture().longValue());
    }

    if (Objects.nonNull(mobilityForStudies.getEqfLevelStudiedAtNomination())) {
      mdto.setEqfLevelNomination(mobilityForStudies.getEqfLevelStudiedAtNomination().longValue());
    }

    /** Isced Code **/
    final SubjectAreaMDTO iscedCode = new SubjectAreaMDTO();
    iscedCode.setIscedCode(mobilityForStudies.getNomineeIscedFCode());
    mdto.setIscedCode(iscedCode);

    /** Language Skills **/
    final List<LanguageSkillMDTO> langSkills = new ArrayList<>();

    for (final StudentMobilityForStudies.NomineeLanguageSkill langSkill : mobilityForStudies
        .getNomineeLanguageSkill()) {
      final LanguageSkillMDTO nomineeLanguageSkill = new LanguageSkillMDTO();
      nomineeLanguageSkill.setCefrLevel(langSkill.getCefrLevel());
      nomineeLanguageSkill.setLanguage(langSkill.getLanguage());
      langSkills.add(nomineeLanguageSkill);
    }

    mdto.setLanguageSkill(langSkills);

    return mdto;
  }
}
