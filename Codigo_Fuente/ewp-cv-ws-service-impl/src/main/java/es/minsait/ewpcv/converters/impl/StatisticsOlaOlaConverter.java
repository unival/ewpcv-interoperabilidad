package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.converter.api.IStatisticsOlaConverter;
import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasOutgoingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Statitics converter.
 *
 * @author (ilanciano@minsait.com)
 */
@Component
@Slf4j
public class StatisticsOlaOlaConverter implements IStatisticsOlaConverter {

    public List<LasOutgoingStatsResponse.AcademicYearLaStats> convertToStatsResponses(List<StatisticsOlaMDTO> mdtos) {
        final List<LasOutgoingStatsResponse.AcademicYearLaStats> respuesta = new ArrayList<>();

        for (final StatisticsOlaMDTO mdto : mdtos) {
            respuesta.add(convertToStatsResponse(mdto));
        }
        return respuesta;
    }

    @Override
    public LasOutgoingStatsResponse.AcademicYearLaStats convertToStatsResponse(StatisticsOlaMDTO mdto) {
        LasOutgoingStatsResponse.AcademicYearLaStats responseItem = new LasOutgoingStatsResponse.AcademicYearLaStats();

        responseItem.setReceivingAcademicYearId(mdto.getAcademicYearStart()+"/"+mdto.getAcademicYearEnd());
        responseItem.setLaOutgoingTotal(BigInteger.valueOf(mdto.getTotalOutgoingNumber()));
        responseItem.setLaOutgoingModifiedAfterApproval(BigInteger.valueOf(mdto.getApprovalModif()));
        responseItem.setLaOutgoingNotModifiedAfterApproval(BigInteger.valueOf(mdto.getApprovalUnModif()));
        responseItem.setLaOutgoingLatestVersionApproved(BigInteger.valueOf(mdto.getLastAppr()));
        responseItem.setLaOutgoingLatestVersionRejected(BigInteger.valueOf(mdto.getLastRej()));
        responseItem.setLaOutgoingLatestVersionAwaiting(BigInteger.valueOf(mdto.getLastWait()));

        return responseItem;
    }
}
