package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converters.impl.OmobilityLasConverter;
import es.minsait.ewpcv.repository.dao.ILearningAgreementDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.*;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.service.api.ILearningAgreementService;
import es.minsait.ewpcv.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Learning agreement service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class LearningAgreementServiceImpl implements ILearningAgreementService {

  public static final String HA_CAMBIADO_LA_PROPIEDAD = "Ha cambiado la propiedad {}";
  @Autowired
  private OmobilityLasConverter lasConverter;

  @Autowired
  private IIiasServiceImpl iiasService;

  @Autowired
  private ILearningAgreementDAO laDao;

  @Autowired
  public LearningAgreementServiceImpl(final IIiasServiceImpl iiasService, final ILearningAgreementDAO laDao,
                                        final OmobilityLasConverter lasConverter) {
    this.iiasService = iiasService;
    this.laDao = laDao;
    this.lasConverter = lasConverter;
  }

  /**
   * Compara dos LearningAgreementMDTO y no actualiza la copia local
   * 
   * @param remoto LearningAgreementMDTO remoto
   * @param local LearningAgreementMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLocalConRemoto(final LearningAgreementMDTO remoto, final LearningAgreementMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    if (!Arrays.equals(remoto.getPdf(), local.getPdf())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "pdf");
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getStudentSign())) {
      log.trace("Comenzamos la comparación de StudentSign");
      final boolean compareRes = compararSignature(remoto.getStudentSign(), local.getStudentSign());
      hayCambios = hayCambios || compareRes;
      log.trace("Fin de la comparación de StudentSign");
    }

    if (Objects.nonNull(remoto.getSenderCoordinatorSign())) {
      log.trace("Comenzamos la comparación de SenderCoordinatorSign");
      final boolean compareRes = compararSignature(remoto.getSenderCoordinatorSign(), local.getSenderCoordinatorSign());
      hayCambios = hayCambios || compareRes;
      log.trace("Fin de la comparación de SenderCoordinatorSign");
    }

    if (Objects.nonNull(remoto.getReciverCoordinatorSign())) {
      log.trace("Comenzamos la comparación de ReciverCoordinatorSign");
      hayCambios =
              hayCambios || compararSignature(remoto.getReciverCoordinatorSign(), local.getReciverCoordinatorSign());
      log.trace("Fin de la comparación de ReciverCoordinatorSign");
    }

    if (!Objects.equals(remoto.getStatus(), local.getStatus())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "status");
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getStudiedLaComponents())) {
      log.trace("Comenzamos la comparación de StudiedLaComponents");
      final boolean aux = compararComponentList(remoto.getStudiedLaComponents(), local.getStudiedLaComponents());
      log.trace("Fin de la comparación de StudiedLaComponents");

      if (aux) {
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getRecognizedLaComponents())) {
      log.trace("Comenzamos la comparación de RecognizedLaComponents");
      final boolean aux = compararComponentList(remoto.getRecognizedLaComponents(), local.getRecognizedLaComponents());
      log.trace("Fin de la comparación de RecognizedLaComponents");

      if (aux) {
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getVirtualLaComponents())) {
      log.trace("Comenzamos la comparación de VirtualLaComponents");
      final boolean aux = compararComponentList(remoto.getVirtualLaComponents(), local.getVirtualLaComponents());
      log.trace("Fin de la comparación de VirtualLaComponents");

      if (aux) {
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getBlendedLaComponents())) {
      log.trace("Comenzamos la comparación de BlendedLaComponents");

      final boolean aux = compararComponentList(remoto.getBlendedLaComponents(), local.getBlendedLaComponents());

      log.trace("Fin de la comparación de BlendedLaComponents");

      if (aux) {
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getDoctoralLaComponents())) {
      log.trace("Comenzamos la comparación de DoctoralLaComponents");

      final boolean aux = compararComponentList(remoto.getDoctoralLaComponents(), local.getDoctoralLaComponents());

      log.trace("Fin de la comparación de DoctoralLaComponents");

      if (aux) {
        hayCambios = Boolean.TRUE;
      }
    }

    return hayCambios;
  }

  /**
   * Compara dos listados LaComponentMDTO y no actualiza la copia local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararComponentList(final List<LaComponentMDTO> remoto, final List<LaComponentMDTO> local) {

    log.trace("Comienza la comparación de LaComponents");

    final List<LaComponentMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLaComponentWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<LaComponentMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLaComponentWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de LaComponents");
    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LaComponentMDTO esta en una lista de LaComponentMDTO
   *
   * @param item LaComponentMDTO
   * @param lista listado de LaComponentMDTO
   * @param esItemLocal indica si el item es un LaComponentMDTO local.
   * @return true si el LaComponentMDTO esta presente en la lista
   */
  private boolean compararLaComponentWithList(final LaComponentMDTO item, final List<LaComponentMDTO> lista,
      final Boolean esItemLocal) {
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararLaComponent(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararLaComponent(item, i));
    }
  }

  /**
   * Compara dos LaComponentMDTO, no actualiza la copia local
   * 
   * @param remoto LaComponentMDTO remoto
   * @param local LaComponentMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLaComponent(final LaComponentMDTO remoto, final LaComponentMDTO local) {

    if (!Objects.equals(remoto.getLosCode(), local.getLosCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.LosCode");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getStatus(), local.getStatus())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.Status");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getTitle(), local.getTitle())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.Title");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReasonCode(), local.getReasonCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.ReasonCode");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReasonText(), local.getReasonText())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.ReasonText");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLaComponentType(), local.getLaComponentType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.ComponentType");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getRecognitionConditions(), local.getRecognitionConditions())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.RecognitionConditions");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getShortDescription(), local.getShortDescription())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.ShortDescription");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getAcademicTermDisplayName())
        && compararAcademicTerm(remoto.getAcademicTermDisplayName(), local.getAcademicTermDisplayName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.AcademicTermDisplayName");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getLoiId()) && compararLearningOportunityInstance(remoto.getLoiId(), local.getLoiId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.LoiId");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getLosId())
        && compararLearningOportunitySpecification(remoto.getLosId(), local.getLosId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LaComponent.LosId");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getCredits())) {
      return compararCreditsList(remoto.getCredits(), local.getCredits());
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos LearningOpportunitySpecificationMDTO, no actualiza la copia local
   * 
   * @param remoto LearningOpportunitySpecificationMDTO remoto
   * @param local LearningOpportunitySpecificationMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLearningOportunitySpecification(final LearningOpportunitySpecificationMDTO remoto,
      final LearningOpportunitySpecificationMDTO local) {
    log.trace("Comienza la comparación de LearningOportunitySpecification");

    if (!Objects.equals(remoto.getInstitutionId(), local.getInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "InstitutionId");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getEqfLevel(), local.getEqfLevel())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EqfLevel");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitId");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLosCode(), local.getLosCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LosCode");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getType(), local.getType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Type");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getIscedf(), local.getIscedf())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Iscedf");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSubjectArea(), local.getSubjectArea())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SubjectArea");
      return Boolean.TRUE;
    }

    if (!remoto.isTopLevelParent() == local.isTopLevelParent()) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TopLevelParent");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getDescription())) {
      log.trace("Comienza la comparacion de Descripcion");

      final boolean res =
          iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.FALSE);

      if (res) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getName())) {
      log.trace("Comienza la comparacion de Description");

      final boolean res = iiasService.compararLanguageItemsList(remoto.getName(), local.getName(), Boolean.FALSE);

      if (res) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getUrl())) {
      log.trace("Comienza la comparacion de Url");

      final boolean res = iiasService.compararLanguageItemsList(remoto.getUrl(), local.getUrl(), Boolean.FALSE);

      if (res) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getLearningOpportunitySpecifications())) {
      final boolean res = compararLearningOportunitySpecificationsList(remoto.getLearningOpportunitySpecifications(),
          local.getLearningOpportunitySpecifications());

      if (res) {
        return Boolean.TRUE;
      }
    }

    log.trace("Fin de la comparación de LearningOportunitySpecification");
    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de LearningOpportunitySpecificationMDTO sin actualizar la copia local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los dos listados
   */
  private boolean compararLearningOportunitySpecificationsList(final List<LearningOpportunitySpecificationMDTO> remoto,
      final List<LearningOpportunitySpecificationMDTO> local) {

    log.trace("Comienza la comparación de LearningOpportunitySpecificationList");

    final List<LearningOpportunitySpecificationMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararLearningOpportunitySpecificationWithList(i, remoto, Boolean.FALSE))
              .collect(Collectors.toList()));
    }

    final List<LearningOpportunitySpecificationMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLearningOpportunitySpecificationWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de LearningOpportunitySpecificationList");
    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LearningOpportunitySpecificationMDTO esta en una lista de LearningOpportunitySpecificationMDTO
   *
   * @param item LearningOpportunitySpecificationMDTO
   * @param lista listado de LearningOpportunitySpecificationMDTO
   * @param esItemLocal indica si el item es un LearningOpportunitySpecificationMDTO local.
   * @return true si el LearningOpportunitySpecificationMDTO esta presente en la lista
   */
  private boolean compararLearningOpportunitySpecificationWithList(final LearningOpportunitySpecificationMDTO item,
      final List<LearningOpportunitySpecificationMDTO> lista, final Boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararLearningOportunitySpecification(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararLearningOportunitySpecification(item, i));
    }
  }

  /**
   * Compara dos listados de LearningOpportunityInstanceMDTO sin actualizar la copia local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los dos listados
   */
  private boolean compararLearningOportunityInstanceList(final List<LearningOpportunityInstanceMDTO> remoto,
      final List<LearningOpportunityInstanceMDTO> local) {
    log.trace("Comienza la comparación de LearningOportunityInstanceList");

    final List<LearningOpportunityInstanceMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLearningOpportunityInstanceWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<LearningOpportunityInstanceMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLearningOpportunityInstanceWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de LearningOportunityInstanceList");
    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LearningOpportunityInstanceMDTO esta en una lista de LearningOpportunityInstanceMDTO
   *
   * @param item LearningOpportunityInstanceMDTO
   * @param lista listado de LearningOpportunityInstanceMDTO
   * @param esItemLocal indica si el item es un LearningOpportunityInstanceMDTO local.
   * @return true si el LearningOpportunityInstanceMDTO esta presente en la lista
   */
  private boolean compararLearningOpportunityInstanceWithList(final LearningOpportunityInstanceMDTO item,
      final List<LearningOpportunityInstanceMDTO> lista, final Boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararLearningOportunityInstance(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararLearningOportunityInstance(item, i));
    }
  }

  /**
   * Compara dos LearningOpportunityInstanceMDTO, no actualiza la copia local
   * 
   * @param remoto LearningOpportunityInstanceMDTO remoto
   * @param local LearningOpportunityInstanceMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLearningOportunityInstance(final LearningOpportunityInstanceMDTO remoto,
      final LearningOpportunityInstanceMDTO local) {
    log.trace("Comienza la comparación de LearningOportunityInstance");

    if (!Utils.nullSafeCompareBigDecimal(remoto.getEngagementHours(), local.getEngagementHours())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EngagemetHours");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLanguageOfInstruction(), local.getLanguageOfInstruction())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LanguageOfInstruction");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getGradingScheme())) {
      final boolean res = compararGradingSchemes(remoto.getGradingScheme(), local.getGradingScheme());

      if (res) {
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getResultDistribution())) {
      final boolean res = compararResultDistribution(remoto.getResultDistribution(), local.getResultDistribution());

      if (res) {
        return Boolean.TRUE;
      }
    }

    //TODO: ES POSIBLE QUE FALTE ALGUNA COMPARACIÓN DE LAS SIGUIENTES
    /*
    String languageOfInstruction;
    BigDecimal engagementHours;
    List<LevelMDTO> levels;
    LoiGroupingMDTO loiGrouping;
    Date startDate;
    Date endDate;
    Long percentageLower;
    Long percentageEqual;
    Long percentageHigher;
    String resultLabel;
    LoiStatus status;
    byte[] extension;
    List<AttachmentMDTO> attachments;
    DiplomaMDTO diploma;
    LearningOpportunitySpecificationMDTO learningOpportunitySpecification;
     */

    log.trace("Fin de la comparación de LearningOportunityInstance");
    return Boolean.FALSE;
  }

  /**
   * Compara dos ResultDistributionMDTO sin actualizar la copia local
   * 
   * @param remoto ResultDistributionMDTO remoto
   * @param local ResultDistributionMDTO local
   * @return true si hay cambios entre los objetos
   */
  public boolean compararResultDistribution(final ResultDistributionMDTO remoto, final ResultDistributionMDTO local) {
    log.trace("Comienza la comparación de ResultDistribution");

    if (Objects.nonNull(remoto.getDescription())) {
      final List<LanguageItemMDTO> localDescripcion =
          CollectionUtils.isNotEmpty(local.getDescription()) ? local.getDescription() : new ArrayList<>();
      final boolean res =
          iiasService.compararLanguageItemsList(remoto.getDescription(), localDescripcion, Boolean.TRUE);
      if (res) {
        local.setDescription(localDescripcion);
        return Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getResultDistributionCategory())) {
      final boolean res = compararResultDistributionCategoryList(remoto.getResultDistributionCategory(),
          local.getResultDistributionCategory());

      if (res) {
        return Boolean.TRUE;
      }
    }

    log.trace("Fin de la comparación de ResultDistribution");
    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de ResultDistributionCategoryMDTO actualizando la copia local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararResultDistributionCategoryList(final List<ResultDistributionCategoryMDTO> remoto,
      final List<ResultDistributionCategoryMDTO> local) {
    log.trace("Comienza la comparación de ResultDistributionCategoryList");

    final List<ResultDistributionCategoryMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararResultDistributionCategoryWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<ResultDistributionCategoryMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararResultDistributionCategoryWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de ResultDistributionCategoryList");
    log.trace("Se van a borrar los siguientes ResultDistributionCategoryMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes ResultDistributionCategoryMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un ResultDistributionCategoryMDTO esta en una lista de ResultDistributionCategoryMDTO
   *
   * @param item ResultDistributionCategoryMDTO
   * @param lista listado de ResultDistributionCategoryMDTO
   * @param esItemLocal indica si el item es un ResultDistributionCategoryMDTO local.
   * @return true si el ResultDistributionCategoryMDTO esta presente en la lista
   */
  private boolean compararResultDistributionCategoryWithList(final ResultDistributionCategoryMDTO item,
      final List<ResultDistributionCategoryMDTO> lista, final Boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararResultDistributionCategory(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararResultDistributionCategory(item, i));
    }
  }

  /**
   * Compara dos ResultDistributionCategoryMDTO sin actualizar la copia local
   *
   * @param remoto ResultDistributionCategoryMDTO remoto
   * @param local ResultDistributionCategoryMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararResultDistributionCategory(final ResultDistributionCategoryMDTO remoto,
      final ResultDistributionCategoryMDTO local) {
    log.trace("Comienza la comparación de ResultDistributionCategoryMDTO");

    if (!Objects.equals(remoto.getCount(), local.getCount())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Count");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLabel(), local.getLabel())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Label");
      return Boolean.TRUE;
    }

    log.trace("Fin de la comparación de ResultDistributionCategoryMDTO");
    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de CreditMDTO actualizando la copia local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararCreditsList(final List<CreditMDTO> remoto, final List<CreditMDTO> local) {
    log.trace("Comienza la comparación de CreditsList");

    final List<CreditMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararCreditWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<CreditMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararCreditWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de CreditsList");
    log.trace("Se van a borrar los siguientes CreditMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes CreditMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un CreditMDTO esta en una lista de CreditMDTO
   *
   * @param item CreditMDTO
   * @param lista listado de CreditMDTO
   * @param esItemLocal indica si el item es un CreditMDTO local.
   * @return true si el CreditMDTO esta presente en la lista
   */
  private boolean compararCreditWithList(final CreditMDTO item, final List<CreditMDTO> lista,
      final Boolean esItemLocal) {
    if (Objects.isNull(lista)) {
      return false;
    }
    if (esItemLocal) {
      return lista.stream().anyMatch(i -> !compararCredit(i, item));
    } else {
      return lista.stream().anyMatch(i -> !compararCredit(item, i));
    }
  }

  /**
   * Compara dos CreditMDTO sin actualizar la copia local
   * 
   * @param remoto CreditMDTO remoto
   * @param local CreditMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararCredit(final CreditMDTO remoto, final CreditMDTO local) {

    log.trace("Comenzando comparación de CreditMDTO");

    if (!Objects.equals(remoto.getLevel(), local.getLevel())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Level");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getScheme(), local.getScheme())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Scheme");
      return Boolean.TRUE;
    }

    if (!Utils.nullSafeCompareBigDecimal(remoto.getValue(), local.getValue())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Value");
      return Boolean.TRUE;
    }

    log.trace("Fin de la comparación de CreditMDTO");

    return Boolean.FALSE;
  }

  /**
   * Compara dos GradingSchemeMDTO sin actualizar la copia local
   * 
   * @param remoto GradingSchemeMDTO remoto
   * @param local GradingSchemeMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararGradingSchemes(final GradingSchemeMDTO remoto, final GradingSchemeMDTO local) {
    log.trace("Comienza la comparacion de GradingSchemes");

    if (iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.FALSE)) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Description");
      return Boolean.TRUE;
    }

    if (iiasService.compararLanguageItemsList(remoto.getLabel(), local.getLabel(), Boolean.FALSE)) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Label");
      return Boolean.TRUE;
    }

    log.trace("Fin de la comparacion de GradingSchemes");
    return Boolean.FALSE;
  }

  /**
   * Compara dos AcademicTermMDTO, no actualiza la copia local
   * 
   * @param remoto AcademicTermMDTO remoto
   * @param local AcademicTermMDTO local
   * @return true si hay diferencias entre los objetos
   */
  @Override
  public boolean compararAcademicTerm(final AcademicTermMDTO remoto, final AcademicTermMDTO local) {
    log.trace("Comienza la comparación de AcedemicTerm");

    if (!Objects.equals(remoto.getInstitutionId(), local.getInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "InstitutionId");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitId");
      return Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getStartDate(), local.getStartDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StartDate");
      return Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getEndDate(), local.getEndDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EndDate");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getTermNumber(), local.getTermNumber())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TermNumber");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getTotalTerms(), local.getTotalTerms())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TotalTerms");
      return Boolean.TRUE;
    }

    log.trace("Comienza la comparación de AcademicYear");

    final AcademicYearMDTO yearRemoto =
            Objects.nonNull(remoto.getAcademicYear()) ? remoto.getAcademicYear() : new AcademicYearMDTO();
    final AcademicYearMDTO yearLocal =
            Objects.nonNull(local.getAcademicYear()) ? local.getAcademicYear() : new AcademicYearMDTO();

    if (!Objects.equals(yearRemoto.getEndYear(), yearLocal.getEndYear())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EndYear");
      return Boolean.TRUE;
    }

    if (!Objects.equals(yearRemoto.getStartYear(), yearLocal.getStartYear())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StartYear");
      return Boolean.TRUE;
    }

    log.trace("Fin de la comparación de AcademicYear");

    if (Objects.nonNull(remoto.getDispName())) {
      return iiasService.compararLanguageItemsList(remoto.getDispName(), local.getDispName(), Boolean.FALSE);
    }

    log.trace("Fin de la comparación de AcedemicTerm");

    return Boolean.FALSE;
  }

  /**
   * Compara dos SignatureMDTO y no actualiza la copia local
   * 
   * @param remoto SignatureMDTO remoto
   * @param local SignatureMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararSignature(final SignatureMDTO remoto, final SignatureMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    if (!Arrays.equals(remoto.getSignature(), local.getSignature())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "signature");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSignerApp(), local.getSignerApp())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "signerApp");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSignerEmail(), local.getSignerEmail())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "signerEmail");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSignerName(), local.getSignerName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "signerName");
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSignerPosition(), local.getSignerPosition())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "signerPosition");
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYHorasYSegundos(remoto.getTimestamp(), local.getTimestamp())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "timestamp");
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  @Override
  public LearningAgreementMDTO buscarLAPorChangesId(final String mobilityId, final String changesId) {

    return laDao.findLearningAgreementByChangesId(mobilityId, changesId);
  }

  @Override
  public void eliminarLA(final String changedElementIds) {}


  /**
   * Compara dos LearningAgreementMDTO, no actualiza la copia local y pero si la fecha de modificacion
   * 
   * @param remoto LearningAgreementMDTO remoto
   * @param local LearningAgreementMDTO local
   * @return true si hay diferencias entre los objetos
   */
  @Override
  public boolean compararLearningAgreements(final LearningAgreementMDTO remoto, final LearningAgreementMDTO local) {

    // si hay cambios actaualizamos la fecha de modificación del LA
    if (compararLocalConRemoto(remoto, local)) {
      local.setModifiedDate(new Date());
      return Boolean.TRUE;
    }

    return Boolean.FALSE;
  }
}
