package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IInstitutionConverter;
import es.minsait.ewpcv.repository.dao.IContactDAO;
import es.minsait.ewpcv.repository.dao.IFactSheetDAO;
import es.minsait.ewpcv.repository.dao.IInstitutionDAO;
import es.minsait.ewpcv.repository.dao.IInstitutionIdentifiersDAO;
import es.minsait.ewpcv.repository.model.dictionary.InstitutionIdentifiersMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import es.minsait.ewpcv.service.api.IInstitutionService;
import eu.erasmuswithoutpaper.api.institutions.InstitutionsResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The type Institution service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Slf4j
@Transactional(readOnly = true)
public class InstitutionServiceImpl implements IInstitutionService {

  public static final String MOBILITY_FACTSHEETS_URL = "Mobility Factsheets URL";
  public static final String HA_CAMBIADO_LA_PROPIEDAD = "Ha cambiado la propiedad {}";
  @Autowired
  private IInstitutionDAO institutionDAO;
  @Autowired
  private IInstitutionIdentifiersDAO institutionIdentifiersDAO;
  @Autowired
  private IContactDAO contactDAO;
  @Autowired
  private IInstitutionConverter institutionConverter;
  @Autowired
  private IIiasServiceImpl iiasService;

  @Override
  @Transactional(readOnly = false)
  public void actualizarInstitutions(final List<InstitutionsResponse.Hei> heis) {
    for (final InstitutionsResponse.Hei hei : heis) {
      if (Objects.nonNull(hei)) {
        actualizarInstitution(hei);
      }
    }
  }

  private void actualizarInstitution(final InstitutionsResponse.Hei hei) {
    // obtenemos la institution de la bbdd
    final InstitutionMDTO local = institutionDAO.getInstitutionById(hei.getHeiId());
    final InstitutionMDTO remoto = institutionConverter.convertToInstitutionMDTO(hei);

    // obtenemos los contactos de la institucion
    final List<ContactMDTO> localContacts = contactDAO.findInstitutionsContacts(Arrays.asList(hei.getHeiId()));
    if (CollectionUtils.isNotEmpty(localContacts)) {
      //eliminamos el contacto principal de la lista ya que se mapea en la institucion
      localContacts.removeIf(c -> Objects.nonNull(local) && Objects.nonNull(local.getPrimaryContactDetails()) && Objects.nonNull(c.getContactDetails()) &&
              local.getPrimaryContactDetails().getId().equalsIgnoreCase(c.getContactDetails().getId()));
    }
    final List<ContactMDTO> remotoContacts =
            institutionConverter.convertToContactMDTOList(hei.getContact(), hei.getHeiId());

    /** Actualizamos la institution **/
    log.trace("Comienza el proceso de comparacion de Institutions");
    if (Objects.isNull(local)) {
      // no tenemos la institucion en bbdd, guardamos la que nos llega
      institutionDAO.saveOrUpdate(remoto);
    } else {
      if (hayCambios(remoto, local)) {
        // actualizamo la informacion de la institucion
        institutionDAO.saveOrUpdate(local);
      }
    }
    log.trace("Fin del proceso de comparacion de Institutions");

    /** Actualizamos los Contacts de la institution **/
    log.trace("Comienza el proceso de comparacion de Contacts para las Institutions");
    if (CollectionUtils.isEmpty(localContacts)) {
      // no tenemos ningun contacto de la institucion, guardamos los que nos
      // llegan
      remotoContacts.forEach(cont -> contactDAO.saveOrUpdate(cont));
    } else {
      final List<ContactMDTO> contactosBorrar = new ArrayList<>();
      final boolean hayCambios = hayCambiosContacts(remotoContacts, localContacts, contactosBorrar);

      // borramos los contactos relacionados con la institucion que se hayan
      // borrado en remoto
      contactosBorrar.forEach(c -> contactDAO.delete(c));

      if (hayCambios) {
        // actualizamo la informacion de la institucion
        localContacts.forEach(cont -> contactDAO.saveOrUpdate(cont));
      }
    }

    log.trace("Finaliza el proceso de comparacion de Contacts para las Institutions");
  }

  /**
   * Compara dos InstitutionMDTO y actualiza la copia local
   *
   * @param remoto InstitutionMDTO remoto
   * @param local InstitutionMDTO local
   * @return true si hay cambios entre los objetos
   */
  private boolean hayCambios(final InstitutionMDTO remoto, final InstitutionMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    if (Objects.nonNull(remoto.getAbbreviation())) {
      if (!remoto.getAbbreviation().equals(local.getAbbreviation())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Abbreviation");
        local.setAbbreviation(remoto.getAbbreviation());
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getLogoUrl())) {
      if (!remoto.getLogoUrl().equals(local.getLogoUrl())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LogoUrl");
        local.setLogoUrl(remoto.getLogoUrl());
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.isNull(local.getPrimaryContactDetails())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PrimaryContactDetails");
      local.setPrimaryContactDetails(remoto.getPrimaryContactDetails());
      hayCambios = Boolean.TRUE;
    } else if (Objects.nonNull(remoto.getPrimaryContactDetails())) {
      final ContactDetailsMDTO localPrimaContactDetails =
              Objects.nonNull(local.getPrimaryContactDetails()) ? local.getPrimaryContactDetails()
                      : new ContactDetailsMDTO();
      if (iiasService.compararContactDetails(remoto.getPrimaryContactDetails(), localPrimaContactDetails,
              Boolean.TRUE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PrimaryContactDetails");
        local.setPrimaryContactDetails(localPrimaContactDetails);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.isNull(local.getName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Name");
      local.setName(remoto.getName());
      hayCambios = Boolean.TRUE;
    } else if (Objects.nonNull(remoto.getName())) {
      final List<LanguageItemMDTO> localName =
              CollectionUtils.isNotEmpty(local.getName()) ? local.getName() : new ArrayList<>();
      if (iiasService.compararLanguageItemsList(remoto.getName(), localName, Boolean.TRUE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Name");
        local.setName(localName);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.isNull(local.getPrimaryContactDetails().getUrl())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "InstitutionsURL");
      local.getPrimaryContactDetails().setUrl(remoto.getPrimaryContactDetails().getUrl());
      hayCambios = Boolean.TRUE;
    } else if (Objects.nonNull(remoto.getPrimaryContactDetails())
            && Objects.nonNull(remoto.getPrimaryContactDetails().getUrl())) {
      final List<LanguageItemMDTO> localUrl = CollectionUtils.isNotEmpty(local.getPrimaryContactDetails().getUrl())
              ? local.getPrimaryContactDetails().getUrl()
              : new ArrayList<>();
      if (iiasService.compararLanguageItemsList(remoto.getPrimaryContactDetails().getUrl(), localUrl, Boolean.TRUE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "InstitutionsURL");
        local.getPrimaryContactDetails().setUrl(localUrl);
        hayCambios = Boolean.TRUE;
      }
    }

    List<LanguageItemMDTO> localUrls = local.getFactsheetUrls();
    List<LanguageItemMDTO> remoteUrls = remoto.getFactsheetUrls();

    if (localUrls == null || !iiasService.compararLanguageItemsList(remoteUrls, localUrls, true)) {
      local.setFactsheetUrls(remoteUrls != null ? new ArrayList<>(remoteUrls) : new ArrayList<>());
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, MOBILITY_FACTSHEETS_URL);
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  private boolean hayCambiosContacts(final List<ContactMDTO> remotoContacts, final List<ContactMDTO> localContacts,
                                     final List<ContactMDTO> elementosABorrar) {
    return iiasService.compararContactsList(remotoContacts, localContacts, elementosABorrar);
  }

  @Override
  public List<InstitutionsResponse.Hei> obtenerInstitutionsInformation(final List<String> heisIds) {
    final List<String> heis2LookFor = new ArrayList<>(heisIds);
    final List<InstitutionMDTO> institutions = institutionDAO.findAllByHeiId(heis2LookFor);
    final List<InstitutionIdentifiersMDTO> institutionsIds = institutionIdentifiersDAO.findAllBySCHAC(heis2LookFor);
    final List<ContactMDTO> contacts = contactDAO.findInstitutionsContacts(heis2LookFor);
    if (CollectionUtils.isNotEmpty(contacts)) {
      //eliminamos el contacto principal de la lista ya que se mapea en la institucion
      contacts.removeIf(c -> CollectionUtils.isNotEmpty(institutions) && Objects.nonNull(c.getContactDetails()) &&
              institutions.stream().anyMatch(
                      i -> Objects.nonNull(i.getPrimaryContactDetails()) && i.getPrimaryContactDetails().getId().equalsIgnoreCase(c.getContactDetails().getId()))
      );
    }

    final Map<String, List<ContactMDTO>> contactsMap = new HashMap<>();
    for (final ContactMDTO c : contacts) {
      List<ContactMDTO> contactList = contactsMap.get(c.getInstitutionId());

      if (Objects.isNull(contactList)) {
        contactList = new ArrayList<>();
      }

      contactList.add(c);
      contactsMap.put(c.getInstitutionId(), contactList);
    }

    List<InstitutionsResponse.Hei> heis = new ArrayList<>();

    if (institutions != null) {
      heis = institutionConverter.convertoToHeisList(institutions, institutionsIds, contactsMap);
      heis.stream().filter(hei -> !heisIds.contains(hei.getHeiId())).collect(Collectors.toList())
              .forEach(hei -> hei.setHeiId(hei.getHeiId().toLowerCase()));
    }

    return heis;
  }


  @Override
  public boolean isKnownHei(final String heiId) {
    return institutionDAO.isKnownHei(heiId);
  }

  @Override
  @Transactional(readOnly = false)
  public void actualizarInstitutionsOunits(final String heiId, final List<OrganizationUnitMDTO> ounits) {
    final InstitutionMDTO institution = institutionDAO.getInstitutionById(heiId);
    institution.setOrganizationUnits(ounits);
    institutionDAO.saveOrUpdate(institution);
  }

  @Override
  @Transactional(readOnly = false)
  public void addOunitToInstitution(OrganizationUnitMDTO ounit, String heiId){
    final InstitutionMDTO institution = institutionDAO.getInstitutionById(heiId);
    institution.getOrganizationUnits().add(ounit);
    institutionDAO.saveOrUpdate(institution);
  }

}
