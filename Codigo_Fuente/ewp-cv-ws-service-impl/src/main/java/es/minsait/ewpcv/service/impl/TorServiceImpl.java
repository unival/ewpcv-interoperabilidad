package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.ITorConverter;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.error.EwpNotFoundException;
import es.minsait.ewpcv.repository.dao.IOmobilityDAO;
import es.minsait.ewpcv.repository.dao.ITorDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionCategoryMDTO;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionMDTO;
import es.minsait.ewpcv.service.api.IIiasService;
import es.minsait.ewpcv.service.api.ILearningAgreementService;
import es.minsait.ewpcv.service.api.ITorService;
import es.minsait.ewpcv.utils.Utils;
import eu.erasmuswithoutpaper.api.imobilities.tors.endpoints.ImobilityTorsGetResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The type Tor service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional
@Slf4j
public class TorServiceImpl implements ITorService {

  private final ITorDAO torDAO;
  private final ITorConverter converter;
  private final IOmobilityDAO mobilityDao;
  private final IIiasService iiasService;
  private final ILearningAgreementService learningAgreementService;


  @Autowired
  public TorServiceImpl(final ITorDAO torDAO, final ITorConverter converter, final IOmobilityDAO mobilityDao,
      final IIiasService iiasService, final ILearningAgreementService learningAgreementService) {
    this.torDAO = torDAO;
    this.converter = converter;
    this.mobilityDao = mobilityDao;
    this.iiasService = iiasService;
    this.learningAgreementService = learningAgreementService;
  }

  @Override
  public List<String> obtenerMobilitiesIds(final String receivingHeiId, final List<String> sendingHeisIds,
      final String modifiedSince, final List<String> receivingHeiIdToFilter) {
    return torDAO.obtenerMobilitiesWithTorIds(receivingHeiId, sendingHeisIds, modifiedSince, receivingHeiIdToFilter);
  }

  @Override
  public MobilityMDTO obtenerMobilityWithTor(final String mobilityId) {
    return torDAO.obtenerMobilityWithTor(mobilityId);
  }

  @Override
  @Transactional(readOnly = false)
  public void guardarMobilityTorCopiaRemota(final ImobilityTorsGetResponse.Tor remoto, final String mobilityId, final String sendingHeiId)
          throws EwpNotFoundException {
    final MobilityMDTO mobility = mobilityDao.findLastRevisionByIdAndSendingInstitution(mobilityId, sendingHeiId);

    if (Objects.nonNull(mobility)) {

      // Obtnemos la ultima revision de LA de la mobilidad y asignamos a ese LA el TOR
      final LearningAgreementMDTO lastLaRevisionWithChanges = mobility.getLearningAgreement().stream()
              .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision)).orElse(null);

      if (Objects.nonNull(lastLaRevisionWithChanges)) {
        final TranscriptOfRecordMDTO torMdto = obtenerTor(converter.convertToTorMDTOList(Arrays.asList(remoto)).get(0));
        lastLaRevisionWithChanges.setTranscriptOfRecord(torMdto);
      }

      mobilityDao.save(mobility);
    } else {
      throw new EwpNotFoundException(
              "Se intenta asignar un TOR a una mobilidad con id " + mobilityId + " que no existe en el sistema");
    }
  }

  @Override
  public List<ImobilityTorsGetResponse.Tor> findTorByOmobilityIdAndReceivingInstitutionId(final String receivingHei,
      final List<String> omobilityId, final List<String> heisCoveredByClient) throws EwpConverterException {
    final List<ImobilityTorsGetResponse.Tor> tors = new ArrayList<>();

    //dividimos la lista de heis en bloques de 1000 e invocamos al DAO para cada bloque
    final int MAX_LIST_SIZE = 1000;
    Collection<List<String>> partitionedList = IntStream.range(0, heisCoveredByClient.size())
            .boxed()
            .collect(Collectors.groupingBy(partition -> (partition / MAX_LIST_SIZE),
                    Collectors.mapping(heisCoveredByClient::get, Collectors.toList())))
            .values();

    final List<MobilityMDTO> torsMobilites = new ArrayList<>();
    for(List<String> heisIds : partitionedList){
      torsMobilites.addAll( torDAO.findTorByOmobilityIdAndReceivingInstitutionId(receivingHei, omobilityId, heisIds));
    }

    // los convertimos en ImobilityTorsGetResponse.Tor
    if (!torsMobilites.isEmpty()) {
      tors.addAll(converter.convertToImobilityTor(torsMobilites));
    }

    return tors;
  }

  @Override
  @Transactional(readOnly = false)
  public boolean actualizarTor(final ImobilityTorsGetResponse.Tor tor, final String sendingHeiId) {
    boolean seActualiza = Boolean.FALSE;

    // obtenemos el tor de la base de datos y lo comparamos con lo que nos llega
    final MobilityMDTO mobility = mobilityDao.findLastRevisionByIdAndSendingInstitution(tor.getOmobilityId(), sendingHeiId);

    final TranscriptOfRecordMDTO torMDTOLocal = obtenerTor(mobility);

    final MobilityMDTO mobilityRemoto = converter.convertToTorMDTOList(Arrays.asList(tor)).get(0);
    final TranscriptOfRecordMDTO torMDTORemoto = obtenerTor(mobilityRemoto);

    if (Objects.isNull(torMDTOLocal) && Objects.nonNull(mobilityRemoto)) {

      final LearningAgreementMDTO lastLaRevisionWithChanges = mobility.getLearningAgreement().stream()
              .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision)).orElse(null);

      // guardamos el tor remoto pq no lo tenemos en base de datos
      if (Objects.nonNull(lastLaRevisionWithChanges)) {
        lastLaRevisionWithChanges.setTranscriptOfRecord(torMDTORemoto);
      }

      mobilityDao.save(mobility);
      seActualiza = Boolean.FALSE;
    } else {
      if (Objects.nonNull(mobilityRemoto)) {
        // comparamos los dos y actualizamos si es necesario
        final boolean hayCambios = compararTors(torMDTORemoto, torMDTOLocal);

        if (hayCambios) {
          // actualizamos la copia local
          log.info("Actualizando copia local del TOR");
          mobilityDao.save(mobility);
          seActualiza = Boolean.TRUE;
        }
      }
    }

    return seActualiza;
  }

  private TranscriptOfRecordMDTO obtenerTor(final MobilityMDTO mobility) {
    if (Objects.nonNull(mobility)) {
      // obtenemos el TOR asociado a la ultima revision del Learning Agreement de la mobilidad
      final LearningAgreementMDTO lastLaRevisionWithChanges = mobility.getLearningAgreement().stream()
          .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision)).orElse(null);

      return Objects.nonNull(lastLaRevisionWithChanges) ? lastLaRevisionWithChanges.getTranscriptOfRecord() : null;
    } else {
      return null;
    }
  }

  /**
   * Compara dos TranscriptOfRecordMDTO actualizando la copia local
   *
   * @param remoto TranscriptOfRecordMDTO remoto
   * @param local TranscriptOfRecordMDTO local
   * @return true si hay cambios entre los dos TranscriptOfRecordMDTO
   */
  public boolean compararTors(final TranscriptOfRecordMDTO remoto, final TranscriptOfRecordMDTO local) {
    boolean hayCambios = Boolean.FALSE;

    log.trace("Comienza el proceso de comparacion de TORs");

    if (remoto != null && local != null) {

      if (Utils.compararFechaYDias(remoto.getGeneratedDate(), local.getGeneratedDate())) {
        // log.trace("Ha cambiado la propiedad {}", "GeneratedDate");
        local.setGeneratedDate(remoto.getGeneratedDate());
        hayCambios = Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getIscedTable())) {
        final boolean compareRes = compararIscedTableList(remoto.getIscedTable(), local.getIscedTable());
        hayCambios = hayCambios || compareRes;
      }

      if (Objects.nonNull(remoto.getAttachments())) {
        final boolean compareRes = compararAttachmentsList(remoto.getAttachments(), local.getAttachments());
        hayCambios = hayCambios || compareRes;
      }

      if (Objects.nonNull(remoto.getReport())) {
        final boolean compareRes = compararReportList(remoto.getReport(), local.getReport());
        hayCambios = hayCambios || compareRes;
      }

//    ignoramos las extensiones de cara a las comparaciones
//    if (Arrays.hashCode(remoto.getExtension()) != Arrays.hashCode(local.getExtension())) {
//      hayCambios = Boolean.TRUE;
//    }

    }

    return hayCambios;
  }

  /**
   * Compara dos listas de IscedTableMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararIscedTableList(final List<IscedTableMDTO> remoto, final List<IscedTableMDTO> local) {
    log.trace("Comienza la comparación de listdos de IscedTableMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<IscedTableMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLanguageSKillWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<IscedTableMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararLanguageSKillWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de IscedTableMDTO");
    log.trace("Se van a borrar los siguientes IscedTableMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes IscedTableMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un IscedTableMDTO esta en una lista de IscedTableMDTO
   *
   * @param item IscedTableMDTO
   * @param list listado de IscedTableMDTO
   * @param esItemLocal indica si el item es un IscedTableMDTO local.
   * @return true si el IscedTableMDTO esta presente en la lista
   */
  private boolean compararLanguageSKillWithList(final IscedTableMDTO item, final List<IscedTableMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararIscedTable(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararIscedTable(item, i));
    }
  }

  /**
   * Compara dos IscedTableMDTO sin actualizar la copia local
   *
   * @param remoto IscedTableMDTO remoto
   * @param local IscedTableMDTO local
   * @return true si hay cambios entre los dos IscedTableMDTO
   */
  public boolean compararIscedTable(final IscedTableMDTO remoto, final IscedTableMDTO local) {
    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getIscedCode(), local.getIscedCode())) {
        // log.trace("Ha cambiado la propiedad {}", "IscedCode");
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getFrequencies())) {
        return compararGradeFrequencyList(remoto.getFrequencies(), local.getFrequencies());
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de GradeFrequencyMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararGradeFrequencyList(final List<GradeFrequencyMDTO> remoto,
      final List<GradeFrequencyMDTO> local) {
    log.trace("Comienza la comparación de listdos de GradeFrequency");

    // todos los elementos de la lista local que no estan en la remota
    final List<GradeFrequencyMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLanguageSKillWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<GradeFrequencyMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararLanguageSKillWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de GradeFrequencyMDTO");
    log.trace("Se van a borrar los siguientes GradeFrequencyMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes GradeFrequencyMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un GradeFrequencyMDTO esta en una lista de IscedTableMDTO
   *
   * @param item GradeFrequencyMDTO
   * @param list listado de GradeFrequencyMDTO
   * @param esItemLocal indica si el item es un GradeFrequencyMDTO local.
   * @return true si el GradeFrequencyMDTO esta presente en la lista
   */
  private boolean compararLanguageSKillWithList(final GradeFrequencyMDTO item, final List<GradeFrequencyMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararGradeFerequency(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararGradeFerequency(item, i));
    }
  }

  /**
   * Compara dos GradeFrequencyMDTO sin actualizar la copia local
   *
   * @param remoto GradeFrequencyMDTO remoto
   * @param local GradeFrequencyMDTO local
   * @return true si hay cambios entre los dos GradeFrequencyMDTO
   */
  public boolean compararGradeFerequency(final GradeFrequencyMDTO remoto, final GradeFrequencyMDTO local) {
    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getLabel(), local.getLabel())) {
        //// log.trace("Ha cambiado la propiedad {}", "GradeFreq - Label");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getPercentage(), local.getPercentage())) {
        //// log.trace("Ha cambiado la propiedad {}", "GradeFreq - Percentage");
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de IscedTableMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararAttachmentsList(final List<AttachmentMDTO> remoto, final List<AttachmentMDTO> local) {
    log.trace("Comienza la comparación de listdos de Attachements");

    // todos los elementos de la lista local que no estan en la remota
    final List<AttachmentMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararAttachmentContentWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<AttachmentMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararAttachmentContentWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de AttachmentMDTO");
    log.trace("Se van a borrar los siguientes AttachmentMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes AttachmentMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un IscedTableMDTO esta en una lista de IscedTableMDTO
   *
   * @param item IscedTableMDTO
   * @param list listado de IscedTableMDTO
   * @param esItemLocal indica si el item es un IscedTableMDTO local.
   * @return true si el IscedTableMDTO esta presente en la lista
   */
  private boolean compararAttachmentContentWithList(final AttachmentMDTO item, final List<AttachmentMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararAttachment(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararAttachment(item, i));
    }
  }

  /**
   * Compara dos AttachmentMDTO sin actualizar la copia local
   *
   * @param remoto AttachmentMDTO remoto
   * @param local AttachmentMDTO local
   * @return true si hay cambios entre los dos AttachmentMDTO
   */
  public boolean compararAttachment(final AttachmentMDTO remoto, final AttachmentMDTO local) {

    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getType(), local.getType())) {
        // log.trace("Ha cambiado la propiedad {}", "Type");
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getContents())) {
        if (compararAttachmentContentList(remoto.getContents(), local.getContents())) {
          // log.trace("Ha cambiado la propiedad {}", "Contents");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getTitle())) {
        if (iiasService.compararLanguageItemsList(remoto.getTitle(), local.getTitle(), Boolean.TRUE)) {
          // log.trace("Ha cambiado la propiedad {}", "Title");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getDescription())) {
        if (iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.TRUE)) {
          // log.trace("Ha cambiado la propiedad {}", "Description");
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de AttachmentContentMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararAttachmentContentList(final List<AttachmentContentMDTO> remoto,
      final List<AttachmentContentMDTO> local) {
    log.trace("Comienza la comparación de listdos de IscedTableMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<AttachmentContentMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararAttachmentContentWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<AttachmentContentMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararAttachmentContentWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de AttachmentMDTO");
    log.trace("Se van a borrar los siguientes AttachmentMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes AttachmentMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un IscedTableMDTO esta en una lista de IscedTableMDTO
   *
   * @param item IscedTableMDTO
   * @param list listado de IscedTableMDTO
   * @param esItemLocal indica si el item es un IscedTableMDTO local.
   * @return true si el IscedTableMDTO esta presente en la lista
   */
  private boolean compararAttachmentContentWithList(final AttachmentContentMDTO item,
      final List<AttachmentContentMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararAttachmentContent(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararAttachmentContent(item, i));
    }
  }

  /**
   * Compara dos AttachmentContentMDTO sin actualizar la copia local
   *
   * @param remoto AttachmentContentMDTO remoto
   * @param local AttachmentContentMDTO local
   * @return true si hay cambios entre los dos AttachmentContentMDTO
   */
  public boolean compararAttachmentContent(final AttachmentContentMDTO remoto, final AttachmentContentMDTO local) {
    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getLanguage(), local.getLanguage())) {
        // log.trace("Ha cambiado la propiedad {}", "Language");
        return Boolean.TRUE;
      }

      if (!Arrays.equals(remoto.getContent(), local.getContent())) {
        // log.trace("Ha cambiado la propiedad {}", "Content");
        return Boolean.TRUE;
      }
    }


    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de ReportMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararReportList(final List<ReportMDTO> remoto, final List<ReportMDTO> local) {
    log.trace("Comienza la comparación de listdos de ReportMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<ReportMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararReportWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<ReportMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararReportWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de ReportMDTO");
    log.trace("Se van a borrar los siguientes ReportMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes ReportMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un ReportMDTO esta en una lista de ReportMDTO
   *
   * @param item ReportMDTO
   * @param list listado de ReportMDTO
   * @param esItemLocal indica si el item es un ReportMDTO local.
   * @return true si el ReportMDTO esta presente en la lista
   */
  private boolean compararReportWithList(final ReportMDTO item, final List<ReportMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararReport(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararReport(item, i));
    }
  }

  /**
   * Compara dos ReportMDTO sin actualizar la copia local
   *
   * @param remoto ReportMDTO remoto
   * @param local ReportMDTO local
   * @return true si hay cambios entre los dos ReportMDTO
   */
  public boolean compararReport(final ReportMDTO remoto, final ReportMDTO local) {

    if (remoto != null && local != null) {

      if (Utils.compararFechaYDias(remoto.getIssueDate(), local.getIssueDate())) {
        // log.trace("Ha cambiado la propiedad {}", "IssueDate");
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getLois())) {
        if (compararLoisList(remoto.getLois(), local.getLois())) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getAttachments())) {
        if (compararAttachmentsList(remoto.getAttachments(), local.getAttachments())) {
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de LearningOpportunityInstanceMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararLoisList(final List<LearningOpportunityInstanceMDTO> remoto,
      final List<LearningOpportunityInstanceMDTO> local) {
    log.trace("Comienza la comparación de listdos de LearningOpportunityInstanceMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<LearningOpportunityInstanceMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararLoiWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<LearningOpportunityInstanceMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLoiWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de LearningOpportunityInstanceMDTO");
    log.trace("Se van a borrar los siguientes LearningOpportunityInstanceMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes LearningOpportunityInstanceMDTO: {}", incluir);

    List<LearningOpportunityInstanceMDTO> modifiableList = new ArrayList<>(local);
    modifiableList.removeAll(borrar);
    modifiableList.addAll(incluir);
    //local.removeAll(borrar);
    //local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LearningOpportunityInstanceMDTO esta en una lista de LearningOpportunityInstanceMDTO
   *
   * @param item LearningOpportunityInstanceMDTO
   * @param list listado de LearningOpportunityInstanceMDTO
   * @param esItemLocal indica si el item es un LearningOpportunityInstanceMDTO local.
   * @return true si el LearningOpportunityInstanceMDTO esta presente en la lista
   */
  private boolean compararLoiWithList(final LearningOpportunityInstanceMDTO item,
      final List<LearningOpportunityInstanceMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararLoi(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararLoi(item, i));
    }
  }

  /**
   * Compara dos LearningOpportunityInstanceMDTO sin actualizar la copia local
   *
   * @param remoto LearningOpportunityInstanceMDTO remoto
   * @param local LearningOpportunityInstanceMDTO local
   * @return true si hay cambios entre los dos LearningOpportunityInstanceMDTO
   */
  public boolean compararLoi(final LearningOpportunityInstanceMDTO remoto,
                             final LearningOpportunityInstanceMDTO local) {

    if (remoto != null && local != null) {

      if (Objects.nonNull(remoto.getGradingScheme())) {
        if (compararGradingScheme(remoto.getGradingScheme(), local.getGradingScheme())) {
          // log.trace("Ha cambiado la propiedad {}", "GradingScheme");
          return Boolean.TRUE;
        }
      }

      if (!Objects.equals(remoto.getLanguageOfInstruction(), local.getLanguageOfInstruction())) {
        // log.trace("Ha cambiado la propiedad {}", "LanguageOfInstruction");
        return Boolean.TRUE;
      }

      if (!Utils.nullSafeCompareBigDecimal(remoto.getEngagementHours(), local.getEngagementHours())) {
        // log.trace("Ha cambiado la propiedad {}", "EngagementHours");
        return Boolean.TRUE;
      }

      if (Utils.compararFechaYDias(remoto.getStartDate(), local.getStartDate())) {
        // log.trace("Ha cambiado la propiedad {}", "StartDate");
        return Boolean.TRUE;
      }

      if (Utils.compararFechaYDias(remoto.getEndDate(), local.getEndDate())) {
        // log.trace("Ha cambiado la propiedad {}", "EndDate");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getPercentageLower(), local.getPercentageLower())) {
        // log.trace("Ha cambiado la propiedad {}", "PercentageLower");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getPercentageEqual(), local.getPercentageEqual())) {
        // log.trace("Ha cambiado la propiedad {}", "PercentageEqual");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getPercentageHigher(), local.getPercentageHigher())) {
        // log.trace("Ha cambiado la propiedad {}", "PercentageHigher");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getResultLabel(), local.getResultLabel())) {
        // log.trace("Ha cambiado la propiedad {}", "ResultLabel");
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getStatus(), local.getStatus())) {
        // log.trace("Ha cambiado la propiedad {}", "Status");
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getResultDistribution())) {
        if (!compararResultDistribution(remoto.getResultDistribution(), local.getResultDistribution())) {
          // log.trace("Ha cambiado la propiedad {}", "ResultDist");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getLevels())) {
        if (!compararLevelList(remoto.getLevels(), local.getLevels())) {
          // log.trace("Ha cambiado la propiedad {}", "Levels");
          return Boolean.TRUE;
        }
      }

      // ignoramos las extensiones de cara a las comparaciones
//    if (!Arrays.equals(remoto.getExtension(), local.getExtension())) {
//      // log.trace("Ha cambiado la propiedad {}", "Extension");
//      return Boolean.TRUE;
//    }

      if (Objects.nonNull(remoto.getLoiGrouping())) {
        if (!compararLoiGrouping(remoto.getLoiGrouping(), local.getLoiGrouping())) {
          // log.trace("Ha cambiado la propiedad {}", "Grouping");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getAttachments())) {
        if (!compararAttachmentsList(remoto.getAttachments(), local.getAttachments())) {
          // log.trace("Ha cambiado la propiedad {}", "Attachments");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getDiploma())) {
        if (!compararDiploma(remoto.getDiploma(), local.getDiploma())) {
          // log.trace("Ha cambiado la propiedad {}", "Diploma");
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getLearningOpportunitySpecification())) {
        if (!compararLos(remoto.getLearningOpportunitySpecification(), local.getLearningOpportunitySpecification())) {
          // log.trace("Ha cambiado la propiedad {}", "LOS");
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos LearningOpportunitySpecificationMDTO sin actualizar la copia local
   *
   * @param remoto LearningOpportunitySpecificationMDTO remoto
   * @param local LearningOpportunitySpecificationMDTO local
   * @return true si hay cambios entre los dos LearningOpportunitySpecificationMDTO
   */
  public boolean compararLos(final LearningOpportunitySpecificationMDTO remoto,
                             final LearningOpportunitySpecificationMDTO local) {

    List<LearningOpportunitySpecificationMDTO> learningOpportunitySpecifications;
    List<LearningOpportunityInstanceMDTO> learningOpportunityInstances;

    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getLosCode(), local.getLosCode())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getType(), local.getType())) {
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getName())) {
        if (iiasService.compararLanguageItemsList(remoto.getName(), local.getName(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getUrl())) {
        if (iiasService.compararLanguageItemsList(remoto.getUrl(), local.getUrl(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (!Objects.equals(remoto.getEqfLevel(), local.getEqfLevel())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getIscedf(), local.getIscedf())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getSubjectArea(), local.getSubjectArea())) {
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getDescription())) {
        if (iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (remoto.isTopLevelParent() != local.isTopLevelParent()) {
        return Boolean.TRUE;
      }

      //no se comparan las extensiones
//    if (!Arrays.equals(remoto.getExtension(), local.getExtension())) {
//      return Boolean.TRUE;
//    }

      if (Objects.nonNull(remoto.getLearningOpportunitySpecifications())
              && !remoto.getLearningOpportunitySpecifications().isEmpty()) {
        if (!compararLosList(remoto.getLearningOpportunitySpecifications(),
                local.getLearningOpportunitySpecifications())) {
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos LoiGroupingMDTO sin actualizar la copia local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararLoiGrouping(final LoiGroupingMDTO remoto, final LoiGroupingMDTO local) {
    final boolean hayCambios = Boolean.FALSE;

    if (remoto != null && local != null) {
      if (Objects.nonNull(remoto.getGroup()) && local.getGroup() != null) {
        if (compararGroup(remoto.getGroup(), local.getGroup())) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getGroupType()) && local.getGroupType() != null) {
        if (compararGroupType(remoto.getGroupType(), local.getGroupType())) {
          return Boolean.TRUE;
        }
      }
    }

    return hayCambios;
  }

  /**
   * Compara dos listas de LearningOpportunitySpecificationMDTO sin actualizar la copia local
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararLosList(final List<LearningOpportunitySpecificationMDTO> remoto,
      final List<LearningOpportunitySpecificationMDTO> local) {
    log.trace("Comienza la comparación de listdos de CreditMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<LearningOpportunitySpecificationMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararLosWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<LearningOpportunitySpecificationMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLosWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de LearningOpportunitySpecificationMDTO");
    log.trace("Se van a borrar los siguientes LearningOpportunitySpecificationMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes LearningOpportunitySpecificationMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LearningOpportunitySpecificationMDTO esta en una lista de CreditMDTO
   *
   * @param item LearningOpportunitySpecificationMDTO
   * @param list listado de LearningOpportunitySpecificationMDTO
   * @param esItemLocal indica si el item es un LearningOpportunitySpecificationMDTO local.
   * @return true si el LearningOpportunitySpecificationMDTO esta presente en la lista
   */
  private boolean compararLosWithList(final LearningOpportunitySpecificationMDTO item,
      final List<LearningOpportunitySpecificationMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararLos(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararLos(item, i));
    }
  }

  /**
   * Compara dos GradingSchemeMDTO sin actualizar la copia local
   *
   * @param remoto GradingSchemeMDTO remoto
   * @param local GradingSchemeMDTO local
   * @return true si hay cambios entre los dos GradingSchemeMDTO
   */
  public boolean compararGradingScheme(final GradingSchemeMDTO remoto, final GradingSchemeMDTO local) {

    if (remoto != null && local != null) {
      if (Objects.nonNull(remoto.getDescription()) && local.getDescription()!= null) {
        if (iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.TRUE)) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getLabel()) && local.getLabel()!= null) {
        if (iiasService.compararLanguageItemsList(remoto.getLabel(), local.getLabel(), Boolean.TRUE)) {
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos ResultDistributionMDTO sin actualizar la copia local
   *
   * @param remoto ResultDistributionMDTO remoto
   * @param local ResultDistributionMDTO local
   * @return true si hay cambios entre los dos ResultDistributionMDTO
   */
  public boolean compararResultDistribution(final ResultDistributionMDTO remoto, final ResultDistributionMDTO local) {

    if (remoto != null && local != null) {
      if (Objects.nonNull(remoto.getDescription()) && local.getDescription() != null) {
        if (!iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getResultDistributionCategory()) && local.getResultDistributionCategory()!= null) {
        if (!compararResultDistributionCategoryList(remoto.getResultDistributionCategory(),
                local.getResultDistributionCategory())) {
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de ResultDistributionCategoryMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararResultDistributionCategoryList(final List<ResultDistributionCategoryMDTO> remoto,
      final List<ResultDistributionCategoryMDTO> local) {
    log.trace("Comienza la comparación de listdos de ResultDistributionCategoryMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<ResultDistributionCategoryMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararResultDistributionCategoryWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<ResultDistributionCategoryMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararResultDistributionCategoryWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de ResultDistributionCategoryMDTO");
    log.trace("Se van a borrar los siguientes ResultDistributionCategoryMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes ResultDistributionCategoryMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un ResultDistributionMDTO esta en una lista de ResultDistributionMDTO
   *
   * @param item ResultDistributionMDTO
   * @param list listado de ResultDistributionMDTO
   * @param esItemLocal indica si el item es un ResultDistributionMDTO local.
   * @return true si el ResultDistributionMDTO esta presente en la lista
   */
  private boolean compararResultDistributionCategoryWithList(final ResultDistributionCategoryMDTO item,
      final List<ResultDistributionCategoryMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararResultDistributionCategory(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararResultDistributionCategory(item, i));
    }
  }

  /**
   * Compara dos ResultDistributionCategoryMDTO sin actualizar la copia local
   *
   * @param remoto ResultDistributionCategoryMDTO remoto
   * @param local ResultDistributionCategoryMDTO local
   * @return true si hay cambios entre los dos ResultDistributionCategoryMDTO
   */
  public boolean compararResultDistributionCategory(final ResultDistributionCategoryMDTO remoto,
                                                    final ResultDistributionCategoryMDTO local) {

    if (remoto != null && local != null) {
      if (!Objects.equals(remoto.getLabel(), local.getLabel())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getCount(), local.getCount())) {
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de LevelMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararLevelList(final List<LevelMDTO> remoto, final List<LevelMDTO> local) {
    log.trace("Comienza la comparación de listdos de LevelMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<LevelMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararLevelWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<LevelMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararLevelWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de LevelMDTO");
    log.trace("Se van a borrar los siguientes LevelMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes LevelMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LevelMDTO esta en una lista de LevelMDTO
   *
   * @param item LevelMDTO
   * @param list listado de LevelMDTO
   * @param esItemLocal indica si el item es un LevelMDTO local.
   * @return true si el LevelMDTO esta presente en la lista
   */
  private boolean compararLevelWithList(final LevelMDTO item, final List<LevelMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararLevel(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararLevel(item, i));
    }
  }

  /**
   * Compara dos LevelMDTO sin actualizar la copia local
   *
   * @param remoto LevelMDTO remoto
   * @param local LevelMDTO local
   * @return true si hay cambios entre los dos LevelMDTO
   */
  public boolean compararLevel(final LevelMDTO remoto, final LevelMDTO local) {

    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getValue(), local.getValue())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getType(), local.getType())) {
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getDescription()) && Objects.nonNull(local.getDescription())) {
        if (!iiasService.compararLanguageItemsList(remoto.getDescription(), local.getDescription(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }


  /**
   * Compara dos GroupTypeMDTO sin actualizar la copia local
   *
   * @param remoto GroupTypeMDTO remoto
   * @param local GroupTypeMDTO local
   * @return true si hay cambios entre los dos GroupTypeMDTO
   */
  public boolean compararGroupType(final GroupTypeMDTO remoto, final GroupTypeMDTO local) {
    if (remoto != null && local != null) {
      if (Objects.nonNull(remoto.getTitle()) && Objects.nonNull(local.getTitle())) {
        if (!iiasService.compararLanguageItems(remoto.getTitle(), local.getTitle(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getTitle()) && Objects.nonNull(local.getTitle())) {
        if (!compararGroupList(remoto.getGroups(), local.getGroups())) {
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de GroupMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  private boolean compararGroupList(final List<GroupMDTO> remoto, final List<GroupMDTO> local) {
    log.trace("Comienza la comparación de listdos de GroupMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<GroupMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(
          local.stream().filter(i -> !compararGroupWithList(i, remoto, Boolean.FALSE)).collect(Collectors.toList()));
    }

    final List<GroupMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararGroupWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de GroupMDTO");
    log.trace("Se van a borrar los siguientes GroupMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes GroupMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un GroupMDTO esta en una lista de GroupMDTO
   *
   * @param item GroupMDTO
   * @param list listado de GroupMDTO
   * @param esItemLocal indica si el item es un GroupMDTO local.
   * @return true si el GroupMDTO esta presente en la lista
   */
  private boolean compararGroupWithList(final GroupMDTO item, final List<GroupMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararGroup(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararGroup(item, i));
    }
  }

  /**
   * Compara dos GroupMDTO sin actualizar la copia local
   *
   * @param remoto GroupMDTO remoto
   * @param local GroupMDTO local
   * @return true si hay cambios entre los dos GroupMDTO
   */
  public boolean compararGroup(final GroupMDTO remoto, final GroupMDTO local) {

    if (remoto != null && local != null) {
      if (Objects.nonNull(remoto.getTitle())) {
        if (!iiasService.compararLanguageItems(remoto.getTitle(), local.getTitle(), Boolean.FALSE)) {
          return Boolean.TRUE;
        }
      }

      if (!Objects.equals(remoto.getSortingKey(), local.getSortingKey())) {
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos DiplomaMDTO sin actualizar la copia local
   *
   * @param remoto DiplomaMDTO remoto
   * @param local DiplomaMDTO local
   * @return true si hay cambios entre los dos DiplomaMDTO
   */
  public boolean compararDiploma(final DiplomaMDTO remoto, final DiplomaMDTO local) {
    List<DiplomaSectionMDTO> section;

    if (remoto != null && local != null) {
      if (!Objects.equals(remoto.getDiplomaVersion(), local.getDiplomaVersion())) {
        return Boolean.TRUE;
      }

      if (Utils.compararFechaYDias(remoto.getIssueDate(), local.getIssueDate())) {
        return Boolean.TRUE;
      }

      if (!Arrays.equals(remoto.getIntroduction(), local.getIntroduction())) {
        return Boolean.TRUE;
      }

      if (!Arrays.equals(remoto.getSignature(), local.getSignature())) {
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getSection()) && Objects.nonNull(local.getSection())) {
        if (!compararDiplomaSectionList(remoto.getSection(), local.getSection())) {
          return Boolean.TRUE;
        }
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de DiplomaSectionMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  private boolean compararDiplomaSectionList(final List<DiplomaSectionMDTO> remoto,
      final List<DiplomaSectionMDTO> local) {
    log.trace("Comienza la comparación de listdos de DiplomaSectionMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<DiplomaSectionMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararDiplomaSectionWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<DiplomaSectionMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararDiplomaSectionWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de DiplomaSectionMDTO");
    log.trace("Se van a borrar los siguientes DiplomaSectionMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes DiplomaSectionMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un DiplomaSectionMDTO esta en una lista de DiplomaSectionMDTO
   *
   * @param item DiplomaSectionMDTO
   * @param list listado de DiplomaSectionMDTO
   * @param esItemLocal indica si el item es un DiplomaSectionMDTO local.
   * @return true si el DiplomaSectionMDTO esta presente en la lista
   */
  private boolean compararDiplomaSectionWithList(final DiplomaSectionMDTO item, final List<DiplomaSectionMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararDiplomaSection(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararDiplomaSection(item, i));
    }
  }

  /**
   * Compara dos DiplomaSectionMDTO sin actualizar la copia local
   *
   * @param remoto DiplomaSectionMDTO remoto
   * @param local DiplomaSectionMDTO local
   * @return true si hay cambios entre los dos DiplomaSectionMDTO
   */
  public boolean compararDiplomaSection(final DiplomaSectionMDTO remoto, final DiplomaSectionMDTO local) {
    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getTitle(), local.getTitle())) {
        return Boolean.TRUE;
      }

      if (!Arrays.equals(remoto.getContent(), local.getContent())) {
        return Boolean.TRUE;
      }

      if (!Objects.equals(remoto.getNumber(), local.getNumber())) {
        return Boolean.TRUE;
      }

      if (Objects.nonNull(remoto.getAttachments()) && Objects.nonNull(local.getAttachments())) {
        if (!compararAttachmentsList(remoto.getAttachments(), local.getAttachments())) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getAdditionalInfo()) && Objects.nonNull(local.getAdditionalInfo())) {
        if (!compararDiplomaAdditionalInfoList(remoto.getAdditionalInfo(), local.getAdditionalInfo())) {
          return Boolean.TRUE;
        }
      }

      if (Objects.nonNull(remoto.getSections()) && Objects.nonNull(local.getSections())) {
        if (!compararDiplomaSectionList(remoto.getSections(), local.getSections())) {
          return Boolean.TRUE;
        }
      }
    }
    return Boolean.FALSE;
  }

  /**
   * Compara dos listas de DiplomaAdditionalInfoMDTO.
   *
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  private boolean compararDiplomaAdditionalInfoList(final List<DiplomaAdditionalInfoMDTO> remoto,
      final List<DiplomaAdditionalInfoMDTO> local) {

    log.trace("Comienza la comparación de listdos de DiplomaAdditionalInfoMDTO");

    // todos los elementos de la lista local que no estan en la remota
    final List<DiplomaAdditionalInfoMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararDiplomaAddtionalInfoWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<DiplomaAdditionalInfoMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir = CollectionUtils.isNotEmpty(remoto) ? remoto.stream()
              .filter(i -> !compararDiplomaAddtionalInfoWithList(i, local, Boolean.TRUE)).collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de DiplomaAdditionalInfoMDTO");
    log.trace("Se van a borrar los siguientes DiplomaAdditionalInfoMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes DiplomaAdditionalInfoMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un DiplomaAdditionalInfoMDTO esta en una lista de DiplomaSectionMDTO
   *
   * @param item DiplomaAdditionalInfoMDTO
   * @param list listado de DiplomaAdditionalInfoMDTO
   * @param esItemLocal indica si el item es un DiplomaAdditionalInfoMDTO local.
   * @return true si el DiplomaAdditionalInfoMDTO esta presente en la lista
   */
  private boolean compararDiplomaAddtionalInfoWithList(final DiplomaAdditionalInfoMDTO item,
      final List<DiplomaAdditionalInfoMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    // en cuanto encuentres une elemento en la lista que al compararlo con el item sea igual devuelves el resultado
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararDiplomaAdditionalInfo(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararDiplomaAdditionalInfo(item, i));
    }
  }

  public boolean compararDiplomaAdditionalInfo(final DiplomaAdditionalInfoMDTO remoto,
                                               final DiplomaAdditionalInfoMDTO local) {
    if (remoto != null && local != null) {

      if (!Objects.equals(remoto.getAdditionalInfo(), local.getAdditionalInfo())) {
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }
}
