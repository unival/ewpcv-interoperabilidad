package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IStatisticsOlaConverter;
import es.minsait.ewpcv.repository.dao.IStatisticsOlaDAO;
import es.minsait.ewpcv.repository.model.omobility.OmobilityStatsMDTO;
import es.minsait.ewpcv.repository.model.statistics.StatisticsOlaMDTO;
import es.minsait.ewpcv.service.api.IStatisticsOlaService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LasOutgoingStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

/**
 * The interface StatisticsOla service.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)loc
 */

@Service
@Slf4j
@Transactional
public class StatisticsOlaServiceImpl implements IStatisticsOlaService {
    @Autowired
    private IStatisticsOlaDAO statisticsOlaDAO;
    @Autowired
    private IStatisticsOlaConverter iStatisticsOlaConverter;

    @Override
    public void obtainStatistics(String user) {

        List<OmobilityStatsMDTO> omoMdtos = statisticsOlaDAO.getStatsOfOmobilitiTable();

        for(OmobilityStatsMDTO omoMdto : omoMdtos)
        {
            if(omoMdto.getSendingInstitution().equalsIgnoreCase(user)) {

                StatisticsOlaMDTO statOla = new StatisticsOlaMDTO();

                statOla.setSendingInstitution(omoMdto.getSendingInstitution());
                statOla.setAcademicYearStart(omoMdto.getAcademicYearStart());
                statOla.setAcademicYearEnd(omoMdto.getAcademicYearEnd());
                statOla.setTotalOutgoingNumber(omoMdto.getTotalOutgoingNumber());
                statOla.setApprovalModif(omoMdto.getApprovalModif());
                statOla.setApprovalUnModif(omoMdto.getApprovalUnModif());
                statOla.setLastAppr(omoMdto.getLastAppr());
                statOla.setLastRej(omoMdto.getLastRej());
                statOla.setLastWait(omoMdto.getLastWait());
                statOla.setDumpDate(new Date());

                statisticsOlaDAO.saveStatsOla(statOla);
            }
        }

    }

    @Override
    public List<LasOutgoingStatsResponse.AcademicYearLaStats> getStatisticsRest() {

        return iStatisticsOlaConverter.convertToStatsResponses(statisticsOlaDAO.getStatisticsRest()) ;
    }
}
