package es.minsait.ewpcv.utils;

import org.apache.commons.lang3.time.DateUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class Utils {

    public static boolean compararFechaYDias(Date dateRemoto, Date dateLocal){
        return (Objects.isNull(dateRemoto) && Objects.nonNull(dateLocal)) ||
                (Objects.nonNull(dateRemoto) && Objects.isNull(dateLocal)) ||
                (Objects.nonNull(dateRemoto) && Objects.nonNull(dateLocal) && !DateUtils.isSameDay(dateRemoto, dateLocal));
    }

    public static boolean compararFechaYHorasYSegundos(Date dateRemoto, Date dateLocal){
        return (Objects.isNull(dateRemoto) && Objects.nonNull(dateLocal)) ||
                (Objects.nonNull(dateRemoto) && Objects.isNull(dateLocal)) ||
                (Objects.nonNull(dateRemoto) && Objects.nonNull(dateLocal) &&
                        (dateRemoto.getTime() / 1000 != dateLocal.getTime() / 1000));
    }

    public static boolean nullSafeCompareBigDecimal(final BigDecimal a, final BigDecimal b) {
        if (Objects.isNull(a) && Objects.isNull(b)) {
            return true;
        }
        if (Objects.isNull(a) || Objects.isNull(b)) {
            return false;
        }
        return a.compareTo(b) == 0;
    }

    public static boolean nullSafeCompareStringIgnoreCase(final String a, final String b) {
        if (Objects.isNull(a) && Objects.isNull(b)) {
            return true;
        }
        if (Objects.isNull(a) || Objects.isNull(b)) {
            return false;
        }
        return a.equalsIgnoreCase(b);
    }

    public static byte[] getByteArrayFromObject(final Object body, final boolean formatedOutput) throws IOException {
        try {
            if(Objects.isNull(body)){
                return new byte[0];
            }

            final byte[] bodyBytes;
            final JAXBContext jaxbContext = JAXBContext.newInstance(body.getClass());
            final Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatedOutput);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                marshaller.marshal(body, baos);
                bodyBytes = baos.toByteArray();
            }
            return bodyBytes;
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
        return new byte[0];
    }
}
