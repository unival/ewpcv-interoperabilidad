package es.minsait.ewpcv.converters.impl;

import es.minsait.ewpcv.converter.api.IStatisticsIiaConverter;
import es.minsait.ewpcv.repository.model.statistics.StatisticsIiaMDTO;
import eu.erasmuswithoutpaper.api.iias7.endpoints.IiasStatsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Objects;

/**
 * The type StatiticsIia converter.
 *
 * @author (ilanciano@minsait.com)
 */
@Component
@Slf4j
public class StatisticsIiaConverter implements IStatisticsIiaConverter {
    @Override
    public IiasStatsResponse convertToStatsResponse(StatisticsIiaMDTO mdto) {

        IiasStatsResponse responseItem = new IiasStatsResponse();
        if(Objects.isNull(mdto)){
            mdto = new StatisticsIiaMDTO();
        }
        responseItem.setIiaFetchable(Objects.nonNull(mdto.getFetchable()) ? BigInteger.valueOf(mdto.getFetchable()): BigInteger.ZERO);
        responseItem.setIiaBothApproved(Objects.nonNull(mdto.getBothApproved()) ? BigInteger.valueOf(mdto.getBothApproved()): BigInteger.ZERO);
        responseItem.setIiaLocalUnapprovedPartnerApproved(Objects.nonNull(mdto.getLocalUnappr_partnerAppr()) ? BigInteger.valueOf(mdto.getLocalUnappr_partnerAppr()): BigInteger.ZERO);
        responseItem.setIiaLocalApprovedPartnerUnapproved(Objects.nonNull(mdto.getLocalAppr_partnerUnappr()) ? BigInteger.valueOf(mdto.getLocalAppr_partnerUnappr()): BigInteger.ZERO);

        return responseItem;
    }
}
