package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converters.impl.OmobilityLasConverter;
import es.minsait.ewpcv.error.EwpConverterException;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.repository.impl.LearningAgreementDAO;
import es.minsait.ewpcv.repository.impl.OmobilityDAO;
import es.minsait.ewpcv.repository.impl.OrganizationUnitDAO;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.course.AcademicYearMDTO;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import es.minsait.ewpcv.service.api.ILearningAgreementService;
import es.minsait.ewpcv.service.api.OmobilityLasService;
import es.minsait.ewpcv.utils.Utils;
import eu.erasmuswithoutpaper.api.omobilities.endpoints.StudentMobilityForStudies;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The type Omobility las service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class OmobilityLasServiceImpl implements OmobilityLasService {

  public static final String HA_CAMBIADO_LA_PROPIEDAD = "Ha cambiado la propiedad {}";
  private OmobilityDAO omobilityDAO;
  private OmobilityLasConverter omobilityConverter;
  private IIiasServiceImpl iiasService;
  private ILearningAgreementService laService;
  private LearningAgreementDAO learningAgreementDAO;
  private OrganizationUnitDAO organizationUnitDAO;

  @Autowired
  public OmobilityLasServiceImpl(final OmobilityDAO omobilityDAO, final OmobilityLasConverter omobilityConverter,
      final IIiasServiceImpl iiasService, final ILearningAgreementService laService,
      final LearningAgreementDAO learningAgreementDAO, final OrganizationUnitDAO organizationUnitDAO) {
    this.omobilityDAO = omobilityDAO;
    this.omobilityConverter = omobilityConverter;
    this.iiasService = iiasService;
    this.laService = laService;
    this.learningAgreementDAO = learningAgreementDAO;
    this.organizationUnitDAO = organizationUnitDAO;
  }

  @Override
  public List<LearningAgreement> findOMobilitiesByIdAndSendingInstitutionId(final String sendingHeiId,
      final List<String> oMobilityIdList, final List<String> receivingHeiIdList) throws EwpConverterException {

    //dividimos la lista de heis en bloques de 1000 e invocamos al DAO para cada bloque
    final int MAX_LIST_SIZE = 1000;
    Collection<List<String>> partitionedList = IntStream.range(0, receivingHeiIdList.size())
            .boxed()
            .collect(Collectors.groupingBy(partition -> (partition / MAX_LIST_SIZE),
                    Collectors.mapping(receivingHeiIdList::get, Collectors.toList())))
            .values();

    List<MobilityMDTO> omobilities = new ArrayList<>();
    for(List<String> heisIds : partitionedList){
      omobilities.addAll(omobilityDAO.findOMobilitiesByIdAndSendingInstitutionId(sendingHeiId, oMobilityIdList, heisIds));
    }

    return omobilityConverter.convertToOmobilities(omobilities);
  }

  @Override
  public List<String> findLocalOMobilitiesId(final String sendingHeiId, final String receivingHei,
      final String modifiedSince) {

    return omobilityDAO.findLocalOMobilitiesId(sendingHeiId, receivingHei);
  }

  @Override
  public List<String> getLaIdsByCriteria(final String sendingHeiId, final List<String> receivingHeiId,
      final String receivingAcademicYearId, final String globalId, final String mobilityType,
      final String modifiedSince) {
    final Map<Object, Object> queryParams = new HashMap<>();

    this.addParam(queryParams, sendingHeiId, "sendingHeiId");
    this.addParam(queryParams, receivingHeiId, "receivingHeiId");
    this.addParam(queryParams, receivingAcademicYearId, "receivingAcademicYearId");
    this.addParam(queryParams, globalId, "globalId");
    this.addParam(queryParams, mobilityType, "mobilityType");
    this.addParam(queryParams, modifiedSince, "modifiedSince");


    return omobilityDAO.getLaIdsByCriteria(queryParams);
  }

  @Override
  public boolean isMatchSendingHeiIdAndOmobilityId(final String sendingHeiId, final String oMobilityId) {
    return omobilityDAO.countOMobilitiesByIdAndSendingInstitutionId(sendingHeiId, oMobilityId) != 0;
  }

  private void addParam(final Map<Object, Object> queryParams, final Object paramValue, final String paramKey) {
    if (Objects.nonNull(paramValue)) {
      queryParams.put(paramKey, paramValue);
    }
  }

  private Boolean participoEnLearningAgreement(final LearningAgreement ola, final String ewpInstance) {
    if (Objects.isNull(ewpInstance) || Objects.isNull(ola) || Objects.isNull(ola.getSendingHei())
        || Objects.isNull(ola.getReceivingHei())) {
      return Boolean.FALSE;
    }
    return ewpInstance.equalsIgnoreCase(ola.getSendingHei().getHeiId())
        || ewpInstance.equalsIgnoreCase(ola.getReceivingHei().getHeiId());
  }

  @Override
  @Transactional(readOnly = false)
  public Boolean insertarOmobilities(final LearningAgreement remoto, final String ewpInstance)
      throws EwpConverterException {
    if (!participoEnLearningAgreement(remoto, ewpInstance)) {
      log.info(" No participamos en el learning agreement, no lo volcamos");
      return Boolean.FALSE;
    }
    log.info(" No hay copia local, convertimos y persistimos el Omobility remoto " + remoto.getOmobilityId());
    final MobilityMDTO remotoMdto = omobilityConverter.convertToOmobilityMDTO(remoto, null, null);

    if(remotoMdto.getLearningAgreement().stream().anyMatch(la ->
            LearningAgreementStatus.CHANGES_PROPOSED.equals(la.getStatus()) &&
                    Objects.nonNull(la.getStudentSign()) &&
                    Objects.nonNull(la.getSenderCoordinatorSign()) &&
                    Objects.nonNull(la.getReciverCoordinatorSign()))){
      log.info(" Se impide el volcado del LA debido a que están las firmas de Sender y Receiver.");
      throw new EwpConverterException(" The LA dump is prevented because both the Sender and Receiver signatures are present.");
    }

    omobilityDAO.save(remotoMdto);
    return Boolean.TRUE;
  }

  @Override
  @Transactional(readOnly = false)
  public Boolean actualizarOmobilities(final LearningAgreement remoto, final MobilityMDTO local,
      final String ewpInstance) throws EwpConverterException {
    if (!participoEnLearningAgreement(remoto, ewpInstance)) {
      log.info(" No participamos en el learning agreement, no lo volcamos");
      return Boolean.FALSE;
    }


    final Optional<LearningAgreementMDTO> referenciaLocal = local.getLearningAgreement().stream()
        .max(Comparator.comparing(LearningAgreementMDTO::getLearningAgreementRevision));

    Integer maxRevision = null;
    String laId = null;
    if (referenciaLocal.isPresent()) {
      maxRevision = referenciaLocal.get().getLearningAgreementRevision();
      laId = referenciaLocal.get().getId();
    }
    // convertimos el LA remoto en un DTO
    final MobilityMDTO remotoMDTO = omobilityConverter.convertToOmobilityMDTO(remoto, laId, maxRevision);

    if(remotoMDTO.getLearningAgreement().stream().anyMatch(la ->
            LearningAgreementStatus.CHANGES_PROPOSED.equals(la.getStatus()) &&
                    Objects.nonNull(la.getStudentSign()) &&
                    Objects.nonNull(la.getSenderCoordinatorSign()) &&
                    Objects.nonNull(la.getReciverCoordinatorSign()))){
      log.info(" Se impide el volcado del LA debido a que están las firmas de Sender y Receiver.");
      throw new EwpConverterException(" The LA dump is prevented because both the Sender and Receiver signatures are present.");
    }

    final boolean hayDiferencias = compararLocalConRemoto(remotoMDTO, local);

    if (hayDiferencias) {
      log.info(" Actualizamos la copia local del Omobility " + remoto.getOmobilityId());
      omobilityDAO.save(local);
    }
    return hayDiferencias;
  }

  /**
   * Compara dos MobilityMDTO y actualiza la copia local
   * 
   * @param remoto MobilityMDTO remoto
   * @param local MobilityMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararLocalConRemoto(final MobilityMDTO remoto, final MobilityMDTO local) {
    log.trace("Comienza el proceso de comparacion de MobilityMDTO");

    boolean hayCambios = Boolean.FALSE;

    if (Utils.compararFechaYDias(remoto.getActualArrivalDate(), local.getActualArrivalDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ActualArrivalDate");
      local.setActualArrivalDate(remoto.getActualArrivalDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getActualDepartureDate(), local.getActualDepartureDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ActualDepartureDate");
      local.setActualDepartureDate(remoto.getActualDepartureDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getPlannedArrivalDate(), local.getPlannedArrivalDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PlannedArrivalDate");
      local.setPlannedArrivalDate(remoto.getPlannedArrivalDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getPlannedDepartureDate(), local.getPlannedDepartureDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PlannedDepartureDate");
      local.setPlannedDepartureDate(remoto.getPlannedDepartureDate());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getEqfLevelDeparture(), local.getEqfLevelDeparture())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EqfLevelDeparture");
      local.setEqfLevelDeparture(remoto.getEqfLevelDeparture());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getEqfLevelNomination(), local.getEqfLevelNomination())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EqfLevelNomination");
      local.setEqfLevelNomination(remoto.getEqfLevelNomination());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getIscedCode(), local.getIscedCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "IscedCode");
      local.setIscedCode(remoto.getIscedCode());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getStatusOutgoing(), local.getStatusOutgoing())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StatusOutgoing");
      local.setStatusOutgoing(remoto.getStatusOutgoing());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getStatusIncomming(), local.getStatusIncomming())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StatusIncoming");
      local.setStatusIncomming(remoto.getStatusIncomming());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReceivingInstitutionId(), local.getReceivingInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ReceivingInstitutionId");
      local.setReceivingInstitutionId(remoto.getReceivingInstitutionId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSendingInstitutionId(), local.getSendingInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SendingInstitutionId");
      local.setSendingInstitutionId(remoto.getSendingInstitutionId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getMobilityType(), local.getMobilityType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "MobilityType");
      local.setMobilityType(remoto.getMobilityType());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getMobilityParticipantId())) {
      final MobilityParticipantMDTO partRemoto = remoto.getMobilityParticipantId();
      final MobilityParticipantMDTO partLocal = local.getMobilityParticipantId();

      if (!Objects.equals(partRemoto.getGlobalId(), partLocal.getGlobalId())) {
        partLocal.setGlobalId(partRemoto.getGlobalId());
        hayCambios = Boolean.TRUE;
      }

      if (Objects.nonNull(partRemoto.getContactDetails())) {
        final ContactMDTO localContact =
            Objects.nonNull(partLocal.getContactDetails()) ? partLocal.getContactDetails() : new ContactMDTO();
        if (iiasService.compararContacts(partRemoto.getContactDetails(), localContact, Boolean.TRUE)) {
          partLocal.setContactDetails(localContact);
          hayCambios = Boolean.TRUE;
        }
      }
    }

    if (!Objects.equals(remoto.getReceivingOrganizationUnitId(), local.getReceivingOrganizationUnitId())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ReceivingOrganizationUnitId");
        local.setReceivingOrganizationUnitId(remoto.getReceivingOrganizationUnitId());
        hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSendingOrganizationUnitId(), local.getSendingOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SendingOrganizationUnitId");
      local.setSendingOrganizationUnitId(remoto.getSendingOrganizationUnitId());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getLanguageSkill())) {
      if (compararLanguageSkillList(remoto.getLanguageSkill(), local.getLanguageSkill())) {
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getSenderContactPerson())) {
      final ContactMDTO localSenderContactPerson =
          Objects.nonNull(local.getSenderContactPerson()) ? local.getSenderContactPerson() : new ContactMDTO();
      if (iiasService.compararContacts(remoto.getSenderContactPerson(), localSenderContactPerson, Boolean.TRUE)) {
        local.setSenderContactPerson(localSenderContactPerson);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getSenderAdmvContactPerson())) {
      final ContactMDTO localSenderAdmvContactPerson =
          Objects.nonNull(local.getSenderAdmvContactPerson()) ? local.getSenderAdmvContactPerson() : new ContactMDTO();
      if (iiasService.compararContacts(remoto.getSenderAdmvContactPerson(), localSenderAdmvContactPerson,
          Boolean.TRUE)) {
        local.setSenderAdmvContactPerson(localSenderAdmvContactPerson);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getReceiverContactPerson())) {
      final ContactMDTO localReceiverContactPerson =
          Objects.nonNull(local.getReceiverContactPerson()) ? local.getReceiverContactPerson() : new ContactMDTO();
      if (iiasService.compararContacts(remoto.getReceiverContactPerson(), localReceiverContactPerson, Boolean.TRUE)) {
        local.setReceiverContactPerson(localReceiverContactPerson);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getReceiverAdmvContactPerson())) {
      final ContactMDTO localReceiverAdmvContactPerson =
          Objects.nonNull(local.getReceiverAdmvContactPerson()) ? local.getReceiverAdmvContactPerson()
              : new ContactMDTO();
      if (iiasService.compararContacts(remoto.getReceiverAdmvContactPerson(), localReceiverAdmvContactPerson,
          Boolean.TRUE)) {
        local.setReceiverAdmvContactPerson(localReceiverAdmvContactPerson);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getLearningAgreement())) {
      if (compararLearningAgreementList(remoto.getLearningAgreement(), local.getLearningAgreement())) {
        hayCambios = Boolean.TRUE;
      }
    }

    log.trace("Fin del proceso de comparacion de MobilityMDTO");

    return hayCambios;
  }

  /**
   * Compara dos listas de LanguageSkillMDTO.
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados, false en caso contrario
   */
  public boolean compararLanguageSkillList(final List<LanguageSkillMDTO> remoto, final List<LanguageSkillMDTO> local) {
    log.trace("Comienza la comparación de listdos de LanguageSkill");

    // todos los elementos de la lista local que no estan en la remota
    final List<LanguageSkillMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLanguageSKillWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<LanguageSkillMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararLanguageSKillWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    log.trace("Fin de la comparación de listados de LanguageSkill");
    log.trace("Se van a borrar los siguientes LanguageSkill: {}", borrar);
    log.trace("Se van a incluir los siguientes LanguageSkill: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un LanguageSkillMDTO esta en una lista de LanguageSkillMDTO
   *
   * @param item LanguageSkillMDTO
   * @param list listado de LanguageSkillMDTO
   * @param esItemLocal indica si el item es un LanguageSkillMDTO local.
   * @return true si el LanguageSkillMDTO esta presente en la lista
   */
  private boolean compararLanguageSKillWithList(final LanguageSkillMDTO item, final List<LanguageSkillMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararLanguageSkill(i, item, Boolean.FALSE));
    } else {
      return list.stream().anyMatch(i -> !compararLanguageSkill(item, i, Boolean.FALSE));
    }
  }

  /**
   * Compara dos LanguageSkillMDTO
   *
   * @param remoto LanguageSkillMDTO remoto
   * @param local LanguageSkillMDTO local
   * @param actulizarLocal indica si hay que actualizar la copia local al encontrar una diferencia
   * @return true si hay cambios entre los dos LanguageSkillMDTO
   */
  public boolean compararLanguageSkill(final LanguageSkillMDTO remoto, final LanguageSkillMDTO local,
      final boolean actulizarLocal) {
    boolean hayCambios = Boolean.FALSE;

    if (!Objects.equals(remoto.getCefrLevel(), local.getCefrLevel())) {
      // log.trace(HA_CAMBIADO_LA_PROPIEDAD, "CefrLevel");
      if (actulizarLocal) {
        local.setCefrLevel(remoto.getCefrLevel());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLanguage(), local.getLanguage())) {
      // log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Language");
      if (actulizarLocal) {
        local.setLanguage(remoto.getLanguage());
      }
      hayCambios = Boolean.TRUE;
    }

    return hayCambios;
  }

  /**
   * Compara dos listados de LearningAgreementMDTO y actualiza la lista local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararLearningAgreementList(final List<LearningAgreementMDTO> remoto,
      final List<LearningAgreementMDTO> local) {

    log.trace("Comienza la comparación de LearningAgreement");

    // los rejecteds no se interoperan por lo que los excluimos de la
    // comparacion
    // Los firstVersion y los Approved deberian ser inmutables por lo que tampoco los comparamos
    final List<LearningAgreementMDTO> reintegrate = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      reintegrate.addAll(local.stream().filter(l -> LearningAgreementStatus.REJECTED.equals(l.getStatus()))
          .collect(Collectors.toList()));
      reintegrate.addAll(local.stream().filter(l -> LearningAgreementStatus.FIRST_VERSION.equals(l.getStatus()))
          .collect(Collectors.toList()));
      reintegrate.addAll(local.stream().filter(l -> LearningAgreementStatus.APROVED.equals(l.getStatus()))
          .collect(Collectors.toList()));
      local.removeAll(reintegrate);
    }
    if (CollectionUtils.isNotEmpty(remoto)) {
      remoto.removeAll(remoto.stream().filter(r -> LearningAgreementStatus.FIRST_VERSION.equals(r.getStatus()))
          .collect(Collectors.toList()));
      remoto.removeAll(remoto.stream().filter(r -> LearningAgreementStatus.APROVED.equals(r.getStatus()))
          .collect(Collectors.toList()));
    }

    final List<LearningAgreementMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararLearningAgreementWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<LearningAgreementMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararLearningAgreementWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }

    local.addAll(reintegrate);
    log.trace("Fin de la comparación de LearningAgreementMDTO");
    log.trace("Se van a borrar los siguientes LearningAgreementMDTO: {}", borrar);
    log.trace("Se van a incluir los siguientes LearningAgreementMDTO: {}", incluir);
    local.removeAll(borrar);
    local.addAll(incluir);

    return !(incluir.isEmpty() && !borrar.isEmpty());
  }

  /**
   * Comprueba si un LearningAgreementMDTO esta en una lista de LearningAgreementMDTO
   *
   * @param item LearningAgreementMDTO
   * @param list listado de LearningAgreementMDTO
   * @param esItemLocal indica si el item es un LearningAgreementMDTO local.
   * @return true si el LanguageSkillMDTO esta presente en la lista
   */
  private boolean compararLearningAgreementWithList(final LearningAgreementMDTO item,
      final List<LearningAgreementMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !laService.compararLearningAgreements(i, item));
    } else {
      return list.stream().anyMatch(i -> !laService.compararLearningAgreements(item, i));
    }
  }

  /**
   * Compara dos OrganizationUnitMDTO, puede actualizar la copia local
   * 
   * @param remoto OrganizationUnitMDTO remoto
   * @param local OrganizationUnitMDTO local
   * @param actualizaLocal indica si hay que actualizar la copia local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararOrganizationUnits(final OrganizationUnitMDTO remoto, final OrganizationUnitMDTO local,
      final boolean actualizaLocal) {
    log.trace("Comienza la comparación de OrganizationUnitMDTO");
    boolean hayCambios = Boolean.FALSE;

    if (!Objects.equals(remoto.getAbbreviation(), local.getAbbreviation())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Abbreviation");
      if (actualizaLocal) {
        local.setAbbreviation(remoto.getAbbreviation());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getLogoUrl(), local.getLogoUrl())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "LogoUrl");
      if (actualizaLocal) {
        local.setLogoUrl(remoto.getLogoUrl());
      }
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitCode(), local.getOrganizationUnitCode())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitCode");
      if (actualizaLocal) {
        local.setOrganizationUnitCode(remoto.getOrganizationUnitCode());
      }
      hayCambios = Boolean.TRUE;
    }

    //if (Objects.nonNull(remoto.getParentOUnitId())) {
    log.trace("Comparamos ParentOUnitId");
    if (!Objects.equals(remoto.getParentOUnitId(), local.getParentOUnitId())) {
      if (actualizaLocal) {
        local.setParentOUnitId(remoto.getParentOUnitId());
      }
      hayCambios = Boolean.TRUE;
    }
    //}

    if (Objects.nonNull(remoto.getName())) {
      log.trace("Comparamos LanguageItem");
      if (iiasService.compararLanguageItemsList(remoto.getName(), local.getName(), Boolean.TRUE)) {
        local.setName(remoto.getName());
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getInformationItems())) {
      log.trace("Comparamos OtherId");
      final List<InformationItemMDTO> localInfoItems =
          CollectionUtils.isNotEmpty(local.getInformationItems()) ? local.getInformationItems() : new ArrayList<>();
      if (compararInformationItemList(remoto.getInformationItems(), localInfoItems)) {
        local.setInformationItems(localInfoItems);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getAdditionalRequirements())) {
      log.trace("Comparamos AdditionalRequirements");
      final List<RequirementsInfoMDTO> localRequirements =
          CollectionUtils.isNotEmpty(local.getAdditionalRequirements()) ? local.getAdditionalRequirements()
              : new ArrayList<>();
      if (compararAdditionalRequirementList(remoto.getAdditionalRequirements(), localRequirements)) {
        local.setAdditionalRequirements(localRequirements);
        hayCambios = Boolean.TRUE;
      }
    }

    if (Objects.nonNull(remoto.getFactSheet())) {
      hayCambios = compararFactSheet(remoto.getFactSheet(), local.getFactSheet());
    }

    log.trace("Fin de la comparación de OrganizarionUnitMDTO");
    return hayCambios;
  }

  /**
   * Compara dos FactSheetMDTO y actualiza la copia local
   * 
   * @param remoto FactSheetMDTO remoto
   * @param local FactSheetMDTO local
   * @return true si hay diferencias entre los objetos
   */
  @Override
  public boolean compararFactSheet(final FactSheetMDTO remoto, final FactSheetMDTO local) {
    log.trace("Comparamos los factsheets");

    boolean hayCambios = Boolean.FALSE;

    if (!Objects.equals(remoto.getDecisionWeeksLimit(), local.getDecisionWeeksLimit())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "DecisionWeekLimit");
      local.setDecisionWeeksLimit(remoto.getDecisionWeeksLimit());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getTorWeeksLimit(), local.getTorWeeksLimit())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TorWeekLimit");
      local.setTorWeeksLimit(remoto.getTorWeeksLimit());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getApplicationSpringTerm(), local.getApplicationSpringTerm())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ApplicationSpringTerm");
      local.setApplicationSpringTerm(remoto.getApplicationSpringTerm());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getApplicationAutumnTerm(), local.getApplicationAutumnTerm())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ApplicationAutumnTerm");
      local.setApplicationAutumnTerm(remoto.getApplicationAutumnTerm());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getNominationsSpringTerm(), local.getNominationsSpringTerm())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "NominationsSpringTerm");
      local.setNominationsSpringTerm(remoto.getNominationsSpringTerm());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getNominationsAutumnTerm(), local.getNominationsAutumnTerm())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "NominationsAutumnTerm");
      local.setNominationsAutumnTerm(remoto.getNominationsAutumnTerm());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      log.trace("Comparando ContactDetails");
      final ContactDetailsMDTO localContactDetails =
          Objects.nonNull(local.getContactDetails()) ? local.getContactDetails() : new ContactDetailsMDTO();
      if (iiasService.compararContactDetails(remoto.getContactDetails(), localContactDetails, Boolean.TRUE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ContactDetails");
        local.setContactDetails(localContactDetails);
        hayCambios = Boolean.TRUE;
      }
    }

    return hayCambios;
  }

  /**
   * Comprueba si un FactSheetDateMDTO esta en una lista de FactSheetDateMDTO
   *
   * @param item FactSheetDateMDTO
   * @param list listado de FactSheetDateMDTO
   * @param esItemLocal indica si el item es un LearningAgreementMDTO local.
   * @return true si el FactSheetDateMDTO esta presente en la lista
   */
  private boolean compararFactsheetDateWithList(final FactSheetDateMDTO item, final List<FactSheetDateMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> compararFactsheetDates(i, item));
    } else {
      return list.stream().anyMatch(i -> compararFactsheetDates(item, i));
    }
  }

  /**
   * Compara dos FactSheetDateMDTO sin actualizar la copia local
   * 
   * @param remoto FactSheetDateMDTO remoto
   * @param local FactSheetDateMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararFactsheetDates(final FactSheetDateMDTO remoto, final FactSheetDateMDTO local) {

    if (!Objects.equals(remoto.getType(), local.getType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Type");
      local.setType(remoto.getType());
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getAcademicTerm())) {
      final AcademicTermMDTO localTerm = Objects.nonNull(local.getAcademicTerm()) ? local.getAcademicTerm() : new AcademicTermMDTO();
      final boolean comparacion = compararAcademicTerms(remoto.getAcademicTerm(), localTerm);
      if (comparacion) {
        local.setAcademicTerm(localTerm);
      }
      return comparacion;
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos AcademicTermMDTO
   * 
   * @param remoto AcademicTermMDTO remoto
   * @param local AcademicTermMDTO local
   * @return true si hay cambios entre los dos AcademicTermMDTO
   */
  public boolean compararAcademicTerms(final AcademicTermMDTO remoto, final AcademicTermMDTO local) {

    boolean hayCambios = Boolean.FALSE;

    if (!Objects.equals(remoto.getTotalTerms(), local.getTotalTerms())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TotalTerms");
      local.setTotalTerms(remoto.getTotalTerms());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getTermNumber(), local.getTermNumber())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "TermNumber");
      local.setTermNumber(local.getTermNumber());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getStartDate(), local.getStartDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StartDate");
      local.setStartDate(local.getStartDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getEndDate(), local.getEndDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EndDate");
      local.setEndDate(local.getEndDate());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getInstitutionId(), local.getInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "InstitutionId");
      local.setInstitutionId(local.getInstitutionId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "OrganizationUnitId");
      local.setOrganizationUnitId(local.getOrganizationUnitId());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getAcademicYear())) {
      final AcademicYearMDTO yearRemoto = remoto.getAcademicYear();
      final AcademicYearMDTO yearLocal = local.getAcademicYear();

      if (Objects.isNull(yearLocal)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "AcademicYear");
        local.setAcademicYear(yearRemoto);
        hayCambios = Boolean.TRUE;
      } else {
        if (!Objects.equals(yearRemoto.getEndYear(), yearLocal.getEndYear())) {
          log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EndYear");
          yearLocal.setEndYear(yearRemoto.getEndYear());
          hayCambios = Boolean.TRUE;
        }

        if (!Objects.equals(yearRemoto.getStartYear(), yearLocal.getStartYear())) {
          log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StartYear");
          yearLocal.setStartYear(yearRemoto.getStartYear());
          hayCambios = Boolean.TRUE;
        }
      }
    }

    if (Objects.nonNull(remoto.getDispName())) {
      hayCambios = iiasService.compararLanguageItemsList(remoto.getDispName(), local.getDispName(), Boolean.TRUE);
      if (hayCambios) {
        local.setDispName(remoto.getDispName());
      }
    }

    return hayCambios;
  }


  /**
   * Compara dos listados de OrganizationUnitMDTO y actualiza la copia local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  private boolean compararOrganizationUnitsList(final List<OrganizationUnitMDTO> remoto,
      final List<OrganizationUnitMDTO> local) {
    log.trace("Comienza la comparación de OrganizationUnits");

    final List<OrganizationUnitMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararOrganizationUnitsWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<OrganizationUnitMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
              CollectionUtils.isNotEmpty(remoto)
                      ? remoto.stream().filter(i -> !compararOrganizationUnitsWithList(i, local, Boolean.TRUE))
                      .collect(Collectors.toList())
                      : new ArrayList<>();
    }


    log.trace("se van a borrar los siguientes OrganizationUnitMDTO {}", borrar.toString());
    log.trace("se van a incluir los siguientes OrganizationUnitMDTO {}", incluir.toString());
    local.addAll(incluir);
    local.removeAll(borrar);

    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un OrganizationUnitMDTO esta en una lista de OrganizationUnitMDTO
   *
   * @param item OrganizationUnitMDTO
   * @param list listado de OrganizationUnitMDTO
   * @param esItemLocal indica si el item es un OrganizationUnitMDTO local.
   * @return true si el OrganizationUnitMDTO esta presente en la lista
   */
  private boolean compararOrganizationUnitsWithList(final OrganizationUnitMDTO item,
      final List<OrganizationUnitMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararOrganizationUnits(i, item, Boolean.FALSE));
    } else {
      return list.stream().anyMatch(i -> !compararOrganizationUnits(item, i, Boolean.FALSE));
    }
  }

  /**
   * Compara dos listados de RequirementsInfoMDTO y actualiza la lista local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  @Override
  public boolean compararAdditionalRequirementList(final List<RequirementsInfoMDTO> remoto,
      final List<RequirementsInfoMDTO> local) {

    log.trace("Comienza la comparación de AdditionalRequirement");

    final List<RequirementsInfoMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararRequirementInfoWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<RequirementsInfoMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararRequirementInfoWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("se van a borrar los siguientes RequirementsInfoMDTO {}", borrar.toString());
    local.removeAll(borrar);
    log.trace("se van a incluir los siguientes RequirementsInfoMDTO {}", incluir.toString());
    local.addAll(incluir);

    log.trace("Fin de la comparación de AdditionalRequirement");

    return !(incluir.isEmpty() && borrar.isEmpty());
  }

  /**
   * Comprueba si un RequirementsInfoMDTO esta en una lista de RequirementsInfoMDTO
   *
   * @param item RequirementsInfoMDTO
   * @param list listado de RequirementsInfoMDTO
   * @param esItemLocal indica si el item es un RequirementsInfoMDTO local.
   * @return true si el RequirementsInfoMDTO esta presente en la lista
   */
  private boolean compararRequirementInfoWithList(final RequirementsInfoMDTO item,
      final List<RequirementsInfoMDTO> list, final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararRequirementInfo(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararRequirementInfo(item, i));
    }
  }

  /**
   * Compara dos RequirementsInfoMDTO sin actualizar la copia local
   * 
   * @param remoto RequirementsInfoMDTO remoto
   * @param local RequirementsInfoMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararRequirementInfo(final RequirementsInfoMDTO remoto, final RequirementsInfoMDTO local) {

    if (!Objects.equals(remoto.getType(), local.getType())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Type");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getDescripcion(), local.getDescripcion())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Descripcion");
      return Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getName(), local.getName())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Name");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      if (iiasService.compararContactDetails(remoto.getContactDetails(), local.getContactDetails(), Boolean.FALSE)) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ContactDetails");
        return Boolean.TRUE;
      }
    }

    return Boolean.FALSE;
  }

  /**
   * Compara dos listados de InformationItemMDTO y actuliza la copia local
   * 
   * @param remoto listado remoto
   * @param local listado local
   * @return true si hay diferencias entre los listados
   */
  @Override
  public boolean compararInformationItemList(final List<InformationItemMDTO> remoto,
      final List<InformationItemMDTO> local) {

    log.trace("Comienza la comparación de InformationItem");

    final List<InformationItemMDTO> borrar = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(local)) {
      borrar.addAll(local.stream().filter(i -> !compararInformationItemWithList(i, remoto, Boolean.FALSE))
          .collect(Collectors.toList()));
    }

    final List<InformationItemMDTO> incluir;
    if (CollectionUtils.isEmpty(local) && CollectionUtils.isNotEmpty(remoto)) {
      incluir = remoto;
    } else {
      incluir =
          CollectionUtils.isNotEmpty(remoto)
              ? remoto.stream().filter(i -> !compararInformationItemWithList(i, local, Boolean.TRUE))
                  .collect(Collectors.toList())
              : new ArrayList<>();
    }

    log.trace("se van a borrar los siguientes InformationItemMDTO {}", borrar.toString());
    local.removeAll(borrar);
    log.trace("se van a incluir los siguientes InformationItemMDTO {}", incluir.toString());
    local.addAll(incluir);

    log.trace("Fin de la comparación de InformationItem");
    return !(borrar.isEmpty() && incluir.isEmpty());
  }

  /**
   * Comprueba si un InformationItemMDTO esta en una lista de InformationItemMDTO
   *
   * @param item InformationItemMDTO
   * @param list listado de InformationItemMDTO
   * @param esItemLocal indica si el item es un InformationItemMDTO local.
   * @return true si el InformationItemMDTO esta presente en la lista
   */
  private boolean compararInformationItemWithList(final InformationItemMDTO item, final List<InformationItemMDTO> list,
      final Boolean esItemLocal) {
    if (Objects.isNull(list)) {
      return false;
    }
    if (esItemLocal) {
      return list.stream().anyMatch(i -> !compararInformationItem(i, item));
    } else {
      return list.stream().anyMatch(i -> !compararInformationItem(item, i));
    }
  }

  /**
   * Compara dos InformationItemMDTO sin actualizar la copia local
   * 
   * @param remoto InformationItemMDTO remoto
   * @param local InformationItemMDTO local
   * @return true si hay diferencias entre los objetos
   */
  public boolean compararInformationItem(final InformationItemMDTO remoto, final InformationItemMDTO local) {

    if (!Objects.equals(remoto.getType(), local.getType())) {
      // log.trace(HA_CAMBIADO_LA_PROPIEDAD, "Type");
      return Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getContactDetails())) {
      return iiasService.compararContactDetails(remoto.getContactDetails(), local.getContactDetails(), Boolean.FALSE);
    }

    return Boolean.FALSE;
  }

  @Override
  @Transactional(readOnly = false)
  public void eliminarMobilityByIdAndSendingHeiId(final String id, final String sendingHeiId) {
    omobilityDAO.eliminarMobilityByIdAndSendingHeiId(id, sendingHeiId);
  }

  @Override
  @Transactional(readOnly = true)
  public MobilityMDTO findLastMobilityRevisionByIdAndSendingHeiId(final String id, final String sendingHeiId) {
    return omobilityDAO.findLastRevisionByIdAndSendingInstitution(id, sendingHeiId);
  }

  @Override
  @Transactional(readOnly = true)
  public String findLastMobilityRevisionReceiverHeiByIdAndSendingHei(final String id, final String sendingHeiId) {
    return omobilityDAO.findLastMobilityRevisionReceiverHeiByIdAndSendingHei(id, sendingHeiId);
  }

  @Override
  public List<String> getIdsByCriteria(final String sendingHeiId, final List<String> receivingHeiId,
      final String receivingAcademicYearId, final String modifiedSince) {
    final Map<Object, Object> queryParams = new HashMap<>();

    this.addParam(queryParams, sendingHeiId, "sendingHeiId");
    this.addParam(queryParams, receivingHeiId, "receivingHeiId");
    this.addParam(queryParams, receivingAcademicYearId, "receivingAcademicYearId");
    this.addParam(queryParams, modifiedSince, "modifiedSince");

    return omobilityDAO.getIdsByCriteria(queryParams);
  }

  @Override
  public List<StudentMobilityForStudies> findStudentOmobilityByIdAndSendingInstitutionIdAndLastRevision(
      final String sendingHeiId, final List<String> receivingHeiIdList, final List<String> omobilityId)
      throws EwpConverterException {
    List<StudentMobilityForStudies> studentMobilityForStudies = new ArrayList<>();

    //dividimos la lista de heis en bloques de 1000 e invocamos al DAO para cada bloque
    final int MAX_LIST_SIZE = 1000;
    Collection<List<String>> partitionedList = IntStream.range(0, receivingHeiIdList.size())
            .boxed()
            .collect(Collectors.groupingBy(partition -> (partition / MAX_LIST_SIZE),
                    Collectors.mapping(receivingHeiIdList::get, Collectors.toList())))
            .values();

    final List<MobilityMDTO> mobilities = new ArrayList<>();
    for(List<String> heiIds : partitionedList){
      mobilities.addAll(omobilityDAO.obtenerMobilites(sendingHeiId, heiIds, omobilityId));
    }

    // convertimos a StudentMobilityForStudies
    if (!mobilities.isEmpty()) {
      studentMobilityForStudies = omobilityConverter.convertToStudentMobilitiesForStudies(mobilities);
    }

    return studentMobilityForStudies;
  }

  @Override
  public List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> findStudentImobilityByIdAndReceivingInstitutionId(
      final String receivingHeiId, final List<String> sendingHeiIdList, final List<String> omobilityId) {
    List<eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies> studentMobilityForStudies =
        new ArrayList<>();

    //dividimos la lista de heis en bloques de 1000 e invocamos al DAO para cada bloque
    final int MAX_LIST_SIZE = 1000;
    Collection<List<String>> partitionedList = IntStream.range(0, sendingHeiIdList.size())
            .boxed()
            .collect(Collectors.groupingBy(partition -> (partition / MAX_LIST_SIZE),
                    Collectors.mapping(sendingHeiIdList::get, Collectors.toList())))
            .values();

    // obtenemos las mobilites
    final List<MobilityMDTO> mobilities = new ArrayList<>();
    for(List<String> heiIds : partitionedList){
      mobilities.addAll(omobilityDAO
              .obtenerMobilitesByReceivingAndHeiRemoteIdAndLastRevision(receivingHeiId, heiIds, omobilityId));
    }

    // convertimos a StudentMobilityForStudies
    if (!mobilities.isEmpty()) {
      studentMobilityForStudies = omobilityConverter.convertToImobilityStudentMobilitiesForStudies(mobilities);
    }

    return studentMobilityForStudies;
  }

  /**
   * Como son imobilities nosotros somos el sender y el partner nos responde aprobando o rechazando la nominacion
   * @param mobilityForStudies
   * @return
   */
  @Override
  @Transactional(readOnly = false)
  public boolean actualizarImobility(
          final eu.erasmuswithoutpaper.api.imobilities.endpoints.StudentMobilityForStudies mobilityForStudies,
          final String ewpInstance) {
    boolean seActualiza = Boolean.FALSE;

    // convertimos la mobilidad a MobilityMDTO
    final MobilityMDTO remoto = omobilityConverter.convertToOmobilityMDTO(mobilityForStudies);

    // obtenemos la mobilidad de base de datos (como son imobilities el sending debemos ser nosotros)
    final MobilityMDTO local = findLastMobilityRevisionByIdAndSendingHeiId(remoto.getId(), ewpInstance);

    if (Objects.isNull(local) && Objects.nonNull(remoto)) {
      log.info("La movilidad no existe en nuestra BBDD, no realizamos accion");
    } else {
      if (Objects.nonNull(remoto)) {
        // comparamos los dos y actualizamos si es necesario
        final boolean hayCambios = compararIncomingMobilities(remoto, local);
        if (hayCambios) {
          log.info("Actualizando copia local");
          omobilityDAO.save(local);
          seActualiza = Boolean.TRUE;
        }
      }
    }

    return seActualiza;
  }

  /**
   * Compara dos (Incoming)MobilityMDTO
   *
   * @param remoto MobilityMDTO remoto
   * @param local MobilityMDTO local
   * @return true si hay cambios entre los dos ContactMDTO
   */
  public boolean compararIncomingMobilities(final MobilityMDTO remoto, final MobilityMDTO local) {
    boolean hayCambios = Boolean.FALSE;
    log.trace("Comienza el proceso de comparacion de mobilities");

    if (Utils.compararFechaYDias(remoto.getActualDepartureDate(), local.getActualDepartureDate())) {
      log.trace("Ha cambiado la propiedad {}", "ActualDepartureDate");
      local.setActualDepartureDate(remoto.getActualDepartureDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getActualArrivalDate(), local.getActualArrivalDate())) {
      log.trace("Ha cambiado la propiedad {}", "ActualArrivalDate");
      local.setActualArrivalDate(remoto.getActualArrivalDate());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getStatusIncomming(), local.getStatusIncomming())) {
      log.trace("Ha cambiado la propiedad {}", "StatusIncoming");
      local.setStatusIncomming(remoto.getStatusIncomming());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getComment(), local.getComment())) {
      log.trace("Ha cambiado la propiedad {}", "Comment");
      local.setComment(remoto.getComment());
      hayCambios = Boolean.TRUE;
    }

    log.trace("Fin del proceso de comparacion de mobilities");
    return hayCambios;
  }

  @Override
  @Transactional
  public boolean actualizarOmobility(final StudentMobilityForStudies mobilityForStudies, final MobilityMDTO local,
      final String ewpInstance) throws EwpConverterException {
    if (!participoEnMobility(mobilityForStudies, ewpInstance)) {
      log.info(" No participamos en la movilidad, no la volcamos");
      return Boolean.FALSE;
    }
    boolean seActualiza = Boolean.FALSE;

    final MobilityMDTO remoto = omobilityConverter.convertToOmobilityMDTO(mobilityForStudies);

    if(Objects.nonNull(remoto.getLearningAgreement()) && remoto.getLearningAgreement().stream().anyMatch(la ->
            LearningAgreementStatus.CHANGES_PROPOSED.equals(la.getStatus()) &&
                    Objects.nonNull(la.getStudentSign()) &&
                    Objects.nonNull(la.getSenderCoordinatorSign()) &&
                    Objects.nonNull(la.getReciverCoordinatorSign()))){
      log.info(" Se impide el volcado del LA debido a que están las firmas de Sender y Receiver.");
      throw new EwpConverterException(" The LA dump is prevented because both the Sender and Receiver signatures are present.");
    }

    log.info("La movilidad existe en nuestra BBDD, la comparamos con la que nos llega");
    final boolean hayCambios = compararOutgoingMobilities(remoto, local);
    if (hayCambios) {
      log.info("Actualizando copia local");
      omobilityDAO.save(local);
      seActualiza = Boolean.TRUE;
    }

    return seActualiza;
  }

  /**
   * Compara dos (Outgoing)MobilityMDTO
   *
   * @param remoto MobilityMDTO remoto
   * @param local MobilityMDTO local
   * @return true si hay cambios entre los dos ContactMDTO
   */
  public boolean compararOutgoingMobilities(final MobilityMDTO remoto, final MobilityMDTO local) {
    boolean hayCambios = Boolean.FALSE;
    log.trace("Comienza el proceso de comparacion de outgoing mobilities");

    if (Utils.compararFechaYDias(remoto.getPlannedDepartureDate(), local.getPlannedDepartureDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PlanedDepartureDate");
      local.setPlannedDepartureDate(remoto.getPlannedDepartureDate());
      hayCambios = Boolean.TRUE;
    }

    if (Utils.compararFechaYDias(remoto.getPlannedArrivalDate(), local.getPlannedArrivalDate())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "PlanedArribalDate");
      local.setPlannedArrivalDate(remoto.getPlannedArrivalDate());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getEqfLevelDeparture(), local.getEqfLevelDeparture())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EqfLevelDeparture");
      local.setEqfLevelDeparture(remoto.getEqfLevelDeparture());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getEqfLevelNomination(), local.getEqfLevelNomination())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "EqfLevelNomination");
      local.setEqfLevelNomination(remoto.getEqfLevelNomination());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getIscedCode(), local.getIscedCode())) {
      if (!Objects.equals(remoto.getIscedCode().getIscedCode(), local.getIscedCode().getIscedCode())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SubjectArea - IscedCode");
        local.getIscedCode().setIscedCode(remoto.getIscedCode().getIscedCode());
        hayCambios = Boolean.TRUE;
      }
    }

    if (!Objects.equals(remoto.getStatusOutgoing(), local.getStatusOutgoing())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "StatusOutgoing");
      local.setStatusOutgoing(remoto.getStatusOutgoing());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getMobilityParticipantId())) {
      boolean cambios = Boolean.FALSE;
      final MobilityParticipantMDTO participantRemoto =
          Objects.nonNull(remoto.getMobilityParticipantId()) ? remoto.getMobilityParticipantId()
              : new MobilityParticipantMDTO();
      final MobilityParticipantMDTO participantLocal =
          Objects.nonNull(local.getMobilityParticipantId()) ? local.getMobilityParticipantId()
              : new MobilityParticipantMDTO();

      if (!Objects.equals(participantRemoto.getGlobalId(), participantLocal.getGlobalId())) {
        log.trace(HA_CAMBIADO_LA_PROPIEDAD, "MobilityParticipant - GlobalId");
        participantLocal.setGlobalId(participantRemoto.getGlobalId());
        cambios = Boolean.TRUE;
      }
      final ContactMDTO localContact =
          Objects.nonNull(participantLocal.getContactDetails()) ? participantLocal.getContactDetails()
              : new ContactMDTO();
      final ContactMDTO remotoContact =
              Objects.nonNull(participantRemoto.getContactDetails()) ? participantRemoto.getContactDetails()
                      : new ContactMDTO();
      final boolean contactCompare =
          iiasService.compararContacts(remotoContact, localContact, Boolean.TRUE);
      if (contactCompare) {
        participantLocal.setContactDetails(localContact);
        cambios = cambios || contactCompare;
      }
      if (cambios) {
        local.setMobilityParticipantId(participantLocal);
      }

      hayCambios = hayCambios || cambios;
    }

    if (Objects.nonNull(remoto.getAcademicTerm())) {
      final AcademicTermMDTO localTerm = Objects.nonNull(local.getAcademicTerm()) ? local.getAcademicTerm() : new AcademicTermMDTO();
      final boolean compareRes = compararAcademicTerms(remoto.getAcademicTerm(), localTerm);
      if (compareRes) {
        local.setAcademicTerm(localTerm);
      }
      hayCambios = hayCambios || compareRes;
    }

    if (!Objects.equals(remoto.getSendingInstitutionId(), local.getSendingInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SendingInstitutionId");
      local.setSendingInstitutionId(remoto.getSendingInstitutionId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getSendingOrganizationUnitId(), local.getSendingOrganizationUnitId())) {
      // solo se interopera el id de la ounit del sending HEI
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "SendingOrganizationUnitId");
      local.setSendingOrganizationUnitId(remoto.getSendingOrganizationUnitId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getIiaId(), local.getIiaId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "IiaId");
      local.setIiaId(remoto.getIiaId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReceivingInstitutionId(), local.getReceivingInstitutionId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ReceivingInstitutionId");
      local.setReceivingInstitutionId(remoto.getReceivingInstitutionId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReceivingOrganizationUnitId(), local.getReceivingOrganizationUnitId())) {
      // solo se interopera el id de la ounit del receiving HEI
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ReceivingOrganizationUnitId");
      local.setReceivingOrganizationUnitId(remoto.getReceivingOrganizationUnitId());
      hayCambios = Boolean.TRUE;
    }

    if (!Objects.equals(remoto.getReceivingIiaId(), local.getReceivingIiaId())) {
      log.trace(HA_CAMBIADO_LA_PROPIEDAD, "ReceivingIiaId");
      local.setReceivingIiaId(remoto.getReceivingIiaId());
      hayCambios = Boolean.TRUE;
    }

    if (Objects.nonNull(remoto.getLanguageSkill())) {
      final boolean compareRes = compararLanguageSkillList(remoto.getLanguageSkill(), local.getLanguageSkill());
      hayCambios = hayCambios || compareRes;
    }

    return hayCambios;
  }

  private boolean participoEnMobility(final StudentMobilityForStudies mobility, final String ewpInstance) {
    if (Objects.isNull(ewpInstance) || Objects.isNull(mobility) || Objects.isNull(mobility.getReceivingHei())
        || Objects.isNull(mobility.getSendingHei())) {
      return Boolean.FALSE;
    }
    return ewpInstance.equalsIgnoreCase(mobility.getReceivingHei().getHeiId())
        || ewpInstance.equalsIgnoreCase(mobility.getSendingHei().getHeiId());
  }

  @Override
  @Transactional(readOnly = false)
  public Boolean guardarOmobilityCopiaRemota(final StudentMobilityForStudies remoto, final String ewpInstance) {
    if (!participoEnMobility(remoto, ewpInstance)) {
      log.info(" No participamos en la movilidad, no la volcamos");
      return Boolean.FALSE;
    }
    log.info(" No hay copia local, convertimos y persistimos la copia del Mobility remoto en local");
    final MobilityMDTO mobilityCopiaRemota = omobilityConverter.convertToOmobilityMDTO(remoto);
    omobilityDAO.save(mobilityCopiaRemota);
    return true;
  }

  @Override
  public List<String> findImobilitiesARefrescar(final String sendingHei) {
    return omobilityDAO.findImobilitiesARefrescar(sendingHei);
  }
}
