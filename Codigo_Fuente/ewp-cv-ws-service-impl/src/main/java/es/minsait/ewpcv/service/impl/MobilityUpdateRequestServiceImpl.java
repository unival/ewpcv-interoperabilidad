package es.minsait.ewpcv.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import es.minsait.ewpcv.converters.impl.CommonsConverter;
import es.minsait.ewpcv.error.EwpIllegalStateException;
import es.minsait.ewpcv.error.EwpNotFoundException;
import es.minsait.ewpcv.mapper.MobilityUpdateRequestMDTOMapper;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.model.omobility.UpdateRequestMobilityTypeEnum;
import es.minsait.ewpcv.repository.dao.ILearningAgreementDAO;
import es.minsait.ewpcv.repository.dao.IMobilityUpdateRequestDAO;
import es.minsait.ewpcv.repository.model.omobility.LearningAgreementMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestMDTO;
import es.minsait.ewpcv.repository.model.omobility.MobilityUpdateRequestType;
import es.minsait.ewpcv.service.api.IMobilityUpdateRequestService;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.OmobilityLasUpdateRequest;
import eu.erasmuswithoutpaper.api.omobilities.las.endpoints.Signature;
import lombok.extern.slf4j.Slf4j;

/**
 * The type Mobility update request service.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez
 *         (lsanchezpe@minsait.com)
 */
@Service
@Slf4j
public class MobilityUpdateRequestServiceImpl implements IMobilityUpdateRequestService {

  private IMobilityUpdateRequestDAO mobilityUpdateRequestDAO;
  private ILearningAgreementDAO learningAgreementDAO;
  private MobilityUpdateRequestMDTOMapper mobilityUpdateRequestMDTOMapper;

  @Autowired
  public MobilityUpdateRequestServiceImpl(IMobilityUpdateRequestDAO mobilityUpdateRequestDAO,
      ILearningAgreementDAO learningAgreementDAO, MobilityUpdateRequestMDTOMapper mobilityUpdateRequestMDTOMapper) {
    this.mobilityUpdateRequestDAO = mobilityUpdateRequestDAO;
    this.learningAgreementDAO = learningAgreementDAO;
    this.mobilityUpdateRequestMDTOMapper = mobilityUpdateRequestMDTOMapper;
  }

  @Override
  @Transactional
  public void updateRequestOutgoing(OmobilityLasUpdateRequest updateRequest, MobilityUpdateRequestType type)
      throws Exception {

    LearningAgreementMDTO la;

    if (MobilityUpdateRequestType.APPROVE_PROPOSAL_V1.equals(type)) {
      la = findLearningAgreement(updateRequest.getApproveProposalV1().getOmobilityId(),
          updateRequest.getApproveProposalV1().getChangesProposalId());
      la = updateLearningAgreementMDTOApproveProposal(la, updateRequest.getApproveProposalV1().getSignature());
    } else {
      la = findLearningAgreement(updateRequest.getCommentProposalV1().getOmobilityId(),
          updateRequest.getCommentProposalV1().getChangesProposalId());
      la = updateLearningAgreementMDTOCommentProposal(la, updateRequest.getCommentProposalV1().getSignature(),
          updateRequest.getCommentProposalV1().getComment());
    }
    learningAgreementDAO.saveLearningAgreement(la);
  }

  @Override
  @Transactional
  public List<MobilityUpdateRequestMDTO> reservarMobilityUpdateRequest(UpdateRequestMobilityTypeEnum tipo,
      int maxRetries, Integer blockSize) {
    List<MobilityUpdateRequestMDTO> mobilityUpdateRequestList =
        mobilityUpdateRequestDAO.findAllNotProcessingByMobilityType(tipo, maxRetries, blockSize);

    mobilityUpdateRequestDAO.changeStatusIsProcessingUpdateRequest(
        mobilityUpdateRequestList.stream().map(MobilityUpdateRequestMDTO::getId).collect(Collectors.toList()),
        Boolean.TRUE);
    return mobilityUpdateRequestList;
  }

  @Override
  @Transactional
  public void updateRequestIncoming(OmobilityLasUpdateRequest updateRequest, MobilityUpdateRequestType type)
      throws Exception {
    LearningAgreementMDTO la;

    if (MobilityUpdateRequestType.APPROVE_PROPOSAL_V1.equals(type)) {
      la = findLearningAgreement(updateRequest.getApproveProposalV1().getOmobilityId(),
          updateRequest.getApproveProposalV1().getChangesProposalId());
      la = updateLearningAgreementMDTOApproveProposal(la, updateRequest.getApproveProposalV1().getSignature());
    } else {
      la = findLearningAgreement(updateRequest.getCommentProposalV1().getOmobilityId(),
          updateRequest.getCommentProposalV1().getChangesProposalId());
      la = updateLearningAgreementMDTOCommentProposal(la, updateRequest.getCommentProposalV1().getSignature(),
          updateRequest.getCommentProposalV1().getComment());
    }
    learningAgreementDAO.saveLearningAgreement(la);
  }

  @Override
  @Transactional
  public void deleteMobillityUpdateRequest(String id) {
    mobilityUpdateRequestDAO.deleteMobillityUpdateRequest(id);
  }

  @Override
  @Transactional
  public void saveMobilityUpdateRequest(String sendingHeiId, byte[] requestUpdate,
      MobilityUpdateRequestType mobilityType) {

    mobilityUpdateRequestDAO
            .saveMobilityUpdateRequest(createMobilityUpdateRequestMDTO(sendingHeiId, requestUpdate, mobilityType));
  }

  @Override
  @Transactional
  public void updateShouldRetryLastError(String id,String errorMessage) {
    mobilityUpdateRequestDAO
            .updateShouldRetryLastError(id,errorMessage);
  }

  @Override
  @Transactional
  public void changeStatusIsProcessingUpdateRequest(List<String> updateRequestList, boolean isProcessing) {
    mobilityUpdateRequestDAO.changeStatusIsProcessingUpdateRequest(updateRequestList, isProcessing);
  }

  @Override
  @Transactional
  public void changeStatusIsNotProcessingAndRetriesUpdateRequest(List<String> updateRequestList) {
    mobilityUpdateRequestDAO.changeStatusIsNotProcessingAndRetriesUpdateRequest(updateRequestList);
  }

  private LearningAgreementMDTO updateLearningAgreementMDTOCommentProposal(LearningAgreementMDTO la,
      Signature signature, String comment) {
    la.setReciverCoordinatorSign(CommonsConverter.convertSignatureToSignatureMDTO(signature));
    la.setStatus(LearningAgreementStatus.REJECTED);
    la.setCommentReject(comment);
    la.setModifiedDate(new Date());
    return la;
  }

  private LearningAgreementMDTO updateLearningAgreementMDTOApproveProposal(LearningAgreementMDTO la,
      Signature signature) {

    la.setReciverCoordinatorSign(CommonsConverter.convertSignatureToSignatureMDTO(signature));
    la.setStatus(!learningAgreementDAO.hasFirstVersion(la.getId()) ? LearningAgreementStatus.FIRST_VERSION
        : LearningAgreementStatus.APROVED);
    la.setModifiedDate(new Date());
    return la;
  }

  private MobilityUpdateRequestMDTO createMobilityUpdateRequestMDTO(String sendingHeiId, byte[] requestUpdate,
      MobilityUpdateRequestType mobilityType) {
    MobilityUpdateRequestMDTO mobilityUpdateRequestMDTO = new MobilityUpdateRequestMDTO();
    mobilityUpdateRequestMDTO.setSendingHeiId(sendingHeiId);
    mobilityUpdateRequestMDTO.setUpdateInformation(requestUpdate);
    mobilityUpdateRequestMDTO.setType(mobilityType);
    mobilityUpdateRequestMDTO.setUpdateRequestDate(new Date());
    mobilityUpdateRequestMDTO.setShouldRetry(Boolean.TRUE);
    mobilityUpdateRequestMDTO.setMobilityType(UpdateRequestMobilityTypeEnum.OUTGOING);
    mobilityUpdateRequestMDTO.setRetries(0);
    return mobilityUpdateRequestMDTO;
  }

  private LearningAgreementMDTO findLearningAgreement(String omobilityId, String changesId) throws Exception {
    LearningAgreementMDTO la = learningAgreementDAO.findLearningAgreementByChangesId(omobilityId, changesId);
    if (Objects.isNull(la)) {
      throw new EwpNotFoundException("Didn't find any LA with the received changesId");
    } else if (!LearningAgreementStatus.CHANGES_PROPOSED.equals(la.getStatus())) {
      throw new EwpIllegalStateException("The changes_id are already approved or rejected");
    }
    return la;
  }

  @Override
  public <T> T bytesToClassImplByClass(byte[] pFicheroXml, Class<?> claseJava) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(claseJava);
    Unmarshaller un = context.createUnmarshaller();

    String ficheroXml = new String(pFicheroXml, Charset.forName("UTF-8"));
    StringBuffer ficheroXmlSB = new StringBuffer(ficheroXml);
    T res = (T) un.unmarshal(new StreamSource(new StringReader(ficheroXmlSB.toString())));
    return res;
  }

  @Override
  public void purgarUpdateRequests(Long maxProcessingTime) {
    List<MobilityUpdateRequestMDTO> requests =
        mobilityUpdateRequestDAO.obtenerRequestProcesandoseByTime(maxProcessingTime);

    log.debug("Se van a purgar {} update requests", requests.size());

    for (MobilityUpdateRequestMDTO request : requests) {
      request.setProcessing(Boolean.FALSE);

      mobilityUpdateRequestDAO.saveMobilityUpdateRequest(request);
    }
  }

}
