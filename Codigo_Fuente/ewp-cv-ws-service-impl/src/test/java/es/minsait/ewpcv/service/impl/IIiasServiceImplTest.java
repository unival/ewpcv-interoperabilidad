package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.repository.model.iia.CooperationConditionSubjAreaLangSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class IIiasServiceImplTest {

    @Test
    public void compareSubjAreaLangSkillSortByCefrLevel() {
        // No subject area to force sorting by language level

        CooperationConditionSubjAreaLangSkillMDTO subjAreaLangSkill1 = new CooperationConditionSubjAreaLangSkillMDTO();
        LanguageSkillMDTO langSkill1 = new LanguageSkillMDTO();
        langSkill1.setLanguage("en");
        langSkill1.setCefrLevel("B1");
        subjAreaLangSkill1.setLanguageSkill(langSkill1);

        CooperationConditionSubjAreaLangSkillMDTO subjAreaLangSkill2 = new CooperationConditionSubjAreaLangSkillMDTO();
        LanguageSkillMDTO langSkill2 = new LanguageSkillMDTO();
        langSkill2.setLanguage("en");
        langSkill2.setCefrLevel("A2");
        subjAreaLangSkill2.setLanguageSkill(langSkill2);

        assertTrue(subjAreaLangSkill1.compareTo(subjAreaLangSkill2) > 0);
    }
}
