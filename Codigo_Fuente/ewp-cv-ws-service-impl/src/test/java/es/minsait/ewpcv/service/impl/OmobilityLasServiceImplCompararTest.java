package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.converters.impl.OmobilityLasConverter;
import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.model.omobility.MobilityStatus;
import es.minsait.ewpcv.model.organization.FactSheetDateType;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import es.minsait.ewpcv.repository.impl.LearningAgreementDAO;
import es.minsait.ewpcv.repository.impl.OmobilityDAO;
import es.minsait.ewpcv.repository.impl.OrganizationUnitDAO;
import es.minsait.ewpcv.repository.model.course.AcademicTermMDTO;
import es.minsait.ewpcv.repository.model.course.AcademicYearMDTO;
import es.minsait.ewpcv.repository.model.omobility.*;
import es.minsait.ewpcv.repository.model.organization.*;
import es.minsait.ewpcv.service.api.ILearningAgreementService;
import es.minsait.ewpcv.service.api.INotificationService;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OmobilityLasServiceImplCompararTest extends TestCase {
    @Autowired
    private OmobilityDAO omobilityDAO;
    @Autowired
    private OmobilityLasConverter omobilityConverter;
    @Autowired
    private IIiaDAO iiaDAO;
    @Autowired
    private IIiaConverter iiaConverter;

    @Autowired
    private EventServiceImpl eventService;
    @Autowired
    private INotificationService notificationService;
    @Autowired
    private IIiaRegressionsDAO iiaRegressionsDAO;
    private IIiasServiceImpl iiasServiceImpl = new IIiasServiceImpl(iiaDAO, iiaConverter, eventService, notificationService, iiaRegressionsDAO);
    @Autowired
    private ILearningAgreementService laService;
    @Autowired
    private LearningAgreementDAO learningAgreementDAO;
    @Autowired
    private OrganizationUnitDAO organizationUnitDAO;

    private OmobilityLasServiceImpl omobilityLasService = new OmobilityLasServiceImpl(omobilityDAO,omobilityConverter,
                                                    iiasServiceImpl, laService, learningAgreementDAO, organizationUnitDAO);

    @Test
    public void testCompararRequirementInfo_SinCambios() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementInfo_SinCambiosType() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("TEST");
        remoto.setType("TEST");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosType() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("TEST");
        remoto.setType("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosType_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType(null);
        remoto.setType("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosType_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("TEST");
        remoto.setType(null);

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_SinCambiosDescripcion() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("TEST");
        remoto.setDescripcion("TEST");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosDescripcion() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("TEST");
        remoto.setDescripcion("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosDescripcion_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion(null);
        remoto.setDescripcion("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosDescripcion_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("TEST");
        remoto.setDescripcion(null);

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_SinCambiosName() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("TEST");
        remoto.setName("TEST");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosName() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("TEST");
        remoto.setName("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosName_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName(null);
        remoto.setName("TEST2");

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosName_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("TEST");
        remoto.setName(null);

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    private ContactDetailsMDTO generarContactDetailLocalTest(){
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        List<String> localLista = new ArrayList<>();
        localLista.add("Test");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        PhoneNumberMDTO phoneNumberMDTOLocal = new PhoneNumberMDTO();

        phoneNumberMDTOLocal.setE164("PRUEBA CAMBIO 1");
        phoneNumberMDTOLocal.setExtensionNumber("+34");
        phoneNumberMDTOLocal.setOtherFormat("us");

        FlexibleAddressMDTO flexibleAddressMDTOLocal = new FlexibleAddressMDTO();

        flexibleAddressMDTOLocal.setRecipientName(localLista);
        flexibleAddressMDTOLocal.setAddressLine(localLista);
        flexibleAddressMDTOLocal.setBuildingNumber("TEST");
        flexibleAddressMDTOLocal.setBuildingName("TEST");
        flexibleAddressMDTOLocal.setStreetName("TEST");
        flexibleAddressMDTOLocal.setUnit("TEST");
        flexibleAddressMDTOLocal.setFloor("TEST");
        flexibleAddressMDTOLocal.setPostOfficeBox("TEST");
        flexibleAddressMDTOLocal.setDeliveryPointCode(localLista);
        flexibleAddressMDTOLocal.setPostalCode("TEST");
        flexibleAddressMDTOLocal.setLocality("TEST");
        flexibleAddressMDTOLocal.setRegion("TEST");
        flexibleAddressMDTOLocal.setCountry("TEST");

        List<PhotoUrlMDTO> photoUrlMDTOListLocal = new ArrayList<>();
        PhotoUrlMDTO photoUrlMDTOLocal = new PhotoUrlMDTO();

        photoUrlMDTOLocal.setUrl("TEST");
        photoUrlMDTOLocal.setSize("TEST");
        photoUrlMDTOLocal.setDate(new Date());
        photoUrlMDTOListLocal.add(photoUrlMDTOLocal);

        local.setEmail(localLista);
        local.setUrl(languageItemListLocal);
        local.setPhoneNumber(phoneNumberMDTOLocal);
        local.setFaxNumber(phoneNumberMDTOLocal);
        local.setStreetAddress(flexibleAddressMDTOLocal);
        local.setMailingAddress(flexibleAddressMDTOLocal);
        local.setPhotoUrls(photoUrlMDTOListLocal);

        return local;
    }

    private ContactDetailsMDTO generarContactDetailRemotoTest(){
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        List<String> remotoLista = new ArrayList<>();
        remotoLista.add("Test2");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        PhoneNumberMDTO phoneNumberMDTORemoto = new PhoneNumberMDTO();

        phoneNumberMDTORemoto.setE164("PRUEBA CAMBIO 2");
        phoneNumberMDTORemoto.setExtensionNumber("+85");
        phoneNumberMDTORemoto.setOtherFormat("es");

        FlexibleAddressMDTO flexibleAddressMDTORemoto = new FlexibleAddressMDTO();

        flexibleAddressMDTORemoto.setRecipientName(remotoLista);
        flexibleAddressMDTORemoto.setAddressLine(remotoLista);
        flexibleAddressMDTORemoto.setBuildingNumber("TEST2");
        flexibleAddressMDTORemoto.setBuildingName("TEST2");
        flexibleAddressMDTORemoto.setStreetName("TEST2");
        flexibleAddressMDTORemoto.setUnit("TEST2");
        flexibleAddressMDTORemoto.setFloor("TEST2");
        flexibleAddressMDTORemoto.setPostOfficeBox("TEST2");
        flexibleAddressMDTORemoto.setDeliveryPointCode(remotoLista);
        flexibleAddressMDTORemoto.setPostalCode("TEST2");
        flexibleAddressMDTORemoto.setLocality("TEST2");
        flexibleAddressMDTORemoto.setRegion("TEST2");
        flexibleAddressMDTORemoto.setCountry("TEST2");

        List<PhotoUrlMDTO> photoUrlMDTOListRemoto = new ArrayList<>();
        PhotoUrlMDTO photoUrlMDTORemoto = new PhotoUrlMDTO();

        photoUrlMDTORemoto.setUrl("TEST2");
        photoUrlMDTORemoto.setSize("TEST2");
        photoUrlMDTORemoto.setDate(new Date(123));
        photoUrlMDTOListRemoto.add(photoUrlMDTORemoto);

        remoto.setEmail(remotoLista);
        remoto.setUrl(languageItemListRemoto);
        remoto.setPhoneNumber(phoneNumberMDTORemoto);
        remoto.setFaxNumber(phoneNumberMDTORemoto);
        remoto.setStreetAddress(flexibleAddressMDTORemoto);
        remoto.setMailingAddress(flexibleAddressMDTORemoto);
        remoto.setPhotoUrls(photoUrlMDTOListRemoto);

        return remoto;
    }

    @Test
    public void testCompararRequirementInfo_ConCambiosAll() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("TEST");
        local.setName("TEST");
        local.setDescripcion("TEST");
        local.setContactDetails(generarContactDetailLocalTest());

        remoto.setType("TEST2");
        remoto.setName("TEST2");
        remoto.setDescripcion("TEST2");
        remoto.setContactDetails(generarContactDetailRemotoTest());

        boolean resultado = omobilityLasService.compararRequirementInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambios() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosTotalTerms() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(BigInteger.ONE);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTotalTerms() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(BigInteger.TEN);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTotalTerms_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTotalTerms(null);
        remoto.setTotalTerms(BigInteger.TEN);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTotalTerms_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosTermNumber() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(BigInteger.ONE);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTermNumber() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(BigInteger.TEN);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTermNumber_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTermNumber(null);
        remoto.setTermNumber(BigInteger.TEN);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosTermNumber_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosStartDate() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date());

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosStartDate() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date(123));

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosStartDate_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setStartDate(null);
        remoto.setStartDate(new Date(123));

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosStartDate_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosEndDate() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date());

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosEndDate() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date(123));

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosEndDate_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setEndDate(null);
        remoto.setEndDate(new Date(123));

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosEndDate_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosInstitutionId() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId("TEST");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosInstitutionId() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosInstitutionId_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setInstitutionId(null);
        remoto.setInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosInstitutionId_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosOrganizationUnitId() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId("TEST");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosOrganizationUnitId() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosOrganizationUnitId_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setOrganizationUnitId(null);
        remoto.setOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosOrganizationUnitId_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId(null);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosAcademicYearStartYear() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setStartYear("TEST");
        yearRemoto.setStartYear("TEST");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearStartYear() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setStartYear("TEST");
        yearRemoto.setStartYear("TEST2");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearStartYear_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setStartYear(null);
        yearRemoto.setStartYear("TEST");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearStartYear_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setStartYear("TEST");
        yearRemoto.setStartYear(null);

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_SinCambiosAcademicYearEndYear() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setEndYear("TEST");
        yearRemoto.setEndYear("TEST");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearEndYear() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setEndYear("TEST");
        yearRemoto.setEndYear("TEST2");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearEndYear_NullLocal() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setEndYear(null);
        yearRemoto.setEndYear("TEST");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearEndYear_NullRemoto() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setEndYear("TEST");
        yearRemoto.setEndYear(null);

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerms_ConCambiosAcademicYearAll() {
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicTermMDTO remoto = new AcademicTermMDTO();

        AcademicYearMDTO yearLocal = new AcademicYearMDTO();
        AcademicYearMDTO yearRemoto = new AcademicYearMDTO();

        yearLocal.setStartYear("TEST");
        yearLocal.setEndYear("TEST");
        yearRemoto.setStartYear("TEST2");
        yearRemoto.setEndYear("TEST2");

        local.setAcademicYear(yearLocal);
        remoto.setAcademicYear(yearRemoto);

        boolean resultado = omobilityLasService.compararAcademicTerms(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkill_SinCambios() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkill_SinCambiosLanguage() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setLanguage("TEST");
        remoto.setLanguage("TEST");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosLanguage() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setLanguage("TEST");
        remoto.setLanguage("TEST2");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLanguage(), local.getLanguage());
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosLanguage_NullLocal() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setLanguage(null);
        remoto.setLanguage("TEST");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLanguage(), local.getLanguage());
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosLanguage_NullRemoto() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setLanguage("TEST");
        remoto.setLanguage(null);

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLanguage(), local.getLanguage());
    }

    @Test
    public void testCompararLanguageSkill_SinCambiosCefrLevel() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setCefrLevel("TEST");
        remoto.setCefrLevel("TEST");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosCefrLevel() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setCefrLevel("TEST");
        remoto.setCefrLevel("TEST2");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCefrLevel(), local.getCefrLevel());
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosCefrLevel_NullLocal() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setCefrLevel(null);
        remoto.setCefrLevel("TEST");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCefrLevel(), local.getCefrLevel());
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosCefrLevel_NullRemoto() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setCefrLevel("TEST");
        remoto.setCefrLevel(null);

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCefrLevel(), local.getCefrLevel());
    }

    @Test
    public void testCompararLanguageSkill_ConCambiosAll() {
        LanguageSkillMDTO local = new LanguageSkillMDTO();
        LanguageSkillMDTO remoto = new LanguageSkillMDTO();
        boolean actualizarLocal = Boolean.TRUE;

        local.setLanguage("TEST");
        local.setCefrLevel("TEST");

        remoto.setLanguage("TEST2");
        remoto.setCefrLevel("TEST2");

        boolean resultado = omobilityLasService.compararLanguageSkill(remoto, local, actualizarLocal);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_SinCambios() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararInformationItem_SinCambiosType() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("TEST");
        remoto.setType("TEST");

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("TEST");
        remoto.setType("TEST2");

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType_NullLocal() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType(null);
        remoto.setType("TEST2");

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType_NullRemoto() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("TEST");
        remoto.setType(null);

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosAll() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("TEST");
        local.setContactDetails(generarContactDetailLocalTest());
        remoto.setType("TEST2");
        remoto.setContactDetails(generarContactDetailRemotoTest());

        boolean resultado = omobilityLasService.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambios() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosDecisionWeeksLimit() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setDecisionWeeksLimit(1);
        remoto.setDecisionWeeksLimit(1);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosDecisionWeeksLimit() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setDecisionWeeksLimit(1);
        remoto.setDecisionWeeksLimit(2);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosDecisionWeeksLimit_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setDecisionWeeksLimit(null);
        remoto.setDecisionWeeksLimit(2);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosDecisionWeeksLimit_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setDecisionWeeksLimit(1);
        remoto.setDecisionWeeksLimit(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosTorWeeksLimit() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setTorWeeksLimit(1);
        remoto.setTorWeeksLimit(1);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosTorWeeksLimit() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setTorWeeksLimit(1);
        remoto.setTorWeeksLimit(2);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosTorWeeksLimit_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setTorWeeksLimit(null);
        remoto.setTorWeeksLimit(2);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosTorWeeksLimit_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setTorWeeksLimit(1);
        remoto.setTorWeeksLimit(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosApplicationSpringTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationSpringTerm(new Date());
        remoto.setApplicationSpringTerm(new Date());

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationSpringTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationSpringTerm(new Date());
        remoto.setApplicationSpringTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationSpringTerm_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationSpringTerm(null);
        remoto.setApplicationSpringTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationSpringTerm_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationSpringTerm(new Date());
        remoto.setApplicationSpringTerm(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosApplicationAutumnTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationAutumnTerm(new Date());
        remoto.setApplicationAutumnTerm(new Date());

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationAutumnTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationAutumnTerm(new Date());
        remoto.setApplicationAutumnTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationAutumnTerm_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationAutumnTerm(null);
        remoto.setApplicationAutumnTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosApplicationAutumnTerm_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setApplicationAutumnTerm(new Date());
        remoto.setApplicationAutumnTerm(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosNominationsSpringTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsSpringTerm(new Date());
        remoto.setNominationsSpringTerm(new Date());

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsSpringTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsSpringTerm(new Date());
        remoto.setNominationsSpringTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsSpringTerm_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsSpringTerm(null);
        remoto.setNominationsSpringTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsSpringTerm_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsSpringTerm(new Date());
        remoto.setNominationsSpringTerm(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_SinCambiosNominationsAutumnTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsAutumnTerm(new Date());
        remoto.setNominationsAutumnTerm(new Date());

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsAutumnTerm() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsAutumnTerm(new Date());
        remoto.setNominationsAutumnTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsAutumnTerm_NullLocal() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsAutumnTerm(null);
        remoto.setNominationsAutumnTerm(new Date(123));

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsAutumnTerm_NullRemoto() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setNominationsAutumnTerm(new Date());
        remoto.setNominationsAutumnTerm(null);

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheet_ConCambiosNominationsAll() {
        FactSheetMDTO local = new FactSheetMDTO();
        FactSheetMDTO remoto = new FactSheetMDTO();

        local.setDecisionWeeksLimit(1);
        local.setTorWeeksLimit(1);
        local.setApplicationSpringTerm(new Date());
        local.setApplicationAutumnTerm(new Date());
        local.setNominationsSpringTerm(new Date());
        local.setNominationsAutumnTerm(new Date());
        local.setContactDetails(generarContactDetailLocalTest());

        remoto.setDecisionWeeksLimit(2);
        remoto.setTorWeeksLimit(2);
        remoto.setApplicationSpringTerm(new Date(123));
        remoto.setApplicationAutumnTerm(new Date(123));
        remoto.setNominationsSpringTerm(new Date(123));
        remoto.setNominationsAutumnTerm(new Date(123));
        remoto.setContactDetails(generarContactDetailRemotoTest());

        boolean resultado = omobilityLasService.compararFactSheet(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheetDates_SinCambios() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheetDates_SinCambiosType() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        local.setType(FactSheetDateType.APPLICATIONS_AUTUMN);
        remoto.setType(FactSheetDateType.APPLICATIONS_AUTUMN);

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFactSheetDates_ConCambiosType() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        local.setType(FactSheetDateType.APPLICATIONS_AUTUMN);
        remoto.setType(FactSheetDateType.APPLICATIONS_SPRING);

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheetDates_ConCambiosType_NullLocal() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        local.setType(null);
        remoto.setType(FactSheetDateType.APPLICATIONS_SPRING);

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheetDates_ConCambiosType_NullRemoto() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        local.setType(FactSheetDateType.APPLICATIONS_AUTUMN);
        remoto.setType(null);

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFactSheetDates_ConCambiosAll() {
        FactSheetDateMDTO local = new FactSheetDateMDTO();
        FactSheetDateMDTO remoto = new FactSheetDateMDTO();

        AcademicTermMDTO academicTermMDTOLocal = new AcademicTermMDTO();
        AcademicTermMDTO academicTermMDTORemoto = new AcademicTermMDTO();

        academicTermMDTOLocal.setInstitutionId("TEST");
        academicTermMDTOLocal.setOrganizationUnitId("TEST");
        academicTermMDTOLocal.setStartDate(new Date());
        academicTermMDTOLocal.setEndDate(new Date());
        academicTermMDTOLocal.setTotalTerms(BigInteger.ONE);
        academicTermMDTOLocal.setTermNumber(BigInteger.ONE);

        academicTermMDTORemoto.setInstitutionId("TEST2");
        academicTermMDTORemoto.setOrganizationUnitId("TEST2");
        academicTermMDTORemoto.setStartDate(new Date(123));
        academicTermMDTORemoto.setEndDate(new Date(123));
        academicTermMDTORemoto.setTotalTerms(BigInteger.TEN);
        academicTermMDTORemoto.setTermNumber(BigInteger.TEN);

        local.setType(FactSheetDateType.APPLICATIONS_AUTUMN);
        local.setAcademicTerm(academicTermMDTOLocal);
        remoto.setType(FactSheetDateType.APPLICATIONS_SPRING);
        remoto.setAcademicTerm(academicTermMDTORemoto);

        boolean resultado = omobilityLasService.compararFactsheetDates(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_SinCambios() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_SinCambiosType() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setAbbreviation("TEST");
        remoto.setAbbreviation("TEST");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosType() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setAbbreviation("TEST");
        remoto.setAbbreviation("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getAbbreviation(), local.getAbbreviation());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosType_NullLocal() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setAbbreviation(null);
        remoto.setAbbreviation("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getAbbreviation(), local.getAbbreviation());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosType_NullRemoto() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setAbbreviation("TEST");
        remoto.setAbbreviation(null);

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getAbbreviation(), local.getAbbreviation());
    }

    @Test
    public void testCompararOrganizationUnits_SinCambiosLogoUrl() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setLogoUrl("TEST");
        remoto.setLogoUrl("TEST");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosLogoUrl() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setLogoUrl("TEST");
        remoto.setLogoUrl("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLogoUrl(), local.getLogoUrl());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosLogoUrl_NullLocal() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setLogoUrl(null);
        remoto.setLogoUrl("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLogoUrl(), local.getLogoUrl());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosLogoUrl_NullRemoto() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setLogoUrl("TEST");
        remoto.setLogoUrl(null);

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLogoUrl(), local.getLogoUrl());
    }

    @Test
    public void testCompararOrganizationUnits_SinCambiosOrganizationUnitCode() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setOrganizationUnitCode("TEST");
        remoto.setOrganizationUnitCode("TEST");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosOrganizationUnitCode() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setOrganizationUnitCode("TEST");
        remoto.setOrganizationUnitCode("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getOrganizationUnitCode(), local.getOrganizationUnitCode());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosOrganizationUnitCode_NullLocal() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setOrganizationUnitCode(null);
        remoto.setOrganizationUnitCode("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getOrganizationUnitCode(), local.getOrganizationUnitCode());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosOrganizationUnitCode_NullRemoto() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setOrganizationUnitCode("TEST");
        remoto.setOrganizationUnitCode(null);

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getOrganizationUnitCode(), local.getOrganizationUnitCode());
    }

    @Test
    public void testCompararOrganizationUnits_SinCambiosParentOUnitId() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setParentOUnitId("TEST");
        remoto.setParentOUnitId("TEST");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosParentOUnitId() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setParentOUnitId("TEST");
        remoto.setParentOUnitId("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getParentOUnitId(), local.getParentOUnitId());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosParentOUnitId_NullLocal() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setParentOUnitId(null);
        remoto.setParentOUnitId("TEST2");

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getParentOUnitId(), local.getParentOUnitId());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosParentOUnitId_NullRemoto() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        local.setParentOUnitId("TEST");
        remoto.setParentOUnitId(null);

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
        assertEquals(remoto.getParentOUnitId(), local.getParentOUnitId());
    }

    @Test
    public void testCompararOrganizationUnits_ConCambiosAll() {
        OrganizationUnitMDTO local = new OrganizationUnitMDTO();
        OrganizationUnitMDTO remoto = new OrganizationUnitMDTO();
        boolean actualizaLocal = Boolean.TRUE;

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        List<InformationItemMDTO> informationItemListLocal = new ArrayList<>();
        List<InformationItemMDTO> informationItemListRemoto = new ArrayList<>();

        InformationItemMDTO informationItemLocal = new InformationItemMDTO();
        InformationItemMDTO informationItemRemoto = new InformationItemMDTO();

        informationItemLocal.setType("TEST");
        informationItemLocal.setContactDetails(generarContactDetailLocalTest());
        informationItemRemoto.setType("TEST2");
        informationItemRemoto.setContactDetails(generarContactDetailRemotoTest());

        informationItemListLocal.add(informationItemLocal);
        informationItemListRemoto.add(informationItemRemoto);

        List<RequirementsInfoMDTO> requirementsInfoListLocal = new ArrayList<>();
        List<RequirementsInfoMDTO> requirementsInfoListRemoto = new ArrayList<>();

        RequirementsInfoMDTO requirementsInfoLocal = new RequirementsInfoMDTO();
        RequirementsInfoMDTO requirementsInfoRemoto = new RequirementsInfoMDTO();

        requirementsInfoLocal.setType("TEST");
        requirementsInfoLocal.setName("TEST");
        requirementsInfoLocal.setDescripcion("TEST");
        requirementsInfoLocal.setContactDetails(generarContactDetailLocalTest());

        requirementsInfoRemoto.setType("TEST2");
        requirementsInfoRemoto.setName("TEST2");
        requirementsInfoRemoto.setDescripcion("TEST2");
        requirementsInfoRemoto.setContactDetails(generarContactDetailRemotoTest());

        requirementsInfoListLocal.add(requirementsInfoLocal);
        requirementsInfoListRemoto.add(requirementsInfoRemoto);

        FactSheetMDTO factSheetLocal = new FactSheetMDTO();
        FactSheetMDTO factSheetRemoto = new FactSheetMDTO();

        factSheetLocal.setDecisionWeeksLimit(1);
        factSheetLocal.setTorWeeksLimit(1);
        factSheetLocal.setApplicationSpringTerm(new Date());
        factSheetLocal.setApplicationAutumnTerm(new Date());
        factSheetLocal.setNominationsSpringTerm(new Date());
        factSheetLocal.setNominationsAutumnTerm(new Date());
        factSheetLocal.setContactDetails(generarContactDetailLocalTest());

        factSheetRemoto.setDecisionWeeksLimit(2);
        factSheetRemoto.setTorWeeksLimit(2);
        factSheetRemoto.setApplicationSpringTerm(new Date(123));
        factSheetRemoto.setApplicationAutumnTerm(new Date(123));
        factSheetRemoto.setNominationsSpringTerm(new Date(123));
        factSheetRemoto.setNominationsAutumnTerm(new Date(123));
        factSheetRemoto.setContactDetails(generarContactDetailRemotoTest());

        local.setAbbreviation("TEST");
        local.setLogoUrl("TEST");
        local.setOrganizationUnitCode("TEST");
        local.setParentOUnitId("TEST");
        local.setName(languageItemListLocal);
        local.setInformationItems(informationItemListLocal);
        local.setAdditionalRequirements(requirementsInfoListLocal);
        local.setFactSheet(factSheetLocal);

        remoto.setAbbreviation("TEST2");
        remoto.setLogoUrl("TEST2");
        remoto.setOrganizationUnitCode("TEST2");
        remoto.setParentOUnitId("TEST2");
        remoto.setName(languageItemListRemoto);
        remoto.setInformationItems(informationItemListRemoto);
        remoto.setAdditionalRequirements(requirementsInfoListRemoto);
        remoto.setFactSheet(factSheetRemoto);

        boolean resultado = omobilityLasService.compararOrganizationUnits(remoto, local, actualizaLocal);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_SinCambios() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_SinCambiosActualDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(new Date());

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualDepartureDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(null);
        remoto.setActualDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualDepartureDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(null);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_SinCambiosActualArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(new Date());

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualArrivalDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(null);
        remoto.setActualArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosActualArrivalDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(null);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_SinCambiosStatusIncomming() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(MobilityStatus.LIVE);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosStatusIncomming() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(MobilityStatus.REJECTED);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosStatusIncomming_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(null);
        remoto.setStatusIncomming(MobilityStatus.REJECTED);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosStatusIncomming_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(null);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_SinCambiosComment() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setComment("TEST");
        remoto.setComment("TEST");

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosComment() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setComment("TEST");
        remoto.setComment("TEST2");

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosComment_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setComment(null);
        remoto.setComment("TEST2");

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosComment_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setComment("TEST");
        remoto.setComment(null);

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIncomingMobilities_ConCambiosAll() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        local.setActualArrivalDate(new Date());
        local.setStatusIncomming(MobilityStatus.LIVE);
        local.setComment("TEST");

        remoto.setActualDepartureDate(new Date(123));
        remoto.setActualArrivalDate(new Date(123));
        remoto.setStatusIncomming(MobilityStatus.REJECTED);
        remoto.setComment("TEST2");

        boolean resultado = omobilityLasService.compararIncomingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambios() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }
    @Test
    public void testCompararOutgoingMobilities_SinCambiosPlannedDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(new Date());

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedDepartureDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(null);
        remoto.setPlannedDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedDepartureDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosPlannedPlannedArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(new Date());

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedPlannedArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedPlannedArrivalDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(null);
        remoto.setPlannedArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosPlannedPlannedArrivalDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosEqfLevelDeparture() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture((long)1);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosEqfLevelDeparture() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture((long)2);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosEqfLevelDeparture_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture(null);
        remoto.setEqfLevelDeparture((long)2);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosEqfLevelDeparture_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosStatusOutgoing() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(MobilityStatus.LIVE);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosStatusOutgoing() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(MobilityStatus.CANCELLED);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosStatusOutgoing_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(null);
        remoto.setStatusOutgoing(MobilityStatus.CANCELLED);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosStatusOutgoing_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosSendingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingInstitutionId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId(null);
        remoto.setSendingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingInstitutionId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosSendingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingOrganizationUnitId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId(null);
        remoto.setSendingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosSendingOrganizationUnitId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosIiaId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setIiaId("TEST");
        remoto.setIiaId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosIiaId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setIiaId("TEST");
        remoto.setIiaId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosIiaId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setIiaId(null);
        remoto.setIiaId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosIiaId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setIiaId("TEST");
        remoto.setIiaId(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosReceivingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosIiaIdReceivingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosReceivingInstitutionId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId(null);
        remoto.setReceivingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosReceivingInstitutionId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosReceivingIiaId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingIiaId("TEST");
        remoto.setReceivingIiaId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosReceivingIiaId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingIiaId("TEST");
        remoto.setReceivingIiaId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosReceivingIiaId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingIiaId(null);
        remoto.setReceivingIiaId("TEST2");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosReceivingIiaId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingIiaId("TEST");
        remoto.setReceivingIiaId(null);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_SinCambiosMobilityParticipantId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId("TEST");
        mobilityParticipantRemoto.setGlobalId("TEST");

        local.setMobilityParticipantId(mobilityParticipantLocal);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosMobilityParticipantId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId("TEST");
        mobilityParticipantRemoto.setGlobalId("TEST2");

        local.setMobilityParticipantId(mobilityParticipantLocal);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosMobilityParticipantId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId(null);
        mobilityParticipantRemoto.setGlobalId("TEST2");

        local.setMobilityParticipantId(mobilityParticipantLocal);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosMobilityParticipantId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId("TEST");
        mobilityParticipantRemoto.setGlobalId(null);

        local.setMobilityParticipantId(mobilityParticipantLocal);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararOutgoingMobilities_ConCambiosAll() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId("TEST");
        mobilityParticipantRemoto.setGlobalId("TEST2");

        AcademicTermMDTO academicTermMDTOLocal = new AcademicTermMDTO();
        AcademicTermMDTO academicTermMDTORemoto = new AcademicTermMDTO();

        academicTermMDTOLocal.setInstitutionId("TEST");
        academicTermMDTOLocal.setOrganizationUnitId("TEST");
        academicTermMDTOLocal.setStartDate(new Date());
        academicTermMDTOLocal.setEndDate(new Date());
        academicTermMDTOLocal.setTotalTerms(BigInteger.ONE);
        academicTermMDTOLocal.setTermNumber(BigInteger.ONE);

        academicTermMDTORemoto.setInstitutionId("TEST2");
        academicTermMDTORemoto.setOrganizationUnitId("TEST2");
        academicTermMDTORemoto.setStartDate(new Date(123));
        academicTermMDTORemoto.setEndDate(new Date(123));
        academicTermMDTORemoto.setTotalTerms(BigInteger.TEN);
        academicTermMDTORemoto.setTermNumber(BigInteger.TEN);

        List<LanguageSkillMDTO> languageSkillListLocal = new ArrayList<>();
        List<LanguageSkillMDTO> languageSkillListRemoto = new ArrayList<>();

        LanguageSkillMDTO languageSkillLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillRemoto = new LanguageSkillMDTO();

        languageSkillLocal.setLanguage("TEST");
        languageSkillLocal.setCefrLevel("TEST");

        languageSkillRemoto.setLanguage("TEST2");
        languageSkillRemoto.setCefrLevel("TEST2");

        languageSkillListLocal.add(languageSkillLocal);
        languageSkillListRemoto.add(languageSkillRemoto);

        SubjectAreaMDTO subjectAreaLocal = new SubjectAreaMDTO();
        subjectAreaLocal.setIscedCode("TEST");
        subjectAreaLocal.setIscedClarification("TEST");

        SubjectAreaMDTO subjectAreaRemoto = new SubjectAreaMDTO();
        subjectAreaRemoto.setIscedCode("TEST2");
        subjectAreaRemoto.setIscedClarification("TEST2");

        local.setPlannedDepartureDate(new Date());
        local.setPlannedArrivalDate(new Date());
        local.setEqfLevelNomination((long)1);
        local.setEqfLevelDeparture((long)1);
        local.setIscedCode(subjectAreaLocal);
        local.setStatusOutgoing(MobilityStatus.LIVE);
        local.setAcademicTerm(academicTermMDTOLocal);
        local.setSendingInstitutionId("TEST");
        local.setSendingOrganizationUnitId("TEST");
        local.setIiaId("TEST");
        local.setReceivingInstitutionId("TEST");
        local.setReceivingOrganizationUnitId("TEST");
        local.setReceivingIiaId("TEST");
        local.setLanguageSkill(languageSkillListLocal);
        local.setMobilityParticipantId(mobilityParticipantLocal);

        remoto.setPlannedDepartureDate(new Date(123));
        remoto.setPlannedArrivalDate(new Date(123));
        remoto.setEqfLevelNomination((long)2);
        remoto.setEqfLevelDeparture((long)2);
        remoto.setIscedCode(subjectAreaRemoto);
        remoto.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setAcademicTerm(academicTermMDTORemoto);
        remoto.setSendingInstitutionId("TEST2");
        remoto.setSendingOrganizationUnitId("TEST2");
        remoto.setIiaId("TEST2");
        remoto.setReceivingInstitutionId("TEST2");
        remoto.setReceivingOrganizationUnitId("TEST2");
        remoto.setReceivingIiaId("TEST2");
        remoto.setLanguageSkill(languageSkillListRemoto);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosActualArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(new Date());

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualArrivalDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(null);
        remoto.setActualArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualArrivalDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualArrivalDate(new Date());
        remoto.setActualArrivalDate(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosActualDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(new Date());

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualDepartureDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(null);
        remoto.setActualDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosActualDepartureDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setActualDepartureDate(new Date());
        remoto.setActualDepartureDate(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosPlannedPlannedArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(new Date());

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedPlannedArrivalDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedPlannedArrivalDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(null);
        remoto.setPlannedArrivalDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedPlannedArrivalDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedArrivalDate(new Date());
        remoto.setPlannedArrivalDate(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosPlannedDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(new Date());

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedDepartureDate() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedDepartureDate_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(null);
        remoto.setPlannedDepartureDate(new Date(123));

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPlannedDepartureDate_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setPlannedDepartureDate(new Date());
        remoto.setPlannedDepartureDate(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosEqfLevelDeparture() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture((long)1);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelDeparture() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture((long)2);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelDeparture_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture(null);
        remoto.setEqfLevelDeparture((long)2);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelDeparture_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelDeparture((long)1);
        remoto.setEqfLevelDeparture(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosEqfLevelNomination() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelNomination((long)1);
        remoto.setEqfLevelNomination((long)1);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelNomination() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelNomination((long)1);
        remoto.setEqfLevelNomination((long)2);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelNomination_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelNomination(null);
        remoto.setEqfLevelNomination((long)2);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosEqfLevelNomination_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setEqfLevelNomination((long)1);
        remoto.setEqfLevelNomination(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosStatusOutgoing() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(MobilityStatus.LIVE);

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusOutgoing() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(MobilityStatus.CANCELLED);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusOutgoing_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(null);
        remoto.setStatusOutgoing(MobilityStatus.CANCELLED);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusOutgoing_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusOutgoing(MobilityStatus.LIVE);
        remoto.setStatusOutgoing(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosStatusIncomming() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(MobilityStatus.LIVE);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusIncomming() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(MobilityStatus.REJECTED);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusIncomming_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(null);
        remoto.setStatusIncomming(MobilityStatus.REJECTED);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatusIncomming_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setStatusIncomming(MobilityStatus.LIVE);
        remoto.setStatusIncomming(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosReceivingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosIiaIdReceivingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosReceivingInstitutionId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId(null);
        remoto.setReceivingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosReceivingInstitutionId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingInstitutionId("TEST");
        remoto.setReceivingInstitutionId(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosSendingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId("TEST");

        boolean resultado = omobilityLasService.compararOutgoingMobilities(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingInstitutionId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingInstitutionId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId(null);
        remoto.setSendingInstitutionId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingInstitutionId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingInstitutionId("TEST");
        remoto.setSendingInstitutionId(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosMobilityType() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeLocal = new MobilityType();
        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeLocal.setMobilityGroup("TEST");
        mobilityTypeLocal.setMobilityCategory("TEST");

        mobilityTypeRemoto.setMobilityGroup("TEST");
        mobilityTypeRemoto.setMobilityCategory("TEST");

        local.setMobilityType(mobilityTypeLocal);
        remoto.setMobilityType(mobilityTypeRemoto);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosMobilityType() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeLocal = new MobilityType();
        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeLocal.setMobilityGroup("TEST");
        mobilityTypeLocal.setMobilityCategory("TEST");

        mobilityTypeRemoto.setMobilityGroup("TEST2");
        mobilityTypeRemoto.setMobilityCategory("TEST2");

        local.setMobilityType(mobilityTypeLocal);
        remoto.setMobilityType(mobilityTypeRemoto);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosMobilityType_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeLocal = new MobilityType();
        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeLocal.setMobilityGroup(null);
        mobilityTypeLocal.setMobilityCategory(null);

        mobilityTypeRemoto.setMobilityGroup("TEST2");
        mobilityTypeRemoto.setMobilityCategory("TEST2");

        local.setMobilityType(mobilityTypeLocal);
        remoto.setMobilityType(mobilityTypeRemoto);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosMobilityType_NullLocal2() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeRemoto.setMobilityGroup("TEST2");
        mobilityTypeRemoto.setMobilityCategory("TEST2");

        local.setMobilityType(null);
        remoto.setMobilityType(mobilityTypeRemoto);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosMobilityType_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeLocal = new MobilityType();
        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeLocal.setMobilityGroup("TEST");
        mobilityTypeLocal.setMobilityCategory("TEST");
        mobilityTypeRemoto.setMobilityGroup(null);
        mobilityTypeRemoto.setMobilityCategory(null);

        local.setMobilityType(mobilityTypeLocal);
        remoto.setMobilityType(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosMobilityType_NullRemoto2() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        MobilityType mobilityTypeLocal = new MobilityType();

        mobilityTypeLocal.setMobilityGroup("TEST2");
        mobilityTypeLocal.setMobilityCategory("TEST2");

        local.setMobilityType(mobilityTypeLocal);
        remoto.setMobilityType(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosReceivingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingOrganizationUnitId("TEST");
        remoto.setReceivingOrganizationUnitId("TEST");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosReceivingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingOrganizationUnitId("TEST");
        remoto.setReceivingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosReceivingOrganizationUnitId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingOrganizationUnitId(null);
        remoto.setReceivingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosReceivingOrganizationUnitId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setReceivingOrganizationUnitId("TEST");
        remoto.setReceivingOrganizationUnitId(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosSendingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId("TEST");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingOrganizationUnitId() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingOrganizationUnitId_NullLocal() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId(null);
        remoto.setSendingOrganizationUnitId("TEST2");

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosSendingOrganizationUnitId_NullRemoto() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        local.setSendingOrganizationUnitId("TEST");
        remoto.setSendingOrganizationUnitId(null);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    public ContactMDTO generarContactLocalTest(){
        ContactMDTO local = new ContactMDTO();

        PersonMDTO personMDTOLocal = new PersonMDTO();

        personMDTOLocal.setBirthDate(new Date());
        personMDTOLocal.setCountryCode("TEST");
        personMDTOLocal.setFirstNames("TEST");
        personMDTOLocal.setGender(Gender.MALE);
        personMDTOLocal.setLastName("TEST");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setRole("TEST");
        local.setPerson(personMDTOLocal);
        local.setName(languageItemListLocal);
        local.setDescription(languageItemListLocal);
        local.setContactDetails(generarContactDetailLocalTest());
        local.setIntitutionId("TEST");

        return local;
    }

    public ContactMDTO generarContactRemotoTest(){
        ContactMDTO remoto = new ContactMDTO();

        PersonMDTO personMDTORemoto = new PersonMDTO();

        personMDTORemoto.setBirthDate(new Date(123));
        personMDTORemoto.setCountryCode("TEST2");
        personMDTORemoto.setFirstNames("TEST2");
        personMDTORemoto.setGender(Gender.FEMALE);
        personMDTORemoto.setLastName("TEST2");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setRole("TEST2");
        remoto.setPerson(personMDTORemoto);
        remoto.setName(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);
        remoto.setContactDetails(generarContactDetailRemotoTest());
        remoto.setIntitutionId("TEST2");

        return remoto;
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosAll() {
        MobilityMDTO local = new MobilityMDTO();
        MobilityMDTO remoto = new MobilityMDTO();

        List<LearningAgreementMDTO> learningAgreementListLocal = new ArrayList<>();
        List<LearningAgreementMDTO> learningAgreementListRemoto = new ArrayList<>();

        LearningAgreementMDTO LearningAgreementLocal = new LearningAgreementMDTO();
        LearningAgreementMDTO LearningAgreementRemoto = new LearningAgreementMDTO();

        SignatureMDTO signatureMDTORemoto = new SignatureMDTO();
        SignatureMDTO signatureMDTOLocal = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];
        byte[] signatureRemoto = new byte[2];

        signatureMDTOLocal.setSignerName("TEST");
        signatureMDTOLocal.setSignerPosition("TEST");
        signatureMDTOLocal.setSignerEmail("TEST");
        signatureMDTOLocal.setTimestamp(new Date());
        signatureMDTOLocal.setSignerApp("TEST");
        signatureMDTOLocal.setSignature(signatureLocal);

        signatureMDTORemoto.setSignerName("TEST2");
        signatureMDTORemoto.setSignerPosition("TEST2");
        signatureMDTORemoto.setSignerEmail("TEST2");
        signatureMDTORemoto.setTimestamp(new Date(123));
        signatureMDTORemoto.setSignerApp("TEST2");
        signatureMDTORemoto.setSignature(signatureRemoto);

        byte[] pdfLocal = new byte[1];
        byte[] pdfRemoto = new byte[2];

        LearningAgreementLocal.setPdf(pdfLocal);
        LearningAgreementLocal.setStudentSign(signatureMDTOLocal);
        LearningAgreementLocal.setSenderCoordinatorSign(signatureMDTOLocal);
        LearningAgreementLocal.setReciverCoordinatorSign(signatureMDTOLocal);
        LearningAgreementLocal.setStatus(LearningAgreementStatus.APROVED);

        LearningAgreementRemoto.setPdf(pdfRemoto);
        LearningAgreementRemoto.setStudentSign(signatureMDTORemoto);
        LearningAgreementRemoto.setSenderCoordinatorSign(signatureMDTORemoto);
        LearningAgreementRemoto.setReciverCoordinatorSign(signatureMDTORemoto);
        LearningAgreementRemoto.setStatus(LearningAgreementStatus.REJECTED);

        learningAgreementListLocal.add(LearningAgreementLocal);
        learningAgreementListRemoto.add(LearningAgreementRemoto);

        SubjectAreaMDTO subjectAreaLocal = new SubjectAreaMDTO();
        subjectAreaLocal.setIscedCode("TEST");
        subjectAreaLocal.setIscedClarification("TEST");

        SubjectAreaMDTO subjectAreaRemoto = new SubjectAreaMDTO();
        subjectAreaRemoto.setIscedCode("TEST2");
        subjectAreaRemoto.setIscedClarification("TEST2");

        MobilityType mobilityTypeLocal = new MobilityType();
        MobilityType mobilityTypeRemoto = new MobilityType();

        mobilityTypeLocal.setMobilityGroup("TEST");
        mobilityTypeLocal.setMobilityCategory("TEST");

        mobilityTypeRemoto.setMobilityGroup("TEST2");
        mobilityTypeRemoto.setMobilityCategory("TEST2");

        MobilityParticipantMDTO mobilityParticipantLocal = new MobilityParticipantMDTO();
        MobilityParticipantMDTO mobilityParticipantRemoto = new MobilityParticipantMDTO();

        mobilityParticipantLocal.setGlobalId("TEST");
        mobilityParticipantLocal.setContactDetails(generarContactLocalTest());
        mobilityParticipantRemoto.setGlobalId("TEST2");
        mobilityParticipantRemoto.setContactDetails(generarContactRemotoTest());

        List<LanguageSkillMDTO> languageSkillListLocal = new ArrayList<>();
        List<LanguageSkillMDTO> languageSkillListRemoto = new ArrayList<>();

        LanguageSkillMDTO languageSkillLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillRemoto = new LanguageSkillMDTO();

        languageSkillLocal.setLanguage("TEST");
        languageSkillLocal.setCefrLevel("TEST");

        languageSkillRemoto.setLanguage("TEST2");
        languageSkillRemoto.setCefrLevel("TEST2");

        languageSkillListLocal.add(languageSkillLocal);
        languageSkillListRemoto.add(languageSkillRemoto);

        local.setActualArrivalDate(new Date());
        local.setActualDepartureDate(new Date());
        local.setPlannedArrivalDate(new Date());
        local.setPlannedDepartureDate(new Date());
        local.setEqfLevelDeparture((long)1);
        local.setEqfLevelNomination((long)1);
        local.setIscedCode(subjectAreaLocal);
        local.setStatusOutgoing(MobilityStatus.LIVE);
        local.setStatusIncomming(MobilityStatus.LIVE);
        local.setReceivingInstitutionId("TEST");
        local.setSendingInstitutionId("TEST");
        local.setReceivingOrganizationUnitId("TEST");
        local.setSendingOrganizationUnitId("TEST");
        local.setMobilityType(mobilityTypeLocal);
        local.setMobilityParticipantId(mobilityParticipantLocal);
        local.setLanguageSkill(languageSkillListLocal);
        local.setSenderContactPerson(generarContactLocalTest());
        local.setSenderAdmvContactPerson(generarContactLocalTest());
        local.setReceiverContactPerson(generarContactLocalTest());
        local.setReceiverAdmvContactPerson(generarContactLocalTest());
        local.setLearningAgreement(learningAgreementListLocal);

        remoto.setActualArrivalDate(new Date(123));
        remoto.setActualDepartureDate(new Date(123));
        remoto.setPlannedArrivalDate(new Date(123));
        remoto.setPlannedDepartureDate(new Date(123));
        remoto.setEqfLevelDeparture((long)2);
        remoto.setEqfLevelNomination((long)2);
        remoto.setIscedCode(subjectAreaRemoto);
        remoto.setStatusOutgoing(MobilityStatus.CANCELLED);
        remoto.setStatusIncomming(MobilityStatus.CANCELLED);
        remoto.setReceivingInstitutionId("TEST2");
        remoto.setSendingInstitutionId("TEST2");
        remoto.setReceivingOrganizationUnitId("TEST2");
        remoto.setSendingOrganizationUnitId("TEST2");
        remoto.setMobilityType(mobilityTypeRemoto);
        remoto.setMobilityParticipantId(mobilityParticipantRemoto);
        remoto.setLanguageSkill(languageSkillListRemoto);
        remoto.setSenderContactPerson(generarContactRemotoTest());
        remoto.setSenderAdmvContactPerson(generarContactRemotoTest());
        remoto.setReceiverContactPerson(generarContactRemotoTest());
        remoto.setReceiverAdmvContactPerson(generarContactRemotoTest());
        remoto.setLearningAgreement(learningAgreementListRemoto);

        boolean resultado = omobilityLasService.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }


}