package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.converters.impl.OmobilityLasConverter;
import es.minsait.ewpcv.model.omobility.LaComponentReasonCode;
import es.minsait.ewpcv.model.omobility.LaComponentType;
import es.minsait.ewpcv.model.omobility.LearningAgreementComponentStatus;
import es.minsait.ewpcv.model.omobility.LearningAgreementStatus;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import es.minsait.ewpcv.repository.dao.ILearningAgreementDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.*;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.service.api.INotificationService;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LearningAgreementServiceImplCompararTest extends TestCase {

    @Autowired
    private OmobilityLasConverter lasConverter;
    @Autowired
    private IIiaDAO iiaDAO;
    @Autowired
    private IIiaConverter iiaConverter;
    @Autowired
    private EventServiceImpl eventService;
    @Autowired
    private INotificationService notificationService;
    @Autowired
    private IIiaRegressionsDAO iiaRegressionsDAO;
    private IIiasServiceImpl iiasServiceImpl = new IIiasServiceImpl(iiaDAO, iiaConverter, eventService, notificationService, iiaRegressionsDAO);
    @Autowired
    private ILearningAgreementDAO laDao;
    private LearningAgreementServiceImpl learningAgreementServiceImpl = new LearningAgreementServiceImpl(iiasServiceImpl, laDao, lasConverter);

    @Test
    public void testCompararAcademicTerm_SinCambios() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosInstitutionId() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosInstitutionId() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test2");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosInstitutionId_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setInstitutionId(null);
        remoto.setInstitutionId("Test2");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosInstitutionId_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosOrganizationUnitId() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosOrganizationUnitId() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosOrganizationUnitId_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setOrganizationUnitId(null);
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosOrganizationUnitId_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosStartDate() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date());

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosStartDate() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosStartDate_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setStartDate(null);
        remoto.setStartDate(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosStartDate_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosEndDate() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date());

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosEndDate() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosEndDate_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setEndDate(null);
        remoto.setEndDate(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosEndDate_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosTermNumber() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(BigInteger.ONE);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTermNumber() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTermNumber_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTermNumber(null);
        remoto.setTermNumber(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTermNumber_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTermNumber(BigInteger.ONE);
        remoto.setTermNumber(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosTotalTerms() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(BigInteger.ONE);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTotalTerms() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTotalTerms_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTotalTerms(null);
        remoto.setTotalTerms(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosTotalTerms_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        local.setTotalTerms(BigInteger.ONE);
        remoto.setTotalTerms(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosAcademicYearEndYear() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setEndYear("TEST");
        academicYearRemoto.setEndYear("TEST");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearEndYear() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setEndYear("TEST");
        academicYearRemoto.setEndYear("TEST2");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearEndYear_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setEndYear(null);
        academicYearRemoto.setEndYear("TEST2");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearEndYear_NullLocal2() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();
        academicYearRemoto.setEndYear("TEST2");

        local.setAcademicYear(null);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearEndYear_NullRemoto2() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        academicYearLocal.setEndYear("TEST");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearEndYear_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setEndYear("TEST");
        academicYearRemoto.setEndYear(null);

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_SinCambiosAcademicYearStartYear() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setStartYear("TEST");
        academicYearRemoto.setStartYear("TEST");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearStartYear() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setStartYear("TEST");
        academicYearRemoto.setStartYear("TEST2");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearStartYear_NullLocal() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setStartYear(null);
        academicYearRemoto.setStartYear("TEST2");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearStartYear_NullLocal2() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();
        academicYearRemoto.setStartYear("TEST2");

        local.setAcademicYear(null);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearStartYear_NullRemoto() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setStartYear("TEST");
        academicYearRemoto.setStartYear(null);

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(academicYearRemoto);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAcademicYearStartYear_NullRemoto2() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        academicYearLocal.setStartYear("TEST");

        local.setAcademicYear(academicYearLocal);
        remoto.setAcademicYear(null);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAcademicTerm_ConCambiosAll() {
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicTermMDTO local = new AcademicTermMDTO();

        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearLocal.setStartYear("TEST");
        academicYearLocal.setEndYear("TEST");

        academicYearRemoto.setStartYear("TEST");
        academicYearRemoto.setEndYear("TEST");

        List<LanguageItemMDTO> languageItemMDTOListLocal = new ArrayList<>();
        List<LanguageItemMDTO> languageItemMDTOListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();

        languageItemMDTOLocal.setLang("TEST");
        languageItemMDTOLocal.setText("TEST");

        languageItemMDTORemoto.setLang("TEST");
        languageItemMDTORemoto.setText("TEST");

        languageItemMDTOListLocal.add(languageItemMDTOLocal);
        languageItemMDTOListRemoto.add(languageItemMDTORemoto);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setAcademicYear(academicYearLocal);
        local.setDispName(languageItemMDTOListLocal);
        local.setStartDate(new Date());
        local.setEndDate(new Date());
        local.setTotalTerms(BigInteger.ONE);
        local.setTermNumber(BigInteger.ONE);

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setAcademicYear(academicYearRemoto);
        remoto.setDispName(languageItemMDTOListRemoto);
        remoto.setStartDate(new Date(123));
        remoto.setEndDate(new Date(123));
        remoto.setTotalTerms(BigInteger.TEN);
        remoto.setTermNumber(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararAcademicTerm(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradingSchemes_SinCambios() {
        GradingSchemeMDTO remoto = new GradingSchemeMDTO();
        GradingSchemeMDTO local = new GradingSchemeMDTO();

        boolean resultado = learningAgreementServiceImpl.compararGradingSchemes(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGradingSchemes_ConCambiosAll() {
        GradingSchemeMDTO remoto = new GradingSchemeMDTO();
        GradingSchemeMDTO local = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");
        languageItemListLocal.add(languageItemMDTOLocal);

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");
        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setLabel(languageItemListLocal);
        local.setDescription(languageItemListLocal);

        remoto.setLabel(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);

        boolean resultado = learningAgreementServiceImpl.compararGradingSchemes(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_SinCambios() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCredit_SinCambiosLevel() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setLevel("TEST");
        remoto.setLevel("TEST");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosLevel() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setLevel("TEST");
        remoto.setLevel("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosLevel_NullLocal() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setLevel(null);
        remoto.setLevel("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosLevel_NullRemoto() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setLevel("TEST");
        remoto.setLevel(null);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_SinCambiosScheme() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setScheme("TEST");
        remoto.setScheme("TEST");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosScheme() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setScheme("TEST");
        remoto.setScheme("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosScheme_NullLocal() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setScheme(null);
        remoto.setScheme("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosScheme_NullRemoto() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setScheme("TEST");
        remoto.setScheme(null);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_SinCambiosValue() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setValue(BigDecimal.ONE);
        remoto.setValue(BigDecimal.ONE);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosValue() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setValue(BigDecimal.ONE);
        remoto.setValue(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosValue_NullLocal() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setValue(null);
        remoto.setValue(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosValue_NullRemoto() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setValue(BigDecimal.ONE);
        remoto.setValue(null);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCredit_ConCambiosAll() {
        CreditMDTO remoto = new CreditMDTO();
        CreditMDTO local = new CreditMDTO();

        local.setScheme("TEST");
        local.setLevel("TEST");
        local.setValue(BigDecimal.ONE);

        remoto.setScheme("TEST2");
        remoto.setLevel("TEST2");
        remoto.setValue(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararCredit(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambios() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignature() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];
        byte[] signatureRemoto = new byte[1];

        local.setSignature(signatureLocal);
        remoto.setSignature(signatureRemoto);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignature() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];
        byte[] signatureRemoto = new byte[2];

        local.setSignature(signatureLocal);
        remoto.setSignature(signatureRemoto);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignature_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        byte[] signatureRemoto = new byte[2];

        local.setSignature(null);
        remoto.setSignature(signatureRemoto);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignature_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];

        local.setSignature(signatureLocal);
        remoto.setSignature(null);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignerApp() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerApp("TEST");
        remoto.setSignerApp("TEST");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerApp() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerApp("TEST");
        remoto.setSignerApp("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerApp_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerApp(null);
        remoto.setSignerApp("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerApp_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerApp("TEST");
        remoto.setSignerApp(null);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignerEmail() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerEmail("TEST");
        remoto.setSignerEmail("TEST");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerEmail() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerEmail("TEST");
        remoto.setSignerEmail("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerEmail_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerEmail(null);
        remoto.setSignerEmail("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerEmail_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerEmail("TEST");
        remoto.setSignerEmail(null);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignerName() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerName("TEST");
        remoto.setSignerName("TEST");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerName() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerName("TEST");
        remoto.setSignerName("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerName_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerName(null);
        remoto.setSignerName("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerName_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerName("TEST");
        remoto.setSignerName(null);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignerPosition() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerPosition("TEST");
        remoto.setSignerPosition("TEST");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerPosition() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerPosition("TEST");
        remoto.setSignerPosition("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerPosition_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerPosition(null);
        remoto.setSignerPosition("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerPosition_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setSignerPosition("TEST");
        remoto.setSignerPosition(null);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_SinCambiosSignerTimestamp() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setTimestamp(new Date());
        remoto.setTimestamp(new Date());

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerTimestamp() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setTimestamp(new Date());
        remoto.setTimestamp(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerTimestampMismoDiaCambiaSegundo() throws ParseException {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        local.setTimestamp(format.parse("2020-01-01 00:00:00"));
        remoto.setTimestamp(format.parse("2020-01-01 00:00:01"));

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerTimestamp_NullLocal() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setTimestamp(new Date());
        remoto.setTimestamp(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosSignerTimestamp_NullRemoto() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        local.setTimestamp(new Date());
        remoto.setTimestamp(new Date(123));

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararSignature_ConCambiosAll() {
        SignatureMDTO remoto = new SignatureMDTO();
        SignatureMDTO local = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];
        byte[] signatureRemoto = new byte[2];

        local.setSignerName("TEST");
        local.setSignerPosition("TEST");
        local.setSignerEmail("TEST");
        local.setTimestamp(new Date());
        local.setSignerApp("TEST");
        local.setSignature(signatureLocal);

        remoto.setSignerName("TEST2");
        remoto.setSignerPosition("TEST2");
        remoto.setSignerEmail("TEST2");
        remoto.setTimestamp(new Date(123));
        remoto.setSignerApp("TEST2");
        remoto.setSignature(signatureRemoto);

        boolean resultado = learningAgreementServiceImpl.compararSignature(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambios() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambiosCount() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(BigInteger.ONE);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount_NullLocal() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setCount(null);
        remoto.setCount(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount_NullRemoto() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(null);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambiosLabel() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST");

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel_NullLocal() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setLabel(null);
        remoto.setLabel("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel_NullRemoto() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel(null);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosAll() {
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        local.setCount(BigInteger.ONE);

        remoto.setLabel("TEST2");
        remoto.setCount(BigInteger.TEN);

        boolean resultado = learningAgreementServiceImpl.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistribution_SinCambios() {
        ResultDistributionMDTO remoto = new ResultDistributionMDTO();
        ResultDistributionMDTO local = new ResultDistributionMDTO();

        boolean resultado = learningAgreementServiceImpl.compararResultDistribution(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistribution_ConCambiosAll() {
        ResultDistributionMDTO remoto = new ResultDistributionMDTO();
        ResultDistributionMDTO local = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListLocal = new ArrayList<>();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListRemoto = new ArrayList<>();
        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setLabel("TEST");
        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryRemoto.setLabel("TEST2");
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);

        resultDistributionCategoryListLocal.add(resultDistributionCategoryLocal);
        resultDistributionCategoryListRemoto.add(resultDistributionCategoryRemoto);

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemLocal = new LanguageItemMDTO();
        LanguageItemMDTO languageItemRemoto = new LanguageItemMDTO();

        languageItemLocal.setText("TEST");
        languageItemLocal.setLang("TEST");
        languageItemRemoto.setText("TEST2");
        languageItemRemoto.setLang("TEST2");

        languageItemListLocal.add(languageItemLocal);
        languageItemListRemoto.add(languageItemRemoto);

        local.setResultDistributionCategory(resultDistributionCategoryListLocal);
        local.setDescription(languageItemListLocal);
        remoto.setResultDistributionCategory(resultDistributionCategoryListRemoto);
        remoto.setDescription(languageItemListRemoto);

        boolean resultado = learningAgreementServiceImpl.compararResultDistribution(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambios() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosInstitutionId() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosInstitutionId() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosInstitutionId_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setInstitutionId(null);
        remoto.setInstitutionId("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosInstitutionId_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setInstitutionId("TEST");
        remoto.setInstitutionId(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosEqfLevel() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte) 1);
        remoto.setEqfLevel((byte) 1);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosEqfLevel() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte) 1);
        remoto.setEqfLevel((byte) 2);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosEqfLevel_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        remoto.setEqfLevel((byte) 2);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosEqfLevel_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte) 1);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosOrganizationUnitId() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosOrganizationUnitId() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosOrganizationUnitId_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setOrganizationUnitId(null);
        remoto.setOrganizationUnitId("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosOrganizationUnitId_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setOrganizationUnitId("TEST");
        remoto.setOrganizationUnitId(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosLosCode() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosLosCode() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosLosCode_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setLosCode(null);
        remoto.setLosCode("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosLosCode_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosType() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.COURSE);
        remoto.setType(LearningOpportunitySpecificationType.COURSE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosType() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.COURSE);
        remoto.setType(LearningOpportunitySpecificationType.MODULE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosType_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setType(null);
        remoto.setType(LearningOpportunitySpecificationType.MODULE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosType_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.COURSE);
        remoto.setType(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosIscedf() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosIscedf() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosIscedf_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setIscedf(null);
        remoto.setIscedf("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosIscedf_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosSubjectArea() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosSubjectArea() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosSubjectArea_NullLocal() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea(null);
        remoto.setSubjectArea("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosSubjectArea_NullRemoto() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_SinCambiosTopLevelParent() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setTopLevelParent(Boolean.TRUE);
        remoto.setTopLevelParent(Boolean.TRUE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosTopLevelParent() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        local.setTopLevelParent(Boolean.TRUE);
        remoto.setTopLevelParent(Boolean.FALSE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunitySpecification_ConCambiosAll() {
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemMDTOListLocal = new ArrayList<>();
        List<LanguageItemMDTO> languageItemMDTOListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();

        languageItemMDTOLocal.setLang("TEST");
        languageItemMDTOLocal.setText("TEST");

        languageItemMDTORemoto.setLang("TEST");
        languageItemMDTORemoto.setText("TEST");

        languageItemMDTOListLocal.add(languageItemMDTOLocal);
        languageItemMDTOListRemoto.add(languageItemMDTORemoto);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setLosCode("TEST");
        local.setType(LearningOpportunitySpecificationType.COURSE);
        local.setName(languageItemMDTOListLocal);
        local.setUrl(languageItemMDTOListLocal);
        local.setEqfLevel((byte) 1);
        local.setIscedf("TEST");
        local.setSubjectArea("TEST");
        local.setDescription(languageItemMDTOListLocal);
        local.setLearningOpportunitySpecifications(null);
        local.setTopLevelParent(Boolean.TRUE);

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setLosCode("TEST2");
        remoto.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setName(languageItemMDTOListRemoto);
        remoto.setUrl(languageItemMDTOListRemoto);
        remoto.setEqfLevel((byte) 1);
        remoto.setIscedf("TEST2");
        remoto.setSubjectArea("TEST2");
        remoto.setDescription(languageItemMDTOListRemoto);
        remoto.setLearningOpportunitySpecifications(null);
        remoto.setTopLevelParent(Boolean.FALSE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunitySpecification(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_SinCambios() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_SinCambiosEngagementHours() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(BigDecimal.ONE);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosEngagementHours() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosEngagementHours_NullLocal() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(null);
        remoto.setEngagementHours(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosEngagementHours_NullRemoto() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_SinCambiosLanguageOfInstruction() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction("TEST");
        remoto.setLanguageOfInstruction("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosLanguageOfInstruction() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction("TEST");
        remoto.setLanguageOfInstruction("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosLanguageOfInstruction_NullLocal() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction(null);
        remoto.setLanguageOfInstruction("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosLanguageOfInstruction_NullRemoto() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction("TEST");
        remoto.setLanguageOfInstruction(null);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLearningOportunityInstance_ConCambiosAll() {
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();

        GradingSchemeMDTO gradingSchemeRemoto = new GradingSchemeMDTO();
        GradingSchemeMDTO gradingSchemeLocal = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");
        languageItemListLocal.add(languageItemMDTOLocal);

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");
        languageItemListRemoto.add(languageItemMDTORemoto);

        gradingSchemeLocal.setLabel(languageItemListLocal);
        gradingSchemeLocal.setDescription(languageItemListLocal);

        gradingSchemeRemoto.setLabel(languageItemListRemoto);
        gradingSchemeRemoto.setDescription(languageItemListRemoto);

        ResultDistributionMDTO resultDistributionLocal = new ResultDistributionMDTO();
        ResultDistributionMDTO resultDistributionRemoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListLocal = new ArrayList<>();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListRemoto = new ArrayList<>();
        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setLabel("TEST");
        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryRemoto.setLabel("TEST2");
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);

        resultDistributionCategoryListLocal.add(resultDistributionCategoryLocal);
        resultDistributionCategoryListRemoto.add(resultDistributionCategoryRemoto);

        resultDistributionLocal.setResultDistributionCategory(resultDistributionCategoryListLocal);
        resultDistributionLocal.setDescription(languageItemListLocal);
        resultDistributionRemoto.setResultDistributionCategory(resultDistributionCategoryListRemoto);
        resultDistributionRemoto.setDescription(languageItemListRemoto);

        local.setGradingScheme(gradingSchemeLocal);
        local.setResultDistribution(resultDistributionLocal);
        local.setLanguageOfInstruction("TEST");
        local.setEngagementHours(BigDecimal.ONE);

        remoto.setGradingScheme(gradingSchemeRemoto);
        remoto.setResultDistribution(resultDistributionRemoto);
        remoto.setLanguageOfInstruction("TEST2");
        remoto.setEngagementHours(BigDecimal.TEN);

        boolean resultado = learningAgreementServiceImpl.compararLearningOportunityInstance(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambios() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosLosCode() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLosCode() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLosCode_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLosCode(null);
        remoto.setLosCode("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLosCode_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosStatus() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setStatus(LearningAgreementComponentStatus.INSERTED);
        remoto.setStatus(LearningAgreementComponentStatus.INSERTED);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosStatus() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setStatus(LearningAgreementComponentStatus.INSERTED);
        remoto.setStatus(LearningAgreementComponentStatus.DELETED);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosStatus_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setStatus(null);
        remoto.setStatus(LearningAgreementComponentStatus.DELETED);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosStatus_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setStatus(LearningAgreementComponentStatus.INSERTED);
        remoto.setStatus(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosTitle() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setTitle("TEST");
        remoto.setTitle("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosTitle() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setTitle("TEST");
        remoto.setTitle("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosTitle_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setTitle(null);
        remoto.setTitle("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosTitle_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setTitle("TEST");
        remoto.setTitle(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosReasonCode() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonCode(LaComponentReasonCode.NOT_AVAILABLE);
        remoto.setReasonCode(LaComponentReasonCode.NOT_AVAILABLE);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonCode() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonCode(LaComponentReasonCode.NOT_AVAILABLE);
        remoto.setReasonCode(LaComponentReasonCode.LANGUAGE_MISMATCH);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonCode_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonCode(null);
        remoto.setReasonCode(LaComponentReasonCode.LANGUAGE_MISMATCH);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonCode_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonCode(LaComponentReasonCode.NOT_AVAILABLE);
        remoto.setReasonCode(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosReasonText() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonText("TEST");
        remoto.setReasonText("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonText() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonText("TEST");
        remoto.setReasonText("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonText_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonText(null);
        remoto.setReasonText("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosReasonText_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setReasonText("TEST");
        remoto.setReasonText(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosLaComponentType() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLaComponentType(LaComponentType.VIRTUAL_COMPONENT);
        remoto.setLaComponentType(LaComponentType.VIRTUAL_COMPONENT);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLaComponentType() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLaComponentType(LaComponentType.VIRTUAL_COMPONENT);
        remoto.setLaComponentType(LaComponentType.BLENDED_COMPONENT);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLaComponentType_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLaComponentType(null);
        remoto.setLaComponentType(LaComponentType.BLENDED_COMPONENT);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosLaComponentType_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setLaComponentType(LaComponentType.VIRTUAL_COMPONENT);
        remoto.setLaComponentType(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosRecognitionConditions() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setRecognitionConditions("TEST");
        remoto.setRecognitionConditions("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosRecognitionConditions() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setRecognitionConditions("TEST");
        remoto.setRecognitionConditions("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosRecognitionConditions_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setRecognitionConditions(null);
        remoto.setRecognitionConditions("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosRecognitionConditions_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setRecognitionConditions("TEST");
        remoto.setRecognitionConditions(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_SinCambiosShortDescription() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setShortDescription("TEST");
        remoto.setShortDescription("TEST");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosShortDescription() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setShortDescription("TEST");
        remoto.setShortDescription("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosShortDescription_NullLocal() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setShortDescription(null);
        remoto.setShortDescription("TEST2");

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLaComponent_ConCambiosShortDescription_NullRemoto() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        local.setShortDescription("TEST");
        remoto.setShortDescription(null);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    public AcademicTermMDTO generarAcademicTermMDTOLocal(){
        AcademicTermMDTO local = new AcademicTermMDTO();
        AcademicYearMDTO academicYearLocal = new AcademicYearMDTO();

        academicYearLocal.setStartYear("TEST");
        academicYearLocal.setEndYear("TEST");

        List<LanguageItemMDTO> languageItemMDTOListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setLang("TEST");
        languageItemMDTOLocal.setText("TEST");

        languageItemMDTOListLocal.add(languageItemMDTOLocal);
        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setAcademicYear(academicYearLocal);
        local.setDispName(languageItemMDTOListLocal);
        local.setStartDate(new Date());
        local.setEndDate(new Date());
        local.setTotalTerms(BigInteger.ONE);
        local.setTermNumber(BigInteger.ONE);

        return local;
    }

    public AcademicTermMDTO generarAcademicTermMDTORemoto(){
        AcademicTermMDTO remoto = new AcademicTermMDTO();
        AcademicYearMDTO academicYearRemoto = new AcademicYearMDTO();

        academicYearRemoto.setStartYear("TEST");
        academicYearRemoto.setEndYear("TEST");

        List<LanguageItemMDTO> languageItemMDTOListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setLang("TEST");
        languageItemMDTORemoto.setText("TEST");

        languageItemMDTOListRemoto.add(languageItemMDTORemoto);
        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setAcademicYear(academicYearRemoto);
        remoto.setDispName(languageItemMDTOListRemoto);
        remoto.setStartDate(new Date(123));
        remoto.setEndDate(new Date(123));
        remoto.setTotalTerms(BigInteger.TEN);
        remoto.setTermNumber(BigInteger.TEN);

        return remoto;
    }

    public LearningOpportunityInstanceMDTO generarLearningOpportunityInstanceMDTOLocal(){
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        GradingSchemeMDTO gradingSchemeLocal = new GradingSchemeMDTO();
        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");
        languageItemListLocal.add(languageItemMDTOLocal);

        gradingSchemeLocal.setLabel(languageItemListLocal);
        gradingSchemeLocal.setDescription(languageItemListLocal);

        ResultDistributionMDTO resultDistributionLocal = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListLocal = new ArrayList<>();
        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        resultDistributionCategoryLocal.setLabel("TEST");
        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryListLocal.add(resultDistributionCategoryLocal);

        resultDistributionLocal.setResultDistributionCategory(resultDistributionCategoryListLocal);
        resultDistributionLocal.setDescription(languageItemListLocal);

        local.setGradingScheme(gradingSchemeLocal);
        local.setResultDistribution(resultDistributionLocal);
        local.setLanguageOfInstruction("TEST");
        local.setEngagementHours(BigDecimal.ONE);

        return local;
    }

    public LearningOpportunityInstanceMDTO generarLearningOpportunityInstanceMDTORemoto(){
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();
        GradingSchemeMDTO gradingSchemeRemoto = new GradingSchemeMDTO();
        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");
        languageItemListRemoto.add(languageItemMDTORemoto);

        gradingSchemeRemoto.setLabel(languageItemListRemoto);
        gradingSchemeRemoto.setDescription(languageItemListRemoto);

        ResultDistributionMDTO resultDistributionRemoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryListRemoto = new ArrayList<>();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryRemoto.setLabel("TEST2");
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);
        resultDistributionCategoryListRemoto.add(resultDistributionCategoryRemoto);

        resultDistributionRemoto.setResultDistributionCategory(resultDistributionCategoryListRemoto);
        resultDistributionRemoto.setDescription(languageItemListRemoto);

        remoto.setGradingScheme(gradingSchemeRemoto);
        remoto.setResultDistribution(resultDistributionRemoto);
        remoto.setLanguageOfInstruction("TEST2");
        remoto.setEngagementHours(BigDecimal.TEN);

        return remoto;
    }

    public LearningOpportunitySpecificationMDTO generarLearningOpportunitySpecificationMDTOLocal(){
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        List<LanguageItemMDTO> languageItemMDTOListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setLang("TEST");
        languageItemMDTOLocal.setText("TEST");

        languageItemMDTOListLocal.add(languageItemMDTOLocal);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setLosCode("TEST");
        local.setType(LearningOpportunitySpecificationType.COURSE);
        local.setName(languageItemMDTOListLocal);
        local.setUrl(languageItemMDTOListLocal);
        local.setEqfLevel((byte) 1);
        local.setIscedf("TEST");
        local.setSubjectArea("TEST");
        local.setDescription(languageItemMDTOListLocal);
        local.setLearningOpportunitySpecifications(null);
        local.setTopLevelParent(Boolean.TRUE);

        return local;
    }

    public LearningOpportunitySpecificationMDTO generarLearningOpportunitySpecificationMDTORemoto(){
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();
        List<LanguageItemMDTO> languageItemMDTOListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setLang("TEST");
        languageItemMDTORemoto.setText("TEST");

        languageItemMDTOListRemoto.add(languageItemMDTORemoto);

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setLosCode("TEST2");
        remoto.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setName(languageItemMDTOListRemoto);
        remoto.setUrl(languageItemMDTOListRemoto);
        remoto.setEqfLevel((byte) 1);
        remoto.setIscedf("TEST2");
        remoto.setSubjectArea("TEST2");
        remoto.setDescription(languageItemMDTOListRemoto);
        remoto.setLearningOpportunitySpecifications(null);
        remoto.setTopLevelParent(Boolean.FALSE);

        return remoto;
    }
    @Test
    public void testCompararLaComponent_ConCambiosAll() {
        LaComponentMDTO remoto = new LaComponentMDTO();
        LaComponentMDTO local = new LaComponentMDTO();

        List<CreditMDTO> creditMDTOListRemoto = new ArrayList<>();
        List<CreditMDTO> creditMDTOListLocal = new ArrayList<>();
        CreditMDTO creditMDTORemoto = new CreditMDTO();
        CreditMDTO creditMDTOLocal = new CreditMDTO();

        creditMDTOLocal.setScheme("TEST");
        creditMDTOLocal.setLevel("TEST");
        creditMDTOLocal.setValue(BigDecimal.ONE);

        creditMDTORemoto.setScheme("TEST2");
        creditMDTORemoto.setLevel("TEST2");
        creditMDTORemoto.setValue(BigDecimal.TEN);

        creditMDTOListLocal.add(creditMDTOLocal);
        creditMDTOListRemoto.add(creditMDTORemoto);

        local.setLosCode("TEST");
        local.setStatus(LearningAgreementComponentStatus.INSERTED);
        local.setTitle("TEST");
        local.setReasonCode(LaComponentReasonCode.NOT_AVAILABLE);
        local.setReasonText("TEST");
        local.setLaComponentType(LaComponentType.VIRTUAL_COMPONENT);
        local.setRecognitionConditions("TEST");
        local.setShortDescription("TEST");
        local.setAcademicTermDisplayName(generarAcademicTermMDTOLocal());
        local.setLoiId(generarLearningOpportunityInstanceMDTOLocal());
        local.setLosId(generarLearningOpportunitySpecificationMDTOLocal());
        local.setCredits(creditMDTOListLocal);

        remoto.setLosCode("TEST2");
        remoto.setStatus(LearningAgreementComponentStatus.DELETED);
        remoto.setTitle("TEST2");
        remoto.setReasonCode(LaComponentReasonCode.LANGUAGE_MISMATCH);
        remoto.setReasonText("TEST2");
        remoto.setLaComponentType(LaComponentType.BLENDED_COMPONENT);
        remoto.setRecognitionConditions("TEST2");
        remoto.setShortDescription("TEST2");
        remoto.setAcademicTermDisplayName(generarAcademicTermMDTORemoto());
        remoto.setLoiId(generarLearningOpportunityInstanceMDTORemoto());
        remoto.setLosId(generarLearningOpportunitySpecificationMDTORemoto());
        remoto.setCredits(creditMDTOListRemoto);

        boolean resultado = learningAgreementServiceImpl.compararLaComponent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambios() {
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();
        LearningAgreementMDTO local = new LearningAgreementMDTO();

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosPdf() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        byte[] pdfLocal = new byte[1];
        byte[] pdfRemoto = new byte[1];

        local.setPdf(pdfLocal);
        remoto.setPdf(pdfRemoto);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPdf() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        byte[] pdfLocal = new byte[1];
        byte[] pdfRemoto = new byte[2];

        local.setPdf(pdfLocal);
        remoto.setPdf(pdfRemoto);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPdf_NullLocal() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        byte[] pdfRemoto = new byte[2];

        local.setPdf(null);
        remoto.setPdf(pdfRemoto);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosPdf_NullRemoto() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        byte[] pdfLocal = new byte[1];

        local.setPdf(pdfLocal);
        remoto.setPdf(null);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_SinCambiosStatus() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        local.setStatus(LearningAgreementStatus.APROVED);
        remoto.setStatus(LearningAgreementStatus.APROVED);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatus() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        local.setStatus(LearningAgreementStatus.APROVED);
        remoto.setStatus(LearningAgreementStatus.REJECTED);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatus_NullLocal() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        local.setStatus(null);
        remoto.setStatus(LearningAgreementStatus.REJECTED);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosStatus_NullRemoto() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        local.setStatus(LearningAgreementStatus.APROVED);
        remoto.setStatus(null);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLocalConRemoto_ConCambiosAll() {
        LearningAgreementMDTO local = new LearningAgreementMDTO();
        LearningAgreementMDTO remoto = new LearningAgreementMDTO();

        SignatureMDTO signatureMDTORemoto = new SignatureMDTO();
        SignatureMDTO signatureMDTOLocal = new SignatureMDTO();

        byte[] signatureLocal = new byte[1];
        byte[] signatureRemoto = new byte[2];

        signatureMDTOLocal.setSignerName("TEST");
        signatureMDTOLocal.setSignerPosition("TEST");
        signatureMDTOLocal.setSignerEmail("TEST");
        signatureMDTOLocal.setTimestamp(new Date());
        signatureMDTOLocal.setSignerApp("TEST");
        signatureMDTOLocal.setSignature(signatureLocal);

        signatureMDTORemoto.setSignerName("TEST2");
        signatureMDTORemoto.setSignerPosition("TEST2");
        signatureMDTORemoto.setSignerEmail("TEST2");
        signatureMDTORemoto.setTimestamp(new Date(123));
        signatureMDTORemoto.setSignerApp("TEST2");
        signatureMDTORemoto.setSignature(signatureRemoto);

        byte[] pdfLocal = new byte[1];
        byte[] pdfRemoto = new byte[2];

        local.setPdf(pdfLocal);
        local.setStudentSign(signatureMDTOLocal);
        local.setSenderCoordinatorSign(signatureMDTOLocal);
        local.setReciverCoordinatorSign(signatureMDTOLocal);
        local.setStatus(LearningAgreementStatus.APROVED);

        remoto.setPdf(pdfRemoto);
        remoto.setStudentSign(signatureMDTORemoto);
        remoto.setSenderCoordinatorSign(signatureMDTORemoto);
        remoto.setReciverCoordinatorSign(signatureMDTORemoto);
        remoto.setStatus(LearningAgreementStatus.REJECTED);

        boolean resultado = learningAgreementServiceImpl.compararLocalConRemoto(remoto, local);

        assertTrue(resultado);
    }
}