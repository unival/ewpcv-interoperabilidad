package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converters.impl.IiaHashGenerator;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class IiaHashGeneratorTest extends TestCase {

    @Test
    public void testGetXslt6() throws IOException, URISyntaxException {
        byte[] xslt = IiaHashGenerator.getXslt6();
        assertNotNull(xslt);
        String xsltS = new String(xslt);
        assertTrue(xsltS.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertTrue(xsltS.contains("version=\"2.0\""));
        assertTrue(xsltS.contains("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\""));
        assertTrue(xsltS.endsWith("</xsl:stylesheet>"));
    }

    @Test
    public void testGetXslt7() throws IOException, URISyntaxException {
        byte[] xslt = IiaHashGenerator.getXslt7();
        assertNotNull(xslt);
        String xsltS = new String(xslt);
        assertTrue(xsltS.startsWith("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
        assertTrue(xsltS.contains("version=\"2.0\""));
        assertTrue(xsltS.contains("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\""));
        assertTrue(xsltS.endsWith("</xsl:stylesheet>"));
    }

    @Test
    public void testGetTextToHash6() throws IOException, URISyntaxException {
        byte[] iia = Files.readAllBytes(Paths.get(this.getClass().getResource("/iiaSample/get-response-v6.xml").toURI()));
        byte[] xslt = IiaHashGenerator.getXslt6();
        byte[] result = IiaHashGenerator.getTextToHash(iia, xslt);

        assertTrue(result.length > 0);
        String resultS = new String(result);
        assertTrue(resultS.startsWith("_iia-id_"));
        assertTrue(resultS.endsWith("_"));
    }

    @Test
    public void testGetTextTohash7() throws IOException, URISyntaxException {
        byte[] iia = Files.readAllBytes(Paths.get(this.getClass().getResource("/iiaSample/get-response-v7.xml").toURI()));
        byte[] xslt = IiaHashGenerator.getXslt7();
        byte[] result = IiaHashGenerator.getTextToHash(iia, xslt);

        assertTrue(result.length > 0);
        String resultS = new String(result);
        assertTrue(resultS.startsWith("_iia-id_"));
        assertTrue(resultS.endsWith("_"));
    }

    @Test
    public void testValidForApprove() throws URISyntaxException, IOException {
        byte[] iia = Files.readAllBytes(Paths.get(this.getClass().getResource("/iiaSample/get-response-v7.xml").toURI()));
        boolean result = IiaHashGenerator.validForApprove(iia);

        assertFalse(result);
    }

    @Test
    public void testGenerateIiaHash7() throws IOException, URISyntaxException {
        byte[] iia = Files.readAllBytes(Paths.get(this.getClass().getResource("/iiaSample/get-response-v7.xml").toURI()));
        byte[] xslt = IiaHashGenerator.getXslt7();
        String result = IiaHashGenerator.generateIiaHash(IiaHashGenerator.getTextToHash(iia, xslt));
        assertFalse(result.isEmpty());
        assertEquals(64, result.length());
        assertEquals("87b33170d7a6c6d894215641f39e7b7de36501265479e5ab3922f32d5b225033", result);
    }

    @Test
    public void testGenerateIiaHash6() throws IOException, URISyntaxException {
        byte[] iia = Files.readAllBytes(Paths.get(this.getClass().getResource("/iiaSample/get-response-v6.xml").toURI()));
        byte[] xslt = IiaHashGenerator.getXslt6();
        String result = IiaHashGenerator.generateIiaHash(IiaHashGenerator.getTextToHash(iia, xslt));
        assertFalse(result.isEmpty());
        assertEquals(64, result.length());
        assertEquals("87b33170d7a6c6d894215641f39e7b7de36501265479e5ab3922f32d5b225033", result);
    }

}
