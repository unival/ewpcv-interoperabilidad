package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.converter.api.ITorConverter;
import es.minsait.ewpcv.model.course.AttachmentType;
import es.minsait.ewpcv.model.course.LevelType;
import es.minsait.ewpcv.model.course.LoiStatus;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import es.minsait.ewpcv.repository.dao.IOmobilityDAO;
import es.minsait.ewpcv.repository.dao.ITorDAO;
import es.minsait.ewpcv.repository.model.course.*;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionCategoryMDTO;
import es.minsait.ewpcv.repository.model.omobility.ResultDistributionMDTO;
import es.minsait.ewpcv.repository.model.organization.LanguageItemMDTO;
import es.minsait.ewpcv.service.api.ILearningAgreementService;
import es.minsait.ewpcv.service.api.INotificationService;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TorServiceImplCompararTest extends TestCase {

    @Autowired
    private ITorDAO torDAO;
    @Autowired
    private ITorConverter converter;
    @Autowired
    private IOmobilityDAO mobilityDao;
    @Autowired
    private IIiaDAO iiaDAO;
    @Autowired
    private IIiaConverter iiaConverter;
    @Autowired
    private EventServiceImpl eventService;
    @Autowired
    private INotificationService notificationService;
    @Autowired
    private IIiaRegressionsDAO iiaRegressionsDAO;
    private IIiasServiceImpl iiasServiceImpl = new IIiasServiceImpl(iiaDAO, iiaConverter, eventService, notificationService, iiaRegressionsDAO);
    @Autowired
    private ILearningAgreementService learningAgreementService;

    private TorServiceImpl torService = new TorServiceImpl(torDAO, converter,
            mobilityDao, iiasServiceImpl, learningAgreementService);



    @Test
    public void testCompararGradeFerequency_SinCambios() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGradeFerequency_SinCambiosLabel() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST");

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosLabel() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST2");

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosLabel_NullLocal() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setLabel(null);
        remoto.setLabel("TEST2");

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosLabel_NullRemoto() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setLabel("TEST");
        remoto.setLabel(null);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_SinCambiosPercentage() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setPercentage((long)1);
        remoto.setPercentage((long)1);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosPercentage() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setPercentage((long)1);
        remoto.setPercentage((long)2);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosPercentage_NullLocal() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setPercentage(null);
        remoto.setPercentage((long)2);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosPercentage_NullRemoto() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setPercentage((long)1);
        remoto.setPercentage(null);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradeFerequency_ConCambiosAll() {
        GradeFrequencyMDTO local = new GradeFrequencyMDTO();
        GradeFrequencyMDTO remoto = new GradeFrequencyMDTO();

        local.setLabel("TEST");
        local.setPercentage((long)1);

        remoto.setLabel("TEST2");
        remoto.setPercentage((long)2);

        boolean resultado = torService.compararGradeFerequency(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIscedTable_SinCambios() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIscedTable_SinCambiosIscedCode() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode("TEST");
        remoto.setIscedCode("TEST");

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIscedTable_ConCambiosIscedCode() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode("TEST");
        remoto.setIscedCode("TEST2");

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIscedTable_ConCambiosIscedCode_NullLocal() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode(null);
        remoto.setIscedCode("TEST2");

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIscedTable_ConCambiosIscedCode_NullRemoto() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode("TEST");
        remoto.setIscedCode(null);

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIscedTable_ConCambiosAll() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        List<GradeFrequencyMDTO> frequenciesListLocal = new ArrayList<>();
        List<GradeFrequencyMDTO> frequenciesListRemoto = new ArrayList<>();

        GradeFrequencyMDTO frequenciesLocal = new GradeFrequencyMDTO();
        GradeFrequencyMDTO frequenciesRemoto = new GradeFrequencyMDTO();

        frequenciesLocal.setLabel("TEST");
        frequenciesLocal.setPercentage((long)1);

        frequenciesRemoto.setLabel("TEST2");
        frequenciesRemoto.setPercentage((long)2);

        frequenciesListLocal.add(frequenciesLocal);
        frequenciesListRemoto.add(frequenciesRemoto);

        local.setIscedCode("TEST");
        local.setFrequencies(frequenciesListLocal);

        remoto.setIscedCode("TEST2");
        remoto.setFrequencies(frequenciesListRemoto);

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_SinCambios() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        boolean resultado = torService.compararLevel(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLevel_SinCambiosValue() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setValue("TEST");
        remoto.setValue("TEST");

        boolean resultado = torService.compararLevel(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosValue() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setValue("TEST");
        remoto.setValue("TEST2");

        boolean resultado = torService.compararLevel(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosValue_NullLocal() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode(null);
        remoto.setIscedCode("TEST2");

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosValue_NullRemoto() {
        IscedTableMDTO local = new IscedTableMDTO();
        IscedTableMDTO remoto = new IscedTableMDTO();

        local.setIscedCode("TEST");
        remoto.setIscedCode(null);

        boolean resultado = torService.compararIscedTable(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_SinCambiosType() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setType(LevelType.EQF);
        remoto.setType(LevelType.EQF);

        boolean resultado = torService.compararLevel(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosType() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setType(LevelType.EQF);
        remoto.setType(LevelType.EQF);

        boolean resultado = torService.compararLevel(remoto, local);

        assertFalse(resultado); //Solo hay un type
    }

    @Test
    public void testCompararLevel_ConCambiosType_NullLocal() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setType(null);
        remoto.setType(LevelType.EQF);

        boolean resultado = torService.compararLevel(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosType_NullRemoto() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        local.setType(LevelType.EQF);
        remoto.setType(null);

        boolean resultado = torService.compararLevel(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLevel_ConCambiosAll() {
        LevelMDTO local = new LevelMDTO();
        LevelMDTO remoto = new LevelMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setType(LevelType.EQF);
        local.setValue("TEST");
        local.setDescription(languageItemListLocal);

        remoto.setType(LevelType.EQF);
        remoto.setValue("TEST2");
        remoto.setDescription(languageItemListRemoto);

        boolean resultado = torService.compararLevel(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_SinCambios() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAttachmentContent_SinCambiosLanguage() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        local.setLanguage("TEST");
        remoto.setLanguage("TEST");

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosLanguage() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        local.setLanguage("TEST");
        remoto.setLanguage("TEST2");

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosLanguage_NullLocal() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        local.setLanguage(null);
        remoto.setLanguage("TEST2");

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosLanguage_NullRemoto() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        local.setLanguage("TEST");
        remoto.setLanguage(null);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_SinCambiosContent() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[1];

        local.setContent(contentLocal);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosContent() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        local.setContent(contentLocal);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosContent_NullLocal() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        byte[] contentRemoto = new byte[2];

        local.setContent(null);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosContent_NullRemoto() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        byte[] contentLocal = new byte[1];

        local.setContent(contentLocal);
        remoto.setContent(null);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachmentContent_ConCambiosAll() {
        AttachmentContentMDTO local = new AttachmentContentMDTO();
        AttachmentContentMDTO remoto = new AttachmentContentMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        local.setLanguage("TEST");
        local.setContent(contentLocal);

        remoto.setLanguage("TEST2");
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararAttachmentContent(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGradingScheme_SinCambios() {
        GradingSchemeMDTO local = new GradingSchemeMDTO();
        GradingSchemeMDTO remoto = new GradingSchemeMDTO();

        boolean resultado = torService.compararGradingScheme(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGradingScheme_ConCambiosAll() {
        GradingSchemeMDTO local = new GradingSchemeMDTO();
        GradingSchemeMDTO remoto = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setLabel(languageItemListLocal);
        local.setDescription(languageItemListLocal);

        remoto.setLabel(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);

        boolean resultado = torService.compararGradingScheme(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGroup_SinCambios() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        boolean resultado = torService.compararGroup(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGroup_SinCambiosLabel() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        local.setSortingKey("TEST");
        remoto.setSortingKey("TEST");

        boolean resultado = torService.compararGroup(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararGroup_ConCambiosLabel() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        local.setSortingKey("TEST");
        remoto.setSortingKey("TEST2");

        boolean resultado = torService.compararGroup(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGroup_ConCambiosLabel_NullLocal() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        local.setSortingKey(null);
        remoto.setSortingKey("TEST2");

        boolean resultado = torService.compararGroup(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGroup_ConCambiosLabel_NullRemoto() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        local.setSortingKey("TEST");
        remoto.setSortingKey(null);

        boolean resultado = torService.compararGroup(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGroup_ConCambiosAll() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        local.setSortingKey("TEST");
        local.setTitle(languageItemMDTOLocal);

        remoto.setSortingKey("TEST2");
        remoto.setTitle(languageItemMDTORemoto);

        boolean resultado = torService.compararGroup(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararGroupType_SinCambios() {
        GroupTypeMDTO local = new GroupTypeMDTO();
        GroupTypeMDTO remoto = new GroupTypeMDTO();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        List<GroupMDTO> groupMDTOListLocal = new ArrayList<>();
        List<GroupMDTO> groupMDTOListRemoto = new ArrayList<>();

        GroupMDTO groupMDTOLocal = new GroupMDTO();
        GroupMDTO groupMDTORemoto = new GroupMDTO();

        groupMDTOLocal.setSortingKey("TEST");
        groupMDTOLocal.setTitle(languageItemMDTOLocal);

        groupMDTORemoto.setSortingKey("TEST2");
        groupMDTORemoto.setTitle(languageItemMDTORemoto);

        groupMDTOListLocal.add(groupMDTOLocal);
        groupMDTOListRemoto.add(groupMDTORemoto);

        local.setGroups(groupMDTOListLocal);
        local.setTitle(languageItemMDTOLocal);

        remoto.setGroups(groupMDTOListRemoto);
        remoto.setTitle(languageItemMDTORemoto);

        boolean resultado = torService.compararGroupType(remoto, local);

        assertTrue(resultado);
    }
    @Test
    public void testCompararGroupType_ConCambiosAll() {
        GroupMDTO local = new GroupMDTO();
        GroupMDTO remoto = new GroupMDTO();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        local.setSortingKey("TEST");
        local.setTitle(languageItemMDTOLocal);

        remoto.setSortingKey("TEST2");
        remoto.setTitle(languageItemMDTORemoto);

        boolean resultado = torService.compararGroup(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachment_SinCambios() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        boolean resultado = torService.compararAttachment(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAttachment_SinCambiosType() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        local.setType(AttachmentType.TOR);
        remoto.setType(AttachmentType.TOR);

        boolean resultado = torService.compararAttachment(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararAttachment_ConCambiosType() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        local.setType(AttachmentType.TOR);
        remoto.setType(AttachmentType.DIPLOMA);

        boolean resultado = torService.compararAttachment(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachment_ConCambiosType_NullLocal() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        local.setType(AttachmentType.TOR);
        remoto.setType(null);

        boolean resultado = torService.compararAttachment(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachment_ConCambiosType_NullRemoto() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        local.setType(null);
        remoto.setType(AttachmentType.DIPLOMA);

        boolean resultado = torService.compararAttachment(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararAttachment_ConCambiosAll() {
        AttachmentMDTO local = new AttachmentMDTO();
        AttachmentMDTO remoto = new AttachmentMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        List<AttachmentContentMDTO> contentListLocal = new ArrayList<>();
        List<AttachmentContentMDTO> contentListRemoto = new ArrayList<>();

        AttachmentContentMDTO attachmentContentLocal = new AttachmentContentMDTO();
        AttachmentContentMDTO attachmentContentRemoto = new AttachmentContentMDTO();

        attachmentContentLocal.setLanguage("TEST");
        attachmentContentLocal.setContent(extensionLocal);

        attachmentContentRemoto.setLanguage("TEST2");
        attachmentContentRemoto.setContent(extensionRemoto);

        contentListLocal.add(attachmentContentLocal);
        contentListRemoto.add(attachmentContentRemoto);

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setType(AttachmentType.TOR);
        local.setExtension(extensionLocal);
        local.setContents(contentListLocal);
        local.setTitle(languageItemListLocal);
        local.setDescription(languageItemListLocal);

        remoto.setType(AttachmentType.DIPLOMA);
        remoto.setExtension(extensionRemoto);
        remoto.setContents(contentListRemoto);
        remoto.setTitle(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);


        boolean resultado = torService.compararAttachment(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaAdditionalInfo_SinCambios() {
        DiplomaAdditionalInfoMDTO local = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO remoto = new DiplomaAdditionalInfoMDTO();

        boolean resultado = torService.compararDiplomaAdditionalInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaAdditionalInfo_SinCambiosVersion() {
        DiplomaAdditionalInfoMDTO local = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO remoto = new DiplomaAdditionalInfoMDTO();

        local.setAdditionalInfo("TEST");
        remoto.setAdditionalInfo("TEST");

        boolean resultado = torService.compararDiplomaAdditionalInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaAdditionalInfo_ConCambiosVersion() {
        DiplomaAdditionalInfoMDTO local = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO remoto = new DiplomaAdditionalInfoMDTO();

        local.setAdditionalInfo("TEST");
        remoto.setAdditionalInfo("TEST2");

        boolean resultado = torService.compararDiplomaAdditionalInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaAdditionalInfo_ConCambiosVersion_NullLocal() {
        DiplomaAdditionalInfoMDTO local = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO remoto = new DiplomaAdditionalInfoMDTO();

        local.setAdditionalInfo(null);
        remoto.setAdditionalInfo("TEST2");

        boolean resultado = torService.compararDiplomaAdditionalInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaAdditionalInfo_ConCambiosVersion_NullRemoto() {
        DiplomaAdditionalInfoMDTO local = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO remoto = new DiplomaAdditionalInfoMDTO();

        local.setAdditionalInfo("TEST");
        remoto.setAdditionalInfo(null);

        boolean resultado = torService.compararDiplomaAdditionalInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_SinCambios() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaSection_SinCambiosTitle() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setTitle("TEST");
        remoto.setTitle("TEST");

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosTitle() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setTitle("TEST");
        remoto.setTitle("TEST2");

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosTitle_NullLocal() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setTitle(null);
        remoto.setTitle("TEST2");

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosTitle_NullRemoto() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setTitle("TEST");
        remoto.setTitle(null);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_SinCambiosContent() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[1];

        local.setContent(contentLocal);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosContent() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        local.setContent(contentLocal);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosContent_NullLocal() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        byte[] contentRemoto = new byte[2];

        local.setContent(null);
        remoto.setContent(contentRemoto);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosContent_NullRemoto() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];

        local.setContent(contentLocal);
        remoto.setContent(null);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_SinCambiosNumber() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setNumber(1);
        remoto.setNumber(1);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosNumber() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setNumber(1);
        remoto.setNumber(2);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosNumber_NullLocal() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setNumber(null);
        remoto.setNumber(2);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosNumber_NullRemoto() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        local.setNumber(1);
        remoto.setNumber(null);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    public AttachmentMDTO generarAttachmentMDTOLocal(){
        AttachmentMDTO local = new AttachmentMDTO();

        byte[] extensionLocal = new byte[1];

        List<AttachmentContentMDTO> contentListLocal = new ArrayList<>();

        AttachmentContentMDTO attachmentContentLocal = new AttachmentContentMDTO();

        attachmentContentLocal.setLanguage("TEST");
        attachmentContentLocal.setContent(extensionLocal);

        contentListLocal.add(attachmentContentLocal);

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        local.setType(AttachmentType.TOR);
        local.setExtension(extensionLocal);
        local.setContents(contentListLocal);
        local.setTitle(languageItemListLocal);
        local.setDescription(languageItemListLocal);

        return local;
    }

    public AttachmentMDTO generarAttachmentMDTORemoto(){
        AttachmentMDTO remoto = new AttachmentMDTO();
        byte[] extensionRemoto = new byte[2];

        List<AttachmentContentMDTO> contentListRemoto = new ArrayList<>();

        AttachmentContentMDTO attachmentContentRemoto = new AttachmentContentMDTO();

        attachmentContentRemoto.setLanguage("TEST2");
        attachmentContentRemoto.setContent(extensionRemoto);

        contentListRemoto.add(attachmentContentRemoto);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        remoto.setType(AttachmentType.DIPLOMA);
        remoto.setExtension(extensionRemoto);
        remoto.setContents(contentListRemoto);
        remoto.setTitle(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);

        return remoto;
    }

    @Test
    public void testCompararDiplomaSection_ConCambiosAll() {
        DiplomaSectionMDTO local = new DiplomaSectionMDTO();
        DiplomaSectionMDTO remoto = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListLocal = new ArrayList<>();
        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListRemoto = new ArrayList<>();

        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoLocal = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoRemoto = new DiplomaAdditionalInfoMDTO();

        diplomaAdditionalInfoLocal.setAdditionalInfo("TEST");
        diplomaAdditionalInfoRemoto.setAdditionalInfo("TEST2");

        diplomaAdditionalInfoListLocal.add(diplomaAdditionalInfoLocal);
        diplomaAdditionalInfoListRemoto.add(diplomaAdditionalInfoRemoto);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        local.setTitle("TEST");
        local.setContent(contentLocal);
        local.setNumber(1);
        local.setAdditionalInfo(diplomaAdditionalInfoListLocal);
        local.setAttachments(attachmentMDTOListLocal);

        remoto.setTitle("TEST2");
        remoto.setContent(contentRemoto);
        remoto.setNumber(2);
        remoto.setAdditionalInfo(diplomaAdditionalInfoListRemoto);
        remoto.setAttachments(attachmentMDTOListRemoto);

        boolean resultado = torService.compararDiplomaSection(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambios() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST");

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambiosLabel() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST");

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel("TEST2");

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel_NullLocal() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setLabel(null);
        remoto.setLabel("TEST2");

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosLabel_NullRemoto() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setLabel("TEST");
        remoto.setLabel(null);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_SinCambiosCount() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(BigInteger.ONE);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(BigInteger.TEN);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount_NullLocal() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setCount(null);
        remoto.setCount(BigInteger.TEN);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosCount_NullRemoto() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(null);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistributionCategory_ConCambiosAll() {
        ResultDistributionCategoryMDTO local = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO remoto = new ResultDistributionCategoryMDTO();

        local.setCount(BigInteger.ONE);
        remoto.setCount(BigInteger.TEN);

        boolean resultado = torService.compararResultDistributionCategory(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararResultDistribution_SinCambios() {
        ResultDistributionMDTO local = new ResultDistributionMDTO();
        ResultDistributionMDTO remoto = new ResultDistributionMDTO();

        boolean resultado = torService.compararResultDistribution(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararResultDistribution_ConCambiosResultDistribution() {
        ResultDistributionMDTO local = new ResultDistributionMDTO();
        ResultDistributionMDTO remoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryLocalList = new ArrayList<>();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryRemotoList = new ArrayList<>();

        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setLabel("TEST");
        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryRemoto.setLabel("TEST2");
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);

        resultDistributionCategoryLocalList.add(resultDistributionCategoryLocal);
        resultDistributionCategoryRemotoList.add(resultDistributionCategoryRemoto);

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setResultDistributionCategory(resultDistributionCategoryLocalList);
        local.setDescription(languageItemListLocal);

        remoto.setResultDistributionCategory(resultDistributionCategoryRemotoList);
        remoto.setDescription(languageItemListRemoto);

        boolean resultado = torService.compararResultDistribution(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararReport_SinCambios() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        boolean resultado = torService.compararReport(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararReport_SinCambiosLabel() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(new Date());

        boolean resultado = torService.compararReport(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararReport_ConCambiosLabel() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(new Date(123));

        boolean resultado = torService.compararReport(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararReport_ConCambiosLabel_NullLocal() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        local.setIssueDate(null);
        remoto.setIssueDate(new Date(123));

        boolean resultado = torService.compararReport(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararReport_ConCambiosLabel_NullRemoto() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(null);

        boolean resultado = torService.compararReport(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararReport_ConCambiosAll() {
        ReportMDTO local = new ReportMDTO();
        ReportMDTO remoto = new ReportMDTO();

        LearningOpportunityInstanceMDTO learningOpportunityInstanceLocal = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO learningOpportunityInstanceRemoto = new LearningOpportunityInstanceMDTO();

        GradingSchemeMDTO gradingSchemeLocal = new GradingSchemeMDTO();
        GradingSchemeMDTO gradingSchemeRemoto = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        gradingSchemeLocal.setLabel(languageItemListLocal);
        gradingSchemeLocal.setDescription(languageItemListLocal);

        gradingSchemeRemoto.setLabel(languageItemListRemoto);
        gradingSchemeRemoto.setDescription(languageItemListRemoto);

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        ResultDistributionMDTO resultDistributionLocal = new ResultDistributionMDTO();
        ResultDistributionMDTO resultDistributionRemoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryLocalList = new ArrayList<>();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryRemotoList = new ArrayList<>();

        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);

        resultDistributionCategoryLocalList.add(resultDistributionCategoryLocal);
        resultDistributionCategoryRemotoList.add(resultDistributionCategoryRemoto);

        resultDistributionLocal.setResultDistributionCategory(resultDistributionCategoryLocalList);
        resultDistributionLocal.setDescription(languageItemListLocal);

        resultDistributionRemoto.setResultDistributionCategory(resultDistributionCategoryRemotoList);
        resultDistributionRemoto.setDescription(languageItemListRemoto);

        List<LevelMDTO> levelListLocal = new ArrayList<>();
        List<LevelMDTO> levelListRemoto = new ArrayList<>();

        LevelMDTO levelLocal = new LevelMDTO();
        LevelMDTO levelRemoto = new LevelMDTO();

        levelLocal.setType(LevelType.EQF);
        levelLocal.setValue("TEST");
        levelLocal.setDescription(languageItemListLocal);

        levelRemoto.setType(LevelType.EQF);
        levelRemoto.setValue("TEST2");
        levelRemoto.setDescription(languageItemListRemoto);

        levelListLocal.add(levelLocal);
        levelListRemoto.add(levelRemoto);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        learningOpportunityInstanceLocal.setGradingScheme(gradingSchemeLocal);
        learningOpportunityInstanceLocal.setLanguageOfInstruction("TEST");
        learningOpportunityInstanceLocal.setResultLabel("TEST");
        learningOpportunityInstanceLocal.setEngagementHours(BigDecimal.ONE);
        learningOpportunityInstanceLocal.setStartDate(new Date());
        learningOpportunityInstanceLocal.setEndDate(new Date());
        learningOpportunityInstanceLocal.setPercentageLower((long)1);
        learningOpportunityInstanceLocal.setPercentageEqual((long)1);
        learningOpportunityInstanceLocal.setPercentageHigher((long)1);
        learningOpportunityInstanceLocal.setStatus(LoiStatus.PASSED);
        learningOpportunityInstanceLocal.setResultDistribution(resultDistributionLocal);
        learningOpportunityInstanceLocal.setLevels(levelListLocal);
        learningOpportunityInstanceLocal.setExtension(extensionLocal);
        learningOpportunityInstanceLocal.setLoiGrouping(generarLoiGroupingLocal());
        learningOpportunityInstanceLocal.setAttachments(attachmentMDTOListLocal);
        learningOpportunityInstanceLocal.setDiploma(generarDiplomaLocal());
        learningOpportunityInstanceLocal.setLearningOpportunitySpecification(generarLOSLocal());

        learningOpportunityInstanceRemoto.setGradingScheme(gradingSchemeRemoto);
        learningOpportunityInstanceRemoto.setLanguageOfInstruction("TEST2");
        learningOpportunityInstanceRemoto.setResultLabel("TEST2");
        learningOpportunityInstanceRemoto.setEngagementHours(BigDecimal.TEN);
        learningOpportunityInstanceRemoto.setStartDate(new Date(123));
        learningOpportunityInstanceRemoto.setEndDate(new Date(123));
        learningOpportunityInstanceRemoto.setPercentageLower((long)2);
        learningOpportunityInstanceRemoto.setPercentageEqual((long)2);
        learningOpportunityInstanceRemoto.setPercentageHigher((long)2);
        learningOpportunityInstanceRemoto.setStatus(LoiStatus.FAILED);
        learningOpportunityInstanceRemoto.setResultDistribution(resultDistributionRemoto);
        learningOpportunityInstanceRemoto.setLevels(levelListRemoto);
        learningOpportunityInstanceRemoto.setExtension(extensionRemoto);
        learningOpportunityInstanceRemoto.setLoiGrouping(generarLoiGroupingRemoto());
        learningOpportunityInstanceRemoto.setAttachments(attachmentMDTOListRemoto);
        learningOpportunityInstanceRemoto.setDiploma(generarDiplomaRemoto());
        learningOpportunityInstanceRemoto.setLearningOpportunitySpecification(generarLOSRemoto());

        List<LearningOpportunityInstanceMDTO> loiLocal = new ArrayList<>();
        List<LearningOpportunityInstanceMDTO> loiRemoto = new ArrayList<>();

        loiLocal.add(learningOpportunityInstanceLocal);
        loiRemoto.add(learningOpportunityInstanceRemoto);

        local.setIssueDate(new Date());
        local.setLois(loiLocal);
        local.setAttachments(attachmentMDTOListLocal);

        remoto.setIssueDate(new Date(123));
        remoto.setLois(loiRemoto);
        remoto.setAttachments(attachmentMDTOListRemoto);

        boolean resultado = torService.compararReport(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoiGrouping_SinCambios() {
        LoiGroupingMDTO local = new LoiGroupingMDTO();
        LoiGroupingMDTO remoto = new LoiGroupingMDTO();

        boolean resultado = torService.compararLoiGrouping(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoiGrouping_ConCambiosAll() {
        LoiGroupingMDTO local = new LoiGroupingMDTO();
        LoiGroupingMDTO remoto = new LoiGroupingMDTO();

        GroupMDTO groupLocal = new GroupMDTO();
        GroupMDTO groupRemoto = new GroupMDTO();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        groupLocal.setSortingKey("TEST");
        groupLocal.setTitle(languageItemMDTOLocal);

        groupRemoto.setSortingKey("TEST2");
        groupRemoto.setTitle(languageItemMDTORemoto);

        GroupTypeMDTO groupTypeLocal = new GroupTypeMDTO();
        GroupTypeMDTO groupTypeRemoto = new GroupTypeMDTO();

        List<GroupMDTO> groupMDTOListLocal = new ArrayList<>();
        List<GroupMDTO> groupMDTOListRemoto = new ArrayList<>();

        GroupMDTO groupMDTOLocal = new GroupMDTO();
        GroupMDTO groupMDTORemoto = new GroupMDTO();

        groupMDTOLocal.setSortingKey("TEST");
        groupMDTOLocal.setTitle(languageItemMDTOLocal);

        groupMDTORemoto.setSortingKey("TEST2");
        groupMDTORemoto.setTitle(languageItemMDTORemoto);

        groupMDTOListLocal.add(groupMDTOLocal);
        groupMDTOListRemoto.add(groupMDTORemoto);

        groupTypeLocal.setGroups(groupMDTOListLocal);
        groupTypeLocal.setTitle(languageItemMDTOLocal);

        groupTypeRemoto.setGroups(groupMDTOListRemoto);
        groupTypeRemoto.setTitle(languageItemMDTORemoto);

        local.setGroup(groupLocal);
        local.setGroupType(groupTypeLocal);

        remoto.setGroup(groupRemoto);
        remoto.setGroupType(groupTypeRemoto);

        boolean resultado = torService.compararLoiGrouping(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambios() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosLanguageOfInstruction() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosLanguageOfInstruction() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction("TEST");
        remoto.setLanguageOfInstruction("TEST2");

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosLanguageOfInstruction_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction(null);
        remoto.setLanguageOfInstruction("TEST2");

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosLanguageOfInstruction_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setLanguageOfInstruction("TEST");
        remoto.setLanguageOfInstruction(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosEngagementHours() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(BigDecimal.ONE);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEngagementHours() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(BigDecimal.TEN);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEngagementHours_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(null);
        remoto.setEngagementHours(BigDecimal.TEN);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEngagementHours_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEngagementHours(BigDecimal.ONE);
        remoto.setEngagementHours(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosStartDate() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date());

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStartDate() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date(123));

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStartDate_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStartDate(null);
        remoto.setStartDate(new Date(123));

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStartDate_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosEndDate() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date());

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEndDate() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date(123));

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEndDate_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEndDate(null);
        remoto.setEndDate(new Date(123));

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosEndDate_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosPercentageLower() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageLower((long)1);
        remoto.setPercentageLower((long)1);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageLower() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageLower((long)1);
        remoto.setPercentageLower((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageLower_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageLower(null);
        remoto.setPercentageLower((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageLower_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageLower((long)1);
        remoto.setPercentageLower(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosPercentageEqual() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageEqual((long)1);
        remoto.setPercentageEqual((long)1);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageEqual() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageEqual((long)1);
        remoto.setPercentageEqual((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageEqual_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageEqual(null);
        remoto.setPercentageEqual((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageEqual_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageEqual((long)1);
        remoto.setPercentageEqual(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosPercentageHigher() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageHigher((long)1);
        remoto.setPercentageHigher((long)1);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageHigher() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageHigher((long)1);
        remoto.setPercentageHigher((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageHigher_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageHigher(null);
        remoto.setPercentageHigher((long)2);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosPercentageHigher_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setPercentageHigher((long)1);
        remoto.setPercentageHigher(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosResultLabel() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setResultLabel("TEST");
        remoto.setResultLabel("TEST");

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosResultLabel() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setResultLabel("TEST");
        remoto.setResultLabel("TEST2");

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosResultLabel_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setResultLabel(null);
        remoto.setResultLabel("TEST2");

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosResultLabel_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setResultLabel("TEST");
        remoto.setResultLabel(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosStatus() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStatus(LoiStatus.PASSED);
        remoto.setStatus(LoiStatus.PASSED);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStatus() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStatus(LoiStatus.PASSED);
        remoto.setStatus(LoiStatus.FAILED);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStatus_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStatus(null);
        remoto.setStatus(LoiStatus.FAILED);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosStatus_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        local.setStatus(LoiStatus.PASSED);
        remoto.setStatus(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLoi_SinCambiosExtension() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosExtension() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosExtension_NullLocal() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        byte[] extensionRemoto = new byte[2];

        local.setExtension(null);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLoi_ConCambiosExtension_NullRemoto() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        byte[] extensionLocal = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(null);

        boolean resultado = torService.compararLoi(remoto, local);

        assertFalse(resultado);
    }

    public LoiGroupingMDTO generarLoiGroupingLocal(){
        LoiGroupingMDTO local = new LoiGroupingMDTO();

        GroupMDTO groupLocal = new GroupMDTO();

        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        groupLocal.setSortingKey("TEST");
        groupLocal.setTitle(languageItemMDTOLocal);

        GroupTypeMDTO groupTypeLocal = new GroupTypeMDTO();
        List<GroupMDTO> groupMDTOListLocal = new ArrayList<>();

        GroupMDTO groupMDTOLocal = new GroupMDTO();

        groupMDTOLocal.setSortingKey("TEST");
        groupMDTOLocal.setTitle(languageItemMDTOLocal);

        groupMDTOListLocal.add(groupMDTOLocal);
        groupTypeLocal.setGroups(groupMDTOListLocal);
        groupTypeLocal.setTitle(languageItemMDTOLocal);

        local.setGroup(groupLocal);
        local.setGroupType(groupTypeLocal);

        return local;
    }

    public LoiGroupingMDTO generarLoiGroupingRemoto(){
        LoiGroupingMDTO remoto = new LoiGroupingMDTO();
        GroupMDTO groupRemoto = new GroupMDTO();

        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        groupRemoto.setSortingKey("TEST2");
        groupRemoto.setTitle(languageItemMDTORemoto);

        GroupTypeMDTO groupTypeRemoto = new GroupTypeMDTO();

        List<GroupMDTO> groupMDTOListRemoto = new ArrayList<>();
        GroupMDTO groupMDTORemoto = new GroupMDTO();

        groupMDTORemoto.setSortingKey("TEST2");
        groupMDTORemoto.setTitle(languageItemMDTORemoto);

        groupMDTOListRemoto.add(groupMDTORemoto);
        groupTypeRemoto.setGroups(groupMDTOListRemoto);
        groupTypeRemoto.setTitle(languageItemMDTORemoto);

        remoto.setGroup(groupRemoto);
        remoto.setGroupType(groupTypeRemoto);

        return remoto;
    }

    public DiplomaMDTO generarDiplomaLocal(){
        DiplomaMDTO local = new DiplomaMDTO();
        byte[] byteLocal = new byte[1];
        List<DiplomaSectionMDTO> diplomaSectionListLocal = new ArrayList<>();

        DiplomaSectionMDTO diplomaSectionLocal = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];

        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListLocal = new ArrayList<>();
        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoLocal = new DiplomaAdditionalInfoMDTO();

        diplomaAdditionalInfoLocal.setAdditionalInfo("TEST");
        diplomaAdditionalInfoListLocal.add(diplomaAdditionalInfoLocal);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());

        diplomaSectionLocal.setTitle("TEST");
        diplomaSectionLocal.setContent(contentLocal);
        diplomaSectionLocal.setNumber(1);
        diplomaSectionLocal.setAdditionalInfo(diplomaAdditionalInfoListLocal);
        diplomaSectionLocal.setAttachments(attachmentMDTOListLocal);

        diplomaSectionListLocal.add(diplomaSectionLocal);

        local.setDiplomaVersion(1);
        local.setIssueDate(new Date());
        local.setIntroduction(byteLocal);
        local.setSignature(byteLocal);
        local.setSection(diplomaSectionListLocal);

        return local;
    }

    public DiplomaMDTO generarDiplomaRemoto(){
        DiplomaMDTO remoto = new DiplomaMDTO();
        byte[] byteRemoto = new byte[2];

        List<DiplomaSectionMDTO> diplomaSectionListRemoto = new ArrayList<>();
        DiplomaSectionMDTO diplomaSectionRemoto = new DiplomaSectionMDTO();
        byte[] contentRemoto = new byte[2];
        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListRemoto = new ArrayList<>();
        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoRemoto = new DiplomaAdditionalInfoMDTO();
        diplomaAdditionalInfoRemoto.setAdditionalInfo("TEST2");

        diplomaAdditionalInfoListRemoto.add(diplomaAdditionalInfoRemoto);

        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        diplomaSectionRemoto.setTitle("TEST2");
        diplomaSectionRemoto.setContent(contentRemoto);
        diplomaSectionRemoto.setNumber(2);
        diplomaSectionRemoto.setAdditionalInfo(diplomaAdditionalInfoListRemoto);
        diplomaSectionRemoto.setAttachments(attachmentMDTOListRemoto);

        diplomaSectionListRemoto.add(diplomaSectionRemoto);

        remoto.setDiplomaVersion(1);
        remoto.setIssueDate(new Date());
        remoto.setIntroduction(byteRemoto);
        remoto.setSignature(byteRemoto);
        remoto.setSection(diplomaSectionListRemoto);

        return remoto;
    }

    public LearningOpportunitySpecificationMDTO generarLOSLocal(){
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        byte[] contentLocal = new byte[1];
        List<LearningOpportunitySpecificationMDTO> losListLocal = new ArrayList<>();
        losListLocal.add(generarSubLosLocal());

        local.setLosCode("TEST");
        local.setType(LearningOpportunitySpecificationType.COURSE);
        local.setName(languageItemListLocal);
        local.setUrl(languageItemListLocal);
        local.setEqfLevel((byte) 1);
        local.setIscedf("TEST");
        local.setSubjectArea("TEST");
        local.setDescription(languageItemListLocal);
        local.setTopLevelParent(Boolean.TRUE);
        local.setExtension(contentLocal);
        local.setLearningOpportunitySpecifications(losListLocal);

        return local;
    }

    public LearningOpportunitySpecificationMDTO generarLOSRemoto(){
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");
        languageItemListRemoto.add(languageItemMDTORemoto);

        byte[] contentRemoto = new byte[2];

        List<LearningOpportunitySpecificationMDTO> losListRemoto = new ArrayList<>();
        losListRemoto.add(generarSubLosRemoto());

        remoto.setLosCode("TEST2");
        remoto.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setName(languageItemListRemoto);
        remoto.setUrl(languageItemListRemoto);
        remoto.setEqfLevel((byte) 2);
        remoto.setIscedf("TEST2");
        remoto.setSubjectArea("TEST2");
        remoto.setDescription(languageItemListRemoto);
        remoto.setTopLevelParent(Boolean.FALSE);
        remoto.setExtension(contentRemoto);
        remoto.setLearningOpportunitySpecifications(losListRemoto);

        return remoto;
    }

    @Test
    public void testCompararLoi_ConCambiosAll() {
        LearningOpportunityInstanceMDTO local = new LearningOpportunityInstanceMDTO();
        LearningOpportunityInstanceMDTO remoto = new LearningOpportunityInstanceMDTO();

        GradingSchemeMDTO gradingSchemeLocal = new GradingSchemeMDTO();
        GradingSchemeMDTO gradingSchemeRemoto = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        gradingSchemeLocal.setLabel(languageItemListLocal);
        gradingSchemeLocal.setDescription(languageItemListLocal);

        gradingSchemeRemoto.setLabel(languageItemListRemoto);
        gradingSchemeRemoto.setDescription(languageItemListRemoto);

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        ResultDistributionMDTO resultDistributionLocal = new ResultDistributionMDTO();
        ResultDistributionMDTO resultDistributionRemoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryLocalList = new ArrayList<>();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryRemotoList = new ArrayList<>();

        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();
        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);

        resultDistributionCategoryLocalList.add(resultDistributionCategoryLocal);
        resultDistributionCategoryRemotoList.add(resultDistributionCategoryRemoto);

        resultDistributionLocal.setResultDistributionCategory(resultDistributionCategoryLocalList);
        resultDistributionLocal.setDescription(languageItemListLocal);

        resultDistributionRemoto.setResultDistributionCategory(resultDistributionCategoryRemotoList);
        resultDistributionRemoto.setDescription(languageItemListRemoto);

        List<LevelMDTO> levelListLocal = new ArrayList<>();
        List<LevelMDTO> levelListRemoto = new ArrayList<>();

        LevelMDTO levelLocal = new LevelMDTO();
        LevelMDTO levelRemoto = new LevelMDTO();

        levelLocal.setType(LevelType.EQF);
        levelLocal.setValue("TEST");
        levelLocal.setDescription(languageItemListLocal);

        levelRemoto.setType(LevelType.EQF);
        levelRemoto.setValue("TEST2");
        levelRemoto.setDescription(languageItemListRemoto);

        levelListLocal.add(levelLocal);
        levelListRemoto.add(levelRemoto);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        local.setGradingScheme(gradingSchemeLocal);
        local.setLanguageOfInstruction("TEST");
        local.setResultLabel("TEST");
        local.setEngagementHours(BigDecimal.ONE);
        local.setStartDate(new Date());
        local.setEndDate(new Date());
        local.setPercentageLower((long)1);
        local.setPercentageEqual((long)1);
        local.setPercentageHigher((long)1);
        local.setStatus(LoiStatus.PASSED);
        local.setResultDistribution(resultDistributionLocal);
        local.setLevels(levelListLocal);
        local.setExtension(extensionLocal);
        local.setLoiGrouping(generarLoiGroupingLocal());
        local.setAttachments(attachmentMDTOListLocal);
        local.setDiploma(generarDiplomaLocal());
        local.setLearningOpportunitySpecification(generarLOSLocal());

        remoto.setGradingScheme(gradingSchemeRemoto);
        remoto.setLanguageOfInstruction("TEST2");
        remoto.setResultLabel("TEST2");
        remoto.setEngagementHours(BigDecimal.TEN);
        remoto.setStartDate(new Date(123));
        remoto.setEndDate(new Date(123));
        remoto.setPercentageLower((long)2);
        remoto.setPercentageEqual((long)2);
        remoto.setPercentageHigher((long)2);
        remoto.setStatus(LoiStatus.FAILED);
        remoto.setResultDistribution(resultDistributionRemoto);
        remoto.setLevels(levelListRemoto);
        remoto.setExtension(extensionRemoto);
        remoto.setLoiGrouping(generarLoiGroupingRemoto());
        remoto.setAttachments(attachmentMDTOListRemoto);
        remoto.setDiploma(generarDiplomaRemoto());
        remoto.setLearningOpportunitySpecification(generarLOSRemoto());

        boolean resultado = torService.compararLoi(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_SinCambios() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        boolean resultado = torService.compararDiploma(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiploma_SinCambiosDiplomaVersion() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setDiplomaVersion(1);
        remoto.setDiplomaVersion(1);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosDiplomaVersion() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setDiplomaVersion(1);
        remoto.setDiplomaVersion(2);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosDiplomaVersion_NullLocal() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setDiplomaVersion(null);
        remoto.setDiplomaVersion(2);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosDiplomaVersion_NullRemoto() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setDiplomaVersion(1);
        remoto.setDiplomaVersion(null);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_SinCambiosIssueDate() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(new Date());

        boolean resultado = torService.compararDiploma(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIssueDate() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(new Date(123));

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIssueDate_NullLocal() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setIssueDate(null);
        remoto.setIssueDate(new Date(123));

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIssueDate_NullRemoto() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        local.setIssueDate(new Date());
        remoto.setIssueDate(null);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_SinCambiosIntroduction() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];
        byte[] byteRemoto = new byte[1];

        local.setIntroduction(byteLocal);
        remoto.setIntroduction(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIntroduction() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];
        byte[] byteRemoto = new byte[2];

        local.setIntroduction(byteLocal);
        remoto.setIntroduction(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIntroduction_NullLocal() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteRemoto = new byte[2];

        local.setIntroduction(null);
        remoto.setIntroduction(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosIntroduction_NullRemoto() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];

        local.setIntroduction(byteLocal);
        remoto.setIntroduction(null);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_SinCambiosSignature() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];
        byte[] byteRemoto = new byte[1];

        local.setSignature(byteLocal);
        remoto.setSignature(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosSignature() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];
        byte[] byteRemoto = new byte[2];

        local.setSignature(byteLocal);
        remoto.setSignature(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosSignature_NullLocal() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteRemoto = new byte[2];

        local.setSignature(null);
        remoto.setSignature(byteRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosSignature_NullRemoto() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];

        local.setSignature(byteLocal);
        remoto.setSignature(null);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararDiploma_ConCambiosAll() {
        DiplomaMDTO local = new DiplomaMDTO();
        DiplomaMDTO remoto = new DiplomaMDTO();

        byte[] byteLocal = new byte[1];
        byte[] byteRemoto = new byte[2];

        List<DiplomaSectionMDTO> diplomaSectionListLocal = new ArrayList<>();
        List<DiplomaSectionMDTO> diplomaSectionListRemoto = new ArrayList<>();

        DiplomaSectionMDTO diplomaSectionLocal = new DiplomaSectionMDTO();
        DiplomaSectionMDTO diplomaSectionRemoto = new DiplomaSectionMDTO();

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListLocal = new ArrayList<>();
        List<DiplomaAdditionalInfoMDTO> diplomaAdditionalInfoListRemoto = new ArrayList<>();

        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoLocal = new DiplomaAdditionalInfoMDTO();
        DiplomaAdditionalInfoMDTO diplomaAdditionalInfoRemoto = new DiplomaAdditionalInfoMDTO();

        diplomaAdditionalInfoLocal.setAdditionalInfo("TEST");
        diplomaAdditionalInfoRemoto.setAdditionalInfo("TEST2");

        diplomaAdditionalInfoListLocal.add(diplomaAdditionalInfoLocal);
        diplomaAdditionalInfoListRemoto.add(diplomaAdditionalInfoRemoto);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        diplomaSectionLocal.setTitle("TEST");
        diplomaSectionLocal.setContent(contentLocal);
        diplomaSectionLocal.setNumber(1);
        diplomaSectionLocal.setAdditionalInfo(diplomaAdditionalInfoListLocal);
        diplomaSectionLocal.setAttachments(attachmentMDTOListLocal);

        diplomaSectionRemoto.setTitle("TEST2");
        diplomaSectionRemoto.setContent(contentRemoto);
        diplomaSectionRemoto.setNumber(2);
        diplomaSectionRemoto.setAdditionalInfo(diplomaAdditionalInfoListRemoto);
        diplomaSectionRemoto.setAttachments(attachmentMDTOListRemoto);

        diplomaSectionListLocal.add(diplomaSectionLocal);
        diplomaSectionListRemoto.add(diplomaSectionRemoto);

        local.setDiplomaVersion(1);
        local.setIssueDate(new Date());
        local.setIntroduction(byteLocal);
        local.setSignature(byteLocal);
        local.setSection(diplomaSectionListLocal);

        remoto.setDiplomaVersion(1);
        remoto.setIssueDate(new Date());
        remoto.setIntroduction(byteRemoto);
        remoto.setSignature(byteRemoto);
        remoto.setSection(diplomaSectionListRemoto);

        boolean resultado = torService.compararDiploma(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambios() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosLosCode() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST");

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosLosCode() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode("TEST2");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosLosCode_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setLosCode(null);
        remoto.setLosCode("TEST");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosLosCode_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setLosCode("TEST");
        remoto.setLosCode(null);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosType() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setType(LearningOpportunitySpecificationType.MODULE);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosType() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setType(LearningOpportunitySpecificationType.COURSE);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosType_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setType(null);
        remoto.setType(LearningOpportunitySpecificationType.COURSE);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosType_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setType(null);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosEqfLevel() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte)1);
        remoto.setEqfLevel((byte)1);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosEqfLevel() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte)1);
        remoto.setEqfLevel((byte)2);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosEqfLevel_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        remoto.setEqfLevel((byte)2);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosEqfLevel_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setEqfLevel((byte)1);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosIscedf() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf("TEST");

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosIscedf() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf("TEST2");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosIscedf_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setIscedf(null);
        remoto.setIscedf("TEST2");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosIscedf_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setIscedf("TEST");
        remoto.setIscedf(null);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosSubjectArea() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea("TEST");

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosSubjectArea() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea("TEST2");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosSubjectArea_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea(null);
        remoto.setSubjectArea("TEST2");

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosSubjectArea_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setSubjectArea("TEST");
        remoto.setSubjectArea(null);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosExtension() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosExtension() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosExtension_NullLocal() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        byte[] extensionRemoto = new byte[2];

        local.setExtension(null);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosExtension_NullRemoto() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        byte[] extensionLocal = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(null);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_SinCambiosTopLevelParent() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setTopLevelParent(Boolean.TRUE);
        remoto.setTopLevelParent(Boolean.TRUE);

        boolean resultado = torService.compararLos(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLos_ConCambiosTopLevelParent() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        local.setTopLevelParent(Boolean.TRUE);
        remoto.setTopLevelParent(Boolean.FALSE);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    public LearningOpportunitySpecificationMDTO generarSubLosLocal(){
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");
        languageItemListLocal.add(languageItemMDTOLocal);
        byte[] contentLocal = new byte[1];

        local.setLosCode("TEST");
        local.setType(LearningOpportunitySpecificationType.COURSE);
        local.setName(languageItemListLocal);
        local.setUrl(languageItemListLocal);
        local.setEqfLevel((byte) 1);
        local.setIscedf("TEST");
        local.setSubjectArea("TEST");
        local.setDescription(languageItemListLocal);
        local.setTopLevelParent(Boolean.TRUE);
        local.setExtension(contentLocal);

        return local;
    }

    public LearningOpportunitySpecificationMDTO generarSubLosRemoto(){
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        byte[] contentRemoto = new byte[2];

        remoto.setLosCode("TEST2");
        remoto.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setName(languageItemListRemoto);
        remoto.setUrl(languageItemListRemoto);
        remoto.setEqfLevel((byte) 2);
        remoto.setIscedf("TEST2");
        remoto.setSubjectArea("TEST2");
        remoto.setDescription(languageItemListRemoto);
        remoto.setTopLevelParent(Boolean.FALSE);
        remoto.setExtension(contentRemoto);

        return remoto;
    }

    @Test
    public void testCompararLos_ConCambiosTopAll() {
        LearningOpportunitySpecificationMDTO local = new LearningOpportunitySpecificationMDTO();
        LearningOpportunitySpecificationMDTO remoto = new LearningOpportunitySpecificationMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        byte[] contentLocal = new byte[1];
        byte[] contentRemoto = new byte[2];

        List<LearningOpportunitySpecificationMDTO> losListLocal = new ArrayList<>();
        List<LearningOpportunitySpecificationMDTO> losListRemoto = new ArrayList<>();

        losListLocal.add(generarSubLosLocal());
        losListRemoto.add(generarSubLosRemoto());

        local.setLosCode("TEST");
        local.setType(LearningOpportunitySpecificationType.COURSE);
        local.setName(languageItemListLocal);
        local.setUrl(languageItemListLocal);
        local.setEqfLevel((byte) 1);
        local.setIscedf("TEST");
        local.setSubjectArea("TEST");
        local.setDescription(languageItemListLocal);
        local.setTopLevelParent(Boolean.TRUE);
        local.setExtension(contentLocal);
        local.setLearningOpportunitySpecifications(losListLocal);

        remoto.setLosCode("TEST2");
        remoto.setType(LearningOpportunitySpecificationType.MODULE);
        remoto.setName(languageItemListRemoto);
        remoto.setUrl(languageItemListRemoto);
        remoto.setEqfLevel((byte) 2);
        remoto.setIscedf("TEST2");
        remoto.setSubjectArea("TEST2");
        remoto.setDescription(languageItemListRemoto);
        remoto.setTopLevelParent(Boolean.FALSE);
        remoto.setExtension(contentRemoto);
        remoto.setLearningOpportunitySpecifications(losListRemoto);

        boolean resultado = torService.compararLos(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararTors_SinCambios() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararTors_SinCambiosLabel() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        local.setGeneratedDate(new Date());
        remoto.setGeneratedDate(new Date());

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosLabel() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        local.setGeneratedDate(new Date());
        remoto.setGeneratedDate(new Date(123));

        boolean resultado = torService.compararTors(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosLabel_NullLocal() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        local.setGeneratedDate(null);
        remoto.setGeneratedDate(new Date(123));

        boolean resultado = torService.compararTors(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosLabel_NullRemoto() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        local.setGeneratedDate(new Date());
        remoto.setGeneratedDate(null);

        boolean resultado = torService.compararTors(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararTors_SinCambiosExtension() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosExtension() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        local.setExtension(extensionLocal);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosExtension_NullLocal() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        byte[] extensionRemoto = new byte[2];

        local.setExtension(null);
        remoto.setExtension(extensionRemoto);

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararTors_ConCambiosExtension_NullRemoto() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        byte[] extensionLocal = new byte[1];

        local.setExtension(extensionLocal);
        remoto.setExtension(null);

        boolean resultado = torService.compararTors(remoto, local);

        assertFalse(resultado);
    }

    public ReportMDTO testGenerarReportLocal(){
        ReportMDTO local = new ReportMDTO();
        LearningOpportunityInstanceMDTO learningOpportunityInstanceLocal = new LearningOpportunityInstanceMDTO();
        GradingSchemeMDTO gradingSchemeLocal = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST2");
        languageItemMDTOLocal.setLang("TEST2");

        languageItemListLocal.add(languageItemMDTOLocal);

        gradingSchemeLocal.setLabel(languageItemListLocal);
        gradingSchemeLocal.setDescription(languageItemListLocal);

        byte[] extensionLocal = new byte[1];

        ResultDistributionMDTO resultDistributionLocal = new ResultDistributionMDTO();
        List<ResultDistributionCategoryMDTO> resultDistributionCategoryLocalList = new ArrayList<>();
        ResultDistributionCategoryMDTO resultDistributionCategoryLocal = new ResultDistributionCategoryMDTO();

        resultDistributionCategoryLocal.setCount(BigInteger.ONE);
        resultDistributionCategoryLocalList.add(resultDistributionCategoryLocal);

        resultDistributionLocal.setResultDistributionCategory(resultDistributionCategoryLocalList);
        resultDistributionLocal.setDescription(languageItemListLocal);

        List<LevelMDTO> levelListLocal = new ArrayList<>();

        LevelMDTO levelLocal = new LevelMDTO();

        levelLocal.setType(LevelType.EQF);
        levelLocal.setValue("TEST");
        levelLocal.setDescription(languageItemListLocal);

        levelListLocal.add(levelLocal);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());

        learningOpportunityInstanceLocal.setGradingScheme(gradingSchemeLocal);
        learningOpportunityInstanceLocal.setLanguageOfInstruction("TEST");
        learningOpportunityInstanceLocal.setResultLabel("TEST");
        learningOpportunityInstanceLocal.setEngagementHours(BigDecimal.ONE);
        learningOpportunityInstanceLocal.setStartDate(new Date());
        learningOpportunityInstanceLocal.setEndDate(new Date());
        learningOpportunityInstanceLocal.setPercentageLower((long)1);
        learningOpportunityInstanceLocal.setPercentageEqual((long)1);
        learningOpportunityInstanceLocal.setPercentageHigher((long)1);
        learningOpportunityInstanceLocal.setStatus(LoiStatus.PASSED);
        learningOpportunityInstanceLocal.setResultDistribution(resultDistributionLocal);
        learningOpportunityInstanceLocal.setLevels(levelListLocal);
        learningOpportunityInstanceLocal.setExtension(extensionLocal);
        learningOpportunityInstanceLocal.setLoiGrouping(generarLoiGroupingLocal());
        learningOpportunityInstanceLocal.setAttachments(attachmentMDTOListLocal);
        learningOpportunityInstanceLocal.setDiploma(generarDiplomaLocal());
        learningOpportunityInstanceLocal.setLearningOpportunitySpecification(generarLOSLocal());

        List<LearningOpportunityInstanceMDTO> loiLocal = new ArrayList<>();
        List<LearningOpportunityInstanceMDTO> loiRemoto = new ArrayList<>();

        loiLocal.add(learningOpportunityInstanceLocal);

        local.setIssueDate(new Date());
        local.setLois(loiLocal);
        local.setAttachments(attachmentMDTOListLocal);

        return local;
    }

    public ReportMDTO testGenerarReportRemoto(){
        ReportMDTO remoto = new ReportMDTO();
        LearningOpportunityInstanceMDTO learningOpportunityInstanceRemoto = new LearningOpportunityInstanceMDTO();
        GradingSchemeMDTO gradingSchemeRemoto = new GradingSchemeMDTO();

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        gradingSchemeRemoto.setLabel(languageItemListRemoto);
        gradingSchemeRemoto.setDescription(languageItemListRemoto);
        byte[] extensionRemoto = new byte[2];
        ResultDistributionMDTO resultDistributionRemoto = new ResultDistributionMDTO();

        List<ResultDistributionCategoryMDTO> resultDistributionCategoryRemotoList = new ArrayList<>();

        ResultDistributionCategoryMDTO resultDistributionCategoryRemoto = new ResultDistributionCategoryMDTO();
        resultDistributionCategoryRemoto.setCount(BigInteger.TEN);
        resultDistributionCategoryRemotoList.add(resultDistributionCategoryRemoto);

        resultDistributionRemoto.setResultDistributionCategory(resultDistributionCategoryRemotoList);
        resultDistributionRemoto.setDescription(languageItemListRemoto);

        List<LevelMDTO> levelListRemoto = new ArrayList<>();

        LevelMDTO levelRemoto = new LevelMDTO();

        levelRemoto.setType(LevelType.EQF);
        levelRemoto.setValue("TEST2");
        levelRemoto.setDescription(languageItemListRemoto);

        levelListRemoto.add(levelRemoto);

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        learningOpportunityInstanceRemoto.setGradingScheme(gradingSchemeRemoto);
        learningOpportunityInstanceRemoto.setLanguageOfInstruction("TEST2");
        learningOpportunityInstanceRemoto.setResultLabel("TEST2");
        learningOpportunityInstanceRemoto.setEngagementHours(BigDecimal.TEN);
        learningOpportunityInstanceRemoto.setStartDate(new Date(123));
        learningOpportunityInstanceRemoto.setEndDate(new Date(123));
        learningOpportunityInstanceRemoto.setPercentageLower((long)2);
        learningOpportunityInstanceRemoto.setPercentageEqual((long)2);
        learningOpportunityInstanceRemoto.setPercentageHigher((long)2);
        learningOpportunityInstanceRemoto.setStatus(LoiStatus.FAILED);
        learningOpportunityInstanceRemoto.setResultDistribution(resultDistributionRemoto);
        learningOpportunityInstanceRemoto.setLevels(levelListRemoto);
        learningOpportunityInstanceRemoto.setExtension(extensionRemoto);
        learningOpportunityInstanceRemoto.setLoiGrouping(generarLoiGroupingRemoto());
        learningOpportunityInstanceRemoto.setAttachments(attachmentMDTOListRemoto);
        learningOpportunityInstanceRemoto.setDiploma(generarDiplomaRemoto());
        learningOpportunityInstanceRemoto.setLearningOpportunitySpecification(generarLOSRemoto());

        List<LearningOpportunityInstanceMDTO> loiRemoto = new ArrayList<>();
        loiRemoto.add(learningOpportunityInstanceRemoto);
        remoto.setIssueDate(new Date(123));
        remoto.setLois(loiRemoto);
        remoto.setAttachments(attachmentMDTOListRemoto);

        return remoto;
    }
    @Test
    public void testCompararTors_ConCambiosAll() {
        TranscriptOfRecordMDTO local = new TranscriptOfRecordMDTO();
        TranscriptOfRecordMDTO remoto = new TranscriptOfRecordMDTO();

        byte[] extensionLocal = new byte[1];
        byte[] extensionRemoto = new byte[2];

        List<ReportMDTO> reportListLocal = new ArrayList<>();
        List<ReportMDTO> reportListRemoto = new ArrayList<>();

        reportListLocal.add(testGenerarReportLocal());
        reportListRemoto.add(testGenerarReportRemoto());

        List<AttachmentMDTO> attachmentMDTOListLocal = new ArrayList<>();
        List<AttachmentMDTO> attachmentMDTOListRemoto = new ArrayList<>();

        attachmentMDTOListLocal.add(generarAttachmentMDTOLocal());
        attachmentMDTOListRemoto.add(generarAttachmentMDTORemoto());

        IscedTableMDTO iscedTableLocal = new IscedTableMDTO();
        IscedTableMDTO iscedTableRemoto = new IscedTableMDTO();

        List<GradeFrequencyMDTO> frequenciesListLocal = new ArrayList<>();
        List<GradeFrequencyMDTO> frequenciesListRemoto = new ArrayList<>();

        GradeFrequencyMDTO frequenciesLocal = new GradeFrequencyMDTO();
        GradeFrequencyMDTO frequenciesRemoto = new GradeFrequencyMDTO();

        frequenciesLocal.setLabel("TEST");
        frequenciesLocal.setPercentage((long)1);

        frequenciesRemoto.setLabel("TEST2");
        frequenciesRemoto.setPercentage((long)2);

        frequenciesListLocal.add(frequenciesLocal);
        frequenciesListRemoto.add(frequenciesRemoto);

        iscedTableLocal.setIscedCode("TEST");
        iscedTableLocal.setFrequencies(frequenciesListLocal);

        iscedTableRemoto.setIscedCode("TEST2");
        iscedTableRemoto.setFrequencies(frequenciesListRemoto);

        List<IscedTableMDTO> iscedTableListLocal = new ArrayList<>();
        List<IscedTableMDTO> iscedTableListRemoto = new ArrayList<>();

        iscedTableListLocal.add(iscedTableLocal);
        iscedTableListRemoto.add(iscedTableRemoto);

        local.setGeneratedDate(new Date());
        local.setExtension(extensionLocal);
        local.setReport(reportListLocal);
        local.setAttachments(attachmentMDTOListLocal);
        local.setIscedTable(iscedTableListLocal);

        remoto.setGeneratedDate(new Date(123));
        remoto.setExtension(extensionRemoto);
        remoto.setReport(reportListRemoto);
        remoto.setAttachments(attachmentMDTOListRemoto);
        remoto.setIscedTable(iscedTableListRemoto);

        boolean resultado = torService.compararTors(remoto, local);

        assertTrue(resultado);
    }
}