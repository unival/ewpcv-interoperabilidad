package es.minsait.ewpcv.service.impl;

import es.minsait.ewpcv.converter.api.IIiaConverter;
import es.minsait.ewpcv.model.iia.DurationUnitVariants;
import es.minsait.ewpcv.model.iia.MobilityNumberVariants;
import es.minsait.ewpcv.repository.dao.IIiaDAO;
import es.minsait.ewpcv.repository.dao.IIiaRegressionsDAO;
import es.minsait.ewpcv.repository.model.iia.*;
import es.minsait.ewpcv.repository.model.omobility.LanguageSkillMDTO;
import es.minsait.ewpcv.repository.model.omobility.SubjectAreaMDTO;
import es.minsait.ewpcv.repository.model.organization.*;
import es.minsait.ewpcv.service.api.INotificationService;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class IIiasServiceImplCompararTest extends TestCase {

    @Autowired
    private IIiaDAO iiaDAO;
    @Autowired
    private IIiaConverter iiaConverter;
    @Autowired
    private EventServiceImpl eventService;
    @Autowired
    private INotificationService notificationService;
    @Autowired
    private IIiaRegressionsDAO iiaRegressionsDAO;
    private IIiasServiceImpl iiasServiceImpl = new IIiasServiceImpl(iiaDAO, iiaConverter, eventService, notificationService, iiaRegressionsDAO);

    @Test
    public void testCompararPhoneNumber_SinCambios() {
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararContactDetails(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosE164() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setE164("PRUEBA CAMBIO 1");
        remoto.setE164("PRUEBA CAMBIO 2");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosE164NullLocal() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setE164(null);
        remoto.setE164("PRUEBA CAMBIO 2");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosE164_NullRemoto() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setE164("PRUEBA CAMBIO 1");
        remoto.setE164(null);

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosExtensionNumber() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setExtensionNumber("+34");
        remoto.setExtensionNumber("+85");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosExtensionNumber_NullLocal() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setExtensionNumber(null);
        remoto.setExtensionNumber("+85");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosExtensionNumber_NullRemoto() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setExtensionNumber("+34");
        remoto.setExtensionNumber(null);

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosOtherFormat() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setOtherFormat("us");
        remoto.setOtherFormat("es");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosOtherFormat_NullLocal() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setOtherFormat(null);
        remoto.setOtherFormat("es");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosOtherFormat_NullRemoto() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setOtherFormat("us");
        remoto.setOtherFormat(null);

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhoneNumber_ConCambiosAll() {
        PhoneNumberMDTO local = new PhoneNumberMDTO();
        PhoneNumberMDTO remoto = new PhoneNumberMDTO();

        local.setE164("PRUEBA CAMBIO 1");
        local.setExtensionNumber("+34");
        local.setOtherFormat("us");

        remoto.setE164("PRUEBA CAMBIO 2");
        remoto.setExtensionNumber("+85");
        remoto.setOtherFormat("es");

        boolean resultado = iiasServiceImpl.compararPhoneNumber(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambios() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosAddressLine() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test");

        local.setAddressLine(listaLocal);
        remoto.setAddressLine(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAddressLine() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setAddressLine(listaLocal);
        remoto.setAddressLine(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAddressLine_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add(null);
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setAddressLine(listaLocal);
        remoto.setAddressLine(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAddressLine_NullLocal2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setAddressLine(null);
        remoto.setAddressLine(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAddressLine_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add(null);

        local.setAddressLine(listaLocal);
        remoto.setAddressLine(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAddressLine_NullRemoto2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");

        local.setAddressLine(listaLocal);
        remoto.setAddressLine(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosDeliveryPointCode() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test");

        local.setDeliveryPointCode(listaLocal);
        remoto.setDeliveryPointCode(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosDeliveryPointCode() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setDeliveryPointCode(listaLocal);
        remoto.setDeliveryPointCode(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosDeliveryPointCode_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add(null);
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setDeliveryPointCode(listaLocal);
        remoto.setDeliveryPointCode(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosDeliveryPointCode_NullLocal2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setDeliveryPointCode(null);
        remoto.setDeliveryPointCode(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosDeliveryPointCode_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add(null);

        local.setDeliveryPointCode(listaLocal);
        remoto.setDeliveryPointCode(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosDeliveryPointCode_NullRemoto2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");

        local.setDeliveryPointCode(listaLocal);
        remoto.setDeliveryPointCode(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosRecipientName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test");

        local.setRecipientName(listaLocal);
        remoto.setRecipientName(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRecipientName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setRecipientName(listaLocal);
        remoto.setRecipientName(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRecipientName_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add(null);

        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("TEST2");

        local.setRecipientName(listaLocal);
        remoto.setRecipientName(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRecipientName_NullLocal2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add("Test2");

        local.setRecipientName(null);
        remoto.setRecipientName(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRecipientName_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");
        List<String> listaRemoto = new ArrayList<>();
        listaRemoto.add(null);

        local.setRecipientName(listaLocal);
        remoto.setRecipientName(listaRemoto);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRecipientName_NullRemoto2() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        List<String> listaLocal = new ArrayList<>();
        listaLocal.add("Test");

        local.setRecipientName(listaLocal);
        remoto.setRecipientName(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosBuildingName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingName("Test");
        remoto.setBuildingName("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingName("Test");
        remoto.setBuildingName("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingName_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingName(null);
        remoto.setBuildingName("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingName_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingName("Test");
        remoto.setBuildingName(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosBuildingNumber() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingNumber("Test");
        remoto.setBuildingNumber("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingNumber() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingNumber("Test");
        remoto.setBuildingNumber("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingNumber_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingNumber(null);
        remoto.setBuildingNumber("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosBuildingNumber_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setBuildingNumber("Test");
        remoto.setBuildingNumber(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosCountry() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setCountry("Test");
        remoto.setCountry("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosCountry() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setCountry("Test");
        remoto.setCountry("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosCountry_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setCountry(null);
        remoto.setCountry("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosCountry_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setCountry("Test");
        remoto.setCountry(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosFloor() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setFloor("Test");
        remoto.setFloor("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosFloor() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setFloor("Test");
        remoto.setFloor("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosFloor_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setFloor(null);
        remoto.setFloor("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosFloor_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setFloor("Test");
        remoto.setFloor(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosLocality() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setLocality("Test");
        remoto.setLocality("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosLocality() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setLocality("Test");
        remoto.setLocality("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosLocality_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setLocality(null);
        remoto.setLocality("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosLocality_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setLocality("Test");
        remoto.setLocality(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosPostalCode() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostalCode("Test");
        remoto.setPostalCode("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostalCode() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostalCode("Test");
        remoto.setPostalCode("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostalCode_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostalCode(null);
        remoto.setPostalCode("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostalCode_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostalCode("Test");
        remoto.setPostalCode(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosPostOfficeBox() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostOfficeBox("Test");
        remoto.setPostOfficeBox("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostOfficeBox() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostOfficeBox("Test");
        remoto.setPostOfficeBox("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostOfficeBox_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostOfficeBox(null);
        remoto.setPostOfficeBox("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosPostOfficeBox_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setPostOfficeBox("Test");
        remoto.setPostOfficeBox(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosRegion() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setRegion("Test");
        remoto.setRegion("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRegion() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setRegion("Test");
        remoto.setRegion("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRegion_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setRegion(null);
        remoto.setRegion("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosRegion_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setRegion("Test");
        remoto.setRegion(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosStreetName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setStreetName("Test");
        remoto.setStreetName("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosStreetName() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setStreetName("Test");
        remoto.setStreetName("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosStreetName_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setStreetName(null);
        remoto.setStreetName("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosStreetName_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setStreetName("Test");
        remoto.setStreetName(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_SinCambiosUnit() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setUnit("Test");
        remoto.setUnit("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosUnit() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setUnit("Test");
        remoto.setUnit("Test2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosUnit_NullLocal() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setUnit(null);
        remoto.setUnit("Test");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosUnit_NullRemoto() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();

        local.setUnit("Test");
        remoto.setUnit(null);

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararFlexibleAdress_ConCambiosAll() {
        FlexibleAddressMDTO local = new FlexibleAddressMDTO();
        FlexibleAddressMDTO remoto = new FlexibleAddressMDTO();
        List<String> localLista = new ArrayList<>();
        localLista.add("TEST");

        local.setRecipientName(localLista);
        local.setAddressLine(localLista);
        local.setBuildingNumber("TEST");
        local.setBuildingName("TEST");
        local.setStreetName("TEST");
        local.setUnit("TEST");
        local.setFloor("TEST");
        local.setPostOfficeBox("TEST");
        local.setDeliveryPointCode(localLista);
        local.setPostalCode("TEST");
        local.setLocality("TEST");
        local.setRegion("TEST");
        local.setCountry("TEST");

        List<String> remotoLista = new ArrayList<>();
        remotoLista.add("TEST2");

        remoto.setRecipientName(remotoLista);
        remoto.setAddressLine(remotoLista);
        remoto.setBuildingNumber("TEST2");
        remoto.setBuildingName("TEST2");
        remoto.setStreetName("TEST2");
        remoto.setUnit("TEST2");
        remoto.setFloor("TEST2");
        remoto.setPostOfficeBox("TEST2");
        remoto.setDeliveryPointCode(remotoLista);
        remoto.setPostalCode("TEST2");
        remoto.setLocality("TEST2");
        remoto.setRegion("TEST2");
        remoto.setCountry("TEST2");

        boolean resultado = iiasServiceImpl.compararFlexibleAdress(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPhotoUrl_SinCambios() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPhotoUrl_SinCambiosDate() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setDate(new Date());
        remoto.setDate(new Date());

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosDate() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setDate(new Date());
        remoto.setDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getDate(), local.getDate());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosDate_NullLocal() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setDate(null);
        remoto.setDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getDate(), local.getDate());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosDate_NullRemoto() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setDate(new Date());
        remoto.setDate(null);

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getDate(), local.getDate());
    }

    @Test
    public void testCompararPhotoUrl_SinCambiosSize() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setSize("Test");
        remoto.setSize("Test");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosSize() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setSize("Test");
        remoto.setSize("Test2");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getSize(), local.getSize());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosSize_NullLocal() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setSize(null);
        remoto.setSize("Test2");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getSize(), local.getSize());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosSize_NullRemoto() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setSize("Test");
        remoto.setSize(null);

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getSize(), local.getSize());
    }

    @Test
    public void testCompararPhotoUrl_SinCambiosUrl() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setUrl("Test");
        remoto.setUrl("Test");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosUrl() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setUrl("Test");
        remoto.setUrl("Test2");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getUrl(), local.getUrl());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosUrl_NullLocal() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setUrl(null);
        remoto.setUrl("Test2");

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getUrl(), local.getUrl());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosUrl_NullRemoto() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setUrl("Test");
        remoto.setUrl(null);

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getUrl(), local.getUrl());
    }

    @Test
    public void testCompararPhotoUrl_ConCambiosAll() {
        PhotoUrlMDTO local = new PhotoUrlMDTO();
        PhotoUrlMDTO remoto = new PhotoUrlMDTO();
        boolean actualizarLocal = true;

        local.setUrl("TEST");
        local.setSize("TEST");
        local.setDate(new Date());

        remoto.setUrl("TEST2");
        remoto.setSize("TEST2");
        remoto.setDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPhotoUrl(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getUrl(), local.getUrl());
    }

    @Test
    public void testCompararLanguageItems_SinCambios() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageItems_SinCambiosLang() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setLang("Test");
        remoto.setLang("Test");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageItems_ConCambiosLang() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setLang("Test");
        remoto.setLang("Test2");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLang(), local.getLang());
    }

    @Test
    public void testCompararLanguageItems_ConCambiosLang_NullLocal() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setLang(null);
        remoto.setLang("Test2");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLang(), local.getLang());
    }

    @Test
    public void testCompararLanguageItems_ConCambiosLang_NullRemoto() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setLang("Test");
        remoto.setLang(null);

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLang(), local.getLang());
    }

    @Test
    public void testCompararLanguageItems_SinCambiosText() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setText("Test");
        remoto.setText("Test");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageItems_ConCambiosText() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setText("Test");
        remoto.setText("Test2");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getText(), local.getText());
    }

    @Test
    public void testCompararLanguageItems_ConCambiosText_NullLocal() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setText(null);
        remoto.setText("Test2");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getText(), local.getText());
    }

    @Test
    public void testCompararLanguageItems_ConCambiosText_NullRemoto() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setText("Test");
        remoto.setText(null);

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getText(), local.getText());
    }

    @Test
    public void testCompararLanguageItems_ConCambiosAll() {
        LanguageItemMDTO local = new LanguageItemMDTO();
        LanguageItemMDTO remoto = new LanguageItemMDTO();
        boolean actualizarLocal = true;

        local.setText("TEST");
        local.setLang("TEST");

        remoto.setText("TEST2");
        remoto.setLang("TEST2");

        boolean resultado = iiasServiceImpl.compararLanguageItems(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getText(), local.getText());
    }

    @Test
    public void testCompararContactDetails_SinCambios() {
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararContactDetails(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararContactDetails_ConCambiosEmail() {
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        List<String> localLista = new ArrayList<>();
        localLista.add("Test");
        List<String> remotoLista = new ArrayList<>();
        remotoLista.add("Test2");

        local.setEmail(localLista);
        remoto.setEmail(remotoLista);
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararContactDetails(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getEmail(), local.getEmail());
    }

    @Test
    public void testCompararContactDetails_ConCambiosAll() {
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        List<String> localLista = new ArrayList<>();
        List<String> remotoLista = new ArrayList<>();
        localLista.add("Test");
        remotoLista.add("Test2");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        PhoneNumberMDTO phoneNumberMDTOLocal = new PhoneNumberMDTO();
        PhoneNumberMDTO phoneNumberMDTORemoto = new PhoneNumberMDTO();

        phoneNumberMDTOLocal.setE164("PRUEBA CAMBIO 1");
        phoneNumberMDTOLocal.setExtensionNumber("+34");
        phoneNumberMDTOLocal.setOtherFormat("us");

        phoneNumberMDTORemoto.setE164("PRUEBA CAMBIO 2");
        phoneNumberMDTORemoto.setExtensionNumber("+85");
        phoneNumberMDTORemoto.setOtherFormat("es");

        FlexibleAddressMDTO flexibleAddressMDTOLocal = new FlexibleAddressMDTO();
        FlexibleAddressMDTO flexibleAddressMDTORemoto = new FlexibleAddressMDTO();


        flexibleAddressMDTOLocal.setRecipientName(localLista);
        flexibleAddressMDTOLocal.setAddressLine(localLista);
        flexibleAddressMDTOLocal.setBuildingNumber("TEST");
        flexibleAddressMDTOLocal.setBuildingName("TEST");
        flexibleAddressMDTOLocal.setStreetName("TEST");
        flexibleAddressMDTOLocal.setUnit("TEST");
        flexibleAddressMDTOLocal.setFloor("TEST");
        flexibleAddressMDTOLocal.setPostOfficeBox("TEST");
        flexibleAddressMDTOLocal.setDeliveryPointCode(localLista);
        flexibleAddressMDTOLocal.setPostalCode("TEST");
        flexibleAddressMDTOLocal.setLocality("TEST");
        flexibleAddressMDTOLocal.setRegion("TEST");
        flexibleAddressMDTOLocal.setCountry("TEST");

        flexibleAddressMDTORemoto.setRecipientName(remotoLista);
        flexibleAddressMDTORemoto.setAddressLine(remotoLista);
        flexibleAddressMDTORemoto.setBuildingNumber("TEST2");
        flexibleAddressMDTORemoto.setBuildingName("TEST2");
        flexibleAddressMDTORemoto.setStreetName("TEST2");
        flexibleAddressMDTORemoto.setUnit("TEST2");
        flexibleAddressMDTORemoto.setFloor("TEST2");
        flexibleAddressMDTORemoto.setPostOfficeBox("TEST2");
        flexibleAddressMDTORemoto.setDeliveryPointCode(remotoLista);
        flexibleAddressMDTORemoto.setPostalCode("TEST2");
        flexibleAddressMDTORemoto.setLocality("TEST2");
        flexibleAddressMDTORemoto.setRegion("TEST2");
        flexibleAddressMDTORemoto.setCountry("TEST2");

        List<PhotoUrlMDTO> photoUrlMDTOListLocal = new ArrayList<>();
        List<PhotoUrlMDTO> photoUrlMDTOListRemoto = new ArrayList<>();
        PhotoUrlMDTO photoUrlMDTOLocal = new PhotoUrlMDTO();
        PhotoUrlMDTO photoUrlMDTORemoto = new PhotoUrlMDTO();

        photoUrlMDTOLocal.setUrl("TEST");
        photoUrlMDTOLocal.setSize("TEST");
        photoUrlMDTOLocal.setDate(new Date());

        photoUrlMDTORemoto.setUrl("TEST2");
        photoUrlMDTORemoto.setSize("TEST2");
        photoUrlMDTORemoto.setDate(new Date(123));
        photoUrlMDTOListLocal.add(photoUrlMDTOLocal);
        photoUrlMDTOListRemoto.add(photoUrlMDTORemoto);

        local.setEmail(localLista);
        local.setUrl(languageItemListLocal);
        local.setPhoneNumber(phoneNumberMDTOLocal);
        local.setFaxNumber(phoneNumberMDTOLocal);
        local.setStreetAddress(flexibleAddressMDTOLocal);
        local.setMailingAddress(flexibleAddressMDTOLocal);
        local.setPhotoUrls(photoUrlMDTOListLocal);

        remoto.setEmail(remotoLista);
        remoto.setUrl(languageItemListRemoto);
        remoto.setPhoneNumber(phoneNumberMDTORemoto);
        remoto.setFaxNumber(phoneNumberMDTORemoto);
        remoto.setStreetAddress(flexibleAddressMDTORemoto);
        remoto.setMailingAddress(flexibleAddressMDTORemoto);
        remoto.setPhotoUrls(photoUrlMDTOListRemoto);

        boolean actualizarLocal = true;
        boolean resultado = iiasServiceImpl.compararContactDetails(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getEmail(), local.getEmail());
    }

    @Test
    public void testCompararPerson_SinCambios() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_SinCambiosBirthDate() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setBirthDate(new Date());
        remoto.setBirthDate(new Date());

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_ConCambiosBirthDate() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setBirthDate(new Date());
        remoto.setBirthDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getBirthDate(), local.getBirthDate());
    }

    @Test
    public void testCompararPerson_ConCambiosBirthDate_NullLocal() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setBirthDate(null);
        remoto.setBirthDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getBirthDate(), local.getBirthDate());
    }

    @Test
    public void testCompararPerson_ConCambiosBirthDate_NullRemoto() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setBirthDate(new Date());
        remoto.setBirthDate(null);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getBirthDate(), local.getBirthDate());
    }

    @Test
    public void testCompararPerson_SinCambiosCountryCode() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setCountryCode("Test");
        remoto.setCountryCode("Test");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_ConCambiosCountryCode() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setCountryCode("Test");
        remoto.setCountryCode("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCountryCode(), local.getCountryCode());
    }

    @Test
    public void testCompararPerson_ConCambiosCountryCode_NullLocal() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setCountryCode(null);
        remoto.setCountryCode("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCountryCode(), local.getCountryCode());
    }

    @Test
    public void testCompararPerson_ConCambiosCountryCode_NullRemoto() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setCountryCode("Test");
        remoto.setCountryCode(null);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getCountryCode(), local.getCountryCode());
    }

    @Test
    public void testCompararPerson_SinCambiosFirstNames() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setFirstNames("Test");
        remoto.setFirstNames("Test");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_ConCambiosFirstNames() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setFirstNames("Test");
        remoto.setFirstNames("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getFirstNames(), local.getFirstNames());
    }

    @Test
    public void testCompararPerson_ConCambiosFirstNames_NullLocal() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setFirstNames(null);
        remoto.setFirstNames("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getFirstNames(), local.getFirstNames());
    }

    @Test
    public void testCompararPerson_ConCambiosFirstNames_NullRemoto() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setFirstNames("Test");
        remoto.setFirstNames(null);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getFirstNames(), local.getFirstNames());
    }

    @Test
    public void testCompararPerson_SinCambiosGender() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setGender(Gender.MALE);
        remoto.setGender(Gender.MALE);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_ConCambiosGender() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setGender(Gender.MALE);
        remoto.setGender(Gender.FEMALE);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getGender(), local.getGender());
    }

    @Test
    public void testCompararPerson_ConCambiosGender_NullLocal() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setGender(null);
        remoto.setGender(Gender.FEMALE);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getGender(), local.getGender());
    }

    @Test
    public void testCompararPerson_ConCambiosGender_NullRemoto() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setGender(Gender.MALE);
        remoto.setGender(null);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getGender(), local.getGender());
    }

    @Test
    public void testCompararPerson_SinCambiosLastName() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setLastName("Test");
        remoto.setLastName("Test");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPerson_ConCambiosLastName() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setLastName("Test");
        remoto.setLastName("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLastName(), local.getLastName());
    }

    @Test
    public void testCompararPerson_ConCambiosLastName_NullLocal() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setLastName(null);
        remoto.setLastName("Test2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLastName(), local.getLastName());
    }

    @Test
    public void testCompararPerson_ConCambiosLastName_NullRemoto() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setLastName("Test");
        remoto.setLastName(null);

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getLastName(), local.getLastName());
    }

    @Test
    public void testCompararPerson_ConCambiosAll() {
        PersonMDTO local = new PersonMDTO();
        PersonMDTO remoto = new PersonMDTO();
        boolean actualizarLocal = true;

        local.setBirthDate(new Date());
        local.setCountryCode("TEST");
        local.setFirstNames("TEST");
        local.setGender(Gender.MALE);
        local.setLastName("TEST");

        remoto.setBirthDate(new Date(123));
        remoto.setCountryCode("TEST2");
        remoto.setFirstNames("TEST2");
        remoto.setGender(Gender.FEMALE);
        remoto.setLastName("TEST2");

        boolean resultado = iiasServiceImpl.compararPerson(remoto, local, actualizarLocal);

        assertTrue(resultado);
    }

    @Test
    public void testCompararContacts_SinCambios() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararContacts_SinCambiosInstitutionId() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararContacts_ConCambiosInstitutionId() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getInstitutionId(), local.getInstitutionId());
    }

    @Test
    public void testCompararContacts_ConCambiosInstitutionId_NullLocal() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setInstitutionId(null);
        remoto.setInstitutionId("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getInstitutionId(), local.getInstitutionId());
    }

    @Test
    public void testCompararContacts_ConCambiosInstitutionId_NullRemoto() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setInstitutionId("Test");
        remoto.setInstitutionId(null);

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getInstitutionId(), local.getInstitutionId());
    }

    @Test
    public void testCompararContacts_SinCambiosOrganizationUnitId() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararContacts_ConCambiosOrganizationUnitId() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId());
    }

    @Test
    public void testCompararContacts_ConCambiosOrganizationUnitId_NullLocal() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setOrganizationUnitId(null);
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getOrganizationUnitId(), local.getOrganizationUnitId());
    }

    @Test
    public void testCompararContacts_ConCambiosOrganizationUnitId_NullRemoto() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId(null);

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getInstitutionId(), local.getInstitutionId());
    }

    @Test
    public void testCompararContacts_SinCambiosRole() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setRole("Test");
        remoto.setRole("Test");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertFalse(resultado);
    }

    @Test
    public void testCompararContacts_ConCambiosRole() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setRole("Test");
        remoto.setRole("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getRole(), local.getRole());
    }

    @Test
    public void testCompararContacts_ConCambiosRole_NullLocal() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setRole(null);
        remoto.setRole("Test2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getRole(), local.getRole());
    }

    @Test
    public void testCompararContacts_ConCambiosRole_NullRemoto() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        local.setRole("Test");
        remoto.setRole(null);

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
        assertEquals(remoto.getRole(), local.getRole());
    }

    private ContactDetailsMDTO generarContactDetailLocalTest(){
        ContactDetailsMDTO local = new ContactDetailsMDTO();
        List<String> localLista = new ArrayList<>();
        localLista.add("Test");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        PhoneNumberMDTO phoneNumberMDTOLocal = new PhoneNumberMDTO();

        phoneNumberMDTOLocal.setE164("PRUEBA CAMBIO 1");
        phoneNumberMDTOLocal.setExtensionNumber("+34");
        phoneNumberMDTOLocal.setOtherFormat("us");

        FlexibleAddressMDTO flexibleAddressMDTOLocal = new FlexibleAddressMDTO();

        flexibleAddressMDTOLocal.setRecipientName(localLista);
        flexibleAddressMDTOLocal.setAddressLine(localLista);
        flexibleAddressMDTOLocal.setBuildingNumber("TEST");
        flexibleAddressMDTOLocal.setBuildingName("TEST");
        flexibleAddressMDTOLocal.setStreetName("TEST");
        flexibleAddressMDTOLocal.setUnit("TEST");
        flexibleAddressMDTOLocal.setFloor("TEST");
        flexibleAddressMDTOLocal.setPostOfficeBox("TEST");
        flexibleAddressMDTOLocal.setDeliveryPointCode(localLista);
        flexibleAddressMDTOLocal.setPostalCode("TEST");
        flexibleAddressMDTOLocal.setLocality("TEST");
        flexibleAddressMDTOLocal.setRegion("TEST");
        flexibleAddressMDTOLocal.setCountry("TEST");

        List<PhotoUrlMDTO> photoUrlMDTOListLocal = new ArrayList<>();
        PhotoUrlMDTO photoUrlMDTOLocal = new PhotoUrlMDTO();

        photoUrlMDTOLocal.setUrl("TEST");
        photoUrlMDTOLocal.setSize("TEST");
        photoUrlMDTOLocal.setDate(new Date());
        photoUrlMDTOListLocal.add(photoUrlMDTOLocal);

        local.setEmail(localLista);
        local.setUrl(languageItemListLocal);
        local.setPhoneNumber(phoneNumberMDTOLocal);
        local.setFaxNumber(phoneNumberMDTOLocal);
        local.setStreetAddress(flexibleAddressMDTOLocal);
        local.setMailingAddress(flexibleAddressMDTOLocal);
        local.setPhotoUrls(photoUrlMDTOListLocal);

        return local;
    }

    private ContactDetailsMDTO generarContactDetailRemotoTest(){
        ContactDetailsMDTO remoto = new ContactDetailsMDTO();
        List<String> remotoLista = new ArrayList<>();
        remotoLista.add("Test2");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        PhoneNumberMDTO phoneNumberMDTORemoto = new PhoneNumberMDTO();

        phoneNumberMDTORemoto.setE164("PRUEBA CAMBIO 2");
        phoneNumberMDTORemoto.setExtensionNumber("+85");
        phoneNumberMDTORemoto.setOtherFormat("es");

        FlexibleAddressMDTO flexibleAddressMDTORemoto = new FlexibleAddressMDTO();

        flexibleAddressMDTORemoto.setRecipientName(remotoLista);
        flexibleAddressMDTORemoto.setAddressLine(remotoLista);
        flexibleAddressMDTORemoto.setBuildingNumber("TEST2");
        flexibleAddressMDTORemoto.setBuildingName("TEST2");
        flexibleAddressMDTORemoto.setStreetName("TEST2");
        flexibleAddressMDTORemoto.setUnit("TEST2");
        flexibleAddressMDTORemoto.setFloor("TEST2");
        flexibleAddressMDTORemoto.setPostOfficeBox("TEST2");
        flexibleAddressMDTORemoto.setDeliveryPointCode(remotoLista);
        flexibleAddressMDTORemoto.setPostalCode("TEST2");
        flexibleAddressMDTORemoto.setLocality("TEST2");
        flexibleAddressMDTORemoto.setRegion("TEST2");
        flexibleAddressMDTORemoto.setCountry("TEST2");

        List<PhotoUrlMDTO> photoUrlMDTOListRemoto = new ArrayList<>();
        PhotoUrlMDTO photoUrlMDTORemoto = new PhotoUrlMDTO();

        photoUrlMDTORemoto.setUrl("TEST2");
        photoUrlMDTORemoto.setSize("TEST2");
        photoUrlMDTORemoto.setDate(new Date(123));
        photoUrlMDTOListRemoto.add(photoUrlMDTORemoto);

        remoto.setEmail(remotoLista);
        remoto.setUrl(languageItemListRemoto);
        remoto.setPhoneNumber(phoneNumberMDTORemoto);
        remoto.setFaxNumber(phoneNumberMDTORemoto);
        remoto.setStreetAddress(flexibleAddressMDTORemoto);
        remoto.setMailingAddress(flexibleAddressMDTORemoto);
        remoto.setPhotoUrls(photoUrlMDTOListRemoto);

        return remoto;
    }

    @Test
    public void testCompararContacts_ConCambiosAll() {
        ContactMDTO local = new ContactMDTO();
        ContactMDTO remoto = new ContactMDTO();
        boolean actualizarLocal = true;

        PersonMDTO personMDTOLocal = new PersonMDTO();
        PersonMDTO personMDTORemoto = new PersonMDTO();

        personMDTOLocal.setBirthDate(new Date());
        personMDTOLocal.setCountryCode("TEST");
        personMDTOLocal.setFirstNames("TEST");
        personMDTOLocal.setGender(Gender.MALE);
        personMDTOLocal.setLastName("TEST");

        personMDTORemoto.setBirthDate(new Date(123));
        personMDTORemoto.setCountryCode("TEST2");
        personMDTORemoto.setFirstNames("TEST2");
        personMDTORemoto.setGender(Gender.FEMALE);
        personMDTORemoto.setLastName("TEST2");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");
        languageItemListLocal.add(languageItemMDTOLocal);

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");
        languageItemListRemoto.add(languageItemMDTORemoto);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setRole("TEST");
        local.setPerson(personMDTOLocal);
        local.setName(languageItemListLocal);
        local.setDescription(languageItemListLocal);
        local.setContactDetails(generarContactDetailLocalTest());
        local.setIntitutionId("TEST");

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setRole("TEST2");
        remoto.setPerson(personMDTORemoto);
        remoto.setName(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);
        remoto.setContactDetails(generarContactDetailRemotoTest());
        remoto.setIntitutionId("TEST2");

        boolean resultado = iiasServiceImpl.compararContacts(remoto, local, actualizarLocal);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_SinCambiosInstitutionId() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosInstitutionId() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId("Test2");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosInstitutionId_NullLocal() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setInstitutionId(null);
        remoto.setInstitutionId("Test2");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosInstitutionId_NullRemoto() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setInstitutionId("Test");
        remoto.setInstitutionId(null);

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_SinCambiosOrganizationUnitId() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosOrganizationUnitId() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosOrganizationUnitId_NullLocal() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setOrganizationUnitId(null);
        remoto.setOrganizationUnitId("Test2");

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosOrganizationUnitId_NullRemoto() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setOrganizationUnitId("Test");
        remoto.setOrganizationUnitId(null);

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_SinCambiosSigningDate() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setSigningDate(new Date());
        remoto.setSigningDate(new Date());

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosSigningDate() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setSigningDate(new Date());
        remoto.setSigningDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosSigningDate_NullLocal() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setSigningDate(null);
        remoto.setSigningDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosSigningDate_NullRemoto() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setSigningDate(new Date());
        remoto.setSigningDate(null);

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }
    public ContactMDTO generarContactLocalTest(){
        ContactMDTO local = new ContactMDTO();

        PersonMDTO personMDTOLocal = new PersonMDTO();

        personMDTOLocal.setBirthDate(new Date());
        personMDTOLocal.setCountryCode("TEST");
        personMDTOLocal.setFirstNames("TEST");
        personMDTOLocal.setGender(Gender.MALE);
        personMDTOLocal.setLastName("TEST");

        List<LanguageItemMDTO> languageItemListLocal = new ArrayList<>();
        LanguageItemMDTO languageItemMDTOLocal = new LanguageItemMDTO();
        languageItemMDTOLocal.setText("TEST");
        languageItemMDTOLocal.setLang("TEST");

        languageItemListLocal.add(languageItemMDTOLocal);

        local.setInstitutionId("TEST");
        local.setOrganizationUnitId("TEST");
        local.setRole("TEST");
        local.setPerson(personMDTOLocal);
        local.setName(languageItemListLocal);
        local.setDescription(languageItemListLocal);
        local.setContactDetails(generarContactDetailLocalTest());
        local.setIntitutionId("TEST");

        return local;
    }

    public ContactMDTO generarContactRemotoTest(){
        ContactMDTO remoto = new ContactMDTO();

        PersonMDTO personMDTORemoto = new PersonMDTO();

        personMDTORemoto.setBirthDate(new Date(123));
        personMDTORemoto.setCountryCode("TEST2");
        personMDTORemoto.setFirstNames("TEST2");
        personMDTORemoto.setGender(Gender.FEMALE);
        personMDTORemoto.setLastName("TEST2");

        List<LanguageItemMDTO> languageItemListRemoto = new ArrayList<>();
        LanguageItemMDTO languageItemMDTORemoto = new LanguageItemMDTO();
        languageItemMDTORemoto.setText("TEST2");
        languageItemMDTORemoto.setLang("TEST2");

        languageItemListRemoto.add(languageItemMDTORemoto);

        remoto.setInstitutionId("TEST2");
        remoto.setOrganizationUnitId("TEST2");
        remoto.setRole("TEST2");
        remoto.setPerson(personMDTORemoto);
        remoto.setName(languageItemListRemoto);
        remoto.setDescription(languageItemListRemoto);
        remoto.setContactDetails(generarContactDetailRemotoTest());
        remoto.setIntitutionId("TEST2");

        return remoto;
    }

    @Test
    public void testCompararPartners_SinCambios() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();

        local.setSigningDate(new Date());
        remoto.setSigningDate(new Date());

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararPartners_ConCambiosAll() {
        IiaPartnerMDTO local = new IiaPartnerMDTO();
        IiaPartnerMDTO remoto = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListLocal = new ArrayList<>();
        List<ContactMDTO> contactMDTOListRemoto = new ArrayList<>();
        contactMDTOListLocal.add(generarContactLocalTest());
        contactMDTOListRemoto.add(generarContactRemotoTest());

        local.setInstitutionId("Test");
        local.setOrganizationUnitId("Test");
        local.setSignerPersonId(generarContactLocalTest());
        local.setSigningDate(new Date());
        local.setContacts(contactMDTOListLocal);

        remoto.setInstitutionId("Test2");
        remoto.setOrganizationUnitId("Test2");
        remoto.setSignerPersonId(generarContactRemotoTest());
        remoto.setSigningDate(new Date(123));
        remoto.setContacts(contactMDTOListRemoto);

        boolean resultado = iiasServiceImpl.compararPartners(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambios() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosStartDate() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date());

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosStartDate() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosStartDate_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setStartDate(new Date());
        remoto.setStartDate(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosStartDate_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setStartDate(null);
        remoto.setStartDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosEndDate() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date());

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEndDate() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEndDate_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setEndDate(new Date());
        remoto.setEndDate(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEndDate_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setEndDate(null);
        remoto.setEndDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosEqfLevel() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();

        eqfLevelListLocal.add((byte) 1);
        eqfLevelListRemoto.add((byte) 1);

        local.setEqfLevel(eqfLevelListLocal);
        remoto.setEqfLevel(eqfLevelListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEqfLevel() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();

        eqfLevelListLocal.add((byte) 1);
        eqfLevelListRemoto.add((byte) 2);

        local.setEqfLevel(eqfLevelListLocal);
        remoto.setEqfLevel(eqfLevelListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEqfLevel_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();

        eqfLevelListLocal.add(null);
        eqfLevelListRemoto.add((byte) 2);

        local.setEqfLevel(eqfLevelListLocal);
        remoto.setEqfLevel(eqfLevelListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEqfLevel_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListRemoto = new ArrayList<>();
        eqfLevelListRemoto.add((byte) 2);

        local.setEqfLevel(null);
        remoto.setEqfLevel(eqfLevelListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEqfLevel_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();

        eqfLevelListLocal.add((byte) 1);
        eqfLevelListRemoto.add(null);

        local.setEqfLevel(eqfLevelListLocal);
        remoto.setEqfLevel(eqfLevelListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosEqfLevel_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        eqfLevelListLocal.add((byte) 1);

        local.setEqfLevel(eqfLevelListLocal);
        remoto.setEqfLevel(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosOtherInfo() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setOtherInfo("Test");
        remoto.setOtherInfo("Test");

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosOtherInfo() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setOtherInfo("Test");
        remoto.setOtherInfo("Test2");

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosOtherInfo_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setOtherInfo(null);
        remoto.setOtherInfo("Test2");

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosOtherInfo_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setOtherInfo("Test");
        remoto.setOtherInfo(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosDurationNumberduration() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(new BigDecimal("111.11"));
        durationRemoto.setNumberduration(new BigDecimal("111.11"));

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_MatematicamenteIgualesDurationNumberduration() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(new BigDecimal("111"));
        durationRemoto.setNumberduration(new BigDecimal("111.00"));

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationNumberduration() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(new BigDecimal("111.11"));
        durationRemoto.setNumberduration(new BigDecimal("222.22"));

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationNumberduration_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(null);
        durationRemoto.setNumberduration(new BigDecimal("222.22"));

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationNumberduration_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationRemoto = new DurationMDTO();
        durationRemoto.setNumberduration(BigDecimal.TEN);

        local.setDuration(null);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationNumberduration_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(new BigDecimal("111.11"));
        durationRemoto.setNumberduration(null);

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationNumberduration_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        durationLocal.setNumberduration(new BigDecimal("111.11"));

        local.setDuration(durationLocal);
        remoto.setDuration(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosDurationUnit() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setUnit(DurationUnitVariants.DAYS);
        durationRemoto.setUnit(DurationUnitVariants.DAYS);

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationUnit() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setUnit(DurationUnitVariants.DAYS);
        durationRemoto.setUnit(DurationUnitVariants.MONTHS);

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationUnit_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setUnit(null);
        durationRemoto.setUnit(DurationUnitVariants.MONTHS);

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationUnit_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationRemoto = new DurationMDTO();
        durationRemoto.setUnit(DurationUnitVariants.MONTHS);

        local.setDuration(null);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationUnit_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setUnit(DurationUnitVariants.DAYS);
        durationRemoto.setUnit(null);

        local.setDuration(durationLocal);
        remoto.setDuration(durationRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosDurationUnit_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        DurationMDTO durationLocal = new DurationMDTO();
        durationLocal.setUnit(DurationUnitVariants.DAYS);

        local.setDuration(durationLocal);
        remoto.setDuration(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosMobilityNumberNumbermobility() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setNumbermobility(1);
        mobilityNumberMDTORemoto.setNumbermobility(1);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberNumbermobility() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setNumbermobility(1);
        mobilityNumberMDTORemoto.setNumbermobility(2);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberNumbermobility_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTORemoto.setNumbermobility(2);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberNumbermobility_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();
        mobilityNumberMDTORemoto.setNumbermobility(2);

        local.setMobilityNumber(null);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberNumbermobility_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setNumbermobility(1);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberNumbermobility_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        mobilityNumberMDTOLocal.setNumbermobility(1);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosMobilityNumberVariants() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);
        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.AVERAGE);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberVariants() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);
        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.TOTAL);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberVariants_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setVariant(null);
        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.TOTAL);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberVariants_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();
        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.TOTAL);

        local.setMobilityNumber(null);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberVariants_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);
        mobilityNumberMDTORemoto.setVariant(null);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityNumberVariants_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);

        local.setMobilityNumber(mobilityNumberMDTOLocal);
        remoto.setMobilityNumber(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosMobilityTypeGroup() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityGroup("Test");
        mobilityTypeMDTORemoto.setMobilityGroup("Test");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeGroup() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityGroup("Test");
        mobilityTypeMDTORemoto.setMobilityGroup("Test2");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeGroup_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityGroup(null);
        mobilityTypeMDTORemoto.setMobilityGroup("Test2");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeGroup_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();
        mobilityTypeMDTORemoto.setMobilityGroup("Test2");

        local.setMobilityType(null);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeGroup_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityGroup("Test");
        mobilityTypeMDTORemoto.setMobilityGroup(null);

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeGroup_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        mobilityTypeMDTOLocal.setMobilityGroup("Test");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosMobilityTypeCategory() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityCategory("Test");
        mobilityTypeMDTORemoto.setMobilityCategory("Test");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeCategory() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityCategory("Test");
        mobilityTypeMDTORemoto.setMobilityCategory("Test2");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeCategory_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityCategory(null);
        mobilityTypeMDTORemoto.setMobilityCategory("Test2");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeCategory_NullLocal2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();
        mobilityTypeMDTORemoto.setMobilityCategory("Test2");

        local.setMobilityType(null);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeCategory_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityCategory("Test");
        mobilityTypeMDTORemoto.setMobilityCategory(null);

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(mobilityTypeMDTORemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosMobilityTypeCategory_NullRemoto2() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        mobilityTypeMDTOLocal.setMobilityCategory("Test");

        local.setMobilityType(mobilityTypeMDTOLocal);
        remoto.setMobilityType(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_SinCambiosBlended() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setBlended(Boolean.TRUE);
        remoto.setBlended(Boolean.TRUE);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosBlended() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setBlended(Boolean.TRUE);
        remoto.setBlended(Boolean.FALSE);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosBlended_NullLocal() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setBlended(null);
        remoto.setBlended(Boolean.FALSE);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosBlended_NullRemoto() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        local.setBlended(Boolean.TRUE);
        remoto.setBlended(null);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_SinCambiosLanguage() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setLanguage("TEST");
        languageSkillMDTORemoto.setLanguage("TEST");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosLanguage() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setLanguage("TEST");
        languageSkillMDTORemoto.setLanguage("TEST2");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosLanguage_NullLocal() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setLanguage(null);
        languageSkillMDTORemoto.setLanguage("TEST2");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosLanguage_NullLocal2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();
        languageSkillMDTORemoto.setLanguage("TEST2");

        local.setLanguageSkill(null);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosLanguage_NullRemoto() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setLanguage("TEST");
        languageSkillMDTORemoto.setLanguage(null);

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosLanguage_NullRemoto2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        languageSkillMDTOLocal.setLanguage("TEST");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(null);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_SinCambiosCefrLevel() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setCefrLevel("TEST");
        languageSkillMDTORemoto.setCefrLevel("TEST");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosCefrLevel() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setCefrLevel("TEST");
        languageSkillMDTORemoto.setCefrLevel("TEST2");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosCefrLevel_NullLocal() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setCefrLevel(null);
        languageSkillMDTORemoto.setCefrLevel("TEST2");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosCefrLevel_NullLocal2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();
        languageSkillMDTORemoto.setCefrLevel("TEST2");

        local.setLanguageSkill(null);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosCefrLevel_NullRemoto() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillMDTORemoto = new LanguageSkillMDTO();

        languageSkillMDTOLocal.setCefrLevel("TEST");
        languageSkillMDTORemoto.setCefrLevel(null);

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosCefrLevel_NullRemoto2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        LanguageSkillMDTO languageSkillMDTOLocal = new LanguageSkillMDTO();
        languageSkillMDTOLocal.setCefrLevel("TEST");

        local.setLanguageSkill(languageSkillMDTOLocal);
        remoto.setLanguageSkill(null);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_SinCambiosIscedClarification() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO languageSkillMDTORemoto = new SubjectAreaMDTO();

        languageSkillMDTOLocal.setIscedClarification("TEST");
        languageSkillMDTORemoto.setIscedClarification("TEST");

        local.setSubjectArea(languageSkillMDTOLocal);
        remoto.setSubjectArea(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedClarification() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO languageSkillMDTORemoto = new SubjectAreaMDTO();

        languageSkillMDTOLocal.setIscedClarification("TEST");
        languageSkillMDTORemoto.setIscedClarification("TEST2");

        local.setSubjectArea(languageSkillMDTOLocal);
        remoto.setSubjectArea(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedClarification_NullLocal() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO languageSkillMDTORemoto = new SubjectAreaMDTO();

        languageSkillMDTOLocal.setIscedClarification(null);
        languageSkillMDTORemoto.setIscedClarification("TEST2");

        local.setSubjectArea(languageSkillMDTOLocal);
        remoto.setSubjectArea(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedClarification_NullLocal2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTORemoto = new SubjectAreaMDTO();
        languageSkillMDTORemoto.setIscedClarification("TEST2");

        local.setSubjectArea(null);
        remoto.setSubjectArea(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedClarification_NullRemoto() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO languageSkillMDTORemoto = new SubjectAreaMDTO();

        languageSkillMDTOLocal.setIscedClarification("TEST");
        languageSkillMDTORemoto.setIscedClarification(null);

        local.setSubjectArea(languageSkillMDTOLocal);
        remoto.setSubjectArea(languageSkillMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedClarification_NullRemoto2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO languageSkillMDTOLocal = new SubjectAreaMDTO();
        languageSkillMDTOLocal.setIscedClarification("TEST");

        local.setSubjectArea(languageSkillMDTOLocal);
        remoto.setSubjectArea(null);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_SinCambiosIscedCode() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaMDTORemoto = new SubjectAreaMDTO();

        subjectAreaMDTOLocal.setIscedCode("TEST");
        subjectAreaMDTORemoto.setIscedCode("TEST");

        local.setSubjectArea(subjectAreaMDTOLocal);
        remoto.setSubjectArea(subjectAreaMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedCode() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaMDTORemoto = new SubjectAreaMDTO();

        subjectAreaMDTOLocal.setIscedCode("TEST");
        subjectAreaMDTORemoto.setIscedCode("TEST2");

        local.setSubjectArea(subjectAreaMDTOLocal);
        remoto.setSubjectArea(subjectAreaMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedCode_NullLocal() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaMDTORemoto = new SubjectAreaMDTO();

        subjectAreaMDTOLocal.setIscedCode(null);
        subjectAreaMDTORemoto.setIscedCode("TEST");

        local.setSubjectArea(subjectAreaMDTOLocal);
        remoto.setSubjectArea(subjectAreaMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedCode_NullLocal2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTORemoto = new SubjectAreaMDTO();
        subjectAreaMDTORemoto.setIscedCode("TEST");

        local.setSubjectArea(null);
        remoto.setSubjectArea(subjectAreaMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedCode_NullRemoto() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTOLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaMDTORemoto = new SubjectAreaMDTO();

        subjectAreaMDTOLocal.setIscedCode("TEST");
        subjectAreaMDTORemoto.setIscedCode(null);

        local.setSubjectArea(subjectAreaMDTOLocal);
        remoto.setSubjectArea(subjectAreaMDTORemoto);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararLanguageSkills_ConCambiosIscedCode_NullRemoto2() {
        CooperationConditionSubjAreaLangSkillMDTO local = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO remoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaMDTOLocal = new SubjectAreaMDTO();
        subjectAreaMDTOLocal.setIscedCode("TEST");

        local.setSubjectArea(subjectAreaMDTOLocal);
        remoto.setSubjectArea(null);

        boolean resultado = iiasServiceImpl.compararLanguageSkills(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararCooperationConditions_ConCambiosAll() {
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();

        List<Byte> eqfLevelListLocal = new ArrayList<>();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();

        eqfLevelListLocal.add((byte) 1);
        eqfLevelListRemoto.add((byte) 2);

        DurationMDTO durationLocal = new DurationMDTO();
        DurationMDTO durationRemoto = new DurationMDTO();

        durationLocal.setNumberduration(new BigDecimal("111.11"));
        durationLocal.setUnit(DurationUnitVariants.DAYS);

        durationRemoto.setNumberduration(new BigDecimal("222.22"));
        durationRemoto.setUnit(DurationUnitVariants.MONTHS);

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();

        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);
        mobilityNumberMDTOLocal.setNumbermobility(1);

        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.TOTAL);
        mobilityNumberMDTORemoto.setNumbermobility(2);

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();

        mobilityTypeMDTOLocal.setMobilityGroup("TEST");
        mobilityTypeMDTOLocal.setMobilityCategory("TEST");

        mobilityTypeMDTORemoto.setMobilityGroup("TEST2");
        mobilityTypeMDTORemoto.setMobilityCategory("TEST2");

        IiaPartnerMDTO iiaPartnerMDTOLocal = new IiaPartnerMDTO();
        IiaPartnerMDTO iiaPartnerMDTORemoto = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListLocal = new ArrayList<>();
        List<ContactMDTO> contactMDTOListRemoto = new ArrayList<>();
        contactMDTOListLocal.add(generarContactLocalTest());
        contactMDTOListRemoto.add(generarContactRemotoTest());

        iiaPartnerMDTOLocal.setInstitutionId("Test");
        iiaPartnerMDTOLocal.setOrganizationUnitId("Test");
        iiaPartnerMDTOLocal.setSignerPersonId(generarContactLocalTest());
        iiaPartnerMDTOLocal.setSigningDate(new Date());
        iiaPartnerMDTOLocal.setContacts(contactMDTOListLocal);

        iiaPartnerMDTORemoto.setInstitutionId("Test2");
        iiaPartnerMDTORemoto.setOrganizationUnitId("Test2");
        iiaPartnerMDTORemoto.setSignerPersonId(generarContactRemotoTest());
        iiaPartnerMDTORemoto.setSigningDate(new Date(123));
        iiaPartnerMDTORemoto.setContacts(contactMDTOListRemoto);

        List<CooperationConditionSubjAreaLangSkillMDTO> languageSkillListLocal = new ArrayList<>();
        List<CooperationConditionSubjAreaLangSkillMDTO> languageSkillListRemoto = new ArrayList<>();

        CooperationConditionSubjAreaLangSkillMDTO CCSALSLocal = new CooperationConditionSubjAreaLangSkillMDTO();
        CooperationConditionSubjAreaLangSkillMDTO CCSALSRemoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaLocal = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaRemoto = new SubjectAreaMDTO();

        subjectAreaLocal.setIscedCode("TEST");
        subjectAreaLocal.setIscedClarification("TEST");

        subjectAreaRemoto.setIscedCode("TEST2");
        subjectAreaRemoto.setIscedClarification("TEST2");

        LanguageSkillMDTO languageSkillLocal = new LanguageSkillMDTO();
        LanguageSkillMDTO languageSkillRemoto = new LanguageSkillMDTO();

        languageSkillLocal.setLanguage("TEST");
        languageSkillLocal.setCefrLevel("TEST");

        languageSkillRemoto.setLanguage("TEST2");
        languageSkillRemoto.setCefrLevel("TEST2");

        CCSALSLocal.setSubjectArea(subjectAreaLocal);
        CCSALSLocal.setLanguageSkill(languageSkillLocal);

        CCSALSRemoto.setSubjectArea(subjectAreaRemoto);
        CCSALSRemoto.setLanguageSkill(languageSkillRemoto);

        languageSkillListLocal.add(CCSALSLocal);
        languageSkillListRemoto.add(CCSALSRemoto);

        List<CooperationConditionSubjectAreaMDTO> subjectAreaListLocal = new ArrayList<>();
        List<CooperationConditionSubjectAreaMDTO> subjectAreaListRemoto = new ArrayList<>();

        CooperationConditionSubjectAreaMDTO coopCondSubjAreaLocal = new CooperationConditionSubjectAreaMDTO();
        CooperationConditionSubjectAreaMDTO coopCondSubjAreaRemoto = new CooperationConditionSubjectAreaMDTO();

        SubjectAreaMDTO subjectAreaLocal2 = new SubjectAreaMDTO();
        SubjectAreaMDTO subjectAreaRemoto2 = new SubjectAreaMDTO();

        subjectAreaLocal2.setIscedCode("TEST");
        subjectAreaLocal2.setIscedClarification("TEST");

        subjectAreaRemoto2.setIscedCode("TEST2");
        subjectAreaRemoto2.setIscedClarification("TEST2");

        coopCondSubjAreaLocal.setSubjectArea(subjectAreaLocal2);
        coopCondSubjAreaRemoto.setSubjectArea(subjectAreaRemoto2);

        subjectAreaListLocal.add(coopCondSubjAreaLocal);
        subjectAreaListRemoto.add(coopCondSubjAreaRemoto);

        local.setEndDate(new Date());
        local.setEqfLevel(eqfLevelListLocal);
        local.setStartDate(new Date());
        local.setDuration(durationLocal);
        local.setMobilityNumber(mobilityNumberMDTOLocal);
        local.setMobilityType(mobilityTypeMDTOLocal);
        local.setReceivingPartner(iiaPartnerMDTOLocal);
        local.setSendingPartner(iiaPartnerMDTOLocal);
        local.setOtherInfo("TEST");
        local.setBlended(Boolean.TRUE);
        local.setLanguageSkill(languageSkillListLocal);
        local.setSubjectAreaList(subjectAreaListLocal);

        remoto.setEndDate(new Date(123));
        remoto.setEqfLevel(eqfLevelListRemoto);
        remoto.setStartDate(new Date(123));
        remoto.setDuration(durationRemoto);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);
        remoto.setMobilityType(mobilityTypeMDTORemoto);
        remoto.setReceivingPartner(iiaPartnerMDTORemoto);
        remoto.setSendingPartner(iiaPartnerMDTORemoto);
        remoto.setOtherInfo("TEST2");
        remoto.setBlended(Boolean.FALSE);
        remoto.setLanguageSkill(languageSkillListRemoto);
        remoto.setSubjectAreaList(subjectAreaListRemoto);

        boolean resultado = iiasServiceImpl.compararCooperationConditions(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_SinCambios() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_SinCambiosPDF() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setPdfId("a");
        remoto.setPdfId("a");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosPDF() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setPdfId("a");
        remoto.setPdfId("b");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosPDF_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setPdfId(null);
        remoto.setPdfId("a");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosPDF_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setPdfId("a");
        remoto.setPdfId(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_SinCambiosRemoteCoopCondHash() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteCoopCondHash("Test");
        remoto.setRemoteCoopCondHash("Test");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteCoopCondHash() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteCoopCondHash("Test");
        remoto.setRemoteCoopCondHash("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteCoopCondHash_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteCoopCondHash(null);
        remoto.setRemoteCoopCondHash("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteCoopCondHash_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteCoopCondHash("Test");
        remoto.setRemoteCoopCondHash(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_SinCambiosApprovalCopCondHash() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalCopCondHash("Test");
        remoto.setApprovalCopCondHash("Test");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_HASH no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalCopCondHash() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalCopCondHash("Test");
        remoto.setApprovalCopCondHash("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_HASH no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalCopCondHash_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalCopCondHash(null);
        remoto.setApprovalCopCondHash("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_HASH no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalCopCondHash_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalCopCondHash("Test");
        remoto.setApprovalCopCondHash(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_SinCambiosRemoteIiaCode() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteIiaCode("Test");
        remoto.setRemoteIiaCode("Test");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteIiaCode() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteIiaCode("Test");
        remoto.setRemoteIiaCode("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteIiaCode_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteIiaCode(null);
        remoto.setRemoteIiaCode("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosRemoteIiaCode_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setRemoteIiaCode("Test");
        remoto.setRemoteIiaCode(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    // el APPROVAL_DATE no se compara
    @Test
    public void testCompararIIA_SinCambiosApprovalDate() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalDate(new Date());
        remoto.setApprovalDate(new Date());

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_DATE no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalDate() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalDate(new Date());
        remoto.setApprovalDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_DATE no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalDate_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalDate(null);
        remoto.setApprovalDate(new Date(123));

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    // el APPROVAL_HASH no se compara
    @Test
    public void testCompararIIA_ConCambiosApprovalDate_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setApprovalDate(new Date());
        remoto.setApprovalDate(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_SinCambiosIiaCode() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setIiaCode("Test");
        remoto.setIiaCode("Test");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosIiaCode() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setIiaCode("Test");
        remoto.setIiaCode("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosIiaCode_NullLocal() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setIiaCode(null);
        remoto.setIiaCode("Test2");

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararIIA_ConCambiosIiaCode_NullRemoto() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        local.setIiaCode("Test");
        remoto.setIiaCode(null);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }


    private CooperationConditionMDTO generarCooperationConditionLocalTest(){
        CooperationConditionMDTO local = new CooperationConditionMDTO();
        List<Byte> eqfLevelListLocal = new ArrayList<>();
        eqfLevelListLocal.add((byte) 1);

        DurationMDTO durationLocal = new DurationMDTO();
        durationLocal.setNumberduration(new BigDecimal("111.11"));
        durationLocal.setUnit(DurationUnitVariants.DAYS);

        MobilityNumberMDTO mobilityNumberMDTOLocal = new MobilityNumberMDTO();
        mobilityNumberMDTOLocal.setVariant(MobilityNumberVariants.AVERAGE);
        mobilityNumberMDTOLocal.setNumbermobility(1);

        MobilityTypeMDTO mobilityTypeMDTOLocal = new MobilityTypeMDTO();
        mobilityTypeMDTOLocal.setMobilityGroup("TEST");
        mobilityTypeMDTOLocal.setMobilityCategory("TEST");

        IiaPartnerMDTO iiaPartnerMDTOLocal = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListLocal = new ArrayList<>();
        contactMDTOListLocal.add(generarContactLocalTest());

        iiaPartnerMDTOLocal.setInstitutionId("Test");
        iiaPartnerMDTOLocal.setOrganizationUnitId("Test");
        iiaPartnerMDTOLocal.setSignerPersonId(generarContactLocalTest());
        iiaPartnerMDTOLocal.setSigningDate(new Date());
        iiaPartnerMDTOLocal.setContacts(contactMDTOListLocal);

        List<CooperationConditionSubjAreaLangSkillMDTO> languageSkillListLocal = new ArrayList<>();
        CooperationConditionSubjAreaLangSkillMDTO CCSALSLocal = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaLocal = new SubjectAreaMDTO();
        subjectAreaLocal.setIscedCode("TEST");
        subjectAreaLocal.setIscedClarification("TEST");

        LanguageSkillMDTO languageSkillLocal = new LanguageSkillMDTO();
        languageSkillLocal.setLanguage("TEST");
        languageSkillLocal.setCefrLevel("TEST");

        CCSALSLocal.setSubjectArea(subjectAreaLocal);
        CCSALSLocal.setLanguageSkill(languageSkillLocal);

        languageSkillListLocal.add(CCSALSLocal);
        List<CooperationConditionSubjectAreaMDTO> subjectAreaListLocal = new ArrayList<>();
        CooperationConditionSubjectAreaMDTO coopCondSubjAreaLocal = new CooperationConditionSubjectAreaMDTO();

        SubjectAreaMDTO subjectAreaLocal2 = new SubjectAreaMDTO();
        subjectAreaLocal2.setIscedCode("TEST");
        subjectAreaLocal2.setIscedClarification("TEST");

        coopCondSubjAreaLocal.setSubjectArea(subjectAreaLocal2);
        subjectAreaListLocal.add(coopCondSubjAreaLocal);

        local.setEndDate(new Date());
        local.setEqfLevel(eqfLevelListLocal);
        local.setStartDate(new Date());
        local.setDuration(durationLocal);
        local.setMobilityNumber(mobilityNumberMDTOLocal);
        local.setMobilityType(mobilityTypeMDTOLocal);
        local.setReceivingPartner(iiaPartnerMDTOLocal);
        local.setSendingPartner(iiaPartnerMDTOLocal);
        local.setOtherInfo("TEST");
        local.setBlended(Boolean.TRUE);
        local.setLanguageSkill(languageSkillListLocal);
        local.setSubjectAreaList(subjectAreaListLocal);

        return local;
    }

    private CooperationConditionMDTO generarCooperationConditionRemotoTest(){
        CooperationConditionMDTO remoto = new CooperationConditionMDTO();
        List<Byte> eqfLevelListRemoto = new ArrayList<>();
        eqfLevelListRemoto.add((byte) 2);

        DurationMDTO durationRemoto = new DurationMDTO();
        durationRemoto.setNumberduration(new BigDecimal("222.22"));
        durationRemoto.setUnit(DurationUnitVariants.MONTHS);

        MobilityNumberMDTO mobilityNumberMDTORemoto = new MobilityNumberMDTO();
        mobilityNumberMDTORemoto.setVariant(MobilityNumberVariants.TOTAL);
        mobilityNumberMDTORemoto.setNumbermobility(2);

        MobilityTypeMDTO mobilityTypeMDTORemoto = new MobilityTypeMDTO();
        mobilityTypeMDTORemoto.setMobilityGroup("TEST2");
        mobilityTypeMDTORemoto.setMobilityCategory("TEST2");

        IiaPartnerMDTO iiaPartnerMDTORemoto = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListRemoto = new ArrayList<>();
        contactMDTOListRemoto.add(generarContactRemotoTest());

        iiaPartnerMDTORemoto.setInstitutionId("Test2");
        iiaPartnerMDTORemoto.setOrganizationUnitId("Test2");
        iiaPartnerMDTORemoto.setSignerPersonId(generarContactRemotoTest());
        iiaPartnerMDTORemoto.setSigningDate(new Date(123));
        iiaPartnerMDTORemoto.setContacts(contactMDTOListRemoto);

        List<CooperationConditionSubjAreaLangSkillMDTO> languageSkillListRemoto = new ArrayList<>();
        CooperationConditionSubjAreaLangSkillMDTO CCSALSRemoto = new CooperationConditionSubjAreaLangSkillMDTO();

        SubjectAreaMDTO subjectAreaRemoto = new SubjectAreaMDTO();
        subjectAreaRemoto.setIscedCode("TEST2");
        subjectAreaRemoto.setIscedClarification("TEST2");

        LanguageSkillMDTO languageSkillRemoto = new LanguageSkillMDTO();
        languageSkillRemoto.setLanguage("TEST2");
        languageSkillRemoto.setCefrLevel("TEST2");

        CCSALSRemoto.setSubjectArea(subjectAreaRemoto);
        CCSALSRemoto.setLanguageSkill(languageSkillRemoto);
        languageSkillListRemoto.add(CCSALSRemoto);

        List<CooperationConditionSubjectAreaMDTO> subjectAreaListRemoto = new ArrayList<>();
        CooperationConditionSubjectAreaMDTO coopCondSubjAreaRemoto = new CooperationConditionSubjectAreaMDTO();

        SubjectAreaMDTO subjectAreaRemoto2 = new SubjectAreaMDTO();
        subjectAreaRemoto2.setIscedCode("TEST2");
        subjectAreaRemoto2.setIscedClarification("TEST2");

        coopCondSubjAreaRemoto.setSubjectArea(subjectAreaRemoto2);
        subjectAreaListRemoto.add(coopCondSubjAreaRemoto);

        remoto.setEndDate(new Date(123));
        remoto.setEqfLevel(eqfLevelListRemoto);
        remoto.setStartDate(new Date(123));
        remoto.setDuration(durationRemoto);
        remoto.setMobilityNumber(mobilityNumberMDTORemoto);
        remoto.setMobilityType(mobilityTypeMDTORemoto);
        remoto.setReceivingPartner(iiaPartnerMDTORemoto);
        remoto.setSendingPartner(iiaPartnerMDTORemoto);
        remoto.setOtherInfo("TEST2");
        remoto.setBlended(Boolean.FALSE);
        remoto.setLanguageSkill(languageSkillListRemoto);
        remoto.setSubjectAreaList(subjectAreaListRemoto);

        return remoto;
    }

    @Test
    public void testCompararIIA_ConCambiosAll() {
        IiaMDTO local = new IiaMDTO();
        IiaMDTO remoto = new IiaMDTO();

        List<CooperationConditionMDTO> cooperationConditionMDTOListLocal = new ArrayList<>();
        List<CooperationConditionMDTO> cooperationConditionMDTOListRemoto = new ArrayList<>();

        cooperationConditionMDTOListLocal.add(generarCooperationConditionLocalTest());
        cooperationConditionMDTOListRemoto.add(generarCooperationConditionRemotoTest());


        IiaPartnerMDTO iiaPartnerLocal = new IiaPartnerMDTO();
        IiaPartnerMDTO iiaPartnerRemoto = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListLocal = new ArrayList<>();
        List<ContactMDTO> contactMDTOListRemoto = new ArrayList<>();
        contactMDTOListLocal.add(generarContactLocalTest());
        contactMDTOListRemoto.add(generarContactRemotoTest());

        iiaPartnerLocal.setInstitutionId("Test");
        iiaPartnerLocal.setOrganizationUnitId("Test");
        iiaPartnerLocal.setSignerPersonId(generarContactLocalTest());
        iiaPartnerLocal.setSigningDate(new Date());
        iiaPartnerLocal.setContacts(contactMDTOListLocal);

        iiaPartnerRemoto.setInstitutionId("Test2");
        iiaPartnerRemoto.setOrganizationUnitId("Test2");
        iiaPartnerRemoto.setSignerPersonId(generarContactRemotoTest());
        iiaPartnerRemoto.setSigningDate(new Date(123));
        iiaPartnerRemoto.setContacts(contactMDTOListRemoto);

        IiaPartnerMDTO iiaPartnerLocal2 = new IiaPartnerMDTO();
        IiaPartnerMDTO iiaPartnerRemoto2 = new IiaPartnerMDTO();
        List<ContactMDTO> contactMDTOListLocal2 = new ArrayList<>();
        List<ContactMDTO> contactMDTOListRemoto2 = new ArrayList<>();
        contactMDTOListLocal2.add(generarContactLocalTest());
        contactMDTOListRemoto2.add(generarContactRemotoTest());

        iiaPartnerLocal2.setInstitutionId("TEST");
        iiaPartnerLocal2.setOrganizationUnitId("TEST");
        iiaPartnerLocal2.setSignerPersonId(generarContactLocalTest());
        iiaPartnerLocal2.setSigningDate(new Date());
        iiaPartnerLocal2.setContacts(contactMDTOListLocal2);

        iiaPartnerRemoto2.setInstitutionId("TEST2");
        iiaPartnerRemoto2.setOrganizationUnitId("TEST2");
        iiaPartnerRemoto2.setSignerPersonId(generarContactRemotoTest());
        iiaPartnerRemoto2.setSigningDate(new Date(123));
        iiaPartnerRemoto2.setContacts(contactMDTOListRemoto2);

        local.setCooperationConditions(cooperationConditionMDTOListLocal);
        local.setIiaCode("TEST");
        local.setStartDate(new Date());
        local.setEndDate(new Date());
        local.setModifyDate(new Date());
        local.setRemoteIiaId("TEST");
        local.setRemoteCoopCondHash("TEST");
        local.setRemoteIiaCode("TEST");
        local.setApprovalDate(new Date());
        local.setApprovalCopCondHash("TEST");
        local.setPdfId("a");
        local.setIsRemote(Boolean.TRUE);
        local.setIsAutogenerated(Boolean.TRUE);
        local.setIsExposed(Boolean.TRUE);
        local.setFirstPartner(iiaPartnerLocal);
        local.setSecondPartner(iiaPartnerLocal2);

        remoto.setCooperationConditions(cooperationConditionMDTOListRemoto);
        remoto.setIiaCode("TEST2");
        remoto.setStartDate(new Date(123));
        remoto.setEndDate(new Date(123));
        remoto.setModifyDate(new Date(123));
        remoto.setRemoteIiaId("TEST2");
        remoto.setRemoteCoopCondHash("TEST2");
        remoto.setRemoteIiaCode("TEST2");
        remoto.setApprovalDate(new Date());
        remoto.setApprovalCopCondHash("TEST");
        remoto.setPdfId("b");
        remoto.setIsRemote(Boolean.FALSE);
        remoto.setIsAutogenerated(Boolean.FALSE);
        remoto.setIsExposed(Boolean.FALSE);
        remoto.setFirstPartner(iiaPartnerRemoto);
        remoto.setSecondPartner(iiaPartnerRemoto2);

        boolean resultado = iiasServiceImpl.compararLocalConRemotoAndModificar(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_SinCambios() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararInformationItem_SinCambiosType() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("Test");
        remoto.setType("Test");

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("Test");
        remoto.setType("Test2");

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType_NullLocal() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType(null);
        remoto.setType("Test2");

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosType_NullRemoto() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("Test");
        remoto.setType(null);

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararInformationItem_ConCambiosAll() {
        InformationItemMDTO local = new InformationItemMDTO();
        InformationItemMDTO remoto = new InformationItemMDTO();

        local.setType("Test");
        local.setContactDetails(generarContactDetailLocalTest());

        remoto.setType("Test2");
        remoto.setContactDetails(generarContactDetailRemotoTest());

        boolean resultado = iiasServiceImpl.compararInformationItem(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_SinCambios() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_SinCambiosName() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("Test");
        remoto.setName("Test");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosName() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("Test");
        remoto.setName("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosName_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName(null);
        remoto.setName("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosName_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setName("Test");
        remoto.setName(null);

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_SinCambiosDescripcion() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("Test");
        remoto.setDescripcion("Test");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosDescripcion() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("Test");
        remoto.setDescripcion("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosDescripcion_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion(null);
        remoto.setDescripcion("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosDescripcion_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setDescripcion("Test");
        remoto.setDescripcion(null);

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_SinCambiosType() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("Test");
        remoto.setType("Test");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertFalse(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosType() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("Test");
        remoto.setType("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosType_NullLocal() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType(null);
        remoto.setType("Test2");

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosType_NullRemoto() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("Test");
        remoto.setType(null);

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }

    @Test
    public void testCompararRequirementsInfo_ConCambiosAll() {
        RequirementsInfoMDTO local = new RequirementsInfoMDTO();
        RequirementsInfoMDTO remoto = new RequirementsInfoMDTO();

        local.setType("Test");
        local.setName("Test");
        local.setDescripcion("Test");
        local.setContactDetails(generarContactDetailLocalTest());

        remoto.setType("Test2");
        remoto.setName("Test2");
        remoto.setDescripcion("Test2");
        remoto.setContactDetails(generarContactDetailRemotoTest());

        boolean resultado = iiasServiceImpl.compararRequirementsInfo(remoto, local);

        assertTrue(resultado);
    }
}