//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * This describes a single "branch" of the "LOS tree": One "parent" node along with all its descendants (via the hasPart
 * elements).
 * <p>
 * Each report element contains a set of such trees (a forest). For example, a report may contain a set of degree
 * programmes, and each of these programme may contain a set of courses. But it is also valid for the report to contain
 * only the courses (without the degree programmes), or a mixture of some programmes alongside some seperate courses
 * (unbound to any degree programme).
 * <p>
 * In theory the depth of such tree is unlimited, but in practice no more than 4 levels are usually reached. Each level
 * has an OPTIONAL type, and these types (if given) SHOULD follow a logical structure - in order of their depth, these
 * would be: "Degree programme", "Module", "Course" and "Class". That is, it is valid to include a "Course" with a
 * "Degree programme" parent, but it would be invalid to include them the other way around.
 * <p>
 * <p>
 * Notes for server implementers =============================
 * <p>
 * Depending on the scenario, EMREX ELMO file will contain only a subset of the student's degree programmes and courses.
 * (E.g. in case of EMREX NCP server, students are allowed to select any subset of courses are to be exported, but only
 * from among the passed/completed courses. This might however be different in other contexts.)
 * <p>
 * <p>
 * Notes of client implemeneters ===================================
 * <p>
 * We cannot guarantee the *all* servers will follow the "max of 4 levels deep" structure, nor that all of them will
 * fill in the "type" elements for all the nodes.
 * <p>
 * If you're trying to import the data into your system, and the "possibly infinite tree" representation seems to be
 * incompatible with your model, then you will need to dynamically analyze the types of all the the nodes included in
 * the report and decide if the structure can be automatically imported.
 *
 *
 * <p>
 * Clase Java para LearningOpportunitySpecification complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="LearningOpportunitySpecification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
 *         &lt;element name="type" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *               &lt;enumeration value="Degree Programme"/&gt;
 *               &lt;enumeration value="Module"/&gt;
 *               &lt;enumeration value="Course"/&gt;
 *               &lt;enumeration value="Class"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="subjectArea" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *         &lt;element name="iscedCode" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="descriptionHtml" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}SimpleHtmlStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="specifies"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="learningOpportunityInstance"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                                     &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element name="academicTerm" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                                       &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                                       &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="status" minOccurs="0"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *                                   &lt;enumeration value="passed"/&gt;
 *                                   &lt;enumeration value="failed"/&gt;
 *                                   &lt;enumeration value="in-progress"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="gradingSchemeLocalId" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                             &lt;element name="resultLabel" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                             &lt;element name="shortenedGrading" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="percentageLower" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="percentageEqual" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                       &lt;element name="percentageHigher" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="resultDistribution" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="category" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                                               &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="credit" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *                                       &lt;element name="level" minOccurs="0"&gt;
 *                                         &lt;simpleType&gt;
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *                                             &lt;enumeration value="Bachelor"/&gt;
 *                                             &lt;enumeration value="Master"/&gt;
 *                                             &lt;enumeration value="PhD"/&gt;
 *                                           &lt;/restriction&gt;
 *                                         &lt;/simpleType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="level" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *                                       &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" minOccurs="0"/&gt;
 *                                       &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="languageOfInstruction" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                             &lt;element name="engagementHours" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                             &lt;element name="attachments" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ref" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="grouping" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                     &lt;attribute name="typeref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;attribute name="idref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
 *                             &lt;element name="diplomaSupplement" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}DiplomaSupplement" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hasPart" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="learningOpportunitySpecification" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}LearningOpportunitySpecification"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LearningOpportunitySpecification", propOrder = {"identifier", "title", "type", "subjectArea",
    "iscedCode", "url", "description", "descriptionHtml", "specifies", "hasPart", "extension"})
public class LearningOpportunitySpecification implements Serializable {

  private static final long serialVersionUID = 6407983607562523464L;
  protected List<LearningOpportunitySpecification.Identifier> identifier;
  @XmlElement(required = true)
  protected List<TokenWithOptionalLang> title;
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  protected String type;
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "token")
  protected String subjectArea;
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "token")
  protected String iscedCode;
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "token")
  protected String url;
  protected List<PlaintextMultilineStringWithOptionalLang> description;
  protected List<SimpleHtmlStringWithOptionalLang> descriptionHtml;
  @XmlElement(required = true)
  protected LearningOpportunitySpecification.Specifies specifies;
  protected List<LearningOpportunitySpecification.HasPart> hasPart;
  protected XmlExtensionContent extension;

  /**
   * Gets the value of the identifier property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the identifier property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getIdentifier().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link LearningOpportunitySpecification.Identifier }
   * 
   * 
   */
  public List<LearningOpportunitySpecification.Identifier> getIdentifier() {
    if (identifier == null) {
      identifier = new ArrayList<LearningOpportunitySpecification.Identifier>();
    }
    return this.identifier;
  }

  /**
   * Gets the value of the title property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the title property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getTitle().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link TokenWithOptionalLang }
   * 
   * 
   */
  public List<TokenWithOptionalLang> getTitle() {
    if (title == null) {
      title = new ArrayList<TokenWithOptionalLang>();
    }
    return this.title;
  }

  /**
   * Obtiene el valor de la propiedad type.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getType() {
    return type;
  }

  /**
   * Define el valor de la propiedad type.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setType(final String value) {
    this.type = value;
  }

  /**
   * Obtiene el valor de la propiedad subjectArea.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSubjectArea() {
    return subjectArea;
  }

  /**
   * Define el valor de la propiedad subjectArea.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSubjectArea(final String value) {
    this.subjectArea = value;
  }

  /**
   * Obtiene el valor de la propiedad iscedCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIscedCode() {
    return iscedCode;
  }

  /**
   * Define el valor de la propiedad iscedCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIscedCode(final String value) {
    this.iscedCode = value;
  }

  /**
   * Obtiene el valor de la propiedad url.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getUrl() {
    return url;
  }

  /**
   * Define el valor de la propiedad url.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setUrl(final String value) {
    this.url = value;
  }

  /**
   * Gets the value of the description property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the description property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getDescription().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link PlaintextMultilineStringWithOptionalLang }
   * 
   * 
   */
  public List<PlaintextMultilineStringWithOptionalLang> getDescription() {
    if (description == null) {
      description = new ArrayList<PlaintextMultilineStringWithOptionalLang>();
    }
    return this.description;
  }

  /**
   * Gets the value of the descriptionHtml property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the descriptionHtml property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getDescriptionHtml().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link SimpleHtmlStringWithOptionalLang }
   * 
   * 
   */
  public List<SimpleHtmlStringWithOptionalLang> getDescriptionHtml() {
    if (descriptionHtml == null) {
      descriptionHtml = new ArrayList<SimpleHtmlStringWithOptionalLang>();
    }
    return this.descriptionHtml;
  }

  /**
   * Obtiene el valor de la propiedad specifies.
   * 
   * @return possible object is {@link LearningOpportunitySpecification.Specifies }
   * 
   */
  public LearningOpportunitySpecification.Specifies getSpecifies() {
    return specifies;
  }

  /**
   * Define el valor de la propiedad specifies.
   * 
   * @param value allowed object is {@link LearningOpportunitySpecification.Specifies }
   * 
   */
  public void setSpecifies(final LearningOpportunitySpecification.Specifies value) {
    this.specifies = value;
  }

  /**
   * Gets the value of the hasPart property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the hasPart property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getHasPart().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link LearningOpportunitySpecification.HasPart }
   * 
   * 
   */
  public List<LearningOpportunitySpecification.HasPart> getHasPart() {
    if (hasPart == null) {
      hasPart = new ArrayList<LearningOpportunitySpecification.HasPart>();
    }
    return this.hasPart;
  }

  /**
   * Obtiene el valor de la propiedad extension.
   * 
   * @return possible object is {@link CustomExtensionsContainer }
   * 
   */
  public XmlExtensionContent getExtension() {
    return extension;
  }

  /**
   * Define el valor de la propiedad extension.
   * 
   * @param value allowed object is {@link CustomExtensionsContainer }
   * 
   */
  public void setExtension(final XmlExtensionContent value) {
    this.extension = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="learningOpportunitySpecification" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}LearningOpportunitySpecification"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"learningOpportunitySpecification"})
  public static class HasPart implements Serializable {

    private static final long serialVersionUID = -7357208467459117983L;
    @XmlElement(required = true)
    protected LearningOpportunitySpecification learningOpportunitySpecification;

    /**
     * Obtiene el valor de la propiedad learningOpportunitySpecification.
     * 
     * @return possible object is {@link LearningOpportunitySpecification }
     * 
     */
    public LearningOpportunitySpecification getLearningOpportunitySpecification() {
      return learningOpportunitySpecification;
    }

    /**
     * Define el valor de la propiedad learningOpportunitySpecification.
     * 
     * @param value allowed object is {@link LearningOpportunitySpecification }
     * 
     */
    public void setLearningOpportunitySpecification(final LearningOpportunitySpecification value) {
      this.learningOpportunitySpecification = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"value"})
  public static class Identifier implements Serializable {

    private static final long serialVersionUID = 3681451289245610418L;
    @XmlValue
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;
    @XmlAttribute(name = "type", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String type;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValue() {
      return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setValue(final String value) {
      this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
      return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setType(final String value) {
      this.type = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="learningOpportunityInstance"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *                           &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element name="academicTerm" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
     *                             &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                             &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="status" minOccurs="0"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
     *                         &lt;enumeration value="passed"/&gt;
     *                         &lt;enumeration value="failed"/&gt;
     *                         &lt;enumeration value="in-progress"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="gradingSchemeLocalId" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *                   &lt;element name="resultLabel" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *                   &lt;element name="shortenedGrading" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="percentageLower" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="percentageEqual" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                             &lt;element name="percentageHigher" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="resultDistribution" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="category" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *                                     &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="credit" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
     *                             &lt;element name="level" minOccurs="0"&gt;
     *                               &lt;simpleType&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
     *                                   &lt;enumeration value="Bachelor"/&gt;
     *                                   &lt;enumeration value="Master"/&gt;
     *                                   &lt;enumeration value="PhD"/&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/simpleType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="level" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
     *                             &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" minOccurs="0"/&gt;
     *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="languageOfInstruction" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *                   &lt;element name="engagementHours" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *                   &lt;element name="attachments" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ref" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="grouping" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                           &lt;attribute name="typeref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                           &lt;attribute name="idref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
     *                   &lt;element name="diplomaSupplement" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}DiplomaSupplement" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"learningOpportunityInstance", "extension"})
  public static class Specifies implements Serializable {

    private static final long serialVersionUID = -8293090138960254414L;
    @XmlElement(required = true)
    protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance learningOpportunityInstance;
    protected XmlExtensionContent extension;

    /**
     * Obtiene el valor de la propiedad learningOpportunityInstance.
     * 
     * @return possible object is {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance }
     * 
     */
    public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance getLearningOpportunityInstance() {
      return learningOpportunityInstance;
    }

    /**
     * Define el valor de la propiedad learningOpportunityInstance.
     * 
     * @param value allowed object is {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance }
     * 
     */
    public void setLearningOpportunityInstance(
        final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance value) {
      this.learningOpportunityInstance = value;
    }

    /**
     * Obtiene el valor de la propiedad extension.
     * 
     * @return possible object is {@link CustomExtensionsContainer }
     * 
     */
    public XmlExtensionContent getExtension() {
      return extension;
    }

    /**
     * Define el valor de la propiedad extension.
     * 
     * @param value allowed object is {@link CustomExtensionsContainer }
     * 
     */
    public void setExtension(final XmlExtensionContent value) {
      this.extension = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
         *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element name="academicTerm" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
         *                   &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *                   &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="status" minOccurs="0"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
         *               &lt;enumeration value="passed"/&gt;
         *               &lt;enumeration value="failed"/&gt;
         *               &lt;enumeration value="in-progress"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="gradingSchemeLocalId" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
         *         &lt;element name="resultLabel" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
         *         &lt;element name="shortenedGrading" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="percentageLower" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="percentageEqual" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                   &lt;element name="percentageHigher" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="resultDistribution" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="category" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *                           &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="credit" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
         *                   &lt;element name="level" minOccurs="0"&gt;
         *                     &lt;simpleType&gt;
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
         *                         &lt;enumeration value="Bachelor"/&gt;
         *                         &lt;enumeration value="Master"/&gt;
         *                         &lt;enumeration value="PhD"/&gt;
         *                       &lt;/restriction&gt;
         *                     &lt;/simpleType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="level" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
         *                   &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" minOccurs="0"/&gt;
         *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="languageOfInstruction" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
         *         &lt;element name="engagementHours" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
         *         &lt;element name="attachments" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ref" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="grouping" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                 &lt;attribute name="typeref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *                 &lt;attribute name="idref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
         *         &lt;element name="diplomaSupplement" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}DiplomaSupplement" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "",
        propOrder = {"identifier", "start", "date", "academicTerm", "status", "gradingSchemeLocalId", "resultLabel",
            "shortenedGrading", "resultDistribution", "credit", "level", "languageOfInstruction", "engagementHours",
            "attachments", "grouping", "extension", "diplomaSupplement"})
    public static class LearningOpportunityInstance implements Serializable {

      private static final long serialVersionUID = -4374917371377089879L;
      protected List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier> identifier;
      @XmlSchemaType(name = "date")
      protected XMLGregorianCalendar start;
      @XmlSchemaType(name = "date")
      protected XMLGregorianCalendar date;
      protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm academicTerm;
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      protected String status;
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String gradingSchemeLocalId;
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String resultLabel;
      protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading shortenedGrading;
      protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution resultDistribution;
      protected List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit> credit;
      protected List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level> level;
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String languageOfInstruction;
      protected BigDecimal engagementHours;
      protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments attachments;
      protected LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping grouping;
      protected XmlExtensionContent extension;
      protected DiplomaSupplement diplomaSupplement;

      /**
       * Gets the value of the identifier property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the identifier property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getIdentifier().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier }
       * 
       * 
       */
      public List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier> getIdentifier() {
        if (identifier == null) {
          identifier =
              new ArrayList<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier>();
        }
        return this.identifier;
      }

      /**
       * Obtiene el valor de la propiedad start.
       * 
       * @return possible object is {@link XMLGregorianCalendar }
       * 
       */
      public XMLGregorianCalendar getStart() {
        return start;
      }

      /**
       * Define el valor de la propiedad start.
       * 
       * @param value allowed object is {@link XMLGregorianCalendar }
       * 
       */
      public void setStart(final XMLGregorianCalendar value) {
        this.start = value;
      }

      /**
       * Obtiene el valor de la propiedad date.
       * 
       * @return possible object is {@link XMLGregorianCalendar }
       * 
       */
      public XMLGregorianCalendar getDate() {
        return date;
      }

      /**
       * Define el valor de la propiedad date.
       * 
       * @param value allowed object is {@link XMLGregorianCalendar }
       * 
       */
      public void setDate(final XMLGregorianCalendar value) {
        this.date = value;
      }

      /**
       * Obtiene el valor de la propiedad academicTerm.
       * 
       * @return possible object is
       *         {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm }
       * 
       */
      public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm getAcademicTerm() {
        return academicTerm;
      }

      /**
       * Define el valor de la propiedad academicTerm.
       * 
       * @param value allowed object is
       *        {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm }
       * 
       */
      public void setAcademicTerm(
          final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm value) {
        this.academicTerm = value;
      }

      /**
       * Obtiene el valor de la propiedad status.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getStatus() {
        return status;
      }

      /**
       * Define el valor de la propiedad status.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setStatus(final String value) {
        this.status = value;
      }

      /**
       * Obtiene el valor de la propiedad gradingSchemeLocalId.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getGradingSchemeLocalId() {
        return gradingSchemeLocalId;
      }

      /**
       * Define el valor de la propiedad gradingSchemeLocalId.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setGradingSchemeLocalId(final String value) {
        this.gradingSchemeLocalId = value;
      }

      /**
       * Obtiene el valor de la propiedad resultLabel.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getResultLabel() {
        return resultLabel;
      }

      /**
       * Define el valor de la propiedad resultLabel.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setResultLabel(final String value) {
        this.resultLabel = value;
      }

      /**
       * Obtiene el valor de la propiedad shortenedGrading.
       * 
       * @return possible object is
       *         {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading }
       * 
       */
      public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading getShortenedGrading() {
        return shortenedGrading;
      }

      /**
       * Define el valor de la propiedad shortenedGrading.
       * 
       * @param value allowed object is
       *        {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading }
       * 
       */
      public void setShortenedGrading(
          final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading value) {
        this.shortenedGrading = value;
      }

      /**
       * Obtiene el valor de la propiedad resultDistribution.
       * 
       * @return possible object is
       *         {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution }
       * 
       */
      public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution getResultDistribution() {
        return resultDistribution;
      }

      /**
       * Define el valor de la propiedad resultDistribution.
       * 
       * @param value allowed object is
       *        {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution }
       * 
       */
      public void setResultDistribution(
          final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution value) {
        this.resultDistribution = value;
      }

      /**
       * Gets the value of the credit property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the credit property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getCredit().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit }
       * 
       * 
       */
      public List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit> getCredit() {
        if (credit == null) {
          credit = new ArrayList<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit>();
        }
        return this.credit;
      }

      /**
       * Gets the value of the level property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the level property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getLevel().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level }
       * 
       * 
       */
      public List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level> getLevel() {
        if (level == null) {
          level = new ArrayList<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level>();
        }
        return this.level;
      }

      /**
       * Obtiene el valor de la propiedad languageOfInstruction.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getLanguageOfInstruction() {
        return languageOfInstruction;
      }

      /**
       * Define el valor de la propiedad languageOfInstruction.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setLanguageOfInstruction(final String value) {
        this.languageOfInstruction = value;
      }

      /**
       * Obtiene el valor de la propiedad engagementHours.
       * 
       * @return possible object is {@link BigDecimal }
       * 
       */
      public BigDecimal getEngagementHours() {
        return engagementHours;
      }

      /**
       * Define el valor de la propiedad engagementHours.
       * 
       * @param value allowed object is {@link BigDecimal }
       * 
       */
      public void setEngagementHours(final BigDecimal value) {
        this.engagementHours = value;
      }

      /**
       * Obtiene el valor de la propiedad attachments.
       * 
       * @return possible object is
       *         {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments }
       * 
       */
      public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments getAttachments() {
        return attachments;
      }

      /**
       * Define el valor de la propiedad attachments.
       * 
       * @param value allowed object is
       *        {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments }
       * 
       */
      public void setAttachments(
          final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments value) {
        this.attachments = value;
      }

      /**
       * Obtiene el valor de la propiedad grouping.
       * 
       * @return possible object is
       *         {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping }
       * 
       */
      public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping getGrouping() {
        return grouping;
      }

      /**
       * Define el valor de la propiedad grouping.
       * 
       * @param value allowed object is
       *        {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping }
       * 
       */
      public void setGrouping(
          final LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping value) {
        this.grouping = value;
      }

      /**
       * Obtiene el valor de la propiedad extension.
       * 
       * @return possible object is {@link CustomExtensionsContainer }
       * 
       */
      public XmlExtensionContent getExtension() {
        return extension;
      }

      /**
       * Define el valor de la propiedad extension.
       * 
       * @param value allowed object is {@link CustomExtensionsContainer }
       * 
       */
      public void setExtension(final XmlExtensionContent value) {
        this.extension = value;
      }

      /**
       * Obtiene el valor de la propiedad diplomaSupplement.
       * 
       * @return possible object is {@link DiplomaSupplement }
       * 
       */
      public DiplomaSupplement getDiplomaSupplement() {
        return diplomaSupplement;
      }

      /**
       * Define el valor de la propiedad diplomaSupplement.
       * 
       * @param value allowed object is {@link DiplomaSupplement }
       * 
       */
      public void setDiplomaSupplement(final DiplomaSupplement value) {
        this.diplomaSupplement = value;
      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
             *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
             *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"title", "start", "end"})
      public static class AcademicTerm implements Serializable {

        private static final long serialVersionUID = -7208734354489940808L;
        @XmlElement(required = true)
        protected List<TokenWithOptionalLang> title;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar start;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar end;

        /**
         * Gets the value of the title property.
         * 
         * <p>
         * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you
         * make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE>
         * method for the title property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * 
         * <pre>
         * getTitle().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list {@link TokenWithOptionalLang }
         * 
         * 
         */
        public List<TokenWithOptionalLang> getTitle() {
          if (title == null) {
            title = new ArrayList<TokenWithOptionalLang>();
          }
          return this.title;
        }

        /**
         * Obtiene el valor de la propiedad start.
         * 
         * @return possible object is {@link XMLGregorianCalendar }
         * 
         */
        public XMLGregorianCalendar getStart() {
          return start;
        }

        /**
         * Define el valor de la propiedad start.
         * 
         * @param value allowed object is {@link XMLGregorianCalendar }
         * 
         */
        public void setStart(final XMLGregorianCalendar value) {
          this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         * 
         * @return possible object is {@link XMLGregorianCalendar }
         * 
         */
        public XMLGregorianCalendar getEnd() {
          return end;
        }

        /**
         * Define el valor de la propiedad end.
         * 
         * @param value allowed object is {@link XMLGregorianCalendar }
         * 
         */
        public void setEnd(final XMLGregorianCalendar value) {
          this.end = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ref" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"ref"})
      public static class Attachments implements Serializable {

        private static final long serialVersionUID = -2008327796962567446L;
        protected List<String> ref;

        /**
         * Gets the value of the ref property.
         * 
         * <p>
         * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you
         * make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE>
         * method for the ref property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * 
         * <pre>
         * getRef().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list {@link String }
         * 
         * 
         */
        public List<String> getRef() {
          if (ref == null) {
            ref = new ArrayList<String>();
          }
          return this.ref;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
             *         &lt;element name="level" minOccurs="0"&gt;
             *           &lt;simpleType&gt;
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
             *               &lt;enumeration value="Bachelor"/&gt;
             *               &lt;enumeration value="Master"/&gt;
             *               &lt;enumeration value="PhD"/&gt;
             *             &lt;/restriction&gt;
             *           &lt;/simpleType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"scheme", "level", "value"})
      public static class Credit implements Serializable {

        private static final long serialVersionUID = -5418354489326176244L;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String scheme;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        protected String level;
        protected BigDecimal value;

        /**
         * Obtiene el valor de la propiedad scheme.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getScheme() {
          return scheme;
        }

        /**
         * Define el valor de la propiedad scheme.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setScheme(final String value) {
          this.scheme = value;
        }

        /**
         * Obtiene el valor de la propiedad level.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getLevel() {
          return level;
        }

        /**
         * Define el valor de la propiedad level.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setLevel(final String value) {
          this.level = value;
        }

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return possible object is {@link BigDecimal }
         * 
         */
        public BigDecimal getValue() {
          return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value allowed object is {@link BigDecimal }
         * 
         */
        public void setValue(final BigDecimal value) {
          this.value = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *       &lt;attribute name="typeref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *       &lt;attribute name="idref" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"value"})
      public static class Grouping implements Serializable {

        private static final long serialVersionUID = 5667793894146494598L;
        @XmlValue
        protected String value;
        @XmlAttribute(name = "typeref")
        protected String typeref;
        @XmlAttribute(name = "idref")
        protected String idref;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getValue() {
          return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setValue(final String value) {
          this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad typeref.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getTyperef() {
          return typeref;
        }

        /**
         * Define el valor de la propiedad typeref.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setTyperef(final String value) {
          this.typeref = value;
        }

        /**
         * Obtiene el valor de la propiedad idref.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getIdref() {
          return idref;
        }

        /**
         * Define el valor de la propiedad idref.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setIdref(final String value) {
          this.idref = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
             *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"value"})
      public static class Identifier implements Serializable {

        private static final long serialVersionUID = -8504014076018624562L;
        @XmlValue
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String value;
        @XmlAttribute(name = "type", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String type;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getValue() {
          return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setValue(final String value) {
          this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getType() {
          return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setType(final String value) {
          this.type = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
             *         &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" minOccurs="0"/&gt;
             *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"type", "description", "value"})
      public static class Level implements Serializable {

        private static final long serialVersionUID = -1683028210271256306L;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String type;
        protected PlaintextMultilineStringWithOptionalLang description;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String value;

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getType() {
          return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setType(final String value) {
          this.type = value;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return possible object is {@link PlaintextMultilineStringWithOptionalLang }
         * 
         */
        public PlaintextMultilineStringWithOptionalLang getDescription() {
          return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value allowed object is {@link PlaintextMultilineStringWithOptionalLang }
         * 
         */
        public void setDescription(final PlaintextMultilineStringWithOptionalLang value) {
          this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getValue() {
          return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setValue(final String value) {
          this.value = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="category" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
             *                 &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"category", "description"})
      public static class ResultDistribution implements Serializable {

        private static final long serialVersionUID = 1134255794376123850L;
        @XmlElement(required = true)
        protected List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category> category;
        protected List<PlaintextMultilineStringWithOptionalLang> description;

        /**
         * Gets the value of the category property.
         * 
         * <p>
         * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you
         * make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE>
         * method for the category property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * 
         * <pre>
         * getCategory().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category }
         * 
         * 
         */
        public List<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category> getCategory() {
          if (category == null) {
            category =
                new ArrayList<LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category>();
          }
          return this.category;
        }

        /**
         * Gets the value of the description property.
         * 
         * <p>
         * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you
         * make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE>
         * method for the description property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * 
         * <pre>
         * getDescription().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list {@link PlaintextMultilineStringWithOptionalLang }
         * 
         * 
         */
        public List<PlaintextMultilineStringWithOptionalLang> getDescription() {
          if (description == null) {
            description = new ArrayList<PlaintextMultilineStringWithOptionalLang>();
          }
          return this.description;
        }


                /**
                 * <p>
                 * Clase Java para anonymous complex type.
                 *
                 * <p>
                 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
                 *       &lt;attribute name="count" use="required" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Category implements Serializable {

          private static final long serialVersionUID = -4170356502576050896L;
          @XmlAttribute(name = "label", required = true)
          @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
          @XmlSchemaType(name = "token")
          protected String label;
          @XmlAttribute(name = "count", required = true)
          @XmlSchemaType(name = "nonNegativeInteger")
          protected BigInteger count;

          /**
           * Obtiene el valor de la propiedad label.
           * 
           * @return possible object is {@link String }
           * 
           */
          public String getLabel() {
            return label;
          }

          /**
           * Define el valor de la propiedad label.
           * 
           * @param value allowed object is {@link String }
           * 
           */
          public void setLabel(final String value) {
            this.label = value;
          }

          /**
           * Obtiene el valor de la propiedad count.
           * 
           * @return possible object is {@link BigInteger }
           * 
           */
          public BigInteger getCount() {
            return count;
          }

          /**
           * Define el valor de la propiedad count.
           * 
           * @param value allowed object is {@link BigInteger }
           * 
           */
          public void setCount(final BigInteger value) {
            this.count = value;
          }

        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="percentageLower" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="percentageEqual" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *         &lt;element name="percentageHigher" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"percentageLower", "percentageEqual", "percentageHigher"})
      public static class ShortenedGrading implements Serializable {

        private static final long serialVersionUID = 9202226758132276706L;
        @XmlElement(required = true)
        protected BigDecimal percentageLower;
        @XmlElement(required = true)
        protected BigDecimal percentageEqual;
        @XmlElement(required = true)
        protected BigDecimal percentageHigher;

        /**
         * Obtiene el valor de la propiedad percentageLower.
         * 
         * @return possible object is {@link BigDecimal }
         * 
         */
        public BigDecimal getPercentageLower() {
          return percentageLower;
        }

        /**
         * Define el valor de la propiedad percentageLower.
         * 
         * @param value allowed object is {@link BigDecimal }
         * 
         */
        public void setPercentageLower(final BigDecimal value) {
          this.percentageLower = value;
        }

        /**
         * Obtiene el valor de la propiedad percentageEqual.
         * 
         * @return possible object is {@link BigDecimal }
         * 
         */
        public BigDecimal getPercentageEqual() {
          return percentageEqual;
        }

        /**
         * Define el valor de la propiedad percentageEqual.
         * 
         * @param value allowed object is {@link BigDecimal }
         * 
         */
        public void setPercentageEqual(final BigDecimal value) {
          this.percentageEqual = value;
        }

        /**
         * Obtiene el valor de la propiedad percentageHigher.
         * 
         * @return possible object is {@link BigDecimal }
         * 
         */
        public BigDecimal getPercentageHigher() {
          return percentageHigher;
        }

        /**
         * Define el valor de la propiedad percentageHigher.
         * 
         * @param value allowed object is {@link BigDecimal }
         * 
         */
        public void setPercentageHigher(final BigDecimal value) {
          this.percentageHigher = value;
        }

      }

    }

  }

}
