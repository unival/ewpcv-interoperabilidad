//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * The section element in the DS refers to the official section numbers (1, 2...), and can include other sections (1.1,
 * 1.2, 2.1, 2.2...)
 *
 *
 * <p>
 * Clase Java para DiplomaSupplementSection complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DiplomaSupplementSection"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *         &lt;element name="content" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                 &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}token" default="text/plain" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="additionalInformation" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="attachment" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}Attachment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="section" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}DiplomaSupplementSection" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="number" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;pattern value="[0-9\.]+"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiplomaSupplementSection",
    propOrder = {"title", "content", "additionalInformation", "attachment", "section"})
public class DiplomaSupplementSection implements Serializable {

  @XmlElement(required = true)
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "token")
  protected String title;
  protected DiplomaSupplementSection.Content content;
  protected List<String> additionalInformation;
  protected List<Attachment> attachment;
  protected List<DiplomaSupplementSection> section;
  @XmlAttribute(name = "number", required = true)
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  protected String number;

  /**
   * Obtiene el valor de la propiedad title.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getTitle() {
    return title;
  }

  /**
   * Define el valor de la propiedad title.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * Obtiene el valor de la propiedad content.
   * 
   * @return possible object is {@link DiplomaSupplementSection.Content }
   * 
   */
  public DiplomaSupplementSection.Content getContent() {
    return content;
  }

  /**
   * Define el valor de la propiedad content.
   * 
   * @param value allowed object is {@link DiplomaSupplementSection.Content }
   * 
   */
  public void setContent(DiplomaSupplementSection.Content value) {
    this.content = value;
  }

  /**
   * Gets the value of the additionalInformation property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the additionalInformation property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getAdditionalInformation().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link String }
   * 
   * 
   */
  public List<String> getAdditionalInformation() {
    if (additionalInformation == null) {
      additionalInformation = new ArrayList<String>();
    }
    return this.additionalInformation;
  }

  /**
   * Gets the value of the attachment property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the attachment property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getAttachment().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Attachment }
   * 
   * 
   */
  public List<Attachment> getAttachment() {
    if (attachment == null) {
      attachment = new ArrayList<Attachment>();
    }
    return this.attachment;
  }

  /**
   * Gets the value of the section property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the section property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getSection().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link DiplomaSupplementSection }
   * 
   * 
   */
  public List<DiplomaSupplementSection> getSection() {
    if (section == null) {
      section = new ArrayList<DiplomaSupplementSection>();
    }
    return this.section;
  }

  /**
   * Obtiene el valor de la propiedad number.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getNumber() {
    return number;
  }

  /**
   * Define el valor de la propiedad number.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setNumber(String value) {
    this.number = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}token" default="text/plain" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"value"})
  public static class Content implements Serializable {

    @XmlValue
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;
    @XmlAttribute(name = "type")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String type;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValue() {
      return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setValue(String value) {
      this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
      if (type == null) {
        return "text/plain";
      } else {
        return type;
      }
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setType(String value) {
      this.type = value;
    }

  }

}
