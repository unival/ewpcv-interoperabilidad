//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;


/**
 * Groups is a way of organizing and sorting over groups of LOI's.
 *
 *
 * <p>
 * Clase Java para Groups complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Groups"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="groupType" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang"/&gt;
 *                   &lt;element name="group" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                           &lt;attribute name="sortingKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Groups", propOrder = {"groupType"})
public class Groups implements Serializable {

  protected List<Groups.GroupType> groupType;

  /**
   * Gets the value of the groupType property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the groupType property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getGroupType().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Groups.GroupType }
   * 
   * 
   */
  public List<Groups.GroupType> getGroupType() {
    if (groupType == null) {
      groupType = new ArrayList<Groups.GroupType>();
    }
    return this.groupType;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang"/&gt;
     *         &lt;element name="group" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                 &lt;attribute name="sortingKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"title", "group"})
  public static class GroupType implements Serializable {

    @XmlElement(required = true)
    protected TokenWithOptionalLang title;
    protected List<Groups.GroupType.Group> group;
    @XmlAttribute(name = "id")
    protected String id;

    /**
     * Obtiene el valor de la propiedad title.
     * 
     * @return possible object is {@link TokenWithOptionalLang }
     * 
     */
    public TokenWithOptionalLang getTitle() {
      return title;
    }

    /**
     * Define el valor de la propiedad title.
     * 
     * @param value allowed object is {@link TokenWithOptionalLang }
     * 
     */
    public void setTitle(TokenWithOptionalLang value) {
      this.title = value;
    }

    /**
     * Gets the value of the group property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the group property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Groups.GroupType.Group }
     * 
     * 
     */
    public List<Groups.GroupType.Group> getGroup() {
      if (group == null) {
        group = new ArrayList<Groups.GroupType.Group>();
      }
      return this.group;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getId() {
      return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setId(String value) {
      this.id = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *       &lt;attribute name="sortingKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"title"})
    public static class Group implements Serializable {

      @XmlElement(required = true)
      protected TokenWithOptionalLang title;
      @XmlAttribute(name = "id")
      protected String id;
      @XmlAttribute(name = "sortingKey")
      protected String sortingKey;

      /**
       * Obtiene el valor de la propiedad title.
       * 
       * @return possible object is {@link TokenWithOptionalLang }
       * 
       */
      public TokenWithOptionalLang getTitle() {
        return title;
      }

      /**
       * Define el valor de la propiedad title.
       * 
       * @param value allowed object is {@link TokenWithOptionalLang }
       * 
       */
      public void setTitle(TokenWithOptionalLang value) {
        this.title = value;
      }

      /**
       * Obtiene el valor de la propiedad id.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getId() {
        return id;
      }

      /**
       * Define el valor de la propiedad id.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setId(String value) {
        this.id = value;
      }

      /**
       * Obtiene el valor de la propiedad sortingKey.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getSortingKey() {
        return sortingKey;
      }

      /**
       * Define el valor de la propiedad sortingKey.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setSortingKey(String value) {
        this.sortingKey = value;
      }

    }

  }

}
