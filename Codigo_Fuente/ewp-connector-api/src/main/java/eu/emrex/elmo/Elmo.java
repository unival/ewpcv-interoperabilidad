//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import org.w3.xmldsig.SignatureType;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;
import eu.europa.cedefop.europass.europass.v2.CountryCode;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="generatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="learner"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="citizenship" type="{http://europass.cedefop.europa.eu/Europass/V2.0}countryCode" minOccurs="0"/&gt;
 *                   &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                           &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="givenNames" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *                   &lt;element name="familyName" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *                   &lt;element name="bday" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                   &lt;element name="placeOfBirth" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                   &lt;element name="birthName" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
 *                   &lt;element name="currentAddress" type="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}FlexibleAddress" minOccurs="0"/&gt;
 *                   &lt;element name="gender" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *                         &lt;enumeration value="0"/&gt;
 *                         &lt;enumeration value="1"/&gt;
 *                         &lt;enumeration value="2"/&gt;
 *                         &lt;enumeration value="9"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="report" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="issuer"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="country" type="{http://europass.cedefop.europa.eu/Europass/V2.0}countryCode" minOccurs="0"/&gt;
 *                             &lt;element name="identifier" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                                     &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                             &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="learningOpportunitySpecification" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}LearningOpportunitySpecification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *                   &lt;element name="gradingScheme" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="localId" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="attachment" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}Attachment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="attachment" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}Attachment" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="groups" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}Groups" minOccurs="0"/&gt;
 *         &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
    propOrder = {"generatedDate", "learner", "report", "attachment", "groups", "extension", "signature"})
@XmlRootElement(name = "elmo")
public class Elmo implements Serializable {

  private static final long serialVersionUID = 9168452360104324581L;
  @XmlElement(required = true)
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar generatedDate;
  @XmlElement(required = true)
  protected Elmo.Learner learner;
  @XmlElement(required = true)
  protected List<Elmo.Report> report;
  protected List<Attachment> attachment;
  protected Groups groups;
  protected XmlExtensionContent extension;
  @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
  protected SignatureType signature;

  /**
   * Obtiene el valor de la propiedad generatedDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getGeneratedDate() {
    return generatedDate;
  }

  /**
   * Define el valor de la propiedad generatedDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setGeneratedDate(final XMLGregorianCalendar value) {
    this.generatedDate = value;
  }

  /**
   * Obtiene el valor de la propiedad learner.
   * 
   * @return possible object is {@link Elmo.Learner }
   * 
   */
  public Elmo.Learner getLearner() {
    return learner;
  }

  /**
   * Define el valor de la propiedad learner.
   * 
   * @param value allowed object is {@link Elmo.Learner }
   * 
   */
  public void setLearner(final Elmo.Learner value) {
    this.learner = value;
  }

  /**
   * Gets the value of the report property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the report property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getReport().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Elmo.Report }
   * 
   * 
   */
  public List<Elmo.Report> getReport() {
    if (report == null) {
      report = new ArrayList<Elmo.Report>();
    }
    return this.report;
  }

  /**
   * Gets the value of the attachment property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the attachment property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getAttachment().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Attachment }
   * 
   * 
   */
  public List<Attachment> getAttachment() {
    if (attachment == null) {
      attachment = new ArrayList<Attachment>();
    }
    return this.attachment;
  }

  /**
   * Obtiene el valor de la propiedad groups.
   * 
   * @return possible object is {@link Groups }
   * 
   */
  public Groups getGroups() {
    return groups;
  }

  /**
   * Define el valor de la propiedad groups.
   * 
   * @param value allowed object is {@link Groups }
   * 
   */
  public void setGroups(final Groups value) {
    this.groups = value;
  }

  /**
   * Obtiene el valor de la propiedad extension.
   * 
   * @return possible object is {@link CustomExtensionsContainer }
   * 
   */
  public XmlExtensionContent getExtension() {
    return extension;
  }

  /**
   * Define el valor de la propiedad extension.
   * 
   * @param value allowed object is {@link CustomExtensionsContainer }
   * 
   */
  public void setExtension(final XmlExtensionContent value) {
    this.extension = value;
  }

  /**
   * Every EMREX ELMO element MUST be signed with xmldsig-core2 enveloped signature. The ds:SignedInfo element MUST
   * contain a single ds:Reference with an empty URI. The key used by the server for signing must often match some
   * external criteria, which are NOT documented here. (E.g. If you're implementing EMREX NCP, then the certificate used
   * must match the one previously published in EMREG for your NCP.) The ds:Signature element SHOULD contain the copy of
   * the its certficate in the ds:KeyInfo element.
   * 
   * 
   * @return possible object is {@link SignatureType }
   * 
   */
  public SignatureType getSignature() {
    return signature;
  }

  /**
   * Define el valor de la propiedad signature.
   * 
   * @param value allowed object is {@link SignatureType }
   * 
   */
  public void setSignature(final SignatureType value) {
    this.signature = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="citizenship" type="{http://europass.cedefop.europa.eu/Europass/V2.0}countryCode" minOccurs="0"/&gt;
     *         &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="givenNames" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
     *         &lt;element name="familyName" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
     *         &lt;element name="bday" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *         &lt;element name="placeOfBirth" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *         &lt;element name="birthName" type="{http://www.w3.org/2001/XMLSchema}token" minOccurs="0"/&gt;
     *         &lt;element name="currentAddress" type="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}FlexibleAddress" minOccurs="0"/&gt;
     *         &lt;element name="gender" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
     *               &lt;enumeration value="0"/&gt;
     *               &lt;enumeration value="1"/&gt;
     *               &lt;enumeration value="2"/&gt;
     *               &lt;enumeration value="9"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"citizenship", "identifier", "givenNames", "familyName", "bday", "placeOfBirth",
      "birthName", "currentAddress", "gender"})
  public static class Learner implements Serializable {

    private static final long serialVersionUID = -1279078972158456463L;
    @XmlSchemaType(name = "string")
    protected CountryCode citizenship;
    protected List<Elmo.Learner.Identifier> identifier;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String givenNames;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String familyName;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bday;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String placeOfBirth;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String birthName;
    protected FlexibleAddress currentAddress;
    protected BigInteger gender;

    /**
     * Obtiene el valor de la propiedad citizenship.
     * 
     * @return possible object is {@link CountryCode }
     * 
     */
    public CountryCode getCitizenship() {
      return citizenship;
    }

    /**
     * Define el valor de la propiedad citizenship.
     * 
     * @param value allowed object is {@link CountryCode }
     * 
     */
    public void setCitizenship(final CountryCode value) {
      this.citizenship = value;
    }

    /**
     * Gets the value of the identifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the identifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Elmo.Learner.Identifier }
     * 
     * 
     */
    public List<Elmo.Learner.Identifier> getIdentifier() {
      if (identifier == null) {
        identifier = new ArrayList<Elmo.Learner.Identifier>();
      }
      return this.identifier;
    }

    /**
     * Obtiene el valor de la propiedad givenNames.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getGivenNames() {
      return givenNames;
    }

    /**
     * Define el valor de la propiedad givenNames.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setGivenNames(final String value) {
      this.givenNames = value;
    }

    /**
     * Obtiene el valor de la propiedad familyName.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFamilyName() {
      return familyName;
    }

    /**
     * Define el valor de la propiedad familyName.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setFamilyName(final String value) {
      this.familyName = value;
    }

    /**
     * Obtiene el valor de la propiedad bday.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getBday() {
      return bday;
    }

    /**
     * Define el valor de la propiedad bday.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setBday(final XMLGregorianCalendar value) {
      this.bday = value;
    }

    /**
     * Obtiene el valor de la propiedad placeOfBirth.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPlaceOfBirth() {
      return placeOfBirth;
    }

    /**
     * Define el valor de la propiedad placeOfBirth.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPlaceOfBirth(final String value) {
      this.placeOfBirth = value;
    }

    /**
     * Obtiene el valor de la propiedad birthName.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getBirthName() {
      return birthName;
    }

    /**
     * Define el valor de la propiedad birthName.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setBirthName(final String value) {
      this.birthName = value;
    }

    /**
     * Obtiene el valor de la propiedad currentAddress.
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getCurrentAddress() {
      return currentAddress;
    }

    /**
     * Define el valor de la propiedad currentAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setCurrentAddress(final FlexibleAddress value) {
      this.currentAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad gender.
     * 
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getGender() {
      return gender;
    }

    /**
     * Define el valor de la propiedad gender.
     * 
     * @param value allowed object is {@link BigInteger }
     * 
     */
    public void setGender(final BigInteger value) {
      this.gender = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
         *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    public static class Identifier implements Serializable {

      private static final long serialVersionUID = -8770920857513255071L;
      @XmlValue
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String value;
      @XmlAttribute(name = "type", required = true)
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String type;

      /**
       * Obtiene el valor de la propiedad value.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getValue() {
        return value;
      }

      /**
       * Define el valor de la propiedad value.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setValue(final String value) {
        this.value = value;
      }

      /**
       * Obtiene el valor de la propiedad type.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getType() {
        return type;
      }

      /**
       * Define el valor de la propiedad type.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setType(final String value) {
        this.type = value;
      }

    }

  }


    /**
     * This describes an institution (issuer) and a *subset* of courses *completed* by the student within this
     * institution.
     *
     *
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="issuer"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="country" type="{http://europass.cedefop.europa.eu/Europass/V2.0}countryCode" minOccurs="0"/&gt;
     *                   &lt;element name="identifier" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *                           &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
     *                   &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="learningOpportunitySpecification" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}LearningOpportunitySpecification" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
     *         &lt;element name="gradingScheme" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="localId" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="attachment" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}Attachment" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "",
      propOrder = {"issuer", "learningOpportunitySpecification", "issueDate", "gradingScheme", "attachment"})
  public static class Report implements Serializable {

    private static final long serialVersionUID = 1007090433713129373L;
    @XmlElement(required = true)
    protected Elmo.Report.Issuer issuer;
    protected List<LearningOpportunitySpecification> learningOpportunitySpecification;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar issueDate;
    protected List<Elmo.Report.GradingScheme> gradingScheme;
    protected List<Attachment> attachment;

    /**
     * Obtiene el valor de la propiedad issuer.
     * 
     * @return possible object is {@link Elmo.Report.Issuer }
     * 
     */
    public Elmo.Report.Issuer getIssuer() {
      return issuer;
    }

    /**
     * Define el valor de la propiedad issuer.
     * 
     * @param value allowed object is {@link Elmo.Report.Issuer }
     * 
     */
    public void setIssuer(final Elmo.Report.Issuer value) {
      this.issuer = value;
    }

    /**
     * Gets the value of the learningOpportunitySpecification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the learningOpportunitySpecification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getLearningOpportunitySpecification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link LearningOpportunitySpecification }
     * 
     * 
     */
    public List<LearningOpportunitySpecification> getLearningOpportunitySpecification() {
      if (learningOpportunitySpecification == null) {
        learningOpportunitySpecification = new ArrayList<LearningOpportunitySpecification>();
      }
      return this.learningOpportunitySpecification;
    }

    /**
     * Obtiene el valor de la propiedad issueDate.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getIssueDate() {
      return issueDate;
    }

    /**
     * Define el valor de la propiedad issueDate.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setIssueDate(final XMLGregorianCalendar value) {
      this.issueDate = value;
    }

    /**
     * Gets the value of the gradingScheme property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the gradingScheme property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getGradingScheme().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Elmo.Report.GradingScheme }
     * 
     * 
     */
    public List<Elmo.Report.GradingScheme> getGradingScheme() {
      if (gradingScheme == null) {
        gradingScheme = new ArrayList<Elmo.Report.GradingScheme>();
      }
      return this.gradingScheme;
    }

    /**
     * Gets the value of the attachment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the attachment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAttachment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Attachment }
     * 
     * 
     */
    public List<Attachment> getAttachment() {
      if (attachment == null) {
        attachment = new ArrayList<Attachment>();
      }
      return this.attachment;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="localId" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"description"})
    public static class GradingScheme implements Serializable {

      private static final long serialVersionUID = -1434190717537578581L;
      protected List<PlaintextMultilineStringWithOptionalLang> description;
      @XmlAttribute(name = "localId", required = true)
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String localId;

      /**
       * Gets the value of the description property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the description property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getDescription().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list {@link PlaintextMultilineStringWithOptionalLang }
       * 
       * 
       */
      public List<PlaintextMultilineStringWithOptionalLang> getDescription() {
        if (description == null) {
          description = new ArrayList<PlaintextMultilineStringWithOptionalLang>();
        }
        return this.description;
      }

      /**
       * Obtiene el valor de la propiedad localId.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getLocalId() {
        return localId;
      }

      /**
       * Define el valor de la propiedad localId.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setLocalId(final String value) {
        this.localId = value;
      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="country" type="{http://europass.cedefop.europa.eu/Europass/V2.0}countryCode" minOccurs="0"/&gt;
         *         &lt;element name="identifier" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
         *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded"/&gt;
         *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"country", "identifier", "title", "url"})
    public static class Issuer implements Serializable {

      private static final long serialVersionUID = -8993936629730498359L;
      @XmlSchemaType(name = "string")
      protected CountryCode country;
      @XmlElement(required = true)
      protected List<Elmo.Report.Issuer.Identifier> identifier;
      @XmlElement(required = true)
      protected List<TokenWithOptionalLang> title;
      @XmlElement(required = true)
      @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
      @XmlSchemaType(name = "token")
      protected String url;

      /**
       * Obtiene el valor de la propiedad country.
       * 
       * @return possible object is {@link CountryCode }
       * 
       */
      public CountryCode getCountry() {
        return country;
      }

      /**
       * Define el valor de la propiedad country.
       * 
       * @param value allowed object is {@link CountryCode }
       * 
       */
      public void setCountry(final CountryCode value) {
        this.country = value;
      }

      /**
       * Gets the value of the identifier property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the identifier property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getIdentifier().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list {@link Elmo.Report.Issuer.Identifier }
       * 
       * 
       */
      public List<Elmo.Report.Issuer.Identifier> getIdentifier() {
        if (identifier == null) {
          identifier = new ArrayList<Elmo.Report.Issuer.Identifier>();
        }
        return this.identifier;
      }

      /**
       * Gets the value of the title property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the title property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getTitle().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list {@link TokenWithOptionalLang }
       * 
       * 
       */
      public List<TokenWithOptionalLang> getTitle() {
        if (title == null) {
          title = new ArrayList<TokenWithOptionalLang>();
        }
        return this.title;
      }

      /**
       * Obtiene el valor de la propiedad url.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getUrl() {
        return url;
      }

      /**
       * Define el valor de la propiedad url.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setUrl(final String value) {
        this.url = value;
      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
             *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"value"})
      public static class Identifier implements Serializable {

        private static final long serialVersionUID = -7092995065775582069L;
        @XmlValue
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String value;
        @XmlAttribute(name = "type", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String type;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getValue() {
          return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setValue(final String value) {
          this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad type.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getType() {
          return type;
        }

        /**
         * Define el valor de la propiedad type.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setType(final String value) {
          this.type = value;
        }

      }

    }

  }

}
