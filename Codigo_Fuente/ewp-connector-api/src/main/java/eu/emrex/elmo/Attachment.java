//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Notes for server implementers:
 * <p>
 * The attachements MUST contain a human-readable version of all the crucial data contained within the reports. This
 * also includes all the data which may help to prove the identity of the student (such as birth date). You MAY include
 * all this data in a single file, or you MAY split it into several ones (e.g. one PDF per report) - we're leaving this
 * decision to individual server implementers.
 * <p>
 * The attachments SHOULD be in PDF format. All other types of attachments MAY be ignored by the clients.
 * <p>
 * If you wonder why attachements are required, then you should remember, that some clients MAY ignore all
 * learningOpportunitySpecification elements (even if they are 100% valid) and simply choose to forward the attached
 * transcript of records to a staff member for manual processing (this is a valid EMREX ELMO use case and all servers
 * need to support it).
 * <p>
 * Try to order your attachments by importance (we'll leave it for the implementer to decide which attachments are more
 * important).
 * <p>
 * Also, try to keep them lightweight - some clients will need to store your EMREX ELMO files for some time. Take their
 * storage space into account when you think about embedding logos or fonts inside your PDFs!
 * <p>
 * Notes for client implementers:
 * <p>
 * You will probably need these attachments in case when something goes wrong, and you want this response to be imported
 * or verified manually.
 * <p>
 * You MAY also use the attachment to view a human-readable "preview" of the ELMO data for the student.
 *
 *
 * <p>
 * Clase Java para Attachment complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Attachment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="identifier" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
 *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="title" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="type" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *               &lt;enumeration value="Diploma"/&gt;
 *               &lt;enumeration value="Diploma Supplement"/&gt;
 *               &lt;enumeration value="Transcript of Records"/&gt;
 *               &lt;enumeration value="EMREX transcript"/&gt;
 *               &lt;enumeration value="Micro Credential"/&gt;
 *               &lt;enumeration value="Letter of Nomination"/&gt;
 *               &lt;enumeration value="Certificate of Training"/&gt;
 *               &lt;enumeration value="Learning Agreement"/&gt;
 *               &lt;enumeration value="Other"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="description" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}PlaintextMultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="content" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}TokenWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="extension" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}CustomExtensionsContainer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Attachment", propOrder = {"identifier", "title", "type", "description", "content", "extension"})
public class Attachment implements Serializable {

  private static final long serialVersionUID = -2963257905913749832L;
  protected List<Attachment.Identifier> identifier;
  protected List<TokenWithOptionalLang> title;
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  protected String type;
  protected List<PlaintextMultilineStringWithOptionalLang> description;
  protected List<TokenWithOptionalLang> content;
  protected XmlExtensionContent extension;

  /**
   * Gets the value of the identifier property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the identifier property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getIdentifier().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Attachment.Identifier }
   * 
   * 
   */
  public List<Attachment.Identifier> getIdentifier() {
    if (identifier == null) {
      identifier = new ArrayList<Attachment.Identifier>();
    }
    return this.identifier;
  }

  /**
   * Gets the value of the title property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the title property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getTitle().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link TokenWithOptionalLang }
   * 
   * 
   */
  public List<TokenWithOptionalLang> getTitle() {
    if (title == null) {
      title = new ArrayList<TokenWithOptionalLang>();
    }
    return this.title;
  }

  /**
   * Obtiene el valor de la propiedad type.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getType() {
    return type;
  }

  /**
   * Define el valor de la propiedad type.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setType(final String value) {
    this.type = value;
  }

  /**
   * Gets the value of the description property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the description property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getDescription().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link PlaintextMultilineStringWithOptionalLang }
   * 
   * 
   */
  public List<PlaintextMultilineStringWithOptionalLang> getDescription() {
    if (description == null) {
      description = new ArrayList<PlaintextMultilineStringWithOptionalLang>();
    }
    return this.description;
  }

  /**
   * Gets the value of the content property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the content property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getContent().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link TokenWithOptionalLang }
   * 
   * 
   */
  public List<TokenWithOptionalLang> getContent() {
    if (content == null) {
      content = new ArrayList<TokenWithOptionalLang>();
    }
    return this.content;
  }

  /**
   * Obtiene el valor de la propiedad extension.
   * 
   * @return possible object is {@link CustomExtensionsContainer }
   * 
   */
  public XmlExtensionContent getExtension() {
    return extension;
  }

  /**
   * Define el valor de la propiedad extension.
   * 
   * @param value allowed object is {@link CustomExtensionsContainer }
   * 
   */
  public void setExtension(final XmlExtensionContent value) {
    this.extension = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;token"&gt;
     *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"value"})
  public static class Identifier implements Serializable {

    private static final long serialVersionUID = 4082011653013883653L;
    @XmlValue
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;
    @XmlAttribute(name = "type", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String type;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getValue() {
      return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setValue(final String value) {
      this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getType() {
      return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setType(final String value) {
      this.type = value;
    }

  }

}
