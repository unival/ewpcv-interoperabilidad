//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * "Simple" (but still dangerous) HTML string. This type is very different from
 * PlaintextMultilineStringWithOptionalLang, because it contains HTML, not plain-text.
 * <p>
 * <p>
 * Notes for SERVER implementers =============================
 * <p>
 * There's the word "Simple" in the name of this type. This is because servers are RECOMMENDED to use only the basic
 * HTML tags for formatting (paragraphs, italics, emphasis, lists, etc.). You SHOULD avoid inserting more complex
 * content (such as tables, images or h1..h6 headers), because clients might not be able to display these.
 * <p>
 * Also note, that this type is usually used in contexts where the content is also provided in non-HTML form (such as
 * the `descriptionHtml` and `description` pair). Therefore, many clients will simply ignore the HTML values, and use
 * the plain-text counterparts instead.
 * <p>
 * <p>
 * Notes for CLIENT implementers =============================
 * <p>
 * DO NOT TRUST THAT THIS MARKUP IS SAFE. If you decide to display this HTML content, then you SHOULD sanitize it.
 * Otherwise you will leave your system vulnerable to various attacks, including XSS. If you're not sure how
 * sanitization works, then you SHOULD NOT display this content in your application. Use the plaintext counterparts
 * instead (if available).
 * <p>
 * Also note, that you SHOULD NOT apply `nl2br`-like funtions on this input (the ones you do apply in the
 * PlaintextMultilineStringWithOptionalLang format above). It's up to the server to make sure that its HTML is valid
 * (properly split into paragraphs). You may however attempt to detect and fix HTML errors, if this HTML seems broken.
 *
 *
 * <p>
 * Clase Java para SimpleHtmlStringWithOptionalLang complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="SimpleHtmlStringWithOptionalLang"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *       &lt;attribute ref="{http://www.w3.org/XML/1998/namespace}lang"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleHtmlStringWithOptionalLang", propOrder = {"value"})
public class SimpleHtmlStringWithOptionalLang implements Serializable {

  @XmlValue
  protected String value;
  @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "language")
  protected String lang;

  /**
   * Obtiene el valor de la propiedad value.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getValue() {
    return value;
  }

  /**
   * Define el valor de la propiedad value.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * Obtiene el valor de la propiedad lang.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLang() {
    return lang;
  }

  /**
   * Define el valor de la propiedad lang.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLang(String value) {
    this.lang = value;
  }

}
