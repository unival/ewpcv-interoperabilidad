//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * eu.emrex.elmo package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlRegistry
public class ObjectFactory {


  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
   * eu.emrex.elmo
   * 
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link Elmo }
   * 
   */
  public Elmo createElmo() {
    return new Elmo();
  }

  /**
   * Create an instance of {@link DiplomaSupplementSection }
   * 
   */
  public DiplomaSupplementSection createDiplomaSupplementSection() {
    return new DiplomaSupplementSection();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification }
   * 
   */
  public LearningOpportunitySpecification createLearningOpportunitySpecification() {
    return new LearningOpportunitySpecification();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies }
   * 
   */
  public LearningOpportunitySpecification.Specifies createLearningOpportunitySpecificationSpecifies() {
    return new LearningOpportunitySpecification.Specifies();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstance() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance();
  }

  /**
   * Create an instance of
   * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceResultDistribution() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution();
  }

  /**
   * Create an instance of {@link Groups }
   * 
   */
  public Groups createGroups() {
    return new Groups();
  }

  /**
   * Create an instance of {@link Groups.GroupType }
   * 
   */
  public Groups.GroupType createGroupsGroupType() {
    return new Groups.GroupType();
  }

  /**
   * Create an instance of {@link Attachment }
   * 
   */
  public Attachment createAttachment() {
    return new Attachment();
  }

  /**
   * Create an instance of {@link Elmo.Report }
   * 
   */
  public Elmo.Report createElmoReport() {
    return new Elmo.Report();
  }

  /**
   * Create an instance of {@link Elmo.Report.Issuer }
   * 
   */
  public Elmo.Report.Issuer createElmoReportIssuer() {
    return new Elmo.Report.Issuer();
  }

  /**
   * Create an instance of {@link Elmo.Learner }
   * 
   */
  public Elmo.Learner createElmoLearner() {
    return new Elmo.Learner();
  }

  /**
   * Create an instance of {@link CustomExtensionsContainer }
   * 
   */
  public CustomExtensionsContainer createCustomExtensionsContainer() {
    return new CustomExtensionsContainer();
  }

  /**
   * Create an instance of {@link TokenWithOptionalLang }
   * 
   */
  public TokenWithOptionalLang createTokenWithOptionalLang() {
    return new TokenWithOptionalLang();
  }

  /**
   * Create an instance of {@link PlaintextMultilineStringWithOptionalLang }
   * 
   */
  public PlaintextMultilineStringWithOptionalLang createPlaintextMultilineStringWithOptionalLang() {
    return new PlaintextMultilineStringWithOptionalLang();
  }

  /**
   * Create an instance of {@link SimpleHtmlStringWithOptionalLang }
   * 
   */
  public SimpleHtmlStringWithOptionalLang createSimpleHtmlStringWithOptionalLang() {
    return new SimpleHtmlStringWithOptionalLang();
  }

  /**
   * Create an instance of {@link DiplomaSupplement }
   * 
   */
  public DiplomaSupplement createDiplomaSupplement() {
    return new DiplomaSupplement();
  }

  /**
   * Create an instance of {@link DiplomaSupplementSection.Content }
   * 
   */
  public DiplomaSupplementSection.Content createDiplomaSupplementSectionContent() {
    return new DiplomaSupplementSection.Content();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Identifier }
   * 
   */
  public LearningOpportunitySpecification.Identifier createLearningOpportunitySpecificationIdentifier() {
    return new LearningOpportunitySpecification.Identifier();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.HasPart }
   * 
   */
  public LearningOpportunitySpecification.HasPart createLearningOpportunitySpecificationHasPart() {
    return new LearningOpportunitySpecification.HasPart();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceIdentifier() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Identifier();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceAcademicTerm() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.AcademicTerm();
  }

  /**
   * Create an instance of
   * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceShortenedGrading() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ShortenedGrading();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceCredit() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Credit();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceLevel() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Level();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceAttachments() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Attachments();
  }

  /**
   * Create an instance of {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceGrouping() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.Grouping();
  }

  /**
   * Create an instance of
   * {@link LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category }
   * 
   */
  public LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category createLearningOpportunitySpecificationSpecifiesLearningOpportunityInstanceResultDistributionCategory() {
    return new LearningOpportunitySpecification.Specifies.LearningOpportunityInstance.ResultDistribution.Category();
  }

  /**
   * Create an instance of {@link Groups.GroupType.Group }
   * 
   */
  public Groups.GroupType.Group createGroupsGroupTypeGroup() {
    return new Groups.GroupType.Group();
  }

  /**
   * Create an instance of {@link Attachment.Identifier }
   * 
   */
  public Attachment.Identifier createAttachmentIdentifier() {
    return new Attachment.Identifier();
  }

  /**
   * Create an instance of {@link Elmo.Report.GradingScheme }
   * 
   */
  public Elmo.Report.GradingScheme createElmoReportGradingScheme() {
    return new Elmo.Report.GradingScheme();
  }

  /**
   * Create an instance of {@link Elmo.Report.Issuer.Identifier }
   * 
   */
  public Elmo.Report.Issuer.Identifier createElmoReportIssuerIdentifier() {
    return new Elmo.Report.Issuer.Identifier();
  }

  /**
   * Create an instance of {@link Elmo.Learner.Identifier }
   * 
   */
  public Elmo.Learner.Identifier createElmoLearnerIdentifier() {
    return new Elmo.Learner.Identifier();
  }

}
