//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The type Xml extension content.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "extension-content")
public class XmlExtensionContent implements Serializable {

  private static final long serialVersionUID = 5981675719947308728L;
  // @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  protected String extensionContent;

  /**
   * Obtiene el valor de la propiedad extension.
   * 
   * @return content as a String
   * 
   */
  public String getExtensionContent() {
    return extensionContent;
  }

  /**
   * Define el valor de la propiedad extension.
   * 
   * @param value allowed object is String
   * 
   */
  public void setExtensionContent(final String value) {
    this.extensionContent = value;
  }

}
