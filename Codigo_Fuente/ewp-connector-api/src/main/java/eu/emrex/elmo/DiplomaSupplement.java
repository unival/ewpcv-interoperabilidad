//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import org.w3.xmldsig.SignatureType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * The Diploma Supplement (DS) is a document accompanying a higher education diploma, providing a standardised
 * description of the nature, level, context, content and status of the studies completed by its holder.
 * <p>
 * Details: http://ec.europa.eu/education/resources/diploma-supplement_en
 *
 *
 * <p>
 * Clase Java para DiplomaSupplement complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DiplomaSupplement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}token"/&gt;
 *         &lt;element name="issueDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="introduction" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="section" type="{https://github.com/emrex-eu/elmo-schemas/tree/v1}DiplomaSupplementSection" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute ref="{http://www.w3.org/XML/1998/namespace}lang"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiplomaSupplement", propOrder = {"version", "issueDate", "introduction", "section", "signature"})
public class DiplomaSupplement implements Serializable {

  private static final long serialVersionUID = 6312390340917198384L;
  @XmlElement(required = true, defaultValue = "2018")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "token")
  protected String version;
  @XmlElement(required = true)
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar issueDate;
  @XmlElement(required = true)
  protected Object introduction;
  @XmlElement(required = true)
  protected List<DiplomaSupplementSection> section;
  @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#")
  protected Object signature;
  @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "language")
  protected String lang;

  /**
   * Obtiene el valor de la propiedad version.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getVersion() {
    return version;
  }

  /**
   * Define el valor de la propiedad version.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setVersion(final String value) {
    this.version = value;
  }

  /**
   * Obtiene el valor de la propiedad issueDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getIssueDate() {
    return issueDate;
  }

  /**
   * Define el valor de la propiedad issueDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setIssueDate(final XMLGregorianCalendar value) {
    this.issueDate = value;
  }

  /**
   * Obtiene el valor de la propiedad introduction.
   * 
   * @return possible object is {@link Object }
   * 
   */
  public Object getIntroduction() {
    return introduction;
  }

  /**
   * Define el valor de la propiedad introduction.
   * 
   * @param value allowed object is {@link Object }
   * 
   */
  public void setIntroduction(final Object value) {
    this.introduction = value;
  }

  /**
   * Gets the value of the section property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the section property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getSection().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link DiplomaSupplementSection }
   * 
   * 
   */
  public List<DiplomaSupplementSection> getSection() {
    if (section == null) {
      section = new ArrayList<DiplomaSupplementSection>();
    }
    return this.section;
  }

  /**
   * The DS itself can be digitally signed, e.g. if the DS issuer is not the same entity as the one issuing the ELMO
   * document.
   * 
   * 
   * @return possible object is {@link SignatureType }
   * 
   */
  public Object getSignature() {
    return signature;
  }

  /**
   * Define el valor de la propiedad signature.
   * 
   * @param value allowed object is {@link SignatureType }
   * 
   */
  public void setSignature(final Object value) {
    this.signature = value;
  }

  /**
   * The language the DS has been issued in, of type xml:lang as defined in BCP 47.
   * 
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLang() {
    return lang;
  }

  /**
   * Define el valor de la propiedad lang.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLang(final String value) {
    this.lang = value;
  }

}
