//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.emrex.elmo;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * This is very similar to TokenWithOptionalLang, but it inherits from xs:string (instead of xs:token), and it MAY
 * contain basic whitespace formatting, such as line breaks and double line breaks (for spliting paragraphs). The values
 * still must be in plain text though (no HTML is allowed).
 * <p>
 * A note for client implementers:
 * <p>
 * Whenever you want to display the values read from a field of this type, you SHOULD make sure that the end user will
 * see the line breaks in the right places. E.g. use your equivalent of the well known `nl2br` function.
 *
 *
 * <p>
 * Clase Java para PlaintextMultilineStringWithOptionalLang complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="PlaintextMultilineStringWithOptionalLang"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *       &lt;attribute ref="{http://www.w3.org/XML/1998/namespace}lang"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlaintextMultilineStringWithOptionalLang", propOrder = {"value"})
public class PlaintextMultilineStringWithOptionalLang implements Serializable {

  @XmlValue
  protected String value;
  @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "language")
  protected String lang;

  /**
   * Obtiene el valor de la propiedad value.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getValue() {
    return value;
  }

  /**
   * Define el valor de la propiedad value.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * Obtiene el valor de la propiedad lang.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLang() {
    return lang;
  }

  /**
   * Define el valor de la propiedad lang.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLang(String value) {
    this.lang = value;
  }

}
