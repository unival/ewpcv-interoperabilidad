//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Clase Java para Signature complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="Signature"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="signer-name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="signer-position" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="signer-email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="signer-app" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Signature", propOrder = {"signerName", "signerPosition", "signerEmail", "timestamp", "signerApp"})
public class Signature implements Serializable {

  @XmlElement(name = "signer-name")
  protected String signerName;
  @XmlElement(name = "signer-position")
  protected String signerPosition;
  @XmlElement(name = "signer-email")
  protected String signerEmail;
  @XmlElement(required = true)
  @XmlSchemaType(name = "dateTime")
  protected XMLGregorianCalendar timestamp;
  @XmlElement(name = "signer-app")
  protected String signerApp;

  /**
   * Obtiene el valor de la propiedad signerName.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSignerName() {
    return signerName;
  }

  /**
   * Define el valor de la propiedad signerName.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSignerName(String value) {
    this.signerName = value;
  }

  /**
   * Obtiene el valor de la propiedad signerPosition.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSignerPosition() {
    return signerPosition;
  }

  /**
   * Define el valor de la propiedad signerPosition.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSignerPosition(String value) {
    this.signerPosition = value;
  }

  /**
   * Obtiene el valor de la propiedad signerEmail.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSignerEmail() {
    return signerEmail;
  }

  /**
   * Define el valor de la propiedad signerEmail.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSignerEmail(String value) {
    this.signerEmail = value;
  }

  /**
   * Obtiene el valor de la propiedad timestamp.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getTimestamp() {
    return timestamp;
  }

  /**
   * Define el valor de la propiedad timestamp.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setTimestamp(XMLGregorianCalendar value) {
    this.timestamp = value;
  }

  /**
   * Obtiene el valor de la propiedad signerApp.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSignerApp() {
    return signerApp;
  }

  /**
   * Define el valor de la propiedad signerApp.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSignerApp(String value) {
    this.signerApp = value;
  }

}
