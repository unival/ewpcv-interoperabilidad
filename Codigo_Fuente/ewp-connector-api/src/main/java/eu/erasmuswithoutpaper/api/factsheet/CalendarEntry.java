//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.factsheet;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Clase Java para CalendarEntry complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="CalendarEntry"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="autumn-term" type="{http://www.w3.org/2001/XMLSchema}gMonthDay"/&gt;
 *         &lt;element name="spring-term" type="{http://www.w3.org/2001/XMLSchema}gMonthDay"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalendarEntry", propOrder = {"autumnTerm", "springTerm"})
public class CalendarEntry implements Serializable {

  @XmlElement(name = "autumn-term", required = true)
  @XmlSchemaType(name = "gMonthDay")
  protected XMLGregorianCalendar autumnTerm;
  @XmlElement(name = "spring-term", required = true)
  @XmlSchemaType(name = "gMonthDay")
  protected XMLGregorianCalendar springTerm;

  /**
   * Obtiene el valor de la propiedad autumnTerm.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getAutumnTerm() {
    return autumnTerm;
  }

  /**
   * Define el valor de la propiedad autumnTerm.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setAutumnTerm(XMLGregorianCalendar value) {
    this.autumnTerm = value;
  }

  /**
   * Obtiene el valor de la propiedad springTerm.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getSpringTerm() {
    return springTerm;
  }

  /**
   * Define el valor de la propiedad springTerm.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setSpringTerm(XMLGregorianCalendar value) {
    this.springTerm = value;
  }

}
