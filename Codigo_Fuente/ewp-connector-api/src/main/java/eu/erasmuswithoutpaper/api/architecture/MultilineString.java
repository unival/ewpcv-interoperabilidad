//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.architecture;

import java.io.Serializable;

import javax.xml.bind.annotation.*;


/**
 * This is very similar to a regular xs:string, but whenever this type is used it indicates that the content MAY contain
 * basic whitespace formatting, such us line breaks and double line breaks (for splitting paragraphs). The values still
 * MUST be in plaintext though (no HTML is allowed).
 * <p>
 * Clients which process data of this type SHOULD respect line breaks when they display the data to the end user (e.g.
 * replace CRs and LFs with <br>
 * s when rendering to HTML).
 *
 *
 * <p>
 * Clase Java para MultilineString complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="MultilineString"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultilineString", propOrder = {"value"})
@XmlSeeAlso({MultilineStringWithOptionalLang.class})
public class MultilineString implements Serializable {

  @XmlValue
  protected String value;

  /**
   * Obtiene el valor de la propiedad value.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getValue() {
    return value;
  }

  /**
   * Define el valor de la propiedad value.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setValue(String value) {
    this.value = value;
  }

}
