//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.endpoints;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Clase Java para MobilityStatus.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 *
 * <pre>
 * &lt;simpleType name="MobilityStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="nomination"/&gt;
 *     &lt;enumeration value="live"/&gt;
 *     &lt;enumeration value="recognized"/&gt;
 *     &lt;enumeration value="cancelled"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlType(name = "MobilityStatus")
@XmlEnum
public enum MobilityStatus {


  /**
   * 
   * The sending HEI has nominated the student for mobility. The proposal has not yet been accepted nor rejected by the
   * receiving HEI.
   * 
   * This is the default status with which mobility entities are first created.
   * 
   * 
   */
  @XmlEnumValue("nomination")
  NOMINATION("nomination"),

  /**
   * 
   * The nomination has been accepted by the receiving HEI, and all initial formalities have been settled (i.e. a first
   * LA version has been signed). This status doesn't usually change throughout the mobility.
   * 
   * While in this status, LA can still be modified (new revisions of it may be created, and signed).
   * 
   * 
   */
  @XmlEnumValue("live")
  LIVE("live"),

  /**
   * 
   * The student has returned from the mobility and all achievements have been recognized as indicated on the
   * `component-recognized` list.
   * 
   * At this point, the mobility SHOULD become read-only. The latest revision of it SHOULD be approved by all parties
   * (no subsequent draft revisions should exist).
   * 
   * 
   */
  @XmlEnumValue("recognized")
  RECOGNIZED("recognized"),

  /**
   * 
   * The nomination has been cancelled (either by the student, or by one of the partner HEIs).
   * 
   * 
   */
  @XmlEnumValue("cancelled")
  CANCELLED("cancelled");
  private final String value;

  MobilityStatus(String v) {
    value = v;
  }

  public static MobilityStatus fromValue(String v) {
    for (MobilityStatus c : MobilityStatus.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }

}
