package eu.erasmuswithoutpaper.api.omobilities.cnr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The type Omobility cnr response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "omobility-cnr-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-cnr/tree/stable-v1")
public class OmobilityCnrResponse implements Serializable {

  private static final long serialVersionUID = 2861574143369759024L;
}

