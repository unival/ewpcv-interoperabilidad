//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.registry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="other-id" type="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}OtherHeiId" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"otherId", "name"})
@XmlRootElement(name = "hei")
public class Hei implements Serializable {

  @XmlElement(name = "other-id")
  protected List<OtherHeiId> otherId;
  @XmlElement(required = true)
  protected List<StringWithOptionalLang> name;
  @XmlAttribute(name = "id", required = true)
  protected String id;

  /**
   * Gets the value of the otherId property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the otherId property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getOtherId().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link OtherHeiId }
   * 
   * 
   */
  public List<OtherHeiId> getOtherId() {
    if (otherId == null) {
      otherId = new ArrayList<OtherHeiId>();
    }
    return this.otherId;
  }

  /**
   * Gets the value of the name property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the name property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getName().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
   * 
   * 
   */
  public List<StringWithOptionalLang> getName() {
    if (name == null) {
      name = new ArrayList<StringWithOptionalLang>();
    }
    return this.name;
  }

  /**
   * Obtiene el valor de la propiedad id.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getId() {
    return id;
  }

  /**
   * Define el valor de la propiedad id.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setId(String value) {
    this.id = value;
  }

}
