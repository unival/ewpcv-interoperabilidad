//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.discovery;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The type Adapter 1.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class Adapter1 extends XmlAdapter<java.lang.String, String> {


  public String unmarshal(java.lang.String value) {
    return new String(value);
  }

  public java.lang.String marshal(String value) {
    if (value == null) {
      return null;
    }
    return value.toString();
  }

}
