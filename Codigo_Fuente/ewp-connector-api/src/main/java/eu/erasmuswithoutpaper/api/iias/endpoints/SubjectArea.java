//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.iias.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Clase Java para SubjectArea complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="SubjectArea"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isced-f-code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isced-clarification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubjectArea", propOrder = {"iscedFCode", "iscedClarification"})
public class SubjectArea implements Serializable {

  @XmlElement(name = "isced-f-code", required = true)
  protected String iscedFCode;
  @XmlElement(name = "isced-clarification")
  protected String iscedClarification;

  /**
   * Obtiene el valor de la propiedad iscedFCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIscedFCode() {
    return iscedFCode;
  }

  /**
   * Define el valor de la propiedad iscedFCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIscedFCode(String value) {
    this.iscedFCode = value;
  }

  /**
   * Obtiene el valor de la propiedad iscedClarification.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIscedClarification() {
    return iscedClarification;
  }

  /**
   * Define el valor de la propiedad iscedClarification.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIscedClarification(String value) {
    this.iscedClarification = value;
  }

}
