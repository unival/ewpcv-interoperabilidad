//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.mt.institutions;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * eu.erasmuswithoutpaper.api.mt.institutions package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlRegistry
public class ObjectFactory {


  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
   * eu.erasmuswithoutpaper.api.mt.institutions
   * 
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link MtInstitutionsResponse }
   * 
   */
  public MtInstitutionsResponse createMtInstitutionsResponse() {
    return new MtInstitutionsResponse();
  }

  /**
   * Create an instance of {@link MtInstitutionsResponse.Hei }
   * 
   */
  public MtInstitutionsResponse.Hei createMtInstitutionsResponseHei() {
    return new MtInstitutionsResponse.Hei();
  }

  /**
   * Create an instance of {@link MtInstitutions }
   * 
   */
  public MtInstitutions createMtInstitutions() {
    return new MtInstitutions();
  }

  /**
   * Create an instance of {@link MtInstitutionsResponse.Hei.ErasmusCharter }
   * 
   */
  public MtInstitutionsResponse.Hei.ErasmusCharter createMtInstitutionsResponseHeiErasmusCharter() {
    return new MtInstitutionsResponse.Hei.ErasmusCharter();
  }

}
