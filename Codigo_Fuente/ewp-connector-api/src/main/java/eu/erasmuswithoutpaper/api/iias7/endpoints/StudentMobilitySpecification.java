//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 A common parent class for all student mobility specifications.
 *             
 * 
 * <p>Clase Java para StudentMobilitySpecification complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="StudentMobilitySpecification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}MobilitySpecification"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}StudentMobilityExtension"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StudentMobilitySpecification", propOrder = {
    "totalMonthsPerYear",
    "blended",
    "eqfLevel"
})
@XmlSeeAlso({
    StudentTraineeshipMobilitySpec.class,
    StudentStudiesMobilitySpec.class
})
public abstract class StudentMobilitySpecification
    extends MobilitySpecification
{

    @XmlElement(name = "total-months-per-year")
    protected BigDecimal totalMonthsPerYear;
    protected boolean blended;
    @XmlElement(name = "eqf-level", type = Byte.class)
    protected List<Byte> eqfLevel;

    /**
     * Obtiene el valor de la propiedad totalMonthsPerYear.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalMonthsPerYear() {
        return totalMonthsPerYear;
    }

    /**
     * Define el valor de la propiedad totalMonthsPerYear.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalMonthsPerYear(BigDecimal value) {
        this.totalMonthsPerYear = value;
    }

    /**
     * Obtiene el valor de la propiedad blended.
     * 
     */
    public boolean isBlended() {
        return blended;
    }

    /**
     * Define el valor de la propiedad blended.
     * 
     */
    public void setBlended(boolean value) {
        this.blended = value;
    }

    /**
     * Gets the value of the eqfLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eqfLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEqfLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Byte }
     * 
     * 
     */
    public List<Byte> getEqfLevel() {
        if (eqfLevel == null) {
            eqfLevel = new ArrayList<Byte>();
        }
        return this.eqfLevel;
    }

}
