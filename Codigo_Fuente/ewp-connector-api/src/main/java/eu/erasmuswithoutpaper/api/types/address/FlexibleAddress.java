//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.types.address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * This type defines an address. It can be used for postal/mailing address, or for street/physical address, or both,
 * depending on context.
 *
 *
 * <p>
 * Clase Java para FlexibleAddress complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="FlexibleAddress"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="addressLine" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="buildingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="buildingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="streetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="floor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="postOfficeBox" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="deliveryPointCode" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="country" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CountryCode" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexibleAddress", propOrder = {"recipientName", "addressLine", "buildingNumber", "buildingName",
    "streetName", "unit", "floor", "postOfficeBox", "deliveryPointCode", "postalCode", "locality", "region", "country"})
public class FlexibleAddress implements Serializable {

  protected List<String> recipientName;
  protected List<String> addressLine;
  protected String buildingNumber;
  protected String buildingName;
  protected String streetName;
  protected String unit;
  protected String floor;
  protected String postOfficeBox;
  protected List<Object> deliveryPointCode;
  protected String postalCode;
  protected String locality;
  protected String region;
  protected String country;

  /**
   * Gets the value of the recipientName property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the recipientName property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getRecipientName().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link String }
   * 
   * 
   */
  public List<String> getRecipientName() {
    if (recipientName == null) {
      recipientName = new ArrayList<String>();
    }
    return this.recipientName;
  }

  /**
   * Gets the value of the addressLine property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the addressLine property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getAddressLine().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link String }
   * 
   * 
   */
  public List<String> getAddressLine() {
    if (addressLine == null) {
      addressLine = new ArrayList<String>();
    }
    return this.addressLine;
  }

  /**
   * Obtiene el valor de la propiedad buildingNumber.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getBuildingNumber() {
    return buildingNumber;
  }

  /**
   * Define el valor de la propiedad buildingNumber.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setBuildingNumber(String value) {
    this.buildingNumber = value;
  }

  /**
   * Obtiene el valor de la propiedad buildingName.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getBuildingName() {
    return buildingName;
  }

  /**
   * Define el valor de la propiedad buildingName.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setBuildingName(String value) {
    this.buildingName = value;
  }

  /**
   * Obtiene el valor de la propiedad streetName.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getStreetName() {
    return streetName;
  }

  /**
   * Define el valor de la propiedad streetName.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setStreetName(String value) {
    this.streetName = value;
  }

  /**
   * Obtiene el valor de la propiedad unit.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getUnit() {
    return unit;
  }

  /**
   * Define el valor de la propiedad unit.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setUnit(String value) {
    this.unit = value;
  }

  /**
   * Obtiene el valor de la propiedad floor.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getFloor() {
    return floor;
  }

  /**
   * Define el valor de la propiedad floor.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setFloor(String value) {
    this.floor = value;
  }

  /**
   * Obtiene el valor de la propiedad postOfficeBox.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getPostOfficeBox() {
    return postOfficeBox;
  }

  /**
   * Define el valor de la propiedad postOfficeBox.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setPostOfficeBox(String value) {
    this.postOfficeBox = value;
  }

  /**
   * Gets the value of the deliveryPointCode property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the deliveryPointCode property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getDeliveryPointCode().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Object }
   * 
   * 
   */
  public List<Object> getDeliveryPointCode() {
    if (deliveryPointCode == null) {
      deliveryPointCode = new ArrayList<Object>();
    }
    return this.deliveryPointCode;
  }

  /**
   * Obtiene el valor de la propiedad postalCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * Define el valor de la propiedad postalCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setPostalCode(String value) {
    this.postalCode = value;
  }

  /**
   * Obtiene el valor de la propiedad locality.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLocality() {
    return locality;
  }

  /**
   * Define el valor de la propiedad locality.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLocality(String value) {
    this.locality = value;
  }

  /**
   * Obtiene el valor de la propiedad region.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getRegion() {
    return region;
  }

  /**
   * Define el valor de la propiedad region.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setRegion(String value) {
    this.region = value;
  }

  /**
   * Obtiene el valor de la propiedad country.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCountry() {
    return country;
  }

  /**
   * Define el valor de la propiedad country.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setCountry(String value) {
    this.country = value;
  }

}
