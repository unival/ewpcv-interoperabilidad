//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.architecture;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A multiline string (as defined in the MultilineString) with an optional (but RECOMMENDED) xml:lang attribute. It is
 * used in places where a description of some entity can be provided in multiple languages.
 *
 *
 * <p>
 * Clase Java para MultilineStringWithOptionalLang complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="MultilineStringWithOptionalLang"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd&gt;MultilineString"&gt;
 *       &lt;attribute ref="{http://www.w3.org/XML/1998/namespace}lang"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultilineStringWithOptionalLang")
public class MultilineStringWithOptionalLang extends MultilineString implements Serializable {

  @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "language")
  protected String lang;

  /**
   * 
   * Also see comments on xml:lang attribute in StringWithOptionalLang type.
   * 
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLang() {
    return lang;
  }

  /**
   * Define el valor de la propiedad lang.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLang(String value) {
    this.lang = value;
  }

}
