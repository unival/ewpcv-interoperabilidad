//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import eu.erasmuswithoutpaper.api.types.phonenumber.PhoneNumber;


/**
 * <p>
 * Clase Java para MobilityInstitution complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="MobilityInstitution"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ounit-name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="contact-person"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="given-names" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="family-name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1}phone-number" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobilityInstitution", propOrder = {"heiId", "ounitName", "ounitId", "contactPerson"})
public class MobilityInstitution implements Serializable {

  @XmlElement(name = "hei-id", required = true)
  protected String heiId;
  @XmlElement(name = "ounit-name")
  protected String ounitName;
  @XmlElement(name = "ounit-id")
  protected String ounitId;
  @XmlElement(name = "contact-person", required = true)
  protected MobilityInstitution.ContactPerson contactPerson;

  /**
   * Obtiene el valor de la propiedad heiId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getHeiId() {
    return heiId;
  }

  /**
   * Define el valor de la propiedad heiId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setHeiId(String value) {
    this.heiId = value;
  }

  /**
   * Obtiene el valor de la propiedad ounitName.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOunitName() {
    return ounitName;
  }

  /**
   * Define el valor de la propiedad ounitName.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOunitName(String value) {
    this.ounitName = value;
  }

  /**
   * Obtiene el valor de la propiedad ounitId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOunitId() {
    return ounitId;
  }

  /**
   * Define el valor de la propiedad ounitId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOunitId(String value) {
    this.ounitId = value;
  }

  /**
   * Obtiene el valor de la propiedad contactPerson.
   * 
   * @return possible object is {@link MobilityInstitution.ContactPerson }
   * 
   */
  public MobilityInstitution.ContactPerson getContactPerson() {
    return contactPerson;
  }

  /**
   * Define el valor de la propiedad contactPerson.
   * 
   * @param value allowed object is {@link MobilityInstitution.ContactPerson }
   * 
   */
  public void setContactPerson(MobilityInstitution.ContactPerson value) {
    this.contactPerson = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="given-names" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="family-name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1}phone-number" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"givenNames", "familyName", "email", "phoneNumber"})
  public static class ContactPerson implements Serializable {

    @XmlElement(name = "given-names", required = true)
    protected String givenNames;
    @XmlElement(name = "family-name", required = true)
    protected String familyName;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(name = "phone-number",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1")
    protected PhoneNumber phoneNumber;

    /**
     * Obtiene el valor de la propiedad givenNames.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getGivenNames() {
      return givenNames;
    }

    /**
     * Define el valor de la propiedad givenNames.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setGivenNames(String value) {
      this.givenNames = value;
    }

    /**
     * Obtiene el valor de la propiedad familyName.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getFamilyName() {
      return familyName;
    }

    /**
     * Define el valor de la propiedad familyName.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setFamilyName(String value) {
      this.familyName = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEmail() {
      return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEmail(String value) {
      this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad phoneNumber.
     * 
     * @return possible object is {@link PhoneNumber }
     * 
     */
    public PhoneNumber getPhoneNumber() {
      return phoneNumber;
    }

    /**
     * Define el valor de la propiedad phoneNumber.
     * 
     * @param value allowed object is {@link PhoneNumber }
     * 
     */
    public void setPhoneNumber(PhoneNumber value) {
      this.phoneNumber = value;
    }

  }

}
