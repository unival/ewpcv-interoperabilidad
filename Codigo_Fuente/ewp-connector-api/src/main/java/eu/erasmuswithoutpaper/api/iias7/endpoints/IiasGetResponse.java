//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import eu.erasmuswithoutpaper.api.types.contact.Contact;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="iia" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="partner" maxOccurs="2" minOccurs="2"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                             &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                             &lt;element name="iia-code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="signing-contact" type="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}Contact" minOccurs="0"/&gt;
 *                             &lt;element name="signing-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="in-effect" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="cooperation-conditions"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-studies-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                             &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-traineeship-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                             &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-teacher-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                             &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-training-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="terminated-as-a-whole" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="iia-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
 *                   &lt;element name="pdf-file" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "iia"
})
@XmlRootElement(name = "iias-get-response")
public class IiasGetResponse {

    protected List<Iia> iia;

    /**
     * Gets the value of the iia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the iia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Iia }
     * 
     * 
     */
    public List<Iia> getIia() {
        if (iia == null) {
            iia = new ArrayList<Iia>();
        }
        return this.iia;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="partner" maxOccurs="2" minOccurs="2"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *                   &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *                   &lt;element name="iia-code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="signing-contact" type="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}Contact" minOccurs="0"/&gt;
     *                   &lt;element name="signing-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="in-effect" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
     *         &lt;element name="cooperation-conditions"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-studies-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-traineeship-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-teacher-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-training-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="terminated-as-a-whole" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="iia-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
     *         &lt;element name="pdf-file" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "partner",
        "inEffect",
        "cooperationConditions",
        "iiaHash",
        "pdfFile"
    })
    public static class Iia {

        @XmlElement(required = true)
        protected List<Partner> partner;
        @XmlElement(name = "in-effect")
        protected boolean inEffect;
        @XmlElement(name = "cooperation-conditions", required = true)
        protected CooperationConditions cooperationConditions;
        @XmlElement(name = "iia-hash", required = true)
        protected String iiaHash;
        @XmlElement(name = "pdf-file")
        protected String pdfFile;

        /**
         * Gets the value of the partner property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the partner property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPartner().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Partner }
         * 
         * 
         */
        public List<Partner> getPartner() {
            if (partner == null) {
                partner = new ArrayList<Partner>();
            }
            return this.partner;
        }

        /**
         * Obtiene el valor de la propiedad inEffect.
         * 
         */
        public boolean isInEffect() {
            return inEffect;
        }

        /**
         * Define el valor de la propiedad inEffect.
         * 
         */
        public void setInEffect(boolean value) {
            this.inEffect = value;
        }

        /**
         * Obtiene el valor de la propiedad cooperationConditions.
         * 
         * @return
         *     possible object is
         *     {@link CooperationConditions }
         *     
         */
        public CooperationConditions getCooperationConditions() {
            return cooperationConditions;
        }

        /**
         * Define el valor de la propiedad cooperationConditions.
         * 
         * @param value
         *     allowed object is
         *     {@link CooperationConditions }
         *     
         */
        public void setCooperationConditions(CooperationConditions value) {
            this.cooperationConditions = value;
        }

        /**
         * Obtiene el valor de la propiedad iiaHash.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIiaHash() {
            return iiaHash;
        }

        /**
         * Define el valor de la propiedad iiaHash.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIiaHash(String value) {
            this.iiaHash = value;
        }

        /**
         * Obtiene el valor de la propiedad pdfFile.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPdfFile() {
            return pdfFile;
        }

        /**
         * Define el valor de la propiedad pdfFile.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPdfFile(String value) {
            this.pdfFile = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-studies-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
         *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}student-traineeship-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
         *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-teacher-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
         *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}staff-training-mobility-spec" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="terminated-as-a-whole" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "studentStudiesMobilitySpec",
            "studentTraineeshipMobilitySpec",
            "staffTeacherMobilitySpec",
            "staffTrainingMobilitySpec"
        })
        public static class CooperationConditions {

            @XmlElement(name = "student-studies-mobility-spec")
            protected List<StudentStudiesMobilitySpec> studentStudiesMobilitySpec;
            @XmlElement(name = "student-traineeship-mobility-spec")
            protected List<StudentTraineeshipMobilitySpec> studentTraineeshipMobilitySpec;
            @XmlElement(name = "staff-teacher-mobility-spec")
            protected List<StaffTeacherMobilitySpec> staffTeacherMobilitySpec;
            @XmlElement(name = "staff-training-mobility-spec")
            protected List<StaffTrainingMobilitySpec> staffTrainingMobilitySpec;
            @XmlAttribute(name = "terminated-as-a-whole")
            protected Boolean terminatedAsAWhole;

            /**
             * 
             *                                                     A list of descriptions of student mobilities *for studies* (not to be
             *                                                     confused with student mobility *for traineeships*).
             *                                                 Gets the value of the studentStudiesMobilitySpec property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the studentStudiesMobilitySpec property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getStudentStudiesMobilitySpec().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link StudentStudiesMobilitySpec }
             * 
             * 
             */
            public List<StudentStudiesMobilitySpec> getStudentStudiesMobilitySpec() {
                if (studentStudiesMobilitySpec == null) {
                    studentStudiesMobilitySpec = new ArrayList<StudentStudiesMobilitySpec>();
                }
                return this.studentStudiesMobilitySpec;
            }

            /**
             * 
             *                                                     A list of descriptions of student mobility *for traineeships*.
             *                                                 Gets the value of the studentTraineeshipMobilitySpec property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the studentTraineeshipMobilitySpec property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getStudentTraineeshipMobilitySpec().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link StudentTraineeshipMobilitySpec }
             * 
             * 
             */
            public List<StudentTraineeshipMobilitySpec> getStudentTraineeshipMobilitySpec() {
                if (studentTraineeshipMobilitySpec == null) {
                    studentTraineeshipMobilitySpec = new ArrayList<StudentTraineeshipMobilitySpec>();
                }
                return this.studentTraineeshipMobilitySpec;
            }

            /**
             * 
             *                                                     A list of descriptions of staff mobility *for teaching*.
             *                                                 Gets the value of the staffTeacherMobilitySpec property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the staffTeacherMobilitySpec property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getStaffTeacherMobilitySpec().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link StaffTeacherMobilitySpec }
             * 
             * 
             */
            public List<StaffTeacherMobilitySpec> getStaffTeacherMobilitySpec() {
                if (staffTeacherMobilitySpec == null) {
                    staffTeacherMobilitySpec = new ArrayList<StaffTeacherMobilitySpec>();
                }
                return this.staffTeacherMobilitySpec;
            }

            /**
             * 
             *                                                     A list of descriptions of staff mobility *for training*.
             *                                                 Gets the value of the staffTrainingMobilitySpec property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the staffTrainingMobilitySpec property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getStaffTrainingMobilitySpec().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link StaffTrainingMobilitySpec }
             * 
             * 
             */
            public List<StaffTrainingMobilitySpec> getStaffTrainingMobilitySpec() {
                if (staffTrainingMobilitySpec == null) {
                    staffTrainingMobilitySpec = new ArrayList<StaffTrainingMobilitySpec>();
                }
                return this.staffTrainingMobilitySpec;
            }

            /**
             * Obtiene el valor de la propiedad terminatedAsAWhole.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isTerminatedAsAWhole() {
                return terminatedAsAWhole;
            }

            /**
             * Define el valor de la propiedad terminatedAsAWhole.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setTerminatedAsAWhole(Boolean value) {
                this.terminatedAsAWhole = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
         *         &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
         *         &lt;element name="iia-code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="signing-contact" type="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}Contact" minOccurs="0"/&gt;
         *         &lt;element name="signing-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "heiId",
            "ounitId",
            "iiaId",
            "iiaCode",
            "signingContact",
            "signingDate",
            "contact"
        })
        public static class Partner {

            @XmlElement(name = "hei-id", required = true)
            protected String heiId;
            @XmlElement(name = "ounit-id")
            protected String ounitId;
            @XmlElement(name = "iia-id")
            protected String iiaId;
            @XmlElement(name = "iia-code")
            protected String iiaCode;
            @XmlElement(name = "signing-contact")
            protected Contact signingContact;
            @XmlElement(name = "signing-date")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar signingDate;
            @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1")
            protected List<Contact> contact;

            /**
             * Obtiene el valor de la propiedad heiId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHeiId() {
                return heiId;
            }

            /**
             * Define el valor de la propiedad heiId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHeiId(String value) {
                this.heiId = value;
            }

            /**
             * Obtiene el valor de la propiedad ounitId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOunitId() {
                return ounitId;
            }

            /**
             * Define el valor de la propiedad ounitId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOunitId(String value) {
                this.ounitId = value;
            }

            /**
             * Obtiene el valor de la propiedad iiaId.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIiaId() {
                return iiaId;
            }

            /**
             * Define el valor de la propiedad iiaId.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIiaId(String value) {
                this.iiaId = value;
            }

            /**
             * Obtiene el valor de la propiedad iiaCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIiaCode() {
                return iiaCode;
            }

            /**
             * Define el valor de la propiedad iiaCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIiaCode(String value) {
                this.iiaCode = value;
            }

            /**
             * Obtiene el valor de la propiedad signingContact.
             * 
             * @return
             *     possible object is
             *     {@link Contact }
             *     
             */
            public Contact getSigningContact() {
                return signingContact;
            }

            /**
             * Define el valor de la propiedad signingContact.
             * 
             * @param value
             *     allowed object is
             *     {@link Contact }
             *     
             */
            public void setSigningContact(Contact value) {
                this.signingContact = value;
            }

            /**
             * Obtiene el valor de la propiedad signingDate.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getSigningDate() {
                return signingDate;
            }

            /**
             * Define el valor de la propiedad signingDate.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setSigningDate(XMLGregorianCalendar value) {
                this.signingDate = value;
            }

            /**
             * 
             *                                                     A list of other partner contacts related to this IIA (or to mobilities related
             *                                                     to this IIA).
             * 
             *                                                     Only some servers provide these, even for the *local* partner. Many HEIs
             *                                                     (especially the smaller ones) don't granulate their contacts in such a detailed
             *                                                     way. Instead, they have a fixed set of contacts described in their Institutions
             *                                                     API.
             * 
             *                                                     These contacts take precedence over contacts defined in the Institutions API
             *                                                     and Organization Units API for the partner HEI/unit. Clients are advised to
             *                                                     display these contacts in a separate section above other contacts - so that the
             *                                                     users will notice them first (before scrolling down to other, more generic
             *                                                     contacts).
             *                                                 Gets the value of the contact property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contact property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContact().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Contact }
             * 
             * 
             */
            public List<Contact> getContact() {
                if (contact == null) {
                    contact = new ArrayList<Contact>();
                }
                return this.contact;
            }

        }

    }

}
