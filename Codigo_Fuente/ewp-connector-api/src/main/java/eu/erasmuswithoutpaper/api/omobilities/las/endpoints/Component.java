//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.types.academicterm.TermId;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="los-id" type="{https://github.com/erasmus-without-paper/ewp-specs-api-courses/tree/stable-v1}LosID" minOccurs="0"/&gt;
 *         &lt;element name="los-code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="loi-id" type="{https://github.com/erasmus-without-paper/ewp-specs-api-courses/tree/stable-v1}LoiID" minOccurs="0"/&gt;
 *         &lt;element name="term-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}TermId" minOccurs="0"/&gt;
 *         &lt;element name="credit" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="recognition-conditions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="short-description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="status"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="inserted"/&gt;
 *             &lt;enumeration value="deleted"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="reason-code"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;enumeration value="not-available"/&gt;
 *             &lt;enumeration value="language-mismatch"/&gt;
 *             &lt;enumeration value="timetable-conflict"/&gt;
 *             &lt;enumeration value="substituting-deleted"/&gt;
 *             &lt;enumeration value="extending-mobility"/&gt;
 *             &lt;enumeration value="adding-virtual-component"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="reason-text" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
    propOrder = {"losId", "losCode", "title", "loiId", "termId", "credit", "recognitionConditions", "shortDescription"})
@XmlRootElement(name = "component")
public class Component implements Serializable {

  @XmlElement(name = "los-id")
  protected String losId;
  @XmlElement(name = "los-code")
  protected String losCode;
  @XmlElement(required = true)
  protected String title;
  @XmlElement(name = "loi-id")
  protected String loiId;
  @XmlElement(name = "term-id")
  protected TermId termId;
  protected List<Component.Credit> credit;
  @XmlElement(name = "recognition-conditions")
  protected String recognitionConditions;
  @XmlElement(name = "short-description")
  protected String shortDescription;
  @XmlAttribute(name = "status")
  protected String status;
  @XmlAttribute(name = "reason-code")
  protected String reasonCode;
  @XmlAttribute(name = "reason-text")
  protected String reasonText;

  /**
   * Obtiene el valor de la propiedad losId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLosId() {
    return losId;
  }

  /**
   * Define el valor de la propiedad losId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLosId(String value) {
    this.losId = value;
  }

  /**
   * Obtiene el valor de la propiedad losCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLosCode() {
    return losCode;
  }

  /**
   * Define el valor de la propiedad losCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLosCode(String value) {
    this.losCode = value;
  }

  /**
   * Obtiene el valor de la propiedad title.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getTitle() {
    return title;
  }

  /**
   * Define el valor de la propiedad title.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setTitle(String value) {
    this.title = value;
  }

  /**
   * Obtiene el valor de la propiedad loiId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLoiId() {
    return loiId;
  }

  /**
   * Define el valor de la propiedad loiId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLoiId(String value) {
    this.loiId = value;
  }

  /**
   * Obtiene el valor de la propiedad termId.
   * 
   * @return possible object is {@link TermId }
   * 
   */
  public TermId getTermId() {
    return termId;
  }

  /**
   * Define el valor de la propiedad termId.
   * 
   * @param value allowed object is {@link TermId }
   * 
   */
  public void setTermId(TermId value) {
    this.termId = value;
  }

  /**
   * Gets the value of the credit property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the credit property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getCredit().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Component.Credit }
   * 
   * 
   */
  public List<Component.Credit> getCredit() {
    if (credit == null) {
      credit = new ArrayList<Component.Credit>();
    }
    return this.credit;
  }

  /**
   * Obtiene el valor de la propiedad recognitionConditions.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getRecognitionConditions() {
    return recognitionConditions;
  }

  /**
   * Define el valor de la propiedad recognitionConditions.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setRecognitionConditions(String value) {
    this.recognitionConditions = value;
  }

  /**
   * Obtiene el valor de la propiedad shortDescription.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getShortDescription() {
    return shortDescription;
  }

  /**
   * Define el valor de la propiedad shortDescription.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setShortDescription(String value) {
    this.shortDescription = value;
  }

  /**
   * Obtiene el valor de la propiedad status.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getStatus() {
    return status;
  }

  /**
   * Define el valor de la propiedad status.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setStatus(String value) {
    this.status = value;
  }

  /**
   * Obtiene el valor de la propiedad reasonCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getReasonCode() {
    return reasonCode;
  }

  /**
   * Define el valor de la propiedad reasonCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setReasonCode(String value) {
    this.reasonCode = value;
  }

  /**
   * Obtiene el valor de la propiedad reasonText.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getReasonText() {
    return reasonText;
  }

  /**
   * Define el valor de la propiedad reasonText.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setReasonText(String value) {
    this.reasonText = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="scheme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"scheme", "value"})
  public static class Credit implements Serializable {

    @XmlElement(required = true)
    protected String scheme;
    @XmlElement(required = true)
    protected BigDecimal value;

    /**
     * Obtiene el valor de la propiedad scheme.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getScheme() {
      return scheme;
    }

    /**
     * Define el valor de la propiedad scheme.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setScheme(String value) {
      this.scheme = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getValue() {
      return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value allowed object is {@link BigDecimal }
     * 
     */
    public void setValue(BigDecimal value) {
      this.value = value;
    }

  }

}
