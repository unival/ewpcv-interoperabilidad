package eu.erasmuswithoutpaper.api.iias.cnr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The type Iia cnr response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "iia-cnr-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iia-cnr/tree/stable-v2")
public class IiaCnrResponse implements Serializable {

  private static final long serialVersionUID = 2861574143369759024L;
}

