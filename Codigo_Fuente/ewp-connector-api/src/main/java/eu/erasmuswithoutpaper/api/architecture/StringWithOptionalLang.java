//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.architecture;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A string with an optional (but RECOMMENDED) xml:lang attribute. It is used in places where a name of some entity can
 * be provided in multiple languages.
 *
 *
 * <p>
 * Clase Java para StringWithOptionalLang complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="StringWithOptionalLang"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *       &lt;attribute ref="{http://www.w3.org/XML/1998/namespace}lang"/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StringWithOptionalLang", propOrder = {"value"})
public class StringWithOptionalLang implements Serializable {

  @XmlValue
  protected String value;
  @XmlAttribute(name = "lang", namespace = "http://www.w3.org/XML/1998/namespace")
  @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
  @XmlSchemaType(name = "language")
  protected String lang;

  /**
   * Obtiene el valor de la propiedad value.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getValue() {
    return value;
  }

  /**
   * Define el valor de la propiedad value.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * 
   * A note for client developers: Keep in mind, that `xml:lang` is of the `xs:language` type. This means that it MAY
   * contain values such as `en`, but also `en-US`, `ar-Cyrl-CO`, or `tlh-Kore-AQ-fonipa`. Most often, you will need to
   * parse only the first component, but in some cases other components might also be pretty useful (like the script
   * subtag - "Latn", "Cyrl", etc.).
   * 
   * The `xml:language` datatype is defined here: https://www.ietf.org/rfc/bcp/bcp47.txt
   * 
   * You can also discuss this tag in context of EWP here:
   * https://github.com/erasmus-without-paper/ewp-specs-architecture/issues/11
   * 
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLang() {
    return lang;
  }

  /**
   * Define el valor de la propiedad lang.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLang(String value) {
    this.lang = value;
  }

}
