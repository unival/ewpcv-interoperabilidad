//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.imobilities.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * This describes the "incoming part" of a single Student Mobility for Studies. "Incoming part" is the set of mobility's
 * properties which the *receiving* HEI is the master of.
 * <p>
 * In the future, it may become a "subclass" of a more generic Mobility parent class (and some of the fields might be
 * moved to the parent).
 *
 *
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *         &lt;element name="status" type="{https://github.com/erasmus-without-paper/ewp-specs-api-imobilities/blob/stable-v1/endpoints/get-response.xsd}NominationStatus"/&gt;
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="actual-arrival-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="actual-departure-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"omobilityId", "status", "comment", "actualArrivalDate", "actualDepartureDate"})
@XmlRootElement(name = "student-mobility-for-studies")
public class StudentMobilityForStudies implements Serializable {

  @XmlElement(name = "omobility-id", required = true)
  protected String omobilityId;
  @XmlElement(required = true)
  @XmlSchemaType(name = "string")
  protected NominationStatus status;
  protected String comment;
  @XmlElement(name = "actual-arrival-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar actualArrivalDate;
  @XmlElement(name = "actual-departure-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar actualDepartureDate;

  /**
   * Obtiene el valor de la propiedad omobilityId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOmobilityId() {
    return omobilityId;
  }

  /**
   * Define el valor de la propiedad omobilityId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOmobilityId(String value) {
    this.omobilityId = value;
  }

  /**
   * Obtiene el valor de la propiedad status.
   * 
   * @return possible object is {@link NominationStatus }
   * 
   */
  public NominationStatus getStatus() {
    return status;
  }

  /**
   * Define el valor de la propiedad status.
   * 
   * @param value allowed object is {@link NominationStatus }
   * 
   */
  public void setStatus(NominationStatus value) {
    this.status = value;
  }

  /**
   * Obtiene el valor de la propiedad comment.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getComment() {
    return comment;
  }

  /**
   * Define el valor de la propiedad comment.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setComment(String value) {
    this.comment = value;
  }

  /**
   * Obtiene el valor de la propiedad actualArrivalDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getActualArrivalDate() {
    return actualArrivalDate;
  }

  /**
   * Define el valor de la propiedad actualArrivalDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setActualArrivalDate(XMLGregorianCalendar value) {
    this.actualArrivalDate = value;
  }

  /**
   * Obtiene el valor de la propiedad actualDepartureDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getActualDepartureDate() {
    return actualDepartureDate;
  }

  /**
   * Define el valor de la propiedad actualDepartureDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setActualDepartureDate(XMLGregorianCalendar value) {
    this.actualDepartureDate = value;
  }

}
