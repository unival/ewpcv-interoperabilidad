//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.architecture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="developer-message" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}MultilineString"/&gt;
 *         &lt;element name="user-message" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}MultilineStringWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"developerMessage", "userMessage"})
@XmlRootElement(name = "error-response")
public class ErrorResponse implements Serializable {

  @XmlElement(name = "developer-message", required = true)
  protected MultilineString developerMessage;
  @XmlElement(name = "user-message")
  protected List<MultilineStringWithOptionalLang> userMessage;

  /**
   * Obtiene el valor de la propiedad developerMessage.
   * 
   * @return possible object is {@link MultilineString }
   * 
   */
  public MultilineString getDeveloperMessage() {
    return developerMessage;
  }

  /**
   * Define el valor de la propiedad developerMessage.
   * 
   * @param value allowed object is {@link MultilineString }
   * 
   */
  public void setDeveloperMessage(MultilineString value) {
    this.developerMessage = value;
  }

  /**
   * Gets the value of the userMessage property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the userMessage property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getUserMessage().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link MultilineStringWithOptionalLang }
   * 
   * 
   */
  public List<MultilineStringWithOptionalLang> getUserMessage() {
    if (userMessage == null) {
      userMessage = new ArrayList<MultilineStringWithOptionalLang>();
    }
    return this.userMessage;
  }

}
