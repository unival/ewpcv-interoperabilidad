//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * eu.erasmuswithoutpaper.api.omobilities.las.endpoints package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlRegistry
public class ObjectFactory {


  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
   * eu.erasmuswithoutpaper.api.omobilities.las.endpoints
   * 
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link Component }
   * 
   */
  public Component createComponent() {
    return new Component();
  }

  /**
   * Create an instance of {@link MobilityInstitution }
   * 
   */
  public MobilityInstitution createMobilityInstitution() {
    return new MobilityInstitution();
  }

  /**
   * Create an instance of {@link LearningAgreement }
   * 
   */
  public LearningAgreement createLearningAgreement() {
    return new LearningAgreement();
  }

  /**
   * Create an instance of {@link OmobilityLasGetResponse }
   * 
   */
  public OmobilityLasGetResponse createOmobilityLasGetResponse() {
    return new OmobilityLasGetResponse();
  }

  /**
   * Create an instance of {@link Component.Credit }
   * 
   */
  public Component.Credit createComponentCredit() {
    return new Component.Credit();
  }

  /**
   * Create an instance of {@link Student }
   * 
   */
  public Student createStudent() {
    return new Student();
  }

  /**
   * Create an instance of {@link ListOfComponents }
   * 
   */
  public ListOfComponents createListOfComponents() {
    return new ListOfComponents();
  }

  /**
   * Create an instance of {@link ComponentList }
   * 
   */
  public ComponentList createComponentList() {
    return new ComponentList();
  }

  /**
   * Create an instance of {@link Signature }
   * 
   */
  public Signature createSignature() {
    return new Signature();
  }

  /**
   * Create an instance of {@link OmobilityLasIndexResponse }
   * 
   */
  public OmobilityLasIndexResponse createOmobilityLasIndexResponse() {
    return new OmobilityLasIndexResponse();
  }

  /**
   * Create an instance of {@link OmobilityLasUpdateResponse }
   * 
   */
  public OmobilityLasUpdateResponse createOmobilityLasUpdateResponse() {
    return new OmobilityLasUpdateResponse();
  }

  /**
   * Create an instance of {@link OmobilityLasUpdateRequest }
   * 
   */
  public OmobilityLasUpdateRequest createOmobilityLasUpdateRequest() {
    return new OmobilityLasUpdateRequest();
  }

  /**
   * Create an instance of {@link ApproveProposalV1 }
   * 
   */
  public ApproveProposalV1 createApproveProposalV1() {
    return new ApproveProposalV1();
  }

  /**
   * Create an instance of {@link CommentProposalV1 }
   * 
   */
  public CommentProposalV1 createCommentProposalV1() {
    return new CommentProposalV1();
  }

  /**
   * Create an instance of {@link MobilityInstitution.ContactPerson }
   * 
   */
  public MobilityInstitution.ContactPerson createMobilityInstitutionContactPerson() {
    return new MobilityInstitution.ContactPerson();
  }

  /**
   * Create an instance of {@link LearningAgreement.StudentLanguageSkill }
   * 
   */
  public LearningAgreement.StudentLanguageSkill createLearningAgreementStudentLanguageSkill() {
    return new LearningAgreement.StudentLanguageSkill();
  }

  /**
   * Create an instance of {@link LearningAgreement.ChangesProposal }
   * 
   */
  public LearningAgreement.ChangesProposal createLearningAgreementChangesProposal() {
    return new LearningAgreement.ChangesProposal();
  }

  /**
   * Create an instance of {@link LasOutgoingStatsResponse }
   *
   */
  public LasOutgoingStatsResponse createLasOutgoingStatsResponse() {
    return new LasOutgoingStatsResponse();
  }

  /**
   * Create an instance of {@link LasOutgoingStatsResponse.AcademicYearLaStats }
   *
   */
  public LasOutgoingStatsResponse.AcademicYearLaStats createLasOutgoingStatsResponseAcademicYearLaStats() {
    return new LasOutgoingStatsResponse.AcademicYearLaStats();
  }

  /**
   * Create an instance of {@link LasIncomingStatsResponse }
   *
   */
  public LasIncomingStatsResponse createLasIncomingStatsResponse() {
    return new LasIncomingStatsResponse();
  }

  /**
   * Create an instance of {@link LasIncomingStatsResponse.AcademicYearLaStats }
   *
   */
  public LasIncomingStatsResponse.AcademicYearLaStats createLasIncomingStatsResponseAcademicYearLaStats() {
    return new LasIncomingStatsResponse.AcademicYearLaStats();
  }

}
