//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.factsheet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.types.phonenumber.PhoneNumber;


/**
 * <p>
 * Clase Java para InformationEntry complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="InformationEntry"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email"/&gt;
 *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1}phone-number"/&gt;
 *         &lt;element name="website-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformationEntry", propOrder = {"email", "phoneNumber", "websiteUrl"})
public class InformationEntry implements Serializable {

  @XmlElement(required = true)
  protected String email;
  @XmlElement(name = "phone-number",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1",
      required = true)
  protected PhoneNumber phoneNumber;
  @XmlElement(name = "website-url", required = true)
  protected List<HTTPWithOptionalLang> websiteUrl;

  /**
   * Obtiene el valor de la propiedad email.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getEmail() {
    return email;
  }

  /**
   * Define el valor de la propiedad email.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setEmail(String value) {
    this.email = value;
  }

  /**
   * Obtiene el valor de la propiedad phoneNumber.
   * 
   * @return possible object is {@link PhoneNumber }
   * 
   */
  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * Define el valor de la propiedad phoneNumber.
   * 
   * @param value allowed object is {@link PhoneNumber }
   * 
   */
  public void setPhoneNumber(PhoneNumber value) {
    this.phoneNumber = value;
  }

  /**
   * Gets the value of the websiteUrl property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the websiteUrl property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getWebsiteUrl().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
   * 
   * 
   */
  public List<HTTPWithOptionalLang> getWebsiteUrl() {
    if (websiteUrl == null) {
      websiteUrl = new ArrayList<HTTPWithOptionalLang>();
    }
    return this.websiteUrl;
  }

}
