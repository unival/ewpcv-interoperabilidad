//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.imobilities.tors.cnr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import eu.erasmuswithoutpaper.api.architecture.Empty;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * eu.erasmuswithoutpaper.api.imobilities.tors.cnr package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlRegistry
public class ObjectFactory {

  private final static QName _ImobilityTorCnrResponse_QNAME =
      new QName("https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tor-cnr/tree/stable-v1",
          "imobility-tor-cnr-response");

  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
   * eu.erasmuswithoutpaper.api.imobilities.tors.cnr
   * 
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link ImobilityTorCnr }
   * 
   */
  public ImobilityTorCnr createImobilityTorCnr() {
    return new ImobilityTorCnr();
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link Empty }{@code >}}
   * 
   */
  @XmlElementDecl(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tor-cnr/tree/stable-v1",
      name = "imobility-tor-cnr-response")
  public JAXBElement<Empty> createImobilityTorCnrResponse(Empty value) {
    return new JAXBElement<Empty>(_ImobilityTorCnrResponse_QNAME, Empty.class, null, value);
  }

}
