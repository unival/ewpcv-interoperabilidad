//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.registry;

import java.io.Serializable;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.ManifestApiEntryBase;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}ManifestApiEntryBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="catalogue-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"catalogueUrl"})
@XmlRootElement(name = "registry",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-registry/blob/stable-v1/manifest-entry.xsd")
public class Registry extends ManifestApiEntryBase implements Serializable {

  @XmlElement(name = "catalogue-url",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-registry/blob/stable-v1/manifest-entry.xsd",
      required = true)
  @XmlSchemaType(name = "anyURI")
  protected String catalogueUrl;

  /**
   * Obtiene el valor de la propiedad catalogueUrl.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCatalogueUrl() {
    return catalogueUrl;
  }

  /**
   * Define el valor de la propiedad catalogueUrl.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setCatalogueUrl(String value) {
    this.catalogueUrl = value;
  }

}
