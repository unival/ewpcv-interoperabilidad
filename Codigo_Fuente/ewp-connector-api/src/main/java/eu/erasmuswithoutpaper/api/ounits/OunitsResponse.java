//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.ounits;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;
import eu.erasmuswithoutpaper.api.types.contact.Contact;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ounit" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *                   &lt;element name="ounit-code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                   &lt;element name="abbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="parent-ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
 *                   &lt;element name="website-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="logo-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS" minOccurs="0"/&gt;
 *                   &lt;element name="mobility-factsheet-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"ounit"})
@XmlRootElement(name = "ounits-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
public class OunitsResponse implements Serializable {

  @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
  protected List<OunitsResponse.Ounit> ounit;

  /**
   * Gets the value of the ounit property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the ounit property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getOunit().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link OunitsResponse.Ounit }
   * 
   * 
   */
  public List<OunitsResponse.Ounit> getOunit() {
    if (ounit == null) {
      ounit = new ArrayList<OunitsResponse.Ounit>();
    }
    return this.ounit;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
     *         &lt;element name="ounit-code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
     *         &lt;element name="abbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="parent-ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
     *         &lt;element name="website-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="logo-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS" minOccurs="0"/&gt;
     *         &lt;element name="mobility-factsheet-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"ounitId", "ounitCode", "name", "abbreviation", "parentOunitId", "streetAddress",
      "mailingAddress", "websiteUrl", "logoUrl", "mobilityFactsheetUrl", "contact"})
  public static class Ounit implements Serializable {

    @XmlElement(name = "ounit-id",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2", required = true)
    protected String ounitId;
    @XmlElement(name = "ounit-code",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2", required = true)
    protected String ounitCode;
    @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2",
        required = true)
    protected List<StringWithOptionalLang> name;
    @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
    protected String abbreviation;
    @XmlElement(name = "parent-ounit-id",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
    protected String parentOunitId;
    @XmlElement(name = "street-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress streetAddress;
    @XmlElement(name = "mailing-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress mailingAddress;
    @XmlElement(name = "website-url",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
    protected List<HTTPWithOptionalLang> websiteUrl;
    @XmlElement(name = "logo-url",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
    @XmlSchemaType(name = "anyURI")
    protected String logoUrl;
    @XmlElement(name = "mobility-factsheet-url",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-ounits/tree/stable-v2")
    protected List<HTTPWithOptionalLang> mobilityFactsheetUrl;
    @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1")
    protected List<Contact> contact;

    /**
     * Obtiene el valor de la propiedad ounitId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOunitId() {
      return ounitId;
    }

    /**
     * Define el valor de la propiedad ounitId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setOunitId(String value) {
      this.ounitId = value;
    }

    /**
     * Obtiene el valor de la propiedad ounitCode.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOunitCode() {
      return ounitCode;
    }

    /**
     * Define el valor de la propiedad ounitCode.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setOunitCode(String value) {
      this.ounitCode = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
     * 
     * 
     */
    public List<StringWithOptionalLang> getName() {
      if (name == null) {
        name = new ArrayList<StringWithOptionalLang>();
      }
      return this.name;
    }

    /**
     * Obtiene el valor de la propiedad abbreviation.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAbbreviation() {
      return abbreviation;
    }

    /**
     * Define el valor de la propiedad abbreviation.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAbbreviation(String value) {
      this.abbreviation = value;
    }

    /**
     * Obtiene el valor de la propiedad parentOunitId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getParentOunitId() {
      return parentOunitId;
    }

    /**
     * Define el valor de la propiedad parentOunitId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setParentOunitId(String value) {
      this.parentOunitId = value;
    }

    /**
     * 
     * Street address of this organizational unit.
     * 
     * This is the address which should work when, for example, the user pastes it into Google Maps. If this
     * organizational unit is spread between multiple addresses, then this address should refer to a "primary" building.
     * If it doesn't have a "primary" building, then the address should be skipped (multiple addresses can still be
     * provided via separate "contact" elements).
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getStreetAddress() {
      return streetAddress;
    }

    /**
     * Define el valor de la propiedad streetAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setStreetAddress(FlexibleAddress value) {
      this.streetAddress = value;
    }

    /**
     * 
     * The postal address of this organizational unit. It MAY be the same as street-address, but doesn't have to be
     * (e.g. it can be a PO box).
     * 
     * This is the primary address. Note, that more addresses may be provided by using the "contact" element, if
     * necessary.
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getMailingAddress() {
      return mailingAddress;
    }

    /**
     * Define el valor de la propiedad mailingAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setMailingAddress(FlexibleAddress value) {
      this.mailingAddress = value;
    }

    /**
     * Gets the value of the websiteUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the websiteUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getWebsiteUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
     * 
     * 
     */
    public List<HTTPWithOptionalLang> getWebsiteUrl() {
      if (websiteUrl == null) {
        websiteUrl = new ArrayList<HTTPWithOptionalLang>();
      }
      return this.websiteUrl;
    }

    /**
     * Obtiene el valor de la propiedad logoUrl.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLogoUrl() {
      return logoUrl;
    }

    /**
     * Define el valor de la propiedad logoUrl.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setLogoUrl(String value) {
      this.logoUrl = value;
    }

    /**
     * Gets the value of the mobilityFactsheetUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the mobilityFactsheetUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getMobilityFactsheetUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
     * 
     * 
     */
    public List<HTTPWithOptionalLang> getMobilityFactsheetUrl() {
      if (mobilityFactsheetUrl == null) {
        mobilityFactsheetUrl = new ArrayList<HTTPWithOptionalLang>();
      }
      return this.mobilityFactsheetUrl;
    }

    /**
     * 
     * A list of important contacts - phones, emails, addresses.
     * 
     * In context of EWP, this will most often be mobility-related contacts, but it's not a rule. Gets the value of the
     * contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Contact }
     * 
     * 
     */
    public List<Contact> getContact() {
      if (contact == null) {
        contact = new ArrayList<Contact>();
      }
      return this.contact;
    }

  }

}
