//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.types.academicterm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;


/**
 * <p>
 * Clase Java para AcademicTerm complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="AcademicTerm"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
 *         &lt;element name="term-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}TermId"/&gt;
 *         &lt;element name="display-name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *         &lt;element name="start-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="end-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcademicTerm", propOrder = {"academicYearId", "termId", "displayName", "startDate", "endDate"})
public class AcademicTerm implements Serializable {

  @XmlElement(name = "academic-year-id", required = true)
  protected String academicYearId;
  @XmlElement(name = "term-id", required = true)
  protected TermId termId;
  @XmlElement(name = "display-name", required = true)
  protected List<StringWithOptionalLang> displayName;
  @XmlElement(name = "start-date", required = true)
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar startDate;
  @XmlElement(name = "end-date", required = true)
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar endDate;

  /**
   * Obtiene el valor de la propiedad academicYearId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getAcademicYearId() {
    return academicYearId;
  }

  /**
   * Define el valor de la propiedad academicYearId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setAcademicYearId(String value) {
    this.academicYearId = value;
  }

  /**
   * Obtiene el valor de la propiedad termId.
   * 
   * @return possible object is {@link TermId }
   * 
   */
  public TermId getTermId() {
    return termId;
  }

  /**
   * Define el valor de la propiedad termId.
   * 
   * @param value allowed object is {@link TermId }
   * 
   */
  public void setTermId(TermId value) {
    this.termId = value;
  }

  /**
   * Gets the value of the displayName property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the displayName property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getDisplayName().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
   * 
   * 
   */
  public List<StringWithOptionalLang> getDisplayName() {
    if (displayName == null) {
      displayName = new ArrayList<StringWithOptionalLang>();
    }
    return this.displayName;
  }

  /**
   * Obtiene el valor de la propiedad startDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getStartDate() {
    return startDate;
  }

  /**
   * Define el valor de la propiedad startDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setStartDate(XMLGregorianCalendar value) {
    this.startDate = value;
  }

  /**
   * Obtiene el valor de la propiedad endDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getEndDate() {
    return endDate;
  }

  /**
   * Define el valor de la propiedad endDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setEndDate(XMLGregorianCalendar value) {
    this.endDate = value;
  }

}
