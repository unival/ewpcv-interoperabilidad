//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *         &lt;element name="changes-proposal-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="signature" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}Signature"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"omobilityId", "changesProposalId", "signature"})
@XmlRootElement(name = "approve-proposal-v1",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd")
public class ApproveProposalV1 implements Serializable {

  @XmlElement(name = "omobility-id",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd",
      required = true)
  protected String omobilityId;
  @XmlElement(name = "changes-proposal-id",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd",
      required = true)
  protected String changesProposalId;
  @XmlElement(
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd",
      required = true)
  protected Signature signature;

  /**
   * Obtiene el valor de la propiedad omobilityId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOmobilityId() {
    return omobilityId;
  }

  /**
   * Define el valor de la propiedad omobilityId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOmobilityId(String value) {
    this.omobilityId = value;
  }

  /**
   * Obtiene el valor de la propiedad changesProposalId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getChangesProposalId() {
    return changesProposalId;
  }

  /**
   * Define el valor de la propiedad changesProposalId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setChangesProposalId(String value) {
    this.changesProposalId = value;
  }

  /**
   * Obtiene el valor de la propiedad signature.
   * 
   * @return possible object is {@link Signature }
   * 
   */
  public Signature getSignature() {
    return signature;
  }

  /**
   * Define el valor de la propiedad signature.
   * 
   * @param value allowed object is {@link Signature }
   * 
   */
  public void setSignature(Signature value) {
    this.signature = value;
  }

}
