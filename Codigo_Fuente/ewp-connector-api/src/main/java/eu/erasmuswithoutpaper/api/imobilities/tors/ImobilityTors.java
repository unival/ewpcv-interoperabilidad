//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.imobilities.tors;

import java.io.Serializable;
import java.math.BigInteger;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.Empty;
import eu.erasmuswithoutpaper.api.architecture.ManifestApiEntryBase;
import eu.erasmuswithoutpaper.api.specs.sec.intro.HttpSecurityOptions;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}ManifestApiEntryBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="http-security" type="{https://github.com/erasmus-without-paper/ewp-specs-sec-intro/tree/stable-v2}HttpSecurityOptions" minOccurs="0"/&gt;
 *         &lt;element name="get-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS"/&gt;
 *         &lt;element name="index-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS"/&gt;
 *         &lt;element name="max-omobility-ids" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *         &lt;element name="sends-notifications" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Empty" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"httpSecurity", "getUrl", "indexUrl", "maxOmobilityIds", "sendsNotifications"})
@XmlRootElement(name = "imobility-tors")
public class ImobilityTors extends ManifestApiEntryBase implements Serializable {

  @XmlElement(name = "http-security")
  protected HttpSecurityOptions httpSecurity;
  @XmlElement(name = "get-url", required = true)
  @XmlSchemaType(name = "anyURI")
  protected String getUrl;
  @XmlElement(name = "index-url", required = true)
  @XmlSchemaType(name = "anyURI")
  protected String indexUrl;
  @XmlElement(name = "max-omobility-ids", required = true)
  @XmlSchemaType(name = "positiveInteger")
  protected BigInteger maxOmobilityIds;
  @XmlElement(name = "sends-notifications")
  protected Empty sendsNotifications;

  /**
   * Obtiene el valor de la propiedad httpSecurity.
   * 
   * @return possible object is {@link HttpSecurityOptions }
   * 
   */
  public HttpSecurityOptions getHttpSecurity() {
    return httpSecurity;
  }

  /**
   * Define el valor de la propiedad httpSecurity.
   * 
   * @param value allowed object is {@link HttpSecurityOptions }
   * 
   */
  public void setHttpSecurity(HttpSecurityOptions value) {
    this.httpSecurity = value;
  }

  /**
   * Obtiene el valor de la propiedad getUrl.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getGetUrl() {
    return getUrl;
  }

  /**
   * Define el valor de la propiedad getUrl.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setGetUrl(String value) {
    this.getUrl = value;
  }

  /**
   * Obtiene el valor de la propiedad indexUrl.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIndexUrl() {
    return indexUrl;
  }

  /**
   * Define el valor de la propiedad indexUrl.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIndexUrl(String value) {
    this.indexUrl = value;
  }

  /**
   * Obtiene el valor de la propiedad maxOmobilityIds.
   * 
   * @return possible object is {@link BigInteger }
   * 
   */
  public BigInteger getMaxOmobilityIds() {
    return maxOmobilityIds;
  }

  /**
   * Define el valor de la propiedad maxOmobilityIds.
   * 
   * @param value allowed object is {@link BigInteger }
   * 
   */
  public void setMaxOmobilityIds(BigInteger value) {
    this.maxOmobilityIds = value;
  }

  /**
   * Obtiene el valor de la propiedad sendsNotifications.
   * 
   * @return possible object is {@link Empty }
   * 
   */
  public Empty getSendsNotifications() {
    return sendsNotifications;
  }

  /**
   * Define el valor de la propiedad sendsNotifications.
   * 
   * @param value allowed object is {@link Empty }
   * 
   */
  public void setSendsNotifications(Empty value) {
    this.sendsNotifications = value;
  }

}
