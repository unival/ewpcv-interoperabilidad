//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.imobilities.endpoints;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>
 * Clase Java para NominationStatus.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 *
 * <pre>
 * &lt;simpleType name="NominationStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="pending"/&gt;
 *     &lt;enumeration value="verified"/&gt;
 *     &lt;enumeration value="rejected"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlType(name = "NominationStatus")
@XmlEnum
public enum NominationStatus {


  /**
   * 
   * The nomination proposal has not yet been accepted nor rejected by the receiving HEI.
   * 
   * 
   */
  @XmlEnumValue("pending")
  PENDING("pending"),

  /**
   * 
   * The nomination was recognized by the receiving HEI as formally correct and complying with cooperation conditions.
   * Now it's time for settling formalities, sending student's documents, creating the first Learning Agreement, etc.
   * 
   * 
   */
  @XmlEnumValue("verified")
  VERIFIED("verified"),

  /**
   * 
   * The nomination has been rejected by the receiving HEI.
   * 
   * 
   */
  @XmlEnumValue("rejected")
  REJECTED("rejected");
  private final String value;

  NominationStatus(String v) {
    value = v;
  }

  public static NominationStatus fromValue(String v) {
    for (NominationStatus c : NominationStatus.values()) {
      if (c.value.equals(v)) {
        return c;
      }
    }
    throw new IllegalArgumentException(v);
  }

  public String value() {
    return value;
  }

}
