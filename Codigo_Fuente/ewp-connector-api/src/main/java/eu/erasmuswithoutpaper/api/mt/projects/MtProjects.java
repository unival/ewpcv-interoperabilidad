//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.mt.projects;

import java.io.Serializable;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.ManifestApiEntryBase;
import eu.erasmuswithoutpaper.api.specs.sec.intro.HttpSecurityOptions;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}ManifestApiEntryBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="http-security" type="{https://github.com/erasmus-without-paper/ewp-specs-sec-intro/tree/stable-v2}HttpSecurityOptions" minOccurs="0"/&gt;
 *         &lt;element name="url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"httpSecurity", "url"})
@XmlRootElement(name = "mt-projects",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-mt-projects/blob/stable-v1/manifest-entry.xsd")
public class MtProjects extends ManifestApiEntryBase implements Serializable {

  @XmlElement(name = "http-security",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-mt-projects/blob/stable-v1/manifest-entry.xsd")
  protected HttpSecurityOptions httpSecurity;
  @XmlElement(
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-mt-projects/blob/stable-v1/manifest-entry.xsd",
      required = true)
  @XmlSchemaType(name = "anyURI")
  protected String url;

  /**
   * Obtiene el valor de la propiedad httpSecurity.
   * 
   * @return possible object is {@link HttpSecurityOptions }
   * 
   */
  public HttpSecurityOptions getHttpSecurity() {
    return httpSecurity;
  }

  /**
   * Define el valor de la propiedad httpSecurity.
   * 
   * @param value allowed object is {@link HttpSecurityOptions }
   * 
   */
  public void setHttpSecurity(HttpSecurityOptions value) {
    this.httpSecurity = value;
  }

  /**
   * Obtiene el valor de la propiedad url.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getUrl() {
    return url;
  }

  /**
   * Define el valor de la propiedad url.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setUrl(String value) {
    this.url = value;
  }

}
