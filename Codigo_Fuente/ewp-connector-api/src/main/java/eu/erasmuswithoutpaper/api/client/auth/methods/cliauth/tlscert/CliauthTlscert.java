//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.client.auth.methods.cliauth.tlscert;

import java.io.Serializable;

import javax.xml.bind.annotation.*;


/**
 * This element uniquely identifies the TLS Client Certificate Authentication method, as described here:
 * <p>
 * https://github.com/erasmus-without-paper/ewp-specs-sec-cliauth-tlscert
 * <p>
 * It can be used in various contexts, whenever someone needs to identify this particular method of client
 * authentication. In particular, it is often seen together with `HttpSecurityOptions` data type described here:
 * <p>
 * https://github.com/erasmus-without-paper/ewp-specs-sec-intro/blob/stable-v2/schema.xsd
 *
 *
 * <p>
 * Clase Java para tlscert element declaration.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;element name="tlscert"&gt;
 *   &lt;complexType&gt;
 *     &lt;complexContent&gt;
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *         &lt;sequence&gt;
 *         &lt;/sequence&gt;
 *         &lt;attribute name="allows-self-signed" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;/restriction&gt;
 *     &lt;/complexContent&gt;
 *   &lt;/complexType&gt;
 * &lt;/element&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "tlscert")
public class CliauthTlscert implements Serializable {

  @XmlAttribute(name = "allows-self-signed", required = true)
  protected boolean allowsSelfSigned;

  /**
   * Obtiene el valor de la propiedad allowsSelfSigned.
   * 
   */
  public boolean isAllowsSelfSigned() {
    return allowsSelfSigned;
  }

  /**
   * Define el valor de la propiedad allowsSelfSigned.
   * 
   */
  public void setAllowsSelfSigned(boolean value) {
    this.allowsSelfSigned = value;
  }

}
