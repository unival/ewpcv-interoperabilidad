//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2023.01.17 a las 11:32:04 AM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="academic-year-la-stats" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
 *                   &lt;element name="la-outgoing-total" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-outgoing-not-modified-after-approval" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-outgoing-modified-after-approval" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-outgoing-latest-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-outgoing-latest-version-rejected" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-outgoing-latest-version-awaiting" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "academicYearLaStats"
})
@XmlRootElement(name = "las-outgoing-stats-response", namespace="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/stats-response.xsd")
public class LasOutgoingStatsResponse {

    @XmlElement(name = "academic-year-la-stats")
    protected List<LasOutgoingStatsResponse.AcademicYearLaStats> academicYearLaStats;

    /**
     * Gets the value of the academicYearLaStats property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the academicYearLaStats property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcademicYearLaStats().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LasOutgoingStatsResponse.AcademicYearLaStats }
     *
     *
     */
    public List<LasOutgoingStatsResponse.AcademicYearLaStats> getAcademicYearLaStats() {
        if (academicYearLaStats == null) {
            academicYearLaStats = new ArrayList<LasOutgoingStatsResponse.AcademicYearLaStats>();
        }
        return this.academicYearLaStats;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     *
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
     *         &lt;element name="la-outgoing-total" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-outgoing-not-modified-after-approval" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-outgoing-modified-after-approval" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-outgoing-latest-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-outgoing-latest-version-rejected" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-outgoing-latest-version-awaiting" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receivingAcademicYearId",
        "laOutgoingTotal",
        "laOutgoingNotModifiedAfterApproval",
        "laOutgoingModifiedAfterApproval",
        "laOutgoingLatestVersionApproved",
        "laOutgoingLatestVersionRejected",
        "laOutgoingLatestVersionAwaiting"
    })
    public static class AcademicYearLaStats implements Serializable {

        @XmlElement(name = "receiving-academic-year-id", required = true)
        protected String receivingAcademicYearId;
        @XmlElement(name = "la-outgoing-total", required = true)
        protected BigInteger laOutgoingTotal;
        @XmlElement(name = "la-outgoing-not-modified-after-approval", required = true)
        protected BigInteger laOutgoingNotModifiedAfterApproval;
        @XmlElement(name = "la-outgoing-modified-after-approval", required = true)
        protected BigInteger laOutgoingModifiedAfterApproval;
        @XmlElement(name = "la-outgoing-latest-version-approved", required = true)
        protected BigInteger laOutgoingLatestVersionApproved;
        @XmlElement(name = "la-outgoing-latest-version-rejected", required = true)
        protected BigInteger laOutgoingLatestVersionRejected;
        @XmlElement(name = "la-outgoing-latest-version-awaiting", required = true)
        protected BigInteger laOutgoingLatestVersionAwaiting;

        /**
         * Obtiene el valor de la propiedad receivingAcademicYearId.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getReceivingAcademicYearId() {
            return receivingAcademicYearId;
        }

        /**
         * Define el valor de la propiedad receivingAcademicYearId.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setReceivingAcademicYearId(String value) {
            this.receivingAcademicYearId = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingTotal.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingTotal() {
            return laOutgoingTotal;
        }

        /**
         * Define el valor de la propiedad laOutgoingTotal.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingTotal(BigInteger value) {
            this.laOutgoingTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingNotModifiedAfterApproval.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingNotModifiedAfterApproval() {
            return laOutgoingNotModifiedAfterApproval;
        }

        /**
         * Define el valor de la propiedad laOutgoingNotModifiedAfterApproval.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingNotModifiedAfterApproval(BigInteger value) {
            this.laOutgoingNotModifiedAfterApproval = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingModifiedAfterApproval.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingModifiedAfterApproval() {
            return laOutgoingModifiedAfterApproval;
        }

        /**
         * Define el valor de la propiedad laOutgoingModifiedAfterApproval.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingModifiedAfterApproval(BigInteger value) {
            this.laOutgoingModifiedAfterApproval = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingLatestVersionApproved.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingLatestVersionApproved() {
            return laOutgoingLatestVersionApproved;
        }

        /**
         * Define el valor de la propiedad laOutgoingLatestVersionApproved.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingLatestVersionApproved(BigInteger value) {
            this.laOutgoingLatestVersionApproved = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingLatestVersionRejected.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingLatestVersionRejected() {
            return laOutgoingLatestVersionRejected;
        }

        /**
         * Define el valor de la propiedad laOutgoingLatestVersionRejected.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingLatestVersionRejected(BigInteger value) {
            this.laOutgoingLatestVersionRejected = value;
        }

        /**
         * Obtiene el valor de la propiedad laOutgoingLatestVersionAwaiting.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaOutgoingLatestVersionAwaiting() {
            return laOutgoingLatestVersionAwaiting;
        }

        /**
         * Define el valor de la propiedad laOutgoingLatestVersionAwaiting.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaOutgoingLatestVersionAwaiting(BigInteger value) {
            this.laOutgoingLatestVersionAwaiting = value;
        }

    }

}
