package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2023.01.23 a las 01:34:50 PM CET
//

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="academic-year-la-stats" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
 *                   &lt;element name="la-incoming-total" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-incoming-some-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-incoming-latest-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-incoming-latest-version-rejected" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="la-incoming-latest-version-awaiting" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "academicYearLaStats"
})
@XmlRootElement(name = "las-incoming-stats-response",
        namespace="https://github.com/erasmus-without-paper/ewp-specs-api-omobility-la-cnr/tree/stable-v1/endpoints/stats-response.xsd")
public class LasIncomingStatsResponse {

    @XmlElement(name = "academic-year-la-stats")
    protected List<LasIncomingStatsResponse.AcademicYearLaStats> academicYearLaStats;

    /**
     * Gets the value of the academicYearLaStats property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the academicYearLaStats property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcademicYearLaStats().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LasIncomingStatsResponse.AcademicYearLaStats }
     *
     *
     */
    public List<LasIncomingStatsResponse.AcademicYearLaStats> getAcademicYearLaStats() {
        if (academicYearLaStats == null) {
            academicYearLaStats = new ArrayList<LasIncomingStatsResponse.AcademicYearLaStats>();
        }
        return this.academicYearLaStats;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     *
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
     *         &lt;element name="la-incoming-total" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-incoming-some-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-incoming-latest-version-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-incoming-latest-version-rejected" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="la-incoming-latest-version-awaiting" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "receivingAcademicYearId",
            "laIncomingTotal",
            "laIncomingSomeVersionApproved",
            "laIncomingLatestVersionApproved",
            "laIncomingLatestVersionRejected",
            "laIncomingLatestVersionAwaiting"
    })
    public static class AcademicYearLaStats {

        @XmlElement(name = "receiving-academic-year-id", required = true)
        protected String receivingAcademicYearId;
        @XmlElement(name = "la-incoming-total", required = true)
        protected BigInteger laIncomingTotal;
        @XmlElement(name = "la-incoming-some-version-approved", required = true)
        protected BigInteger laIncomingSomeVersionApproved;
        @XmlElement(name = "la-incoming-latest-version-approved", required = true)
        protected BigInteger laIncomingLatestVersionApproved;
        @XmlElement(name = "la-incoming-latest-version-rejected", required = true)
        protected BigInteger laIncomingLatestVersionRejected;
        @XmlElement(name = "la-incoming-latest-version-awaiting", required = true)
        protected BigInteger laIncomingLatestVersionAwaiting;

        /**
         * Obtiene el valor de la propiedad receivingAcademicYearId.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getReceivingAcademicYearId() {
            return receivingAcademicYearId;
        }

        /**
         * Define el valor de la propiedad receivingAcademicYearId.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setReceivingAcademicYearId(String value) {
            this.receivingAcademicYearId = value;
        }

        /**
         * Obtiene el valor de la propiedad laIncomingTotal.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaIncomingTotal() {
            return laIncomingTotal;
        }

        /**
         * Define el valor de la propiedad laIncomingTotal.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaIncomingTotal(BigInteger value) {
            this.laIncomingTotal = value;
        }

        /**
         * Obtiene el valor de la propiedad laIncomingSomeVersionApproved.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaIncomingSomeVersionApproved() {
            return laIncomingSomeVersionApproved;
        }

        /**
         * Define el valor de la propiedad laIncomingSomeVersionApproved.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaIncomingSomeVersionApproved(BigInteger value) {
            this.laIncomingSomeVersionApproved = value;
        }

        /**
         * Obtiene el valor de la propiedad laIncomingLatestVersionApproved.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaIncomingLatestVersionApproved() {
            return laIncomingLatestVersionApproved;
        }

        /**
         * Define el valor de la propiedad laIncomingLatestVersionApproved.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaIncomingLatestVersionApproved(BigInteger value) {
            this.laIncomingLatestVersionApproved = value;
        }

        /**
         * Obtiene el valor de la propiedad laIncomingLatestVersionRejected.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaIncomingLatestVersionRejected() {
            return laIncomingLatestVersionRejected;
        }

        /**
         * Define el valor de la propiedad laIncomingLatestVersionRejected.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaIncomingLatestVersionRejected(BigInteger value) {
            this.laIncomingLatestVersionRejected = value;
        }

        /**
         * Obtiene el valor de la propiedad laIncomingLatestVersionAwaiting.
         *
         * @return
         *     possible object is
         *     {@link BigInteger }
         *
         */
        public BigInteger getLaIncomingLatestVersionAwaiting() {
            return laIncomingLatestVersionAwaiting;
        }

        /**
         * Define el valor de la propiedad laIncomingLatestVersionAwaiting.
         *
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *
         */
        public void setLaIncomingLatestVersionAwaiting(BigInteger value) {
            this.laIncomingLatestVersionAwaiting = value;
        }

    }

}
