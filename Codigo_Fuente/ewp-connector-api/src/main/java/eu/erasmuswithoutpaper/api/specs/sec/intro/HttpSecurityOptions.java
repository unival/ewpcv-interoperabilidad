//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.specs.sec.intro;

import org.w3c.dom.Element;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;


/**
 * Describes security policies supported by a single particular server HTTP endpoint, or a set of HTTP endpoints
 * (depending on context).
 * <p>
 * Whenever possible, API designers are RECOMMENDED to reuse this data type in their `manifest-entry.xsd` files (if all
 * APIs use the same data type for describing their security options, then it makes client development easier).
 *
 *
 * <p>
 * Clase Java para HttpSecurityOptions complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="HttpSecurityOptions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="client-auth-methods" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="server-auth-methods" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="request-encryption-methods" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="response-encryption-methods" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HttpSecurityOptions",
    propOrder = {"clientAuthMethods", "serverAuthMethods", "requestEncryptionMethods", "responseEncryptionMethods"})
public class HttpSecurityOptions implements Serializable {

  @XmlElement(name = "client-auth-methods")
  protected HttpSecurityOptions.ClientAuthMethods clientAuthMethods;
  @XmlElement(name = "server-auth-methods")
  protected HttpSecurityOptions.ServerAuthMethods serverAuthMethods;
  @XmlElement(name = "request-encryption-methods")
  protected HttpSecurityOptions.RequestEncryptionMethods requestEncryptionMethods;
  @XmlElement(name = "response-encryption-methods")
  protected HttpSecurityOptions.ResponseEncryptionMethods responseEncryptionMethods;

  /**
   * Obtiene el valor de la propiedad clientAuthMethods.
   * 
   * @return possible object is {@link HttpSecurityOptions.ClientAuthMethods }
   * 
   */
  public HttpSecurityOptions.ClientAuthMethods getClientAuthMethods() {
    return clientAuthMethods;
  }

  /**
   * Define el valor de la propiedad clientAuthMethods.
   * 
   * @param value allowed object is {@link HttpSecurityOptions.ClientAuthMethods }
   * 
   */
  public void setClientAuthMethods(HttpSecurityOptions.ClientAuthMethods value) {
    this.clientAuthMethods = value;
  }

  /**
   * Obtiene el valor de la propiedad serverAuthMethods.
   * 
   * @return possible object is {@link HttpSecurityOptions.ServerAuthMethods }
   * 
   */
  public HttpSecurityOptions.ServerAuthMethods getServerAuthMethods() {
    return serverAuthMethods;
  }

  /**
   * Define el valor de la propiedad serverAuthMethods.
   * 
   * @param value allowed object is {@link HttpSecurityOptions.ServerAuthMethods }
   * 
   */
  public void setServerAuthMethods(HttpSecurityOptions.ServerAuthMethods value) {
    this.serverAuthMethods = value;
  }

  /**
   * Obtiene el valor de la propiedad requestEncryptionMethods.
   * 
   * @return possible object is {@link HttpSecurityOptions.RequestEncryptionMethods }
   * 
   */
  public HttpSecurityOptions.RequestEncryptionMethods getRequestEncryptionMethods() {
    return requestEncryptionMethods;
  }

  /**
   * Define el valor de la propiedad requestEncryptionMethods.
   * 
   * @param value allowed object is {@link HttpSecurityOptions.RequestEncryptionMethods }
   * 
   */
  public void setRequestEncryptionMethods(HttpSecurityOptions.RequestEncryptionMethods value) {
    this.requestEncryptionMethods = value;
  }

  /**
   * Obtiene el valor de la propiedad responseEncryptionMethods.
   * 
   * @return possible object is {@link HttpSecurityOptions.ResponseEncryptionMethods }
   * 
   */
  public HttpSecurityOptions.ResponseEncryptionMethods getResponseEncryptionMethods() {
    return responseEncryptionMethods;
  }

  /**
   * Define el valor de la propiedad responseEncryptionMethods.
   * 
   * @param value allowed object is {@link HttpSecurityOptions.ResponseEncryptionMethods }
   * 
   */
  public void setResponseEncryptionMethods(HttpSecurityOptions.ResponseEncryptionMethods value) {
    this.responseEncryptionMethods = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"any"})
  public static class ClientAuthMethods implements Serializable {

    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Object } {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
      if (any == null) {
        any = new ArrayList<Object>();
      }
      return this.any;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"any"})
  public static class RequestEncryptionMethods implements Serializable {

    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Object } {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
      if (any == null) {
        any = new ArrayList<Object>();
      }
      return this.any;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"any"})
  public static class ResponseEncryptionMethods implements Serializable {

    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Object } {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
      if (any == null) {
        any = new ArrayList<Object>();
      }
      return this.any;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"any"})
  public static class ServerAuthMethods implements Serializable {

    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Object } {@link Element }
     * 
     * 
     */
    public List<Object> getAny() {
      if (any == null) {
        any = new ArrayList<Object>();
      }
      return this.any;
    }

  }

}
