//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.mt.institutions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hei" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="pic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="erasmus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="erasmus-charter" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="startDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                           &lt;attribute name="endDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"hei"})
@XmlRootElement(name = "mt-institutions-response")
public class MtInstitutionsResponse implements Serializable {

  protected List<MtInstitutionsResponse.Hei> hei;

  /**
   * Gets the value of the hei property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the hei property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getHei().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link MtInstitutionsResponse.Hei }
   * 
   * 
   */
  public List<MtInstitutionsResponse.Hei> getHei() {
    if (hei == null) {
      hei = new ArrayList<MtInstitutionsResponse.Hei>();
    }
    return this.hei;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="pic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="erasmus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="erasmus-charter" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="startDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *                 &lt;attribute name="endDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"pic", "erasmus", "erasmusCharter", "name", "mailingAddress"})
  public static class Hei implements Serializable {

    protected String pic;
    protected String erasmus;
    @XmlElement(name = "erasmus-charter")
    protected MtInstitutionsResponse.Hei.ErasmusCharter erasmusCharter;
    @XmlElement(required = true)
    protected List<StringWithOptionalLang> name;
    @XmlElement(name = "mailing-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress mailingAddress;

    /**
     * Obtiene el valor de la propiedad pic.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getPic() {
      return pic;
    }

    /**
     * Define el valor de la propiedad pic.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setPic(String value) {
      this.pic = value;
    }

    /**
     * Obtiene el valor de la propiedad erasmus.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getErasmus() {
      return erasmus;
    }

    /**
     * Define el valor de la propiedad erasmus.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setErasmus(String value) {
      this.erasmus = value;
    }

    /**
     * Obtiene el valor de la propiedad erasmusCharter.
     * 
     * @return possible object is {@link MtInstitutionsResponse.Hei.ErasmusCharter }
     * 
     */
    public MtInstitutionsResponse.Hei.ErasmusCharter getErasmusCharter() {
      return erasmusCharter;
    }

    /**
     * Define el valor de la propiedad erasmusCharter.
     * 
     * @param value allowed object is {@link MtInstitutionsResponse.Hei.ErasmusCharter }
     * 
     */
    public void setErasmusCharter(MtInstitutionsResponse.Hei.ErasmusCharter value) {
      this.erasmusCharter = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
     * 
     * 
     */
    public List<StringWithOptionalLang> getName() {
      if (name == null) {
        name = new ArrayList<StringWithOptionalLang>();
      }
      return this.name;
    }

    /**
     * 
     * The postal address of the institution.
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getMailingAddress() {
      return mailingAddress;
    }

    /**
     * Define el valor de la propiedad mailingAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setMailingAddress(FlexibleAddress value) {
      this.mailingAddress = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="startDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *       &lt;attribute name="endDate" use="required" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    public static class ErasmusCharter implements Serializable {

      @XmlValue
      protected String value;
      @XmlAttribute(name = "startDate", required = true)
      @XmlSchemaType(name = "date")
      protected XMLGregorianCalendar startDate;
      @XmlAttribute(name = "endDate", required = true)
      @XmlSchemaType(name = "date")
      protected XMLGregorianCalendar endDate;

      /**
       * Obtiene el valor de la propiedad value.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getValue() {
        return value;
      }

      /**
       * Define el valor de la propiedad value.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setValue(String value) {
        this.value = value;
      }

      /**
       * Obtiene el valor de la propiedad startDate.
       * 
       * @return possible object is {@link XMLGregorianCalendar }
       * 
       */
      public XMLGregorianCalendar getStartDate() {
        return startDate;
      }

      /**
       * Define el valor de la propiedad startDate.
       * 
       * @param value allowed object is {@link XMLGregorianCalendar }
       * 
       */
      public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
      }

      /**
       * Obtiene el valor de la propiedad endDate.
       * 
       * @return possible object is {@link XMLGregorianCalendar }
       * 
       */
      public XMLGregorianCalendar getEndDate() {
        return endDate;
      }

      /**
       * Define el valor de la propiedad endDate.
       * 
       * @param value allowed object is {@link XMLGregorianCalendar }
       * 
       */
      public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
      }

    }

  }

}
