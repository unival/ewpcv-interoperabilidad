//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Clase Java para SubjectArea complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubjectArea"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isced-f-code"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd&gt;isced"&gt;
 *                 &lt;attribute name="v6-value"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;pattern value="[0-9]{1,3}"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="isced-clarification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubjectArea", propOrder = {
    "iscedFCode",
    "iscedClarification"
})
public class SubjectArea {

    @XmlElement(name = "isced-f-code", required = true)
    protected IscedFCode iscedFCode;
    @XmlElement(name = "isced-clarification")
    protected String iscedClarification;

    /**
     * Obtiene el valor de la propiedad iscedFCode.
     * 
     * @return
     *     possible object is
     *     {@link IscedFCode }
     *     
     */
    public IscedFCode getIscedFCode() {
        return iscedFCode;
    }

    /**
     * Define el valor de la propiedad iscedFCode.
     * 
     * @param value
     *     allowed object is
     *     {@link IscedFCode }
     *     
     */
    public void setIscedFCode(IscedFCode value) {
        this.iscedFCode = value;
    }

    /**
     * Obtiene el valor de la propiedad iscedClarification.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIscedClarification() {
        return iscedClarification;
    }

    /**
     * Define el valor de la propiedad iscedClarification.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIscedClarification(String value) {
        this.iscedClarification = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd&gt;isced"&gt;
     *       &lt;attribute name="v6-value"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;pattern value="[0-9]{1,3}"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class IscedFCode {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "v6-value")
        protected String v6Value;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad v6Value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getV6Value() {
            return v6Value;
        }

        /**
         * Define el valor de la propiedad v6Value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setV6Value(String value) {
            this.v6Value = value;
        }

    }

}
