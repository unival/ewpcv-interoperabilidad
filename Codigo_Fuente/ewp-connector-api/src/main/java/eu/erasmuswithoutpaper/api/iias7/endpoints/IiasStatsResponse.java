//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2024.01.11 a las 11:47:11 AM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="iia-fetchable" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="iia-local-unapproved-partner-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="iia-local-approved-partner-unapproved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="iia-both-approved" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "iiaFetchable",
    "iiaLocalUnapprovedPartnerApproved",
    "iiaLocalApprovedPartnerUnapproved",
    "iiaBothApproved"
})
@XmlRootElement(name = "iias-stats-response", namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/stats-response.xsd")
public class IiasStatsResponse {

    @XmlElement(name = "iia-fetchable", required = true, namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/stats-response.xsd")
    protected BigInteger iiaFetchable;
    @XmlElement(name = "iia-local-unapproved-partner-approved", required = true, namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/stats-response.xsd")
    protected BigInteger iiaLocalUnapprovedPartnerApproved;
    @XmlElement(name = "iia-local-approved-partner-unapproved", required = true, namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/stats-response.xsd")
    protected BigInteger iiaLocalApprovedPartnerUnapproved;
    @XmlElement(name = "iia-both-approved", required = true, namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/stats-response.xsd")
    protected BigInteger iiaBothApproved;

    /**
     * Obtiene el valor de la propiedad iiaFetchable.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIiaFetchable() {
        return iiaFetchable;
    }

    /**
     * Define el valor de la propiedad iiaFetchable.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIiaFetchable(BigInteger value) {
        this.iiaFetchable = value;
    }

    /**
     * Obtiene el valor de la propiedad iiaLocalUnapprovedPartnerApproved.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIiaLocalUnapprovedPartnerApproved() {
        return iiaLocalUnapprovedPartnerApproved;
    }

    /**
     * Define el valor de la propiedad iiaLocalUnapprovedPartnerApproved.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIiaLocalUnapprovedPartnerApproved(BigInteger value) {
        this.iiaLocalUnapprovedPartnerApproved = value;
    }

    /**
     * Obtiene el valor de la propiedad iiaLocalApprovedPartnerUnapproved.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIiaLocalApprovedPartnerUnapproved() {
        return iiaLocalApprovedPartnerUnapproved;
    }

    /**
     * Define el valor de la propiedad iiaLocalApprovedPartnerUnapproved.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIiaLocalApprovedPartnerUnapproved(BigInteger value) {
        this.iiaLocalApprovedPartnerUnapproved = value;
    }

    /**
     * Obtiene el valor de la propiedad iiaBothApproved.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIiaBothApproved() {
        return iiaBothApproved;
    }

    /**
     * Define el valor de la propiedad iiaBothApproved.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIiaBothApproved(BigInteger value) {
        this.iiaBothApproved = value;
    }

}
