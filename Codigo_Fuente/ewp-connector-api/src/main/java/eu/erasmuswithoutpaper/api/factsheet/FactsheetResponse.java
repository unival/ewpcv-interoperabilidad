//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.factsheet;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="factsheet" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="calendar"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="student-nominations" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
 *                             &lt;element name="student-applications" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="application-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                   &lt;element name="additional-requirement" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="details" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="information-website" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="decision-weeks-limit" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="tor-weeks-limit" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                   &lt;element name="accessibility" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="type"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="infrastructure"/&gt;
 *                                   &lt;enumeration value="service"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="information" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="housing-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                   &lt;element name="visa-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                   &lt;element name="insurance-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                   &lt;element name="additional-info" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"factsheet"})
@XmlRootElement(name = "factsheet-response")
public class FactsheetResponse implements Serializable {

  protected List<FactsheetResponse.Factsheet> factsheet;

  /**
   * Gets the value of the factsheet property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the factsheet property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getFactsheet().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link FactsheetResponse.Factsheet }
   * 
   * 
   */
  public List<FactsheetResponse.Factsheet> getFactsheet() {
    if (factsheet == null) {
      factsheet = new ArrayList<FactsheetResponse.Factsheet>();
    }
    return this.factsheet;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="calendar"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="student-nominations" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
     *                   &lt;element name="student-applications" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="application-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *         &lt;element name="additional-requirement" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="details" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="information-website" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="decision-weeks-limit" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="tor-weeks-limit" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *         &lt;element name="accessibility" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="type"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                         &lt;enumeration value="infrastructure"/&gt;
     *                         &lt;enumeration value="service"/&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="information" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="housing-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *         &lt;element name="visa-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *         &lt;element name="insurance-info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *         &lt;element name="additional-info" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "",
      propOrder = {"heiId", "calendar", "applicationInfo", "additionalRequirement", "decisionWeeksLimit",
          "torWeeksLimit", "accessibility", "housingInfo", "visaInfo", "insuranceInfo", "additionalInfo"})
  public static class Factsheet implements Serializable {

    @XmlElement(name = "hei-id", required = true)
    protected String heiId;
    @XmlElement(required = true)
    protected FactsheetResponse.Factsheet.Calendar calendar;
    @XmlElement(name = "application-info", required = true)
    protected InformationEntry applicationInfo;
    @XmlElement(name = "additional-requirement")
    protected List<FactsheetResponse.Factsheet.AdditionalRequirement> additionalRequirement;
    @XmlElement(name = "decision-weeks-limit", required = true)
    protected BigInteger decisionWeeksLimit;
    @XmlElement(name = "tor-weeks-limit", required = true)
    protected BigInteger torWeeksLimit;
    protected List<FactsheetResponse.Factsheet.Accessibility> accessibility;
    @XmlElement(name = "housing-info", required = true)
    protected InformationEntry housingInfo;
    @XmlElement(name = "visa-info", required = true)
    protected InformationEntry visaInfo;
    @XmlElement(name = "insurance-info", required = true)
    protected InformationEntry insuranceInfo;
    @XmlElement(name = "additional-info")
    protected List<FactsheetResponse.Factsheet.AdditionalInfo> additionalInfo;

    /**
     * Obtiene el valor de la propiedad heiId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHeiId() {
      return heiId;
    }

    /**
     * Define el valor de la propiedad heiId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setHeiId(String value) {
      this.heiId = value;
    }

    /**
     * Obtiene el valor de la propiedad calendar.
     * 
     * @return possible object is {@link FactsheetResponse.Factsheet.Calendar }
     * 
     */
    public FactsheetResponse.Factsheet.Calendar getCalendar() {
      return calendar;
    }

    /**
     * Define el valor de la propiedad calendar.
     * 
     * @param value allowed object is {@link FactsheetResponse.Factsheet.Calendar }
     * 
     */
    public void setCalendar(FactsheetResponse.Factsheet.Calendar value) {
      this.calendar = value;
    }

    /**
     * Obtiene el valor de la propiedad applicationInfo.
     * 
     * @return possible object is {@link InformationEntry }
     * 
     */
    public InformationEntry getApplicationInfo() {
      return applicationInfo;
    }

    /**
     * Define el valor de la propiedad applicationInfo.
     * 
     * @param value allowed object is {@link InformationEntry }
     * 
     */
    public void setApplicationInfo(InformationEntry value) {
      this.applicationInfo = value;
    }

    /**
     * Gets the value of the additionalRequirement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the additionalRequirement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalRequirement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FactsheetResponse.Factsheet.AdditionalRequirement }
     * 
     * 
     */
    public List<FactsheetResponse.Factsheet.AdditionalRequirement> getAdditionalRequirement() {
      if (additionalRequirement == null) {
        additionalRequirement = new ArrayList<FactsheetResponse.Factsheet.AdditionalRequirement>();
      }
      return this.additionalRequirement;
    }

    /**
     * Obtiene el valor de la propiedad decisionWeeksLimit.
     * 
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getDecisionWeeksLimit() {
      return decisionWeeksLimit;
    }

    /**
     * Define el valor de la propiedad decisionWeeksLimit.
     * 
     * @param value allowed object is {@link BigInteger }
     * 
     */
    public void setDecisionWeeksLimit(BigInteger value) {
      this.decisionWeeksLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad torWeeksLimit.
     * 
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getTorWeeksLimit() {
      return torWeeksLimit;
    }

    /**
     * Define el valor de la propiedad torWeeksLimit.
     * 
     * @param value allowed object is {@link BigInteger }
     * 
     */
    public void setTorWeeksLimit(BigInteger value) {
      this.torWeeksLimit = value;
    }

    /**
     * Gets the value of the accessibility property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the accessibility property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAccessibility().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FactsheetResponse.Factsheet.Accessibility }
     * 
     * 
     */
    public List<FactsheetResponse.Factsheet.Accessibility> getAccessibility() {
      if (accessibility == null) {
        accessibility = new ArrayList<FactsheetResponse.Factsheet.Accessibility>();
      }
      return this.accessibility;
    }

    /**
     * Obtiene el valor de la propiedad housingInfo.
     * 
     * @return possible object is {@link InformationEntry }
     * 
     */
    public InformationEntry getHousingInfo() {
      return housingInfo;
    }

    /**
     * Define el valor de la propiedad housingInfo.
     * 
     * @param value allowed object is {@link InformationEntry }
     * 
     */
    public void setHousingInfo(InformationEntry value) {
      this.housingInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad visaInfo.
     * 
     * @return possible object is {@link InformationEntry }
     * 
     */
    public InformationEntry getVisaInfo() {
      return visaInfo;
    }

    /**
     * Define el valor de la propiedad visaInfo.
     * 
     * @param value allowed object is {@link InformationEntry }
     * 
     */
    public void setVisaInfo(InformationEntry value) {
      this.visaInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad insuranceInfo.
     * 
     * @return possible object is {@link InformationEntry }
     * 
     */
    public InformationEntry getInsuranceInfo() {
      return insuranceInfo;
    }

    /**
     * Define el valor de la propiedad insuranceInfo.
     * 
     * @param value allowed object is {@link InformationEntry }
     * 
     */
    public void setInsuranceInfo(InformationEntry value) {
      this.insuranceInfo = value;
    }

    /**
     * Gets the value of the additionalInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the additionalInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdditionalInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link FactsheetResponse.Factsheet.AdditionalInfo }
     * 
     * 
     */
    public List<FactsheetResponse.Factsheet.AdditionalInfo> getAdditionalInfo() {
      if (additionalInfo == null) {
        additionalInfo = new ArrayList<FactsheetResponse.Factsheet.AdditionalInfo>();
      }
      return this.additionalInfo;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="type"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *               &lt;enumeration value="infrastructure"/&gt;
         *               &lt;enumeration value="service"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="information" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"type", "name", "description", "information"})
    public static class Accessibility implements Serializable {

      @XmlElement(required = true)
      protected String type;
      @XmlElement(required = true)
      protected String name;
      protected String description;
      @XmlElement(required = true)
      protected InformationEntry information;

      /**
       * Obtiene el valor de la propiedad type.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getType() {
        return type;
      }

      /**
       * Define el valor de la propiedad type.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setType(String value) {
        this.type = value;
      }

      /**
       * Obtiene el valor de la propiedad name.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getName() {
        return name;
      }

      /**
       * Define el valor de la propiedad name.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setName(String value) {
        this.name = value;
      }

      /**
       * Obtiene el valor de la propiedad description.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getDescription() {
        return description;
      }

      /**
       * Define el valor de la propiedad description.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setDescription(String value) {
        this.description = value;
      }

      /**
       * Obtiene el valor de la propiedad information.
       * 
       * @return possible object is {@link InformationEntry }
       * 
       */
      public InformationEntry getInformation() {
        return information;
      }

      /**
       * Define el valor de la propiedad information.
       * 
       * @param value allowed object is {@link InformationEntry }
       * 
       */
      public void setInformation(InformationEntry value) {
        this.information = value;
      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="info" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}InformationEntry"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"type", "info"})
    public static class AdditionalInfo implements Serializable {

      @XmlElement(required = true)
      protected String type;
      @XmlElement(required = true)
      protected InformationEntry info;

      /**
       * Obtiene el valor de la propiedad type.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getType() {
        return type;
      }

      /**
       * Define el valor de la propiedad type.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setType(String value) {
        this.type = value;
      }

      /**
       * Obtiene el valor de la propiedad info.
       * 
       * @return possible object is {@link InformationEntry }
       * 
       */
      public InformationEntry getInfo() {
        return info;
      }

      /**
       * Define el valor de la propiedad info.
       * 
       * @param value allowed object is {@link InformationEntry }
       * 
       */
      public void setInfo(InformationEntry value) {
        this.info = value;
      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="details" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="information-website" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"name", "details", "informationWebsite"})
    public static class AdditionalRequirement implements Serializable {

      @XmlElement(required = true)
      protected String name;
      @XmlElement(required = true)
      protected String details;
      @XmlElement(name = "information-website")
      protected List<HTTPWithOptionalLang> informationWebsite;

      /**
       * Obtiene el valor de la propiedad name.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getName() {
        return name;
      }

      /**
       * Define el valor de la propiedad name.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setName(String value) {
        this.name = value;
      }

      /**
       * Obtiene el valor de la propiedad details.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getDetails() {
        return details;
      }

      /**
       * Define el valor de la propiedad details.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setDetails(String value) {
        this.details = value;
      }

      /**
       * Gets the value of the informationWebsite property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the informationWebsite property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getInformationWebsite().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
       * 
       * 
       */
      public List<HTTPWithOptionalLang> getInformationWebsite() {
        if (informationWebsite == null) {
          informationWebsite = new ArrayList<HTTPWithOptionalLang>();
        }
        return this.informationWebsite;
      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="student-nominations" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
         *         &lt;element name="student-applications" type="{https://github.com/erasmus-without-paper/ewp-specs-api-factsheet/tree/stable-v1}CalendarEntry"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"studentNominations", "studentApplications"})
    public static class Calendar implements Serializable {

      @XmlElement(name = "student-nominations", required = true)
      protected CalendarEntry studentNominations;
      @XmlElement(name = "student-applications", required = true)
      protected CalendarEntry studentApplications;

      /**
       * Obtiene el valor de la propiedad studentNominations.
       * 
       * @return possible object is {@link CalendarEntry }
       * 
       */
      public CalendarEntry getStudentNominations() {
        return studentNominations;
      }

      /**
       * Define el valor de la propiedad studentNominations.
       * 
       * @param value allowed object is {@link CalendarEntry }
       * 
       */
      public void setStudentNominations(CalendarEntry value) {
        this.studentNominations = value;
      }

      /**
       * Obtiene el valor de la propiedad studentApplications.
       * 
       * @return possible object is {@link CalendarEntry }
       * 
       */
      public CalendarEntry getStudentApplications() {
        return studentApplications;
      }

      /**
       * Define el valor de la propiedad studentApplications.
       * 
       * @param value allowed object is {@link CalendarEntry }
       * 
       */
      public void setStudentApplications(CalendarEntry value) {
        this.studentApplications = value;
      }

    }

  }

}
