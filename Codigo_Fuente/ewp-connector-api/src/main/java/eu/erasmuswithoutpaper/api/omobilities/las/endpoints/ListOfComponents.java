//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;


/**
 * Describes a list of components that make up various tables in LA.
 *
 *
 * <p>
 * Clase Java para ListOf_Components complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="ListOf_Components"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="components-studied" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ComponentList" minOccurs="0"/&gt;
 *         &lt;element name="components-recognized" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ComponentList" minOccurs="0"/&gt;
 *         &lt;element name="virtual-components" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ComponentList" minOccurs="0"/&gt;
 *         &lt;element name="blended-mobility-components" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ComponentList" minOccurs="0"/&gt;
 *         &lt;element name="short-term-doctoral-components" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ComponentList" minOccurs="0"/&gt;
 *         &lt;element name="student-signature" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}Signature"/&gt;
 *         &lt;element name="sending-hei-signature" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}Signature"/&gt;
 *         &lt;element name="receiving-hei-signature" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}Signature" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOf_Components",
    propOrder = {"componentsStudied", "componentsRecognized", "virtualComponents", "blendedMobilityComponents",
        "shortTermDoctoralComponents", "studentSignature", "sendingHeiSignature", "receivingHeiSignature"})
@XmlSeeAlso({eu.erasmuswithoutpaper.api.omobilities.las.endpoints.LearningAgreement.ChangesProposal.class})
public class ListOfComponents implements Serializable {

  @XmlElement(name = "components-studied")
  protected ComponentList componentsStudied;
  @XmlElement(name = "components-recognized")
  protected ComponentList componentsRecognized;
  @XmlElement(name = "virtual-components")
  protected ComponentList virtualComponents;
  @XmlElement(name = "blended-mobility-components")
  protected ComponentList blendedMobilityComponents;
  @XmlElement(name = "short-term-doctoral-components")
  protected ComponentList shortTermDoctoralComponents;
  @XmlElement(name = "student-signature", required = true)
  protected Signature studentSignature;
  @XmlElement(name = "sending-hei-signature", required = true)
  protected Signature sendingHeiSignature;
  @XmlElement(name = "receiving-hei-signature")
  protected Signature receivingHeiSignature;

  /**
   * Obtiene el valor de la propiedad componentsStudied.
   * 
   * @return possible object is {@link ComponentList }
   * 
   */
  public ComponentList getComponentsStudied() {
    return componentsStudied;
  }

  /**
   * Define el valor de la propiedad componentsStudied.
   * 
   * @param value allowed object is {@link ComponentList }
   * 
   */
  public void setComponentsStudied(ComponentList value) {
    this.componentsStudied = value;
  }

  /**
   * Obtiene el valor de la propiedad componentsRecognized.
   * 
   * @return possible object is {@link ComponentList }
   * 
   */
  public ComponentList getComponentsRecognized() {
    return componentsRecognized;
  }

  /**
   * Define el valor de la propiedad componentsRecognized.
   * 
   * @param value allowed object is {@link ComponentList }
   * 
   */
  public void setComponentsRecognized(ComponentList value) {
    this.componentsRecognized = value;
  }

  /**
   * Obtiene el valor de la propiedad virtualComponents.
   * 
   * @return possible object is {@link ComponentList }
   * 
   */
  public ComponentList getVirtualComponents() {
    return virtualComponents;
  }

  /**
   * Define el valor de la propiedad virtualComponents.
   * 
   * @param value allowed object is {@link ComponentList }
   * 
   */
  public void setVirtualComponents(ComponentList value) {
    this.virtualComponents = value;
  }

  /**
   * Obtiene el valor de la propiedad blendedMobilityComponents.
   * 
   * @return possible object is {@link ComponentList }
   * 
   */
  public ComponentList getBlendedMobilityComponents() {
    return blendedMobilityComponents;
  }

  /**
   * Define el valor de la propiedad blendedMobilityComponents.
   * 
   * @param value allowed object is {@link ComponentList }
   * 
   */
  public void setBlendedMobilityComponents(ComponentList value) {
    this.blendedMobilityComponents = value;
  }

  /**
   * Obtiene el valor de la propiedad shortTermDoctoralComponents.
   * 
   * @return possible object is {@link ComponentList }
   * 
   */
  public ComponentList getShortTermDoctoralComponents() {
    return shortTermDoctoralComponents;
  }

  /**
   * Define el valor de la propiedad shortTermDoctoralComponents.
   * 
   * @param value allowed object is {@link ComponentList }
   * 
   */
  public void setShortTermDoctoralComponents(ComponentList value) {
    this.shortTermDoctoralComponents = value;
  }

  /**
   * Obtiene el valor de la propiedad studentSignature.
   * 
   * @return possible object is {@link Signature }
   * 
   */
  public Signature getStudentSignature() {
    return studentSignature;
  }

  /**
   * Define el valor de la propiedad studentSignature.
   * 
   * @param value allowed object is {@link Signature }
   * 
   */
  public void setStudentSignature(Signature value) {
    this.studentSignature = value;
  }

  /**
   * Obtiene el valor de la propiedad sendingHeiSignature.
   * 
   * @return possible object is {@link Signature }
   * 
   */
  public Signature getSendingHeiSignature() {
    return sendingHeiSignature;
  }

  /**
   * Define el valor de la propiedad sendingHeiSignature.
   * 
   * @param value allowed object is {@link Signature }
   * 
   */
  public void setSendingHeiSignature(Signature value) {
    this.sendingHeiSignature = value;
  }

  /**
   * Obtiene el valor de la propiedad receivingHeiSignature.
   * 
   * @return possible object is {@link Signature }
   * 
   */
  public Signature getReceivingHeiSignature() {
    return receivingHeiSignature;
  }

  /**
   * Define el valor de la propiedad receivingHeiSignature.
   * 
   * @param value allowed object is {@link Signature }
   * 
   */
  public void setReceivingHeiSignature(Signature value) {
    this.receivingHeiSignature = value;
  }

}
