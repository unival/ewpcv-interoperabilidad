//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.registry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.MultilineString;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="host" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}admin-email" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}admin-notes" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}apis-implemented" minOccurs="0"/&gt;
 *                   &lt;element name="institutions-covered" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="client-credentials-in-use" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="certificate" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="server-credentials-in-use" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="institutions"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}hei" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="binaries" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;base64Binary"&gt;
 *                           &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"host", "institutions", "binaries"})
@XmlRootElement(name = "catalogue")
public class Catalogue implements Serializable {

  @XmlElement(required = true)
  protected List<Catalogue.Host> host;
  @XmlElement(required = true)
  protected Catalogue.Institutions institutions;
  protected Catalogue.Binaries binaries;

  /**
   * Gets the value of the host property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the host property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getHost().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link Catalogue.Host }
   * 
   * 
   */
  public List<Catalogue.Host> getHost() {
    if (host == null) {
      host = new ArrayList<Catalogue.Host>();
    }
    return this.host;
  }

  /**
   * Obtiene el valor de la propiedad institutions.
   * 
   * @return possible object is {@link Catalogue.Institutions }
   * 
   */
  public Catalogue.Institutions getInstitutions() {
    return institutions;
  }

  /**
   * Define el valor de la propiedad institutions.
   * 
   * @param value allowed object is {@link Catalogue.Institutions }
   * 
   */
  public void setInstitutions(Catalogue.Institutions value) {
    this.institutions = value;
  }

  /**
   * Obtiene el valor de la propiedad binaries.
   * 
   * @return possible object is {@link Catalogue.Binaries }
   * 
   */
  public Catalogue.Binaries getBinaries() {
    return binaries;
  }

  /**
   * Define el valor de la propiedad binaries.
   * 
   * @param value allowed object is {@link Catalogue.Binaries }
   * 
   */
  public void setBinaries(Catalogue.Binaries value) {
    this.binaries = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;base64Binary"&gt;
     *                 &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"rsaPublicKey"})
  public static class Binaries implements Serializable {

    @XmlElement(name = "rsa-public-key")
    protected List<Catalogue.Binaries.RsaPublicKey> rsaPublicKey;

    /**
     * Gets the value of the rsaPublicKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the rsaPublicKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getRsaPublicKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Catalogue.Binaries.RsaPublicKey }
     * 
     * 
     */
    public List<Catalogue.Binaries.RsaPublicKey> getRsaPublicKey() {
      if (rsaPublicKey == null) {
        rsaPublicKey = new ArrayList<Catalogue.Binaries.RsaPublicKey>();
      }
      return this.rsaPublicKey;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;base64Binary"&gt;
         *       &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    public static class RsaPublicKey implements Serializable {

      @XmlValue
      protected byte[] value;
      @XmlAttribute(name = "sha-256", required = true)
      protected String sha256;

      /**
       * Obtiene el valor de la propiedad value.
       * 
       * @return possible object is byte[]
       */
      public byte[] getValue() {
        return value;
      }

      /**
       * Define el valor de la propiedad value.
       * 
       * @param value allowed object is byte[]
       */
      public void setValue(byte[] value) {
        this.value = value;
      }

      /**
       * Obtiene el valor de la propiedad sha256.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getSha256() {
        return sha256;
      }

      /**
       * Define el valor de la propiedad sha256.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setSha256(String value) {
        this.sha256 = value;
      }

    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}admin-email" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}admin-notes" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}apis-implemented" minOccurs="0"/&gt;
     *         &lt;element name="institutions-covered" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="client-credentials-in-use" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="certificate" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="server-credentials-in-use" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"adminEmail", "adminNotes", "apisImplemented", "institutionsCovered",
      "clientCredentialsInUse", "serverCredentialsInUse"})
  public static class Host implements Serializable {

    @XmlElement(name = "admin-email",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd")
    protected List<String> adminEmail;
    @XmlElement(name = "admin-notes",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd")
    protected MultilineString adminNotes;
    @XmlElement(name = "apis-implemented")
    protected ApisImplemented apisImplemented;
    @XmlElement(name = "institutions-covered")
    protected Catalogue.Host.InstitutionsCovered institutionsCovered;
    @XmlElement(name = "client-credentials-in-use")
    protected Catalogue.Host.ClientCredentialsInUse clientCredentialsInUse;
    @XmlElement(name = "server-credentials-in-use")
    protected Catalogue.Host.ServerCredentialsInUse serverCredentialsInUse;

    /**
     * Gets the value of the adminEmail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the adminEmail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getAdminEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     * 
     * 
     */
    public List<String> getAdminEmail() {
      if (adminEmail == null) {
        adminEmail = new ArrayList<String>();
      }
      return this.adminEmail;
    }

    /**
     * Obtiene el valor de la propiedad adminNotes.
     * 
     * @return possible object is {@link MultilineString }
     * 
     */
    public MultilineString getAdminNotes() {
      return adminNotes;
    }

    /**
     * Define el valor de la propiedad adminNotes.
     * 
     * @param value allowed object is {@link MultilineString }
     * 
     */
    public void setAdminNotes(MultilineString value) {
      this.adminNotes = value;
    }

    /**
     * Obtiene el valor de la propiedad apisImplemented.
     * 
     * @return possible object is {@link ApisImplemented }
     * 
     */
    public ApisImplemented getApisImplemented() {
      return apisImplemented;
    }

    /**
     * Define el valor de la propiedad apisImplemented.
     * 
     * @param value allowed object is {@link ApisImplemented }
     * 
     */
    public void setApisImplemented(ApisImplemented value) {
      this.apisImplemented = value;
    }

    /**
     * Obtiene el valor de la propiedad institutionsCovered.
     * 
     * @return possible object is {@link Catalogue.Host.InstitutionsCovered }
     * 
     */
    public Catalogue.Host.InstitutionsCovered getInstitutionsCovered() {
      return institutionsCovered;
    }

    /**
     * Define el valor de la propiedad institutionsCovered.
     * 
     * @param value allowed object is {@link Catalogue.Host.InstitutionsCovered }
     * 
     */
    public void setInstitutionsCovered(Catalogue.Host.InstitutionsCovered value) {
      this.institutionsCovered = value;
    }

    /**
     * Obtiene el valor de la propiedad clientCredentialsInUse.
     * 
     * @return possible object is {@link Catalogue.Host.ClientCredentialsInUse }
     * 
     */
    public Catalogue.Host.ClientCredentialsInUse getClientCredentialsInUse() {
      return clientCredentialsInUse;
    }

    /**
     * Define el valor de la propiedad clientCredentialsInUse.
     * 
     * @param value allowed object is {@link Catalogue.Host.ClientCredentialsInUse }
     * 
     */
    public void setClientCredentialsInUse(Catalogue.Host.ClientCredentialsInUse value) {
      this.clientCredentialsInUse = value;
    }

    /**
     * Obtiene el valor de la propiedad serverCredentialsInUse.
     * 
     * @return possible object is {@link Catalogue.Host.ServerCredentialsInUse }
     * 
     */
    public Catalogue.Host.ServerCredentialsInUse getServerCredentialsInUse() {
      return serverCredentialsInUse;
    }

    /**
     * Define el valor de la propiedad serverCredentialsInUse.
     * 
     * @param value allowed object is {@link Catalogue.Host.ServerCredentialsInUse }
     * 
     */
    public void setServerCredentialsInUse(Catalogue.Host.ServerCredentialsInUse value) {
      this.serverCredentialsInUse = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="certificate" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"certificate", "rsaPublicKey"})
    public static class ClientCredentialsInUse implements Serializable {

      protected List<Catalogue.Host.ClientCredentialsInUse.Certificate> certificate;
      @XmlElement(name = "rsa-public-key")
      protected List<Catalogue.Host.ClientCredentialsInUse.RsaPublicKey> rsaPublicKey;

      /**
       * Gets the value of the certificate property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the certificate property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getCertificate().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link Catalogue.Host.ClientCredentialsInUse.Certificate }
       * 
       * 
       */
      public List<Catalogue.Host.ClientCredentialsInUse.Certificate> getCertificate() {
        if (certificate == null) {
          certificate = new ArrayList<Catalogue.Host.ClientCredentialsInUse.Certificate>();
        }
        return this.certificate;
      }

      /**
       * Gets the value of the rsaPublicKey property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the rsaPublicKey property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getRsaPublicKey().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link Catalogue.Host.ClientCredentialsInUse.RsaPublicKey }
       * 
       * 
       */
      public List<Catalogue.Host.ClientCredentialsInUse.RsaPublicKey> getRsaPublicKey() {
        if (rsaPublicKey == null) {
          rsaPublicKey = new ArrayList<Catalogue.Host.ClientCredentialsInUse.RsaPublicKey>();
        }
        return this.rsaPublicKey;
      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "")
      public static class Certificate implements Serializable {

        @XmlAttribute(name = "sha-256", required = true)
        protected String sha256;

        /**
         * Obtiene el valor de la propiedad sha256.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getSha256() {
          return sha256;
        }

        /**
         * Define el valor de la propiedad sha256.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setSha256(String value) {
          this.sha256 = value;
        }

      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "")
      public static class RsaPublicKey implements Serializable {

        @XmlAttribute(name = "sha-256", required = true)
        protected String sha256;

        /**
         * Obtiene el valor de la propiedad sha256.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getSha256() {
          return sha256;
        }

        /**
         * Define el valor de la propiedad sha256.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setSha256(String value) {
          this.sha256 = value;
        }

      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"heiId"})
    public static class InstitutionsCovered implements Serializable {

      @XmlElement(name = "hei-id")
      protected List<String> heiId;

      /**
       * Gets the value of the heiId property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the heiId property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getHeiId().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list {@link String }
       * 
       * 
       */
      public List<String> getHeiId() {
        if (heiId == null) {
          heiId = new ArrayList<String>();
        }
        return this.heiId;
      }

    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="rsa-public-key" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"rsaPublicKey"})
    public static class ServerCredentialsInUse implements Serializable {

      @XmlElement(name = "rsa-public-key")
      protected List<Catalogue.Host.ServerCredentialsInUse.RsaPublicKey> rsaPublicKey;

      /**
       * Gets the value of the rsaPublicKey property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the rsaPublicKey property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getRsaPublicKey().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link Catalogue.Host.ServerCredentialsInUse.RsaPublicKey }
       * 
       * 
       */
      public List<Catalogue.Host.ServerCredentialsInUse.RsaPublicKey> getRsaPublicKey() {
        if (rsaPublicKey == null) {
          rsaPublicKey = new ArrayList<Catalogue.Host.ServerCredentialsInUse.RsaPublicKey>();
        }
        return this.rsaPublicKey;
      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;attribute name="sha-256" use="required" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "")
      public static class RsaPublicKey implements Serializable {

        @XmlAttribute(name = "sha-256", required = true)
        protected String sha256;

        /**
         * Obtiene el valor de la propiedad sha256.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getSha256() {
          return sha256;
        }

        /**
         * Define el valor de la propiedad sha256.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setSha256(String value) {
          this.sha256 = value;
        }

      }

    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}hei" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"hei"})
  public static class Institutions implements Serializable {

    protected List<Hei> hei;

    /**
     * Gets the value of the hei property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the hei property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getHei().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Hei }
     * 
     * 
     */
    public List<Hei> getHei() {
      if (hei == null) {
        hei = new ArrayList<Hei>();
      }
      return this.hei;
    }

  }

}
