//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sending-hei-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd}approve-proposal-v1"/&gt;
 *           &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd}comment-proposal-v1"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"sendingHeiId", "approveProposalV1", "commentProposalV1"})
@XmlRootElement(name = "omobility-las-update-request",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd")
public class OmobilityLasUpdateRequest implements Serializable {

  @XmlElement(name = "sending-hei-id",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd",
      required = true)
  protected String sendingHeiId;
  @XmlElement(name = "approve-proposal-v1",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd")
  protected ApproveProposalV1 approveProposalV1;
  @XmlElement(name = "comment-proposal-v1",
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/update-request.xsd")
  protected CommentProposalV1 commentProposalV1;

  /**
   * Obtiene el valor de la propiedad sendingHeiId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSendingHeiId() {
    return sendingHeiId;
  }

  /**
   * Define el valor de la propiedad sendingHeiId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSendingHeiId(String value) {
    this.sendingHeiId = value;
  }

  /**
   * Obtiene el valor de la propiedad approveProposalV1.
   * 
   * @return possible object is {@link ApproveProposalV1 }
   * 
   */
  public ApproveProposalV1 getApproveProposalV1() {
    return approveProposalV1;
  }

  /**
   * Define el valor de la propiedad approveProposalV1.
   * 
   * @param value allowed object is {@link ApproveProposalV1 }
   * 
   */
  public void setApproveProposalV1(ApproveProposalV1 value) {
    this.approveProposalV1 = value;
  }

  /**
   * Obtiene el valor de la propiedad commentProposalV1.
   * 
   * @return possible object is {@link CommentProposalV1 }
   * 
   */
  public CommentProposalV1 getCommentProposalV1() {
    return commentProposalV1;
  }

  /**
   * Define el valor de la propiedad commentProposalV1.
   * 
   * @param value allowed object is {@link CommentProposalV1 }
   * 
   */
  public void setCommentProposalV1(CommentProposalV1 value) {
    this.commentProposalV1 = value;
  }

}
