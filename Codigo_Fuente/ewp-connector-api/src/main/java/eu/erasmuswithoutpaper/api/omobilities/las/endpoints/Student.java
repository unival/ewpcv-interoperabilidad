//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;
import java.math.BigInteger;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="given-names" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="family-name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="global-id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="birth-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="citizenship" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CountryCode" minOccurs="0"/&gt;
 *         &lt;element name="gender" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Gender" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"givenNames", "familyName", "globalId", "birthDate", "citizenship", "gender", "email"})
@XmlRootElement(name = "student")
public class Student implements Serializable {

  @XmlElement(name = "given-names")
  protected String givenNames;
  @XmlElement(name = "family-name")
  protected String familyName;
  @XmlElement(name = "global-id")
  protected String globalId;
  @XmlElement(name = "birth-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar birthDate;
  protected String citizenship;
  protected BigInteger gender;
  protected String email;

  /**
   * Obtiene el valor de la propiedad givenNames.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getGivenNames() {
    return givenNames;
  }

  /**
   * Define el valor de la propiedad givenNames.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setGivenNames(String value) {
    this.givenNames = value;
  }

  /**
   * Obtiene el valor de la propiedad familyName.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getFamilyName() {
    return familyName;
  }

  /**
   * Define el valor de la propiedad familyName.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setFamilyName(String value) {
    this.familyName = value;
  }

  /**
   * Obtiene el valor de la propiedad globalId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getGlobalId() {
    return globalId;
  }

  /**
   * Define el valor de la propiedad globalId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setGlobalId(String value) {
    this.globalId = value;
  }

  /**
   * Obtiene el valor de la propiedad birthDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getBirthDate() {
    return birthDate;
  }

  /**
   * Define el valor de la propiedad birthDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setBirthDate(XMLGregorianCalendar value) {
    this.birthDate = value;
  }

  /**
   * Obtiene el valor de la propiedad citizenship.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getCitizenship() {
    return citizenship;
  }

  /**
   * Define el valor de la propiedad citizenship.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setCitizenship(String value) {
    this.citizenship = value;
  }

  /**
   * Obtiene el valor de la propiedad gender.
   * 
   * @return possible object is {@link BigInteger }
   * 
   */
  public BigInteger getGender() {
    return gender;
  }

  /**
   * Define el valor de la propiedad gender.
   * 
   * @param value allowed object is {@link BigInteger }
   * 
   */
  public void setGender(BigInteger value) {
    this.gender = value;
  }

  /**
   * Obtiene el valor de la propiedad email.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getEmail() {
    return email;
  }

  /**
   * Define el valor de la propiedad email.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setEmail(String value) {
    this.email = value;
  }

}
