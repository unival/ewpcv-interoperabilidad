//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.approval;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="approval" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *                   &lt;element name="iia-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
 *                   &lt;element name="pdf-file" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "approval"
})
@XmlRootElement(name = "iias-approval-response")
public class IiasApprovalResponse {

    protected List<Approval> approval;

    /**
     * Gets the value of the approval property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the approval property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApproval().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Approval }
     * 
     * 
     */
    public List<Approval> getApproval() {
        if (approval == null) {
            approval = new ArrayList<Approval>();
        }
        return this.approval;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
     *         &lt;element name="iia-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
     *         &lt;element name="pdf-file" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "iiaId",
        "iiaHash",
        "pdfFile"
    })
    public static class Approval {

        @XmlElement(name = "iia-id", required = true)
        protected String iiaId;
        @XmlElement(name = "iia-hash", required = true)
        protected String iiaHash;
        @XmlElement(name = "pdf-file")
        protected String pdfFile;

        /**
         * Obtiene el valor de la propiedad iiaId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIiaId() {
            return iiaId;
        }

        /**
         * Define el valor de la propiedad iiaId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIiaId(String value) {
            this.iiaId = value;
        }

        /**
         * Obtiene el valor de la propiedad iiaHash.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIiaHash() {
            return iiaHash;
        }

        /**
         * Define el valor de la propiedad iiaHash.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIiaHash(String value) {
            this.iiaHash = value;
        }

        /**
         * Obtiene el valor de la propiedad pdfFile.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPdfFile() {
            return pdfFile;
        }

        /**
         * Define el valor de la propiedad pdfFile.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPdfFile(String value) {
            this.pdfFile = value;
        }

    }

}
