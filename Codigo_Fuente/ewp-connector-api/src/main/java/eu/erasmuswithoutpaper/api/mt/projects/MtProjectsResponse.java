//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.mt.projects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="project" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="agreement-number" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="start-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                   &lt;element name="end-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                   &lt;element name="action-type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"project"})
@XmlRootElement(name = "mt-projects-response")
public class MtProjectsResponse implements Serializable {

  protected List<MtProjectsResponse.Project> project;

  /**
   * Gets the value of the project property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the project property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getProject().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link MtProjectsResponse.Project }
   * 
   * 
   */
  public List<MtProjectsResponse.Project> getProject() {
    if (project == null) {
      project = new ArrayList<MtProjectsResponse.Project>();
    }
    return this.project;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="agreement-number" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="start-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *         &lt;element name="end-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *         &lt;element name="action-type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"agreementNumber", "startDate", "endDate", "actionType"})
  public static class Project implements Serializable {

    @XmlElement(name = "agreement-number", required = true)
    protected String agreementNumber;
    @XmlElement(name = "start-date", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlElement(name = "end-date", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    @XmlElement(name = "action-type", required = true)
    protected String actionType;

    /**
     * Obtiene el valor de la propiedad agreementNumber.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAgreementNumber() {
      return agreementNumber;
    }

    /**
     * Define el valor de la propiedad agreementNumber.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAgreementNumber(String value) {
      this.agreementNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad startDate.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getStartDate() {
      return startDate;
    }

    /**
     * Define el valor de la propiedad startDate.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setStartDate(XMLGregorianCalendar value) {
      this.startDate = value;
    }

    /**
     * Obtiene el valor de la propiedad endDate.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getEndDate() {
      return endDate;
    }

    /**
     * Define el valor de la propiedad endDate.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setEndDate(XMLGregorianCalendar value) {
      this.endDate = value;
    }

    /**
     * Obtiene el valor de la propiedad actionType.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getActionType() {
      return actionType;
    }

    /**
     * Define el valor de la propiedad actionType.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setActionType(String value) {
      this.actionType = value;
    }

  }

}
