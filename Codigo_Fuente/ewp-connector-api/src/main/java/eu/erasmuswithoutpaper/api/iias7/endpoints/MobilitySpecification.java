//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import eu.erasmuswithoutpaper.api.types.contact.Contact;


/**
 * 
 *                 A common parent class for all mobility specifications.
 * 
 *                 Note that "mobility specification" is a name we (EWP designers) made up. We
 *                 needed to have a descriptive name for this particular entity, and we couldn't
 *                 find such name in existing Erasmus+ templates. In other words, this is an
 *                 EWP-specific term only, and you should probably avoid using it in other
 *                 contexts.
 * 
 *                 Mobility specification may also be considered as a subclass of "cooperation
 *                 condition", but - since currently all nonabstract subclasses inherit from
 *                 MobilitySpecification - no CooperationCondition class is introduced. See here:
 *                 https://github.com/erasmus-without-paper/ewp-wp3-data-model/issues/10
 * 
 *                 Each specification has a sending and receiving partner. Each specification
 *                 represents an agreement that, for each of the academic years listed within, the
 *                 sending partner will send a particular number of people to the receiving
 *                 partner, for a particular average duration each (e.g. for 8 weeks). This
 *                 describes an unidirectional flow - if people are sent in both directions, two
 *                 separate mobility specifications need to be defined (one for each direction).
 * 
 *                 More requirements (e.g. *who* is being sent to do *what*) are defined in
 *                 specific `*-mobility-spec` subclasses.
 * 
 *                 This type, nor any of its subclasses, SHOULD NOT be referenced outside of the
 *                 EWP IIAs API. It is likely to be modified, or to be removed, in the future.
 *             
 * 
 * <p>Clase Java para MobilitySpecification complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MobilitySpecification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}MobilityMain"/&gt;
 *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}recommended-language-skill" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;group ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}MobilityAdditional"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobilitySpecification", propOrder = {
    "sendingHeiId",
    "sendingOunitId",
    "sendingContact",
    "receivingHeiId",
    "receivingOunitId",
    "receivingContact",
    "receivingFirstAcademicYearId",
    "receivingLastAcademicYearId",
    "mobilitiesPerYear",
    "recommendedLanguageSkill",
    "subjectArea",
    "otherInfoTerms"
})
@XmlSeeAlso({
    StaffMobilitySpecification.class,
    StudentMobilitySpecification.class
})
public abstract class MobilitySpecification {

    @XmlElement(name = "sending-hei-id", required = true)
    protected String sendingHeiId;
    @XmlElement(name = "sending-ounit-id")
    protected String sendingOunitId;
    @XmlElement(name = "sending-contact")
    protected List<Contact> sendingContact;
    @XmlElement(name = "receiving-hei-id", required = true)
    protected String receivingHeiId;
    @XmlElement(name = "receiving-ounit-id")
    protected String receivingOunitId;
    @XmlElement(name = "receiving-contact")
    protected List<Contact> receivingContact;
    @XmlElement(name = "receiving-first-academic-year-id", required = true)
    protected String receivingFirstAcademicYearId;
    @XmlElement(name = "receiving-last-academic-year-id", required = true)
    protected String receivingLastAcademicYearId;
    @XmlElement(name = "mobilities-per-year", required = true)
    protected MobilitiesPerYear mobilitiesPerYear;
    @XmlElement(name = "recommended-language-skill")
    protected List<RecommendedLanguageSkill> recommendedLanguageSkill;
    @XmlElement(name = "subject-area")
    protected List<SubjectArea> subjectArea;
    @XmlElement(name = "other-info-terms")
    protected String otherInfoTerms;

    /**
     * Obtiene el valor de la propiedad sendingHeiId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendingHeiId() {
        return sendingHeiId;
    }

    /**
     * Define el valor de la propiedad sendingHeiId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendingHeiId(String value) {
        this.sendingHeiId = value;
    }

    /**
     * Obtiene el valor de la propiedad sendingOunitId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendingOunitId() {
        return sendingOunitId;
    }

    /**
     * Define el valor de la propiedad sendingOunitId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendingOunitId(String value) {
        this.sendingOunitId = value;
    }

    /**
     * Gets the value of the sendingContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sendingContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSendingContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contact }
     * 
     * 
     */
    public List<Contact> getSendingContact() {
        if (sendingContact == null) {
            sendingContact = new ArrayList<Contact>();
        }
        return this.sendingContact;
    }

    /**
     * Obtiene el valor de la propiedad receivingHeiId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingHeiId() {
        return receivingHeiId;
    }

    /**
     * Define el valor de la propiedad receivingHeiId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingHeiId(String value) {
        this.receivingHeiId = value;
    }

    /**
     * Obtiene el valor de la propiedad receivingOunitId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingOunitId() {
        return receivingOunitId;
    }

    /**
     * Define el valor de la propiedad receivingOunitId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingOunitId(String value) {
        this.receivingOunitId = value;
    }

    /**
     * Gets the value of the receivingContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receivingContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceivingContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Contact }
     * 
     * 
     */
    public List<Contact> getReceivingContact() {
        if (receivingContact == null) {
            receivingContact = new ArrayList<Contact>();
        }
        return this.receivingContact;
    }

    /**
     * Obtiene el valor de la propiedad receivingFirstAcademicYearId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingFirstAcademicYearId() {
        return receivingFirstAcademicYearId;
    }

    /**
     * Define el valor de la propiedad receivingFirstAcademicYearId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingFirstAcademicYearId(String value) {
        this.receivingFirstAcademicYearId = value;
    }

    /**
     * Obtiene el valor de la propiedad receivingLastAcademicYearId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivingLastAcademicYearId() {
        return receivingLastAcademicYearId;
    }

    /**
     * Define el valor de la propiedad receivingLastAcademicYearId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivingLastAcademicYearId(String value) {
        this.receivingLastAcademicYearId = value;
    }

    /**
     * Obtiene el valor de la propiedad mobilitiesPerYear.
     * 
     * @return
     *     possible object is
     *     {@link MobilitiesPerYear }
     *     
     */
    public MobilitiesPerYear getMobilitiesPerYear() {
        return mobilitiesPerYear;
    }

    /**
     * Define el valor de la propiedad mobilitiesPerYear.
     * 
     * @param value
     *     allowed object is
     *     {@link MobilitiesPerYear }
     *     
     */
    public void setMobilitiesPerYear(MobilitiesPerYear value) {
        this.mobilitiesPerYear = value;
    }

    /**
     * Gets the value of the recommendedLanguageSkill property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recommendedLanguageSkill property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecommendedLanguageSkill().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecommendedLanguageSkill }
     * 
     * 
     */
    public List<RecommendedLanguageSkill> getRecommendedLanguageSkill() {
        if (recommendedLanguageSkill == null) {
            recommendedLanguageSkill = new ArrayList<RecommendedLanguageSkill>();
        }
        return this.recommendedLanguageSkill;
    }

    /**
     * Gets the value of the subjectArea property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subjectArea property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubjectArea().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubjectArea }
     * 
     * 
     */
    public List<SubjectArea> getSubjectArea() {
        if (subjectArea == null) {
            subjectArea = new ArrayList<SubjectArea>();
        }
        return this.subjectArea;
    }

    /**
     * Obtiene el valor de la propiedad otherInfoTerms.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherInfoTerms() {
        return otherInfoTerms;
    }

    /**
     * Define el valor de la propiedad otherInfoTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherInfoTerms(String value) {
        this.otherInfoTerms = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;positiveInteger"&gt;
     *       &lt;attGroup ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}not-yet-defined"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class MobilitiesPerYear {

        @XmlValue
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger value;
        @XmlAttribute(name = "not-yet-defined")
        protected Boolean notYetDefined;

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setValue(BigInteger value) {
            this.value = value;
        }

        /**
         * Obtiene el valor de la propiedad notYetDefined.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNotYetDefined() {
            return notYetDefined;
        }

        /**
         * Define el valor de la propiedad notYetDefined.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNotYetDefined(Boolean value) {
            this.notYetDefined = value;
        }

    }

}
