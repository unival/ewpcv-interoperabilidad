//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.iias.approval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="approval" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *                   &lt;element name="conditions-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
 *                   &lt;element name="pdf" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"approval"})
@XmlRootElement(name = "iias-approval-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/tree/stable-v1")
public class IiasApprovalResponse implements Serializable {

  @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/tree/stable-v1")
  protected List<IiasApprovalResponse.Approval> approval;

  /**
   * Gets the value of the approval property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the approval property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getApproval().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link IiasApprovalResponse.Approval }
   * 
   * 
   */
  public List<IiasApprovalResponse.Approval> getApproval() {
    if (approval == null) {
      approval = new ArrayList<IiasApprovalResponse.Approval>();
    }
    return this.approval;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
     *         &lt;element name="conditions-hash" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Sha256Hex"/&gt;
     *         &lt;element name="pdf" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"iiaId", "conditionsHash", "pdf"})
  public static class Approval implements Serializable {

    @XmlElement(name = "iia-id",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/tree/stable-v1",
        required = true)
    protected String iiaId;
    @XmlElement(name = "conditions-hash",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/tree/stable-v1",
        required = true)
    protected String conditionsHash;
    @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-iias-approval/tree/stable-v1")
    protected byte[] pdf;

    /**
     * Obtiene el valor de la propiedad iiaId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIiaId() {
      return iiaId;
    }

    /**
     * Define el valor de la propiedad iiaId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIiaId(String value) {
      this.iiaId = value;
    }

    /**
     * Obtiene el valor de la propiedad conditionsHash.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getConditionsHash() {
      return conditionsHash;
    }

    /**
     * Define el valor de la propiedad conditionsHash.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setConditionsHash(String value) {
      this.conditionsHash = value;
    }

    /**
     * Obtiene el valor de la propiedad pdf.
     * 
     * @return possible object is byte[]
     */
    public byte[] getPdf() {
      return pdf;
    }

    /**
     * Define el valor de la propiedad pdf.
     * 
     * @param value allowed object is byte[]
     */
    public void setPdf(byte[] value) {
      this.pdf = value;
    }

  }

}
