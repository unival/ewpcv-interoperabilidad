//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.erasmuswithoutpaper.api.iias7.endpoints package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.erasmuswithoutpaper.api.iias7.endpoints
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IiasGetResponse }
     * 
     */
    public IiasGetResponse createIiasGetResponse() {
        return new IiasGetResponse();
    }

    /**
     * Create an instance of {@link SubjectArea }
     * 
     */
    public SubjectArea createSubjectArea() {
        return new SubjectArea();
    }

    /**
     * Create an instance of {@link IiasGetResponse.Iia }
     * 
     */
    public IiasGetResponse.Iia createIiasGetResponseIia() {
        return new IiasGetResponse.Iia();
    }

    /**
     * Create an instance of {@link RecommendedLanguageSkill }
     * 
     */
    public RecommendedLanguageSkill createRecommendedLanguageSkill() {
        return new RecommendedLanguageSkill();
    }

    /**
     * Create an instance of {@link StudentStudiesMobilitySpec }
     * 
     */
    public StudentStudiesMobilitySpec createStudentStudiesMobilitySpec() {
        return new StudentStudiesMobilitySpec();
    }

    /**
     * Create an instance of {@link MobilitySpecification.MobilitiesPerYear }
     * 
     */
    public MobilitySpecification.MobilitiesPerYear createMobilitySpecificationMobilitiesPerYear() {
        return new MobilitySpecification.MobilitiesPerYear();
    }

    /**
     * Create an instance of {@link StudentTraineeshipMobilitySpec }
     * 
     */
    public StudentTraineeshipMobilitySpec createStudentTraineeshipMobilitySpec() {
        return new StudentTraineeshipMobilitySpec();
    }

    /**
     * Create an instance of {@link StaffTeacherMobilitySpec }
     * 
     */
    public StaffTeacherMobilitySpec createStaffTeacherMobilitySpec() {
        return new StaffTeacherMobilitySpec();
    }

    /**
     * Create an instance of {@link StaffTrainingMobilitySpec }
     * 
     */
    public StaffTrainingMobilitySpec createStaffTrainingMobilitySpec() {
        return new StaffTrainingMobilitySpec();
    }

    /**
     * Create an instance of {@link IiasIndexResponse }
     * 
     */
    public IiasIndexResponse createIiasIndexResponse() {
        return new IiasIndexResponse();
    }

    /**
     * Create an instance of {@link SubjectArea.IscedFCode }
     * 
     */
    public SubjectArea.IscedFCode createSubjectAreaIscedFCode() {
        return new SubjectArea.IscedFCode();
    }

    /**
     * Create an instance of {@link IiasGetResponse.Iia.Partner }
     * 
     */
    public IiasGetResponse.Iia.Partner createIiasGetResponseIiaPartner() {
        return new IiasGetResponse.Iia.Partner();
    }

    /**
     * Create an instance of {@link IiasGetResponse.Iia.CooperationConditions }
     * 
     */
    public IiasGetResponse.Iia.CooperationConditions createIiasGetResponseIiaCooperationConditions() {
        return new IiasGetResponse.Iia.CooperationConditions();
    }

    /**
     * Create an instance of {@link IiasStatsResponse }
     *
     */
    public IiasStatsResponse createIiasStatsResponse(){
        return new IiasStatsResponse();
    }

}
