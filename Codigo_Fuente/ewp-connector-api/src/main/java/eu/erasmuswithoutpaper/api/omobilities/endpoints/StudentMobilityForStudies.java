//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.endpoints;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.erasmuswithoutpaper.api.architecture.Empty;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;
import eu.erasmuswithoutpaper.api.types.phonenumber.PhoneNumber;


/**
 * This describes a single Student Mobility for Studies.
 * <p>
 * In the future, it may become a "subclass" of a more generic Mobility parent class (and some of the fields might be
 * moved to the parent).
 * <p>
 * Immutable elements ------------------
 * <p>
 * Every mobility has its unique sending-hei, receiving-hei and nominee, which MUST NOT change after the mobility is
 * created. For example, if it turns out that a different student will be nominated for this mobility, then a new
 * omobility-id MUST be created for such nomination. However, the properties of these mobility-related entities still
 * MAY change. E.g. the student's name may get updated.
 *
 *
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *         &lt;element name="sending-hei"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                   &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="receiving-hei"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                   &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;choice&gt;
 *           &lt;element name="sending-academic-term-ewp-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v1}EwpAcademicTermId"/&gt;
 *           &lt;element name="non-standard-mobility-period" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Empty"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v1}AcademicYearId"/&gt;
 *         &lt;element name="student"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="given-names" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                   &lt;element name="family-name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                   &lt;element name="global-id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="birth-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                   &lt;element name="citizenship" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CountryCode" minOccurs="0"/&gt;
 *                   &lt;element name="gender" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Gender" minOccurs="0"/&gt;
 *                   &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email" minOccurs="0"/&gt;
 *                   &lt;element name="photo-url" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd&gt;HTTPS"&gt;
 *                           &lt;attribute name="size-px"&gt;
 *                             &lt;simpleType&gt;
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                 &lt;pattern value="[0-9]+x[0-9]+"/&gt;
 *                               &lt;/restriction&gt;
 *                             &lt;/simpleType&gt;
 *                           &lt;/attribute&gt;
 *                           &lt;attribute name="public" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *                           &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1}phone-number" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="status" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobilities/blob/stable-v1/endpoints/get-response.xsd}MobilityStatus"/&gt;
 *         &lt;element name="planned-arrival-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="planned-departure-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="eqf-level-studied-at-nomination" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}EqfLevel" minOccurs="0"/&gt;
 *         &lt;element name="eqf-level-studied-at-departure" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}EqfLevel" minOccurs="0"/&gt;
 *         &lt;element name="nominee-isced-f-code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nominee-language-skill" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}language"/&gt;
 *                   &lt;element name="cefr-level" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CefrLevel" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
    propOrder = {"omobilityId", "sendingHei", "receivingHei", "sendingAcademicTermEwpId", "nonStandardMobilityPeriod",
        "receivingAcademicYearId", "student", "status", "plannedArrivalDate", "plannedDepartureDate",
        "eqfLevelStudiedAtNomination", "eqfLevelStudiedAtDeparture", "nomineeIscedFCode", "nomineeLanguageSkill"})
@XmlRootElement(name = "student-mobility-for-studies")
public class StudentMobilityForStudies implements Serializable {

  @XmlElement(name = "omobility-id", required = true)
  protected String omobilityId;
  @XmlElement(name = "sending-hei", required = true)
  protected StudentMobilityForStudies.SendingHei sendingHei;
  @XmlElement(name = "receiving-hei", required = true)
  protected StudentMobilityForStudies.ReceivingHei receivingHei;
  @XmlElement(name = "sending-academic-term-ewp-id")
  protected String sendingAcademicTermEwpId;
  @XmlElement(name = "non-standard-mobility-period")
  protected Empty nonStandardMobilityPeriod;
  @XmlElement(name = "receiving-academic-year-id", required = true)
  protected String receivingAcademicYearId;
  @XmlElement(required = true)
  protected StudentMobilityForStudies.Student student;
  @XmlElement(required = true)
  @XmlSchemaType(name = "string")
  protected MobilityStatus status;
  @XmlElement(name = "planned-arrival-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar plannedArrivalDate;
  @XmlElement(name = "planned-departure-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar plannedDepartureDate;
  @XmlElement(name = "eqf-level-studied-at-nomination")
  protected Byte eqfLevelStudiedAtNomination;
  @XmlElement(name = "eqf-level-studied-at-departure")
  protected Byte eqfLevelStudiedAtDeparture;
  @XmlElement(name = "nominee-isced-f-code")
  protected String nomineeIscedFCode;
  @XmlElement(name = "nominee-language-skill")
  protected List<StudentMobilityForStudies.NomineeLanguageSkill> nomineeLanguageSkill;

  /**
   * Obtiene el valor de la propiedad omobilityId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOmobilityId() {
    return omobilityId;
  }

  /**
   * Define el valor de la propiedad omobilityId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOmobilityId(String value) {
    this.omobilityId = value;
  }

  /**
   * Obtiene el valor de la propiedad sendingHei.
   * 
   * @return possible object is {@link StudentMobilityForStudies.SendingHei }
   * 
   */
  public StudentMobilityForStudies.SendingHei getSendingHei() {
    return sendingHei;
  }

  /**
   * Define el valor de la propiedad sendingHei.
   * 
   * @param value allowed object is {@link StudentMobilityForStudies.SendingHei }
   * 
   */
  public void setSendingHei(StudentMobilityForStudies.SendingHei value) {
    this.sendingHei = value;
  }

  /**
   * Obtiene el valor de la propiedad receivingHei.
   * 
   * @return possible object is {@link StudentMobilityForStudies.ReceivingHei }
   * 
   */
  public StudentMobilityForStudies.ReceivingHei getReceivingHei() {
    return receivingHei;
  }

  /**
   * Define el valor de la propiedad receivingHei.
   * 
   * @param value allowed object is {@link StudentMobilityForStudies.ReceivingHei }
   * 
   */
  public void setReceivingHei(StudentMobilityForStudies.ReceivingHei value) {
    this.receivingHei = value;
  }

  /**
   * Obtiene el valor de la propiedad sendingAcademicTermEwpId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getSendingAcademicTermEwpId() {
    return sendingAcademicTermEwpId;
  }

  /**
   * Define el valor de la propiedad sendingAcademicTermEwpId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setSendingAcademicTermEwpId(String value) {
    this.sendingAcademicTermEwpId = value;
  }

  /**
   * Obtiene el valor de la propiedad nonStandardMobilityPeriod.
   * 
   * @return possible object is {@link Empty }
   * 
   */
  public Empty getNonStandardMobilityPeriod() {
    return nonStandardMobilityPeriod;
  }

  /**
   * Define el valor de la propiedad nonStandardMobilityPeriod.
   * 
   * @param value allowed object is {@link Empty }
   * 
   */
  public void setNonStandardMobilityPeriod(Empty value) {
    this.nonStandardMobilityPeriod = value;
  }

  /**
   * Obtiene el valor de la propiedad receivingAcademicYearId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getReceivingAcademicYearId() {
    return receivingAcademicYearId;
  }

  /**
   * Define el valor de la propiedad receivingAcademicYearId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setReceivingAcademicYearId(String value) {
    this.receivingAcademicYearId = value;
  }

  /**
   * Obtiene el valor de la propiedad student.
   * 
   * @return possible object is {@link StudentMobilityForStudies.Student }
   * 
   */
  public StudentMobilityForStudies.Student getStudent() {
    return student;
  }

  /**
   * Define el valor de la propiedad student.
   * 
   * @param value allowed object is {@link StudentMobilityForStudies.Student }
   * 
   */
  public void setStudent(StudentMobilityForStudies.Student value) {
    this.student = value;
  }

  /**
   * Obtiene el valor de la propiedad status.
   * 
   * @return possible object is {@link MobilityStatus }
   * 
   */
  public MobilityStatus getStatus() {
    return status;
  }

  /**
   * Define el valor de la propiedad status.
   * 
   * @param value allowed object is {@link MobilityStatus }
   * 
   */
  public void setStatus(MobilityStatus value) {
    this.status = value;
  }

  /**
   * Obtiene el valor de la propiedad plannedArrivalDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getPlannedArrivalDate() {
    return plannedArrivalDate;
  }

  /**
   * Define el valor de la propiedad plannedArrivalDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setPlannedArrivalDate(XMLGregorianCalendar value) {
    this.plannedArrivalDate = value;
  }

  /**
   * Obtiene el valor de la propiedad plannedDepartureDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getPlannedDepartureDate() {
    return plannedDepartureDate;
  }

  /**
   * Define el valor de la propiedad plannedDepartureDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setPlannedDepartureDate(XMLGregorianCalendar value) {
    this.plannedDepartureDate = value;
  }

  /**
   * Obtiene el valor de la propiedad eqfLevelStudiedAtNomination.
   * 
   * @return possible object is {@link Byte }
   * 
   */
  public Byte getEqfLevelStudiedAtNomination() {
    return eqfLevelStudiedAtNomination;
  }

  /**
   * Define el valor de la propiedad eqfLevelStudiedAtNomination.
   * 
   * @param value allowed object is {@link Byte }
   * 
   */
  public void setEqfLevelStudiedAtNomination(Byte value) {
    this.eqfLevelStudiedAtNomination = value;
  }

  /**
   * Obtiene el valor de la propiedad eqfLevelStudiedAtDeparture.
   * 
   * @return possible object is {@link Byte }
   * 
   */
  public Byte getEqfLevelStudiedAtDeparture() {
    return eqfLevelStudiedAtDeparture;
  }

  /**
   * Define el valor de la propiedad eqfLevelStudiedAtDeparture.
   * 
   * @param value allowed object is {@link Byte }
   * 
   */
  public void setEqfLevelStudiedAtDeparture(Byte value) {
    this.eqfLevelStudiedAtDeparture = value;
  }

  /**
   * Obtiene el valor de la propiedad nomineeIscedFCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getNomineeIscedFCode() {
    return nomineeIscedFCode;
  }

  /**
   * Define el valor de la propiedad nomineeIscedFCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setNomineeIscedFCode(String value) {
    this.nomineeIscedFCode = value;
  }

  /**
   * Gets the value of the nomineeLanguageSkill property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the nomineeLanguageSkill property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getNomineeLanguageSkill().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link StudentMobilityForStudies.NomineeLanguageSkill }
   * 
   * 
   */
  public List<StudentMobilityForStudies.NomineeLanguageSkill> getNomineeLanguageSkill() {
    if (nomineeLanguageSkill == null) {
      nomineeLanguageSkill = new ArrayList<StudentMobilityForStudies.NomineeLanguageSkill>();
    }
    return this.nomineeLanguageSkill;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}language"/&gt;
     *         &lt;element name="cefr-level" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CefrLevel" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"language", "cefrLevel"})
  public static class NomineeLanguageSkill implements Serializable {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlElement(name = "cefr-level")
    protected String cefrLevel;

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLanguage() {
      return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setLanguage(String value) {
      this.language = value;
    }

    /**
     * Obtiene el valor de la propiedad cefrLevel.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCefrLevel() {
      return cefrLevel;
    }

    /**
     * Define el valor de la propiedad cefrLevel.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCefrLevel(String value) {
      this.cefrLevel = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *         &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"heiId", "ounitId", "iiaId"})
  public static class ReceivingHei implements Serializable {

    @XmlElement(name = "hei-id", required = true)
    protected String heiId;
    @XmlElement(name = "ounit-id")
    protected String ounitId;
    @XmlElement(name = "iia-id")
    protected String iiaId;

    /**
     * Obtiene el valor de la propiedad heiId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHeiId() {
      return heiId;
    }

    /**
     * Define el valor de la propiedad heiId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setHeiId(String value) {
      this.heiId = value;
    }

    /**
     * Obtiene el valor de la propiedad ounitId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOunitId() {
      return ounitId;
    }

    /**
     * Define el valor de la propiedad ounitId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setOunitId(String value) {
      this.ounitId = value;
    }

    /**
     * Obtiene el valor de la propiedad iiaId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIiaId() {
      return iiaId;
    }

    /**
     * Define el valor de la propiedad iiaId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIiaId(String value) {
      this.iiaId = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *         &lt;element name="iia-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"heiId", "ounitId", "iiaId"})
  public static class SendingHei implements Serializable {

    @XmlElement(name = "hei-id", required = true)
    protected String heiId;
    @XmlElement(name = "ounit-id")
    protected String ounitId;
    @XmlElement(name = "iia-id")
    protected String iiaId;

    /**
     * Obtiene el valor de la propiedad heiId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHeiId() {
      return heiId;
    }

    /**
     * Define el valor de la propiedad heiId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setHeiId(String value) {
      this.heiId = value;
    }

    /**
     * Obtiene el valor de la propiedad ounitId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOunitId() {
      return ounitId;
    }

    /**
     * Define el valor de la propiedad ounitId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setOunitId(String value) {
      this.ounitId = value;
    }

    /**
     * Obtiene el valor de la propiedad iiaId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getIiaId() {
      return iiaId;
    }

    /**
     * Define el valor de la propiedad iiaId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setIiaId(String value) {
      this.iiaId = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="given-names" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
     *         &lt;element name="family-name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
     *         &lt;element name="global-id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="birth-date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *         &lt;element name="citizenship" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CountryCode" minOccurs="0"/&gt;
     *         &lt;element name="gender" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Gender" minOccurs="0"/&gt;
     *         &lt;element name="email" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}Email" minOccurs="0"/&gt;
     *         &lt;element name="photo-url" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd&gt;HTTPS"&gt;
     *                 &lt;attribute name="size-px"&gt;
     *                   &lt;simpleType&gt;
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                       &lt;pattern value="[0-9]+x[0-9]+"/&gt;
     *                     &lt;/restriction&gt;
     *                   &lt;/simpleType&gt;
     *                 &lt;/attribute&gt;
     *                 &lt;attribute name="public" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
     *                 &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1}phone-number" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"givenNames", "familyName", "globalId", "birthDate", "citizenship", "gender",
      "email", "photoUrl", "streetAddress", "mailingAddress", "phoneNumber"})
  public static class Student implements Serializable {

    @XmlElement(name = "given-names", required = true)
    protected List<StringWithOptionalLang> givenNames;
    @XmlElement(name = "family-name", required = true)
    protected List<StringWithOptionalLang> familyName;
    @XmlElement(name = "global-id")
    protected String globalId;
    @XmlElement(name = "birth-date")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDate;
    protected String citizenship;
    protected BigInteger gender;
    protected String email;
    @XmlElement(name = "photo-url")
    protected List<StudentMobilityForStudies.Student.PhotoUrl> photoUrl;
    @XmlElement(name = "street-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress streetAddress;
    @XmlElement(name = "mailing-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress mailingAddress;
    @XmlElement(name = "phone-number",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-phonenumber/tree/stable-v1")
    protected List<PhoneNumber> phoneNumber;

    /**
     * Gets the value of the givenNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the givenNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getGivenNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
     * 
     * 
     */
    public List<StringWithOptionalLang> getGivenNames() {
      if (givenNames == null) {
        givenNames = new ArrayList<StringWithOptionalLang>();
      }
      return this.givenNames;
    }

    /**
     * Gets the value of the familyName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the familyName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getFamilyName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
     * 
     * 
     */
    public List<StringWithOptionalLang> getFamilyName() {
      if (familyName == null) {
        familyName = new ArrayList<StringWithOptionalLang>();
      }
      return this.familyName;
    }

    /**
     * Obtiene el valor de la propiedad globalId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getGlobalId() {
      return globalId;
    }

    /**
     * Define el valor de la propiedad globalId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setGlobalId(String value) {
      this.globalId = value;
    }

    /**
     * Obtiene el valor de la propiedad birthDate.
     * 
     * @return possible object is {@link XMLGregorianCalendar }
     * 
     */
    public XMLGregorianCalendar getBirthDate() {
      return birthDate;
    }

    /**
     * Define el valor de la propiedad birthDate.
     * 
     * @param value allowed object is {@link XMLGregorianCalendar }
     * 
     */
    public void setBirthDate(XMLGregorianCalendar value) {
      this.birthDate = value;
    }

    /**
     * Obtiene el valor de la propiedad citizenship.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCitizenship() {
      return citizenship;
    }

    /**
     * Define el valor de la propiedad citizenship.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCitizenship(String value) {
      this.citizenship = value;
    }

    /**
     * Obtiene el valor de la propiedad gender.
     * 
     * @return possible object is {@link BigInteger }
     * 
     */
    public BigInteger getGender() {
      return gender;
    }

    /**
     * Define el valor de la propiedad gender.
     * 
     * @param value allowed object is {@link BigInteger }
     * 
     */
    public void setGender(BigInteger value) {
      this.gender = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getEmail() {
      return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setEmail(String value) {
      this.email = value;
    }

    /**
     * Gets the value of the photoUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the photoUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getPhotoUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StudentMobilityForStudies.Student.PhotoUrl }
     * 
     * 
     */
    public List<StudentMobilityForStudies.Student.PhotoUrl> getPhotoUrl() {
      if (photoUrl == null) {
        photoUrl = new ArrayList<StudentMobilityForStudies.Student.PhotoUrl>();
      }
      return this.photoUrl;
    }

    /**
     * 
     * Optional street address of the person's home.
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getStreetAddress() {
      return streetAddress;
    }

    /**
     * Define el valor de la propiedad streetAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setStreetAddress(FlexibleAddress value) {
      this.streetAddress = value;
    }

    /**
     * 
     * The postal address of this person. (This is usually much more useful than street address.)
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getMailingAddress() {
      return mailingAddress;
    }

    /**
     * Define el valor de la propiedad mailingAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setMailingAddress(FlexibleAddress value) {
      this.mailingAddress = value;
    }

    /**
     * 
     * A list of phone numbers at which this person can possibly be reached. The "best" numbers should be listed first.
     * Gets the value of the phoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the phoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link PhoneNumber }
     * 
     * 
     */
    public List<PhoneNumber> getPhoneNumber() {
      if (phoneNumber == null) {
        phoneNumber = new ArrayList<PhoneNumber>();
      }
      return this.phoneNumber;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd&gt;HTTPS"&gt;
         *       &lt;attribute name="size-px"&gt;
         *         &lt;simpleType&gt;
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *             &lt;pattern value="[0-9]+x[0-9]+"/&gt;
         *           &lt;/restriction&gt;
         *         &lt;/simpleType&gt;
         *       &lt;/attribute&gt;
         *       &lt;attribute name="public" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
         *       &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"value"})
    public static class PhotoUrl implements Serializable {

      @XmlValue
      protected String value;
      @XmlAttribute(name = "size-px")
      protected String sizePx;
      @XmlAttribute(name = "public")
      protected Boolean _public;
      @XmlAttribute(name = "date")
      @XmlSchemaType(name = "date")
      protected XMLGregorianCalendar date;

      /**
       * 
       * Secure (HTTPS), absolute URL.
       * 
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getValue() {
        return value;
      }

      /**
       * Define el valor de la propiedad value.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setValue(String value) {
        this.value = value;
      }

      /**
       * Obtiene el valor de la propiedad sizePx.
       * 
       * @return possible object is {@link String }
       * 
       */
      public String getSizePx() {
        return sizePx;
      }

      /**
       * Define el valor de la propiedad sizePx.
       * 
       * @param value allowed object is {@link String }
       * 
       */
      public void setSizePx(String value) {
        this.sizePx = value;
      }

      /**
       * Obtiene el valor de la propiedad public.
       * 
       * @return possible object is {@link Boolean }
       * 
       */
      public boolean isPublic() {
        if (_public == null) {
          return false;
        } else {
          return _public;
        }
      }

      /**
       * Define el valor de la propiedad public.
       * 
       * @param value allowed object is {@link Boolean }
       * 
       */
      public void setPublic(Boolean value) {
        this._public = value;
      }

      /**
       * Obtiene el valor de la propiedad date.
       * 
       * @return possible object is {@link XMLGregorianCalendar }
       * 
       */
      public XMLGregorianCalendar getDate() {
        return date;
      }

      /**
       * Define el valor de la propiedad date.
       * 
       * @param value allowed object is {@link XMLGregorianCalendar }
       * 
       */
      public void setDate(XMLGregorianCalendar value) {
        this.date = value;
      }

    }

  }

}
