//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.types.academicterm;

import java.io.Serializable;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An identifier of the academic term.
 * <p>
 * This identifier does not relate to a year, so to identify an academic term that is fixed in time you need to also
 * specify the academic year (see `AcademicTerm` type).
 *
 *
 * <p>
 * Clase Java para TermId complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="TermId"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="term-number" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="total-terms" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TermId", propOrder = {"termNumber", "totalTerms"})
public class TermId implements Serializable {

  @XmlElement(name = "term-number", required = true)
  protected BigInteger termNumber;
  @XmlElement(name = "total-terms", required = true)
  protected BigInteger totalTerms;

  /**
   * Obtiene el valor de la propiedad termNumber.
   * 
   * @return possible object is {@link BigInteger }
   * 
   */
  public BigInteger getTermNumber() {
    return termNumber;
  }

  /**
   * Define el valor de la propiedad termNumber.
   * 
   * @param value allowed object is {@link BigInteger }
   * 
   */
  public void setTermNumber(BigInteger value) {
    this.termNumber = value;
  }

  /**
   * Obtiene el valor de la propiedad totalTerms.
   * 
   * @return possible object is {@link BigInteger }
   * 
   */
  public BigInteger getTotalTerms() {
    return totalTerms;
  }

  /**
   * Define el valor de la propiedad totalTerms.
   * 
   * @param value allowed object is {@link BigInteger }
   * 
   */
  public void setTotalTerms(BigInteger value) {
    this.totalTerms = value;
  }

}
