//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.endpoints;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * eu.erasmuswithoutpaper.api.omobilities.endpoints package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlRegistry
public class ObjectFactory {


  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
   * eu.erasmuswithoutpaper.api.omobilities.endpoints
   * 
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link StudentMobilityForStudies }
   * 
   */
  public StudentMobilityForStudies createStudentMobilityForStudies() {
    return new StudentMobilityForStudies();
  }

  /**
   * Create an instance of {@link StudentMobilityForStudies.Student }
   * 
   */
  public StudentMobilityForStudies.Student createStudentMobilityForStudiesStudent() {
    return new StudentMobilityForStudies.Student();
  }

  /**
   * Create an instance of {@link OmobilitiesIndexResponse }
   * 
   */
  public OmobilitiesIndexResponse createOmobilitiesIndexResponse() {
    return new OmobilitiesIndexResponse();
  }

  /**
   * Create an instance of {@link OmobilitiesGetResponse }
   * 
   */
  public OmobilitiesGetResponse createOmobilitiesGetResponse() {
    return new OmobilitiesGetResponse();
  }

  /**
   * Create an instance of {@link StudentMobilityForStudies.SendingHei }
   * 
   */
  public StudentMobilityForStudies.SendingHei createStudentMobilityForStudiesSendingHei() {
    return new StudentMobilityForStudies.SendingHei();
  }

  /**
   * Create an instance of {@link StudentMobilityForStudies.ReceivingHei }
   * 
   */
  public StudentMobilityForStudies.ReceivingHei createStudentMobilityForStudiesReceivingHei() {
    return new StudentMobilityForStudies.ReceivingHei();
  }

  /**
   * Create an instance of {@link StudentMobilityForStudies.NomineeLanguageSkill }
   * 
   */
  public StudentMobilityForStudies.NomineeLanguageSkill createStudentMobilityForStudiesNomineeLanguageSkill() {
    return new StudentMobilityForStudies.NomineeLanguageSkill();
  }

  /**
   * Create an instance of {@link StudentMobilityForStudies.Student.PhotoUrl }
   * 
   */
  public StudentMobilityForStudies.Student.PhotoUrl createStudentMobilityForStudiesStudentPhotoUrl() {
    return new StudentMobilityForStudies.Student.PhotoUrl();
  }

}
