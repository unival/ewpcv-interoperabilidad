package eu.erasmuswithoutpaper.api.imobilities.tors.cnr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The type Imobility tor cnr response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "imobility-tor-cnr-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tor-cnr/tree/stable-v1")
public class ImobilityTorCnrResponse implements Serializable {

  private static final long serialVersionUID = 2861574143369759024L;
}

