//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.imobilities.tors.endpoints;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import eu.emrex.elmo.Elmo;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tor" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *                   &lt;element ref="{https://github.com/emrex-eu/elmo-schemas/tree/v1}elmo"/&gt;
 *                   &lt;element name="gradeConversionTable" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="iscedTable" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="gradeFrequency" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                                               &lt;attribute name="percentage" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="iscedCode" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"tor"})
@XmlRootElement(name = "imobility-tors-get-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v1/endpoints/get-response.xsd")
public class ImobilityTorsGetResponse implements Serializable {

  @XmlElement(
      namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v1/endpoints/get-response.xsd")
  protected List<ImobilityTorsGetResponse.Tor> tor;

  /**
   * Gets the value of the tor property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the tor property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getTor().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link ImobilityTorsGetResponse.Tor }
   * 
   * 
   */
  public List<ImobilityTorsGetResponse.Tor> getTor() {
    if (tor == null) {
      tor = new ArrayList<ImobilityTorsGetResponse.Tor>();
    }
    return this.tor;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
     *         &lt;element ref="{https://github.com/emrex-eu/elmo-schemas/tree/v1}elmo"/&gt;
     *         &lt;element name="gradeConversionTable" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="iscedTable" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="gradeFrequency" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *                                     &lt;attribute name="percentage" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="iscedCode" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"omobilityId", "elmo", "gradeConversionTable"})
  public static class Tor implements Serializable {

    @XmlElement(name = "omobility-id",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v2/endpoints/get-response.xsd",
        required = true)
    protected String omobilityId;
    @XmlElement(namespace = "https://github.com/emrex-eu/elmo-schemas/tree/v1", required = true)
    protected Elmo elmo;
    @XmlElement(
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v2/endpoints/get-response.xsd")
    protected ImobilityTorsGetResponse.Tor.GradeConversionTable gradeConversionTable;

    /**
     * Obtiene el valor de la propiedad omobilityId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getOmobilityId() {
      return omobilityId;
    }

    /**
     * Define el valor de la propiedad omobilityId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setOmobilityId(String value) {
      this.omobilityId = value;
    }

    /**
     * 
     * The Transcript of Records for this mobility, in the EMREX ELMO format.
     * 
     * ## Why we reuse EMREX ELMO format?
     * 
     * It's worth taking note of some slight disadvantages of this approach - in particular, there's the fact that some
     * features required by EMREX ELMO are quite unnecessary in EWP's use case.
     * 
     * For example, we do not need `learner` and `issuer` elements, because we can determine their content by other
     * means.
     * 
     * Despite these, we have chosen to require EMREX ELMO XML format here, "as it is", without any changes. Why?
     * 
     * 1. Because some partners have already implemented EMREX, and it will simply be easier for them to reuse its
     * output. 2. Because we want to encourage EWP partners to implement EMREX too. 3. Because we don't want to
     * introduce a competing ToR format unless we have to. It seems better to promote existing formats. 4. Because
     * *some* clients might still make some use of the features which we might otherwise deem unnecessary (e.g. PDF
     * attachments).
     * 
     * ## Cross-referencing IDs
     * 
     * EMREX ELMO allows implementers to attach an unlimited number of identifiers to many of its elements. In order to
     * facilitate cross-referencing, we need to assign some explicit values for the `type` attribute used in the
     * `identifier` elements:
     * 
     * * In the `issuer` element, you SHOULD use the "schac" identifier type to refer to EWP's HEI IDs. E.g.
     * <identifier type="schac">uw.edu.pl</identifier>
     * https://github.com/erasmus-without-paper/ewp-specs-api-registry/#schac-identifiers
     * 
     * * In the `learningOpportunitySpecification` elements, you SHOULD use the "ewp-los-id" identifier type to refer to
     * LOS IDs introduced in EWP's Courses API:
     * https://github.com/erasmus-without-paper/ewp-specs-api-courses#unique-identifiers
     * 
     * * In the `learningOpportunityInstance` elements, you SHOULD use the "ewp-loi-id" identifier type to refer to LOI
     * IDs introduced in EWP's Courses API:
     * https://github.com/erasmus-without-paper/ewp-specs-api-courses#unique-identifiers
     * 
     * (At the time of writing this, EMREX ELMO schema v1.1.0 does not allow specifying identifiers for LOI elements,
     * but we have requested this change and expect it to be introduced in future versions:
     * https://github.com/emrex-eu/elmo-schemas/issues/10)
     * 
     * * The PDF version of the transcript, if present, SHOULD be located inside the `report` element and have type
     * 'Transcript of Records'.
     * 
     * 
     * @return possible object is {@link Elmo }
     * 
     */
    public Elmo getElmo() {
      return elmo;
    }

    /**
     * Define el valor de la propiedad elmo.
     * 
     * @param value allowed object is {@link Elmo }
     * 
     */
    public void setElmo(Elmo value) {
      this.elmo = value;
    }

    /**
     * Obtiene el valor de la propiedad gradeConversionTable.
     * 
     * @return possible object is {@link ImobilityTorsGetResponse.Tor.GradeConversionTable }
     * 
     */
    public ImobilityTorsGetResponse.Tor.GradeConversionTable getGradeConversionTable() {
      return gradeConversionTable;
    }

    /**
     * Define el valor de la propiedad gradeConversionTable.
     * 
     * @param value allowed object is {@link ImobilityTorsGetResponse.Tor.GradeConversionTable }
     * 
     */
    public void setGradeConversionTable(ImobilityTorsGetResponse.Tor.GradeConversionTable value) {
      this.gradeConversionTable = value;
    }


        /**
         * <p>
         * Clase Java para anonymous complex type.
         *
         * <p>
         * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="iscedTable" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="gradeFrequency" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *                           &lt;attribute name="percentage" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="iscedCode" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
         */
        @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {"iscedTable"})
    public static class GradeConversionTable implements Serializable {

      @XmlElement(
          namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v2/endpoints/get-response.xsd",
          required = true)
      protected List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable> iscedTable;

      /**
       * Gets the value of the iscedTable property.
       * 
       * <p>
       * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make
       * to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method
       * for the iscedTable property.
       * 
       * <p>
       * For example, to add a new item, do as follows:
       * 
       * <pre>
       * getIscedTable().add(newItem);
       * </pre>
       * 
       * 
       * <p>
       * Objects of the following type(s) are allowed in the list
       * {@link ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable }
       * 
       * 
       */
      public List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable> getIscedTable() {
        if (iscedTable == null) {
          iscedTable = new ArrayList<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable>();
        }
        return this.iscedTable;
      }


            /**
             * <p>
             * Clase Java para anonymous complex type.
             *
             * <p>
             * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="gradeFrequency" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
             *                 &lt;attribute name="percentage" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="iscedCode" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
             */
            @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(name = "", propOrder = {"gradeFrequency"})
      public static class IscedTable implements Serializable {

        @XmlElement(
            namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-imobility-tors/blob/stable-v2/endpoints/get-response.xsd",
            required = true)
        protected List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency> gradeFrequency;
        @XmlAttribute(name = "iscedCode", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String iscedCode;

        /**
         * Gets the value of the gradeFrequency property.
         * 
         * <p>
         * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you
         * make to the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE>
         * method for the gradeFrequency property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * 
         * <pre>
         * getGradeFrequency().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency }
         * 
         * 
         */
        public List<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency> getGradeFrequency() {
          if (gradeFrequency == null) {
            gradeFrequency =
                new ArrayList<ImobilityTorsGetResponse.Tor.GradeConversionTable.IscedTable.GradeFrequency>();
          }
          return this.gradeFrequency;
        }

        /**
         * Obtiene el valor de la propiedad iscedCode.
         * 
         * @return possible object is {@link String }
         * 
         */
        public String getIscedCode() {
          return iscedCode;
        }

        /**
         * Define el valor de la propiedad iscedCode.
         * 
         * @param value allowed object is {@link String }
         * 
         */
        public void setIscedCode(String value) {
          this.iscedCode = value;
        }


                /**
                 * <p>
                 * Clase Java para anonymous complex type.
                 *
                 * <p>
                 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 *
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}token" /&gt;
                 *       &lt;attribute name="percentage" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 *
                 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class GradeFrequency implements Serializable {

          @XmlAttribute(name = "label", required = true)
          @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
          @XmlSchemaType(name = "token")
          protected String label;
          @XmlAttribute(name = "percentage", required = true)
          protected BigDecimal percentage;

          /**
           * Obtiene el valor de la propiedad label.
           * 
           * @return possible object is {@link String }
           * 
           */
          public String getLabel() {
            return label;
          }

          /**
           * Define el valor de la propiedad label.
           * 
           * @param value allowed object is {@link String }
           * 
           */
          public void setLabel(String value) {
            this.label = value;
          }

          /**
           * Obtiene el valor de la propiedad percentage.
           * 
           * @return possible object is {@link BigDecimal }
           * 
           */
          public BigDecimal getPercentage() {
            return percentage;
          }

          /**
           * Define el valor de la propiedad percentage.
           * 
           * @param value allowed object is {@link BigDecimal }
           * 
           */
          public void setPercentage(BigDecimal value) {
            this.percentage = value;
          }

        }

      }

    }

  }

}
