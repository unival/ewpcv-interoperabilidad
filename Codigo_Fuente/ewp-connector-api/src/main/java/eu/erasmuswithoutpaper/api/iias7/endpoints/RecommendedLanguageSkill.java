//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.12.14 a las 12:10:09 PM CET 
//


package eu.erasmuswithoutpaper.api.iias7.endpoints;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}language"/&gt;
 *         &lt;element name="cefr-level" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[ABC][12]"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="subject-area" type="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}SubjectArea" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v7/endpoints/get-response.xsd}not-yet-defined"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "language",
    "cefrLevel",
    "subjectArea"
})
@XmlRootElement(name = "recommended-language-skill")
public class RecommendedLanguageSkill {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlElement(name = "cefr-level")
    protected String cefrLevel;
    @XmlElement(name = "subject-area")
    protected SubjectArea subjectArea;
    @XmlAttribute(name = "not-yet-defined")
    protected Boolean notYetDefined;

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Obtiene el valor de la propiedad cefrLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCefrLevel() {
        return cefrLevel;
    }

    /**
     * Define el valor de la propiedad cefrLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCefrLevel(String value) {
        this.cefrLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad subjectArea.
     * 
     * @return
     *     possible object is
     *     {@link SubjectArea }
     *     
     */
    public SubjectArea getSubjectArea() {
        return subjectArea;
    }

    /**
     * Define el valor de la propiedad subjectArea.
     * 
     * @param value
     *     allowed object is
     *     {@link SubjectArea }
     *     
     */
    public void setSubjectArea(SubjectArea value) {
        this.subjectArea = value;
    }

    /**
     * Obtiene el valor de la propiedad notYetDefined.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotYetDefined() {
        return notYetDefined;
    }

    /**
     * Define el valor de la propiedad notYetDefined.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotYetDefined(Boolean value) {
        this.notYetDefined = value;
    }

}
