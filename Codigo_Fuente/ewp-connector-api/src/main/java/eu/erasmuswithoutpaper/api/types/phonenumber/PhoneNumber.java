//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.types.phonenumber;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A phone number. Whenever possible, this SHOULD be provided in E.164 format (with the leading "+" sign).
 * <p>
 * More information and reasoning here: https://github.com/erasmus-without-paper/ewp-specs-architecture/issues/15
 *
 *
 * <p>
 * Clase Java para PhoneNumber complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="PhoneNumber"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="e164" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="\+[0-9]{1,15}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ext" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *         &lt;element name="other-format" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhoneNumber", propOrder = {"e164", "ext", "otherFormat"})
public class PhoneNumber implements Serializable {

  protected String e164;
  protected Object ext;
  @XmlElement(name = "other-format")
  protected String otherFormat;

  /**
   * Obtiene el valor de la propiedad e164.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getE164() {
    return e164;
  }

  /**
   * Define el valor de la propiedad e164.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setE164(String value) {
    this.e164 = value;
  }

  /**
   * Obtiene el valor de la propiedad ext.
   * 
   * @return possible object is {@link Object }
   * 
   */
  public Object getExt() {
    return ext;
  }

  /**
   * Define el valor de la propiedad ext.
   * 
   * @param value allowed object is {@link Object }
   * 
   */
  public void setExt(Object value) {
    this.ext = value;
  }

  /**
   * Obtiene el valor de la propiedad otherFormat.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOtherFormat() {
    return otherFormat;
  }

  /**
   * Define el valor de la propiedad otherFormat.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOtherFormat(String value) {
    this.otherFormat = value;
  }

}
