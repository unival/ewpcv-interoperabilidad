//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación
// de la referencia de enlace (JAXB) XML v2.2.11
// Visite <a
// href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve
// a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.omobilities.las.endpoints;

import java.io.Serializable;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>
 * Clase Java para LearningAgreement complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="LearningAgreement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="omobility-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier"/&gt;
 *         &lt;element name="sending-hei" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}MobilityInstitution"/&gt;
 *         &lt;element name="receiving-hei" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}MobilityInstitution"/&gt;
 *         &lt;element name="receiving-academic-year-id" type="{https://github.com/erasmus-without-paper/ewp-specs-types-academic-term/tree/stable-v2}AcademicYearId"/&gt;
 *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}student"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="start-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *           &lt;element name="start-year-month" type="{http://www.w3.org/2001/XMLSchema}gYearMonth"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="end-date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *           &lt;element name="end-year-month" type="{http://www.w3.org/2001/XMLSchema}gYearMonth"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="eqf-level-studied-at-departure" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}EqfLevel"/&gt;
 *         &lt;element name="isced-f-code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isced-clarification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="student-language-skill"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}language"/&gt;
 *                   &lt;element name="cefr-level" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CefrLevel"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="first-version" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ListOf_Components" minOccurs="0"/&gt;
 *         &lt;element name="approved-changes" type="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ListOf_Components" minOccurs="0"/&gt;
 *         &lt;element name="changes-proposal" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ListOf_Components"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}student" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="learning-outcomes-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTP" minOccurs="0"/&gt;
 *         &lt;element name="provisions-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTP" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LearningAgreement",
    propOrder = {"omobilityId", "sendingHei", "receivingHei", "receivingAcademicYearId", "student", "startDate",
        "startYearMonth", "endDate", "endYearMonth", "eqfLevelStudiedAtDeparture", "iscedFCode", "iscedClarification",
        "studentLanguageSkill", "firstVersion", "approvedChanges", "changesProposal", "learningOutcomesUrl",
        "provisionsUrl"})
public class LearningAgreement implements Serializable {

  @XmlElement(name = "omobility-id", required = true)
  protected String omobilityId;
  @XmlElement(name = "sending-hei", required = true)
  protected MobilityInstitution sendingHei;
  @XmlElement(name = "receiving-hei", required = true)
  protected MobilityInstitution receivingHei;
  @XmlElement(name = "receiving-academic-year-id", required = true)
  protected String receivingAcademicYearId;
  @XmlElement(required = true)
  protected Student student;
  @XmlElement(name = "start-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar startDate;
  @XmlElement(name = "start-year-month")
  @XmlSchemaType(name = "gYearMonth")
  protected XMLGregorianCalendar startYearMonth;
  @XmlElement(name = "end-date")
  @XmlSchemaType(name = "date")
  protected XMLGregorianCalendar endDate;
  @XmlElement(name = "end-year-month")
  @XmlSchemaType(name = "gYearMonth")
  protected XMLGregorianCalendar endYearMonth;
  @XmlElement(name = "eqf-level-studied-at-departure")
  protected byte eqfLevelStudiedAtDeparture;
  @XmlElement(name = "isced-f-code", required = true)
  protected String iscedFCode;
  @XmlElement(name = "isced-clarification")
  protected String iscedClarification;
  @XmlElement(name = "student-language-skill", required = true)
  protected LearningAgreement.StudentLanguageSkill studentLanguageSkill;
  @XmlElement(name = "first-version")
  protected ListOfComponents firstVersion;
  @XmlElement(name = "approved-changes")
  protected ListOfComponents approvedChanges;
  @XmlElement(name = "changes-proposal")
  protected LearningAgreement.ChangesProposal changesProposal;
  @XmlElement(name = "learning-outcomes-url")
  @XmlSchemaType(name = "anyURI")
  protected String learningOutcomesUrl;
  @XmlElement(name = "provisions-url")
  @XmlSchemaType(name = "anyURI")
  protected String provisionsUrl;

  /**
   * Obtiene el valor de la propiedad omobilityId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getOmobilityId() {
    return omobilityId;
  }

  /**
   * Define el valor de la propiedad omobilityId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setOmobilityId(String value) {
    this.omobilityId = value;
  }

  /**
   * Obtiene el valor de la propiedad sendingHei.
   * 
   * @return possible object is {@link MobilityInstitution }
   * 
   */
  public MobilityInstitution getSendingHei() {
    return sendingHei;
  }

  /**
   * Define el valor de la propiedad sendingHei.
   * 
   * @param value allowed object is {@link MobilityInstitution }
   * 
   */
  public void setSendingHei(MobilityInstitution value) {
    this.sendingHei = value;
  }

  /**
   * Obtiene el valor de la propiedad receivingHei.
   * 
   * @return possible object is {@link MobilityInstitution }
   * 
   */
  public MobilityInstitution getReceivingHei() {
    return receivingHei;
  }

  /**
   * Define el valor de la propiedad receivingHei.
   * 
   * @param value allowed object is {@link MobilityInstitution }
   * 
   */
  public void setReceivingHei(MobilityInstitution value) {
    this.receivingHei = value;
  }

  /**
   * Obtiene el valor de la propiedad receivingAcademicYearId.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getReceivingAcademicYearId() {
    return receivingAcademicYearId;
  }

  /**
   * Define el valor de la propiedad receivingAcademicYearId.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setReceivingAcademicYearId(String value) {
    this.receivingAcademicYearId = value;
  }

  /**
   * Obtiene el valor de la propiedad student.
   * 
   * @return possible object is {@link Student }
   * 
   */
  public Student getStudent() {
    return student;
  }

  /**
   * Define el valor de la propiedad student.
   * 
   * @param value allowed object is {@link Student }
   * 
   */
  public void setStudent(Student value) {
    this.student = value;
  }

  /**
   * Obtiene el valor de la propiedad startDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getStartDate() {
    return startDate;
  }

  /**
   * Define el valor de la propiedad startDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setStartDate(XMLGregorianCalendar value) {
    this.startDate = value;
  }

  /**
   * Obtiene el valor de la propiedad startYearMonth.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getStartYearMonth() {
    return startYearMonth;
  }

  /**
   * Define el valor de la propiedad startYearMonth.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setStartYearMonth(XMLGregorianCalendar value) {
    this.startYearMonth = value;
  }

  /**
   * Obtiene el valor de la propiedad endDate.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getEndDate() {
    return endDate;
  }

  /**
   * Define el valor de la propiedad endDate.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setEndDate(XMLGregorianCalendar value) {
    this.endDate = value;
  }

  /**
   * Obtiene el valor de la propiedad endYearMonth.
   * 
   * @return possible object is {@link XMLGregorianCalendar }
   * 
   */
  public XMLGregorianCalendar getEndYearMonth() {
    return endYearMonth;
  }

  /**
   * Define el valor de la propiedad endYearMonth.
   * 
   * @param value allowed object is {@link XMLGregorianCalendar }
   * 
   */
  public void setEndYearMonth(XMLGregorianCalendar value) {
    this.endYearMonth = value;
  }

  /**
   * Obtiene el valor de la propiedad eqfLevelStudiedAtDeparture.
   * 
   */
  public byte getEqfLevelStudiedAtDeparture() {
    return eqfLevelStudiedAtDeparture;
  }

  /**
   * Define el valor de la propiedad eqfLevelStudiedAtDeparture.
   * 
   */
  public void setEqfLevelStudiedAtDeparture(byte value) {
    this.eqfLevelStudiedAtDeparture = value;
  }

  /**
   * Obtiene el valor de la propiedad iscedFCode.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIscedFCode() {
    return iscedFCode;
  }

  /**
   * Define el valor de la propiedad iscedFCode.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIscedFCode(String value) {
    this.iscedFCode = value;
  }

  /**
   * Obtiene el valor de la propiedad iscedClarification.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getIscedClarification() {
    return iscedClarification;
  }

  /**
   * Define el valor de la propiedad iscedClarification.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setIscedClarification(String value) {
    this.iscedClarification = value;
  }

  /**
   * Obtiene el valor de la propiedad studentLanguageSkill.
   * 
   * @return possible object is {@link LearningAgreement.StudentLanguageSkill }
   * 
   */
  public LearningAgreement.StudentLanguageSkill getStudentLanguageSkill() {
    return studentLanguageSkill;
  }

  /**
   * Define el valor de la propiedad studentLanguageSkill.
   * 
   * @param value allowed object is {@link LearningAgreement.StudentLanguageSkill }
   * 
   */
  public void setStudentLanguageSkill(LearningAgreement.StudentLanguageSkill value) {
    this.studentLanguageSkill = value;
  }

  /**
   * Obtiene el valor de la propiedad firstVersion.
   * 
   * @return possible object is {@link ListOfComponents }
   * 
   */
  public ListOfComponents getFirstVersion() {
    return firstVersion;
  }

  /**
   * Define el valor de la propiedad firstVersion.
   * 
   * @param value allowed object is {@link ListOfComponents }
   * 
   */
  public void setFirstVersion(ListOfComponents value) {
    this.firstVersion = value;
  }

  /**
   * Obtiene el valor de la propiedad approvedChanges.
   * 
   * @return possible object is {@link ListOfComponents }
   * 
   */
  public ListOfComponents getApprovedChanges() {
    return approvedChanges;
  }

  /**
   * Define el valor de la propiedad approvedChanges.
   * 
   * @param value allowed object is {@link ListOfComponents }
   * 
   */
  public void setApprovedChanges(ListOfComponents value) {
    this.approvedChanges = value;
  }

  /**
   * Obtiene el valor de la propiedad changesProposal.
   * 
   * @return possible object is {@link LearningAgreement.ChangesProposal }
   * 
   */
  public LearningAgreement.ChangesProposal getChangesProposal() {
    return changesProposal;
  }

  /**
   * Define el valor de la propiedad changesProposal.
   * 
   * @param value allowed object is {@link LearningAgreement.ChangesProposal }
   * 
   */
  public void setChangesProposal(LearningAgreement.ChangesProposal value) {
    this.changesProposal = value;
  }

  /**
   * Obtiene el valor de la propiedad learningOutcomesUrl.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getLearningOutcomesUrl() {
    return learningOutcomesUrl;
  }

  /**
   * Define el valor de la propiedad learningOutcomesUrl.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setLearningOutcomesUrl(String value) {
    this.learningOutcomesUrl = value;
  }

  /**
   * Obtiene el valor de la propiedad provisionsUrl.
   * 
   * @return possible object is {@link String }
   * 
   */
  public String getProvisionsUrl() {
    return provisionsUrl;
  }

  /**
   * Define el valor de la propiedad provisionsUrl.
   * 
   * @param value allowed object is {@link String }
   * 
   */
  public void setProvisionsUrl(String value) {
    this.provisionsUrl = value;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}ListOf_Components"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-api-omobility-las/blob/stable-v1/endpoints/get-response.xsd}student" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"student"})
  public static class ChangesProposal extends ListOfComponents implements Serializable {

    protected Student student;
    @XmlAttribute(name = "id", required = true)
    protected String id;

    /**
     * 
     * This element represents the student's personal data that has changed after the last receiving HEI's approval.
     * 
     * Note: Only those fields that have changed should be present here. Changes to the following fields are considered
     * as important and should be mentioned here: `given-names`, `family-name`, `global-id`, `birth-date`. Other fields
     * may change without the need to issue a proposal.
     * 
     * 
     * @return possible object is {@link Student }
     * 
     */
    public Student getStudent() {
      return student;
    }

    /**
     * Define el valor de la propiedad student.
     * 
     * @param value allowed object is {@link Student }
     * 
     */
    public void setStudent(Student value) {
      this.student = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getId() {
      return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setId(String value) {
      this.id = value;
    }

  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}language"/&gt;
     *         &lt;element name="cefr-level" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}CefrLevel"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"language", "cefrLevel"})
  public static class StudentLanguageSkill implements Serializable {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlElement(name = "cefr-level", required = true)
    protected String cefrLevel;

    /**
     * Obtiene el valor de la propiedad language.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLanguage() {
      return language;
    }

    /**
     * Define el valor de la propiedad language.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setLanguage(String value) {
      this.language = value;
    }

    /**
     * Obtiene el valor de la propiedad cefrLevel.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getCefrLevel() {
      return cefrLevel;
    }

    /**
     * Define el valor de la propiedad cefrLevel.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setCefrLevel(String value) {
      this.cefrLevel = value;
    }

  }

}
