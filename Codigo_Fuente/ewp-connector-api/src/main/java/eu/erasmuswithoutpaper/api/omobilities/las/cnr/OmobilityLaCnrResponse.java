package eu.erasmuswithoutpaper.api.omobilities.las.cnr;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The type Omobility la cnr response.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "omobility-la-cnr-response",
    namespace = "https://github.com/erasmus-without-paper/ewp-specs-api-omobility-la-cnr/tree/stable-v1")
public class OmobilityLaCnrResponse implements Serializable {

  private static final long serialVersionUID = 2861574143369759024L;
}

