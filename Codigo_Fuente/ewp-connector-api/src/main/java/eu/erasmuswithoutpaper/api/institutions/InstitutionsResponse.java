//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.institutions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import eu.erasmuswithoutpaper.api.architecture.HTTPWithOptionalLang;
import eu.erasmuswithoutpaper.api.architecture.StringWithOptionalLang;
import eu.erasmuswithoutpaper.api.registry.OtherHeiId;
import eu.erasmuswithoutpaper.api.types.address.FlexibleAddress;
import eu.erasmuswithoutpaper.api.types.contact.Contact;


/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hei" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="other-id" type="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}OtherHeiId" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
 *                   &lt;element name="abbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
 *                   &lt;element name="website-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="logo-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS" minOccurs="0"/&gt;
 *                   &lt;element name="mobility-factsheet-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="root-ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
 *                   &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"hei"})
@XmlRootElement(name = "institutions-response")
public class InstitutionsResponse implements Serializable {

  protected List<InstitutionsResponse.Hei> hei;

  /**
   * Gets the value of the hei property.
   * 
   * <p>
   * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
   * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
   * the hei property.
   * 
   * <p>
   * For example, to add a new item, do as follows:
   * 
   * <pre>
   * getHei().add(newItem);
   * </pre>
   * 
   * 
   * <p>
   * Objects of the following type(s) are allowed in the list {@link InstitutionsResponse.Hei }
   * 
   * 
   */
  public List<InstitutionsResponse.Hei> getHei() {
    if (hei == null) {
      hei = new ArrayList<InstitutionsResponse.Hei>();
    }
    return this.hei;
  }


    /**
     * <p>
     * Clase Java para anonymous complex type.
     *
     * <p>
     * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="hei-id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="other-id" type="{https://github.com/erasmus-without-paper/ewp-specs-api-registry/tree/stable-v1}OtherHeiId" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="name" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}StringWithOptionalLang" maxOccurs="unbounded"/&gt;
     *         &lt;element name="abbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}street-address" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1}mailing-address" minOccurs="0"/&gt;
     *         &lt;element name="website-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="logo-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPS" minOccurs="0"/&gt;
     *         &lt;element name="mobility-factsheet-url" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}HTTPWithOptionalLang" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element ref="{https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1}contact" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="root-ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" minOccurs="0"/&gt;
     *         &lt;element name="ounit-id" type="{https://github.com/erasmus-without-paper/ewp-specs-architecture/blob/stable-v1/common-types.xsd}AsciiPrintableIdentifier" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
     */
    @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = {"heiId", "otherId", "name", "abbreviation", "streetAddress", "mailingAddress",
      "websiteUrl", "logoUrl", "mobilityFactsheetUrl", "courseCatalogueUrl", "contact", "rootOunitId", "ounitId"})
  public static class Hei implements Serializable {

    @XmlElement(name = "hei-id", required = true)
    protected String heiId;
    @XmlElement(name = "other-id")
    protected List<OtherHeiId> otherId;
    @XmlElement(required = true)
    protected List<StringWithOptionalLang> name;
    protected String abbreviation;
    @XmlElement(name = "street-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress streetAddress;
    @XmlElement(name = "mailing-address",
        namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-address/tree/stable-v1")
    protected FlexibleAddress mailingAddress;
    @XmlElement(name = "website-url")
    protected List<HTTPWithOptionalLang> websiteUrl;
    @XmlElement(name = "logo-url")
    @XmlSchemaType(name = "anyURI")
    protected String logoUrl;
    @XmlElement(name = "mobility-factsheet-url")
    protected List<HTTPWithOptionalLang> mobilityFactsheetUrl;
    @XmlElement(name = "course-catalogue-url")
    protected List<HTTPWithOptionalLang> courseCatalogueUrl;
    @XmlElement(namespace = "https://github.com/erasmus-without-paper/ewp-specs-types-contact/tree/stable-v1")
    protected List<Contact> contact;
    @XmlElement(name = "root-ounit-id")
    protected String rootOunitId;
    @XmlElement(name = "ounit-id")
    protected List<String> ounitId;

    /**
     * Obtiene el valor de la propiedad heiId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getHeiId() {
      return heiId;
    }

    /**
     * Define el valor de la propiedad heiId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setHeiId(String value) {
      this.heiId = value;
    }

    /**
     * Gets the value of the otherId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the otherId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getOtherId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link OtherHeiId }
     * 
     * 
     */
    public List<OtherHeiId> getOtherId() {
      if (otherId == null) {
        otherId = new ArrayList<OtherHeiId>();
      }
      return this.otherId;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link StringWithOptionalLang }
     * 
     * 
     */
    public List<StringWithOptionalLang> getName() {
      if (name == null) {
        name = new ArrayList<StringWithOptionalLang>();
      }
      return this.name;
    }

    /**
     * Obtiene el valor de la propiedad abbreviation.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getAbbreviation() {
      return abbreviation;
    }

    /**
     * Define el valor de la propiedad abbreviation.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setAbbreviation(String value) {
      this.abbreviation = value;
    }

    /**
     * 
     * The street address of the institution.
     * 
     * This is the address which should work when, for example, the user pastes it into Google Maps. If this HEI has
     * many campuses, then this address should refer to a "main" campus. If this HEI doesn't have a "main" campus, then
     * the address should simply contain the name of the institution, a city, and a country. In extreme cases, even a
     * single country entry is better than nothing. Also see a related discussion here:
     * 
     * https://github.com/erasmus-without-paper/ewp-specs-api-ounits/issues/2#issuecomment-266775582
     * 
     * This is the primary address. Note, that more addresses may be provided by using the "contact" element.
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getStreetAddress() {
      return streetAddress;
    }

    /**
     * Define el valor de la propiedad streetAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setStreetAddress(FlexibleAddress value) {
      this.streetAddress = value;
    }

    /**
     * 
     * The postal address of the institution. It MAY be the same as street-address, but doesn't have to be (e.g. it can
     * be a PO box).
     * 
     * This is the primary address. Note, that more addresses may be provided by using the "contact" element.
     * 
     * 
     * @return possible object is {@link FlexibleAddress }
     * 
     */
    public FlexibleAddress getMailingAddress() {
      return mailingAddress;
    }

    /**
     * Define el valor de la propiedad mailingAddress.
     * 
     * @param value allowed object is {@link FlexibleAddress }
     * 
     */
    public void setMailingAddress(FlexibleAddress value) {
      this.mailingAddress = value;
    }

    /**
     * Gets the value of the websiteUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the websiteUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getWebsiteUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
     * 
     * 
     */
    public List<HTTPWithOptionalLang> getWebsiteUrl() {
      if (websiteUrl == null) {
        websiteUrl = new ArrayList<HTTPWithOptionalLang>();
      }
      return this.websiteUrl;
    }

    /**
     * Obtiene el valor de la propiedad logoUrl.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getLogoUrl() {
      return logoUrl;
    }

    /**
     * Define el valor de la propiedad logoUrl.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setLogoUrl(String value) {
      this.logoUrl = value;
    }

    /**
     * Gets the value of the mobilityFactsheetUrl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the mobilityFactsheetUrl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getMobilityFactsheetUrl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link HTTPWithOptionalLang }
     * 
     * 
     */
    public List<HTTPWithOptionalLang> getMobilityFactsheetUrl() {
      if (mobilityFactsheetUrl == null) {
        mobilityFactsheetUrl = new ArrayList<HTTPWithOptionalLang>();
      }
      return this.mobilityFactsheetUrl;
    }

    public List<HTTPWithOptionalLang> getCourseCatalogueUrl() {
      return courseCatalogueUrl;
    }

    public void setCourseCatalogueUrl(List<HTTPWithOptionalLang> courseCatalogueUrl) {
      this.courseCatalogueUrl = courseCatalogueUrl;
    }

    /**
     * 
     * A list of important contacts - phones, emails, addresses.
     * 
     * In context of EWP, this will most often be mobility-related contacts, but it's not a rule. This means that server
     * developers MAY choose to include all other kinds of contacts here (such as, for example, the phone number and
     * address of the Institution's Library). Such contacts SHOULD however come AFTER the "more important" ones.
     * 
     * See https://github.com/erasmus-without-paper/ewp-specs-types-contact for more information on the purpose and
     * format of this element. Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Contact }
     * 
     * 
     */
    public List<Contact> getContact() {
      if (contact == null) {
        contact = new ArrayList<Contact>();
      }
      return this.contact;
    }

    /**
     * Obtiene el valor de la propiedad rootOunitId.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getRootOunitId() {
      return rootOunitId;
    }

    /**
     * Define el valor de la propiedad rootOunitId.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setRootOunitId(String value) {
      this.rootOunitId = value;
    }

    /**
     * Gets the value of the ounitId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to
     * the returned list will be present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for
     * the ounitId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getOunitId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     * 
     * 
     */
    public List<String> getOunitId() {
      if (ounitId == null) {
        ounitId = new ArrayList<String>();
      }
      return this.ounitId;
    }

  }

}
