//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML
// v2.2.11
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen.
// Generado el: 2021.03.16 a las 10:19:53 PM CET
//


package eu.erasmuswithoutpaper.api.iias.endpoints;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.*;


/**
 * A common parent class for all staff mobility specifications.
 *
 *
 * <p>
 * Clase Java para StaffMobilitySpecification complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="StaffMobilitySpecification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{https://github.com/erasmus-without-paper/ewp-specs-api-iias/blob/stable-v6/endpoints/get-response.xsd}MobilitySpecification"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="total-days-per-year"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;minExclusive value="0"/&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StaffMobilitySpecification", propOrder = {"totalDaysPerYear"})
@XmlSeeAlso({StaffTrainingMobilitySpec.class, StaffTeacherMobilitySpec.class})
public abstract class StaffMobilitySpecification extends MobilitySpecification implements Serializable {

  @XmlElement(name = "total-days-per-year", required = true)
  protected BigDecimal totalDaysPerYear;

  /**
   * Obtiene el valor de la propiedad totalDaysPerYear.
   * 
   * @return possible object is {@link BigDecimal }
   * 
   */
  public BigDecimal getTotalDaysPerYear() {
    return totalDaysPerYear;
  }

  /**
   * Define el valor de la propiedad totalDaysPerYear.
   * 
   * @param value allowed object is {@link BigDecimal }
   * 
   */
  public void setTotalDaysPerYear(BigDecimal value) {
    this.totalDaysPerYear = value;
  }

}
