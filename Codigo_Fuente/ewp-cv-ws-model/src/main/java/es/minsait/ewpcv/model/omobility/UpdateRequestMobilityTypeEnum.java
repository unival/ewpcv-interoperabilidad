package es.minsait.ewpcv.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Update request mobility type enum.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum UpdateRequestMobilityTypeEnum {
  OUTGOING, INCOMING;

  public static String[] names() {
    UpdateRequestMobilityTypeEnum[] types = values();
    return Arrays.stream(types).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[types.length]);
  }
}
