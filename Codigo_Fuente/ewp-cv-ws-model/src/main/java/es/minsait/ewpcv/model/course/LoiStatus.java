
package es.minsait.ewpcv.model.course;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Loi status.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LoiStatus {
  PASSED("passed"), FAILED("failed"), IN_PROGRESS("in-progress");

  private final String value;

  LoiStatus(final String v) {
    value = v;
  }

  public static String[] names() {
    final LoiStatus[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public String value() {
    return value;
  }
}
