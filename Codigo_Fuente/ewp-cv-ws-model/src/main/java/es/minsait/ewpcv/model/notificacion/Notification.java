
package es.minsait.ewpcv.model.notificacion;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Notification.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_NOTIFICATION")
@Data
public class Notification implements Serializable {

  private static final long serialVersionUID = 8130079757486819172L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "CHANGED_ELEMENT_IDS")
  private String changedElementIds;

  @Column(name = "HEI_ID")
  private String heiId;

  @Column(name = "NOTIFICATION_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date notificationDate;

  @Column(name = "TYPE")
  private NotificationTypes type;

  @Column(name = "CNR_TYPE")
  private CnrType cnrType;

  @Column(name = "RETRIES")
  private Integer retries;

  @Column(name = "PROCESSING")
  private Boolean processing;

  @Column(name = "OWNER_HEI")
  private String ownerHeiId;

  @Column(name = "PROCESSING_DATE")
  private Date processingDate;

  @Column(name = "SHOULD_RETRY")
  private Boolean shouldRetry;

  @Column(name = "LAST_ERROR")
  private String lastError;
}
