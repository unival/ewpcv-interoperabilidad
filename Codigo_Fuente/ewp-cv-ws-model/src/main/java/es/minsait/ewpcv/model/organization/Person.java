
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;


/**
 * The type Person.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_PERSON")
@Data
public class Person implements Serializable {

  private static final long serialVersionUID = -8150468720088027751L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "BIRTH_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date birthDate;

  @Column(name = "COUNTRY_CODE")
  private String countryCode;

  @Column(name = "FIRST_NAMES")
  private String firstNames;

  @Convert(converter = GenderEnumConverter.class)
  @Column(name = "GENDER")
  private Gender gender;

  @Column(name = "LAST_NAME")
  private String lastName;
}
