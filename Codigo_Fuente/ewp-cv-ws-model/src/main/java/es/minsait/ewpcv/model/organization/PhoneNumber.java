
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * Represetation of phone numbers and fax numbers.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_PHONE_NUMBER")
@Data
public class PhoneNumber implements Serializable {

  private static final long serialVersionUID = -4026216272930862157L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "E164")
  private String e164;

  @Column(name = "EXTENSION_NUMBER")
  private String extensionNumber;

  @Column(name = "OTHER_FORMAT")
  private String otherFormat;

}
