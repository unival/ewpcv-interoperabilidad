package es.minsait.ewpcv.model.dictionary;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Dictionary.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_DICTIONARY")
@Data
public class Dictionary implements Serializable {

  private static final long serialVersionUID = -8797038690427623440L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
          strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "ID", unique = true, nullable = false)
  private String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "CODE")
  private String code;

  @Column(name = "DESCRIPTION")
  private String description;

  @Column(name = "DICTIONARY_TYPE")
  private String dictionaryType;

  @Column(name = "CALL_YEAR")
  private String callYear;
}
