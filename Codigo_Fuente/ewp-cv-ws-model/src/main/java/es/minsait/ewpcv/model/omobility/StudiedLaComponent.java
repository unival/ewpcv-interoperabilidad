
package es.minsait.ewpcv.model.omobility;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Studied la component.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_STUDIED_LA_COMPONENT")
@Data
public class StudiedLaComponent implements Serializable {

  private static final long serialVersionUID = -7076078259311168993L;

  @Id
  @Column(name = "LEARNING_AGREEMENT_ID")
  private String learningAgreementId;

  @Column(name = "STUDIED_LA_COMPONENT_ID")
  private String studiedLaComponentId;

  @Column(name = "LEARNING_AGREEMENT_REVISION")
  private int learningAgreementRevision;

}
