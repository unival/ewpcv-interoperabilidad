package es.minsait.ewpcv.model.notificacion;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Event.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_EVENT")
@Data
public class Event implements Serializable {

  private static final long serialVersionUID = 8130079757486819172L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Column(name = "EVENT_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date eventDate;

  @Column(name = "TRIGGERING_HEI")
  private String triggeringHei;

  @Column(name = "OWNER_HEI")
  private String ownerHei;

  @Column(name = "EVENT_TYPE")
  private String eventType;

  @Column(name = "CHANGED_ELEMENT_ID")
  private String changedElementIds;

  @Column(name = "ELEMENT_TYPE")
  private NotificationTypes elementType;

  @Column(name = "OBSERVATIONS")
  private String observations;

  @Column(name = "STATUS")
  private String status;

  @Column(name = "REQUEST_PARAMS")
  private String requestParams;

  @Column(name = "MESSAGE")
  private byte[] message;

  @Column(name = "X_REQUEST_ID")
  private String xRequestId;

  @Column(name = "REQUEST_BODY")
  private byte[] requestBody;
}
