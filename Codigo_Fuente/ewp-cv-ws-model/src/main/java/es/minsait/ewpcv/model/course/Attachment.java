package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Attachment.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_ATTACHMENT")
@Getter
@Setter
@NoArgsConstructor
public class Attachment implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ATTACH_TYPE")
  private AttachmentType type;

  @Column(name = "EXTENSION")
  private byte[] extension;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "ATTACHMENT_ID", nullable = false)
  private List<AttachmentContent> contents;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_ATTACHMENT_TITLE",
      joinColumns = @JoinColumn(name = "ATTACHMENT_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ID", referencedColumnName = "ID"))
  private List<LanguageItem> title;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_ATTACHMENT_DESCRIPTION",
      joinColumns = @JoinColumn(name = "ATTACHMENT_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ID", referencedColumnName = "ID"))
  private List<LanguageItem> description;
}
