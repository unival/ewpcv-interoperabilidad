package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.course.AcademicTerm;
import es.minsait.ewpcv.model.course.Credit;
import es.minsait.ewpcv.model.course.LearningOpportunityInstance;
import es.minsait.ewpcv.model.course.LearningOpportunitySpecification;
import lombok.Data;

/**
 * The type La component.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LA_COMPONENT")
@Data
public class LaComponent implements Serializable {

  private static final long serialVersionUID = -7076078259311168993L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  private String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "ACADEMIC_TERM_DISPLAY_NAME")
  private AcademicTerm academicTermDisplayName;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_COMPONENT_CREDITS",
      joinColumns = @JoinColumn(name = "COMPONENT_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "CREDITS_ID", referencedColumnName = "ID"))
  private List<Credit> credits;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "LOI_ID")
  private LearningOpportunityInstance loiId;

  @Column(name = "LOS_CODE")
  private String losCode;

  @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
  @JoinColumn(name = "LOS_ID")
  private LearningOpportunitySpecification losId;

  @Column(name = "STATUS")
  private LearningAgreementComponentStatus status;

  @Column(name = "TITLE")
  private String title;

  @Column(name = "REASON_CODE")
  private LaComponentReasonCode reasonCode;

  @Column(name = "REASON_TEXT")
  private String reasonText;

  @Column(name = "LA_COMPONENT_TYPE")
  private LaComponentType laComponentType;

  @Column(name = "RECOGNITION_CONDITIONS")
  private String recognitionConditions;

  @Column(name = "SHORT_DESCRIPTION")
  private String shortDescription;

  @Override
  public String toString() {
    return "LaComponent{" + "id='" + id + '\'' + ", version=" + version + ", losCode='" + losCode + '\'' + ", status="
        + status + ", title='" + title + '\'' + ", reasonCode='" + reasonCode + '\'' + ", reasonText='" + reasonText
        + '\'' + ", laComponentType=" + laComponentType + ", recognitionConditions='" + recognitionConditions + '\''
        + ", shortDescription='" + shortDescription + '\'' + '}';
  }
}
