package es.minsait.ewpcv.model.iia;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The type StatisticsIia.
 *
 @author Iván Lanciano (ilanciano@minsait.com)
 */
@Entity
@Table(name = "EWPCV_STATS_IIA")
@Data
public class StatisticsIia implements Serializable {

    @Id
    @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
    @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
            strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
    @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
    String Id;

    @Version
    @Column(name = "VERSION")
    private Integer version;

    @Column(name = "FETCHABLE")
    private Integer fetchable;

    @Column(name = "LOCAL_UNAPPR_PARTNER_APPR")
    private Integer localUnappr_partnerAppr;

    @Column(name = "LOCAL_APPR_PARTNER_UNAPPR")
    private Integer localAppr_partnerUnappr;

    @Column(name = "BOTH_APPROVED")
    private Integer bothApproved;

    @Column(name = "DUMP_DATE")
    private Date dumpDate;


}
