package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Subject area.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_SUBJECT_AREA")
@Data
public class SubjectArea implements Serializable {

  private static final long serialVersionUID = 4862358824218462779L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID")
  private String id;

  @Column(name = "ISCED_CODE")
  private String iscedCode;

  @Column(name = "ISCED_CLARIFICATION")
  private String iscedClarification;

}
