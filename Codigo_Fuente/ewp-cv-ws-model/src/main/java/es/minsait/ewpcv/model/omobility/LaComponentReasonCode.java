package es.minsait.ewpcv.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum La component reason code.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LaComponentReasonCode {
  NOT_AVAILABLE("not-available"), LANGUAGE_MISMATCH("language-mismatch"), TIMETABLE_CONFLICT(
      "timetable-conflict"), SUBTITUTING_DELETED("substituting-deleted"), EXTENDING_MOBILITY(
          "extending-mobility"), ADDING_VIRTUAL_COMPONENT("adding-virtual-component");

  private String value;

  LaComponentReasonCode(String value) {
    this.value = value;
  }

  public static String[] names() {
    LaComponentReasonCode[] codes = values();
    return Arrays.stream(codes).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[codes.length]);
  }

  public static LaComponentReasonCode fromValue(String value) {
    LaComponentReasonCode[] codes = values();
    return Arrays.stream(codes).filter(s -> s.value().equalsIgnoreCase(value)).findFirst().orElse(null);
  }

  public String value() {
    return value;
  }
}
