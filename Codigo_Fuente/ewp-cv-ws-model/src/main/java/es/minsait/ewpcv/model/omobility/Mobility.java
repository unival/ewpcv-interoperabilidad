
package es.minsait.ewpcv.model.omobility;

import org.apache.johnzon.mapper.JohnzonConverter;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.StandardDateConverter;
import es.minsait.ewpcv.model.course.AcademicTerm;
import es.minsait.ewpcv.model.iia.MobilityType;
import es.minsait.ewpcv.model.organization.Contact;
import es.minsait.ewpcv.model.organization.MobilityParticipant;
import es.minsait.ewpcv.model.organization.OrganizationUnit;
import lombok.Data;

/**
 * The type Mobility.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_MOBILITY")
@Data
public class Mobility implements Serializable {

  private static final long serialVersionUID = 4168283348724389903L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
      strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "MOBILITY_ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String mobilityId;

  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
          strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "ID")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ACTUAL_ARRIVAL_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date actualArrivalDate;

  @Column(name = "ACTUAL_DEPARTURE_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date actualDepartureDate;

  @Column(name = "COOPERATION_CONDITION_ID")
  private String cooperationConditionId;

  @Column(name = "IIA_ID")
  private String iiaId;

  @Column(name = "RECEIVING_IIA_ID")
  private String receivingIiaId;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "ISCED_CODE", referencedColumnName = "ID")
  private SubjectArea iscedCode;

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH},
      fetch = FetchType.LAZY)
  @JoinColumn(name = "MOBILITY_PARTICIPANT_ID")
  private MobilityParticipant mobilityParticipantId;

  @Column(name = "MOBILITY_REVISION", nullable = false)
  private Integer mobilityRevision;

  @Column(name = "PLANNED_ARRIVAL_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date plannedArrivalDate;

  @Column(name = "PLANNED_DEPARTURE_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date plannedDepartureDate;

  @Column(name = "RECEIVING_INSTITUTION_ID")
  private String receivingInstitutionId;

  @Column(name = "RECEIVING_ORGANIZATION_UNIT_ID")
  private String receivingOrganizationUnitId;

  @Column(name = "SENDING_INSTITUTION_ID")
  private String sendingInstitutionId;

  @Column(name = "SENDING_ORGANIZATION_UNIT_ID")
  private String sendingOrganizationUnitId;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_MOBILITY_LA",
      joinColumns = {@JoinColumn(name = "MOBILITY_ID", referencedColumnName = "MOBILITY_ID")},
      inverseJoinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")})
  private List<LearningAgreement> learningAgreement;

  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH},
      fetch = FetchType.LAZY)
  @JoinColumn(name = "MOBILITY_TYPE_ID", referencedColumnName = "ID")
  private MobilityType mobilityType;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "EWPCV_MOBILITY_LANG_SKILL",
      joinColumns = {@JoinColumn(name = "MOBILITY_ID", referencedColumnName = "MOBILITY_ID")},
      inverseJoinColumns = {@JoinColumn(name = "LANGUAGE_SKILL_ID", referencedColumnName = "ID")})
  private List<LanguageSkill> languageSkill;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "SENDER_CONTACT_ID", referencedColumnName = "ID")
  private Contact senderContactPerson;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "SENDER_ADMV_CONTACT_ID", referencedColumnName = "ID")
  private Contact senderAdmvContactPerson;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "RECEIVER_CONTACT_ID", referencedColumnName = "ID")
  private Contact receiverContactPerson;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "RECEIVER_ADMV_CONTACT_ID", referencedColumnName = "ID")
  private Contact receiverAdmvContactPerson;

  @Column(name = "REMOTE_MOBILITY_ID")
  private String remoteMobilityId;

  @Column(name = "MOBILITY_COMMENT")
  private String comment;

  @Column(name = "EQF_LEVEL_NOMINATION")
  private Long eqfLevelNomination;

  @Column(name = "EQF_LEVEL_DEPARTURE")
  private Long eqfLevelDeparture;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "ACADEMIC_TERM_ID", referencedColumnName = "ID")
  private AcademicTerm academicTerm;

  @Column(name = "STATUS_OUTGOING")
  private MobilityStatus statusOutgoing;

  @Column(name = "STATUS_INCOMING")
  private MobilityStatus statusIncomming;

  @JohnzonConverter(StandardDateConverter.class)
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "MODIFY_DATE")
  private Date modifyDate;

  @Override
  public String toString() {
    return "Mobility{" + "id='" + id + '\'' + ", version=" + version + '}';
  }

  @PrePersist
  public void prePersist() {
    this.modifyDate = new Date();
  }

  @PreUpdate
  public void preUpdate() {
    this.modifyDate = new Date();
  }

}
