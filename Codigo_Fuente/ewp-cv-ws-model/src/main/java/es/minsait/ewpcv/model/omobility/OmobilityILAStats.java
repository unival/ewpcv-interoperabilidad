package es.minsait.ewpcv.model.omobility;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "INCOMING_LA_STATS")
@Data
@IdClass(OmobilityILAStats.class)
public class OmobilityILAStats implements Serializable {

    @Column(name = "RECEIVING_INSTITUTION")
    private String receivingInstitution;

    @Id
    @Column(name = "START_YEAR")
    private String academicYearStart;

    @Column(name = "END_YEAR")
    private String academicYearEnd;

    @Column(name = "TOTAL")
    private Integer totalNumber;

    @Column(name = "LAST_PENDING")
    private Integer lastWait;

    @Column(name = "LAST_APPROVED")
    private Integer lastAppr;

    @Column(name = "LAST_REJECTED")
    private Integer lastRej;

    @Column(name = "APPROVAL_MODIFIED")
    private Integer approvalModif;

    @Column(name = "APPROVAL_UNMODIFIED")
    private Integer approvalUnModif;

    @Column(name = "SOME_VERSION_APPROVED")
    private Integer sameVersionAppr;

}
