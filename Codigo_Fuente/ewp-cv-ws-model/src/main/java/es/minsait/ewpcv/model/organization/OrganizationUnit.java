
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Organization unit.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_ORGANIZATION_UNIT")
@Data
public class OrganizationUnit implements Serializable {

  private static final long serialVersionUID = -257826672831810231L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
          strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name="INTERNAL_ID",nullable = false)
  private String internalId;

  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ABBREVIATION")
  private String abbreviation;

  @Column(name = "LOGO_URL")
  private String logoUrl;

  @Column(name = "ORGANIZATION_UNIT_CODE")
  private String organizationUnitCode;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "FACT_SHEET")
  private FactSheet factSheet;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_OU_INF_ITEM",
      joinColumns = @JoinColumn(name = "ORGANIZATION_UNIT_ID", referencedColumnName = "INTERNAL_ID"),
      inverseJoinColumns = @JoinColumn(name = "INFORMATION_ID", referencedColumnName = "ID"))
  private List<InformationItem> informationItems;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_ORGANIZATION_UNIT_NAME",
      joinColumns = @JoinColumn(name = "ORGANIZATION_UNIT_ID", referencedColumnName = "INTERNAL_ID"),
      inverseJoinColumns = @JoinColumn(name = "NAME_ID", referencedColumnName = "ID"))
  private List<LanguageItem> name;

  @Column(name = "PARENT_OUNIT_ID")
  private String parentOUnitId;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_OU_REQUIREMENT_INFO",
      joinColumns = @JoinColumn(name = "OU_ID", referencedColumnName = "INTERNAL_ID"),
      inverseJoinColumns = @JoinColumn(name = "REQUERIMENT_ID", referencedColumnName = "ID"))
  private List<RequirementsInfo> additionalRequirements;

  @Column(name = "IS_TREE_STRUCTURE")
  private Boolean isTreeStructure;

  @Column(name = "IS_EXPOSED")
  private Boolean isExposed;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "PRIMARY_CONTACT_DETAIL_ID", referencedColumnName = "ID")
  private ContactDetails primaryContactDetails;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_OUNIT_FACT_SHEET_URL",
          joinColumns = @JoinColumn(name = "OUNIT_ID", referencedColumnName = "INTERNAL_ID"),
          inverseJoinColumns = @JoinColumn(name = "URL_ID", referencedColumnName = "ID"))
  private List<LanguageItem> factsheetUrls;

}
