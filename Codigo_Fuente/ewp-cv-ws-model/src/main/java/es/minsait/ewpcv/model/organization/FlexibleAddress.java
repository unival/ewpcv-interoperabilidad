
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Flexible address.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_FLEXIBLE_ADDRESS")
@Data
public class FlexibleAddress implements Serializable {

  private static final long serialVersionUID = -2228016055343417318L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "BUILDING_NAME")
  private String buildingName;

  @Column(name = "BUILDING_NUMBER")
  private String buildingNumber;

  @Column(name = "COUNTRY")
  private String country;

  @Column(name = "FLOOR")
  private String floor;

  @Column(name = "LOCALITY")
  private String locality;

  @Column(name = "POST_OFFICE_BOX")
  private String postOfficeBox;

  @Column(name = "POSTAL_CODE")
  private String postalCode;

  @Column(name = "REGION")
  private String region;

  @Column(name = "STREET_NAME")
  private String streetName;

  @Column(name = "UNIT")
  private String unit;

  @ElementCollection
  @CollectionTable(name = "EWPCV_FLEXAD_RECIPIENT_NAME")
  private List<String> recipientName;

  @ElementCollection
  @CollectionTable(name = "EWPCV_FLEXIBLE_ADDRESS_LINE")
  private List<String> addressLine;

  @ElementCollection
  @CollectionTable(name = "EWPCV_FLEXAD_DELIV_POINT_COD")
  private List<String> deliveryPointCode;

}
