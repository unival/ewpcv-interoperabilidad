package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Report.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_REPORT")
@Getter
@Setter
@NoArgsConstructor
public class Report implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ISSUE_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date issueDate;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "REPORT_ID", nullable = false)
  private List<LearningOpportunityInstance> lois;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_REPORT_ATT", joinColumns = @JoinColumn(name = "REPORT_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ATTACHMENT_ID", referencedColumnName = "ID"))
  private List<Attachment> attachments;
}
