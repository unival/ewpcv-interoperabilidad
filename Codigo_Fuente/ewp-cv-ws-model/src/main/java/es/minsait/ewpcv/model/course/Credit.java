
package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Credit.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_CREDIT")
@Data
public class Credit implements Serializable {

  private static final long serialVersionUID = 4000143547761837489L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "CREDIT_LEVEL")
  private String level;

  @Column(name = "SCHEME")
  private String scheme;

  @Column(name = "CREDIT_VALUE", precision = 5, scale = 1)
  private BigDecimal value;

}
