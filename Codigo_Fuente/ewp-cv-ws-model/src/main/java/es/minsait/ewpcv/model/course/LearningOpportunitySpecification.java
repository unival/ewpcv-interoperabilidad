
package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Data;

/**
 * The type Learning opportunity specification.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LOS")
@Data
public class LearningOpportunitySpecification implements Serializable {

  private static final long serialVersionUID = 8971620442928886996L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
      strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "EQF_LEVEL")
  private Byte eqfLevel;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "ISCEDF")
  private String iscedf;

  @Column(name = "LOS_CODE")
  private String losCode;

  @Column(name = "ORGANIZATION_UNIT_ID")
  private String organizationUnitId;

  @Column(name = "SUBJECT_AREA")
  private String subjectArea;

  @Column(name = "TOP_LEVEL_PARENT")
  private boolean topLevelParent;

  @Column(name = "TYPE")
  private LearningOpportunitySpecificationType type;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_LOS_NAME", joinColumns = @JoinColumn(name = "LOS_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "NAME_ID", referencedColumnName = "ID"))
  private List<LanguageItem> name;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_LOS_URLS", joinColumns = @JoinColumn(name = "LOS_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "URL_ID", referencedColumnName = "ID"))
  private List<LanguageItem> url;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_LOS_DESCRIPTION", joinColumns = @JoinColumn(name = "LOS_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "DESCRIPTION_ID", referencedColumnName = "ID"))
  private List<LanguageItem> description;

  @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH},
      fetch = FetchType.LAZY, orphanRemoval = false)
  @JoinTable(name = "EWPCV_LOS_LOS", joinColumns = @JoinColumn(name = "LOS_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "OTHER_LOS_ID", referencedColumnName = "ID"))
  private List<LearningOpportunitySpecification> learningOpportunitySpecifications;

  @Column(name = "EXTENSION")
  private byte[] extension;

}
