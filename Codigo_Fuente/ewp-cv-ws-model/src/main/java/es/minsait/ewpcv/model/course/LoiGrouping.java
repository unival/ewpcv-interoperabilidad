package es.minsait.ewpcv.model.course;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Loi grouping.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LOI_GROUPING")
@Getter
@Setter
@NoArgsConstructor
public class LoiGrouping implements Serializable {
  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @OneToOne
  @JoinColumn(name = "LOI_ID", referencedColumnName = "ID")
  private LearningOpportunityInstance loi;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "GROUP_ID")
  private Group group;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "GROUP_TYPE_ID")
  private GroupType groupType;
}
