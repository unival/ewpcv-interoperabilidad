
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Institution.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_INSTITUTION")
@Data
public class Institution implements Serializable {

  private static final long serialVersionUID = 1257132030785463254L;


  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ABBREVIATION")
  private String abbreviation;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "LOGO_URL")
  private String logoUrl;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "FACT_SHEET")
  private FactSheet factSheet;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_INST_INF_ITEM",
      joinColumns = @JoinColumn(name = "INSTITUTION_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "INFORMATION_ID", referencedColumnName = "ID"))
  private List<InformationItem> informationItems;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_INSTITUTION_NAME")
  private List<LanguageItem> name;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_INST_ORG_UNIT",
      inverseJoinColumns = @JoinColumn(name = "ORGANIZATION_UNITS_ID", referencedColumnName = "INTERNAL_ID"),
      joinColumns = @JoinColumn(name = "INSTITUTION_ID", referencedColumnName = "ID"))
  private List<OrganizationUnit> organizationUnits;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_INS_REQUIREMENTS",
      joinColumns = @JoinColumn(name = "INSTITUTION_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "REQUIREMENT_ID", referencedColumnName = "ID"))
  private List<RequirementsInfo> additionalRequirements;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "PRIMARY_CONTACT_DETAIL_ID", referencedColumnName = "ID")
  private ContactDetails primaryContactDetails;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_INST_FACT_SHEET_URL",
          joinColumns = @JoinColumn(name = "INSTITUTION_ID", referencedColumnName = "ID"),
          inverseJoinColumns = @JoinColumn(name = "URL_ID", referencedColumnName = "ID"))
  private List<LanguageItem> factsheetUrls;
}
