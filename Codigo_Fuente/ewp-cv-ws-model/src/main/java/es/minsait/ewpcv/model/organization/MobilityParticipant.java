package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Mobility participant.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_MOBILITY_PARTICIPANT")
@Data
public class MobilityParticipant implements Serializable {

  private static final long serialVersionUID = -4567983444147659307L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;
  @Column(name = "GLOBAL_ID")
  String globalId;
  @Version
  @Column(name = "VERSION")
  private Integer version;
  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "CONTACT_ID", referencedColumnName = "ID")
  private Contact contactDetails;
}
