package es.minsait.ewpcv.model.iia;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.Contact;
import lombok.Data;

/**
 * The type Iia partner contacts.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_IIA_PARTNER_CONTACTS")
@Data
@IdClass(IiaPartnerContactsRelationId.class)
public class IiaPartnerContacts {

  @Id
  @Column(name = "IIA_PARTNER_ID")
  private String iiaId;

  @Id
  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "CONTACTS_ID", referencedColumnName = "ID")
  private Contact contactsId;

}
