package es.minsait.ewpcv.model.omobility;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The type StatisticsOla.
 *
 @author Iván Lanciano (ilanciano@minsait.com)
 */
@Entity
@Table(name = "EWPCV_STATS_OLA")
@Data
//@IdClass(StatisticsOla.class)
public class StatisticsOla  implements Serializable {

    @Id
    @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
    @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
            strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
    @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
    String Id;

    @Version
    @Column(name = "VERSION")
    private Integer version;

    @Column(name = "START_YEAR")
    private String academicYearStart;

    @Column(name = "END_YEAR")
    private String academicYearEnd;

    @Column(name = "TOTAL")
    private Integer totalOutgoingNumber;

    @Column(name = "APPROVAL_UNMODIFIED")
    private Integer approvalUnModif;

    @Column(name = "APPROVAL_MODIFIED")
    private Integer approvalModif;

    @Column(name = "LAST_APPROVED")
    private Integer lastAppr;

    @Column(name = "LAST_REJECTED")
    private Integer lastRej;

    @Column(name = "LAST_PENDING")
    private Integer lastWait;

    @Column(name = "SENDING_INSTITUTION")
    private String sendingInstitution;

    @Column(name = "DUMP_DATE")
    private Date dumpDate;

}
