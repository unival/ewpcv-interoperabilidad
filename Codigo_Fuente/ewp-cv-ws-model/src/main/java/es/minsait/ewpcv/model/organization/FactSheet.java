
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Fact sheet.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_FACT_SHEET")
@Data
public class FactSheet implements Serializable {

  private static final long serialVersionUID = 2417309357032483454L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "CONTACT_DETAILS_ID", referencedColumnName = "ID")
  private ContactDetails contactDetails;

  @Column(name = "DECISION_WEEKS_LIMIT")
  private Integer decisionWeeksLimit;

  @Column(name = "TOR_WEEKS_LIMIT")
  private Integer torWeeksLimit;

  @Column(name = "NOMINATIONS_AUTUM_TERM")
  private Date nominationsAutumnTerm;

  @Column(name = "NOMINATIONS_SPRING_TERM")
  private Date nominationsSpringTerm;

  @Column(name = "APPLICATION_AUTUM_TERM")
  private Date applicationAutumnTerm;

  @Column(name = "APPLICATION_SPRING_TERM")
  private Date applicationSpringTerm;

}
