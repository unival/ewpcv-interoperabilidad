
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Contact.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_CONTACT")
@Data
public class Contact implements Serializable {

  private static final long serialVersionUID = -3048561632288824769L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ORGANIZATION_UNIT_ID")
  private String organizationUnitId;

  @Column(name = "CONTACT_ROLE")
  private String role;

  @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "CONTACT_DETAILS_ID", referencedColumnName = "ID")
  private ContactDetails contactDetails;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "PERSON_ID")
  private Person person;

  @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_CONTACT_NAME")
  private List<LanguageItem> name;

  @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_CONTACT_DESCRIPTION")
  private List<LanguageItem> description;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

}
