package es.minsait.ewpcv.model.dictionary;

/**
 * The enum Dictionary type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum DictionaryType {
  KEY_ACTIONS("KEY_ACTIONS"), ACTION_TYPES("ACTION_TYPES"), COUNTRIES("COUNTRIES"), COUNTRY_REGIONS(
      "COUNTRY_REGIONS"), COUNTRY_GROUPS("COUNTRY_GROUPS"), DISTANCE_BANDS("DISTANCE_BANDS"), EDUCATION_FIELDS(
          "EDUCATIONS_FILEDS"), EDUCATION_LEVELS("EDUCATION_LEVELS"), FIELD_GROUPS("FIELD_GROUPS"), LENGUAGE_GROUPS(
              "LENGUAGE_GROUPS"), LANGUAGES("LANGUAGES"), ORGANIZATION_TYPES("ORGANIZATION_TYPES"), SENIORITIES(
                  "SENIORITIES"), TRAINING_TYPES("TRAINING_TYPES"), WORKING_CATEGORIES("WORKING_CATEGORIES");

  private String type;

  DictionaryType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
