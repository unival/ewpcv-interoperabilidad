package es.minsait.ewpcv.model.iia;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IIAS_STATS")
@Data
public class IiaStats implements Serializable {


    @Id
    @Column(name = "FETCHABLES")
    private Integer fetchable;

    @Column(name = "PARTNER_APPROVED")
    private Integer localUnappr_partnerAppr;

    @Column(name = "LOCAL_APPROVED")
    private Integer localAppr_partnerUnappr;

    @Column(name = "BOTH_APPROVED")
    private Integer bothApproved;

}
