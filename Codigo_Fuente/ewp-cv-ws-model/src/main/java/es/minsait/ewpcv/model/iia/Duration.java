
package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Duration.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_DURATION")
@Data
public class Duration implements Serializable {

  private static final long serialVersionUID = -426878123854498190L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "NUMBERDURATION")
  private BigDecimal numberduration;

  @Column(name = "UNIT")
  private DurationUnitVariants unit;

}
