package es.minsait.ewpcv.model.converter;

import javax.annotation.Nullable;
import javax.persistence.AttributeConverter;

import es.minsait.ewpcv.model.enums.SiNoEnum;

/**
 * The type Si no converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class SiNoConverter implements AttributeConverter<SiNoEnum, String> {

  private static final String CONTENT_NO = "N";
  private static final String CONTENT_SI = "S";

  @Override
  @Nullable
  public String convertToDatabaseColumn(SiNoEnum attribute) {
    if (attribute != null) {
      switch (attribute) {
        case SI:
          return CONTENT_SI;
        case NO:
          return CONTENT_NO;
        default:
          return null;

      }
    }
    return null;
  }

  @Override
  @Nullable
  public SiNoEnum convertToEntityAttribute(String dbData) {
    if (dbData != null) {
      switch (dbData) {
        case CONTENT_SI:
          return SiNoEnum.SI;
        case CONTENT_NO:
          return SiNoEnum.NO;
        default:
          throw new IllegalArgumentException("Valor de BBDD no controlado " + dbData);
      }
    }
    return null;
  }
}
