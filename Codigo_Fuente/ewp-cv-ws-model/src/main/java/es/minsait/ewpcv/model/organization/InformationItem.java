
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Information item.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_INFORMATION_ITEM")
@Data
public class InformationItem implements Serializable {

  private static final long serialVersionUID = -8898882055121273422L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "TYPE")
  private String type;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "CONTACT_DETAIL_ID")
  private ContactDetails contactDetails;
}
