
package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Requirements info.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_REQUIREMENTS_INFO")
@Data
public class RequirementsInfo implements Serializable {

  private static final long serialVersionUID = -8898882055121273422L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  private String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "TYPE")
  private String type;

  @Column(name = "NAME")
  private String name;

  @Column(name = "DESCRIPTION")
  private String descripcion;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "CONTACT_DETAIL_ID")
  private ContactDetails contactDetails;
}
