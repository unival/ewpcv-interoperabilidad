package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Language skill.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LANGUAGE_SKILL")
@Data
public class LanguageSkill implements Serializable {

  private static final long serialVersionUID = 7820300548143672763L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Column(name = "LANGUAGE")
  private String language;

  @Column(name = "CEFR_LEVEL")
  private String cefrLevel;

}
