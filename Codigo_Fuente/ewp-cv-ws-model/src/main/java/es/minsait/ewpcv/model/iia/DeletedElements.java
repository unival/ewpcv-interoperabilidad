package es.minsait.ewpcv.model.iia;

import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "EWPCV_DELETED_ELEMENTS")
@Data
public class DeletedElements implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
    private String id;
    @Column(name = "ELEMENT_ID")
    private String elementId;
    @Column(name = "ELEMENT_TYPE")
    private NotificationTypes elementType;
    @Column(name = "OWNER_SCHAC")
    private String ownerSchac;
    @Column(name = "DELETION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletionDate;

}
