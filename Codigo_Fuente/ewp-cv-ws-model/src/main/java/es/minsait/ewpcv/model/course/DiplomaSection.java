package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Diploma section.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_DIPLOMA_SECTION")
@Getter
@Setter
@NoArgsConstructor
public class DiplomaSection implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "TITLE")
  private String title;

  @Column(name = "CONTENT")
  private byte[] content;

  @Column(name = "SECTION_NUMBER")
  private Integer number;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "SECTION_ID", nullable = false)
  private List<DiplomaAdditionalInfo> additionalInfo;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_DIPLOMA_SEC_ATTACH",
      joinColumns = @JoinColumn(name = "SECTION_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ATTACHMENT_ID", referencedColumnName = "ID"))
  private List<Attachment> attachments;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_DIPLOMA_SECTION_SECTION",
      joinColumns = @JoinColumn(name = "ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "SECTION_ID", referencedColumnName = "ID"))
  private List<DiplomaSection> sections;

}
