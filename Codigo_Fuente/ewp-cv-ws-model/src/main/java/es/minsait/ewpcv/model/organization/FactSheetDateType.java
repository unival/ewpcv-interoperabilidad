
package es.minsait.ewpcv.model.organization;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Fact sheet date type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum FactSheetDateType {
  NOMINATIONS_AUTUMN, NOMINATIONS_SPRING, APPLICATIONS_AUTUMN, APPLICATIONS_SPRING;

  public static String[] types() {
    FactSheetDateType[] types = values();
    return Arrays.stream(types).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[types.length]);
  }
}
