package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import es.minsait.ewpcv.model.omobility.SubjectArea;
import lombok.Data;

/**
 * The type Cooperation condition subject area.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_COOPCOND_SUBAR")
@Data
public class CooperationConditionSubjectArea implements Serializable {
  private static final long serialVersionUID = -3191897428811957932L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  private String id;

  @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
  @JoinColumn(name = "ISCED_CODE", referencedColumnName = "ID")
  private SubjectArea subjectArea;

  @Column(name = "ORDER_INDEX_SUB_AR")
  private Integer orderIndexSubAr;

}
