package es.minsait.ewpcv.model.dictionary;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Institution identifiers.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_INSTITUTION_IDENTIFIERS")
@Data
public class InstitutionIdentifiers implements Serializable {

  private static final long serialVersionUID = -2907208300490717835L;

  @Id
  @Column(name = "SCHAC", unique = true, nullable = false)
  private String schac;

  @Column(name = "PREVIOUS_SCHAC")
  private String previousSchac;

  @Column(name = "PIC")
  private String pic;

  @Column(name = "ERASMUS")
  private String erasmus;

  @Column(name = "EUC")
  private String euc;

  @Column(name = "ERASMUS_CHARTER")
  private String erasmusCharter;

  @Column(name = "INSTITUTION_NAMES")
  private String institutionNames;

  @Column(name = "OTHER_IDS")
  private String otherIds;

}
