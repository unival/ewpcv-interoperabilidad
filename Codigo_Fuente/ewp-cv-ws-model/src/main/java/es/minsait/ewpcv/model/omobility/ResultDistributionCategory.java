
package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Result distribution category.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_RESULT_DIST_CATEGORY")
@Data
public class ResultDistributionCategory implements Serializable {

  private static final long serialVersionUID = -7173459721532857368L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "DISTRUBTION_COUNT")
  private BigInteger count;

  @Column(name = "LABEL")
  private String label;

}
