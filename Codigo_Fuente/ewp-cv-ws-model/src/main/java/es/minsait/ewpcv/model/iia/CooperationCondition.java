
package es.minsait.ewpcv.model.iia;

import org.apache.johnzon.mapper.JohnzonConverter;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.StandardDateConverter;
import lombok.Data;

/**
 * The type Cooperation condition.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_COOPERATION_CONDITION")
@Data
public class CooperationCondition implements Serializable {

  private static final long serialVersionUID = -2684795206773117429L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  private String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "END_DATE")
  @JohnzonConverter(StandardDateConverter.class)
  @Temporal(TemporalType.TIMESTAMP)
  private Date endDate;

  @ElementCollection
  @CollectionTable(name = "EWPCV_COOPCOND_EQFLVL")
  @OrderBy("eqf_level")
  private List<Byte> eqfLevel;

  @Column(name = "START_DATE")
  @JohnzonConverter(StandardDateConverter.class)
  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "DURATION_ID", referencedColumnName = "ID")
  private Duration duration;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "MOBILITY_NUMBER_ID", referencedColumnName = "ID")
  private MobilityNumber mobilityNumber;

  @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH},
      fetch = FetchType.LAZY)
  @JoinColumn(name = "MOBILITY_TYPE_ID", referencedColumnName = "ID")
  private MobilityType mobilityType;

  @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "RECEIVING_PARTNER_ID")
  private IiaPartner receivingPartner;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "SENDING_PARTNER_ID")
  private IiaPartner sendingPartner;

  @Column(name = "OTHER_INFO")
  private String otherInfo;

  @Column(name = "BLENDED")
  private Boolean blended;

  @Column(name = "TERMINATED")
  private Boolean terminated;

  @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "COOPERATION_CONDITION_ID", referencedColumnName = "ID", nullable = false)
  @OrderBy("orderIndexLang")
  private List<CooperationConditionSubjAreaLangSkill> languageSkill;

  @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "COOPERATION_CONDITION_ID", referencedColumnName = "ID", nullable = false)
  @OrderBy("orderIndexSubAr")
  private List<CooperationConditionSubjectArea> subjectAreaList;

  @Column(name = "ORDER_INDEX")
  private Integer orderIndex;

  @Column(name = "IIA_APPROVAL_ID")
  private String iiaApproval;
}