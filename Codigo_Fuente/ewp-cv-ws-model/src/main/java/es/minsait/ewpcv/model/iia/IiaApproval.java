
package es.minsait.ewpcv.model.iia;

import es.minsait.ewpcv.model.StandardDateConverter;
import lombok.Data;
import org.apache.johnzon.mapper.JohnzonConverter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The type Iia Approval.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_IIA_APPROVALS")
@Data
public class IiaApproval implements Serializable {

  private static final long serialVersionUID = -2684795206773117429L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid",  strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  private String id;

  @Column(name = "LOCAL_IIA_ID")
  private String localIia;

  @Column(name = "REMOTE_IIA_ID")
  private String remoteIia;

  @Column(name = "IIA_API_VERSION")
  private String iiaApiVersion;

  @Column(name = "LOCAL_HASH")
  private String localHash;

  @Column(name = "REMOTE_HASH")
  private String remoteHash;

  @Column(name = "DATE_IIA_APPROVAL")
  @JohnzonConverter(StandardDateConverter.class)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateIiaApproval;

  @Column(name = "LOCAL_HASH_V7")
  private String localHashv7;

  @Column(name = "REMOTE_HASH_V7")
  private String remoteHashv7;

  @Column(name = "LOCAL_MESSAGE")
  private byte[] localMessage;

    @Column(name = "REMOTE_MESSAGE")
  private byte[] remoteMessage;
}