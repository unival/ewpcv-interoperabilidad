package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Signature.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_SIGNATURE")
@Getter
@Setter
@NoArgsConstructor
public class Signature implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Column(name = "SIGNER_NAME")
  private String signerName;

  @Column(name = "SIGNER_POSITION")
  private String signerPosition;

  @Column(name = "SIGNER_EMAIL")
  private String signerEmail;

  @Column(name = "TIMESTAMP")
  private Date timestamp;

  @Column(name = "SIGNER_APP")
  private String signerApp;

  // @Lob
  @Column(name = "SIGNATURE")
  private byte[] signature;
}
