package es.minsait.ewpcv.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum La component type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LaComponentType {
  STUDIED_COMPONENT, RECOGNIZED_COMPONENT, VIRTUAL_COMPONENT, BLENDED_COMPONENT, SHORT_TER_DOCTORAL_COMPONENT;

  public static String[] names() {
    LaComponentType[] types = values();
    return Arrays.stream(types).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[types.length]);
  }
}
