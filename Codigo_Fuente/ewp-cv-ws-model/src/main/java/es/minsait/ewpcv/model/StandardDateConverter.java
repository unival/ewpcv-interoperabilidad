package es.minsait.ewpcv.model;

import org.apache.johnzon.mapper.Converter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.DatatypeConverter;

/**
 * The type Standard date converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public class StandardDateConverter implements Converter<Date> {
  @Override
  public String toString(final Date instance) {
    final Calendar cal = GregorianCalendar.getInstance();
    cal.setTime(instance);
    return DatatypeConverter.printDateTime(cal);
  }

  @Override
  public Date fromString(final String text) {
    return DatatypeConverter.parseDateTime(text).getTime();
  }
}
