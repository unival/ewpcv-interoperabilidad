
package es.minsait.ewpcv.model.omobility;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The enum Mobility status.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum MobilityStatus {
  CANCELLED("cancelled"), LIVE("live"), NOMINATION("nomination"), RECOGNIZED("recognized"), REJECTED(
      "rejected"), PENDING("pending"), VERIFIED("verified");

  private final String value;

  MobilityStatus(final String v) {
    value = v;
  }

  public static String[] names() {
    final MobilityStatus[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public static MobilityStatus statusFromValue(final String value) {
    if (Objects.isNull(value)) {
      return null;
    }
    final MobilityStatus[] status = values();
    return Arrays.stream(status).filter(s -> s.value().compareTo(value) == 0).findFirst().orElse(null);
  }

  public String value() {
    return value;
  }
}
