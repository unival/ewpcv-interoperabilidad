package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import es.minsait.ewpcv.model.notificacion.NotificationTypes;
import lombok.Data;

@Entity
@Table(name = "EWPCV_IIA_REGRESSIONS")
@Data
public class IiaRegressions implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
    private String id;

    @Column(name = "IIA_ID", nullable = false)
    private String iiaId;

    @Column(name = "IIA_HASH", nullable = false)
    private String iiaHash;

    @Column(name = "OWNER_SCHAC", nullable = false)
    private String ownerSchac;

    @Column(name = "REGRESSION_DATE", nullable = false)
    private Date regressionDate;
}
