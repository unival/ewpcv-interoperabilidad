
package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Mobility update request.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_MOBILITY_UPDATE_REQUEST")
@Data
public class MobilityUpdateRequest implements Serializable {

  private static final long serialVersionUID = 7239150549088109409L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "SENDING_HEI_ID")
  private String sendingHeiId;

  @Column(name = "TYPE")
  private MobilityUpdateRequestType type;

  // @Lob
  @Column(name = "UPDATE_INFORMATION")
  private byte[] updateInformation;

  @Column(name = "UPDATE_REQUEST_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updateRequestDate;

  @Column(name = "IS_PROCESSING")
  private boolean isProcessing;

  @Column(name = "MOBILITY_TYPE")
  private UpdateRequestMobilityTypeEnum mobilityType;

  @Column(name = "RETRIES")
  private Integer retries;

  @Column(name = "PROCESSING_DATE")
  @Temporal(TemporalType.TIMESTAMP)
  private Date processingDate;

  @Column(name = "SHOULD_RETRY")
  private boolean shouldRetry;

  @Column(name = "LAST_ERROR")
  private String lastError;

}
