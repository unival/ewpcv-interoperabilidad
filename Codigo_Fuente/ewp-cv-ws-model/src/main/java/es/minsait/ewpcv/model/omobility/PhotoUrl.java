package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Photo url.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_PHOTO_URL")
@Getter
@Setter
@NoArgsConstructor
public class PhotoUrl implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "PHOTO_URL_ID", unique = true, nullable = false)
  String id;

  @Column(name = "URL")
  private String url;

  @Column(name = "PHOTO_SIZE")
  private String size;

  @Column(name = "PHOTO_DATE")
  private Date date;

  @Column(name = "PHOTO_PUBLIC")
  private boolean photoPublic;
}
