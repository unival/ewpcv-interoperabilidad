package es.minsait.ewpcv.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * The type Entidad abstracta.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@MappedSuperclass
public abstract class EntidadAbstracta implements Serializable {

  private static final long serialVersionUID = 6822668361629123819L;

  @Version
  private Integer version;

  public EntidadAbstracta() {
    super();
  }

  public abstract String getId();

  public abstract void setId(String id);

  public Integer getVersion() {
    return this.version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }
}
