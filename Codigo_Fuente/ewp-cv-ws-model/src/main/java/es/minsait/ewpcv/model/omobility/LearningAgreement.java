
package es.minsait.ewpcv.model.omobility;


import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.course.TranscriptOfRecord;
import es.minsait.ewpcv.model.organization.Contact;
import lombok.Data;

/**
 * The type Learning agreement.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LEARNING_AGREEMENT")
@Data
@IdClass(LearningAgreementRelationId.class)
public class LearningAgreement implements Serializable {

  private static final long serialVersionUID = 6724617347694900328L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
      strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Id
  @Column(name = "LEARNING_AGREEMENT_REVISION")
  private Integer learningAgreementRevision;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "STUDENT_SIGN")
  private Signature studentSign;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "SENDER_COORDINATOR_SIGN")
  private Signature senderCoordinatorSign;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "RECEIVER_COORDINATOR_SIGN")
  private Signature reciverCoordinatorSign;

  @Column(name = "MODIFIED_DATE")
  private Date modifiedDate;

  // @Lob
  @Column(name = "PDF")
  private byte[] pdf;

  @Column(name = "COMMENT_REJECT")
  private String commentReject;

  @Column(name = "STATUS")
  private LearningAgreementStatus status;

  @Column(name = "CHANGES_ID")
  private String changesId;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_STUDIED_LA_COMPONENT",
      joinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")},
      inverseJoinColumns = @JoinColumn(name = "STUDIED_LA_COMPONENT_ID", referencedColumnName = "ID"))
  private List<LaComponent> studiedLaComponents;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_RECOGNIZED_LA_COMPONENT",
      joinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")},
      inverseJoinColumns = @JoinColumn(name = "RECOGNIZED_LA_COMPONENT_ID", referencedColumnName = "ID"))
  private List<LaComponent> recognizedLaComponents;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_VIRTUAL_LA_COMPONENT",
      joinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")},
      inverseJoinColumns = @JoinColumn(name = "VIRTUAL_LA_COMPONENT_ID", referencedColumnName = "ID"))
  private List<LaComponent> virtualLaComponents;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_BLENDED_LA_COMPONENT",
      joinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")},
      inverseJoinColumns = @JoinColumn(name = "BLENDED_LA_COMPONENT_ID", referencedColumnName = "ID"))
  private List<LaComponent> blendedLaComponents;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_DOCTORAL_LA_COMPONENT",
      joinColumns = {@JoinColumn(name = "LEARNING_AGREEMENT_ID", referencedColumnName = "ID"),
          @JoinColumn(name = "LEARNING_AGREEMENT_REVISION", referencedColumnName = "LEARNING_AGREEMENT_REVISION")},
      inverseJoinColumns = @JoinColumn(name = "DOCTORAL_LA_COMPONENTS_ID", referencedColumnName = "ID"))
  private List<LaComponent> doctoralLaComponents;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "MODIFIED_STUDENT_CONTACT_ID")
  private Contact modifiedStudentContactId;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "TOR_ID")
  private TranscriptOfRecord transcriptOfRecord;

  @Override
  public String toString() {
    return "LearningAgreement{" + "id='" + id + '\'' + ", version=" + version + ", learningAgreementRevision="
        + learningAgreementRevision + ", modifiedDate=" + modifiedDate + ", status=" + status + '}';
  }

}
