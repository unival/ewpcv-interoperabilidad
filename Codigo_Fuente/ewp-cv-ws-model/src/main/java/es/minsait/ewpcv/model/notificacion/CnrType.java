package es.minsait.ewpcv.model.notificacion;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Cnr type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum CnrType {
  REFRESH(0), NOTIFY(1);

  private int value;

  CnrType(int vale) {
    this.value = vale;
  }

  public static String[] names() {
    CnrType[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public int getValue() {
    return value;
  }

  public void setValue(int vale) {
    this.value = vale;
  }
}
