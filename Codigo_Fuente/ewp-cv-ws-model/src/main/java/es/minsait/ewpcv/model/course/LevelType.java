
package es.minsait.ewpcv.model.course;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Level type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LevelType {
  // BACHELOR("bachelor"), MASTER("master"), PhD("phd");
  EQF("eqf");

  private final String value;

  LevelType(final String v) {
    value = v;
  }

  public static String[] names() {
    final LevelType[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public String value() {
    return value;
  }
}
