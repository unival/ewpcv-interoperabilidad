package es.minsait.ewpcv.model.notificacion;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Notification types.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum NotificationTypes {
  // OJO: Los valores han de ser correlativos. Si no, hay que implementar un converter como en SiNoConverter.
  IIA(0), OMOBILITY(1), IMOBILITY(2), IMOBILITY_TOR(3), IIA_APPROVAL(4), LA(5), ECHO(6), FACTSHEET(7), INSTITUTIONS(
      8), MANIFEST(9), OMOBILITY_LAS(10), ORGANIZATION_UNIT(11), UNDEFINED(12), STATISTICS_OLA_NAMESPACE(13), STATISTICS_ILA_NAMESPACE(14), STATISTICS_IIA_NAMESPACE(15), FILE_API(16);

  private int value;

  NotificationTypes(final int value) {
    this.value = value;
  }

  public static String[] names() {
    final NotificationTypes[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public int getValue() {
    return value;
  }

  public void setValue(final int value) {
    this.value = value;
  }
}
