
package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.omobility.ResultDistribution;
import lombok.Data;

/**
 * The type Learning opportunity instance.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LOI")
@Data
public class LearningOpportunityInstance implements Serializable {

  private static final long serialVersionUID = -2604144519613228629L;

  @Id
  @GeneratedValue(generator = "useExistingIdOrGenerateUuidGenerator")
  @GenericGenerator(name = "useExistingIdOrGenerateUuidGenerator",
      strategy = "es.minsait.ewpcv.repository.utiles.UseExistingIdOrGenerateUuidGenerator")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "ENGAGEMENT_HOURS")
  private BigDecimal engagementHours;

  @Column(name = "LANGUAGE_OF_INSTRUCTION")
  private String languageOfInstruction;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "GRADING_SCHEME_ID", referencedColumnName = "ID")
  private GradingScheme gradingScheme;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "RESULT_DISTRIBUTION", referencedColumnName = "ID")
  private ResultDistribution resultDistribution;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(name = "EWPCV_LEVELS", joinColumns = @JoinColumn(name = "LOI_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "LEVEL_ID", referencedColumnName = "ID"))
  private List<Level> levels;

  @OneToOne(mappedBy = "loi")
  private LoiGrouping loiGrouping;

  @Column(name = "START_DATE")
  private Date startDate;

  @Column(name = "END_DATE")
  private Date endDate;

  @Column(name = "PERCENTAGE_LOWER")
  private Long percentageLower;

  @Column(name = "PERCENTAGE_EQUAL")
  private Long percentageEqual;

  @Column(name = "PERCENTAGE_HIGHER")
  private Long percentageHigher;

  @Column(name = "RESULT_LABEL")
  private String resultLabel;

  @Column(name = "STATUS")
  private LoiStatus status;

  @Column(name = "EXTENSION")
  private byte[] extension;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_LOI_ATT", joinColumns = @JoinColumn(name = "LOI_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ATTACHMENT_ID", referencedColumnName = "ID"))
  private List<Attachment> attachments;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "DIPLOMA_ID")
  private Diploma diploma;

  @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
  @JoinTable(name = "EWPCV_LOS_LOI", joinColumns = @JoinColumn(name = "LOI_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "LOS_ID", referencedColumnName = "ID"))
  private LearningOpportunitySpecification learningOpportunitySpecification;
}
