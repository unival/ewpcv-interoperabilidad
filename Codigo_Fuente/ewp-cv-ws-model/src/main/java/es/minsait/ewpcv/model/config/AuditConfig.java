package es.minsait.ewpcv.model.config;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The type Audit config.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_AUDIT_CONFIG")
@Data
public class AuditConfig implements Serializable {

  private static final long serialVersionUID = 8130079757486819172L;

  @Id
  @Column(name = "NAME")
  private String name;

  @Column(name = "VALUE")
  private String value;

}
