package es.minsait.ewpcv.model.omobility;

import java.io.Serializable;

import lombok.Data;

/**
 * The type Learning agreement relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class LearningAgreementRelationId implements Serializable {
  private static final long serialVersionUID = 8971914881907667602L;
  private String id;
  private int learningAgreementRevision;
}
