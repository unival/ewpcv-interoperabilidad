package es.minsait.ewpcv.model.organization;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.omobility.PhotoUrl;
import lombok.Data;


/**
 * The type Contact details.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_CONTACT_DETAILS")
@Data
public class ContactDetails implements Serializable {

  private static final long serialVersionUID = 682260206392998614L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "FAX_NUMBER")
  private PhoneNumber faxNumber;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "MAILING_ADDRESS")
  private FlexibleAddress mailingAddress;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "PHONE_NUMBER")
  private PhoneNumber phoneNumber;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "STREET_ADDRESS")
  private FlexibleAddress streetAddress;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_CONTACT_URL")
  private List<LanguageItem> url;

  @ElementCollection
  @CollectionTable(name = "EWPCV_CONTACT_DETAILS_EMAIL")
  private List<String> email;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "CONTACT_DETAILS_ID", nullable = false)
  private List<PhotoUrl> photoUrls;


}
