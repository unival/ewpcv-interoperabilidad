
package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.Contact;
import lombok.Data;

/**
 * The type Iia partner.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_IIA_PARTNER")
@Data
public class IiaPartner implements Serializable {

  private static final long serialVersionUID = 7019557359826044184L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "ORGANIZATION_UNIT_ID")
  private String organizationUnitId;

  @Column(name = "SIGNING_DATE")
  private Date signingDate;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "SIGNER_PERSON_CONTACT_ID")
  private Contact signerPersonId;

  @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
  @JoinTable(name = "EWPCV_IIA_PARTNER_CONTACTS")
  private List<Contact> contacts;

}
