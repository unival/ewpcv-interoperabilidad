package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import es.minsait.ewpcv.model.omobility.LanguageSkill;
import es.minsait.ewpcv.model.omobility.SubjectArea;
import lombok.Data;

/**
 * The type Cooperation condition subj area lang skill.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_COOPCOND_SUBAR_LANSKIL")
@Data
public class CooperationConditionSubjAreaLangSkill implements Serializable {

  private static final long serialVersionUID = -8519850324614949295L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  private String id;

  @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
  @JoinColumn(name = "ISCED_CODE", referencedColumnName = "ID")
  private SubjectArea subjectArea;

  @ManyToOne
  @JoinColumn(name = "LANGUAGE_SKILL_ID")
  private LanguageSkill languageSkill;

  @Column(name = "ORDER_INDEX_LANG")
  private Integer orderIndexLang;

}
