
package es.minsait.ewpcv.model.omobility;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Data;

/**
 * The type Result distribution.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_RESULT_DISTRIBUTION")
@Data
public class ResultDistribution implements Serializable {

  private static final long serialVersionUID = -7400481907040556249L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;


  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "RESULT_DIST_ID", nullable = false)
  private List<ResultDistributionCategory> resultDistributionCategory;


  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_DISTR_DESCRIPTION")
  private List<LanguageItem> description;

}
