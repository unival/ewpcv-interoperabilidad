package es.minsait.ewpcv.model.iia;

import java.io.Serializable;

import es.minsait.ewpcv.model.organization.Contact;
import lombok.Data;

/**
 * The type Iia partner contacts relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Data
public class IiaPartnerContactsRelationId implements Serializable {
  private static final long serialVersionUID = 2983079722766864925L;
  private String iiaId;
  private Contact contactsId;
}
