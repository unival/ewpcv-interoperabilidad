
package es.minsait.ewpcv.model.course;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Attachment type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum AttachmentType {
  DIPLOMA("Diploma"), DIPLOMA_SUPPLEMENT("Diploma Supplement"), TOR("Transcript of Records"), EMREX_TRANSCRIPT(
      "EMREX transcript"), MICRO_CREDENTIAL("Micro Credential"), LETTER_NOMINATION("Letter of Nomination"), CERTIFICATE(
          "Certificate of Training"), LEARNING_AGREMENT("Learning Agreement"), OTHER("Other");

  private final String value;

  AttachmentType(final String v) {
    value = v;
  }

  public static String[] names() {
    final AttachmentType[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }

  public String value() {
    return value;
  }
}
