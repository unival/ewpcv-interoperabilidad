
package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Academic year.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_ACADEMIC_YEAR")
@Data
public class AcademicYear implements Serializable {

  private static final long serialVersionUID = 6114337618375700712L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  private String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "END_YEAR")
  private String endYear;

  @Column(name = "START_YEAR")
  private String startYear;

}
