package es.minsait.ewpcv.model.omobility;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Learning agreement status.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum LearningAgreementStatus {
  CHANGES_PROPOSED, FIRST_VERSION, APROVED, REJECTED, DISCARDED;

  public static String[] names() {
    LearningAgreementStatus[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }
}
