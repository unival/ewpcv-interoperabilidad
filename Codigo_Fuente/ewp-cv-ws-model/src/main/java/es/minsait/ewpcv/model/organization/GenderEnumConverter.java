package es.minsait.ewpcv.model.organization;

import java.math.BigInteger;
import java.util.Objects;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * The type Gender enum converter.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Converter
public class GenderEnumConverter implements AttributeConverter<Gender, BigInteger> {

  @Override
  public BigInteger convertToDatabaseColumn(final Gender gender) {
    return Objects.nonNull(gender) ? gender.value() : null;
  }

  @Override
  public Gender convertToEntityAttribute(final BigInteger bigInteger) {
    return Gender.genderFromValue(bigInteger);
  }
}
