package es.minsait.ewpcv.model.fileApi;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type StatisticsOla.
 *
 * @author Iván Lanciano (ilanciano@minsait.com)
 */
@Entity
@Table(name = "EWPCV_FILES")
@Data
public class File implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false, columnDefinition = "BINARY(16)")
  String id;

  @Column(name = "FILE_ID")
  private String fileId;

  @Column(name = "SCHAC")
  private String schac;

  @Column(name = "FILE_CONTENT")
  private byte[] fileContent;

  @Column(name="MIME_TYPE")
  private String mimeType;


}
