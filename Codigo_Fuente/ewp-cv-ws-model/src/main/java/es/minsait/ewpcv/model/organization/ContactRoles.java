
package es.minsait.ewpcv.model.organization;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Contact roles.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum ContactRoles {
  ADMISSION, COURSE, HOUSING, INSURANCE, VISAS;

  public static String[] names() {
    ContactRoles[] statuses = values();
    return Arrays.stream(statuses).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[statuses.length]);
  }
}
