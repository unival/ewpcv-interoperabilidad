
package es.minsait.ewpcv.model.organization;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Information item type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum InformationItemType {
  HOUSING, VISA, INSURANCE;

  public static String[] types() {
    InformationItemType[] types = values();
    return Arrays.stream(types).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[types.length]);
  }
}
