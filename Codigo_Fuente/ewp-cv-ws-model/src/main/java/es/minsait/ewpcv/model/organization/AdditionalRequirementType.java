
package es.minsait.ewpcv.model.organization;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The enum Additional requirement type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum AdditionalRequirementType {
  INFRASTRUCTURE, SERVICE, REQUIREMENT;

  public static String[] types() {
    AdditionalRequirementType[] types = values();
    return Arrays.stream(types).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[types.length]);
  }
}
