
package es.minsait.ewpcv.model.iia;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

/**
 * The type Mobility type.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_MOBILITY_TYPE")
@Data
public class MobilityType implements Serializable {

  private static final long serialVersionUID = -4470599040226589760L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Column(name = "MOBILITY_CATEGORY")
  private String mobilityCategory;

  @Column(name = "MOBILITY_GROUP")
  private String mobilityGroup;

}
