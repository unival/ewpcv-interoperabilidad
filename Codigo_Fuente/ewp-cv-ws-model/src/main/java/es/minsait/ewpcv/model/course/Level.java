package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The type Level.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_LEVEL")
@Getter
@Setter
@NoArgsConstructor
public class Level implements Serializable {

  private static final long serialVersionUID = 3028009532203741470L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Column(name = "LEVEL_TYPE", nullable = false)
  private LevelType type;

  @Column(name = "LEVEL_VALUE", nullable = false)
  private String value;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_LEVEL_DESCRIPTION",
      joinColumns = @JoinColumn(name = "LEVEL_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "LEVEL_DESCRIPTION_ID", referencedColumnName = "ID"))
  private List<LanguageItem> description;

}
