
package es.minsait.ewpcv.model.organization;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The enum Gender.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum Gender {
  NOT_KNOWN(BigInteger.ZERO), MALE(BigInteger.ONE), FEMALE(BigInteger.valueOf(2)), NOT_APPLICABLE(
      BigInteger.valueOf(9));

  private final BigInteger value;

  Gender(final BigInteger v) {
    value = v;
  }

  public static String[] names() {
    final Gender[] genders = values();
    return Arrays.stream(genders).map(s -> s.name()).collect(Collectors.toList()).toArray(new String[genders.length]);
  }

  public BigInteger value() {
    return value;
  }

  public static Gender genderFromValue(final BigInteger value) {
    if (Objects.isNull(value)) {
      return null;
    }
    final Gender[] genders = values();
    return Arrays.stream(genders).filter(g -> g.value().compareTo(value) == 0).findFirst().orElse(null);
  }
}
