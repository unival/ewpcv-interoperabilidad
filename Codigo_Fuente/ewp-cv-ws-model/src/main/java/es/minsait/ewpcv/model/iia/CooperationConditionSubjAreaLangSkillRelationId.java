package es.minsait.ewpcv.model.iia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

/**
 * The type Cooperation condition subj area lang skill relation id.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Embeddable
@Data
public class CooperationConditionSubjAreaLangSkillRelationId implements Serializable {

  private static final long serialVersionUID = 6368878071034625008L;

  @Column(name = "COOPERATION_CONDITION_ID", nullable = false)
  private String cooperationConditionId;
  @Column(name = "ISCED_CODE", nullable = false)
  private String subjectArea;
  @Column(name = "LANGUAGE_SKILL_ID", nullable = false)
  private String languageSkill;

}
