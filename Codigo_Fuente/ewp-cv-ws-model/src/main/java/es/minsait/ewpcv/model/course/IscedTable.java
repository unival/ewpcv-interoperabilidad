package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The type Isced table.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_ISCED_TABLE")
@Getter
@Setter
@NoArgsConstructor
public class IscedTable implements Serializable {
  private static final long serialVersionUID = -2772710177892629740L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Column(name = "ISCED_CODE")
  private String iscedCode;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "ISCED_TABLE_ID", nullable = false)
  private List<GradeFrequency> frequencies;
}
