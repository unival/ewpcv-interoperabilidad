package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Data;

/**
 * The type Grading scheme.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_GRADING_SCHEME")
@Data
public class GradingScheme implements Serializable {

  private static final long serialVersionUID = -7714466344777453044L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_GRADING_SCHEME_LABEL",
      joinColumns = @JoinColumn(name = "GRADING_SCHEME_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "LABEL_ID", referencedColumnName = "ID"))
  private List<LanguageItem> label;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_GRADING_SCHEME_DESC",
      joinColumns = @JoinColumn(name = "GRADING_SCHEME_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "DESCRIPTION_ID", referencedColumnName = "ID"))
  private List<LanguageItem> description;

}
