
package es.minsait.ewpcv.model.course;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import es.minsait.ewpcv.model.organization.LanguageItem;
import lombok.Data;

/**
 * The type Academic term.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
@Entity
@Table(name = "EWPCV_ACADEMIC_TERM")
@Data
public class AcademicTerm implements Serializable {

  private static final long serialVersionUID = 4394267287691573092L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Column(name = "ID", unique = true, nullable = false)
  String id;

  @Version
  @Column(name = "VERSION")
  private Integer version;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "END_DATE")
  private Date endDate;

  @Column(name = "INSTITUTION_ID")
  private String institutionId;

  @Column(name = "ORGANIZATION_UNIT_ID")
  private String organizationUnitId;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "START_DATE")
  private Date startDate;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "ACADEMIC_YEAR_ID", referencedColumnName = "ID")
  private AcademicYear academicYear;

  @Column(name = "TOTAL_TERMS")
  private BigInteger totalTerms;

  @Column(name = "TERM_NUMBER")
  private BigInteger termNumber;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinTable(name = "EWPCV_ACADEMIC_TERM_NAME")
  private List<LanguageItem> dispName;

}
