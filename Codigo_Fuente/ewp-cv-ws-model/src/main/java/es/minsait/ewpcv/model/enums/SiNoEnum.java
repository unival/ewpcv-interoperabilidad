package es.minsait.ewpcv.model.enums;

/**
 * The enum Si no enum.
 *
 * @author Juan Vicente Marco (jvmarco@minsait.com); Jose Alberto Frias (jafcastillejo@minsait.com); Luis Sanchez (lsanchezpe@minsait.com)
 */
public enum SiNoEnum {
  SI("S"), NO("N");

  /** The value. */
  private final String descripcion;

  private SiNoEnum(String descripcion) {
    this.descripcion = descripcion;
  }

  public static SiNoEnum fromCodigo(String codigo) {
    for (SiNoEnum valor : SiNoEnum.values()) {
      if (valor.getValue().contentEquals(codigo)) {
        return valor;
      }
    }
    return null;
  }

  public String getValue() {
    return this.descripcion;
  }

  @Override
  public String toString() {
    return this.descripcion;
  }
}
