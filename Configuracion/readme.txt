Las modificaciones a realizar sobre esta carpeta son:

- Incluir el fichero con el certificado PKCS12

- Configurar las siguientes propuiedades 
		- logging.config: Ruta absoluta en el sistema de archivos del servidor al fichero de configuración de LOGS.
		- logging.file: Ruta absoluta en el sistema de archivos del servidor al fichero de LOGS. 
		- ewp.name: Nombre de la universidad.
		- ewp.host.name: Nombre del host de la URL pública.
		- ewp.base.uri: URL pública base del despliegue.
		- ewp.keystore.location: Ruta absoluta en el sistema de archivos del servidor al almacen de certificado PKCS12.
		- ewp.keystore.password: Pasword del keysotre (debe ser igual a la del alias en caso de haberla).
		- ewp.keystore.certificate.alias: Alias del certificado a utilizar para la autenticación.
		- ewp.truststore.location= Ruta absoluta en el sistema de archivos del servidor al trust store
		- ewp.truststore.password= Pasword del trust store
		- manifestFile: Ruta absoluta en el sistema de archivos del servidor del fichero Manifest, por ejemplo 
		


		
- En el caso de utilizar SQL SERVER, modificar :
		- spring.jpa.database-platform=org.hibernate.dialect.SQLServer2012Dialect
		- spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.SQLServer2012Dialect